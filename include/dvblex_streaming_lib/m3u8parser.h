/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once
#include <string>
#include <vector>

#define M3U8_EXTM3U_TAG             "#EXTM3U"
#define M3U8_EXTINF_TAG             "#EXTINF"
#define M3U8_TARGET_DURATION_TAG    "#EXT-X-TARGETDURATION"
#define M3U8_MEDIA_SEQUENCE_TAG     "#EXT-X-MEDIA-SEQUENCE"
#define M3U8_EXT_KEY_TAG            "#EXT-X-KEY"
#define M3U8_EXT_STREAM_INF_TAG     "#EXT-X-STREAM-INF"
#define M3U8_EXT_VERSION_TAG        "#EXT-X-VERSION"
#define M3U8_EXT_END_LIST_TAG       "#EXT-X-ENDLIST"

#define M3U8_SI_PROGRAM_ID          "PROGRAM-ID"
#define M3U8_SI_BANDWIDTH           "BANDWIDTH"
#define M3U8_SI_CODECS              "CODECS"
#define M3U8_SI_RESOLUTION          "RESOLUTION"

#define M3U8_LAST_SEGMENT_ID	    -1

enum m3u8_tag
{
    tag_unknown = 0,
    tag_extm3u,
    tag_extinf,
    tag_ext_x_targetduration,
    tag_ext_x_media_sequence,
    tag_ext_x_key,
    tag_ext_x_stream_inf,
    tag_ext_x_version,
    tag_ext_x_endlist
};

struct m3u8_play_list
{
    m3u8_play_list() : bandwidth(-1), programId(-1) {}
    void reset() { *this = m3u8_play_list(); }

	int bandwidth;
	int programId;
	std::string url;
    std::string codecs;
    std::string resolution;
};

typedef std::vector<m3u8_play_list> m3u8_playlist_list;

struct m3u8_media_segment
{
    m3u8_media_segment() : duration(0), last(false), encrypted(false) {}
    void reset() { *this = m3u8_media_segment(); }

	std::string url;
	std::string title;
	unsigned long duration; // duration in *milliseconds* (0, if unknown)
	bool last;
    
    bool encrypted;
    std::string aes_iv;
    std::string aes_key;
};

struct m3u8_media_segments
{
	int sequenceBaseNum;
	int targetDuration;
	std::vector<m3u8_media_segment> segments;
};

bool m3u8_parse(const std::string& root_url, const char* contents, m3u8_media_segments& segments,
    m3u8_playlist_list& playlists, const char* user_agent);

const std::string url_header_separator = "://";

std::string hls_make_full_url(const std::string& root_url, std::string& suffix);
