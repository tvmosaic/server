/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once
#include <string>
#include <vector>
#include <dl_errors.h>
#include <dl_socket_api.h>

namespace dvblink {
namespace streaming {

struct satip_server_info
{
    unsigned int device_id; // DEVICEID.SES.COM
    unsigned int boot_id;   // BOOTID.UPNP.ORG
    unsigned int config_id; // CONFIGID.UPNP.ORG
    std::string descr_url;  // LOCATION
    std::string usn;        // USN
    dvblink::sock_addr device_addr;
};

const std::string dvbs_fe_key = "dvbs";
const std::string dvbc_fe_key = "dvbc";
const std::string dvbt_fe_key = "dvbt";

typedef std::map<std::string, int> sat2ip_fe_num_map_t;

struct sat2ip_server_desc
{
    std::string friendly_name;
    std::string manufacturer;
    std::string model_name;
    std::string model_number;
    std::string udn;
    std::string uri;
    std::string device_type;
    sat2ip_fe_num_map_t fe_num_map;

    int get_fe_num(const std::vector<std::string>& fe_to_combine);
    void parse_frontends(const std::string& fe_str);

};

typedef std::vector<satip_server_info> satip_servers_t;
const timeout_t discovery_timeout = boost::posix_time::milliseconds(4000);

typedef bool (*sat2ip_new_server_cb_func)(satip_servers_t* list, void* user_param);

errcode_t discover_satip_servers(satip_servers_t& list,
    timeout_t to = discovery_timeout,
    sat2ip_new_server_cb_func cb_func = NULL,
    void* user_param = NULL);

errcode_t discover_satip_servers(dvblink::sock_addr sa,
    satip_servers_t& list, timeout_t to = discovery_timeout,
    sat2ip_new_server_cb_func cb_func = NULL, void* user_param = NULL);

errcode_t get_server_props_from_description_xml(const std::string& desc_url, sat2ip_server_desc& desc);

} // namespace streaming
} // namespace dvblink

// $Id: sat-ip.h 12666 2016-04-13 13:32:39Z pashab $
