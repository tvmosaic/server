/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once
#include <vector>
#include <string>
#include <boost/thread.hpp>
#include <boost/cstdint.hpp>
#include <dl_event.h>
#include "m3u8parser.h"

namespace dvblink {
namespace streaming {

class playback_segments
{
public:
    explicit playback_segments(const m3u8_media_segments& segments);
    ~playback_segments();

private:
    typedef std::string buffer_type;
    typedef boost::shared_ptr<buffer_type> buffer_ptr;

    class buffer_pool
    {
    public:
        explicit buffer_pool(size_t size);
        
        buffer_ptr get_buffer();
        void put_buffer(buffer_ptr ptr);

    private:
        boost::mutex lock_;
        std::vector<buffer_ptr> pool_;
    };

    struct segment_info
    {
        segment_info() :
            segment_size(0), estimated_size(0), load_failed_(false) {}
        bool decrypt_data();

        m3u8_media_segment seg;
        buffer_ptr buf_ptr;     // pointer to segment data
        size_t segment_size;    // segment size, in bytes (0 if unknown)
        size_t estimated_size;  // estimated segment size (for seek)
        bool load_failed_;
    };

    typedef std::vector<segment_info> segment_list_t;
    typedef boost::shared_ptr<boost::thread> thread_ptr;

public:
    bool initialize();
    bool read_data(unsigned char* buffer, size_t& len);
    bool seek(boost::uint64_t offset);

    boost::uint64_t get_full_size() const
    {
        return seek_size_;
    }

private:
    bool shutdown();
    void segment_loader();
    void update_eof();
    bool load_segment(unsigned int seg_num);
    unsigned int find_segment_by_offset() const;
    bool move_to_next_segment(unsigned int cur_seg);
    size_t get_relative_offset(unsigned int cur_seg) const;
    void free_unused_segments(unsigned int cur_seg);

private:
    mutable boost::mutex lock_;
    thread_ptr thread_ptr_;
    unsigned long target_duration_; // duration in seconds
    segment_list_t segment_list_;
    boost::uint64_t offset_;        // current offset (in bytes)
    boost::uint64_t eof_;

    dvblink::event read_event_;
    dvblink::event load_event_;
    
    boost::uint64_t seek_size_;
    boost::uint64_t seek_offs_;
    bool seeking_;

    buffer_type tmpbuf_;
    buffer_pool pool_;
    bool shutdown_;
};

typedef boost::shared_ptr<playback_segments> playback_segments_ptr;

} // namespace streaming
} // namespace dvblink

// $Id: playback_segments.h 7823 2013-02-08 09:16:40Z mike $
