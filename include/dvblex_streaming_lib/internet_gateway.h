/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once
#include <string>
#include <vector>
#include <dl_errors.h>
#include <dl_socket_api.h>

namespace dvblink {
namespace streaming {

struct igd_desc_t
{
    std::string friendly_name;
    std::string udn;
    std::string service_type;
    std::string service_url;
};

struct igd_info_t
{
    //taken from upnp broadcast info
    std::string descr_url;  // LOCATION
    std::string usn;        // USN
    dvblink::sock_addr device_addr;
    dvblink::sock_addr local_addr;
    //taken from description url
    errcode_t descr_processing_result;
    igd_desc_t gateway_descr;
};

struct port_mapping_desc_t
{
    unsigned short ext_port;
    unsigned short int_port;
    std::string protocol;
    std::string description;
};

typedef std::vector<igd_info_t> igd_list_t;

const timeout_t igd_discovery_timeout = boost::posix_time::milliseconds(2000);

errcode_t discover_internet_gateways(igd_list_t& igd_list, timeout_t to = igd_discovery_timeout);

errcode_t add_port_mapping(igd_info_t& igd_info, port_mapping_desc_t& port_mapping, std::string& raw_response);

errcode_t remove_port_mapping(igd_info_t& igd_info, port_mapping_desc_t& port_mapping, std::string& raw_response);

} // namespace streaming
} // namespace dvblink

