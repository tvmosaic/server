/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread.hpp>
#include <dl_network_helper.h>
#include <dl_http_comm.curl.h>
#include "m3u8segmentlist.h"

class m3u8_segment_list;

class m3u8_loader
{
public:
	m3u8_loader(const char* user_agent = NULL);
	~m3u8_loader();

	bool Start(const char* url, m3u8_segment_list* segmentList);
	bool Stop();

protected:
    void UpdateThread();
    void GetSegmentList(const char* m3u8file, m3u8_media_segment_list& segments);
    bool GetActualPlaylist(const char* m3u8file, std::string& playlist_url);

	std::string m_URL;
	m3u8_segment_list* m_SegmentList;
	bool m_ExitThread;
	boost::thread* m_UpdateThread;
	std::string user_agent_;
};
