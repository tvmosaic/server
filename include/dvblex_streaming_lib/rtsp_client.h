/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once
#include <string>
#include <boost/date_time.hpp>
#include <dl_socket_api.h>
#include "http_utils.h"

namespace dvblink {
namespace streaming {

const timeout_t _connect_timeout = boost::posix_time::milliseconds(4000);
const timeout_t _receive_timeout = boost::posix_time::milliseconds(4000);

class rtsp_client
{
public:
    explicit rtsp_client(const std::string& user_agent = "");
    virtual ~rtsp_client();

    bool connect(const std::string& rtsp_server_addr,
        timeout_t connect_timeout = _connect_timeout,
        unsigned short rtsp_server_port = 554);
    bool disconnect();
    bool is_connected() const;

    bool prepare_request(const std::string& req, std::string& new_req);
    bool send_request(const std::string& req);
    bool receive_response(std::string& status_line,
        http_headers& headers, std::string& body,
        timeout_t receive_timeout = _receive_timeout);

    void get_last_received_buffer(std::string& buffer);

protected:
    std::string user_agent_;
    tcp_socket::sock_ptr sock_ptr_;
    unsigned int cseq_;
    std::string session_id_;
    unsigned int session_timeout_; // seconds
    std::string buffer_;
    size_t data_size_;
};

} // namespace streaming
} // namespace dvblink

// $Id: rtsp_client.h 6677 2012-10-18 14:27:57Z mike $
