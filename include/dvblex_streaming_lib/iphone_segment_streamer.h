/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread.hpp>
#include <dl_ts_info.h>
#include <dl_circle_buffer.h>
#include <dl_ts_aligner.h>

class m3u8_segment_list;
class m3u8_media_segmentEx;

typedef void (*LP_IPHONE_STREAMER_TS_SEND_FUNC)(unsigned char* buf, int len, void* param);

class hls_segment_streamer
{
public:
	hls_segment_streamer(const char* user_agent = NULL);
	~hls_segment_streamer();

    bool StartReading(m3u8_segment_list* seglist, LP_IPHONE_STREAMER_TS_SEND_FUNC cbfunc, void* param);
	bool StartStreaming();
	bool Stop();

protected:
    enum EStreamerState
    {
        ESS_PAT,
        ESS_PMT,
        ESS_STREAMING
    };

    void ReadThread();
    void StreamingThread();
	void ReadSegment(m3u8_media_segment_ex* segment);
	static void SendStreamingData(const unsigned char* buf, int len, void* param);
	void ProcessReadSegment(unsigned char* buf, int len);
    void ProcessPAT(unsigned char* buf);
    void ProcessPMT(unsigned char* buf);
    void SendBuffer(unsigned char* buf, int len);
    __int64 GetBufferMaxPCR(unsigned char* buffer, int len);
    static void __stdcall AlignerCallback(const unsigned char* buf, unsigned long len, void* user_param);

    unsigned short m_NewPMTContinuityCounter;
	dvblink::engine::ts_section_payload_parser m_sectionParser;
    unsigned short m_pmtPid;
    unsigned short m_pcrPid;
    dvblink::engine::ts_packet_generator m_PacketGen;
    unsigned char* m_pmt_section;
    int m_pmt_section_len;

	m3u8_segment_list* m_SegmentList;
	LP_IPHONE_STREAMER_TS_SEND_FUNC m_Callback;
	void* m_UserParam;
	bool m_ExitThread;
	boost::thread* m_StreamingThread;
	boost::thread* m_ReadThread;
	dvblink::engine::ts_circle_buffer* m_readBuffer;
	unsigned long m_readBufferNodeSize;
	unsigned char* m_recvBuf;
	EStreamerState m_State;
    double m_prebufferingTime;
    dvblink::engine::ts_packet_aligner packet_aligner_;
    std::string user_agent_;
};
