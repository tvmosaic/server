/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include "m3u8parser.h"
#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/locks.hpp>
#include <list>
#include <map>

class m3u8_media_segment_ex : public m3u8_media_segment
{
public:
	int sequenceBaseNum;

	m3u8_media_segment_ex(){};
	m3u8_media_segment_ex(int seqnum, m3u8_media_segment& seg)
	{
		url = seg.url;
		title = seg.title;
		duration = seg.duration;
		last = seg.last;
		sequenceBaseNum = seqnum;
        
        encrypted = seg.encrypted;
        aes_key = seg.aes_key;
        aes_iv = seg.aes_iv;
	}

	bool operator < (const m3u8_media_segment_ex& item)
	{
		if (sequenceBaseNum == -1)
			return false;
		return sequenceBaseNum < item.sequenceBaseNum;
	}
};


typedef std::list<m3u8_media_segment_ex> m3u8_media_segment_list;

class m3u8_segment_list
{
public:
	m3u8_segment_list();
	~m3u8_segment_list();

	void AddSegments(m3u8_media_segment_list& segments);
	bool PopFirstPlayableSegment(m3u8_media_segment_ex& segment);
	void Clear();

protected:
	m3u8_media_segment_list m_SegmentList;
    boost::shared_mutex m_Lock;
	void SortSegmentList();
	void CheckAndAddSegment(m3u8_media_segment_ex& segment);
	bool GetMaxSeqNum(int& maxnum);
};
