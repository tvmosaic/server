/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <dl_ts_info.h>
#include <dl_fnv.h>

namespace dvblex {

//generic tags
const std::string m3u_playlist_header = "#EXTM3U";
const std::string m3u_playlist_inf_section = "#EXTINF";
const std::string m3u_playlist_extgrp_header = "#EXTGRP";
const std::string m3u_playlist_exttv_header = "#EXTTV";

static const std::string m3u_hls_content_type = "application/vnd.apple.mpegurl";

//vlc specific
const std::string m3u_playlist_extvlc_option = "#EXTVLCOPT";

//m3u plus
static const std::string m3u_plus_tvg_id = "tvg-id";
static const std::string m3u_plus_tvg_name = "tvg-name";
static const std::string m3u_plus_tvg_logo = "tvg-logo";
static const std::string m3u_plus_tvg_shift = "tvg-shift";
static const std::string m3u_plus_group_title = "group-title";
static const std::string m3u_plus_audio_track = "audio-track";
static const std::string m3u_plus_aspect_ratio = "aspect-ratio";

//extra - specific to tvmosaic
static const std::string m3u_plus_tvg_chno = "tvg-chno";
static const std::string m3u_plus_tvg_type = "tvg-type";
static const std::string m3u_plus_tvg_type_tv = "tv";
static const std::string m3u_plus_tvg_type_radio = "radio";

} // namespace dvblex
