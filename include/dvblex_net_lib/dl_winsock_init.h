/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_WINSOCK_INIT_H__
#define __DVBLINK_WINSOCK_INIT_H__

#ifndef _WIN32
#error Win32 specific code
#endif

// force compiler to instantiate the _winsock_initializer object
#ifdef _WIN64
#pragma comment(linker, "/include:_winsock_initializer")
#else
#pragma comment(linker, "/include:__winsock_initializer")
#endif

#include <boost/noncopyable.hpp>

namespace dvblink {

class winsock_init : public boost::noncopyable
{
public:
    winsock_init();
    ~winsock_init();
};

} // namespace dvblink

#endif  // __DVBLINK_WINSOCK_INIT_H__

// $Id: dl_winsock_init.h 2012 2011-03-03 11:48:15Z mike $
