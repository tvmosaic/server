/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_HTTP_COMM_CURL_H__
#define __DVBLINK_HTTP_COMM_CURL_H__

#include <string>
#include <vector>
#include <boost/noncopyable.hpp>

#ifdef _WIN32
#ifdef NDEBUG
#pragma comment(lib, "libcurl.lib")
#pragma comment(lib, "libeay32.lib")
#pragma comment(lib, "ssleay32.lib")
#else
#pragma comment(lib, "libcurld.lib")
#pragma comment(lib, "libeay32d.lib")
#pragma comment(lib, "ssleay32d.lib")
#endif
#pragma comment(lib, "wldap32.lib")
#pragma comment(lib, "ws2_32.lib")
#endif

#ifndef INTERNET_DEFAULT_HTTP_PORT
#define INTERNET_DEFAULT_HTTP_PORT 80
#endif

#ifndef INTERNET_DEFAULT_HTTPS_PORT
#define INTERNET_DEFAULT_HTTPS_PORT 443
#endif

namespace dvblink {

enum http_auth
{
    http_auth_any,
    http_auth_anysafe,
    http_auth_basic,
    http_auth_digest,
    http_auth_digest_ie,
    http_auth_gss_negotiate,
    http_auth_ntlm,
    http_auth_ntlm_wb
};

class http_comm_handler : public boost::noncopyable
{
public:
    http_comm_handler(const char* session_name,
        const char* address, const char* login,
        const char* pswd, unsigned short port,
        const char* data_dir = NULL,
        const char* ca_cert_name = NULL, // ca-certificates.crt
        const char* client_cert_name = NULL);

    http_comm_handler(const char* session_name,
        const char* address, const char* login,
        const char* pswd, unsigned short port,
        const char* data_dir,
        const char* ca_cert_name, // ca-certificates.crt
        const char* client_cert,
        const char* client_priv_key);

    virtual ~http_comm_handler();

    http_comm_handler* Clone();

    bool Init();
    bool Term();

    void set_connection_timeout_sec(long timeout_sec){timeout_sec_ = timeout_sec;}
    long get_connection_timeout_sec(){return timeout_sec_;}

    typedef std::vector<std::string> http_headers_t;

    bool ExecuteGetWithResponseGZ(const char* url_suffix,
        bool bSecure, std::string& response,
        http_headers_t* headers = NULL,
        http_headers_t* out_headers = NULL);

    bool ExecuteGetWithResponse(const char* url_suffix,
        bool bSecure, std::string& response,
        http_headers_t* headers = NULL,
        http_headers_t* out_headers = NULL);

    bool ExecutePostWithResponse(const char* url_suffix,
        const char* post_data, bool bSecure, std::string& response,
        http_headers_t* headers = NULL,
        http_headers_t* out_headers = NULL);

	bool ExecutePostDataWithResponse(const char* url_suffix, 
        const char* post_data, size_t post_data_size, bool bSecure, std::string& response, 
        http_headers_t* in_headers = NULL, 
        http_headers_t* out_headers = NULL);

    bool ExecutePutWithResponse(const char* url_suffix,
      const char* put_data, size_t put_data_size, bool bSecure, std::string& response, 
        http_headers_t* in_headers = NULL,
        http_headers_t* out_headers = NULL);

    bool ExecuteDeleteWithResponse(const char* url_suffix,
        bool bSecure, std::string& response,
        http_headers_t* headers = NULL,
        http_headers_t* out_headers = NULL);

    void* SendGetRequest(const char* url_suffix, bool bSecure,
        http_headers_t* headers = NULL);
    
    void* SendPostRequest(const char* url_suffix,
        const char* post_data, bool bSecure,
        http_headers_t* headers = NULL);

    std::string get_last_effective_url();

    bool ReadResponseHeaders(void* request_handle, http_headers_t* headers);

    bool ReadRequestResponse(void* request_handle,
        unsigned char* buffer, unsigned long& buffer_size, bool* b_exit_flag);

    bool CancelRequest(void* request_handle);
    void CloseRequest(void* request_handle);
    
    void GetAddress(std::string& address)
    {
        address = address_;
    }

    void SetHttpAuth(http_auth auth)
    {
        auth_ = auth;
    }

protected:
    const std::string address_;
    std::string login_;
    std::string password_;
    std::string user_agent_;
    unsigned short port_;
    http_auth auth_;
    std::string data_dir_;
    std::string ca_cert_name_;
    std::string last_effective_url_;

    std::string client_cert_name_;
    std::string client_cert_;
    std::string client_priv_key_;
    long timeout_sec_;
};

} // namespace dvblink

#endif  // __DVBLINK_HTTP_COMM_CURL_H__

// $Id: dl_http_comm.curl.h 13093 2016-07-25 16:20:51Z pashab $
