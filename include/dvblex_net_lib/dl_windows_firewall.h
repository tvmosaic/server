/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include "dl_win_firewall_errors.h"


class CDLWinXPFirewall;
class CDLVistaFirewall;

class CWinFirewall
{
public:
    CWinFirewall();
    ~CWinFirewall();

    FW_ERROR_CODE Initialize();
    FW_ERROR_CODE Uninitialize();

    FW_ERROR_CODE AddRule(const wchar_t* lpszProcessImageFileName,
        const wchar_t* lpszRuleName, const wchar_t* lpszRuleGroupName);
    FW_ERROR_CODE RemoveRule(const wchar_t* lpszRuleName);

private:
    CDLWinXPFirewall* m_pXPFirewall;
    CDLVistaFirewall* m_pVistaFirewall;
};

