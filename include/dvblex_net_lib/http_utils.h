/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once
#include <string>
#include <vector>

#include <dl_errors.h>
#include <dl_types.h>
#include <dl_filesystem_path.h>

namespace dvblink {
namespace streaming {

struct header_field
{
    std::string name;
    std::string value;
};

typedef std::vector<header_field> http_headers;

bool parse_http_message(const std::string& msg,
    std::string& start_line, http_headers& headers,
    size_t& body_offs);

bool parse_status_line(const std::string& status_line,
    std::string& protocol, std::string& version,
    unsigned int& status_code, std::string& text);

bool find_http_header(const http_headers& headers,
    const std::string& header_name, std::string& header_value);

dvblink::errcode_t get_description_xml(const std::string& url, std::string& xml);

bool download_http_image(const dvblink::url_address_t& url, const dvblink::filesystem_path_t& file_base_path, dvblink::filesystem_path_t& outfile_path);

} // namespace streaming
} // namespace dvblink

// $Id: http_utils.h 11070 2015-02-03 17:44:49Z pashab $
