/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_SOCK_ADDR_H__
#define __DVBLINK_SOCK_ADDR_H__

#ifdef _WIN32
#include <winsock2.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#endif

#include <string>
#include <boost/cstdint.hpp>
#include <dl_errors.h>

namespace dvblink {

enum ip_version { ip_v4, ip_v6 };
typedef boost::uint16_t port_t;

class sock_addr
{
public:
    explicit sock_addr(ip_version ipver = ip_v4);
    sock_addr(const struct sockaddr& sa);
    sock_addr(const sock_addr& addr);

    sock_addr& operator=(const sock_addr& rhs);

    ip_version get_ip_version() const;

    port_t get_port() const;
    void set_port(port_t port);

    errcode_t get_address(std::string& addr) const;
    errcode_t set_address(const std::string& addr);

    bool is_multicast() const;

    bool is_inaddr_any();

    operator const sockaddr*() const
    {
        return &saddr_;
    }

private:
    struct sockaddr saddr_;
};

} // namespace dvblink

#endif  // __DVBLINK_SOCK_ADDR_H__

// $Id: sock_addr.h 6488 2012-10-05 13:38:16Z mike $
