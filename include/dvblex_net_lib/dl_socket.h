/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_SOCKET_H__
#define __DVBLINK_SOCKET_H__

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <dl_sock_addr.h>
#include <dl_utils.h>

namespace dvblink {

class basic_socket : public boost::noncopyable
{
public:
    enum socket_type { sock_tcp, sock_udp, sock_raw };

#ifdef _WINSOCKAPI_
    typedef int socklen_t;
    typedef SOCKET handle_t;
    static const handle_t bad_socket = INVALID_SOCKET;
#else
    typedef int handle_t;
    static const handle_t bad_socket = -1;
#endif

public:
    explicit basic_socket(socket_type type, ip_version ipv = ip_v4);
    basic_socket(handle_t sock, socket_type type, ip_version ipv = ip_v4);
    
    virtual ~basic_socket();

    socket_type get_type() const
    {
        return type_;
    }

    ip_version get_ip_version() const
    {
        return ipver_;
    }

    errcode_t get_recv_bufsize(int& size);
    errcode_t set_recv_bufsize(int size);

    errcode_t get_send_bufsize(int& size);
    errcode_t set_send_bufsize(int size);

    errcode_t get_sock_error(int& err);

    errcode_t set_reuse_addr(bool onoff = true);
    errcode_t set_blocking_mode(bool onoff = true);

    errcode_t bind(const sock_addr& sa);
    errcode_t get_sock_addr(sock_addr& sa);

    errcode_t wait_for_readable(timeout_t to = infinite_timeout);
    errcode_t wait_for_writable(timeout_t to = infinite_timeout);

    bool is_readable();
    bool is_writable();

    basic_socket::handle_t get_handle() const
    {
        return sock_;
    }

protected:
    errcode_t open();
    errcode_t close();

    template <typename T>
    errcode_t set_option(int code, int level, const T& value)
    {
        if (sock_ == bad_socket)
        {
            return err_closed;
        }

        int rc = ::setsockopt(sock_, level, code,
            reinterpret_cast<const char*>(&value), sizeof(T));

        return ((rc == 0) ? err_none : err_error);
    }

    template <typename T>
    errcode_t get_option(int code, int level, T& value)
    {
        if (sock_ == bad_socket)
        {
            return err_closed;
        }

        socklen_t size = sizeof(T);
        int rc = ::getsockopt(sock_, level, code,
            reinterpret_cast<char*>(&value), &size);

        return ((rc == 0) ? err_none : err_error);
    }

protected:
    handle_t sock_;
    socket_type type_;
    ip_version ipver_;
};

class tcp_socket : public basic_socket
{
public:
    typedef boost::shared_ptr<tcp_socket> sock_ptr;

public:
    explicit tcp_socket(ip_version ipv = ip_v4);
    tcp_socket(handle_t sock, ip_version ipv = ip_v4);

    ~tcp_socket();

    errcode_t set_no_delay(bool onoff = true);
    errcode_t set_keep_alive(bool onoff = true);

    errcode_t connect(const sock_addr& sa, timeout_t to = infinite_timeout);
    errcode_t listen();

    errcode_t accept(tcp_socket::sock_ptr& new_sock,
        sock_addr& peer_addr);

    errcode_t get_peer_address(sock_addr& peer_addr);

    bool is_connected();
    errcode_t disconnect(timeout_t to = zero_timeout);

    errcode_t send(const void* data,
        size_t data_size, size_t& bytes_sent,
        timeout_t to = infinite_timeout);

    errcode_t receive(void* buf,
        size_t buf_size, size_t& bytes_received,
        timeout_t to = infinite_timeout,
        bool peek = false);

    errcode_t receive_max(void* buf,
        size_t buf_size, size_t& bytes_received,
        timeout_t to = infinite_timeout);

    errcode_t peek_max(void* buf,
        size_t buf_size, size_t& bytes_received,
        timeout_t to = infinite_timeout);
};

class udp_socket : public basic_socket
{
public:
    typedef boost::shared_ptr<udp_socket> sock_ptr;
    friend class udp_endpoint;

public:
    explicit udp_socket(ip_version ipv = ip_v4);
    udp_socket(handle_t sock, ip_version ipv = ip_v4);

    errcode_t send_datagram(const void* dg, size_t dg_size,
        const sock_addr& dst_addr);

    errcode_t receive_datagram(void* buf, size_t buf_size,
        size_t& bytes_received, sock_addr& from, bool peek = false);

    // multicast
    errcode_t add_membership(const sock_addr& group_addr,
        const sock_addr& local_addr);
    errcode_t drop_membership(const sock_addr& group_addr,
        const sock_addr& local_addr);

    // source-specific multicast
    errcode_t add_ssm_membership(const sock_addr& group_addr, const sock_addr& source_addr,
        const sock_addr& local_addr);
    errcode_t drop_ssm_membership(const sock_addr& group_addr, const sock_addr& source_addr,
        const sock_addr& local_addr);

    // TTL values:
    //   0 - restricted to the same host 
    //   1 - restricted to the same subnet 
    //  32 - restricted to the same site 
    //  64 - restricted to the same region 
    // 128 - restricted to the same continent 
    // 255 - unrestricted 
    //   
    errcode_t set_ttl(int ttl);
    errcode_t set_multicast_if(const sock_addr& addr);
    errcode_t set_multicast_loopback(bool onoff);

    errcode_t set_broadcast(bool onoff = true);
};

} // namespace dvblink

#endif  // __DVBLINK_SOCKET_H__

// $Id: socket.h 11237 2015-03-25 09:55:28Z pashab $
