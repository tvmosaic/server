/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <netfw.h>
#include <dl_win_firewall_errors.h>

///<summary>
/// Class implements the methods for working with Windows XP SP2 Firewall
///</summary>
class CDLWinXPFirewall
{
public:
	CDLWinXPFirewall();
	~CDLWinXPFirewall();

	// You should call after CoInitialize() is called
	FW_ERROR_CODE Initialize();
	// This function is automatically called by destructor, but should be called before CoUninitialize() is called
	FW_ERROR_CODE Uninitialize();

	FW_ERROR_CODE IsWindowsFirewallOn(BOOL& bOn);
	
	FW_ERROR_CODE TurnOnWindowsFirewall();
	FW_ERROR_CODE TurnOffWindowsFirewall();

	// lpszProcessImageFilaName: File path
	// lpszRegisterName: You can see this name throught the control panel
	FW_ERROR_CODE AddApplication(const wchar_t* lpszProcessImageFileName, const wchar_t* lpszRegisterName);
	FW_ERROR_CODE RemoveApplication(const wchar_t* lpszProcessImageFileName);
	FW_ERROR_CODE IsAppEnabled(const wchar_t* lpszProcessImageFileName, BOOL& bEnable);

	// lpszRegisterName: You can see this name throught the control panel
	FW_ERROR_CODE AddPort(LONG lPortNumber, NET_FW_IP_PROTOCOL ipProtocol, const wchar_t* lpszRegisterName);
	FW_ERROR_CODE RemovePort(LONG lPortNumber, NET_FW_IP_PROTOCOL ipProtocol);
	FW_ERROR_CODE IsPortEnabled(LONG lPortNumber, NET_FW_IP_PROTOCOL ipProtocol, BOOL& bEnable);

	FW_ERROR_CODE IsExceptionNotAllowed(BOOL& bNotAllowed);
	FW_ERROR_CODE SetExceptionNotAllowed(BOOL bNotAllowed);

	FW_ERROR_CODE IsNotificationDisabled(BOOL& bDisabled);
	FW_ERROR_CODE SetNotificationDisabled(BOOL bDisabled);

	FW_ERROR_CODE IsUnicastResponsesToMulticastBroadcastDisabled(BOOL& bDisabled);
	FW_ERROR_CODE SetUnicastResponsesToMulticastBroadcastDisabled(BOOL bDisabled);

protected:
	INetFwProfile* m_pFireWallProfile;
};