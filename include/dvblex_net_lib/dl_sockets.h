/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#ifndef WIN32
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <netdb.h>
    #include <sys/socket.h>

    //Sockets related stuff
    typedef int SOCKET;
    #define INVALID_SOCKET -1
    #define SOCKET_ERROR   -1
    #define closesocket(s) close(s);
    typedef hostent * LPHOSTENT;
    typedef in_addr * LPIN_ADDR;
    typedef sockaddr SOCKADDR;
#else
    //add windows headers here if needed
    typedef int socklen_t;
#endif

inline bool socket_check_if_local_connection(SOCKET sock)
{
  sockaddr_in remote_addr, local_addr;
  socklen_t len = sizeof(remote_addr);
  getpeername(sock, (sockaddr *)&remote_addr, &len);
  getsockname(sock, (sockaddr *)&local_addr, &len);
#ifdef WIN32
  return (remote_addr.sin_addr.S_un.S_addr == local_addr.sin_addr.S_un.S_addr);
#else
  return (remote_addr.sin_addr.s_addr == local_addr.sin_addr.s_addr);
#endif
 }
