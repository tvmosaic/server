/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_TCP_SERVER_H__
#define __DVBLINK_TCP_SERVER_H__

#include <boost/thread.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <dl_socket.h>

namespace dvblink {

class tcp_server : public boost::noncopyable
{
public:
    class callbacks
    {
    public:
        virtual void on_accept(tcp_socket::sock_ptr sockptr,
            const sock_addr& peer_addr) = 0;
        virtual void on_error(dvblink::errcode_t err,
            const std::string& info = "") = 0;
    };

    typedef boost::shared_ptr<callbacks> callbacks_ptr;

public:
    tcp_server(const sock_addr& server_addr, callbacks_ptr cb_ptr,
        bool reuse_address = false);
    ~tcp_server();

    void shutdown();

private:
    void thread_func();

private:
    const sock_addr srvaddr_;
    callbacks_ptr cb_ptr_;
    tcp_socket::sock_ptr sock_ptr_;
    boost::shared_ptr<boost::thread> thread_ptr_;
    bool cancel_flag_;
};

typedef boost::shared_ptr<tcp_server> tcp_server_ptr;

} // namespace dvblink

#endif  // __DVBLINK_TCP_SERVER_H__

// $Id: tcp_server.h 3512 2011-09-27 13:45:18Z mike $
