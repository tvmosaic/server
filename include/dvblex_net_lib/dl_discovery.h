/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <vector>
#include <string>
#include <boost/thread.hpp>
#include <boost/cstdint.hpp>
#include <boost/shared_ptr.hpp>
#include <dl_utils.h>
#include <dl_socket.h>

namespace dvblink {

class discovery_server
{
public:
    struct server_settings
    {
        std::string server_id;
        std::string server_name;

        unsigned short discovery_port;
        unsigned short http_port;
        unsigned short https_port;

        server_settings() :
            discovery_port(0), http_port(0), https_port(0)
        {
        }
    };

public:
    explicit discovery_server(const server_settings& settings);
    ~discovery_server();

private:
    bool wait_for_readable(dvblink::timeout_t to);
    void thread_func();
    void create_xml(const std::vector<std::string>& server_ips,
        std::string& xml);

    struct socket_info
    {
        std::string addr;
        udp_socket::sock_ptr sock_ptr;
    };

private:
    server_settings settings_;
    typedef boost::shared_ptr<boost::thread> thread_ptr;
    thread_ptr thread_;
    bool shutdown_;
    std::vector<socket_info> sockets_;
};

const unsigned char discovery_request_code = 0xDC;
const timeout_t default_discovery_timeout = boost::posix_time::milliseconds(2000);
const unsigned short default_discovery_port = 65332;

bool discover_dvblink_servers(std::vector<std::string>& server_info,
    timeout_t timeout = default_discovery_timeout,
    unsigned short discovery_port = default_discovery_port);

} // namespace dvblink
