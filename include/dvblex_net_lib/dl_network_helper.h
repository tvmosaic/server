/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <map>

namespace dvblink { 

typedef struct _SNetworkAdaptersInfo
{
    std::wstring  m_strGUID;
    std::wstring  m_strDescription;
    std::wstring  m_strName;
    std::wstring  m_strAddress;
    std::wstring  m_MAC;
} SNetworkAdaptersInfo, *PSNetworkAdaptersInfo;

typedef struct _SNetworkHostInfo
{
    std::string  ip_addr;
    std::string  mac_addr;
    std::string  name;
} SNetworkHostInfo, *PSNetworkHostInfo;

typedef std::vector<SNetworkAdaptersInfo>  TNetworkAdaptersInfo;

enum EDL_NET_PROTOCOLS
{
	DL_NET_PROTO_UDP,
	DL_NET_PROTO_RTP,
	DL_NET_PROTO_HTTP,
	DL_NET_PROTO_RTSP,
	DL_NET_PROTO_HTTPS,
	DL_NET_PROTO_RTMP,
	DL_NET_PROTO_UNKNOWN
};

class network_helper
{
public:
    static bool get_local_net_adapters(TNetworkAdaptersInfo& adapters);
    static bool get_local_ip_address(const std::wstring& network_adapter, std::wstring& ip_address);
    static unsigned long get_ip_address(const std::wstring& host_name);
    
	static bool is_external_network_initialized();
    static bool wait_for_network_initialization(time_t seconds = -1);

    static bool get_remote_host_by_addr(const std::string& ip_address, SNetworkHostInfo& host_info);
    static bool get_remote_host_by_name(const std::string& host_name, SNetworkHostInfo& host_info);

    static bool wakeup_remote_host(const std::string& mac_addr);

    static unsigned char decode_hex_to_char(const std::string& str);
    static const std::string decode_char_to_hex(unsigned char ch);

    static EDL_NET_PROTOCOLS parse_net_url(const char* url, std::string& address, std::string& user, std::string& pswd, 
        unsigned short& portNum, std::string& url_suffix);

    static EDL_NET_PROTOCOLS get_proto(const char* url);
        
    static const char* get_proto_string(EDL_NET_PROTOCOLS proto);
    
    static bool is_tcp_port_available(unsigned short port);
    static bool is_udp_port_available(unsigned short port);

    static bool get_local_hostname(std::string& hostname);

    static void parse_http_suffix_params(const std::string& suffix, std::map<std::string, std::string>& http_params);

private:
    network_helper();
    ~network_helper();

    static bool get_remote_mac_addr(const std::string& ip_address, std::string& mac_addr);
};

}
