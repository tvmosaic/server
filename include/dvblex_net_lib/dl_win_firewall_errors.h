/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>


enum FW_ERROR_CODE
{
    FW_UNKNOWN = 0,                     // Unknown
    FW_NOERROR,                         // Success
    FW_ERR_INITIALIZED,					// Already initialized or doesn't call Initialize()
    FW_ERR_CREATE_SETTING_MANAGER,		// Can't create an instance of the firewall settings manager
    FW_ERR_LOCAL_POLICY,				// Can't get local firewall policy
    FW_ERR_PROFILE,						// Can't get the firewall profile
    FW_ERR_FIREWALL_IS_ENABLED,			// Can't get the firewall enable information
    FW_ERR_FIREWALL_ENABLED,			// Can't set the firewall enable option
    FW_ERR_INVALID_ARG,					// Invalid Arguments
    FW_ERR_AUTH_APPLICATIONS,			// Failed to get authorized application list
    FW_ERR_APP_ENABLED,					// Failed to get the application is enabled or not
    FW_ERR_CREATE_APP_INSTANCE,			// Failed to create an instance of an authorized application
    FW_ERR_SYS_ALLOC_STRING,			// Failed to alloc a memory for BSTR
    FW_ERR_PUT_PROCESS_IMAGE_NAME,		// Failed to put Process Image File Name to Authorized Application
    FW_ERR_PUT_REGISTER_NAME,			// Failed to put a registered name
    FW_ERR_ADD_TO_COLLECTION,			// Failed to add to the Firewall collection
    FW_ERR_REMOVE_FROM_COLLECTION,		// Failed to remove from the Firewall collection
    FW_ERR_GLOBAL_OPEN_PORTS,			// Failed to retrieve the globally open ports
    FW_ERR_PORT_IS_ENABLED,				// Can't get the firewall port enable information
    FW_ERR_PORT_ENABLED,				// Can't set the firewall port enable option
    FW_ERR_CREATE_PORT_INSTANCE,		// Failed to create an instance of an authorized port
    FW_ERR_SET_PORT_NUMBER,				// Failed to set port number
    FW_ERR_SET_IP_PROTOCOL,				// Failed to set IP Protocol
    FW_ERR_EXCEPTION_NOT_ALLOWED,		// Failed to get or put the exception not allowed
    FW_ERR_NOTIFICATION_DISABLED,		// Failed to get or put the notification disabled
    FW_ERR_UNICAST_MULTICAST,			// Failed to get or put the UnicastResponses To MulticastBroadcast Disabled Property 
    FW_ERR_ALL = 26                     // All error count
};

static wchar_t* FW_ERROR_MESSAGE[] = 
{
    L"Unknown error",
    L"Success",
    L"Already initialized or doesn't call Initialize()",
    L"Can't create an instance of the firewall settings manager",
    L"Can't get local firewall policy",
    L"Can't get the firewall profile",
    L"Can't get the firewall enable information",
    L"Can't set the firewall enable option",
    L"Invalid Arguments",
    L"Failed to get authorized application list",
    L"Failed to get the application is enabled or not",
    L"Failed to create an instance of an authorized application",
    L"Failed to alloc a memory for BSTR",
    L"Failed to put Process Image File Name to Authorized Application",
    L"Failed to put a registered name",
    L"Failed to add to the Firewall collection",
    L"Failed to remove from the Firewall collection",
    L"Failed to retrieve the globally open ports",
    L"Can't get the firewall port enable information",
    L"Can't set the firewall port enable option",
    L"Failed to create an instance of an authorized port",
    L"Failed to set port number",
    L"Failed to set IP Protocol",
    L"Failed to get or put the exception not allowed",
    L"Failed to get or put the notification disabled",
    L"Failed to get or put the UnicastResponses To MulticastBroadcast Disabled Property",
};

inline void GetFirewallErrorMessage(FW_ERROR_CODE errorCode, std::wstring& errorMessage)
{
    if (FW_ERR_ALL > errorCode)
    {
        errorMessage = FW_ERROR_MESSAGE[errorCode];	
    }
    else
    {
        errorMessage = FW_ERROR_MESSAGE[FW_UNKNOWN];
    }
}
