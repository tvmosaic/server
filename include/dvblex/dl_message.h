/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <list>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <dl_types.h>
#include <dl_message_queue.h>
#include <dli_message_driven.h>

namespace dvblink { namespace messaging {

//
// parameter REQUEST is an user defined event type declared with using CRTP. 
// To define your own event follow the C++ construct
// a.k.a. curiously recurring template pattern (CRTP)
//     class MyEvent : public message_post<MyEvent>
//     {
//     ...  // here is your specific class content. 
//          // there is no restriction what we may and what we should
//     };
//
template <typename REQUEST>
class message_post
{
public:
    typedef REQUEST                    request_t;
    typedef message_post<request_t>    this_t;

public:
    class subscriber
    {
    public:
        subscriber(const message_queue_t& queue) :
            queue_(queue)
        {
            queue_->register_message(typeid(request_t).name(), this, &request_t::deliver_message);
        }

        virtual ~subscriber()
        {
            queue_->unregister_message(typeid(request_t).name());
        }

        void invoke_handler(const message_sender_t& sender, const message_id_t& /*id*/, const request_t& message) // NVI idiom
        { 
            try
            {
                handle(sender, message);
            }
            catch (...)
            {
                assert(0);
            }
        }

    private:
        // derived user_message_t type shall override this method
        virtual void handle(const message_sender_t& sender, const request_t& message) = 0;

    private:
        message_queue_t queue_;
    };

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& /*ar*/, const unsigned int /*version*/) 
    { 
    } 

    static void deliver_message(void* s, const message_id_t& id, const message_sender_t& sender, const std::string& message)
    {
        std::istringstream archive_stream(message);
        boost::archive::text_iarchive archive(archive_stream);

        request_t msg;
        archive >> msg;

        reinterpret_cast<subscriber*>(s)->invoke_handler(sender, id, *static_cast<const request_t*>(&msg));
    }
};

//
// parameter REQUEST is an user defined event type declared with using CRTP. 
// To define your own event follow the C++ construct a.k.a. curiously recurring template pattern (CRTP)
//     class MyEvent : public message_post<MyEvent>
//     {
//     ...  // here is your specific class content. 
//          // there is no restriction what we may and what we should
//     };
//
// parameter RESPONSE is an user defined class with any structure
//
template <typename REQUEST, typename RESPONSE>
class message_send
{
public:
    typedef REQUEST                    request_t;
    typedef RESPONSE                   response_t;
    typedef message_send<request_t, response_t>  this_t;

public:
    class subscriber
    {
    public:
        subscriber(const message_queue_t& queue) :
            queue_(queue)
        {
            queue_->register_message(typeid(request_t).name(), this, &request_t::deliver_message);
        }

        virtual ~subscriber()
        {
            queue_->unregister_message(typeid(request_t).name());
        }

        void invoke_handler(const message_sender_t& sender, const message_id_t& id, const request_t& message) // NVI idiom
        { 
            try
            {
                response_t response;
                handle(sender, message, response);
                queue_->response(id, success, sender.get(), response);
            }
            catch (...)
            {
                assert(0);
            }
        }

    private:
        // derived user_message_t type shall override this method
        virtual void handle(const message_sender_t& sender, const request_t& message, response_t& response) = 0;

    private:
        message_queue_t queue_;
    };

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& /*ar*/, const unsigned int /*version*/) 
    { 
    } 

    static void deliver_message(void* s, const message_id_t& id, const message_sender_t& sender, const std::string& message)
    {
        std::istringstream archive_stream(message);
        boost::archive::text_iarchive archive(archive_stream);

        request_t msg;
        archive >> msg;

        reinterpret_cast<subscriber*>(s)->invoke_handler(sender, id, *static_cast<const request_t*>(&msg));
    }
};

class empty_serializator
{
private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& /*ar*/, const unsigned int /*version*/) 
    { 
    } 
};

} //messaging
} //dvblink
