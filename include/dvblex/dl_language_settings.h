/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
# pragma once

#include <map>
#include <boost/filesystem.hpp>
#include <boost/thread/mutex.hpp>
#include <dl_types.h>
#include <dl_filesystem_path.h>


namespace dvblink { namespace engine {

class language_settings
{
    struct language_desc_t
    {
        std::string name;
        std::string id;
        filesystem_path_t p;
    };

    typedef std::map<item_id_t, std::string> map_item_id_to_item_name_t;
    typedef map_item_id_to_item_name_t::const_iterator map_item_id_to_item_name_const_iter_t;
    typedef map_item_id_to_item_name_t::iterator map_item_id_to_item_name_iter_t;

    typedef std::map<std::string, language_desc_t> map_lang_name_to_lang_file_name_t;
    typedef map_lang_name_to_lang_file_name_t::const_iterator map_lang_name_to_lang_file_name_const_iter_t;
    typedef map_lang_name_to_lang_file_name_t::iterator map_lang_name_to_lang_file_name_iter_t;

public:
    static void CreateInstance(const filesystem_path_t& pathToBinary);
    static void DestroyInstance();
    static language_settings* GetInstance();

    void ReInitialize();

//    void GetLanguageList(std::vector<std::string>& vecLanguage);
    bool GetCurrentLanguageId(std::string& lang_id);
    bool SetCurrentLanguageId(const std::string& lang_id);

    bool GetItemName(const item_id_t& itemID, std::string& strItemName);
    std::string GetItemName(const item_id_t& itemID);

protected:
    void Init();
    bool InitItemMap();
    void InitLangFileMap();

    void Reset();

    bool GetLangFromXml(const boost::filesystem::path& xmlFile, language_desc_t& language_desc) const;

private:
    language_settings(const filesystem_path_t& pathToBinary);
    ~language_settings();

    // Avoid copying
    language_settings(const language_settings&);
    language_settings& operator = (const language_settings&);
    bool get_path_by_language_id(const std::string& id, filesystem_path_t& p);

private:
    friend class std::auto_ptr<language_settings>;
    static std::auto_ptr<language_settings> s_pInstance;

    filesystem_path_t m_pathToBinary;
    map_item_id_to_item_name_t m_mapItemIdToItemName;
    map_lang_name_to_lang_file_name_t m_mapLangToFileName;

    std::string m_strLangId;
    boost::mutex m_lock;
};

} //engine
} //dvblink
