/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <boost/serialization/vector.hpp>
#include <dl_types.h>

namespace dvblex { 

struct server_info_t
{
    dvblink::client_id_t  install_id_;
    dvblink::product_id_t product_id_;
    dvblink::product_name_t product_name_;
    dvblink::version_t version_;
    dvblink::build_t build_;
};

enum live_protocols_e
{
	lpe_unknown = 0,
	lpe_http = 1,
	lpe_udp = 2,
	lpe_rtsp = 4,
	lpe_asf = 8,
	lpe_hls = 16,
	lpe_webm = 32,
	lpe_mp4 = 64
};

#define lpe_all_protocols (xFFFFFFFF)

enum live_transcoders_e
{
	lte_unknown = 0,
	lte_wmv = 1,
	lte_wma = 2,
	lte_h264 = 4,
	lte_aac = 8,
	lte_raw = 16,
	lte_vpx = 32,
	lte_vorbis = 64
};

#define lte_all_trancoders (xFFFFFFFF)

enum playback_protocols_e
{
	ppe_unknown = 0,
	ppe_http = 1,
	ppe_udp = 2,
	ppe_hls = 4
};

#define ppe_all_protocols (xFFFFFFFF)

enum playback_transcoders_e
{
	pte_unknown = 0,
	pte_h264 = 1,
	pte_aac = 2,
	pte_raw = 4
};

#define pte_all_transcoders (xFFFFFFFF)

struct server_capabilities_t
{
	server_capabilities_t() :
		protocols_(lpe_unknown), pb_protocols_(ppe_unknown), transcoders_(lte_unknown), pb_transcoders_(pte_unknown),
		can_record_(false), supports_timeshift_(false), supports_device_mgmt_(false), timeshift_version_(0)
	{}
	dvblink::bitmask_t protocols_;
	dvblink::bitmask_t pb_protocols_;
	dvblink::bitmask_t transcoders_;
	dvblink::bitmask_t pb_transcoders_;
	std::vector<dvblink::message_addressee_t> addressees_;
	dvblink::bool_flag_t can_record_;
	dvblink::bool_flag_t supports_timeshift_;
	dvblink::bool_flag_t supports_device_mgmt_;
	dvblink::timeshift_version_t timeshift_version_;
};

} //dvblex

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::server_info_t& pi, const unsigned int /*version*/)
{
    ar & pi.install_id_;
    ar & pi.product_id_;
    ar & pi.product_name_;
    ar & pi.version_;
    ar & pi.build_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::server_capabilities_t& caps, const unsigned int /*version*/)
{
    ar & caps.protocols_;
    ar & caps.pb_protocols_;
    ar & caps.transcoders_;
    ar & caps.pb_transcoders_;
    ar & caps.addressees_;
    ar & caps.can_record_;
    ar & caps.supports_timeshift_;
    ar & caps.supports_device_mgmt_;
    ar & caps.timeshift_version_;
}

} // namespace serialization
} // namespace boost
