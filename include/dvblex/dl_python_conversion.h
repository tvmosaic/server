/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <boost/python.hpp>
#include <dl_runtime_error.h>
#include <dl_strings.h>


namespace dvblex {

    template<typename Vector>
    bool convert_to_list(boost::python::list& python_list, const Vector& vector)
    {
        bool success = true;

        try
        {
            typename Vector::const_iterator it = vector.begin();
            while (it != vector.end())
            {
                boost::python::dict python_dict;
                struct_to_dict(*it, python_dict);
                python_list.append(python_dict);
                ++it;
            }
        }
        catch (dvblink::runtime_error&)
        {
            success = false;
        }

        return success;
    }

    template<typename Vector>
    bool convert_from_list(const boost::python::list& python_list, Vector& vector)
    {
        bool success = true;

        try
        {
            for (ssize_t i = 0; i < boost::python::len(python_list); i++) 
            {                   
                boost::python::dict python_dict = boost::python::extract<boost::python::dict>(python_list[i]);
                typename Vector::value_type type;
                dict_to_struct(python_dict, type);
                vector.push_back(type);
            }
        }
        catch (boost::python::error_already_set&) 
        { 
            success = false;
        }

        return success;
    }

    inline void convert_to_pyerror(const dvblink::runtime_error& e)
    {
        PyErr_SetString(PyExc_RuntimeError, e.get_message().c_str());
    }
}
