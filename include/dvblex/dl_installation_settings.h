/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <dl_filesystem_path.h>
#include <dl_message_queue.h>


namespace dvblex { namespace settings {

const std::string config_component_name     = "config";
const std::string common_component_name     = "common";
const std::string scanners_component_name   = "scanners";

const dvblink::fileitem_name_t common_item_sqlite = "sqlite";
const dvblink::fileitem_name_t common_item_ffmpeg = "ffmpeg";
const dvblink::fileitem_name_t common_item_ffprobe = "ffprobe";
const dvblink::fileitem_name_t common_item_comskip  = "comskip";
const dvblink::fileitem_name_t common_item_temp  = "temp";
const dvblink::fileitem_name_t common_item_xmltv_cat_definitions = "xmltv_category_definitions";

struct common_file_item_desc_t
{
    dvblink::filesystem_path_t file_;
    dvblink::filesystem_path_t dir_;
};

class installation_settings_t
{
public:
    installation_settings_t(){}
    ~installation_settings_t(){}

    virtual void init(dvblink::messaging::message_queue_t& message_queue);

    const dvblink::filesystem_path_t get_config_path() { return private_config_path_;}
    const dvblink::filesystem_path_t get_private_common_path() { return private_common_path_;}
    const dvblink::filesystem_path_t get_shared_common_path() { return shared_common_path_;}
    const dvblink::filesystem_path_t get_temp_path() { return shared_temp_path_;}
    const common_file_item_desc_t& get_sqlite() {return sqlite_;}
    const common_file_item_desc_t& get_ffmpeg() {return ffmpeg_;}
    const common_file_item_desc_t& get_ffprobe() {return ffprobe_;}
    const common_file_item_desc_t& get_comskip() {return comskip_;}
    const common_file_item_desc_t& get_xmltv_cat_definitions() {return xmltv_cat_defs_;}

    const dvblink::filesystem_path_t get_private_component_path(const std::string& component_name);
    const dvblink::filesystem_path_t get_shared_component_path(const std::string& component_name);

protected:
    dvblink::filesystem_path_t private_common_path_;
    dvblink::filesystem_path_t private_config_path_;
    dvblink::filesystem_path_t shared_temp_path_;
    dvblink::filesystem_path_t shared_common_path_;
    common_file_item_desc_t sqlite_;
    common_file_item_desc_t ffmpeg_;
    common_file_item_desc_t ffprobe_;
    common_file_item_desc_t comskip_;
    common_file_item_desc_t xmltv_cat_defs_;

    dvblink::messaging::message_queue_t message_queue_;
};

typedef boost::shared_ptr<installation_settings_t> installation_settings_obj_t;

} //settings
} //dvblex

