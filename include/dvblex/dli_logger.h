/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_types.h>
#include <dli_base.h>

namespace dvblink { namespace logging {

// {2F28D214-02E8-4832-9D00-3ED76D487099}
static const i_guid logger_interface =
{ 0x2f28d214, 0x2e8, 0x4832, { 0x9d, 0x0, 0x3e, 0xd7, 0x6d, 0x48, 0x70, 0x99 } };

enum e_log_level
{
    log_level_none = 0,            /*!< No logging */
    log_level_errors_and_warnings, /*!< Errors and warnings only */
    log_level_info,                /*!< Additional life-cycle information */
    log_level_extended_info,       /*!< Extended (debug) information */
    log_level_forced_info          /*!< This information is always written to a log*/
};

struct i_logger : public i_base_object
{
    /*!	
        This function writes message to a log
        \param log_level - specifies the log level of the message
        \param log_str - specifies the log message itself
    */
    virtual void __stdcall log_message(e_log_level log_level, const wchar_t* log_str) const = 0;
};

typedef boost::shared_ptr<i_logger> i_logger_t;

} //logging
} //dvblink
