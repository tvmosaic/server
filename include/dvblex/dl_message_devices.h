/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_types.h>
#include <dl_message.h>
#include <dl_devices.h>
#include <dl_provider_info.h>
#include <dl_parameters.h>
#include <dl_scan_info.h>
#include <dl_headend.h>
#include <dl_channels.h>

namespace dvblink { namespace messaging { namespace devices {

//
// devices_info
//
struct devices_info_response
{
    dvblex::device_info_list_t device_info_list_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & device_info_list_;
        ar & result_;
    }
};

struct devices_info_request : public message_send<devices_info_request, devices_info_response>
{
};

//
// get_scanners
//
struct get_scanners_response
{
    dvblex::provider_info_map_t providers_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & providers_;
        ar & result_;
    }
};

struct get_scanners_request : public message_send<get_scanners_request, get_scanners_response>
{
    get_scanners_request(){}
    get_scanners_request(const dvblink::device_id_t& device_id, const dvblink::standard_set_t& standard) :
        standard_(standard), device_id_(device_id)
        {}

    dvblink::standard_set_t standard_;
    dvblink::device_id_t device_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & standard_;
        ar & device_id_;
    }
};

//
// start_scan
//
struct start_scan_response
{
    dvblex::channel_start_scan_result_e scan_start_result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & scan_start_result_;
    }
};

struct start_scan_request : public message_send<start_scan_request, start_scan_response>
{
    start_scan_request(){}
    start_scan_request(const dvblink::device_id_t& device_id, const dvblex::concise_param_map_t& parameters) :
        device_id_(device_id), parameters_(parameters)
        {}

    dvblink::device_id_t device_id_;
    dvblex::concise_param_map_t parameters_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & device_id_;
        ar & parameters_;
    }
};

//
// get rescan provider settings
//
struct get_rescan_settings_response
{
    bool result_;
    dvblex::parameters_container_t settings_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
        ar & settings_;
    }
};

struct get_rescan_settings_request : public message_send<get_rescan_settings_request, get_rescan_settings_response>
{
    get_rescan_settings_request(){}
    get_rescan_settings_request(const dvblink::device_id_t& device_id, const dvblink::headend_id_t& headend_id) :
        device_id_(device_id), headend_id_(headend_id)
        {}

    dvblink::device_id_t device_id_;
    dvblink::headend_id_t headend_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & device_id_;
        ar & headend_id_;
    }
};

//
// rescan provider on device
//
struct rescan_provider_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct rescan_provider_request : public message_send<rescan_provider_request, rescan_provider_response>
{
    rescan_provider_request(){}
    rescan_provider_request(const dvblink::device_id_t& device_id, const dvblink::headend_id_t& headend_id, const dvblex::concise_param_map_t& settings) :
        device_id_(device_id), headend_id_(headend_id), settings_(settings)
        {}

    dvblink::device_id_t device_id_;
    dvblink::headend_id_t headend_id_;
    dvblex::concise_param_map_t settings_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & device_id_;
        ar & headend_id_;
        ar & settings_;
    }
};

//
// apply_scan
//
struct apply_scan_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct apply_scan_request : public message_send<apply_scan_request, apply_scan_response>
{
    apply_scan_request(){}
    apply_scan_request(const dvblink::device_id_t& device_id) :
        device_id_(device_id)
        {}

    dvblink::device_id_t device_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & device_id_;
    }
};

//
// cancel_scan
//
struct cancel_scan_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct cancel_scan_request : public message_send<cancel_scan_request, cancel_scan_response>
{
    cancel_scan_request(){}
    cancel_scan_request(const dvblink::device_id_t& device_id) :
        device_id_(device_id)
        {}

    dvblink::device_id_t device_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & device_id_;
    }
};

//
// get_networks
//
struct get_networks_response
{
    bool result_;
    dvblex::scanned_network_list_t networks_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
        ar & networks_;
    }
};

struct get_networks_request : public message_send<get_networks_request, get_networks_response>
{
    get_networks_request(){}
    get_networks_request(const dvblink::device_id_t& device_id) :
        device_id_(device_id)
        {}

    dvblink::device_id_t device_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & device_id_;
    }
};

//
// get_device_status
//
struct get_device_status_response
{
    bool result_;
    dvblex::device_status_ex_t status_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
        ar & status_;
    }
};

struct get_device_status_request : public message_send<get_device_status_request, get_device_status_response>
{
    get_device_status_request(){}
    get_device_status_request(const dvblink::device_id_t& device_id) :
        device_id_(device_id)
        {}

    dvblink::device_id_t device_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & device_id_;
    }
};

//
// drop_headend_on_device
//
struct drop_headend_on_device_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct drop_headend_on_device_request : public message_send<drop_headend_on_device_request, drop_headend_on_device_response>
{
    drop_headend_on_device_request(){}
    drop_headend_on_device_request(const dvblink::device_id_t& device_id, const dvblink::headend_id_t& headend_id) :
        device_id_(device_id), headend_id_(headend_id)
        {}

    dvblink::device_id_t device_id_;
    dvblink::headend_id_t headend_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & device_id_;
        ar & headend_id_;
    }
};

//
// get_device_settings
//
struct get_device_settings_response
{
    bool result_;
    dvblex::parameters_container_t settings_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
        ar & settings_;
    }
};

struct get_device_settings_request : public message_send<get_device_settings_request, get_device_settings_response>
{
    get_device_settings_request(){}
    get_device_settings_request(const dvblink::device_id_t& device_id) :
        device_id_(device_id)
        {}

    dvblink::device_id_t device_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & device_id_;
    }
};

//
// set_device_settings
//
struct set_device_settings_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct set_device_settings_request : public message_send<set_device_settings_request, set_device_settings_response>
{
    set_device_settings_request(){}
    set_device_settings_request(const dvblink::device_id_t& device_id, const dvblex::concise_param_map_t& settings) :
        device_id_(device_id), settings_(settings)
        {}

    dvblink::device_id_t device_id_;
    dvblex::concise_param_map_t settings_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & device_id_;
        ar & settings_;
    }
};

//
// get_device_templates
//
struct get_device_templates_response
{
    dvblex::parameters_container_t templates_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & templates_;
    }
};

struct get_device_templates_request : public message_send<get_device_templates_request, get_device_templates_response>
{
    get_device_templates_request(){}

};

//
// create_manual_device
//
struct create_manual_device_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct create_manual_device_request : public message_send<create_manual_device_request, create_manual_device_response>
{
    create_manual_device_request(){}
    create_manual_device_request(const dvblex::concise_param_map_t& params) :
        params_(params)
        {}

    dvblex::concise_param_map_t params_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & params_;
    }
};

//
// delete_manual_device
//
struct delete_manual_device_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct delete_manual_device_request : public message_send<delete_manual_device_request, delete_manual_device_response>
{
    delete_manual_device_request(){}
    delete_manual_device_request(const dvblink::device_id_t& device_id) :
        device_id_(device_id)
        {}

    dvblink::device_id_t device_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & device_id_;
    }
};

//
// get device channel url
//
struct get_device_channel_url_response
{
    bool result_;
    dvblink::url_address_t url_;
    dvblink::mime_type_t mime_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
        ar & url_;
        ar & mime_;
    }
};

struct get_device_channel_url_request : public message_send<get_device_channel_url_request, get_device_channel_url_response>
{
    get_device_channel_url_request(){}
    get_device_channel_url_request(const dvblink::channel_id_t& channel_id, dvblex::device_channel_stream_type_e format) :
        channel_id_(channel_id), format_(format)
        {}

    dvblink::channel_id_t channel_id_;
    dvblex::device_channel_stream_type_e format_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & channel_id_;
        ar & format_;
    }
};


} //devices
} //messaging
} //dvblink
