/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_EVENT_H__
#define __DVBLINK_EVENT_H__

#include <memory>
#include <boost/noncopyable.hpp>
#include <dl_utils.h>

namespace dvblink {

class event : public boost::noncopyable
{    
public:
    event();
    ~event();   // non-virtual destructor

    errcode_t signal();
    errcode_t wait(timeout_t to = infinite_timeout) const;
    errcode_t reset();
    bool is_signaled() const;

    // added for compatibility with deprecated version
    static const unsigned int INFINITE_TIMEOUT = 0xFFFFFFFF;
    
    bool wait(unsigned int to = INFINITE_TIMEOUT)
    {
        errcode_t err = wait((to == INFINITE_TIMEOUT) ?
            infinite_timeout : boost::posix_time::milliseconds(to));
        return (err == err_none);
    }

private:
    struct _impl;
    typedef std::auto_ptr<_impl> impl_ptr;
    impl_ptr pimpl_;
};

} // namespace dvblink

#endif  // __DVBLINK_EVENT_H__

// $Id: dl_event.h 4816 2012-03-22 12:48:38Z mike $
