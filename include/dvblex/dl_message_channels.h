/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_types.h>
#include <dl_message.h>
#include <dl_devices.h>
#include <dl_channels.h>

namespace dvblink { namespace messaging { namespace channels {

//
// get_device_headend_info
//
struct get_device_headend_info_response
{
    dvblex::device_map_to_headend_list_t devices_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & devices_;
        ar & result_;
    }
};

struct get_device_headend_info_request : public message_send<get_device_headend_info_request, get_device_headend_info_response>
{
};

//
// get_provider_channels
//
struct get_provider_channels_response
{
    dvblex::concise_headend_desc_list_t headends_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & headends_;
        ar & result_;
    }
};

struct get_provider_channels_request : public message_send<get_provider_channels_request, get_provider_channels_response>
{
    get_provider_channels_request()
    {}

    get_provider_channels_request(const dvblink::headend_id_t& headend_id) :
        headend_id_(headend_id)
        {}

    dvblink::headend_id_t headend_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & headend_id_;
    }
};

//
// get_provider_channels
//
struct get_provider_channels_id_response
{
    dvblex::idonly_headend_desc_list_t headends_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & headends_;
        ar & result_;
    }
};

struct get_provider_channels_id_request : public message_send<get_provider_channels_id_request, get_provider_channels_id_response>
{
    get_provider_channels_id_request()
        {}

    get_provider_channels_id_request(const dvblink::headend_id_t& headend_id) :
        headend_id_(headend_id)
        {}

    dvblink::headend_id_t headend_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & headend_id_;
    }
};

//
// get_scanned_channels
//

static const boost::uint32_t get_scanned_channels_level_headends = 1;
static const boost::uint32_t get_scanned_channels_level_transponders = 2;
static const boost::uint32_t get_scanned_channels_level_channels = 3;

struct get_scanned_channels_response
{
    dvblex::concise_headend_desc_list_t headends_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & headends_;
        ar & result_;
    }
};

struct get_scanned_channels_request : public message_send<get_scanned_channels_request, get_scanned_channels_response>
{
    get_scanned_channels_request() 
        : info_level_(get_scanned_channels_level_channels)
    {}

        boost::uint32_t info_level_;
        dvblink::headend_id_t headend_id_;
        dvblink::transponder_id_t transponder_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & info_level_;
        ar & headend_id_;
        ar & transponder_id_;
    }
};

//
// get_channel_favorites
//
struct get_channel_favorites_response
{
    dvblex::channel_favorite_list_t favorites_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & favorites_;
        ar & result_;
    }
};

struct get_channel_favorites_request : public message_send<get_channel_favorites_request, get_channel_favorites_response>
{
    get_channel_favorites_request() 
    {}

    get_channel_favorites_request(const dvblink::favorite_id_t& favorite_id)
        : favorite_id_(favorite_id)
    {}

    dvblink::favorite_id_t favorite_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & favorite_id_;
    }
};

//
// set_channel_favorites
//
struct set_channel_favorites_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct set_channel_favorites_request : public message_send<set_channel_favorites_request, set_channel_favorites_response>
{
    set_channel_favorites_request() {}
    set_channel_favorites_request(const dvblex::channel_favorite_list_t& favorites) :
        favorites_(favorites)
        {}

    dvblex::channel_favorite_list_t favorites_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & favorites_;
    }
};

//
// get_channel_visibility
//
struct get_channel_visibility_response
{
    dvblex::invisible_channel_map_t channel_visibility_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & channel_visibility_;
        ar & result_;
    }
};

struct get_channel_visibility_request : public message_send<get_channel_visibility_request, get_channel_visibility_response>
{
};

//
// set_channel_visibility
//
struct set_channel_visibility_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct set_channel_visibility_request : public message_send<set_channel_visibility_request, set_channel_visibility_response>
{
    set_channel_visibility_request() {}
    set_channel_visibility_request(const dvblex::invisible_channel_map_t& channel_visibility) :
        channel_visibility_(channel_visibility)
        {}

    dvblex::invisible_channel_map_t channel_visibility_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & channel_visibility_;
    }
};

//
// get full channel description for channel id(s)
//
struct get_channels_description_response
{
    dvblex::channel_desc_list_t channels_desc_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & channels_desc_;
        ar & result_;
    }
};

struct get_channels_description_request : public message_send<get_channels_description_request, get_channels_description_response>
{
    get_channels_description_request()
    {}

    dvblex::channel_id_list_t channels_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & channels_;
    }
};

//
// get channel logos
//
struct get_channel_logo_response
{
    dvblex::channel_logo_map_t channel_logo_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & channel_logo_;
    }
};

struct get_channel_logo_request : public message_send<get_channel_logo_request, get_channel_logo_response>
{
    get_channel_logo_request()
    {}

    get_channel_logo_request(const dvblink::url_address_t& server_address, const dvblink::url_proto_t& proto) :
        server_address_(server_address), proto_(proto)
        {}

    dvblink::url_address_t server_address_;
    dvblink::url_proto_t proto_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & server_address_;
        ar & proto_;
    }
};

//
// get channel sets for each device
//
struct get_device_channel_sets_response
{
    dvblex::ts_source_TO_set_of_set_of_channels_set_t device_channel_sets_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & device_channel_sets_;
        ar & result_;
    }
};

struct get_device_channel_sets_request : public message_send<get_device_channel_sets_request, get_device_channel_sets_response>
{
};

///////////////////////////////////////////////////////////////////////////////
//
//Notifies all intersted parties that channel config has changed

struct channel_config_changed_request : public message_post<channel_config_changed_request>
{
    channel_config_changed_request () {}
    channel_config_changed_request(const dvblex::channel_changes_desc_t& changes) : 
        changes_(changes) {}
        
    dvblex::channel_changes_desc_t changes_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & changes_;
    } 
};

//
// get_channel_overwrites
//
struct get_channel_overwrites_response
{
    dvblex::channel_overwrite_map_t overwrites_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & overwrites_;
        ar & result_;
    }
};

struct get_channel_overwrites_request : public message_send<get_channel_overwrites_request, get_channel_overwrites_response>
{
};

//
// set_channel_overwrites
//
struct set_channel_overwrites_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct set_channel_overwrites_request : public message_send<set_channel_overwrites_request, set_channel_overwrites_response>
{
    set_channel_overwrites_request() {}
    set_channel_overwrites_request(const dvblex::channel_overwrite_map_t& overwrites) :
        overwrites_(overwrites)
        {}

    dvblex::channel_overwrite_map_t overwrites_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & overwrites_;
    }
};


} //channels
} //messaging
} //dvblink
