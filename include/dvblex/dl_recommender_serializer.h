/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <map>
#include <vector>
#include <sstream>
#include <dl_types.h>
#include <dl_recommender_types.h>
#include <dl_epgevent.h>
#include <dl_recommender_cmd.h>
#include <dl_pugixml_helper.h>

namespace dvblex {

const std::string create_profile_cmd_node = "create_profile";
const std::string get_profiles_cmd_node = "get_profiles";
const std::string delete_profile_cmd_node = "delete_profile";
const std::string change_profile_name_cmd_node = "change_profile_name";
const std::string get_providers_cmd_node = "get_providers";
const std::string get_provider_channels_cmd_node = "get_provider_channels";
const std::string get_mapped_channels_cmd_node = "get_mapped_channels";
const std::string set_mapped_channels_cmd_node = "set_mapped_channels";
const std::string rate_program_cmd_node = "rate_program";
const std::string get_epg_cmd_node = "get_epg";
const std::string recommendations_node = "recommendations";
const std::string recommend_by_recom_profiles_node = "recommend_by_profiles";
const std::string recommend_like_this_node = "recommend_like_this";

const std::string recom_channel_node = "channel";
const std::string recom_channels_node = "channels";
const std::string recom_channel_id_node = "channel_id";
const std::string recom_channel_id_attr = "channel_id";
const std::string recom_channel_name_node = "channel_name";
const std::string recom_dvblink_channel_id_node = "dvblink_channel_id";
const std::string recom_dvblink_channel_id_attr = "dvblink_channel_id";

const std::string recom_profiles_node = "profiles";
const std::string recom_profile_node = "profile";
const std::string recom_profile_id_node = "profile_id";
const std::string recom_profile_name_node = "profile_name";

const std::string recom_provider_node = "provider";
const std::string recom_provider_id_attr = "provider_id";
const std::string recom_provider_id_node = "provider_id";
const std::string recom_provider_name_node = "provider_name";

const std::string recom_program_node = "program";
const std::string recom_program_id_node = "program_id";
const std::string recom_program_name_node = "program_name";
const std::string recom_event_node = "event";

const std::string recom_rating_node = "rating";
const std::string recom_score_node = "score";
const std::string recom_is_series_node = "is_series";

const std::string recom_start_time_node = "start_time";
const std::string recom_end_time_node = "end_time";
const std::string recommendations_from = "recommendations_from";
const std::string recommendations_to = "recommendations_to";
const std::string extended_epg_node = "extended_epg";

///////////////////////////////////////////////////////////////////////////////

struct create_profile_req
{
    create_profile_req() {}
    create_profile_req(const dvblink::ncanto::dvblink_channel_id_t& chid, const dvblink::ncanto::profile_name_t& profile_name, time_t start, time_t end) :
        chid_(chid),
        profile_name_(profile_name),
        start_(start),
        end_(end)
    {}

    dvblink::ncanto::dvblink_channel_id_t chid_;
    dvblink::ncanto::profile_name_t profile_name_;
    time_t start_, end_;
};

inline pugi::xml_node& operator >> (pugi::xml_node& node, create_profile_req& cmd)
{
    if (NULL != node)
    {       
        std::string str_value;
        if (dvblink::pugixml_helpers::get_node_value(node, recom_channel_id_node, str_value))
        {
            cmd.chid_ = str_value;
        }			
        if (dvblink::pugixml_helpers::get_node_value(node, recom_profile_name_node, str_value))
        {
            cmd.profile_name_ = str_value;
        }			
        if (dvblink::pugixml_helpers::get_node_value(node, recom_start_time_node, str_value))
        {
            cmd.start_ = boost::lexical_cast<time_t>(str_value);
        }			
        if (dvblink::pugixml_helpers::get_node_value(node, recom_end_time_node, str_value))
        {
            cmd.end_ = boost::lexical_cast<time_t>(str_value);
        }			
    }
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const create_profile_req& cmd)
{
    pugi::xml_node cp_node = doc.append_child(create_profile_cmd_node.c_str());
    if (cp_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(cp_node, recom_channel_id_node, cmd.chid_.get());
        dvblink::pugixml_helpers::new_child(cp_node, recom_profile_name_node, cmd.profile_name_.get());

        std::stringstream buf;

        buf.clear(); buf.str("");
        buf << cmd.start_;
        dvblink::pugixml_helpers::new_child(cp_node, recom_start_time_node, buf.str());

        buf.clear(); buf.str("");
        buf << cmd.end_;
        dvblink::pugixml_helpers::new_child(cp_node, recom_end_time_node, buf.str());

    }
    return doc;
}

///////////////////////////////////////////////////////////////////////////////

struct create_profile_response
{
    create_profile_response() {}
    create_profile_response(const dvblink::ncanto::profile_id_t& profile_id) :
        profile_id_(profile_id)
    {}

    dvblink::ncanto::profile_id_t profile_id_;
};

inline pugi::xml_document& operator << (pugi::xml_document& doc, const create_profile_response& cmd)
{
    pugi::xml_node cp_node = doc.append_child(create_profile_cmd_node.c_str());
    if (cp_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(cp_node, recom_profile_id_node, cmd.profile_id_.get());
    }
    return doc;
}

inline pugi::xml_node& operator >> (pugi::xml_node& node, create_profile_response& cmd)
{
    if (NULL != node)
    {       
        std::string str_value;
        if (dvblink::pugixml_helpers::get_node_value(node, recom_profile_id_node, str_value))
        {
            cmd.profile_id_ = str_value;
        }			
    }
    return node;
}

///////////////////////////////////////////////////////////////////////////////

struct get_profiles_req
{
    get_profiles_req() {}
};

inline pugi::xml_node& operator >> (pugi::xml_node& node, get_profiles_req& cmd)
{
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const get_profiles_req& cmd)
{
    pugi::xml_node cp_node = doc.append_child(get_profiles_cmd_node.c_str());
    return doc;
}

///////////////////////////////////////////////////////////////////////////////

struct get_profiles_response
{
    get_profiles_response() {}
    get_profiles_response(const dvblex::tvadviser::map_provider_profiles_t& profiles) :
        profiles_(profiles)
    {}

    dvblex::tvadviser::map_provider_profiles_t profiles_;
};

inline pugi::xml_document& operator << (pugi::xml_document& doc, const get_profiles_response& cmd)
{
    pugi::xml_node gp_node = doc.append_child(get_profiles_cmd_node.c_str());
    if (gp_node != NULL)
    {
        dvblex::tvadviser::map_provider_profiles_t::const_iterator iter = cmd.profiles_.begin();
        while (iter != cmd.profiles_.end())
        {
            pugi::xml_node prov_node = dvblink::pugixml_helpers::new_child(gp_node, recom_provider_node);
            if (prov_node != NULL)
            {
                const dvblink::ncanto::provider_id_t& provider_id = iter->first;

                dvblink::pugixml_helpers::add_node_attribute(prov_node, recom_provider_id_attr, provider_id.get());

                const dvblex::tvadviser::vec_profile_data_t& vec = iter->second;
                for (size_t i = 0; i < vec.size(); ++i)
                {
                    pugi::xml_node prof_node = dvblink::pugixml_helpers::new_child(prov_node, recom_profile_node);
                    if (prof_node != NULL)
                    {
                        const dvblex::tvadviser::profile_data& data = vec[i];
                        dvblink::pugixml_helpers::new_child(prof_node, recom_profile_id_node, data.id_.get());
                        dvblink::pugixml_helpers::new_child(prof_node, recom_profile_name_node, data.name_.get());
                    }
                }
            }

            ++iter;
        }
    }
    return doc;
}

inline pugi::xml_node& operator >> (pugi::xml_node& node, get_profiles_response& cmd)
{
    if (NULL != node)
    {       
        std::string str_value;

        pugi::xml_node prov_node = node.first_child();
        while (prov_node != NULL)
        {
            if (boost::iequals(prov_node.name(), recom_provider_node))
            {
                dvblink::ncanto::provider_id_t provider_id;

                if (dvblink::pugixml_helpers::get_node_attribute(prov_node, recom_provider_id_attr, str_value))
                    provider_id = str_value;

                dvblex::tvadviser::vec_profile_data_t vec;
                pugi::xml_node prof_node = prov_node.first_child();
                while (prof_node != NULL)
                {
                    if (boost::iequals(prof_node.name(), recom_profile_node))
                    {
                        dvblex::tvadviser::profile_data data;

                        if (dvblink::pugixml_helpers::get_node_value(prof_node, recom_profile_id_node, str_value))
                            data.id_ = str_value;
                        if (dvblink::pugixml_helpers::get_node_value(prof_node, recom_profile_name_node, str_value))
                            data.name_ = str_value;

                        vec.push_back(data);
                    }

                    prof_node = prof_node.next_sibling();
                }
                cmd.profiles_[provider_id] = vec;
            }
            prov_node = prov_node.next_sibling();
        }
    }
    return node;
}

///////////////////////////////////////////////////////////////////////////////

struct delete_profile_req
{
    delete_profile_req() {}
    delete_profile_req(const dvblink::ncanto::profile_id_t& profile_id) :
        profile_id_(profile_id)
    {}

    dvblink::ncanto::profile_id_t profile_id_;
};

inline pugi::xml_node& operator >> (pugi::xml_node& node, delete_profile_req& cmd)
{
    if (NULL != node)
    {       
        std::string str_value;
        if (dvblink::pugixml_helpers::get_node_value(node, recom_profile_id_node, str_value))
        {
            cmd.profile_id_ = str_value;
        }			
    }
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const delete_profile_req& cmd)
{
    pugi::xml_node cp_node = doc.append_child(delete_profile_cmd_node.c_str());
    if (cp_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(cp_node, recom_profile_id_node, cmd.profile_id_.get());
    }
    return doc;
}

///////////////////////////////////////////////////////////////////////////////

struct change_profile_name_req
{
    change_profile_name_req() {}
    change_profile_name_req(const dvblink::ncanto::profile_id_t& profile_id, const dvblink::ncanto::profile_name_t& new_profile_name) :
        profile_id_(profile_id),
        new_profile_name_(new_profile_name)
    {}

    dvblink::ncanto::profile_id_t profile_id_;
    dvblink::ncanto::profile_name_t new_profile_name_;
};

inline pugi::xml_node& operator >> (pugi::xml_node& node, change_profile_name_req& cmd)
{
    if (NULL != node)
    {       
        std::string str_value;
        if (dvblink::pugixml_helpers::get_node_value(node, recom_profile_id_node, str_value))
        {
            cmd.profile_id_ = str_value;
        }			
        if (dvblink::pugixml_helpers::get_node_value(node, recom_profile_name_node, str_value))
        {
            cmd.new_profile_name_ = str_value;
        }			
    }
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const change_profile_name_req& cmd)
{
    pugi::xml_node cp_node = doc.append_child(change_profile_name_cmd_node.c_str());
    if (cp_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(cp_node, recom_profile_id_node, cmd.profile_id_.get());
        dvblink::pugixml_helpers::new_child(cp_node, recom_profile_name_node, cmd.new_profile_name_.get());
    }
    return doc;
}

///////////////////////////////////////////////////////////////////////////////

struct get_providers_req
{
    get_providers_req() {}
};

inline pugi::xml_node& operator >> (pugi::xml_node& node, get_providers_req& cmd)
{
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const get_providers_req& cmd)
{
    pugi::xml_node cp_node = doc.append_child(get_providers_cmd_node.c_str());
    return doc;
}

///////////////////////////////////////////////////////////////////////////////

struct get_providers_response
{
    get_providers_response() {}
    get_providers_response(const dvblink::ncanto::map_provider_id_name_t& providers) :
        providers_(providers)
    {}

    dvblink::ncanto::map_provider_id_name_t providers_;
};

inline pugi::xml_document& operator << (pugi::xml_document& doc, const get_providers_response& cmd)
{
    pugi::xml_node gp_node = doc.append_child(get_providers_cmd_node.c_str());
    if (gp_node != NULL)
    {
        dvblink::ncanto::map_provider_id_name_t::const_iterator iter = cmd.providers_.begin();
        while (iter != cmd.providers_.end())
        {
            pugi::xml_node prov_node = dvblink::pugixml_helpers::new_child(gp_node, recom_provider_node);
            if (prov_node != NULL)
            {
                dvblink::pugixml_helpers::new_child(prov_node, recom_provider_id_node, iter->first.get());
                dvblink::pugixml_helpers::new_child(prov_node, recom_provider_name_node, iter->second.get());
            }
            ++iter;
        }
    }
    return doc;
}

inline pugi::xml_node& operator >> (pugi::xml_node& node, get_providers_response& cmd)
{
    if (NULL != node)
    {       
        std::string str_value;

        pugi::xml_node prov_node = node.first_child();
        while (prov_node != NULL)
        {
            if (boost::iequals(prov_node.name(), recom_provider_node))
            {
                dvblink::ncanto::provider_id_t provider_id;
                dvblink::ncanto::provider_name_t provider_name;

                if (dvblink::pugixml_helpers::get_node_value(prov_node, recom_provider_id_node, str_value))
                    provider_id = str_value;

                if (dvblink::pugixml_helpers::get_node_value(prov_node, recom_provider_name_node, str_value))
                    provider_name = str_value;

                cmd.providers_[provider_id] = provider_name;
            }
            prov_node = prov_node.next_sibling();
        }
    }
    return node;
}

///////////////////////////////////////////////////////////////////////////////

struct get_provider_channels_req
{
    get_provider_channels_req() {}
    get_provider_channels_req(const dvblink::ncanto::provider_id_t provider_id) :
        provider_id_(provider_id)    
    {}

    dvblink::ncanto::provider_id_t provider_id_;
};

inline pugi::xml_node& operator >> (pugi::xml_node& node, get_provider_channels_req& cmd)
{
    if (NULL != node)
    {       
        std::string str_value;
        if (dvblink::pugixml_helpers::get_node_value(node, recom_provider_id_node, str_value))
        {
            cmd.provider_id_ = str_value;
        }			
    }
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const get_provider_channels_req& cmd)
{
    pugi::xml_node node = doc.append_child(get_provider_channels_cmd_node.c_str());
    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, recom_provider_id_node, cmd.provider_id_.get());
    }
    return doc;
}

///////////////////////////////////////////////////////////////////////////////

struct get_provider_channels_response
{
    get_provider_channels_response() {}
    get_provider_channels_response(const dvblink::ncanto::map_source_id_to_name_t& channels) :
        channels_(channels)
    {}

    dvblink::ncanto::map_source_id_to_name_t channels_;
};

inline pugi::xml_document& operator << (pugi::xml_document& doc, const get_provider_channels_response& cmd)
{
    pugi::xml_node gpc_node = doc.append_child(get_provider_channels_cmd_node.c_str());
    if (gpc_node != NULL)
    {
        dvblink::ncanto::map_source_id_to_name_t::const_iterator iter = cmd.channels_.begin();
        while (iter != cmd.channels_.end())
        {
            pugi::xml_node ch_node = dvblink::pugixml_helpers::new_child(gpc_node, recom_channel_node);
            if (ch_node != NULL)
            {
                dvblink::pugixml_helpers::new_child(ch_node, recom_channel_id_node, iter->first.get());
                dvblink::pugixml_helpers::new_child(ch_node, recom_channel_name_node, iter->second.get());
            }
            ++iter;
        }
    }
    return doc;
}

inline pugi::xml_node& operator >> (pugi::xml_node& node, get_provider_channels_response& cmd)
{
    if (NULL != node)
    {       
        std::string str_value;

        pugi::xml_node ch_node = node.first_child();
        while (ch_node != NULL)
        {
            if (boost::iequals(ch_node.name(), recom_channel_node))
            {
                dvblink::ncanto::source_id_t source_id;
                dvblink::ncanto::source_name_t source_name;

                if (dvblink::pugixml_helpers::get_node_value(ch_node, recom_channel_id_node, str_value))
                    source_id = str_value;

                if (dvblink::pugixml_helpers::get_node_value(ch_node, recom_channel_name_node, str_value))
                    source_name = str_value;

                cmd.channels_[source_id] = source_name;
            }
            ch_node = ch_node.next_sibling();
        }
    }
    return node;
}

///////////////////////////////////////////////////////////////////////////////

struct get_mapped_channels_req
{
    get_mapped_channels_req() {}
};

inline pugi::xml_node& operator >> (pugi::xml_node& node, get_mapped_channels_req& cmd)
{
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const get_mapped_channels_req& cmd)
{
    pugi::xml_node cp_node = doc.append_child(get_mapped_channels_cmd_node.c_str());
    return doc;
}

///////////////////////////////////////////////////////////////////////////////

struct get_mapped_channels_response
{
    get_mapped_channels_response() {}
    get_mapped_channels_response(const dvblink::ncanto::map_provider_to_map_dvblink_ch_ncanto_ch_t& channels) :
        channels_(channels)
    {}

    dvblink::ncanto::map_provider_to_map_dvblink_ch_ncanto_ch_t channels_;
};

inline pugi::xml_document& operator << (pugi::xml_document& doc, const get_mapped_channels_response& cmd)
{
    pugi::xml_node gmc_node = doc.append_child(get_mapped_channels_cmd_node.c_str());
    if (gmc_node != NULL)
    {
        dvblink::ncanto::map_provider_to_map_dvblink_ch_ncanto_ch_t::const_iterator iter = cmd.channels_.begin();
        while (iter != cmd.channels_.end())
        {
            pugi::xml_node prov_node = dvblink::pugixml_helpers::new_child(gmc_node, recom_provider_node);
            if (prov_node != NULL)
            {
                const dvblink::ncanto::provider_id_t& provider_id = iter->first;
                dvblink::pugixml_helpers::add_node_attribute(prov_node, recom_provider_id_attr, provider_id.get());

                const dvblink::ncanto::map_dvblink_channel_ncanto_channel_t& map = iter->second;
                dvblink::ncanto::map_dvblink_channel_ncanto_channel_t::const_iterator it = map.begin();
                while (it != map.end())
                {
                    pugi::xml_node ch_node = dvblink::pugixml_helpers::new_child(prov_node, recom_channel_node);
                    if (ch_node != NULL)
                    {
                        dvblink::pugixml_helpers::new_child(ch_node, recom_dvblink_channel_id_node, it->first.get());
                        dvblink::pugixml_helpers::new_child(ch_node, recom_channel_id_node, it->second.get());
                    }
                    ++it;
                }
            }
            ++iter;
        }
    }
    return doc;
}

inline pugi::xml_node& operator >> (pugi::xml_node& node, get_mapped_channels_response& cmd)
{
    if (NULL != node)
    {       
        std::string str_value;

        pugi::xml_node prov_node = node.first_child();
        while (prov_node != NULL)
        {
            if (boost::iequals(prov_node.name(), recom_provider_node))
            {
                dvblink::ncanto::provider_id_t provider_id;

                if (dvblink::pugixml_helpers::get_node_attribute(prov_node, recom_provider_id_attr, str_value))
                    provider_id = str_value;

                dvblink::ncanto::map_dvblink_channel_ncanto_channel_t chmap;
                pugi::xml_node ch_node = prov_node.first_child();
                while (ch_node != NULL)
                {
                    if (boost::iequals(ch_node.name(), recom_channel_node))
                    {
                        dvblink::ncanto::dvblink_channel_id_t channel_id;
                        dvblink::ncanto::source_id_t source_id;

                        if (dvblink::pugixml_helpers::get_node_value(ch_node, recom_dvblink_channel_id_node, str_value))
                            channel_id = str_value;
                        if (dvblink::pugixml_helpers::get_node_value(ch_node, recom_channel_id_node, str_value))
                            source_id = str_value;

                        chmap[channel_id] = source_id;
                    }
                    ch_node = ch_node.next_sibling();
                }

                cmd.channels_[provider_id] = chmap;
            }
            prov_node = prov_node.next_sibling();
        }
    }
    return node;
}


///////////////////////////////////////////////////////////////////////////////

struct rate_program_req
{
    rate_program_req() {}
    rate_program_req(const dvblink::ncanto::profile_id_t& profile_id, const dvblink::ncanto::dvblink_channel_id_t& channel_id, time_t start_time, time_t end_time, const dvblink::ncanto::rating_t& rating) :
        profile_id_(profile_id),
        channel_id_(channel_id),
        start_time_(start_time),
        end_time_(end_time),
        rating_(rating)
    {}

    dvblink::ncanto::profile_id_t profile_id_;
    dvblink::ncanto::dvblink_channel_id_t channel_id_;
    time_t start_time_, end_time_;
    dvblink::ncanto::rating_t rating_;
};

inline pugi::xml_node& operator >> (pugi::xml_node& node, rate_program_req& cmd)
{
    if (NULL != node)
    {       
        std::string str_value;
        if (dvblink::pugixml_helpers::get_node_value(node, recom_profile_id_node, str_value))
        {
            cmd.profile_id_ = str_value;
        }			
        if (dvblink::pugixml_helpers::get_node_value(node, recom_dvblink_channel_id_node, str_value))
        {
            cmd.channel_id_ = str_value;
        }			
        if (dvblink::pugixml_helpers::get_node_value(node, recom_start_time_node, str_value))
        {
            cmd.start_time_ = boost::lexical_cast<time_t>(str_value);
        }			
        if (dvblink::pugixml_helpers::get_node_value(node, recom_end_time_node, str_value))
        {
            cmd.end_time_ = boost::lexical_cast<time_t>(str_value);
        }			
        if (dvblink::pugixml_helpers::get_node_value(node, recom_rating_node, str_value))
        {
            cmd.rating_ = boost::lexical_cast<dvblink::ncanto::rating_t::type_t>(str_value);
        }			
    }
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const rate_program_req& cmd)
{
    pugi::xml_node node = doc.append_child(rate_program_cmd_node.c_str());
    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, recom_profile_id_node, cmd.profile_id_.get());
        dvblink::pugixml_helpers::new_child(node, recom_dvblink_channel_id_node, cmd.channel_id_.get());

        std::stringstream buf;

        buf.clear(); buf.str("");
        buf << cmd.start_time_;
        dvblink::pugixml_helpers::new_child(node, recom_start_time_node, buf.str());

        buf.clear(); buf.str("");
        buf << cmd.end_time_;
        dvblink::pugixml_helpers::new_child(node, recom_end_time_node, buf.str());

        buf.clear(); buf.str("");
        buf << cmd.rating_.get();
        dvblink::pugixml_helpers::new_child(node, recom_rating_node, buf.str());

    }
    return doc;
}

///////////////////////////////////////////////////////////////////////////////

struct recommend_by_profiles_req
{
    recommend_by_profiles_req() {}
    recommend_by_profiles_req(const dvblink::ncanto::profile_ids_t& profiles, time_t start_time, time_t end_time) :
        profiles_(profiles),
        start_time_(start_time),
        end_time_(end_time)
    {}

    dvblink::ncanto::profile_ids_t profiles_;
    time_t start_time_, end_time_;
};

inline pugi::xml_node& operator >> (pugi::xml_node& node, recommend_by_profiles_req& cmd)
{
    if (NULL != node)
    {       
        std::string val;
        if (dvblink::pugixml_helpers::get_node_value(node, recom_start_time_node, val))
        {
            cmd.start_time_ = boost::lexical_cast<time_t>(val);
        }
        if (dvblink::pugixml_helpers::get_node_value(node, recom_end_time_node, val))
        {
            cmd.end_time_ = boost::lexical_cast<time_t>(val);
        }

        pugi::xml_node profiles = node.child(recom_profiles_node.c_str());
        if (profiles != NULL)
        {
            pugi::xml_node profile = profiles.first_child();
            while (profile != NULL && boost::iequals(profile.name(), recom_profile_id_node)) 
            {
                std::string profile_id = profile.child_value();
                cmd.profiles_.push_back(profile_id);
                profile = profile.next_sibling();
            }
        }
    }
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const recommend_by_profiles_req& cmd)
{
    pugi::xml_node node = doc.append_child(recommend_by_recom_profiles_node.c_str());
    if (node != NULL)
    {
        std::stringstream buf;

        buf.clear(); buf.str("");
        buf << cmd.start_time_;
        dvblink::pugixml_helpers::new_child(node, recom_start_time_node, buf.str());

        buf.clear(); buf.str("");
        buf << cmd.end_time_;
        dvblink::pugixml_helpers::new_child(node, recom_end_time_node, buf.str());

        pugi::xml_node profiles = dvblink::pugixml_helpers::new_child(node, recom_profiles_node);
        if (profiles != NULL)
        {
            for (size_t i=0; i<cmd.profiles_.size(); i++)
                dvblink::pugixml_helpers::new_child(profiles, recom_profile_id_node, cmd.profiles_[i].get());
        }
    }
    return doc;
}

///////////////////////////////////////////////////////////////////////////////

struct recommend_like_this_req
{
    recommend_like_this_req() {}
    recommend_like_this_req(const dvblink::ncanto::dvblink_channel_id_t& dch_id, time_t program_start_time,
            time_t program_end_time, time_t recommendations_start_time, time_t recommendations_end_time) :
        dch_id_(dch_id),
        program_start_time_(program_start_time),
        program_end_time_(program_end_time),
        recommendations_from_(recommendations_start_time),
        recommendations_to_(recommendations_end_time)
    {}

    dvblink::ncanto::dvblink_channel_id_t dch_id_;
    time_t program_start_time_, program_end_time_;
    time_t recommendations_from_, recommendations_to_;
};

inline pugi::xml_node& operator >> (pugi::xml_node& node, recommend_like_this_req& cmd)
{
    if (node != NULL)
    {       
        std::string val;
        if (dvblink::pugixml_helpers::get_node_value(node, recom_dvblink_channel_id_node, val))
        {
            cmd.dch_id_ = val;
        }
        if (dvblink::pugixml_helpers::get_node_value(node, recom_start_time_node, val))
        {
            cmd.program_start_time_ = boost::lexical_cast<time_t>(val);
        }
        if (dvblink::pugixml_helpers::get_node_value(node, recom_end_time_node, val))
        {
            cmd.program_end_time_ = boost::lexical_cast<time_t>(val);
        }
        if (dvblink::pugixml_helpers::get_node_value(node, recommendations_from, val))
        {
            cmd.recommendations_from_ = boost::lexical_cast<time_t>(val);
        }
        if (dvblink::pugixml_helpers::get_node_value(node, recommendations_to, val))
        {
            cmd.recommendations_to_ = boost::lexical_cast<time_t>(val);
        }
    }
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const recommend_like_this_req& cmd)
{
    pugi::xml_node node = doc.append_child(recommend_like_this_node.c_str());
    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, recom_dvblink_channel_id_node, cmd.dch_id_ .get());

        std::stringstream buf;

        buf.clear(); buf.str("");
        buf << cmd.program_start_time_;
        dvblink::pugixml_helpers::new_child(node, recom_start_time_node, buf.str());

        buf.clear(); buf.str("");
        buf << cmd.program_end_time_;
        dvblink::pugixml_helpers::new_child(node, recom_end_time_node, buf.str());

        buf.clear(); buf.str("");
        buf << cmd.recommendations_from_;
        dvblink::pugixml_helpers::new_child(node, recommendations_from, buf.str());

        buf.clear(); buf.str("");
        buf << cmd.recommendations_to_;
        dvblink::pugixml_helpers::new_child(node, recommendations_to, buf.str());

    }
    return doc;
}

///////////////////////////////////////////////////////////////////////////////

struct recommend_assets_response
{
    recommend_assets_response() {}
    recommend_assets_response(const dvblex::tvadviser::map_channel_programs_t& map) :
        map_(map)
    {}
    dvblex::tvadviser::map_channel_programs_t map_;
};

inline pugi::xml_document& operator << (pugi::xml_document& doc, const recommend_assets_response& cmd)
{
    pugi::xml_node rec_node = doc.append_child(recommendations_node.c_str());
    if (rec_node != NULL)
    {
        dvblex::tvadviser::map_channel_programs_t::const_iterator iter = cmd.map_.begin();
        while (iter != cmd.map_.end())
        {
            const dvblink::ncanto::dvblink_channel_id_t& dch_id = iter->first;
            const dvblex::tvadviser::mixed_program_info_t& vec = iter->second;

            pugi::xml_node ch_node = dvblink::pugixml_helpers::new_child(rec_node, recom_channel_node);
            if (ch_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(ch_node, recom_dvblink_channel_id_attr, dch_id.get());
                for (size_t i = 0; i < vec.size(); ++i)
                {
                    pugi::xml_node prg_node = dvblink::pugixml_helpers::new_child(ch_node, recom_program_node);
                    if (prg_node != NULL)
                    {
                        const dvblex::tvadviser::mixed_program_info& info = vec[i];

                        dvblink::pugixml_helpers::new_child(prg_node, recom_start_time_node, boost::lexical_cast<std::string>(info.start_));
                        dvblink::pugixml_helpers::new_child(prg_node, recom_end_time_node, boost::lexical_cast<std::string>(info.end_));

                        dvblink::pugixml_helpers::new_child(prg_node, recom_rating_node, boost::lexical_cast<std::string>(info.rating_.get()));
                        dvblink::pugixml_helpers::new_child(prg_node, recom_score_node, boost::lexical_cast<std::string>(info.score_.get()));
                        if (info.is_series_)
                            dvblink::pugixml_helpers::new_child(prg_node, recom_is_series_node, "");

                        pugi::xml_node ev_node = dvblink::pugixml_helpers::new_child(prg_node, recom_event_node);
                        if (ev_node != NULL)
                            serialize(const_cast<dvblink::engine::DLEPGEvent&>(info.event_), ev_node);
                    }
                }
            }
            ++iter;
        }
    }

    return doc;
}

inline pugi::xml_node& operator >> (pugi::xml_node& node, recommend_assets_response& cmd)
{
    if (node != NULL)
    {       
        std::string val;

        pugi::xml_node ch_node = node.first_child();
        while (ch_node != NULL) 
        {
            if (boost::iequals(ch_node.name(), recom_channel_node))
            {
                dvblink::ncanto::dvblink_channel_id_t dch_id;
                if (dvblink::pugixml_helpers::get_node_attribute(ch_node, recom_dvblink_channel_id_attr, val))
                    dch_id = val;

                dvblex::tvadviser::mixed_program_info_t prg_vec;
                pugi::xml_node prg_node = ch_node.first_child();
                while (prg_node != NULL) 
                {
                    if (boost::iequals(prg_node.name(), recom_program_node))
                    {
                        dvblex::tvadviser::mixed_program_info info;

                        if (dvblink::pugixml_helpers::get_node_value(prg_node, recom_start_time_node, val))
                            info.start_ = boost::lexical_cast<time_t>(val);

                        if (dvblink::pugixml_helpers::get_node_value(prg_node, recom_end_time_node, val))
                            info.end_ = boost::lexical_cast<time_t>(val);

                        if (dvblink::pugixml_helpers::get_node_value(prg_node, recom_rating_node, val))
                            info.rating_ = boost::lexical_cast<float>(val);

                        if (dvblink::pugixml_helpers::get_node_value(prg_node, recom_score_node, val))
                            info.score_ = boost::lexical_cast<float>(val);

                        info.is_series_ = (prg_node.child(recom_is_series_node.c_str()) != NULL);

                        pugi::xml_node ev_node = prg_node.child(recom_event_node.c_str());
                        if (ev_node != NULL)
                            deserialize(ev_node, info.event_);

                        prg_vec.push_back(info);
                    }
                    prg_node = prg_node.next_sibling();
                }

                cmd.map_[dch_id] = prg_vec;
            }
            ch_node = ch_node.next_sibling();
        }
    }
    return node;
}

} //dvblex
