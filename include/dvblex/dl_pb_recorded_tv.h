/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <dl_platforms.h>
#include <dl_pb_item.h>
#include <dl_pb_video_info.h>
#include <dl_pugixml_helper.h>

namespace dvblex { namespace playback {

enum pb_recorded_tv_state_e
{
    rtvs_in_progress,
    rtvs_error,
    rtvs_forced_to_completion,
    rtvs_completed
};

struct pb_recorded_tv_t : public pb_item_t
{
    pb_recorded_tv_t() :
      pb_item_t(pbit_item_recorded_tv), channel_number_(-1), channel_subnumber_(0), state_(rtvs_completed), series_schedule_(false) { }
      
    pb_video_info_t video_info_;
    dvblink::channel_id_t channel_id_;
    dvblink::channel_name_t channel_name_;
	dvblink::channel_number_t channel_number_;
	dvblink::channel_subnumber_t channel_subnumber_;
	pb_recorded_tv_state_e state_;
    dvblink::schedule_id_t schedule_id_;
    dvblink::schedule_name_t schedule_name_;
    bool series_schedule_;
};


const std::string recorded_tv_root_node              = "recorded_tv";
const std::string recorded_tv_channel_id_node        = "channel_id";
const std::string recorded_tv_channel_name_node      = "channel_name";
const std::string recorded_tv_channel_number_node    = "channel_number";
const std::string recorded_tv_channel_subnumber_node = "channel_subnumber";
const std::string recorded_tv_state_node             = "state";
const std::string recorded_tv_schedule_id_node       = "schedule_id";
const std::string recorded_tv_schedule_name_node     = "schedule_name";
const std::string recorded_tv_schedule_series_node   = "schedule_series";

inline void write_to_node(pugi::xml_node& node, const pb_recorded_tv_t& recorded_tv)
{
    std::stringstream str_out;

    pugi::xml_node rectv_node = dvblink::pugixml_helpers::new_child(node, recorded_tv_root_node);
    if (rectv_node != NULL)
    {
        write_to_node(rectv_node, (pb_item_t)recorded_tv);
	
        dvblink::pugixml_helpers::new_child(rectv_node, recorded_tv_channel_id_node, recorded_tv.channel_id_.get());
        dvblink::pugixml_helpers::new_child(rectv_node, recorded_tv_channel_name_node, recorded_tv.channel_name_.get());

        str_out.clear(); str_out.str("");
        str_out << recorded_tv.channel_number_.get();
        dvblink::pugixml_helpers::new_child(rectv_node, recorded_tv_channel_number_node, str_out.str());

        str_out.clear(); str_out.str("");
        str_out << recorded_tv.channel_subnumber_.get();
        dvblink::pugixml_helpers::new_child(rectv_node, recorded_tv_channel_subnumber_node, str_out.str());

        str_out.clear(); str_out.str("");
        str_out << recorded_tv.state_;
        dvblink::pugixml_helpers::new_child(rectv_node, recorded_tv_state_node, str_out.str());

        dvblink::pugixml_helpers::new_child(rectv_node, recorded_tv_schedule_id_node, recorded_tv.schedule_id_.get());
        dvblink::pugixml_helpers::new_child(rectv_node, recorded_tv_schedule_name_node, recorded_tv.schedule_name_.get());

        if (recorded_tv.series_schedule_)
            dvblink::pugixml_helpers::new_child(rectv_node, recorded_tv_schedule_series_node, dvblink::pugixml_helpers::xmlnode_value_true);

        write_to_node(rectv_node, recorded_tv.video_info_);
    }
}

inline void read_from_node(pugi::xml_node& node, pb_recorded_tv_t& recorded_tv)
{
    std::string str;

    read_from_node(node, (pb_item_t&)recorded_tv);

	if (dvblink::pugixml_helpers::get_node_value(node, recorded_tv_channel_id_node, str))
		recorded_tv.channel_id_ = str;

	if (dvblink::pugixml_helpers::get_node_value(node, recorded_tv_channel_name_node, str))
		recorded_tv.channel_name_ = str;

    boost::int32_t value;
    if (dvblink::pugixml_helpers::get_node_value(node, recorded_tv_channel_number_node, str))
    {
        dvblink::engine::string_conv::apply(str.c_str(), value, (boost::int32_t)recorded_tv.channel_number_.get());
        recorded_tv.channel_number_ = value;
    }

    if (dvblink::pugixml_helpers::get_node_value(node, recorded_tv_channel_subnumber_node, str))
    {
        dvblink::engine::string_conv::apply(str.c_str(), value, (boost::int32_t)recorded_tv.channel_subnumber_.get());
        recorded_tv.channel_subnumber_ = value;
    }

    boost::uint32_t uvalue;
    if (dvblink::pugixml_helpers::get_node_value(node, recorded_tv_state_node, str))
    {
        dvblink::engine::string_conv::apply(str.c_str(), uvalue, (boost::uint32_t)recorded_tv.state_);
        recorded_tv.state_ = (pb_recorded_tv_state_e)uvalue;
    }

	if (dvblink::pugixml_helpers::get_node_value(node, recorded_tv_schedule_id_node, str))
		recorded_tv.schedule_id_ = str;

	if (dvblink::pugixml_helpers::get_node_value(node, recorded_tv_schedule_name_node, str))
		recorded_tv.schedule_name_ = str;

    recorded_tv.series_schedule_ = dvblink::pugixml_helpers::get_node_value(node, recorded_tv_schedule_series_node, str) &&
        boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);

    read_from_node(node, recorded_tv.video_info_);
}

struct pb_stop_recording_t
{
    dvblink::object_id_t object_id_;
};

const std::string stop_recording_root_node    = "stop_recording";
const std::string stop_recording_id_node      = "object_id";

inline pugi::xml_node& operator>> (pugi::xml_node& node, pb_stop_recording_t& pb_stop_recording_request)
{
    if (NULL != node)
    {
        std::string str;

        if (dvblink::pugixml_helpers::get_node_value(node, stop_recording_id_node, str))
            pb_stop_recording_request.object_id_ = str;
    }
    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const pb_stop_recording_t& pb_stop_recording_request)
{
    pugi::xml_node srr_node = doc.append_child(stop_recording_root_node.c_str());
    if (srr_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(srr_node, stop_recording_id_node, pb_stop_recording_request.object_id_.get());
    }

    return doc;
}

} // playback
} // dvblex

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::playback::pb_recorded_tv_t& prtv, const unsigned int /*version*/)
{
    ar & prtv.object_id_;
    ar & prtv.parent_id_;
    ar & prtv.url_;
    ar & prtv.thumbnail_;
    ar & prtv.item_type_;
    ar & prtv.can_be_deleted_;
    ar & prtv.size_;
    ar & prtv.creation_time_;
    ar & prtv.video_info_;
    ar & prtv.channel_id_;
    ar & prtv.channel_name_;
    ar & prtv.channel_number_;
    ar & prtv.channel_subnumber_;
    ar & prtv.state_;
    ar & prtv.schedule_id_;
    ar & prtv.schedule_name_;
    ar & prtv.series_schedule_;
}

} // namespace serialization
} // namespace boost
