/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_PLATFORMS_H_
#define __DVBLINK_DL_PLATFORMS_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <stdexcept>

#if (defined(_MSC_VER))
#define UINT64_CONST(value) value ## UI64
#else
#define UINT64_CONST(value) value ## ULL
#endif

#ifdef _WIN32
typedef unsigned __int64 __uint64;

#define dvblink_pure_virtual(x) x

#else

#include <boost/cstdint.hpp>
#include <inttypes.h>
typedef int64_t __int64;
typedef uint64_t __uint64;

#define wcstok_s wcstok

//Function prototypes
#define _stdcall
#define __stdcall
#define __interface struct
#define __forceinline
#define __cdecl

//Windows types
#define HWND void*
#define HINSTANCE void*

//interface substitutes
#define dvblink_pure_virtual(x) virtual x = 0
#define UNREFERENCED_PARAMETER(x) { (x) = (x); }

union LARGE_INTEGER
{
    struct
    {
        unsigned long LowPart;
        long HighPart;
    };
    struct
    {
        unsigned long LowPart;
        long HighPart;
    } u;
    long long QuadPart;
};

typedef boost::uint32_t DWORD;
typedef boost::uint16_t WORD;

#endif

#ifdef __ANDROID__

inline wchar_t* wcsdup(const wchar_t* ptr)
{
    wchar_t* dup = (wchar_t*) malloc((wcslen(ptr) + 1) * sizeof(wchar_t));
    if (dup != NULL) 
        wcscpy(dup, ptr);

    return dup;
}

#endif

namespace dvblink {

template<typename T, size_t sz>
struct swap_bytes
{
    inline T operator()(T val)
    {
        throw std::out_of_range("data size");
    }
};

template<typename T>
struct swap_bytes<T, 1>
{
    inline T operator()(T val)
    {
        return val;
    }
};

template<typename T>
struct swap_bytes<T, 2>
{
    inline T operator()(T val)
    {
        return ((((val) >> 8) & 0xff) | (((val) & 0xff) << 8));
    }
};

template<typename T>
struct swap_bytes<T, 4>
{
    inline T operator()(T val)
    {
        return ((((val) & 0xff000000) >> 24) |
            (((val) & 0x00ff0000) >>  8) |
            (((val) & 0x0000ff00) <<  8) |
            (((val) & 0x000000ff) << 24));
    }
};

template<typename T>
struct swap_bytes<T, 8>
{
    inline T operator()(T val)
    {
        return ((((val) & 0xff00000000000000ull) >> 56) |
            (((val) & 0x00ff000000000000ull) >> 40) |
            (((val) & 0x0000ff0000000000ull) >> 24) |
            (((val) & 0x000000ff00000000ull) >> 8 ) |
            (((val) & 0x00000000ff000000ull) << 8 ) |
            (((val) & 0x0000000000ff0000ull) << 24) |
            (((val) & 0x000000000000ff00ull) << 40) |
            (((val) & 0x00000000000000ffull) << 56));
    }
};

template<class T>
struct do_byte_swap
{
    inline T operator()(T value)
    {
        return swap_bytes<T, sizeof(T)>()(value);
    }
};

template<class T>
inline T byte_swap(T value)
{
    return do_byte_swap<T>()(value);
}

} // namespace dvblink

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_PLATFORMS_H_
