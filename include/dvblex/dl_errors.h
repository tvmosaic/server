/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_ERRORS_H__
#define __DVBLINK_ERRORS_H__

namespace dvblink {

enum errcode_t
{
    err_none            = 0,
    err_ok              = 0,
    err_success         = 0,
    err_error           = 10000,
    err_unexpected,
    err_no_memory,
    err_no_resources,
    err_no_space,
    err_overflow,
    err_divide_by_zero,
    err_out_of_range,
    err_limit_reached,
    err_timeout,
    err_access_denied,
    err_open_error,
    err_create_error,
    err_eof,
    err_shutdown,
    err_canceled,
    err_busy,
    err_retry,
    err_io_error,
    err_deadlock,
    err_closed,
    err_aborted,
    err_in_progress,
    err_read_error,
    err_write_error,
    err_db_error,
    err_config_error,
    err_delete_error,
    err_exception,
    err_not_found,
    err_not_exists,
    err_not_implemented,
    err_not_initialized,
    err_not_logged_in,
    err_not_bound,
    err_not_connected,
    err_not_ready,
    err_not_resolved,
    err_not_installed,
    err_not_opened,
    err_not_listen,
    err_not_allowed,
    err_not_running,
    err_not_empty,
    err_already_exists,
    err_already_bound,
    err_already_connected,
    err_already_running,
    err_already_opened,
    err_already_initialized,        
    err_invalid_param,
    err_invalid_handle,
    err_invalid_pointer,
    err_invalid_size,
    err_invalid_path,
    err_invalid_address,
    err_invalid_state,
    err_invalid_data,
    err_invalid_format,
    err_invalid_request,        
    err_net_down,
    err_net_unreached,
    err_would_block,
    err_host_unreached,
    err_conn_refused,
    err_send_error,
    err_receive_error,
    err_handshake_failed,
    err_unexpected_command
};

} // namespace dvblink

#endif  // __DVBLINK_ERRORS_H__

// $Id: dl_errors.h 2007 2011-03-03 11:11:21Z mike $
