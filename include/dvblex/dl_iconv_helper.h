/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <wchar.h>
#include <string>

namespace dvblink
{
namespace iconv_helper
{

enum ECodePageIDs
{
	CPID_UTF8,
	CPID_8859_1,
	CPID_8859_2,
	CPID_8859_5,
	CPID_8859_6,
	CPID_8859_7,
	CPID_8859_8,
	CPID_8859_9,
	CPID_8859_13,
	CPID_8859_15,
	CPID_6937,
	CPID_KS_C_5601_1987,
	CPID_X_CP20936, //simplified chinese (GB2312)
	CPID_BIG5,
	CPID_UTF16_BE,
	CPID_KOI8_R,
	CPID_CP_1252,
	CPID_8859_11,
	CPID_8859_3,
	CPID_8859_4
};

bool MultibyteToUnicode(ECodePageIDs codepage, const char* instr, int len, std::wstring& outstr);
bool UnicodeToMultibyte(ECodePageIDs codepage, const wchar_t* instr, int len, std::string& outstr);

} //iconv_helper
} //dvblink

