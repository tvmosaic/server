/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_TS_INFO_H_
#define __DVBLINK_DL_TS_INFO_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <wchar.h>
#include <string>
#include <vector>
#include <ets.h>
#include <dl_platforms.h>
#include <dl_ts.h>
#include <boost/cstdint.hpp>

namespace dvblink { namespace engine {

//****************** STSStreamInfo *********

#define TSI_MAX_NAME_LENGTH	128

/*! Type definition for the information about stream
*/
struct STSStreamInfo
{
    unsigned short currentProgram;	/*!< current program ID */
    unsigned short TSID;				/*!< Transport stream Id */
    unsigned short PMTPid;			/*!< PMT PID */
    unsigned short NetworkId;			/*!< Network ID */
    unsigned short channelType;		/*!< Type of Channel. i.e. 1- Video, 2 - Radio (as in TS SI spec) */
    unsigned short PCRPid;			/*!< PCR PID */
    unsigned char AudioStreamType;	/*!< Type of audio stream:
                                    0 = unknown
                                    3=(ISO/IEC 11172 Audio)
                                    4=(ISO/IEC 13818-3 Audio)
                                    6=(ISO/IEC 13818-1 PES packets containing private data): AC3, DTS etc.*/
    std::wstring ChannelName;
    std::wstring ProviderName;
    unsigned short AudioPID;			/*!< PID of current audio stream */
    unsigned short VideoPID;			/*!< PID of video stream*/
    unsigned short TeletextPID;		/*!< PID of Teletext stream*/
    unsigned char TeletextStreamType;	/*!< Type of teletext stream (as taken from PMT). If 0 - type is unknown. */
    unsigned char VideoStreamType;	/*!< Type of video stream 
							0 - type is unknown. 
							0x02 - ITU-T Rec. H.262 | ISO/IEC 13818-2 Video or ISO/IEC 11172-2 constrained parameter video stream
							*/
};

/*! Type definition for the information about DVB-T channel
*/
struct SDVBTChannelInfo
{
	unsigned long                       centerFreq;	/*!< Center frequency (DVB-T frequency)*/
	ETS_BANDWIDTH_TYPES         bandwidth;	/*!< Bandwidth */
	ETS_CONSTELLATION_TYPES     constellation; /*!< Constellation */
	ETS_HIERARCHY_INFO_TYPES    hierarchyInfo; /*!< Hierarchy information */
	ETS_CODE_RATE_TYPES         hpCodeRate; /*!< Code rate (high) */
	ETS_CODE_RATE_TYPES         lpCodeRate; /*!< Code rate (low) */
	ETS_GUARD_INTERVAL_TYPES    guardInterval; /*!< Guard interval */
	ETS_TRANSMISSION_MODE_TYPES transmission_mode; /*!< Transmission mode */
};

/*! Type definition for LCN (local channel number) structure
*/
struct SDVBTLCNDesc
{
	unsigned short service_id;	/*!< Service ID*/
	unsigned short ts_id;	    /*!< Transport stream ID*/
	unsigned short network_id;	/*!< Network ID*/
	unsigned short onid;	    /*!< Original Network ID*/
	int lcn;	                /*!< Channel number */
};

/*! Type definition for LCN structures list
*/
typedef std::vector<SDVBTLCNDesc> TDVBTLCNList;

/*! This function sets fields of the supplied SDVBTChannelInfo structure to some 
	default values
	\param channel_info - pointer to a structure to fill with the default data
*/
void SetDefaultDVBTChannelInfo(SDVBTChannelInfo* channel_info);

struct STSServiceInfo
{
	unsigned short onid;	/*!< Network ID */
	unsigned short tid;	/*!< Transport stream ID */
	unsigned short sid;	/*!< Service ID */
    int ch_num;
};

struct STSNITTSInfo
{
	unsigned short nid;	/*!< Network ID */
	unsigned short onid;	/*!< Original Network ID */
	unsigned short tid;	/*!< Transport stream ID */
};

/*! Type definition for the extended service information
*/
struct STSServiceInfoEx
{
	unsigned short nid;	/*!< Network ID */
	unsigned short tid;	/*!< Transport stream ID */
	unsigned short sid;	/*!< Service ID */
	bool encrypted;	/*!< encrypted flag */
	unsigned char type; /*!< service type: 1 - tv, 2 - radio etc. as in TS spec */
	std::wstring name;	/*!< service name */
	std::wstring provider;	/*!< provider name */
};

/*! Type definition for the extended ATSC service information
*/
struct STSTVCTServiceInfoEx
{
	unsigned short tid;	/*!< Transport stream ID */
	unsigned short sid;	/*!< service ID */
	unsigned short mj_num; 	/*!< major number  */
	unsigned short mn_num; 	/*!< minor number  */
	unsigned char type; /*!< service type: 1 - tv, 2 - radio etc. as in TS spec */
	unsigned short source_id;	/*!< program source ID */
	std::wstring name;	/*!< service name */
};

/*! Type definition for the MGT tables information
*/
struct STSMGTTablesInfo
{
	unsigned short table_type;	/*!< Table type */
	unsigned short table_pid;	/*!< PID carrying tables of this type*/
};

/*! Type definition for the service information in PMT
*/
struct STSPATServiceInfo
{
	unsigned short tid;		/*!< transport stream ID */
	unsigned short sid;		/*!< Service ID */
	unsigned short pmt_pid;	/*!< PMT PID */
};

struct STSCADescriptorInfo
{
	unsigned short CI_system_ID;		/*!< CI System ID */
	unsigned short PID;				/*!< EMM or ECM pid*/
};

typedef std::vector<unsigned char> TCA_PMT_CA_DESC;

struct TCA_PMT_ES_DESC
{
    unsigned short pid;
    unsigned char type;
	std::vector<TCA_PMT_CA_DESC> ca_desc_list;
    std::vector<TCA_PMT_CA_DESC> other_desc_list;
};

/*! Type definition for the extended elementary stream information
*/
struct STSESInfo
{
	unsigned short PID;	/*!< stream PID */
	unsigned char type; 	/*!< stream type */
    std::vector<STSCADescriptorInfo> ca_descriptors;
};

enum ETS_DELIVERY_SYSTEM
{
    ETS_DS_UNKNOWN = 0x00,
    ETS_DS_CABLE,
    ETS_DS_SAT,
    ETS_DS_SAT_S2,
    ETS_DS_TERRESTRIAL
};

enum ETS_POLARIZATION_TYPES
{
    ETS_PT_HOR = 0x00,
    ETS_PT_VER,
    ETS_PT_LEFT,
    ETS_PT_RIGHT
};

enum ETS_MODULATION_TYPES
{
    ETS_MT_QAM16 = 0x00,
    ETS_MT_QAM32,
    ETS_MT_QAM64,
    ETS_MT_QAM128,
    ETS_MT_QAM256,
    ETS_MT_QPSK,
    ETS_MT_8PSK,
    ETS_MT_NBCQPSK,
};

enum ETS_FEC_TYPES
{
    ETS_FT_AUTO = 0x00,
    ETS_FT_12,
    ETS_FT_23,
    ETS_FT_34,
    ETS_FT_56,
    ETS_FT_78,
    ETS_FT_89,
    ETS_FT_910
};

/*! Type definition for delivery system information
*/
struct STSDeliverySystemInfo
{
    STSDeliverySystemInfo() :
        bFilterServices(false)
    {}

    void add_service(const STSServiceInfo& si)
    {
        bool bFound = false;
        for (unsigned int i=0; i<expectedServices.size(); i++)
        {
            if (expectedServices[i].sid== si.sid && 
                expectedServices[i].tid== si.tid && 
                expectedServices[i].onid == si.onid)
            {
                //found! - update the number if it not -1
                if (si.ch_num != -1)
                    expectedServices[i].ch_num = si.ch_num;

                bFound = true;
                break;
            }
        }
        if (!bFound)
            expectedServices.push_back(si);
    }

    unsigned short nid;                           /*!< network ID */
    unsigned short tid;                           /*!< transport stream ID */
    std::wstring networkName;           /*!< Network name */
    bool bFilterServices;               /*!< Indicates whether expectedServices has valid data*/
    std::vector<STSServiceInfo> expectedServices; /*!< expected services on this transponder */
    ETS_DELIVERY_SYSTEM systemType;     /*!< delivery system type */
    unsigned long frequency;            /*!< transponder frequency */
    unsigned long symbol_rate;          /*!< symbol rate */
    ETS_MODULATION_TYPES modulation;    /*!< modulation */
    ETS_FEC_TYPES fec;                  /*!< fec */
    unsigned char bandwidth;            /*!< dvb-t bandwidth: 6, 7, 8 */
    ETS_POLARIZATION_TYPES polarization;/*!< polarization*/
};

/*! This helper class implements transport stream processing routines
*/
class ts_process_routines
{
public:
	ts_process_routines(){}
	~ts_process_routines(){}

    static boost::uint32_t read_u32bit_from_memory(unsigned char* buffer, int bytes_to_read, unsigned char first_byte_mask);
    

	/*! This function increment version of a secion 
		\param version - in/out version number to increment
	*/
	static void IncPacketVersion(unsigned char& version);

	/*! This function returns incremented continuity counter (and handles the wrap arounds correctly)
		\param counter - current continuity counter value
		\retval Incremented continuity counter value
	*/
	static unsigned short GetNextContinuityCounter(unsigned short counter);

	/*! This function substitutes packet PID to become a NULL packet
		\param packet - pointer to transport stream packet buffer
	*/
    static void change_packet_to_null(unsigned char* packet);

	/*! This function returns payload start indicator value  from TS packet header
		\param buffer - pointer to transport stream packet buffer
		\retval payload start indicator value
	*/
	static int GetPayloadStartIndicator(const void* buffer);

	/*! This function returns transport stream error indicator from TS packet header
		\param buffer - pointer to transport stream packet buffer
		\retval transport stream error indicator
	*/
	static int GetTSErrorIndicator(const void* buffer);

	/*! This function returns continuity indicator from TS packet header
		\param buffer - pointer to transport stream packet buffer
		\retval continuity indicator
	*/
	static unsigned char GetContinuityCounter(const void* buffer);

	/*! This function sets new continuity counter
		\param buffer - pointer to transport stream packet buffer
		\retval continuity indicator
	*/
	static void SetContinuityCounter(const void* buffer, unsigned char new_counter);

	/*! This function returns adaptation field from TS packet header
		\param buffer - pointer to transport stream packet buffer
		\retval adaptation field
	*/
	static int GetAdaptationFieldValue(void* buffer);

	/*! This function returns offset to section data inside the transport stream packet
		\param buffer - pointer to transport stream packet buffer
		\retval -1 if there is no section data in this packet or offset to it otherwise
	*/
	static int GetSectionDataOffset(const void* buffer);

	/*! This function checks whether section's CRC is good and returns true if it is
		\param buffer - pointer to section buffer
		\retval - true if section's CRC matches calculated CRC
	*/
	static bool CheckSectionCRC(const void* buffer);

	/*! This function returns offset to payload of transport stream packet
		\param buffer - pointer to transport stream packet buffer
		\retval -1 in case of adaptation field only packet or offset to payload otherwise 
	*/
	static int GetPayloadOffset(const void* buffer);

	/*! This function fills DVB-T network descriptor with the values of SDVBTChannelInfo structure
		\param buffer - pointer to preallocated network descriptor buffer
		\param ch_info - pointer to SDVBTChannelInfo structure to be used to fill the descriptor
	*/
	static void FillDVBTNetworkDescriptor(unsigned char* buffer, SDVBTChannelInfo* ch_info);

	/*! This function returns PID of transport stream packet
		\param buffer - pointer to transport stream packet buffer
		\retval PID
	*/
	static unsigned short GetPacketPID(const unsigned char* buffer);

	/*! This function sets new PID of transport stream packet
		\param packetBuffer - pointer to transport stream packet buffer
		\param newPid - new PID to set
		\retval PID
	*/
	static bool SetPacketPID(void* packetBuffer, unsigned short newPid);

	/*! This function returns length of the section
		\param buffer - pointer to section buffer
		\retval length of the section
	*/
	static int GetSectionLength(const void* buffer);

	/*! This function finds a descriptor with a particular descriptor ID within an array of descriptors.
		\param desc_ptr - pointer to a descriptors array
		\param desc_len - length of descriptors array
		\param desc_tag - tag of descriptor to llok for
		\retval pointer to a found descriptor in case of success or NULL otherwise
	*/
	static unsigned char* FindDescriptor(unsigned char* desc_ptr, int desc_len, unsigned char desc_tag);

	/*! This function returns PTS of the PES, starting in this transport stream packet
		\param buffer - pointer to transport stream packet
		\retval PTS of the PES or -1 if PES does not start in this packet
	*/
	static boost::int64_t GetPTSValue(const void* buffer);

	/*! This function returns DTS of the PES, starting in this transport stream packet
		\param buffer - pointer to transport stream packet
		\retval DTS of the PES or -1 if PES does not start in this packet
	*/
	static boost::int64_t GetDTSValue(const void* buffer);

    static void SetPTSValue(const void* buffer, boost::int64_t value);
    static void SetDTSValue(const void* buffer, boost::int64_t value);

	/*! This function retrieves length of text descriptor, formatted according to ETSI EN 300 468, Appendix A
		\param name_buf - pointer to text descriptor buffer
		\retval length (in bytes) of text descriptor
	*/
	static int GetTextDescriptorLength(unsigned char* name_buf);

	/*! This function returns PCR value of PES in this transport stream packet
		\param buffer - pointer to transport stream packet
		\param pcr - found PCR value
		\retval true if this transport stream packet contains PCR, false otherwise
	*/
	static bool GetPCRValue(void* buffer, boost::uint64_t& pcr);

	static bool packet_starts_with_keyframe(const unsigned char* buffer);

	//PAT routines

	/*! This function retrieves transport stream ID from PAT section
		\param buffer - pointer to PAT section buffer
		\param pat_section - length of PAT section
		\param TSID - retrieved transport stream ID
		\retval always true 
	*/
	static bool GetTSIDFromPATSection(void* buffer, int pat_section, unsigned short& TSID);

	/*! This function retrieves information about all services, present in PAT section
		\param pat_section - pointer to PAT section buffer
		\param pat_len - length of PAT section
		\param services - array, which contains all found services
		\retval true if PAT was successfully processed, false otherwise
	*/
	static bool GetPATStreamPIDs(unsigned char* pat_section, int pat_len, std::vector<STSPATServiceInfo>& services);

	/*! This function retrieves a list of CA descriptors from CAT section
		\param catBuffer - pointer to CAT section buffer
		\param catLength - length of CAT section
		\param ca_desc_list - list with CA descriptor info
		\retval none
	*/
	static void GetCADescriptorsFromCAT(void* catBuffer, int catLength, std::vector<STSCADescriptorInfo>& ca_desc_list);

	//PMT routines

	/*! This function retrieves a list of CA descriptors from PMT section
		\param pmtBuffer - pointer to PMT section buffer
		\param pmtLength - length of PMT section
		\param ca_desc_list - list with CA descriptor info
		\retval none
	*/
	static void GetCADescriptorsFromPMT(void* pmtBuffer, int pmtLength, std::vector<STSCADescriptorInfo>& ca_desc_list);

	static void GetCADescriptorsFromPMTWithDesc(void* pmtBuffer, int pmtLength, std::vector<TCA_PMT_CA_DESC>& ca_desc_list);

	static bool GetPMTStreamsInfoWithDesc(unsigned char* pmt_section, int pmt_len, unsigned short& ServiceId, std::vector<TCA_PMT_ES_DESC>& stream_info);

	/*! This function retrieves version of PMT section
		\param buffer - pointer to PMT section buffer
		\param sdt_length - length of PMT section
		\retval PMT section version value
	*/
	static unsigned char GetPMTVersion(void* buffer, int length);

	/*! This function sets a new PID for the elementary stream with a provided PID
		\param pmtBuffer - pointer to PMT section buffer
		\param pmtLength - length of PMT section
		\param origPid - original PID
		\param newPid - new PID
		\retval true if PMT was successfully updated, false otherwise
	*/
	static bool ChangePMTSectionStreamPID(void* pmtBuffer, int pmtLength, unsigned short origPid, unsigned short newPid);

	/*! This function write given PCR PID to the provided PMT section and recalculates CRC
		\param pmt_buffer - pointer to PMT section buffer
		\param pmt_length - length of PMT section
		\param pcrPid - new PCR PID
		\retval true if PMT was successfully updated, false otherwise
	*/
	static bool SetPMTSectionPCRPID(void* pmtBuffer, int pmtLength, unsigned short pcrPid);

	/*! This function retrieves PCR PID from the provided PMT section
		\param pmt_buffer - pointer to PMT section buffer
		\param pmt_length - length of PMT section
		\param pcrPid - returned PCR PID
		\retval true if PCR PID was successfully retrieved, false otherwise
	*/
	static bool GetPMTSectionPCRPID(void* pmtBuffer, int pmtLength, unsigned short& pcrPid);

	/*! This function sets a new stream type for a stream with a given stream PID
		\param pmt_buffer - pointer to PMT section buffer
		\param pmt_length - length of PMT section
		\param stream_pid - PID of the stream to set a new type for
		\param stream_type - new stream type
		\retval true if PMT was successfully updated, false otherwise
	*/
	static bool SetPMTStreamType(void* pmt_buffer, int pmt_length, unsigned short stream_pid, unsigned char stream_type);

	/*! This function write given Service ID to the provided PMT section and recalculates CRC
		\param pmt_buffer - pointer to PMT section buffer
		\param pmt_length - length of PMT section
		\param ServiceId - new service ID
		\retval true if PMT was successfully updated, false otherwise
	*/
	static bool SetPMTSectionServiceID(void* pmt_buffer, int pmt_length, unsigned short ServiceId);

	/*! This function retrieves Service ID from the provided PMT section
		\param pmt_buffer - pointer to PMT section buffer
		\param pmt_length - length of PMT section
		\param ServiceId - retrieved service ID
		\retval true if PMT was successfully updated, false otherwise
	*/
	static bool GetPMTSectionServiceID(void* pmt_buffer, int pmt_length, unsigned short& ServiceId);

	/*! This function reteives descriptor from PMT section for a given PID
		\param pid_to_find - PID of the required stream
		\param buffer - pointer to PMT section buffer
		\param pmt_len - length of PMT section
		\param stream_type - this variable receives type of the stream
		\param ret_ptr - pointer to a pointer to a buffer, holding descriptor (allocated inside the function. Must be freed by caller)
		\param desc_length - size of a buffer, holding descriptor
		\retval true if descriptor found and copied, false otherwise
	*/
	static bool GetPMTDescForPID(unsigned short pid_to_find, void* buffer, int pmt_len, unsigned char& stream_type, unsigned char** ret_ptr, int& desc_length);

	/*! This function retrieves PIDs of all streams, described in a given PMT section
		\param pmt_section - pointer to PMT section buffer
		\param pmt_len - length of PMT section
		\param pids - array, which contains all found PIDs
		\retval true if PMT was successfully processed, false otherwise
	*/
	static bool GetPMTStreamPIDs(unsigned char* pmt_section, int pmt_len, std::vector<unsigned short>& pids);

	/*! This function retrieves information about elementary streams within PMT
		\param pmt_section - pointer to PMT section buffer
		\param pmt_len - length of PMT section
		\param stream_info - array, which contains information about ESs
		\retval true if PMT was successfully processed, false otherwise
	*/
	static bool GetPMTStreamsInfo(unsigned char* pmt_section, int pmt_len, std::vector<STSESInfo>& stream_info);

	//VCT routines
	static bool GetTVCTSectionTSID(unsigned char* section, int len, unsigned short& tid);
	static bool GetTVCTSectionStats(unsigned char* section, int len, unsigned char& version, unsigned char& cur_next);
	static bool GetTVCTSectionNumbers(unsigned char* section, int len, unsigned char& cur_section_num, unsigned char& max_section_num);
	static bool GetTVCTServicesEx(unsigned char* section, int len, std::vector<STSTVCTServiceInfoEx>& services);
	static unsigned char GetTVCTTableId(unsigned char* section, int len);

    //MGT routines
	static bool GetMGTTables(unsigned char* section, int len, std::vector<STSMGTTablesInfo>& tables);
	static int GetMGTCurNextIndicator(unsigned char* buffer, int len);

    //STT routines
    static bool GetSTTGPSTimeAndOffset(unsigned char* section, int len, boost::uint32_t& gps_time, boost::uint32_t& gps_offset);

    //ETT ATSC
	static unsigned char GetEttAtscTableId(unsigned char* section, int len);
	static int GetEttAtscCurNextIndicator(unsigned char* buffer, int len);

    //EIT ATSC
	static unsigned char GetEitAtscTableId(unsigned char* section, int len);
	static int GetEitAtscCurNextIndicator(unsigned char* buffer, int len);
	static unsigned short GetEitAtscSourceId(unsigned char* section, int len);
	static unsigned char GetEitAtscSectionVersion(unsigned char* section, int len);
	static unsigned char GetEitAtscMaxSectionNum(unsigned char* section, int len);
	static unsigned char GetEitAtscSectionNum(unsigned char* section, int len);

	//SDT routines

	/*! This function retrieves transport stream ID and network ID from SDT section
		\param buffer - pointer to SDT section buffer
		\param sdt_length - length of SDT section
		\param NetworkId - retrieved network ID
		\param TSId - retrieved transport stream ID
		\retval always true 
	*/
	static bool GetSDTSectionIds(void* buffer, int sdt_length, unsigned short& NetworkId, unsigned short& TSId);

	/*! This function retrieves IDs of all services, described in a given SDT section
		\param sdt_section - pointer to SDT section buffer
		\param sdt_len - length of SDT section
		\param services - array, which contains all found services
		\retval true if SDT was successfully processed, false otherwise
	*/
	static bool GetSDTServices(unsigned char* sdt_section, int sdt_len, std::vector<unsigned short>& services);

	/*! This function retrieves extended information about all services, described in a given SDT section
		\param sdt_section - pointer to SDT section buffer
		\param sdt_len - length of SDT section
		\param services - array, which contains extended information about all found services
		\retval true if SDT was successfully processed, false otherwise
	*/
	static bool GetSDTServicesEx(unsigned char* sdt_section, int sdt_len, std::vector<STSServiceInfoEx>& services);

	/*! This function retrieves version of SDT section
		\param buffer - pointer to SDT section buffer
		\param sdt_length - length of SDT section
		\retval SDT section version value
	*/
	static unsigned char GetSDTVersion(void* buffer, int eit_length);

	/*! This function retrieves number of SDT section
		\param buffer - pointer to SDT section buffer
		\param sdt_length - length of SDT section
		\retval SDT section number value
	*/
	static unsigned char GetSDTCurSectionNum(void* buffer, int eit_length);

	/*! This function retrieves maximum section number of SDT section
		\param buffer - pointer to SDT section buffer
		\param sdt_length - length of SDT section
		\retval maximum section number value
	*/
	static unsigned char GetSDTMaxSectionNum(void* buffer, int sdt_length);

	/*! This function retrieves current-next indicator from SDT section
		\param buffer - pointer to SDT section buffer
		\param sdt_length - length of SDT section
		\retval current-next indicator value
	*/
	static int GetSDTCurNextIndicator(void* buffer, int sdt_length);

	/*! This function retrieves current SDT table id from SDT section
		\param buffer - pointer to SDT section buffer
		\param sdt_length - length of SDT section
		\retval current SDT table id
	*/
	static unsigned char GetSDTTableId(void* buffer, int sdt_length);

    //NIT routines
	/*! This function retrieves all services, described by NIT
		\param buffer - pointer to NIT section buffer
		\param nit_length - length of NIT section
		\param delivery_systems - receives list of all delivery systems, described in this NIT
		\retval true if success
	*/
    static bool GetDeliverySystemsFromNIT(void* buffer, int nit_length, std::vector<STSDeliverySystemInfo>& delivery_systems);

	/*! This function retrieves all transport streams, described in this NIT
		\param buffer - pointer to NIT section buffer
		\param nit_length - length of NIT section
		\param streams_info - receives list of all transport streams, described in this NIT
		\retval true if success
	*/
    static bool GetTSInfoFromNIT(void* buffer, int nit_length, std::vector<STSNITTSInfo>& streams_info);

    /*! This function retrieves NIT section version and current-next indicator
		\param buffer - pointer to NIT section buffer
		\param nit_length - length of NIT section
		\param version - receives version
		\param cur_next - receives current-next indicator
		\retval true if success
	*/
    static bool GetNITSectionStats(unsigned char* section, int len, unsigned char& version, unsigned char& cur_next);

	/*! This function retrieves all services from NIT section that have LCN assigned 
		\param buffer - pointer to NIT section buffer
		\param nit_length - length of NIT section
		\param lcn_services - receives services with LCNs
		\retval true if success
	*/
    static bool GetLCNFromNIT(void* buffer, int nit_length, std::vector<SDVBTLCNDesc>& lcn_services);

	/*! This function retrieves NIT section table ID
		\param buffer - pointer to NIT section buffer
		\param nit_length - length of NIT section
		\retval section table ID
	*/
    static unsigned char GetNITTableId(void* buffer, int sdt_length);

	/*! This function retrieves NIT section current number and maximum number of sections
		\param buffer - pointer to NIT section buffer
		\param nit_length - length of NIT section
		\param cur_section_num - receives current section number
		\param max_section_num - receives maximum sections number
		\retval true if success
	*/
    static bool GetNITSectionNumbers(unsigned char* section, int len, unsigned char& cur_section_num, unsigned char& max_section_num);

	/*! This function retrieves Network ID from NIT
		\param buffer - pointer to NIT section buffer
		\param nit_length - length of NIT section
		\param nid - receives Network ID
		\retval true if success
	*/
    static bool GetNetworkIDFromNIT(void* buffer, int nit_length, unsigned short& nid);

	//EIT routines
	/*! This function retrieves transport stream ID, service ID and network ID from EIT section
		\param buffer - pointer to EIT section buffer
		\param eit_length - length of EIT section
		\param NetworkId - retrieved network ID
		\param TSId - retrieved transport stream ID
		\param ServiceId - retrieved service ID
		\retval always true 
	*/
	static bool GetEITSectionIds(void* buffer, int eit_length, unsigned short& NetworkId, unsigned short& TSId, unsigned short& ServiceId);

	/*! This function sets transport stream ID, service ID and network ID in EIT section
		\param buffer - pointer to EIT section buffer
		\param eit_length - length of EIT section
		\param NetworkId - network ID to set
		\param TSId - transport stream ID  to set
		\param ServiceId - service ID  to set
		\retval always true 
	*/
	static bool SetEITSectionIds(void* buffer, int eit_length, unsigned short NetworkId, unsigned short TSId, unsigned short ServiceId);

	/*! This function retrieves current-next indicator from EIT section
		\param buffer - pointer to EIT section buffer
		\param eit_length - length of EIT section
		\retval current-next indicator value
	*/
	static int GetEITCurNextIndicator(void* buffer, int eit_length);

	/*! This function retrieves version of EIT section
		\param buffer - pointer to EIT section buffer
		\param eit_length - length of EIT section
		\retval EIT section version value
	*/
	static unsigned char GetEITVersion(void* buffer, int eit_length);

	/*! This function retrieves number of EIT section
		\param buffer - pointer to EIT section buffer
		\param eit_length - length of EIT section
		\retval EIT section number value
	*/
	static unsigned char GetEITCurSectionNum(void* buffer, int eit_length);

	/*! This function retrieves maximum section number of EIT section
		\param buffer - pointer to EIT section buffer
		\param eit_length - length of EIT section
		\retval maximum section number value
	*/
	static unsigned char GetEITMaxSectionNum(void* buffer, int eit_length);

	/*! This function retrieves maximum number of segments from EIT section
		\param buffer - pointer to EIT section buffer
		\param eit_length - length of EIT section
		\retval maximum number of segments
	*/
	static unsigned char GetEITMaxSegmentSectionNum(void* buffer, int eit_length);

	/*! This function retrieves last table id from EIT section
		\param buffer - pointer to EIT section buffer
		\param eit_length - length of EIT section
		\retval last table id
	*/
	static unsigned char GetEITLastTableId(void* buffer, int eit_length);

	/*! This function retrieves current EIT table id from EIT section
		\param buffer - pointer to EIT section buffer
		\param eit_length - length of EIT section
		\retval current EIT table id
	*/
	static unsigned char GetEITTableId(void* buffer, int eit_length);

	//TDT routines
	/*! This function retrieves time/date in unix format from TDT section
		\param buffer - pointer to TDT section buffer
		\param length - length of TDT section
		\param out retval - unix time from TDT section
		\retval true if successful
	*/
	static bool GetUTCTimeFromTDT(unsigned char* buffer, int length, time_t& retval);

	//TOT routines
	/*! This function retrieves time/date in unix format from TOT section
		\param buffer - pointer to TOT section buffer
		\param length - length of TOT section
		\param out retval - unix time from TOT section
		\retval true if successful
	*/
	static bool GetUTCTimeFromTOT(unsigned char* buffer, int length, time_t& retval);

	/*! This function converts MJD and seconds offset into UTC unix time
		\param mjd - MJD value in seconds
		\param secs - offset om seconds from mjd
		\retval UTC unux time
	*/
    static time_t MJD2time_t(int mjd, int secs)
    {
        return (mjd - 40587)* 86400 + secs;     /* 40587 is the 1.1.1970 */
    }

	/*! This function converts BCD time into hours/minutes/seconds
		\param bcd_time - pointer to a string with BCD time
		\param out hh - number of hours
		\param out mm - number of minutes
		\param out ss - number of seconds
		\param out total_sec - total number of seconds
		\retval none
	*/
    static void GetHHMMSSFromBCD(unsigned char* bcd_time, int* hh, int* mm, int* ss, int* total_sec)
    {
	    int h = ((bcd_time[0]&0xF0) >> 4)*10+(bcd_time[0]&0x0F);
	    if (hh != NULL)
		    *hh = h;
	    int m = ((bcd_time[1]&0xF0) >> 4)*10+(bcd_time[1]&0x0F);
	    if (mm != NULL)
		    *mm = m;
	    int s = ((bcd_time[2]&0xF0) >> 4)*10+(bcd_time[2]&0x0F);
	    if (ss != NULL)
		    *ss = s;
	    if (total_sec != NULL)
		    *total_sec = h*3600 + m*60 + s;
    }

    static void DescriptorsLinearToArray(SDataChunk& linear_desc, std::vector<TCA_PMT_CA_DESC>& vector_desc);

	static bool IsAudioStream(unsigned char type, std::vector<TCA_PMT_CA_DESC>& stream_desc_list);
	static bool IsVideoStream(unsigned char type, std::vector<TCA_PMT_CA_DESC>& stream_desc_list);
	static bool IsSubtitlesStream(unsigned char type, std::vector<TCA_PMT_CA_DESC>& stream_desc_list);

	static void InsertPCRInPacket(unsigned char* packet_ptr, boost::uint64_t pcr, bool update_adaptation_field = true);

protected:
    static void GetSatDeliverySystemInfo(unsigned char* desc_ptr, int len, STSDeliverySystemInfo& ds_info);
    static void GetCableDeliverySystemInfo(unsigned char* desc_ptr, int len, STSDeliverySystemInfo& ds_info);
    static void GetTerrestrialDeliverySystemInfo(unsigned char* desc_ptr, int len, STSDeliverySystemInfo& ds_info);
};

/*! This structure defines information, required to create PMT section
*/
class STSPMTSectionInfo
{
public:
	STSPMTSectionInfo(){audio_stream_desc = NULL; teletext_stream_desc = NULL; video_stream_desc = NULL; 
						audio_stream_desc_len = 0; teletext_stream_desc_len = 0; video_stream_desc_len = 0;}
	STSStreamInfo ts_info; /*!< information about stream */
	unsigned char* audio_stream_desc; /*!< pointer to audio descriptor buffer (may be NULL if no descriptor is present) */
	unsigned short audio_stream_desc_len; /*!< length of audio descriptor buffer */
	unsigned char* teletext_stream_desc; /*!< pointer to teletext descriptor buffer (may be NULL if no descriptor is present) */
	unsigned short teletext_stream_desc_len; /*!< length of teletext descriptor buffer */
	unsigned char* video_stream_desc; /*!< pointer to video descriptor buffer (may be NULL if no descriptor is present) */
	unsigned short video_stream_desc_len; /*!< length of video descriptor buffer */
};

//Callback function for sending the TS packets
typedef void (*LP_PACKGEN_TS_SEND_FUNC)(const unsigned char* buf, int len, void* param);

/*! This class generates transport stream packets and sections. It also provides functionality
	to split a single section or PES buffer into separate transport stream packets
*/
class ts_packet_generator
{
public:
	ts_packet_generator();
	~ts_packet_generator();

	/*! This function increases version number and wraps it around if needed
		\param version - version number to increase
	*/
	static void IncreaseVersionNumber(unsigned short& version);

	/*! This function creates PAT packet
		\param packet_length - (output param) length of newly created PAT packet (188 bytes)
		\param version - version of PAT packet
		\param cont_counter - continuity counter (it is incremented inside the function)
		\param stream_info - information about the stream
		\retval pointer to newly created PAT packet. This pointer points to a member of a class and should not be deleted by caller!
	*/
	unsigned char* CreatePATPacket(int& packet_length, unsigned char version, unsigned short* cont_counter, 
        unsigned short tid, unsigned short sid, unsigned short pmt_pid);

	/*! This function creates NIT packet
		\param packet_length - (output param) length of newly created NIT packet (188 bytes)
		\param version - version of NIT packet
		\param cont_counter - continuity counter (it is incremented inside the function)
		\param stream_info - information about the stream
		\param dvbt_info - information about the DVB-T channel
		\retval pointer to newly created NIT packet. This pointer points to a member of a class and should not be deleted by caller!
	*/
	unsigned char* CreateNITPacket(int& packet_length, unsigned char version, unsigned short* cont_counter, 
		STSStreamInfo* stream_info, SDVBTChannelInfo* dvbt_info,
		TDVBTLCNList* lcn_list = NULL, unsigned char section_pid = 0x40);

	/*! This function creates PMT section
		\param section_length - (output param) length of newly created PMT section
		\param version - version of PMT section
		\param stream_info - information about the stream
		\retval pointer to newly created PMT section. This pointer points to a member of a class and should not be deleted by caller!
	*/
	unsigned char* CreatePMTSection(int& section_length, unsigned char version, STSPMTSectionInfo* stream_info);

	/*! This function creates SDT packet
		\param packet_length - (output param) length of newly created SDT packet (188 bytes)
		\param version - version of SDT packet
		\param cont_counter - continuity counter (it is incremented inside the function)
		\param stream_info - information about the stream
		\retval pointer to newly created SDT packet. This pointer points to a member of a class and should not be deleted by caller!
	*/
	unsigned char* CreateSDTPacket(int& packet_length, unsigned char version, unsigned short* cont_counter, STSStreamInfo* stream_info, unsigned char section_pid = 0x42);

	/*! This function creates discontinuity packet
		\param packet_length - (output param) length of newly created discontinuity packet (188 bytes)
		\param PID - PID of discontinuity packet
		\param cont_counter - continuity counter (it is incremented inside the function)
		\retval pointer to newly created discontinuity packet. This pointer points to a member of a class and should not be deleted by caller!
	*/
	unsigned char* CreateDiscontinuityPacket(int& packet_length, unsigned short PID, unsigned short* cont_counter);

	//"Split and send" functions
	/*! This function splits provided section buffer into transport stream packets and sends them one by one to provided "send function"
		\param SectionBuf - pointer to section buffer
		\param SectionBufLen - length of section buffer
		\param cont_counter - continuity counter (it is incremented inside the function)
		\param pid - pid of transport stream packets holding this section
		\param send_func - callback function, used to send transport stream packets
		\param param - user parameter passed to callback function
	*/
	void SplitAndSendSectionBuffer(unsigned char* SectionBuf, int SectionBufLen, unsigned short* cont_counter, unsigned short pid,
		LP_PACKGEN_TS_SEND_FUNC send_func, void* param);

	/*! This function splits provided PES buffer into transport stream packets and sends them one by one to provided "send function"
		\param PESBuf - pointer to PES buffer
		\param PESBufLen - length of PES buffer
		\param cont_counter - continuity counter (it is incremented inside the function)
		\param pid - pid of transport stream packets holding this section
		\param pcr - PCR value to insert into first transport stream packet (set to -1 if not required)
		\param payload_start - indicator whether payload starts in this PES 
		\param send_func - callback function, used to send transport stream packets
		\param param - user parameter passed to callback function
	*/
	void SplitAndSendPESBuffer(unsigned char* PESBuf, int PESBufLen, unsigned short* cont_counter, unsigned short pid, boost::int64_t pcr,
		bool payload_start, LP_PACKGEN_TS_SEND_FUNC send_func, void* param);

protected:
	//This buffer is used to create new TS packets
	unsigned char m_TSPacket[TS_PACKET_SIZE];
	//This buffer is used in split-and-send operations
	unsigned char m_TSSendPacket[TS_PACKET_SIZE];
	//Buffer for the entire section (note that section can be more than 1 TS packet)
	unsigned char m_SectionBuffer[2048];

};

enum EPS_PROC_STREAM_TYPES
{
	EPS_PST_VIDEO = 0,
	EPS_PST_VIDEO_MPEG2,
	EPS_PST_VIDEO_MPEG4,
	EPS_PST_AUDIO,
	EPS_PST_AUDIO_MPEG2,
	EPS_PST_AUDIO_AC3,
	EPS_PST_TELETEXT,
    EPS_PST_UNKNOWN
};

/*! This helper class implements program stream processing routines
*/
class ps_process_routines
{
public:
	ps_process_routines(){}
	~ps_process_routines(){}

	/*! This function returns program stream packet ID
		\param buffer - pointer to program stream packet
		\retval program stream packet ID
	*/
	static int GetPackID(unsigned char* buffer);

	/*! This function returns SCR from program stream packet header
		\param buffer - pointer to program stream packet header
		\retval SCR
	*/
    static boost::uint64_t GetPSPackHeaderSCR(unsigned char* buffer);

	/*! This function returns mux rate from program stream packet header
		\param buffer - pointer to program stream packet header
		\retval mux rate
	*/
	static int GetPSPackHeaderMuxRate(unsigned char* buffer);

	/*! This function returns offset to elementary stream payload inside program stream packet
		\param buffer - pointer to program stream packet
		\retval offset to elementary stream payload
	*/
	static int GetESDataOffset(unsigned char* buffer);

	/*! This function checks whether PES packet starts in a given PES buffer
		\param buffer - pointer to PES buffer
		\param buf_len - length of PES buffer
		\param stream_type - type of the stream
		\retval true if PES starts in this PES buffer, false otherwise
	*/
	static bool IsValidPESPacketStart(unsigned char* buffer, int buf_len, EPS_PROC_STREAM_TYPES stream_type);

	/*! This function returns PTS from PES buffer
		\param buffer - pointer to PES buffer
		\retval PTS
	*/
	static boost::int64_t GetPTSValueFromPES(void* buffer);

	/*! This function returns DTS from PES buffer
		\param buffer - pointer to DTS buffer
		\retval DTS
	*/
    static boost::int64_t GetDTSValueFromPES(void* buffer);

    static void SetPTSInPES(void* buffer, boost::int64_t value);

    static void SetDTSInPES(void* buffer, boost::int64_t value);

protected:
    static void SetxTSInPES(void* buffer, boost::int64_t value, int offset, unsigned char start_mask);
};

class pes_mpeg12_video_process_routines
{
public:
	pes_mpeg12_video_process_routines(){}
	~pes_mpeg12_video_process_routines(){}

	static bool CheckVideoSequenceHdr(unsigned char* buffer);
};

class pes_mpeg12_audio_process_routines
{
public:
	pes_mpeg12_audio_process_routines(){}
	~pes_mpeg12_audio_process_routines(){}

	static bool CheckAudioFrameStart(unsigned char* buffer);
};

/*! This helper class calculates 32bit CRC of a a given buffer
*/
class ts_crc_handler
{
protected:
	ts_crc_handler();
	~ts_crc_handler(){}
	boost::uint32_t m_CRC32Table[256];

public:
	static ts_crc_handler* GetCRCHandler();
	/*! This helper class calculates 32bit CRC of a a given buffer
		\param TSPacket - pointer to a buffer to calculate CRC for
		\param len - size of a buffer
		\retval CRC
	*/
	boost::uint32_t CalculateCRC(unsigned char *TSPacket, int len);
    void AddCRC(unsigned char* buffer, int len, unsigned char* add_ptr);

};

enum BDAC_PAYLOAD_PARSER_STATE
{
	BDAC_PPS_IDLE = 0,
	BDAC_PPS_WAITING_FIRST_SECTION_PACKET,
	BDAC_PPS_COLLECTING_ALL_SECTION_PACKETS,
	BDAC_PPS_READY
};

typedef struct
{
	int m_Length;
	unsigned char* m_PayloadData;
} TMBTSPayloadDesc;

/*! This helper class assembles sections or PES scattered across a number of transport stream packets
*/
class ts_payload_parser
{
public:
	struct ts_section_info
	{
		unsigned char* section;
		int length;
	};

	typedef std::vector<ts_payload_parser::ts_section_info> ts_section_list;

	ts_payload_parser();
	virtual ~ts_payload_parser();

	/*! This function initializes parser (and resets all internal data structures)
		\param pid - PID of the transport stream packets to assemble
		\retval 1 if success, 0 if failed
	*/
	bool Init(unsigned short pid);

	/*! This function resets assembly process (e.g. it can be started again for packets with the same PID)
	*/
	void Reset();

	/*! This function adds transport stream packet to a parser
		\param packet - pointer to transport stream packet
		\param length - length of a transport stream packet (188 bytes)
		\param found_sections - vector, receiving sections, found as a result of parsing
		\retval number of completed sections in found_sections array
	*/
	int AddPacket(const unsigned char* packet, int length, ts_payload_parser::ts_section_list& found_sections);

	/*! This function resets the contents of array with found sections and deletes them
		\param found_sections - vector with sections to delete
		\retval none
	*/
	void ResetFoundSections(ts_payload_parser::ts_section_list& found_sections);

protected:
	bool CheckIfPayloadComplete(int& section_size);
	virtual int GetPayloadLength(){return 0;}
	virtual int GetPayloadMaxLength(){return 0;}
	virtual int GetPayloadDataOffset(const void* /*buffer*/)
    {
        return 0;
    };
	virtual int GetPayloadFirstByteOffset(const void* /*buffer*/){return 0;}
	virtual bool CheckFoundData(void* /*buffer*/, int /*len*/){return false;}

	BDAC_PAYLOAD_PARSER_STATE m_State;
	TMBTSPayloadDesc m_Payload;
	unsigned short m_TSContinuityCounter;

	//Info about current TS
	unsigned short m_PID;
};

/*! This class assembles PES packets from multiple transport stream packets
*/
class ts_pes_payload_parser : public ts_payload_parser
{
public:
	ts_pes_payload_parser();
	virtual ~ts_pes_payload_parser();
protected:
	virtual int GetPayloadLength();
	virtual int GetPayloadMaxLength();
	virtual int GetPayloadDataOffset(const void* buffer);
	virtual int GetPayloadFirstByteOffset(const void* buffer);
	virtual bool CheckFoundData(void* buffer, int len);
};

/*! This class assembles sections from multiple transport stream packets
*/
class ts_section_payload_parser : public ts_payload_parser
{
public:
	ts_section_payload_parser();
	virtual ~ts_section_payload_parser();
protected:
	virtual int GetPayloadLength();
	virtual int GetPayloadMaxLength();
	virtual int GetPayloadDataOffset(const void* buffer);
	virtual int GetPayloadFirstByteOffset(const void* buffer);
	virtual bool CheckFoundData(void* buffer, int len);
};

/*! This class creates ETSI EN 300 468, Appendix A compliant descriptor from a unicode string
*/
class ts_decriptor_gen
{
public:
	ts_decriptor_gen(const wchar_t* desc_string);
	const unsigned char* GetDescriptor(){return m_Buffer;}
	unsigned char GetDescriptorLength(){return m_DescriptorLength;}

protected:
	static const int m_MaxBufSize = 4096;
	unsigned char m_Buffer[m_MaxBufSize];
	unsigned char m_DescriptorLength;

};

} //engine
} //dvblink

//////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_TS_INFO_H_
