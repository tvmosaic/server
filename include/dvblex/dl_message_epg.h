/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_types.h>
#include <dl_message.h>
#include <dl_epgevent.h>
#include <dl_channels.h>
#include <dl_epg_channels.h>

namespace dvblink { namespace messaging { namespace epg {

//
// get registered epg sources
//
struct get_epg_sources_response
{
    dvblex::epg_source_map_t epg_sources_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & epg_sources_;
        ar & result_;
    }
};

struct get_epg_sources_request : public message_send<get_epg_sources_request, get_epg_sources_response>
{
};

//
// register epg source
//
struct register_epg_source_response
{
    bool result_;
    bool periodic_updates_enabled_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
        ar & periodic_updates_enabled_;
    }
};

struct register_epg_source_request : public message_send<register_epg_source_request, register_epg_source_response>
{
    register_epg_source_request(){}
    register_epg_source_request(const dvblex::epg_source_t& epg_source) :
        epg_source_(epg_source)
        {}

        dvblex::epg_source_t epg_source_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & epg_source_;
    }
};

//
// unregister epg source
//
struct unregister_epg_source_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct unregister_epg_source_request : public message_send<unregister_epg_source_request, unregister_epg_source_response>
{
    unregister_epg_source_request(){}
    unregister_epg_source_request(const dvblink::epg_source_id_t& id) :
        id_(id)
        {}

    dvblink::epg_source_id_t id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & id_;
    }
};

//
// get epg channels from source
//
struct get_epg_channels_response
{
    dvblex::epg_source_channel_map_t channels_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & channels_;
        ar & result_;
    }
};

struct get_epg_channels_request : public message_send<get_epg_channels_request, get_epg_channels_response>
{
};

//
// get_channel_epg
//
struct get_channel_epg_response
{
    dvblink::engine::DLEPGEventList epg_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & epg_;
        ar & result_;
    }
};

struct get_channel_epg_request : public message_send<get_channel_epg_request, get_channel_epg_response>
{
    get_channel_epg_request(){}
    get_channel_epg_request(const dvblink::epg_channel_id_t& channel_id) :
        channel_id_(channel_id)
        {}

    dvblink::epg_channel_id_t channel_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & channel_id_;
    }
};

///////////////////////////////////////////////////////////////////////////////
//
//Notifies epg manager that epg on the epg source has been updated

struct epg_updated_request : public message_post<epg_updated_request>
{
    epg_updated_request() {}
        
    epg_source_id_t epg_source_id_;
    std::vector<epg_channel_id_t> updated_epg_channels_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & epg_source_id_;
        ar & updated_epg_channels_;
    } 
};

//
// get channel epg config
//
struct get_channel_epg_config_response
{
    dvblex::channel_to_epg_source_map_t epg_config_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & epg_config_;
        ar & result_;
    }
};

struct get_channel_epg_config_request : public message_send<get_channel_epg_config_request, get_channel_epg_config_response>
{
    get_channel_epg_config_request(){}

    dvblex::channel_id_list_t channels_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & channels_;
    }
};

//
// set channel epg config
//
struct set_channel_epg_config_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct set_channel_epg_config_request : public message_send<set_channel_epg_config_request, set_channel_epg_config_response>
{
    set_channel_epg_config_request(){}

    dvblex::channel_to_epg_source_map_t epg_config_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & epg_config_;
    }
};

//
// match dvblex channels to epg channels
//
struct match_epg_channels_response
{
    dvblex::epg_channel_match_map_t match_info_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & match_info_;
        ar & result_;
    }
};

struct match_epg_channels_request : public message_send<match_epg_channels_request, match_epg_channels_response>
{
    match_epg_channels_request(const dvblink::epg_source_id_t& epg_source_id, const dvblex::channel_id_list_t& channels) :
        epg_source_id_(epg_source_id), channels_(channels)
        {}

    match_epg_channels_request()
    {}

    dvblink::epg_source_id_t epg_source_id_;
    dvblex::channel_id_list_t channels_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & epg_source_id_;
        ar & channels_;
    }
};

///////////////////////////////////////////////////////////////////////////////
//
//Enabled or disables periodic epg updatesd

struct enable_periodic_epg_updates_request : public message_post<enable_periodic_epg_updates_request>
{
    enable_periodic_epg_updates_request()
    {}

    bool enable_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & enable_;
    }
};


///////////////////////////////////////////////////////////////////////////////
//
//Requests all epg sources to re-scan/re-fetch epg information

struct force_epg_update_request : public message_post<force_epg_update_request>
{
};

} //channels
} //messaging
} //dvblink
