/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_BASE_TYPES_H_
#define __DVBLINK_DL_BASE_TYPES_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <string>

namespace dvblink {

template <typename TYPE, int n> class base_type_t
{
public:
    typedef TYPE type_t;

public:
    base_type_t() {}
    base_type_t(const base_type_t& bid) : id_(bid.id_) {}

    base_type_t(const type_t& id) : id_(id) {}

    base_type_t& operator = (const base_type_t& right) {id_ = right.id_; return *this;}
    void operator = (const type_t& id) {id_ = id;}

    bool operator != (const base_type_t& right) const {return id_ != right.id_;}
    bool operator == (const base_type_t& right) const {return id_ == right.id_;}
    bool operator < (const base_type_t& right) const {return id_ < right.id_;}
    bool operator > (const base_type_t& right) const {return id_ > right.id_;}

    void set(const type_t& id) {id_ = id;}
    const type_t& get() const {return id_;}
    type_t* object() {return &id_;}

    template <class Archive>
    void serialize(Archive& ar, unsigned int /*file_version*/)
    {
        ar & id_;
    }

protected:
    type_t id_;
};

} //dvblink

namespace std {
template <class Char, class Traits, typename TYPE, int n>
inline std::basic_ostream<Char, Traits>& operator << (std::basic_ostream<Char, Traits>& os, const dvblink::base_type_t<TYPE, n>& p)
{
    return os << p.get();
}
} //std

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_BASE_TYPES_H_
