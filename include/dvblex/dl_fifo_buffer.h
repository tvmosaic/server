/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DL_FIFO_BUFFER_H__
#define __DL_FIFO_BUFFER_H__

#include <deque>
#include <vector>
#include <boost/thread.hpp>
#include <boost/cstdint.hpp>
#include <boost/shared_ptr.hpp>
#include <dl_event.h>

namespace dvblink {
namespace engine {

class fifo_buffer
{
public:
    explicit fifo_buffer(size_t max_size = 0,
        size_t block_size = default_block_size);
    ~fifo_buffer();

    bool read(void* data, size_t size);
    bool write(const void* data, size_t size);
    
    bool wait_for_readable(dvblink::timeout_t to = dvblink::infinite_timeout);
    bool wait_for_writable(dvblink::timeout_t to = dvblink::infinite_timeout);

    size_t size() const;
    bool empty() const;

    void clear();
    void swap(fifo_buffer& right);

private:
    typedef boost::uint8_t byte_t;
    typedef std::vector<byte_t> block_t;
    typedef boost::shared_ptr<block_t> block_ptr;
    typedef std::deque<block_ptr> queue_t;
    static const size_t default_block_size = (64 * 1024);

private:
    block_ptr _create_block();
    void _remove_block();
    bool _validate();

private:
    size_t max_size_;    
    size_t block_size_;
    size_t size_;
    size_t read_offs_;
    
    mutable boost::mutex lock_;
    dvblink::event read_event_; 
    dvblink::event write_event_; 

    queue_t queue_;
    queue_t pool_;    
};

typedef boost::shared_ptr<fifo_buffer> fifobuf_ptr;

} // namespace engine
} // namespace dvblink

#endif  // __DL_FIFO_BUFFER_H__

// $Id: dl_fifo_buffer.h 6337 2012-09-25 07:53:29Z mike $
