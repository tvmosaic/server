/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <boost/thread/shared_mutex.hpp>

#include <dl_ts.h>


namespace dvblink {  namespace engine {

class ts_section_payload_parser; //should be removed once dltsinfo.h becomes obsolete

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	class CTSPatInfo
	{
	public:
		CTSPatInfo();
		~CTSPatInfo();

		void Init(unsigned char* pat_section, int section_len);
		void Clear();

		bool GetTSID(unsigned short& TSID);
		bool GetServices(std::vector<SPatProg>& services);
	protected:
		std::basic_string<unsigned char> m_pat;
	};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	class CTSPmtInfo
	{
	public:
		CTSPmtInfo();

		void Init(unsigned char* pmt_section, int section_len);
		void Clear();

		bool GetSectionServiceID(unsigned short& ServiceId);
		bool GetExternalDescriptors(SDataChunk& ext_descriptors);
		bool GetStreams(std::vector<SESInfo>& stream_info);

        const unsigned char* GetPMTPointer(size_t& len);

		static bool ClearESInfoVector(std::vector<SESInfo>& stream_info);
		static bool ClearESInfoStruct(SESInfo& es_info);
	protected:
		std::basic_string<unsigned char> m_pmt;
	};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	class CTSPmtParser
	{
	public:
		CTSPmtParser();
		~CTSPmtParser();

		void Init(int service_id = -1);
		void Term();

		void ProcessStream(unsigned char* buffer, int len);

		CTSPmtInfo* GetPmtInfo();
		CTSPatInfo* GetPatInfo();
		unsigned short GetPMTPid(){return m_pmtPid;};

	protected:
		enum EState
		{
			ES_STOPPED,
			ES_PAT,
			ES_PMT,
			ES_READY
		};

		void ProcessPMTSection(unsigned char* pmt_buffer, int pmt_len);
		void ProcessPATSection(unsigned char* pat_buffer, int pat_len);

		int m_serviceId;
		unsigned short m_pmtPid;
		CTSPmtInfo m_pmtInfo;
		CTSPatInfo m_patInfo;
		EState m_state;
		ts_section_payload_parser* m_sectionParser;
	    boost::shared_mutex m_lock;
	};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//Callback function for sending the TS packets
	typedef void (*LP_PACKGEN_TS_SEND_FUNC)(const unsigned char* buf, int len, void* param);

	/*! This class generates transport stream packets and sections. It also provides functionality
		to split a single section or PES buffer into separate transport stream packets
	*/
	class CTSPacketGenerator
	{
	public:
		CTSPacketGenerator();
		~CTSPacketGenerator();

	/*! This function creates PMT section
		\param section_length - (output param) length of newly created PMT section
		\param pmt - pointer to pmt header
		\param ext_decriptors - external descriptor loop
		\param streams - information about elementary streams
		\retval pointer to newly created PMT section. This pointer points to a member of a class and should not be deleted by caller!
	*/
	unsigned char* CreatePMTSection(int& section_length, SPmt* pmt, SDataChunk* ext_decriptors, std::vector<SESInfo>& streams);

	//"Split and send" functions
	/*! This function splits provided section buffer into transport stream packets and sends them one by one to provided "send function"
		\param section_buf - pointer to section buffer
		\param section_len - length of section buffer
		\param cont_counter - continuity counter (it is incremented inside the function)
		\param pid - pid of transport stream packets holding this section
		\param send_func - callback function, used to send transport stream packets
		\param param - user parameter passed to callback function
	*/
	void SplitAndSendSectionBuffer(unsigned char* section_buf, int section_len, unsigned char* cont_counter, unsigned short pid,
		LP_PACKGEN_TS_SEND_FUNC send_func, void* param);

	protected:
		//This buffer is used to create new TS packets
		unsigned char m_TSPacket[TS_PACKET_SIZE];
		//This buffer is used in split-and-send operations
		unsigned char m_TSSendPacket[TS_PACKET_SIZE];
		//Buffer for the entire section (note that section can be more than 1 TS packet)
		unsigned char m_SectionBuffer[4096];
	};


}; //engine
}; //dvblink

