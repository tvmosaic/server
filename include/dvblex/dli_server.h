/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dli_base.h>
#include <dli_message_driven.h>

namespace dvblink {

#pragma pack(push, 1)

// {5FE24AC3-427E-46f0-857B-D6C07D2A17EE}
const i_guid server_interface = 
{ 0x5fe24ac3, 0x427e, 0x46f0, { 0x85, 0x7b, 0xd6, 0xc0, 0x7d, 0x2a, 0x17, 0xee } };

struct i_server : public i_base_object
{
    virtual void __stdcall register_queue(boost::shared_ptr<messaging::i_client_message_queue> queue) = 0;
    virtual void __stdcall unregister_queue(const message_queue_id_t& queue) = 0;

    virtual i_result __stdcall query_object_interface(
        const base_id_t& requestor_id, const base_id_t& parent_object_id, const i_guid& queried_iid, i_base_object_t& queried_obj) = 0;

    template<class T>
    i_result query_object_interface(const base_id_t& requestor_id, const base_id_t& parent_object_id, const i_guid& queried_iid, boost::shared_ptr<T>& queried_obj)
    {
        i_base_object_t base;
        i_result res = query_object_interface(requestor_id, parent_object_id, queried_iid, base);
        if (res == i_success)
        {
            //
            // Workaround for gcc 4.3.2 (iomega's toolchain)
            //
            //queried_obj = boost::dynamic_pointer_cast<typename boost::shared_ptr<T>::element_type, i_base_object>(base);
            //queried_obj = boost::shared_ptr<T>(base, boost::detail::static_cast_tag());
            //static_cast_tag is not a member of modern boost anymore
            queried_obj = boost::static_pointer_cast<T>(base);
        }
        return res;
    }

    virtual void __stdcall register_object(const i_base_object_t& object) = 0;
    virtual void __stdcall unregister_object(const base_id_t& object_id) = 0;
};

typedef boost::shared_ptr<i_server> i_server_t;

#pragma pack(pop)

} //dvblink
