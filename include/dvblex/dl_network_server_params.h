/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <sstream>
#include <boost/algorithm/string.hpp> 
#include <dl_types.h>
#include <dl_pugixml_helper.h>

namespace dvblex { 

const std::string get_net_server_params_cmd              = "get_net_server_params";
const std::string set_net_server_params_cmd              = "set_net_server_params";

struct network_server_params
{
    network_server_params() :
        forward_ports_(false),
        use_authentication_(false)
    {}

    bool forward_ports_;
    std::vector<boost::uint16_t> ports_;
    bool use_authentication_;
    std::string user_;
    std::string password_;
};

const std::string network_server_params_root_node              = "network_server_params";
const std::string network_server_params_forward_node           = "forward_ports";
const std::string network_server_params_ports_node             = "ports";
const std::string network_server_params_port_node              = "port";
const std::string network_server_params_use_auth_node          = "use_auth";
const std::string network_server_params_user_node              = "user";
const std::string network_server_params_password_node          = "password";

inline pugi::xml_node& operator >> (pugi::xml_node& node, network_server_params& nss)
{
    if (NULL != node)
    {
        std::string str;

        if (dvblink::pugixml_helpers::get_node_value(node, network_server_params_forward_node, str) &&
            boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true))
            nss.forward_ports_ = true;

        pugi::xml_node ports_node = node.child(network_server_params_ports_node.c_str());

        if (ports_node != NULL)
        {
            pugi::xml_node port_node = ports_node.first_child();
            while (port_node != NULL)
            {
                if (boost::iequals(port_node.name(), network_server_params_port_node))
                {
                    boost::uint16_t p = atoi(port_node.child_value());
                    if (p != 0)
                        nss.ports_.push_back(p);
                }
                port_node = port_node.next_sibling();
            }
        }

        if (dvblink::pugixml_helpers::get_node_value(node, network_server_params_use_auth_node, str) &&
            boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true))
            nss.use_authentication_ = true;

        dvblink::pugixml_helpers::get_node_value(node, network_server_params_user_node, nss.user_);
        dvblink::pugixml_helpers::get_node_value(node, network_server_params_password_node, nss.password_);

    }
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const network_server_params& nss)
{
    pugi::xml_node node = doc.append_child(network_server_params_root_node.c_str());
    if (node != NULL)
    {
        if (nss.forward_ports_)
            dvblink::pugixml_helpers::new_child(node, network_server_params_forward_node, dvblink::pugixml_helpers::xmlnode_value_true);

        pugi::xml_node ports_node = dvblink::pugixml_helpers::new_child(node, network_server_params_ports_node);
	    if (ports_node != NULL)
	    {
            for (size_t j=0; j<nss.ports_.size(); j++)
            {
                std::stringstream buf;
                buf << nss.ports_[j];

                dvblink::pugixml_helpers::new_child(ports_node, network_server_params_port_node, buf.str());
            }
	    }

        if (nss.use_authentication_)
            dvblink::pugixml_helpers::new_child(node, network_server_params_use_auth_node, dvblink::pugixml_helpers::xmlnode_value_true);

        dvblink::pugixml_helpers::new_child(node, network_server_params_user_node, nss.user_);
        dvblink::pugixml_helpers::new_child(node, network_server_params_password_node, nss.password_);
    }
    return doc;
}

} //dvblex
