/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/shared_ptr.hpp>
#include <dl_types.h>
#include <dli_base.h>

namespace dvblink { namespace messaging {

#pragma pack(push, 1)

// {C368F8A4-EFBB-4b0c-A8CF-8D78B02F2EBE}
const i_guid play_source_control_interface = 
{ 0xc368f8a4, 0xefbb, 0x4b0c, { 0xa8, 0xcf, 0x8d, 0x78, 0xb0, 0x2f, 0x2e, 0xbe } };

struct i_play_source_control : public i_base_object
{
    virtual bool __stdcall read_data(const object_handle_safe_t& handle, unsigned char* buffer, boost::uint32_t& buffer_length) = 0;
};

typedef boost::shared_ptr<i_play_source_control> i_play_source_control_t;

#pragma pack(pop)

} //auxes
} //dvblink
