/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread/shared_mutex.hpp>
#include <dl_event.h>

namespace dvblink {
namespace engine {

class hdd_ring_buffer
{
public:
    hdd_ring_buffer();
    ~hdd_ring_buffer();
    
    bool init(const dvblink::filesystem_path_t& temp_file, boost::uint64_t max_size);
    void term();
    
    bool write_stream(const unsigned char* buffer, size_t size);
    bool read_stream(unsigned char* buffer, boost::uint64_t& size, size_t timeout_ms = 100);
    
    void get_stats(boost::uint64_t& position, boost::uint64_t& length, time_t& duration, time_t& position_sec);
    boost::uint64_t get_max_buffer_size();

    bool read_seek_bytes(boost::int64_t pos, int whence, boost::uint64_t& cur_pos);
    bool read_seek_time(time_t pos, int whence, boost::uint64_t& cur_pos);
    
protected:
    boost::uint64_t position_to_buffer(boost::uint64_t pos);

    boost::uint64_t max_size_;
    
    dvblink::filesystem_path_t temp_file_;
    boost::uint64_t read_pos_;
    boost::uint64_t write_pos_;
    dvblink::event event_;
    boost::shared_mutex lock_;
    
    time_t start_time_;
    time_t buffer_duration_;
    
    FILE* f_write_;
};

} // engine
} // dvblink
