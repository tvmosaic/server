/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_FILE_PROCEDURES_H_
#define __DVBLINK_DL_FILE_PROCEDURES_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <boost/filesystem.hpp>
#include <dl_filesystem_path.h>

namespace dvblink { namespace engine { namespace filesystem {

typedef std::vector<boost::filesystem::path> ff_t;
//bool find_files(const boost::filesystem::path& dir, const wchar_t* mask, ff_t& result);
bool find_files(const boost::filesystem::path& dir_path, ff_t& result, const std::wstring& file_ext = L"");
bool create_directory(const boost::filesystem::path& dir);
bool copy_directory_recursive(const boost::filesystem::path& src, const boost::filesystem::path& dest);
bool delete_directory_recursive(const boost::filesystem::path& src);
bool delete_directory_contents(const boost::filesystem::path& dir_path);

boost::int64_t get_file_size(const std::wstring& fname);
boost::int64_t get_file_size(const std::string& fname);

FILE* universal_open_file(const dvblink::filesystem_path_t& fname, const char* access_mode);
int universal_file_seek(FILE* f, boost::int64_t offset, int origin);

dvblink::filesystem_path_t make_preferred_dvblink_filepath(const dvblink::filesystem_path_t& fname);

char* read_file_into_memory(const dvblink::filesystem_path_t& fname, boost::int64_t& size);

struct directory_tree
{
    dvblink::filesystem_path_t parent_path_;
    std::vector<dvblink::filesystem_path_t> tree_;

    directory_tree() {}
    directory_tree(const dvblink::filesystem_path_t& p) :
        parent_path_(p) {}

    directory_tree(const std::string& p) :
        parent_path_(p) {}

    directory_tree(const std::wstring& p) :
        parent_path_(p) {}

    directory_tree(const boost::filesystem::path& p) :
        parent_path_(p) {}

    void add_dir(const dvblink::filesystem_path_t& dir)
    {
        tree_.push_back(dir);
    }

    void trace()
    {
        //std::cout << "parent_dir:        " << parent_path_ << std::endl;
        //std::cout << " normalized:       " << parent_path_.normalize().string() << std::endl;
        //std::cout << " root path:        " << parent_path_.root_path() << std::endl;
        //std::cout << " root name:        " << parent_path_.root_name() << std::endl;
        //std::cout << " root directory:   " << parent_path_.root_directory() << std::endl;
        //std::cout << " parent path:      " << parent_path_.parent_path() << std::endl;

        //for (size_t i = 0; i < tree_.size(); ++i)
        //{
        //    std::cout << "    " << tree_[i] << std::endl;
        //}
        //std::cout << std::endl;
    }
};

bool get_directory_tree(directory_tree& dt);

} //filesystem
} //engine
} //dvblink

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_FILE_PROCEDURES_H_
