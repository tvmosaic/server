/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_TIMER_PROCEDURE_H_
#define __DVBLINK_DL_TIMER_PROCEDURE_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <boost/asio.hpp>
#include <boost/thread.hpp>

namespace dvblink { namespace engine {

class timer_procedure_cluster
{
    public:
        timer_procedure_cluster(size_t threads_num) :
            threads_num_(threads_num),
            work_(io_service_)
        {
            for (size_t i=0; i<threads_num_; i++)
                threads_.create_thread( boost::bind( &boost::asio::io_service::run, &io_service_ ) );
            io_service_.poll();
        }
        
        ~timer_procedure_cluster()
        {
            io_service_.stop();
            
            try
            {
                threads_.join_all();
            } catch ( ... ) {}
        }
    
        boost::asio::io_service& get_io_service() {return io_service_; }
        
protected:
    boost::asio::io_service io_service_;
    size_t threads_num_;
    boost::asio::io_service::work work_;
    boost::thread_group threads_;
};

template <typename T>
class timer_procedure
{
    typedef void (T::*fn)(const boost::system::error_code& e);

    fn fn_;
    T& t_;
    long timeout_;
    boost::asio::io_service io_;
    boost::asio::deadline_timer timer_;
    bool cyclic_timer_;
    boost::thread* check_thread_;

public:
    timer_procedure(fn f, T& t, long timeout_millisecons, bool cyclic_timer = true) :
        fn_(f),
        t_(t),
        timeout_(timeout_millisecons),
        io_(),
        timer_(io_, boost::posix_time::milliseconds(timeout_)),
        cyclic_timer_(cyclic_timer),
        check_thread_(NULL)
    {
        timer_.async_wait(boost::bind(&timer_procedure::procedure, this, _1));
        check_thread_ = new boost::thread(boost::bind(&boost::asio::io_service::run, &io_));
        io_.poll();
    }

    timer_procedure(fn f, T& t, long timeout_millisecons, boost::asio::io_service& io_service, bool cyclic_timer = true) :
        fn_(f),
        t_(t),
        timeout_(timeout_millisecons),
        io_(),
        timer_(io_service, boost::posix_time::milliseconds(timeout_)),
        cyclic_timer_(cyclic_timer),
        check_thread_(NULL)
    {
        io_.stop(); //this one is not used
        
        timer_.async_wait(boost::bind(&timer_procedure::procedure, this, _1));
    }

    ~timer_procedure()
    {
        timer_.cancel();
        
        if (check_thread_ != NULL)
        {
            io_.stop();

            check_thread_->join();
            delete check_thread_;
        }
    }

    void restart(long new_timeout_milli = 0)
    {
        if (new_timeout_milli)
        {
            timeout_ = new_timeout_milli;
        }

        timer_.expires_from_now(boost::posix_time::milliseconds(timeout_));
        timer_.async_wait(boost::bind(&timer_procedure::procedure, this, _1));
    }

    void procedure(const boost::system::error_code& e)
    {
        if (e != boost::asio::error::operation_aborted)
        {
            (t_.*fn_)(e);

            if (cyclic_timer_)
            {
                timer_.expires_from_now(boost::posix_time::milliseconds(timeout_));
                timer_.async_wait(boost::bind(&timer_procedure::procedure, this, _1));
            }
        }
    }
};

} //engine
} //dvblink

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_TIMER_PROCEDURE_H_
