/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread.hpp>
#include <map>
#include <dl_types.h>
#include <dl_filesystem_path.h>
#include <dl_os_version.h>

namespace dvblink { namespace engine {

struct wakeup_timer
{
    wakeup_timer()
        : wakeup_time(0), daily(false)
    {}

    wakeup_timer(time_t wt)
        : wakeup_time(wt), daily(false)
    {}

    time_t wakeup_time;
    bool daily;
};

class no_standby_thread;
class wakeup_timer_man;

class power_manager
{
public:
    power_manager();
    virtual ~power_manager();

    //wakeup timers
    bool schedule_wakeup_timer(const client_id_t& client_id, wakeup_timer& timer, const filesystem_path_t& app_path = L"02ec03f1-26e3-4b10-a9f0-fd29b86b270b.exe", const std::wstring& app_parameters = L"");
    bool get_wakeup_timer(const client_id_t& client_id, wakeup_timer& timer);
    bool remove_wakeup_timer(const client_id_t& client_id);

    //power state
    bool set_idle_power_state(const client_id_t& client_id);
    bool set_no_standby_power_state(const client_id_t& client_id);

    enum e_power_state {eps_idle, eps_no_standby};
    void get_power_state(e_power_state& state);
    
protected:
    bool set_power_state(e_power_state state);

private:
    typedef boost::shared_ptr<no_standby_thread> no_standby_thread_t;
    no_standby_thread_t no_standby_thread_;
    std::map<client_id_t, client_id_t> standby_client_map_;

    os_version version_;
    boost::mutex lock_;

    wakeup_timer_man* man_;
};

} //engine
} //dvblink
