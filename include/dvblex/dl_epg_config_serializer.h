/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <dl_types.h>
#include <dl_pugixml_helper.h>
#include <dl_epg_channels.h>

namespace dvblex { 

const std::string epg_config_sources_response_root              = "epg_sources";
const std::string epg_config_source_node                        = "source";
const std::string epg_config_source_id_node                     = "id";
const std::string epg_config_source_name_node                   = "name";
const std::string epg_config_source_settings_node               = "settings";
const std::string epg_config_source_is_default_node               = "is_default";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const dvblex::epg_source_map_t& epg_sources)
{
    pugi::xml_node ges_node = doc.append_child(epg_config_sources_response_root.c_str());
    if (ges_node != NULL)
    {
        dvblex::epg_source_map_t::const_iterator it = epg_sources.begin();
        while (it != epg_sources.end())
        {
            pugi::xml_node src_node = dvblink::pugixml_helpers::new_child(ges_node, epg_config_source_node);
            if (src_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(src_node, epg_config_source_id_node, it->second.id_.to_string());
                dvblink::pugixml_helpers::add_node_attribute(src_node, epg_config_source_name_node, it->second.name_.get());
                if (it->second.has_settings_.get())
                    dvblink::pugixml_helpers::add_node_attribute(src_node, epg_config_source_settings_node, dvblink::pugixml_helpers::xmlnode_value_true);
                if (it->second.is_default_.get())
                    dvblink::pugixml_helpers::add_node_attribute(src_node, epg_config_source_is_default_node, dvblink::pugixml_helpers::xmlnode_value_true);
            }
            ++it;
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, dvblex::epg_source_map_t& epg_sources)
{
    if (NULL != node)
    {
        std::string str;

        pugi::xml_node epg_src_node = node.first_child();
        while (epg_src_node != NULL)
        {
            dvblex::epg_source_t es;
            if (dvblink::pugixml_helpers::get_node_attribute(epg_src_node, epg_config_source_id_node, str))
                es.id_ = str;

            if (dvblink::pugixml_helpers::get_node_attribute(epg_src_node, epg_config_source_name_node, str))
                es.name_ = str;

            es.has_settings_ = dvblink::pugixml_helpers::get_node_attribute(epg_src_node, epg_config_source_settings_node, str) &&
                boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);

            es.is_default_ = dvblink::pugixml_helpers::get_node_attribute(epg_src_node, epg_config_source_is_default_node, str) &&
                boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
				
            epg_sources[es.id_] = es;

            epg_src_node = epg_src_node.next_sibling();
        }
    }

    return node;
}


////////////////////////////////////////////////////////////////////

struct get_epg_channels_request_t
{
    dvblink::epg_source_id_t epg_source_id_;
};

const std::string epg_config_epg_source_id_node                   = "epg_source_id";
const std::string epg_config_channels_response_root             = "epg_channels";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_epg_channels_request_t& gecr)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, epg_config_epg_source_id_node, str))
            gecr.epg_source_id_ = str;
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const dvblex::get_epg_channels_request_t& gecr)
{
    pugi::xml_node ges_node = doc.append_child(epg_config_epg_source_id_node.c_str());
    if (ges_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(ges_node, epg_config_epg_source_id_node, gecr.epg_source_id_.to_string());
    }

    return doc;
}


////////////////////////////////////////////////////////////////////

const std::string epg_config_epg_channel_node                   = "epg_channel";
const std::string epg_config_channel_id_node                    = "id";
const std::string epg_config_channel_name_node                  = "name";
const std::string epg_config_channel_number_node                = "num";
const std::string epg_config_channel_subnumber_node             = "subnum";
const std::string epg_config_channel_logo_node                  = "logo";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const dvblex::epg_source_channel_map_t& channels)
{
    std::stringstream buf;

    pugi::xml_node gecr_node = doc.append_child(epg_config_channels_response_root.c_str());
    if (gecr_node != NULL)
    {
        dvblex::epg_source_channel_map_t::const_iterator it = channels.begin();
        while (it != channels.end())
        {
            pugi::xml_node channel_node = dvblink::pugixml_helpers::new_child(gecr_node, epg_config_epg_channel_node);
            if (channel_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(channel_node, epg_config_channel_id_node, it->second.id_.get());
                dvblink::pugixml_helpers::add_node_attribute(channel_node, epg_config_channel_name_node, it->second.name_.get());

                if (it->second.number_.get() > 0)
                {
                    buf.clear(); buf.str("");
                    buf << it->second.number_.get();
                    dvblink::pugixml_helpers::add_node_attribute(channel_node, epg_config_channel_number_node, buf.str());
                }

                if (it->second.sub_number_.get() > 0)
                {
                    buf.clear(); buf.str("");
                    buf << it->second.sub_number_.get();

                    dvblink::pugixml_helpers::add_node_attribute(channel_node, epg_config_channel_subnumber_node, buf.str());
                }

                if (!it->second.logo_.get().empty())
                    dvblink::pugixml_helpers::new_child(channel_node, epg_config_channel_logo_node, it->second.logo_.get());
            }
            ++it;
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, dvblex::epg_source_channel_map_t& channels)
{
    if (NULL != node)
    {
        std::string str;

        pugi::xml_node epg_ch_node = node.first_child();
        while (epg_ch_node != NULL)
        {
            dvblex::epg_source_channel_t esc;

            if (dvblink::pugixml_helpers::get_node_attribute(epg_ch_node, epg_config_channel_id_node, str))
                esc.id_ = str;

            if (dvblink::pugixml_helpers::get_node_attribute(epg_ch_node, epg_config_channel_name_node, str))
                esc.name_ = str;

            boost::int32_t i32v;
            if (dvblink::pugixml_helpers::get_node_value(epg_ch_node, epg_config_channel_number_node, str))
            {
                dvblink::engine::string_conv::apply(str.c_str(), i32v, esc.number_.get());
                esc.number_ = i32v;
            }

            if (dvblink::pugixml_helpers::get_node_value(epg_ch_node, epg_config_channel_subnumber_node, str))
            {
                dvblink::engine::string_conv::apply(str.c_str(), i32v, esc.sub_number_.get());
                esc.sub_number_ = i32v;
            }

            if (dvblink::pugixml_helpers::get_node_attribute(epg_ch_node, epg_config_channel_logo_node, str))
                esc.logo_ = str;

            channels[esc.id_] = esc;

            epg_ch_node = epg_ch_node.next_sibling();
        }
    }

    return node;
}

////////////////////////////////////////////////////////////////////

const std::string epg_config_epg_channels_config_response_root     = "epg_channels_config";
const std::string epg_config_channel_node                          = "channel";

struct get_epg_channel_config_request_t
{
    dvblex::channel_id_list_t channels_;
};

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_epg_channel_config_request_t& eccr)
{
    if (NULL != node)
    {
        pugi::xml_node channel_node = node.first_child();
        while (channel_node != NULL)
        {
            std::string channel_id;
            dvblink::pugixml_helpers::get_node_attribute(channel_node, epg_config_channel_id_node, channel_id);

            eccr.channels_.push_back(channel_id);

            channel_node = channel_node.next_sibling();
        }
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_epg_channel_config_request_t& eccr)
{
    pugi::xml_node egc_node = doc.append_child(epg_config_epg_channels_config_response_root.c_str());
    if (egc_node != NULL)
    {
        for (size_t i=0; i<eccr.channels_.size(); i++)
        {
            pugi::xml_node channel_node = dvblink::pugixml_helpers::new_child(egc_node, epg_config_channel_node);
            if (channel_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(channel_node, epg_config_channel_id_node, eccr.channels_[i].get());
            }
        }
    }

    return doc;
}

////////////////////////////////////////////////////////////////////

const std::string epg_config_epg_channel_id_node                   = "epg_channel_id";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const dvblex::channel_to_epg_source_map_t& epg_channels_config)
{
    pugi::xml_node egc_node = doc.append_child(epg_config_epg_channels_config_response_root.c_str());
    if (egc_node != NULL)
    {
        dvblex::channel_to_epg_source_map_t::const_iterator it = epg_channels_config.begin();
        while (it != epg_channels_config.end())
        {
            pugi::xml_node channel_node = dvblink::pugixml_helpers::new_child(egc_node, epg_config_channel_node);
            if (channel_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(channel_node, epg_config_channel_id_node, it->first.get());
                dvblink::pugixml_helpers::new_child(channel_node, epg_config_epg_channel_id_node, it->second.epg_channel_id_.get());
                dvblink::pugixml_helpers::new_child(channel_node, epg_config_epg_source_id_node, it->second.epg_source_id_.get());
            }
            ++it;
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, dvblex::channel_to_epg_source_map_t& epg_channels_config)
{
    if (NULL != node)
    {
        std::string str;

        pugi::xml_node channel_node = node.first_child();
        while (channel_node != NULL)
        {
            dvblink::channel_id_t channel_id;
            if (dvblink::pugixml_helpers::get_node_attribute(channel_node, epg_config_channel_id_node, str))
                channel_id = str;

            epg_source_channel_id_t esc;

            if (dvblink::pugixml_helpers::get_node_value(channel_node, epg_config_epg_channel_id_node, str))
                esc.epg_channel_id_ = str;

            if (dvblink::pugixml_helpers::get_node_value(channel_node, epg_config_epg_source_id_node, str))
                esc.epg_source_id_ = str;

            epg_channels_config[channel_id] = esc;

            channel_node = channel_node.next_sibling();
        }
    }

    return node;
}

////////////////////////////////////////////////////////////////////

struct match_epg_channels_request_t
{
    dvblink::epg_source_id_t epg_source_id_;
    dvblex::channel_id_list_t channels_;
};

const std::string epg_config_channels_node                       = "channels";
const std::string match_epg_channels_response_root     = "match_info";

inline pugi::xml_node& operator>> (pugi::xml_node& node, match_epg_channels_request_t& mecr)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, epg_config_epg_source_id_node, str))
            mecr.epg_source_id_ = str;

        pugi::xml_node channels_node = node.child(epg_config_channels_node.c_str());
        if (channels_node != NULL)
        {
            pugi::xml_node channel_node = channels_node.first_child();
            while (channel_node != NULL)
            {
                std::string channel_id;
                dvblink::pugixml_helpers::get_node_attribute(channel_node, epg_config_channel_id_node, channel_id);

                mecr.channels_.push_back(channel_id);

                channel_node = channel_node.next_sibling();
            }
        }
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const match_epg_channels_request_t& mecr)
{
    pugi::xml_node node = doc.append_child(match_epg_channels_response_root.c_str());
    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, epg_config_epg_source_id_node, mecr.epg_source_id_.get());

        pugi::xml_node channels_node = dvblink::pugixml_helpers::new_child(node, epg_config_channels_node);
        if (channels_node != NULL)
        {
            for (size_t i=0; i<mecr.channels_.size(); i++)
            {
                pugi::xml_node channel_node = dvblink::pugixml_helpers::new_child(channels_node, epg_config_channel_node);
                if (channel_node != NULL)
                    dvblink::pugixml_helpers::add_node_attribute(channel_node, epg_config_channel_id_node, mecr.channels_[i].get());
            }
        }
    }

    return doc;
}

////////////////////////////////////////////////////////////////////

const std::string epg_config_exact_match_node       = "exact";
const std::string epg_config_partial_match_node       = "partial";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const dvblex::epg_channel_match_map_t& match_info)
{
    pugi::xml_node mecr_node = doc.append_child(match_epg_channels_response_root.c_str());
    if (mecr_node != NULL)
    {
        epg_channel_match_map_t::const_iterator it = match_info.begin();
        while (it != match_info.end())
        {
            pugi::xml_node channel_node = dvblink::pugixml_helpers::new_child(mecr_node, epg_config_channel_node);
            if (channel_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(channel_node, epg_config_channel_id_node, it->first.get());

                pugi::xml_node exact_node = dvblink::pugixml_helpers::new_child(channel_node, epg_config_exact_match_node);
                if (exact_node != NULL)
                {
                    for (size_t i=0; i<it->second.exact_match_.size(); i++)
                    {
                        pugi::xml_node epg_channel_node = dvblink::pugixml_helpers::new_child(exact_node, epg_config_epg_channel_node);
                        if (epg_channel_node != NULL)
                        {
                            dvblink::pugixml_helpers::add_node_attribute(epg_channel_node, epg_config_epg_channel_id_node, it->second.exact_match_[i].epg_channel_id_.get());
                            dvblink::pugixml_helpers::add_node_attribute(epg_channel_node, epg_config_epg_source_id_node, it->second.exact_match_[i].epg_source_id_.to_string());
                        }
                    }
                }

                pugi::xml_node partial_node = dvblink::pugixml_helpers::new_child(channel_node, epg_config_partial_match_node);
                if (partial_node != NULL)
                {
                    for (size_t i=0; i<it->second.partial_match_.size(); i++)
                    {
                        pugi::xml_node epg_channel_node = dvblink::pugixml_helpers::new_child(partial_node, epg_config_epg_channel_node);
                        if (epg_channel_node != NULL)
                        {
                            dvblink::pugixml_helpers::add_node_attribute(epg_channel_node, epg_config_epg_channel_id_node, it->second.partial_match_[i].epg_channel_id_.get());
                            dvblink::pugixml_helpers::add_node_attribute(epg_channel_node, epg_config_epg_source_id_node, it->second.partial_match_[i].epg_source_id_.to_string());
                        }
                    }
                }
            }

            ++it;
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, dvblex::epg_channel_match_map_t& match_info)
{
    if (NULL != node)
    {
        std::string str;

        pugi::xml_node channel_node = node.first_child();
        while (channel_node != NULL)
        {
            dvblink::channel_id_t channel_id;
            if (dvblink::pugixml_helpers::get_node_attribute(channel_node, epg_config_channel_id_node, str))
                channel_id = str;

            epg_channel_match_t cm;
            pugi::xml_node exact_node = channel_node.child(epg_config_exact_match_node.c_str());
            if (exact_node != NULL)
            {
                pugi::xml_node epg_channel_node = exact_node.first_child();
                while (epg_channel_node != NULL)
                {
                    epg_source_channel_id_t esc;

                    if (dvblink::pugixml_helpers::get_node_attribute(epg_channel_node, epg_config_epg_channel_id_node, str))
                        esc.epg_channel_id_ = str;

                    if (dvblink::pugixml_helpers::get_node_attribute(epg_channel_node, epg_config_epg_source_id_node, str))
                        esc.epg_source_id_ = str;

                    cm.exact_match_.push_back(esc);

                    epg_channel_node = epg_channel_node.next_sibling();
                }
            }

            pugi::xml_node partial_node = channel_node.child(epg_config_partial_match_node.c_str());
            if (partial_node != NULL)
            {
                pugi::xml_node epg_channel_node = partial_node.first_child();
                while (epg_channel_node != NULL)
                {
                    epg_source_channel_id_t esc;

                    if (dvblink::pugixml_helpers::get_node_attribute(epg_channel_node, epg_config_epg_channel_id_node, str))
                        esc.epg_channel_id_ = str;

                    if (dvblink::pugixml_helpers::get_node_attribute(epg_channel_node, epg_config_epg_source_id_node, str))
                        esc.epg_source_id_ = str;

                    cm.partial_match_.push_back(esc);

                    epg_channel_node = epg_channel_node.next_sibling();
                }
            }

            match_info[channel_id] = cm;

            channel_node = channel_node.next_sibling();
        }
    }

    return node;
}

////////////////////////////////////////////////////////////////////

} //dvblex
