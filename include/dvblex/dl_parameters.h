/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>

#include <dl_types.h>

namespace dvblex { 

const std::string PARAM_VALUE_YES = "yes";
const std::string PARAM_VALUE_NO = "no";

inline const std::string get_yes_no_param_from_bool(bool b)
{
    return b ? PARAM_VALUE_YES : PARAM_VALUE_NO;
}

//selectable parameters (selected item from the list)

struct selectable_param_element_t
{
    selectable_param_element_t(){}
    selectable_param_element_t(const dvblink::parameter_value_t& value, const dvblink::parameter_name_t& name) :
        value_(value), name_(name)
    {}

    dvblink::parameter_value_t value_;
    dvblink::parameter_name_t name_;
};

typedef std::vector<selectable_param_element_t> selectable_param_list_t;

struct selectable_param_description_t
{
    selectable_param_description_t() :
        selected_param_(0)
    {}

    dvblink::parameter_key_t key_;
    dvblink::parameter_name_t name_;
    selectable_param_list_t parameters_list_;
    dvblink::parameter_idx_t selected_param_;
};

typedef std::map<dvblink::parameter_key_t, selectable_param_description_t> selectable_param_map_t; //key -> elements

// editable parameters

enum editable_param_format_e
{
    epf_string,
    epf_number
};

struct editable_param_element_t
{
    editable_param_element_t() :
        format_(epf_string)
    {}

    dvblink::parameter_key_t key_;
    dvblink::parameter_name_t name_;
    dvblink::parameter_value_t value_;
    editable_param_format_e format_;
};

// parameters container (e.g. several related user-editable parameters)

typedef std::vector<editable_param_element_t> editable_param_list_t;
typedef std::vector<selectable_param_description_t> selectable_param_desc_list_t;

struct parameters_container_t
{
    dvblink::container_id_t id_;
    dvblink::container_name_t name_;
    dvblink::container_desc_t desc_;
    editable_param_list_t editable_params_;
    selectable_param_desc_list_t selectable_params_;

    bool is_empty() const {return editable_params_.size() == 0 && selectable_params_.size() == 0;}
};

//selected parameters
typedef std::map<dvblink::parameter_key_t, dvblink::parameter_value_t> concise_param_map_t; //key -> value

// helpers

inline bool is_key_present(const dvblink::parameter_key_t& key, const concise_param_map_t& params)
{
    return params.find(key) != params.end();
}

inline dvblink::parameter_value_t get_value_for_param(const dvblink::parameter_key_t& key, const concise_param_map_t& params)
{
    concise_param_map_t::const_iterator it = params.find(key);
    if (it != params.end())
        return it->second;

    return "";
}

}
