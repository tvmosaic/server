/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include "dl_ts_info.h"
#include "dl_ts_proc.h"
#include <vector>
#include <map>

namespace dvblink {

class smooth_streamer_t
{
public:
    typedef void (*smooth_streamer_callback_p)(const unsigned char* buf, size_t len, void* user_param);

	smooth_streamer_t(smooth_streamer_callback_p callback, void* param, volatile bool* exit_flag);
	~smooth_streamer_t();

	void init();
	void term();

	void process_stream(const unsigned char* buf, size_t len);

private:
    engine::CTSPmtParser pmt_parser_;
    smooth_streamer_callback_p callback_;
    void* param_;
    volatile bool* exit_flag_;
    unsigned short pcr_pid_;
    __int64 pcr0_;
    boost::posix_time::ptime t0_;
};

} //dvblink

