/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <sstream>
#include <vector>
#include <map>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <dl_pugixml_helper.h>
#include <dl_channels.h>

namespace dvblex {

const std::string get_channel_logo_packages_cmd	      = "get_channel_logo_packages";
const std::string get_channel_logo_package_items_cmd  = "get_channel_logo_package_items";
const std::string set_channel_logo_cmd                = "set_channel_logo";
const std::string get_channel_logo_cmd	              = "get_channel_logo";
const std::string match_package_logo_cmd	          = "match_package_logo";

//package list
const std::string logo_package_list_root_node         = "packages";
const std::string logo_package_node                   = "package";
const std::string logo_package_id_node                = "id";

inline pugi::xml_node& operator>> (pugi::xml_node& node, logo_package_list_t& logo_package_list)
{
    if (NULL != node)
    {
        std::string str;
        pugi::xml_node package_node = node.first_child();
        while (package_node != NULL)
        {
            dvblink::logo_package_t package;

            if (dvblink::pugixml_helpers::get_node_attribute(package_node, logo_package_id_node, str))
                package = str;
            
            logo_package_list.push_back(package);

            package_node = package_node.next_sibling();
        }
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const logo_package_list_t& logo_package_list)
{
    pugi::xml_node node = doc.append_child(logo_package_list_root_node.c_str());

    if (node != NULL)
    {
        for (size_t i=0; i<logo_package_list.size(); i++)
        {
            pugi::xml_node package_node = dvblink::pugixml_helpers::new_child(node, logo_package_node);
            if (package_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(package_node, logo_package_id_node, logo_package_list[i].get());
            }
        }
    }

    return doc;
}

//get package items

struct get_logo_package_items_req
{
	dvblink::logo_package_t package_;
};

const std::string logo_package_items_root_node                = "logo_package_items";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_logo_package_items_req& req)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, logo_package_node, str))
            req.package_ = str;
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_logo_package_items_req& req)
{
    pugi::xml_node node = doc.append_child(logo_package_items_root_node.c_str());

    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, logo_package_node, req.package_.get());
    }

    return doc;
}

const std::string logo_package_logo_item_node              = "item";
const std::string logo_package_logo_id_node                = "id";
const std::string logo_package_logo_name_node              = "name";
const std::string logo_package_logo_url_node               = "url";

inline pugi::xml_node& operator>> (pugi::xml_node& node, channel_package_logo_item_list_t& item_list)
{
    if (NULL != node)
    {

        std::string str;
        pugi::xml_node item_node = node.first_child();
        while (item_node != NULL)
        {
            channel_package_logo_item_t item;

            if (dvblink::pugixml_helpers::get_node_attribute(item_node, logo_package_logo_id_node, str))
                item.id_ = str;

            if (dvblink::pugixml_helpers::get_node_value(item_node, logo_package_logo_name_node, str))
                item.name_ = str;

            if (dvblink::pugixml_helpers::get_node_value(item_node, logo_package_logo_url_node, str))
                item.url_ = str;

            item_list.push_back(item);

            item_node = item_node.next_sibling();
        }
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const channel_package_logo_item_list_t& item_list)
{
    pugi::xml_node node = doc.append_child(logo_package_items_root_node.c_str());

    if (node != NULL)
    {
        for (size_t i=0; i<item_list.size(); i++)
        {
            pugi::xml_node item_node = dvblink::pugixml_helpers::new_child(node, logo_package_logo_item_node);
            if (item_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(item_node, logo_package_logo_id_node, item_list[i].id_.get());
                dvblink::pugixml_helpers::new_child(item_node, logo_package_logo_name_node, item_list[i].name_.get());
                dvblink::pugixml_helpers::new_child(item_node, logo_package_logo_url_node, item_list[i].url_.get());
            }
        }
    }

    return doc;
}

// get channel logo descriptions
const std::string logo_desc_root_node                    = "channel_logo";

const std::string logo_desc_channel_node                 = "channel";
const std::string logo_desc_channel_id_node              = "id";
const std::string logo_desc_logo_id_node                 = "logo_id";
const std::string logo_desc_logo_url_node                = "logo_url";
const std::string logo_desc_default_logo_node            = "default_logo";

inline pugi::xml_node& operator>> (pugi::xml_node& node, channel_logo_desc_map_t& channel_logo_map)
{
    if (NULL != node)
    {

        std::string str;
        pugi::xml_node channel_node = node.first_child();
        while (channel_node != NULL)
        {
            channel_logo_t logo;
            dvblink::channel_id_t channel_id;

            if (dvblink::pugixml_helpers::get_node_attribute(channel_node, logo_desc_channel_id_node, str))
                channel_id = str;

            if (dvblink::pugixml_helpers::get_node_value(channel_node, logo_desc_logo_id_node, str))
                logo.id_ = str;

            if (dvblink::pugixml_helpers::get_node_value(channel_node, logo_desc_logo_url_node, str))
                logo.url_ = str;

            if (dvblink::pugixml_helpers::get_node_value(channel_node, logo_desc_default_logo_node, str))
                logo.default_logo_ = str;

            channel_logo_map[channel_id] = logo;

            channel_node = channel_node.next_sibling();
        }
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const channel_logo_desc_map_t& channel_logo_map)
{
    pugi::xml_node node = doc.append_child(logo_desc_root_node.c_str());

    if (node != NULL)
    {
        channel_logo_desc_map_t::const_iterator it = channel_logo_map.begin();
        while (it != channel_logo_map.end())
        {
            pugi::xml_node channel_node = dvblink::pugixml_helpers::new_child(node, logo_desc_channel_node);
            if (channel_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(channel_node, logo_desc_channel_id_node, it->first.get());
                dvblink::pugixml_helpers::new_child(channel_node, logo_desc_logo_id_node, it->second.id_.get());
                dvblink::pugixml_helpers::new_child(channel_node, logo_desc_logo_url_node, it->second.url_.get());
                dvblink::pugixml_helpers::new_child(channel_node, logo_desc_default_logo_node, it->second.default_logo_.get());
            }
            ++it;
        }
    }

    return doc;
}

//set channel logo

inline pugi::xml_node& operator>> (pugi::xml_node& node, channel_logo_id_map_t& channel_logo_map)
{
    if (NULL != node)
    {
        std::string str;
        pugi::xml_node channel_node = node.first_child();
        while (channel_node != NULL)
        {
            dvblink::logo_id_t logo_id;
            dvblink::channel_id_t channel_id;

            if (dvblink::pugixml_helpers::get_node_attribute(channel_node, logo_desc_channel_id_node, str))
                channel_id = str;

            if (dvblink::pugixml_helpers::get_node_value(channel_node, logo_desc_logo_id_node, str))
                logo_id = str;

            channel_logo_map[channel_id] = logo_id;

            channel_node = channel_node.next_sibling();
        }
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const channel_logo_id_map_t& channel_logo_map)
{
    pugi::xml_node node = doc.append_child(logo_desc_root_node.c_str());

    if (node != NULL)
    {
        channel_logo_id_map_t::const_iterator it = channel_logo_map.begin();
        while (it != channel_logo_map.end())
        {
            pugi::xml_node channel_node = dvblink::pugixml_helpers::new_child(node, logo_desc_channel_node);
            if (channel_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(channel_node, logo_desc_channel_id_node, it->first.get());
                dvblink::pugixml_helpers::new_child(channel_node, logo_desc_logo_id_node, it->second.get());
            }
            ++it;
        }
    }

    return doc;
}

//match logos in package to channels request

struct match_logo_package_req
{
	dvblink::logo_package_t package_;
    dvblex::channel_id_list_t channels_;
};

const std::string logo_package_match_items_root_node                = "match_logo_package";
const std::string logo_desc_channels_node                           = "channels";

inline pugi::xml_node& operator>> (pugi::xml_node& node, match_logo_package_req& req)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, logo_package_node, str))
            req.package_ = str;

        pugi::xml_node channels_node = node.child(logo_desc_channels_node.c_str());
        if (channels_node != NULL)
        {
            pugi::xml_node channel_node = channels_node.first_child();
            while (channel_node != NULL)
            {
                if (dvblink::pugixml_helpers::get_node_attribute(channel_node, logo_desc_channel_id_node, str))
                    req.channels_.push_back(str);

                channel_node = channel_node.next_sibling();
            }
        }

    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const match_logo_package_req& req)
{
    pugi::xml_node node = doc.append_child(logo_package_match_items_root_node.c_str());

    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, logo_package_node, req.package_.get());

        pugi::xml_node channels_node = dvblink::pugixml_helpers::new_child(node, logo_desc_channels_node);
        if (channels_node != NULL)
        {
            for (size_t i=0; i<req.channels_.size(); i++)
            {
                pugi::xml_node channel_node = dvblink::pugixml_helpers::new_child(channels_node, logo_desc_channel_node);
                if (channel_node != NULL)
                    dvblink::pugixml_helpers::add_node_attribute(channel_node, logo_desc_channel_id_node, req.channels_[i].get());
            }
        }

    }

    return doc;
}

const std::string logo_desc_logo_node                 = "logo";
const std::string logo_package_exact_match_node       = "exact";
const std::string logo_package_partial_match_node       = "partial";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const dvblex::channel_logo_match_map_t& match_info)
{
    pugi::xml_node mecr_node = doc.append_child(logo_package_match_items_root_node.c_str());
    if (mecr_node != NULL)
    {
        channel_logo_match_map_t::const_iterator it = match_info.begin();
        while (it != match_info.end())
        {
            pugi::xml_node channel_node = dvblink::pugixml_helpers::new_child(mecr_node, logo_desc_channel_node);
            if (channel_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(channel_node, logo_desc_channel_id_node, it->first.get());

                pugi::xml_node exact_node = dvblink::pugixml_helpers::new_child(channel_node, logo_package_exact_match_node);
                if (exact_node != NULL)
                {
                    for (size_t i=0; i<it->second.exact_match_.size(); i++)
                    {
                        pugi::xml_node logo_node = dvblink::pugixml_helpers::new_child(exact_node, logo_desc_logo_node);
                        if (logo_node != NULL)
                        {
                            dvblink::pugixml_helpers::add_node_attribute(logo_node, logo_desc_logo_id_node, it->second.exact_match_[i].to_string());
                        }
                    }
                }

                pugi::xml_node partial_node = dvblink::pugixml_helpers::new_child(channel_node, logo_package_partial_match_node);
                if (partial_node != NULL)
                {
                    for (size_t i=0; i<it->second.partial_match_.size(); i++)
                    {
                        pugi::xml_node logo_node = dvblink::pugixml_helpers::new_child(partial_node, logo_desc_logo_node);
                        if (logo_node != NULL)
                        {
                            dvblink::pugixml_helpers::add_node_attribute(logo_node, logo_desc_logo_id_node, it->second.partial_match_[i].to_string());
                        }
                    }
                }
            }

            ++it;
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, dvblex::channel_logo_match_map_t& match_info)
{
    if (NULL != node)
    {
        std::string str;

        pugi::xml_node channel_node = node.first_child();
        while (channel_node != NULL)
        {
            dvblink::channel_id_t channel_id;
            if (dvblink::pugixml_helpers::get_node_attribute(channel_node, logo_desc_channel_id_node, str))
                channel_id = str;

            channel_logo_match_t cm;
            pugi::xml_node exact_node = channel_node.child(logo_package_exact_match_node.c_str());
            if (exact_node != NULL)
            {
                pugi::xml_node logo_node = exact_node.first_child();
                while (logo_node != NULL)
                {
                    dvblink::logo_id_t logo_id;

                    if (dvblink::pugixml_helpers::get_node_attribute(logo_node, logo_desc_logo_id_node, str))
                        logo_id = str;

                    cm.exact_match_.push_back(logo_id);

                    logo_node = logo_node.next_sibling();
                }
            }

            pugi::xml_node partial_node = channel_node.child(logo_package_partial_match_node.c_str());
            if (partial_node != NULL)
            {
                pugi::xml_node logo_node = partial_node.first_child();
                while (logo_node != NULL)
                {
                    dvblink::logo_id_t logo_id;

                    if (dvblink::pugixml_helpers::get_node_attribute(logo_node, logo_desc_logo_id_node, str))
                        logo_id = str;

                    cm.partial_match_.push_back(logo_id);

                    logo_node = logo_node.next_sibling();
                }
            }

            match_info[channel_id] = cm;

            channel_node = channel_node.next_sibling();
        }
    }

    return node;
}


}
