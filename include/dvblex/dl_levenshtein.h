/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

namespace dvblex { 

enum levenshtein_distance_result_e
{
    ldr_none,
    ldr_exact,
    ldr_partial
};

class levenshtein_distance_calc
{
public:
    levenshtein_distance_calc();

    levenshtein_distance_result_e match(const std::string& first_str, const std::string& second_str);

protected:

    template <typename T>
    typename T::size_type levenshtein_distance(const T& src, const T& dst) const
    {
        const typename T::size_type m = src.size();
        const typename T::size_type n = dst.size();        
        if (m == 0)
        {
            return n;
        }
        if (n == 0) 
        {
            return m;
        }
        
        std::vector<std::vector<typename T::size_type> > matrix(m + 1);
        for (typename T::size_type i = 0; i <= m; ++i) 
        {
            matrix[i].resize(n + 1);
            matrix[i][0] = i;
        }
        for (typename T::size_type i = 0; i <= n; ++i) 
        {
            matrix[0][i] = i;
        }

        typename T::size_type above_cell, left_cell, diagonal_cell, cost;
        for (typename T::size_type i = 1; i <= m; ++i) 
        {
            for (typename T::size_type j = 1; j <= n; ++j) 
            {
                cost = src[i - 1] == dst[j - 1] ? 0 : 1;
                above_cell = matrix[i - 1][j];
                left_cell = matrix[i][j - 1];
                diagonal_cell = matrix[i - 1][j - 1];
                matrix[i][j] = (std::min)((std::min)(above_cell + 1, left_cell + 1), diagonal_cell + cost);
            }
        }

        return matrix[m][n];
    }

};

}
