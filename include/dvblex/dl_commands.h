/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <dl_types.h>
#include <dl_pugixml_helper.h>

namespace dvblex { 

const std::string get_channels_cmd = "get_channels";
const std::string get_stream_info_cmd = "get_stream_info";
const std::string get_streaming_caps_cmd = "get_streaming_capabilities";
const std::string play_channel_cmd = "play_channel";
const std::string stop_channel_cmd = "stop_channel";
const std::string search_epg_cmd = "search_epg";
const std::string get_recordings_cmd = "get_recordings";
const std::string get_recording_settings_cmd = "get_recording_settings";
const std::string set_recording_settings_cmd = "set_recording_settings";
const std::string get_schedules_cmd = "get_schedules";
const std::string add_schedule_cmd = "add_schedule";
const std::string update_schedule_cmd = "update_schedule";
const std::string remove_schedule_cmd = "remove_schedule";
const std::string remove_recording_cmd = "remove_recording";
const std::string set_parental_lock_cmd = "set_parental_lock";
const std::string get_parental_status_cmd = "get_parental_status";
const std::string get_playlist_m3u_cmd = "get_playlist_m3u";
const std::string get_objects_cmd = "get_object";
const std::string search_objects_cmd = "search_objects";
const std::string remove_object_cmd = "remove_object";
const std::string stop_recording_cmd = "stop_recording";
const std::string get_recordedtv_m3u_cmd = "get_recordedtv_m3u";
const std::string execute_command_cmd = "execute_command";
const std::string get_favorites_cmd = "get_favorites";
const std::string set_favorites_cmd = "set_favorites";
const std::string get_server_info_cmd = "get_server_info";
const std::string get_xmltv_epg_cmd = "get_xmltv_epg";
const std::string timeshift_get_stats_cmd = "timeshift_get_stats";
const std::string timeshift_seek_cmd = "timeshift_seek";
const std::string get_devices_cmd = "get_devices";
const std::string get_device_settings_cmd = "get_device_settings";
const std::string set_device_settings_cmd = "set_device_settings";
const std::string get_device_templates_cmd = "get_device_templates";
const std::string create_manual_device_cmd = "create_manual_device";
const std::string delete_manual_device_cmd = "delete_manual_device";
const std::string get_scanners_cmd = "get_scanners";
const std::string start_scan_cmd = "start_scan";
const std::string get_rescan_settings_cmd = "get_rescan_settings";
const std::string rescan_provider_cmd = "rescan_provider";
const std::string apply_scan_cmd = "apply_scan";
const std::string cancel_scan_cmd = "cancel_scan";
const std::string get_networks_cmd = "get_networks";
const std::string get_device_status_cmd = "get_device_status";
const std::string repair_database_cmd = "repair_database";
const std::string force_epg_update_cmd = "force_epg_update";
const std::string enable_epg_updates_cmd = "enable_epg_updates";
const std::string get_raw_channels_cmd = "get_raw_channels";
const std::string get_scanned_channels_cmd = "get_scanned_channels";
const std::string get_channels_visibility_cmd = "get_channels_visibility";
const std::string set_channels_visibility_cmd = "set_channels_visibility";
const std::string get_channel_overwrites_cmd = "get_channel_overwrites";
const std::string set_channel_overwrites_cmd = "set_channel_overwrites";
const std::string drop_provider_on_device_cmd = "drop_provider_on_device";
const std::string set_object_resume_info_cmd = "set_object_resume_info";
const std::string get_object_resume_info_cmd = "get_object_resume_info";
const std::string get_oob_channel_url_cmd = "get_oob_channel_url";

const std::string get_epg_sources_cmd = "get_epg_sources";
const std::string get_epg_channels_cmd = "get_epg_channels";
const std::string get_epg_channel_config_cmd = "get_epg_channel_config";
const std::string set_epg_channel_config_cmd = "set_epg_channel_config";
const std::string match_epg_channels_cmd = "match_epg_channels";

const std::string get_installed_products_cmd = "get_installed_products";
const std::string activate_product_cmd = "activate_product";
const std::string activate_product_trial_cmd = "activate_product_trial";

const std::string response_root_node                 = "response";
const std::string status_code_node                   = "status_code";
const std::string xml_result_node                    = "xml_result";

class command_response_t
{
public:
    command_response_t(int status)
        : status_(status), xml_result_(NULL) { }

    command_response_t(int status, const std::string* xml_result)
        : status_(status), xml_result_(xml_result) { }

    int status_;
    const std::string* xml_result_;
};

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const command_response_t& response)
{
    pugi::xml_node response_node = doc.append_child(response_root_node.c_str());

    if (response_node != NULL)
    {
        std::stringstream str_out;                
        str_out << response.status_;
        dvblink::pugixml_helpers::new_child(response_node, status_code_node, str_out.str());

        if (response.xml_result_ != NULL)
            dvblink::pugixml_helpers::new_child(response_node, xml_result_node, *(response.xml_result_));
    }

    return doc;
}

} //dvblex
