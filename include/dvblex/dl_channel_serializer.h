/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_pugixml_helper.h>
#include <dl_channels.h>

namespace dvblex { 

const std::string channels_root_node                     = "channels";
const std::string channel_channel_node                   = "channel";
const std::string channel_id_node                        = "channel_id";
const std::string channel_dvblink_id_node                = "channel_dvblink_id";
const std::string channel_name_node                      = "channel_name";
const std::string channel_number_node                    = "channel_number";
const std::string channel_subnumber_node                 = "channel_subnumber";
const std::string channel_type_node                      = "channel_type";
const std::string channel_childlock_node                 = "channel_child_lock";
const std::string channel_logo_node                      = "channel_logo";
const std::string channel_encrypted_node                 = "channel_encrypted";
const std::string channel_comment_node                   = "channel_comment";

enum external_channel_type_e
{
    ect_tv = 0,
    ect_radio = 1,
    ect_other = 2
};

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const channel_desc_list_t& cdl)
{
    std::stringstream buf;

    pugi::xml_node channels_node = doc.append_child(channels_root_node.c_str());

    if (channels_node != NULL)
    {
        for (size_t k=0; k<cdl.size(); k++)
        {
            const channel_description_t* cd = &(cdl[k]);

            pugi::xml_node channel_node = dvblink::pugixml_helpers::new_child(channels_node, channel_channel_node);
	        if (channel_node != NULL)
	        {
                dvblink::pugixml_helpers::new_child(channel_node, channel_id_node, cd->id_.get());

                dvblink::pugixml_helpers::new_child(channel_node, channel_dvblink_id_node, cd->id_.get());

                dvblink::pugixml_helpers::new_child(channel_node, channel_name_node, cd->name_.get());

                if (cd->num_ != invalid_channel_number_)
                {
                    buf.clear(); buf.str("");
                    buf << cd->num_;
                    dvblink::pugixml_helpers::new_child(channel_node, channel_number_node, buf.str());

                    if (cd->sub_num_ != invalid_channel_number_)
                    {
                        buf.clear(); buf.str("");
                        buf << cd->sub_num_;
                        dvblink::pugixml_helpers::new_child(channel_node, channel_subnumber_node, buf.str());
                    }
                }

                int chtype = ect_tv;
                if (cd->type_ == ct_radio)
                    chtype = ect_radio;

                buf.clear(); buf.str("");
                buf << chtype;
                dvblink::pugixml_helpers::new_child(channel_node, channel_type_node, buf.str());

                if (cd->child_lock_.get())
                    dvblink::pugixml_helpers::new_child(channel_node, channel_childlock_node, dvblink::pugixml_helpers::xmlnode_value_true);

                if (!cd->logo_.get().empty())
                    dvblink::pugixml_helpers::new_child(channel_node, channel_logo_node, cd->logo_.get());

                buf.clear(); buf.str("");
                buf << cd->encrypted_.get() ? "1" : "0";
                dvblink::pugixml_helpers::new_child(channel_node, channel_encrypted_node, buf.str());
	        }

        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, channel_desc_list_t& cdl)
{
    if (NULL != node)
    {
        std::string str;
        pugi::xml_node channel_node = node.first_child();
        while (channel_node != NULL)
        {
            if (boost::iequals(channel_node.name(), channel_channel_node))
            {
                channel_description_t cd;

                if (dvblink::pugixml_helpers::get_node_value(channel_node, channel_id_node, str))
                    cd.id_ = str;

                if (dvblink::pugixml_helpers::get_node_value(channel_node, channel_name_node, str))
                    cd.name_ = str;

                if (dvblink::pugixml_helpers::get_node_value(channel_node, channel_number_node, str))
                {
                    boost::int32_t value;
                    dvblink::engine::string_conv::apply<boost::int32_t, boost::int32_t, char>(str.c_str(), value, (boost::int32_t)invalid_channel_number_);
                    cd.num_ = value;
                }

                if (dvblink::pugixml_helpers::get_node_value(channel_node, channel_subnumber_node, str))
                {
                    boost::int32_t value;
                    dvblink::engine::string_conv::apply<boost::int32_t, boost::int32_t, char>(str.c_str(), value, (boost::int32_t)invalid_channel_number_);
                    cd.sub_num_ = value;
                }

                if (dvblink::pugixml_helpers::get_node_value(channel_node, channel_type_node, str))
                {
                    boost::uint32_t value;
                    dvblink::engine::string_conv::apply<boost::int32_t, boost::uint32_t, char>(str.c_str(), value, (boost::uint32_t)ect_tv);
                    cd.type_ = (channel_type_e)value;
                }

                if (dvblink::pugixml_helpers::get_node_value(channel_node, channel_childlock_node, str) &&
                    boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true))
                {
                    cd.child_lock_ = true;
                }

                if (dvblink::pugixml_helpers::get_node_value(channel_node, channel_logo_node, str))
                    cd.logo_ = str;

                if (dvblink::pugixml_helpers::get_node_value(channel_node, channel_encrypted_node, str) &&
                    boost::iequals(str, "1"))
                {
                    cd.encrypted_ = true;
                }

                cdl.push_back(cd);
            }

            channel_node = channel_node.next_sibling();
        }
    }

    return node;
}

//////////////////////////////////////////

const std::string channels_providers_root_node              = "providers";
const std::string channel_provider_node                     = "provider";
const std::string channel_provider_id_node                  = "id";
const std::string channel_provider_name_node                = "name";
const std::string channel_provider_desc_node                = "desc";
const std::string channel_transponder_node                  = "transponder";
const std::string channel_transponder_id_node               = "id";
const std::string channel_transponder_name_node             = "name";
const std::string channel_origin_node                       = "channel_origin";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const dvblex::concise_headend_desc_list_t& pdl)
{
    std::stringstream buf;

    pugi::xml_node providers_node = doc.append_child(channels_providers_root_node.c_str());

    if (providers_node != NULL)
    {
        for (size_t i=0; i<pdl.size(); i++)
        {
            const concise_channel_headend_t* pd = &(pdl[i]);

            pugi::xml_node provider_node = dvblink::pugixml_helpers::new_child(providers_node, channel_provider_node);
	        if (provider_node != NULL)
	        {
                dvblink::pugixml_helpers::add_node_attribute(provider_node, channel_provider_id_node, pd->id_.get());
                dvblink::pugixml_helpers::add_node_attribute(provider_node, channel_provider_name_node, pd->name_.get());
                dvblink::pugixml_helpers::add_node_attribute(provider_node, channel_provider_desc_node, pd->desc_.get());
                for (size_t j=0; j<pd->transponders_.size(); j++)
                {
                    const concise_transponder_description_t* td = &(pd->transponders_[j]);
                    pugi::xml_node tr_node = dvblink::pugixml_helpers::new_child(provider_node, channel_transponder_node);
                    if (tr_node != NULL)
                    {
                        dvblink::pugixml_helpers::add_node_attribute(tr_node, channel_transponder_id_node, td->tr_id_.get());
                        dvblink::pugixml_helpers::add_node_attribute(tr_node, channel_transponder_name_node, td->tr_name_.get());
                        for (size_t k=0; k<td->channels_.size(); k++)
                        {
                            const concise_device_channel_t* cd = &(td->channels_[k]);
                            pugi::xml_node ch_node = dvblink::pugixml_helpers::new_child(tr_node, channel_channel_node);
                            if (ch_node != NULL)
                            {
                                dvblink::pugixml_helpers::new_child(ch_node, channel_id_node, cd->id_.get());
                                dvblink::pugixml_helpers::new_child(ch_node, channel_name_node, cd->name_.get());

                                if (!cd->origin_.empty())
                                    dvblink::pugixml_helpers::new_child(ch_node, channel_origin_node, cd->origin_.get());

                                if (!cd->logo_.empty())
                                    dvblink::pugixml_helpers::new_child(ch_node, channel_logo_node, cd->logo_.get());

                                if (!cd->comment_.empty())
                                    dvblink::pugixml_helpers::new_child(ch_node, channel_comment_node, cd->comment_.get());

                                if (cd->encrypted_.get())
                                    dvblink::pugixml_helpers::new_child(ch_node, channel_encrypted_node, dvblink::pugixml_helpers::xmlnode_value_true);

                                if (cd->num_ != invalid_channel_number_)
                                {
                                    buf.clear(); buf.str("");
                                    buf << cd->num_;
                                    dvblink::pugixml_helpers::new_child(ch_node, channel_number_node, buf.str());

                                    if (cd->sub_num_ != invalid_channel_number_)
                                    {
                                        buf.clear(); buf.str("");
                                        buf << cd->sub_num_;
                                        dvblink::pugixml_helpers::new_child(ch_node, channel_subnumber_node, buf.str());
                                    }
                                }

                                buf.clear(); buf.str("");
                                buf << cd->type_;
                                dvblink::pugixml_helpers::new_child(ch_node, channel_type_node, buf.str());
                            }
                        }
                    }
                }
            }
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, dvblex::concise_headend_desc_list_t& pdl)
{
    if (NULL != node)
    {
        std::string str;
        pugi::xml_node provider_node = node.first_child();
        while (provider_node != NULL)
        {
            if (boost::iequals(provider_node.name(), channel_provider_node))
            {
                concise_channel_headend_t pd;

                if (dvblink::pugixml_helpers::get_node_attribute(provider_node, channel_provider_id_node, str))
                    pd.id_ = str;

                if (dvblink::pugixml_helpers::get_node_attribute(provider_node, channel_provider_name_node, str))
                    pd.name_ = str;

                if (dvblink::pugixml_helpers::get_node_attribute(provider_node, channel_provider_desc_node, str))
                    pd.desc_ = str;

                pugi::xml_node tr_node = provider_node.first_child();
                while (tr_node != NULL)
                {
                    if (boost::iequals(tr_node.name(), channel_transponder_node))
                    {
                        concise_transponder_description_t td;

                        if (dvblink::pugixml_helpers::get_node_attribute(tr_node, channel_transponder_id_node, str))
                            td.tr_id_ = str;

                        if (dvblink::pugixml_helpers::get_node_attribute(tr_node, channel_transponder_name_node, str))
                            td.tr_name_ = str;

                        pugi::xml_node ch_node = tr_node.first_child();
                        while (ch_node != NULL)
                        {
                            if (boost::iequals(ch_node.name(), channel_channel_node))
                            {
                                concise_device_channel_t cd;

                                if (dvblink::pugixml_helpers::get_node_value(ch_node, channel_id_node, str))
                                    cd.id_ = str;

                                if (dvblink::pugixml_helpers::get_node_value(ch_node, channel_name_node, str))
                                    cd.name_ = str;

                                if (dvblink::pugixml_helpers::get_node_value(ch_node, channel_origin_node, str))
                                    cd.origin_ = str;

                                if (dvblink::pugixml_helpers::get_node_value(ch_node, channel_logo_node, str))
                                    cd.logo_ = str;

                                if (dvblink::pugixml_helpers::get_node_value(ch_node, channel_comment_node, str))
                                    cd.comment_ = str;

                                cd.encrypted_ = dvblink::pugixml_helpers::get_node_value(ch_node, channel_encrypted_node, str) &&
                                    boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);

                                if (dvblink::pugixml_helpers::get_node_value(ch_node, channel_number_node, str))
                                {
                                    boost::int32_t value;
                                    dvblink::engine::string_conv::apply<boost::int32_t, boost::int32_t, char>(str.c_str(), value, (boost::int32_t)invalid_channel_number_);
                                    cd.num_ = value;
                                }

                                if (dvblink::pugixml_helpers::get_node_value(ch_node, channel_subnumber_node, str))
                                {
                                    boost::int32_t value;
                                    dvblink::engine::string_conv::apply<boost::int32_t, boost::int32_t, char>(str.c_str(), value, (boost::int32_t)invalid_channel_number_);
                                    cd.sub_num_ = value;
                                }

                                if (dvblink::pugixml_helpers::get_node_value(ch_node, channel_type_node, str))
                                {
                                    boost::uint32_t value;
                                    dvblink::engine::string_conv::apply<boost::int32_t, boost::uint32_t, char>(str.c_str(), value, (boost::uint32_t)ect_tv);
                                    cd.type_ = (channel_type_e)value;
                                }

                                td.channels_.push_back(cd);
                            }

                            ch_node = ch_node.next_sibling();
                        }
                        pd.transponders_.push_back(td);
                    }

                    tr_node = tr_node.next_sibling();
                }
                pdl.push_back(pd);
            }

            provider_node = provider_node.next_sibling();
        }
    }

    return node;
}

//////////////////////////////////////////

const std::string channels_favorites_root                = "favorites";
const std::string channel_favorite_node                  = "favorite";
const std::string channel_favorite_id_node               = "id";
const std::string channel_favorite_name_node             = "name";
const std::string channel_favorite_flags_node             = "flags";
const std::string channel_favorite_channels_node         = "channels";
const std::string channel_favorite_channel_node         = "channel";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const channel_favorite_list_t& cfl)
{
    pugi::xml_node favorites_node = doc.append_child(channels_favorites_root.c_str());

    if (favorites_node)
    {
        for (size_t i=0; i<cfl.size(); i++)
        {
            pugi::xml_node favorite_node = dvblink::pugixml_helpers::new_child(favorites_node, channel_favorite_node);
			if (favorite_node != NULL)
			{
                dvblink::pugixml_helpers::new_child(favorite_node, channel_favorite_id_node, cfl[i].id_.get());
                dvblink::pugixml_helpers::new_child(favorite_node, channel_favorite_name_node, cfl[i].name_.get());

                std::stringstream buf;
                buf.clear(); buf.str("");
                buf << cfl[i].flags_;
                dvblink::pugixml_helpers::new_child(favorite_node, channel_favorite_flags_node, buf.str());

                pugi::xml_node channels_node = dvblink::pugixml_helpers::new_child(favorite_node, channel_favorite_channels_node);
			    if (channels_node != NULL)
			    {
                    for (size_t j=0; j<cfl[i].channels_.size(); j++)
                        dvblink::pugixml_helpers::new_child(channels_node, channel_favorite_channel_node, cfl[i].channels_[j].get());
			    }
			}
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, channel_favorite_list_t& cfl)
{
    if (NULL != node)
    {
        std::string str;
        pugi::xml_node fav_node = node.first_child();
        while (fav_node != NULL)
        {
            if (boost::iequals(fav_node.name(), channel_favorite_node))
            {
                channel_favorite_t fav;

                if (dvblink::pugixml_helpers::get_node_value(fav_node, channel_favorite_id_node, str))
                    fav.id_ = str;

                if (dvblink::pugixml_helpers::get_node_value(fav_node, channel_favorite_name_node, str))
                    fav.name_ = str;

                if (dvblink::pugixml_helpers::get_node_value(fav_node, channel_favorite_flags_node, str))
                {
                    boost::uint32_t value;
                    dvblink::engine::string_conv::apply(str.c_str(), value, (boost::uint32_t)cft_none);
                    fav.flags_ = (channel_favorite_type_e)value;
                }

                pugi::xml_node channels_node = fav_node.child(channel_favorite_channels_node.c_str());

                if (channels_node != NULL)
                {
                    pugi::xml_node channel_node = channels_node.first_child();
                    while (channel_node != NULL)
                    {
                        if (boost::iequals(channel_node.name(), channel_favorite_channel_node))
                        {
                            fav.channels_.push_back(channel_node.child_value());
                        }
                        channel_node = channel_node.next_sibling();
                    }
                }

                cfl.push_back(fav);
            }

            fav_node = fav_node.next_sibling();
        }
    }
    return node;
}

//////////////////////////////////////////

const std::string channels_visibility_root                = "invisible_channels";
const std::string channel_visibility_channel_node         = "channel";
const std::string channel_visibility_id_node              = "id";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const invisible_channel_map_t& icm)
{
    pugi::xml_node cv_node = doc.append_child(channels_visibility_root.c_str());

    if (cv_node != NULL)
    {
        invisible_channel_map_t::const_iterator it = icm.begin();
        while (it != icm.end())
        {
            pugi::xml_node channel_node = dvblink::pugixml_helpers::new_child(cv_node, channel_visibility_channel_node);

			if (channel_node != NULL)
                dvblink::pugixml_helpers::add_node_attribute(channel_node, channel_visibility_id_node, it->first.get());

            ++it;
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, invisible_channel_map_t& icm)
{
    if (NULL != node)
    {
        std::string str;
        pugi::xml_node channel_node = node.first_child();
        while (channel_node != NULL)
        {
            if (boost::iequals(channel_node.name(), channel_visibility_channel_node))
            {
                if (dvblink::pugixml_helpers::get_node_attribute(channel_node, channel_visibility_id_node, str))
                    icm[str] = str;
            }

            channel_node = channel_node.next_sibling();
        }
    }

    return node;
}

//////////////////////////////////////////

const std::string channels_overwrites_root                = "channel_overwrites";
const std::string channel_overwrites_ov_node              = "overwrite";
const std::string channel_overwrites_id_node              = "channel_id";
const std::string channel_overwrites_name_node              = "name";
const std::string channel_overwrites_num_node              = "num";
const std::string channel_overwrites_subnum_node              = "subnum";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const channel_overwrite_map_t& com)
{
    std::stringstream buf;

    pugi::xml_node root_node = doc.append_child(channels_overwrites_root.c_str());
    if (root_node != NULL)
    {
        channel_overwrite_map_t::const_iterator it = com.begin();
        while (it != com.end())
        {
            pugi::xml_node ov_node = dvblink::pugixml_helpers::new_child(root_node, channel_overwrites_ov_node);
            if (ov_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(ov_node, channel_overwrites_id_node, it->first.get());
                if (!it->second.name_.empty())
                    dvblink::pugixml_helpers::add_node_attribute(ov_node, channel_overwrites_name_node, it->second.name_.get()); 

                if (it->second.number_.get() > 0)
                {
                    buf.clear(); buf.str("");
                    buf << it->second.number_.get();
                    dvblink::pugixml_helpers::add_node_attribute(ov_node, channel_overwrites_num_node, buf.str()); 

                    if (it->second.subnumber_.get() > 0)
                    {
                        buf.clear(); buf.str("");
                        buf << it->second.subnumber_.get();
                        dvblink::pugixml_helpers::add_node_attribute(ov_node, channel_overwrites_subnum_node, buf.str()); 
                    }
                }
            }
            ++it;
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, channel_overwrite_map_t& com)
{
    std::string str;

    if (NULL != node)
    {
        pugi::xml_node ov_node = node.first_child();
        while (ov_node != NULL)
        {
            if (boost::iequals(ov_node.name(), channel_overwrites_ov_node))
            {
                dvblink::channel_id_t chid;
                channel_overwrite_t ov;
                if (dvblink::pugixml_helpers::get_node_attribute(ov_node, channel_overwrites_id_node, str))
                {
                    chid = str;

                    if (dvblink::pugixml_helpers::get_node_attribute(ov_node, channel_overwrites_name_node, str))
                        ov.name_ = str;

                    if (dvblink::pugixml_helpers::get_node_attribute(ov_node, channel_overwrites_num_node, str))
                    {
                        boost::int32_t i32;
                        dvblink::engine::string_conv::apply(str.c_str(), i32, ov.number_.get());
                        ov.number_ = i32;

                        if (dvblink::pugixml_helpers::get_node_attribute(ov_node, channel_overwrites_subnum_node, str))
                        {
                            boost::int32_t i32;
                            dvblink::engine::string_conv::apply(str.c_str(), i32, ov.subnumber_.get());
                            ov.subnumber_ = i32;
                        }
                    }
                    com[chid] = ov;
                }
            }

            ov_node = ov_node.next_sibling();
        }
    }

    return node;
}

//////////////////////////////////////////////////////////////////////
struct get_channels_request_t
{
    dvblink::favorite_id_t favorite_id_;
};

const std::string get_channels_request_root                = "channels_request";
const std::string get_channels_request_favorite_id_node    = "favorite_id";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_channels_request_t& request)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, get_channels_request_favorite_id_node, str))
            request.favorite_id_ = str;
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_channels_request_t& request)
{
    pugi::xml_node req_node = doc.append_child(get_channels_request_root.c_str());

    if (req_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(req_node, get_channels_request_favorite_id_node, request.favorite_id_.get());
    }

    return doc;
}

//////////////////////////////////////////////////////////////////////
struct get_oob_channel_url_request_t
{
    get_oob_channel_url_request_t()
        : format_(dcst_raw)
    {}

    dvblink::channel_id_t channel_id_;
    device_channel_stream_type_e format_;
};

const std::string get_oob_channel_url_request_root                = "get_oob_channel_url";
const std::string get_oob_channel_url_request_format_node         = "format";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_oob_channel_url_request_t& request)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, channel_id_node, str))
            request.channel_id_ = str;

        if (dvblink::pugixml_helpers::get_node_value(node, get_oob_channel_url_request_format_node, str))
        {
            boost::int32_t i32;
            dvblink::engine::string_conv::apply(str.c_str(), i32, (boost::int32_t)request.format_);
            request.format_ = (device_channel_stream_type_e)i32;
        }

    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_oob_channel_url_request_t& request)
{
    std::stringstream buf;

    pugi::xml_node req_node = doc.append_child(get_oob_channel_url_request_root.c_str());
    if (req_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(req_node, channel_id_node, request.channel_id_.to_string());

        buf.clear(); buf.str("");
        buf << request.format_;
        dvblink::pugixml_helpers::new_child(req_node, get_oob_channel_url_request_format_node, buf.str());
    }

    return doc;
}

//////////////////////////////////////////////////////////////////////
struct get_oob_channel_url_response_t
{
    dvblink::url_address_t url_;
    dvblink::mime_type_t mime_;
};

const std::string get_oob_channel_url_response_root                = "oob_channel_url";
const std::string get_oob_channel_url_url_node    = "url";
const std::string get_oob_channel_url_mime_node    = "mime";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_oob_channel_url_response_t& response)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, get_oob_channel_url_url_node, str))
            response.url_ = str;

        if (dvblink::pugixml_helpers::get_node_value(node, get_oob_channel_url_mime_node, str))
            response.mime_ = str;
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_oob_channel_url_response_t& response)
{
    pugi::xml_node req_node = doc.append_child(get_oob_channel_url_response_root.c_str());

    if (req_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(req_node, get_oob_channel_url_url_node, response.url_.to_string());
        dvblink::pugixml_helpers::new_child(req_node, get_oob_channel_url_mime_node, response.mime_);
    }

    return doc;
}

} //dvblex
