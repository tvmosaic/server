/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <map>
#include <sstream>
#include <string>

namespace dvblink {

/* Converts a hex character to its integer value */
inline char from_hex(char ch) {
  return isdigit(ch) ? ch - '0' : static_cast<char>(tolower(ch) - 'a' + 10);
}

/* Converts an integer value to its hex character*/
inline char to_hex(char code) {
  static char hex[] = "0123456789abcdef";
  return hex[tolower(code) & 0x0F];
}

/* Returns a url-encoded version of str */
inline void url_encode(const std::string& str, std::string& res_str) 
{
    res_str.clear();
    res_str.resize(str.size() * 3 + 1);

    unsigned char* pbuf = (unsigned char*)&res_str[0];
    for (unsigned int i=0; i<str.size(); i++) 
    {
        if (isalnum((unsigned char)str[i]) || str[i] == '-' || str[i] == '_' || str[i] == '.' || str[i] == '~') 
            *pbuf++ = str[i];
        else if (str[i] == ' ') 
            *pbuf++ = '+';
            else 
                *pbuf++ = '%', *pbuf++ = to_hex(str[i] >> 4), *pbuf++ = to_hex(str[i] & 0x0F);
    }
    *pbuf = '\0';
    res_str.resize(strlen(res_str.c_str()));
}

/* Returns a url-decoded version of str */
inline void url_decode(const char *str, std::string& res_str) 
{
    res_str.clear();
    res_str.resize(strlen(str) + 1);

    const unsigned char *pstr = (const unsigned char*)str;
    unsigned char *pbuf = (unsigned char*)&res_str[0];
    while (*pstr) 
    {
        if (*pstr == '%') 
        {
            if (pstr[1] && pstr[2]) 
            {
                *pbuf++ = from_hex(pstr[1]) << 4 | from_hex(pstr[2]);
                pstr += 2;
            }
        } else if (*pstr == '+') 
        { 
            *pbuf++ = ' ';
        } else 
        {
            *pbuf++ = *pstr;
        }
        pstr++;
    }
    *pbuf = '\0';
    res_str.resize(strlen(res_str.c_str()));
}

inline void make_url_encoded_param_string(std::map<std::string, std::string>& params, std::string& res_str)
{
    res_str.clear();
    std::string str;
    std::ostringstream ostr;
    std::map<std::string, std::string>::iterator it = params.begin();
    while (it != params.end())
    {
        if (ostr.str().size() > 0)
            ostr << "&";
        ostr << it->first << "=";
        url_encode(it->second, str);
        ostr << str;
        
        it++;
    }
    res_str = ostr.str();
}

}
