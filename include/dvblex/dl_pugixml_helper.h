/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <pugixml.hpp>
#include <dl_strings.h>

namespace dvblink { namespace pugixml_helpers {

const std::string xmlnode_value_true                 = "true";
const std::string xmlnode_value_false                = "false";

pugi::xml_node new_child(pugi::xml_node& node, const std::string& name);

pugi::xml_node new_child(pugi::xml_node& node, const std::string& name, const std::string& value);
pugi::xml_node new_child(pugi::xml_node& node, const std::string& name, const std::wstring& value);
pugi::xml_node new_child(pugi::xml_node& node, const std::string& name, const char* value);
pugi::xml_node new_child(pugi::xml_node& node, const std::string& name, const wchar_t* value);

void add_node_attribute(pugi::xml_node& node, const std::string& name, const std::wstring& value);
void add_node_attribute(pugi::xml_node& node, const std::string& name, const std::string& value);

template <typename INT>
pugi::xml_node new_child(pugi::xml_node& node, const std::string& name, INT value)
{
    std::string svalue = boost::lexical_cast<std::string>(value);
    return new_child(node, name, svalue);
}

bool get_node_value(const pugi::xml_node& parent_node, const std::string& name, std::string& value);
bool get_node_value(const pugi::xml_node& parent_node, const std::string& name, std::wstring& value);

template <typename INT>
bool get_node_value(const pugi::xml_node& parent_node, const std::string& name, INT& value)
{
    bool res = false;
    std::string svalue;
    if (get_node_value(parent_node, name, svalue))
    {
        try
        {
            value = boost::lexical_cast<INT>(svalue);
            res = true;
        }
        catch (boost::bad_lexical_cast& /*e*/)
        {
        }
    }
    return res;
}

template <typename INT>
bool get_node_value(const pugi::xml_node& parent_node, const std::string& name, INT default_value, INT& value)
{
    std::string svalue;
    if (get_node_value(parent_node, name, svalue))
    {
        engine::string_conv::apply(svalue.c_str(), value, default_value);
        return true;
    }
    return false;
}

bool get_node_attribute(const pugi::xml_node& node, const std::string& attribute_name, std::string& value);
bool get_node_attribute(const pugi::xml_node& node, const std::string& attribute_name, std::wstring& value);

template <typename INT>
bool get_node_attribute(const pugi::xml_node& node, const std::string& attribute_name, INT& value)
{
    bool res = false;
    std::string svalue;
    if (get_node_attribute(node, attribute_name, svalue))
    {
        try
        {
            value = boost::lexical_cast<INT>(svalue);
            res = true;
        }
        catch (boost::bad_lexical_cast& /*e*/)
        {
        }
    }
    return res;
}

template <typename INT>
bool get_node_attribute(const pugi::xml_node& node, const std::string& attribute_name, INT default_value, INT& value)
{
    std::string svalue;
    if (get_node_attribute(node, attribute_name, svalue))
    {
        engine::string_conv::apply(svalue.c_str(), value, default_value);
        return true;
    }
    return false;
}

void xmldoc_dump_to_string(const pugi::xml_document& doc, std::string& dump);
bool xmldoc_dump_to_file(const pugi::xml_document& doc, const std::string& filename);

} //pugixml_helpers
} //dvblink
