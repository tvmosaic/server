/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <map>
#include <vector>
#include <sstream>
#include <string>
#include <iomanip>
#include <boost/algorithm/string.hpp>
#include <boost/range/algorithm/remove_if.hpp>
#include <dl_epgevent.h>
#include <dl_strings.h>

namespace dvblink {

enum filename_pattern_fields_e
{
	e_fpf_channel_name = 0,
	e_fpf_channel_number,
	e_fpf_program_name,
	e_fpf_program_subname,
	e_fpf_program_season_episode,
	e_fpf_program_date_time,
	e_fpf_program_date,
	e_fpf_program_time,
	e_fpf_last_field
};

static const char* filename_pattern_filed_names[] = {"%channel.name%", "%channel.number%", "%program.name%", "%program.subname%", "%program.season/episode%", "%program.date/time%", "%program.date%", "%program.time%" };

typedef std::vector<filename_pattern_fields_e> filename_pattern_list_t;

struct filename_pattern_t
{
	filename_pattern_t() :
		separator("-")
		{}

	filename_pattern_list_t fields;
	std::string separator;
};

inline void string_to_filename_pattern(const std::string& pattern_str, filename_pattern_t& filename_pattern)
{
	std::string str = pattern_str;

	while (str.size() > 0)
	{
		size_t idx = e_fpf_last_field;
		for (size_t i=0; i<e_fpf_last_field; i++)
		{
			if (boost::starts_with(str, filename_pattern_filed_names[i]))
			{
				idx = i;
				break;
			}
		}
		if (idx != e_fpf_last_field)
		{
			//token
			filename_pattern.fields.push_back((filename_pattern_fields_e)idx);
			str.erase(0, strlen(filename_pattern_filed_names[idx]));
		} else
		{
			//separator
			filename_pattern.separator = str[0];
			str.erase(0, 1);
		}
	}
}

inline void filename_pattern_to_string(const filename_pattern_t& filename_pattern, std::string& pattern_str)
{
	for (size_t i=0; i<filename_pattern.fields.size(); i++)
	{
		if (pattern_str.size() > 0)
			pattern_str += filename_pattern.separator;

		pattern_str += filename_pattern_filed_names[ filename_pattern.fields[i] ];
	}
}

inline std::wstring filename_pattern_prepare_safe_string(const std::wstring& str, bool allow_extended_chars)
{
	std::wstring wstr;
	//get only alpha-numeric characters
	if (allow_extended_chars)
	{
		wstr = str;
		wstr.erase(boost::remove_if(wstr, boost::is_any_of(L"/\n\r\t\f`~?#*\\<>|\":")), wstr.end());
	} else
	{
		for (size_t i = 0; i < str.size(); i++)
		{
			if (str[i] > 0)
			{
				if ( isalnum(str[i]) )
				{
					wstr += str[i];
				}
				else
				if (isspace(str[i]))
				{
					wstr += '_';
				}
			}
		}
	}
	return wstr;
}

inline void filename_pattern_add_token(const std::wstring& token, const std::wstring& separator, bool allow_extended_chars, std::wstring& str)
{
	std::wstring safe_token = filename_pattern_prepare_safe_string(token, allow_extended_chars);
	if (safe_token.size() > 0)
	{
		if (str.size() > 0)
			str += separator;

		str += safe_token;
	}
}

inline void filename_pattern_string_to_filename(const std::string& pattern_string, const engine::DLEPGEvent& epg_event, 
										 const std::string& ch_name, int ch_num, int ch_subnum, bool allow_extended_chars, std::wstring& filename)
{
	filename.clear();

	filename_pattern_t pattern;
	string_to_filename_pattern(pattern_string, pattern);

	std::wstring separator = engine::string_cast<engine::EC_UTF8>(pattern.separator);
	for (size_t i = 0; i<pattern.fields.size(); i++)
	{
		switch (pattern.fields[i])
		{
		case e_fpf_channel_name:
            {
				std::wstring wstr = engine::string_cast<engine::EC_UTF8>(ch_name);
    			filename_pattern_add_token(wstr, separator, allow_extended_chars, filename);
            }
			break;
		case e_fpf_channel_number:
			{
				if (ch_num > 0)
				{
					std::wstringstream ss;
					ss << ch_num;
					if (ch_subnum > 0)
						ss << L"." << ch_subnum;
					filename_pattern_add_token(ss.str(), separator, allow_extended_chars, filename);
				}
			}
			break;
		case e_fpf_program_name:
			{
				std::wstring wstr = engine::string_cast<engine::EC_UTF8>(epg_event.m_Name);
				filename_pattern_add_token(wstr, separator, allow_extended_chars, filename);
			}
			break;
		case e_fpf_program_subname:
			{
				std::wstring wstr = engine::string_cast<engine::EC_UTF8>(epg_event.m_SecondName);
				filename_pattern_add_token(wstr, separator, allow_extended_chars, filename);
			}
			break;
		case e_fpf_program_season_episode:
			if (epg_event.m_SeasonNum != 0 || epg_event.m_EpisodeNum != 0)
			{
				std::wstringstream ss;
				if (epg_event.m_SeasonNum != 0)
				{
					ss << L"S" << std::setw(2) << std::setfill(L'0') << epg_event.m_SeasonNum;
				}
				if (epg_event.m_EpisodeNum != 0)
				{
					ss << L"E" << std::setw(2) << std::setfill(L'0') << epg_event.m_EpisodeNum;
				}
				filename_pattern_add_token(ss.str(), separator, allow_extended_chars, filename);
			}
			break;
		case e_fpf_program_date_time:
			{
				char dt_buf[1024]; dt_buf[0] = '\0';
				if (struct tm* ti = localtime(&epg_event.m_StartTime))
				{
					sprintf(dt_buf, "%0.2d%0.2d", ti->tm_hour, ti->tm_min);
					std::wstring wstr = engine::string_cast<engine::EC_UTF8>(std::string(dt_buf));
					filename_pattern_add_token(wstr, separator, allow_extended_chars, filename);

					sprintf(dt_buf, "%d%0.2d%0.2d", ti->tm_year + 1900, ti->tm_mon + 1, ti->tm_mday);
					wstr = engine::string_cast<engine::EC_UTF8>(std::string(dt_buf));
					filename_pattern_add_token(wstr, separator, allow_extended_chars, filename);
				}
			}
			break;
		case e_fpf_program_date:
			{
				char dt_buf[1024]; dt_buf[0] = '\0';
				if (struct tm* ti = localtime(&epg_event.m_StartTime))
				{
					sprintf(dt_buf, "%d%0.2d%0.2d", ti->tm_year + 1900, ti->tm_mon + 1, ti->tm_mday);
                    std::wstring wstr = engine::string_cast<engine::EC_UTF8>(std::string(dt_buf));
					filename_pattern_add_token(wstr, separator, allow_extended_chars, filename);
				}
			}
			break;
		case e_fpf_program_time:
			{
				char dt_buf[1024]; dt_buf[0] = '\0';
				if (struct tm* ti = localtime(&epg_event.m_StartTime))
				{
					sprintf(dt_buf, "%0.2d%0.2d", ti->tm_hour, ti->tm_min);
					std::wstring wstr = engine::string_cast<engine::EC_UTF8>(std::string(dt_buf));
					filename_pattern_add_token(wstr, separator, allow_extended_chars, filename);
				}
			}
			break;
		}
	}
}

}
