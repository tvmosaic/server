/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <dl_pugixml_helper.h>

namespace dvblex {

template <typename T>
bool write_to_xml(const T& object, std::string& xml_string)
{
    bool success = false;
    
    try
    {
        pugi::xml_document doc;
        doc << object;
        dvblink::pugixml_helpers::xmldoc_dump_to_string(doc, xml_string);

        success = true;
    }
    catch (...)
    {
        success = false;
    }

    return success;
}

template <typename T>
bool read_from_xml(const std::string& xml_string, T& object)
{
    bool success = false;
    
    try
    {
        pugi::xml_document doc;
        if (doc.load_buffer(xml_string.c_str(), xml_string.length()).status == pugi::status_ok)
        {
            pugi::xml_node root_node = doc.first_child();
            if (root_node != NULL)
            {
                root_node >> object;
                success = true;
            }
        }
    }
    catch (...)
    {
        success = false;
    }
    
    return success;
}

}
