/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_COMMON_H_
#define __DVBLINK_DL_COMMON_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <string>

namespace dvblink
{

#define COMPANY_NAME                            L"DVBLogic"
#define PRODUCT_NAME                            L"TVMosaic"
#define PRODUCT_NAME_UTF8                       "TVMosaic"
#define TVMOSAIC_SERVER_NAME                    "TVMosaic Server"
#define SERVICE_NAME                            L"tvmosaic_server"

#define RECORDINGS_DATA_FILE_NAME               L"tvmosaic_recordings_data.csv"

#define TVMOSAIC_ROOT_CONFIG_DIR_ENV            "TVMOSAIC_ROOT_CONFIG_DIR"

#define CONFIG_STORAGE_ROOT_NODE_NAME           "tvmosaic_configuration"
#define PERMANENT_CONFIG_STORAGE_FILE_NAME      L"tvmosaic_configuration.xml"

#define SERVER_INSTALL_PATH                     L"/install_path"
#define PRIVATE_ROOT                            L"/private_root"
#define SHARED_ROOT                             L"/shared_root"
#define SERVER_ID                               L"/server_id"

//Annex A text descriptors helpers
enum EDVBDefaultCodepage
{
    EDDC_ISO_8859_1 = 0,
    EDDC_ISO_6937,
    EDDC_ISO_8859_2
};

struct server_version_t
{
    int v1;
    int v2;
    int v3;
    int v4;
};

enum dvblink_server_state_e
{
    dss_unknown,
    dss_init,
    dss_starting,
    dss_active,
    dss_going_to_standby,
    dss_standby,
    dss_waking_up,
    dss_shutting_down,
    dss_shut_down,
    dss_backup_active,
    dss_restore_active
};

#define XML_ENCODING                            "utf-8"

const std::string xmlcmd_result_success = "success";
const std::string xmlcmd_result_error = "error";

#define COMMON_DIRECTORY                        L"common"
#define FFMPEG_DIRECTORY                        L"ffmpeg"
#define LANGUAGES_DIRECTORY                     L"language"
#define LICENSES_DIRECTORY                      L"licenses"
#define PACKAGES_TEMP_DIR                       L"temp"
#define SHARE_DIR                               L"share"
#define UPDATE_FILES_DIR                        L"update_info"
#define TIMESHIFT_TEMP_DIR                      L"temp"

#define DISCOVERY_DEFAULT_PORT                  65432

#define SERVER_PROCESS_PRIORITY_DEFAULT_VALUE   NORMAL_PRIORITY_CLASS

#define EPG_ROOT_NODE                           "dvblink_epg"
#define EPG_ROOT_NODE_UC                        L"dvblink_epg"
#define EPG_CHANNEL                             "channel"
#define EPG_CHANNEL_UC                          L"channel"
#define EPG_CHANNEL_NAME                        "name"
#define EPG_CHANNEL_NAME_UC                     L"name"
#define EPG_CHANNEL_ID                          "id"
#define EPG_CHANNEL_ID_UC                       L"id"
#define EPG_PROGRAM                             "program"
#define EPG_PROGRAM_ID                          "id"
#define EPG_PROGRAM_UC                          L"program"
#define EPG_PROGRAM_NAME                        "name"
#define EPG_PROGRAM_NAME_UC                     L"name"
#define EPG_PROGRAM_SHORT_DESC                  "short_desc"
#define EPG_PROGRAM_SHORT_DESC_UC               L"short_desc"
#define EPG_PROGRAM_START_TIME                  "start_time"
#define EPG_PROGRAM_START_TIME_UC               L"start_time"
#define EPG_PROGRAM_DURATION                    "duration"
#define EPG_PROGRAM_DURATION_UC                 L"duration"
#define EPG_PROGRAM_SUBNAME                     "subname"
#define EPG_PROGRAM_SUBNAME_UC                  L"subname"
#define EPG_PROGRAM_LANGUAGE                    "language"
#define EPG_PROGRAM_LANGUAGE_UC                 L"language"
#define EPG_PROGRAM_ACTORS                      "actors"
#define EPG_PROGRAM_ACTORS_UC                   L"actors"
#define EPG_PROGRAM_DIRECTORS                   "directors"
#define EPG_PROGRAM_DIRECTORS_UC                L"directors"
#define EPG_PROGRAM_WRITERS                     "writers"
#define EPG_PROGRAM_WRITERS_UC                  L"writers"
#define EPG_PROGRAM_PRODUCERS                   "producers"
#define EPG_PROGRAM_PRODUCERS_UC                L"producers"
#define EPG_PROGRAM_GUESTS                      "guests"
#define EPG_PROGRAM_GUESTS_UC                   L"guests"
#define EPG_PROGRAM_IMAGE                       "image"
#define EPG_PROGRAM_IMAGE_UC                    L"image"
#define EPG_PROGRAM_YEAR                        "year"
#define EPG_PROGRAM_YEAR_UC                     L"year"
#define EPG_PROGRAM_EPISODE_NUM                 "episode_num"
#define EPG_PROGRAM_EPISODE_NUM_UC              L"episode_num"
#define EPG_PROGRAM_SEASON_NUM                  "season_num"
#define EPG_PROGRAM_SEASON_NUM_UC               L"season_num"
#define EPG_PROGRAM_STARS_NUM                   "stars_num"
#define EPG_PROGRAM_STARS_NUM_UC                L"stars_num"
#define EPG_PROGRAM_STARSMAX_NUM                "starsmax_num"
#define EPG_PROGRAM_STARSMAX_NUM_UC             L"starsmax_num"
#define EPG_PROGRAM_HDTV                        "hdtv"
#define EPG_PROGRAM_HDTV_UC                     L"hdtv"
#define EPG_PROGRAM_PREMIERE                    "premiere"
#define EPG_PROGRAM_PREMIERE_UC                 L"premiere"
#define EPG_PROGRAM_REPEAT                      "repeat"
#define EPG_PROGRAM_REPEAT_UC                   L"repeat"
#define EPG_PROGRAM_CATEGORIES                  "categories"
#define EPG_PROGRAM_CATEGORIES_UC               L"categories"
#define EPG_PROGRAM_CAT_ACTION                  "cat_action"
#define EPG_PROGRAM_CAT_ACTION_UC               L"cat_action"
#define EPG_PROGRAM_CAT_COMEDY                  "cat_comedy"
#define EPG_PROGRAM_CAT_COMEDY_UC               L"cat_comedy"
#define EPG_PROGRAM_CAT_DOCUMENTARY             "cat_documentary"
#define EPG_PROGRAM_CAT_DOCUMENTARY_UC          L"cat_documentary"
#define EPG_PROGRAM_CAT_DRAMA                   "cat_drama"
#define EPG_PROGRAM_CAT_DRAMA_UC                L"cat_drama"
#define EPG_PROGRAM_CAT_EDUCATIONAL             "cat_educational"
#define EPG_PROGRAM_CAT_EDUCATIONAL_UC          L"cat_educational"
#define EPG_PROGRAM_CAT_HORROR                  "cat_horror"
#define EPG_PROGRAM_CAT_HORROR_UC               L"cat_horror"
#define EPG_PROGRAM_CAT_KIDS                    "cat_kids"
#define EPG_PROGRAM_CAT_KIDS_UC                 L"cat_kids"
#define EPG_PROGRAM_CAT_MOVIE                   "cat_movie"
#define EPG_PROGRAM_CAT_MOVIE_UC                L"cat_movie"
#define EPG_PROGRAM_CAT_MUSIC                   "cat_music"
#define EPG_PROGRAM_CAT_MUSIC_UC                L"cat_music"
#define EPG_PROGRAM_CAT_NEWS                    "cat_news"
#define EPG_PROGRAM_CAT_NEWS_UC                 L"cat_news"
#define EPG_PROGRAM_CAT_REALITY                 "cat_reality"
#define EPG_PROGRAM_CAT_REALITY_UC              L"cat_reality"
#define EPG_PROGRAM_CAT_ROMANCE                 "cat_romance"
#define EPG_PROGRAM_CAT_ROMANCE_UC              L"cat_romance"
#define EPG_PROGRAM_CAT_SCIFI                   "cat_scifi"
#define EPG_PROGRAM_CAT_SCIFI_UC                L"cat_scifi"
#define EPG_PROGRAM_CAT_SERIAL                  "cat_serial"
#define EPG_PROGRAM_CAT_SERIAL_UC               L"cat_serial"
#define EPG_PROGRAM_CAT_SOAP                    "cat_soap"
#define EPG_PROGRAM_CAT_SOAP_UC                 L"cat_soap"
#define EPG_PROGRAM_CAT_SPECIAL                 "cat_special"
#define EPG_PROGRAM_CAT_SPECIAL_UC              L"cat_special"
#define EPG_PROGRAM_CAT_SPORTS                  "cat_sports"
#define EPG_PROGRAM_CAT_SPORTS_UC               L"cat_sports"
#define EPG_PROGRAM_CAT_THRILLER                "cat_thriller"
#define EPG_PROGRAM_CAT_THRILLER_UC             L"cat_thriller"
#define EPG_PROGRAM_CAT_ADULT                   "cat_adult"
#define EPG_PROGRAM_CAT_ADULT_UC                L"cat_adult"

#define LANGUAGE_TOCKEN                         "language"
#define LANGUAGE_NAME                           "name"
#define LANGUAGE_ID                             "id"
#define LANGUAGE_ITEM_VALUE                     "value"

#define STOP_STREAM_FREQUENCY					0

#define SERVER_STOP_TIMEOUT                     30 //s
#define SERVER_START_TIMEOUT                    30 //s

#define CHECK_NETWORK_DELAY_TIMEOUT             100 //ms

#define DEFAULT_LANGUAGE_FILE_NAME              L"en.xml"

#define TIME_ZONE_DB_FILE_NAME                  L"date_time_zonespec.csv"

#define NETWORK_CHECK_TIMEOUT_DEFAULT_VAL       15          //s

#define DEFAULT_MAX_TIMESHIFT_FILE_SIZE         4000000000ULL

} //dvblink

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_COMMON_H_
