/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_types.h>
#include <dli_base.h>

namespace dvblink { namespace messaging {

// {8345C40E-BFF2-4c8b-A358-095FE522193F}
static const i_guid client_message_queue_interface =
{ 0x8345c40e, 0xbff2, 0x4c8b, { 0xa3, 0x58, 0x9, 0x5f, 0xe5, 0x22, 0x19, 0x3f } };

// {840CA327-052B-413f-BB35-AF5A1A5E04E2}
static const i_guid server_messaging_core_interface = 
{ 0x840ca327, 0x52b, 0x413f, { 0xbb, 0x35, 0xaf, 0x5a, 0x1a, 0x5e, 0x4, 0xe2 } };


enum message_error
{
    success,
    error,
    timeout,
    buffer_size_is_not_enough,
    addressee_not_found,
    queue_shutdown,
    handler_not_registered
};

static message_addressee_t broadcast_addressee("AAAF14BB-2976-4e03-BFE1-B8CAD9C65911");

struct i_server_messaging_core;
typedef boost::shared_ptr<i_server_messaging_core> i_server_messaging_core_t;

/*!	
    Message queue interface. Must be implemented in the module (server/source/sink/etc) where message loop is.
*/
struct i_client_message_queue : public i_base_object
{
    enum type {post, send};

    virtual void __stdcall init(const i_server_messaging_core_t& msg_core) = 0;
    virtual void __stdcall shutdown() = 0;

    virtual void __stdcall set_id(const message_queue_id_t& id) = 0;

    /*
        message dispatcher calls this method to put message in the queue
    */
    virtual void __stdcall put_message(type message_type, const message_id_t& id, const message_sender_t& from,
        const char* message_type_name, const void* message_body, size_t message_body_size) = 0;

    virtual void __stdcall put_response(const message_id_t& id, message_error error, const message_sender_t& from, const void* message_body, size_t message_body_size) = 0;
};

typedef boost::shared_ptr<i_client_message_queue> i_client_message_queue_t;

/*!	
    Message core interface - dispatches messages between queues. Must be implemented in the server
*/
struct i_server_messaging_core : public i_base_object
{
    virtual void __stdcall start() = 0;
    virtual void __stdcall shutdown() = 0;

    virtual void __stdcall register_queue(const i_client_message_queue_t& queue) = 0;
    virtual void __stdcall unregister_queue(const message_queue_id_t& id) = 0;

    virtual message_error __stdcall post(const message_addressee_t& to,
        const message_sender_t& from, const char* message_type_name, const void* message_body, size_t message_body_size) = 0;
    virtual message_error __stdcall send(const message_id_t id, const message_addressee_t& to,
        const message_sender_t& from, const char* message_type_name, const void* message_body, size_t message_body_size) = 0;
    virtual void __stdcall response(const message_id_t id, message_error error,
        const message_addressee_t& to, const message_sender_t& from, const void* message_body, size_t message_body_size) = 0;
};

} //messaging
} //dvblink
