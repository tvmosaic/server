/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>
#include <Dshow.h>

namespace dvblink { namespace engine {

IBaseFilter* get_preferred_filter(const IID& clsid, const wchar_t* device_path, std::wstring& name);
IBaseFilter* get_filter_by_name(const IID& clsid, const wchar_t* name_to_find, std::wstring& name);
IPin* get_pin(IBaseFilter * pFilter, PIN_DIRECTION dir, const GUID* major_type, const GUID* sub_type);
IPin* get_pin_byidx(IBaseFilter * pFilter, PIN_DIRECTION dir, long idx);
bool ConnectUpFilterToPin(IGraphBuilder *pGraph, IBaseFilter* pFilterUpstream, IPin* pPin);
bool ConnectDownFilterToPin(IGraphBuilder *pGraph, IPin* pPin, IBaseFilter* pFilterDownstream);
bool ConnectFilters(IGraphBuilder *pGraph, IBaseFilter* pFilterUpstream, IBaseFilter* pFilterDownstream);
bool start_ds_graph(IMediaControl* pmediactrl, unsigned long timeout);
bool stop_ds_graph(IMediaControl* pmediactrl, unsigned long timeout);
bool pause_ds_graph(IMediaControl* pmediactrl, unsigned long timeout);
void _DeleteMediaType(AM_MEDIA_TYPE *pmt);
void _FreeMediaType(AM_MEDIA_TYPE& mt);
bool AddGraphToRot( IUnknown *pUnkGraph, unsigned long *pdwRegister );
void RemoveGraphFromRot( unsigned long pdwRegister );
bool OpenPropertyPage( IBaseFilter *pFilter, HWND AppHandle );
void get_filter_name(GUID *clsid, wchar_t* wsCLSID);
HRESULT get_filter_property_set(IBaseFilter *pfilter, IKsPropertySet **pkspropertyset);
HRESULT get_pin_property_set(IBaseFilter *pfilter, int sdir, IKsPropertySet **pkspropertyset);

} //engine
} //dvblink
