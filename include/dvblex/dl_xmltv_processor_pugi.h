/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>
#include <map>
#include <vector>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <dl_epgevent.h>
#include <dl_epg_channels.h>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_filesystem_path.h>
#include <dl_strptime.h>
#include <dl_pugixml_helper.h>

#ifndef _WIN32
#define strtok_s strtok_r
#endif

namespace dvblink { namespace engine {

typedef std::vector<std::string> xmltv_keyword_map_t;

struct xmltv_category_keywords_t
{
    void reset()
    {
        m_Action.clear();
        m_Comedy.clear();
        m_Documentary.clear();
        m_Drama.clear();
        m_Educational.clear();
        m_Horror.clear();
        m_Kids.clear();
        m_Movie.clear();
        m_Music.clear();
        m_News.clear();
        m_Reality.clear();
        m_Romance.clear();
        m_ScienceFiction.clear();
        m_Serial.clear();
        m_Soap.clear();
        m_Special.clear();
        m_Sports.clear();
        m_Thriller.clear();
        m_Adult.clear();
    }

    xmltv_keyword_map_t m_Action;
    xmltv_keyword_map_t m_Comedy;
    xmltv_keyword_map_t m_Documentary;
    xmltv_keyword_map_t m_Drama;
    xmltv_keyword_map_t m_Educational;
    xmltv_keyword_map_t m_Horror;
    xmltv_keyword_map_t m_Kids;
    xmltv_keyword_map_t m_Movie;
    xmltv_keyword_map_t m_Music;
    xmltv_keyword_map_t m_News;
    xmltv_keyword_map_t m_Reality;
    xmltv_keyword_map_t m_Romance;
    xmltv_keyword_map_t m_ScienceFiction;
    xmltv_keyword_map_t m_Serial;
    xmltv_keyword_map_t m_Soap;
    xmltv_keyword_map_t m_Special;
    xmltv_keyword_map_t m_Sports;
    xmltv_keyword_map_t m_Thriller;
    xmltv_keyword_map_t m_Adult;
};

inline void ReadKeywords(pugi::xml_node category_node, xmltv_keyword_map_t& keyword_map)
{
    pugi::xml_node xmlnode = category_node.first_child();
    while (xmlnode != NULL)
    {
        std::string str;
        str = xmlnode.child_value();
        if (str.size() > 0)
        {
            boost::to_lower(str);
            keyword_map.push_back(str);
        }

        xmlnode = xmlnode.next_sibling();
    }
}

inline void ReadCategoryKeywords(const dvblink::filesystem_path_t& mapfile, xmltv_category_keywords_t* keywords_map)
{
    pugi::xml_document pDoc;
    pugi::xml_parse_result res = pDoc.load_file(mapfile.to_string().c_str());
    if (res.status == pugi::status_ok)
	{
		//Get root element
        pugi::xml_node root_elem = pDoc.first_child();
        if (root_elem)
		{
            pugi::xml_node xmlnode = root_elem.first_child();
			while (xmlnode != NULL)
			{
			    if (boost::iequals(xmlnode.name(), "Action"))
                    ReadKeywords(xmlnode, keywords_map->m_Action);
			    if (boost::iequals(xmlnode.name(), "Adult"))
                    ReadKeywords(xmlnode, keywords_map->m_Adult);
			    if (boost::iequals(xmlnode.name(), "Comedy"))
                    ReadKeywords(xmlnode, keywords_map->m_Comedy);
			    if (boost::iequals(xmlnode.name(), "Documentary"))
                    ReadKeywords(xmlnode, keywords_map->m_Documentary);
			    if (boost::iequals(xmlnode.name(), "Drama"))
                    ReadKeywords(xmlnode, keywords_map->m_Drama);
			    if (boost::iequals(xmlnode.name(), "Educational"))
                    ReadKeywords(xmlnode, keywords_map->m_Educational);
			    if (boost::iequals(xmlnode.name(), "Horror"))
                    ReadKeywords(xmlnode, keywords_map->m_Horror);
			    if (boost::iequals(xmlnode.name(), "Kids"))
                    ReadKeywords(xmlnode, keywords_map->m_Kids);
			    if (boost::iequals(xmlnode.name(), "Movie"))
                    ReadKeywords(xmlnode, keywords_map->m_Movie);
			    if (boost::iequals(xmlnode.name(), "Music"))
                    ReadKeywords(xmlnode, keywords_map->m_Music);
			    if (boost::iequals(xmlnode.name(), "News"))
                    ReadKeywords(xmlnode, keywords_map->m_News);
			    if (boost::iequals(xmlnode.name(), "Reality"))
                    ReadKeywords(xmlnode, keywords_map->m_Reality);
			    if (boost::iequals(xmlnode.name(), "Romance"))
                    ReadKeywords(xmlnode, keywords_map->m_Romance);
			    if (boost::iequals(xmlnode.name(), "ScienceFiction"))
                    ReadKeywords(xmlnode, keywords_map->m_ScienceFiction);
			    if (boost::iequals(xmlnode.name(), "Serial"))
                    ReadKeywords(xmlnode, keywords_map->m_Serial);
			    if (boost::iequals(xmlnode.name(), "Soap"))
                    ReadKeywords(xmlnode, keywords_map->m_Soap);
			    if (boost::iequals(xmlnode.name(), "Special"))
                    ReadKeywords(xmlnode, keywords_map->m_Special);
			    if (boost::iequals(xmlnode.name(), "Sports"))
                    ReadKeywords(xmlnode, keywords_map->m_Sports);
			    if (boost::iequals(xmlnode.name(), "Thriller"))
                    ReadKeywords(xmlnode, keywords_map->m_Thriller);

                xmlnode = xmlnode.next_sibling();
			}
		}
        pDoc.reset();
    }
}

const std::string XMLTV_ALL_CHANNEL_ID = "6AAF3CCD-0EB8-4346-9D87-799217A283EA";

struct xmltv_channel_info
{
	xmltv_channel_info(){offset = 0;bAdult=false;bHdtv=false;};
	std::string id;
	int offset;
	bool bAdult;
	bool bHdtv;
};

typedef std::map<std::string, xmltv_channel_info> xmltv_channel_info_map_t;

inline void AppendNodeContents(pugi::xml_node xmlnode, const char* tagname, std::string& contents)
{
    if (boost::iequals(xmlnode.name(), tagname))
    {
        std::string str;
        str = xmlnode.child_value();
        if (str.size() > 0)
        {
            if (contents.size() > 0)
                contents += "/";
                
            contents += str;
        }
    }
}

inline void ParseProgramCredits(pugi::xml_node programnode, dvblink::engine::DLEPGEvent& epg_event)
{
    //find Credits node
    pugi::xml_node credits_node = programnode.child("credits");
    if (credits_node != NULL)
    {
        //walk through all subnodes
        pugi::xml_node xmlnode = credits_node.first_child();
        while (xmlnode != NULL)
        {
            AppendNodeContents(xmlnode, "director", epg_event.m_Directors);
            AppendNodeContents(xmlnode, "actor", epg_event.m_Actors);
            AppendNodeContents(xmlnode, "writer", epg_event.m_Writers);
            AppendNodeContents(xmlnode, "producer", epg_event.m_Producers);
            AppendNodeContents(xmlnode, "guest", epg_event.m_Guests);
            
            xmlnode = xmlnode.next_sibling();
        }
    }
}

inline bool CheckKeywords(const char* string_to_check, xmltv_keyword_map_t& keywords)
{
    bool ret_val = false;

    for (unsigned int i=0; i<keywords.size(); i++)
    {
        if (strstr(string_to_check, keywords[i].c_str()) != NULL)
        {
            ret_val = true;
            break;
        }
    }

    return ret_val;
}

inline void ParseProgramCategories(pugi::xml_node programnode, xmltv_category_keywords_t* categoryKeywords,
                                   dvblink::engine::DLEPGEvent& epg_event)
{
    //Reset all categories for this event
    epg_event.m_IsAction = false;
    epg_event.m_IsComedy = false;
    epg_event.m_IsDocumentary = false;
    epg_event.m_IsDrama = false;
    epg_event.m_IsEducational = false;
    epg_event.m_IsHorror = false;
    epg_event.m_IsKids = false;
    epg_event.m_IsMovie = false;
    epg_event.m_IsMusic = false;
    epg_event.m_IsNews = false;
    epg_event.m_IsReality = false;
    epg_event.m_IsRomance = false;
    epg_event.m_IsScienceFiction = false;
    epg_event.m_IsSerial = false;
    epg_event.m_IsSoap = false;
    epg_event.m_IsSpecial = false;
    epg_event.m_IsSports = false;
    epg_event.m_IsThriller = false;
    epg_event.m_IsAdult = false;
    //walk through all subnodes
    pugi::xml_node xmlnode = programnode.first_child();
    while (xmlnode != NULL)
    {
        if (boost::iequals(xmlnode.name(), "category"))
        {
            std::string s = xmlnode.child_value();
            if (s.size() > 0)
            {
                char* str = strdup(s.c_str());
                //split string into tokens 
                char* next_token = NULL;
                char* token = strtok_s( str, "/", &next_token);
                while (token != NULL)
                {
                    //Add category to the keyword list
                    if (epg_event.m_Categories.size() == 0)
                    {
                        epg_event.m_Categories = token;
                    }
                    else
                    {
                        epg_event.m_Categories += "/";
                        epg_event.m_Categories += token;
                    }
                    //Set category flags
                    std::string token_str = token;
                    boost::to_lower(token_str);
                    epg_event.m_IsAction |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Action);
                    epg_event.m_IsAdult |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Adult);
                    epg_event.m_IsComedy |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Comedy);
                    epg_event.m_IsDocumentary |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Documentary);
                    epg_event.m_IsDrama |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Drama);
                    epg_event.m_IsEducational |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Educational);
                    epg_event.m_IsHorror |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Horror);
                    epg_event.m_IsKids |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Kids);
                    epg_event.m_IsMovie |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Movie);
                    epg_event.m_IsMusic |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Music);
                    epg_event.m_IsNews |= CheckKeywords(token_str.c_str(), categoryKeywords->m_News);
                    epg_event.m_IsReality |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Reality);
                    epg_event.m_IsRomance |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Romance);
                    epg_event.m_IsScienceFiction |= CheckKeywords(token_str.c_str(), categoryKeywords->m_ScienceFiction);
                    epg_event.m_IsSerial |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Serial);
                    epg_event.m_IsSoap |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Soap);
                    epg_event.m_IsSpecial |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Special);
                    epg_event.m_IsSports |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Sports);
                    epg_event.m_IsThriller |= CheckKeywords(token_str.c_str(), categoryKeywords->m_Thriller);

                    token = strtok_s( NULL, "/", &next_token);
                }
                free(str);
            }
        }
	    xmlnode = xmlnode.next_sibling();
    }
}

inline void ParseEpisodeNum(pugi::xml_node programnode, dvblink::engine::DLEPGEvent& epg_event)
{
    epg_event.m_EpisodeNum = -1;
    epg_event.m_SeasonNum = -1;
    //search for episode-num tag
    pugi::xml_node episode_node = programnode.first_child();
    while (episode_node != NULL)
    {
        if (boost::iequals(episode_node.name(), "episode-num"))
        {
            //check episode coding system
            std::string coding_name;
            if (dvblink::pugixml_helpers::get_node_attribute(episode_node, "system", coding_name) &&
                boost::iequals(coding_name, "xmltv_ns"))
            {
                std::string str = episode_node.child_value();
                if (str.size() > 0)
                {
                    //remove spaces
                    boost::replace_all(str, " ", "");

                    typedef boost::tokenizer<boost::char_separator<char>, std::string::const_iterator, std::string> tokenizer_t;
                    boost::char_separator<char> sep(".", 0, boost::keep_empty_tokens);
                    tokenizer_t tokens(str, sep);

                    int token_count = 0;
                    for (tokenizer_t::iterator tok_iter = tokens.begin(); tok_iter != tokens.end(); ++tok_iter)
                    {
                        switch (token_count)
                        {
                        case 0:
                            //season num
                            {
                                //check whether it has just a number or x/y format
                                long l1, l2;
                                if (sscanf((*tok_iter).c_str(), "%ld/%ld", &l1, &l2) == 2)
                                    epg_event.m_SeasonNum = l1;
                                else
                                    if (tok_iter->size() > 0)
                                        dvblink::engine::string_conv::apply<long, long>((*tok_iter).c_str(), epg_event.m_SeasonNum, -1);
                            }
                            break;
                        case 1:
                            //episode num
                            {
                                //check whether it has just a number or x/y format
                                long l1, l2;
                                if (sscanf((*tok_iter).c_str(), "%ld/%ld", &l1, &l2) == 2)
                                    epg_event.m_EpisodeNum = l1;
                                else
                                    if (tok_iter->size() > 0)
                                        dvblink::engine::string_conv::apply<long, long>((*tok_iter).c_str(), epg_event.m_EpisodeNum, -1);
                            }
                            break;
                        case 2:
                            //episode part
                            break;
                        }
                        ++token_count;
                    }

                    //found correct episode info with correct coding. we can stop now.
                    break;
                }
            }
        }
	    episode_node = episode_node.next_sibling();
    }
    if (epg_event.m_EpisodeNum != -1)
        epg_event.m_EpisodeNum += 1;
    if (epg_event.m_SeasonNum != -1)
        epg_event.m_SeasonNum += 1;
}

inline void ParseStarRating(pugi::xml_node programnode, dvblink::engine::DLEPGEvent& epg_event)
{
    epg_event.m_StarNum = -1;
    epg_event.m_StarNumMax = -1;
    //search for star rating tag
    pugi::xml_node star_node = programnode.child("star-rating");
    if (star_node != NULL)
    {
        std::string rating_str;
        if (dvblink::pugixml_helpers::get_node_value(star_node, "value", rating_str))
        {
            long l1, l2;
            if (sscanf(rating_str.c_str(), "%ld/%ld", &l1, &l2) == 2)
            {
                epg_event.m_StarNum = l1;
                epg_event.m_StarNumMax = l2;
            }
        }
    }
}

inline bool IsHDTVProgram(pugi::xml_node programnode)
{
    bool ret_val = false;
    //search for video description tag
    pugi::xml_node video_node = programnode.child("video");
    if (video_node != NULL)
    {
        std::string quality_str;
        if (dvblink::pugixml_helpers::get_node_value(video_node, "quality", quality_str))
        {
            ret_val = boost::iequals(quality_str.c_str(), "hdtv");
        }
    }
    return ret_val;
}

inline xmltv_channel_info* GetChannelInfo(const std::string& id, xmltv_channel_info_map_t* chinfo_map)
{
	xmltv_channel_info* ret_val = NULL;

    if (chinfo_map != NULL)
    {
	    if (chinfo_map->find(id) != chinfo_map->end())
        {
		    ret_val = &(chinfo_map->find(id)->second);
        }
	    else //else check for "all channels" settings
	    if (chinfo_map->find(XMLTV_ALL_CHANNEL_ID.c_str()) != chinfo_map->end())
        {
		    ret_val = &(chinfo_map->find(XMLTV_ALL_CHANNEL_ID.c_str())->second);
        }
    }

	return ret_val;
}

inline int get_year_from_date_tag(const char* tag)
{
    int ret_val = -1;

	struct tm dt_tm;
	char* conv_res = NULL;
	//YYYYMMDD +-z
	conv_res = dl_strptime(tag, "%Y%m%d%z", &dt_tm);

	//YYYYMMDD
	if (conv_res == NULL)
    	conv_res = dl_strptime(tag, "%Y%m%d", &dt_tm);

	//YYYYMM +-z
	if (conv_res == NULL)
    	conv_res = dl_strptime(tag, "%Y%m%z", &dt_tm);

	//YYYYMM
	if (conv_res == NULL)
    	conv_res = dl_strptime(tag, "%Y%m", &dt_tm);

	//YYYY +-z
	if (conv_res == NULL)
    	conv_res = dl_strptime(tag, "%Y%z", &dt_tm);

	//YYYY
	if (conv_res == NULL)
    	conv_res = dl_strptime(tag, "%Y", &dt_tm);

	if (conv_res != NULL)
        ret_val = dt_tm.tm_year + 1900;

    return ret_val;
}

inline void process_xmltv_doc(pugi::xml_document& pDoc, xmltv_category_keywords_t& keywords_map, xmltv_channel_info_map_t* chinfo_map,
                              dvblex::epg_source_channel_map_t& epg_channels_map, map_epg_channel_to_epg_t& epg_events_map,
                              bool* read_exit_flag)
{
    if (pDoc)
	{
		//Get root element
        pugi::xml_node root_elem = pDoc.first_child();
        if (root_elem)
		{
		    pugi::xml_node xmlnode = root_elem.first_child();
		    while (!(*read_exit_flag) && xmlnode != NULL)
			{
                if (boost::iequals(xmlnode.name(), "channel"))
                {
                    std::string channel_id;
                    std::string str;
                    dvblex::epg_source_channel_t epg_channel;
                    if (dvblink::pugixml_helpers::get_node_attribute(xmlnode, "id", channel_id) &&
                        dvblink::pugixml_helpers::get_node_value(xmlnode, "display-name", str))
                    {
                        //make id lower case
                        boost::to_lower(channel_id);

                        epg_channel.name_ = str;
                        epg_channel.id_ = channel_id;

                        //get channel logo if available
                        pugi::xml_node icon_node = xmlnode.child("icon");
                        if (icon_node != NULL)
                            if (dvblink::pugixml_helpers::get_node_attribute(icon_node, "src", str) && boost::istarts_with(str, "http"))
                                epg_channel.logo_ = str;

                        //add channel to the map
                        epg_channels_map[channel_id] = epg_channel;
                    }
                }
                if (boost::iequals(xmlnode.name(), "programme"))
			    {
				    dvblink::engine::DLEPGEvent epg_event;
				    //channel id
				    std::string channel_id;
				    dvblink::pugixml_helpers::get_node_attribute(xmlnode, "channel", channel_id);
				    //make channel id lower case
				    boost::to_lower(channel_id);
					//find channel info for this channel
					xmltv_channel_info* chinfo = GetChannelInfo(channel_id, chinfo_map);
				    //name
				    dvblink::pugixml_helpers::get_node_value(xmlnode, "title", epg_event.m_Name);
                    //Sub-name
                    dvblink::pugixml_helpers::get_node_value(xmlnode, "sub-title", epg_event.m_SecondName);
				    //description
				    dvblink::pugixml_helpers::get_node_value(xmlnode, "desc", epg_event.m_ShortDesc);
                    //language
                    dvblink::pugixml_helpers::get_node_value(xmlnode, "language", epg_event.m_Language);
                    //image
                    pugi::xml_node icon_node = xmlnode.child("icon");
                    if (icon_node != NULL)
                        dvblink::pugixml_helpers::get_node_attribute(icon_node, "src", epg_event.m_ImageURL);
                    //Year
                    std::string tempstr;
                    epg_event.m_Year = 0;
                    if (dvblink::pugixml_helpers::get_node_value(xmlnode, "date", tempstr))
                    {
                        epg_event.m_Year = get_year_from_date_tag(tempstr.c_str());
                        if (epg_event.m_Year < 0)
                            epg_event.m_Year = 0;
                    }
                    //Credits
                    ParseProgramCredits(xmlnode, epg_event);
                    //Categories
                    ParseProgramCategories(xmlnode, &keywords_map, epg_event);
                    //Episode and season number
                    ParseEpisodeNum(xmlnode, epg_event);
                    //star rating
                    ParseStarRating(xmlnode, epg_event);
                    //HDTV flag
                    epg_event.m_IsHDTV = IsHDTVProgram(xmlnode);
                    //premiere
                    epg_event.m_IsPremiere = false;
                    if (xmlnode.child("premiere") != NULL)
                        epg_event.m_IsPremiere = true;
                    //repeat flag
                    epg_event.m_IsRepeatFlag = false;
                    if (xmlnode.child("previously-shown") != NULL)
                        epg_event.m_IsRepeatFlag = true;
				    //start time/date
				    std::string start_td;
				    dvblink::pugixml_helpers::get_node_attribute(xmlnode, "start", start_td);
                    time_t start_time = dvblink::engine::GetDateTimeFromString(start_td.c_str());
				    if (start_time != -1)
				    {
					    epg_event.m_StartTime = start_time;
					    epg_event.m_Duration = 0;
					    //end time/date
					    std::string end_td;
					    dvblink::pugixml_helpers::get_node_attribute(xmlnode, "stop", end_td);
					    time_t end_time = dvblink::engine::GetDateTimeFromString(end_td.c_str());
					    if (end_time != -1)
					    {
						    epg_event.m_Duration = end_time - start_time;
						    if (epg_event.m_Duration <= 0)
						    {
							    epg_event.m_Duration = 0;
                                dvblink::logging::log_ext_info(L"process_xmltv_doc. Program %s has invalid duration %d (%s until %s). Program start time will be used for correction") % epg_event.m_Name.c_str() % epg_event.m_Duration % start_td.c_str() % end_td.c_str();
						    }
					    }
					    //check if this channel already exists in the map
					    if (epg_events_map.find(channel_id) == epg_events_map.end())
					    {
						    //new channel
						    dvblink::engine::DLEPGEventList ev_list;
						    epg_events_map[channel_id] = ev_list;
					    }
						//if channel info not NULL - adjust event parameters
						if (chinfo != NULL)
						{
							epg_event.m_StartTime += chinfo->offset*60;
							if (chinfo->bHdtv)
								epg_event.m_IsHDTV = true;
							if (chinfo->bAdult)
								epg_event.m_IsAdult = true;
						}
					    epg_events_map[channel_id].push_back(epg_event);
				    } else
				    {
					    dvblink::logging::log_error(L"process_xmltv_doc. Start time %s of a program %s has invalid format") % start_td.c_str() % epg_event.m_Name.c_str();
				    }
			    }
				xmlnode = xmlnode.next_sibling();
			}
		}
	}
}

} //engine
} //dvblink

