/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_CIRCLE_BUFFER_H_
#define __DVBLINK_DL_CIRCLE_BUFFER_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <stdio.h>
#include <string>
#include <list>
#include <boost/thread/mutex.hpp>
#include <boost/filesystem/fstream.hpp>
#include <dl_event.h>
#include <dl_ts.h>

namespace dvblink { namespace engine {

class circular_node
{
public:
    circular_node(size_t node_size);

    void reset() {written_length_ = 0; read_offset_ = 0;}

    // size of the buffer_out MUST be not less than node_size_!!!
    size_t read_data(unsigned char* buffer_out, size_t buffer_size);
    size_t write_data(const unsigned char* buffer_in, size_t length);

    const unsigned char* data() const {return &data_[0];}
    size_t written_size() const {return written_length_;}
    size_t size_to_read(){return written_length_ - read_offset_;}

private:
    std::vector<unsigned char> data_;
    size_t node_size_;
    size_t written_length_;
    size_t read_offset_;
};

class ts_circle_buffer
{
public:
    typedef circular_node* node_t;

public:
    ts_circle_buffer(size_t node_num, size_t node_size, const wchar_t* context, const boost::filesystem::path* data_file = NULL);
    ~ts_circle_buffer();

    void set_expansion_props(size_t max_node_num, size_t expansion_block_node_num);

    void reset();

    node_t tear_node(long timeout_ms);
    void put_node(node_t node);

    size_t read_stream(long timeout_ms, size_t expected_data, unsigned char* buffer, size_t buffer_size);
    size_t write_stream(const unsigned char* stream, size_t len);

    size_t get_node_size() const
    {
        return node_size_;
    }

    size_t get_free_nodes() const
    {
        boost::mutex::scoped_lock lock(lock_);
        return free_queue_.size();
    }

private:
    size_t node_size_;
    std::list<node_t> busy_queue_;
    std::list<node_t> free_queue_;
    boost::filesystem::ofstream* stream_file_;
    bool is_data_available(size_t size);

    mutable boost::mutex lock_;
    event wait_read_;
    size_t data_trigger_amount_;
    int write_miss_count_;
    std::wstring context_;
    //expansion props
    size_t node_num_;
    size_t max_node_num_;
    size_t expansion_block_node_num_;
};

} //engine
} //dvblink

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_CIRCLE_BUFFER_H_
