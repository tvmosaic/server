/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <map>
#include <vector>
#include <dl_types.h>

namespace dvblink { namespace ncanto {

enum re_type_id
{
    response_code_id = 10000,

    store_id,
    asset_id,
    source_id,
    source_name,
    source_logo_url_id,
    source_group_id,
    program_id,
    series_id,
    subscriber_id,
    entitlement_id,
    context_id,
    profile_id,
    profile_ref_id,
    list_id,
    lang_code_id,
    title_id,
    program_type_id,
    asset_type_id,
    genre_id,
    predicate_id,
    contribution_id,
    engine_name_id,
    rating_id,
    score_id,
    enable_id,
    recording_suggestion_id,
    image_url_id,
    person_function_id,
    person_canonical_id,
    group_id,
    profile_name_id,
    provider_id,
    provider_name_id,
    object_id,
    product_id,
    dvblink_channel_id,
    program_name_id,
    annotation_id,
    attribute_id,
    synopsis_id,
    seed_ref_id,
    asset_ratings_id,
};

typedef base_type_string_t<store_id> store_id_t;
typedef base_type_string_t<asset_id> asset_id_t;
typedef base_type_string_t<source_id> source_id_t;
typedef base_type_string_t<source_name> source_name_t;
typedef base_type_string_t<source_logo_url_id> source_logo_url_t;
typedef base_type_string_t<source_group_id> source_group_id_t;
typedef base_type_string_t<program_id> program_id_t;
typedef base_type_string_t<series_id> series_id_t;
typedef base_type_string_t<subscriber_id> subscriber_id_t;
typedef base_type_string_t<entitlement_id> entitlement_id_t;
typedef base_type_string_t<profile_id> profile_id_t;
typedef base_type_string_t<profile_ref_id> profile_ref_t;
typedef base_type_string_t<list_id> list_id_t;
typedef base_type_string_t<lang_code_id> lang_code_t;
typedef base_type_string_t<title_id> title_name_t;
typedef base_type_string_t<program_type_id> program_type_t;
typedef base_type_string_t<asset_type_id> asset_type_t;
typedef base_type_string_t<genre_id> genre_type_t;
typedef base_type_string_t<context_id> context_id_t;
typedef base_type_string_t<predicate_id> predicate_t;
typedef base_type_string_t<engine_name_id> engine_name_t;
typedef base_type_string_t<image_url_id> image_url_t;
typedef base_type_string_t<person_function_id> person_function_t;
typedef base_type_string_t<person_canonical_id> person_canonical_t;
typedef base_type_string_t<group_id> group_id_t;
typedef base_type_string_t<profile_name_id> profile_name_t;
typedef base_type_string_t<provider_name_id> provider_name_t;
typedef base_type_string_t<provider_id> provider_id_t;
typedef base_type_string_t<object_id> object_id_t;
typedef base_type_string_t<product_id> product_id_t;
typedef base_type_string_t<annotation_id> annotation_id_t;
typedef base_type_string_t<attribute_id> attribute_name_t;
typedef base_type_string_t<synopsis_id> synopsis_t;
typedef base_type_string_t<seed_ref_id> seed_ref_t;
typedef base_type_string_t<asset_ratings_id> asset_ratings_id_t;

typedef channel_id_t dvblink_channel_id_t;
typedef base_type_t<int, contribution_id> contribution_t;
typedef base_type_t<float, rating_id> rating_t;
typedef base_type_t<float, score_id> score_t;
typedef base_type_t<int, enable_id> state_flag_t;
typedef base_type_t<bool, recording_suggestion_id> recording_suggestion_t;

typedef std::vector<entitlement_id_t> entitlement_ids_t;
typedef std::vector<profile_id_t> profile_ids_t;
typedef std::vector<profile_name_t> profiles_name_t;
typedef std::vector<list_id_t> list_ids_t;
typedef std::vector<source_id_t> source_ids_t;
typedef std::vector<asset_id_t> asset_ids_t;
typedef std::vector<subscriber_id_t> subscriber_ids_t;
typedef std::vector<provider_id_t> provider_ids_t;

typedef std::map<source_id_t, source_name_t> map_source_id_to_name_t;
typedef std::map<provider_id_t, provider_name_t> map_provider_id_name_t;
typedef std::map<dvblink_channel_id_t, source_id_t> map_dvblink_channel_ncanto_channel_t;
typedef std::map<provider_id_t, map_dvblink_channel_ncanto_channel_t> map_provider_to_map_dvblink_ch_ncanto_ch_t;
typedef std::map<subscriber_id_t, source_ids_t> map_subscriber_to_sources_t;

} //ncanto
} //dvblink
