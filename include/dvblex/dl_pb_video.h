/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <dl_pb_item.h>
#include <dl_pb_video_info.h>

namespace dvblex { namespace playback {

struct pb_video_t : public pb_item_t
{
    pb_video_t() :
      pb_item_t(pbit_item_video) { }
      
    pb_video_info_t video_info_;
};

const std::string video_root_node                    = "video";

inline void write_to_node(pugi::xml_node& node, const pb_video_t& video)
{
    pugi::xml_node v_node = dvblink::pugixml_helpers::new_child(node, video_root_node);
    if (v_node != NULL)
    {
        write_to_node(v_node, (pb_item_t)video);
        write_to_node(v_node, video.video_info_);
    }
}

inline void read_from_node(pugi::xml_node& node, pb_video_t& video)
{
    read_from_node(node, (pb_item_t&)video);
    read_from_node(node, video.video_info_);
}

} // playback
} // dvblex

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::playback::pb_video_t& pv, const unsigned int /*version*/)
{
    ar & pv.object_id_;
    ar & pv.parent_id_;
    ar & pv.url_;
    ar & pv.thumbnail_;
    ar & pv.item_type_;
    ar & pv.can_be_deleted_;
    ar & pv.size_;
    ar & pv.creation_time_;
    ar & pv.video_info_;
}

} // namespace serialization
} // namespace boost
