/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_TS_H_
#define __DVBLINK_DL_TS_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

namespace dvblink { namespace engine {

typedef unsigned char u_char;
const u_char SYNC_BYTE                              = 0x47;
const unsigned short INVALID_PID                    = 0xffff;

const unsigned short TS_PACKET_SIZE = 188;

/* Packet Identifier */
const unsigned short PID_PAT                                = 0x00;	    /* Program Association Table */
const unsigned short PID_CAT                                = 0x01;	    /* Conditional Access Table */
const unsigned short PID_NIT                                = 0x10;	    /* Network Information Table */
const unsigned short PID_BAT                                = 0x11;	    /* Bouquet Association Table */
const unsigned short PID_SDT                                = 0x11;	    /* Service Description Table */
const unsigned short PID_EIT                                = 0x12;	    /* Event Information Table */
const unsigned short PID_RST                                = 0x13;	    /* Running Status Table */
const unsigned short PID_TDT                                = 0x14;	    /* Time Date Table */
const unsigned short PID_TOT                                = 0x14;	    /* Time Offset Table */
const unsigned short PID_ST                                 = 0x14;	    /* Stuffing Table */
const unsigned short PID_ATSC_BASE                          = 0x1FFB;	/* ATSC Base PID*/
const unsigned short PID_CLEARQAM_BASE                      = 0x1FFC;	/* ClearQAM Base PID*/
/* 0x15 - 0x1F */	                                /* Reserved for future use */

/* Table Identifier */
const u_char TID_PAT                                = 0x00;	    /* Program Association Section */
const u_char TID_CAT                                = 0x01;	    /* Conditional Access Section */
const u_char TID_PMT                                = 0x02;	    /* Program Map Table */
/* 0x03 - 0x3F */	                                /* Reserved for future use */
const u_char TID_NIT_ACT                            = 0x40;	    /* Network Information Section - actual */
const u_char TID_NIT_OTH                            = 0x41;	    /* Network Information Section - other */
const u_char TID_SDT_ACT                            = 0x42;	    /* Service Description Section - actual */
const u_char TID_SDT_OTH                            = 0x46;	    /* Service Description Section - other */
const u_char TID_BAT                                = 0x4A;
const u_char TID_EIT_ACT                            = 0x4E;	    /* Event Information Section - actual */
const u_char TID_EIT_OTH                            = 0x4F;	    /* Event Information Section - other */
const u_char TID_EIT_ACT_SCH                        = 0x50;	    /* Event Information Section - actual, schedule  */
const u_char TID_EIT_OTH_SCH                        = 0x60;	    /* Event Information Section - other, schedule */
const u_char TID_TDT                                = 0x70;	    /* Time Date Section */
const u_char TID_TOT                                = 0x73;	    /* Time Offset Section */
const u_char TID_CA_ECM_0                           = 0x80;
const u_char TID_CA_ECM_1                           = 0x81;
const u_char TID_ATSC_MGT                           = 0xC7;
const u_char TID_ATSC_VCT                           = 0xC8;
const u_char TID_CLEARQAM_LVCT                      = 0xC9;
const u_char TID_ATSC_EIT                           = 0xCB;
const u_char TID_ATSC_STT                           = 0xCD;
const u_char TID_ATSC_ETT                           = 0xCC;

const short PCR_PACKET_PID                          = 0x1ffe;
const short NULL_PACKET_PID                         = 0x1fff;
const short INVALID_PACKET_PID                      = 0x7fff;
const short CRC_SIZE_BYTES							= 4;

//Stream types
const u_char STREAM_TYPE_VIDEO_MPEG1                = 0x01; 
const u_char STREAM_TYPE_VIDEO_MPEG2                = 0x02; 
const u_char STREAM_TYPE_AUDIO_MPEG1                = 0x03; 
const u_char STREAM_TYPE_AUDIO_MPEG2                = 0x04; 
//const u_char STREAM_TYPE_PRIVATE_SECTION            = 0x05; 
const u_char STREAM_TYPE_PRIVATE_DATA               = 0x06; 
//const u_char STREAM_TYPE_MHEG                       = 0x07; 
//const u_char STREAM_TYPE_DSMCC_TYPE_A               = 0x0A; 
//const u_char STREAM_TYPE_DSMCC_TYPE_B               = 0x0B; 
//const u_char STREAM_TYPE_DSMCC_TYPE_C               = 0x0C; 
//const u_char STREAM_TYPE_DSMCC_TYPE_D               = 0x0D; 
const u_char STREAM_TYPE_AUDIO_AAC                  = 0x0F; 
const u_char STREAM_TYPE_VIDEO_MPEG4                = 0x10; 
const u_char STREAM_TYPE_VIDEO_H264                 = 0x1B; 
const u_char STREAM_TYPE_AUDIO_AC3                  = 0x81; 
const u_char STREAM_TYPE_AUDIO_DTS                  = 0x8A; 

const u_char USER_PRIVATE_DESCRIPTOR                = 0xFF;

//descriptors
const u_char ISO639_LANG_DESC_TAG					= 0x0A;
const u_char TELETEXT_DESC_TAG					    = 0x56;
const u_char SUBTITLING_DESC_TAG					= 0x59;
const u_char VIDEO_STERAM_DESC_TAG					= 0x02;

struct SDataChunk
{
	int length;
	unsigned char* data;
};

#ifndef _TVM_BIG_ENDIAN
// Little endian

struct STransportHeader
{
    u_char sync_byte                  /*:8*/;

    u_char PID_hi                     :5;
    u_char transport_priority         :1;
    u_char payload_unit_start_indicator :1;
    u_char transport_error_indicator  :1;

    u_char PID_lo                     /*:8*/;

    u_char continuity_counter         :4;
    u_char adaptation_field_control   :2;
    u_char transport_scrambling_control :2;
};

struct STransportAdaptationField
{
    u_char adaptation_field_extension_flag          :1;
    u_char transport_private_data_flag              :1;
    u_char splicing_point_flag                      :1;
    u_char OPCR_flag                                :1;
    u_char PCR_flag                                 :1;
    u_char elementary_stream_priority_indicator     :1;
    u_char random_access_indicator                  :1;
    u_char discontinuity_indicator                  :1;
};

struct STransportPCR
{
    u_char program_clock_reference_base_1           : 8;
    u_char program_clock_reference_base_2		    : 8;
    u_char program_clock_reference_base_3		    : 8;
    u_char program_clock_reference_base_4		    : 8;
    u_char program_clock_reference_extension_hi	    : 1;
    u_char 						                    : 6;
    u_char program_clock_reference_base_5		    : 1;
    u_char program_clock_reference_extension_lo	    : 8;
};

struct SPat
{
    u_char table_id                                 /*:8*/;

    u_char section_length_hi                        :4;
    u_char                                          :2;
    u_char dummy                                    :1;        // has to be 0
    u_char section_syntax_indicator                 :1;
    u_char section_length_lo                        /*:8*/;

    u_char transport_stream_id_hi                   /*:8*/;
    u_char transport_stream_id_lo                   /*:8*/;
    u_char current_next_indicator                   :1;
    u_char version_number                           :5;
    u_char                                          :2;
    u_char section_number                           /*:8*/;
    u_char last_section_number                      /*:8*/;
};

struct SPatProg
{
    u_char program_number_hi                        /*:8*/;
    u_char program_number_lo                        /*:8*/;
    u_char network_pid_hi                           :5;
    u_char                                          :3;
    u_char network_pid_lo                           /*:8*/;
    /* or program_map_pid (if prog_num=0)*/
};

struct SPmt
{
    u_char table_id                   /*:8*/;

    u_char section_length_hi          :4;
    u_char                            :2;
    u_char dummy                      :1; // has to be 0
    u_char section_syntax_indicator   :1;
    u_char section_length_lo          /*:8*/;

    u_char program_number_hi          /*:8*/;
    u_char program_number_lo          /*:8*/;

    u_char current_next_indicator     :1;
    u_char version_number             :5;
    u_char                            :2;
    u_char section_number             /*:8*/;
    u_char last_section_number        /*:8*/;
    u_char PCR_PID_hi                 :5;
    u_char                            :3;
    u_char PCR_PID_lo                 /*:8*/;
    u_char program_info_length_hi     :4;
    u_char                            :4;
    u_char program_info_length_lo     /*:8*/;
};

struct SESHeader
{
    u_char stream_type                /*:8*/;
    u_char elementary_PID_hi          :5;
    u_char                            :3;
    u_char elementary_PID_lo          /*:8*/;
    u_char ES_info_length_hi          :4;
    u_char                            :4;
    u_char ES_info_length_lo          /*:8*/;
};

struct SESInfo
{
	SESHeader es_header;
	SDataChunk descriptors;
};

struct SDescriptorHeader
{
    u_char descriptor_tag             /*:8*/;
    u_char descriptor_length          /*:8*/;
};

typedef struct eit {
    u_char table_id                               /*:8*/;
    u_char section_length_hi                      :4;
    u_char                                        :3;
    u_char section_syntax_indicator               :1;
    u_char section_length_lo                      /*:8*/;
    u_char service_id_hi                          /*:8*/;
    u_char service_id_lo                          /*:8*/;
    u_char current_next_indicator                 :1;
    u_char version_number                         :5;
    u_char                                        :2;
    u_char section_number                         /*:8*/;
    u_char last_section_number                    /*:8*/;
    u_char transport_stream_id_hi                 /*:8*/;
    u_char transport_stream_id_lo                 /*:8*/;
    u_char original_network_id_hi                 /*:8*/;
    u_char original_network_id_lo                 /*:8*/;
    u_char segment_last_section_number            /*:8*/;
    u_char segment_last_table_id                  /*:8*/;
//    u_char data[]; /* struct eit_event */
} eit_t;

typedef struct eit_event {
    u_char event_id_hi                            /*:8*/;
    u_char event_id_lo                            /*:8*/;
    u_char mjd_hi                                 /*:8*/;
    u_char mjd_lo                                 /*:8*/;
    u_char start_time_h                           /*:8*/;
    u_char start_time_m                           /*:8*/;
    u_char start_time_s                           /*:8*/;
    u_char duration_h                             /*:8*/;
    u_char duration_m                             /*:8*/;
    u_char duration_s                             /*:8*/;
    u_char descriptors_loop_length_hi             :4;
    u_char free_ca_mode                           :1;
    u_char running_status                         :3;
    u_char descriptors_loop_length_lo             /*:8*/;
//    u_char data[]; /* struct descr_gen */
} eit_event_t;

///////////////////////////////////////////////////////////////////////////////
#else
// Big endian

struct STransportHeader
{
    u_char sync_byte                  /*:8*/;

    u_char transport_error_indicator  :1;
    u_char payload_unit_start_indicator :1;
    u_char transport_priority         :1;
    u_char PID_hi                     :5;
    
    u_char PID_lo                     /*:8*/;

    u_char transport_scrambling_control :2;
    u_char adaptation_field_control   :2;
    u_char continuity_counter         :4;    
};

struct STransportAdaptationField
{
    u_char discontinuity_indicator                  :1;
    u_char random_access_indicator                  :1;
    u_char elementary_stream_priority_indicator     :1;
    u_char PCR_flag                                 :1;
    u_char OPCR_flag                                :1;
    u_char splicing_point_flag                      :1;
    u_char transport_private_data_flag              :1;
    u_char adaptation_field_extension_flag          :1;
};

struct STransportPCR
{
    u_char program_clock_reference_base_1           : 8;
    u_char program_clock_reference_base_2		    : 8;
    u_char program_clock_reference_base_3		    : 8;
    u_char program_clock_reference_base_4		    : 8;
    
    u_char program_clock_reference_base_5		    : 1;
    u_char 						                    : 6;
    u_char program_clock_reference_extension_hi	    : 1;    
    
    u_char program_clock_reference_extension_lo	    : 8;
};

struct SPat
{
    u_char table_id                                 /*:8*/;

    u_char section_syntax_indicator                 :1;
    u_char dummy                                    :1;        // has to be 0
    u_char                                          :2;
    u_char section_length_hi                        :4;    

    u_char section_length_lo                        /*:8*/;

    u_char transport_stream_id_hi                   /*:8*/;
    u_char transport_stream_id_lo                   /*:8*/;
    
    u_char                                          :2;
    u_char version_number                           :5;
    u_char current_next_indicator                   :1;    
    
    u_char section_number                           /*:8*/;
    u_char last_section_number                      /*:8*/;
};

struct SPatProg
{
    u_char program_number_hi                        /*:8*/;
    u_char program_number_lo                        /*:8*/;
    u_char                                          :3;
    u_char network_pid_hi                           :5;
    u_char network_pid_lo                           /*:8*/;    
};

struct SPmt
{
    u_char table_id                   /*:8*/;

    u_char section_syntax_indicator   :1;
    u_char dummy                      :1; // has to be 0
    u_char                            :2;
    u_char section_length_hi          :4;    
    
    u_char section_length_lo          /*:8*/;
    u_char program_number_hi          /*:8*/;
    u_char program_number_lo          /*:8*/;

    u_char                            :2;
    u_char version_number             :5;
    u_char current_next_indicator     :1;
    
    u_char section_number             /*:8*/;
    u_char last_section_number        /*:8*/;

    u_char                            :3;
    u_char PCR_PID_hi                 :5;    
    
    u_char PCR_PID_lo                 /*:8*/;
    
    u_char                            :4;
    u_char program_info_length_hi     :4;

    u_char program_info_length_lo     /*:8*/;
};

struct SESHeader
{
    u_char stream_type                /*:8*/;
    u_char                            :3;
    u_char elementary_PID_hi          :5;
    u_char elementary_PID_lo          /*:8*/;
    u_char                            :4;
    u_char ES_info_length_hi          :4;
    u_char ES_info_length_lo          /*:8*/;
};

struct SESInfo
{
    SESHeader es_header;
    SDataChunk descriptors;
};

struct SDescriptorHeader
{
    u_char descriptor_tag             /*:8*/;
    u_char descriptor_length          /*:8*/;
};

typedef struct eit {
    u_char table_id                               /*:8*/;
    
    u_char section_syntax_indicator               :1;
    u_char                                        :3;
    u_char section_length_hi                      :4;
       
    u_char section_length_lo                      /*:8*/;
    u_char service_id_hi                          /*:8*/;
    u_char service_id_lo                          /*:8*/;

    u_char                                        :2;
    u_char version_number                         :5;
    u_char current_next_indicator                 :1;    

    u_char section_number                         /*:8*/;
    u_char last_section_number                    /*:8*/;
    u_char transport_stream_id_hi                 /*:8*/;
    u_char transport_stream_id_lo                 /*:8*/;
    u_char original_network_id_hi                 /*:8*/;
    u_char original_network_id_lo                 /*:8*/;
    u_char segment_last_section_number            /*:8*/;
    u_char segment_last_table_id                  /*:8*/;
    //    u_char data[]; /* struct eit_event */
} eit_t;

typedef struct eit_event {
    u_char event_id_hi                            /*:8*/;
    u_char event_id_lo                            /*:8*/;
    u_char mjd_hi                                 /*:8*/;
    u_char mjd_lo                                 /*:8*/;
    u_char start_time_h                           /*:8*/;
    u_char start_time_m                           /*:8*/;
    u_char start_time_s                           /*:8*/;
    u_char duration_h                             /*:8*/;
    u_char duration_m                             /*:8*/;
    u_char duration_s                             /*:8*/;

    u_char running_status                         :3;
    u_char free_ca_mode                           :1;
    u_char descriptors_loop_length_hi             :4;
    
    u_char descriptors_loop_length_lo             /*:8*/;
    //    u_char data[]; /* struct descr_gen */
} eit_event_t;

#endif

#define HILO_GET(x)         (x##_hi << 8 | x##_lo)
#define HILO_PUT(val, x)    {val##_hi = x >> 8; val##_lo = u_char(x);}

} //engine
} //dvblink

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_TS_H_
