/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <dl_types.h>
#include <dl_parameters.h>

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::selectable_param_element_t& spe, const unsigned int /*version*/)
{
    ar & spe.value_;
    ar & spe.name_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::selectable_param_description_t& spd, const unsigned int /*version*/)
{
    ar & spd.key_;
    ar & spd.name_;
    ar & spd.parameters_list_;
    ar & spd.selected_param_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::editable_param_element_t& epe, const unsigned int /*version*/)
{
    ar & epe.key_;
    ar & epe.name_;
    ar & epe.value_;
    ar & epe.format_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::parameters_container_t& pc, const unsigned int /*version*/)
{
    ar & pc.id_;
    ar & pc.name_;
    ar & pc.desc_;	
    ar & pc.editable_params_;
    ar & pc.selectable_params_;
}

} // namespace serialization
} // namespace boost
