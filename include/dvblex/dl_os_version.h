/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once
#ifdef _WIN32
#include <windows.h>
#endif

namespace dvblink { namespace engine {

enum e_processor_architecture
{
    processor_architecture_intel,
    processor_architecture_mips,
    processor_architecture_alpha,
    processor_architecture_ppc,
    processor_architecture_shx,
    processor_architecture_arm,
    processor_architecture_ia64,
    processor_architecture_alpha64,
    processor_architecture_msil,
    processor_architecture_amd64,
    processor_architecture_ia32_on_win64,

    processor_architecture_unknown = 0xffff
};

enum e_os_ver
{
    os_undefined
#ifdef _WIN32
    ,
    windows_2000,
    
    windows_xp,
    windows_xp_professional_x64_edition,
    windows_server_2003,
    windows_home_server,
    windows_server_2003_r2,

    windows_vista,
    windows_server_2008,

    windows_server_2008_r2,
    windows_7,
#else
#endif
};

enum e_product_type
{
    product_undefined
#ifdef _WIN32
    ,
    product_ultimate,
    product_home_basic,
    product_home_premium,
    product_enterprise,
    product_home_basic_n,
    product_business,
    product_standard_server,
    product_datacenter_server,
    product_smallbusiness_server,
    product_enterprise_server,
    product_starter,
    product_datacenter_server_core,
    product_standard_server_core,
    product_enterprise_server_core,
    product_enterprise_server_ia64,
    product_business_n,
    product_web_server,
    product_cluster_server,
    product_home_server,
    product_storage_express_server,
    product_storage_standard_server,
    product_storage_workgroup_server,
    product_storage_enterprise_server,
    product_server_for_smallbusiness,
    product_smallbusiness_server_premium,
#else
#endif
};

class os_version
{
public:
    os_version();

    e_os_ver version() const {return os_version_;}
    e_product_type product_type() const {return product_type_;}
    e_processor_architecture processor_architecture() const {return processor_architecture_;}

#ifdef _WIN32
    bool is_xp() const;
    bool is_vista() const;
    bool is_7() const;

#else
#endif

    bool is_x86() const {return processor_architecture_ == processor_architecture_intel;}
    bool is_amd64() const {return processor_architecture_ == processor_architecture_amd64;}
    bool is_arm() const {return processor_architecture_ == processor_architecture_arm;}

private:
    unsigned long major_version_;
    unsigned long minor_version_;
    
    e_os_ver os_version_;
    e_product_type product_type_;
    e_processor_architecture processor_architecture_;
};

} //engine
} //dvblink
