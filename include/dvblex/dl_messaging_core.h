/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <map>
#include <boost/thread.hpp>
#include <dli_message_driven.h>

namespace dvblink { namespace messaging {

class messaging_core;
typedef boost::shared_ptr<messaging_core> messaging_core_t;

class messaging_core : public i_server_messaging_core
{
public:
    messaging_core();
    ~messaging_core();

    const boost::uuids::uuid& __stdcall get_uid() {return uid_.get();}
    void __stdcall start();
    void __stdcall shutdown();

    i_result __stdcall query_interface(const base_id_t& requestor_id, const i_guid& iid, i_base_object_t& obj);

    void __stdcall register_queue(const i_client_message_queue_t& queue);
    void __stdcall unregister_queue(const message_queue_id_t& id);

    message_error __stdcall post(const message_addressee_t& to,
        const message_sender_t& from, const char* message_type_name, const void* message_body, size_t message_body_size);

    message_error __stdcall send(const message_id_t id, const message_addressee_t& to,
        const message_sender_t& from, const char* message_type_name, const void* message_body, size_t message_body_size);

    void __stdcall response(const message_id_t id, message_error error,
        const message_addressee_t& to, const message_sender_t& from, const void* message_body, size_t message_body_size);

private:
    typedef std::map<message_queue_id_t, i_client_message_queue_t> map_id_to_queue_t;
    typedef map_id_to_queue_t::iterator iter_map_id_to_queue_t;
    typedef map_id_to_queue_t::const_iterator const_iter_map_id_to_queue_t;

    map_id_to_queue_t map_queue_;
    boost::shared_mutex lock_;

    bool shutdown_;
    base_id_t uid_;
};

} //messaging
} //dvblink
