/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <dl_types.h>
#include <dl_pugixml_helper.h>
#include <dl_server_info.h>

namespace dvblex { 

const std::string server_info_root_node                 = "server_info";
const std::string install_id_node                       = "install_id";
const std::string server_id_node                        = "server_id";
const std::string version_node                          = "version";
const std::string build_node                            = "build";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const server_info_t& si)
{
    pugi::xml_node si_node = doc.append_child(server_info_root_node.c_str());
    if (si_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(si_node, install_id_node, si.install_id_.to_string());
        dvblink::pugixml_helpers::new_child(si_node, server_id_node, si.product_id_.to_string());
        dvblink::pugixml_helpers::new_child(si_node, version_node, si.version_.get());
        dvblink::pugixml_helpers::new_child(si_node, build_node, si.build_.get());
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, server_info_t& si)
{
    if (NULL != node)
    {                       
        std::string str;

        if (dvblink::pugixml_helpers::get_node_value(node, install_id_node, str))
            si.install_id_ = str;

        if (dvblink::pugixml_helpers::get_node_value(node, server_id_node, str))
            si.product_id_ = str;

        if (dvblink::pugixml_helpers::get_node_value(node, version_node, str))
            si.version_ = str;

        if (dvblink::pugixml_helpers::get_node_value(node, build_node, str))
            si.build_ = str;
    }

    return node;
}

////////////////////////////////////////////////////////

const std::string server_caps_root_node                 = "streaming_caps";
const std::string server_caps_protocols_node                 = "protocols";
const std::string server_caps_pb_protocols_node                 = "pb_protocols";
const std::string server_caps_transcoders_node                 = "transcoders";
const std::string server_caps_pb_transcoders_node                 = "pb_transcoders";
const std::string server_caps_addressees_node                 = "addressees";
const std::string server_caps_addressee_node                 = "addressee";
const std::string server_caps_id_node						= "id";
const std::string server_caps_record_node                 = "can_record";
const std::string server_caps_timeshift_node                 = "supports_timeshift";
const std::string server_caps_timeshift_v_node                 = "timeshift_version";
const std::string server_caps_device_mgmt_node                 = "device_management";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const server_capabilities_t& caps)
{
    pugi::xml_node caps_node = doc.append_child(server_caps_root_node.c_str());

    if (caps_node != NULL)
    {
		std::stringstream buf;
		buf << caps.protocols_.get();
        dvblink::pugixml_helpers::new_child(caps_node, server_caps_protocols_node, buf.str());
		buf.clear(); buf.str("");
		buf << caps.transcoders_.get();
        dvblink::pugixml_helpers::new_child(caps_node, server_caps_transcoders_node, buf.str());
		buf.clear(); buf.str("");
		buf << caps.pb_transcoders_.get();
        dvblink::pugixml_helpers::new_child(caps_node, server_caps_pb_transcoders_node, buf.str());
		buf.clear(); buf.str("");
		buf << caps.pb_protocols_.get();
        dvblink::pugixml_helpers::new_child(caps_node, server_caps_pb_protocols_node, buf.str());

        pugi::xml_node ads_node = dvblink::pugixml_helpers::new_child(caps_node, server_caps_addressees_node);
		if (ads_node != NULL)
		{
			for (size_t i=0; i<caps.addressees_.size(); i++)
			{
                pugi::xml_node ad_node = dvblink::pugixml_helpers::new_child(ads_node, server_caps_addressee_node);
				if (ad_node != NULL)
					dvblink::pugixml_helpers::new_child(ad_node, server_caps_id_node, caps.addressees_[i].to_string());
			}
		}

		if (caps.can_record_.get())
			dvblink::pugixml_helpers::new_child(caps_node, server_caps_record_node, dvblink::pugixml_helpers::xmlnode_value_true);

		if (caps.supports_timeshift_.get())
        {
            dvblink::pugixml_helpers::new_child(caps_node, server_caps_timeshift_node, dvblink::pugixml_helpers::xmlnode_value_true);

		    buf.clear(); buf.str("");
		    buf << caps.timeshift_version_.get();
            dvblink::pugixml_helpers::new_child(caps_node, server_caps_timeshift_v_node, buf.str());
        }


		if (caps.supports_device_mgmt_.get())
            dvblink::pugixml_helpers::new_child(caps_node, server_caps_device_mgmt_node, dvblink::pugixml_helpers::xmlnode_value_true);
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, server_capabilities_t& caps)
{
    if (NULL != node)
    {                       
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, server_caps_protocols_node, str))
        {
            boost::uint32_t value;
            dvblink::engine::string_conv::apply<boost::uint32_t, boost::uint32_t, char>(str.c_str(), value, (boost::uint32_t)dvblex::lpe_unknown);
            caps.protocols_ = value;
        }
        if (dvblink::pugixml_helpers::get_node_value(node, server_caps_transcoders_node, str))
        {
            boost::uint32_t value;
            dvblink::engine::string_conv::apply<boost::uint32_t, boost::uint32_t, char>(str.c_str(), value, (boost::uint32_t)dvblex::lte_unknown);
            caps.transcoders_ = value;
        }
        if (dvblink::pugixml_helpers::get_node_value(node, server_caps_pb_protocols_node, str))
        {
            boost::uint32_t value;
            dvblink::engine::string_conv::apply<boost::uint32_t, boost::uint32_t, char>(str.c_str(), value, (boost::uint32_t)dvblex::lpe_unknown);
            caps.pb_protocols_ = value;
        }
        if (dvblink::pugixml_helpers::get_node_value(node, server_caps_pb_transcoders_node, str))
        {
            boost::uint32_t value;
            dvblink::engine::string_conv::apply<boost::uint32_t, boost::uint32_t, char>(str.c_str(), value, (boost::uint32_t)dvblex::lte_unknown);
            caps.pb_transcoders_ = value;
        }

        pugi::xml_node addressees_node = node.child(server_caps_addressees_node.c_str());
        if (NULL != addressees_node)
        {
			pugi::xml_node ids_node = addressees_node.first_child();
			while (NULL != ids_node)                
			{
                pugi::xml_node addressee_node = ids_node.child(server_caps_addressee_node.c_str());
                if (NULL != addressee_node)
                    if (dvblink::pugixml_helpers::get_node_value(addressee_node, server_caps_id_node, str))
                        caps.addressees_.push_back(str);
                
                ids_node = ids_node.next_sibling();
            }
        }

        caps.can_record_ = (dvblink::pugixml_helpers::get_node_value(node, server_caps_record_node, str) && 
            boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true));

        caps.supports_timeshift_ = (dvblink::pugixml_helpers::get_node_value(node, server_caps_timeshift_node, str) && 
            boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true));

        if (caps.supports_timeshift_.get())
        {
            if (dvblink::pugixml_helpers::get_node_value(node, server_caps_timeshift_v_node, str))
            {
                boost::uint32_t value;
                dvblink::engine::string_conv::apply<boost::uint32_t, boost::uint32_t, char>(str.c_str(), value, (boost::uint32_t)0);
                caps.timeshift_version_ = value;
            }
        }

        caps.supports_device_mgmt_ = (dvblink::pugixml_helpers::get_node_value(node, server_caps_device_mgmt_node, str) && 
            boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true));
    }

    return node;
}

//////////////////////////////////////////////
struct server_caps_request_t
{
	std::vector<dvblink::message_addressee_t> addressees_to_check_;
};

inline pugi::xml_node& operator>> (pugi::xml_node& node, server_caps_request_t& server_caps_request)
{
    if (NULL != node)
    {                       
        pugi::xml_node addressees_node = node.child(server_caps_addressees_node.c_str());
        if (NULL != addressees_node)
        {
			pugi::xml_node ids_node = addressees_node.first_child();
			while (NULL != ids_node)                
			{
                if (boost::iequals(ids_node.name(), server_caps_addressee_node))
				{
					std::string str_value;
					if (dvblink::pugixml_helpers::get_node_value(ids_node, server_caps_id_node, str_value))
					{
						dvblink::message_addressee_t addressee(str_value);
						server_caps_request.addressees_to_check_.push_back(addressee);
					}
				}
				ids_node = ids_node.next_sibling();
			}
        }
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const server_caps_request_t& server_caps_request)
{
    pugi::xml_node caps_node = doc.append_child(server_caps_root_node.c_str());

    if (caps_node != NULL)
    {
        pugi::xml_node ads_node = dvblink::pugixml_helpers::new_child(caps_node, server_caps_addressees_node);
		if (ads_node != NULL)
		{
			for (size_t i=0; i<server_caps_request.addressees_to_check_.size(); i++)
			{
                pugi::xml_node ad_node = dvblink::pugixml_helpers::new_child(ads_node, server_caps_addressee_node);
				if (ad_node != NULL)
					dvblink::pugixml_helpers::new_child(ad_node, server_caps_id_node, server_caps_request.addressees_to_check_[i].to_string());
			}
		}
    }

    return doc;
}

///////////////////////////////////////////////////////////////////////////////

const std::string get_resume_info_root_node                 = "get_resume_info";
const std::string set_resume_info_root_node                 = "set_resume_info";
const std::string resume_info_root_node                     = "resume_info";
const std::string resume_info_obj_id_node                       = "object_id";
const std::string resume_info_pos_node                       = "pos";

struct object_get_resume_info_request
{
    dvblink::object_id_t object_id_;
};

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const object_get_resume_info_request& gri_req)
{
    pugi::xml_node node = doc.append_child(get_resume_info_root_node.c_str());
    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, resume_info_obj_id_node, gri_req.object_id_.to_string());
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, object_get_resume_info_request& gri_req)
{
    if (NULL != node)
    {                       
        std::string str;

        if (dvblink::pugixml_helpers::get_node_value(node, resume_info_obj_id_node, str))
            gri_req.object_id_ = str;
    }

    return node;
}

struct object_get_resume_info_response
{
    object_get_resume_info_response()
        : pos_(0)
    {}

    boost::int32_t pos_;
};

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const object_get_resume_info_response& gri_resp)
{
    pugi::xml_node node = doc.append_child(resume_info_root_node.c_str());
    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, resume_info_pos_node, boost::lexical_cast<std::string>(gri_resp.pos_));
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, object_get_resume_info_response& gri_resp)
{
    if (NULL != node)
    {                       
        std::string str;

        if (dvblink::pugixml_helpers::get_node_value(node, resume_info_pos_node, str))
            gri_resp.pos_ = atoi(str.c_str());
    }

    return node;
}

struct object_set_resume_info_request
{
    object_set_resume_info_request()
        : pos_(0)
    {}

    dvblink::object_id_t object_id_;
    boost::int32_t pos_;
};

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const object_set_resume_info_request& sri_req)
{
    pugi::xml_node node = doc.append_child(get_resume_info_root_node.c_str());
    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, resume_info_obj_id_node, sri_req.object_id_.to_string());
        dvblink::pugixml_helpers::new_child(node, resume_info_pos_node, boost::lexical_cast<std::string>(sri_req.pos_));
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, object_set_resume_info_request& sri_req)
{
    if (NULL != node)
    {                       
        std::string str;

        if (dvblink::pugixml_helpers::get_node_value(node, resume_info_obj_id_node, str))
            sri_req.object_id_ = str;

        if (dvblink::pugixml_helpers::get_node_value(node, resume_info_pos_node, str))
            sri_req.pos_ = atoi(str.c_str());

    }

    return node;
}


} //dvblex
