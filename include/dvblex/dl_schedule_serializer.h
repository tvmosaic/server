/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <dl_types.h>
#include <dl_pugixml_helper.h>
#include <dl_epgevent.h>
#include <dl_program_serializer.h>
#include <dl_schedules.h>

namespace dvblex { 

const std::string schedules_root_node                     = "schedules";
const std::string schedule_root_node                     = "schedule";
const std::string schedule_id_node                     = "schedule_id";
const std::string schedule_user_param_node               = "user_param";
const std::string schedule_margin_before_node               = "margine_before";
const std::string schedule_margin_after_node               = "margine_after";
const std::string schedule_start_before_node               = "start_before";
const std::string schedule_start_after_node               = "start_after";
const std::string schedule_targets_node                     = "targets";
const std::string schedule_active_node                     = "active";
const std::string schedule_priority_node                   = "priority";
const std::string schedule_target_node                     = "target";
const std::string schedule_by_epg_node                     = "by_epg";
const std::string schedule_channel_id_node                     = "channel_id";
const std::string schedule_program_id_node                     = "program_id";
const std::string schedule_repeat_node                     = "repeat";
const std::string schedule_new_only_node                     = "new_only";
const std::string schedule_record_series_anytime_node                     = "record_series_anytime";
const std::string schedule_recordings_to_keep_node                     = "recordings_to_keep";
const std::string schedule_manual_node                     = "manual";
const std::string schedule_title_node                     = "title";
const std::string schedule_start_time_node                     = "start_time";
const std::string schedule_duration_node                     = "duration";
const std::string schedule_day_mask_node                     = "day_mask";
const std::string schedule_by_pattern_node                     = "by_pattern";
const std::string schedule_key_phrase_node                     = "key_phrase";
const std::string schedule_genre_mask_node                     = "genre_mask";
const std::string schedule_update_schedule_node                     = "update_schedule";
const std::string schedule_remove_schedule_node                     = "remove_schedule";

inline pugi::xml_node& operator<< (pugi::xml_node& parent_node, const dvblex::recorder::schedule_item& si)
{
    std::stringstream buf;

    if (parent_node != NULL)
    {
        pugi::xml_node schedule_node = dvblink::pugixml_helpers::new_child(parent_node, schedule_root_node);

        if (schedule_node != NULL)
        {
            if (si.schedule_item_id_.get() != DL_RECORDER_INVALID_ID)
            {
                buf.clear(); buf.str("");
                buf << si.schedule_item_id_.get();
                dvblink::pugixml_helpers::new_child(schedule_node, schedule_id_node, buf.str());
            }

            dvblink::pugixml_helpers::new_child(schedule_node, schedule_user_param_node, si.extra_param_);

            buf.clear(); buf.str("");
            buf << si.margin_before_;
            dvblink::pugixml_helpers::new_child(schedule_node, schedule_margin_before_node, buf.str());

            buf.clear(); buf.str("");
            buf << si.margin_after_;
            dvblink::pugixml_helpers::new_child(schedule_node, schedule_margin_after_node, buf.str());

            buf.clear(); buf.str("");
            buf << si.priority_;
            dvblink::pugixml_helpers::new_child(schedule_node, schedule_priority_node, buf.str());

            dvblink::pugixml_helpers::new_child(schedule_node, schedule_active_node, si.active_ ? dvblink::pugixml_helpers::xmlnode_value_true : dvblink::pugixml_helpers::xmlnode_value_false);

            pugi::xml_node targets_node = dvblink::pugixml_helpers::new_child(schedule_node, schedule_targets_node);
			if (targets_node != NULL)
			{
                for (size_t i=0; i<si.targets_.size(); i++)
                    dvblink::pugixml_helpers::new_child(targets_node, schedule_target_node, si.targets_[i].to_string());
            }
            if (si.is_epg_based())
            {
                pugi::xml_node byepg_node = dvblink::pugixml_helpers::new_child(schedule_node, schedule_by_epg_node);
			    if (byepg_node != NULL)
			    {
                    dvblink::pugixml_helpers::new_child(byepg_node, schedule_channel_id_node, si.channel_.get());
                    dvblink::pugixml_helpers::new_child(byepg_node, schedule_program_id_node, si.epg_event_id_.get());
                    if (si.record_series_)
                        dvblink::pugixml_helpers::new_child(byepg_node, schedule_repeat_node, dvblink::pugixml_helpers::xmlnode_value_true);
                    if (si.record_series_new_only_)
                        dvblink::pugixml_helpers::new_child(byepg_node, schedule_new_only_node, dvblink::pugixml_helpers::xmlnode_value_true);

                    buf.clear(); buf.str("");
                    buf << (int)si.day_mask_;
                    dvblink::pugixml_helpers::new_child(byepg_node, schedule_day_mask_node, buf.str());

                    buf.clear(); buf.str("");
                    buf << si.start_after_sec_;
                    dvblink::pugixml_helpers::new_child(byepg_node, schedule_start_after_node, buf.str());

                    buf.clear(); buf.str("");
                    buf << si.start_before_sec_;
                    dvblink::pugixml_helpers::new_child(byepg_node, schedule_start_before_node, buf.str());

                    //for compatibility
                    bool record_series_anytime = si.start_after_sec_ == -1 && si.start_before_sec_ == -1;
                    dvblink::pugixml_helpers::new_child(byepg_node, schedule_record_series_anytime_node, record_series_anytime ? dvblink::pugixml_helpers::xmlnode_value_true : dvblink::pugixml_helpers::xmlnode_value_false);

                    buf.clear(); buf.str("");
                    buf << si.number_of_recordings_to_keep_;
                    dvblink::pugixml_helpers::new_child(byepg_node, schedule_recordings_to_keep_node, buf.str());

                    dvblink::engine::DLEPGEventList epgEventList;
                    if (dvblink::engine::EPGReadEventsFromXML(si.epg_program_info_, epgEventList) && epgEventList.size() == 1)
                    {
                        dvblex::recorder::epg_item_ex epg_item;
                        epg_item.event_ = epgEventList[0];
                        byepg_node << epg_item;
                    }
                }
            } else
            if (si.is_manual())
            {
                pugi::xml_node manual_node = dvblink::pugixml_helpers::new_child(schedule_node, schedule_manual_node);
			    if (manual_node != NULL)
			    {
                    dvblink::pugixml_helpers::new_child(manual_node, schedule_channel_id_node, si.channel_.get());
                    dvblink::pugixml_helpers::new_child(manual_node, schedule_title_node, si.name_.get());

                    buf.clear(); buf.str("");
                    buf << si.start_time_;
                    dvblink::pugixml_helpers::new_child(manual_node, schedule_start_time_node, buf.str());

                    buf.clear(); buf.str("");
                    buf << si.duration_;
                    dvblink::pugixml_helpers::new_child(manual_node, schedule_duration_node, buf.str());

                    buf.clear(); buf.str("");
                    buf << (int)si.day_mask_;
                    dvblink::pugixml_helpers::new_child(manual_node, schedule_day_mask_node, buf.str());

                    buf.clear(); buf.str("");
                    buf << si.number_of_recordings_to_keep_;
                    dvblink::pugixml_helpers::new_child(manual_node, schedule_recordings_to_keep_node, buf.str());
                }
            } else
            if (si.is_pattern_based())
            {
                pugi::xml_node pattern_node = dvblink::pugixml_helpers::new_child(schedule_node, schedule_by_pattern_node);
			    if (pattern_node != NULL)
			    {
                    dvblink::pugixml_helpers::new_child(pattern_node, schedule_channel_id_node, si.channel_.get());
                    dvblink::pugixml_helpers::new_child(pattern_node, schedule_key_phrase_node, si.key_phrase_.get());

                    buf.clear(); buf.str("");
                    buf << si.genre_mask_;
                    dvblink::pugixml_helpers::new_child(pattern_node, schedule_genre_mask_node, buf.str());

                    buf.clear(); buf.str("");
                    buf << si.number_of_recordings_to_keep_;
                    dvblink::pugixml_helpers::new_child(pattern_node, schedule_recordings_to_keep_node, buf.str());

                    buf.clear(); buf.str("");
                    buf << (int)si.day_mask_;
                    dvblink::pugixml_helpers::new_child(pattern_node, schedule_day_mask_node, buf.str());

                    buf.clear(); buf.str("");
                    buf << si.start_after_sec_;
                    dvblink::pugixml_helpers::new_child(pattern_node, schedule_start_after_node, buf.str());

                    buf.clear(); buf.str("");
                    buf << si.start_before_sec_;
                    dvblink::pugixml_helpers::new_child(pattern_node, schedule_start_before_node, buf.str());

                }
            }
        }
    }

    return parent_node;
}

inline pugi::xml_node& operator>> (pugi::xml_node& parent_node, dvblex::recorder::schedule_item& si)
{
    if (NULL != parent_node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(parent_node, schedule_id_node, str))
        {
            int l;
            dvblink::engine::string_conv::apply(str.c_str(), l, (int)DL_RECORDER_INVALID_ID);
            si.schedule_item_id_ = l;
        }

        if (dvblink::pugixml_helpers::get_node_value(parent_node, schedule_user_param_node, str))
            si.extra_param_ = str;
        if (dvblink::pugixml_helpers::get_node_value(parent_node, schedule_margin_before_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), si.margin_before_, si.margin_before_);
        if (dvblink::pugixml_helpers::get_node_value(parent_node, schedule_margin_after_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), si.margin_after_, si.margin_after_);

        if (dvblink::pugixml_helpers::get_node_value(parent_node, schedule_priority_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), si.priority_, si.priority_);

        if (dvblink::pugixml_helpers::get_node_value(parent_node, schedule_active_node, str))
            si.active_ = boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);

        pugi::xml_node targets_node = parent_node.child(schedule_targets_node.c_str());
        if (targets_node != NULL)
        {
            pugi::xml_node target_node = targets_node.first_child();
            while (target_node != NULL)
            {
                str = target_node.child_value();
                if (!str.empty())
                    si.targets_.push_back(str);

                target_node = target_node.next_sibling();
            }
        }

        pugi::xml_node by_epg_node = parent_node.child(schedule_by_epg_node.c_str());
        if (by_epg_node != NULL)
        {
            si.type_ = dvblex::recorder::sitEpgBased;
            if (dvblink::pugixml_helpers::get_node_value(by_epg_node, schedule_channel_id_node, str))
                si.channel_ = str;
            if (dvblink::pugixml_helpers::get_node_value(by_epg_node, schedule_program_id_node, str))
                si.epg_event_id_ = str;
            if (dvblink::pugixml_helpers::get_node_value(by_epg_node, schedule_repeat_node, str) &&
                boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true))
                si.record_series_ = true;
            if (dvblink::pugixml_helpers::get_node_value(by_epg_node, schedule_new_only_node, str) &&
                boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true))
                si.record_series_new_only_ = true;
            
            if (dvblink::pugixml_helpers::get_node_value(by_epg_node, schedule_day_mask_node, str))
                dvblink::engine::string_conv::apply<int, boost::uint8_t, char>(str.c_str(), si.day_mask_, si.day_mask_);

            if (dvblink::pugixml_helpers::get_node_value(by_epg_node, schedule_start_before_node, str))
                dvblink::engine::string_conv::apply(str.c_str(), si.start_before_sec_, si.start_before_sec_);

            if (dvblink::pugixml_helpers::get_node_value(by_epg_node, schedule_start_after_node, str))
                dvblink::engine::string_conv::apply(str.c_str(), si.start_after_sec_, si.start_after_sec_);

            if (dvblink::pugixml_helpers::get_node_value(by_epg_node, schedule_recordings_to_keep_node, str))
                dvblink::engine::string_conv::apply(str.c_str(), si.number_of_recordings_to_keep_, si.number_of_recordings_to_keep_);

            pugi::xml_node prg_node = by_epg_node.child(program_program_node.c_str());
            if (prg_node != NULL)
            {
                dvblex::recorder::epg_item_ex epg_item;
                prg_node >> epg_item;
                dvblink::engine::DLEPGEvent epg_event = epg_item.event_;
                dvblink::engine::DLEPGEventList epgEventList;
                epgEventList.push_back(epg_event);
                dvblink::engine::EPGWriteEventsToXML(epgEventList, si.epg_program_info_);
            }
        }
        pugi::xml_node manual_node = parent_node.child(schedule_manual_node.c_str());
        if (manual_node != NULL)
        {
            si.type_ = dvblex::recorder::sitManual;
            if (dvblink::pugixml_helpers::get_node_value(manual_node, schedule_channel_id_node, str))
                si.channel_ = str;
            if (dvblink::pugixml_helpers::get_node_value(manual_node, schedule_title_node, str))
                si.name_ = str;
            if (dvblink::pugixml_helpers::get_node_value(manual_node, schedule_start_time_node, str))
                dvblink::engine::string_conv::apply(str.c_str(), si.start_time_, si.start_time_);
            if (dvblink::pugixml_helpers::get_node_value(manual_node, schedule_duration_node, str))
                dvblink::engine::string_conv::apply(str.c_str(), si.duration_, si.duration_);
            if (dvblink::pugixml_helpers::get_node_value(manual_node, schedule_day_mask_node, str))
                dvblink::engine::string_conv::apply<int, boost::uint8_t, char>(str.c_str(), si.day_mask_, si.day_mask_);
            if (dvblink::pugixml_helpers::get_node_value(manual_node, schedule_recordings_to_keep_node, str))
                dvblink::engine::string_conv::apply(str.c_str(), si.number_of_recordings_to_keep_, si.number_of_recordings_to_keep_);
        }
        pugi::xml_node by_pattern_node = parent_node.child(schedule_by_pattern_node.c_str());
        if (by_pattern_node != NULL)
        {
            si.type_ = dvblex::recorder::sitPatternBased;
            if (dvblink::pugixml_helpers::get_node_value(by_pattern_node, schedule_channel_id_node, str))
                si.channel_ = str;
            if (dvblink::pugixml_helpers::get_node_value(by_pattern_node, schedule_key_phrase_node, str))
                si.key_phrase_ = str;
            if (dvblink::pugixml_helpers::get_node_value(by_pattern_node, schedule_genre_mask_node, str))
                dvblink::engine::string_conv::apply(str.c_str(), si.genre_mask_, si.genre_mask_);
            if (dvblink::pugixml_helpers::get_node_value(by_pattern_node, schedule_recordings_to_keep_node, str))
                dvblink::engine::string_conv::apply(str.c_str(), si.number_of_recordings_to_keep_, si.number_of_recordings_to_keep_);

            if (dvblink::pugixml_helpers::get_node_value(by_pattern_node, schedule_day_mask_node, str))
                dvblink::engine::string_conv::apply<int, boost::uint8_t, char>(str.c_str(), si.day_mask_, si.day_mask_);

            if (dvblink::pugixml_helpers::get_node_value(by_pattern_node, schedule_start_before_node, str))
                dvblink::engine::string_conv::apply(str.c_str(), si.start_before_sec_, si.start_before_sec_);

            if (dvblink::pugixml_helpers::get_node_value(by_pattern_node, schedule_start_after_node, str))
                dvblink::engine::string_conv::apply(str.c_str(), si.start_after_sec_, si.start_after_sec_);

        }
    }

    return parent_node;
}

/////////////////////////////////////////////////////////////////////

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const dvblex::recorder::schedule_list_t& schedules)
{
    pugi::xml_node schedules_node = doc.append_child(schedules_root_node.c_str());

    if (schedules_node != NULL)
    {

        dvblex::recorder::schedule_list_t::const_iterator it = schedules.begin();
        while (it != schedules.end()) 
        {
            schedules_node << *it;
            ++it;
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, dvblex::recorder::schedule_list_t& schedules)
{
    if (NULL != node) 
    {
        pugi::xml_node schedule_node = node.first_child();
        while (NULL != schedule_node)                
        {
            dvblex::recorder::schedule_item schedule;
            schedule_node >> schedule;
            schedules.push_back(schedule);

            schedule_node = schedule_node.next_sibling();
        }
    }

    return node;
}  

/////////////////////////////////////////////////////////////////////

inline pugi::xml_node& operator>> (pugi::xml_node& node, dvblex::recorder::update_schedule_item_info& usi)
{
    if (NULL != node)
    {
        //set that v2 schedule is not supported and update it afterwards
        usi.supports_v2_schedule_ = false;

        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, schedule_id_node, str))
        {
            int l;
            dvblink::engine::string_conv::apply(str.c_str(), l, DL_RECORDER_INVALID_ID);
            usi.schedule_item_id_ = l;
        }

        if (dvblink::pugixml_helpers::get_node_value(node, schedule_margin_before_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), usi.margin_before_, usi.margin_before_);

        if (dvblink::pugixml_helpers::get_node_value(node, schedule_margin_after_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), usi.margin_after_, usi.margin_after_);

        if (dvblink::pugixml_helpers::get_node_value(node, schedule_priority_node, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), usi.priority_, usi.priority_);
            //presence of the priority field defines that v2 schedule fields are supported
            usi.supports_v2_schedule_ = true;
        }

        if (dvblink::pugixml_helpers::get_node_value(node, schedule_active_node, str))
            usi.active_ = boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);

        if (dvblink::pugixml_helpers::get_node_value(node, schedule_new_only_node, str) &&
            boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true))
            usi.record_series_new_only_ = true;

        if (dvblink::pugixml_helpers::get_node_value(node, schedule_day_mask_node, str))
            dvblink::engine::string_conv::apply<int, boost::uint8_t, char>(str.c_str(), usi.day_mask_, usi.day_mask_);

        if (dvblink::pugixml_helpers::get_node_value(node, schedule_start_after_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), usi.start_after_sec_, usi.start_after_sec_);

        if (dvblink::pugixml_helpers::get_node_value(node, schedule_start_before_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), usi.start_before_sec_, usi.start_before_sec_);

        if (!usi.supports_v2_schedule_)
        {
            bool record_series_anytime = true;
            if (dvblink::pugixml_helpers::get_node_value(node, schedule_record_series_anytime_node, str))
                record_series_anytime = boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);

            if (record_series_anytime)
            {
                usi.start_before_sec_ = -1;
                usi.start_after_sec_ = -1;
            } else
            {
                //set them to special values to indicate that margins are used, but actual values have to be preservd
                usi.start_before_sec_ = DL_RECORDER_INVALID_START_MARGIN_VALUE;
                usi.start_after_sec_ = DL_RECORDER_INVALID_START_MARGIN_VALUE;
            }
        }

        if (dvblink::pugixml_helpers::get_node_value(node, schedule_recordings_to_keep_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), usi.number_of_recordings_to_keep_, usi.number_of_recordings_to_keep_);

        pugi::xml_node targets_node = node.child(schedule_targets_node.c_str());
        if (targets_node != NULL)
        {
            pugi::xml_node target_node = targets_node.first_child();
            while (target_node != NULL)
            {
                str = target_node.child_value();
                if (!str.empty())
                    usi.targets_.push_back(str);

                target_node = target_node.next_sibling();
            }
        }
    }
    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const dvblex::recorder::update_schedule_item_info& usi)
{
    pugi::xml_node usi_node = doc.append_child(schedule_update_schedule_node.c_str());

    if (usi_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(usi_node, schedule_id_node, usi.schedule_item_id_.get());
        dvblink::pugixml_helpers::new_child(usi_node, schedule_margin_before_node, usi.margin_before_);
        dvblink::pugixml_helpers::new_child(usi_node, schedule_margin_after_node, usi.margin_after_);
        dvblink::pugixml_helpers::new_child(usi_node, schedule_priority_node, usi.priority_);
        dvblink::pugixml_helpers::new_child(usi_node, schedule_active_node, usi.active_ ? dvblink::pugixml_helpers::xmlnode_value_true : dvblink::pugixml_helpers::xmlnode_value_false);

        if (usi.record_series_new_only_)
            dvblink::pugixml_helpers::new_child(usi_node, schedule_new_only_node, dvblink::pugixml_helpers::xmlnode_value_true);

        int dm = usi.day_mask_;
        dvblink::pugixml_helpers::new_child(usi_node, schedule_day_mask_node, dm);

        dvblink::pugixml_helpers::new_child(usi_node, schedule_start_before_node, usi.start_before_sec_);
        dvblink::pugixml_helpers::new_child(usi_node, schedule_start_after_node, usi.start_after_sec_);

        std::stringstream buf;

        buf.clear(); buf.str("");
        buf << usi.number_of_recordings_to_keep_;
        dvblink::pugixml_helpers::new_child(usi_node, schedule_recordings_to_keep_node, buf.str());

        pugi::xml_node targets_node = dvblink::pugixml_helpers::new_child(usi_node, schedule_targets_node);
        if (targets_node != NULL)
        {
            for (size_t i=0; i<usi.targets_.size(); i++)
                dvblink::pugixml_helpers::new_child(targets_node, schedule_target_node, usi.targets_[i].to_string());
        }

    }

    return doc;
}

/////////////////////////////////////////////////////////////////////

struct schedule_remover_t
{
    dvblink::schedule_item_id_t schedule_item_id_;
};

inline pugi::xml_node& operator>> (pugi::xml_node& node, schedule_remover_t& sr)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, schedule_id_node, str))
        {
            int l;
            dvblink::engine::string_conv::apply(str.c_str(), l, DL_RECORDER_INVALID_ID);
            sr.schedule_item_id_ = l;
        }
    }
    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const schedule_remover_t& sr)
{
    pugi::xml_node sr_node = doc.append_child(schedule_remove_schedule_node.c_str());

    if (sr_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(sr_node, schedule_id_node, sr.schedule_item_id_.get());
    }

    return doc;
}

} //dvblex
