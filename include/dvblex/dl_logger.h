/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_LOGGER_H_
#define __DVBLINK_DL_LOGGER_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <boost/format.hpp>
#include <stdarg.h>
#include <memory>
#include <dli_logger.h>

namespace dvblink { namespace logging {

class logger
{
public:
    static logger* instance();

    void set_interface(const i_logger_t& logger);
    i_logger_t get_interface() const {return logger_;}

    void shutdown();
    void log_message(e_log_level log_level, const wchar_t* log_str) const;

private:
    logger() {}
    ~logger() {shutdown();}

private:
    i_logger_t logger_;
};

template <e_log_level level> class formatted_log_t
{
public:
    formatted_log_t(const std::wstring& msg) :
        fmt_(msg)
    {
    }

    ~formatted_log_t()
    {
        logger::instance()->log_message(level, fmt_.str().c_str());
    }

    template <typename T> formatted_log_t& operator % (T value)
    {
        fmt_ % value;
        return *this;
    }

protected:
    boost::wformat fmt_;
};

template <e_log_level level> formatted_log_t<level> log(const wchar_t* prefix, const wchar_t* msg)
{
    return formatted_log_t<level>(std::wstring(prefix) + msg);
}

inline formatted_log_t<log_level_errors_and_warnings> log_warning(const wchar_t* msg)
{
    return log<log_level_errors_and_warnings>(L"[W] ", msg);
}

inline formatted_log_t<log_level_errors_and_warnings> log_error(const wchar_t* msg)
{
    return log<log_level_errors_and_warnings>(L"[E] ", msg);
}

inline formatted_log_t<log_level_info> log_info(const wchar_t* msg)
{
    return log<log_level_info>(L"[I] ", msg);
}

inline formatted_log_t<log_level_extended_info> log_ext_info(const wchar_t* msg)
{
    return log<log_level_extended_info>(L"[I] ", msg);
}

inline formatted_log_t<log_level_forced_info> log_forced_info(const wchar_t* msg)
{
    return log<log_level_forced_info>(L"[I] ", msg);
}

} //logging
} //dvblink

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_LOGGER_LOCAL_CLIENT_H_
