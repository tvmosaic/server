/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <sstream>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <dl_epgevent.h>
#include <dl_pugixml_helper.h>

namespace dvblex { namespace playback {

typedef struct dvblink::engine::DLEPGEvent pb_video_info_t;

const std::string video_info_root_node               = "video_info";
const std::string epg_channel_name                        = "name";
const std::string epg_program_short_desc                  = "short_desc";
const std::string epg_program_subname                     = "subname";
const std::string epg_program_language                    = "language";
const std::string epg_program_actors                      = "actors";
const std::string epg_program_directors                   = "directors";
const std::string epg_program_writers                     = "writers";
const std::string epg_program_producers                   = "producers";
const std::string epg_program_guests                      = "guests";
const std::string epg_program_categories                  = "categories";
const std::string epg_program_image                       = "image";
const std::string epg_program_start_time                  = "start_time";
const std::string epg_program_duration                    = "duration";
const std::string epg_program_year                        = "year";
const std::string epg_program_episode_num                 = "episode_num";
const std::string epg_program_season_num                  = "season_num";
const std::string epg_program_stars_num                   = "stars_num";
const std::string epg_program_starsmax_num                = "starsmax_num";
const std::string epg_program_hdtv                        = "hdtv";
const std::string epg_program_premiere                    = "premiere";
const std::string epg_program_repeat                      = "repeat";
const std::string epg_program_cat_action                  = "cat_action";
const std::string epg_program_cat_comedy                  = "cat_comedy";
const std::string epg_program_cat_documentary             = "cat_documentary";
const std::string epg_program_cat_drama                   = "cat_drama";
const std::string epg_program_cat_educational             = "cat_educational";
const std::string epg_program_cat_horror                  = "cat_horror";
const std::string epg_program_cat_kids                    = "cat_kids";
const std::string epg_program_cat_movie                   = "cat_movie";
const std::string epg_program_cat_music                   = "cat_music";
const std::string epg_program_cat_news                    = "cat_news";
const std::string epg_program_cat_reality                 = "cat_reality";
const std::string epg_program_cat_romance                 = "cat_romance";
const std::string epg_program_cat_scifi                   = "cat_scifi";
const std::string epg_program_cat_serial                  = "cat_serial";
const std::string epg_program_cat_soap                    = "cat_soap";
const std::string epg_program_cat_special                 = "cat_special";
const std::string epg_program_cat_sports                  = "cat_sports";
const std::string epg_program_cat_thriller                = "cat_thriller";
const std::string epg_program_cat_adult                   = "cat_adult";

inline void write_to_node(pugi::xml_node& node, const pb_video_info_t& epg_event)
{
    std::stringstream str_out;

    pugi::xml_node vi_node = dvblink::pugixml_helpers::new_child(node, video_info_root_node);
    if (vi_node != NULL)
    {
	    if (!epg_event.m_Name.empty())
            dvblink::pugixml_helpers::new_child(vi_node, epg_channel_name, epg_event.m_Name);

        if (!epg_event.m_ShortDesc.empty())
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_short_desc, epg_event.m_ShortDesc);

    	if (!epg_event.m_SecondName.empty())
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_subname, epg_event.m_SecondName);

        if (!epg_event.m_Language.empty())
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_language, epg_event.m_Language);

        if (!epg_event.m_Actors.empty())
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_actors, epg_event.m_Actors);

        if (!epg_event.m_Directors.empty())
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_directors, epg_event.m_Directors);

        if (!epg_event.m_Writers.empty())
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_writers, epg_event.m_Writers);

        if (!epg_event.m_Producers.empty())
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_producers, epg_event.m_Producers);

        if (!epg_event.m_Guests.empty())
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_guests, epg_event.m_Guests);

        if (!epg_event.m_Categories.empty())
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_categories, epg_event.m_Categories);

        if (!epg_event.m_ImageURL.empty())
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_image, epg_event.m_ImageURL);

        std::stringstream str_out;

        str_out.clear(); str_out.str("");
        str_out << epg_event.m_StartTime;
        dvblink::pugixml_helpers::new_child(vi_node, epg_program_start_time, str_out.str());

        str_out.clear(); str_out.str("");
        str_out << epg_event.m_Duration;
        dvblink::pugixml_helpers::new_child(vi_node, epg_program_duration, str_out.str());

	    if (epg_event.m_Year > 0)
	    {
		    str_out.clear(); str_out.str("");
		    str_out << epg_event.m_Year;
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_year, str_out.str());
	    }

	    if (epg_event.m_EpisodeNum > 0)
	    {
		    str_out.clear(); str_out.str("");
		    str_out << epg_event.m_EpisodeNum;
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_episode_num, str_out.str());
	    }

	    if (epg_event.m_SeasonNum > 0)
	    {
		    str_out.clear(); str_out.str("");
		    str_out << epg_event.m_SeasonNum;
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_season_num, str_out.str());
	    }

	    if (epg_event.m_StarNum > 0)
	    {
		    str_out.clear(); str_out.str("");
		    str_out << epg_event.m_StarNum;
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_stars_num, str_out.str());
	    }

	    if (epg_event.m_StarNumMax > 0)
	    {
		    str_out.clear(); str_out.str("");
		    str_out << epg_event.m_StarNumMax;
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_starsmax_num, str_out.str());
	    }

        if (epg_event.m_IsHDTV)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_hdtv, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsPremiere)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_premiere, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsRepeatFlag)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_repeat, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsAction)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_action, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsComedy)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_comedy, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsDocumentary)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_documentary, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsDrama)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_drama, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsEducational)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_educational, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsHorror)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_horror, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsKids)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_kids, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsMovie)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_movie, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsMusic)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_music, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsNews)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_news, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsReality)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_reality, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsRomance)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_romance, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsScienceFiction)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_scifi, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsSerial)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_serial, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsSoap)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_soap, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsSpecial)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_special, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsSports)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_sports, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsThriller)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_thriller, dvblink::pugixml_helpers::xmlnode_value_true);

        if (epg_event.m_IsAdult)
            dvblink::pugixml_helpers::new_child(vi_node, epg_program_cat_adult, dvblink::pugixml_helpers::xmlnode_value_true);
    }
}

inline void read_from_node(pugi::xml_node& node, pb_video_info_t& epg_event)
{
    pugi::xml_node vi_node = node.child(video_info_root_node.c_str());
    if (vi_node != NULL)
    {
        std::string str;

	    if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_channel_name, str))
		    epg_event.m_Name = str;

	    if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_short_desc, str))
		    epg_event.m_ShortDesc = str;

	    if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_subname, str))
		    epg_event.m_SecondName = str;

	    if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_language, str))
		    epg_event.m_Language = str;

	    if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_actors, str))
		    epg_event.m_Actors = str;

	    if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_directors, str))
		    epg_event.m_Directors = str;

	    if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_writers, str))
		    epg_event.m_Writers = str;

	    if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_producers, str))
		    epg_event.m_Producers = str;

	    if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_guests, str))
		    epg_event.m_Guests = str;

	    if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_categories, str))
		    epg_event.m_Categories = str;

	    if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_image, str))
		    epg_event.m_ImageURL = str;

        if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_start_time, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), epg_event.m_StartTime, (time_t)0);
        }

        if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_duration, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), epg_event.m_Duration, (time_t)0);
        }

        if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_year, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), epg_event.m_Year, (long)0);
        }

        if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_episode_num, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), epg_event.m_EpisodeNum, (long)0);
        }

        if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_season_num, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), epg_event.m_SeasonNum, (long)0);
        }

        if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_stars_num, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), epg_event.m_StarNum, (long)0);
        }

        if (dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_starsmax_num, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), epg_event.m_StarNumMax, (long)0);
        }

        epg_event.m_IsHDTV = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_hdtv, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsPremiere = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_premiere, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsRepeatFlag = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_repeat, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsAction = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_action, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsComedy = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_comedy, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsDocumentary = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_documentary, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsDrama = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_drama, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsEducational = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_educational, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsHorror = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_horror, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsKids = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_kids, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsMovie = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_movie, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsMusic = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_music, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsNews = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_news, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsReality = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_reality, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsRomance = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_romance, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsScienceFiction = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_scifi, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsSerial = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_serial, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsSoap = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_soap, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsSpecial = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_special, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsSports = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_sports, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsThriller = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_thriller, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
        epg_event.m_IsAdult = dvblink::pugixml_helpers::get_node_value(vi_node, epg_program_cat_adult, str) && boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
    }
}


} // playback
} // dvblex

