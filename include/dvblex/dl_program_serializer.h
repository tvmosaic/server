/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <dl_types.h>
#include <dl_pugixml_helper.h>
#include <dl_channels.h>
#include <dl_programs.h>

namespace dvblex { 

const std::string program_dvblink_epg_node                   = "dvblink_epg";
const std::string program_program_node                   = "program";
const std::string program_id_node                   = "program_id";
const std::string program_name_node                   = "name";
const std::string program_start_time_node                   = "start_time";
const std::string program_duration_node                   = "duration";
const std::string program_short_desc_node                   = "short_desc";
const std::string program_subname_node                   = "subname";
const std::string program_language_node                   = "language";
const std::string program_actors_node                   = "actors";
const std::string program_directors_node                   = "directors";
const std::string program_writers_node                   = "writers";
const std::string program_producers_node                   = "producers";
const std::string program_quests_node                   = "guests";
const std::string program_categories_node                   = "categories";
const std::string program_image_node                   = "image";
const std::string program_year_node                   = "year";
const std::string program_episode_num_node                   = "episode_num";
const std::string program_season_num_node                   = "season_num";
const std::string program_stars_num_node                   = "stars_num";
const std::string program_starsmax_num_node                   = "starsmax_num";
const std::string program_hdtv_node                   = "hdtv";
const std::string program_premiere_node                   = "premiere";
const std::string program_repeat_node                   = "repeat";
const std::string program_cat_action_node                   = "cat_action";
const std::string program_cat_comedy_node                   = "cat_comedy";
const std::string program_cat_documentary_node                   = "cat_documentary";
const std::string program_cat_drama_node                   = "cat_drama";
const std::string program_cat_edu_node                   = "cat_educational";
const std::string program_cat_horror_node                   = "cat_horror";
const std::string program_cat_kids_node                   = "cat_kids";
const std::string program_cat_movie_node                   = "cat_movie";
const std::string program_cat_music_node                   = "cat_music";
const std::string program_cat_news_node                   = "cat_news";
const std::string program_cat_reality_node                   = "cat_reality";
const std::string program_cat_romance_node                   = "cat_romance";
const std::string program_cat_scifi_node                   = "cat_scifi";
const std::string program_cat_serial_node                   = "cat_serial";
const std::string program_cat_soap_node                   = "cat_soap";
const std::string program_cat_special_node                   = "cat_special";
const std::string program_cat_sports_node                   = "cat_sports";
const std::string program_cat_thriller_node                   = "cat_thriller";
const std::string program_cat_adult_node                   = "cat_adult";
const std::string program_is_record_node                   = "is_record";
const std::string program_is_record_series_node                   = "is_series";
const std::string program_is_repeat_record_node                   = "is_repeat_record";
const std::string program_is_record_conflict_node                   = "is_record_conflict";
const std::string program_epg_searcher_node                   = "epg_searcher";
const std::string program_channel_ids_node                   = "channels_ids";
const std::string program_channel_id_node                   = "channel_id";
const std::string program_genre_mask_node                   = "genre_mask";
const std::string program_keywords_node                   = "keywords";
const std::string program_requested_count_node                   = "requested_count";
const std::string program_end_time_node                   = "end_time";
const std::string program_epg_short_node                   = "epg_short";
const std::string program_channel_epg_node                   = "channel_epg";
const std::string program_timestamp_attrib                   = "timestamp";

inline pugi::xml_node& operator<< (pugi::xml_node& parent_node, const dvblex::recorder::epg_item_ex& ee)
{
    std::stringstream buf;

    pugi::xml_node program_node = dvblink::pugixml_helpers::new_child(parent_node, program_program_node);

    if (program_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(program_node, program_id_node, ee.event_.id_);
        dvblink::pugixml_helpers::new_child(program_node, program_name_node, ee.event_.m_Name);

        buf.clear(); buf.str("");
        buf << ee.event_.m_StartTime;
        dvblink::pugixml_helpers::new_child(program_node, program_start_time_node, buf.str());

        buf.clear(); buf.str("");
        buf << ee.event_.m_Duration;
        dvblink::pugixml_helpers::new_child(program_node, program_duration_node, buf.str());

        if (!ee.event_.m_ShortDesc.empty())
            dvblink::pugixml_helpers::new_child(program_node, program_short_desc_node, ee.event_.m_ShortDesc);

        if (!ee.event_.m_SecondName.empty())
            dvblink::pugixml_helpers::new_child(program_node, program_subname_node, ee.event_.m_SecondName);

        if (!ee.event_.m_Language.empty())
            dvblink::pugixml_helpers::new_child(program_node, program_language_node, ee.event_.m_Language);

        if (!ee.event_.m_Actors.empty())
            dvblink::pugixml_helpers::new_child(program_node, program_actors_node, ee.event_.m_Actors);

        if (!ee.event_.m_Directors.empty())
            dvblink::pugixml_helpers::new_child(program_node, program_directors_node, ee.event_.m_Directors);

        if (!ee.event_.m_Writers.empty())
            dvblink::pugixml_helpers::new_child(program_node, program_writers_node, ee.event_.m_Writers);

        if (!ee.event_.m_Producers.empty())
            dvblink::pugixml_helpers::new_child(program_node, program_producers_node, ee.event_.m_Producers);

        if (!ee.event_.m_Guests.empty())
            dvblink::pugixml_helpers::new_child(program_node, program_quests_node, ee.event_.m_Guests);

        if (!ee.event_.m_Categories.empty())
            dvblink::pugixml_helpers::new_child(program_node, program_categories_node, ee.event_.m_Categories);

        if (!ee.event_.m_ImageURL.empty())
            dvblink::pugixml_helpers::new_child(program_node, program_image_node, ee.event_.m_ImageURL);

        if (ee.event_.m_Year > 0)
        {
            buf.clear(); buf.str("");
            buf << ee.event_.m_Year;
            dvblink::pugixml_helpers::new_child(program_node, program_year_node, buf.str());
        }

        if (ee.event_.m_EpisodeNum > 0)
        {
            buf.clear(); buf.str("");
            buf << ee.event_.m_EpisodeNum;
            dvblink::pugixml_helpers::new_child(program_node, program_episode_num_node, buf.str());
        }

        if (ee.event_.m_SeasonNum > 0)
        {
            buf.clear(); buf.str("");
            buf << ee.event_.m_SeasonNum;
            dvblink::pugixml_helpers::new_child(program_node, program_season_num_node, buf.str());
        }

        if (ee.event_.m_StarNum > 0)
        {
            buf.clear(); buf.str("");
            buf << ee.event_.m_StarNum;
            dvblink::pugixml_helpers::new_child(program_node, program_stars_num_node, buf.str());
        }

        if (ee.event_.m_StarNumMax > 0)
        {
            buf.clear(); buf.str("");
            buf << ee.event_.m_StarNumMax;
            dvblink::pugixml_helpers::new_child(program_node, program_starsmax_num_node, buf.str());
        }

        if (ee.event_.m_IsHDTV)
            dvblink::pugixml_helpers::new_child(program_node, program_hdtv_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsPremiere)
            dvblink::pugixml_helpers::new_child(program_node, program_premiere_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsRepeatFlag)
            dvblink::pugixml_helpers::new_child(program_node, program_repeat_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsAction)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_action_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsComedy)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_comedy_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsDocumentary)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_documentary_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsDrama)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_drama_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsEducational)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_edu_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsHorror)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_horror_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsKids)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_kids_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsMovie)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_movie_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsNews)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_news_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsReality)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_reality_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsRomance)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_romance_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsScienceFiction)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_scifi_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsSerial)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_serial_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsSoap)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_soap_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsSpecial)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_special_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsSports)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_sports_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsThriller)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_thriller_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.event_.m_IsAdult)
            dvblink::pugixml_helpers::new_child(program_node, program_cat_adult_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.is_record_)
            dvblink::pugixml_helpers::new_child(program_node, program_is_record_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.is_series_)
            dvblink::pugixml_helpers::new_child(program_node, program_is_record_series_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.is_repeat_record_)
            dvblink::pugixml_helpers::new_child(program_node, program_is_repeat_record_node, dvblink::pugixml_helpers::xmlnode_value_true);

        if (ee.is_record_conflict_)
            dvblink::pugixml_helpers::new_child(program_node, program_is_record_conflict_node, dvblink::pugixml_helpers::xmlnode_value_true);
    }

    return parent_node;
}

/////////////////////////////////////////////////////////////////////

inline pugi::xml_node& operator>> (pugi::xml_node& node, dvblex::recorder::epg_item_ex& ee)
{
    if (NULL != node)
    {
        std::string str;

        dvblink::pugixml_helpers::get_node_value(node, program_id_node, ee.event_.id_);
        dvblink::pugixml_helpers::get_node_value(node, program_name_node, ee.event_.m_Name);
        
        if (dvblink::pugixml_helpers::get_node_value(node, program_start_time_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), ee.event_.m_StartTime, (time_t)0);

        if (dvblink::pugixml_helpers::get_node_value(node, program_duration_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), ee.event_.m_Duration, (time_t)0);

        dvblink::pugixml_helpers::get_node_value(node, program_short_desc_node, ee.event_.m_ShortDesc);
        dvblink::pugixml_helpers::get_node_value(node, program_subname_node, ee.event_.m_SecondName);
        dvblink::pugixml_helpers::get_node_value(node, program_language_node, ee.event_.m_Language);
        dvblink::pugixml_helpers::get_node_value(node, program_actors_node, ee.event_.m_Actors);
        dvblink::pugixml_helpers::get_node_value(node, program_directors_node, ee.event_.m_Directors);
        dvblink::pugixml_helpers::get_node_value(node, program_writers_node, ee.event_.m_Writers);
        dvblink::pugixml_helpers::get_node_value(node, program_producers_node, ee.event_.m_Producers);
        dvblink::pugixml_helpers::get_node_value(node, program_quests_node, ee.event_.m_Guests);
        dvblink::pugixml_helpers::get_node_value(node, program_categories_node, ee.event_.m_Categories);
        dvblink::pugixml_helpers::get_node_value(node, program_image_node, ee.event_.m_ImageURL);

        if (dvblink::pugixml_helpers::get_node_value(node, program_year_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), ee.event_.m_Year, ee.event_.m_Year);

        if (dvblink::pugixml_helpers::get_node_value(node, program_episode_num_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), ee.event_.m_EpisodeNum, ee.event_.m_EpisodeNum);

        if (dvblink::pugixml_helpers::get_node_value(node, program_season_num_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), ee.event_.m_SeasonNum, ee.event_.m_SeasonNum);

        if (dvblink::pugixml_helpers::get_node_value(node, program_stars_num_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), ee.event_.m_StarNum, ee.event_.m_StarNum);

        if (dvblink::pugixml_helpers::get_node_value(node, program_starsmax_num_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), ee.event_.m_StarNumMax, ee.event_.m_StarNumMax);

        ee.event_.m_IsHDTV = node.child(program_hdtv_node.c_str()) != NULL;
        ee.event_.m_IsPremiere = node.child(program_premiere_node.c_str()) != NULL;
        ee.event_.m_IsRepeatFlag = node.child(program_repeat_node.c_str()) != NULL;
        ee.event_.m_IsAction = node.child(program_cat_action_node.c_str()) != NULL;
        ee.event_.m_IsComedy = node.child(program_cat_comedy_node.c_str()) != NULL;
        ee.event_.m_IsDocumentary = node.child(program_cat_documentary_node.c_str()) != NULL;
        ee.event_.m_IsDrama = node.child(program_cat_drama_node.c_str()) != NULL;
        ee.event_.m_IsEducational = node.child(program_cat_edu_node.c_str()) != NULL;
        ee.event_.m_IsHorror = node.child(program_cat_horror_node.c_str()) != NULL;
        ee.event_.m_IsKids = node.child(program_cat_kids_node.c_str()) != NULL;
        ee.event_.m_IsMovie = node.child(program_cat_movie_node.c_str()) != NULL;
        ee.event_.m_IsNews = node.child(program_cat_news_node.c_str()) != NULL;
        ee.event_.m_IsReality = node.child(program_cat_reality_node.c_str()) != NULL;
        ee.event_.m_IsRomance = node.child(program_cat_romance_node.c_str()) != NULL;
        ee.event_.m_IsScienceFiction = node.child(program_cat_scifi_node.c_str()) != NULL;
        ee.event_.m_IsSerial = node.child(program_cat_serial_node.c_str()) != NULL;
        ee.event_.m_IsSoap = node.child(program_cat_soap_node.c_str()) != NULL;
        ee.event_.m_IsSpecial = node.child(program_cat_special_node.c_str()) != NULL;
        ee.event_.m_IsSports = node.child(program_cat_sports_node.c_str()) != NULL;
        ee.event_.m_IsThriller = node.child(program_cat_thriller_node.c_str()) != NULL;
        ee.event_.m_IsAdult = node.child(program_cat_adult_node.c_str()) != NULL;
        ee.is_record_ = node.child(program_is_record_node.c_str()) != NULL;
        ee.is_series_ = node.child(program_is_record_series_node.c_str()) != NULL;
        ee.is_repeat_record_ = node.child(program_is_repeat_record_node.c_str()) != NULL;
        ee.is_record_conflict_ = node.child(program_is_record_conflict_node.c_str()) != NULL;
    }
    return node;
}

/////////////////////////////////////////////////////////////////////

inline pugi::xml_node& operator<< (pugi::xml_node& parent_node, const dvblex::recorder::epg_event_ex_list_t& events)
{
    pugi::xml_node epg_node = dvblink::pugixml_helpers::new_child(parent_node, program_dvblink_epg_node);

    if (epg_node != NULL)
    {
        dvblex::recorder::epg_event_ex_list_t::const_iterator it = events.begin();
        while (it != events.end()) 
        {
            epg_node << *it;
            ++it;
        }
    }

    return parent_node;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, dvblex::recorder::epg_event_ex_list_t& events)
{
    if (NULL != node)
    {
        std::string str;

        pugi::xml_node epg_node = node.first_child();
        while (epg_node != NULL)
        {
            if (boost::iequals(epg_node.name(), program_program_node))
            {
                dvblex::recorder::epg_item_ex ei;
                epg_node >> ei;

                events.push_back(ei);
            }

            epg_node = epg_node.next_sibling();
        }
    }
    return node;
}

/////////////////////////////////////////////////////////////////////

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const dvblex::recorder::timed_epg_ex_channel_info_map_t& timed_epg_map)
{
    pugi::xml_node searcher_node = doc.append_child(program_epg_searcher_node.c_str());

    if (searcher_node)
    {
        std::stringstream buf;
        buf << timed_epg_map.time_stamp_;
        dvblink::pugixml_helpers::add_node_attribute(searcher_node, program_timestamp_attrib, buf.str());

        dvblex::recorder::epg_ex_channel_info_map_t::const_iterator it = timed_epg_map.epg_map_.begin();
        while (it != timed_epg_map.epg_map_.end())
        {
            pugi::xml_node channel_node = dvblink::pugixml_helpers::new_child(searcher_node, program_channel_epg_node);
		    if (channel_node != NULL)
		    {
                dvblink::pugixml_helpers::new_child(channel_node, program_channel_id_node, it->first.get());

                channel_node << it->second;
            }

            ++it;
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, dvblex::recorder::timed_epg_ex_channel_info_map_t& timed_epg_map)
{
    if (NULL != node)
    {
        std::string str;

        if (dvblink::pugixml_helpers::get_node_attribute(node, program_timestamp_attrib, str))
            dvblink::engine::string_conv::apply(str.c_str(), timed_epg_map.time_stamp_, (time_t)timed_epg_map.time_stamp_);

        pugi::xml_node channel_node = node.first_child();
        while (channel_node != NULL)
        {
            if (boost::iequals(channel_node.name(), program_channel_epg_node))
            {
                dvblink::channel_id_t channel_id;
                dvblex::recorder::epg_event_ex_list_t epg_event_ex_list;

                if (dvblink::pugixml_helpers::get_node_value(channel_node, program_channel_id_node, str))
                    channel_id = str;

                pugi::xml_node epg_node = channel_node.child(program_dvblink_epg_node.c_str());
                if (epg_node != NULL)
                    epg_node >> epg_event_ex_list;

                timed_epg_map.epg_map_[channel_id] = epg_event_ex_list;
            }
            channel_node = channel_node.next_sibling();
        }
    }
    return node;
}

/////////////////////////////////////////////////////////////////////

struct search_epg_request_t
{
    search_epg_request_t() :
        genre_mask_(0), requested_count_(-1), start_time_(0), end_time_(0), short_epg_(false)
    {}

    bool is_program_info_request() {return !program_id_.empty();}
    bool is_keyword_search() {return !keywords_.empty();}
    bool is_time_search() {return start_time_ && end_time_;}

    channel_id_list_t channels_;
    dvblink::epg_event_id_t program_id_;
    dvblink::key_phrase_t keywords_;
    unsigned long genre_mask_;
    int requested_count_;
    time_t start_time_;
    time_t end_time_;
    bool short_epg_;
};

inline pugi::xml_node& operator>> (pugi::xml_node& node, search_epg_request_t& ser)
{
    if (NULL != node)
    {
        std::string str;

        pugi::xml_node channels_node = node.child(program_channel_ids_node.c_str());
        if (channels_node != NULL)
        {
            pugi::xml_node channel_node = channels_node.first_child();
            while (channel_node != NULL)
            {
                str = channel_node.child_value();
                if (!str.empty())
                    ser.channels_.push_back(str);

                channel_node = channel_node.next_sibling();
            }
        }

        if (dvblink::pugixml_helpers::get_node_value(node, program_id_node, str))
            ser.program_id_ = str;

        if (dvblink::pugixml_helpers::get_node_value(node, program_keywords_node, str))
            ser.keywords_ = str;

        if (dvblink::pugixml_helpers::get_node_value(node, program_genre_mask_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), ser.genre_mask_, ser.genre_mask_);

        if (dvblink::pugixml_helpers::get_node_value(node, program_requested_count_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), ser.requested_count_, ser.requested_count_);

        if (dvblink::pugixml_helpers::get_node_value(node, program_start_time_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), ser.start_time_, ser.start_time_);

        if (dvblink::pugixml_helpers::get_node_value(node, program_end_time_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), ser.end_time_, ser.end_time_);

        if (dvblink::pugixml_helpers::get_node_value(node, program_epg_short_node, str) &&
            boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true) )
            ser.short_epg_ = true;
    }
    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const search_epg_request_t& ser)
{
    pugi::xml_node searcher_node = doc.append_child(program_epg_searcher_node.c_str());

    if (searcher_node != NULL)
    {
        pugi::xml_node channels_node = dvblink::pugixml_helpers::new_child(searcher_node, program_channel_ids_node);
        if (channels_node != NULL)
        {
            for (size_t i=0; i<ser.channels_.size(); i++)
                dvblink::pugixml_helpers::new_child(channels_node, program_channel_id_node, ser.channels_[i].get());
        }

        dvblink::pugixml_helpers::new_child(searcher_node, program_id_node, ser.program_id_.get());
        dvblink::pugixml_helpers::new_child(searcher_node, program_keywords_node, ser.keywords_.get());

        std::stringstream buf;

        buf.clear(); buf.str("");
        buf << ser.genre_mask_;
        dvblink::pugixml_helpers::new_child(searcher_node, program_genre_mask_node, buf.str());

        buf.clear(); buf.str("");
        buf << ser.requested_count_;
        dvblink::pugixml_helpers::new_child(searcher_node, program_requested_count_node, buf.str());

        buf.clear(); buf.str("");
        buf << ser.start_time_;
        dvblink::pugixml_helpers::new_child(searcher_node, program_start_time_node, buf.str());

        buf.clear(); buf.str("");
        buf << ser.end_time_;
        dvblink::pugixml_helpers::new_child(searcher_node, program_end_time_node, buf.str());

        if (ser.short_epg_)
            dvblink::pugixml_helpers::new_child(searcher_node, program_epg_short_node, dvblink::pugixml_helpers::xmlnode_value_true);
    }

    return doc;
}


} //dvblex
