/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <sstream>
#include <vector>
#include <map>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <dl_pugixml_helper.h>
#include <dl_send_to_item.h>
#include <dl_parameters.h>
#include <dl_parameters_serializer.h>

namespace dvblex {

    const std::string SEND_TO_TARGET_CMD_GET_FORMATTERS           = "get_formatters";
    const std::string SEND_TO_TARGET_CMD_GET_DESTINATIONS         = "get_destinations";
    const std::string SEND_TO_TARGET_CMD_ACTIVATE_WORK_UNIT       = "activate_wu";
    const std::string SEND_TO_TARGET_CMD_GET_TARGETS			  = "get_targets";
    const std::string SEND_TO_TARGET_CMD_SET_TARGETS			  = "set_targets";
    const std::string SEND_TO_TARGET_CMD_GET_COMSKIP_SETTINGS     = "get_comskip_settings";

    const std::string SEND_TO_WU_NODE							  = "work_unit";
    const std::string SEND_TO_WU_ID_NODE						  = "id";
    const std::string SEND_TO_WU_ACTIVATED_NODE					  = "activated";
    const std::string SEND_TO_WU_ACTIVATION_INFO_NODE			  = "activation_info";

    const std::string SEND_TO_TARGET_NODE						  = "target";
    const std::string SEND_TO_TARGET_ID_NODE					  = "id";
    const std::string SEND_TO_TARGET_NAME_NODE					  = "name";
    const std::string SEND_TO_TARGET_DELETE_ON_SUCCESS_NODE		  = "delete_on_success";
    const std::string SEND_TO_TARGET_DEFAULT_NODE				  = "default";	
    const std::string SEND_TO_TARGET_FRM_ID_NODE				  = "frm_id";
    const std::string SEND_TO_TARGET_FRM_PARAMS_NODE			  = "frm_params";
    const std::string SEND_TO_TARGET_DST_ID_NODE				  = "dst_id";
    const std::string SEND_TO_TARGET_DST_PARAMS_NODE			  = "dst_params";
    const std::string SEND_TO_TARGET_USE_COMSKIP_NODE		      = "use_comskip";
    const std::string SEND_TO_TARGET_COMSKIP_PARAMS_NODE		  = "comskip_params";

    const std::string SEND_TO_COMSKIP_SETTINGS                    = "comskip_settings";
    const std::string SEND_TO_COMSKIP_PARAMS                      = "comskip_params";
    const std::string SEND_TO_COMSKIP_ENABLED                     = "comskip_enabled";
    const std::string SEND_TO_COMSKIP_DEFAULT_PARAMS              = "comskip_default_params";

//------------------------------------------------------------------

	inline void write_wu(pugi::xml_node& node, const send_to_work_unit_info& wu)
    {
		std::string str;
		dvblink::pugixml_helpers::new_child(node, SEND_TO_WU_ID_NODE, wu.id);
		str = boost::lexical_cast<std::string>(wu.is_activated);
		dvblink::pugixml_helpers::new_child(node, SEND_TO_WU_ACTIVATED_NODE, str);
		dvblink::pugixml_helpers::new_child(node, SEND_TO_WU_ACTIVATION_INFO_NODE, wu.activation_info.get());
    }

    inline void read_wu(pugi::xml_node& node, send_to_work_unit_info& wu)
    {
		std::string str;

		dvblink::pugixml_helpers::get_node_value(node, SEND_TO_WU_ID_NODE, wu.id);

		if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_WU_ACTIVATED_NODE, str))
			wu.is_activated = boost::lexical_cast<bool>(str);

		if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_WU_ACTIVATION_INFO_NODE, str))
			wu.activation_info = str;
    }

//------------------------------------------------------------------

	inline void write_target(pugi::xml_node& node, const send_to_target_info& target)
    {
		std::string str;
		dvblink::pugixml_helpers::new_child(node, SEND_TO_TARGET_ID_NODE, target.id.to_string());
		dvblink::pugixml_helpers::new_child(node, SEND_TO_TARGET_NAME_NODE, target.name);
		str = boost::lexical_cast<std::string>(target.delete_on_success);
		dvblink::pugixml_helpers::new_child(node, SEND_TO_TARGET_DELETE_ON_SUCCESS_NODE, str);
		str = boost::lexical_cast<std::string>(target.use_comskip);
		dvblink::pugixml_helpers::new_child(node, SEND_TO_TARGET_USE_COMSKIP_NODE, str);
        pugi::xml_node comskip_params_node = dvblink::pugixml_helpers::new_child(node, SEND_TO_TARGET_COMSKIP_PARAMS_NODE);
        if (comskip_params_node != NULL)
            write_to_node(target.comskip_params, comskip_params_node);
        dvblink::pugixml_helpers::new_child(node, SEND_TO_TARGET_FRM_ID_NODE, target.formatter_info.id);
		dvblink::pugixml_helpers::new_child(node, SEND_TO_TARGET_FRM_PARAMS_NODE, target.formatter_info.params.get());
		dvblink::pugixml_helpers::new_child(node, SEND_TO_TARGET_DST_ID_NODE, target.dest_info.id);
		dvblink::pugixml_helpers::new_child(node, SEND_TO_TARGET_DST_PARAMS_NODE, target.dest_info.params.get());
		str = boost::lexical_cast<std::string>(target.default_);
		dvblink::pugixml_helpers::new_child(node, SEND_TO_TARGET_DEFAULT_NODE, str);
    }

    inline void read_target(pugi::xml_node& node, send_to_target_info& target)
    {
		std::string str;

		if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_TARGET_ID_NODE, str))
			target.id = str;

		dvblink::pugixml_helpers::get_node_value(node, SEND_TO_TARGET_NAME_NODE, target.name);

		if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_TARGET_DELETE_ON_SUCCESS_NODE, str))
			target.delete_on_success = boost::lexical_cast<bool>(str);

		if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_TARGET_USE_COMSKIP_NODE, str))
            target.use_comskip = boost::lexical_cast<bool>(str);

        pugi::xml_node comskip_params_node = node.child(SEND_TO_TARGET_COMSKIP_PARAMS_NODE.c_str());
        if (comskip_params_node != NULL)
            read_from_node(comskip_params_node, target.comskip_params);

		dvblink::pugixml_helpers::get_node_value(node, SEND_TO_TARGET_FRM_ID_NODE, target.formatter_info.id);

		if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_TARGET_FRM_PARAMS_NODE, str))
			target.formatter_info.params = str;

		dvblink::pugixml_helpers::get_node_value(node, SEND_TO_TARGET_DST_ID_NODE, target.dest_info.id);

		if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_TARGET_DST_PARAMS_NODE, str))
			target.dest_info.params = str;

		if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_TARGET_DEFAULT_NODE, str))
			target.default_ = boost::lexical_cast<bool>(str);
	}

//------------------------------------------------------------------

    class send_to_get_formatters_response
    {
    public:
        send_to_get_formatters_response() {}

		send_to_work_unit_info_list_t formatters_;
    };
    
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_get_formatters_response& response)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_TARGET_CMD_GET_FORMATTERS.c_str());

        if (node != NULL)
        {
		    for (size_t i=0; i<response.formatters_.size(); i++)
		    {
                pugi::xml_node wu_node = dvblink::pugixml_helpers::new_child(node, SEND_TO_WU_NODE);

                if (wu_node != NULL)
    			    write_wu(wu_node, response.formatters_[i]);
		    }
        }

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_get_formatters_response& response)
    {
        if (NULL != node)
        {                       
            pugi::xml_node wu_node = node.first_child();
            while (NULL != wu_node)                
            {
				send_to_work_unit_info wu;
				read_wu(wu_node, wu);
				response.formatters_.push_back(wu);

                wu_node = wu_node.next_sibling();
            }
        }

        return node;
    }

//------------------------------------------------------------------

    class send_to_get_destinations_response
    {
    public:
        send_to_get_destinations_response() {}

		send_to_work_unit_info_list_t destinations_;
    };
  
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_get_destinations_response& response)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_TARGET_CMD_GET_DESTINATIONS.c_str());

        if (node != NULL)
        {
		    for (size_t i=0; i<response.destinations_.size(); i++)
		    {
                pugi::xml_node wu_node = dvblink::pugixml_helpers::new_child(node, SEND_TO_WU_NODE);
                if (wu_node != NULL)
    			    write_wu(wu_node, response.destinations_[i]);
		    }
        }

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_get_destinations_response& response)
    {
        if (NULL != node)
        {                       
            pugi::xml_node wu_node = node.first_child();
            while (NULL != wu_node)                
            {
				send_to_work_unit_info wu;
				read_wu(wu_node, wu);
				response.destinations_.push_back(wu);

                wu_node = wu_node.next_sibling();
            }
        }

        return node;
    }

//------------------------------------------------------------------

    class send_to_activate_work_unit_request
    {
    public:
        send_to_activate_work_unit_request() {}

		send_to_work_unit_id wu_id_;
		dvblink::xml_string_t activation_info_;
    };
    
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_activate_work_unit_request& request)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_TARGET_CMD_ACTIVATE_WORK_UNIT.c_str());

        if (node != NULL)
        {
		    dvblink::pugixml_helpers::new_child(node, SEND_TO_WU_ID_NODE, request.wu_id_);
		    dvblink::pugixml_helpers::new_child(node, SEND_TO_WU_ACTIVATION_INFO_NODE, request.activation_info_.get());
        }        

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_activate_work_unit_request& request)
    {
        if (NULL != node)
        {                       
			std::string str;
			dvblink::pugixml_helpers::get_node_value(node, SEND_TO_WU_ID_NODE, request.wu_id_);

            if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_WU_ACTIVATION_INFO_NODE, str))
				request.activation_info_ = str;
        }

        return node;
    }

//------------------------------------------------------------------

    class send_to_get_targets_response
    {
    public:
        send_to_get_targets_response() {}

		send_to_target_list_t targets_;
    };
    
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_get_targets_response& response)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_TARGET_CMD_GET_TARGETS.c_str());

        if (node != NULL)
        {
		    for (size_t i=0; i<response.targets_.size(); i++)
		    {
                pugi::xml_node tgt_node = dvblink::pugixml_helpers::new_child(node, SEND_TO_TARGET_NODE);
                if (tgt_node != NULL)
    			    write_target(tgt_node, response.targets_[i]);
		    }
        }

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_get_targets_response& response)
    {
        if (NULL != node)
        {                       
            pugi::xml_node tgt_node = node.first_child();
            while (NULL != tgt_node)                
            {
				send_to_target_info target;
				read_target(tgt_node, target);
				response.targets_.push_back(target);

                tgt_node = tgt_node.next_sibling();
            }
        }

        return node;
    }

//------------------------------------------------------------------

    class send_to_set_targets_request
    {
    public:
        send_to_set_targets_request() {}

		send_to_target_list_t targets_;
    };
    
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_set_targets_request& request)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_TARGET_CMD_SET_TARGETS.c_str());

        if (node != NULL)
        {
		    for (size_t i=0; i<request.targets_.size(); i++)
		    {
                pugi::xml_node tgt_node = dvblink::pugixml_helpers::new_child(node, SEND_TO_TARGET_NODE);
                if (tgt_node != NULL)
			        write_target(tgt_node, request.targets_[i]);
		    }
        }

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_set_targets_request& request)
    {
        if (NULL != node)
        {                       
            pugi::xml_node tgt_node = node.first_child();
            while (NULL != tgt_node)                
            {
				send_to_target_info target;
				read_target(tgt_node, target);
				request.targets_.push_back(target);

                tgt_node = tgt_node.next_sibling();
            }
        }

        return node;
    }

//------------------------------------------------------------------

    class send_to_get_comskip_settings_request
    {
    public:
        send_to_get_comskip_settings_request() {}

        concise_param_map_t params_;
    };
    
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_get_comskip_settings_request& request)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_TARGET_CMD_GET_COMSKIP_SETTINGS.c_str());

        if (node != NULL)
        {
            pugi::xml_node params_node = dvblink::pugixml_helpers::new_child(node, SEND_TO_COMSKIP_PARAMS);
            if (params_node != NULL)
                write_to_node(request.params_, params_node);
        }

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_get_comskip_settings_request& request)
    {
        if (NULL != node)
        {                       
            pugi::xml_node params_node = node.child(SEND_TO_COMSKIP_PARAMS.c_str());
            if (NULL != params_node)                
                read_from_node(params_node, request.params_);
        }

        return node;
    }

//------------------------------------------------------------------

    class send_to_get_comskip_settings_response
    {
    public:
        send_to_get_comskip_settings_response() :
            comskip_enabled_(false)
        {}

        bool comskip_enabled_;
        parameters_container_t settings_;
        concise_param_map_t default_params_;
    };
    
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_get_comskip_settings_response& request)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_TARGET_CMD_GET_COMSKIP_SETTINGS.c_str());

        if (node != NULL)
        {
            std::string str = boost::lexical_cast<std::string>(request.comskip_enabled_);
		    dvblink::pugixml_helpers::new_child(node, SEND_TO_COMSKIP_ENABLED, str);

            pugi::xml_node settings_node = dvblink::pugixml_helpers::new_child(node, SEND_TO_COMSKIP_SETTINGS);
            if (settings_node != NULL)
                write_to_node(request.settings_, settings_node);

            pugi::xml_node default_params_node = dvblink::pugixml_helpers::new_child(node, SEND_TO_COMSKIP_DEFAULT_PARAMS);
            if (default_params_node != NULL)
                write_to_node(request.default_params_, default_params_node);
        }

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_get_comskip_settings_response& request)
    {
        if (NULL != node)
        {                       
            std::string str;
            if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_COMSKIP_ENABLED, str))
                request.comskip_enabled_ = boost::lexical_cast<bool>(str);

            pugi::xml_node settings_node = node.child(SEND_TO_COMSKIP_SETTINGS.c_str());
            if (NULL != settings_node)
            {
                pugi::xml_node container_node = settings_node.child(params_container_node.c_str());
                read_from_node(container_node, request.settings_);
            }

            pugi::xml_node default_params_node = node.child(SEND_TO_COMSKIP_DEFAULT_PARAMS.c_str());
            if (NULL != default_params_node)
                read_from_node(default_params_node, request.default_params_);
        }

        return node;
    }

}
