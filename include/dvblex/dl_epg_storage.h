/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <dli_server.h>
#include <dl_message_queue.h>
#include <dl_message_epg.h>
#include <dl_message_common.h>
#include <dl_types.h>
#include <dl_filesystem_path.h>
#include <dl_epg_channels.h>
#include <dl_event.h>
#include <dl_parameters.h>
#include <dl_gzip_string_container.h>

#include <boost/thread.hpp>

namespace dvblex { 

class epg_storage_t
{
    class message_handler : 
        public dvblink::messaging::epg::force_epg_update_request::subscriber,
        public dvblink::messaging::epg::enable_periodic_epg_updates_request::subscriber,
        public dvblink::messaging::epg::get_channel_epg_request::subscriber,
        public dvblink::messaging::epg::get_epg_channels_request::subscriber,
        public dvblink::messaging::xml_message_request::subscriber
    {
    public:
        message_handler(epg_storage_t* epg_storage, dvblink::messaging::message_queue_t message_queue) :
            dvblink::messaging::epg::force_epg_update_request::subscriber(message_queue),
            dvblink::messaging::epg::enable_periodic_epg_updates_request::subscriber(message_queue),
            dvblink::messaging::epg::get_channel_epg_request::subscriber(message_queue),
            dvblink::messaging::epg::get_epg_channels_request::subscriber(message_queue),
            dvblink::messaging::xml_message_request::subscriber(message_queue),
            epg_storage_(epg_storage),
            message_queue_(message_queue)
        {
        }

        ~message_handler()
        {
        }

    protected:
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_channel_epg_request& request, dvblink::messaging::epg::get_channel_epg_response& response)
        {
            epg_storage_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_epg_channels_request& request, dvblink::messaging::epg::get_epg_channels_response& response)
        {
            epg_storage_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::enable_periodic_epg_updates_request& request)
        {
            epg_storage_->handle(sender, request);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::force_epg_update_request& request)
        {
            epg_storage_->handle(sender, request);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response)
        {
            epg_storage_->handle(sender, request, response);
        }

    private:
        epg_storage_t* epg_storage_;
        dvblink::messaging::message_queue_t message_queue_;
    };

    typedef std::map<dvblink::epg_channel_id_t, dvblink::engine::gzip_string_container> memory_epg_storage_map_t;

public:
    epg_storage_t(const dvblink::epg_source_id_t& id, const dvblink::epg_source_name_t& name, 
        const dvblink::i_server_t& server, const dvblink::filesystem_path_t& disk_path);

    epg_storage_t(dvblink::messaging::message_queue_t& message_queue, const dvblink::epg_source_name_t& name, const dvblink::filesystem_path_t& disk_path);

    virtual ~epg_storage_t();

    virtual bool start();
    bool is_started();
    void stop();

    void refresh() {force_refresh_.signal();}

    bool get_epg_channels(epg_source_channel_map_t& epg_channel_map);
    bool get_epg_for_channel(const dvblink::epg_channel_id_t& channel_id, dvblink::engine::DLEPGEventList& events);

    dvblink::epg_source_id_t get_id(){return id_;}
    virtual dvblink::epg_source_name_t get_name(){return name_;}

    virtual bool do_prefetch(epg_source_channel_map_t& epg_channel_map){return false;}
    virtual bool download_epg(){return false;}
    virtual void cleanup(){}
    virtual bool get_channel_epg(const dvblink::epg_channel_id_t& channel_id, dvblink::engine::DLEPGEventList& events){return false;}
    virtual time_t get_next_update_time(bool bsuccess){return 0;}
    virtual time_t get_first_update_delay_sec();

    virtual bool get_source_settings(dvblex::parameters_container_t& settings){return false;}
    virtual bool set_source_settings(const dvblex::concise_param_map_t& settings){return false;}
protected:
    dvblink::messaging::message_queue_t message_queue_;
    std::auto_ptr<message_handler> message_handler_;
    dvblink::i_server_t server_;
    dvblink::filesystem_path_t epg_storage_path_;
    dvblink::filesystem_path_t schedules_path_;
    dvblink::filesystem_path_t channels_file_path_;
    boost::thread* update_thread_;
    bool exit_flag_;
    boost::mutex lock_;
    dvblink::epg_source_id_t id_;
    dvblink::epg_source_name_t name_;
    dvblink::bool_flag_t has_settings_;
    dvblink::event force_refresh_;
    bool periodic_updates_enabled_;

    //memory storage
    bool use_memory_storage_;
    epg_source_channel_map_t memory_channel_map_storage_;
    memory_epg_storage_map_t memory_epg_storage_map_;

    void update_thread_func();

    std::string get_encoded_channel_id(const dvblink::epg_channel_id_t& channel_id);
    dvblink::epg_channel_id_t get_decoded_channel_id(const std::string& encoded_channel_id);
    bool store_channel_schedule(const dvblink::epg_channel_id_t& id, dvblink::engine::DLEPGEventList& schedule);
    bool register_source();
    bool unregister_source();
    void notify_epg_updated();
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_channel_epg_request& request, dvblink::messaging::epg::get_channel_epg_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_epg_channels_request& request, dvblink::messaging::epg::get_epg_channels_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::force_epg_update_request& request);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::enable_periodic_epg_updates_request& request);
    void remove_directory_content(const dvblink::filesystem_path_t& dir);
private:
    bool write_channels_file(const epg_source_channel_map_t& channels, std::vector<dvblink::epg_channel_id_t>& non_existent_channels);
    bool read_channels_file(const dvblink::filesystem_path_t& f, epg_source_channel_map_t& channels);
    void delete_non_existent_channels(const std::vector<dvblink::epg_channel_id_t>& non_existent_channels);
};

}
