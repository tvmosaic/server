/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <boost/algorithm/string.hpp> 
#include <dl_types.h>
#include <dl_pugixml_helper.h>
#include <dl_parameters.h>

namespace dvblex { 

const std::string params_container_node               = "container";
const std::string params_edit_node                    = "edit";
const std::string params_selection_node               = "selection";
const std::string params_format_node                    = "format";
const std::string params_id_node                       = "id";
const std::string params_name_node                     = "name";
const std::string params_desc_node                    = "desc";
const std::string params_value_node                   = "value";

static const char* params_edit_format_strings[] = {"string", "number"};

inline pugi::xml_node write_to_node(const parameters_container_t& container, pugi::xml_node node)
{
    std::stringstream buf;

    //editable parameters container
    pugi::xml_node container_node = dvblink::pugixml_helpers::new_child(node, params_container_node);
    if (container_node != NULL)
    {
        dvblink::pugixml_helpers::add_node_attribute(container_node, params_id_node, container.id_.get());
        dvblink::pugixml_helpers::add_node_attribute(container_node, params_name_node, container.name_.get());
        dvblink::pugixml_helpers::add_node_attribute(container_node, params_desc_node, container.desc_.get());

        //edit boxes
        editable_param_list_t::const_iterator edit_it = container.editable_params_.begin();
        while (edit_it != container.editable_params_.end())
        {
            pugi::xml_node edit_node = dvblink::pugixml_helpers::new_child(container_node, params_edit_node, edit_it->value_);
            if (edit_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(edit_node, params_id_node, edit_it->key_.get());
                dvblink::pugixml_helpers::add_node_attribute(edit_node, params_name_node, edit_it->name_.get());
                dvblink::pugixml_helpers::add_node_attribute(edit_node, params_format_node, params_edit_format_strings[edit_it->format_]);
            }
            ++edit_it;
        }
        //select boxes
        selectable_param_desc_list_t::const_iterator select_it = container.selectable_params_.begin();
        while (select_it != container.selectable_params_.end())
        {
            buf.clear(); buf.str("");
            buf << select_it->selected_param_.get();

            pugi::xml_node select_node = dvblink::pugixml_helpers::new_child(container_node, params_selection_node, buf.str());
            if (select_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(select_node, params_id_node, select_it->key_.get());
                dvblink::pugixml_helpers::add_node_attribute(select_node, params_name_node, select_it->name_.get());

                for (size_t i=0; i<select_it->parameters_list_.size(); i++)
                {
                    pugi::xml_node value_node = dvblink::pugixml_helpers::new_child(select_node, params_value_node);
                    if (value_node != NULL)
                    {
                        dvblink::pugixml_helpers::add_node_attribute(value_node, params_id_node, select_it->parameters_list_[i].value_.get());
                        dvblink::pugixml_helpers::add_node_attribute(value_node, params_name_node, select_it->parameters_list_[i].name_.get());
                    }
                }
            }
            ++select_it;
        }
    }
    return container_node;
}

inline void read_from_node(const pugi::xml_node node, parameters_container_t& container)
{
    std::string str;
    if (node != NULL)
    {
        if (dvblink::pugixml_helpers::get_node_attribute(node, params_id_node, str))
            container.id_ = str;

        if (dvblink::pugixml_helpers::get_node_attribute(node, params_name_node, str))
            container.name_ = str;

        if (dvblink::pugixml_helpers::get_node_attribute(node, params_desc_node, str))
            container.desc_ = str;

        pugi::xml_node element_node = node.first_child();
        while (element_node != NULL)
        {
            if (boost::iequals(element_node.name(), params_edit_node))
            {
                editable_param_element_t edit_element;

                edit_element.value_ = element_node.child_value();

                if (dvblink::pugixml_helpers::get_node_attribute(element_node, params_id_node, str))
                    edit_element.key_ = str;

                if (dvblink::pugixml_helpers::get_node_attribute(element_node, params_name_node, str))
                    edit_element.name_ = str;

                if (dvblink::pugixml_helpers::get_node_attribute(element_node, params_format_node, str))
                {
                    for (size_t i=0; i<sizeof(params_edit_format_strings) / sizeof(char*); i++)
                    {
                        if (boost::iequals(params_edit_format_strings[i], str))
                            edit_element.format_ = (editable_param_format_e)i;
                    }
                }

                container.editable_params_.push_back(edit_element);
            }

            if (boost::iequals(element_node.name(), params_selection_node))
            {
                selectable_param_description_t selectable_element;

                boost::int32_t i32v;
                dvblink::engine::string_conv::apply(element_node.child_value(), i32v, (boost::int32_t)0);
                selectable_element.selected_param_ = i32v;

                if (dvblink::pugixml_helpers::get_node_attribute(element_node, params_id_node, str))
                    selectable_element.key_ = str;

                if (dvblink::pugixml_helpers::get_node_attribute(element_node, params_name_node, str))
                    selectable_element.name_ = str;

                pugi::xml_node value_node = element_node.first_child();
                while (value_node != NULL)
                {
                    if (boost::iequals(value_node.name(), params_value_node))
                    {
                        selectable_param_element_t value_param;

                        if (dvblink::pugixml_helpers::get_node_attribute(value_node, params_id_node, str))
                            value_param.value_ = str;

                        if (dvblink::pugixml_helpers::get_node_attribute(value_node, params_name_node, str))
                            value_param.name_ = str;

                        selectable_element.parameters_list_.push_back(value_param);
                    }

                    value_node = value_node.next_sibling();
                }


                container.selectable_params_.push_back(selectable_element);
            }

            element_node = element_node.next_sibling();
        }

    }
}

static const std::string params_param_node                     = "param";

inline pugi::xml_node write_to_node(const concise_param_map_t& params, pugi::xml_node node)
{
    if (node != NULL)
    {
        concise_param_map_t::const_iterator cp_it = params.begin();
        while (cp_it != params.end())
        {
            pugi::xml_node param_node = dvblink::pugixml_helpers::new_child(node, params_param_node, cp_it->second.get());
            if (param_node != NULL)
                dvblink::pugixml_helpers::add_node_attribute(param_node, params_id_node, cp_it->first.get());

            ++cp_it;
        }
    }

    return node;
}

inline void read_from_node(const pugi::xml_node node, concise_param_map_t& params)
{
    std::string str;
    if (node != NULL)
    {
        pugi::xml_node param_node = node.first_child();
        while (param_node != NULL)
        {
            if (boost::iequals(param_node.name(), params_param_node))
            {
                if (dvblink::pugixml_helpers::get_node_attribute(param_node, params_id_node, str))
                    params[str] = param_node.child_value();
            }

            param_node = param_node.next_sibling();
        }
    }
}

} //dvblex
