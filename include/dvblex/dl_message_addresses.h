/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_types.h>

namespace dvblink { namespace messaging {

static dvblink::message_addressee_t server_message_queue_addressee("10befee1-10dd-404c-8873-8f78336161e3");
static dvblink::message_addressee_t source_manager_message_queue_addressee("bb6df768-6fa8-4f39-bc1c-2a4c06b051c1");
static dvblink::message_addressee_t network_server_message_queue_addressee("c34e397a-d055-41f9-b2e2-f051ce9daeb8");
static dvblink::message_addressee_t recorder_message_queue_addressee("0bb1fa3a-8763-43e6-9b2d-23abc74a545");
static dvblink::message_addressee_t epg_manager_message_queue_addressee("e96f83f5-79ad-4ea4-af2b-6682f233ad1a");
static dvblink::message_addressee_t favorites_message_queue_addressee("98e85737-510c-404a-985c-3b97a04d8236");
static dvblink::message_addressee_t tvadviser_message_queue_addressee("284cb518-d074-456d-949c-1423f9023e0d");
static dvblink::message_addressee_t xmltv_message_queue_addressee("90ce1309-71f4-4706-9b8f-aa18f0aee5b2");
static dvblink::message_addressee_t logo_manager_message_queue_addressee("13868c28-a9ad-4432-a90d-c4f161b57b99");
static dvblink::message_addressee_t recorded_tv_message_queue_addressee("8f94b459-efc0-4d91-9b29-ec3d72e92677");
static dvblink::message_addressee_t social_message_queue_addressee("a8b42f8e-6a08-418a-8917-e767f766d576");

} //dvblink
} //messaging
