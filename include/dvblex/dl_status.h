/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#define DO_STATUS_ENUM(e, _value)       DvbLink_##e = (_value),

#define DEF_STATUS(DEF)                      \
    DEF(StatusOk,                       0)   \
    DEF(StatusError,                    1000)\
    DEF(StatusInvalidData,              1001)\
    DEF(StatusInvalidParam,             1002)\
    DEF(StatusNotImplemented,           1003)\
    DEF(StatusSocketError,              1004)\
    DEF(StatusMCNotRunning,             1005)\
    DEF(StatusNoDefaultRecorder,        1006)\
    DEF(StatusBufferTooSmall,           1007)\
    DEF(StatusMCEConnectionError,       1008)\
    DEF(StatusAdresseeNotFound,         1009)\
    DEF(StatusTimeout,                  1010)\
    DEF(StatusHandlerNotRegistered,     1011) \
    DEF(StatusNotActivated,             1012) \
    DEF(StatusNoFreeTuner,              1013)

namespace dvblink { 

enum EStatus
{
    DEF_STATUS(DO_STATUS_ENUM)
};

inline bool STATUS_SUCCESS(EStatus Status) {return Status == DvbLink_StatusOk;}

} //dvblink

