/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <sstream>
#include <dl_types.h>
#include <dl_network_helper.h>
#include <dl_pugixml_helper.h>

namespace dvblex { 

const std::string get_dlna_params_cmd              = "get_dlna_params";
const std::string set_dlna_params_cmd              = "set_dlna_params";

struct dlna_params
{
    dlna_params() :
        is_timeshift_enabled_(false), is_dlna_disabled_(false)
    {}

    std::string audio_track_;
    std::string network_adapter_;
    bool is_timeshift_enabled_;
    bool is_dlna_disabled_;
    dvblink::TNetworkAdaptersInfo network_adapters_;
};

const std::string dlna_params_root_node              = "dlna_params";
const std::string dlna_params_audio_track_node          = "audio_track";
const std::string dlna_params_network_adapter_node          = "network_adapter";
const std::string dlna_params_timeshift_enabled_node          = "timeshift_enabled";
const std::string dlna_params_dlna_disabled_node          = "dlna_disabled";
const std::string dlna_params_adapters_node          = "adapters";
const std::string dlna_params_adapter_node          = "adapter";
const std::string dlna_params_adapter_id_node          = "id";
const std::string dlna_params_adapter_name_node          = "name";
const std::string dlna_params_adapter_desc_node          = "desc";
const std::string dlna_params_adapter_addr_node          = "addr";

inline pugi::xml_node& operator >> (pugi::xml_node& node, dlna_params& dp)
{
    if (NULL != node)
    {       
        std::string str;

        dvblink::pugixml_helpers::get_node_value(node, dlna_params_audio_track_node, dp.audio_track_);
        dvblink::pugixml_helpers::get_node_value(node, dlna_params_network_adapter_node, dp.network_adapter_);
        dp.is_timeshift_enabled_ = dvblink::pugixml_helpers::get_node_value(node, dlna_params_timeshift_enabled_node, str);
        dp.is_dlna_disabled_ = dvblink::pugixml_helpers::get_node_value(node, dlna_params_dlna_disabled_node, str);

        pugi::xml_node adapters_node = node.child(dlna_params_adapters_node.c_str());
        if (adapters_node != NULL)
        {
            pugi::xml_node adapter_node = adapters_node.first_child();
            while (adapter_node != NULL)
            {
                dvblink::SNetworkAdaptersInfo nai;
                dvblink::pugixml_helpers::get_node_attribute(adapter_node, dlna_params_adapter_id_node, nai.m_strGUID);
                dvblink::pugixml_helpers::get_node_attribute(adapter_node, dlna_params_adapter_name_node, nai.m_strName);
                dvblink::pugixml_helpers::get_node_attribute(adapter_node, dlna_params_adapter_desc_node, nai.m_strDescription);
                dvblink::pugixml_helpers::get_node_attribute(adapter_node, dlna_params_adapter_addr_node, nai.m_strAddress);

                dp.network_adapters_.push_back(nai);

                adapter_node = adapter_node.next_sibling();
            }
        }
    }
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const dlna_params& dp)
{
    pugi::xml_node node = doc.append_child(dlna_params_root_node.c_str());
    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, dlna_params_audio_track_node, dp.audio_track_);
        dvblink::pugixml_helpers::new_child(node, dlna_params_network_adapter_node, dp.network_adapter_);
        if (dp.is_timeshift_enabled_)
            dvblink::pugixml_helpers::new_child(node, dlna_params_timeshift_enabled_node, dvblink::pugixml_helpers::xmlnode_value_true);
        if (dp.is_dlna_disabled_)
            dvblink::pugixml_helpers::new_child(node, dlna_params_dlna_disabled_node, dvblink::pugixml_helpers::xmlnode_value_true);

        pugi::xml_node adapters_node = dvblink::pugixml_helpers::new_child(node, dlna_params_adapters_node);
        if (adapters_node != NULL)
        {
            for (size_t i=0; i<dp.network_adapters_.size(); i++)
            {
                const dvblink::SNetworkAdaptersInfo& nai = dp.network_adapters_[i];
                pugi::xml_node adapter_node = dvblink::pugixml_helpers::new_child(adapters_node, dlna_params_adapter_node);
                if (adapter_node != NULL)
                {
                    dvblink::pugixml_helpers::add_node_attribute(adapter_node, dlna_params_adapter_id_node, nai.m_strGUID);
                    dvblink::pugixml_helpers::add_node_attribute(adapter_node, dlna_params_adapter_name_node, nai.m_strName);
                    dvblink::pugixml_helpers::add_node_attribute(adapter_node, dlna_params_adapter_desc_node, nai.m_strDescription);
                    dvblink::pugixml_helpers::add_node_attribute(adapter_node, dlna_params_adapter_addr_node, nai.m_strAddress);
                }
            }
        }
    }
    return doc;
}

} //dvblex
