/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_TYPES_H_
#define __DVBLINK_DL_TYPES_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#undef _DEBUG_ID

#include <sstream>
#include <boost/cstdint.hpp>
#include <boost/uuid/uuid_serialize.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/base_object.hpp>
#include <dl_uuid.h>
#include <dl_base_types.h>
#include <dl_strings.h>

#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable:4996)
#endif

namespace dvblink {

template <typename STRING, int n> class base_type_str_t : public base_type_t<STRING, n>
{
    typedef base_type_t<STRING, n> base_t;
public:
    typedef STRING string_t;

    base_type_str_t() {}
    base_type_str_t(const base_type_str_t& id) : base_type_t<typename base_t::type_t, n>(id) {}

    base_type_str_t(const string_t& id) : base_t(id)                        {}
    base_type_str_t(const typename string_t::value_type* sz) : base_t(sz)   {}

    bool empty() const {return base_t::id_.empty();}
    void clear() {base_t::id_.clear();}

    base_type_str_t& operator += (const base_type_str_t& wid)               {base_t::id_ += wid.id_; return *this;}
    base_type_str_t& operator += (const string_t& id)                       {base_t::id_ += id; return *this;}
    base_type_str_t& operator += (const typename string_t::value_type* id)  {base_t::id_ += id; return *this;}

    const typename string_t::value_type* c_str() const                      {return base_t::id_.c_str();}
    void resize(size_t new_size)                                            {base_t::id_.resize(new_size);}

    void operator = (const typename string_t::value_type* id)               {base_t::id_ = id;}
    void operator = (const string_t& id)                                    {base_t::id_ = id;}
};


template <int n> class base_type_wstring_t : public base_type_str_t<std::wstring, n>
{
    typedef base_type_str_t<std::wstring, n> base_t;
public:
    base_type_wstring_t() : base_t() {}
    base_type_wstring_t(const base_type_wstring_t& id) : base_t(id) {}

    base_type_wstring_t(const std::wstring& id) : base_t(id) {}
    base_type_wstring_t(const wchar_t* sz) : base_t(sz) {}

    base_type_wstring_t(const std::string& id) : base_t(engine::string_cast<engine::EC_UTF8>(id)) {}
    base_type_wstring_t(const char* sz) : base_t(engine::string_cast<engine::EC_UTF8>(std::string(sz))) {}

    void operator = (const std::string& id) {base_t::id_ = engine::string_cast<engine::EC_UTF8>(id);}
    void operator = (const char* id) {base_t::id_ = engine::string_cast<engine::EC_UTF8>(std::string(id));}

    std::wstring to_wstring() const {return base_t::id_;}
    std::string to_string() const {return engine::string_cast<engine::EC_UTF8>(base_t::id_);}

private:
    friend class boost::serialization::access;

    template<class Archive>
    void save(Archive & ar, const unsigned int /*version*/) const
    {
        std::string utf8str = to_string();
        ar & utf8str;
    }

    template<class Archive>
    void load(Archive & ar, const unsigned int /*version*/)
    {
        std::string utf8;
        ar & utf8;
        base_t::id_ = engine::string_cast<engine::EC_UTF8>(utf8);
    }

    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

template <int n> class base_type_string_t : public base_type_str_t<std::string, n>
{
    typedef base_type_str_t<std::string, n> base_t;

public:
    base_type_string_t() : base_t() {}
    base_type_string_t(const base_type_string_t& id) : base_t(id) {}

    base_type_string_t(const std::string& id) : base_t(id) {}
    base_type_string_t(const char* sz) : base_t(sz) {}

    base_type_string_t(const std::wstring& id) : base_t(engine::string_cast<engine::EC_UTF8>(id)) {}
    base_type_string_t(const wchar_t* sz) : base_t(engine::string_cast<engine::EC_UTF8>(std::wstring(sz))) {}

    void operator = (const std::wstring& id) {base_t::id_ = engine::string_cast<engine::EC_UTF8>(id);}
    void operator = (const wchar_t* sz) {base_t::id_ = engine::string_cast<engine::EC_UTF8>(std::wstring(sz));}

    std::string to_string() const {return base_t::id_;}
    std::wstring to_wstring() const {return engine::string_cast<engine::EC_UTF8>(base_t::id_);}

private: 
    friend class boost::serialization::access;

    template <class Archive>
    void serialize(Archive& ar, unsigned int /*file_version*/)
    {
        ar & base_t::id_;
    }
};

template <int n>
inline base_type_string_t<n> operator + (const base_type_string_t<n>& lhs, const base_type_string_t<n>& rhs)
{
    base_type_string_t<n> tmp(lhs);
    tmp += rhs;
    return tmp;
}

template <int n>
inline base_type_wstring_t<n> operator + (const base_type_wstring_t<n>& lhs, const base_type_wstring_t<n>& rhs)
{
    base_type_wstring_t<n> tmp(lhs);
    tmp += rhs;
    return tmp;
}

template <int n> class base_type_uuid_t : public base_type_t<boost::uuids::uuid, n>
{
    typedef base_type_t<boost::uuids::uuid, n> base_t;
public:
    base_type_uuid_t() :
        base_t()
    {
         base_t::id_ = boost::uuids::nil_uuid();
    }
    base_type_uuid_t(const base_type_uuid_t& id) :
        base_t(id.id_)
    {
#ifdef _DEBUG_ID
        set(id.id_);
#endif
    }
    base_type_uuid_t(const typename base_t::type_t& id) :
        base_t(id)
    {
#ifdef _DEBUG_ID
        set(id);
#endif
    }

    base_type_uuid_t(const std::string& sid) {set(sid);}
    base_type_uuid_t(const std::wstring& sid) {set(sid);}
    base_type_uuid_t(const char* sid) {set(sid);}
    base_type_uuid_t(const wchar_t* sid) {set(sid);}

    bool empty() const {return base_t::id_.is_nil();}
    void clear() {memset(base_t::id_.data, 0, base_t::id_.size());}

    std::string to_string() const {return boost::uuids::to_string(base_t::id_);}
    std::wstring to_wstring() const {return boost::uuids::to_wstring(base_t::id_);}

    void operator = (const base_type_uuid_t<n>& id) {set(id.id_);}
    void operator = (const char* id) {set(id);}
    void operator = (const std::string& id) {set(id);}
    void operator = (const wchar_t* id) {set(id);}
    void operator = (const std::wstring& id) {set(id);}

    void set(const std::string& sid) {set(sid.c_str());}
    void set(const std::wstring& sid)  {set(sid.c_str());}
    void set(const typename base_t::type_t& id)
    {
        base_t::id_ = id;
#ifdef _DEBUG_ID
        std::stringstream ss;
        ss << id;
        memcpy(dbg_id_, ss.str().c_str(), sizeof(dbg_id_));
#endif
    }

    void set(const char* sid)
    {
        std::stringstream stream_uuid;
        stream_uuid << sid;
        stream_uuid >> base_t::id_;
#ifdef _DEBUG_ID
        memcpy(dbg_id_, sid, sizeof(dbg_id_));
#endif
    }

    void set(const wchar_t* sid)
    {
        std::wstringstream stream_uuid;
        stream_uuid << sid;
        stream_uuid >> base_t::id_;
#ifdef _DEBUG_ID
        memcpy(dbg_id_, engine::string_cast<engine::EC_UTF8>(sid).c_str(), sizeof(dbg_id_));
#endif
    }

private:
    friend class boost::serialization::access;

#ifdef _DEBUG_ID
    #error Are you sure?
    char dbg_id_[37];
#endif

    template<class Archive>
    void save(Archive & ar, const unsigned int /*version*/) const
    {
        std::string utf8str = to_string();
        ar & utf8str;
    }

    template<class Archive>
    void load(Archive & ar, const unsigned int /*version*/)
    {
        std::string utf8;
        ar & utf8;
        set(utf8);
    }

    BOOST_SERIALIZATION_SPLIT_MEMBER()
};

template <typename Archive> 
void serialize(Archive &ar, boost::uuids::uuid &p, const unsigned int version) 
{ 
    ar & p.data; 
}

enum type_id
{
    base_id,

    message_id,
    message_queue_id,
    message_sender_id,
    message_addressee_id,
    message_command_id,

    xml_string_id,
    xml_cmd_result_id,
    xml_cmd_command_id,
    url_address_id,
    network_port_id,
    ip_address_id,
    mac_address_id,
    url_proto_id,

    module_name_id,
    fileitem_name_id,

    server_state_id,

    registry_path_id,
    filesystem_path_id,

    channel_id_id,
    channel_name_id,
    channel_origin_id,
    channel_scan_id_id,
    channel_enc_id,
    channel_number_id,
    channel_subnumber_id,
    channel_tuning_params_id,
    channel_lock_id,
    channel_logo_addr_id,
    channel_comment_id,

    logo_id_id,
    logo_name_id,
    logo_package_id,

    transponder_id_id,
    transponder_name_id,

    standard_set_id,
    standard_name_id,
    provider_id_id,
    provider_name_id,
    provider_country_id,
    scan_element_id,

    headend_id_id,
    headend_name_id,
    headend_desc_id,

    parameter_key_id,
    parameter_name_id,
    parameter_value_id,
    parameter_desc_id,
    parameter_idx_id,

    device_id_id,
    device_name_id,
    device_uri_id,
    device_make_id,
    device_model_id,
    device_model_num_id,
    device_uuid_id,
    device_uuid_make_id,
    device_vid_id,
    device_pid_id,

    streamer_id_id,

    scan_lock_id,
    scan_comment_id,
    scan_network_id_id,
    scan_network_name_id,

    client_id_id,
    product_id_id,
    product_name_id,
    product_desc_id,
    version_id,
    build_id,
    product_type_id,
    license_name_id,
    license_key_id,
    hw_fingerprint_id,
    machine_id_id,
    coupon_code_id,
    email_id,
    user_name_id,
    product_file_id,

    bitmask_id,
	bool_flag_id,
	timeshift_version_id,

    favorite_id_id,
    favorite_name_id,
    favorite_flags_id,

    stream_client_id_id,

    schedule_name_id,
    schedule_item_id_id,
    schedule_id_id,
    epg_event_id_id,
    key_phrase_id,
    timer_id_id,
    epg_channel_id_id,
    epg_channel_name_id,
    recording_id_id,

    channel_set_id_id,
    concurrency_num_id,
    csv_string_id,

    epg_data_id,
    epg_source_id_id,
    epg_box_id_id,
    epg_source_name_id,

    item_id_id,

    updater_component_id_id,
    updater_component_name_id,
    updater_component_desc_id,

    object_id_id,
    object_name_id,
    object_desc_id,
    object_handle_id,
    safe_object_id,

    pb_source_id_id,
    pb_source_name_id,

    work_item_id,

    container_id_id,
    container_name_id,
    container_desc_id,

    storage_path_id,
    value_id,
    node_name_id,

    env_var_id,
    launch_param_id,
    launch_condition_id,
    launch_cnd_value_id,

    tc_usecase_id_id,
    tc_usecase_name_id,
    tc_usecase_mime_id,

    search_string_id,

    mime_type_id,

    last_id_number
};

typedef base_type_uuid_t<base_id>                       base_id_t;

typedef base_type_uuid_t<message_sender_id>             message_sender_t;
typedef base_type_uuid_t<message_queue_id>              message_queue_id_t;
typedef base_type_uuid_t<message_addressee_id>          message_addressee_t;
typedef base_type_t<boost::int32_t, message_command_id> message_command_t;
typedef base_type_t<boost::uint32_t, message_id>        message_id_t;
typedef base_type_string_t<xml_string_id>               xml_string_t;
typedef base_type_string_t<xml_cmd_result_id>           xml_cmd_result_t;
typedef base_type_string_t<xml_cmd_command_id>          xml_cmd_command_t;

typedef base_type_string_t<url_address_id>              url_address_t;
typedef base_type_t<boost::uint16_t, network_port_id>   network_port_t;
typedef base_type_string_t<ip_address_id>               ip_address_t;
typedef base_type_string_t<mac_address_id>              mac_address_t;
typedef base_type_string_t<url_proto_id>                url_proto_t;

typedef base_type_string_t<module_name_id>              module_name_t;
typedef base_type_t<boost::uint32_t, server_state_id>   server_state_t;
typedef base_type_string_t<fileitem_name_id>            fileitem_name_t;

typedef base_type_string_t<channel_id_id>               channel_id_t;
typedef base_type_string_t<channel_name_id>             channel_name_t;
typedef base_type_string_t<channel_origin_id>           channel_origin_t;
typedef base_type_t<boost::uint16_t, channel_scan_id_id>        channel_scan_id_t;
typedef base_type_t<bool, channel_enc_id>               channel_enc_t;
typedef base_type_t<boost::int32_t, channel_number_id>  channel_number_t;
typedef base_type_t<boost::int32_t, channel_subnumber_id>  channel_subnumber_t;
typedef base_type_string_t<channel_tuning_params_id>    channel_tuning_params_t;
typedef base_type_string_t<transponder_id_id>           transponder_id_t;
typedef base_type_string_t<transponder_name_id>         transponder_name_t;
typedef base_type_t<bool, channel_lock_id>              channel_lock_t;
typedef base_type_string_t<channel_logo_addr_id>        channel_logo_addr_t;
typedef base_type_string_t<channel_comment_id>          channel_comment_t;

typedef base_type_string_t<logo_id_id>                  logo_id_t;
typedef base_type_string_t<logo_name_id>                logo_name_t;
typedef base_type_string_t<logo_package_id>             logo_package_t;

typedef base_type_t<boost::uint32_t, standard_set_id>   standard_set_t;
typedef base_type_string_t<standard_name_id>            standard_name_t;
typedef base_type_string_t<provider_id_id>              provider_id_t;
typedef base_type_string_t<provider_name_id>            provider_name_t;
typedef base_type_string_t<provider_country_id>         provider_country_t;
typedef base_type_string_t<scan_element_id>             scan_element_t;

typedef base_type_string_t<headend_id_id>               headend_id_t;
typedef base_type_string_t<headend_name_id>             headend_name_t;
typedef base_type_string_t<headend_desc_id>             headend_desc_t;

typedef base_type_string_t<parameter_key_id>            parameter_key_t;
typedef base_type_string_t<parameter_name_id>           parameter_name_t;
typedef base_type_string_t<parameter_value_id>          parameter_value_t;
typedef base_type_string_t<parameter_desc_id>           parameter_desc_t;
typedef base_type_t<boost::int32_t, parameter_idx_id>   parameter_idx_t;

typedef base_type_string_t<container_id_id>             container_id_t;
typedef base_type_string_t<container_name_id>           container_name_t;
typedef base_type_string_t<container_desc_id>           container_desc_t;

typedef base_type_string_t<device_id_id>                device_id_t;
typedef base_type_string_t<device_name_id>              device_name_t;
typedef base_type_string_t<device_uri_id>               device_uri_t;
typedef base_type_string_t<device_make_id>              device_make_t;
typedef base_type_string_t<device_model_id>             device_model_t;
typedef base_type_string_t<device_model_num_id>         device_model_num_t;
typedef base_type_string_t<device_uuid_id>              device_uuid_t;
typedef base_type_string_t<device_uuid_make_id>         device_uuid_make_t;
typedef base_type_string_t<device_vid_id>               device_vid_t;
typedef base_type_string_t<device_pid_id>               device_pid_t;

typedef base_type_string_t<streamer_id_id>              streamer_id_t;

typedef base_type_t<bool, scan_lock_id>                 scan_lock_t;
typedef base_type_string_t<scan_comment_id>             scan_comment_t;
typedef base_type_string_t<scan_network_id_id>          scan_network_id_t;
typedef base_type_string_t<scan_network_name_id>        scan_network_name_t;

typedef base_type_uuid_t<client_id_id>                  client_id_t;
typedef base_type_uuid_t<product_id_id>                 product_id_t;
typedef base_type_string_t<product_name_id>             product_name_t;
typedef base_type_string_t<product_desc_id>             product_desc_t;
typedef base_type_string_t<version_id>                  version_t;
typedef base_type_string_t<build_id>                    build_t;
typedef base_type_string_t<product_type_id>             product_type_t;

typedef base_type_string_t<license_name_id>             license_name_t;
typedef base_type_string_t<license_key_id>              license_key_t;
typedef base_type_string_t<hw_fingerprint_id>           hw_fingerprint_t;
typedef base_type_string_t<machine_id_id>               machine_id_t;
typedef base_type_string_t<coupon_code_id>              coupon_code_t;
typedef base_type_string_t<user_name_id>                user_name_t;
typedef base_type_string_t<email_id>                    email_t;
typedef base_type_string_t<product_file_id>             product_file_t;

typedef base_type_t<boost::uint32_t, bitmask_id>		bitmask_t;
typedef base_type_t<bool, bool_flag_id>					bool_flag_t;
typedef base_type_t<boost::uint32_t, timeshift_version_id> timeshift_version_t;

typedef base_type_string_t<favorite_id_id>              favorite_id_t;
typedef base_type_string_t<favorite_name_id>            favorite_name_t;
typedef base_type_t<boost::uint32_t, favorite_flags_id>	favorite_flags_t;

typedef base_type_string_t<stream_client_id_id>         stream_client_id_t;

typedef base_type_string_t<schedule_name_id>            schedule_name_t;
typedef base_type_t<boost::int32_t, schedule_item_id_id> schedule_item_id_t;
typedef base_type_string_t<schedule_id_id>              schedule_id_t;
typedef base_type_string_t<epg_event_id_id>             epg_event_id_t;
typedef base_type_string_t<key_phrase_id>               key_phrase_t;
typedef base_type_string_t<timer_id_id>                 timer_id_t;
typedef base_type_string_t<recording_id_id>             recording_id_t;

typedef base_type_t<boost::int32_t, concurrency_num_id> concurrency_num_t;
typedef base_type_t<boost::uint32_t, channel_set_id_id> channel_set_id_t;
typedef base_type_string_t<csv_string_id>               csv_string_t;

typedef base_type_string_t<epg_data_id>                 epg_data_t;
typedef base_type_string_t<epg_channel_id_id>           epg_channel_id_t;
typedef base_type_string_t<epg_channel_name_id>         epg_channel_name_t;
typedef base_type_uuid_t<epg_source_id_id>              epg_source_id_t;
typedef base_type_string_t<epg_source_name_id>          epg_source_name_t;
typedef base_type_uuid_t<epg_box_id_id>                 epg_box_id_t;

typedef base_type_string_t<item_id_id>                  item_id_t;

typedef base_type_uuid_t<updater_component_id_id>       updater_component_id_t;
typedef base_type_string_t<updater_component_name_id>   updater_component_name_t;
typedef base_type_string_t<updater_component_desc_id>   updater_component_desc_t;

typedef base_type_string_t<object_id_id>                object_id_t;
typedef base_type_string_t<object_name_id>              object_name_t;
typedef base_type_string_t<object_desc_id>              object_desc_t;
typedef base_type_string_t<object_handle_id>            object_handle_t;
typedef base_type_t<const char*, safe_object_id>        object_handle_safe_t;

typedef base_type_uuid_t<pb_source_id_id>               pb_source_id_t;
typedef base_type_string_t<pb_source_name_id>           pb_source_name_t;

typedef base_type_string_t<work_item_id>				work_item_id_t;

typedef base_type_wstring_t<node_name_id>               node_name_t;
typedef base_type_wstring_t<value_id>                   value_t;

typedef base_type_string_t<env_var_id>				    env_var_t;
typedef base_type_string_t<launch_param_id>				launch_param_t;
typedef base_type_string_t<launch_condition_id>			launch_condition_t;
typedef base_type_string_t<launch_cnd_value_id>			launch_cnd_value_t;

typedef base_type_string_t<tc_usecase_id_id>		    tc_usecase_id_t;
typedef base_type_string_t<tc_usecase_name_id>			tc_usecase_name_t;
typedef base_type_string_t<tc_usecase_mime_id>			tc_usecase_mime_t;

typedef base_type_string_t<search_string_id>		    search_string_t;

typedef base_type_string_t<mime_type_id>		        mime_type_t;

} //dvblink

#ifdef _WIN32
#pragma warning(pop)
#endif

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_TYPES_H_
