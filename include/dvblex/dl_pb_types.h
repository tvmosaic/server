/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <vector>
#include <dl_pugixml_helper.h>
#include <dl_pb_item.h>
#include <dl_pb_recorded_tv.h>
#include <dl_pb_video.h>
#include <dl_pb_audio.h>
#include <dl_pb_image.h>
#include <boost/shared_ptr.hpp>

namespace dvblex { namespace playback {

typedef std::vector<boost::shared_ptr<pb_item_t> > pb_item_list_t;

const std::string items_root_node                    = "items";

inline void write_to_node(pugi::xml_node& node, const pb_item_list_t& pb_item_list)
{
    pugi::xml_node items_node = dvblink::pugixml_helpers::new_child(node, items_root_node);
    if (items_node != NULL)
    {
        pb_item_list_t::const_iterator item_it = pb_item_list.begin();
        while (item_it != pb_item_list.end())
        {
            switch ((*item_it)->item_type_)
            {
            case pbit_item_recorded_tv:
                {
                    boost::shared_ptr<pb_recorded_tv_t> recorded_tv = boost::static_pointer_cast<pb_recorded_tv_t>(*item_it);
                    if (recorded_tv)
                        write_to_node(items_node, *recorded_tv);
                }
                break;
            case pbit_item_video:
                {
                    boost::shared_ptr<pb_video_t> video = boost::static_pointer_cast<pb_video_t>(*item_it);
                    if (video)
                        write_to_node(items_node, *video);
                }
                break;
            case pbit_item_audio:
                {
                    boost::shared_ptr<pb_audio_t> audio = boost::static_pointer_cast<pb_audio_t>(*item_it);
                    if (audio)
                        write_to_node(items_node, *audio);
                }
                break;
            case pbit_item_image:
                {
                    boost::shared_ptr<pb_image_t> image = boost::static_pointer_cast<pb_image_t>(*item_it);
                    if (image)
                        write_to_node(items_node, *image);
                }
                break;
            default:
                break;
            }
            ++item_it;
        }
    }
}

inline void read_from_node(pugi::xml_node& node, pb_item_list_t& pb_item_list)
{
    std::stringstream buf;

    pugi::xml_node items_node = node.child(items_root_node.c_str());
    if (items_node != NULL)
    {
		pugi::xml_node item_node = items_node.first_child();
		while (NULL != item_node)
		{
            boost::shared_ptr<dvblex::playback::pb_item_t> media_item;

            if (boost::iequals(item_node.name(), recorded_tv_root_node))
			{
                boost::shared_ptr<dvblex::playback::pb_recorded_tv_t> recorded_tv(new dvblex::playback::pb_recorded_tv_t());
                read_from_node(item_node, *recorded_tv);
                media_item = boost::static_pointer_cast<dvblex::playback::pb_item_t>(recorded_tv);
			}

            if (boost::iequals(item_node.name(), video_root_node))
			{
                boost::shared_ptr<dvblex::playback::pb_video_t> v_item(new dvblex::playback::pb_video_t());
                read_from_node(item_node, *v_item);
                media_item = boost::static_pointer_cast<dvblex::playback::pb_item_t>(v_item);
			}

            if (boost::iequals(item_node.name(), audio_root_node))
			{
                boost::shared_ptr<dvblex::playback::pb_audio_t> a_item(new dvblex::playback::pb_audio_t());
                read_from_node(item_node, *a_item);
                media_item = boost::static_pointer_cast<dvblex::playback::pb_item_t>(a_item);
			}

            if (boost::iequals(item_node.name(), image_root_node))
			{
                boost::shared_ptr<dvblex::playback::pb_image_t> i_item(new dvblex::playback::pb_image_t());
                read_from_node(item_node, *i_item);
                media_item = boost::static_pointer_cast<dvblex::playback::pb_item_t>(i_item);
			}

            if (media_item)
                pb_item_list.push_back(media_item);

            item_node = item_node.next_sibling();
        }
    }
}

} // playback
} // dvblex


namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::playback::pb_item_list_t& pil, const unsigned int /*version*/)
{
    if (Archive::is_loading::value)
    {
        //reading from archive

        //read number of items first
        boost::int32_t num;
        ar & num;

        for (boost::int32_t i=0; i<num; i++)
        {
            //read type of the item
            boost::uint32_t type;
            ar & type;

            dvblex::playback::pb_item_type_e item_type = (dvblex::playback::pb_item_type_e)type;

            boost::shared_ptr<dvblex::playback::pb_item_t> media_item;

            switch (item_type)
            {
            case dvblex::playback::pbit_item_recorded_tv:
                {
                    boost::shared_ptr<dvblex::playback::pb_recorded_tv_t> recorded_tv(new dvblex::playback::pb_recorded_tv_t());
                    ar & *recorded_tv;
                    media_item = boost::static_pointer_cast<dvblex::playback::pb_item_t>(recorded_tv);
                }
                break;
            case dvblex::playback::pbit_item_video:
                {
                    boost::shared_ptr<dvblex::playback::pb_video_t> v_item(new dvblex::playback::pb_video_t());
                    ar & *v_item;
                    media_item = boost::static_pointer_cast<dvblex::playback::pb_item_t>(v_item);
                }
                break;
            case dvblex::playback::pbit_item_audio:
                {
                    boost::shared_ptr<dvblex::playback::pb_audio_t> a_item(new dvblex::playback::pb_audio_t());
                    ar & *a_item;
                    media_item = boost::static_pointer_cast<dvblex::playback::pb_item_t>(a_item);
                }
                break;
            case dvblex::playback::pbit_item_image:
                {
                    boost::shared_ptr<dvblex::playback::pb_image_t> i_item(new dvblex::playback::pb_image_t());
                    ar & *i_item;
                    media_item = boost::static_pointer_cast<dvblex::playback::pb_item_t>(i_item);
                }
                break;
            default:
                break;
            }

            if (media_item)
                pil.push_back(media_item);
        }

    } else
    {
        //writing to archive

        //write number of items first
        boost::int32_t num = pil.size();
        ar & num;

        for (boost::int32_t i=0; i<num; i++)
        {
            //write type of the item
            boost::uint32_t type = pil[i]->item_type_;
            ar & type;

            switch (pil[i]->item_type_)
            {
            case dvblex::playback::pbit_item_recorded_tv:
                {
                    boost::shared_ptr<dvblex::playback::pb_recorded_tv_t> recorded_tv = boost::static_pointer_cast<dvblex::playback::pb_recorded_tv_t>(pil[i]);
                    if (recorded_tv)
                        ar & *recorded_tv;
                }
                break;
            case dvblex::playback::pbit_item_video:
                {
                    boost::shared_ptr<dvblex::playback::pb_video_t> video = boost::static_pointer_cast<dvblex::playback::pb_video_t>(pil[i]);
                    if (video)
                        ar & *video;
                }
                break;
            case dvblex::playback::pbit_item_audio:
                {
                    boost::shared_ptr<dvblex::playback::pb_audio_t> audio = boost::static_pointer_cast<dvblex::playback::pb_audio_t>(pil[i]);
                    if (audio)
                        ar & *audio;
                }
                break;
            case dvblex::playback::pbit_item_image:
                {
                    boost::shared_ptr<dvblex::playback::pb_image_t> image = boost::static_pointer_cast<dvblex::playback::pb_image_t>(pil[i]);
                    if (image)
                        ar & *image;
                }
                break;
            default:
                break;
            }
        }
    }
}

} // namespace serialization
} // namespace boost
