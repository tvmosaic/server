/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_filesystem_path.h>
#include <dl_installation_settings.h>

namespace dvblex { namespace settings {

class external_control
{
public:
    static bool unzip(installation_settings_t& ds, const dvblink::filesystem_path_t& zipfile, const dvblink::filesystem_path_t& dest_dir);
//    static bool untar(dvblink::filesystem_path_t& tarfile, dvblink::filesystem_path_t& dest_dir);

#ifdef _WIN32
enum process_priority_e
{
    RealtimePriority            = REALTIME_PRIORITY_CLASS,
    HighPriority                = HIGH_PRIORITY_CLASS,
    AboveNormalPriority         = ABOVE_NORMAL_PRIORITY_CLASS,
    NormalPriority              = NORMAL_PRIORITY_CLASS,
    BelowNormalPriority         = BELOW_NORMAL_PRIORITY_CLASS,
    IdlePriority                = IDLE_PRIORITY_CLASS
};
#else
enum process_priority_e
{
    RealtimePriority            = 2,
    HighPriority                = 6,
    AboveNormalPriority         = 10,
    NormalPriority              = 20,
    BelowNormalPriority         = 30,
    IdlePriority                = 39
};
#endif

	static boost::int64_t start_process(const dvblink::filesystem_path_t& exe_path, const dvblink::filesystem_path_t* exe_dir,
        const std::vector<dvblink::launch_param_t>& params, process_priority_e priority, const std::vector<dvblink::env_var_t>* env_vars = NULL);

	static bool is_process_running(boost::int64_t pid);
	static bool kill_process(boost::int64_t pid);
};

} //settings
} //dvblex

