/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <dl_types.h>
#include <dl_params.h>
#include <dl_device_info.h>
#include <dl_provider_info.h>
#include <dl_scan_info.h>
#include <dl_headend.h>

namespace dvblex { 

enum device_info_state_e
{
    dis_unknown,
    dis_new,
    dis_active,
    dis_missing,
    dis_error
};

struct device_info_t
{
    device_descriptor_t descriptor_;
    device_info_state_e state_;
    headend_info_list_t headends_;
};

typedef std::vector<device_info_t> device_info_list_t;

struct device_status_ex_t
{
    device_status_t device_status_;
    scan_log_t scan_log_;
};

typedef std::vector<dvblink::provider_id_t> device_provider_list_t;
typedef std::map<dvblink::device_id_t, device_provider_list_t> device_map_to_provider_list_t;

typedef std::vector<dvblink::device_id_t> device_id_list_t;

//device-to-device-channel map
typedef std::map<dvblink::device_id_t, device_channel_t> device_to_device_channel_map_t;

} //dvblex

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::device_descriptor_t& dd, const unsigned int /*version*/)
{
    ar & dd.id_;
    ar & dd.name_;
    ar & dd.tuners_num_;
    ar & dd.state_;
    ar & dd.supported_standards_;
    ar & dd.uri_;
    ar & dd.make_;
    ar & dd.model_;
    ar & dd.model_num_;
    ar & dd.uuid_make_;
    ar & dd.uuid_;
    ar & dd.connection_type_;
    ar & dd.should_be_merged_;
    ar & dd.vid_;
    ar & dd.pid_;
    ar & dd.can_be_deleted_;
    ar & dd.has_settings_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::device_info_t& di, const unsigned int /*version*/)
{
    ar & di.descriptor_;
    ar & di.state_;
    ar & di.headends_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::provider_description_t& pd, const unsigned int /*version*/)
{
    ar & pd.standard_;
    ar & pd.standard_name_;
    ar & pd.parameters_;
    ar & pd.providers_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::provider_info_t& pi, const unsigned int /*version*/)
{
    ar & pi.id_;
    ar & pi.name_;
    ar & pi.country_;
    ar & pi.desc_;
    ar & pi.scan_params_;
    ar & pi.param_container_;
    ar & pi.standard_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::scanned_network_t& sn, const unsigned int /*version*/)
{
    ar & sn.network_id_;
    ar & sn.name_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::signal_info_t& si, const unsigned int /*version*/)
{
    ar & si.lock_;
    ar & si.signal_strength_;
    ar & si.signal_quality_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::scan_log_entry_t& sle, const unsigned int /*version*/)
{
    ar & sle.scan_data_;
    ar & sle.channels_found_;
    ar & sle.comment_;
    ar & sle.signal_info_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::stream_stats_t& ss, const unsigned int /*version*/)
{
    ar & ss.bitrate_;
    ar & ss.packet_count_;
    ar & ss.error_packet_count_;
    ar & ss.discontinuity_count_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::channel_scanner_progress_t& sp, const unsigned int /*version*/)
{
    ar & sp.state_;
    ar & sp.progress_;
    ar & sp.channels_found_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::stream_channel_stats_t& scs, const unsigned int /*version*/)
{
    ar & scs.channel_id_;
    ar & scs.channel_name_;
    ar & scs.stream_stats_;
    ar & scs.consumer_num_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::device_status_t& ds, const unsigned int /*version*/)
{
    ar & ds.id_;
    ar & ds.state_;
    ar & ds.signal_info_;
    ar & ds.stream_channels_;
    ar & ds.max_priority_;
    ar & ds.scan_progress_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::device_status_ex_t& dse, const unsigned int /*version*/)
{
    ar & dse.device_status_;
    ar & dse.scan_log_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::configurable_device_props_t& cdp, const unsigned int /*version*/)
{
    ar & cdp.fta_concurrent_streams_;
    ar & cdp.enc_concurrent_streams_;
    ar & cdp.param_container_;
}

} // namespace serialization
} // namespace boost
