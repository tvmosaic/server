/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_FILESYSTEM_PATH_H_
#define __DVBLINK_DL_FILESYSTEM_PATH_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <boost/algorithm/string.hpp>
#include <boost/filesystem/path.hpp>
#include <dl_types.h>

namespace dvblink {

class filesystem_path_t : public base_type_wstring_t<filesystem_path_id>
{
protected:
    std::string convert(const std::wstring& wstr)
    {
        return dvblink::engine::string_cast<dvblink::engine::EC_UTF8>(wstr);
    }

    std::wstring convert(const std::string& str)
    {
        return dvblink::engine::string_cast<dvblink::engine::EC_UTF8>(str);
    }

    void append(const std::string& p)
    {
#ifdef WIN32
		boost::filesystem::path res = to_wstring();
		res /= convert(p);
		id_ = res.wstring();
#else
		boost::filesystem::path res = to_string();
		res /= p;
		id_ = convert(res.string());
#endif
	}

    void append(const std::wstring& p)
    {
#ifdef WIN32
		boost::filesystem::path res = to_wstring();
		res /= p;
		id_ = res.wstring();
#else
		boost::filesystem::path res = to_string();
		res /= convert(p);
		id_ = convert(res.string());
#endif
	}

public:
    filesystem_path_t() :
        base_type_wstring_t<filesystem_path_id>()
    {
    }

    filesystem_path_t(const filesystem_path_t& path) :
        base_type_wstring_t<filesystem_path_id>()
    {
        id_ = path.id_;
    }

    filesystem_path_t(const boost::filesystem::path& path) :
        base_type_wstring_t<filesystem_path_id>()
    {
#ifdef WIN32
        id_ = path.wstring();
#else
        id_ = convert(path.string());
#endif
    }

    filesystem_path_t(const std::wstring& path) :
        base_type_wstring_t<filesystem_path_id>()
    {
        id_ = path;
    }

    filesystem_path_t(const wchar_t* path) :
        base_type_wstring_t<filesystem_path_id>()
    {
        id_ = path;
    }

    boost::filesystem::path to_boost_filesystem() const
    {
#ifdef WIN32
        return boost::filesystem::path(to_wstring());
#else
        return boost::filesystem::path(to_string());
#endif
    }

    const wchar_t* c_str() const {return id_.c_str();}
    std::string to_string() const {return dvblink::engine::string_cast<dvblink::engine::EC_UTF8>(id_);}
    std::wstring to_wstring() const {return get();}

    filesystem_path_t& operator = (const filesystem_path_t& path)
    {
        id_ = path.get();
        return *this;
    }

    filesystem_path_t& operator = (const boost::filesystem::path& path)
    {
#ifdef WIN32
        id_ = path.wstring();
#else
        id_ = convert(path.string());
#endif
        return *this;
    }

    filesystem_path_t& operator = (const std::wstring& path)
    {
        id_ = path;
        return *this;
    }

    filesystem_path_t& operator = (const wchar_t* path)
    {
        id_ = path;
        return *this;
    }

    filesystem_path_t& operator /= (const boost::filesystem::path& p)
    {
#ifdef WIN32
        append(p.wstring());
#else
        append(p.string());
#endif
        return *this;
    }

    filesystem_path_t& operator /= (const filesystem_path_t& p)
    {
    	append(p.to_wstring());
        return *this;
    }

    filesystem_path_t& operator /= (const std::wstring& p)
    {
    	append(p);
        return *this;
    }

    filesystem_path_t& operator /= (const wchar_t* p)
    {
    	append(std::wstring(p));
        return *this;
    }
};

inline filesystem_path_t operator / (const filesystem_path_t& lhs, const filesystem_path_t& rhs) {return filesystem_path_t(lhs) /= rhs;}
inline filesystem_path_t operator / (const filesystem_path_t& lhs, const boost::filesystem::path& rhs) {return filesystem_path_t(lhs) /= rhs;}
inline filesystem_path_t operator / (const filesystem_path_t& lhs, const std::wstring& rhs) {return filesystem_path_t(lhs) /= rhs;}
inline filesystem_path_t operator / (const filesystem_path_t& lhs, const wchar_t* rhs) {return filesystem_path_t(lhs) /= rhs;}

template <class Char, class Traits>
inline std::basic_ostream<Char, Traits>&
operator << (std::basic_ostream<Char, Traits>& os, const filesystem_path_t& p)
{
    return os << p.get();
}

template <class Char, class Traits>
inline std::basic_istream<Char, Traits>&
operator >> (std::basic_istream<Char, Traits>& is, filesystem_path_t& p)
{
    std::basic_string<Char> str;
    is >> str;
    p = str;
    return is;
}

} //dvblink

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_FILESYSTEM_PATH_H_
