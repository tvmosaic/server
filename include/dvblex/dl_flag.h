/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include "boost/interprocess/detail/atomic.hpp"
#include <boost/thread/mutex.hpp>

namespace dvblink {
namespace engine {

class atomic_switch
{
    protected:
        static const boost::uint32_t set_value_ = 1;
        static const boost::uint32_t reset_value_ = 0;
        
    public:
        atomic_switch()
        {
            value_ = reset_value_;
        }
        
        //Resets the flag. Returns true if flag was set
        bool check_and_reset()
        {
#ifdef _TVM_NO_BOOST_ATOMIC
    	    boost::unique_lock<boost::mutex> lock(lock_);

    	    boost::uint32_t v = value_;

    	    if (value_ == set_value_)
    	    	value_ = reset_value_;

#else
            boost::uint32_t v = boost::interprocess::ipcdetail::atomic_cas32(&value_, reset_value_, set_value_);
#endif
            return (v == set_value_);
        }
        
        //Sets the flag. Returns true if flag was reset
        bool check_and_set()
        {
#ifdef _TVM_NO_BOOST_ATOMIC
    	    boost::unique_lock<boost::mutex> lock(lock_);

    	    boost::uint32_t v = value_;

    	    if (value_ == reset_value_)
    	    	value_ = set_value_;

#else
            boost::uint32_t v = boost::interprocess::ipcdetail::atomic_cas32(&value_, set_value_, reset_value_);
#endif
            return (v == reset_value_);
        }
        
    protected:
        volatile boost::uint32_t value_;
#ifdef _TVM_NO_BOOST_ATOMIC
        boost::mutex lock_;
#endif
};

} // namespace engine
} // namespace dvblink
