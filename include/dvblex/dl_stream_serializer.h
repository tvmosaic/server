/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <dl_types.h>
#include <dl_pugixml_helper.h>
#include <dl_channels.h>
#include <dl_stream.h>

namespace dvblex { 

////////////////////////////////////////////////////////////////////
struct get_stream_info_request_t
{
    std::string stream_client_id_;
    std::string server_address_;
    channel_id_list_t channels_;
};

const std::string stream_info_root                        = "stream_info";
const std::string stream_client_id_node                   = "client_id";
const std::string stream_server_addr_node                 = "server_address";
const std::string stream_channel_dvblink_ids_node         = "channels_dvblink_ids";
const std::string stream_channel_dvblink_id_node          = "channel_dvblink_id";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_stream_info_request_t& request)
{
    if (NULL != node)
    {
        dvblink::pugixml_helpers::get_node_value(node, stream_client_id_node, request.stream_client_id_);
        dvblink::pugixml_helpers::get_node_value(node, stream_server_addr_node, request.server_address_);

        std::string str;
        pugi::xml_node channels_node = node.child(stream_channel_dvblink_ids_node.c_str());
        if (channels_node != NULL)
        {
            pugi::xml_node channel_node = channels_node.first_child();
            while (channel_node != NULL)
            {
                if (boost::iequals(channel_node.name(), stream_channel_dvblink_id_node))
                {
                    str = channel_node.child_value();
                    if (!str.empty())
                        request.channels_.push_back(str);
                }
                channel_node = channel_node.next_sibling();
            }
        }
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_stream_info_request_t& sir)
{
    std::stringstream buf;

    pugi::xml_node si_node = doc.append_child(stream_info_root.c_str());

    if (si_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(si_node, stream_client_id_node, sir.stream_client_id_);
        dvblink::pugixml_helpers::new_child(si_node, stream_server_addr_node, sir.server_address_);

        pugi::xml_node ids_node = dvblink::pugixml_helpers::new_child(si_node, stream_channel_dvblink_ids_node);
		if (ids_node != NULL)
		{
            for (size_t i=0; i<sir.channels_.size(); i++)
                dvblink::pugixml_helpers::new_child(ids_node, stream_channel_dvblink_id_node, sir.channels_[i].get());
        }
    }

    return doc;
}

const std::string stream_channel_node      = "channel";
const std::string stream_url_node          = "url";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const url_stream_info_list_t& usil)
{
    std::stringstream buf;

    pugi::xml_node si_node = doc.append_child(stream_info_root.c_str());

    if (si_node != NULL)
    {
        for (size_t i=0; i<usil.size(); i++)
        {
            pugi::xml_node sc_node = dvblink::pugixml_helpers::new_child(si_node, stream_channel_node);
			if (sc_node != NULL)
			{
                dvblink::pugixml_helpers::new_child(sc_node, stream_channel_dvblink_id_node, usil[i].channel_id_.get());
                dvblink::pugixml_helpers::new_child(sc_node, stream_url_node, usil[i].stream_url_.get());
			}
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, url_stream_info_list_t& usil)
{
    if (NULL != node)
    {
        std::string str;
        pugi::xml_node si_node = node.first_child();
        while (si_node != NULL)
        {
            if (boost::iequals(si_node.name(), stream_channel_node))
            {
                dvblex::url_stream_info_t si;
                if (dvblink::pugixml_helpers::get_node_value(si_node, stream_channel_dvblink_id_node, str))
                    si.channel_id_ = str;
                if (dvblink::pugixml_helpers::get_node_value(si_node, stream_url_node, str))
                    si.stream_url_ = str;

                usil.push_back(si);
            }
            si_node = si_node.next_sibling();
        }
    }

    return node;
}

////////////////////////////////////////////////////////////////////
const boost::uint32_t pcr_invalid_transcoder_value = 0;

struct play_channel_request_t
{
    play_channel_request_t() :
        transcoded_stream_(false), tr_width_(pcr_invalid_transcoder_value), tr_height_(pcr_invalid_transcoder_value), tr_bitrate_(pcr_invalid_transcoder_value), tr_video_scale_(pcr_invalid_transcoder_value)
        {}

    dvblink::channel_id_t channel_id_;
    std::string stream_client_id_;
    std::string stream_type_;
    bool transcoded_stream_;
    std::string audio_track_;
    boost::uint32_t tr_width_;
    boost::uint32_t tr_height_;
    boost::uint32_t tr_bitrate_;
    boost::uint32_t tr_video_scale_;
};

const std::string play_channel_root                             = "stream";
const std::string play_channel_channel_id_node                  = "channel_dvblink_id";
const std::string play_channel_client_id_node                   = "client_id";
const std::string play_channel_stream_type_node                 = "stream_type";
const std::string play_channel_transcoder_node                  = "transcoder";
const std::string play_channel_width_node                       = "width";
const std::string play_channel_height_node                      = "height";
const std::string play_channel_bitrate_node                     = "bitrate";
const std::string play_channel_audio_track_node                 = "audio_track";
const std::string play_channel_scale_node                     	= "scale";

inline pugi::xml_node& operator>> (pugi::xml_node& node, play_channel_request_t& pcr)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, play_channel_channel_id_node, str))
            pcr.channel_id_ = str;

        dvblink::pugixml_helpers::get_node_value(node, play_channel_client_id_node, pcr.stream_client_id_);
        dvblink::pugixml_helpers::get_node_value(node, play_channel_stream_type_node, pcr.stream_type_);

        pugi::xml_node tr_node = node.child(play_channel_transcoder_node.c_str());
        if (tr_node != NULL)
        {
            pcr.transcoded_stream_ = true;

            boost::uint32_t value;
            if (dvblink::pugixml_helpers::get_node_value(tr_node, play_channel_width_node, str))
            {
                dvblink::engine::string_conv::apply(str.c_str(), value, pcr.tr_width_);
                pcr.tr_width_ = value;
            }
            if (dvblink::pugixml_helpers::get_node_value(tr_node, play_channel_height_node, str))
            {
                dvblink::engine::string_conv::apply(str.c_str(), value, pcr.tr_height_);
                pcr.tr_height_ = value;
            }
            if (dvblink::pugixml_helpers::get_node_value(tr_node, play_channel_bitrate_node, str))
            {
                dvblink::engine::string_conv::apply(str.c_str(), value, pcr.tr_bitrate_);
                pcr.tr_bitrate_ = value;
            }
            if (dvblink::pugixml_helpers::get_node_value(tr_node, play_channel_scale_node, str))
            {
                dvblink::engine::string_conv::apply(str.c_str(), value, pcr.tr_video_scale_);
                pcr.tr_video_scale_ = value;
            }

            dvblink::pugixml_helpers::get_node_value(tr_node, play_channel_audio_track_node, pcr.audio_track_);
        }
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const play_channel_request_t& pcr)
{
    pugi::xml_node pcr_node = doc.append_child(play_channel_root.c_str());
    if (pcr_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(pcr_node, play_channel_channel_id_node, pcr.channel_id_.get());
        dvblink::pugixml_helpers::new_child(pcr_node, play_channel_client_id_node, pcr.stream_client_id_);
        dvblink::pugixml_helpers::new_child(pcr_node, play_channel_stream_type_node, pcr.stream_type_);
        if (pcr.transcoded_stream_)
        {
            pugi::xml_node tr_node = dvblink::pugixml_helpers::new_child(pcr_node, play_channel_transcoder_node);
            if (tr_node != NULL)
            {
                if (!pcr.audio_track_.empty())
                    dvblink::pugixml_helpers::new_child(tr_node, play_channel_audio_track_node, pcr.audio_track_);

                std::stringstream buf;
                if (pcr.tr_width_ != pcr_invalid_transcoder_value)
                {
                    buf.clear(); buf.str("");
                    buf << pcr.tr_width_;
                    dvblink::pugixml_helpers::new_child(tr_node, play_channel_width_node, buf.str());
                }

                if (pcr.tr_height_ != pcr_invalid_transcoder_value)
                {
                    buf.clear(); buf.str("");
                    buf << pcr.tr_height_;
                    dvblink::pugixml_helpers::new_child(tr_node, play_channel_height_node, buf.str());
                }

                if (pcr.tr_bitrate_ != pcr_invalid_transcoder_value)
                {
                    buf.clear(); buf.str("");
                    buf << pcr.tr_bitrate_;
                    dvblink::pugixml_helpers::new_child(tr_node, play_channel_bitrate_node, buf.str());
                }

                if (pcr.tr_video_scale_ != pcr_invalid_transcoder_value)
                {
                    buf.clear(); buf.str("");
                    buf << pcr.tr_video_scale_;
                    dvblink::pugixml_helpers::new_child(tr_node, play_channel_scale_node, buf.str());
                }
			}
        }
    }

    return doc;
}

////////////////////////////////////////////////////////////////////
struct play_channel_response_t
{
    int channel_handle_;
    std::string url_;
};

const std::string play_channel_handle_node                   = "channel_handle";
const std::string play_channel_url_node                   = "url";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const play_channel_response_t& pcr)
{
    std::stringstream buf;

    pugi::xml_node pcr_node = doc.append_child(play_channel_root.c_str());
    if (pcr_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(pcr_node, play_channel_url_node, pcr.url_);

        buf.clear(); buf.str("");
        buf << pcr.channel_handle_;
        dvblink::pugixml_helpers::new_child(pcr_node, play_channel_handle_node, buf.str());
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, play_channel_response_t& pcr)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, play_channel_url_node, str))
            pcr.url_ = str;

        if (dvblink::pugixml_helpers::get_node_value(node, play_channel_handle_node, str))
        {
            int value;
            dvblink::engine::string_conv::apply(str.c_str(), value, (int)0);
            pcr.channel_handle_ = value;
        }
    }

    return node;
}

////////////////////////////////////////////////////////////////////
struct stop_channel_request_t
{
    stop_channel_request_t() :
        channel_handle_(0)
    {
    }

    int channel_handle_;
    std::string stream_client_id_;
};

const std::string stop_channel_root                             = "stream";
const std::string stop_channel_client_id_node                   = "client_id";
const std::string stop_channel_handle_node                   = "channel_handle";

inline pugi::xml_node& operator>> (pugi::xml_node& node, stop_channel_request_t& scr)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, stop_channel_handle_node, str))
            scr.channel_handle_ = atoi(str.c_str());

        dvblink::pugixml_helpers::get_node_value(node, stop_channel_client_id_node, scr.stream_client_id_);
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const stop_channel_request_t& scr)
{
    std::stringstream buf;

    pugi::xml_node scr_node = doc.append_child(stop_channel_root.c_str());
    if (scr_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(scr_node, stop_channel_client_id_node,  scr.stream_client_id_);

        buf.clear(); buf.str("");
        buf << scr.channel_handle_;
        dvblink::pugixml_helpers::new_child(scr_node, stop_channel_handle_node, buf.str());
    }

    return doc;
}

////////////////////////////////////////////////////////////////////
struct timeshift_stats_request_t
{
    timeshift_stats_request_t() :
        channel_handle_(0)
    {
    }

    int channel_handle_;
};

const std::string timeshift_stats_request_root                   = "get_timeshift_stats";
const std::string timeshift_stats_handle_node                   = "channel_handle";

inline pugi::xml_node& operator>> (pugi::xml_node& node, timeshift_stats_request_t& tss)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, timeshift_stats_handle_node.c_str(), str))
            tss.channel_handle_ = atoi(str.c_str());
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const timeshift_stats_request_t& tsr)
{
    std::stringstream buf;

    pugi::xml_node ts_node = doc.append_child(timeshift_stats_request_root.c_str());

    if (ts_node != NULL)
    {
        buf.clear(); buf.str("");
        buf << tsr.channel_handle_;
        dvblink::pugixml_helpers::new_child(ts_node, timeshift_stats_handle_node, buf.str());
    }

    return doc;
}


////////////////////////////////////////////////////////////////////
struct timeshift_stats_response_t
{
    boost::uint64_t max_buffer_length_;
    boost::uint64_t buffer_length_;
    boost::uint64_t cur_pos_bytes_;
    boost::uint64_t buffer_duration_;
    boost::uint64_t cur_pos_sec_;
};

const std::string timeshift_status_response_root                   = "timeshift_status";
const std::string timeshift_status_max_buffer_length_node                   = "max_buffer_length";
const std::string timeshift_status_buffer_length_node                   = "buffer_length";
const std::string timeshift_status_cur_pos_bytes_node                   = "cur_pos_bytes";
const std::string timeshift_status_buffer_duration_node                   = "buffer_duration";
const std::string timeshift_status_cur_pos_sec_node                   = "cur_pos_sec";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const timeshift_stats_response_t& tsr)
{
    std::stringstream buf;

    pugi::xml_node ts_node = doc.append_child(timeshift_status_response_root.c_str());

    if (ts_node != NULL)
    {
        buf.clear(); buf.str("");
        buf << tsr.max_buffer_length_;
        dvblink::pugixml_helpers::new_child(ts_node, timeshift_status_max_buffer_length_node, buf.str());

        buf.clear(); buf.str("");
        buf << tsr.buffer_length_;
        dvblink::pugixml_helpers::new_child(ts_node, timeshift_status_buffer_length_node, buf.str());

        buf.clear(); buf.str("");
        buf << tsr.cur_pos_bytes_;
        dvblink::pugixml_helpers::new_child(ts_node, timeshift_status_cur_pos_bytes_node, buf.str());

        buf.clear(); buf.str("");
        buf << tsr.buffer_duration_;
        dvblink::pugixml_helpers::new_child(ts_node, timeshift_status_buffer_duration_node, buf.str());

        buf.clear(); buf.str("");
        buf << tsr.cur_pos_sec_;
        dvblink::pugixml_helpers::new_child(ts_node, timeshift_status_cur_pos_sec_node, buf.str());
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, timeshift_stats_response_t& tsr)
{
    if (NULL != node)
    {
        std::string str;

        if (dvblink::pugixml_helpers::get_node_value(node, timeshift_status_max_buffer_length_node, str))
            dvblink::engine::string_conv::apply<boost::uint64_t, boost::uint64_t, char>(str.c_str(), tsr.max_buffer_length_, (boost::uint64_t)0);

        if (dvblink::pugixml_helpers::get_node_value(node, timeshift_status_buffer_length_node, str))
            dvblink::engine::string_conv::apply<boost::uint64_t, boost::uint64_t, char>(str.c_str(), tsr.buffer_length_, (boost::uint64_t)0);

        if (dvblink::pugixml_helpers::get_node_value(node, timeshift_status_cur_pos_bytes_node, str))
            dvblink::engine::string_conv::apply<boost::uint64_t, boost::uint64_t, char>(str.c_str(), tsr.cur_pos_bytes_, (boost::uint64_t)0);

        if (dvblink::pugixml_helpers::get_node_value(node, timeshift_status_buffer_duration_node, str))
            dvblink::engine::string_conv::apply<boost::uint64_t, boost::uint64_t, char>(str.c_str(), tsr.buffer_duration_, (boost::uint64_t)0);

        if (dvblink::pugixml_helpers::get_node_value(node, timeshift_status_cur_pos_sec_node, str))
            dvblink::engine::string_conv::apply<boost::uint64_t, boost::uint64_t, char>(str.c_str(), tsr.cur_pos_sec_, (boost::uint64_t)0);
    }

    return node;
}

////////////////////////////////////////////////////////////////////
struct timeshift_seek_request_t
{
    timeshift_seek_request_t() :
        channel_handle_(0)
    {
    }

    int channel_handle_;
    int type_;
    boost::int64_t offset_;
    int whence_;
};

const std::string timeshift_seek_request_root                   = "timeshift_seek";
const std::string timeshift_seek_handle_node                   = "channel_handle";
const std::string timeshift_seek_type_node                   = "type";
const std::string timeshift_seek_offset_node                   = "offset";
const std::string timeshift_seek_whence_node                   = "whence";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const timeshift_seek_request_t& tsr)
{
    std::stringstream buf;

    pugi::xml_node ts_node = doc.append_child(timeshift_seek_request_root.c_str());

    if (ts_node != NULL)
    {
        buf.clear(); buf.str("");
        buf << tsr.channel_handle_;
        dvblink::pugixml_helpers::new_child(ts_node, timeshift_seek_handle_node, buf.str());

        buf.clear(); buf.str("");
        buf << tsr.type_;
        dvblink::pugixml_helpers::new_child(ts_node, timeshift_seek_type_node, buf.str());

        buf.clear(); buf.str("");
        buf << tsr.offset_;
        dvblink::pugixml_helpers::new_child(ts_node, timeshift_seek_offset_node, buf.str());

        buf.clear(); buf.str("");
        buf << tsr.whence_;
        dvblink::pugixml_helpers::new_child(ts_node, timeshift_seek_whence_node, buf.str());
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, timeshift_seek_request_t& tsr)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, timeshift_seek_handle_node, str))
            tsr.channel_handle_ = atoi(str.c_str());

        if (dvblink::pugixml_helpers::get_node_value(node, timeshift_seek_type_node, str))
            tsr.type_ = atoi(str.c_str());

        if (dvblink::pugixml_helpers::get_node_value(node, timeshift_seek_whence_node, str))
            tsr.whence_ = atoi(str.c_str());

        if (dvblink::pugixml_helpers::get_node_value(node, timeshift_seek_offset_node, str))
        {
            try
            {
                tsr.offset_ = boost::lexical_cast<boost::int64_t>(str);
            }
            catch (const boost::bad_lexical_cast&)
            {
                tsr.offset_ = 0;
            }
        }

    }

    return node;
}


} //dvblex
