/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <map>
#include <sstream>
#include <dl_types.h>
#include <dl_programs.h>
#include <dl_epgevent.h>
#include <dl_strings.h>

namespace dvblex { namespace recorder {

#define DL_RECORDER_KEEP_ALL				    (0)

//invalid start marginhas to be more that 24 hours in seconds
#define DL_RECORDER_INVALID_START_MARGIN_VALUE  (100000)

#define DL_RECORDER_SERIES_TIME_DELTA_SEC       time_t(15 * 60)

enum schedule_item_type_e
{
    sitManual,
    sitEpgBased,
    sitPatternBased
};

inline const std::string convert_targets_to_string(const std::vector<dvblink::base_id_t>& targets)
{
    std::stringstream strbuf;
    for (size_t i=0; i<targets.size(); i++)
    {
        if (i != 0)
            strbuf << ",";

        strbuf << targets[i].to_string();
    }
    return strbuf.str();
}

inline void parse_targets_from_string(const std::string& str, std::vector<dvblink::base_id_t>& targets)
{
    if (str.size() > 0)
    {
        std::vector<std::string> tokens;
        boost::split( tokens, str, boost::is_any_of(","), boost::token_compress_off);
        for (size_t i=0; i<tokens.size(); i++)
        {
            targets.push_back(dvblink::base_id_t(tokens[i]));
        }
    }
}

class schedule_item
{
public:
    schedule_item() :
        schedule_item_id_(DL_RECORDER_INVALID_ID),
        number_of_recordings_to_keep_(DL_RECORDER_KEEP_ALL),
        record_series_(false),
        record_series_new_only_(false),
        disabled_(false),
        genre_mask_(0),
        margin_before_(0),
        margin_after_(0),
        start_time_(0),
        duration_(0),
        day_mask_(0),
        start_after_sec_(-1),
        start_before_sec_(-1),
        priority_(0),
        active_(true)
    {
    }
    
    ~schedule_item() {}

    bool is_recursive() const {return (is_manual() && day_mask_ != 0) || (is_epg_based() && record_series_) || is_pattern_based();}

    schedule_item_type_e type() const {return type_;}

    dvblink::schedule_name_t get_schedule_name() const
    {
        if (is_manual()) {return name_;}
        if (is_epg_based())
        {
            dvblink::engine::DLEPGEventList epgEventList;
            if (dvblink::engine::EPGReadEventsFromXML(epg_program_info_, epgEventList) && epgEventList.size() == 1)
            {
                return dvblink::engine::string_cast<dvblink::engine::EC_UTF8>(epgEventList[0].m_Name);
            }
        }
        if (is_pattern_based())
        {
            if (key_phrase_.get().size() > 0)
            {
                std::string ret_val = key_phrase_.get();
                ret_val = "*" + ret_val + "*";
                return ret_val;
            } else
            {
                //by genre
                std::stringstream strbuf;
                strbuf << "By genre (" << schedule_item_id_ << ")";
                return strbuf.str();
            }
        }

        std::stringstream strbuf;
        strbuf << schedule_item_id_;
        return strbuf.str();
    }

    bool is_manual() const {return type_ == sitManual;}
    bool is_epg_based() const {return type_ == sitEpgBased;}
    bool is_pattern_based() const {return type_ == sitPatternBased;}

    const dvblink::schedule_item_id_t& schedule_item_id() const {return schedule_item_id_;}
    
    time_t real_start_time() const {return start_time_ - margin_before_;}
    time_t real_duration() const {return duration_ + margin_after_ + margin_before_;}
    time_t main_start_time() const {return start_time_;}
    time_t main_duration() const {return duration_;}
    time_t margin_before() const {return margin_before_;}
    time_t margin_after() const {return margin_after_;}

    const dvblink::channel_id_t& channel() const {return channel_;}
    const dvblink::schedule_name_t& name() const {return name_;}
    boost::uint8_t day_mask() const {return day_mask_;}
    bool record_series() const {return record_series_;}
    bool record_series_new_only() const {return record_series_new_only_;}
    const std::string& extra_param() const {return extra_param_;}
    const dvblink::epg_event_id_t& epg_event_id() const {return epg_event_id_;}
    const std::string& epg_program_info() const {return epg_program_info_;}
    int number_of_recordings_to_keep() const {return number_of_recordings_to_keep_;}
    bool disabled() const {return disabled_;}

    const std::vector<dvblink::base_id_t>& get_sendto_targets() {return targets_;}

    const std::string get_targets_as_string() const
    {
        return convert_targets_to_string(targets_);
    }

    const dvblink::key_phrase_t& get_key_phrase() const {return key_phrase_;}
    unsigned long get_genre_mask() const { return genre_mask_;}

    void set_disabled(bool disabled) {disabled_ = disabled;}
    void set_number_of_recordings_to_keep(int number_of_recordings_to_keep) {number_of_recordings_to_keep_ = number_of_recordings_to_keep;}
    void set_epg_program_info(const std::string& info) {epg_program_info_ = info;}

    void set(char** values, std::map<std::string, int>& value_map)
    {
        if (value_map.find("id") != value_map.end() && values[value_map["id"]] != NULL)
        {
            dvblink::schedule_item_id_t::type_t temp = schedule_item_id_.get();
            dvblink::engine::string_conv::apply(values[value_map["id"]], temp, static_cast<dvblink::schedule_item_id_t::type_t>(DL_RECORDER_INVALID_ID));
            schedule_item_id_.set(temp);
        }

        if (value_map.find("type") != value_map.end() && values[value_map["type"]] != NULL)
            dvblink::engine::string_conv::apply<int>(values[value_map["type"]], type_, sitManual);

        if (value_map.find("channel") != value_map.end() && values[value_map["channel"]] != NULL)
            channel_ = values[value_map["channel"]];

        if (value_map.find("extra_param") != value_map.end() && values[value_map["extra_param"]] != NULL)
            extra_param_ = values[value_map["extra_param"]];

        if (value_map.find("name") != value_map.end() && values[value_map["name"]] != NULL)
            name_ = values[value_map["name"]];

        if (value_map.find("start_time") != value_map.end() && values[value_map["start_time"]] != NULL)
            dvblink::engine::string_conv::apply(values[value_map["start_time"]], start_time_, time_t(0));

        if (value_map.find("margin_before") != value_map.end() && values[value_map["margin_before"]] != NULL)
            dvblink::engine::string_conv::apply(values[value_map["margin_before"]], margin_before_, time_t(0));

        if (value_map.find("margin_after") != value_map.end() && values[value_map["margin_after"]] != NULL)
            dvblink::engine::string_conv::apply(values[value_map["margin_after"]], margin_after_, time_t(0));

        if (value_map.find("duration") != value_map.end() && values[value_map["duration"]] != NULL)
            dvblink::engine::string_conv::apply(values[value_map["duration"]], duration_, time_t(0));

        if (value_map.find("day_mask") != value_map.end() && values[value_map["day_mask"]] != NULL)
            dvblink::engine::string_conv::apply<int, unsigned char, char>(values[value_map["day_mask"]], day_mask_, (unsigned char)(0));

        if (value_map.find("epg_event_id") != value_map.end() && values[value_map["epg_event_id"]] != NULL)
            epg_event_id_ = values[value_map["epg_event_id"]];

        if (value_map.find("epg_program_info") != value_map.end() && values[value_map["epg_program_info"]] != NULL)
            epg_program_info_ = values[value_map["epg_program_info"]];

        if (value_map.find("series") != value_map.end() && values[value_map["series"]] != NULL)
            dvblink::engine::string_conv::apply(values[value_map["series"]], record_series_, false);

        if (value_map.find("series_new_only") != value_map.end() && values[value_map["series_new_only"]] != NULL)
            dvblink::engine::string_conv::apply(values[value_map["series_new_only"]], record_series_new_only_, false);

        if (value_map.find("number_to_keep") != value_map.end() && values[value_map["number_to_keep"]] != NULL)
        {
            dvblink::engine::string_conv::apply(values[value_map["number_to_keep"]], number_of_recordings_to_keep_, DL_RECORDER_KEEP_ALL);
            if (number_of_recordings_to_keep_ < 0)
                number_of_recordings_to_keep_ = DL_RECORDER_KEEP_ALL;
        }

        if (value_map.find("disabled") != value_map.end() && values[value_map["disabled"]] != NULL)
        {
            int temp_int = 0;
            dvblink::engine::string_conv::apply(values[value_map["disabled"]], temp_int, 0);
            disabled_ = temp_int != 0;
        }
        
        if (value_map.find("genre_mask") != value_map.end() && values[value_map["genre_mask"]] != NULL)
            dvblink::engine::string_conv::apply(values[value_map["genre_mask"]], genre_mask_, (unsigned long)(0));

        if (value_map.find("key_phrase") != value_map.end() && values[value_map["key_phrase"]] != NULL)
            key_phrase_ = values[value_map["key_phrase"]];

        if (value_map.find("sendto_targets") != value_map.end() && values[value_map["sendto_targets"]] != NULL)
            parse_targets_from_string(values[value_map["sendto_targets"]], targets_);

        if (value_map.find("start_after_sec") != value_map.end() && values[value_map["start_after_sec"]] != NULL)
            dvblink::engine::string_conv::apply(values[value_map["start_after_sec"]], start_after_sec_, -1);

        if (value_map.find("start_before_sec") != value_map.end() && values[value_map["start_before_sec"]] != NULL)
            dvblink::engine::string_conv::apply(values[value_map["start_before_sec"]], start_before_sec_, -1);

        if (value_map.find("priority") != value_map.end() && values[value_map["priority"]] != NULL)
            dvblink::engine::string_conv::apply(values[value_map["priority"]], priority_, -1);

        if (value_map.find("active") != value_map.end() && values[value_map["active"]] != NULL)
            dvblink::engine::string_conv::apply(values[value_map["active"]], active_, true);
    }

    void generate_default_start_margins()
    {
        bool b = false;
        if (is_epg_based() && record_series_)
        {
            dvblink::engine::DLEPGEventList epgEventList;
            if (dvblink::engine::EPGReadEventsFromXML(epg_program_info_, epgEventList) && epgEventList.size() == 1)
            {
                //start after
                time_t t = epgEventList[0].m_StartTime - DL_RECORDER_SERIES_TIME_DELTA_SEC;
                struct tm* lt = localtime(&t);
                start_after_sec_ = lt->tm_sec + lt->tm_min*60 + lt->tm_hour*3600;

                //start before
                t = epgEventList[0].m_StartTime + DL_RECORDER_SERIES_TIME_DELTA_SEC;
                lt = localtime(&t);
                start_before_sec_ = lt->tm_sec + lt->tm_min*60 + lt->tm_hour*3600;

                b = true;
            }
        }
        //if there was an error - reset values to defaults
        if (!b)
        {
            start_after_sec_ = -1;
            start_before_sec_ = -1;
        }
    }

public:
    dvblink::schedule_item_id_t      schedule_item_id_;
    schedule_item_type_e    type_;
    std::string             extra_param_;
    dvblink::channel_id_t            channel_;
    
	int number_of_recordings_to_keep_; //number of recordings to keep (more than that will be deleted automatically on new record). -1 - keep all

    time_t                  margin_before_;
    time_t                  margin_after_;

    //manual
    dvblink::schedule_name_t         name_;
    time_t                  start_time_;
    time_t                  duration_;
    
    //manual recording: day of the week mask for recurring recordings (0 bit: Sunday, 1 bit: Monday etc.)
    //recursive epg/pattern: days on which to record (if 0 - record on all days)
    boost::uint8_t          day_mask_;  
    
    //epg based
    dvblink::epg_event_id_t          epg_event_id_;
    std::string             epg_program_info_;
    bool                    record_series_;
	bool			        record_series_new_only_;  //record either new only or new and rerun

    //means that schedule has been deleted, but there are still recordings from this schedule in database
    //schedule wil be actuall deleted when all its recordings are deleted
    bool                    disabled_;
    
    //pattern based
    dvblink::key_phrase_t            key_phrase_;
    unsigned long           genre_mask_;

    //sendto targets
    std::vector<dvblink::base_id_t> targets_;

    //new things

    //start after some time (local time) and before some time (local time)
    //both optional, no limit if set to -1
    //substitute record_series_anytime_. Applicable for recursive epg and pattern
    int start_after_sec_;
    int start_before_sec_;

    //schedule priority. 0 is normal, -1 is low, +1 is high
    int priority_;

    //schedule can be temporarily deactivated
    bool active_;
};

class update_schedule_item_info
{
public:
    update_schedule_item_info() :
        schedule_item_id_(DL_RECORDER_INVALID_ID),
        number_of_recordings_to_keep_(DL_RECORDER_KEEP_ALL),
        record_series_new_only_(false),
        margin_before_(DL_RECORDER_INVALID_TIME),
        margin_after_(DL_RECORDER_INVALID_TIME),
        day_mask_(0),
        start_after_sec_(-1),
        start_before_sec_(-1),
        priority_(0),
        active_(true),
        supports_v2_schedule_(true)
    {
    }

    const std::string get_targets_as_string() const
    {
        return convert_targets_to_string(targets_);
    }

public:
    dvblink::schedule_item_id_t schedule_item_id_;
	int number_of_recordings_to_keep_; //number of recordings to keep (more than that will be deleted automatically on new record). -1 - keep all
	bool record_series_new_only_;  //record either new only or new and rerun

    time_t  margin_before_;
    time_t  margin_after_;
    std::vector<dvblink::base_id_t> targets_;
    boost::uint8_t          day_mask_;
    int start_after_sec_;
    int start_before_sec_;
    int priority_;
    bool active_;

    bool supports_v2_schedule_;
};

typedef std::vector<schedule_item> schedule_list_t;
typedef std::map<dvblink::schedule_item_id_t, schedule_item> schedule_map_t;

inline void make_schedule_map_from_list(schedule_list_t& schedule_list, schedule_map_t& schedule_map)
{
    schedule_map.clear();
    schedule_list_t::iterator it = schedule_list.begin();
    while (it != schedule_list.end())
    {
        schedule_map[it->schedule_item_id()] = *it;
        ++it;
    }
}

} //recorder
} //dvblex
