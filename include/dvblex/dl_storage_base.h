/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_CONFIG_STORAGE_STORAGE_BASE_H_
#define __DVBLINK_CONFIG_STORAGE_STORAGE_BASE_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
#include <boost/lexical_cast.hpp>
#include <dl_types.h>
#include <dl_storage_path.h>
#include <dl_pugixml_helper.h>
#include <dl_filesystem_path.h>

namespace dvblex {

namespace settings {

class node;

struct node_description
{
    node_description() {}
    node_description(const storage_path& path, value_t value) :
        path_(path),
        value_(value)
    {}
    storage_path path_;
    value_t      value_;
};

typedef std::vector<node_description> storage_node_content_t;

class storage_base
{
    template <typename Out>
    bool enum_leaf_impl(const storage_path& p, Out& result)
    {
        if (node* n = find_node(p))
        {
            enum_leaf(n, result);
            return true;
        }
        return false;
    }

public:
    enum save_type
    {
        saving_deffered,
        saving_at_once
    };

    storage_base(const std::string& root_node_name, bool b_encode_tags = false);
    virtual ~storage_base();

    bool create();
    bool open(const dvblink::filesystem_path_t& p);
    bool save(xml_string_t* xml = NULL);

    bool reload();

    bool is_initialized() {return initialized_;}

    template <typename T>
    bool get_value(const storage_path& path, T& value)
    {
        boost::unique_lock<boost::shared_mutex> lock(lock_);

        value_t val;
        if (get_value(path, val))
        {
            try
            {
                value = boost::lexical_cast<T>(val.get());
                return true;
            }
            catch (boost::bad_lexical_cast& /*e*/)
            {
                return false;
            }
        }
        return false;
    }

    template <typename T>
    bool set_value(const storage_path& path, const T& value, storage_base::save_type save = saving_deffered)
    {
        boost::unique_lock<boost::shared_mutex> lock(lock_);

        try
        {
            value_t val(boost::lexical_cast<std::wstring>(value));
            bool res = set_value(path, val);
            if (res && save == saving_at_once)
            {   
                storage_base::do_save();
            }
            return res;
        }
        catch (boost::bad_lexical_cast& /*e*/)
        {
            return false;
        }
    }

    template <typename Out>
    bool enum_leaf(const storage_path& p, Out& result)
    {
        boost::unique_lock<boost::shared_mutex> lock(lock_);
        return enum_leaf_impl(p, result);
    }

    bool copy_recursive(const storage_path& src_path, const storage_path& dst_path);
    bool remove(const storage_path& src_path, storage_base::save_type save = saving_deffered);
    bool rename(const storage_path& src_path, std::wstring new_name);
    node* create_node(const storage_path& path);

    bool is_exist(const storage_path& path) {return find_node(path) ? true : false;}

    bool read_node(const storage_path& path, storage_node_content_t& node_content);
    bool write_node(const storage_path& path, const storage_node_content_t& node_content, bool clean = false);

    bool insert_xml(const pugi::xml_node& chapter_node, const storage_path& path_root, storage_base::save_type save = saving_deffered);

private:
    bool read_node_impl(const storage_path& path_root, const storage_path& path_middle, storage_node_content_t& node_content);
    bool remove_impl(const storage_path& path);
    void enum_leaf(node* n, std::vector<std::wstring>& collection);

    bool read_xml_node(const pugi::xml_node& chapter_node, node* parent);
    bool write_xml_node(const node* src_node, pugi::xml_node& dst_node);

    node* find_node(const storage_path& path);
    bool get_value(const storage_path& path, value_t& value);
    bool set_value(const storage_path& path, const value_t& value);

    bool do_save(xml_string_t* xml = NULL);
    bool do_open(const dvblink::filesystem_path_t& p);
    bool do_create();

    std::string convert_tag(const std::wstring& from);
    std::wstring convert_tag(const pugi::char_t* from);
    std::string convert(const std::wstring& from);
    std::wstring convert(const pugi::char_t* from);
    std::wstring convert(const std::string& from);

private:
    mutable boost::shared_mutex lock_;

    node* node_;
    std::string root_node_name_;
    dvblink::filesystem_path_t config_path_;

    bool initialized_;
    bool encode_tags_;
};

} // configuration
} // dvblink

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_CONFIG_STORAGE_STORAGE_BASE_H_
