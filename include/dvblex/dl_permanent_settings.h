/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread/mutex.hpp>
#include <dl_types.h>
#include <dl_filesystem_path.h>
#include <dl_storage_base.h>

namespace dvblex { namespace settings {

class permanent_settings_storage : public storage_base
{
    friend class std::auto_ptr<permanent_settings_storage>;
public:
    bool open();
    static permanent_settings_storage& instance();

    static void shutdown();

private:
    permanent_settings_storage();
    ~permanent_settings_storage();

    permanent_settings_storage& operator = (permanent_settings_storage&);
    bool get_permanent_settings_file_directory(filesystem_path_t& folder);

private:
    static std::auto_ptr<permanent_settings_storage> instance_;
    static boost::mutex lock_;
};

class permanent_settings
{
public:
    static filesystem_path_t get_server_install_path();
    static void set_server_install_path(filesystem_path_t& install_path);

    static filesystem_path_t get_private_root();
    static void set_private_root(filesystem_path_t& private_root);

    static filesystem_path_t get_shared_root();
    static void set_shared_root(filesystem_path_t& shared_root);

    static client_id_t get_server_id();
    static bool set_server_id(const client_id_t& id);
};

} //settings
} //dvblink

