/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <dl_types.h>
#include <dl_channel_info.h>
#include <dl_provider_info.h>
#include <dl_headend.h>

namespace dvblex { 

struct channel_changes_desc_t
{
    bool has_changes() const
    {
        return has_config_changes() || has_visibility_changes() || has_epg_config_changes();
    }

    bool has_config_changes() const
    {
        return added_channels_.size() > 0 || deleted_channels_.size() > 0 || added_headends_.size() > 0 || deleted_headends_.size() > 0;
    }

    bool has_visibility_changes() const
    {
        return hidden_channels_.size() > 0 || shown_channels_.size() > 0;
    }

    bool has_epg_config_changes() const
    {
        return epg_config_channels_.size() > 0;
    }

    channel_id_list_t added_channels_;
    channel_id_list_t deleted_channels_;
    headend_id_list_t added_headends_;
    headend_id_list_t deleted_headends_;
    channel_id_list_t hidden_channels_;
    channel_id_list_t shown_channels_;
    channel_id_list_t epg_config_channels_;
};

////////////////////////////////////////////////////////////////

struct concise_device_channel_t
{
    concise_device_channel_t() :
        encrypted_(false), num_(invalid_channel_number_), sub_num_(invalid_channel_number_)
    {
    }

    void from_device_channel(const device_channel_t& ch)
    {
        id_ = ch.id_;
        name_ = ch.name_;
        origin_ = ch.origin_;
        num_ = ch.num_;
        sub_num_ = ch.sub_num_;
        type_ = ch.type_;
        encrypted_ = ch.encrypted_;
        logo_ = ch.logo_;
        comment_ = ch.comment_;
    }

    dvblink::channel_id_t id_;
    dvblink::channel_name_t name_;
    dvblink::channel_origin_t origin_;
    dvblink::channel_enc_t encrypted_;
    dvblink::channel_number_t num_;
    dvblink::channel_subnumber_t sub_num_;
    channel_type_e type_;
    dvblink::channel_logo_addr_t logo_;
    dvblink::channel_comment_t comment_;
};

typedef std::vector<concise_device_channel_t> concise_channel_desc_list_t;

struct concise_transponder_description_t
{
    dvblink::transponder_id_t tr_id_;
    dvblink::transponder_name_t tr_name_;
    concise_channel_desc_list_t channels_;
};

typedef std::vector<concise_transponder_description_t> concise_transponder_desc_list_t;

struct concise_channel_headend_t
{
    dvblink::headend_id_t id_;
    dvblink::headend_name_t name_;
    dvblink::headend_desc_t desc_;
    concise_transponder_desc_list_t transponders_;
};

typedef std::vector<concise_channel_headend_t> concise_headend_desc_list_t;

////////////////////////////////////////////////////////////////

struct idonly_transponder_description_t
{
    dvblink::transponder_id_t id_;
    channel_id_list_t channels_;
};

typedef std::vector<idonly_transponder_description_t> idonly_transponder_desc_list_t;

struct idonly_channel_headend_t
{
    dvblink::headend_id_t id_;
    idonly_transponder_desc_list_t transponders_;
};

typedef std::vector<idonly_channel_headend_t> idonly_headend_desc_list_t;

////////////////////////////////////////////////////////////////


struct channel_description_t
{
    channel_description_t() :
        encrypted_(false), num_(invalid_channel_number_), sub_num_(invalid_channel_number_),
        type_(ct_tv), child_lock_(false)
    {}

    void from_concise_channel(const concise_device_channel_t& ch)
    {
        id_ = ch.id_;
        name_ = ch.name_;
        origin_ = ch.origin_;
        num_ = ch.num_;
        sub_num_ = ch.sub_num_;
        type_ = ch.type_;
        encrypted_ = ch.encrypted_;
        logo_ = ch.logo_.get();
    }

    dvblink::channel_id_t id_;
    dvblink::channel_name_t name_;
    dvblink::channel_origin_t origin_;
    dvblink::channel_enc_t encrypted_;
    dvblink::channel_number_t num_;
    dvblink::channel_subnumber_t sub_num_;
    channel_type_e type_;
    dvblink::url_address_t logo_;
    dvblink::channel_lock_t child_lock_;
};

typedef std::vector<channel_description_t> channel_desc_list_t;

////////////////////////////////////////////////////////////////////////////

enum channel_favorite_type_e
{
    cft_none = 0x00000000,
    cft_auto = 0x00000001,
    cft_manual = 0x00000002
};

struct channel_favorite_t
{
    channel_favorite_t() :
        flags_(cft_none)
    {}

    dvblink::favorite_id_t id_;
    dvblink::favorite_name_t name_;
    dvblink::favorite_flags_t flags_;
    channel_id_list_t channels_;
};

typedef std::vector<channel_favorite_t> channel_favorite_list_t;

///////////////////////////////////////////////////////////
typedef std::map<dvblink::channel_id_t, dvblink::channel_id_t> invisible_channel_map_t;

///////////////////////////////////////////////////////////

static const dvblink::concurrency_num_t unlimited_concurrency_channel_num(-1); // it means that unlimited number of channels 
                                                                      // from the same channel set can be played simultaneously
struct channel_set_t
{
    channel_set_t() {}
    channel_set_t(const dvblink::channel_set_id_t& channel_set_id, const dvblink::concurrency_num_t& max_concurrency_num, const std::vector<dvblink::channel_id_t>& channels) :
        channel_set_id_(channel_set_id),
        max_concurrency_num_(max_concurrency_num),
        channels_(channels)
    {

    }
    dvblink::channel_set_id_t channel_set_id_;
    dvblink::concurrency_num_t max_concurrency_num_;
    std::vector<dvblink::channel_id_t> channels_;
};

typedef std::vector<channel_set_t> set_of_channel_sets_t;                  // aka whole transponder with channel subsets
typedef std::vector<set_of_channel_sets_t> set_of_set_of_channels_set_t;    // aka all transponders data for the ts_source
typedef std::map<dvblink::device_id_t, set_of_set_of_channels_set_t> ts_source_TO_set_of_set_of_channels_set_t; // channel sets info for all ts_sources

//channel logo

struct channel_logo_t
{
    dvblink::logo_id_t id_;
    dvblink::url_address_t url_;
    dvblink::url_address_t default_logo_;
};

typedef std::map<dvblink::channel_id_t, channel_logo_t> channel_logo_desc_map_t;
typedef std::map<dvblink::channel_id_t, dvblink::logo_id_t> channel_logo_id_map_t;

typedef std::vector<dvblink::logo_package_t> logo_package_list_t;

struct channel_package_logo_item_t
{
    dvblink::logo_id_t id_;
    dvblink::logo_name_t name_;
    dvblink::url_address_t url_;
};

typedef std::vector<channel_package_logo_item_t> channel_package_logo_item_list_t;
typedef std::map<dvblink::channel_id_t, dvblink::url_address_t> channel_logo_map_t;

typedef std::vector<dvblink::logo_id_t> channel_logo_id_list_t;

struct channel_logo_match_t
{
    channel_logo_id_list_t exact_match_;
    channel_logo_id_list_t partial_match_;
};

typedef std::map<dvblink::channel_id_t, channel_logo_match_t> channel_logo_match_map_t;

//channel overwrites

struct channel_overwrite_t
{
    channel_overwrite_t() :
        number_(invalid_channel_number_), subnumber_(invalid_channel_number_)
    {}

    bool is_empty() {return name_.empty() && (number_.get() <= 0);}

    dvblink::channel_name_t name_;
    dvblink::channel_number_t number_;
    dvblink::channel_subnumber_t subnumber_;
};

typedef std::map<dvblink::channel_id_t, channel_overwrite_t> channel_overwrite_map_t;

} //dvblex

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::device_channel_t& dc, const unsigned int /*version*/)
{
    ar & dc.id_;
    ar & dc.name_;
    ar & dc.origin_;
    ar & dc.tid_;
    ar & dc.nid_;
    ar & dc.sid_;
    ar & dc.encrypted_;
    ar & dc.num_;
    ar & dc.sub_num_;
    ar & dc.logo_;
    ar & dc.type_;
    ar & dc.tune_params_;
    ar & dc.comment_;    
}

template<class Archive>
void serialize(Archive& ar, dvblex::channel_changes_desc_t& ccd, const unsigned int /*version*/)
{
    ar & ccd.added_channels_;
    ar & ccd.deleted_channels_;
    ar & ccd.added_headends_;
    ar & ccd.deleted_headends_;
    ar & ccd.hidden_channels_;
    ar & ccd.shown_channels_;
    ar & ccd.epg_config_channels_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::channel_description_t& cd, const unsigned int /*version*/)
{
    ar & cd.id_;
    ar & cd.name_;
    ar & cd.origin_;
    ar & cd.encrypted_;
    ar & cd.num_;
    ar & cd.sub_num_;
    ar & cd.type_;
    ar & cd.logo_;
    ar & cd.child_lock_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::concise_device_channel_t& cd, const unsigned int /*version*/)
{
    ar & cd.id_;
    ar & cd.name_;
    ar & cd.origin_;
    ar & cd.encrypted_;
    ar & cd.num_;
    ar & cd.sub_num_;
    ar & cd.logo_;
    ar & cd.type_;
    ar & cd.comment_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::channel_favorite_t& cf, const unsigned int /*version*/)
{
    ar & cf.id_;
    ar & cf.name_;
    ar & cf.flags_;
    ar & cf.channels_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::concise_transponder_description_t& td, const unsigned int /*version*/)
{
    ar & td.tr_id_;
    ar & td.tr_name_;
    ar & td.channels_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::concise_channel_headend_t& cp, const unsigned int /*version*/)
{
    ar & cp.id_;
    ar & cp.name_;
    ar & cp.desc_;
    ar & cp.transponders_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::idonly_transponder_description_t& iotd, const unsigned int /*version*/)
{
    ar & iotd.id_;
    ar & iotd.channels_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::idonly_channel_headend_t& iocp, const unsigned int /*version*/)
{
    ar & iocp.id_;
    ar & iocp.transponders_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::channel_set_t& cs, const unsigned int /*version*/)
{
    ar & cs.channel_set_id_;
    ar & cs.max_concurrency_num_;
    ar & cs.channels_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::channel_overwrite_t& ov, const unsigned int /*version*/)
{
    ar & ov.name_;
    ar & ov.number_;
    ar & ov.subnumber_;
}

} // namespace serialization
} // namespace boost
