/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <dl_strings.h>
#include <dl_types.h>
#include <dl_url_encoding.h>
#include <dl_pb_container.h>
#include <dl_pb_types.h>
#include <boost/lexical_cast.hpp>
#include <dl_pugixml_helper.h>

namespace dvblex { namespace playback {

const dvblink::object_id_t object_root_id = "";
const int object_count_all = -1;
const int object_start_position = 0;
const std::string object_id_sep = ":";

enum pb_object_type_e
{
    pot_object_unknown = -1,
    pot_object_container,
    pot_object_item
};

struct pb_object_t
{
    pb_object_t() :
      actual_count_(0), total_count_(0) { }

    pb_container_list_t container_list_;
    pb_item_list_t item_list_;
    boost::int32_t actual_count_;
    boost::int32_t total_count_;
};

const std::string object_root_node                   = "object";
const std::string object_actual_count_node           = "actual_count";
const std::string object_total_count_node            = "total_count";


inline pugi::xml_document& operator<< (pugi::xml_document& doc, const pb_object_t& object_info)
{
    std::stringstream buf;

    pugi::xml_node object_node = doc.append_child(object_root_node.c_str());
    if (object_node != NULL)
    {
        write_to_node(object_node, object_info.container_list_);
        
        write_to_node(object_node, object_info.item_list_);

        std::stringstream str_out;

        str_out.clear(); str_out.str("");
        str_out << object_info.actual_count_;
        dvblink::pugixml_helpers::new_child(object_node, object_actual_count_node, str_out.str());
        
        str_out.clear(); str_out.str("");
        str_out << object_info.total_count_;
        dvblink::pugixml_helpers::new_child(object_node, object_total_count_node, str_out.str());
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, pb_object_t& object_info)
{
    if (NULL != node)
    {
        read_from_node(node, object_info.container_list_);
        
        read_from_node(node, object_info.item_list_);

        std::string str;

        boost::int32_t value;
        if (dvblink::pugixml_helpers::get_node_value(node, object_actual_count_node, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), value, (boost::int32_t)object_info.actual_count_);
            object_info.actual_count_ = value;
        }

        if (dvblink::pugixml_helpers::get_node_value(node, object_total_count_node, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), value, (boost::int32_t)object_info.total_count_);
            object_info.total_count_ = value;
        }
    }
    return node;
}


struct pb_object_requester_t
{
    pb_object_requester_t(bool is_children_request = false) :
      object_type_(pot_object_unknown), item_type_(pbit_item_unknown),
      start_position_(object_start_position), requested_count_(object_count_all), is_children_request_(is_children_request) { }

    bool is_root_request() const { return object_id_ == object_root_id; }
    bool is_obj_children_request() const { return is_children_request_; }

    dvblink::object_id_t object_id_;
    pb_object_type_e object_type_;
    pb_item_type_e item_type_;
    int start_position_;
    int requested_count_;
    bool is_children_request_;
    dvblink::url_address_t server_address_;
};

const std::string object_requester_root_node         = "object_requester";
const std::string object_requester_object_id_node    = "object_id";
const std::string object_requester_object_type_node  = "object_type";
const std::string object_requester_item_type_node    = "item_type";
const std::string object_requester_position_node     = "start_position";
const std::string object_requester_count_node        = "requested_count";
const std::string object_requester_type_node         = "children_request";
const std::string object_requester_server_addr_node  = "server_address";

inline pugi::xml_node& operator>> (pugi::xml_node& node, pb_object_requester_t& object_requester)
{
    if (NULL != node)
    {
        std::string str;

        if (dvblink::pugixml_helpers::get_node_value(node, object_requester_object_id_node, str))
            object_requester.object_id_ = str;

        unsigned long ul;
        if (dvblink::pugixml_helpers::get_node_value(node, object_requester_object_type_node, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), ul, (unsigned long)pot_object_unknown);
            object_requester.object_type_ = (pb_object_type_e)ul;
        }

        if (dvblink::pugixml_helpers::get_node_value(node, object_requester_item_type_node, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), ul, (unsigned long)pbit_item_unknown);
            object_requester.item_type_ = (pb_item_type_e)ul;
        }

        if (dvblink::pugixml_helpers::get_node_value(node, object_requester_position_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), object_requester.start_position_, (int)object_start_position);

        if (dvblink::pugixml_helpers::get_node_value(node, object_requester_count_node, str))
            dvblink::engine::string_conv::apply(str.c_str(), object_requester.requested_count_, (int)object_count_all);

        if (dvblink::pugixml_helpers::get_node_value(node, object_requester_type_node, str) &&
            boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true))
            object_requester.is_children_request_ = true;

    }
    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const pb_object_requester_t& object_requester)
{
    std::stringstream buf;

    pugi::xml_node or_node = doc.append_child(object_requester_root_node.c_str());
    if (or_node != NULL)
    {

        dvblink::pugixml_helpers::new_child(or_node, object_requester_object_id_node, object_requester.object_id_.get());

        std::stringstream str_out;

        str_out.clear(); str_out.str("");
        str_out << object_requester.object_type_;
        dvblink::pugixml_helpers::new_child(or_node, object_requester_object_type_node, str_out.str());

        str_out.clear(); str_out.str("");
        str_out << object_requester.item_type_;
        dvblink::pugixml_helpers::new_child(or_node, object_requester_item_type_node, str_out.str());

        str_out.clear(); str_out.str("");
        str_out << object_requester.start_position_;
        dvblink::pugixml_helpers::new_child(or_node, object_requester_position_node, str_out.str());

        str_out.clear(); str_out.str("");
        str_out << object_requester.requested_count_;
        dvblink::pugixml_helpers::new_child(or_node, object_requester_count_node, str_out.str());

        if (object_requester.is_children_request_)
            dvblink::pugixml_helpers::new_child(or_node, object_requester_type_node, dvblink::pugixml_helpers::xmlnode_value_true);
    }

    return doc;
}

///////////////////////////////////////////////////////////////////////////////////

struct pb_object_remover_t
{
    dvblink::object_id_t object_id_;
};

const std::string object_remover_root_node         = "object_remover";
const std::string object_remover_object_id_node      = "object_id";

inline pugi::xml_node& operator>> (pugi::xml_node& node, pb_object_remover_t& object_remover)
{
    if (NULL != node)
    {
        std::string str;

        if (dvblink::pugixml_helpers::get_node_value(node, object_remover_object_id_node, str))
            object_remover.object_id_ = str;
    }
    return node;
}

//object searcher
struct pb_object_searcher_t
{
    pb_object_searcher_t() :
      object_type_(pot_object_unknown), item_type_(pbit_item_unknown)
      { }

    bool is_root_request() const { return object_id_ == object_root_id; }

    dvblink::object_id_t object_id_;
    pb_object_type_e object_type_;
    pb_item_type_e item_type_;
    dvblink::url_address_t server_address_;
    dvblink::search_string_t search_string_;
};

const std::string object_searcher_root_node         = "object_searcher";
const std::string object_searcher_search_string_node         = "search_string";

inline pugi::xml_node& operator>> (pugi::xml_node& node, pb_object_searcher_t& object_searcher)
{
    if (NULL != node)
    {
        std::string str;

        if (dvblink::pugixml_helpers::get_node_value(node, object_requester_object_id_node, str))
            object_searcher.object_id_ = str;

        unsigned long ul;
        if (dvblink::pugixml_helpers::get_node_value(node, object_requester_object_type_node, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), ul, (unsigned long)pot_object_unknown);
            object_searcher.object_type_ = (pb_object_type_e)ul;
        }

        if (dvblink::pugixml_helpers::get_node_value(node, object_requester_item_type_node, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), ul, (unsigned long)pbit_item_unknown);
            object_searcher.item_type_ = (pb_item_type_e)ul;
        }

        if (dvblink::pugixml_helpers::get_node_value(node, object_searcher_search_string_node, str))
            object_searcher.search_string_ = str;

    }
    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const pb_object_searcher_t& object_searcher)
{
    std::stringstream buf;

    pugi::xml_node or_node = doc.append_child(object_searcher_root_node.c_str());
    if (or_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(or_node, object_requester_object_id_node, object_searcher.object_id_.get());

        std::stringstream str_out;

        str_out.clear(); str_out.str("");
        str_out << object_searcher.object_type_;
        dvblink::pugixml_helpers::new_child(or_node, object_requester_object_type_node, str_out.str());

        str_out.clear(); str_out.str("");
        str_out << object_searcher.item_type_;
        dvblink::pugixml_helpers::new_child(or_node, object_requester_item_type_node, str_out.str());

        dvblink::pugixml_helpers::new_child(or_node, object_searcher_search_string_node, object_searcher.search_string_.get());
    }

    return doc;
}

///////////////////////////////////////////////////////////////////////////////////

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const pb_object_remover_t& object_remover)
{
    pugi::xml_node or_node = doc.append_child(object_remover_root_node.c_str());
    if (or_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(or_node, object_remover_object_id_node, object_remover.object_id_.get());
    }

    return doc;
}

const std::string dvblex_playback_prefix           = "/stream/playback";

inline const std::string make_object_url(const std::string& proto_header, const dvblink::url_address_t& server_addr, const dvblink::network_port_t& server_port, const std::string& object_id)
{
    std::string object_id_str;
    dvblink::url_encode(object_id, object_id_str);
    std::stringstream wstream;
    wstream << proto_header << "://" << server_addr.get() << ":" << server_port.get() << dvblex_playback_prefix << "?object=" << object_id_str;
    return wstream.str();
}

inline const dvblink::object_id_t make_object_id(const std::string& source_id, const std::string& source_object_id)
{
    std::stringstream wstream;
    wstream << source_id << object_id_sep << source_object_id;
    return wstream.str();
}

inline bool parse_object_id(const dvblink::object_id_t& object_id, std::string& source_id, std::string& source_object_id)
{
    bool ret_val = false;

    std::string::size_type pos = object_id.get().find(object_id_sep);
    if (pos != std::string::npos)
    {
        source_id = object_id.get().substr(0, pos);
        source_object_id = object_id.get().substr(pos + 1, object_id.get().size() - pos - 1);
        ret_val = true;
    }

    return ret_val;
}

} // playback
} // dvblex

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::playback::pb_object_t& po, const unsigned int /*version*/)
{
    ar & po.container_list_;
    ar & po.item_list_;
    ar & po.actual_count_;
    ar & po.total_count_;
}

} // namespace serialization
} // namespace boost
