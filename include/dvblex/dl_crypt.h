/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#ifndef __CRYPT_H__
#define __CRYPT_H__

#include <string>

namespace dvblink {
namespace engine {

static const unsigned char PADDING[128] =
{
	0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

/* SHA1 context. */
typedef struct {
	unsigned int state[5];		/* state (ABCD) */
	unsigned int count[2];		/* number of bits, modulo 2^64 */
	unsigned char buffer[64];	/* input buffer */
} SHA1_CTX;

char* str_reverce(char* str);
std::string str_reverce(std::string src);
std::string dec2hex(int dec);

//static void sha_encode32(unsigned char *output, unsigned int *input, unsigned int len);
//static void sha_decode32(unsigned int *output, const unsigned char *input, unsigned int len);
void sha1_init(SHA1_CTX *context);
//static void sha1_transform(unsigned int state[5], const unsigned char block[64]);
void sha1_update(SHA1_CTX *context, const unsigned char *input, unsigned int input_len);
void sha1_final(unsigned char digest[20], SHA1_CTX *context);
//static inline void bin2hex(char *out, const unsigned char *in, int in_len);
void make_sha1_digest(char *sha1str, unsigned char *digest);
std::string sha1(const char *key);

std::string encode_xml(std::string xml, std::string key);
std::string decode_xml(std::string hash, std::string key);

} // namespace engine
} // namespace dvblink

#endif //__CRYPT_H__
