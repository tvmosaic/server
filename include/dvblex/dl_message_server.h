/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>

#include <dl_types.h>
#include <dl_message.h>
#include <dl_filesystem_path.h>
#include <dl_server_info.h>
#include <dl_transcoder.h>

namespace dvblink { namespace messaging { namespace server {

//
// server_info
//
struct server_info_response
{
    dvblex::server_info_t server_info_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & server_info_;
    }
};

struct server_info_request : public message_send<server_info_request, server_info_response>
{
};

//
// server state
//
struct server_state_response
{
    server_state_t state_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & state_;
    }
};

struct server_state_request : public message_send<server_state_request, server_state_response>
{
};

//
// shared module path
//
struct shared_module_path_response
{
    dvblink::filesystem_path_t path_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & path_;
    }
};

struct shared_module_path_request : public message_send<shared_module_path_request, shared_module_path_response>
{
    shared_module_path_request() {}
    shared_module_path_request(const module_name_t& module_name) : module_name_(module_name) {}
    module_name_t module_name_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & module_name_;
    }
};


//
// private module path
//
struct private_module_path_response
{
    dvblink::filesystem_path_t path_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & path_;
    }
};

struct private_module_path_request : public message_send<private_module_path_request, private_module_path_response>
{
    private_module_path_request() {}
    private_module_path_request(const module_name_t& module_name) : module_name_(module_name) {}
    module_name_t module_name_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & module_name_;
    }
};

//
// common item file info
//
struct common_item_file_info_response
{
    dvblink::filesystem_path_t file_;
    dvblink::filesystem_path_t dir_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & file_;
        ar & dir_;
        ar & result_;
    }
};

struct common_item_file_info_request : public message_send<common_item_file_info_request, common_item_file_info_response>
{
    common_item_file_info_request() {}
    common_item_file_info_request(const fileitem_name_t& file_item) : file_item_(file_item) {}
    dvblink::fileitem_name_t file_item_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & file_item_;
    }
};

//
// get ffmpeg launch params for usecase
//
struct ffmpeg_launch_params_response
{
    dvblink::transcoder::ffmpeg_launch_params_t launch_params_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & launch_params_;
        ar & result_;
    }
};

struct ffmpeg_launch_params_request : public message_send<ffmpeg_launch_params_request, ffmpeg_launch_params_response>
{
    ffmpeg_launch_params_request() {}
    ffmpeg_launch_params_request(const dvblink::tc_usecase_id_t& usecase_id) : usecase_id_(usecase_id) {}

    dvblink::tc_usecase_id_t usecase_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & usecase_id_;
    }
};


} //server
} //messaging
} //dvblink
