/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once
#include <string>
#include <boost/array.hpp>
#include <boost/cstdint.hpp>
#include <boost/filesystem/path.hpp>

namespace dvblink {

class md5
{
public:
    typedef boost::array<boost::uint8_t, 16> hash_t;

public:
    md5();
    md5(const void* data, size_t size);
    explicit md5(const std::string& data);

    bool update(const void* data, size_t size);
    bool update(const std::string& data);

    const hash_t& operator()();

private:
    void reset();

private:
    bool finished_;
    boost::uint32_t state_[4];
    boost::uint32_t count_[2];
    boost::uint8_t buffer_[64];
    hash_t hash_;
};

std::string to_string(const md5::hash_t& hash);

} // namespace dvblink

// $Id: md5.h 8705 2013-07-18 19:12:31Z mike $
