/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <sstream>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <dl_filesystem_path.h>
#include <dl_pugixml_helper.h>
#include <dl_runtime_error.h>

namespace dvblex { 

    const std::string REC_SETTINGS_ROOT_NODE             = "recording_settings";
    const std::string REC_SETTINGS_BEFORE_MARGIN_NODE    = "before_margin";
    const std::string REC_SETTINGS_AFTER_MARGIN_NODE     = "after_margin";
    const std::string REC_SETTINGS_REC_PATH_NODE         = "recording_path";
    const std::string REC_SETTINGS_TOTAL_SPACE_NODE      = "total_space";
    const std::string REC_SETTINGS_AVAIL_SPACE_NODE      = "avail_space";
    const std::string REC_SETTINGS_CHECK_DELETE_NODE     = "check_deleted";
    const std::string REC_SETTINGS_DS_AUTO_MODE_NODE     = "ds_auto_mode";
    const std::string REC_SETTINGS_DS_MAN_VALUE_NODE     = "ds_man_value";
    const std::string REC_SETTINGS_AUTO_DELETE_NODE      = "auto_delete";
    const std::string REC_SETTINGS_NEW_ONLY_ALGO_NODE    = "new_only_algo_type";
    const std::string REC_SETTINGS_FILENAME_PATTERN_NODE = "filename_pattern";
    const std::string REC_SETTINGS_NEW_ONLY_DEFAULT_NODE    = "new_only_default_value";

    enum recorder_new_only_algo_type_e
    {
        rec_noat_not_seen_before,
        rec_noat_epg_repeat_flag,
	    rec_noat_epg_premiere_flag
    };

    enum recorder_new_only_default_value_e
    {
        rec_nodv_not_set,
        rec_nodv_true,
        rec_nodv_false
    };

    class rd_recording_settings_t
    {
        static const boost::int64_t SPACE_UNKNOWN = -1;

    public:
        rd_recording_settings_t() :
          before_margin_(0), after_margin_(0), total_space_kb_(SPACE_UNKNOWN), avail_space_kb_(SPACE_UNKNOWN), check_deleted_recordings_(false),
          auto_disk_space_mode_(true), autodelete_algo_(false), manual_disk_space_threshold_kb_(SPACE_UNKNOWN), 
          new_only_algo_type_(rec_noat_not_seen_before), new_only_default_value_(rec_nodv_not_set) { }
        
        const dvblink::filesystem_path_t get_recording_path() const { return recording_path_; }
        int get_before_margin() const { return before_margin_; }
        int get_after_margin() const { return after_margin_; }
        boost::int64_t get_total_space_kb() const { return total_space_kb_; }
        boost::int64_t get_avail_space_kb() const { return avail_space_kb_; }
        bool check_deleted_recordings() const { return check_deleted_recordings_; }
        bool use_auto_disk_space_mode() const { return auto_disk_space_mode_; }
        boost::int64_t get_manual_disk_space_kb() const { return manual_disk_space_threshold_kb_; }
        bool use_autodelete_algo() const { return autodelete_algo_; }
        recorder_new_only_algo_type_e get_new_only_algo() const { return new_only_algo_type_; }
		const std::string get_filename_pattern() const { return filename_pattern_; }
        recorder_new_only_default_value_e get_new_only_default_value() const {return new_only_default_value_;}

        void set_recording_path(const dvblink::filesystem_path_t& recording_path) { recording_path_ = recording_path; }
        void set_before_margin(int before_margin) { before_margin_ = before_margin; }
        void set_after_margin(int after_margin) { after_margin_ = after_margin; }
        void set_total_space_kb(boost::int64_t total_space_kb) { total_space_kb_ = total_space_kb; }
        void set_avail_space_kb(boost::int64_t avail_space_kb) { avail_space_kb_ = avail_space_kb; }
        void set_check_deleted_recordings(bool check) { check_deleted_recordings_ = check; }
        void set_auto_disk_space_mode(bool auto_disk_space_mode) { auto_disk_space_mode_ = auto_disk_space_mode; }
        void set_manual_disk_space_kb(boost::int64_t disk_space) { manual_disk_space_threshold_kb_ = disk_space; }
        void set_autodelete_algo(bool autodelete_algo) { autodelete_algo_ = autodelete_algo; }
        void set_new_only_algo(recorder_new_only_algo_type_e algo_type) { new_only_algo_type_ = algo_type; }
		void set_filename_pattern(const std::string& filename_pattern) { filename_pattern_ = filename_pattern; }
        void set_new_only_default_value(recorder_new_only_default_value_e value) {new_only_default_value_ = value;}

    private:
        int before_margin_;
        int after_margin_;
        dvblink::filesystem_path_t recording_path_;
        boost::int64_t total_space_kb_;
        boost::int64_t avail_space_kb_;
        bool check_deleted_recordings_;
        bool auto_disk_space_mode_;
        boost::int64_t manual_disk_space_threshold_kb_;
        bool autodelete_algo_;
        recorder_new_only_algo_type_e new_only_algo_type_;
		std::string filename_pattern_;
        recorder_new_only_default_value_e new_only_default_value_;
    };

    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const rd_recording_settings_t& settings)
    {
        pugi::xml_node settings_node = doc.append_child(REC_SETTINGS_ROOT_NODE.c_str());
        if (settings_node != NULL)
        {
            dvblink::pugixml_helpers::new_child(settings_node, REC_SETTINGS_REC_PATH_NODE, settings.get_recording_path().get());               

            std::stringstream str_out;
            str_out << settings.get_before_margin();
            dvblink::pugixml_helpers::new_child(settings_node, REC_SETTINGS_BEFORE_MARGIN_NODE, str_out.str());

            str_out.clear(); str_out.str("");
            str_out << settings.get_after_margin();
            dvblink::pugixml_helpers::new_child(settings_node, REC_SETTINGS_AFTER_MARGIN_NODE, str_out.str());

            str_out.clear(); str_out.str("");
            str_out << settings.get_total_space_kb();
            dvblink::pugixml_helpers::new_child(settings_node, REC_SETTINGS_TOTAL_SPACE_NODE, str_out.str());

            str_out.clear(); str_out.str("");
            str_out << settings.get_avail_space_kb();
            dvblink::pugixml_helpers::new_child(settings_node, REC_SETTINGS_AVAIL_SPACE_NODE, str_out.str());

            if (settings.check_deleted_recordings())
            {
                dvblink::pugixml_helpers::new_child(settings_node, REC_SETTINGS_CHECK_DELETE_NODE, dvblink::pugixml_helpers::xmlnode_value_true);
            }

            if (!settings.use_auto_disk_space_mode())
            {
                dvblink::pugixml_helpers::new_child(settings_node, REC_SETTINGS_DS_AUTO_MODE_NODE, dvblink::pugixml_helpers::xmlnode_value_false);
            }

            str_out.clear(); str_out.str("");
            str_out << settings.get_manual_disk_space_kb();
            dvblink::pugixml_helpers::new_child(settings_node, REC_SETTINGS_DS_MAN_VALUE_NODE, str_out.str());

            if (settings.use_autodelete_algo())
            {
                dvblink::pugixml_helpers::new_child(settings_node, REC_SETTINGS_AUTO_DELETE_NODE, dvblink::pugixml_helpers::xmlnode_value_true);
            }

            str_out.clear(); str_out.str("");
            str_out << settings.get_new_only_algo();
            dvblink::pugixml_helpers::new_child(settings_node, REC_SETTINGS_NEW_ONLY_ALGO_NODE, str_out.str());

            str_out.clear(); str_out.str("");
            str_out << settings.get_new_only_default_value();
            dvblink::pugixml_helpers::new_child(settings_node, REC_SETTINGS_NEW_ONLY_DEFAULT_NODE, str_out.str());

		    dvblink::pugixml_helpers::new_child(settings_node, REC_SETTINGS_FILENAME_PATTERN_NODE, settings.get_filename_pattern());
        }

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, rd_recording_settings_t& settings)
    {
        if (NULL != node)
        {
            std::wstring str_value;
            std::string str;

            if (dvblink::pugixml_helpers::get_node_value(node, REC_SETTINGS_REC_PATH_NODE, str_value))
                settings.set_recording_path(str_value);

            if (dvblink::pugixml_helpers::get_node_value(node, REC_SETTINGS_BEFORE_MARGIN_NODE, str_value))
            {
                try
                {
                    settings.set_before_margin(boost::lexical_cast<int>(str_value));
                }
                catch (boost::bad_lexical_cast&)
                {
                    throw dvblink::runtime_error(dvblink::xml_error_parse_msg);
                }
            }

            if (dvblink::pugixml_helpers::get_node_value(node, REC_SETTINGS_AFTER_MARGIN_NODE, str_value))
            {
                try
                {
                    settings.set_after_margin(boost::lexical_cast<int>(str_value));
                }
                catch (boost::bad_lexical_cast&)
                {
                    throw dvblink::runtime_error(dvblink::xml_error_parse_msg);
                }
            }

            if (dvblink::pugixml_helpers::get_node_value(node, REC_SETTINGS_TOTAL_SPACE_NODE, str_value))
            {
                try
                {
                    settings.set_total_space_kb(boost::lexical_cast<boost::int64_t>(str_value));
                }
                catch (boost::bad_lexical_cast&)
                {
                    throw dvblink::runtime_error(dvblink::xml_error_parse_msg);
                }
            }

            if (dvblink::pugixml_helpers::get_node_value(node, REC_SETTINGS_AVAIL_SPACE_NODE, str_value))
            {
                try
                {
                    settings.set_avail_space_kb(boost::lexical_cast<boost::int64_t>(str_value));
                }
                catch (boost::bad_lexical_cast&)
                {
                    throw dvblink::runtime_error(dvblink::xml_error_parse_msg);
                }
            }

            if (dvblink::pugixml_helpers::get_node_value(node, REC_SETTINGS_NEW_ONLY_ALGO_NODE, str_value))
            {
                try
                {
                    int l = boost::lexical_cast<int>(str_value);
                    settings.set_new_only_algo((recorder_new_only_algo_type_e)l);
                }
                catch (boost::bad_lexical_cast&)
                {
                    throw dvblink::runtime_error(dvblink::xml_error_parse_msg);
                }
            }

            if (dvblink::pugixml_helpers::get_node_value(node, REC_SETTINGS_NEW_ONLY_DEFAULT_NODE, str_value))
            {
                try
                {
                    int l = boost::lexical_cast<int>(str_value);
                    settings.set_new_only_default_value((recorder_new_only_default_value_e)l);
                }
                catch (boost::bad_lexical_cast&)
                {
                    throw dvblink::runtime_error(dvblink::xml_error_parse_msg);
                }
            }
            
            if (dvblink::pugixml_helpers::get_node_value(node, REC_SETTINGS_CHECK_DELETE_NODE, str_value) && boost::iequals(str_value, dvblink::pugixml_helpers::xmlnode_value_true))
            {
                settings.set_check_deleted_recordings(true);
            }

            if (dvblink::pugixml_helpers::get_node_value(node, REC_SETTINGS_DS_AUTO_MODE_NODE, str_value) && boost::iequals(str_value, dvblink::pugixml_helpers::xmlnode_value_false))
            {
                settings.set_auto_disk_space_mode(false);
            }

            if (dvblink::pugixml_helpers::get_node_value(node, REC_SETTINGS_DS_MAN_VALUE_NODE, str_value))
            {
                try
                {
                    settings.set_manual_disk_space_kb(boost::lexical_cast<boost::int64_t>(str_value));
                }
                catch (boost::bad_lexical_cast&)
                {
                    throw dvblink::runtime_error(dvblink::xml_error_parse_msg);
                }
            }

            if (dvblink::pugixml_helpers::get_node_value(node, REC_SETTINGS_AUTO_DELETE_NODE, str_value) && boost::iequals(str_value, dvblink::pugixml_helpers::xmlnode_value_true))
            {
                settings.set_autodelete_algo(true);
            }

            if (dvblink::pugixml_helpers::get_node_value(node, REC_SETTINGS_FILENAME_PATTERN_NODE, str))
            {
				settings.set_filename_pattern(str);
			}


        }

        return node;
    }

}
