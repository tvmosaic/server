/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_COMMAND_QUEUE_H_
#define __DVBLINK_DL_COMMAND_QUEUE_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <stdio.h>
#include <string>
#include <list>
#include <boost/thread/mutex.hpp>
#include <dl_event.h>

namespace dvblink { namespace engine {

struct SDLCommandItem
{
    unsigned long id;
    void* param;
    event wait_event;
};

class command_queue
{
public:
    static const unsigned long post_command_offset = 0x00010000;

    typedef bool (*item_compare_f)(const SDLCommandItem* item, void* user_param);

public:
    command_queue();
    ~command_queue();

    bool ExecuteCommand(unsigned long cmd_id, void* param);
    bool PostCommand(unsigned long cmd_id, void* param);
    bool PeekCommand(SDLCommandItem** item);
    bool FinishCommand(SDLCommandItem** item);

    void remove_commands(item_compare_f compare_func, void* param);

protected:
    std::list<SDLCommandItem*> m_Queue;
    boost::mutex m_cs;
};

} //engine
} //dvblink

//////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_COMMAND_QUEUE_H_
