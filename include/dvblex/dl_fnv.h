/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string.h>
#include <boost/cstdint.hpp>

namespace dvblink {
namespace engine {

// default values recommended by http://isthe.com/chongo/tech/comp/fnv/
const boost::uint32_t Prime = 0x01000193; //   16777619
const boost::uint32_t Seed  = 0x811C9DC5; // 2166136261

/// hash a single byte
inline boost::uint32_t fnv1a(unsigned char oneByte, boost::uint32_t hash = Seed)
{
  return (oneByte ^ hash) * Prime;
}

/// hash a block of memory
inline boost::uint32_t fnv1a(const void* data, size_t numBytes, boost::uint32_t hash = Seed)
{
  const unsigned char* ptr = (const unsigned char*)data;
  while (numBytes--)
    hash = fnv1a(*ptr++, hash);
  return hash;
}

/// hash a C-style string
inline boost::uint32_t fnv1a(const char* text, boost::uint32_t hash = Seed)
{
  while (*text)
    hash = fnv1a((unsigned char)*text++, hash);

  return hash;
}

/// hash an std::string
inline boost::uint32_t fnv1a(const std::string& text, boost::uint32_t hash = Seed)
{
  return fnv1a(text.c_str(), text.length(), hash);
}

/*
//64 bit fnvla functions
const boost::uint64_t Prime64 = 0x100000001b3;
const boost::uint64_t Seed64 = 0xcbf29ce484222325;

inline boost::uint64_t fnv1a_64(const void* data, boost::uint64_t numBytes, boost::uint64_t hash = Seed64)
{
    const unsigned char* ptr = (const unsigned char*)data;

    for(int i = 0; i < numBytes; ++i) 
    {
        boost::uint8_t value = ptr[i];
        hash = hash ^ value;
        hash *= Prime64;
    }

    return hash;

}

inline boost::uint64_t fnv1a_64(const char* text, boost::uint64_t hash = Seed64)
{
    return fnv1a_64(text, strlen(text));
}

inline boost::uint64_t fnv1a_64(const std::string& text, boost::uint64_t hash = Seed64)
{
    return fnv1a_64(text.c_str(), text.length(), hash);
}
*/

} // namespace engine
} // namespace dvblink
