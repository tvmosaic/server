/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <sstream>
#include <vector>
#include <map>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <dl_pugixml_helper.h>
#include <dl_send_to_item.h>

namespace dvblex {

    const std::string SEND_TO_CMD_ADD_ITEM	                      = "send_to_add_item";
    const std::string SEND_TO_CMD_CANCEL_ITEM	                  = "send_to_cancel_item";
    const std::string SEND_TO_CMD_DELETE_ITEMS	                  = "send_to_delete_items";
    const std::string SEND_TO_CMD_GET_ITEMS						  = "send_to_get_items";
    const std::string SEND_TO_CMD_LOOKUP_ITEMS					  = "send_to_lookup_items";

    const std::string SEND_TO_ITEM_NODE							  = "item";
    const std::string SEND_TO_ITEM_ID_NODE						  = "item_id";
    const std::string SEND_TO_OBJECT_ID_NODE					  = "object_id";
    const std::string SEND_TO_DESCRIPTION_NODE					  = "description";
    const std::string SEND_TO_CREATION_TIME_NODE				  = "created";
    const std::string SEND_TO_TARGETID_NODE						  = "target";
    const std::string SEND_TO_STATUS_NODE				          = "status";
    const std::string SEND_TO_COMPLETED_NODE				      = "completed";
    const std::string SEND_TO_ITEM_TYPE_NODE				      = "type";


//------------------------------------------------------------------
inline void write_item(pugi::xml_node& node, const send_to_work_item& item)
{
	std::string str;
	dvblink::pugixml_helpers::new_child(node, SEND_TO_ITEM_ID_NODE, item.item_id.get());
	dvblink::pugixml_helpers::new_child(node, SEND_TO_OBJECT_ID_NODE, item.pb_object_id.get());
	dvblink::pugixml_helpers::new_child(node, SEND_TO_DESCRIPTION_NODE, item.description);

	str = boost::lexical_cast<std::string>(item.creation_time);
	dvblink::pugixml_helpers::new_child(node, SEND_TO_CREATION_TIME_NODE, str);

	dvblink::pugixml_helpers::new_child(node, SEND_TO_TARGETID_NODE, item.target_id.to_wstring());

	str = boost::lexical_cast<std::string>(item.status);
	dvblink::pugixml_helpers::new_child(node, SEND_TO_STATUS_NODE, str);

	str = boost::lexical_cast<std::string>(item.completion_time);
	dvblink::pugixml_helpers::new_child(node, SEND_TO_COMPLETED_NODE, str);
}

inline void read_item(pugi::xml_node& node, send_to_work_item& item)
{
	std::string str;

	if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_ITEM_ID_NODE, str))
		item.item_id = str;

	if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_OBJECT_ID_NODE, str))
		item.pb_object_id = str;

	dvblink::pugixml_helpers::get_node_value(node, SEND_TO_DESCRIPTION_NODE, item.description);

	if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_CREATION_TIME_NODE, str))
		item.creation_time = boost::lexical_cast<time_t>(str);

	if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_TARGETID_NODE, str))
		item.target_id = str;

	if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_STATUS_NODE, str))
		item.status = (send_to_work_item_status_e)boost::lexical_cast<unsigned long>(str);

	if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_COMPLETED_NODE, str))
		item.completion_time = boost::lexical_cast<time_t>(str);
}

//------------------------------------------------------------------

    class send_to_add_item_request
    {
    public:
        send_to_add_item_request() {}

		send_to_work_item_list_t work_items_;
    };
    
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_add_item_request& request)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_CMD_ADD_ITEM.c_str());

        if (node != NULL)
        {
            for (size_t i=0; i<request.work_items_.size(); i++)
		    {
                pugi::xml_node wi_node = dvblink::pugixml_helpers::new_child(node, SEND_TO_ITEM_NODE);
                if (wi_node != NULL)
    			    write_item(wi_node, request.work_items_[i]);
		    }
        }

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_add_item_request& request)
    {
        if (NULL != node)
        {                       
            pugi::xml_node item_node = node.first_child();
            while (NULL != item_node)                
            {
			    send_to_work_item item;
			    read_item(item_node, item);
			    request.work_items_.push_back(item);

                item_node = item_node.next_sibling();
            }
        }

        return node;
    }

/////////////////////////////////////////////

    class send_to_add_item_response
    {
    public:
        send_to_add_item_response() {}

		send_to_work_item_id_list_t item_ids_;
    };
    
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_add_item_response& response)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_CMD_ADD_ITEM.c_str());

        if (node != NULL)
        {
		    for (size_t i=0; i<response.item_ids_.size(); i++)
                dvblink::pugixml_helpers::new_child(node, SEND_TO_ITEM_ID_NODE, response.item_ids_[i].get());
        }        

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_add_item_response& response)
    {
        if (NULL != node)
        {                       
			std::string str;

            pugi::xml_node item_node = node.first_child();
            while (NULL != item_node)                
            {
                response.item_ids_.push_back(dvblink::work_item_id_t(item_node.child_value()));

                item_node = item_node.next_sibling();
            }
        }

        return node;
    }

//------------------------------------------------------------------

    class send_to_delete_item_request
    {
    public:
		send_to_delete_item_request() 
			{}

		send_to_work_item_id_list_t item_ids_;
    };
    
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_delete_item_request& request)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_CMD_DELETE_ITEMS.c_str());

        if (node != NULL)
        {
		    for (size_t i=0; i<request.item_ids_.size(); i++)
                dvblink::pugixml_helpers::new_child(node, SEND_TO_ITEM_ID_NODE, request.item_ids_[i].get());
        }

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_delete_item_request& request)
    {
        if (NULL != node)
        {                       
            pugi::xml_node item_node = node.first_child();
            while (NULL != item_node)                
            {
				request.item_ids_.push_back(dvblink::work_item_id_t(item_node.child_value()));

                item_node = item_node.next_sibling();
            }

        }

        return node;
    }

//------------------------------------------------------------------

    class send_to_cancel_item_request
    {
    public:
        send_to_cancel_item_request() {}

		dvblink::work_item_id_t work_item_id_;
    };
    
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_cancel_item_request& request)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_CMD_CANCEL_ITEM.c_str());

        if (node != NULL)
        {
            dvblink::pugixml_helpers::new_child(node, SEND_TO_ITEM_ID_NODE, request.work_item_id_.get());
        }        

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_cancel_item_request& request)
    {
        if (NULL != node)
        {                       
			std::string str;
			if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_ITEM_ID_NODE, str))
				request.work_item_id_ = str;
        }

        return node;
    }

//------------------------------------------------------------------

	enum send_to_get_items_type_e
	{
		e_stgit_all,
		e_stgit_active,
		e_stgit_completed
	};

    class send_to_get_items_request
    {
    public:
		send_to_get_items_request() :
			item_type_(e_stgit_all)
			{}

		send_to_get_items_type_e item_type_;
    };
    
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_get_items_request& request)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_CMD_GET_ITEMS.c_str());

        if (node != NULL)
        {
		    std::string str;
		    str = boost::lexical_cast<std::string>(request.item_type_);
            dvblink::pugixml_helpers::new_child(node, SEND_TO_ITEM_TYPE_NODE, str);
        }        

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_get_items_request& request)
    {
        if (NULL != node)
        {                       
			std::string str;

			if (dvblink::pugixml_helpers::get_node_value(node, SEND_TO_ITEM_TYPE_NODE, str))
				request.item_type_ = (send_to_get_items_type_e)boost::lexical_cast<unsigned long>(str);

        }
        return node;
    }

//////////////////////////////////////////////////////////////////

    class send_to_get_items_response
    {
    public:
		send_to_get_items_response()
			{}

		send_to_work_item_list_t work_items_;
    };
    
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_get_items_response& response)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_CMD_GET_ITEMS.c_str());

        if (node != NULL)
        {
		    for (size_t i=0; i<response.work_items_.size(); i++)
		    {
			    const send_to_work_item* work_item = &response.work_items_[i];

                pugi::xml_node item_node = dvblink::pugixml_helpers::new_child(node, SEND_TO_ITEM_NODE);
                if (item_node != NULL)
    			    write_item(item_node, *work_item);
		    }
        }

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_get_items_response& response)
    {
        if (NULL != node)
        {                       
            pugi::xml_node item_node = node.first_child();
            while (NULL != item_node)                
            {
				send_to_work_item item;
				read_item(item_node, item);
				response.work_items_.push_back(item);

                item_node = item_node.next_sibling();
            }

        }

        return node;
    }

//------------------------------------------------------------------

    class send_to_lookup_items_request
    {
    public:
		send_to_lookup_items_request() 
			{}

		send_to_object_id_list_t object_ids_;
    };
    
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_lookup_items_request& request)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_CMD_LOOKUP_ITEMS.c_str());

        if (node != NULL)
        {
		    for (size_t i=0; i<request.object_ids_.size(); i++)
		    {
                dvblink::pugixml_helpers::new_child(node, SEND_TO_OBJECT_ID_NODE, request.object_ids_[i].get());
		    }
        }        

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_lookup_items_request& request)
    {
        if (NULL != node)
        {                       
            pugi::xml_node item_node = node.first_child();
            while (NULL != item_node)                
            {
				request.object_ids_.push_back(dvblink::object_id_t(item_node.child_value()));

                item_node = item_node.next_sibling();
            }

        }

        return node;
    }

////////////////////////////////////////////////////////

    class send_to_lookup_items_response
    {
    public:
		send_to_lookup_items_response()
			{}

		send_to_work_item_list_t work_items_;
    };
    
    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const send_to_lookup_items_response& response)
    {
        pugi::xml_node node = doc.append_child(SEND_TO_CMD_LOOKUP_ITEMS.c_str());

        if (node != NULL)
        {
		    for (size_t i=0; i<response.work_items_.size(); i++)
		    {
			    const send_to_work_item* work_item = &response.work_items_[i];

                pugi::xml_node item_node = dvblink::pugixml_helpers::new_child(node, SEND_TO_ITEM_NODE);
                if (item_node != NULL)
    			    write_item(item_node, *work_item);
		    }
        }        

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, send_to_lookup_items_response& response)
    {
        if (NULL != node)
        {                       
            pugi::xml_node item_node = node.first_child();
            while (NULL != item_node)                
            {
				send_to_work_item item;
				read_item(item_node, item);
				response.work_items_.push_back(item);

                item_node = item_node.next_sibling();
            }
        }

        return node;
    }
}
