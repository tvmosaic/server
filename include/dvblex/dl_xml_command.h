/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <sstream>
#include <vector>
#include <dl_types.h>
#include <dl_pugixml_helper.h>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/predicate.hpp>

namespace dvblex {

const std::string xmlcmd_cmd_root_node                    = "xml_cmd";
const std::string xmlcmd_response_root_node               = "xml_response";
const std::string xmlcmd_cmd_node                         = "cmd";
const std::string xmlcmd_result_node                      = "result";
const std::string xmlcmd_param_node                       = "param";
const std::string xml_cmd_addressee_node                  = "addressee";


    class xml_command_base
    {
        public:
            xml_command_base(){}
            
            xml_command_base(const dvblink::message_addressee_t& addressee, const dvblink::xml_cmd_command_t& cmd_id, const dvblink::xml_string_t& in_params) :
                addressee_(addressee), cmd_id_(cmd_id), in_params_(in_params) {}

            const dvblink::xml_cmd_command_t& get_command_id() const { return cmd_id_; }
            void set_command_id(const dvblink::xml_cmd_command_t& id) { cmd_id_ = id; }

            const dvblink::xml_string_t& get_in_params() const { return in_params_; }
            void set_in_params(const dvblink::xml_string_t& params) { in_params_ = params; }
            
            const dvblink::message_addressee_t& get_addressee() const { return addressee_; }
            void set_addressee(const dvblink::message_addressee_t& addressee) { addressee_ = addressee; }
            
        private:
            dvblink::message_addressee_t addressee_;
            dvblink::xml_cmd_command_t cmd_id_;
            dvblink::xml_string_t in_params_;
    };

    class xml_response_base
    {
        public:
            xml_response_base(){}
            xml_response_base(const dvblink::xml_cmd_result_t& result, const dvblink::xml_string_t& out_params) :
                result_(result), out_params_(out_params) {}

            const dvblink::xml_cmd_result_t& get_command_result() const { return result_; }
            void set_command_result(const dvblink::xml_cmd_result_t& res) { result_ = res; }

            const dvblink::xml_string_t& get_out_params() const { return out_params_; }
            void set_out_params(const dvblink::xml_string_t& params) { out_params_ = params; }
            
        private:
            dvblink::xml_cmd_result_t result_;
            dvblink::xml_string_t out_params_;
    };

    inline pugi::xml_node& operator>> (pugi::xml_node& node, xml_command_base& xml_cmd)
    {
        if (NULL != node)
        {
            std::string str_value;

            if (dvblink::pugixml_helpers::get_node_value(node, xmlcmd_cmd_node, str_value))
            {
                xml_cmd.set_command_id(str_value);
            }

            if (dvblink::pugixml_helpers::get_node_value(node, xmlcmd_param_node, str_value))
            {
                xml_cmd.set_in_params(str_value);
            }

            if (dvblink::pugixml_helpers::get_node_value(node, xml_cmd_addressee_node, str_value))
            {
                xml_cmd.set_addressee(str_value);
            }
        }

        return node;
    }

    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const xml_command_base& xml_cmd)
    {
        pugi::xml_node cmd_node = doc.append_child(xmlcmd_cmd_root_node.c_str());

        if (cmd_node != NULL)
        {
            dvblink::pugixml_helpers::new_child(cmd_node, xmlcmd_cmd_node, xml_cmd.get_command_id().get());
            dvblink::pugixml_helpers::new_child(cmd_node, xmlcmd_param_node, xml_cmd.get_in_params().get());
            dvblink::pugixml_helpers::new_child(cmd_node, xml_cmd_addressee_node, xml_cmd.get_addressee().get());
        }

        return doc;
    }

////////////////////////////////////////////////////////////

    inline pugi::xml_node& operator>> (pugi::xml_node& node, xml_response_base& xml_response)
    {
        if (NULL != node)
        {
            std::string str_value;

            if (dvblink::pugixml_helpers::get_node_value(node, xmlcmd_result_node, str_value))
            {
                xml_response.set_command_result(str_value);
            }

            if (dvblink::pugixml_helpers::get_node_value(node, xmlcmd_param_node, str_value))
            {
                xml_response.set_out_params(str_value);
            }

        }

        return node;
    }

    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const xml_response_base& xml_response)
    {
        pugi::xml_node resp_node = doc.append_child(xmlcmd_response_root_node.c_str());

        if (resp_node != NULL)
        {
            dvblink::pugixml_helpers::new_child(resp_node, xmlcmd_result_node, xml_response.get_command_result().get());
            dvblink::pugixml_helpers::new_child(resp_node, xmlcmd_param_node, xml_response.get_out_params().get());
        }

        return doc;
    }

}
