/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <sstream>
#include <dl_strings.h>
#include <dl_types.h>
#include <dl_logger.h>
#include <dl_parameters_serializer.h>
#include <dl_pugixml_helper.h>

namespace dvblex { 

const std::string epg_source_refresh_cmd              = "epg_source_refresh";
const std::string epg_source_get_settings_cmd         = "epg_source_get_settings";
const std::string epg_source_set_settings_cmd         = "epg_source_set_settings";

////////////////////////////////////////////////////////////////////

struct get_epg_source_settings_response_t
{
    dvblex::parameters_container_t settings_;
};


const std::string epg_source_settings_request_root_node            = "epg_source_settings";
const std::string epg_source_settings_settings_node                = "settings";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_epg_source_settings_response_t& request)
{
    if (NULL != node)
    {
        pugi::xml_node settings_node = node.child(params_container_node.c_str());
        read_from_node(settings_node, request.settings_);
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_epg_source_settings_response_t& request)
{
    pugi::xml_node node = doc.append_child(epg_source_settings_request_root_node.c_str());

    if (node != NULL)
    {
        write_to_node(request.settings_, node);
    }

    return doc;
}

////////////////////////////////////////////////////////////////////

struct set_epg_source_settings_request_t
{
    dvblex::concise_param_map_t settings_;
};

inline pugi::xml_node& operator>> (pugi::xml_node& node, set_epg_source_settings_request_t& request)
{
    if (NULL != node)
    {
        std::string str;

        pugi::xml_node settings_node = node.child(epg_source_settings_settings_node.c_str());
        read_from_node(settings_node, request.settings_);
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const set_epg_source_settings_request_t& request)
{
    pugi::xml_node node = doc.append_child(epg_source_settings_request_root_node.c_str());

    if (node != NULL)
    {
        pugi::xml_node settings_node = dvblink::pugixml_helpers::new_child(node, epg_source_settings_settings_node);
        write_to_node(request.settings_, settings_node);
    }

    return doc;
}

//enable/disable periodic epg updates
struct enable_epg_updates_request_t
{
    enable_epg_updates_request_t() :
        enable_(false)
    {
    }

    bool enable_;
};

const std::string enable_epg_updates_root_node         = "enable_epg_updates";
const std::string enable_epg_updates_flag_node         = "enabled";

inline pugi::xml_node& operator>> (pugi::xml_node& node, enable_epg_updates_request_t& request)
{
    if (NULL != node)
    {
        std::string str;
        request.enable_ = dvblink::pugixml_helpers::get_node_value(node, enable_epg_updates_flag_node, str) &&
            boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const enable_epg_updates_request_t& request)
{
    pugi::xml_node node = doc.append_child(enable_epg_updates_root_node.c_str());

    if (node != NULL)
    {
        if (request.enable_)
            dvblink::pugixml_helpers::new_child(node, enable_epg_updates_flag_node, dvblink::pugixml_helpers::xmlnode_value_true);
    }

    return doc;
}

} //dvblex
