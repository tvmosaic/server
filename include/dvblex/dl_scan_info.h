/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <dl_types.h>
#include <dl_channel_info.h>
#include <dl_parameters.h>

namespace dvblex { 

enum channel_start_scan_result_e
{
    cssr_error,
    cssr_scan_started,
    cssr_scan_joined
};

enum channel_scanner_state_e
{
    css_idle,
    css_scanning_networks,
    css_networks_ready,
    css_scanning_channels,
    css_ready
};

//scan progress 
struct channel_scanner_progress_t
{
	channel_scanner_progress_t()
	{
		reset();
	}

	void reset()
	{
		state_ = css_idle;
		progress_ = 0;
		channels_found_ = 0;
	}

    void from_channel_scanner_progress(const channel_scanner_progress_t& csp)
    {
		state_ = csp.state_;
		progress_ = csp.progress_;
		channels_found_ = csp.channels_found_;
    }

    channel_scanner_state_e state_;
    boost::uint32_t progress_;
    boost::uint32_t channels_found_;
};

struct scan_log_entry_t
{
    scan_log_entry_t() :
        channels_found_(0)
    {
    }

    void reset()
    {
        channels_found_ = 0;
        scan_data_.clear();
        comment_.clear();
        signal_info_.reset();
    }

    void add_comment(const std::string& comment)
    {
        std::string str = comment_.get();

        if (!str.empty())
            str += ", ";

        str += comment;

        comment_ = str;
    }

    dvblink::scan_element_t scan_data_;
    boost::uint32_t channels_found_;
    dvblink::scan_comment_t comment_;
    signal_info_t signal_info_;
};

typedef std::vector<scan_log_entry_t> scan_log_t;

struct scanned_network_t
{
    dvblink::scan_network_id_t network_id_;
    dvblink::scan_network_name_t name_;
};

typedef std::vector<scanned_network_t> scanned_network_list_t;

}
