/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_epgevent.h>
#include <dl_recommender_types.h>
#include <dl_common.h>

namespace dvblex { namespace tvadviser {

const std::string create_profile_cmd        = "create_profile";
const std::string get_profiles_cmd          = "get_profiles";
const std::string delete_profile_cmd        = "delete_profile";
const std::string change_profile_name_cmd   = "rename_profile";
const std::string get_providers_cmd         = "get_providers";
const std::string get_provider_sources_cmd  = "get_provider_channels";
const std::string get_mapped_sources_cmd    = "get_mapped_channels";
//const std::string set_mapped_sources_cmd    = "set_mapped_channels";
//const std::string get_epg_cmd               = "get_epg";
const std::string rate_program_cmd          = "rate_program";
const std::string recommend_by_profiles_cmd = "recommend_by_profiles";
const std::string recommend_like_this_cmd   = "recommend_like_this";
const std::string get_asset_id_cmd          = "get_asset_id";
const std::string find_programs_cmd         = "find_programs";
const std::string get_playlists_content_cmd = "get_playlists_content";
const std::string add_program_into_playlist_cmd = "add_program_into_playlist";
const std::string remove_program_from_playlist_cmd = "remove_program_from_playlist";
const std::string is_subscriber_the_same_cmd = "is_subscriber_the_same";

const std::string success                   = dvblink::xmlcmd_result_success;
const std::string fail                      = dvblink::xmlcmd_result_error;

struct program_info;
typedef boost::shared_ptr<program_info> program_info_t;

struct program_info
{
    program_info() :
        start_(0),
        end_(0)
    {}
        
        program_info(const dvblink::ncanto::asset_id_t& id, const dvblink::ncanto::title_name_t& name, time_t start, time_t end) :
        id_(id),
        name_(name),
        start_(start),
        end_(end)
    {}

    dvblink::ncanto::asset_id_t id_;
    dvblink::ncanto::title_name_t name_;
    time_t start_;
    time_t end_;
};

typedef std::vector<program_info_t> programs_t;
typedef std::map<dvblink::ncanto::source_id_t, programs_t> map_sources_to_programs_t;

struct mixed_program_info
{
    mixed_program_info() :
        start_(0),
        end_(0),
        rating_(0),
        score_(0),
        is_series_(true)
    {}
        
    mixed_program_info(/*const dvblink::ncanto::asset_id_t& ncanto_id, */time_t start,
            time_t end, const dvblink::ncanto::rating_t& rating, const dvblink::ncanto::score_t& score/*, const dvblink::ncanto::title_name_t& title*/) :
        //ncanto_id_(ncanto_id),
        start_(start),
        end_(end),
        rating_(rating),
        score_(score),
        //title_(title),
        is_series_(true)
    {}

    //dvblink::ncanto::asset_id_t ncanto_id_;
    time_t start_;
    time_t end_;
    dvblink::ncanto::rating_t rating_;
    dvblink::ncanto::score_t score_;
    //dvblink::ncanto::title_name_t title_;
    dvblink::engine::DLEPGEvent event_;
    bool is_series_;
};

typedef std::vector<mixed_program_info> mixed_program_info_t;
typedef std::map<dvblink::channel_id_t, mixed_program_info_t> map_channel_programs_t;

struct profile_data
{
    profile_data() {}
    profile_data(const dvblink::ncanto::profile_id_t& id, const dvblink::ncanto::profile_name_t& name) :
        id_(id),
        name_(name)
    {}
    dvblink::ncanto::profile_id_t id_;
    dvblink::ncanto::profile_name_t name_;
};
typedef std::vector<profile_data> vec_profile_data_t;
typedef std::map<dvblink::ncanto::provider_id_t, vec_profile_data_t> map_provider_profiles_t;

} //tvadviser
} //dvblex

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::tvadviser::program_info& p, const unsigned int /*version*/)
{
    ar & p.id_;
    ar & p.name_;
    ar & p.start_;
    ar & p.end_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::tvadviser::mixed_program_info& p, const unsigned int /*version*/)
{
    //ar & p.ncanto_id_;
    ar & p.start_;
    ar & p.end_;
    ar & p.rating_;
    ar & p.score_;
    //ar & p.title_;
    ar & p.event_;
    ar & p.is_series_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::tvadviser::profile_data& p, const unsigned int /*version*/)
{
    ar & p.id_;
    ar & p.name_;
}

} // namespace serialization
} // namespace boost
