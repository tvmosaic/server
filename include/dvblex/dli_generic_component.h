/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/shared_ptr.hpp>
#include <dl_types.h>
#include <dli_base.h>
#include <dli_server.h>

namespace dvblink { namespace messaging {

#pragma pack(push, 1)

// {98458DEF-7C96-49a3-AB63-4DD731472511}
const i_guid generic_component_interface = 
{ 0x98458def, 0x7c96, 0x49a3, { 0xab, 0x63, 0x4d, 0xd7, 0x31, 0x47, 0x25, 0x11 } };

struct i_generic_component : public i_base_object
{
    virtual bool __stdcall init(const i_server_t& server) = 0;
};

typedef boost::shared_ptr<i_generic_component> i_generic_component_t;

#pragma pack(pop)

} //auxes
} //dvblink
