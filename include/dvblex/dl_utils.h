/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_UTILS_H_
#define __DVBLINK_DL_UTILS_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#ifdef _WIN32
#pragma warning(push,3)
#pragma comment(lib, "winmm")
#endif

#include <time.h>
#include <string>
#include <vector>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>
#include <dl_common.h>
#include <dl_platforms.h>
#include <dl_types.h>
#include <dl_errors.h>
#include <dl_filesystem_path.h>

#ifdef _WIN32
#include <objbase.h>
#else
#include <unistd.h>
#include <sys/types.h>
#include <dl_iconv_helper.h>
#include <boost/thread.hpp>
#endif

#ifdef _WIN32
#pragma warning(pop)
#endif

namespace dvblink {

typedef boost::posix_time::time_duration timeout_t;
const timeout_t infinite_timeout = boost::date_time::pos_infin;
const timeout_t zero_timeout = boost::posix_time::milliseconds(0);

const unsigned long MILLI = 1000;
const unsigned long MICRO = 1000000;
const unsigned long NANO  = 1000000000;

void sleep(timeout_t to);

namespace engine {

boost::posix_time::ptime from_time_t(std::time_t t);
std::time_t to_time_t(boost::posix_time::ptime t);


#ifdef _WIN32
inline void sleep(int milli_seconds)
{
    Sleep(milli_seconds);
}
#else
inline void sleep(int milli_seconds)
{
    boost::this_thread::sleep(boost::posix_time::milliseconds(milli_seconds));
}
#endif

#ifdef _WIN32
std::wstring format_error_message(unsigned long error);
#else
dvblink::errcode_t daemonize(pid_t& child_pid);
#endif

#define EXECUTE_SCRIPT_INFINITE_WAIT	0xFFFFFFFF

bool execute_script(const dvblink::filesystem_path_t& script_path,
    const dvblink::filesystem_path_t* current_directory = NULL, unsigned long wait_timeout = EXECUTE_SCRIPT_INFINITE_WAIT);

void SetDefaultDVBCodepage(EDVBDefaultCodepage cp);
void ConvertAnnexATextToUnicode(unsigned char* annexa_text, int text_len, std::wstring& out_str);
unsigned char* RemoveControlChFromAnnexAText(unsigned char* annexa_text, int length, int& new_length);
bool ProcessControlChInAnnexAText(unsigned char* annexa_text, int length, unsigned char& enc, std::vector<std::string>& resText);

void GetNumberFromBCD(unsigned char* bcd_num, int chnum, unsigned long& result);

void GetISO_639_3_Synonyms(const char* iso_639_3_code, std::vector<std::string>& synonyms);

void substitute_invalid_file_chars(std::wstring& str);

#ifdef _WIN32

class CDLCoInitHolder
{
public:
    CDLCoInitHolder(unsigned long options)
    {
        HRESULT hr = CoInitializeEx(NULL, options);
        bInitialized = (hr == S_OK) || (hr == S_FALSE);
    };
    ~CDLCoInitHolder(){if (bInitialized) CoUninitialize();}

protected:
    bool bInitialized;
};

#endif //WIN32

bool get_app_locations(dvblink::filesystem_path_t& full_path, dvblink::filesystem_path_t& working_dir);

} //engine
} //dvblink

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_UTILS_H_
