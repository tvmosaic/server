/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <dl_common.h>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <dl_utils.h>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_epgevent.h>
#include <dl_epg_channels.h>
#include <dl_file_procedures.h>
#include <dl_network_helper.h>
#include <dl_filesystem_path.h>
#include <dl_http_comm.curl.h>
#include <dl_xmltv_processor_pugi.h>
#include <dl_zip.h>
#include <dl_tar.h>
#include <dl_gzip.h>
#include <dl_xz.h>

namespace dvblink { namespace engine {

class xmltv_file_processor
{
protected:
    enum e_xmltv_download_item_type
    {
        exdit_unknown = 0,
        exdit_xml,
        exdit_zip,
        exdit_gz,
        exdit_tar,
        exdit_targz,
        exdit_tarbz2,
        exdit_xz
    };

public:
    xmltv_file_processor(const xmltv_category_keywords_t& keywords_map, dvblex::epg_source_channel_map_t& epg_channels_map, map_epg_channel_to_epg_t& epg_events_map)
        : keywords_map_(keywords_map), epg_channels_map_(epg_channels_map), epg_events_map_(epg_events_map)
    {}
    ~xmltv_file_processor(){}

    void reset()
    {
        epg_channels_map_.clear();
        epg_events_map_.clear();
    }

    bool read_xmltv_file(const dvblink::filesystem_path_t& xmltv_file_path, xmltv_channel_info_map_t* chinfo_map, bool& read_exit_flag)
    {
        bool ret_val = false;

	    EDL_NET_PROTOCOLS proto = network_helper::get_proto(xmltv_file_path.to_string().c_str());
	    if (proto == DL_NET_PROTO_UNKNOWN)
	    {
    	    logging::log_info(L"xmltv_file_processor::read_xmltv_file. Processing file %1%") % xmltv_file_path.to_wstring();

            pugi::xml_document doc;
            pugi::xml_parse_result res = doc.load_file(xmltv_file_path.to_string().c_str());
            if (res.status == pugi::status_ok)
            {
                process_xmltv_doc(doc, keywords_map_, chinfo_map, epg_channels_map_, epg_events_map_, &read_exit_flag);
                ret_val = true;
            }
        }
	    return ret_val;
    }

    bool read_directory(const dvblink::filesystem_path_t& input_dir, xmltv_channel_info_map_t* chinfo_map, bool& read_exit_flag, const wchar_t* extension = L".xml")
    {
	    //enumerate all .xml files in xmltv directory
        logging::log_info(L"xmltv_file_processor::read_directory. Enumerating all .xml files in directory %1%") % input_dir.to_wstring();

	    std::vector<boost::filesystem::path> file_list;
        dvblink::engine::filesystem::find_files(input_dir.to_boost_filesystem(), file_list, extension);

	    for (unsigned int i = 0; i < file_list.size(); i++)
	    {
	        filesystem_path_t temp_path(input_dir);
		    temp_path /= file_list[i].filename();
		    logging::log_info(L"xmltv_file_processor::read_directory. Processing file %1%") % temp_path.to_wstring();

            pugi::xml_document doc;
            pugi::xml_parse_result res = doc.load_file(temp_path.to_string().c_str());
            if (res.status == pugi::status_ok)
                process_xmltv_doc(doc, keywords_map_, chinfo_map, epg_channels_map_, epg_events_map_, &read_exit_flag);
	    }
	    return true;
    }

    bool process_internet_file(const dvblink::url_address_t& xmltv_url, const dvblink::filesystem_path_t& temp_dir, xmltv_channel_info_map_t* chinfo_map, bool& read_exit_flag)
    {
        e_xmltv_download_item_type file_type = exdit_unknown;
        dvblink::filesystem_path_t downloaded_file;
        if (download_file(xmltv_url, temp_dir, downloaded_file, file_type, read_exit_flag))
        {
            //temporary directory for xmltv files
            boost::uuids::uuid new_guid = boost::uuids::random_generator()();
            dvblink::filesystem_path_t xmltv_dir = temp_dir / boost::lexical_cast<std::string>(new_guid);

            try {
                boost::filesystem::create_directories(xmltv_dir.to_boost_filesystem());
            } catch(...) {}

            if (convert_downloaded_file_to_xml(downloaded_file, xmltv_dir, file_type))
            {
                //Read EPG information from xmltv files
                read_directory(xmltv_dir, chinfo_map, read_exit_flag, L"");
            }
        }

        return true;
    }

protected:
    xmltv_category_keywords_t keywords_map_;
    dvblex::epg_source_channel_map_t& epg_channels_map_;
    map_epg_channel_to_epg_t& epg_events_map_;

    bool is_gzip_compressed(const std::string& header)
    {
        bool res = false;
        boost::smatch result;
        static boost::regex http_regexp(std::string("(?i)\\s*+content-encoding:\\s*+(gzip)"));
        if (boost::regex_search(header, result, http_regexp))
        {
            res = boost::iequals(result[1].str(), "gzip");
        }
        return res;
    }


    bool download_file(const dvblink::url_address_t& url, const dvblink::filesystem_path_t& dir, dvblink::filesystem_path_t& downloaded_file, 
        e_xmltv_download_item_type& file_type, bool& read_exit_flag)
    {
        bool ret_val = false;
        file_type = exdit_unknown;

        std::string address;
        std::string user;
        std::string pswd;
	    unsigned short port;
	    std::string url_suffix;

        logging::log_info(L"xmltv_file_processor::download_file. Processing %1%") % string_cast<EC_UTF8>(url.get());
	    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(url.get().c_str(), address, user, pswd, port, url_suffix);
	    if (proto != DL_NET_PROTO_UNKNOWN)
	    {
            http_comm_handler http_comm(PRODUCT_NAME_UTF8, address.c_str(),
                user.c_str(), pswd.c_str(), port);

            if (http_comm.Init())
            {
                if (user.length() > 0)
                    http_comm.SetHttpAuth(http_auth_basic); //only basic authentication is supported

                http_comm_handler::http_headers_t headers;
                headers.push_back("Accept-Encoding: gzip");

                void* handle = http_comm.SendGetRequest(url_suffix.c_str(), proto == DL_NET_PROTO_HTTPS, &headers);
	            if (handle != NULL)
	            {
                    bool success = true;
                    bool b_headers_processed = false;

                    boost::uuids::uuid new_guid = boost::uuids::random_generator()();
                    downloaded_file = dir / boost::lexical_cast<std::string>(new_guid);
                    
                    //create output file
#ifdef WIN32
                    std::ofstream fout(downloaded_file.to_wstring().c_str(), std::ios_base::out | std::ios_base::binary);
#else
                    std::ofstream fout(downloaded_file.to_string().c_str(), std::ios_base::out | std::ios_base::binary);
#endif
                    boost::iostreams::filtering_ostream fos;

                    unsigned long allocated_buffer_size = 256*1024;
                    unsigned char* buffer = new unsigned char[allocated_buffer_size];
                    unsigned long read_size = 0;
                    unsigned long buf_size = allocated_buffer_size;
                    while (http_comm.ReadRequestResponse(handle, buffer, buf_size, &read_exit_flag) && !read_exit_flag)
                    {
                        if (!b_headers_processed)
                        {
                            http_comm_handler::http_headers_t headers;
                            http_comm.ReadResponseHeaders(handle, &headers);

                            //determine content type from headers
                            get_type_from_http_headers(headers, file_type);

                            //if not successful - use extension
                            if (file_type == exdit_unknown)
                                get_type_from_extension(url_suffix, file_type);

                            //check content encoding
                            for (size_t hdr_idx = 0; hdr_idx<headers.size(); hdr_idx++)
                            {
                                if (is_gzip_compressed(headers[hdr_idx]))
                                {
                                    fos.push(boost::iostreams::gzip_decompressor());
                                    break;
                                }
                            }
                            fos.push(fout);

                            b_headers_processed = true;
                        }

                        if (buf_size > 0)
                        {
                            try {
                                success = fos.write((const char*)buffer, buf_size).good();
                            } catch (...) 
                            {
                                success = false;
                            }
                        } else
                        {
                            break;
                        }

                        read_size += buf_size;

                        //break on error during writing
                        if (!success)
                            break;

                        buf_size = allocated_buffer_size;
                    }
                    delete buffer;

                    ret_val = !read_exit_flag && success && read_size > 0;
                    http_comm.CloseRequest(handle);
	            } else
	            {
                    logging::log_error(L"xmltv_file_processor::download_file. SendGetRequest failed");
	            }
		        http_comm.Term();
	        } else
            {
                logging::log_error(L"xmltv_file_processor::download_file. http_comm_handler init failed");
            }
        } else
        {
            logging::log_error(L"xmltv_file_processor::download_file. cannot parse download url");
        }
        return ret_val;
    }


    bool convert_downloaded_file_to_xml(const dvblink::filesystem_path_t& downloaded_file, const dvblink::filesystem_path_t& output_dir, e_xmltv_download_item_type file_type)
    {
        bool ret_val = false;

        logging::log_info(L"xmltv_file_processor::convert_downloaded_file_to_xml. Detected file type is %1%") % file_type;

        //if fyle type is unknown - assume that it is xml
        if (file_type == exdit_unknown)
            file_type = exdit_xml;

        switch (file_type)
        {
        case exdit_xml:
            {
                boost::filesystem::path dest = output_dir.to_boost_filesystem() / downloaded_file.to_boost_filesystem().filename();
                //copy file to the destination folder
                ret_val = true;
                try {
                    boost::filesystem::copy_file(downloaded_file.to_boost_filesystem(), dest, boost::filesystem::copy_option::overwrite_if_exists);
                } catch(...)
                {
                    ret_val = false;
                }

                //delete original file
                try {
                    boost::filesystem::remove(downloaded_file.to_boost_filesystem());
                } catch(...) {}

            }
            break;
        case exdit_zip:
            {
                zip::zipfile_reader zipfile_reader(downloaded_file);
                ret_val = zipfile_reader.decompress(output_dir);
            }
            break;
        case exdit_gz:
            {
                dvblink::filesystem_path_t output_file;
                gzip::gzipfile_reader gzipfile_reader(downloaded_file);
                ret_val = gzipfile_reader.decompress(output_dir, output_file);
            }
            break;
        case exdit_xz:
            {
                dvblink::filesystem_path_t output_file;
                xz::xzfile_reader xzfile_reader(downloaded_file);
                ret_val = xzfile_reader.decompress(output_dir, output_file);
            }
            break;
        case exdit_tar:
        case exdit_targz:
        case exdit_tarbz2:
            {
                tar::tar_file_type_e t = tar::tft_auto;
                if (file_type == exdit_tar)
                    t = tar::tft_tar;
                else if (file_type == exdit_targz)
                    t = tar::tft_tar_gz;
                else if (file_type == exdit_tarbz2)
                    t = tar::tft_tar_bz2;

                tar::tarfile_reader tfr(downloaded_file, t);
                ret_val = tfr.decompress(output_dir);
            }
            break;
        default:
            logging::log_error(L"xmltv_file_processor::convert_downloaded_file_to_xml. Unsupported file type (%1%)") % file_type;
            break;
        }

        return ret_val;
    }

    bool get_type_from_http_headers(dvblink::http_comm_handler::http_headers_t& headers, e_xmltv_download_item_type& file_type)
    {
        file_type = exdit_unknown;

        std::string contents;
        if (get_header(headers, "Content-Disposition", contents))
        {
            std::string token = "filename=";
            size_t n = contents.find(token);
            if (n != std::string::npos)
            {
                //everything between token and the end of the string is a filename
                std::string fname = contents.substr(n + token.length(), contents.length() - (n + token.length()));
                //remove start/end quotes
                boost::algorithm::trim_if(fname, boost::algorithm::is_any_of("\""));

                get_type_from_extension(fname, file_type);
            }
        }

        if (file_type == exdit_unknown)
        {
            std::string contents;
            if (get_header(headers, "Content-Type", contents))
            {
                if (boost::icontains(contents, "application/xml") || boost::icontains(contents, "text/xml"))
                    file_type = exdit_xml;
                else if (boost::icontains(contents, "application/zip"))
                    file_type = exdit_zip;
                else if (boost::icontains(contents, "application/x-bzip"))
                    file_type = exdit_tarbz2;
                else if (boost::icontains(contents, "application/x-tar"))
                    file_type = exdit_tar;
                else if (boost::icontains(contents, "application/x-gzip"))
                    file_type = exdit_gz;
            }
        }

        return file_type != exdit_unknown;
    }

    bool get_type_from_extension(const std::string& extension, e_xmltv_download_item_type& file_type)
    {
        file_type = exdit_unknown;

        if (boost::iends_with(extension, ".xml"))
            file_type = exdit_xml;
        else if (boost::iends_with(extension, ".zip"))
            file_type = exdit_zip;
        else if (boost::iends_with(extension, ".tar.gz"))
            file_type = exdit_targz;
        else if (boost::iends_with(extension, ".tar.bz2"))
            file_type = exdit_tarbz2;
        else if (boost::iends_with(extension, ".tar"))
            file_type = exdit_tar;
        else if (boost::iends_with(extension, ".gz"))
            file_type = exdit_gz;
        else if (boost::iends_with(extension, ".xz"))
            file_type = exdit_xz;

        return file_type != exdit_unknown;
    }

bool get_header(const http_comm_handler::http_headers_t& headers, const char* header, std::string& contents)
{
    bool ret_val = false;

    for (size_t i=0; i<headers.size(); i++)
    {
        size_t n = headers[i].find(":");
        if (n != std::string::npos)
        {
            std::string h = headers[i].substr(0, n);
            std::string v = headers[i].substr(n+1, headers[i].length() - n);
            boost::trim(h);
            boost::trim(v);
            if (boost::iequals(h, header))
            {
                contents = v;
                ret_val = true;
                break;
            }
        }
    }

    return ret_val;
}

};

} //engine
} //dvblink

