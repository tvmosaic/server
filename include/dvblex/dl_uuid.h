/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_UUID_H_
#define __DVBLINK_DL_UUID_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <iostream>
#include <sstream>
#include <iomanip>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

namespace dvblink { namespace engine {

class uuid
{
public:
    uuid()
    {
        gen_uuid(uuid_);
    }

    static boost::uuids::uuid gen_uuid()
    {
        boost::uuids::random_generator gen;
        return gen();
    }

    static void gen_uuid(std::wstring& str_uuid)
    {
        boost::uuids::uuid guid = gen_uuid();
        str_uuid = boost::uuids::to_wstring(guid);
    }

    static void gen_uuid(std::string& str_uuid)
    {
        boost::uuids::uuid guid = gen_uuid();
        str_uuid = boost::uuids::to_string(guid);
    }

    static boost::uuids::uuid from_uint64(boost::uint64_t u1, boost::uint64_t u2)
    {
        std::stringstream ss;
        ss << std::setw(16) << std::setfill('0') << u1 << std::setw(16) << std::setfill('0') << u2;
        boost::uuids::string_generator gen;
        return gen(ss.str());
    }

public:
    std::wstring uuid_;
};

template<typename STRING>
boost::uuids::uuid string_cast(const STRING& arg)
{
    std::basic_stringstream<typename STRING::value_type,
        std::char_traits<typename STRING::value_type>,
        std::allocator<typename STRING::value_type> > stream_uuid;
    stream_uuid << arg;
    boost::uuids::uuid uid;
    stream_uuid >> uid;
    return uid;
}

template<typename STRING>
STRING string_cast(const boost::uuids::uuid& arg);

template<>
inline std::wstring string_cast<std::wstring>(const boost::uuids::uuid& arg)
{
    return boost::uuids::to_wstring(arg);
}

template<>
inline std::string string_cast<std::string>(const boost::uuids::uuid& arg)
{
    return boost::uuids::to_string(arg);
}

} //engine
} //dvblink

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_UUID_H_
