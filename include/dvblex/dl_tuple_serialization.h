/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_TUPLE_SERIALIZATION_H_
#define __DVBLINK_DL_TUPLE_SERIALIZATION_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable:4512)
#pragma warning(disable:4099)
#endif
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/type_traits.hpp>
#include <boost/tuple/tuple.hpp>
#ifdef _WIN32
#pragma warning(pop)
#endif


namespace boost { namespace serialization {

template<class Archive, BOOST_PP_ENUM_PARAMS(10, class T)>
inline void serialize(Archive& ar, boost::tuples::tuple<BOOST_PP_ENUM_PARAMS(10, T)>& p, const unsigned int /* file_version */)
{
    typedef boost::tuples::tuple<BOOST_PP_ENUM_PARAMS(10, T)> tuple_t;
    ar & static_cast<boost::tuples::cons<typename tuple_t::head_type, typename tuple_t::tail_type>&>(p);
}

template<class Archive>
inline void serialize(Archive& /*ar*/, boost::tuples::tuple<>& /*p*/, const unsigned int /* file_version */)
{
}

template<class Archive, class Head, class Tail>
inline void serialize(Archive& ar, boost::tuples::cons<Head, Tail>& p, const unsigned int /* file_version */)
{
    ar & p.head & p.tail;
}

template<class Archive, class Head>
inline void serialize(Archive& ar, boost::tuples::cons<Head, boost::tuples::null_type>& p, const unsigned int /* file_version */)
{
    ar & p.head;
}

} //serialization
} //boost

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_TUPLE_SERIALIZATION_H_
