/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_CONFIG_STORAGE_STORAGE_PATH_H_
#define __DVBLINK_CONFIG_STORAGE_STORAGE_PATH_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable:4996)
#endif

#include <vector>
#include <boost/algorithm/string.hpp>
#include <dl_types.h>


using namespace dvblink;

namespace dvblex { namespace settings {

const wchar_t path_delimeter = L'/';

class storage_path : public base_type_wstring_t<storage_path_id>
{
public:
    storage_path()
    {
    }

    storage_path(const storage_path& path) :
        base_type_wstring_t<storage_path_id>(path)
    {
        adjust();
        remove_slash_right();
    }

    storage_path(const std::wstring& path) :
        base_type_wstring_t<storage_path_id>(path)
    {
        adjust();
        remove_slash_right();
    }

    storage_path(wchar_t const* path) :
        base_type_wstring_t<storage_path_id>(path)
    {
        adjust();
        remove_slash_right();
    }

    const wchar_t* c_str() const
    {
        return base_type_wstring_t<storage_path_id>::id_.c_str();
    }

    void operator = (const storage_path& id)
    {
        base_type_wstring_t<storage_path_id>::id_ = id.get();
        adjust();
        remove_slash_right();
    }

    void operator = (const wchar_t* id)
    {
        base_type_wstring_t<storage_path_id>::id_ = id;
        adjust();
        remove_slash_right();
    }

    void operator = (const std::wstring& id)
    {
        base_type_wstring_t<storage_path_id>::id_ = id;
        adjust();
        remove_slash_right();
    }

    storage_path& operator /= (const storage_path& p)
    {
        id_ += L"/" + p.get();
        return *this;
    }

    storage_path& operator /= (const std::wstring& p)
    {
        storage_path r(p);
        r.remove_slash_left();
        id_ += L"/" + r.get();
        return *this;
    }

    storage_path& operator /= (const wchar_t* p)
    {
        storage_path r(p);
        r.remove_slash_left();
        id_ += L"/" + r.get();
        return *this;
    }

    static wchar_t get_delimeter()
    {
        return path_delimeter;
    }

    static bool is_delimeter(wchar_t ch)
    {
        return ch == path_delimeter;
    }

    typedef std::vector<std::wstring> split_path_t;
    void split(split_path_t& result)
    {
        boost::algorithm::split(result, id_, &storage_path::is_delimeter);
    }

private:
    void adjust()
    {
        boost::algorithm::replace_all(id_, "\\", "/");
    }

    void remove_slash_right()
    {
        boost::algorithm::trim_right_if(id_, boost::algorithm::is_any_of("/"));
    }

    void remove_slash_left()
    {
        boost::algorithm::trim_left_if(id_, boost::algorithm::is_any_of("/"));
    }

    void remove_slash_right(storage_path& path)
    {
        path.set(boost::algorithm::trim_right_copy_if(path.get(), boost::algorithm::is_any_of("/")));
    }

    void remove_slash_left(storage_path& path)
    {
        path.set(boost::algorithm::trim_left_copy_if(path.get(), boost::algorithm::is_any_of("/")));
    }

    std::wstring remove_slash_right(const std::wstring& path)
    {
        return boost::algorithm::trim_right_copy_if(path, boost::algorithm::is_any_of("/"));
    }

    std::wstring remove_slash_left(const std::wstring& path)
    {
        return boost::algorithm::trim_left_copy_if(path, boost::algorithm::is_any_of("/"));
    }

    // hidden operators
    storage_path& operator += (const storage_path& wid);
    storage_path& operator += (const std::wstring& id);
    storage_path& operator += (const wchar_t* id);

    storage_path operator + (const storage_path& wid);
    storage_path operator + (const std::wstring& id);
    storage_path operator + (const wchar_t* id);
};

inline storage_path operator / (const storage_path& lhs, const storage_path& rhs)  {return storage_path(lhs) /= rhs;}
inline storage_path operator / (const storage_path& lhs, const std::wstring& rhs) {return storage_path(lhs) /= rhs;}
inline storage_path operator / (const storage_path& lhs, const wchar_t* rhs) {return storage_path(lhs) /= rhs;}

} //configuration
} //dvblink

#ifdef _WIN32
#pragma warning(pop)
#endif

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_CONFIG_STORAGE_STORAGE_PATH_H_
