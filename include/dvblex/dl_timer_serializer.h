/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <dl_types.h>
#include <dl_runtime_error.h>
#include <dl_pugixml_helper.h>
#include <dl_program_serializer.h>

namespace dvblex {

    const std::string RECORDING_REMOVE_ROOT_NODE         = "remove_recording";
    const std::string RECORDING_ID_NODE                  = "recording_id";

    const std::string RECORDINGS_ROOT_NODE               = "recordings";
    const std::string RECORDING_ROOT_NODE                = "recording";
    const std::string RECORDING_SCHEDULE_ID_NODE         = "schedule_id";
    const std::string RECORDING_CHANNEL_ID_NODE          = "channel_id";
    const std::string RECORDING_IS_ACTIVE_NODE           = "is_active";
    const std::string RECORDING_IS_CONFLICT_NODE         = "is_conflict";
    const std::string PROGRAM_ROOT_NODE                  = "program";

    class rd_recording_t
    {
    public:
        rd_recording_t()
            : schedule_id_(DL_RECORDER_INVALID_ID), is_active_(false), is_conflicting_(false) { }

        const dvblink::recording_id_t get_recording_id() const { return recording_id_; }
        const dvblink::schedule_item_id_t get_schedule_id() const { return schedule_id_; }
        const dvblink::channel_id_t get_channel_id() const { return channel_id_; }
        bool is_active() const { return is_active_; }
        bool is_conflicting() const { return is_conflicting_; }
        const dvblex::recorder::epg_item_ex get_program() const { return program_; }

        void set_recording_id(const dvblink::recording_id_t& id) { recording_id_ = id; }
        void set_schedule_id(const dvblink::schedule_item_id_t& id) { schedule_id_ = id; }
        void set_channel_id(const dvblink::channel_id_t& channel_id) { channel_id_ = channel_id; }
        void set_active(bool is_active) { is_active_ = is_active; }
        void set_conflicting(bool is_conflicting) { is_conflicting_ = is_conflicting; }
        void set_program(const dvblex::recorder::epg_item_ex& program) { program_ = program; }

    private:
        dvblink::recording_id_t recording_id_;
        dvblink::schedule_item_id_t schedule_id_;
        dvblink::channel_id_t channel_id_;
        bool is_active_;
        bool is_conflicting_;
        dvblex::recorder::epg_item_ex program_;
    };

    typedef std::vector<rd_recording_t> rd_recording_list_t;

    inline pugi::xml_node& operator<< (pugi::xml_node& parent_node, const rd_recording_t& recording)
    {
        pugi::xml_node recording_node = dvblink::pugixml_helpers::new_child(parent_node, RECORDING_ROOT_NODE);

        if (recording_node == NULL)
        {
            throw dvblink::runtime_error(dvblink::xml_error_parse_msg);
        }
        
        dvblink::pugixml_helpers::new_child(recording_node, RECORDING_ID_NODE, recording.get_recording_id().get());

        std::stringstream buf;
        buf << recording.get_schedule_id().get();
        dvblink::pugixml_helpers::new_child(recording_node, RECORDING_SCHEDULE_ID_NODE, buf.str());

        dvblink::pugixml_helpers::new_child(recording_node, RECORDING_CHANNEL_ID_NODE, recording.get_channel_id().get());
        
        if (recording.is_active())
        {
            dvblink::pugixml_helpers::new_child(recording_node, RECORDING_IS_ACTIVE_NODE, dvblink::pugixml_helpers::xmlnode_value_true);
        }

        if (recording.is_conflicting())
        {
            dvblink::pugixml_helpers::new_child(recording_node, RECORDING_IS_CONFLICT_NODE, dvblink::pugixml_helpers::xmlnode_value_true);
        }

        recording_node << recording.get_program();

        return parent_node;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& parent_node, rd_recording_t& recording)
    {
        if (NULL != parent_node)
        {
            std::string str_value;
            if (dvblink::pugixml_helpers::get_node_value(parent_node, RECORDING_ID_NODE, str_value))
            {
                recording.set_recording_id(str_value);
            }

            if (dvblink::pugixml_helpers::get_node_value(parent_node, RECORDING_SCHEDULE_ID_NODE, str_value))
            {
                int l;
                dvblink::engine::string_conv::apply(str_value.c_str(), l, DL_RECORDER_INVALID_ID);
                recording.set_schedule_id(l);
            }

            if (dvblink::pugixml_helpers::get_node_value(parent_node, RECORDING_CHANNEL_ID_NODE, str_value))
            {
                recording.set_channel_id(str_value);
            }       

            if (dvblink::pugixml_helpers::get_node_value(parent_node, RECORDING_IS_ACTIVE_NODE, str_value) && boost::iequals(str_value, dvblink::pugixml_helpers::xmlnode_value_true))
            {
                recording.set_active(true);
            }

            if (dvblink::pugixml_helpers::get_node_value(parent_node, RECORDING_IS_CONFLICT_NODE, str_value) && boost::iequals(str_value, dvblink::pugixml_helpers::xmlnode_value_true))
            {
                recording.set_conflicting(true);
            }

            pugi::xml_node prg_node = parent_node.child(PROGRAM_ROOT_NODE.c_str());
            if (prg_node != NULL)
            {
                dvblex::recorder::epg_item_ex program;
                prg_node >> program;
                recording.set_program(program);
            }
        }

        return parent_node;
    }

    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const rd_recording_list_t& recordings)
    {
        pugi::xml_node recordings_node = doc.append_child(RECORDINGS_ROOT_NODE.c_str());

        if (NULL == recordings_node)
        {
            throw dvblink::runtime_error(dvblink::xml_error_parse_msg);
        }
        
        rd_recording_list_t::const_iterator it = recordings.begin();
        while (it != recordings.end()) 
        {
            recordings_node << *it;
            ++it;
        }

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, rd_recording_list_t& recording_list)
    {
        if (NULL != node) 
        {
            pugi::xml_node recordings_node = node.first_child();
            while (NULL != recordings_node)                
            {
                rd_recording_t recording;
                recordings_node >> recording;
                recording_list.push_back(recording);

                recordings_node = recordings_node.next_sibling();
            }
        }

        return node;
    }  

///////////////////////////////////////////////////////////////////////////////


    class rd_recording_remover_t
    {
    public:
        const dvblink::recording_id_t get_recording_id() const { return recording_id_; }
        void set_recording_id(const dvblink::recording_id_t& recording_id) { recording_id_ = recording_id; }
   
    private:
        dvblink::recording_id_t recording_id_;
    };

    inline pugi::xml_document& operator<< (pugi::xml_document& doc, const rd_recording_remover_t& recording)
    {
        pugi::xml_node rr_node = doc.append_child(RECORDING_REMOVE_ROOT_NODE.c_str());

        if (NULL == rr_node)
        {       
            throw dvblink::runtime_error(dvblink::xml_error_parse_msg);
        }
        
        dvblink::pugixml_helpers::new_child(rr_node, RECORDING_ID_NODE, recording.get_recording_id().get());               

        return doc;
    }

    inline pugi::xml_node& operator>> (pugi::xml_node& node, rd_recording_remover_t& recording)
    {
        if (NULL != node)
        {
            std::string str_value;

            if (dvblink::pugixml_helpers::get_node_value(node, RECORDING_ID_NODE, str_value))
                recording.set_recording_id(str_value);
        }

        return node;
    }

}
