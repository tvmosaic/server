/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/serialization/vector.hpp>
#include <dl_types.h>
#include <dl_message.h>
#include <dl_products.h>

namespace dvblink { namespace messaging { namespace license {

//
// get installed products
//
struct get_installed_products_response
{
    get_installed_products_response() :
        result_(false)
    {}

    dvblex::product_info_list_t products_;
    bool_flag_t result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & products_;
        ar & result_;
    }
};

struct get_installed_products_request : public message_send<get_installed_products_request, get_installed_products_response>
{
};


//
// activate product
//
struct activate_product_response
{
    activate_product_response() :
        result_(dvblex::eapr_error)
    {}

    dvblex::product_activation_result_e result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & result_;
    }
};

struct activate_product_request : public message_send<activate_product_request, activate_product_response>
{
    activate_product_request() {}
    activate_product_request(const dvblex::product_activation_info_t& activation_info) : activation_info_(activation_info) {}

    dvblex::product_activation_info_t activation_info_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & activation_info_;
    }
};

//
// activate product
//
struct activate_product_trial_response
{
    activate_product_trial_response() :
        result_(dvblex::eapr_error)
    {}

    dvblex::product_activation_result_e result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & result_;
    }
};

struct activate_product_trial_request : public message_send<activate_product_trial_request, activate_product_trial_response>
{
    activate_product_trial_request() {}
    activate_product_trial_request(const dvblink::product_id_t& product_id) : product_id_(product_id) {}

    dvblink::product_id_t product_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & product_id_;
    }
};

} //license
} //messaging
} //dvblink
