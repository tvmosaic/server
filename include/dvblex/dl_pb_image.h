/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>
#include <dl_pb_item.h>
#include <dl_pugixml_helper.h>

namespace dvblex { namespace playback {

struct pb_image_t : public pb_item_t
{
    pb_image_t() :
      pb_item_t(pbit_item_image) { }
};

const std::string image_root_node                    = "image";

inline void write_to_node(pugi::xml_node& node, const pb_image_t& image)
{
    pugi::xml_node i_node = dvblink::pugixml_helpers::new_child(node, image_root_node);
    if (i_node != NULL)
    {
        write_to_node(i_node, (pb_item_t)image);
    }
}

inline void read_from_node(pugi::xml_node& node, pb_image_t& image)
{
    read_from_node(node, (pb_item_t&)image);
}

} // playback
} // dvblex

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::playback::pb_image_t& pi, const unsigned int /*version*/)
{
    ar & pi.object_id_;
    ar & pi.parent_id_;
    ar & pi.url_;
    ar & pi.thumbnail_;
    ar & pi.item_type_;
    ar & pi.can_be_deleted_;
    ar & pi.size_;
    ar & pi.creation_time_;
}

} // namespace serialization
} // namespace boost
