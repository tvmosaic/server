/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/shared_ptr.hpp>
#include <dl_types.h>
#include <dli_base.h>
#include <dl_streamer.h>
#include <dl_epg_box.h>
#include <dl_status.h>

namespace dvblink { 

enum start_scan_status_e
{
    sss_success = 0,
    sss_error = 1,
    sss_no_free_device = 2
};
    
namespace messaging {

#pragma pack(push, 1)

//{1131E546-B578-4cdf-8904-EC8F627D2BB6}
const i_guid stream_source_control_interface = 
{ 0x1131e546, 0xb578, 0x4cdf, { 0x89, 0x04, 0xec, 0x8f, 0x62, 0x7d, 0x2b, 0xb6 } };

struct i_stream_source_control : public i_base_object
{
    //request stream
    virtual EStatus __stdcall start_channel(const char* channel_id, streamer_object_t& streamer) = 0;
    virtual EStatus __stdcall start_channel_on_device(const char* channel_id, const char* device_id, const char* tuning_params, streamer_object_t& streamer) = 0;
    virtual void __stdcall stop_streamer(const char* streamer_id) = 0;
    //request epg scan
    virtual start_scan_status_e __stdcall start_epg_scan(const char* provider_id, epg_box_obj_t& epg_box) = 0;
};

typedef boost::shared_ptr<i_stream_source_control> i_stream_source_control_t;

#pragma pack(pop)

} //auxes
} //dvblink
