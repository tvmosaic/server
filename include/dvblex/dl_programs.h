/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <dl_types.h>
#include <dl_epgevent.h>

namespace dvblex { namespace recorder {

enum epg_genre_category_e
{
    DRGC_ANY                                    = 0x00000000,
    DRGC_NEWS                                   = 0x00000001,
    DRGC_KIDS                                   = 0x00000002,
    DRGC_MOVIE                                  = 0x00000004,
    DRGC_SPORT                                  = 0x00000008,
    DRGC_DOCUMENTARY                            = 0x00000010,
    DRGC_ACTION                                 = 0x00000020,
    DRGC_COMEDY                                 = 0x00000040,
    DRGC_DRAMA                                  = 0x00000080,
    DRGC_EDU                                    = 0x00000100,
    DRGC_HORROR                                 = 0x00000200,
    DRGC_MUSIC                                  = 0x00000400,
    DRGC_REALITY                                = 0x00000800,
    DRGC_ROMANCE                                = 0x00001000,
    DRGC_SCIFI                                  = 0x00002000,
    DRGC_SERIAL                                 = 0x00004000,
    DRGC_SOAP                                   = 0x00008000,
    DRGC_SPECIAL                                = 0x00010000,
    DRGC_THRILLER                               = 0x00020000,
    DRGC_ADULT                                  = 0x00040000,
    DRGC_LAST                                   = DRGC_ADULT
};

#define DL_RECORDER_INVALID_ID                  (-1)
#define DL_RECORDER_INVALID_TIME                (-1)

class epg_item
{
public:
    epg_item() {}
    ~epg_item() {}
    
    std::string generate_id(const time_t& start_time)
    {
        std::stringstream strbuf;
        strbuf << start_time;
        return strbuf.str();
    }

    void make_id()
    {
        if (event_.id_.empty())
        {
            event_.id_ = generate_id(event_.m_StartTime);
        }
    }

public:
    dvblink::engine::DLEPGEvent event_;
};

class epg_item_ex : public epg_item
{
public:
    epg_item_ex() :
        is_record_(false), is_series_(false), is_repeat_record_(false), is_record_conflict_(false)
    {}

    void from_item(const epg_item& item)
    {
        event_ = item.event_;
    }

public:
    bool is_record_;
    bool is_series_;
    bool is_repeat_record_;
    bool is_record_conflict_;
};

typedef std::vector<epg_item_ex> epg_event_ex_list_t;
typedef std::map<dvblink::channel_id_t, epg_event_ex_list_t> epg_ex_channel_info_map_t;

struct timed_epg_ex_channel_info_map_t
{
    timed_epg_ex_channel_info_map_t() :
        time_stamp_(0)
    {}

    time_t time_stamp_;
    epg_ex_channel_info_map_t epg_map_;
};

} //recorder
} //dvblex
