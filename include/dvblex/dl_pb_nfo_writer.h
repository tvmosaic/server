/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <dl_common.h>
#include <dl_pugixml_helper.h>
#include <dl_pb_video_info.h>
#include <dl_pb_video.h>
#include <dl_pb_recorded_tv.h>

namespace dvblex { namespace playback {

const std::string nfo_episode_details_root = "episodedetails";
const std::string nfo_movie_root = "movie";
const std::string nfo_title_node = "title";
const std::string nfo_showtitle_node = "showtitle";
const std::string nfo_ratings_node = "ratings";
const std::string nfo_rating_node = "rating";
const std::string nfo_max_node = "max";
const std::string nfo_default_node = "default";
const std::string nfo_value_node = "value";
const std::string nfo_season_node = "season";
const std::string nfo_episode_node = "episode";
const std::string nfo_plot_node = "plot";
const std::string nfo_runtime_node = "runtime";
const std::string nfo_thumb_node = "thumb";
const std::string nfo_genre_node = "genre";
const std::string nfo_tag_node = "tag";
const std::string nfo_credits_node = "credits";
const std::string nfo_director_node = "director";
const std::string nfo_year_node = "year";
const std::string nfo_aired_node = "aired";
const std::string nfo_actor_node = "actor";
const std::string nfo_name_node = "name";

inline void split_list_into_tokens(const std::string& keyword_list, std::vector<std::string>& tokens)
{
    boost::split( tokens, keyword_list, boost::is_any_of("/"), boost::token_compress_on);
}

inline void split_list_and_add_keywords(pugi::xml_node node, const char* keyword, const std::string& keyword_list)
{
    if (!keyword_list.empty())
    {
	    std::vector<std::string> tokens;
        split_list_into_tokens(keyword_list, tokens);

	    for (size_t i=0; i<tokens.size(); i++)
        {
            std::string t = boost::trim_copy(tokens[i]);
            if (!t.empty())
                dvblink::pugixml_helpers::new_child(node, keyword, t);
        }
    }
}

inline void add_genre_to_nfo(bool is_set, const char* genre_tag, const std::map<std::string, std::string>& genre_names, pugi::xml_node node)
{
    if (is_set)
    {
        std::map<std::string, std::string>::const_iterator it = genre_names.find(genre_tag);
        if (it != genre_names.end())
            dvblink::pugixml_helpers::new_child(node, nfo_genre_node, it->second);
    }
}

inline bool create_nfo_from_pb_item(const boost::shared_ptr<pb_item_t>& pb_item, const std::map<std::string, std::string>& genre_names, 
                                    const std::string& thumb_local_file, std::string& nfo)
{
    bool ret_val = false;

    pb_video_info_t vi;

	switch (pb_item->item_type_)
	{
	case pbit_item_recorded_tv:
		{
            boost::shared_ptr<pb_recorded_tv_t> recorded_tv = boost::static_pointer_cast<pb_recorded_tv_t>(pb_item);
            vi = recorded_tv->video_info_;
            ret_val = true;
		}
        break;
	case pbit_item_video:
		{
            boost::shared_ptr<pb_video_t> pb = boost::static_pointer_cast<pb_video_t>(pb_item);
            vi = pb->video_info_;
            ret_val = true;
		}
		break;
	default:
		{
		}
		break;
	}

    if (ret_val)
    {
        std::string str;

        //movie or tv episode?
        bool is_item_movie = !(vi.m_SeasonNum > 0 || vi.m_EpisodeNum > 0 || !vi.m_SecondName.empty());

        pugi::xml_document doc;
        pugi::xml_node root_node = doc.append_child(is_item_movie ? nfo_movie_root.c_str() : nfo_episode_details_root.c_str());
        if (root_node != NULL)
        {
            if (is_item_movie)
            {
                //title
	            dvblink::pugixml_helpers::new_child(root_node, nfo_title_node, vi.m_Name);
            } else
            {
                //title
                dvblink::pugixml_helpers::new_child(root_node, nfo_title_node, !vi.m_SecondName.empty() ? vi.m_SecondName : vi.m_Name);
                //show title
	            dvblink::pugixml_helpers::new_child(root_node, nfo_showtitle_node, vi.m_Name);
                //season
                if (vi.m_SeasonNum > 0)
                {
                    str = boost::lexical_cast<std::string>(vi.m_SeasonNum);
                    dvblink::pugixml_helpers::new_child(root_node, nfo_season_node, str);
                }
                //episode
                if (vi.m_EpisodeNum > 0)
                {
                    str = boost::lexical_cast<std::string>(vi.m_EpisodeNum);
                    dvblink::pugixml_helpers::new_child(root_node, nfo_episode_node, str);
                }
                
            }
            //rating
            if (vi.m_StarNum > 0)
            {
                pugi::xml_node ratings_node = dvblink::pugixml_helpers::new_child(root_node, nfo_ratings_node);
                if (ratings_node != NULL)
                {
                    pugi::xml_node rating_node = dvblink::pugixml_helpers::new_child(ratings_node, nfo_rating_node);
                    if (rating_node != NULL)
                    {
	                    dvblink::pugixml_helpers::add_node_attribute(rating_node, nfo_name_node, "default");
                        str = boost::lexical_cast<std::string>(vi.m_StarNumMax);
	                    dvblink::pugixml_helpers::add_node_attribute(rating_node, nfo_max_node, str);
	                    dvblink::pugixml_helpers::add_node_attribute(rating_node, nfo_default_node, "true");
                        str = boost::lexical_cast<std::string>(vi.m_StarNum);
            	        dvblink::pugixml_helpers::new_child(rating_node, nfo_value_node, str);
                    }
                }
            }
            //plot (aka description)
            dvblink::pugixml_helpers::new_child(root_node, nfo_plot_node, vi.m_ShortDesc);
            //runtime
            str = boost::lexical_cast<std::string>(vi.m_Duration / 60);
            dvblink::pugixml_helpers::new_child(root_node, nfo_runtime_node, str);
            //thumb
            if (!thumb_local_file.empty())
                dvblink::pugixml_helpers::new_child(root_node, nfo_thumb_node, thumb_local_file);
            else if (!vi.m_ImageURL.empty())
                dvblink::pugixml_helpers::new_child(root_node, nfo_thumb_node, vi.m_ImageURL);
            //genres
            add_genre_to_nfo(vi.m_IsMovie, EPG_PROGRAM_CAT_MOVIE, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsAction, EPG_PROGRAM_CAT_ACTION, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsAdult, EPG_PROGRAM_CAT_ADULT, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsComedy, EPG_PROGRAM_CAT_COMEDY, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsDocumentary, EPG_PROGRAM_CAT_DOCUMENTARY, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsDrama, EPG_PROGRAM_CAT_DRAMA, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsEducational, EPG_PROGRAM_CAT_EDUCATIONAL, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsHorror, EPG_PROGRAM_CAT_HORROR, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsKids, EPG_PROGRAM_CAT_KIDS, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsMusic, EPG_PROGRAM_CAT_MUSIC, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsNews, EPG_PROGRAM_CAT_NEWS, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsReality, EPG_PROGRAM_CAT_REALITY, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsRomance, EPG_PROGRAM_CAT_ROMANCE, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsScienceFiction, EPG_PROGRAM_CAT_SCIFI, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsSerial, EPG_PROGRAM_CAT_SERIAL, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsSoap, EPG_PROGRAM_CAT_SOAP, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsSpecial, EPG_PROGRAM_CAT_SPECIAL, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsSports, EPG_PROGRAM_CAT_SPORTS, genre_names, root_node);
            add_genre_to_nfo(vi.m_IsThriller, EPG_PROGRAM_CAT_THRILLER, genre_names, root_node);

            //tags
            split_list_and_add_keywords(root_node, nfo_tag_node.c_str(), vi.m_Categories);
            //writers
            split_list_and_add_keywords(root_node, nfo_credits_node.c_str(), vi.m_Writers);
            //directors
            split_list_and_add_keywords(root_node, nfo_director_node.c_str(), vi.m_Directors);
            //year
            if (vi.m_Year > 0)
            {
                str = boost::lexical_cast<std::string>(vi.m_Year);
                dvblink::pugixml_helpers::new_child(root_node, nfo_year_node, str);
            }
            //aired (aka start time)
            {
                char dt_buf[1024];
                if (struct tm* ti = localtime(&vi.m_StartTime))
                {
                    dt_buf[0] = '\0';
                    sprintf(dt_buf, "%d-%0.2d-%0.2d", ti->tm_year + 1900, ti->tm_mon + 1, ti->tm_mday);

                    dvblink::pugixml_helpers::new_child(root_node, nfo_aired_node, dt_buf);
                }
            }
            //actors
            std::vector<std::string> actors_list;
            split_list_into_tokens(vi.m_Actors, actors_list);
            for (size_t i=0; i<actors_list.size(); i++)
            {
                std::string t = boost::trim_copy(actors_list[i]);
                if (!t.empty())
                {
                    pugi::xml_node actor_node = dvblink::pugixml_helpers::new_child(root_node, nfo_actor_node);
                    if (actor_node != NULL)
                        pugi::xml_node name_node = dvblink::pugixml_helpers::new_child(actor_node, nfo_name_node, t);
                }
            }

            dvblink::pugixml_helpers::xmldoc_dump_to_string(doc, nfo);
        }

    }

    return ret_val;
}

} // playback
} // dvblex

