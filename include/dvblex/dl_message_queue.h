/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <map>
#include <vector>
#include <queue>
#include <boost/thread.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <dl_event.h>
#include <dli_message_driven.h>

namespace dvblink { namespace messaging {

typedef void (*client_invoke_function_t)(void* handler, const message_id_t& id, const message_sender_t& sender, const std::string& message_body);

struct message_container
{
    struct item_message
    {
        item_message() :
            subscriber_(NULL),
            callback_(NULL)
        {

        }
        item_message(void* subscriber, client_invoke_function_t callback) :
            subscriber_(subscriber),
            callback_(callback)
        {

        }
        void* subscriber_;
        client_invoke_function_t callback_;
    };
    typedef std::map<std::string, item_message> dispatcher_list_t;

    boost::shared_mutex lock_;
    dispatcher_list_t dispatcher_list_;

    void reg(const char* type_name, void* subscriber, client_invoke_function_t fn)
    {
        boost::unique_lock<boost::shared_mutex> lock(lock_);        
        item_message item(subscriber, fn);
        std::string name(type_name);
        
        std::pair<dispatcher_list_t::iterator, bool> res =
            dispatcher_list_.insert(dispatcher_list_t::value_type(name, item));
            //dispatcher_list_.insert(std::make_pair(name, item));
       
        assert(res.second); //such a message already exists in the map
    }

    void unreg(const char* type_name)
    {
        boost::unique_lock<boost::shared_mutex> lock(lock_);
        dispatcher_list_t::iterator iter = dispatcher_list_.find(type_name);
        assert(iter != dispatcher_list_.end());
        
        if (iter != dispatcher_list_.end())
        {
            dispatcher_list_.erase(iter);
        }
    }

    bool get(const char* message_type_name, item_message& item)
    {
        boost::shared_lock<boost::shared_mutex> lock(lock_);
        dispatcher_list_t::iterator iter = dispatcher_list_.find(message_type_name);
        if (iter != dispatcher_list_.end())
        {
            item = iter->second;
            return true;
        }
        return false;
    }
};

class message_queue : public i_client_message_queue
{
    template <typename REQUEST> friend class message_post;
    template <typename REQUEST, typename RESPONSE> friend class message_send;

    struct message_out
    {
        message_out(message_id_t& id, message_addressee_t to) :
            id_(id),
            to_(to)
        {
            id.set(id.get() + 1);
        }

        message_id_t id_;
        message_addressee_t to_;

        message_error error_;

        std::string message_type_name_;
        std::string message_body_;

        event event_;
    };

    struct message_in
    {
        message_in(message_addressee_t to, message_sender_t from, type message_type,
                const std::string& message_type_name, const boost::uint8_t* message_body, size_t message_body_size, const message_id_t& id) :
            id_(id),
            to_(to),
            from_(from),
            type_(message_type),
            message_type_name_(message_type_name)
        {
            message_body_.assign(message_body, message_body + message_body_size);
        }
        
        message_id_t id_;

        message_addressee_t to_;
        message_sender_t from_;

        type type_;

        std::string message_type_name_;
        std::string message_body_;
    };

    typedef boost::shared_ptr<message_in> message_in_t;
    typedef boost::shared_ptr<message_out> message_out_t;

public:
    message_queue();
    message_queue(const message_queue_id_t& id);
    virtual ~message_queue();

    void __stdcall init(const i_server_messaging_core_t& msg_core);
    void __stdcall shutdown();
    const boost::uuids::uuid& __stdcall get_uid()       {return id_.get();}

    void __stdcall set_id(const message_queue_id_t& id) {id_ = id;}
    const message_queue_id_t& __stdcall get_id() const  {return id_;}

    i_result __stdcall query_interface(const base_id_t& /*requestor_id*/, const i_guid& /*iid*/, i_base_object_t& /*obj*/) {return i_not_implemented;}

protected:
    void __stdcall put_message(type message_type, const message_id_t& id, const message_sender_t& from,
        const char* message_type_name, const void* message_body, size_t message_body_size);

    void __stdcall put_response(const message_id_t& id, message_error error, const message_sender_t& from, const void* message_body, size_t message_body_size);

public:
    /*
        method is called by sender
    */
    template <typename REQUEST>
    message_error post(const message_addressee_t& to, const REQUEST& request)
    {
        if (shutdown_)
            return queue_shutdown;

        std::ostringstream archive_stream;
        boost::archive::text_oarchive archive(archive_stream);

        archive << request;
        return msg_core_->post(to, message_sender_t(id_.get()), typeid(request).name(), archive_stream.str().c_str(), archive_stream.str().length());
    }

    template <typename REQUEST, typename RESPONSE>
    message_error send(const message_addressee_t& to, const REQUEST& request, RESPONSE& response, timeout_t timeout_ms = infinite_timeout)
    {
        // type of REQUEST::response_t and RESPONSE must be the same!!!
        BOOST_MPL_ASSERT((boost::is_same<typename REQUEST::response_t, RESPONSE>));
        assert(to != broadcast_addressee);

        if (shutdown_)
            return queue_shutdown;

        // serialize request
        std::ostringstream archive_stream;
        boost::archive::text_oarchive archive(archive_stream);
        archive << request;

        message_out_t msg_out;
        {
            boost::unique_lock<boost::shared_mutex> lock(lock_out_);
            msg_out = message_out_t(new message_out(last_out_id_, to));
            queue_out_.insert(map_out_messages_t::value_type(msg_out->id_, msg_out));
        }

        message_error status = msg_core_->send(msg_out->id_, to,
            message_sender_t(id_.get()), typeid(request).name(), archive_stream.str().c_str(), archive_stream.str().length());
        
        if (status == success)
        {
            // wait unless request completes
            errcode_t err = msg_out->event_.wait(timeout_ms);
            if (err == err_none)
            {
                // response is in the msg_out already
                // unserialize response
                if (msg_out->error_ != handler_not_registered)
                {
                    if (!msg_out->message_body_.empty())
                    {
                        std::istringstream archive_stream(msg_out->message_body_);
                        boost::archive::text_iarchive archive(archive_stream);
                        archive >> response;
                    }
                }
                else
                {
                    status = handler_not_registered;
                }
            }
            else
            {
                status = (err == err_timeout ? timeout : error);
            }
        }

        boost::unique_lock<boost::shared_mutex> lock(lock_out_);
        iter_map_out_messages_t iter = queue_out_.find(msg_out->id_);
        if (iter != queue_out_.end())
        {
            queue_out_.erase(iter);
        }

        return status;
    }

    template <typename RESPONSE>
    void response(const message_id_t& id, message_error error, const message_addressee_t& to, const RESPONSE& response)
    {
        std::ostringstream archive_stream;
        boost::archive::text_oarchive archive(archive_stream);

        archive << response;
        msg_core_->response(id, error, to, message_sender_t(id_.get()), archive_stream.str().c_str(), archive_stream.str().length());
    }

private:
    // container where info for dispatching of for all message types is registered processed by this queue 
    message_container msg_container_;

    void register_message(const char* type_name, void* subscriber, client_invoke_function_t fn);
    void unregister_message(const char* type_name);
    void deliver_message(const char* message_type_name, const message_id_t& id, const message_sender_t& sender, const std::string& message_body);

    void queue_proc();

private:
    message_queue_id_t id_;
    i_server_messaging_core_t msg_core_;

    volatile bool shutdown_;

    ///////////////////////////////////////////////////////////////////////////
    // input queue
    boost::shared_mutex lock_in_;
    std::queue<message_in_t> queue_in_;
    event new_msg_in_event_;

    boost::thread* queue_in_thread_;
    volatile bool quit_;

    ///////////////////////////////////////////////////////////////////////////
    // output queue
    typedef std::map<message_id_t, message_out_t> map_out_messages_t;
    typedef map_out_messages_t::iterator iter_map_out_messages_t;
    typedef map_out_messages_t::const_iterator const_iter_map_out_messages_t;

    boost::shared_mutex lock_out_;
    map_out_messages_t queue_out_;
    message_id_t last_out_id_;
};

typedef boost::shared_ptr<message_queue> message_queue_t;

} //messaging
} //dvblink
