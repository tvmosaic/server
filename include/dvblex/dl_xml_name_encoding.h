/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <map>
#include <sstream>
#include <string>
#include <dl_url_encoding.h>

namespace dvblink {

#define DL_XML_NAME_ENC_START_STR   "dl_xmltag_"
#define DL_XML_NAME_HEX_ENC_CH      '_'

/* Returns a url-encoded version of str */
inline void xml_name_encode(const std::string& str, std::string& res_str) 
{
    res_str.clear();

	bool b_encode = false;
	//check if this string has to be encoded
	for (unsigned int i=0; i<str.size() && !b_encode; i++)
	{
		if (!isalnum((unsigned char)str[i]) && str[i] != '_')
			b_encode = true;

		if (i == 0 && (str[i] == '_' || isdigit(str[i])))
			b_encode = true;
	}

	if (b_encode)
	{
		//add dvblink start seq at the beginning as xml element name should always start with alpha ch or _ or :
		res_str += DL_XML_NAME_ENC_START_STR;
	    
		for (unsigned int i=0; i<str.size(); i++)
		{
			if (isalnum((unsigned char)str[i])) 
			{
				res_str += str[i];
			}
			else
			{
				res_str += DL_XML_NAME_HEX_ENC_CH;
				res_str += to_hex(str[i] >> 4);
				res_str += to_hex(str[i] & 0x0F);
			}
		}
	} else
	{
		res_str = str;
	}
}

/* Returns a url-decoded version of str */
inline void xml_name_decode(const char *str, std::string& res_str) 
{
    res_str.clear();
    
    //check that string starts with DL_XML_NAME_ENC_START_STR
    if (strlen(str) < strlen(DL_XML_NAME_ENC_START_STR) ||
        (strlen(str) >= strlen(DL_XML_NAME_ENC_START_STR) && strncmp(str, DL_XML_NAME_ENC_START_STR, strlen(DL_XML_NAME_ENC_START_STR)) != 0))
    {
        //this is not our encoded string. return it as is
        res_str = str;
        return;
    }
    
    const char *pstr = str + strlen(DL_XML_NAME_ENC_START_STR); //skip DL_XML_NAME_ENC_START_STR
    
    while (*pstr) 
    {
        if (*pstr == DL_XML_NAME_HEX_ENC_CH) 
        {
            if (pstr[1] && pstr[2]) 
            {
                res_str += (from_hex(pstr[1]) << 4 | from_hex(pstr[2]));
                pstr += 2;
            }
        } else 
        {
            res_str += *pstr;
        }
        pstr++;
    }
}

}
