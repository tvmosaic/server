/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <fstream>
#include <iostream>
#include <map>
#include <stdexcept>
#include <vector>
#include <dl_filesystem_path.h>

namespace dvblink { namespace zip {

template<class T>
void Read_Primitive(std::istream& stream, T& x);

template<class T>
void Write_Primitive(std::ostream& stream, const T& x);

struct zipfile_header
{
    const static unsigned int central_file_header_signature_ = 0x02014b50;
    const static unsigned int local_file_header_signature_ = 0x04034b50;

    boost::uint16_t version;
    boost::uint16_t flags;
    boost::uint16_t compression_type;
    boost::uint16_t stamp_date, stamp_time;
    boost::uint32_t crc;
    boost::uint32_t compressed_size, uncompressed_size;
    std::string filename;
    boost::uint32_t header_offset; // local header offset

    zipfile_header();
    zipfile_header(const std::string& filename_input);

    void fill_defaults();

    bool is_directory();
    bool is_zip_compressed();
    bool is_not_compressed();

public:
    bool read(std::istream& istream, bool global);
    void write(std::ostream& ostream, const bool global) const;
};

class zipfile_reader
{
public:
    zipfile_reader(const dvblink::filesystem_path_t& filename);
    virtual ~zipfile_reader();

    bool is_valid();
    bool decompress(const dvblink::filesystem_path_t& destination);

protected:
    bool load();
    void reset();

private:
    typedef std::map<std::string, zipfile_header*> filename_to_header_map_t;

    filename_to_header_map_t filename_to_header_;
    std::ifstream istream;
    dvblink::filesystem_path_t zip_file_;

    bool find_and_read_central_header();
};

class zip_file_writer
{
    const static unsigned int end_of_central_signature_ = 0x06054b50;

    std::ofstream ostream_;
    std::vector<zipfile_header*> files_;

public:
    zip_file_writer(const dvblink::filesystem_path_t& file_name);
    virtual ~zip_file_writer();

    bool add_file(const dvblink::filesystem_path_t& file_name);
    void flush();

protected:
    void write_local_file_header(const dvblink::filesystem_path_t& file_name,
        boost::uint32_t compressed_size, boost::uint32_t uncompressed_size);

private:
    bool flushed_;
};

}
}
