/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <boost/shared_ptr.hpp>
#include <dl_types.h>
#include <dl_platforms.h>
#include <dl_channel_info.h>
#include <dl_epgevent.h>

namespace dvblink { 

struct epg_channel_tune_info_t
{
    dvblink::channel_id_t channel_id_;
    dvblex::concise_channel_tune_info_t tuning_params_;
};

typedef std::vector<epg_channel_tune_info_t> epg_channel_tune_info_list_t;

class epg_box_t
{
public:
    epg_box_t() :
        cancelled_(false)
    {}

    virtual ~epg_box_t(){}

    void set_channels(const dvblex::channel_id_list_t& channels) {channels_ = channels;}
    void get_channels(dvblex::channel_id_list_t& channels){channels = channels_;}

    void set_channels_to_scan(const epg_channel_tune_info_list_t& scan_channels){scan_channels_ = scan_channels;}
    void get_channels_to_scan(epg_channel_tune_info_list_t& scan_channels){scan_channels = scan_channels_;}

    void cancel(){cancelled_ = true;}
    bool is_cancelled(){return cancelled_;}

    virtual void process_epg_data(const dvblink::channel_id_t& /* channel_id */, const dvblink::engine::DLEPGEventList& /* epg_data */){}
    virtual void scan_finished(){}
    virtual void scan_aborted(){}

    static void __stdcall release(epg_box_t* obj) {delete obj;}

protected:
    dvblex::channel_id_list_t channels_;
    epg_channel_tune_info_list_t scan_channels_;
    bool cancelled_;
};

typedef boost::shared_ptr<epg_box_t> epg_box_obj_t;

template<class T>
boost::shared_ptr<T> share_epg_box_safely(T* obj)
{
    return boost::shared_ptr<T>(obj, &T::release);
}

}
