/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_types.h>
#include <dl_message.h>
#include <dl_filesystem_path.h>

namespace dvblink { namespace messaging {

//
// start
//
struct start_response
{
    start_response() : result_(false) {}
    start_response(bool result) : result_(result) {}
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & result_;
    } 
};
    
struct start_request : public message_send<start_request, start_response>
{
};

//
// shutdown
//
struct shutdown_response
{
    shutdown_response() : result_(false) {}
    shutdown_response(bool result) : result_(result) {}
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & result_;
    } 
};

struct shutdown_request : public message_send<shutdown_request, shutdown_response>
{
};

//
// sleep
//
struct standby_response
{
    standby_response() : result_(false) {}
    standby_response(bool result) : result_(result) {}
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & result_;
    } 
};

struct standby_request : public message_send<standby_request, standby_response>
{
};

//
// resume
//
struct resume_response
{
    resume_response() : result_(false) {}
    resume_response(bool result) : result_(result) {}
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & result_;
    } 
};

struct resume_request : public message_send<resume_request, resume_response>
{
};

//
// xml commands
//
struct xml_message_response
{
    xml_string_t xml_;
    xml_cmd_result_t result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & xml_;
        ar & result_;
    }
};

struct xml_message_request : public message_send<xml_message_request, xml_message_response>
{
    xml_message_request() :
        proto_("http")
    {}

    xml_message_request(const dvblink::url_proto_t& proto, const url_address_t& server_address, const xml_cmd_command_t& cmd_id, const xml_string_t& xml) : 
        cmd_id_(cmd_id), xml_(xml), server_address_(server_address), proto_(proto) {}

    xml_cmd_command_t cmd_id_;
    xml_string_t xml_;
    url_address_t server_address_;
    dvblink::url_proto_t proto_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & cmd_id_;
        ar & xml_;
        ar & server_address_;
        ar & proto_;
    }
};

struct xml_message_post_request : public message_post<xml_message_post_request>
{
    xml_message_post_request() {}
    xml_message_post_request(const url_address_t& server_address, const xml_cmd_command_t& cmd_id, const xml_string_t& xml) : 
        cmd_id_(cmd_id), xml_(xml), server_address_(server_address) {}

    xml_cmd_command_t cmd_id_;
    xml_string_t xml_;
    url_address_t server_address_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & cmd_id_;
        ar & xml_;
        ar & server_address_;
    }
};

///////////////////////////////////////////////////////////////////////////////
//
//Notifies all intersted parties about DVBLink server state change

struct server_state_change_request : public message_post<server_state_change_request>
{
    server_state_change_request () {}
    server_state_change_request (server_state_t old_state, server_state_t new_state) : 
        old_state_(old_state), new_state_(new_state) {}
        
    server_state_t old_state_;
    server_state_t new_state_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & old_state_;
        ar & new_state_;
    } 
};

//
// release my interfaces!
//
struct release_me_response
{
    release_me_response() : result_(false) {}
    release_me_response(bool result) : result_(result) {}
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & result_;
    } 
};

struct release_me_request : public message_send<release_me_request, release_me_response>
{
    release_me_request() {}
    release_me_request(const base_id_t& id) : id_(id) {}
    base_id_t id_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & id_;
    } 
};

} //messaging
} //dvblink
