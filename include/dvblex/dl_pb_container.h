/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <vector>
#include <boost/lexical_cast.hpp>
#include <dl_pb_item.h>
#include <dl_types.h>
#include <dl_pugixml_helper.h>

namespace dvblex { namespace playback {

const int pb_object_count_unknown = -1;

enum pb_container_type_e
{
    pbct_container_unknown = -1,
    pbct_container_source,
    pbct_container_type,
    pbct_container_category,
    pbct_container_category_sort,
    pbct_container_category_group,
    pbct_container_group
};

struct pb_container_t
{
    pb_container_t() :
      container_type_(pbct_container_unknown), content_type_(pbit_item_unknown), total_count_(pb_object_count_unknown)
    {}

    pb_container_t(pb_container_type_e container_type, pb_item_type_e content_type) :
      container_type_(container_type), content_type_(content_type), total_count_(pb_object_count_unknown) { }

    dvblink::object_id_t object_id_;
    dvblink::object_id_t parent_id_;
    dvblink::object_name_t name_;
    dvblink::object_desc_t description_;
    dvblink::url_address_t logo_;
    pb_container_type_e container_type_;
    pb_item_type_e content_type_;
    int total_count_;
    dvblink::object_id_t source_id_;
};

typedef std::vector<pb_container_t> pb_container_list_t;


const std::string containers_root_node               = "containers";
const std::string container_root_node                = "container";
const std::string container_object_id_node           = "object_id";
const std::string container_parent_id_node           = "parent_id";
const std::string container_name_node                = "name";
const std::string container_description_node         = "description";
const std::string container_logo_node                = "logo";
const std::string container_type_node                = "container_type";
const std::string container_content_type_node        = "content_type";
const std::string container_total_count_node         = "total_count";
const std::string container_source_id_node           = "source_id";

inline void write_to_node(pugi::xml_node& node, const pb_container_list_t& containers)
{
    std::stringstream buf;

    pugi::xml_node containers_node = dvblink::pugixml_helpers::new_child(node, containers_root_node);
    if (containers_node != NULL)
    {
        for (size_t i=0; i<containers.size(); i++)
        {
            pugi::xml_node container_node = dvblink::pugixml_helpers::new_child(containers_node, container_root_node);
			if (container_node != NULL)
			{
                dvblink::pugixml_helpers::new_child(container_node, container_object_id_node, containers[i].object_id_.get());
                dvblink::pugixml_helpers::new_child(container_node, container_parent_id_node, containers[i].parent_id_.get());
                dvblink::pugixml_helpers::new_child(container_node, container_name_node, containers[i].name_.get());
                dvblink::pugixml_helpers::new_child(container_node, container_description_node, containers[i].description_.get());
                dvblink::pugixml_helpers::new_child(container_node, container_logo_node, containers[i].logo_.get());

                if (!containers[i].source_id_.empty())
                    dvblink::pugixml_helpers::new_child(container_node, container_source_id_node, containers[i].source_id_.get());
                
                std::stringstream str_out;

                str_out.clear(); str_out.str("");
                str_out << containers[i].container_type_;
                dvblink::pugixml_helpers::new_child(container_node, container_type_node, str_out.str());

                str_out.clear(); str_out.str("");
                str_out << containers[i].content_type_;
                dvblink::pugixml_helpers::new_child(container_node, container_content_type_node, str_out.str());

                str_out.clear(); str_out.str("");
                str_out << containers[i].total_count_;
                dvblink::pugixml_helpers::new_child(container_node, container_total_count_node, str_out.str());
            }
        }
    }
}

inline void read_from_node(pugi::xml_node& node, pb_container_list_t& containers)
{
    std::stringstream buf;

    pugi::xml_node containers_node = node.child(containers_root_node.c_str());
    if (containers_node != NULL)
    {
		pugi::xml_node container_node = containers_node.first_child();
		while (NULL != container_node)
		{
            if (boost::iequals(container_node.name(), container_root_node))
			{
                pb_container_t container;

				std::string str;
				if (dvblink::pugixml_helpers::get_node_value(container_node, container_object_id_node, str))
					container.object_id_ = str;

				if (dvblink::pugixml_helpers::get_node_value(container_node, container_parent_id_node, str))
					container.parent_id_ = str;

				if (dvblink::pugixml_helpers::get_node_value(container_node, container_name_node, str))
					container.name_ = str;

				if (dvblink::pugixml_helpers::get_node_value(container_node, container_description_node, str))
					container.description_ = str;

				if (dvblink::pugixml_helpers::get_node_value(container_node, container_logo_node, str))
					container.logo_ = str;

				if (dvblink::pugixml_helpers::get_node_value(container_node, container_source_id_node, str))
					container.source_id_ = str;

                boost::uint32_t uvalue;
                if (dvblink::pugixml_helpers::get_node_value(container_node, container_type_node, str))
                {
                    dvblink::engine::string_conv::apply(str.c_str(), uvalue, (boost::uint32_t)container.container_type_);
                    container.container_type_ = (pb_container_type_e)uvalue;
                }

                if (dvblink::pugixml_helpers::get_node_value(container_node, container_content_type_node, str))
                {
                    dvblink::engine::string_conv::apply(str.c_str(), uvalue, (boost::uint32_t)container.content_type_);
                    container.content_type_ = (pb_item_type_e)uvalue;
                }

                boost::int32_t value;
                if (dvblink::pugixml_helpers::get_node_value(container_node, container_total_count_node, str))
                {
                    dvblink::engine::string_conv::apply(str.c_str(), value, (boost::int32_t)container.total_count_);
                    container.total_count_ = value;
                }

                containers.push_back(container);
			}

            container_node = container_node.next_sibling();
        }
    }
}


} // playback
} // dvblex

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::playback::pb_container_t& pc, const unsigned int /*version*/)
{
    ar & pc.object_id_;
    ar & pc.parent_id_;
    ar & pc.name_;
    ar & pc.description_;
    ar & pc.logo_;
    ar & pc.container_type_;
    ar & pc.content_type_;
    ar & pc.total_count_;
    ar & pc.source_id_;
}

} // namespace serialization
} // namespace boost
