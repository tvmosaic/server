/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>

namespace dvblex {

const std::string unknown_device_name                     = "IDS_SERVER_UNKNOWN_DEVICE_NAME";
const std::string scan_providers_name                     = "IDS_SERVER_CHANNEL_SCAN_PROVIDERS";
const std::string available_networks_name                 = "IDS_SERVER_AVAILABLE_NETWORKS";
const std::string tv_channels_favorites_name              = "IDS_SERVER_TV_FAVORITES_NAME";
const std::string radio_channels_favorites_name           = "IDS_SERVER_RADIO_FAVORITES_NAME";
const std::string manual_device_type_name                  = "IDS_SERVER_MANUAL_DEVICE_TYPE";

const std::string file_source_standard_name		           = "IDS_SERVER_STANDARD_FILE_NAME";
const std::string file_source_standard_desc		           = "IDS_SERVER_STANDARD_FILE_DESC";
const std::string file_source_device_name		           = "IDS_SERVER_DEVICE_FILE_NAME";

const std::string iptv_source_standard_name		           = "IDS_SERVER_STANDARD_IPTV_NAME";
const std::string iptv_source_standard_desc		           = "IDS_SERVER_STANDARD_IPTV_DESC";
const std::string iptv_source_device_name		           = "IDS_SERVER_DEVICE_IPTV_NAME";

const std::string iptv_local_playlist_provider_name		   = "IDS_IPTV_LOCAL_PLAYLIST_PROVIDER_NAME";

const std::string iptv_url_entry_provider_name		       = "IDS_SERVER_URL_ENTRY_PROVIDER_NAME";
const std::string iptv_url_entry_provider_desc		       = "IDS_SERVER_URL_ENTRY_PROVIDER_DESC";
const std::string iptv_url_entry_field_name		           = "IDS_SERVER_URL_ENTRY_FIELD_NAME";

const std::string iptv_hdhomerun_provider_name		       = "IDS_SERVER_HDHOMERUN_PROVIDER_NAME";
const std::string iptv_hdhomerun_provider_desc		       = "IDS_SERVER_HDHOMERUN_PROVIDER_DESC";
const std::string iptv_hdhomerun_addr_field_name		   = "IDS_SERVER_HDHOMERUN_ADDR_FIELD_NAME";

const std::string iptv_xtream_codes_provider_name		   = "IDS_SERVER_XTREAM_CODES_PROVIDER_NAME";
const std::string iptv_xtream_codes_provider_desc		   = "IDS_SERVER_XTREAM_CODES_PROVIDER_DESC";
const std::string iptv_xtream_codes_url_field_name		   = "IDS_SERVER_XTREAM_CODES_URL_FIELD_NAME";
const std::string iptv_xtream_codes_user_field_name		   = "IDS_SERVER_XTREAM_CODES_USER_FIELD_NAME";
const std::string iptv_xtream_codes_pswd_field_name		   = "IDS_SERVER_XTREAM_CODES_PSWD_FIELD_NAME";

const std::string iptv_no_group_transponder_name		   = "IDS_IPTV_NO_GROUP_TRANSPONDER_NAME";
const std::string iptv_all_channes_transponder_name		   = "IDS_IPTV_ALL_CHANNELS_TRANSPONDER_NAME";

const std::string iptv_parameters_container_name		= "IDS_SERVER_IPTV_PARAMS_CONTAINER_NAME";
const std::string iptv_parameters_container_desc		= "IDS_SERVER_IPTV_PARAMS_CONTAINER_DESC";

const std::string provider_tag_param_name		        = "IDS_SERVER_PROVIDER_TAG_PARAM_NAME";

const std::string device_params_container_name	            = "IDS_SERVER_DEVICE_PARAMS_CNTR_NAME";
const std::string ip_addr_param_name		                = "IDS_SERVER_IP_ADDR_PARAM_NAME";
const std::string ip_addr_auto_param_name		            = "IDS_SERVER_IP_ADDR_AUTO_PARAM_NAME";
const std::string prebuf_time_ms_param_name		            = "IDS_PREBUF_TIME_MS_PARAM_NAME";
const std::string xmltv_url_param_name		                = "IDS_IPTV_XMLTV_URL_PARAM_NAME";

const std::string xmltv_settings_container_name		        = "IDS_XMLTV_SETTINGS_CONTAINER_NAME";
const std::string xmltv_settings_container_desc		        = "IDS_XMLTV_SETTINGS_CONTAINER_DESC";
const std::string xmltv_settings_ul1_field_name		        = "IDS_XMLTV_SETTINGS_URL1_FIELD_NAME";
const std::string xmltv_settings_ul2_field_name		        = "IDS_XMLTV_SETTINGS_URL2_FIELD_NAME";
const std::string xmltv_settings_update_period_field_name		        = "IDS_XMLTV_SETTINGS_UPDATE_PERIOD_FIELD_NAME";

const std::string default_epg_source_name_name		        = "IDS_DEFAULT_EPG_SRC_NAME";

const std::string hide_new_channels_on_rescan_name		        = "IDS_HIDE_NEW_CHANNELS_ON_RESCAN";
const std::string rescan_settings_container_name		        = "IDS_RESCAN_SETTINGS_CONTAINER";

const std::string parameter_value_yes                   = "IDS_SERVER_SCAN_PARAMS_FTA_ONLY_YES";
const std::string parameter_value_no                    = "IDS_SERVER_SCAN_PARAMS_FTA_ONLY_NO";

const std::string ffmpeg_params_container_name	            = "IDS_SERVER_FFMPEG_PARAMS_CNTR_NAME";
const std::string ffmpeg_params_container_desc	            = "IDS_SERVER_FFMPEG_PARAMS_CNTR_DESC";
const std::string ffmpeg_default_profile_name	    = "IDS_SERVER_FFMPEG_DEFAULT_PROFILE_NAME";

const std::string iptv_pb_source_item_by_name		   = "IDS_IPTV_PB_SRC_ITEMS_BY_NAME";
const std::string iptv_pb_source_item_by_group		   = "IDS_IPTV_PB_SRC_ITEMS_BY_GROUP";
const std::string iptv_pb_source_all_items	   	       = "IDS_IPTV_PB_SRC_ALL_ITEMS";
const std::string IDS_IPTV_PB_SRC_VOD       	       = "IDS_IPTV_PB_SRC_VOD";

const std::string IDS_SERVER_LOGLEVEL_NONE	   	       = "IDS_SERVER_LOGLEVEL_NONE";
const std::string IDS_SERVER_LOGLEVEL_ERRORS  	       = "IDS_SERVER_LOGLEVEL_ERRORS";
const std::string IDS_SERVER_LOGLEVEL_INFO	   	       = "IDS_SERVER_LOGLEVEL_INFO";
const std::string IDS_SERVER_LOGLEVEL_EXTENDED	       = "IDS_SERVER_LOGLEVEL_EXTENDED";

const std::string IDS_SERVER_CODEPAGE_8859_1	       = "IDS_SERVER_CODEPAGE_8859_1";
const std::string IDS_SERVER_CODEPAGE_6937	       = "IDS_SERVER_CODEPAGE_6937";
const std::string IDS_SERVER_CODEPAGE_8859_2	       = "IDS_SERVER_CODEPAGE_8859_2";

const std::string IDS_SERVER_PATTERN_CHANNEL_NAME	       = "IDS_SERVER_PATTERN_CHANNEL_NAME";
const std::string IDS_SERVER_PATTERN_CHANNEL_NUM	       = "IDS_SERVER_PATTERN_CHANNEL_NUM";
const std::string IDS_SERVER_PATTERN_PROGRAM_NAME	       = "IDS_SERVER_PATTERN_PROGRAM_NAME";
const std::string IDS_SERVER_PATTERN_PROGRAM_SUBNAME	       = "IDS_SERVER_PATTERN_PROGRAM_SUBNAME";
const std::string IDS_SERVER_PATTERN_SEASON_EPISODE	       = "IDS_SERVER_PATTERN_SEASON_EPISODE";
const std::string IDS_SERVER_PATTERN_DATE_TIME	       = "IDS_SERVER_PATTERN_DATE_TIME";
const std::string IDS_SERVER_PATTERN_DATE	       = "IDS_SERVER_PATTERN_DATE";
const std::string IDS_SERVER_PATTERN_TIME	       = "IDS_SERVER_PATTERN_TIME";

const std::string IDS_SERVER_MARGIN_ON_TIME	       = "IDS_SERVER_MARGIN_ON_TIME";
const std::string IDS_SERVER_MARGIN_MIN_LATER	       = "IDS_SERVER_MARGIN_MIN_LATER";
const std::string IDS_SERVER_MARGIN_MIN_EARLIER	       = "IDS_SERVER_MARGIN_MIN_EARLIER";

const std::string IDS_SERVER_NEWONLY_NOT_SEEN_BEFORE	       = "IDS_SERVER_NEWONLY_NOT_SEEN_BEFORE";
const std::string IDS_SERVER_NEWONLY_REPEAT_FLAG	       = "IDS_SERVER_NEWONLY_REPEAT_FLAG";
const std::string IDS_SERVER_NEWONLY_PREMIERE_FLAG	       = "IDS_SERVER_NEWONLY_PREMIERE_FLAG";

const std::string IDS_SERVER_UNKNOWN_VALUE	       = "IDS_SERVER_UNKNOWN_VALUE";

const std::string IDS_SENDTO_COMSKIP_SETTINGS_NAME	       = "IDS_SENDTO_COMSKIP_SETTINGS_NAME";
const std::string IDS_SENDTO_COMSKIP_SETTINGS_DESC	       = "IDS_SENDTO_COMSKIP_SETTINGS_DESC";
const std::string IDS_SENDTO_COMSKIP_SETTINGS_PROFILE	       = "IDS_SENDTO_COMSKIP_SETTINGS_PROFILE";

const std::string IDS_CONNECT_TVRECORDINGS_SORT_BY = "IDS_CONNECT_TVRECORDINGS_SORT_BY";

const std::string IDS_GENRE_MOVIE = "IDS_GENRE_MOVIE";
const std::string IDS_GENRE_ACTION = "IDS_GENRE_ACTION";
const std::string IDS_GENRE_ADULT = "IDS_GENRE_ADULT";
const std::string IDS_GENRE_COMEDY = "IDS_GENRE_COMEDY";
const std::string IDS_GENRE_DOCUMENTARY = "IDS_GENRE_DOCUMENTARY";
const std::string IDS_GENRE_DRAMA = "IDS_GENRE_DRAMA";
const std::string IDS_GENRE_EDU = "IDS_GENRE_EDU";
const std::string IDS_GENRE_HORROR = "IDS_GENRE_HORROR";
const std::string IDS_GENRE_KIDS = "IDS_GENRE_KIDS";
const std::string IDS_GENRE_MUSIC = "IDS_GENRE_MUSIC";
const std::string IDS_GENRE_NEWS = "IDS_GENRE_NEWS";
const std::string IDS_GENRE_REALITY = "IDS_GENRE_REALITY";
const std::string IDS_GENRE_ROMANCE = "IDS_GENRE_ROMANCE";
const std::string IDS_GENRE_SCIFI = "IDS_GENRE_SCIFI";
const std::string IDS_GENRE_SERIAL = "IDS_GENRE_SERIAL";
const std::string IDS_GENRE_SOAP = "IDS_GENRE_SOAP";
const std::string IDS_GENRE_SPECIAL = "IDS_GENRE_SPECIAL";
const std::string IDS_GENRE_SPORT = "IDS_GENRE_SPORT";
const std::string IDS_GENRE_THRILLER = "IDS_GENRE_THRILLER";

const std::string IDS_SERVER_NEWONLY_DEFAULT_NOT_SET = "IDS_SERVER_NEWONLY_DEFAULT_NOT_SET";
const std::string IDS_SERVER_NEWONLY_DEFAULT_TRUE = "IDS_SERVER_NEWONLY_DEFAULT_TRUE";
const std::string IDS_SERVER_NEWONLY_DEFAULT_FALSE = "IDS_SERVER_NEWONLY_DEFAULT_FALSE";


} //dvblex
