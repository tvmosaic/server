/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

enum ETS_BANDWIDTH_TYPES
{
	ETS_BT_8MHZ = 0,
	ETS_BT_7MHZ
};

enum ETS_CONSTELLATION_TYPES
{
	ETS_CT_QPSK = 0,
	ETS_CT_16QAM,
	ETS_CT_64QAM
};

enum ETS_HIERARCHY_INFO_TYPES
{
	ETS_HIT_NON_HIERARCHICAL = 0,
	ETS_HIT_ALPHA_1,
	ETS_HIT_ALPHA_2,
	ETS_HIT_ALPHA_4
};

enum ETS_CODE_RATE_TYPES
{
	ETS_CR_12 = 0,
	ETS_CR_23,
	ETS_CR_34,
	ETS_CR_56,
	ETS_CR_78
};

enum ETS_GUARD_INTERVAL_TYPES
{
	ETS_GI_132 = 0,
	ETS_GI_116,
	ETS_GI_18,
	ETS_GI_14
};

enum ETS_TRANSMISSION_MODE_TYPES
{
	ETS_TM_2K = 0,
	ETS_TM_8K
};
