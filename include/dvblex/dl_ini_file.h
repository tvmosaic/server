/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_INI_FILE_H_
#define __DVBLINK_DL_INI_FILE_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <string>
#include <vector>
#include <map>

namespace dvblink { namespace engine {

struct SDLIniFileLine
{
    std::string key;
    std::string value;
};

typedef std::vector<SDLIniFileLine> TDLIniFileLines;
typedef std::map<std::string, TDLIniFileLines> TDLIniFileTree;

class ini_file_reader
{
public:
    ini_file_reader(){};
    ~ini_file_reader(){};

    bool read_ini_file(const wchar_t* fpath, TDLIniFileTree& contents);
    bool read_ini_file(const char* fpath, TDLIniFileTree& contents);
protected:
	void adjust_ini_string_for_comment(std::string& ini_string);
};

} //settings
} //dvblink

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_INI_FILE_H_
