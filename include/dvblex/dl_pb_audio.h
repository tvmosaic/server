/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>
#include <boost/lexical_cast.hpp>
#include <dl_pb_item.h>
#include <dl_pugixml_helper.h>

namespace dvblex { namespace playback {

struct pb_audio_t : public pb_item_t
{
    pb_audio_t() :
      pb_item_t(pbit_item_audio) { }
};

const std::string audio_root_node                    = "audio";

inline void write_to_node(pugi::xml_node& node, const pb_audio_t& audio)
{
    pugi::xml_node a_node = dvblink::pugixml_helpers::new_child(node, audio_root_node);
    if (a_node != NULL)
    {
        write_to_node(a_node, (pb_item_t)audio);
    }
}

inline void read_from_node(pugi::xml_node& node, pb_audio_t& audio)
{
    read_from_node(node, (pb_item_t&)audio);
}


} // playback
} // dvblex

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::playback::pb_audio_t& pa, const unsigned int /*version*/)
{
    ar & pa.object_id_;
    ar & pa.parent_id_;
    ar & pa.url_;
    ar & pa.thumbnail_;
    ar & pa.item_type_;
    ar & pa.can_be_deleted_;
    ar & pa.size_;
    ar & pa.creation_time_;
}

} // namespace serialization
} // namespace boost
