/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>
#include <vector>
#include <map>

#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>

#include <dl_types.h>
#include <dl_filesystem_path.h>

namespace dvblink { namespace transcoder {

// transcoding use-cases

const std::string tu_generate_thumb_from_file_id  = "thumb_from_file";
const std::string tu_transcode_live_to_ts_id  = "transcode_live_to_ts";
const std::string tu_transcode_live_to_hls_id  = "transcode_live_to_hls";
const std::string tu_transcode_recording_to_hls_id  = "transcode_recoding_to_hls";
const std::string tu_transcode_sendto_transcode_id  = "sendto_transcode";
const std::string tu_transcode_live_to_mp4_id  = "transcode_live_to_mp4";

const std::string tu_generate_thumb_from_file_name  = "IDS_SERVER_TU_THUMB_FROM_FILE";
const std::string tu_transcode_live_to_ts_name  = "IDS_SERVER_TU_LIVE_TO_TS";
const std::string tu_transcode_live_to_hls_name  = "IDS_SERVER_TU_LIVE_TO_HLS";
const std::string tu_transcode_recording_to_hls_name  = "IDS_SERVER_TU_RECORDING_TO_HLS";
const std::string tu_transcode_sendto_transcode_name  = "IDS_SERVER_TU_SENDTO_TRANSCODE";
const std::string tu_transcode_live_to_mp4_name  = "IDS_SERVER_TU_LIVE_TO_MP4";

struct transcoder_usecase_t
{
    dvblink::tc_usecase_id_t id_;
    dvblink::tc_usecase_name_t name_;
};

typedef std::vector<dvblink::tc_usecase_id_t> transcoder_usecase_id_list_t;

typedef std::vector<transcoder_usecase_t> transcoder_usecase_list_t;

void get_transcoder_usecase_list(transcoder_usecase_list_t& tu_list);

/////////////////////////////////////////////////////////////////////////////////////////////////

//transcoding parameters
const std::string ffmpeg_condition_dimensions  = "dimensions";
const std::string ffmpeg_condition_bitrate  = "bitrate";
const std::string ffmpeg_condition_scale  = "scale";

// conditions
typedef std::map<dvblink::launch_condition_t, dvblink::launch_cnd_value_t> ffmpeg_param_condition_map_t;  //condition -> value

//ffmpeg parameter template (conditions + transcoding parameters)

struct ffmpeg_param_template_t
{
    dvblink::launch_param_t param_;
    ffmpeg_param_condition_map_t conditions_;
};

typedef std::vector<ffmpeg_param_template_t> ffmpeg_param_template_list_t;

//environment variavles
typedef std::vector<dvblink::env_var_t> ffmpeg_env_var_list_t;

//ffmpeg launch parameters
struct ffmpeg_launch_params_t
{
    ffmpeg_launch_params_t()
        : is_transport_stream_(true), mime_("video/mpeg")
    {}

    dvblink::filesystem_path_t ffmpeg_dir_;
    dvblink::filesystem_path_t ffmpeg_exepath_;
    ffmpeg_env_var_list_t ffmpeg_env_;
    ffmpeg_param_template_list_t arguments_;
    dvblink::bool_flag_t is_transport_stream_;
    dvblink::tc_usecase_mime_t mime_;
};

//template init parameters

const boost::int32_t ffmpeg_param_invalid_dimension = -1;
const boost::int32_t ffmpeg_param_invalid_bitrate = -1;
const boost::int32_t ffmpeg_param_invalid_offset_time = -1;

struct ffmpeg_param_template_init_t
{
    ffmpeg_param_template_init_t() :
        width_(ffmpeg_param_invalid_dimension),
        height_(ffmpeg_param_invalid_dimension),
        bitrate_kbits_(ffmpeg_param_invalid_bitrate),
        offset_sec_(ffmpeg_param_invalid_offset_time),
        video_scale_(ffmpeg_param_invalid_dimension)
        {}

    std::string src_;
    std::string dest_;
    boost::int32_t width_;
    boost::int32_t height_;
    boost::int32_t bitrate_kbits_;
    boost::int32_t offset_sec_;
    boost::int32_t video_scale_;
    std::vector<dvblink::launch_param_t> extra_params;
};

bool init_ffmpeg_launch_arguments(const ffmpeg_param_template_list_t& template_arguments, 
    const ffmpeg_param_template_init_t& init_info, std::vector<dvblink::launch_param_t>& arguments);

} //transcoder
} //dvblink

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblink::transcoder::ffmpeg_param_template_t& ffpt, const unsigned int /*version*/)
{
    ar & ffpt.param_;
    ar & ffpt.conditions_;
}

template<class Archive>
void serialize(Archive& ar, dvblink::transcoder::ffmpeg_launch_params_t& fflp, const unsigned int /*version*/)
{
    ar & fflp.ffmpeg_dir_;
    ar & fflp.ffmpeg_exepath_;
    ar & fflp.ffmpeg_env_;
    ar & fflp.arguments_;
    ar & fflp.is_transport_stream_;
    ar & fflp.mime_;
}

} // namespace serialization
} // namespace boost

