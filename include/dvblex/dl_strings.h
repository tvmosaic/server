/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_STRINGS_H_
#define __DVBLINK_DL_STRINGS_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <wchar.h>
#include <string>
#include <boost/lexical_cast.hpp>

#ifdef _WIN32
#include <objbase.h>
#else
#include <dl_iconv_helper.h>
#endif

namespace dvblink { namespace engine {

#ifdef _WIN32

enum ECodepage
{
    EC_UTF8 = CP_UTF8,
    EC_ISO_8859_1 = 28591,
    EC_ISO_8859_2 = 28592,
    EC_ISO_8859_3 = 28593,
    EC_ISO_8859_4 = 28594,
    EC_ISO_8859_5 = 28595,
    EC_ISO_8859_6 = 28596,
    EC_ISO_8859_7 = 28597,
    EC_ISO_8859_8 = 28598,
    EC_ISO_8859_9 = 28599,
    EC_ISO_8859_11 = 874,
    EC_ISO_8859_13 = 28603,
    EC_ISO_8859_15 = 28605,
    EC_ISO_6937 = 20269,
    EC_KS_C_5601_1987 = 949,
    EC_X_CP20936 = 20936, //simplified chinese (GB2312)
    EC_BIG5 = 950,
    EC_UTF16_BE = 1200,
    EC_KOI8_R = 20866,
    EC_CP_1252 = 1252
};

#else

enum ECodepage
{
    EC_UTF8 = dvblink::iconv_helper::CPID_UTF8,
    EC_ISO_8859_1 = dvblink::iconv_helper::CPID_8859_1,
    EC_ISO_8859_2 = dvblink::iconv_helper::CPID_8859_2,
    EC_ISO_8859_3 = dvblink::iconv_helper::CPID_8859_3,
    EC_ISO_8859_4 = dvblink::iconv_helper::CPID_8859_4,
    EC_ISO_8859_5 = dvblink::iconv_helper::CPID_8859_5,
    EC_ISO_8859_6 = dvblink::iconv_helper::CPID_8859_6,
    EC_ISO_8859_7 = dvblink::iconv_helper::CPID_8859_7,
    EC_ISO_8859_8 = dvblink::iconv_helper::CPID_8859_8,
    EC_ISO_8859_9 = dvblink::iconv_helper::CPID_8859_9,
    EC_ISO_8859_11 = dvblink::iconv_helper::CPID_8859_11,
    EC_ISO_8859_13 = dvblink::iconv_helper::CPID_8859_13,
    EC_ISO_8859_15 = dvblink::iconv_helper::CPID_8859_15,
    EC_ISO_6937 = dvblink::iconv_helper::CPID_6937,
    EC_KS_C_5601_1987 = dvblink::iconv_helper::CPID_KS_C_5601_1987,
    EC_X_CP20936 = dvblink::iconv_helper::CPID_X_CP20936, //simplified chinese (GB2312)
    EC_BIG5 = dvblink::iconv_helper::CPID_BIG5,
    EC_UTF16_BE = dvblink::iconv_helper::CPID_UTF16_BE,
    EC_KOI8_R = dvblink::iconv_helper::CPID_KOI8_R,
    EC_CP_1252 = dvblink::iconv_helper::CPID_CP_1252
};

#endif

bool ConvertMultibyteToUC(ECodepage codepage, const char* src, int src_len, std::wstring& dest);
bool ConvertMultibyteToUC(ECodepage codepage, const char* src, std::wstring& dest);
bool ConvertUCToMultibyte(ECodepage codepage, const wchar_t* src, std::string& dest);

bool IsStringEmpty(const wchar_t* str_to_test);
bool IsStringEmpty(const char* str_to_test);
void SubstSpecialChars(std::wstring& str, const wchar_t* str_to_subst, const wchar_t* subst_str);
void RemoveIllegalXMLCharacters(std::wstring& str);
void RemoveIllegalXMLCharacters(std::string& str);

template<dvblink::engine::ECodepage page>
inline std::wstring string_cast(const std::string& arg)
{
    std::wstring res;
    dvblink::engine::ConvertMultibyteToUC(page, arg.c_str(), static_cast<int>(arg.size()), res);
    return res;
}

template<dvblink::engine::ECodepage page>
inline std::string string_cast(const std::wstring& arg)
{
    std::string res;
    dvblink::engine::ConvertUCToMultibyte(page, arg.c_str(), res);
    return res;
}

struct string_conv
{
    template <typename CHAR_TYPE, typename RESULT>
    static void apply(const CHAR_TYPE* value, RESULT& result, RESULT default_value)
    {
        try
        {
            result = boost::lexical_cast<RESULT>(value);
        }
        catch (boost::bad_lexical_cast& /*e*/)
        {
            result = default_value;
        }
    }

    template <typename THROUGH, typename RESULT, typename CHAR_TYPE>
    static void apply(const CHAR_TYPE* value, RESULT& result, RESULT default_value)
    {
        try
        {
            result = static_cast<RESULT>(boost::lexical_cast<THROUGH>(value));
        }
        catch (boost::bad_lexical_cast& /*e*/)
        {
            result = default_value;
        }
    }

    template <typename INPUT, typename RESULT>
    static RESULT to_string(const INPUT& value)
    {
        RESULT result;
        try
        {
            result = boost::lexical_cast<RESULT>(value);
        }
        catch (boost::bad_lexical_cast& /*e*/)
        {
            result.clear();
        }
        return result;
    }
};


struct isequal
{
    template <typename T> bool operator() (T l, T r) const
    {
        return std::tolower(l) == std::tolower(r);
    }
};

template <typename T>
bool istrncmp(const T* s1, const T* s2, size_t n)
{
    return std::equal(s1, s1 + n, s2, isequal());
}


} //engine
} //dvblink

///////////////////////////////////////////////////////////////////////////////
#endif
