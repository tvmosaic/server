/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_types.h>
#include <dl_message.h>
#include <dl_pb_object.h>
#include <dl_pb_source.h>

namespace dvblink { namespace messaging { namespace playback {

//
// register playback source
//
struct register_pb_source_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct register_pb_source_request : public message_send<register_pb_source_request, register_pb_source_response>
{
    register_pb_source_request(){}
    register_pb_source_request(const dvblex::playback::pb_source_t& pb_source) :
        pb_source_(pb_source)
        {}

        dvblex::playback::pb_source_t pb_source_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & pb_source_;
    }
};

//
// unregister playback source
//
struct unregister_pb_source_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct unregister_pb_source_request : public message_send<unregister_pb_source_request, unregister_pb_source_response>
{
    unregister_pb_source_request(){}
    unregister_pb_source_request(const dvblink::pb_source_id_t& id) :
        id_(id)
        {}

    dvblink::pb_source_id_t id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & id_;
    }
};

//
// open_item
//
struct open_item_response
{
    object_handle_t handle_;
    boost::uint64_t item_size_bytes_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & item_size_bytes_;
        ar & handle_;
        ar & result_;
    } 
};

struct open_item_request : public message_send<open_item_request, open_item_response>
{
    open_item_request() {}
    open_item_request(const object_id_t& object_id) : object_id_(object_id) {}
    object_id_t object_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & object_id_;
    } 
};

//
// seek_item
//
struct seek_item_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & result_;
    } 
};

struct seek_item_request : public message_send<seek_item_request, seek_item_response>
{
    seek_item_request() {}
    seek_item_request(const object_handle_t& handle, boost::uint64_t offset) : handle_(handle), offset_(offset) {}
    object_handle_t handle_;
    boost::uint64_t offset_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & handle_;
        ar & offset_;
    } 
};

//
// close_item
//
struct close_item_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & result_;
    } 
};

struct close_item_request : public message_send<close_item_request, close_item_response>
{
    close_item_request() {}
    close_item_request(const object_handle_t& handle) : handle_(handle) {}
    object_handle_t handle_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & handle_;
    } 
};

//
// get objects
//
struct get_objects_response
{
    dvblex::playback::pb_object_t object_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & object_;
        ar & result_;
    } 
};

struct get_objects_request : public message_send<get_objects_request, get_objects_response>
{
    get_objects_request():
      object_type_(dvblex::playback::pot_object_unknown), item_type_(dvblex::playback::pbit_item_unknown),
      start_position_(dvblex::playback::object_start_position), requested_count_(dvblex::playback::object_count_all), 
      is_children_request_(false), proto_("http")
      {}

    dvblink::object_id_t object_id_;
    dvblex::playback::pb_object_type_e object_type_;
    dvblex::playback::pb_item_type_e item_type_;
    boost::int32_t start_position_;
    boost::int32_t requested_count_;
    bool is_children_request_;

    dvblink::url_address_t server_address_;
    dvblink::url_proto_t proto_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & object_id_;
        ar & object_type_;
        ar & item_type_;
        ar & start_position_;
        ar & requested_count_;
        ar & is_children_request_;
        ar & server_address_;
        ar & proto_;
    } 
};

//
// get objects
//
struct search_objects_response
{
    dvblex::playback::pb_object_t object_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & object_;
        ar & result_;
    } 
};

struct search_objects_request : public message_send<search_objects_request, search_objects_response>
{
    search_objects_request():
      object_type_(dvblex::playback::pot_object_unknown), item_type_(dvblex::playback::pbit_item_unknown),
      proto_("http")
      {}

    dvblink::object_id_t object_id_;
    dvblex::playback::pb_object_type_e object_type_;
    dvblex::playback::pb_item_type_e item_type_;
    dvblink::search_string_t search_string_;

    dvblink::url_address_t server_address_;
    dvblink::url_proto_t proto_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & object_id_;
        ar & object_type_;
        ar & item_type_;
        ar & search_string_;
        ar & server_address_;
        ar & proto_;
    } 
};

//
// remove_object
//
struct remove_object_response
{
    bool result_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    } 
};

struct remove_object_request : public message_send<remove_object_request, remove_object_response>
{
    remove_object_request() {}
    remove_object_request(const dvblink::object_id_t& object_id) : object_id_(object_id) {}
    dvblink::object_id_t object_id_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & object_id_;
    } 
};

//
// get source object container
//
struct get_source_container_response
{
    dvblex::playback::pb_container_t container_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & container_;
        ar & result_;
    } 
};

struct get_source_container_request : public message_send<get_source_container_request, get_source_container_response>
{
    get_source_container_request() :
        proto_("http")
    {}

    get_source_container_request(const dvblink::url_address_t& server_address, const dvblink::url_proto_t& proto) : 
        server_address_(server_address), proto_(proto) {}
    dvblink::url_address_t server_address_;
    dvblink::url_proto_t proto_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & server_address_;
        ar & proto_;
    } };

//
// stop recording
//
struct stop_recording_response
{
    bool result_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    } 
};

struct stop_recording_request : public message_send<stop_recording_request, stop_recording_response>
{
    stop_recording_request() {}
    stop_recording_request(const dvblink::object_id_t& object_id) : object_id_(object_id) {}
    dvblink::object_id_t object_id_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & object_id_;
    } 
};

//
// get_local_object_info
//
struct local_object_info_response
{
	dvblink::filesystem_path_t file_path_;
	bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & file_path_;
        ar & result_;
    } 
};

struct local_object_info_request : public message_send<local_object_info_request, local_object_info_response>
{
    local_object_info_request() {}
    local_object_info_request(const dvblink::object_id_t& object_id) : object_id_(object_id) {}
    dvblink::object_id_t object_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & object_id_;
    } 
};

//
// get_playback_streaming_port
//
struct get_playback_streaming_port_response
{
	dvblink::network_port_t port_;
	dvblink::network_port_t https_port_;
	bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & port_;
        ar & https_port_;
        ar & result_;
    } 
};

struct get_playback_streaming_port_request : public message_send<get_playback_streaming_port_request, get_playback_streaming_port_response>
{
};

//
// set resume position
//
struct set_pb_resume_info_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    }
};

struct set_pb_resume_info_request : public message_send<set_pb_resume_info_request, set_pb_resume_info_response>
{
    set_pb_resume_info_request(){}
    set_pb_resume_info_request(const dvblink::object_id_t& object_id, const boost::int32_t& pos) :
        object_id_(object_id), pos_(pos)
        {}

        dvblink::object_id_t object_id_;
        boost::int32_t pos_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & object_id_;
        ar & pos_;
    }
};

//
// get resume position
//
struct get_pb_resume_info_response
{
    bool result_;
    boost::int32_t pos_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
        ar & pos_;
    }
};

struct get_pb_resume_info_request : public message_send<get_pb_resume_info_request, get_pb_resume_info_response>
{
    get_pb_resume_info_request(){}
    get_pb_resume_info_request(const dvblink::object_id_t& object_id) :
        object_id_(object_id)
        {}

        dvblink::object_id_t object_id_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & object_id_;
    }
};


} //playback
} //messaging
} //dvblink
