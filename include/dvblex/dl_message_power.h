/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_types.h>
#include <dl_message.h>

namespace dvblink { namespace messaging { namespace power {

//
// add wake-up timer
//
struct add_wakeup_time_response
{
    add_wakeup_time_response() : result_(false) {}
    add_wakeup_time_response(bool result) : result_(result) {}
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & result_;
    } 
};

struct add_wakeup_time_request : public message_send<add_wakeup_time_request, add_wakeup_time_response>
{
    add_wakeup_time_request() {}
    add_wakeup_time_request(const client_id_t& client_id, time_t wakeup_time) : client_id_(client_id), wakeup_time_(wakeup_time) {}
    client_id_t client_id_;
    time_t wakeup_time_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & client_id_;
        ar & wakeup_time_;
    } 
};

//
// remove wake-up timer
//
struct remove_wakeup_time_response
{
    remove_wakeup_time_response() : result_(false) {}
    remove_wakeup_time_response(bool result) : result_(result) {}
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & result_;
    } 
};

struct remove_wakeup_time_request : public message_send<remove_wakeup_time_request, remove_wakeup_time_response>
{
    remove_wakeup_time_request() {}
    remove_wakeup_time_request(const client_id_t& client_id) : client_id_(client_id) {}
    client_id_t client_id_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & client_id_;
    } 
};

//
// get wake-up timer
//
struct get_wakeup_time_response
{
    get_wakeup_time_response() : result_(false) {}
    get_wakeup_time_response(bool result, time_t wakeup_time) : result_(result), wakeup_time_(wakeup_time) {}
    bool result_;
    time_t wakeup_time_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & result_;
        ar & wakeup_time_;
    } 
};

struct get_wakeup_time_request : public message_send<get_wakeup_time_request, get_wakeup_time_response>
{
    get_wakeup_time_request() {}
    get_wakeup_time_request(const client_id_t& client_id) : client_id_(client_id) {}
    client_id_t client_id_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & client_id_;
    } 
};

//
// enable standby
//
struct enable_standby_response
{
    enable_standby_response() : result_(false) {}
    enable_standby_response(bool result) : result_(result) {}
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & result_;
    } 
};

struct enable_standby_request : public message_send<enable_standby_request, enable_standby_response>
{
    enable_standby_request() {}
    enable_standby_request(const client_id_t& client_id) : client_id_(client_id) {}
    client_id_t client_id_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & client_id_;
    } 
};

//
// disable standby
//
struct disable_standby_response
{
    disable_standby_response() : result_(false) {}
    disable_standby_response(bool result) : result_(result) {}
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    { 
        ar & result_;
    } 
};

struct disable_standby_request : public message_send<disable_standby_request, disable_standby_response>
{
    disable_standby_request() {}
    disable_standby_request(const client_id_t& client_id) : client_id_(client_id) {}
    client_id_t client_id_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & client_id_;
    } 
};

} //power
} //messaging
} //dvblink
