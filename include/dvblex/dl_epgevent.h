/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <boost/serialization/string.hpp>
#include <vector>
#include <map>
#include <string>
#include <dl_types.h>
#include <dl_filesystem_path.h>

namespace pugi {
    class xml_node;
}

namespace dvblink { namespace engine {

struct DLEPGEvent
{
    DLEPGEvent()
    {
        m_IsAction = m_IsComedy = m_IsDocumentary = m_IsDrama = m_IsEducational = m_IsHorror = m_IsKids = false;
        m_IsMovie = m_IsMusic = m_IsNews = m_IsReality = m_IsRomance = m_IsScienceFiction = m_IsSerial = false;
        m_IsSoap = m_IsSpecial = m_IsSports = m_IsThriller = m_IsAdult = false;
        m_Year = m_EpisodeNum = m_SeasonNum = m_StarNum = m_StarNumMax = 0;
        m_IsHDTV = m_IsPremiere = m_IsRepeatFlag = false;
        m_StartTime = m_Duration = 0;

    };
    std::string id_;
    std::string m_Name;
    std::string m_ShortDesc;
    time_t m_StartTime;
    time_t m_Duration;
    //new fields
    std::string m_SecondName;
    std::string m_Language;
    std::string m_Actors;
    std::string m_Directors;
    std::string m_Writers;
    std::string m_Producers;
    std::string m_Guests;
    bool m_IsAction;
    bool m_IsComedy;
    bool m_IsDocumentary;
    bool m_IsDrama;
    bool m_IsEducational;
    bool m_IsHorror;
    bool m_IsKids;
    bool m_IsMovie;
    bool m_IsMusic;
    bool m_IsNews;
    bool m_IsReality;
    bool m_IsRomance;
    bool m_IsScienceFiction;
    bool m_IsSerial;
    bool m_IsSoap;
    bool m_IsSpecial;
    bool m_IsSports;
    bool m_IsThriller;
    bool m_IsAdult;
    std::string m_ImageURL;
    long m_Year;
    long m_EpisodeNum;
    long m_SeasonNum;
    long m_StarNum;
    long m_StarNumMax;
    std::string m_Categories;
    bool m_IsHDTV;
    bool m_IsPremiere;
    bool m_IsRepeatFlag;
};

typedef std::vector<DLEPGEvent> DLEPGEventList;
typedef std::map<channel_id_t, DLEPGEventList> map_channel_to_epg_t;
typedef std::map<epg_channel_id_t, DLEPGEventList> map_epg_channel_to_epg_t;

bool EPGWriteEventsToXML(const DLEPGEventList& epgEventList, std::string& programs_xml);
bool EPGReadEventsFromXML(const std::string& programs_xml, DLEPGEventList& epgEventList);

void PostProcessEPGEvents(DLEPGEventList& epgEventList);

bool EPGWriteEventsToFile(DLEPGEventList& epgEventList, const dvblink::filesystem_path_t& fname);
bool EPGReadEventsFromFile(const dvblink::filesystem_path_t& fname, DLEPGEventList& epgEventList);

void EPGAddEventsToXMLDocument(const DLEPGEventList& eventList, pugi::xml_node& root_node);
void serialize(const DLEPGEvent& event, pugi::xml_node& program_node);
void deserialize(const pugi::xml_node& program_node, DLEPGEvent& epg_event);

}
}

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblink::engine::DLEPGEvent& e, const unsigned int /*version*/)
{
    ar & e.id_;
    ar & e.m_Name;
    ar & e.m_ShortDesc;
    ar & e.m_StartTime;
    ar & e.m_Duration;

    ar & e.m_SecondName;
    ar & e.m_Language;
    ar & e.m_Actors;
    ar & e.m_Directors;
    ar & e.m_Writers;
    ar & e.m_Producers;
    ar & e.m_Guests;
    ar & e.m_IsAction;
    ar & e.m_IsComedy;
    ar & e.m_IsDocumentary;
    ar & e.m_IsDrama;
    ar & e.m_IsEducational;
    ar & e.m_IsHorror;
    ar & e.m_IsKids;
    ar & e.m_IsMovie;
    ar & e.m_IsMusic;
    ar & e.m_IsNews;
    ar & e.m_IsReality;
    ar & e.m_IsRomance;
    ar & e.m_IsScienceFiction;
    ar & e.m_IsSerial;
    ar & e.m_IsSoap;
    ar & e.m_IsSpecial;
    ar & e.m_IsSports;
    ar & e.m_IsThriller;
    ar & e.m_IsAdult;
    ar & e.m_ImageURL;
    ar & e.m_Year;
    ar & e.m_EpisodeNum;
    ar & e.m_SeasonNum;
    ar & e.m_StarNum;
    ar & e.m_StarNumMax;
    ar & e.m_Categories;
    ar & e.m_IsHDTV;
    ar & e.m_IsPremiere;
    ar & e.m_IsRepeatFlag;
}

}
}
