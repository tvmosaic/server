/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <sstream>
#include <dl_types.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_base64.h>
#include <dl_filesystem_path.h>
#include <dl_pugixml_helper.h>
#include <dl_parameters.h>
#include <dl_parameters_serializer.h>

namespace dvblex { 

const std::string get_server_params_cmd              = "get_server_params";
const std::string set_server_params_cmd              = "set_server_params";
const std::string get_server_log_cmd                 = "get_server_log";
const std::string get_directory_tree_cmd             = "get_directory_tree";
const std::string do_installation_backup_cmd         = "do_installation_backup";
const std::string do_installation_restore_cmd        = "do_installation_restore";
const std::string get_server_directory_cmd           = "get_server_directory";
const std::string get_transcoding_params_cmd         = "get_transcoding_params";
const std::string set_transcoding_params_cmd         = "set_transcoding_params";

struct server_params
{
    server_params() :
        log_level_(dvblink::logging::log_level_errors_and_warnings),
        code_page_(dvblink::EDDC_ISO_8859_1)
    {}

    dvblink::logging::e_log_level log_level_;
    dvblink::EDVBDefaultCodepage code_page_;
};

const std::string server_params_root_node              = "server_params";
const std::string server_params_loglevel_node          = "log_level";
const std::string server_params_codepage_node          = "codepage";

inline pugi::xml_node& operator >> (pugi::xml_node& node, server_params& ss)
{
    if (NULL != node)
    {       
        std::string str;
        boost::uint32_t value;

        if (dvblink::pugixml_helpers::get_node_value(node, server_params_loglevel_node, str))
        {
            dvblink::engine::string_conv::apply<boost::uint32_t, boost::uint32_t, char>(str.c_str(), value, (boost::uint32_t)dvblink::logging::log_level_errors_and_warnings);
            ss.log_level_ = (dvblink::logging::e_log_level)value;
        }

        if (dvblink::pugixml_helpers::get_node_value(node, server_params_codepage_node, str))
        {
            dvblink::engine::string_conv::apply<boost::uint32_t, boost::uint32_t, char>(str.c_str(), value, (boost::uint32_t)dvblink::EDDC_ISO_8859_1);
            ss.code_page_ = (dvblink::EDVBDefaultCodepage)value;
        }
    }
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const server_params& ss)
{
    pugi::xml_node node = doc.append_child(server_params_root_node.c_str());
    if (node != NULL)
    {
        std::stringstream buf;

        buf.clear(); buf.str("");
        buf << (boost::uint32_t)ss.log_level_;
        dvblink::pugixml_helpers::new_child(node, server_params_loglevel_node, buf.str());

        buf.clear(); buf.str("");
        buf << (boost::uint32_t)ss.code_page_;
        dvblink::pugixml_helpers::new_child(node, server_params_codepage_node, buf.str());
    }
    return doc;
}

///////////////////////////////////////////////////////////////////////////////

struct server_binary_data_param
{
    server_binary_data_param()
    {}

    server_binary_data_param(const std::vector<boost::uint8_t>& data, const std::string& mime) :
        mime_(mime)
    {
        dvblink::base64_encode_for_url(data, log_);
    }

    void get_binary_data(std::vector<boost::uint8_t>& data) const
    {
        dvblink::base64_decode_for_url<std::vector<boost::uint8_t>, char>(log_, data);
    }

    std::string log_;
    std::string mime_;
};

const std::string server_log_root_node          = "binary_data";
const std::string server_log_data_node          = "data";
const std::string server_log_mime_node          = "mime";

inline pugi::xml_node& operator >> (pugi::xml_node& node, server_binary_data_param& lr)
{
    if (NULL != node)
    {       
        dvblink::pugixml_helpers::get_node_value(node, server_log_mime_node, lr.mime_);
        dvblink::pugixml_helpers::get_node_value(node, server_log_data_node, lr.log_);
    }
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const server_binary_data_param& lr)
{
    pugi::xml_node node = doc.append_child(server_log_root_node.c_str());
    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, server_log_mime_node, lr.mime_);
        dvblink::pugixml_helpers::new_child(node, server_log_data_node, lr.log_);
    }
    return doc;
}

///////////////////////////////////////////////////////////////////////////////

struct directory_tree_request
{
    dvblink::filesystem_path_t path_;
};

struct directory_tree_response
{
    dvblink::filesystem_path_t path_;
    std::vector<dvblink::filesystem_path_t> tree_;
};

const std::string directory_tree_root_node          = "directory_tree";
const std::string directory_tree_path_node          = "path";
const std::string directory_tree_tree_node          = "tree";

inline pugi::xml_document& operator << (pugi::xml_document& doc, const directory_tree_request& lr)
{
    pugi::xml_node node = doc.append_child(directory_tree_root_node.c_str());
    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, directory_tree_path_node, lr.path_.to_string());
    }
    return doc;
}

inline pugi::xml_node& operator >> (pugi::xml_node& node, directory_tree_request& lr)
{
    if (node != NULL && node.name() == directory_tree_root_node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, directory_tree_path_node, str))
            lr.path_ = str;
    }
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const directory_tree_response& lr)
{
    pugi::xml_node node = doc.append_child(directory_tree_root_node.c_str());
    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, directory_tree_path_node, lr.path_.to_string());
        pugi::xml_node tree_node = node.append_child(directory_tree_tree_node.c_str());
        for (size_t i = 0; i < lr.tree_.size(); ++i)
        {
            dvblink::pugixml_helpers::new_child(tree_node, directory_tree_path_node.c_str(), lr.tree_[i].to_string().c_str());
        }
    }
    return doc;
}

inline pugi::xml_node& operator >> (pugi::xml_node& node, directory_tree_response& lr)
{
    if (NULL != node && node.name() == directory_tree_root_node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, directory_tree_path_node, str))
            lr.path_ = str;

        pugi::xml_node tree_node = node.child(directory_tree_tree_node.c_str());
        if (tree_node)
        {
            pugi::xml_node path_node = tree_node.first_child();
            while (path_node && std::string(path_node.name()) == directory_tree_path_node)
            {
                lr.tree_.push_back(dvblink::filesystem_path_t(path_node.child_value()));
                path_node = path_node.next_sibling();
            }
        }
    }
    return node;
}

//  get server directory
enum server_directory_type_e
{
    sdt_shared_root,
    sdt_private_root,
    sdt_temp_directory
};
struct get_server_directory_request
{
    server_directory_type_e directory_type_;
};

struct get_server_directory_response
{
    dvblink::filesystem_path_t path_;
};


const std::string get_server_directory_root_node          = "server_directory";
const std::string get_server_directory_type_node          = "type";
const std::string get_server_directory_path_node          = "path";

inline pugi::xml_document& operator << (pugi::xml_document& doc, const get_server_directory_request& dr)
{
    pugi::xml_node node = doc.append_child(get_server_directory_root_node.c_str());
    if (node != NULL)
    {
        std::stringstream buf;

        buf.clear(); buf.str("");
        buf << (boost::uint32_t)dr.directory_type_;
        dvblink::pugixml_helpers::new_child(node, get_server_directory_type_node, buf.str());
    }
    return doc;
}

inline pugi::xml_node& operator >> (pugi::xml_node& node, get_server_directory_request& dr)
{
    if (node != NULL)
    {
        std::string str;
        boost::uint32_t value;

        if (dvblink::pugixml_helpers::get_node_value(node, get_server_directory_type_node, str))
        {
            dvblink::engine::string_conv::apply<boost::uint32_t, boost::uint32_t, char>(str.c_str(), value, (boost::uint32_t)sdt_shared_root);
            dr.directory_type_ = (server_directory_type_e)value;
        }
    }
    return node;
}

inline pugi::xml_document& operator << (pugi::xml_document& doc, const get_server_directory_response& dr)
{
    pugi::xml_node node = doc.append_child(get_server_directory_root_node.c_str());
    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, get_server_directory_path_node, dr.path_.to_string());
    }
    return doc;
}

inline pugi::xml_node& operator >> (pugi::xml_node& node, get_server_directory_response& dr)
{
    if (node != NULL)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, get_server_directory_path_node, str))
            dr.path_ = str;
    }
    return node;
}

////////////////////////////////////////////////////////////////////

struct get_transcoding_params_response_t
{
    dvblex::parameters_container_t params_;
};

const std::string transcoding_params_root_node                = "transcoding_params";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_transcoding_params_response_t& request)
{
    if (NULL != node)
    {
        pugi::xml_node params_node = node.child(params_container_node.c_str());
        read_from_node(params_node, request.params_);
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_transcoding_params_response_t& request)
{
    pugi::xml_node node = doc.append_child(transcoding_params_root_node.c_str());

    if (node != NULL)
    {
        write_to_node(request.params_, node);
    }

    return doc;
}

////////////////////////////////////////////////////////////////////

struct set_transcoding_params_request_t
{
    dvblex::concise_param_map_t params_;
};

const std::string transcoding_params_params_node                = "parameters";

inline pugi::xml_node& operator>> (pugi::xml_node& node, set_transcoding_params_request_t& request)
{
    if (NULL != node)
    {
        pugi::xml_node params_node = node.child(transcoding_params_params_node.c_str());
        read_from_node(params_node, request.params_);
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const set_transcoding_params_request_t& request)
{
    pugi::xml_node node = doc.append_child(transcoding_params_root_node.c_str());

    if (node != NULL)
    {
        pugi::xml_node params_node = dvblink::pugixml_helpers::new_child(node, transcoding_params_params_node);
        write_to_node(request.params_, params_node);
    }

    return doc;
}

} //dvblex
