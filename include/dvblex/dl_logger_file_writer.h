/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

# pragma once

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <dli_logger.h>
#include <dl_uuid.h>
#include <dl_filesystem_path.h>

namespace dvblink { namespace logging {

class logger_file_writer : public i_logger
{
public:
    logger_file_writer(const filesystem_path_t& file_name, e_log_level log_level, bool cmd_line_mode = false);
    ~logger_file_writer();

    void __stdcall log_message(e_log_level log_level, const wchar_t* message) const;
    i_result __stdcall query_interface(const base_id_t& /*requestor_id*/, const i_guid& /*iid*/, i_base_object_t& /*obj*/) {return i_not_implemented;}
    const boost::uuids::uuid& __stdcall get_uid() {return uid_.get();}

    e_log_level get_log_level() const {return log_level_;}

    bool start();
    void stop();

    bool truncate_log_file(boost::uint64_t max_size);
    void set_command_line_mode(bool cmd_line_mode);

    void set_log_level(e_log_level log_level);

protected:
    void out_message(const std::wstring& message, const boost::posix_time::ptime& time) const;

private:
    base_id_t uid_;
    filesystem_path_t file_name_;
    e_log_level log_level_;
    volatile bool quit_flag_;
    bool command_line_mode_;

#ifdef _WIN32
    HANDLE log_file_;
#else
    FILE* log_file_;
#endif

    mutable boost::mutex lock_;
};

} //logging
} //dvblink
