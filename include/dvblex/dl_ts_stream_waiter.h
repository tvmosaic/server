/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <boost/thread/shared_mutex.hpp>

#include <dl_ts_info.h>
#include <dl_ts_proc.h>

namespace dvblink { namespace engine {

class CTSStreamWaiter
{
public:
    enum EState
    {
        ES_STOPPED,
        ES_WAITING_STREAM,
        ES_WAITING_DECRYPTION,
        ES_WAITING_KEY_FRAME,
        ES_STREAMING
    };

    CTSStreamWaiter(LP_PACKGEN_TS_SEND_FUNC cb_func, void* param);
    ~CTSStreamWaiter();

    void Start(CTSPmtParser* pmt_parser);
    void Stop();

    void ProcessStream(unsigned char* buffer, int len);

	unsigned short GetPid();

	EState GetState();

protected:

    EState m_state;
	unsigned short pid_;
	unsigned short video_pid_;
	LP_PACKGEN_TS_SEND_FUNC cb_func_;
	void* param_;
	boost::shared_mutex mutex_;

	unsigned short GetPidToCheck(CTSPmtParser* pmt_parser);
	bool IsStreamEncrypted(const unsigned char* pPacket);
	unsigned short GetVideoPid(CTSPmtParser* pmt_parser);
};

}; //engine
}; //dvblink

