/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_DYNLIB_H_
#define __DVBLINK_DL_DYNLIB_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <string>

namespace dvblink { namespace engine {

typedef void *dynlib;

class dyn_lib
{
public:
    dyn_lib();
    dyn_lib(const std::string& lib_name);
    dyn_lib(const std::wstring& lib_name);
    virtual ~dyn_lib();

    void SetLibName(const std::string& lib_name);
    void SetLibName(const std::wstring& lib_name);

    const std::string& GetLibName() const {return lib_name_;}

    bool Load();
    bool Unload();

    void* GetFuncAddr(const char* func_name) const;
    template <typename T> T GetFuncAddr(const char* func_name) const
    {
        return reinterpret_cast<T>(GetFuncAddr(func_name));
    }

    const char* GetError() const;
    const void GetError(std::wstring& werror) const;

    dynlib GetHandle() const {return handle_;}
    bool is_loaded() const {return handle_ != NULL;}

private:
    void SetNoError() const;

    dynlib DynLibLoad(const std::string& lib_name);
    bool DynLibUnload(dynlib handle);
    void* DynLibGetProc(dynlib handle, const char* proc_name) const;
    const char* DynLibGetError() const;

private:
    dynlib handle_;
    std::string lib_name_;
    mutable std::string m_strError;

#ifdef _WIN32
    mutable char m_szErrorBuf[1024];
#endif
};

} //engine
} //dvblink

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_DYNLIB_H_
