/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <dl_types.h>
#include <dl_pugixml_helper.h>
#include <dl_products.h>

namespace dvblex { 

const std::string products_response_root              = "installed_products";
const std::string products_product_node               = "product";
const std::string products_id_node                    = "id";
const std::string products_name_node                    = "name";
const std::string products_url_node                    = "url";
const std::string products_version_node                    = "version";
const std::string products_description_node                    = "description";
const std::string products_build_node                    = "build";
const std::string products_product_type_node             = "product_type";
const std::string products_trial_available_node                    = "trial_available";
const std::string products_requires_registration_node                    = "requires_registration";
const std::string products_requires_subscription_node                    = "requires_subscription";
const std::string products_requires_coupon_node                    = "requires_coupon";
const std::string products_days_left_node                    = "days_left";
const std::string products_license_state_node                    = "license_state";
const std::string products_license_name_node                    = "license_name";
const std::string products_license_key_node                    = "license_key";
const std::string products_fingerprint_node                    = "fingerprint";
const std::string products_machine_id_node                    = "machine_id";
const std::string products_product_file_node                    = "product_file";
const std::string activation_in_progress_node                    = "activation_in_progress";

static const char* product_license_state_strings[] = {"wrong_fp", "free", "trial", "registered", "expired", "no_license_file", "no_subscription", 
    "subscribed", "subscription_expired", "subscription_wrong_fp", "no_coupon", "coupon_wrong_fp"};

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const dvblex::product_info_list_t& products)
{
    std::stringstream buf;

    pugi::xml_node products_node = doc.append_child(products_response_root.c_str());
    if (products_node != NULL)
    {
        for (size_t i=0; i<products.size(); i++)
        {
            pugi::xml_node product_node = dvblink::pugixml_helpers::new_child(products_node, products_product_node);
            if (product_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(product_node, products_id_node, products[i].id_.to_string());
                dvblink::pugixml_helpers::new_child(product_node, products_name_node,  products[i].name_.get());
                dvblink::pugixml_helpers::new_child(product_node, products_url_node,  products[i].url_.get());
                dvblink::pugixml_helpers::new_child(product_node, products_version_node,  products[i].version_.get());
                dvblink::pugixml_helpers::new_child(product_node, products_description_node,  products[i].description_.get());
                dvblink::pugixml_helpers::new_child(product_node, products_build_node,  products[i].revision_.get());
                if (!products[i].product_type_.empty())
                    dvblink::pugixml_helpers::new_child(product_node, products_product_type_node,  products[i].product_type_.get());
                if (products[i].trial_available_.get())
                    dvblink::pugixml_helpers::new_child(product_node, products_trial_available_node, dvblink::pugixml_helpers::xmlnode_value_true);
                if (products[i].requires_registration_.get())
                    dvblink::pugixml_helpers::new_child(product_node, products_requires_registration_node, dvblink::pugixml_helpers::xmlnode_value_true);
                if (products[i].requires_subscription_.get())
                    dvblink::pugixml_helpers::new_child(product_node, products_requires_subscription_node, dvblink::pugixml_helpers::xmlnode_value_true);
                if (products[i].requires_coupon_.get())
                    dvblink::pugixml_helpers::new_child(product_node, products_requires_coupon_node, dvblink::pugixml_helpers::xmlnode_value_true);
                if (products[i].activation_in_progress_.get())
                    dvblink::pugixml_helpers::new_child(product_node, activation_in_progress_node, dvblink::pugixml_helpers::xmlnode_value_true);

                buf.clear(); buf.str("");
                buf << products[i].days_left_;
                dvblink::pugixml_helpers::new_child(product_node, products_days_left_node, buf.str());

                dvblink::pugixml_helpers::new_child(product_node, products_license_state_node,  product_license_state_strings[products[i].license_state_]);
                dvblink::pugixml_helpers::new_child(product_node, products_license_name_node,  products[i].license_name_.get());
                dvblink::pugixml_helpers::new_child(product_node, products_license_key_node,  products[i].license_key_.get());
                dvblink::pugixml_helpers::new_child(product_node, products_fingerprint_node,  products[i].hw_fingerprint_.get());
                dvblink::pugixml_helpers::new_child(product_node, products_machine_id_node,  products[i].machine_id_.get());
                dvblink::pugixml_helpers::new_child(product_node, products_product_file_node,  products[i].product_file_.get());
            }
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, dvblex::product_info_list_t& products)
{
    if (NULL != node)
    {
        std::string str;
        pugi::xml_node product_node = node.first_child();
        while (product_node != NULL)
        {
            if (boost::iequals(product_node.name(), products_product_node))
            {
                dvblex::product_info_t product;

                if (dvblink::pugixml_helpers::get_node_attribute(product_node, products_id_node, str))
                    product.id_ = str;

                if (dvblink::pugixml_helpers::get_node_value(product_node, products_name_node, str))
                    product.name_ = str;

                if (dvblink::pugixml_helpers::get_node_value(product_node, products_url_node, str))
                    product.url_ = str;

                if (dvblink::pugixml_helpers::get_node_value(product_node, products_version_node, str))
                    product.version_ = str;

                if (dvblink::pugixml_helpers::get_node_value(product_node, products_description_node, str))
                    product.description_ = str;

                if (dvblink::pugixml_helpers::get_node_value(product_node, products_build_node, str))
                    product.revision_ = str;

                if (dvblink::pugixml_helpers::get_node_value(product_node, products_product_type_node, str))
                    product.product_type_ = str;

                product.trial_available_ = dvblink::pugixml_helpers::get_node_value(product_node, products_trial_available_node, str) &&
                    boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);

                product.requires_registration_ = dvblink::pugixml_helpers::get_node_value(product_node, products_requires_registration_node, str) &&
                    boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);

                product.requires_subscription_ = dvblink::pugixml_helpers::get_node_value(product_node, products_requires_subscription_node, str) &&
                    boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);

                product.requires_coupon_ = dvblink::pugixml_helpers::get_node_value(product_node, products_requires_coupon_node, str) &&
                    boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);

                product.activation_in_progress_ = dvblink::pugixml_helpers::get_node_value(product_node, activation_in_progress_node, str) &&
                    boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);

                if (dvblink::pugixml_helpers::get_node_value(product_node, products_days_left_node, str))
                    dvblink::engine::string_conv::apply(str.c_str(), product.days_left_, (boost::int32_t)0);

                if (dvblink::pugixml_helpers::get_node_value(product_node, products_license_state_node, str))
                {
                    for (size_t i=0; i<sizeof(product_license_state_strings) / sizeof(char*); i++)
                    {
                        if (boost::iequals(product_license_state_strings[i], str))
                            product.license_state_ = (license_state_e)i;
                    }
                }

                if (dvblink::pugixml_helpers::get_node_value(product_node, products_license_name_node, str))
                    product.license_name_ = str;

                if (dvblink::pugixml_helpers::get_node_value(product_node, products_license_key_node, str))
                    product.license_key_ = str;

                if (dvblink::pugixml_helpers::get_node_value(product_node, products_fingerprint_node, str))
                    product.hw_fingerprint_ = str;

                if (dvblink::pugixml_helpers::get_node_value(product_node, products_machine_id_node, str))
                    product.machine_id_ = str;

                if (dvblink::pugixml_helpers::get_node_value(product_node, products_product_file_node, str))
                    product.product_file_ = str;
				
                products.push_back(product);
            }
            product_node = product_node.next_sibling();
        }
    }

    return node;
}

////////////////////////////////////////////////////////////////////
struct activate_product_request_t
{
    activate_product_request_t()
    {}

    product_activation_info_t activation_info_;
};

const std::string products_activate_product_root               = "activate_product";
const std::string products_serial_node               = "serial";
const std::string products_email_node               = "email";
const std::string products_user_name_node               = "user_name";

inline pugi::xml_node& operator>> (pugi::xml_node& node, activate_product_request_t& activate_product_request)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, products_id_node, str))
            activate_product_request.activation_info_.id_ = str;
        if (dvblink::pugixml_helpers::get_node_value(node, products_serial_node, str))
            activate_product_request.activation_info_.coupon_code_ = str;
        if (dvblink::pugixml_helpers::get_node_value(node, products_email_node, str))
            activate_product_request.activation_info_.email_ = str;
        if (dvblink::pugixml_helpers::get_node_value(node, products_user_name_node, str))
            activate_product_request.activation_info_.user_name_ = str;
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const dvblex::activate_product_request_t& activate_product_request)
{
    pugi::xml_node node = doc.append_child(products_activate_product_root.c_str());
    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, products_id_node, activate_product_request.activation_info_.id_.get());
        dvblink::pugixml_helpers::new_child(node, products_serial_node, activate_product_request.activation_info_.coupon_code_.get());
        dvblink::pugixml_helpers::new_child(node, products_email_node, activate_product_request.activation_info_.email_.get());
        dvblink::pugixml_helpers::new_child(node, products_user_name_node, activate_product_request.activation_info_.user_name_.get());
    }

    return doc;
}


////////////////////////////////////////////////////////////////////
struct activate_product_response_t
{
    activate_product_response_t() :
        result_(eapr_error)
    {}

    product_activation_result_e result_;
};

static const char* product_activation_result_strings[] = {"success", "no_server_connection", "no_activations_available", "already_activated", 
    "invalid_login", "in_progress", "file_write_error", "error", "invalid_xml", "invalid_data", "invalid_coupon", "already_used_coupon",
    "email_already_in_use", "other_product_coupon", "need_user_info"};

const std::string products_result_node               = "result";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const activate_product_response_t& apr)
{
    pugi::xml_node apr_node = doc.append_child(products_activate_product_root.c_str());

    if (apr_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(apr_node, products_result_node, product_activation_result_strings[apr.result_]);
    }
    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, activate_product_response_t& apr)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, products_result_node, str))
        {
            for (size_t i=0; i<sizeof(product_activation_result_strings) / sizeof(char*); i++)
            {
                if (boost::iequals(product_activation_result_strings[i], str))
                    apr.result_ = (product_activation_result_e)i;
            }
        }
    }

    return node;
}

} //dvblex
