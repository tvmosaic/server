/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <boost/shared_ptr.hpp>
#include <dl_types.h>
#include <dl_platforms.h>

namespace dvblink { 

#define STREAMER_PRIORITY_MAX   256
#define STREAMER_PRIORITY_MIN   0

class streamer_callbacks_t
{
public:
    streamer_callbacks_t(){}

    virtual void streamer_disconnected(const dvblink::streamer_id_t& /*id*/){};
};

class streamer_t;
typedef boost::shared_ptr<streamer_t> streamer_object_t;

class streamer_t
{
public:
    streamer_t(const dvblink::streamer_id_t& id) :
        callbacks_(NULL),
        streamer_id_(id)
        {}

    virtual ~streamer_t(){}

    virtual dvblink::streamer_id_t get_id(){return streamer_id_;}
    virtual boost::uint32_t get_priority(){return STREAMER_PRIORITY_MIN;}
    virtual void write_stream(const unsigned char* /*data*/, size_t /*len*/){}

    virtual bool start(streamer_callbacks_t* cb){callbacks_ = cb; return true;}
    virtual void notify_streamer_deleted(){callbacks_ = NULL;}

    static void __stdcall release(streamer_t* obj) {delete obj;}

protected:
    streamer_callbacks_t* callbacks_;
    dvblink::streamer_id_t streamer_id_;
};

template<class T>
boost::shared_ptr<T> share_streamer_safely(T* obj)
{
    return boost::shared_ptr<T>(obj, &T::release);
}

}
