/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <dl_types.h>
#include <dl_channel_info.h>
#include <dl_parameters.h>
#include <dl_scan_info.h>
#include <dl_streamer.h>

namespace dvblex { 

//device state

enum device_state_e
{
    ds_none,
    ds_idle,
    ds_streaming,
    ds_channel_scan_in_progress,
    ds_networks_scanned,
    ds_channel_scan_finished,
    ds_epg_scan_in_progress,
    ds_epg_scan_finished
};

enum device_connection_type_e
{
	edct_unknown = 0,
    edct_network,
    edct_local,
    edct_usb,
    edct_pcie
};

struct device_runtime_info_t
{
    device_runtime_info_t() :
        tuners_num_(1), supported_standards_(SOURCE_TYPE_DVB_ALL)
    {}

    dvblink::device_name_t name_;
    boost::int32_t tuners_num_;
    dvblink::standard_set_t supported_standards_; //OR combination of source_type_e
};

struct device_descriptor_t
{
    device_descriptor_t() :
        tuners_num_(1), state_(ds_none), supported_standards_(st_unknown), 
        should_be_merged_(false), can_be_deleted_(false), has_settings_(false) {}

    void from_descriptor(const device_descriptor_t& d)
    {
        id_ = d.id_;
        name_ = d.name_;
        state_ = d.state_;
        tuners_num_ = d.tuners_num_;
        supported_standards_ = d.supported_standards_;
        uri_ = d.uri_;
        make_ = d.make_;
        model_ = d.model_;
        model_num_ = d.model_num_;
        uuid_ = d.uuid_;
        uuid_make_ = d.uuid_make_;
        connection_type_ = d.connection_type_;
        should_be_merged_ = d.should_be_merged_;
        can_be_deleted_ = d.can_be_deleted_;
        has_settings_ = d.has_settings_;
    }

    dvblink::device_id_t id_;
    dvblink::device_name_t name_;
    boost::int32_t tuners_num_;
    device_state_e state_;
    dvblink::standard_set_t supported_standards_; //OR combination of source_type_e
    dvblink::device_uri_t uri_;
    dvblink::device_make_t make_;
    dvblink::device_model_t model_;
    dvblink::device_model_num_t model_num_;
    dvblink::device_uuid_t uuid_;
    dvblink::device_uuid_make_t uuid_make_;
    device_connection_type_e connection_type_;
    bool should_be_merged_;
    dvblink::device_vid_t vid_;
    dvblink::device_pid_t pid_;
    bool can_be_deleted_;
    bool has_settings_;
};

typedef std::vector<device_descriptor_t> device_descriptor_list_t;

struct stream_stats_t
{
    stream_stats_t() :
        bitrate_(0), packet_count_(0), error_packet_count_(0), discontinuity_count_(0)
    {}

    boost::uint64_t bitrate_;
    boost::uint64_t packet_count_;
    boost::uint64_t error_packet_count_;
    boost::uint64_t discontinuity_count_;
};

struct stream_channel_stats_t
{
    stream_channel_stats_t() :
        consumer_num_(0)
    {}

    dvblink::channel_id_t channel_id_;
    dvblink::channel_name_t channel_name_;
    stream_stats_t stream_stats_;
    boost::uint32_t consumer_num_;
};

typedef std::vector<stream_channel_stats_t> stream_channel_stats_list_t;

struct device_status_t
{
    device_status_t() :
        state_(ds_none), max_priority_(STREAMER_PRIORITY_MIN)
        {}

    void from_device_status(const device_status_t& status)
    {
        id_ = status.id_;
        state_ = status.state_;
        signal_info_.from_signal_info(status.signal_info_);
        stream_channels_ = status.stream_channels_;
        max_priority_ = status.max_priority_;
        scan_progress_.from_channel_scanner_progress(status.scan_progress_);
    }

    dvblink::device_id_t id_;

    device_state_e state_;

    //valid if streaming
    signal_info_t signal_info_;
    stream_channel_stats_list_t stream_channels_;
    boost::uint32_t max_priority_;

    //valid if scanning
    channel_scanner_progress_t scan_progress_;
};

typedef std::vector<device_status_t> device_status_list_t;

//configurable device properties
struct configurable_device_props_t
{
    configurable_device_props_t() :
        fta_concurrent_streams_(-1), enc_concurrent_streams_(1)
    {}

    boost::int32_t fta_concurrent_streams_;
    boost::int32_t enc_concurrent_streams_;

    parameters_container_t param_container_;
};

typedef std::map<dvblink::device_id_t, configurable_device_props_t> configurable_device_props_map_t;

enum set_device_props_result_e
{
    sdpr_error = 0x000000000,
    sdpr_success = 0x000000001,
    sdpr_epg_rescan_needed = 0x000000002
};

//manual devices
struct manual_device_t
{
    dvblink::device_id_t id_;
    concise_param_map_t params_;
};

typedef std::vector<manual_device_t> manual_device_list_t;

const std::string manual_device_template_id_key		= "manual_device_template_id";

}
