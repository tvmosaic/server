/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>
#include <dl_types.h>
#include <dl_message.h>
#include <dl_message_addresses.h>
#include <dl_channels.h>
#include <dl_pb_types.h>

namespace dvblink { namespace messaging { namespace recorder {

//
// get_recorder_info
//
struct get_recorder_info_response
{
    xml_string_t info_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & info_;
    } 
};

struct get_recorder_info_request : public message_send<get_recorder_info_request, get_recorder_info_response>
{
};

//
// force_update
//
struct force_update_request : public message_post<force_update_request>
{
};

//
// set_recording_options
//
struct set_recording_options_response
{
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    } 
};

struct set_recording_options_request : public message_send<set_recording_options_request, set_recording_options_response>
{
    set_recording_options_request() {}
    set_recording_options_request(const xml_string_t& options) : options_(options) {}
    xml_string_t options_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & options_;
    } 
};

//
// get_recording_options
//
struct get_recording_options_response
{
    xml_string_t options_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & options_;
        ar & result_;
    } 
};

struct get_recording_options_request : public message_send<get_recording_options_request, get_recording_options_response>
{
};

//
// search_epg
//
struct search_epg_response
{
    xml_string_t epg_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & epg_;
        ar & result_;
    } 
};

struct search_epg_request : public message_send<search_epg_request, search_epg_response>
{
    search_epg_request() {}
    search_epg_request(const xml_string_t& data, const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port) : 
        search_data_(data), server_address_(server_address), server_port_(server_port) {}
    xml_string_t search_data_;
    dvblink::url_address_t server_address_;
    dvblink::network_port_t server_port_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & search_data_;
        ar & server_address_;
        ar & server_port_;
    } 
};

//
// add_schedule
//
struct add_schedule_response
{
    xml_string_t conflicts_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & conflicts_;
        ar & result_;
    } 
};

struct add_schedule_request : public message_send<add_schedule_request, add_schedule_response>
{
    add_schedule_request() {}
    add_schedule_request(const xml_string_t& schedule) : schedule_(schedule) {}
    xml_string_t schedule_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & schedule_;
    } 
};

//
// get_schedule
//
struct get_schedule_response
{
    xml_string_t schedule_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & schedule_;
        ar & result_;
    } 
};

struct get_schedule_request : public message_send<get_schedule_request, get_schedule_response>
{
};

//
// update_schedule
//
struct update_schedule_response
{
    xml_string_t conflicts_;
    bool result_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & conflicts_;
        ar & result_;
    } 
};

struct update_schedule_request : public message_send<update_schedule_request, update_schedule_response>
{
    update_schedule_request() {}
    update_schedule_request(const xml_string_t& schedule) : schedule_(schedule) {}
    xml_string_t schedule_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & schedule_;
    } 
};

//
// remove_schedule
//
struct remove_schedule_response
{
    bool result_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    } 
};

struct remove_schedule_request : public message_send<remove_schedule_request, remove_schedule_response>
{
    remove_schedule_request() {}
    remove_schedule_request(const xml_string_t& schedule) : schedule_(schedule) {}
    xml_string_t schedule_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & schedule_;
    } 
};

//
// get_recordings
//
struct get_recordings_response
{
    xml_string_t recordings_;
    bool result_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & recordings_;
        ar & result_;
    } 
};

struct get_recordings_request : public message_send<get_recordings_request, get_recordings_response>
{
};

//
// remove_recording
//
struct remove_recording_response
{
    bool result_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    } 
};

struct remove_recording_request : public message_send<remove_recording_request, remove_recording_response>
{
    remove_recording_request() {}
    remove_recording_request(const xml_string_t& recording) : recording_(recording) {}
    xml_string_t recording_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & recording_;
    } 
};

//
// get completed recordings
//
struct get_completed_recordings_response
{
    dvblex::playback::pb_item_list_t completed_recordings_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & completed_recordings_;
        ar & result_;
    } 
};

struct get_completed_recordings_request : public message_send<get_completed_recordings_request, get_completed_recordings_response>
{
};

//
// remove_completed_recording
//
struct remove_completed_recording_response
{
    bool result_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    } 
};

struct remove_completed_recording_request : public message_send<remove_completed_recording_request, remove_completed_recording_response>
{
    remove_completed_recording_request() {}
    remove_completed_recording_request(const dvblink::object_id_t& recording_id) : recording_id_(recording_id) {}
    dvblink::object_id_t recording_id_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & recording_id_;
    } 
};

//
// update epg for given logical channels
//
struct update_recorder_epg_response
{
    bool result_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    } 
};

struct update_recorder_epg_request : public message_send<update_recorder_epg_request, update_recorder_epg_response>
{
    update_recorder_epg_request() {}
    update_recorder_epg_request(const dvblex::channel_id_list_t& channel_list) : channel_list_(channel_list) {}
    dvblex::channel_id_list_t channel_list_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & channel_list_;
    } 
};

//
// get schedules in csv format
//
struct get_schedules_in_csv_response
{
    csv_string_t schedules_;
    bool result_;

private: 
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & schedules_;
        ar & result_;
    } 
};

struct get_schedules_in_csv_request : public message_send<get_schedules_in_csv_request, get_schedules_in_csv_response>
{
};

//
// repair recording database
//
struct repair_database_response
{
    bool result_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
    } 
};

struct repair_database_request : public message_send<repair_database_request, repair_database_response>
{
};

//Recording completed broadcast

struct timer_recording_completed_request : public message_post<timer_recording_completed_request>
{
    timer_recording_completed_request() {}
    timer_recording_completed_request(const timer_id_t& timer_id, const std::vector<base_id_t>& targets) : 
        timer_id_(timer_id), targets_(targets) {}

    timer_id_t timer_id_;
    std::vector<base_id_t> targets_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & timer_id_;
        ar & targets_;
    } 
};

//
// get epg in xmltv format
//
struct get_xmltv_epg_response
{
    bool result_;
    std::string epg_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & result_;
        ar & epg_;
    } 
};

struct get_xmltv_epg_request : public message_send<get_xmltv_epg_request, get_xmltv_epg_response>
{
    get_xmltv_epg_request() :
        days_(0),
        simple_time_format_(true)
        {}

    dvblex::channel_desc_list_t channels_;
    boost::int32_t days_;
    bool simple_time_format_;

private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive& ar, const unsigned int /*version*/) 
    {
        ar & channels_;
        ar & days_;
        ar & simple_time_format_;
    } 
};


} //recorder
} //messaging
} //dvblink
