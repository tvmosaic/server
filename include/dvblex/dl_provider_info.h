/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <map>
#include <dl_types.h>
#include <dl_channel_info.h>
#include <dl_parameters.h>

namespace dvblex { 

const std::string provider_auto_origin_favorites_key = "c9de2e47";
const std::string provider_hide_new_channels_on_rescan_key = "c9de2e48";

struct provider_info_t
{
    dvblink::provider_id_t id_;
    dvblink::provider_name_t name_;
    dvblink::provider_country_t country_;
    dvblink::parameter_desc_t desc_;
    concise_param_map_t scan_params_;
    parameters_container_t param_container_;
    source_type_e standard_;
};

typedef std::vector<provider_info_t> provider_info_list_t;
typedef std::vector<dvblink::provider_id_t> provider_id_list_t;
typedef std::map<dvblink::provider_id_t, provider_info_t> provider_id_to_info_map_t;

struct provider_description_t
{
    source_type_e standard_;
    dvblink::standard_name_t standard_name_;
    parameters_container_t parameters_;
    provider_info_list_t providers_;
};

typedef std::map<source_type_e, provider_description_t> provider_info_map_t;

typedef std::vector<dvblink::scan_element_t> provider_scan_list_t;

}
