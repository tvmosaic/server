/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <map>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_channel_info.h>
#include <dl_filesystem_path.h>

namespace dvblex { 

//////////////////////////////////////////////////////////////////////////////

struct epg_source_t
{
    epg_source_t() :
        has_settings_(false), is_default_(false)
    {}

    epg_source_t(const dvblink::epg_source_id_t& id, const dvblink::epg_source_name_t& name, const dvblink::bool_flag_t& has_settings) :
        id_(id), name_(name), has_settings_(has_settings), is_default_(false)
    {}

    dvblink::epg_source_id_t id_;
    dvblink::epg_source_name_t name_;
    dvblink::bool_flag_t has_settings_;
    dvblink::bool_flag_t is_default_;
};

typedef std::map<dvblink::epg_source_id_t, epg_source_t> epg_source_map_t;
typedef std::vector<dvblink::epg_source_id_t> epg_source_id_list_t;

//////////////////////////////////////////////////////////////////////////////

const dvblink::channel_number_t epg_invalid_channel_number_ = -1;
const dvblink::channel_scan_id_t epg_invalid_scan_id_ = 0xFFFF;

struct epg_source_channel_t
{
    epg_source_channel_t() :
        number_(epg_invalid_channel_number_), sub_number_(epg_invalid_channel_number_), nid_(epg_invalid_scan_id_), tid_(epg_invalid_scan_id_), sid_(epg_invalid_scan_id_)
    {}

    dvblink::epg_channel_id_t id_;
    dvblink::epg_channel_name_t name_;
    dvblink::channel_number_t number_;
    dvblink::channel_number_t sub_number_;
    dvblink::channel_scan_id_t nid_;
    dvblink::channel_scan_id_t tid_;
    dvblink::channel_scan_id_t sid_;
    dvblink::url_address_t logo_;
};

typedef std::map<dvblink::epg_channel_id_t, epg_source_channel_t> epg_source_channel_map_t;

//////////////////////////////////////////////////////////////////////////////

struct epg_source_channel_id_t
{
    epg_source_channel_id_t()
    {}

    epg_source_channel_id_t(const dvblink::epg_source_id_t& epg_source_id, const dvblink::epg_channel_id_t& epg_channel_id) :
        epg_source_id_(epg_source_id), epg_channel_id_(epg_channel_id)
    {}

    dvblink::epg_source_id_t epg_source_id_;
    dvblink::epg_channel_id_t epg_channel_id_;
};

typedef std::map<dvblink::channel_id_t, epg_source_channel_id_t> channel_to_epg_source_map_t;
typedef std::vector<epg_source_channel_id_t> epg_source_channel_id_list_t;

//////////////////////////////////////////////////////////////////////////////

struct epg_channel_match_t
{
    epg_source_channel_id_list_t exact_match_;
    epg_source_channel_id_list_t partial_match_;
};

typedef std::map<dvblink::channel_id_t, epg_channel_match_t> epg_channel_match_map_t;

//////////////////////////////////////////////////////////////////////////////

bool write_epg_channels_to_file(const epg_source_channel_map_t& channel_map, const dvblink::filesystem_path_t& fname);
bool read_epg_channels_from_file(const dvblink::filesystem_path_t& fname, epg_source_channel_map_t& channel_map);

} //dvblex

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::epg_source_channel_t& esc, const unsigned int /*version*/)
{
    ar & esc.id_;
    ar & esc.name_;
    ar & esc.number_;
    ar & esc.sub_number_;
    ar & esc.nid_;
    ar & esc.tid_;
    ar & esc.sid_;
    ar & esc.logo_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::epg_source_channel_id_t& ces, const unsigned int /*version*/)
{
    ar & ces.epg_source_id_;
    ar & ces.epg_channel_id_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::epg_source_t& es, const unsigned int /*version*/)
{
    ar & es.id_;
    ar & es.name_;
    ar & es.has_settings_;
    ar & es.is_default_;    
}

template<class Archive>
void serialize(Archive& ar, dvblex::epg_channel_match_t& ecm, const unsigned int /*version*/)
{
    ar & ecm.exact_match_;
    ar & ecm.partial_match_;
}

} // namespace serialization
} // namespace boost
