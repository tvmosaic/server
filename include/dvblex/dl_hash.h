/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <dl_ts_info.h>
#include <dl_fnv.h>

namespace dvblink {
namespace engine {

inline boost::uint64_t calculate_64bit_string_hash(const std::string& text)
{
    boost::uint32_t fnv_hash = fnv1a(text);
    boost::uint32_t crc_hash = ts_crc_handler::GetCRCHandler()->CalculateCRC((unsigned char*)text.c_str(), text.size());

    boost::uint64_t ret_val = fnv_hash;
    ret_val = (ret_val << 32) | crc_hash;

    return ret_val;
}

} // namespace engine
} // namespace dvblink
