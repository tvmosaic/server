/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>
#include <map>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <dl_types.h>
#include <dl_common.h>
#include <dl_epgevent.h>
#include <dl_channels.h>
#include <dl_strings.h>
#include <dl_pugixml_helper.h>
#include <dl_strptime.h>

namespace dvblex {

class xmltv_creator
{
public:
    enum data_time_format_e
    {
        dtf_simple,
        dtf_with_tz
    };

public:
	xmltv_creator() : 
	  root_node_(NULL),
      df_(dtf_simple)
	{}

	~xmltv_creator()
	{}

	bool start(data_time_format_e df = dtf_simple)
	{
        df_ = df;

		bool ret_val = false;

        root_node_ = doc_.append_child("tv");
        if (root_node_ != NULL)
        {
            dvblink::pugixml_helpers::add_node_attribute(root_node_, "generator-info-name", PRODUCT_NAME_UTF8);
			ret_val = true;
		}

		return ret_val;
	}

	bool finish_and_create_string(std::string& xml)
	{
        //dump xml string to memory
        dvblink::pugixml_helpers::xmldoc_dump_to_string(doc_, xml);

		return true;
	}

	bool finish_and_write_to_file(const dvblink::filesystem_path_t& filepath)
	{
        dvblink::pugixml_helpers::xmldoc_dump_to_file(doc_, filepath.to_string());

		return true;
	}

    void add_channel(const dvblex::channel_description_t& ch)
	{
        pugi::xml_node ch_node = dvblink::pugixml_helpers::new_child(root_node_, "channel");
		if (ch_node != NULL)
		{
            dvblink::pugixml_helpers::add_node_attribute(ch_node, "id", ch.id_.get());

            dvblink::pugixml_helpers::new_child(ch_node, "display-name", ch.name_.get());

			if (!ch.logo_.empty())
			{
                pugi::xml_node logo_node = dvblink::pugixml_helpers::new_child(ch_node, "icon");
		        if (logo_node != NULL)
                    dvblink::pugixml_helpers::add_node_attribute(logo_node, "src", ch.logo_.get());
			}
		}
	}

	void add_event(const dvblink::channel_id_t& id, const dvblink::engine::DLEPGEvent& ev)
	{
        pugi::xml_node prg_node = dvblink::pugixml_helpers::new_child(root_node_, "programme");
		if (prg_node != NULL)
		{
            dvblink::pugixml_helpers::add_node_attribute(prg_node, "channel", id.get());

            std::string str;
			std::stringstream strbuf;

			boost::posix_time::ptime pt_start = boost::posix_time::from_time_t(ev.m_StartTime);
            str = get_datetime_string(pt_start);
            dvblink::pugixml_helpers::add_node_attribute(prg_node, "start", str);

			boost::posix_time::ptime pt_stop = boost::posix_time::from_time_t(ev.m_StartTime + ev.m_Duration);
            str = get_datetime_string(pt_stop);
            dvblink::pugixml_helpers::add_node_attribute(prg_node, "stop", str);

            dvblink::pugixml_helpers::new_child(prg_node, "title", ev.m_Name);

			if (ev.m_ShortDesc.size() > 0)
                dvblink::pugixml_helpers::new_child(prg_node, "desc", ev.m_ShortDesc);

			if (ev.m_SecondName.size() > 0)
                dvblink::pugixml_helpers::new_child(prg_node, "sub-title", ev.m_SecondName);

			if (ev.m_Actors.size() > 0 ||
				ev.m_Directors.size() > 0 ||
				ev.m_Writers.size() > 0 ||
				ev.m_Producers.size() > 0 ||
				ev.m_Guests.size() > 0)
			{
                pugi::xml_node credits_node = dvblink::pugixml_helpers::new_child(prg_node, "credits");
		        if (credits_node != NULL)
		        {
					split_and_add_keywords(credits_node, "actor", ev.m_Actors);
					split_and_add_keywords(credits_node, "director", ev.m_Directors);
					split_and_add_keywords(credits_node, "writer", ev.m_Writers);
					split_and_add_keywords(credits_node, "producer", ev.m_Producers);
					split_and_add_keywords(credits_node, "guest", ev.m_Guests);
				}
			}

			if (ev.m_Categories.size() > 0)
				split_and_add_keywords(prg_node, "category", ev.m_Categories);

			if (ev.m_Language.size() > 0)
                dvblink::pugixml_helpers::new_child(prg_node, "language", ev.m_Language);

			if (ev.m_ImageURL.size() > 0)
			{
                pugi::xml_node logo_node = dvblink::pugixml_helpers::new_child(prg_node, "icon");
		        if (logo_node != NULL)
                    dvblink::pugixml_helpers::add_node_attribute(logo_node, "src", ev.m_ImageURL);
			}

			if (ev.m_EpisodeNum > 0 || ev.m_SeasonNum > 0)
			{
				strbuf.clear(); strbuf.str("");
				if (ev.m_SeasonNum > 0)
					strbuf << ev.m_SeasonNum - 1 << ".";
				else
					strbuf << ".";

				if (ev.m_EpisodeNum > 0)
					strbuf << ev.m_EpisodeNum - 1 << ".";
				else
					strbuf << ".";

                pugi::xml_node enum_node = dvblink::pugixml_helpers::new_child(prg_node, "episode-num", strbuf.str());
		        if (enum_node != NULL)
                    dvblink::pugixml_helpers::add_node_attribute(enum_node, "system", "xmltv_ns");
			}

			if (ev.m_IsHDTV)
                dvblink::pugixml_helpers::new_child(prg_node, "quality", "HDTV");

			if (ev.m_IsRepeatFlag)
                dvblink::pugixml_helpers::new_child(prg_node, "previously-shown", "");

			if (ev.m_IsPremiere)
                dvblink::pugixml_helpers::new_child(prg_node, "premiere", "");

			if (ev.m_StarNum > 0)
			{
                pugi::xml_node rating_node = dvblink::pugixml_helpers::new_child(prg_node, "star-rating");
		        if (rating_node != NULL)
                {
					strbuf.clear(); strbuf.str("");
					strbuf << ev.m_StarNum << "/" << ev.m_StarNumMax;
                    dvblink::pugixml_helpers::add_node_attribute(rating_node, "value", strbuf.str());
				}
			}

			if (ev.m_Year > 0)
            {
				strbuf.clear(); strbuf.str("");
				strbuf << ev.m_Year;
                dvblink::pugixml_helpers::new_child(prg_node, "date", strbuf.str());
            }
		}
	}

protected:
    pugi::xml_document doc_;
    pugi::xml_node root_node_;
    data_time_format_e df_;

    std::string get_datetime_string(boost::posix_time::ptime pt)
    {
        std::string str;

        if (df_ == dtf_with_tz)
            dvblink::engine::get_iso8601_time_string(pt, str);
        else if (df_ == dtf_simple)
            dvblink::engine::get_iso8601_plain_time_string(pt, str);

        return str;
    }

	void split_and_add_keywords(pugi::xml_node node, const char* keyword, const std::string& keyword_list)
	{
        if (!keyword_list.empty())
        {
		    std::vector<std::string> tokens;
		    boost::split( tokens, keyword_list, boost::is_any_of("/"), boost::token_compress_on);

		    for (size_t i=0; i<tokens.size(); i++)
                dvblink::pugixml_helpers::new_child(node, keyword, tokens[i]);
        }
	}

};

} //dvblex

