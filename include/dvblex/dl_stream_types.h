/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DL_STREAM_TYPES_H_
#define __DVBLINK_DL_STREAM_TYPES_H_
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

namespace dvblink { namespace engine {

#define MBTS_ST_VIDEO_MPEG1     (0x01)
#define MBTS_ST_VIDEO_MPEG2     (0x02)
#define MBTS_ST_AUDIO_MPEG1     (0x03)
#define MBTS_ST_AUDIO_MPEG2     (0x04)
#define MBTS_ST_PRIVATE_SECTION (0x05)
#define MBTS_ST_PRIVATE_DATA    (0x06)
#define MBTS_ST_MHEG            (0x07)
#define MBTS_ST_DSMCC_TYPE_A    (0x0A)
#define MBTS_ST_DSMCC_TYPE_B    (0x0B)
#define MBTS_ST_DSMCC_TYPE_C    (0x0C)
#define MBTS_ST_DSMCC_TYPE_D    (0x0D)
#define MBTS_ST_AUDIO_AAC       (0x0F)
#define MBTS_ST_VIDEO_MPEG4     (0x10)
#define MBTS_ST_VIDEO_H264      (0x1B)
#define MBTS_ST_AUDIO_AC3       (0x81)
#define MBTS_ST_AUDIO_DTS       (0x8A)

} //engine
} //dvblink

///////////////////////////////////////////////////////////////////////////////
#endif //__DVBLINK_DL_STREAM_TYPES_H_
