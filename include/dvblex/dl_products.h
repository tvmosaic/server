/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <dl_types.h>
#include <dl_filesystem_path.h>

namespace dvblex {

enum license_state_e
{
    lse_wrong_fingerprint = 0,
    lse_free,
    lse_trial,
    lse_registered,
    lse_expired,
    lse_no_license_file,
    lse_no_subscription,
    lse_subscribed,
    lse_subscription_expired,
    lse_subscription_wrong_fingerprint,
	lse_no_coupon,
    lse_coupon_wrong_fingerprint
};

struct product_info_t
{
    product_info_t() :
        trial_available_(false),
        requires_registration_(false),
        requires_subscription_(false),
		requires_coupon_(false),
        activation_in_progress_(false),
        display_priority_(0),
        days_left_(0),
        license_state_(lse_wrong_fingerprint)
    {
    }
    //generic
    dvblink::product_id_t id_;
    dvblink::product_name_t name_;
    dvblink::url_address_t url_;
    dvblink::version_t version_;
    dvblink::build_t revision_;
    dvblink::product_desc_t description_;
    dvblink::bool_flag_t trial_available_;
    dvblink::bool_flag_t requires_registration_;
    dvblink::bool_flag_t requires_subscription_;
    dvblink::bool_flag_t requires_coupon_;
    dvblink::product_type_t product_type_;
    dvblink::bool_flag_t activation_in_progress_;
    boost::int32_t display_priority_;
    //licensing
    boost::int32_t days_left_;
    license_state_e license_state_;
    dvblink::license_name_t license_name_;
    dvblink::license_key_t license_key_;
	dvblink::hw_fingerprint_t hw_fingerprint_;
	dvblink::machine_id_t machine_id_;
	dvblink::product_file_t product_file_;    
};

typedef std::vector<product_info_t> product_info_list_t;

enum product_activation_result_e
{
    eapr_success,
    eapr_no_server_connection,
    eapr_no_activations_available,
    eapr_already_activated,
    eapr_invalid_login,
    eapr_in_progress,
    eapr_file_write_error,
    eapr_error,
    eapr_invalid_xml,
    eapr_invalid_data,
    eapr_invalid_coupon,
    eapr_already_used_coupon,
    eapr_email_already_in_use,
    eapr_other_product_coupon,
    eapr_need_user_info
};

struct product_activation_info_t
{
    dvblink::product_id_t id_;
    dvblink::coupon_code_t coupon_code_;
    dvblink::email_t email_;
    dvblink::user_name_t user_name_;
};

} // dvblex

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::product_info_t& product_info, const unsigned int /*version*/)
{
    ar & product_info.id_;
    ar & product_info.name_;
    ar & product_info.url_;
    ar & product_info.version_;
    ar & product_info.revision_;
    ar & product_info.description_;
    ar & product_info.trial_available_;
    ar & product_info.requires_registration_;
    ar & product_info.requires_subscription_;
    ar & product_info.requires_coupon_;
    ar & product_info.product_type_;
    ar & product_info.activation_in_progress_;
    ar & product_info.display_priority_;
    //licensing
    ar & product_info.days_left_;
    ar & product_info.license_state_;
    ar & product_info.license_name_;
    ar & product_info.license_key_;
    ar & product_info.hw_fingerprint_;
    ar & product_info.machine_id_;
    ar & product_info.product_file_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::product_activation_info_t& pai, const unsigned int /*version*/)
{
    ar & pai.id_;
    ar & pai.coupon_code_;
    ar & pai.email_;
    ar & pai.user_name_;
}

} // serialization
} // boost
