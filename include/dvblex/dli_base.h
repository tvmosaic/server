/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string.h>
#include <boost/cstdint.hpp>
#include <boost/shared_ptr.hpp>
#include <dl_types.h>
#include <dl_platforms.h>

#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable:4996)
#endif 

namespace dvblink {

#pragma pack(push, 1)

struct i_guid
{
    boost::uint32_t data1;
    boost::uint16_t data2;
    boost::uint16_t data3;
    boost::uint8_t  data4[8];

    typedef boost::uint8_t value_type;
    typedef boost::uint8_t& reference;
    typedef boost::uint8_t const& const_reference;
    typedef boost::uint8_t* iterator;
    typedef boost::uint8_t const* const_iterator;
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;

    static size_type static_size() {return 16;}

    iterator begin() {return reinterpret_cast<iterator>(&data1);} /* throw() */
    const_iterator begin() const {return reinterpret_cast<const_iterator>(&data1);} /* throw() */
    iterator end() {return reinterpret_cast<iterator>(&data1) + size();} /* throw() */
    const_iterator end() const {return reinterpret_cast<const_iterator>(&data1) + size();} /* throw() */

    size_type size() const {return static_size();} /* throw() */
};

inline bool operator==(i_guid const& lhs, i_guid const& rhs) /* throw() */
{
    return std::equal(lhs.begin(), lhs.end(), rhs.begin());
}

inline bool operator!=(i_guid const& lhs, i_guid const& rhs) /* throw() */
{
    return !(lhs == rhs);
}

inline bool operator<(i_guid const& lhs, i_guid const& rhs) /* throw() */
{
    return std::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
}

inline bool operator>(i_guid const& lhs, i_guid const& rhs) /* throw() */
{
    return rhs < lhs;
}
inline bool operator<=(i_guid const& lhs, i_guid const& rhs) /* throw() */
{
    return !(rhs < lhs);
}

inline bool operator>=(i_guid const& lhs, i_guid const& rhs) /* throw() */
{
    return !(lhs < rhs);
}

enum i_result
{
    i_success,
    i_error,
    i_not_found,
    i_not_implemented
};

inline bool i_result_success(i_result status) {return status == i_success;}

/*
    Base interface for all dvblink interfaces
*/
struct i_base_object;
typedef boost::shared_ptr<i_base_object> i_base_object_t;

struct i_base_object
{
    i_base_object() {}
    virtual ~i_base_object() {}

    virtual i_result __stdcall query_interface(const base_id_t& requestor_id, const i_guid& iid, i_base_object_t& obj) = 0;
    virtual const boost::uuids::uuid& __stdcall get_uid() = 0;

    static void __stdcall release(i_base_object* obj) {delete obj;}

    template<class T>
    i_result query_interface(const base_id_t& requestor_id, const i_guid& iid, boost::shared_ptr<T>& obj)
    {
        i_base_object_t base;
        i_result res = query_interface(requestor_id, iid, base);
        if (res == i_success)
        {
            //
            // Workaround for gcc 4.3.2 (iomega's toolchain)
            //
            //obj = boost::dynamic_pointer_cast<typename boost::shared_ptr<T>::element_type, i_base_object>(base);
            
	    //obj = boost::shared_ptr<T>(base, boost::detail::static_cast_tag());
            //static_cast_tag is not a member of modern boost anymore
            obj = boost::static_pointer_cast<T>(base);
        }
        return res;
    }
};

/*
    Entry point exported by any modules for querying needed interface
*/
#define MODULE_ENTRY_POINT_NAME     "dvblink_query_interface"

typedef i_result (__stdcall *dvblink_query_interface_fn_t)(const base_id_t& requestor_id, const i_guid& iid, i_base_object_t& obj);
extern "C" i_result __stdcall dvblink_query_interface(const base_id_t& requestor_id, const i_guid& iid, i_base_object_t& obj);

template<class T>
i_result query_interface(dvblink_query_interface_fn_t qi, const base_id_t& requestor_id, const i_guid& iid, boost::shared_ptr<T>& obj)
{
    i_base_object_t base;
    i_result res = qi(requestor_id, iid, base);
    if (res == i_success)
    {
        //
        // Workaround for gcc 4.3.2 (iomega's toolchain)
        //
        //obj = boost::dynamic_pointer_cast<typename boost::shared_ptr<T>::element_type, i_base_object>(base);
        //obj = boost::shared_ptr<T>(base, boost::detail::static_cast_tag());
        //static_cast_tag is not a member of modern boost anymore
        obj = boost::static_pointer_cast<T>(base);
    }
    return res;
}

template<class T>
boost::shared_ptr<T> share_object_safely(T* obj)
{
    return boost::shared_ptr<T>(obj, &T::release);
}

template<class T>
boost::shared_ptr<T> base_to_derived(i_base_object_t const& obj)
{
    //return boost::dynamic_pointer_cast<T, i_base_object>(obj);
    //return boost::shared_ptr<T>(obj, boost::detail::static_cast_tag());
    //static_cast_tag is not a member of modern boost anymore
    return boost::static_pointer_cast<T>(obj);

}

#pragma pack(pop)

} //dvblink

#ifdef _WIN32
#pragma warning(pop)
#endif
