/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>
#include <boost/cstdint.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include "boost/archive/iterators/transform_width.hpp"

namespace dvblink
{

template<typename InputContainerType>
    inline void base64_encode(const InputContainerType& cont, std::string& enc)
{
    using namespace boost::archive::iterators;
    typedef base64_from_binary<transform_width<typename InputContainerType::const_pointer, 6, 8, boost::uint8_t> > base64_t;
    typename InputContainerType::const_pointer ptr = &cont[0];
    enc.assign(base64_t(ptr), base64_t(ptr + cont.size()));
    
    size_t padding_size = (4 - (enc.size() & 3)) & 3;
    
    if (padding_size > 0)
    {
        enc.append(padding_size, '=');
    }
}

template<typename InputContainerType>
    inline void base64_encode_for_url(const InputContainerType& cont, std::string& enc)
{
//RFC 3548 compatible
    base64_encode(cont, enc);
    std::replace(enc.begin(), enc.end(), '+', '-');
    std::replace(enc.begin(), enc.end(), '/', '_');
}

template<typename Container, typename CharT>
    inline void base64_decode(const std::basic_string<CharT>& str, Container& cont)
{
    typedef boost::archive::iterators::transform_width<boost::archive::iterators::binary_from_base64<CharT *>, 8, 6, boost::uint8_t> binary_t;

    typename std::basic_string<CharT>::size_type pos = str.find_last_not_of(CharT('='));
    // calculate how many characters we need to process
    pos = (pos == std::basic_string<CharT>::npos) ? str.size() : pos + 1;
    cont.assign(binary_t(&str[0]), binary_t(&str[0] + pos));
}

template<typename Container, typename CharT>
    inline void base64_decode_for_url(const std::basic_string<CharT>& str, Container& cont)
{
//RFC 3548 compatible
    std::string copy_str(str.begin(), str.end());
    std::replace(copy_str.begin(), copy_str.end(), CharT('-'), CharT('+'));
    std::replace(copy_str.begin(), copy_str.end(), CharT('_'), CharT('/'));
    base64_decode(copy_str, cont);
}

}

/*
usage:
    std::string str = "test";
    std::string res = base64_encode<string>(str);
    std::string res1 = base64_decode<string, char>(res);
*/
