/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <dl_types.h>
#include <dl_pugixml_helper.h>
#include <dl_devices.h>
#include <dl_provider_info.h>
#include <dl_locale_strings.h>
#include <dl_scan_info.h>
#include <dl_language_settings.h>
#include <dl_parameters_serializer.h>

namespace dvblex { 

const std::string devices_root_node                     = "devices";
const std::string devices_device_node                   = "device";
const std::string devices_id_node                       = "id";
const std::string devices_name_node                     = "name";
const std::string devices_state_node                    = "state";
const std::string devices_status_node                   = "status";
const std::string devices_standards_node                = "standards";
const std::string devices_standard_node                 = "standard";
const std::string devices_providers_node                = "providers";
const std::string devices_provider_node                = "provider";
const std::string devices_provider_id_node                = "provider_id";
const std::string devices_provider_name_node                = "provider_name";
const std::string devices_provider_country_node                = "provider_country";
const std::string devices_provider_desc_node                = "provider_desc";
const std::string devices_can_be_deleted_node               = "can_be_deleted";
const std::string devices_has_settings_node               = "has_settings";

const std::string devices_consumers_node                       = "consumers";
const std::string devices_bitrate_node                       = "bitrate";
const std::string devices_packets_node                       = "packets";
const std::string devices_errors_node                       = "errors";
const std::string devices_dscnt_node                       = "dscnt";

static const char* device_info_status_strings[] = {"idle", "idle", "streaming", "scanning", "networks_scanned", "scan_finished", "epg_scanning", "epg_scan_finished"}; //unknown state is handled as idle
static const char* device_info_state_strings[] = {"unknown", "new", "active", "missing", "error"};

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const device_info_list_t& dil)
{
    std::stringstream buf;

    pugi::xml_node devices_node = doc.append_child(devices_root_node.c_str());
    if (devices_node != NULL)
    {
        for (size_t i=0; i<dil.size(); i++)
        {
            pugi::xml_node device_node = dvblink::pugixml_helpers::new_child(devices_node, devices_device_node);
			if (device_node != NULL)
			{
                dvblink::pugixml_helpers::new_child(device_node, devices_id_node, dil[i].descriptor_.id_.to_string());
                dvblink::pugixml_helpers::new_child(device_node, devices_name_node, dil[i].descriptor_.name_.to_string());
                dvblink::pugixml_helpers::new_child(device_node, devices_status_node, device_info_status_strings[dil[i].descriptor_.state_]);
                dvblink::pugixml_helpers::new_child(device_node, devices_state_node, device_info_state_strings[dil[i].state_]);

                buf.clear(); buf.str("");
                buf << dil[i].descriptor_.supported_standards_;
                dvblink::pugixml_helpers::new_child(device_node, devices_standards_node, buf.str());

                if (dil[i].descriptor_.can_be_deleted_)
                    dvblink::pugixml_helpers::new_child(device_node, devices_can_be_deleted_node, dvblink::pugixml_helpers::xmlnode_value_true);

                if (dil[i].descriptor_.has_settings_)
                    dvblink::pugixml_helpers::new_child(device_node, devices_has_settings_node, dvblink::pugixml_helpers::xmlnode_value_true);
                
                //headends
                pugi::xml_node providers_node = dvblink::pugixml_helpers::new_child(device_node, devices_providers_node);
			    if (providers_node != NULL)
			    {
                    for (size_t j=0; j<dil[i].headends_.size(); j++)
                    {
                        pugi::xml_node provider_node = dvblink::pugixml_helpers::new_child(providers_node, devices_provider_node);
			            if (provider_node != NULL)
			            {
                            dvblink::pugixml_helpers::new_child(provider_node, devices_provider_id_node, dil[i].headends_[j].id_.get());
                            dvblink::pugixml_helpers::new_child(provider_node, devices_provider_name_node, dil[i].headends_[j].name_.get());
                            dvblink::pugixml_helpers::new_child(provider_node, devices_provider_desc_node, dil[i].headends_[j].desc_.get());
                        }
                    }
                }
			}
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, dvblex::device_info_list_t& dil)
{
    if (NULL != node)
    {
        std::string str;

        pugi::xml_node device_node = node.first_child();
        while (device_node != NULL)
        {
            if (boost::iequals(device_node.name(), devices_device_node))
            {
                device_info_t di;

                if (dvblink::pugixml_helpers::get_node_value(device_node, devices_id_node, str))
                    di.descriptor_.id_ = str;

                if (dvblink::pugixml_helpers::get_node_value(device_node, devices_name_node, str))
                    di.descriptor_.name_ = str;

                if (dvblink::pugixml_helpers::get_node_value(device_node, devices_status_node, str))
                {
                    for (size_t i=0; i<sizeof(device_info_status_strings) / sizeof(char*); i++)
                    {
                        if (boost::iequals(device_info_status_strings[i], str))
                            di.descriptor_.state_ = (device_state_e)i;
                    }
                }

                di.descriptor_.can_be_deleted_ = dvblink::pugixml_helpers::get_node_value(device_node, devices_can_be_deleted_node, str) &&
                    boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);

                di.descriptor_.has_settings_ = dvblink::pugixml_helpers::get_node_value(device_node, devices_has_settings_node, str) &&
                    boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);

                if (dvblink::pugixml_helpers::get_node_value(device_node, devices_state_node, str))
                {
                    for (size_t i=0; i<sizeof(device_info_state_strings) / sizeof(char*); i++)
                    {
                        if (boost::iequals(device_info_state_strings[i], str))
                            di.state_ = (device_info_state_e)i;
                    }
                }

                boost::uint32_t u32v;
                if (dvblink::pugixml_helpers::get_node_value(device_node, devices_standards_node, str))
                {
                    dvblink::engine::string_conv::apply(str.c_str(), u32v, (boost::uint32_t)0);
                    di.descriptor_.supported_standards_ = u32v;
                }

                pugi::xml_node providers_node = device_node.child(devices_providers_node.c_str());
                if (providers_node != NULL)
                {
                    pugi::xml_node provider_node = providers_node.first_child();
                    while (provider_node != NULL)
                    {
                        if (boost::iequals(provider_node.name(), devices_provider_node))
                        {
                            headend_info_t pi;

                            if (dvblink::pugixml_helpers::get_node_value(provider_node, devices_provider_id_node, str))
                                pi.id_ = str;

                            if (dvblink::pugixml_helpers::get_node_value(provider_node, devices_provider_name_node, str))
                                pi.name_ = str;

                            if (dvblink::pugixml_helpers::get_node_value(provider_node, devices_provider_desc_node, str))
                                pi.desc_ = str;

                            di.headends_.push_back(pi);
                        }

                        provider_node = provider_node.next_sibling();
                    }

                }

                dil.push_back(di);
            }

            device_node = device_node.next_sibling();
        }
    }
    return node;
}


////////////////////////////////////////////////////////////////////
struct get_scanners_request_t
{
    get_scanners_request_t() :
        standard_(st_unknown)
    {}

    dvblink::device_id_t device_id_;
    dvblink::standard_set_t standard_;
};

const std::string devices_get_scanners_root_node                = "get_scanners";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_scanners_request_t& get_scanners_request)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, devices_device_node, str))
            get_scanners_request.device_id_ = str;

        if (dvblink::pugixml_helpers::get_node_value(node, devices_standard_node, str))
        {
            boost::uint32_t ul;
            dvblink::engine::string_conv::apply(str.c_str(), ul, (boost::uint32_t)st_unknown);
            get_scanners_request.standard_ = ul;
        }
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_scanners_request_t& get_scanners_request)
{
    pugi::xml_node node = doc.append_child(devices_get_scanners_root_node.c_str());

    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, devices_device_node, get_scanners_request.device_id_.get());

        std::stringstream buf;

        buf.clear(); buf.str("");
        buf << get_scanners_request.standard_.get();
        dvblink::pugixml_helpers::new_child(node, devices_standard_node, buf.str());

    }

    return doc;
}

const std::string devices_scanners_node                = "scanners";
const std::string devices_settings_node                = "settings";
const std::string devices_setting_node                 = "setting";
const std::string devices_provider_tag                 = "provider";
const std::string devices_value_node                   = "value";
const std::string devices_desc_node                    = "desc";
const std::string devices_country_node                 = "country";
const std::string devices_network_tag                    = "network";
const std::string devices_networks_tag                    = "networks";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const provider_info_map_t& pim)
{
    std::stringstream buf;

    pugi::xml_node scanners_node = doc.append_child(devices_scanners_node.c_str());
    if (scanners_node != NULL)
    {
        provider_info_map_t::const_iterator pim_it = pim.begin();
        while (pim_it != pim.end())
        {
            pugi::xml_node standard_node = dvblink::pugixml_helpers::new_child(scanners_node, devices_standard_node);
			if (standard_node != NULL)
			{
                buf.clear(); buf.str("");
                buf << pim_it->second.standard_;
                dvblink::pugixml_helpers::add_node_attribute(standard_node, devices_id_node, buf.str());
                dvblink::pugixml_helpers::add_node_attribute(standard_node, devices_name_node, pim_it->second.standard_name_.get());

                pugi::xml_node settings_node = dvblink::pugixml_helpers::new_child(standard_node, devices_settings_node);
			    if (settings_node != NULL)
			    {
                    //providers
                    pugi::xml_node setting_node = dvblink::pugixml_helpers::new_child(settings_node, devices_setting_node);
			        if (setting_node != NULL)
			        {
                        dvblink::pugixml_helpers::add_node_attribute(setting_node, devices_id_node, devices_provider_tag);
                        dvblink::pugixml_helpers::add_node_attribute(setting_node, devices_name_node, dvblink::engine::language_settings::GetInstance()->GetItemName(scan_providers_name));

                        provider_info_list_t::const_iterator provider_it = pim_it->second.providers_.begin();
                        while (provider_it != pim_it->second.providers_.end())
                        {
                            if (provider_it->param_container_.is_empty())
                            {
                                //value
                                pugi::xml_node value_node = dvblink::pugixml_helpers::new_child(setting_node, devices_value_node);
			                    if (value_node != NULL)
			                    {
                                    dvblink::pugixml_helpers::add_node_attribute(value_node, devices_id_node, provider_it->id_.get());
                                    dvblink::pugixml_helpers::add_node_attribute(value_node, devices_name_node, provider_it->name_.get());
                                    dvblink::pugixml_helpers::add_node_attribute(value_node, devices_desc_node, provider_it->desc_.get());
                                    if (!provider_it->country_.empty())
                                        dvblink::pugixml_helpers::add_node_attribute(value_node, devices_country_node, provider_it->country_.get());
		                        }
                            } else
                            {
                                write_to_node(provider_it->param_container_, setting_node);
                            }
                            ++provider_it;
                        }
			        }
                    //extra parameters
                    if (!pim_it->second.parameters_.is_empty())
                    {
                        write_to_node(pim_it->second.parameters_, settings_node);
                    }
			    }
			}
            ++pim_it;
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, provider_info_map_t& pim)
{
    std::string str;

    if (NULL != node)
    {
        pugi::xml_node standard_node = node.first_child();
        while (standard_node != NULL)
        {
            if (boost::iequals(standard_node.name(), devices_standard_node))
            {
                provider_description_t pd;

                boost::uint32_t u32v;
                if (dvblink::pugixml_helpers::get_node_attribute(standard_node, devices_id_node, str))
                {
                    dvblink::engine::string_conv::apply(str.c_str(), u32v, (boost::uint32_t)st_unknown);
                    pd.standard_ = (source_type_e)u32v;
                }

                if (dvblink::pugixml_helpers::get_node_attribute(standard_node, devices_name_node, str))
                    pd.standard_name_ = str;

                pugi::xml_node settings_node = standard_node.child(devices_settings_node.c_str());
                if (settings_node != NULL)
                {
                    //providers
                    pugi::xml_node setting_node = settings_node.child(devices_setting_node.c_str());
                    if (setting_node != NULL)
                    {
                        pugi::xml_node value_node = setting_node.first_child();
                        while (value_node != NULL)
                        {
                            if (boost::iequals(value_node.name(), devices_value_node))
                            {
                                //value
                                provider_info_t pi;

                                if (dvblink::pugixml_helpers::get_node_attribute(value_node, devices_id_node, str))
                                    pi.id_ = str;

                                if (dvblink::pugixml_helpers::get_node_attribute(value_node, devices_name_node, str))
                                    pi.name_ = str;

                                if (dvblink::pugixml_helpers::get_node_attribute(value_node, devices_desc_node, str))
                                    pi.desc_ = str;

                                if (dvblink::pugixml_helpers::get_node_attribute(value_node, devices_country_node, str))
                                    pi.country_ = str;

                                pd.providers_.push_back(pi);
                            }
                            if (boost::iequals(value_node.name(), params_container_node))
                            {
                                //container
                                provider_info_t pi;
                                read_from_node(value_node, pi.param_container_);
                                pi.id_ = pi.param_container_.id_.get();
                                pi.name_ = pi.param_container_.name_.get();
                                pi.desc_ = pi.param_container_.desc_.get();
                                pi.standard_ = pd.standard_;

                                pd.providers_.push_back(pi);
                            }
                            value_node = value_node.next_sibling();
                        }

                    }
                    //parameters
                    pugi::xml_node container_node = settings_node.child(params_container_node.c_str());
                    if (container_node != NULL)
                        read_from_node(container_node, pd.parameters_);

                }
                pim[pd.standard_] = pd;
            }
            standard_node = standard_node.next_sibling();
        }
    }

    return node;
}

////////////////////////////////////////////////////////////////////
struct start_scan_request_t
{
    dvblink::device_id_t device_id_;
    dvblex::concise_param_map_t parameters_;
};

const std::string start_scan_root_node                = "start_scan";

inline pugi::xml_node& operator>> (pugi::xml_node& node, start_scan_request_t& start_scan_request)
{
    if (NULL != node)
    {
        pugi::xml_node value_node = node.first_child();
        while (value_node != NULL)
        {
            std::string key = value_node.name();
            std::string value = value_node.child_value();
            if (!value.empty())
            {
                if (boost::iequals(key, devices_device_node))
                    start_scan_request.device_id_ = value;
                else
                    start_scan_request.parameters_[key] = value;
            }

            value_node = value_node.next_sibling();
        }
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const start_scan_request_t& start_scan_request)
{
    pugi::xml_node node = doc.append_child(start_scan_root_node.c_str());

    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, devices_device_node, start_scan_request.device_id_.get());

        dvblex::concise_param_map_t::const_iterator it = start_scan_request.parameters_.begin();
        while (it != start_scan_request.parameters_.end())
        {
            dvblink::pugixml_helpers::new_child(node, it->first.get(), it->second.get());
            ++it;
        }
    }

    return doc;
}

////////////////////////////////////////////////////////////////////
struct start_scan_response_t
{
    dvblex::channel_start_scan_result_e result_;
};

const std::string devices_scan_start_result_node                 = "scan_start_result";

inline pugi::xml_node& operator>> (pugi::xml_node& node, start_scan_response_t& start_scan_response)
{
    if (NULL != node)
    {
        std::string str;
        boost::uint32_t u32v;
        if (dvblink::pugixml_helpers::get_node_value(node, devices_scan_start_result_node, str))
        {
            dvblink::engine::string_conv::apply(str.c_str(), u32v, (boost::uint32_t)cssr_error);
            start_scan_response.result_ = (channel_start_scan_result_e)u32v;
        }
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const start_scan_response_t& start_scan_response)
{
    pugi::xml_node node = doc.append_child(start_scan_root_node.c_str());

    if (node != NULL)
    {
        std::stringstream buf;
        buf.clear(); buf.str("");
        buf << start_scan_response.result_;
        dvblink::pugixml_helpers::new_child(node, devices_scan_start_result_node, buf.str());
    }

    return doc;
}

////////////////////////////////////////////////////////////////////
struct get_rescan_provider_settings_request_t
{
    dvblink::device_id_t device_id_;
    dvblink::headend_id_t headend_id_;
};

const std::string rescan_provider_settings_request_root_node                = "rescan_provider_settings";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_rescan_provider_settings_request_t& request)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, devices_device_node, str))
            request.device_id_ = str;

        if (dvblink::pugixml_helpers::get_node_value(node, devices_provider_id_node, str))
            request.headend_id_ = str;
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_rescan_provider_settings_request_t& request)
{
    pugi::xml_node node = doc.append_child(rescan_provider_settings_request_root_node.c_str());

    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, devices_device_node, request.device_id_.get());
        dvblink::pugixml_helpers::new_child(node, devices_provider_id_node, request.headend_id_.get());
    }

    return doc;
}

////////////////////////////////////////////////////////////////////

struct get_rescan_provider_settings_response_t
{
    dvblex::parameters_container_t settings_;
};

const std::string rescan_provider_settings_node                = "settings";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_rescan_provider_settings_response_t& request)
{
    if (NULL != node)
    {
        pugi::xml_node settings_node = node.child(params_container_node.c_str());
        if (settings_node != NULL)
            read_from_node(settings_node, request.settings_);
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_rescan_provider_settings_response_t& request)
{
    pugi::xml_node node = doc.append_child(rescan_provider_settings_node.c_str());

    if (node != NULL)
    {
        write_to_node(request.settings_, node);
    }

    return doc;
}

////////////////////////////////////////////////////////////////////

struct rescan_provider_request_t
{
    dvblink::device_id_t device_id_;
    dvblink::headend_id_t headend_id_;
    dvblex::concise_param_map_t settings_;
};

const std::string rescan_provider_root_node                = "rescan_provider";
const std::string rescan_provider_settings_settings_node   = "settings";

inline pugi::xml_node& operator>> (pugi::xml_node& node, rescan_provider_request_t& rescan_provider_request)
{
    if (NULL != node)
    {
        std::string str;

        if (dvblink::pugixml_helpers::get_node_value(node, devices_device_node, str))
            rescan_provider_request.device_id_ = str;

        if (dvblink::pugixml_helpers::get_node_value(node, devices_provider_id_node, str))
            rescan_provider_request.headend_id_ = str;
        
        pugi::xml_node settings_node = node.child(rescan_provider_settings_settings_node.c_str());
        if (settings_node != NULL)
            read_from_node(settings_node, rescan_provider_request.settings_);
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const rescan_provider_request_t& rescan_provider_request)
{
    pugi::xml_node node = doc.append_child(rescan_provider_root_node.c_str());

    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, devices_device_node, rescan_provider_request.device_id_.get());
        dvblink::pugixml_helpers::new_child(node, devices_provider_id_node, rescan_provider_request.headend_id_.get());

        pugi::xml_node settings_node = dvblink::pugixml_helpers::new_child(node, rescan_provider_settings_settings_node);
        write_to_node(rescan_provider_request.settings_, settings_node);
    }

    return doc;
}


////////////////////////////////////////////////////////////////////
struct apply_scan_request_t
{
    dvblink::device_id_t device_id_;
};

const std::string apply_scan_root_node                = "apply_scan";

inline pugi::xml_node& operator>> (pugi::xml_node& node, apply_scan_request_t& apply_scan_request)
{
    std::string str;
    if (NULL != node)
    {
        if (dvblink::pugixml_helpers::get_node_value(node, devices_device_node, str))
            apply_scan_request.device_id_ = str;
    }
    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const apply_scan_request_t& apply_scan_request)
{
    pugi::xml_node node = doc.append_child(apply_scan_root_node.c_str());

    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, devices_device_node, apply_scan_request.device_id_.get());
    }

    return doc;
}

////////////////////////////////////////////////////////////////////
struct cancel_scan_request_t
{
    dvblink::device_id_t device_id_;
};

const std::string cancel_scan_root_node                = "cancel_scan";

inline pugi::xml_node& operator>> (pugi::xml_node& node, cancel_scan_request_t& cancel_scan_request)
{
    std::string str;
    if (NULL != node)
    {
        if (dvblink::pugixml_helpers::get_node_value(node, devices_device_node, str))
            cancel_scan_request.device_id_ = str;
    }
    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const cancel_scan_request_t& cancel_scan_request)
{
    pugi::xml_node node = doc.append_child(cancel_scan_root_node.c_str());

    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, devices_device_node, cancel_scan_request.device_id_.get());
    }

    return doc;
}

////////////////////////////////////////////////////////////////////
struct get_networks_request_t
{
    dvblink::device_id_t device_id_;
};

const std::string get_networks_root_node                = "get_networks";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_networks_request_t& get_networks_request)
{
    std::string str;
    if (NULL != node)
    {
        if (dvblink::pugixml_helpers::get_node_value(node, devices_device_node, str))
            get_networks_request.device_id_ = str;
    }
    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_networks_request_t& get_networks_request)
{
    pugi::xml_node node = doc.append_child(get_networks_root_node.c_str());

    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, devices_device_node, get_networks_request.device_id_.get());
    }

    return doc;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const scanned_network_list_t& snl)
{
    pugi::xml_node networks_node = doc.append_child(devices_networks_tag.c_str());

    if (networks_node != NULL)
    {
        pugi::xml_node settings_node = dvblink::pugixml_helpers::new_child(networks_node, devices_settings_node);
        if (settings_node != NULL)
        {
            pugi::xml_node setting_node = dvblink::pugixml_helpers::new_child(settings_node, devices_setting_node);
            if (setting_node != NULL)
            {
                dvblink::pugixml_helpers::add_node_attribute(setting_node, devices_id_node, devices_network_tag);
                dvblink::pugixml_helpers::add_node_attribute(setting_node, devices_name_node, dvblink::engine::language_settings::GetInstance()->GetItemName(available_networks_name));
                for (size_t i=0; i<snl.size(); i++)
                {
                    pugi::xml_node value_node = dvblink::pugixml_helpers::new_child(setting_node, devices_value_node);
                    if (value_node != NULL)
                    {
                        dvblink::pugixml_helpers::add_node_attribute(value_node, devices_id_node, snl[i].network_id_.get());
                        dvblink::pugixml_helpers::add_node_attribute(value_node, devices_name_node, snl[i].name_.get());
                    }
                }
            }
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, scanned_network_list_t& snl)
{
    std::string str;
    if (NULL != node)
    {
        pugi::xml_node settings_node = node.child(devices_settings_node.c_str());
        if (settings_node != NULL)
        {
            pugi::xml_node setting_node = settings_node.child(devices_setting_node.c_str());
            if (setting_node != NULL)
            {
                pugi::xml_node value_node = setting_node.first_child();
                while (value_node != NULL)
                {
                    scanned_network_t sn;

                    if (dvblink::pugixml_helpers::get_node_attribute(value_node, devices_id_node, str))
                        sn.network_id_ = str;

                    if (dvblink::pugixml_helpers::get_node_attribute(value_node, devices_name_node, str))
                        sn.name_ = str;

                    snl.push_back(sn);

                    value_node = value_node.next_sibling();
                }
            }
        }
    }
    return node;
}

////////////////////////////////////////////////////////////////////
struct get_device_status_request_t
{
    dvblink::device_id_t device_id_;
};

const std::string get_device_status_root_node                = "get_device_status";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_device_status_request_t& get_device_status_request)
{
    std::string str;
    if (NULL != node)
    {
        if (dvblink::pugixml_helpers::get_node_value(node, devices_device_node, str))
            get_device_status_request.device_id_ = str;
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_device_status_request_t& get_device_status_request)
{
    pugi::xml_node node = doc.append_child(get_device_status_root_node.c_str());

    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, devices_device_node, get_device_status_request.device_id_.get());
    }

    return doc;
}

const std::string devices_status_root_node               = "device_status";
const std::string devices_scanning_node               = "scanning";
const std::string devices_progress_node               = "progress";
const std::string devices_channels_node               = "channels";
const std::string devices_channel_node               = "channel";
const std::string devices_transponders_node               = "transponders";
const std::string devices_transponder_node               = "transponder";
const std::string devices_lock_node               = "lock";
const std::string devices_strength_node               = "strength";
const std::string devices_quality_node               = "quality";
const std::string devices_streaming_node               = "streaming";

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const device_status_ex_t& dse)
{
    std::stringstream buf;

    pugi::xml_node status_node = doc.append_child(devices_status_root_node.c_str());

    if (status_node != NULL)
    {
        dvblink::pugixml_helpers::new_child(status_node, devices_status_node, device_info_status_strings[dse.device_status_.state_]);
        dvblink::pugixml_helpers::new_child(status_node, devices_device_node, dse.device_status_.id_.get());

        if (dse.device_status_.state_ == ds_channel_scan_in_progress || dse.device_status_.state_ == ds_networks_scanned || 
            dse.device_status_.state_ == ds_channel_scan_finished)
        {
            //scanning info
            pugi::xml_node scan_info_node = dvblink::pugixml_helpers::new_child(status_node, devices_scanning_node);
            if (scan_info_node != NULL)
            {
                buf.clear(); buf.str("");
                buf << dse.device_status_.scan_progress_.progress_;
                dvblink::pugixml_helpers::new_child(scan_info_node, devices_progress_node, buf.str());

                buf.clear(); buf.str("");
                buf << dse.device_status_.scan_progress_.channels_found_;
                dvblink::pugixml_helpers::new_child(scan_info_node, devices_channels_node, buf.str());

                //transponders
                pugi::xml_node transponders_node = dvblink::pugixml_helpers::new_child(scan_info_node, devices_transponders_node);
                if (transponders_node)
                {
                    for (size_t i=0; i<dse.scan_log_.size(); i++)
                    {
                        pugi::xml_node transponder_node = dvblink::pugixml_helpers::new_child(transponders_node, devices_transponder_node, dse.scan_log_[i].comment_);
                        if (transponder_node != NULL)
                        {
                            dvblink::pugixml_helpers::add_node_attribute(transponder_node, devices_id_node, dse.scan_log_[i].scan_data_.get());

                            buf.clear(); buf.str("");
                            buf << dse.scan_log_[i].channels_found_;
                            dvblink::pugixml_helpers::add_node_attribute(transponder_node, devices_channels_node, buf.str());

                            buf.clear(); buf.str("");
                            buf << dse.scan_log_[i].signal_info_.lock_.get();
                            dvblink::pugixml_helpers::add_node_attribute(transponder_node, devices_lock_node, buf.str());

                            buf.clear(); buf.str("");
                            buf << dse.scan_log_[i].signal_info_.signal_strength_;
                            dvblink::pugixml_helpers::add_node_attribute(transponder_node, devices_strength_node, buf.str());

                            buf.clear(); buf.str("");
                            buf << dse.scan_log_[i].signal_info_.signal_quality_;
                            dvblink::pugixml_helpers::add_node_attribute(transponder_node, devices_quality_node, buf.str());
                        }
                    }
                }
            }
        }
        if (dse.device_status_.state_ == ds_streaming)
        {
            //streaming info
            pugi::xml_node stream_info_node = dvblink::pugixml_helpers::new_child(status_node, devices_streaming_node);
            if (stream_info_node != NULL)
            {
                for (size_t i=0; i<dse.device_status_.stream_channels_.size(); i++)
                {
                    const stream_channel_stats_t& sch = dse.device_status_.stream_channels_[i];

                    pugi::xml_node channel_node = dvblink::pugixml_helpers::new_child(stream_info_node, devices_channel_node);
                    if (channel_node != NULL)
                    {
                        dvblink::pugixml_helpers::add_node_attribute(channel_node, devices_id_node, sch.channel_id_.to_string());
                        dvblink::pugixml_helpers::add_node_attribute(channel_node, devices_name_node, sch.channel_name_.to_string());

                        buf.clear(); buf.str("");
                        buf << dse.device_status_.signal_info_.lock_.get();
                        dvblink::pugixml_helpers::add_node_attribute(channel_node, devices_lock_node.c_str(), buf.str());

                        buf.clear(); buf.str("");
                        buf << dse.device_status_.signal_info_.signal_strength_;
                        dvblink::pugixml_helpers::add_node_attribute(channel_node, devices_strength_node, buf.str());

                        buf.clear(); buf.str("");
                        buf << dse.device_status_.signal_info_.signal_quality_;
                        dvblink::pugixml_helpers::add_node_attribute(channel_node, devices_quality_node, buf.str());

                        buf.clear(); buf.str("");
                        buf << sch.consumer_num_;
                        dvblink::pugixml_helpers::add_node_attribute(channel_node, devices_consumers_node, buf.str());

                        buf.clear(); buf.str("");
                        buf << sch.stream_stats_.bitrate_;
                        dvblink::pugixml_helpers::add_node_attribute(channel_node, devices_bitrate_node, buf.str());

                        buf.clear(); buf.str("");
                        buf << sch.stream_stats_.packet_count_;
                        dvblink::pugixml_helpers::add_node_attribute(channel_node, devices_packets_node, buf.str());

                        buf.clear(); buf.str("");
                        buf << sch.stream_stats_.error_packet_count_;
                        dvblink::pugixml_helpers::add_node_attribute(channel_node, devices_errors_node, buf.str());

                        buf.clear(); buf.str("");
                        buf << sch.stream_stats_.discontinuity_count_;
                        dvblink::pugixml_helpers::add_node_attribute(channel_node, devices_dscnt_node, buf.str());

                    }
                }
            }
        }
    }

    return doc;
}

inline pugi::xml_node& operator>> (pugi::xml_node& node, device_status_ex_t& dse)
{
    std::string str;
    if (NULL != node)
    {
        if (dvblink::pugixml_helpers::get_node_value(node, devices_device_node, str))
            dse.device_status_.id_ = str;

        if (dvblink::pugixml_helpers::get_node_value(node, devices_status_node, str))
        {
            for (size_t i=0; i<sizeof(device_info_status_strings) / sizeof(char*); i++)
            {
                if (boost::iequals(device_info_status_strings[i], str))
                    dse.device_status_.state_ = (device_state_e)i;
            }
        }

        pugi::xml_node scan_info_node = node.child(devices_scanning_node.c_str());
        if (scan_info_node != NULL)
        {
            if (dvblink::pugixml_helpers::get_node_value(scan_info_node, devices_progress_node, str))
                dvblink::engine::string_conv::apply(str.c_str(), dse.device_status_.scan_progress_.progress_, (boost::uint32_t)0);

            if (dvblink::pugixml_helpers::get_node_value(scan_info_node, devices_channels_node, str))
                dvblink::engine::string_conv::apply(str.c_str(), dse.device_status_.scan_progress_.channels_found_, (boost::uint32_t)0);

            pugi::xml_node transponders_node = scan_info_node.child(devices_transponders_node.c_str());
            if (transponders_node != NULL)
            {
                pugi::xml_node transponder_node = transponders_node.first_child();
                while (transponder_node != NULL)
                {
                    scan_log_entry_t sle;
                    sle.comment_ = transponder_node.child_value();

                    if (dvblink::pugixml_helpers::get_node_attribute(transponder_node, devices_id_node, str))
                        sle.scan_data_ = str;

                    if (dvblink::pugixml_helpers::get_node_attribute(transponder_node, devices_channels_node, str))
                        dvblink::engine::string_conv::apply(str.c_str(), sle.channels_found_, (boost::uint32_t)0);

                    if (dvblink::pugixml_helpers::get_node_attribute(transponder_node, devices_lock_node, str))
                    {
                        bool b;
                        dvblink::engine::string_conv::apply(str.c_str(), b, false);
                        sle.signal_info_.lock_ = b;
                    }

                    if (dvblink::pugixml_helpers::get_node_attribute(transponder_node, devices_strength_node, str))
                        dvblink::engine::string_conv::apply(str.c_str(), sle.signal_info_.signal_strength_, (boost::uint32_t)0);

                    if (dvblink::pugixml_helpers::get_node_attribute(transponder_node, devices_quality_node, str))
                        dvblink::engine::string_conv::apply(str.c_str(), sle.signal_info_.signal_quality_, (boost::uint32_t)0);

                    dse.scan_log_.push_back(sle);

                    transponder_node = transponder_node.next_sibling();
                }
            }
        }

        pugi::xml_node streaming_node = node.child(devices_streaming_node.c_str());
        if (streaming_node != NULL)
        {
            pugi::xml_node channel_node = streaming_node.first_child();
            while (channel_node != NULL)
            {
                stream_channel_stats_t sch;

                if (dvblink::pugixml_helpers::get_node_attribute(channel_node, devices_id_node, str))
                    sch.channel_id_ = str;

                if (dvblink::pugixml_helpers::get_node_attribute(channel_node, devices_name_node, str))
                    sch.channel_name_ = str;

                if (dvblink::pugixml_helpers::get_node_attribute(channel_node, devices_lock_node, str))
                {
                    bool b;
                    dvblink::engine::string_conv::apply(str.c_str(), b, false);
                    dse.device_status_.signal_info_.lock_ = b;
                }

                if (dvblink::pugixml_helpers::get_node_attribute(channel_node, devices_strength_node, str))
                    dvblink::engine::string_conv::apply(str.c_str(), dse.device_status_.signal_info_.signal_strength_, (boost::uint32_t)0);

                if (dvblink::pugixml_helpers::get_node_attribute(channel_node, devices_quality_node, str))
                    dvblink::engine::string_conv::apply(str.c_str(), dse.device_status_.signal_info_.signal_quality_, (boost::uint32_t)0);

                if (dvblink::pugixml_helpers::get_node_attribute(channel_node, devices_consumers_node, str))
                    dvblink::engine::string_conv::apply(str.c_str(), sch.consumer_num_, (boost::uint32_t)0);

                if (dvblink::pugixml_helpers::get_node_attribute(channel_node, devices_bitrate_node, str))
                    dvblink::engine::string_conv::apply(str.c_str(), sch.stream_stats_.bitrate_, (boost::uint64_t)0);

                if (dvblink::pugixml_helpers::get_node_attribute(channel_node, devices_packets_node, str))
                    dvblink::engine::string_conv::apply(str.c_str(), sch.stream_stats_.packet_count_, (boost::uint64_t)0);

                if (dvblink::pugixml_helpers::get_node_attribute(channel_node, devices_errors_node, str))
                    dvblink::engine::string_conv::apply(str.c_str(), sch.stream_stats_.error_packet_count_, (boost::uint64_t)0);

                if (dvblink::pugixml_helpers::get_node_attribute(channel_node, devices_dscnt_node, str))
                    dvblink::engine::string_conv::apply(str.c_str(), sch.stream_stats_.discontinuity_count_, (boost::uint64_t)0);

                dse.device_status_.stream_channels_.push_back(sch);

                channel_node = channel_node.next_sibling();
            }
        }
    }
    return node;
}

////////////////////////////////////////////////////////////////////
struct drop_provider_on_device_request_t
{
    drop_provider_on_device_request_t()
    {}

    dvblink::device_id_t device_id_;
    dvblink::headend_id_t headend_id_;
};

const std::string devices_drop_provider_root            = "drop_provider";
const std::string devices_device_id_node                = "device_id";

inline pugi::xml_node& operator>> (pugi::xml_node& node, drop_provider_on_device_request_t& drop_provider_request)
{
    if (NULL != node)
    {
        std::string str;

        if (dvblink::pugixml_helpers::get_node_value(node, devices_provider_id_node, str))
            drop_provider_request.headend_id_ = str;

        if (dvblink::pugixml_helpers::get_node_value(node, devices_device_id_node, str))
            drop_provider_request.device_id_ = str;
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const drop_provider_on_device_request_t& drop_provider_request)
{
    pugi::xml_node node = doc.append_child(devices_drop_provider_root.c_str());

    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, devices_provider_id_node, drop_provider_request.headend_id_.to_string());
        dvblink::pugixml_helpers::new_child(node, devices_device_id_node, drop_provider_request.device_id_.to_string());
    }

    return doc;
}

////////////////////////////////////////////////////////////////////
struct get_device_settings_request_t
{
    dvblink::device_id_t device_id_;
};

const std::string device_settings_request_root_node                = "device_settings";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_device_settings_request_t& request)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, devices_device_node, str))
            request.device_id_ = str;
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_device_settings_request_t& request)
{
    pugi::xml_node node = doc.append_child(device_settings_request_root_node.c_str());

    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, devices_device_node, request.device_id_.get());

    }

    return doc;
}

////////////////////////////////////////////////////////////////////

struct get_device_settings_response_t
{
    dvblex::parameters_container_t settings_;
};

const std::string device_settings_settings_node                = "settings";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_device_settings_response_t& request)
{
    if (NULL != node)
    {
        pugi::xml_node settings_node = node.child(params_container_node.c_str());
        read_from_node(settings_node, request.settings_);
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_device_settings_response_t& request)
{
    pugi::xml_node node = doc.append_child(device_settings_request_root_node.c_str());

    if (node != NULL)
    {
        write_to_node(request.settings_, node);
    }

    return doc;
}

////////////////////////////////////////////////////////////////////

struct set_device_settings_request_t
{
    dvblink::device_id_t device_id_;
    dvblex::concise_param_map_t settings_;
};

inline pugi::xml_node& operator>> (pugi::xml_node& node, set_device_settings_request_t& request)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, devices_device_node, str))
            request.device_id_ = str;

        pugi::xml_node settings_node = node.child(device_settings_settings_node.c_str());
        read_from_node(settings_node, request.settings_);
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const set_device_settings_request_t& request)
{
    pugi::xml_node node = doc.append_child(device_settings_request_root_node.c_str());

    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, devices_device_node, request.device_id_.get());

        pugi::xml_node settings_node = dvblink::pugixml_helpers::new_child(node, device_settings_settings_node);
        write_to_node(request.settings_, settings_node);
    }

    return doc;
}

////////////////////////////////////////////////////////////////////

struct create_manual_device_request_t
{
    dvblex::concise_param_map_t params_;
};

const std::string device_settings_params_node                = "device_params";
const std::string manual_device_request_root_node            = "manual_device";

inline pugi::xml_node& operator>> (pugi::xml_node& node, create_manual_device_request_t& request)
{
    if (NULL != node)
    {
        pugi::xml_node params_node = node.child(device_settings_params_node.c_str());
        read_from_node(params_node, request.params_);
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const create_manual_device_request_t& request)
{
    pugi::xml_node node = doc.append_child(manual_device_request_root_node.c_str());

    if (node != NULL)
    {
        pugi::xml_node params_node = dvblink::pugixml_helpers::new_child(node, device_settings_params_node);
        write_to_node(request.params_, params_node);
    }

    return doc;
}

////////////////////////////////////////////////////////////////////

struct delete_manual_device_request_t
{
    dvblink::device_id_t device_id_;
};

inline pugi::xml_node& operator>> (pugi::xml_node& node, delete_manual_device_request_t& request)
{
    if (NULL != node)
    {
        std::string str;
        if (dvblink::pugixml_helpers::get_node_value(node, devices_device_node, str))
            request.device_id_ = str;
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const delete_manual_device_request_t& request)
{
    pugi::xml_node node = doc.append_child(manual_device_request_root_node.c_str());

    if (node != NULL)
    {
        dvblink::pugixml_helpers::new_child(node, devices_device_node, request.device_id_.get());
    }

    return doc;
}

////////////////////////////////////////////////////////////////////

struct get_device_templates_response_t
{
    dvblex::parameters_container_t templates_;
};

const std::string device_templates_request_root_node            = "device_templates";

inline pugi::xml_node& operator>> (pugi::xml_node& node, get_device_templates_response_t& request)
{
    if (NULL != node)
    {
        pugi::xml_node container_node = node.child(params_container_node.c_str());
        read_from_node(container_node, request.templates_);
    }

    return node;
}

inline pugi::xml_document& operator<< (pugi::xml_document& doc, const get_device_templates_response_t& request)
{
    pugi::xml_node node = doc.append_child(device_templates_request_root_node.c_str());

    if (node != NULL)
    {
        write_to_node(request.templates_, node);
    }

    return doc;
}

} //dvblex
