/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <dl_types.h>
#include <dl_parameters.h>

namespace dvblex { 

enum source_type_e
{
    st_unknown = 0x00000000,
    st_dvb_t_t2 = 0x00000001,
    st_dvb_c = 0x00000002,
    st_dvb_s_s2 = 0x00000004,
    st_atsc = 0x00000008,
    st_cqam = 0x00000010,
    st_iptv = 0x00000020,
    st_file = 0x00000040
};

#define SOURCE_TYPE_DVB_ALL (st_dvb_t_t2 | st_dvb_c | st_dvb_s_s2 | st_atsc | st_cqam)
#define SOURCE_TYPE_DVB_EU (st_dvb_t_t2 | st_dvb_c | st_dvb_s_s2)
#define SOURCE_TYPE_DVB_US (st_atsc | st_cqam)
#define SOURCE_TYPE_ANY (0xFFFFFFFF)

static const source_type_e source_type_values[] = {st_unknown, st_dvb_t_t2, st_dvb_c, st_dvb_s_s2, st_atsc, st_cqam, st_iptv, st_file};
static const char* source_type_strings[] = {"unknown", "dvbt", "dvbc", "dvbs", "atsc", "clearqam", "iptv", "file"};
static const size_t source_type_num = sizeof(source_type_values) / sizeof(source_type_e);

inline std::string get_source_type_string_from_value(source_type_e v)
{
    std::string ret_val;

    for (size_t i=0; i<source_type_num; i++)
    {
        if (source_type_values[i] == v)
        {
            ret_val = source_type_strings[i];
            break;
        }
    }

    return ret_val;
}


enum channel_type_e
{
    ct_tv,
    ct_radio
};

static const int invalid_channel_number_ = -1;
static const boost::uint32_t invalid_stream_id_ = 0;

struct device_channel_t
{

    device_channel_t() :
        tid_(invalid_stream_id_), nid_(invalid_stream_id_), sid_(invalid_stream_id_), encrypted_(false), num_(invalid_channel_number_), sub_num_(invalid_channel_number_), type_(ct_tv)
    {
    }

    dvblink::channel_id_t id_;
    dvblink::channel_name_t name_;
    dvblink::channel_origin_t origin_;
    dvblink::channel_scan_id_t tid_;
    dvblink::channel_scan_id_t nid_;
    dvblink::channel_scan_id_t sid_;
    dvblink::channel_enc_t encrypted_;
    dvblink::channel_number_t num_;
    dvblink::channel_subnumber_t sub_num_;
    dvblink::channel_logo_addr_t logo_;
    channel_type_e type_;
    dvblink::channel_tuning_params_t tune_params_;
    dvblink::channel_comment_t comment_;
};

typedef std::vector<device_channel_t> device_channel_list_t;
typedef std::vector<dvblink::channel_id_t> channel_id_list_t;

struct transponder_t
{
    dvblink::transponder_id_t tr_id_;
    dvblink::transponder_name_t tr_name_;
    device_channel_list_t channels_;
};

typedef std::vector<transponder_t> transponder_list_t;

struct signal_info_t
{
    signal_info_t() :
        lock_(false), signal_strength_(0), signal_quality_(0)
    {
    }

    void reset()
    {
        lock_ = false;
        signal_strength_ = 0;
        signal_quality_ = 0;
    }

    void from_signal_info(const signal_info_t& si)
    {
        lock_ = si.lock_;
        signal_strength_ = si.signal_strength_;
        signal_quality_ = si.signal_quality_;
    }

    dvblink::scan_lock_t lock_;
    boost::uint32_t signal_strength_;
    boost::uint32_t signal_quality_;
};

struct concise_channel_tune_info_t
{
    dvblink::channel_tuning_params_t tuning_params_;
    concise_param_map_t scanner_settings_;
};

enum device_channel_stream_type_e
{
    dcst_raw,
    dcst_chromecast
};

}
