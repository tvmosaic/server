/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <boost/algorithm/string.hpp>
#include <dl_types.h>
#include <dl_pugixml_helper.h>

namespace dvblex { namespace playback {

enum pb_item_type_e
{
    pbit_item_unknown = -1,
    pbit_item_recorded_tv,
    pbit_item_video,
    pbit_item_audio,
    pbit_item_image
};

struct pb_item_t
{
    pb_item_t() :
      item_type_(pbit_item_unknown), can_be_deleted_(false), size_(0), creation_time_(0) 
    {}

    pb_item_t(pb_item_type_e item_type, bool can_be_deleted = false) :
      item_type_(item_type), can_be_deleted_(can_be_deleted), size_(0), creation_time_(0) 
    {}

    dvblink::object_id_t object_id_;
    dvblink::object_id_t parent_id_;
    dvblink::url_address_t url_;
    dvblink::url_address_t thumbnail_;
    pb_item_type_e item_type_;
    bool can_be_deleted_;
    boost::uint64_t size_;
    time_t creation_time_;
};

const std::string item_object_id_node                = "object_id";
const std::string item_parent_id_node                = "parent_id";
const std::string item_url_node                      = "url";
const std::string item_thumbnail_node                = "thumbnail";
const std::string item_can_be_deleted_node           = "can_be_deleted";
const std::string item_size_node                     = "size";
const std::string item_creation_time_node            = "creation_time";

inline void write_to_node(pugi::xml_node& node, const pb_item_t& item)
{
    std::stringstream str_out;

    dvblink::pugixml_helpers::new_child(node, item_object_id_node, item.object_id_.get());
    dvblink::pugixml_helpers::new_child(node, item_parent_id_node, item.parent_id_.get());
    dvblink::pugixml_helpers::new_child(node, item_url_node, item.url_.get());
    dvblink::pugixml_helpers::new_child(node, item_thumbnail_node, item.thumbnail_.get());

    if (item.can_be_deleted_)
        dvblink::pugixml_helpers::new_child(node, item_can_be_deleted_node, dvblink::pugixml_helpers::xmlnode_value_true);

    str_out.clear(); str_out.str("");
    str_out << item.size_;
    dvblink::pugixml_helpers::new_child(node, item_size_node, str_out.str());

    str_out.clear(); str_out.str("");
    str_out << item.creation_time_;
    dvblink::pugixml_helpers::new_child(node, item_creation_time_node, str_out.str());
}

inline void read_from_node(pugi::xml_node& node, pb_item_t& item)
{
    std::stringstream buf;
	std::string str;

	if (dvblink::pugixml_helpers::get_node_value(node, item_object_id_node, str))
		item.object_id_ = str;

	if (dvblink::pugixml_helpers::get_node_value(node, item_parent_id_node, str))
		item.parent_id_ = str;

	if (dvblink::pugixml_helpers::get_node_value(node, item_url_node, str))
		item.url_ = str;

	if (dvblink::pugixml_helpers::get_node_value(node, item_thumbnail_node, str))
		item.thumbnail_ = str;

    item.can_be_deleted_ = dvblink::pugixml_helpers::get_node_value(node, item_can_be_deleted_node, str) &&
        boost::iequals(str, dvblink::pugixml_helpers::xmlnode_value_true);

    boost::uint64_t value;
    if (dvblink::pugixml_helpers::get_node_value(node, item_size_node, str))
    {
        dvblink::engine::string_conv::apply(str.c_str(), value, item.size_);
        item.size_ = value;
    }

    if (dvblink::pugixml_helpers::get_node_value(node, item_creation_time_node, str))
    {
        dvblink::engine::string_conv::apply(str.c_str(), item.creation_time_, item.creation_time_);
    }
}

} // playback
} // dvblex
