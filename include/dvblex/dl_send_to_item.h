/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <map>
#include <dl_types.h>
#include <dl_params.h>
#include <dl_filesystem_path.h>

namespace dvblex {

//work items

enum send_to_work_item_status_e
{
	e_stwis_pending,
	e_stwis_fetching,
	e_stwis_formatting,
	e_stwis_sending,
	e_stwis_success,
	e_stwis_error,
	e_stwis_canceled
};

inline bool is_work_item_completed(send_to_work_item_status_e status)
{
	return status == e_stwis_success || status == e_stwis_canceled || status == e_stwis_error;
}

class send_to_work_item
{
public:
	send_to_work_item() :
		creation_time(0), status(e_stwis_pending), completion_time(0)
	{}

	void set_finished_status(send_to_work_item_status_e s)
	{
		status = s;
		time(&completion_time);
	}

    dvblink::work_item_id_t item_id;
	dvblink::object_id_t pb_object_id;
	std::string description;
	time_t creation_time;
	dvblink::base_id_t target_id;
	send_to_work_item_status_e status;
	time_t completion_time;
};

typedef std::vector<send_to_work_item> send_to_work_item_list_t;

typedef std::vector<dvblink::work_item_id_t> send_to_work_item_id_list_t;

typedef std::vector<dvblink::object_id_t> send_to_object_id_list_t;

//targets, formatters and destinations

typedef std::string send_to_work_unit_id;

struct send_to_work_unit_info
{
	send_to_work_unit_info() :
		is_activated(false)
		{}

	void reset()
	{
		id.clear();
		is_activated = false;
		activation_info.c_str();
	}

	send_to_work_unit_id id;
	bool is_activated;
	dvblink::xml_string_t activation_info;
};

typedef std::vector<send_to_work_unit_info> send_to_work_unit_info_list_t;

struct send_to_work_unit_init_info
{
	send_to_work_unit_id id;
    dvblink::xml_string_t params;
};

struct send_to_target_info
{
	send_to_target_info() :
		delete_on_success(false), default_(false), use_comskip(false)
		{}
	dvblink::base_id_t id;
	std::string name;
	send_to_work_unit_init_info formatter_info;
	send_to_work_unit_init_info dest_info;
	bool delete_on_success;
	bool default_;
    bool use_comskip;
    concise_param_map_t comskip_params;
};

typedef std::vector<send_to_target_info> send_to_target_list_t;

} //dvblex
