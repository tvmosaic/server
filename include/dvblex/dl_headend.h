/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <map>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <dl_types.h>
#include <dl_channel_info.h>
#include <dl_parameters.h>

namespace dvblex { 

struct headend_info_t
{
    dvblink::headend_id_t id_;
    dvblink::provider_id_t provider_id_;
    dvblink::headend_name_t name_;
    dvblink::headend_desc_t desc_;
    source_type_e standard_;
    concise_param_map_t device_config_params_;
};

typedef std::vector<headend_info_t> headend_info_list_t;
typedef std::vector<dvblink::headend_id_t> headend_id_list_t;
typedef std::map<dvblink::headend_id_t, headend_info_t> headend_info_map_t;

struct headend_description_for_device_t
{
    dvblink::headend_id_t headend_id_;
    concise_param_map_t device_config_params_;
};

typedef std::vector<headend_description_for_device_t> headend_description_for_device_list_t;

typedef std::map<dvblink::device_id_t, headend_description_for_device_list_t> device_map_to_headend_list_t;

}

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::headend_info_t& hi, const unsigned int /*version*/)
{
    ar & hi.id_;
    ar & hi.provider_id_;
    ar & hi.name_;
    ar & hi.desc_;
    ar & hi.standard_;
    ar & hi.device_config_params_;
}

template<class Archive>
void serialize(Archive& ar, dvblex::headend_description_for_device_t& hdd, const unsigned int /*version*/)
{
    ar & hdd.headend_id_;
    ar & hdd.device_config_params_;
}

} // namespace serialization
} // namespace boost
