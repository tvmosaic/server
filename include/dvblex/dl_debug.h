/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_DEBUG_H__
#define __DVBLINK_DEBUG_H__

namespace dvblink {

void _debug_print(const char* format, ...);
inline void _do_nothing(...){}

#if defined(_WIN32) && !defined(NDEBUG)
#define _TRACE _debug_print
#define _BREAKPOINT DebugBreak()
#else
#define _TRACE _do_nothing
#define _BREAKPOINT
#endif

#if defined(_WIN32)
#define _XTRACE _debug_print
#else
#define _XTRACE _do_nothing
#endif

} // namespace dvblink

#endif  // __DVBLINK_DEBUG_H__

// $Id: dl_debug.h 5375 2012-05-17 15:42:06Z mike $
