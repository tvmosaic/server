/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <boost/thread/shared_mutex.hpp>

#include <dl_ts_info.h>
#include <dl_ts_proc.h>

namespace dvblink { namespace engine {

class CTSAudioSelector
{
public:
    CTSAudioSelector(dvblink::engine::LP_PACKGEN_TS_SEND_FUNC cb_func, void* param);
    ~CTSAudioSelector();

    void Start(const char* lng, unsigned short pmt_pid = dvblink::engine::INVALID_PACKET_PID);
    void Start(const char* lng, CTSPmtParser* pmt_parser);
    void Stop();

    void ProcessStream(unsigned char* buffer, int len);

protected:
    enum EState
    {
        ES_STOPPED,
        ES_PAT,
        ES_PMT,
        ES_STREAMING
    };

    void ProcessPMTSection(CTSPmtInfo& pmtinfo);
    void ProcessPATSection(unsigned char* pat_buffer, int pat_len);
	void SendPMTSection(unsigned char* pmt_buffer, int pmt_len);

    unsigned char m_contCounter;
    unsigned short m_pmt_pid;
    std::string m_lng;
    dvblink::engine::LP_PACKGEN_TS_SEND_FUNC m_cb_func;
    void* m_param;
    EState m_state;
	std::map<unsigned short, unsigned short> pid_exclude_list_;

    dvblink::engine::CTSPacketGenerator m_packetGenerator;
    dvblink::engine::ts_section_payload_parser m_sectionParser;
    boost::shared_mutex m_lock;
};

}; //engine
}; //dvblink

