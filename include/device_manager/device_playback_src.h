/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <dl_types.h>
#include <dl_logger.h>
#include <dli_server.h>
#include <dl_hash.h>
#include <dl_uuid.h>
#include <dl_parameters.h>
#include <dl_provider_info.h>
#include <dl_headend.h>
#include <dl_message_queue.h>
#include <dl_message_playback.h>
#include <dl_message_addresses.h>

namespace dvblex { 

struct device_playback_src_init_item_t
{
    headend_info_t headend_info_;
    provider_info_t provider_info_;
    provider_scan_list_t scan_list_;
};

typedef std::vector<device_playback_src_init_item_t> device_playback_src_init_list_t;

class device_playback_src_t
{
    class message_handler : 
        public dvblink::messaging::playback::get_objects_request::subscriber,
        public dvblink::messaging::playback::search_objects_request::subscriber,
        public dvblink::messaging::playback::get_source_container_request::subscriber
{
    public:
        message_handler(device_playback_src_t* device_playback_src, dvblink::messaging::message_queue_t message_queue) :
            dvblink::messaging::playback::get_objects_request::subscriber(message_queue),
            dvblink::messaging::playback::search_objects_request::subscriber(message_queue),
            dvblink::messaging::playback::get_source_container_request::subscriber(message_queue),
            device_playback_src_(device_playback_src),
            message_queue_(message_queue)
        {
        }

        ~message_handler()
        {
        }

    protected:
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_objects_request& request, dvblink::messaging::playback::get_objects_response& response)
        {
            device_playback_src_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::search_objects_request& request, dvblink::messaging::playback::search_objects_response& response)
        {
            device_playback_src_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_source_container_request& request, dvblink::messaging::playback::get_source_container_response& response)
        {
            device_playback_src_->handle(sender, request, response);
        }

    private:
        device_playback_src_t* device_playback_src_;
        dvblink::messaging::message_queue_t message_queue_;
    };

public:
    device_playback_src_t(){}
    virtual ~device_playback_src_t(){}

    virtual bool init(const dvblink::device_id_t& device_id, 
        const device_playback_src_init_list_t& device_playback_src_init_list, 
        const dvblink::i_server_t& server)
    {
        server_ = server;
        device_id_ = device_id;
        device_playback_src_init_list_ = device_playback_src_init_list;
        
        //generate id for this source
        boost::uint64_t id_hash = dvblink::engine::calculate_64bit_string_hash(std::string("device#") + device_id.to_string());
        id_ = dvblink::engine::uuid::from_uint64(0, id_hash);
        //create and register message queue
        message_queue_ = dvblink::share_object_safely(new dvblink::messaging::message_queue(id_.get()));
        server_->register_queue(message_queue_);
        //create message handler
        message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));
        //register playback source
        dvblex::playback::pb_source_t pb_source;
        pb_source.id_ = id_.get();
        pb_source.name_ = get_source_name();

        dvblink::logging::log_info(L"device_playback_src_t::init. Registering playback source %1% (%2%)") % pb_source.id_.to_wstring() % pb_source.name_.to_wstring();
        dvblink::messaging::playback::register_pb_source_request req(pb_source);
        dvblink::messaging::playback::register_pb_source_response resp;
        if (message_queue_->send(dvblink::messaging::server_message_queue_addressee, req, resp) != dvblink::messaging::success || !resp.result_)
        {
            dvblink::logging::log_warning(L"device_playback_src_t::init. Error, registering source %1%") % pb_source.name_.to_wstring();
        }

        source_init();

        return true;
    }

    virtual void term()
    {
        source_term();

        //unregister playback source
        dvblink::messaging::playback::unregister_pb_source_request req(id_.get());
        dvblink::messaging::playback::unregister_pb_source_response resp;
        if (message_queue_->send(dvblink::messaging::server_message_queue_addressee, req, resp) != dvblink::messaging::success || !resp.result_)
        {
            dvblink::logging::log_warning(L"device_playback_src_t::term. Error, unregistering source %1%") % id_.to_wstring();
        }

        server_->unregister_queue(message_queue_->get_id());
        message_queue_->shutdown();
        message_handler_.reset();
    }

protected:
    dvblink::i_server_t server_;
    dvblink::base_id_t id_;
    dvblink::device_id_t device_id_;
    device_playback_src_init_list_t device_playback_src_init_list_;

    dvblink::messaging::message_queue_t message_queue_;
    std::auto_ptr<message_handler> message_handler_;

    virtual void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_objects_request& request, dvblink::messaging::playback::get_objects_response& response){}
    virtual void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_source_container_request& request, dvblink::messaging::playback::get_source_container_response& response){}
    virtual void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::search_objects_request& request, dvblink::messaging::playback::search_objects_response& response){}
    virtual void source_init(){}
    virtual void source_term(){}
    virtual std::string get_source_name(){return "Playback source";}
};

typedef boost::shared_ptr<device_playback_src_t> device_playback_src_obj_t;

}
