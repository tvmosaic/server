/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/ptime.hpp>

#include <dl_filesystem_path.h>
#include <dl_streamer.h>
#include <dl_epg_box.h>
#include <dl_provider_info.h>
#include <dl_device_info.h>
#include <dl_headend.h>
#include <dl_scan_info.h>
#include <dl_timer_procedure.h>
#include <dl_installation_settings.h>
#include <device_manager/device_playback_src.h>
#include <drivers/aux_module.h>

namespace dvblex { 

class device_t;
class channel_scanner_t;
class epg_scanner_t;
class device_reactor_t;
class provider_manager_t;
class streamer_container_t;
class directory_settings_t;

class device_manager_t
{
protected:
    typedef std::map<dvblink::channel_id_t, streamer_container_t*> streamer_container_map_t; //channel id -> streamer_container

    struct device_info_t
    {
        device_info_t() :
            device_(NULL), channel_scanner_(NULL), epg_scanner_(NULL)
        {
        }

        dvblink::device_id_t id_;
        device_t* device_;
        //streaming
        streamer_container_map_t streamer_container_map_;
        //scanning
        channel_scanner_t* channel_scanner_;
        //scanning epg
        epg_scanner_t* epg_scanner_;
    };

    typedef std::map<dvblink::device_id_t, device_info_t> device_map_t; //device id -> device

public:
    device_manager_t(dvblex::settings::installation_settings_obj_t& install_settings);
    ~device_manager_t();

    bool init(const manual_device_list_t& manual_devices);
    void term();

    //providers
    void get_providers(dvblink::standard_set_t standard, provider_info_map_t& providers);
    bool get_provider_details(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, provider_info_t& provider_info, provider_scan_list_t& scan_list);
    bool get_rescan_settings(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, parameters_container_t& settings);

    source_type_e get_source_type_from_tuning_params(const dvblink::device_uuid_t& u, const dvblink::channel_tuning_params_t& tune_params);

    bool get_device_list(const dvblink::device_uuid_t& u, device_descriptor_list_t& device_list);
    device_state_e get_device_state(const dvblink::device_id_t& device_id);
    bool get_device_info(const dvblink::device_id_t& device_id, device_descriptor_t& device_info);

    bool get_device_status(const dvblink::device_id_t& device_id, device_status_t& status);

    bool get_device_configurable_props(const dvblink::device_id_t& device_id, configurable_device_props_t& props);
    set_device_props_result_e set_device_configurable_props(const dvblink::device_id_t& device_id, const concise_param_map_t& props);

    //manual device
    void get_device_template_list(parameters_container_t& templates);
    bool create_manual_device(const concise_param_map_t& device_params);
    bool delete_manual_device(const dvblink::device_id_t& device_id);
    void get_manual_devices(manual_device_list_t& devices);

    //channel scan functions
    bool scan_start(const dvblink::device_id_t& device_id, const provider_info_t& provider_info, const provider_scan_list_t& scan_data, const concise_param_map_t& scanner_settings);
    bool scan_network(const dvblink::device_id_t& device_id, const dvblink::scan_network_id_t& network_id);
    bool scan_cancel(const dvblink::device_id_t& device_id);
    bool scan_get_progress(const dvblink::device_id_t& device_id, channel_scanner_progress_t& progress_info);
    bool scan_get_channels(const dvblink::device_id_t& device_id, headend_info_t& headend, concise_param_map_t& scanner_settings, transponder_list_t& channels);
    bool scan_get_networks(const dvblink::device_id_t& device_id, scanned_network_list_t& networks);
    bool scan_get_log(const dvblink::device_id_t& device_id, scan_log_t& scan_log);

    //streaming
    bool add_streamer(const dvblink::device_id_t& device_id, const dvblink::channel_id_t& channel_id, const concise_channel_tune_info_t& tune_params, dvblink::streamer_object_t& streamer);
    bool remove_streamer(const dvblink::streamer_id_t& streamer_id);
    bool can_join_streaming(const dvblink::device_id_t& device_id, const concise_channel_tune_info_t& channel_tune_params);
    bool is_channel_present_on_device(const dvblink::device_id_t& device_id, const dvblink::device_id_t& channel_id);

    //epg scan
    bool start_epg_scan(const dvblink::device_id_t& device_id, dvblink::epg_box_obj_t& epg_box);

    //cancel any ongoing operation on the device
    void reset_device(const dvblink::device_id_t& device_id);

    //report if all devices are currently idle
    bool is_idle();

    // headends
    bool get_headend_info(const headend_description_for_device_t& headend, headend_info_t& headend_info);
    bool get_headend_from_provider(const provider_info_t& provider_info, const concise_param_map_t& scanner_settings, headend_info_t& headend_info);
    void find_conflicting_headends(const dvblink::device_id_t& device_id, const headend_description_for_device_list_t& existing_headends, 
        const headend_description_for_device_t& new_headend, headend_id_list_t& conflicting_headends);


    dvblink::filesystem_path_t create_shared_dir_for_device(const device_descriptor_t& dd);
    dvblink::filesystem_path_t get_scanners_directory();

    device_playback_src_obj_t get_playback_src_for_device(const dvblink::device_id_t& device_id);

    bool get_channel_url_for_format(const dvblink::device_id_t& device_id, const concise_channel_tune_info_t& tune_params, 
        device_channel_stream_type_e format, dvblink::url_address_t& url, dvblink::mime_type_t& mime);

protected:
    device_map_t device_map_;
    boost::shared_ptr<device_reactor_t> device_reactor_;
    boost::shared_ptr<directory_settings_t> directory_settings_;
    boost::shared_mutex lock_;
    dvblink::aux_module_list_t aux_module_list_;
    boost::shared_ptr<provider_manager_t> provider_manager_;
	dvblink::engine::timer_procedure<device_manager_t>* idle_timer_;
    long idle_timer_period_msec_;

    device_state_e get_device_state_int(const dvblink::device_id_t& device_id);
    bool switch_to_idle(device_state_e state, const dvblink::device_id_t& device_id, bool create_device = false);
	void idle_timer_func(const boost::system::error_code& e);
    void remove_all_streams_int(const dvblink::device_id_t& device_id);
};

}
