/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/uuid/uuid.hpp>

#include <dli_server.h>
#include <dl_message_queue.h>
#include <dl_message_server.h>
#include <dl_message_common.h>
#include <dl_message_playback.h>
#include <dli_generic_component.h>

namespace dvblink {
    class discovery_server;
}

namespace pion { namespace http {
    class plugin_server;
    class plugin_service;
}}

namespace dvblex {

class command_service;
class stream_service;
class network_server_settings_t;
class http_server_type;
class ext_port_mapper;

typedef boost::shared_ptr<pion::http::plugin_server> plugin_server_obj_t;

class network_server_t : public dvblink::messaging::i_generic_component
{
    struct web_service_desc_t
    {
        web_service_desc_t()
            : web_service_(NULL)
        {}

        web_service_desc_t(http_server_type* web_service, const std::string& resource)
            : web_service_(web_service), resource_(resource)
        {}

        http_server_type* web_service_;
        std::string resource_;
    };


    class message_handler : 
        public dvblink::messaging::playback::get_playback_streaming_port_request::subscriber,
        public dvblink::messaging::start_request::subscriber,
        public dvblink::messaging::shutdown_request::subscriber,
        public  dvblink::messaging::xml_message_request::subscriber
    {
    public:
        message_handler(network_server_t* network_server, dvblink::messaging::message_queue_t message_queue) :
            dvblink::messaging::playback::get_playback_streaming_port_request::subscriber(message_queue),
            dvblink::messaging::start_request::subscriber(message_queue),
            dvblink::messaging::shutdown_request::subscriber(message_queue),
            dvblink::messaging::xml_message_request::subscriber(message_queue),
            network_server_(network_server),
            message_queue_(message_queue)
        {
        }

        ~message_handler()
        {
        }

    protected:
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response)
        {
            network_server_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response)
        {
            network_server_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_playback_streaming_port_request& request, dvblink::messaging::playback::get_playback_streaming_port_response& response)
        {
            network_server_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response)
        {
            network_server_->handle(sender, request, response);
        }

    private:
        network_server_t* network_server_;
        dvblink::messaging::message_queue_t message_queue_;
    };

public:
    network_server_t();
    ~network_server_t();

    virtual bool __stdcall init(const dvblink::i_server_t& server);
    dvblink::i_result __stdcall query_interface(const dvblink::base_id_t& requestor_id, const dvblink::i_guid& iid, dvblink::i_base_object_t& obj);
    virtual const boost::uuids::uuid& __stdcall get_uid(){return id_.get();}

private:
    std::vector<plugin_server_obj_t> servers_;
    network_server_settings_t* settings_;
    dvblink::base_id_t id_;
    dvblink::i_server_t server_;
    dvblink::messaging::message_queue_t message_queue_;
    std::auto_ptr<message_handler> message_handler_;
    boost::shared_ptr<dvblink::discovery_server> discovery_server_;
    boost::uint16_t streaming_port_;
    boost::uint16_t https_streaming_port_;
    boost::shared_ptr<ext_port_mapper> ext_port_mapper_;

private:
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_playback_streaming_port_request& request, dvblink::messaging::playback::get_playback_streaming_port_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response);
    bool start();
    void stop();
    plugin_server_obj_t start_web_server(unsigned short port, std::vector<web_service_desc_t> web_services);
    void apply_authentication();
};

typedef boost::shared_ptr<network_server_t> network_server_obj_t;

} // dvblink
