/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/uuid/uuid.hpp>

#include <dli_server.h>
#include <dl_message_queue.h>
#include <dl_message_server.h>
#include <dl_message_common.h>
#include <dl_message_epg.h>
#include <dl_message_channels.h>
#include <dli_generic_component.h>
#include <dl_channels.h>

namespace dvblex {

class device_epg_manager_t;
class epg_manager_settings_t;

class epg_manager_t : public dvblink::messaging::i_generic_component
{
    class message_handler : 
        public dvblink::messaging::epg::enable_periodic_epg_updates_request::subscriber,
        public dvblink::messaging::channels::channel_config_changed_request::subscriber,
        public dvblink::messaging::start_request::subscriber,
        public dvblink::messaging::shutdown_request::subscriber,
        public dvblink::messaging::epg::get_channel_epg_config_request::subscriber,
        public dvblink::messaging::epg::set_channel_epg_config_request::subscriber,
        public dvblink::messaging::epg::epg_updated_request::subscriber,
        public dvblink::messaging::epg::match_epg_channels_request::subscriber,
        public dvblink::messaging::epg::get_epg_sources_request::subscriber,
        public dvblink::messaging::epg::register_epg_source_request::subscriber,
        public dvblink::messaging::epg::unregister_epg_source_request::subscriber
    {
    public:
        message_handler(epg_manager_t* epg_manager, dvblink::messaging::message_queue_t message_queue) :
            dvblink::messaging::epg::enable_periodic_epg_updates_request::subscriber(message_queue),
            dvblink::messaging::channels::channel_config_changed_request::subscriber(message_queue),
            dvblink::messaging::start_request::subscriber(message_queue),
            dvblink::messaging::shutdown_request::subscriber(message_queue),
            dvblink::messaging::epg::get_channel_epg_config_request::subscriber(message_queue),
            dvblink::messaging::epg::set_channel_epg_config_request::subscriber(message_queue),
            dvblink::messaging::epg::epg_updated_request::subscriber(message_queue),
            dvblink::messaging::epg::match_epg_channels_request::subscriber(message_queue),
            dvblink::messaging::epg::get_epg_sources_request::subscriber(message_queue),
            dvblink::messaging::epg::register_epg_source_request::subscriber(message_queue),
            dvblink::messaging::epg::unregister_epg_source_request::subscriber(message_queue),
            epg_manager_(epg_manager),
            message_queue_(message_queue)
        {
        }

        ~message_handler()
        {
        }

    protected:
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response)
        {
            epg_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response)
        {
            epg_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_channel_epg_config_request& request, dvblink::messaging::epg::get_channel_epg_config_response& response)
        {
            epg_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::set_channel_epg_config_request& request, dvblink::messaging::epg::set_channel_epg_config_response& response)
        {
            epg_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::epg_updated_request& request)
        {
            epg_manager_->handle(sender, request);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::enable_periodic_epg_updates_request& request)
        {
            epg_manager_->handle(sender, request);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::register_epg_source_request& request, dvblink::messaging::epg::register_epg_source_response& response)
        {
            epg_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::unregister_epg_source_request& request, dvblink::messaging::epg::unregister_epg_source_response& response)
        {
            epg_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_epg_sources_request& request, dvblink::messaging::epg::get_epg_sources_response& response)
        {
            epg_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::match_epg_channels_request& request, dvblink::messaging::epg::match_epg_channels_response& response)
        {
            epg_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::channel_config_changed_request& request)
        {
            epg_manager_->handle(sender, request);
        }

    private:
        epg_manager_t* epg_manager_;
        dvblink::messaging::message_queue_t message_queue_;
    };

public:
    epg_manager_t();
    ~epg_manager_t();

    virtual bool __stdcall init(const dvblink::i_server_t& server);
    dvblink::i_result __stdcall query_interface(const dvblink::base_id_t& requestor_id, const dvblink::i_guid& iid, dvblink::i_base_object_t& obj);
    virtual const boost::uuids::uuid& __stdcall get_uid(){return id_.get();}

private:
    typedef std::map<dvblink::channel_id_t, dvblink::logo_id_t> channel_id_lookup_map_t;

    typedef std::map<std::string, channel_id_lookup_map_t> epg_source_to_channel_map_t;

    dvblink::base_id_t id_;
    dvblink::i_server_t server_;
    dvblink::messaging::message_queue_t message_queue_;
    std::auto_ptr<message_handler> message_handler_;
    boost::shared_ptr<device_epg_manager_t> device_epg_manager_;
    boost::shared_ptr<epg_manager_settings_t> settings_;
    epg_source_map_t epg_sources_;
    channel_to_epg_source_map_t epg_channels_;
    epg_source_to_channel_map_t epg_source_to_channel_map_;
    dvblink::epg_source_id_t default_epg_source_id_;
    bool periodic_updates_enabled_;

private:
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_channel_epg_config_request& request, dvblink::messaging::epg::get_channel_epg_config_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::set_channel_epg_config_request& request, dvblink::messaging::epg::set_channel_epg_config_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::epg_updated_request& request);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::register_epg_source_request& request, dvblink::messaging::epg::register_epg_source_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::unregister_epg_source_request& request, dvblink::messaging::epg::unregister_epg_source_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_epg_sources_request& request, dvblink::messaging::epg::get_epg_sources_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::match_epg_channels_request& request, dvblink::messaging::epg::match_epg_channels_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::enable_periodic_epg_updates_request& request);
    bool get_channel_id_from_epg_source(const dvblink::epg_source_id_t& epg_source_id, const dvblink::epg_channel_id_t& epg_channel_id, channel_id_lookup_map_t& channel_ids);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::channel_config_changed_request& request);
    std::string get_epg_source_channel_hash(const epg_source_channel_id_t& es);
    void rebuild_inverse_lookup_map();
    void save_config();
    void load_config();
};

typedef boost::shared_ptr<epg_manager_t> epg_manager_obj_t;

} // dvblex
