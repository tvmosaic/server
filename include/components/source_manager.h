/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/uuid/uuid.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <dli_server.h>
#include <dl_message_queue.h>
#include <dl_message_server.h>
#include <dl_message_stream.h>
#include <dl_message_common.h>
#include <dl_message_channels.h>
#include <dl_message_devices.h>
#include <dli_generic_component.h>
#include <dli_stream_source.h>

namespace dvblex {

class source_manager_settings_t;
class device_manager_t;
class channel_storage_t;
class favorites_storage_t;
class channel_visibility_storage_t;
class channel_overwrites_storage_t;
class device_params_storage_t;
class stream_power_manager_t;
class playback_source_manager_t;

class source_manager_t : public dvblink::messaging::i_generic_component, public boost::enable_shared_from_this<source_manager_t>
{
    class stream_handler : public dvblink::messaging::i_stream_source_control, public boost::enable_shared_from_this<stream_handler>
    {
        public:
            stream_handler(source_manager_t* source_manager)
                : source_manager_(source_manager)
                {}

            dvblink::i_result __stdcall query_interface(const dvblink::base_id_t& requestor_id, const dvblink::i_guid& iid, dvblink::i_base_object_t& obj)
            {
                dvblink::i_result res = dvblink::i_error;
                return res;
            }

            virtual const boost::uuids::uuid& __stdcall get_uid(){return source_manager_->get_uid();}

            virtual dvblink::EStatus __stdcall start_channel(const char* channel_id, dvblink::streamer_object_t& streamer)
            {
                return source_manager_->start_channel(channel_id, streamer);
            }

            virtual dvblink::EStatus __stdcall start_channel_on_device(const char* channel_id, const char* device_id, const char* tuning_params, dvblink::streamer_object_t& streamer)
            {
                return source_manager_->start_channel_on_device(channel_id, device_id, tuning_params, streamer);
            }

            virtual void __stdcall stop_streamer(const char* streamer_id)
            {
                source_manager_->stop_streamer(streamer_id);
            }

            virtual dvblink::start_scan_status_e __stdcall start_epg_scan(const char* provider_id, dvblink::epg_box_obj_t& epg_box)
            {
                return source_manager_->start_epg_scan(provider_id, epg_box);
            }

    protected:
        source_manager_t* source_manager_;
    };


    class message_handler : 
        public  dvblink::messaging::stream::stop_stream_request::subscriber,
        public  dvblink::messaging::channels::get_device_channel_sets_request::subscriber,
        public  dvblink::messaging::channels::get_channel_favorites_request::subscriber,
        public  dvblink::messaging::channels::set_channel_favorites_request::subscriber,
        public  dvblink::messaging::channels::get_channel_overwrites_request::subscriber,
        public  dvblink::messaging::channels::set_channel_overwrites_request::subscriber,
        public  dvblink::messaging::channels::get_scanned_channels_request::subscriber,
        public  dvblink::messaging::channels::get_provider_channels_request::subscriber,
        public  dvblink::messaging::channels::get_provider_channels_id_request::subscriber,
        public  dvblink::messaging::channels::get_device_headend_info_request::subscriber,
        public  dvblink::messaging::channels::channel_config_changed_request::subscriber,
        public  dvblink::messaging::channels::get_channel_visibility_request::subscriber,
        public  dvblink::messaging::channels::set_channel_visibility_request::subscriber,
        public  dvblink::messaging::channels::get_channels_description_request::subscriber,
        public  dvblink::messaging::devices::devices_info_request::subscriber,
        public  dvblink::messaging::devices::get_scanners_request::subscriber,
        public  dvblink::messaging::devices::start_scan_request::subscriber,
        public  dvblink::messaging::devices::rescan_provider_request::subscriber,       
        public  dvblink::messaging::devices::get_rescan_settings_request::subscriber,       
        public  dvblink::messaging::devices::cancel_scan_request::subscriber,
        public  dvblink::messaging::devices::apply_scan_request::subscriber,
        public  dvblink::messaging::devices::get_networks_request::subscriber,
        public  dvblink::messaging::devices::get_device_status_request::subscriber,
        public  dvblink::messaging::devices::drop_headend_on_device_request::subscriber,
        public  dvblink::messaging::devices::get_device_settings_request::subscriber,
        public  dvblink::messaging::devices::set_device_settings_request::subscriber,
        public  dvblink::messaging::devices::get_device_templates_request::subscriber,
        public  dvblink::messaging::devices::create_manual_device_request::subscriber,
        public  dvblink::messaging::devices::delete_manual_device_request::subscriber,
        public  dvblink::messaging::devices::get_device_channel_url_request::subscriber,
        public dvblink::messaging::start_request::subscriber,
        public dvblink::messaging::shutdown_request::subscriber,
        public dvblink::messaging::standby_request::subscriber,
        public dvblink::messaging::resume_request::subscriber
    {
    public:
        message_handler(source_manager_t* source_manager, dvblink::messaging::message_queue_t message_queue) :
            dvblink::messaging::stream::stop_stream_request::subscriber(message_queue),
            dvblink::messaging::channels::get_device_channel_sets_request::subscriber(message_queue),
            dvblink::messaging::channels::get_provider_channels_id_request::subscriber(message_queue),
            dvblink::messaging::channels::get_provider_channels_request::subscriber(message_queue),
            dvblink::messaging::channels::get_scanned_channels_request::subscriber(message_queue),
            dvblink::messaging::channels::get_channel_favorites_request::subscriber(message_queue),
            dvblink::messaging::channels::set_channel_favorites_request::subscriber(message_queue),
            dvblink::messaging::channels::get_channel_overwrites_request::subscriber(message_queue),
            dvblink::messaging::channels::set_channel_overwrites_request::subscriber(message_queue),
            dvblink::messaging::channels::get_device_headend_info_request::subscriber(message_queue),
            dvblink::messaging::channels::channel_config_changed_request::subscriber(message_queue),
            dvblink::messaging::channels::get_channel_visibility_request::subscriber(message_queue),
            dvblink::messaging::channels::set_channel_visibility_request::subscriber(message_queue),
            dvblink::messaging::channels::get_channels_description_request::subscriber(message_queue),
            dvblink::messaging::devices::devices_info_request::subscriber(message_queue),
            dvblink::messaging::devices::get_scanners_request::subscriber(message_queue),
            dvblink::messaging::devices::start_scan_request::subscriber(message_queue),
            dvblink::messaging::devices::rescan_provider_request::subscriber(message_queue),
            dvblink::messaging::devices::get_rescan_settings_request::subscriber(message_queue),
            dvblink::messaging::devices::apply_scan_request::subscriber(message_queue),
            dvblink::messaging::devices::cancel_scan_request::subscriber(message_queue),
            dvblink::messaging::devices::get_networks_request::subscriber(message_queue),
            dvblink::messaging::devices::get_device_status_request::subscriber(message_queue),
            dvblink::messaging::devices::drop_headend_on_device_request::subscriber(message_queue),
            dvblink::messaging::devices::get_device_settings_request::subscriber(message_queue),
            dvblink::messaging::devices::set_device_settings_request::subscriber(message_queue),
            dvblink::messaging::devices::get_device_templates_request::subscriber(message_queue),
            dvblink::messaging::devices::create_manual_device_request::subscriber(message_queue),
            dvblink::messaging::devices::delete_manual_device_request::subscriber(message_queue),
            dvblink::messaging::devices::get_device_channel_url_request::subscriber(message_queue),
            dvblink::messaging::start_request::subscriber(message_queue),
            dvblink::messaging::shutdown_request::subscriber(message_queue),
            dvblink::messaging::standby_request::subscriber(message_queue),
            dvblink::messaging::resume_request::subscriber(message_queue),
            source_manager_(source_manager),
            message_queue_(message_queue)
        {
        }

        ~message_handler()
        {
        }

    protected:

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::standby_request& request, dvblink::messaging::standby_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::resume_request& request, dvblink::messaging::resume_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::devices_info_request& request, dvblink::messaging::devices::devices_info_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_scanners_request& request, dvblink::messaging::devices::get_scanners_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::start_scan_request& request, dvblink::messaging::devices::start_scan_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::rescan_provider_request& request, dvblink::messaging::devices::rescan_provider_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_rescan_settings_request& request, dvblink::messaging::devices::get_rescan_settings_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::cancel_scan_request& request, dvblink::messaging::devices::cancel_scan_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::apply_scan_request& request, dvblink::messaging::devices::apply_scan_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_networks_request& request, dvblink::messaging::devices::get_networks_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_device_status_request& request, dvblink::messaging::devices::get_device_status_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::drop_headend_on_device_request& request, dvblink::messaging::devices::drop_headend_on_device_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_device_settings_request& request, dvblink::messaging::devices::get_device_settings_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::set_device_settings_request& request, dvblink::messaging::devices::set_device_settings_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::delete_manual_device_request& request, dvblink::messaging::devices::delete_manual_device_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::create_manual_device_request& request, dvblink::messaging::devices::create_manual_device_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_device_templates_request& request, dvblink::messaging::devices::get_device_templates_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_device_channel_url_request& request, dvblink::messaging::devices::get_device_channel_url_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_device_channel_sets_request& request, dvblink::messaging::channels::get_device_channel_sets_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_scanned_channels_request& request, dvblink::messaging::channels::get_scanned_channels_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_provider_channels_request& request, dvblink::messaging::channels::get_provider_channels_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_provider_channels_id_request& request, dvblink::messaging::channels::get_provider_channels_id_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_device_headend_info_request& request, dvblink::messaging::channels::get_device_headend_info_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channel_favorites_request& request, dvblink::messaging::channels::get_channel_favorites_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::set_channel_favorites_request& request, dvblink::messaging::channels::set_channel_favorites_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channel_overwrites_request& request, dvblink::messaging::channels::get_channel_overwrites_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::set_channel_overwrites_request& request, dvblink::messaging::channels::set_channel_overwrites_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channel_visibility_request& request, dvblink::messaging::channels::get_channel_visibility_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::set_channel_visibility_request& request, dvblink::messaging::channels::set_channel_visibility_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channels_description_request& request, dvblink::messaging::channels::get_channels_description_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::channel_config_changed_request& request)
        {
            source_manager_->handle(sender, request);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::stream::stop_stream_request& request, dvblink::messaging::stream::stop_stream_response& response)
        {
            source_manager_->handle(sender, request, response);
        }

    private:
        source_manager_t* source_manager_;
        dvblink::messaging::message_queue_t message_queue_;
    };

public:
    source_manager_t();
    ~source_manager_t();

    virtual bool __stdcall init(const dvblink::i_server_t& server);
    dvblink::i_result __stdcall query_interface(const dvblink::base_id_t& requestor_id, const dvblink::i_guid& iid, dvblink::i_base_object_t& obj);
    virtual const boost::uuids::uuid& __stdcall get_uid(){return id_.get();}

private:
    typedef std::map<std::string, dvblink::channel_id_t> epg_source_to_channel_map_t;

    dvblink::base_id_t id_;
    dvblink::i_server_t server_;
    dvblink::messaging::message_queue_t message_queue_;
    std::auto_ptr<message_handler> message_handler_;
    boost::shared_ptr<source_manager_settings_t> settings_;
    boost::shared_ptr<device_manager_t> device_manager_;
    boost::shared_ptr<channel_storage_t> channel_storage_;
    boost::shared_ptr<favorites_storage_t> favorites_storage_;
    boost::shared_ptr<channel_visibility_storage_t> channel_visibility_storage_;
    boost::shared_ptr<channel_overwrites_storage_t> channel_overwrites_storage_;
    boost::shared_ptr<device_params_storage_t> device_params_storage_;
    boost::shared_ptr<stream_power_manager_t> stream_power_manager_;
    boost::shared_ptr<stream_handler> stream_handler_;    
    boost::shared_ptr<playback_source_manager_t> playback_source_manager_;

private:
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::standby_request& request, dvblink::messaging::standby_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::resume_request& request, dvblink::messaging::resume_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::devices_info_request& request, dvblink::messaging::devices::devices_info_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_scanners_request& request, dvblink::messaging::devices::get_scanners_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::start_scan_request& request, dvblink::messaging::devices::start_scan_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::rescan_provider_request& request, dvblink::messaging::devices::rescan_provider_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_rescan_settings_request& request, dvblink::messaging::devices::get_rescan_settings_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::cancel_scan_request& request, dvblink::messaging::devices::cancel_scan_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::apply_scan_request& request, dvblink::messaging::devices::apply_scan_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_networks_request& request, dvblink::messaging::devices::get_networks_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_device_status_request& request, dvblink::messaging::devices::get_device_status_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::drop_headend_on_device_request& request, dvblink::messaging::devices::drop_headend_on_device_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_device_settings_request& request, dvblink::messaging::devices::get_device_settings_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::set_device_settings_request& request, dvblink::messaging::devices::set_device_settings_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::delete_manual_device_request& request, dvblink::messaging::devices::delete_manual_device_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::create_manual_device_request& request, dvblink::messaging::devices::create_manual_device_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_device_templates_request& request, dvblink::messaging::devices::get_device_templates_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_scanned_channels_request& request, dvblink::messaging::channels::get_scanned_channels_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_provider_channels_request& request, dvblink::messaging::channels::get_provider_channels_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_provider_channels_id_request& request, dvblink::messaging::channels::get_provider_channels_id_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_device_headend_info_request& request, dvblink::messaging::channels::get_device_headend_info_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channel_favorites_request& request, dvblink::messaging::channels::get_channel_favorites_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::set_channel_favorites_request& request, dvblink::messaging::channels::set_channel_favorites_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channel_overwrites_request& request, dvblink::messaging::channels::get_channel_overwrites_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::set_channel_overwrites_request& request, dvblink::messaging::channels::set_channel_overwrites_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::channel_config_changed_request& request);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channel_visibility_request& request, dvblink::messaging::channels::get_channel_visibility_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::set_channel_visibility_request& request, dvblink::messaging::channels::set_channel_visibility_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channels_description_request& request, dvblink::messaging::channels::get_channels_description_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_device_channel_sets_request& request, dvblink::messaging::channels::get_device_channel_sets_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::stream::stop_stream_request& request, dvblink::messaging::stream::stop_stream_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_device_channel_url_request& request, dvblink::messaging::devices::get_device_channel_url_response& response);

    void init_device_manager();
    void term_device_manager();

    void get_headend_channels(const dvblink::headend_id_t& provider_id, dvblex::concise_headend_desc_list_t& headend_channels,
        const invisible_channel_map_t* invisible_map = NULL, int info_level = dvblink::messaging::channels::get_scanned_channels_level_channels, 
        const dvblink::transponder_id_t* transponder_id = NULL);

    void get_headend_channels(const dvblink::headend_id_t& provider_id, dvblex::idonly_headend_desc_list_t& headend_channels, 
        const invisible_channel_map_t* invisible_map);

    dvblink::EStatus start_channel(const char* channel_id, dvblink::streamer_object_t& streamer);
    dvblink::EStatus start_channel_on_device(const char* channel_id, const char* device_id, const char* tuning_params, dvblink::streamer_object_t& streamer);
    dvblink::EStatus start_channel_int(const char* channel_id, dvblink::streamer_object_t& streamer, void* device_channel_map, dvblink::device_id_t& device_id);
    void stop_streamer(const char* streamer_id);
    dvblink::start_scan_status_e start_epg_scan(const char* provider_id, dvblink::epg_box_obj_t& epg_box);
    bool find_headend_info(const dvblink::device_id_t& device_id, const dvblink::headend_id_t& headend_id, headend_info_t& headend_info);

    void add_playback_src_for_device(const dvblink::device_id_t& device_id, const headend_id_list_t& headend_list);
};

typedef boost::shared_ptr<source_manager_t> source_manager_obj_t;

} // dvblex
