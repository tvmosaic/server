/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/uuid/uuid.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <dli_server.h>
#include <dl_message_queue.h>
#include <dl_message_server.h>
#include <dl_message_common.h>
#include <dli_generic_component.h>
#include <dli_playback_source.h>
#include <dl_message_playback.h>
#include <dl_message_recorder.h>
#include <dl_installation_settings.h>

class rtv_content_storage_t;

namespace dvblex {

class recorded_tv_t : public dvblink::messaging::i_generic_component, public boost::enable_shared_from_this<recorded_tv_t>
{
    class playback_handler : public dvblink::messaging::i_play_source_control, public boost::enable_shared_from_this<playback_handler>
    {
        public:
            playback_handler(recorded_tv_t* recorded_tv)
                : recorded_tv_(recorded_tv)
                {}

            dvblink::i_result __stdcall query_interface(const dvblink::base_id_t& requestor_id, const dvblink::i_guid& iid, dvblink::i_base_object_t& obj)
            {
                dvblink::i_result res = dvblink::i_error;
                return res;
            }

            virtual const boost::uuids::uuid& __stdcall get_uid(){return recorded_tv_->get_uid();}

            virtual bool __stdcall read_data(const dvblink::object_handle_safe_t& handle, unsigned char* buffer, boost::uint32_t& buffer_length)
            {
                return recorded_tv_->read_data(handle, buffer, buffer_length);
            }

    protected:
        recorded_tv_t* recorded_tv_;
    };

    class message_handler : 
        public dvblink::messaging::start_request::subscriber,
        public dvblink::messaging::shutdown_request::subscriber,
        public dvblink::messaging::playback::open_item_request::subscriber,
        public dvblink::messaging::playback::seek_item_request::subscriber,
        public dvblink::messaging::playback::close_item_request::subscriber,
        public dvblink::messaging::playback::local_object_info_request::subscriber,
        public dvblink::messaging::recorder::timer_recording_completed_request::subscriber,
        public dvblink::messaging::playback::remove_object_request::subscriber,
        public dvblink::messaging::playback::stop_recording_request::subscriber,
        public dvblink::messaging::playback::get_objects_request::subscriber,
        public dvblink::messaging::playback::get_source_container_request::subscriber
{
    public:
        message_handler(recorded_tv_t* recorded_tv, dvblink::messaging::message_queue_t message_queue) :
            dvblink::messaging::start_request::subscriber(message_queue),
            dvblink::messaging::shutdown_request::subscriber(message_queue),
            dvblink::messaging::playback::open_item_request::subscriber(message_queue),
            dvblink::messaging::playback::seek_item_request::subscriber(message_queue),
            dvblink::messaging::playback::close_item_request::subscriber(message_queue),
            dvblink::messaging::playback::local_object_info_request::subscriber(message_queue),
            dvblink::messaging::recorder::timer_recording_completed_request::subscriber(message_queue),
            dvblink::messaging::playback::remove_object_request::subscriber(message_queue),
            dvblink::messaging::playback::stop_recording_request::subscriber(message_queue),
            dvblink::messaging::playback::get_objects_request::subscriber(message_queue),
            dvblink::messaging::playback::get_source_container_request::subscriber(message_queue),
            recorded_tv_(recorded_tv),
            message_queue_(message_queue)
        {
        }

        ~message_handler()
        {
        }

    protected:
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response)
        {
            recorded_tv_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response)
        {
            recorded_tv_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::open_item_request& request, dvblink::messaging::playback::open_item_response& response)
        {
            recorded_tv_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::seek_item_request& request, dvblink::messaging::playback::seek_item_response& response)
        {
            recorded_tv_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::close_item_request& request, dvblink::messaging::playback::close_item_response& response)
        {
            recorded_tv_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::local_object_info_request& request, dvblink::messaging::playback::local_object_info_response& response)
        {
            recorded_tv_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::timer_recording_completed_request& request)
        {
            recorded_tv_->handle(sender, request);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::remove_object_request& request, dvblink::messaging::playback::remove_object_response& response)
        {
            recorded_tv_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::stop_recording_request& request, dvblink::messaging::playback::stop_recording_response& response)
        {
            recorded_tv_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_objects_request& request, dvblink::messaging::playback::get_objects_response& response)
        {
            recorded_tv_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_source_container_request& request, dvblink::messaging::playback::get_source_container_response& response)
        {
            recorded_tv_->handle(sender, request, response);
        }

    private:
        recorded_tv_t* recorded_tv_;
        dvblink::messaging::message_queue_t message_queue_;
    };

public:
    recorded_tv_t();
    ~recorded_tv_t();

    virtual bool __stdcall init(const dvblink::i_server_t& server);
    dvblink::i_result __stdcall query_interface(const dvblink::base_id_t& requestor_id, const dvblink::i_guid& iid, dvblink::i_base_object_t& obj);
    virtual const boost::uuids::uuid& __stdcall get_uid(){return id_.get();}
    virtual bool __stdcall read_data(const dvblink::object_handle_safe_t& handle, unsigned char* buffer, boost::uint32_t& buffer_length);

private:
    typedef std::map<dvblink::pb_source_id_t, dvblex::playback::pb_source_t> pb_source_map_t;

    dvblink::base_id_t id_;
    dvblink::i_server_t server_;
    dvblink::messaging::message_queue_t message_queue_;
    std::auto_ptr<message_handler> message_handler_;
    boost::shared_ptr<playback_handler> playback_handler_;
    dvblex::settings::installation_settings_t settings_;
	rtv_content_storage_t* content_storage_;
    boost::shared_mutex file_info_lock_;
    std::map<dvblink::object_handle_t, FILE*> file_info_map_;
    boost::shared_mutex clients_lock_;
    std::map<dvblink::base_id_t, dvblink::base_id_t> connected_clients_;
    bool shutting_down_;
    dvblink::network_port_t https_streaming_port_;
    dvblink::network_port_t streaming_port_;

private:
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response);

    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::open_item_request& request, dvblink::messaging::playback::open_item_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::seek_item_request& request, dvblink::messaging::playback::seek_item_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::close_item_request& request, dvblink::messaging::playback::close_item_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_objects_request& request, dvblink::messaging::playback::get_objects_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_source_container_request& request, dvblink::messaging::playback::get_source_container_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::local_object_info_request& request, dvblink::messaging::playback::local_object_info_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::timer_recording_completed_request& request);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::remove_object_request& request, dvblink::messaging::playback::remove_object_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::stop_recording_request& request, dvblink::messaging::playback::stop_recording_response& response);

    bool add_container_to_container_list(const std::string& container_id, std::string& source_id, const dvblink::url_proto_t& proto_header, const dvblink::url_address_t& server_address, 
        const dvblink::network_port_t& server_port, dvblex::playback::pb_container_list_t& container_list);
    bool generate_root_response(const dvblink::url_proto_t& proto, const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, dvblex::playback::pb_object_t& object_info);
    bool get_image_pathname(std::string& container_id, std::string& item_id, dvblink::filesystem_path_t& path);
    dvblink::network_port_t get_port_from_proto(const dvblink::url_proto_t& proto);
    void update_streaming_port();
    std::string get_source_id();
};

typedef boost::shared_ptr<recorded_tv_t> recorded_tv_obj_t;

} // dvblex
