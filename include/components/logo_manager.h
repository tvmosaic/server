/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/uuid/uuid.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <dli_server.h>
#include <dl_message_queue.h>
#include <dl_message_server.h>
#include <dl_message_common.h>
#include <dli_generic_component.h>
#include <dl_installation_settings.h>

namespace dvblex {

class logo_storage_t;

class logo_manager_t : public dvblink::messaging::i_generic_component, public boost::enable_shared_from_this<logo_manager_t>
{
    class message_handler : 
        public dvblink::messaging::start_request::subscriber,
        public dvblink::messaging::shutdown_request::subscriber
    {
    public:
        message_handler(logo_manager_t* logo_manager, dvblink::messaging::message_queue_t message_queue) :
            dvblink::messaging::start_request::subscriber(message_queue),
            dvblink::messaging::shutdown_request::subscriber(message_queue),
            logo_manager_(logo_manager),
            message_queue_(message_queue)
        {
        }

        ~message_handler()
        {
        }

    protected:
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response)
        {
            logo_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response)
        {
            logo_manager_->handle(sender, request, response);
        }

    private:
        logo_manager_t* logo_manager_;
        dvblink::messaging::message_queue_t message_queue_;
    };

public:
    logo_manager_t();
    ~logo_manager_t();

    virtual bool __stdcall init(const dvblink::i_server_t& server);
    dvblink::i_result __stdcall query_interface(const dvblink::base_id_t& requestor_id, const dvblink::i_guid& iid, dvblink::i_base_object_t& obj);
    virtual const boost::uuids::uuid& __stdcall get_uid(){return id_.get();}

private:
    dvblink::base_id_t id_;
    dvblink::i_server_t server_;
    dvblink::messaging::message_queue_t message_queue_;
    std::auto_ptr<message_handler> message_handler_;
    dvblex::settings::installation_settings_t settings_;
    boost::shared_ptr<logo_storage_t> logo_storage_;
    bool product_activated_;
    dvblink::product_id_t product_id_;

private:
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response);
};

typedef boost::shared_ptr<logo_manager_t> logo_manager_obj_t;

} // dvblink
