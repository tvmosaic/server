/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/uuid/uuid.hpp>

#include <dli_server.h>
#include <dl_message_queue.h>
#include <dli_generic_component.h>

namespace dvblex {

namespace recorder {
    class sink_recorder;
}

class recorder_t : public dvblink::messaging::i_generic_component
{
public:
    recorder_t();
    ~recorder_t();

    virtual bool __stdcall init(const dvblink::i_server_t& server);
    dvblink::i_result __stdcall query_interface(const dvblink::base_id_t& requestor_id, const dvblink::i_guid& iid, dvblink::i_base_object_t& obj);
    virtual const boost::uuids::uuid& __stdcall get_uid(){return id_.get();}

private:
    dvblink::base_id_t id_;
    dvblex::recorder::sink_recorder* recorder_;
};

typedef boost::shared_ptr<recorder_t> recorder_obj_t;

} // dvblex
