/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_strings.h>
#include <dl_filesystem_path.h>

namespace dvblink { namespace transcoder {

const std::string stream_type_audio = "audio";
const std::string stream_type_video = "video";
const std::string stream_type_subtitle = "subtitle";

const std::string stream_codec_hevc = "hevc";
const std::string stream_codec_aac_latm = "aac_latm";
const std::string stream_codec_dvb_subtitle = "dvb_subtitle";

struct ffprobe_stream_desc_t
{
    ffprobe_stream_desc_t() 
        : index(0)
    {}

    int index;
    std::string codec;
    std::string codec_type;
};

typedef std::vector<ffprobe_stream_desc_t> ffprobe_stream_desc_list_t;

class tc_ffprobe_wrapper
{
public:
    tc_ffprobe_wrapper(const dvblink::filesystem_path_t& ffprobe_file_path, const dvblink::filesystem_path_t& ffprobe_dir, const dvblink::filesystem_path_t& output_temp_dir);

    bool get_stream_descriptions(const dvblink::filesystem_path_t& stream_file, ffprobe_stream_desc_list_t& stream_list);
protected:
    dvblink::filesystem_path_t ffprobe_file_path_;
    dvblink::filesystem_path_t ffprobe_dir_;
    dvblink::filesystem_path_t output_temp_dir_;
};

} //transcoder
} //dvblink
