/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>

namespace dvblink { namespace transcoder {
/*
inline bool platform_transcoding_supported()
{
    bool transcoding_supported = false;

#if defined(_WIN32) || defined(_SYNOLOGY_X86_64) || defined(_SYNOLOGY_EVANSPORT) || defined(_ASUSTOR_X86_64) || defined(_ASUSTOR_X86) || defined(_NETGEAR_X86_64) || defined(_NETGEAR_R6_X86_64) || defined(_QNAP_X86_64) || defined(_UBUNTU_X86) || defined(_UBUNTU_X86_64) || defined(_WD_X86_64) || defined(_DARWIN_X86_64)
    transcoding_supported = true;
#endif

    return transcoding_supported;
}
*/
struct transcoder_params
{
    transcoder_params() :
        tr_width_(0), tr_height_(0), tr_bitrate_(0), tr_video_scale_(0)
    {}

    std::string audio_track_;
    boost::uint32_t tr_width_;
    boost::uint32_t tr_height_;
    boost::uint32_t tr_bitrate_;
    boost::uint32_t tr_video_scale_;
};

} //transcoder
} //dvblink
