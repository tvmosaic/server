/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#ifdef _WIN32
#include <windows.h>
#endif
#include <boost/thread.hpp>
#include <boost/cstdint.hpp>
#include <dl_platforms.h>
#include <dl_circle_buffer.h>
#include <dl_event.h>
#include <tc_file_handle.h>
//#define BEFORE_PIPE_WRITE_FILE

namespace dvblink { namespace transcoder {

class data_provider
{
    template <typename T>
    friend class ffmpeg_wrapper;

    class pipe_wait_thread
    {
    public:
        pipe_wait_thread();
        ~pipe_wait_thread();

		void thread(const file_handle& pipe, dvblink::engine::ts_circle_buffer* queue);
        void quit() {quit_ = true;}
        void write_handler(dvblink::event* wait_send, int* result, const boost::system::error_code& error, std::size_t bytes_transferred);

    private:
        volatile bool quit_;
#ifdef BEFORE_PIPE_WRITE_FILE
        FILE* m_pFile;
        char m_buffer[BUFSIZ * 10];
#endif
    };

public:
    data_provider(const file_handle& pipe_handle);
    virtual ~data_provider();

    size_t get_free_space() const
    {
        return (queue_.get_free_nodes() * queue_.get_node_size());
    }

protected:
    virtual bool process_data(const unsigned char* data, unsigned int len, boost::int64_t pts_27_mhz);
//    HANDLE pipe_handle() {return pipe_handle_;}

private:
    file_handle pipe_handle_;

    boost::thread* pipe_wait_thread_;
    pipe_wait_thread data_process_thread_;
	dvblink::engine::ts_circle_buffer queue_;
};

} //transcoder
} //dvblink
