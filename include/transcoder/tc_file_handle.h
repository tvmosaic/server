/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#ifdef _WIN32
#include <windows.h>
#else
#endif

namespace dvblink { namespace transcoder {

class file_handle
{
public:
#ifdef _WIN32
    typedef HANDLE handle_type;
#else
    typedef int handle_type;
#endif

    file_handle() :
        handle_(invalid_handle())
    {
    }

    file_handle(handle_type h) :
        handle_(h) 
    { 
        BOOST_ASSERT(handle_ != invalid_handle()); 
    }

    file_handle(const file_handle &fh) :
        handle_(fh.handle_) 
    { 
        fh.handle_ = invalid_handle(); 
    }

    ~file_handle()
    { 
        if (valid()) 
            close(); 
    }

    file_handle& operator=(const handle_type handle)
    { 
        handle_ = handle; 
        return *this; 
    }

    file_handle& operator=(const file_handle& fs)
    { 
        handle_ = fs.handle_;
        fs.handle_ = invalid_handle();
        return *this; 
    }

    bool valid() const 
    { 
        return handle_ != invalid_handle(); 
    }

    void close() 
    { 
        if (valid())
        {
#ifdef _WIN32
            ::CloseHandle(handle_); 
#else
            ::close(handle_); 
#endif 
        }
        handle_ = invalid_handle(); 
    }

    handle_type get() const 
    { 
        BOOST_ASSERT(valid()); 
        return handle_; 
    }

private:
    const handle_type invalid_handle() const
    {
#ifdef _WIN32
        return INVALID_HANDLE_VALUE;
#else
        return -1;
#endif
    }

private:
    mutable handle_type handle_;
};

} //transcoder
} //dvblink
