/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <sstream>
#include <iostream>
#ifndef _WIN32
#include <unistd.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <signal.h>
#endif
#include <boost/asio.hpp> 
#include <boost/thread.hpp>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <dl_strings.h>
#include <dl_ts.h>
#include <dl_filesystem_path.h>
#include <dl_logger.h>
#include <dl_ts_aligner.h>
#include <dl_strings.h>
#include <dl_uuid.h>
#include <tc_file_handle.h>
#include <tc_data_provider.h>

namespace dvblink { namespace transcoder {

#ifdef _WIN32
    const std::string ffmpeg_pipe_name("\\\\.\\pipe\\TVMosaicTranscoderTsPipe");
#endif

const unsigned long buf_size = dvblink::engine::TS_PACKET_SIZE * 8;

template <typename T>
class ffmpeg_wrapper
{
    typedef void (T::*send_fn)(const boost::uint8_t*, boost::uint32_t);
    send_fn fn_;
    T* t_;

public:
    typedef std::vector<dvblink::launch_param_t> ffmpeg_params_t;

public:
    ffmpeg_wrapper(T* t, send_fn f,
        const dvblink::filesystem_path_t& ffmpeg_exepath,
        const dvblink::filesystem_path_t& ffmpeg_dir,
        bool use_aligner = true);
    virtual ~ffmpeg_wrapper();

    void push_packets(const boost::uint8_t* ts_packet, boost::uint32_t length);

    bool is_started() const {return is_started_;}

    bool start(const ffmpeg_params_t& launch_params, const std::vector<dvblink::env_var_t>& env_vars);
    bool stop();

    bool is_queues_empty();
    bool process_data(const dvblink::engine::u_char* pBuffer, unsigned long uSize, bool overwrite = true);

    const std::string get_in_pipe_name() {return pipe_in_name_;}
    const std::string get_out_pipe_name() {return pipe_out_name_;}

private:
    bool setup_pipes();
    bool launch_ffmpeg(const ffmpeg_params_t& launch_params, const std::vector<dvblink::env_var_t>& env_vars);
#ifdef _WIN32
    bool create_ffmpeg_process(const wchar_t* app);
#else
    bool create_ffmpeg_process(char* const* arg_list, char* const* env_list);
    void start_read();
    void handle_read_input(const boost::system::error_code& error, std::size_t length);
#endif
    void terminate_ffmpeg();
    void read_thread();
    static void __stdcall aligner_callback(const unsigned char* buf, unsigned long len, void* user_param);

private:
    bool is_started_;
    bool ffmpeg_launched_;
    dvblink::filesystem_path_t ffmpeg_path_;
    dvblink::filesystem_path_t ffmpeg_dir_;

    file_handle ffmpeg_handle_;

	bool read_thread_quit_;
	boost::thread* read_thread_handle_;
    file_handle ffmpeg_in_pipe_;
    file_handle ffmpeg_out_pipe_;
    std::string pipe_in_name_;
    std::string pipe_out_name_;

    data_provider* data_provider_;
    dvblink::engine::ts_packet_aligner* aligner_;

#ifndef _WIN32
	boost::asio::io_service io_service_;
    boost::asio::posix::stream_descriptor* input_;
    unsigned char input_buffer_[buf_size];
#endif
};

template <typename T>
ffmpeg_wrapper<T>::ffmpeg_wrapper(T* t, send_fn fn,
        const dvblink::filesystem_path_t& ffmpeg_exepath,
        const dvblink::filesystem_path_t& ffmpeg_dir,
        bool use_aligner) :
    fn_(fn),
    t_(t),
    is_started_(false),
    ffmpeg_launched_(false),
    ffmpeg_path_(ffmpeg_exepath),
	ffmpeg_dir_(ffmpeg_dir),
    ffmpeg_handle_(),
	read_thread_handle_(NULL),
    data_provider_(NULL),
    aligner_(NULL)
#ifndef _WIN32
    ,input_(NULL)
#endif
{
    if (use_aligner)
    {
        aligner_ = new dvblink::engine::ts_packet_aligner(aligner_callback, this);
    }

#ifdef _WIN32
    std::stringstream pipe_name_in, pipe_name_out;

    std::string guid;
    dvblink::engine::uuid::gen_uuid(guid);
    //input pipe
    pipe_name_in << ffmpeg_pipe_name << "_" << guid << "_input";
    pipe_in_name_ = pipe_name_in.str();
    //output pipe
    pipe_name_out << ffmpeg_pipe_name << "_" << guid << "_output";
    pipe_out_name_ = pipe_name_out.str();
#else
    pipe_in_name_ = "pipe:0";
    pipe_out_name_ = "pipe:1";
#endif
}

template <typename T>
ffmpeg_wrapper<T>::~ffmpeg_wrapper()
{
    stop();
}

#ifdef _WIN32
template <class T>
bool ffmpeg_wrapper<T>::create_ffmpeg_process(const wchar_t* app)
{
	PROCESS_INFORMATION piProcInfo; 
	STARTUPINFO siStartInfo;
	BOOL bSuccess = false; 
    
    // Set up members of the PROCESS_INFORMATION structure
	ZeroMemory(&piProcInfo, sizeof(PROCESS_INFORMATION));
    
    // Set up members of the STARTUPINFO structure
	ZeroMemory(&siStartInfo, sizeof(STARTUPINFO));
	siStartInfo.cb = sizeof(STARTUPINFO); 
	siStartInfo.dwFlags |= STARTF_USESHOWWINDOW;
	siStartInfo.wShowWindow = SW_NORMAL;

	bSuccess = CreateProcessW(NULL, 
		(wchar_t*)app,  // command line 
		NULL,           // process security attributes 
		NULL,           // primary thread security attributes 
		FALSE,          // handles are inherited 
		0,              // creation flags 
		NULL,           // use parent's environment 
        ffmpeg_dir_.empty() ? NULL : ffmpeg_dir_.c_str(),// use parent's current directory
		&siStartInfo,   // STARTUPINFO pointer 
		&piProcInfo);   // receives PROCESS_INFORMATION 
   
   // If an error occurs, exit the application. 
	if (bSuccess)
	{
		ffmpeg_handle_ = piProcInfo.hProcess;
		CloseHandle(piProcInfo.hThread);
	}
    else
	{
        dvblink::logging::log_error(L"create_ffmpeg_process: CreateProcess() failed (%1%)") % app;
	}

    return bSuccess ? true : false;
}
#else

#ifndef OPEN_MAX_DESC
	#define OPEN_MAX_DESC 256
#endif

template <class T>
bool ffmpeg_wrapper<T>::create_ffmpeg_process(char* const* arg_list, char* const* env_list)
{
    bool res = false;

    int pipes[4];
//    pipe2(pipes, O_NONBLOCK);		// set up 1st pipe
//    pipe2(pipes + 2, O_NONBLOCK);	// set up 2nd pipe
    pipe(pipes);		// set up 1st pipe
    pipe(pipes + 2);	// set up 2nd pipe
    int* pipe_in = pipes;
    int* pipe_out = pipes + 2;

    ffmpeg_handle_ = fork();
    if (ffmpeg_handle_.get() == -1)
    {
    	dvblink::logging::log_error(L"ffmpeg_wrapper<T>::create_ffmpeg_process: fork() failed");
		close(pipe_in[0]);
		close(pipe_in[1]);
		close(pipe_out[0]);
		close(pipe_out[1]);
    } else
    {
		if (ffmpeg_handle_.get() == 0)
		{
			  int         fd, fds;
			  struct stat st;

			  /* Make sure all open descriptors other than the standard ones are closed */
                #ifndef __ANDROID__
                  if ((fds = getdtablesize()) == -1)
                #else
                    if ((fds = sysconf(_SC_OPEN_MAX)) == -1)
                #endif
				  fds = OPEN_MAX_DESC;
			  for (fd = 3;  fd < fds;  fd++)
			  {
				  if (fd != pipe_in[0] && fd != pipe_in[1] && fd != pipe_out[0] && fd != pipe_out[1])
					  close(fd);
			  }

			// child process
			close(pipe_in[1]);
			dup2(pipe_in[0], STDIN_FILENO);
			close(pipe_in[0]);

			close(pipe_out[0]);
			dup2(pipe_out[1], STDOUT_FILENO);
			close(pipe_out[1]);

			std::string path = dvblink::engine::string_cast<dvblink::engine::EC_UTF8>(ffmpeg_path_.c_str());
			execve(path.c_str(), arg_list, env_list);
			dvblink::logging::log_error(L"ffmpeg_wrapper<T>::create_ffmpeg_process: execvp() failed");
			exit(0);
		}
		else
		{
			// parent process
			close(pipe_in[0]);
			ffmpeg_in_pipe_ = pipe_in[1];

			close(pipe_out[1]);
			ffmpeg_out_pipe_ = pipe_out[0];
			res = true;
		}
    }

    return res;
}
#endif

template <typename T>
bool ffmpeg_wrapper<T>::launch_ffmpeg(const ffmpeg_params_t& launch_params, const std::vector<dvblink::env_var_t>& env_vars)
{
	bool res = false;
    std::string command_line = ffmpeg_path_.to_string();

    //log exe and command line params
	std::wostringstream bufstr;
    bufstr << ffmpeg_path_.to_wstring() << L" ";

	for (size_t i=0; i<launch_params.size(); i++)
        bufstr << launch_params[i].to_wstring() << L" ";

    logging::log_ext_info(L"launch_ffmpeg. Executing %1%") % bufstr.str();

#ifdef _WIN32
    for (size_t i = 0; i < launch_params.size(); i++)
	{
    	command_line += " ";
    	command_line += launch_params[i].to_string();
	}
	
    std::wstring wcommand_line = dvblink::engine::string_cast<dvblink::engine::EC_UTF8>(command_line.c_str());
	res = create_ffmpeg_process(wcommand_line.c_str());
#else

    //command line arguments
	size_t argnum = launch_params.size() + 2;
	if (char** arg_list = new char*[argnum])
	{
		arg_list[0] = strdup(command_line.c_str());

		for (size_t i = 0; i < launch_params.size(); i++)
		{
			arg_list[i + 1] = strdup(launch_params[i].c_str());
		}

		arg_list[launch_params.size() + 1] = NULL;

        //environment vars
        size_t env_var_num = env_vars.size() + 1;
        if (!ffmpeg_dir_.empty())
            env_var_num += 1;

        char** env_list = new char*[env_var_num];
        size_t env_start_idx = 0;
        if (!ffmpeg_dir_.empty())
        {
		    std::string envstr = ffmpeg_dir_.to_string();
		    envstr = "LD_LIBRARY_PATH="+ envstr;
		    env_list[env_start_idx] = strdup(envstr.c_str());
            env_start_idx += 1;
        }

        for (size_t i=0; i<env_vars.size(); i++)
    		env_list[env_start_idx + i] = strdup(env_vars[i].c_str());

		env_list[env_var_num - 1] = NULL;

        for (size_t i = 0; i < env_var_num - 1; i++)
            dvblink::logging::log_ext_info(L"ffmpeg_wrapper<T>::create_ffmpeg_process: env var %1%") % dvblink::engine::string_cast<dvblink::engine::EC_UTF8>(env_list[i]);

		res = create_ffmpeg_process(arg_list, env_list);

		//delete command line parameters
		for (size_t i = 0; i < argnum - 1; i++)
			free(arg_list[i]);

		delete arg_list;

		//delete environmant vars
		for (size_t i = 0; i < env_var_num - 1; i++)
			free(env_list[i]);

		delete env_list;    
    }
	else
	{
    	dvblink::logging::log_error(L"ffmpeg_wrapper<T>::launch_ffmpeg: new() failed");
	}
#endif
	return res;
}

template <typename T>
void ffmpeg_wrapper<T>::terminate_ffmpeg()
{
#ifdef _WIN32
    TerminateProcess(ffmpeg_handle_.get(), 0);
#else
    kill(ffmpeg_handle_.get(), SIGKILL);
    waitpid(ffmpeg_handle_.get(), NULL, 0);
#endif
}

#ifdef _WIN32
template <typename T>
void ffmpeg_wrapper<T>::read_thread()
{
    // start an overlapped connect operation
    OVERLAPPED ov;
    memset(&ov, 0, sizeof(ov));
    ov.hEvent = CreateEvent(NULL, TRUE, TRUE, NULL);

    if (ConnectNamedPipe(ffmpeg_out_pipe_.get(), &ov))
    {
        // overlapped ConnectNamedPipe should return FALSE
        return;
    }

    switch (GetLastError()) 
    { 
    case ERROR_IO_PENDING: 
        // overlapped connection in progress
        break; 

    case ERROR_PIPE_CONNECTED:
        // client is already connected, so signal an event
        SetEvent(ov.hEvent);
        break;

    default:
        // an error occurs
        return;
    }

    unsigned char buf[buf_size] = { 0 };

    unsigned long bytes_read = 0;
    while (!read_thread_quit_)
    {
        if (WaitForSingleObject(ov.hEvent, 100) != WAIT_OBJECT_0)
        {
            continue;
        }

        if (read_thread_quit_)
        {
            break;
        }

        bytes_read = 0;
        BOOL ok = GetOverlappedResult(ffmpeg_out_pipe_.get(), &ov, &bytes_read, FALSE);
        if (!ok)
        {
            if (GetLastError() == ERROR_IO_PENDING)
            {
                continue;
            }
            else
            {
                break;
            }
        }
        else
        if (bytes_read > 0)
        {
            if (aligner_)
            {
                aligner_->write_stream(buf, bytes_read);
            }
            else
            {
                (t_->*(ffmpeg_wrapper<T>::fn_))(buf, bytes_read);
            }
        }

        ok = ReadFile(ffmpeg_out_pipe_.get(), buf, buf_size, &bytes_read, &ov);
        if (!ok)
        {
            if (GetLastError() == ERROR_IO_PENDING)
            {
                continue;
            }
            else
            {
                break;
            }
        }
        else
        if (bytes_read > 0)
        {
            SetEvent(ov.hEvent);
        }
        else
        {
            break;
        }
	}
    
    DisconnectNamedPipe(ffmpeg_out_pipe_.get());
}
#else
template <typename T>
void ffmpeg_wrapper<T>::read_thread()
{
	input_ = new boost::asio::posix::stream_descriptor(io_service_, ffmpeg_out_pipe_.get());

	start_read();
	io_service_.run();

	delete input_;
	input_ = NULL;
}

template <typename T>
void ffmpeg_wrapper<T>::start_read()
{
	boost::asio::async_read(*input_, boost::asio::buffer(input_buffer_, buf_size),
			  boost::bind(&ffmpeg_wrapper<T>::handle_read_input, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

template <typename T>
void ffmpeg_wrapper<T>::handle_read_input(const boost::system::error_code& error, std::size_t bytes_read)
{
    if (!error && !read_thread_quit_)
    {
        if (bytes_read)
        {
            if (aligner_)
            {
                aligner_->write_stream(input_buffer_, bytes_read);
            }
            else
            {
                (t_->*(ffmpeg_wrapper<T>::fn_))(input_buffer_, bytes_read);
            }
        }
        start_read();
    }
    else
    {
    }
}
#endif

template <typename T>
void ffmpeg_wrapper<T>::aligner_callback(const unsigned char* buf,
    unsigned long len, void* user_param)
{
    ffmpeg_wrapper<T>* this_ = reinterpret_cast<ffmpeg_wrapper<T>* >(user_param);
    (this_->t_->*(this_->fn_))(buf, len);
}

template <typename T>
bool ffmpeg_wrapper<T>::setup_pipes()
{
#ifdef _WIN32
    ffmpeg_out_pipe_ = CreateNamedPipeA(
        pipe_out_name_.c_str(),
        PIPE_ACCESS_INBOUND | FILE_FLAG_OVERLAPPED,
        PIPE_WAIT,
        PIPE_UNLIMITED_INSTANCES,
        dvblink::engine::TS_PACKET_SIZE * 1024, // output buffer size
        dvblink::engine::TS_PACKET_SIZE * 1024, // input buffer size 
        NMPWAIT_USE_DEFAULT_WAIT,
        NULL);

    ffmpeg_in_pipe_ = CreateNamedPipeA(
        pipe_in_name_.c_str(),
        PIPE_ACCESS_OUTBOUND | FILE_FLAG_OVERLAPPED,
        0,
        PIPE_UNLIMITED_INSTANCES,
        dvblink::engine::TS_PACKET_SIZE * 1024,
        dvblink::engine::TS_PACKET_SIZE * 1024,
        NMPWAIT_USE_DEFAULT_WAIT,
        NULL);

    return ffmpeg_out_pipe_.valid() && ffmpeg_in_pipe_.valid();
#else
    return true;
#endif
}

template <typename T>
bool ffmpeg_wrapper<T>::start(const ffmpeg_params_t& launch_params, const std::vector<dvblink::env_var_t>& env_vars)
{
    if (ffmpeg_out_pipe_.valid())
    {
        return false; // not stopped
    }

    if (setup_pipes())
    {
#ifdef _WIN32
        data_provider_ = new data_provider(ffmpeg_in_pipe_);
        ffmpeg_launched_ = launch_ffmpeg(launch_params, env_vars);
#else
        ffmpeg_launched_ = launch_ffmpeg(launch_params, env_vars);
        if (ffmpeg_launched_)
        	data_provider_ = new data_provider(ffmpeg_in_pipe_);
#endif
        
        if (!ffmpeg_launched_)
        {
            stop();
        }
        else
        {
		    read_thread_quit_ = false;
		    read_thread_handle_ = new boost::thread(boost::bind(&ffmpeg_wrapper::read_thread, this));

            dvblink::engine::sleep(400); // short delay
            is_started_ = true;
        }
    }

    return is_started_;
}

template <typename T>
bool ffmpeg_wrapper<T>::stop()
{
    is_started_ = false;

    if (ffmpeg_launched_)
    {
        terminate_ffmpeg();
        ffmpeg_launched_ = false;
    }

	if (data_provider_ != NULL)
	{
		delete data_provider_;
		data_provider_ = NULL;
	}

	if (read_thread_handle_)
	{
        read_thread_quit_ = true;
#ifndef _WIN32
        io_service_.stop();
#endif
		read_thread_handle_->join();
		delete read_thread_handle_;
		read_thread_handle_ = NULL;
	}

    ffmpeg_in_pipe_.close();
    ffmpeg_out_pipe_.close();

    return true;
}

template <typename T>
bool ffmpeg_wrapper<T>::process_data(const dvblink::engine::u_char* pBuffer,
    unsigned long uSize, bool overwrite)
{
	if (!data_provider_ || !pBuffer)
    {
        return false;
    }

    if (!overwrite && (data_provider_->get_free_space() < uSize))
    {
        return false;
    }

    return data_provider_->process_data(pBuffer, uSize, 0);
}

template <typename T>
void ffmpeg_wrapper<T>::push_packets(const boost::uint8_t* ts_packet,
    boost::uint32_t length)
{
}

} //transcoder
} //dvblink
