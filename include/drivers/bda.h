/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <dl_ts.h>
#include <dl_ts_aligner.h>

#include <drivers/deviceapi.h>
#include <drivers/tuner_factory.h>

interface IGraphBuilder;
interface IBaseFilter;
interface IMediaControl;
interface ITuner;
interface ITuningSpace;
interface IBDA_FrequencyFilter;
interface IBDA_SignalStatistics;
interface IBDA_LNBInfo;
interface IBDA_DigitalDemodulator;
interface IBDA_DiseqCommand;

class sample_grabber_man_t;
class property_set_manager_t;
class ext_ci_manager;

namespace dvblink
{

class bda_tuner_factory_t : public tuner_factory_t
{
public:
    bda_tuner_factory_t(const dvblink::filesystem_path_t& device_config_path);

    static const char* get_name(){return "bda";}

    virtual std::string get_factory_name(){return get_name();}
    virtual int DeviceGetList(PDevAPIDevListEx pDL);
    virtual int ExtCIDeviceGetList(const std::string& device_path, PExtCIDeviceList pDL);
    virtual tuner_t* get_tuner(const std::string& device_path, int fdx);
    virtual void get_aux_list(aux_module_list_t& aux_list);

protected:
    static void device_enum_thread(dvblink::PDevAPIDevListEx pDL);
    static void ext_ci_device_enum_thread(dvblink::PExtCIDeviceList pDL);
};

class bda_tuner_t : public tuner_t
{
public:
    bda_tuner_t(const dvblink::filesystem_path_t& dll_path, const std::string& device_path, int fdx);
    ~bda_tuner_t();

    virtual void SetExtCIDevice(const std::wstring& ext_ci_device_id);
    virtual int StartDevice(DL_E_TUNER_TYPES tuner_type);
	virtual int StopDevice();
	virtual int AddFilter(int pid);
	virtual int DelFilter(int pid);
	virtual int SetTuner(PTransponderInfo Tp);
	virtual int GetTunerState(PSignalInfo TunerState, PTransponderInfo Tp);
	virtual int SendDiseqc(PDiseqcCmd RawDiseqc, int ToneBurst);
	virtual int CISendPMT(unsigned char *buf, int len, int listmng);
	virtual int LNBPower(DL_E_LNB_POWER_TYPES lnb_power);

protected:
    DL_E_TUNER_TYPES tuner_type_;
    IGraphBuilder *pGraph_;
    IBaseFilter *pfltnetworkprovider_;
    sample_grabber_man_t* sample_grabber_man_;
    IBaseFilter *pfltdemuxer_;
    IBaseFilter *pflttif_;
    IMediaControl *pmediactrl_;
    ITuner *ptuner_;
    ITuningSpace *pTuningSpace_;
    bool is_device_started_;
    int Tone_; // Tone burst value
    int Diseqc_;
    DL_E_SERVICE_DECRYPTION_CMD decryption_state_;
    IBDA_FrequencyFilter *pBDAFrequencyFilter_;
    IBDA_SignalStatistics *pBDASignalStatistics_;
    IBDA_LNBInfo* pBDALNBInfo_;
    IBDA_DigitalDemodulator* pBDADigitalDemodulator_;
    IBDA_DiseqCommand* pDiseqcCommandInterface_;
    IBaseFilter *pfltTuner_;
    IBaseFilter *pfltCapture_;
    IBaseFilter *pfltCaptureEx_;
    IBaseFilter *pfltCaptureEx1_;
    wchar_t wsTunerName_[256];
    wchar_t wsDevicePath_[512];
    property_set_manager_t* ps_man_;
    ext_ci_manager* ext_ci_man_;
    dvblink::engine::ts_packet_aligner packet_aligner_;
    dvblink::filesystem_path_t dll_path_;
    std::wstring ext_ci_id_;

    int BDACleanUp();
    HRESULT BDASetTuner(int tuner_type, PTransponderInfo Tp, int diseqc_port, unsigned long timeout);
    void update_property_set_graph_info();
    HRESULT ConnectExtCIFilter(IBaseFilter* upstream_filter, IBaseFilter* sample_grabber_filter);
    static void grabber_callback(unsigned char* data, int len, void* param);
    static void __stdcall aligner_callback(const unsigned char* buf, unsigned long len, void* user_param);
};

} //namespace dvblink

