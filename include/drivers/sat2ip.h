/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <drivers/deviceapi.h>
#include <drivers/tuner_factory.h>

namespace dvblink
{

class sat2ip_tuner_factory_t : public tuner_factory_t
{
public:
    sat2ip_tuner_factory_t(const dvblink::filesystem_path_t& device_config_path);

    static const char* get_name(){return "sat2ip";}

    virtual std::string get_factory_name(){return get_name();}
    virtual int DeviceGetList(PDevAPIDevListEx pDL);
    virtual int ExtCIDeviceGetList(const std::string& device_path, PExtCIDeviceList pDL);
    virtual tuner_t* get_tuner(const std::string& device_path, int fdx);
    virtual bool get_tuner_info(const std::string& device_path, int fdx, TDevAPIDevInfoEx& tuner_info);
};

class sat2ip_streamer;

class sat2ip_tuner_t : public tuner_t
{
public:
    sat2ip_tuner_t(const std::string& device_path, int fdx);
    ~sat2ip_tuner_t();

    virtual void SetExtCIDevice(const std::wstring& ext_ci_device_id);
    virtual int StartDevice(DL_E_TUNER_TYPES tuner_type);
	virtual int StopDevice();
	virtual int AddFilter(int pid);
	virtual int DelFilter(int pid);
	virtual int SetTuner(PTransponderInfo Tp);
	virtual int GetTunerState(PSignalInfo TunerState, PTransponderInfo Tp);
	virtual int SendDiseqc(PDiseqcCmd RawDiseqc, int ToneBurst);
	virtual int CISendPMTPid(unsigned short pmt_pid, unsigned short sid, int listmng);
protected:
    sat2ip_streamer* streamer_;
    dvblink::DL_E_DISEQC_TYPE last_diseqc_;
    std::wstring ext_ci_device_path_;

    static void stream_function(const unsigned char* buffer, size_t length, void* user_param);
};

} //namespace dvblink

