/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
//-----------------------------------------------------------------------------
// Copyright(c) 2008-2011 DVBLogic (info@dvblogic.com)
// All rights reserved
//-----------------------------------------------------------------------------
#pragma once

#include <dl_filesystem_path.h>
#include <drivers/deviceapi.h>
#include <drivers/aux_module.h>

#ifdef WIN32
    #pragma warning(disable: 4100)
#endif

namespace dvblink
{

class tuner_t
{
public:
    typedef void TTSCallback(unsigned char *Data, int len, void* param);

public:
    tuner_t(const std::string& device_path, int fdx) :
      callback_(NULL), callback_param_(NULL), device_path_(device_path), fdx_(fdx) {}

        virtual ~tuner_t() {}

	void SetCallback(TTSCallback *callback, void* param){callback_ = callback; callback_param_ = param;}; // Sets TS callback function

    virtual void SetExtCIDevice(const std::wstring& ext_ci_device_id){}
    virtual int StartDevice(DL_E_TUNER_TYPES tuner_type){return dvblink::FAIL;}
	virtual int StopDevice(){return dvblink::FAIL;}
	virtual int AddFilter(int pid){return dvblink::FAIL;} // Requests a pid if hardware does HW filtering.
	virtual int DelFilter(int pid){return dvblink::FAIL;} // Deletes pid from HW filters
	virtual int SetTuner(PTransponderInfo Tp){return dvblink::FAIL;}
	virtual int GetTunerState(PSignalInfo TunerState, PTransponderInfo Tp){return dvblink::FAIL;} // Gets signal level
	virtual int LNBPower(DL_E_LNB_POWER_TYPES lnb_power){return dvblink::FAIL;}
	virtual int SendDiseqc(PDiseqcCmd RawDiseqc, int ToneBurst){return dvblink::FAIL;}
	virtual int CISendPMT(unsigned char *buf, int len, int listmng){return dvblink::FAIL;}
	virtual int CISendPMTPid(unsigned short pmt_pid, unsigned short sid, int listmng){return dvblink::FAIL;}

    const std::string& get_device_path(){return device_path_;}
    int get_frontend_idx(){return fdx_;}

protected:
    TTSCallback* callback_;
    void* callback_param_;
    std::string device_path_;
    int fdx_;
};

class tuner_factory_t
{
public:
    tuner_factory_t(const dvblink::filesystem_path_t& device_config_path) :
        device_config_path_(device_config_path)
    {}

    virtual int DeviceGetList(PDevAPIDevListEx pDL){return dvblink::FAIL;}
    virtual int ExtCIDeviceGetList(const std::string& device_path, PExtCIDeviceList pDL){return dvblink::FAIL;}
    virtual tuner_t* get_tuner(const std::string& device_path, int fdx){return NULL;}
    virtual std::string get_factory_name(){return "";}
    virtual void get_aux_list(aux_module_list_t& aux_list){}

    virtual bool get_tuner_info(const std::string& device_path, int fdx, TDevAPIDevInfoEx& tuner_info)
    {
        bool ret_val = false;

        std::wstring wdevice_path = dvblink::engine::string_cast<dvblink::engine::EC_UTF8>(device_path);

        TDevAPIDevListEx device_list;
        DeviceGetList(&device_list);
        for (int i=0; i<device_list.Count; i++)
        {
            if (boost::iequals(device_list.Devices[i].szDevicePathW, wdevice_path) && device_list.Devices[i].FrontendIdx == fdx)
            {
                tuner_info = device_list.Devices[i];
                ret_val = true;
                break;
            }
        }
        return ret_val;
    }

protected:
    dvblink::filesystem_path_t device_config_path_;
};
} //namespace dvblink
