/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <drivers/deviceapi.h>
#include <drivers/tuner_factory.h>
#include <boost/thread.hpp>
#include <dl_ts_aligner.h>

namespace dvblink
{

class stream_converter;

class file_tuner_factory_t : public tuner_factory_t
{
public:
    file_tuner_factory_t(const dvblink::filesystem_path_t& device_config_path) :
        tuner_factory_t(device_config_path)
    {}

    static const char* get_name(){return "file";}

    virtual std::string get_factory_name(){return get_name();}
    virtual int DeviceGetList(PDevAPIDevListEx pDL);
    virtual tuner_t* get_tuner(const std::string& device_path, int fdx);
};

class file_tuner_t : public tuner_t
{
public:
    file_tuner_t(const std::string& device_path, int fdx, const dvblink::filesystem_path_t& dir);
    ~file_tuner_t();

    virtual int StartDevice(DL_E_TUNER_TYPES tuner_type);
	virtual int StopDevice();
	virtual int SetTuner(PTransponderInfo Tp);
	virtual int GetTunerState(PSignalInfo TunerState, PTransponderInfo Tp);
protected:
    dvblink::filesystem_path_t stream_file_;
    stream_converter* stream_converter_;
    boost::thread* streaming_thread_;
    volatile bool exit_flag_;
    engine::ts_packet_aligner packet_aligner_;

    static void process(const unsigned char* buf, size_t len, void* user_param);
    static void __stdcall aligner_callback(const unsigned char* buf, unsigned long len, void* user_param);
    void streaming_thread();
    FILE* open_file();
    void read_file(FILE* f);
};

} //namespace dvblink

