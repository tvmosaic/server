/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <dl_platforms.h>

namespace dvblink
{
	enum DL_E_STATUS_CODES
	{
		SUCCESS = 0,
		FAIL,
		ENOMEMORY,
		EHARDWARE,
		ETIMEOUT,
		ENOTIMPLEMENTED
	};

	// Polarizations
	enum  DL_E_POLARIZARION_TYPES
	{
		POL_HORIZONTAL = 0, 	/* 18 Volt */
		POL_VERTICAL = 1, 		/* 13 Volt */
		POL_LNBPOWEROFF = 10	/* 0 Volt */
	};

	// LNB power
	enum  DL_E_LNB_POWER_TYPES
	{
        POW_OFF,
        POW_13V,
        POW_18V
	};

	// Tuner Types
	enum DL_E_TUNER_TYPES
	{
		TUNERTYPE_UNKNOWN = 0x00000000,
		TUNERTYPE_DVBS = 0x00000001,
		TUNERTYPE_DVBC = 0x00000002,
		TUNERTYPE_DVBT = 0x00000004,
		TUNERTYPE_ATSC = 0x00000008,
		TUNERTYPE_CLEARQAM = 0x00000010
	};

	// FEC,  ETSI 302 307
	enum DL_E_FEC_TYPES
	{
		FEC_AUTO = 0x00,
		FEC_1_2 = 0x12,
		FEC_1_3 = 0x13,
		FEC_1_4 = 0x14,
		FEC_2_3 = 0x23,
		FEC_2_5 = 0x25,
		FEC_3_4 = 0x34,
		FEC_3_5 = 0x35,
		FEC_4_5 = 0x45,
		FEC_5_6 = 0x56,
		FEC_7_8 = 0x78,  // not in specs (used by DN)
		FEC_8_9 = 0x89,
		FEC_9_10 = 0x9a,
		FEC_6_7 = 0x67,
		FEC_5_11 = 0x5b
	};

	// LNB Selection - Khz
	enum DL_E_LNB_SELECTION
	{
		LNB_SELECTION_0 = 0,
		LNB_SELECTION_22 = 1,
		LNB_SELECTION_33 = 2,
		LNB_SELECTION_44 = 3,
		LNB_SELECTION_COUNT = 4
	};

	// Tone Burst  (Simple Diseqc)
	enum DL_E_TONE_BURST
	{
		TONEBURST_NONE = 0,	   	// Tone/Data burst OFF,
		TONEBURST_1 = 1,		// Tone Burst ON
		TONEBURST_2 = 2			// Data Burst ON
	};

	// QAM Values
	enum DL_E_MOD_DVBC
	{
		MOD_DVBC_QAM_4 = 2, 	// DVB-C: Modulation:  4-QAM
		MOD_DVBC_QAM_16 = 3, 	// DVB-C: Modulation:  16-QAM
		MOD_DVBC_QAM_32 = 4, 	// DVB-C: Modulation:  32-QAM
		MOD_DVBC_QAM_64 = 5, 	// DVB-C: Modulation:  64-QAM (default)
		MOD_DVBC_QAM_128 = 6, 	// DVB-C: Modulation: 128-QAM
		MOD_DVBC_QAM_256 = 7 	// DVB-C: Modulation: 256-QAM
	};

	//  DVB-S Modulations,  ETSI DVB-S2 March 2005 specs introduces 4 modulation types
	enum DL_E_MOD_DVBS
	{
		 MOD_DVBS_QPSK = 0,
		 MOD_DVBS_8PSK = 1, 	// DVB-S2 8PSK
		 MOD_DVBS_16APSK = 2,
		 MOD_DVBS_32APSK = 3,
		 MOD_TURBO_QPSK = 4, 	// Dish Network QPSK (Turbo)
		 MOD_TURBO_8PSK = 5, 	// Dish Network 8PSK (Turbo)
		 MOD_DVBS_NBC_QPSK = 6 	// DVB-S2 QPSK (NBC)
	};

	// Bandwidth
	enum DL_E_BANDWIDTH
	{
		BANDWIDTH_6_MHZ = 1,
		BANDWIDTH_7_MHZ = 2,
		BANDWIDTH_8_MHZ = 3
	};

	// Spectral Inversion
	enum DL_E_SPECTRAL_INVERSION
	{
		SPECTRAL_INVERSION_AUTO = 2,
		SPECTRAL_INVERSION_ON = 0,
		SPECTRAL_INVERSION_OFF = 1
	};

	//Diseqc Values
	enum DL_E_DISEQC_TYPE
	{
		DISEQC_NONE = 0,
		DISEQC_SIMPLE_A = 1,
		DISEQC_SIMPLE_B = 2,
		DISEQC_LEVEL_1_A_A = 3,
		DISEQC_LEVEL_1_B_A = 4,
		DISEQC_LEVEL_1_A_B = 5,
		DISEQC_LEVEL_1_B_B = 6,
		DISEQC_LEVEL_11_1 = 7,
		DISEQC_LEVEL_11_2 = 8,
		DISEQC_LEVEL_11_3 = 9,
		DISEQC_LEVEL_11_4 = 10,
		DISEQC_LEVEL_11_5 = 11,
		DISEQC_LEVEL_11_6 = 12,
		DISEQC_LEVEL_11_7 = 13,
		DISEQC_LEVEL_11_8 = 14,
		DISEQC_LEVEL_11_9 = 15,
		DISEQC_LEVEL_11_10 = 16,
		DISEQC_LEVEL_11_11 = 17,
		DISEQC_LEVEL_11_12 = 18,
		DISEQC_LEVEL_11_13 = 19,
		DISEQC_LEVEL_11_14 = 20,
		DISEQC_LEVEL_11_15 = 21,
		DISEQC_LEVEL_11_16 = 22,
		DISEQC_12 = 23,
		DISEQC_UNICABLE = 24
	};

	enum DL_E_SERVICE_DECRYPTION_CMD
	{
		SERVICE_DECRYPT_ADD = 0,
		SERVICE_DECRYPT_REMOVE,
		SERVICE_DECRYPT_FIRST, //used internally by the device driver module
		SERVICE_DECRYPT_UPDATE,
	};

	enum DL_E_TUNER_CONNECTION_TYPE
	{
		TCT_UNKNOWN = 0,
        TCT_NETWORK,
        TCT_LOCAL,
        TCT_USB,
        TCT_PCIE
	};

	/////////////////////////////////////////////////////////////////////////////////
	// ALIGNED STRUCTURES
	////////////////////////////////////////////////////////////////////////////////
	// some structures aligned, some are not, so we use the following
#ifdef WIN32
	#pragma pack(push, 1)
#endif

	// Signal Information
	typedef struct
	{
		unsigned long dwSize;
		unsigned char Level;
		unsigned char Quality;
		unsigned char Locked;
	}TSignalInfo, *PSignalInfo ;

	// Transponder Info
	typedef struct
	{
		int dwSize;
		int dwModulation;	//DVB-S:MOD_DVBS_xxx, DVB-C/DVB-T: not used
		int dwFreq;      // For DVB-x: Frequency in KHz, for ATSC - physical channel number
		unsigned char  Pol;         // Polarization. DVB-S: POL_xxx, DVB-T/C - not used
		int dwSr;        // Symbol Rate in Kc,  DVB-S/DVB-C - symbol rate
		int dwLOF;       // LOF in KHz for DVB-S, DVB-C: MOD_DVBC_QAM_xxx, bandwidth for DVB-T (numbers as Mhz:6, 7 or 8)
		int dwLnbKHz;    // DVB-S only: LNB_SELECTION_xxx
		int dwInversion; // Spectral Inversion. Not used.
		int dwFec;       // Viterbi Rate, DVB-S only, FEC_xxx
		//-------------- New: check structure size!! ---------------
		//LOF settings
		unsigned long LOF1;		//LOF 1
		unsigned long LOF2;		//LOF 2
		unsigned long LOFSW;	//LOF sw
		int plp_id; // DVB-T: value is 0. DVB-T2: PLP ID = value - 1
	} TTransponderInfo, *PTransponderInfo;

	// Raw Diseqc Command Data
	typedef struct
	{
		unsigned long dwSize;
		unsigned char iLength;
		unsigned char Data[32];
	}TDiseqcCmd, *PDiseqcCmd;

	typedef struct
	{
		char szName[256];
		int TunerType;
	} TDevAPIDevInfo, *PDevAPIDevInfo;

#ifdef WIN32
	#define DL_DEVAPI_MAX_DEVICES	48
#else
	#define DL_DEVAPI_MAX_DEVICES	32
#endif

	typedef struct
	{
		int Count;
		TDevAPIDevInfo Devices[DL_DEVAPI_MAX_DEVICES];
	} TDevAPIDevList, *PDevAPIDevList;

	struct TDevAPIDevInfoEx
	{
        TDevAPIDevInfoEx() :
            TunerType(TUNERTYPE_UNKNOWN), TunerIdx(0), FrontendIdx(0)
        {
            szName[0] = '\0';
            szDevicePathW[0] = L'\0';
            uri[0] = '\0';
            make[0] = '\0';
            model[0] = '\0';
            modelNum[0] = '\0';
            uuid[0] = '\0';
            strcpy(uuid_make, "unknown");
            vid[0] = '\0';
            pid[0] = '\0';
            connection_type = TCT_LOCAL;
            fta_concurrent_streams = -1;
            enc_concurrent_streams = 1;
        }

		char szName[256];
		wchar_t szDevicePathW[512];
		int TunerType;
        int TunerIdx;
        int FrontendIdx;
		char uri[256];
		char make[256];
		char model[256];
		char modelNum[256];
		char uuid[512];
		char uuid_make[256];
		char vid[32];
		char pid[32];
        DL_E_TUNER_CONNECTION_TYPE connection_type;
        int fta_concurrent_streams;
        int enc_concurrent_streams;
	};

    typedef TDevAPIDevInfoEx* PDevAPIDevInfoEx;

	struct TDevAPIDevListEx
	{
        TDevAPIDevListEx() :
            Count(0)
            {}

		int Count;
		TDevAPIDevInfoEx Devices[DL_DEVAPI_MAX_DEVICES];
	};

    typedef TDevAPIDevListEx* PDevAPIDevListEx;

    const std::wstring external_ci_device_id_none = L"13203e71-d6b4-42fc-84a6-7b3c44aec6fa";

	struct TExtCIDevice
	{
		wchar_t name[256];
		wchar_t hwid[256];
	};

	struct TExtCIDeviceList
	{
        TExtCIDeviceList() :
            Count(0)
            {}

		int Count;
		TExtCIDevice Devices[DL_DEVAPI_MAX_DEVICES];
	};

    typedef TExtCIDeviceList* PExtCIDeviceList;

    inline void adjust_qam_for_atsc(unsigned long& type)
    {
        if ((type & TUNERTYPE_ATSC) != 0 && (type & TUNERTYPE_DVBC) != 0)
        {
            //reset dvb-c
            type &= (~TUNERTYPE_DVBC);
            //set clearqam
            type |= TUNERTYPE_CLEARQAM;
        }
    }

#ifdef WIN32
	#pragma pack(pop)
#endif

	// END of ALIGNED STRUCTURES
	/////////////////////////////////////////////////////////////////////////////////
} //namespace dvblink
