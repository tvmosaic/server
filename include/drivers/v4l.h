/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <drivers/deviceapi.h>
#include <drivers/tuner_factory.h>

struct dvb_frontend_info;
class CV4lTunersStreamSrc;
class CV4lTunerCAHandler;

namespace dvblink
{

class v4l_tuner_factory_t : public tuner_factory_t
{
public:
    v4l_tuner_factory_t(const dvblink::filesystem_path_t& device_config_path) :
        tuner_factory_t(device_config_path)
    {}

    static const char* get_name(){return "v4l";}

    virtual std::string get_factory_name(){return get_name();}
    virtual int DeviceGetList(PDevAPIDevListEx pDL);
    virtual tuner_t* get_tuner(const std::string& device_path, int fdx);
};


class v4l_tuner_t : public tuner_t
{
public:
    v4l_tuner_t(const std::string& device_path, int fdx);
    ~v4l_tuner_t();

    virtual int StartDevice(DL_E_TUNER_TYPES tuner_type);
	virtual int StopDevice();
	virtual int AddFilter(int pid);
	virtual int DelFilter(int pid);
	virtual int SetTuner(PTransponderInfo Tp);
	virtual int GetTunerState(PSignalInfo TunerState, PTransponderInfo Tp);
	virtual int SendDiseqc(PDiseqcCmd RawDiseqc, int ToneBurst);
	virtual int LNBPower(DL_E_LNB_POWER_TYPES lnb_power);
	virtual int CISendPMT(unsigned char *buf, int len, int listmng);
protected:
	int g_frontend_fd;
	dvb_frontend_info* g_fe_info;
	CV4lTunersStreamSrc* g_streamSrc;
	unsigned long g_devNum;
	CV4lTunerCAHandler* g_caHandler;
	std::string vendor_id_;
	std::string product_id_;
	dvblink::DL_E_TUNER_TYPES tuner_type_;
	
	static void stream_callback(const unsigned char* buf, unsigned long len, void* user_param);
	bool v4lStartStreaming();
	void v4lStopStreaming();
	bool IsSignalLocked();
	bool get_dvbv5_strength(int& strength);
	bool get_dvbv5_quality(int& quality);
	void GetSignalInfo(int& strength, int& quality);
	bool SetupLNB(dvblink::PTransponderInfo Tp);
};

} //namespace dvblink
