#ifndef __CURL_CURLBUILD_H
#define __CURL_CURLBUILD_H
#ifdef _WIN32
#include "curlbuild.win32.h"
#else
#include "curlbuild.linux.h"
#endif
#endif /* __CURL_CURLBUILD_H */
