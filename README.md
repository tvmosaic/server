# TVMosaic CE (Community Edition) server

## What is TVMosaic CE?

TVMosaic is a cross-platform TV server for live and recorded TV. It is an opensourced version of the commercial TVMosaic product by DVBLogic.

## License
The source code is licensed under the MIT license.

## What are the feature differences between TVMosaic and TVMosaic Community Edition?

TVMosaic Community Edition has everything that commercial TVMosaic product had except
* Licensing engine (TVMosaic CE is free)
* DLNA server
* TVAdvisor
* TVButler support (except Windows)

Also, unlike its commercial variant, TVMosaic CE only features a web-based UI (e.g. no standalone UI executable with the Electron-based video player on Windows/Ubuntu platforms).

## Is there any user documentation available for TVMosaic CE?

The user documentation is available at http://wiki.tv-mosaic.com/.
Please disregard the parts that are specific to the commercial TVMosaic version.

## What existing TVMosaic clients are compatible with the TVMosaic CE?

The following clients are compatible:
* TVMosaic Android app
* TVMosaic iOS and AppleTV apps
* Kodi

In general, all existing TVMosaic clients shall be compatible as DVBLink/TVMosaic API support has not changed in TVMosaic CE.

## Will TVMosaic Android and iOS/AppleTV clients be made available as opensource?
Yes, TVMosaic apps for Android/iOS/AppleTV will be made available as opensource soon.

## Which tuners are compatible with TVMosaic CE?

Tuner support differs per platform.
HDHR and SAT2IP network tuners are supported on all platforms.
In addition to that TVMosaic on Windows supports BDA tuners and TVMosaic on Ubuntu/RaspberryPi supports v4l tuners.

## Is it possible to update an existing TVMosaic installation to TVMosaic CE?
Yes, it is possible. For all platforms, except for Windows and Ubuntu, it can be done without removing an already installed version. Please, make a backup before updating!

For Windows and Ubuntu platforms it is recommended to make a backup, uninstall existing version, instal TVMosaic CE and restore backup afterwards.

*Warning*: if you are using authenticated access to your TVMosaic installation (with user name and passowrd), disable this functionality before updating to TVMosaic CE. You will not be able to access the TVMosaic otherwise as TVMosaic CE employs a different password hashing mechanism.

## How can I build TVMosaic CE from the source code?
Please refer to the instructions inside a build/ directory to build TVMosaic CE.

## Where can I download a pre-built TVMosaic CE installers?
TVMosaic CE installers can be downloaded from the Deployments -> Releases.
(Please note that TVMosaic CE MacOS installer is not being built at the moment).

## Development
As our other commitments do not allow us to spend a lot of time on maintaining TVMosaic CE, **we are looking for maintainers, developers and testers**.
Please, let us know if you are interested!

## Contact
You can always reach us with your questions, suggestions and ideas at opensource@dvblogic.com


