#!/bin/bash

if [ -n "$TVMOSAIC_CROSS" ]; then
    export CC="${TVMOSAIC_CROSS}gcc"
    export AR="${TVMOSAIC_CROSS}ar"
    export RANLIB="${TVMOSAIC_CROSS}ranlib"
    export LD="${TVMOSAIC_CROSS}ld"
fi

export CFLAGS="-O3 -fPIC"
export USE_ARCH=32

make clean

./Configure linux-armv4 shared enable-camellia --prefix=${TVMOSAIC_STAGE_DIR}

make depend
make

if [ -n "$TVMOSAIC_STAGE_DIR" ]; then
    make install
fi
