#!/bin/bash

if [ -n "$TVMOSAIC_CROSS" ]; then
    export CC="${TVMOSAIC_CROSS}gcc"
    export AR="${TVMOSAIC_CROSS}ar"
    export RANLIB="${TVMOSAIC_CROSS}ranlib"
    export LD="${TVMOSAIC_CROSS}ld"
fi

export CFLAGS="-O3 -fPIC -march=armv7-a -mfpu=vfpv3-d16 -mfloat-abi=hard"
export USE_ARCH=32

make clean

./Configure linux-armv4 shared --prefix=${TVMOSAIC_STAGE_DIR}

make depend
make

if [ -n "$TVMOSAIC_STAGE_DIR" ]; then
    make install
fi
