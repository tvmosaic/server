#!/bin/bash

#if cross variable is set - assign it to build vars
if [ -n "$TVMOSAIC_CROSS" ]; then
    export CC="${TVMOSAIC_CROSS}gcc"
    export AR="${TVMOSAIC_CROSS}ar"
    export RANLIB="${TVMOSAIC_CROSS}ranlib"
    export LD="${TVMOSAIC_CROSS}ld"
fi

export CFLAGS="-fPIC -O3"
export PKG_CONFIG_PATH=${TVMOSAIC_STAGE_DIR}/lib/pkgconfig

make clean

./configure \
--prefix=${TVMOSAIC_STAGE_DIR} \
--host=x86_64-apple-darwin \
--enable-rpath=no \
--enable-shared \
--enable-static \
--enable-optimize

make

if [ -n "$TVMOSAIC_STAGE_DIR" ]; then
    make install
fi
