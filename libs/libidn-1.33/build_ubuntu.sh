#!/bin/bash

export PREFIX="/opt/sdk/ubuntu/x86_64"
export CROSS="${PREFIX}/bin/x86_64-linux-gnu-"
export CC="${CROSS}gcc"
export AR="${CROSS}ar"
export RANLIB="${CROSS}ranlib"
export LD="${CROSS}ld"
export CFLAGS="-fPIC -O3"

make clean

./configure \
--prefix=${PREFIX} \
--host=x86_64-linux-gnu \
--enable-gtk-doc-html=no \
--enable-shared \
--enable-static

make
make install
