#!/bin/bash

#if cross variable is set - assign it to build vars
if [ -n "$TVMOSAIC_CROSS" ]; then
    export CC="${TVMOSAIC_CROSS}gcc"
    export AR="${TVMOSAIC_CROSS}ar"
    export RANLIB="${TVMOSAIC_CROSS}ranlib"
    export LD="${TVMOSAIC_CROSS}ld"
fi

export CFLAGS="-fPIC -O3"

make clean

./configure \
--prefix=${TVMOSAIC_STAGE_DIR} \
--host=powerpc-none-linux-gnuspe \
--enable-gtk-doc-html=no \
--enable-shared \
--enable-static

make

if [ -n "$TVMOSAIC_STAGE_DIR" ]; then
    make install
fi
