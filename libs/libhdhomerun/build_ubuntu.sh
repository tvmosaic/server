#!/bin/bash

export PREFIX="/opt/sdk/ubuntu/x86_64"
export CROSS_COMPILE="${PREFIX}/bin/x86_64-linux-gnu-"

make clean
make all
