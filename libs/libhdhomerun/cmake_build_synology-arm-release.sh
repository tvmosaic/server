#!/bin/bash

#if cross variable is set - assign it to build vars
if [ -n "$TVMOSAIC_CROSS" ]; then
    export CROSS_COMPILE="${TVMOSAIC_CROSS}"
fi

export PREFIX=${TVMOSAIC_STAGE_DIR}
export CFLAGS="-fPIC -O3"

make clean

make

if [ -n "$TVMOSAIC_STAGE_DIR" ]; then
DEST_INC=${TVMOSAIC_STAGE_DIR}/include/libhdhomerun
DEST_LIB=${TVMOSAIC_STAGE_DIR}/lib

mkdir -p ${DEST_INC}
mkdir -p ${DEST_LIB}

cp -rf ./*.h ${DEST_INC}/
cp -rf ./libhdhomerun.so ${DEST_LIB}/

fi



