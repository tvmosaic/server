#!/bin/bash

export CFLAGS="-fPIC -O3"
export CXXFLAGS="-fPIC"

rm -rf ./CMakeFiles
rm ./CMakeCache.txt

make clean

cmake -DRAPIDJSON_BUILD_EXAMPLES=OFF -DCMAKE_INSTALL_PREFIX="${TVMOSAIC_STAGE_DIR}" .

make

if [ -n "$TVMOSAIC_STAGE_DIR" ]; then
    make install
fi





