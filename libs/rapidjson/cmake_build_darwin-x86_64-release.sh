#!/bin/bash

export CFLAGS="-fPIC -O3"
export CXXFLAGS="-fPIC"

rm -rf ./CMakeFiles
rm ./CMakeCache.txt

make clean

cmake -DRAPIDJSON_BUILD_EXAMPLES=OFF -DCMAKE_INSTALL_PREFIX="${TVMOSAIC_STAGE_DIR}" -DCMAKE_SYSTEM_NAME="Darwin" -DCMAKE_C_COMPILER="gcc" -DCMAKE_CXX_COMPILER="g++" .

make

if [ -n "$TVMOSAIC_STAGE_DIR" ]; then
    make install
fi





