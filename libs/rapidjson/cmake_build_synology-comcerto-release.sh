#!/bin/bash

export CFLAGS="-fPIC -O3"
export CXXFLAGS="-fPIC"

rm -rf ./CMakeFiles
rm ./CMakeCache.txt

make clean

cmake -DRAPIDJSON_BUILD_EXAMPLES=OFF -DCMAKE_INSTALL_PREFIX="" -DCMAKE_SYSTEM_NAME="Linux" -DCMAKE_C_COMPILER="${TVMOSAIC_CROSS}gcc" -DCMAKE_CXX_COMPILER="${TVMOSAIC_CROSS}g++" .

make

if [ -n "$TVMOSAIC_STAGE_DIR" ]; then
    make DESTDIR="${TVMOSAIC_STAGE_DIR}" install
fi





