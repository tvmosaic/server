#!/bin/bash

export CFLAGS="-fPIC -O3 -march=armv7-a -mfpu=vfpv3-d16 -mfloat-abi=hard"
export CXXFLAGS="-fPIC -O3 -march=armv7-a -mfpu=vfpv3-d16 -mfloat-abi=hard"

rm -rf ./CMakeFiles
rm ./CMakeCache.txt

make clean

cmake -DRAPIDJSON_BUILD_EXAMPLES=OFF -DCMAKE_INSTALL_PREFIX="" -DCMAKE_SYSTEM_NAME="Linux" -DCMAKE_C_COMPILER="${TVMOSAIC_CROSS}gcc" -DCMAKE_CXX_COMPILER="${TVMOSAIC_CROSS}g++" .

make

if [ -n "$TVMOSAIC_STAGE_DIR" ]; then
    make DESTDIR="${TVMOSAIC_STAGE_DIR}" install
fi





