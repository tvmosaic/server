#!/bin/bash

export SYSROOT="/home/user/android/android-ndk-r13/platforms/android-19/arch-arm"
export cross="/home/user/android/android-ndk-r13/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin/arm-linux-androideabi-"
export CC="${cross}gcc --sysroot $SYSROOT"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3 -march=armv7-a"

make clean

./configure \
--prefix=${SYSROOT}/usr \
--host=arm-linux-androideabi \
--disable-debug \
--enable-optimize \
--enable-ares \
--enable-shared \
--enable-static \
--enable-http \
--disable-ftp \
--disable-file \
--disable-ldaps \
--disable-rtsp \
--enable-proxy \
--disable-dict \
--disable-telnet \
--disable-tftp \
--disable-pop3 \
--disable-imap \
--disable-smtp \
--disable-gopher \
--disable-manual \
--disable-ipv6 \
--enable-nonblocking \
--enable-crypto-auth \
--enable-tls-srp \
--enable-cookies

make
