#!/bin/bash

export PREFIX="/opt/sdk/ubuntu/x86_64"
export CROSS="${PREFIX}/bin/x86_64-linux-gnu-"
export CC="${CROSS}gcc"
export AR="${CROSS}ar"
export RANLIB="${CROSS}ranlib"
export LD="${CROSS}ld"
export CFLAGS="-fPIC -O3"

./configure \
--prefix=${PREFIX} \
--disable-debug \
--enable-optimize \
--enable-ares \
--enable-shared \
--enable-static \
--enable-http \
--disable-ftp \
--disable-file \
--disable-ldaps \
--disable-rtsp \
--enable-proxy \
--disable-dict \
--disable-telnet \
--disable-tftp \
--disable-pop3 \
--disable-imap \
--disable-smtp \
--disable-gopher \
--disable-manual \
--disable-ipv6 \
--enable-nonblocking \
--enable-crypto-auth \
--enable-tls-srp \
--enable-cookies

make clean
make
make install
