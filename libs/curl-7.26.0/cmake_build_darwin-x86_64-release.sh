#!/bin/bash

#if cross variable is set - assign it to build vars
if [ -n "$TVMOSAIC_CROSS" ]; then
    export CC="${TVMOSAIC_CROSS}gcc"
    export AR="${TVMOSAIC_CROSS}ar"
    export RANLIB="${TVMOSAIC_CROSS}ranlib"
    export LD="${TVMOSAIC_CROSS}ld"
fi

export CFLAGS="-fPIC -O3"
#export LD_FLAGS="-L${TVMOSAIC_STAGE_DIR}/lib"

export PKG_CONFIG_PATH=${TVMOSAIC_STAGE_DIR}/lib/pkgconfig

make clean

./configure \
--prefix=${TVMOSAIC_STAGE_DIR} \
--with-ssl=${TVMOSAIC_STAGE_DIR} \
--host=x86_64-apple-darwin \
--enable-rpath=no \
--disable-debug \
--enable-optimize \
--enable-ares \
--enable-shared \
--enable-static \
--enable-http \
--disable-ftp \
--disable-file \
--disable-ldaps \
--disable-rtsp \
--enable-proxy \
--disable-dict \
--disable-telnet \
--disable-tftp \
--disable-pop3 \
--disable-imap \
--disable-smtp \
--disable-gopher \
--disable-manual \
--disable-ipv6 \
--enable-nonblocking \
--enable-crypto-auth \
--enable-tls-srp \
--enable-cookies

make depend
make
if [ -n "$TVMOSAIC_STAGE_DIR" ]; then
    make install
fi
