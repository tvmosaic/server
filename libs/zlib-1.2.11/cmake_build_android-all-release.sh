#!/bin/bash

#if cross variable is set - assign it to build vars
if [ -n "$TVMOSAIC_CROSS" ]; then
    export CC="${TVMOSAIC_CROSS}gcc"
    export CXX="${TVMOSAIC_CROSS}g++"
    export AR="${TVMOSAIC_CROSS}ar"
    export RANLIB="${TVMOSAIC_CROSS}ranlib"
    export LD="${TVMOSAIC_CROSS}ld"
fi

export CFLAGS="-fPIC -O3 -fPIE"

make clean

./configure \
--prefix=${TVMOSAIC_STAGE_DIR}

make

if [ -n "$TVMOSAIC_STAGE_DIR" ]; then
    make install
fi
