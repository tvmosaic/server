LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LIB_ROOT := ..
LOCAL_MODULE := z
LOCAL_CFLAGS := -O3 -fPIC

LOCAL_SRC_FILES := \
  $(LIB_ROOT)/adler32.c \
  $(LIB_ROOT)/compress.c \
  $(LIB_ROOT)/crc32.c \
  $(LIB_ROOT)/deflate.c \
  $(LIB_ROOT)/gzclose.c \
  $(LIB_ROOT)/gzlib.c \
  $(LIB_ROOT)/gzread.c \
  $(LIB_ROOT)/gzwrite.c \
  $(LIB_ROOT)/inflate.c \
  $(LIB_ROOT)/infback.c \
  $(LIB_ROOT)/inftrees.c \
  $(LIB_ROOT)/inffast.c \
  $(LIB_ROOT)/trees.c \
  $(LIB_ROOT)/uncompr.c \
  $(LIB_ROOT)/zutil.c

LOCAL_C_INCLUDES += \
  $(LIB_ROOT)/
 
include $(BUILD_STATIC_LIBRARY)
