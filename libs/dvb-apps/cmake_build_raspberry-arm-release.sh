#!/bin/bash

#if cross variable is set - assign it to build vars
if [ -n "$TVMOSAIC_CROSS" ]; then
    export CROSS="${TVMOSAIC_CROSS}"
    export CC="${TVMOSAIC_CROSS}gcc"
    export AR="${TVMOSAIC_CROSS}ar"
    export RANLIB="${TVMOSAIC_CROSS}ranlib"
    export LD="${TVMOSAIC_CROSS}ld"
fi

export CFLAGS="-fPIC -O3 -march=armv7-a -mfpu=vfpv3-d16 -mfloat-abi=hard -I${TVMOSAIC_STAGE_DIR}/../linux/include/dvbapi"

make clean

make static=1

if [ -n "$TVMOSAIC_STAGE_DIR" ]; then
    make install prefix="${TVMOSAIC_STAGE_DIR}"
fi

