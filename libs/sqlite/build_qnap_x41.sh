#!/bin/bash

export cross="/opt/cross-project/arm/linaro/bin/arm-linux-gnueabihf-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3"

./configure \
--host=arm-linux-gnueabi \
--enable-threadsafe=no --enable-dynamic-extensions=no --enable-static

make clean
make
#make install
