#!/bin/bash

export cross="/opt/sdk/tvmosaic/netgear/x86_64/bin/x86_64-linux-gnu-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3"

./configure \
--host=x86_64-linux-gnu \
--enable-threadsafe=no --enable-dynamic-extensions=no --enable-static

make clean
make
#make install
