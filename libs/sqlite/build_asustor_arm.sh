#!/bin/bash

export cross="/opt/sdk/tvmosaic/asustor/arm/bin/arm-marvell-linux-gnueabi-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3"

./configure \
--host=arm-linux-gnueabi \
--enable-threadsafe=no --enable-dynamic-extensions=no --enable-static

make clean
make
#make install
