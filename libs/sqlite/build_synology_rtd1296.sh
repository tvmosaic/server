#!/bin/bash

export cross="/opt/sdk/tvmosaic/synology/rtd1296/aarch64-unknown-linux-gnueabi/bin/aarch64-unknown-linux-gnueabi-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3"

./configure \
--host=aarch64-linux-gnu \
--enable-threadsafe=no --enable-dynamic-extensions=no --enable-static

make clean
make
#make install
