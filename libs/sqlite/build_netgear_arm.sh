#!/bin/bash

export cross="/opt/sdk/tvmosaic/netgear/arm/bin/arm-none-linux-gnueabi-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3"

./configure \
--host=arm-linux-gnueabi \
--enable-threadsafe=no --enable-dynamic-extensions=no --enable-static

make clean
make
#make install
