LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LIB_ROOT := ../src

LOCAL_SRC_FILES := \
	$(LIB_ROOT)/pugixml.cpp

LOCAL_CPPFLAGS += -fexceptions
LOCAL_CPPFLAGS += -frtti
LOCAL_CFLAGS += -fPIC

LOCAL_C_INCLUDES += \
	$(LIB_ROOT)/

LOCAL_MODULE := libpugixml
include $(BUILD_STATIC_LIBRARY)
