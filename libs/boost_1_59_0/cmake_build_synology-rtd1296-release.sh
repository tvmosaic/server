#!/bin/bash

cwd=$(pwd)

if [ -z "$TVMOSAIC_STAGE_DIR" ]; then

#define local dir as stage
TVMOSAIC_STAGE_DIR=${cwd}/"stage"

#re-create stage dir
rm -rf ${TVMOSAIC_STAGE_DIR}

fi

rm -rf ./bin.v2

mkdir -p ${TVMOSAIC_STAGE_DIR}

export BOOST_BUILD_PATH="$TVMOSAIC_STAGE_DIR"

#write user-config.jam
USER_CONFIG_PATH="${BOOST_BUILD_PATH}/user-config.jam"

echo -e "using gcc : : ${TVMOSAIC_CROSS}g++ -fPIC ;\n" > "$USER_CONFIG_PATH"

./b2 \
--toolset=gcc \
-s BZIP2_SOURCE=${cwd}/../bzip2-1.0.6 \
-s ZLIB_SOURCE=${cwd}/../zlib-1.2.11 \
variant=release \
threading=multi \
link=static \
--without-context \
--without-coroutine \
--without-coroutine2 \
runtime-link=shared \
--without-python \
--prefix="$TVMOSAIC_STAGE_DIR" \
install


