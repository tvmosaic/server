LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LIB_ROOT := ../json_spirit

LOCAL_SRC_FILES := \
	$(LIB_ROOT)/json_spirit_reader.cpp \
	$(LIB_ROOT)/json_spirit_value.cpp \
	$(LIB_ROOT)/json_spirit_writer.cpp

LOCAL_CPPFLAGS += -fexceptions
LOCAL_CPPFLAGS += -frtti
LOCAL_CFLAGS += -fPIC

LOCAL_C_INCLUDES += \
	$(LIB_ROOT)/ \
	$(LIB_ROOT)/../../boost_1_59_0/

LOCAL_MODULE := libjson_spirit
include $(BUILD_STATIC_LIBRARY)
