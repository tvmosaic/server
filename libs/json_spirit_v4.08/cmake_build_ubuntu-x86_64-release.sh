#!/bin/bash

export CFLAGS="-fPIC -O3"
export CXXFLAGS="-fPIC"

export BOOST_ROOT="${TVMOSAIC_STAGE_DIR}"
#export DESTDIR="${TVMOSAIC_STAGE_DIR}"

rm -rf ./CMakeFiles
rm ./CMakeCache.txt

make clean

cmake -DCMAKE_INSTALL_PREFIX="${TVMOSAIC_STAGE_DIR}" .

make

if [ -n "$TVMOSAIC_STAGE_DIR" ]; then
    make install
fi





