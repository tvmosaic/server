#!/bin/bash

mkdir build_ubuntu_x86_64
cd build_ubuntu_x86_64

export BOOST_INCLUDEDIR="/home/user/projects/dvblex/libs/boost_1_59_0"
export PREFIX="/opt/sdk/ubuntu/x86_64"
export CROSS="${PREFIX}/bin/x86_64-linux-gnu-"
export CC="${CROSS}gcc"
export CXX="${CROSS}g++"
export AR="${CROSS}ar"
export RANLIB="${CROSS}ranlib"
export LD="${CROSS}g++"
export CFLAGS="-fPIC -O3"
export CXXFLAGS="-fPIC"

make clean
cmake ..
make
