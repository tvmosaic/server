#!/bin/bash

mkdir -p /opt/cross-project
ln -sf /opt/sdk/tvmosaic/qnap/armx41 /opt/cross-project/arm

CROSS_COMPILER="/opt/sdk/tvmosaic/qnap/armx41/linaro/bin/arm-linux-gnueabihf-"

CONFIG_TYPE=qnap-armx41-release

mkdir ${CONFIG_TYPE}
cd ${CONFIG_TYPE}

cmake .. -DTVM_BUILD_TYPE="Release" -DTVM_CONFIG_TYPE="${CONFIG_TYPE}" \
 -DTVM_CROSS="${CROSS_COMPILER}"

CORES=`getconf _NPROCESSORS_ONLN`

make -j $CORES

cd ..
