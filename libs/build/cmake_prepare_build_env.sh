#!/bin/bash

OPTIND=1

cross=""
prefix=""
TVMOSAIC_SCRIPT=""

while getopts "c:p:s:" opt; do
    case "$opt" in
	c) cross=$OPTARG
	   ;;
	p) prefix=$OPTARG
	   ;;
	s) TVMOSAIC_SCRIPT=$OPTARG
	   ;;
    esac
done

#break with error if script is not set
if [ -z "$TVMOSAIC_SCRIPT" ]; then
    echo "Script is not set. Exiting with error\n"
    exit 1
fi

export TVMOSAIC_STAGE_DIR="$prefix"
export TVMOSAIC_CROSS="$cross"

. "$TVMOSAIC_SCRIPT"

