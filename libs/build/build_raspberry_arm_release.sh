#!/bin/bash

CROSS_COMPILER="/opt/sdk/tvmosaic/raspberry/gcc-linaro-4.9.4-2017.01-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-"

CONFIG_TYPE=raspberry-arm-release

mkdir ${CONFIG_TYPE}
cd ${CONFIG_TYPE}

cmake .. -DTVM_BUILD_TYPE="Release" -DTVM_CONFIG_TYPE="${CONFIG_TYPE}" \
 -DTVM_CROSS="${CROSS_COMPILER}"

CORES=`getconf _NPROCESSORS_ONLN`

make -j $CORES

cd ..
