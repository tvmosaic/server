cmake_minimum_required(VERSION 3.0.0)

project(tvmosaic_libs)

#TVM_BUILD_TYPE
if(DEFINED TVM_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE ${TVM_BUILD_TYPE})
    message(status "CMAKE_BUILD_TYPE is set to " ${CMAKE_BUILD_TYPE})
else()
    message(error "TVM_BUILD_TYPE has to be set")
    return()
endif()

#TVM_CONFIG_TYPE
if(DEFINED TVM_CONFIG_TYPE)
    set(TVMOSAIC_BUILD_CONFIG ${TVM_CONFIG_TYPE})
    message(status "TVMOSAIC_BUILD_CONFIG is set to " ${TVMOSAIC_BUILD_CONFIG})
else()
    message(error "TVM_CONFIG_TYPE is not set")
    return()
endif()

#TVM_CROSS
if(DEFINED TVM_CROSS)
    message(status "TVM_CROSS is set to " ${TVM_CROSS})
endif()

set(SO_EXTENSION so)
if (DEFINED TVM_BUILD_DARWIN)
    set(SO_EXTENSION dylib)
endif()

#set variable for strip
set(TVM_STRIP ${TVM_CROSS}strip)

#locations

set(TVMOSAIC_ANKER_DIR ${CMAKE_SOURCE_DIR})

set(TVMOSAIC_STAGE_DIR ${TVMOSAIC_ANKER_DIR}/../../stage/${TVMOSAIC_BUILD_CONFIG})
set(TVMOSAIC_INC_STAGE ${TVMOSAIC_STAGE_DIR}/include)
set(TVMOSAIC_LIB_STAGE ${TVMOSAIC_STAGE_DIR}/lib)
set(TVMOSAIC_INC_STAGE_LINUX ${TVMOSAIC_STAGE_DIR}/../linux/include)
set(TVMOSAIC_STAGE_DARWIN ${TVMOSAIC_STAGE_DIR}/../macos)

set(LIBRARY_OUTPUT_PATH ${TVMOSAIC_LIB_STAGE}/tvmosaic)
#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${TVMOSAIC_STAGE_DIR}/bin/tvmosaic)

#libbz2

set(TVMOSAIC_LIBBZ2_BINARY ${TVMOSAIC_LIB_STAGE}/libbz2.a)
add_custom_command(
    OUTPUT ${TVMOSAIC_LIBBZ2_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/bzip2-1.0.6
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}"
)

add_custom_target(libbz2 ALL DEPENDS ${TVMOSAIC_LIBBZ2_BINARY})

#openssl

set(TVMOSAIC_LIBSSL_BINARY ${TVMOSAIC_LIB_STAGE}/libssl.${SO_EXTENSION})

if (DEFINED TVM_USE_OPENSSL_098)

add_custom_command(
    OUTPUT ${TVMOSAIC_LIBSSL_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/openssl-0.9.8zh
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}" 
)

else()

add_custom_command(
    OUTPUT ${TVMOSAIC_LIBSSL_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/openssl-1.0.2l
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}" 
)

endif()

add_custom_target(libopenssl ALL DEPENDS ${TVMOSAIC_LIBSSL_BINARY})

#libboost

set(TVMOSAIC_LIBBOOST_BINARY ${TVMOSAIC_LIB_STAGE}/libboost_thread.a)
add_custom_command(
    OUTPUT ${TVMOSAIC_LIBBOOST_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/boost_1_59_0
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}"
)

add_custom_target(libboost ALL DEPENDS ${TVMOSAIC_LIBBOOST_BINARY})

#libpion

set(TVMOSAIC_LIBPION_BINARY ${TVMOSAIC_LIB_STAGE}/libpion.a)
add_custom_command(
    OUTPUT ${TVMOSAIC_LIBPION_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/pion-5.0.7
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}"
    DEPENDS libboost libopenssl
)

add_custom_target(libpion ALL DEPENDS ${TVMOSAIC_LIBPION_BINARY})

#libz

set(TVMOSAIC_LIBZ_BINARY ${TVMOSAIC_LIB_STAGE}/libz.a)
add_custom_command(
    OUTPUT ${TVMOSAIC_LIBZ_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/zlib-1.2.11
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}"
)

add_custom_target(libz ALL DEPENDS ${TVMOSAIC_LIBZ_BINARY})

#libcares

set(TVMOSAIC_LIBCARES_BINARY ${TVMOSAIC_LIB_STAGE}/libcares.${SO_EXTENSION})
add_custom_command(
    OUTPUT ${TVMOSAIC_LIBCARES_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/c-ares-1.12.0
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}"
)

add_custom_target(libcares ALL DEPENDS ${TVMOSAIC_LIBCARES_BINARY})

#libidn

set(TVMOSAIC_LIBIDN_BINARY ${TVMOSAIC_LIB_STAGE}/libidn.${SO_EXTENSION})
add_custom_command(
    OUTPUT ${TVMOSAIC_LIBIDN_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/libidn-1.33
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}"
)

add_custom_target(libidn ALL DEPENDS ${TVMOSAIC_LIBIDN_BINARY})

#libssh2

set(TVMOSAIC_LIBSSH2_BINARY ${TVMOSAIC_LIB_STAGE}/libssh2.${SO_EXTENSION})
add_custom_command(
    OUTPUT ${TVMOSAIC_LIBSSH2_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/libssh2-1.8.0
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}"
    DEPENDS libopenssl
)

add_custom_target(libssh2 ALL DEPENDS ${TVMOSAIC_LIBSSH2_BINARY})

#libcurl

set(TVMOSAIC_LIBCURL_BINARY ${TVMOSAIC_LIB_STAGE}/libcurl.${SO_EXTENSION})
add_custom_command(
    OUTPUT ${TVMOSAIC_LIBCURL_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/curl-7.26.0
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}"
    DEPENDS libopenssl libcares libidn libssh2
)

add_custom_target(libcurl ALL DEPENDS ${TVMOSAIC_LIBCURL_BINARY})

#libhdhomerun

set(TVMOSAIC_LIBHDHR_BINARY ${TVMOSAIC_LIB_STAGE}/libhdhomerun.${SO_EXTENSION})
add_custom_command(
    OUTPUT ${TVMOSAIC_LIBHDHR_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/libhdhomerun
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}"
)

add_custom_target(libhdhomerun ALL DEPENDS ${TVMOSAIC_LIBHDHR_BINARY})

#libiconv

set(TVMOSAIC_LIBICONV_BINARY ${TVMOSAIC_LIB_STAGE}/libiconv.${SO_EXTENSION})
add_custom_command(
    OUTPUT ${TVMOSAIC_LIBICONV_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/libiconv-1.15
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}"
)

add_custom_target(libiconv ALL DEPENDS ${TVMOSAIC_LIBICONV_BINARY})

if (NOT DEFINED TVM_BUILD_DARWIN)

    #dvb-apps

    set(TVMOSAIC_LIBDVBAPPS_BINARY ${TVMOSAIC_LIB_STAGE}/libdvben50221.${SO_EXTENSION})
    add_custom_command(
        OUTPUT ${TVMOSAIC_LIBDVBAPPS_BINARY}
        WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/dvb-apps
        COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}"
    )

    add_custom_target(libdvben50221 ALL DEPENDS ${TVMOSAIC_LIBDVBAPPS_BINARY})

endif()

#libjsonspirit

set(TVMOSAIC_LIBJSPIRIT_BINARY ${TVMOSAIC_LIB_STAGE}/libjson_spirit.a)
add_custom_command(
    OUTPUT ${TVMOSAIC_LIBJSPIRIT_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/json_spirit_v4.08
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}"
    DEPENDS libboost
)

add_custom_target(libjson_spirit ALL DEPENDS ${TVMOSAIC_LIBJSPIRIT_BINARY})

#libpugi

set(TVMOSAIC_LIBPUGI_BINARY ${TVMOSAIC_LIB_STAGE}/libpugixml.a)
add_custom_command(
    OUTPUT ${TVMOSAIC_LIBPUGI_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/pugixml-1.8
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}"
)

add_custom_target(libpugi ALL DEPENDS ${TVMOSAIC_LIBPUGI_BINARY})

#rapidjson

set(TVMOSAIC_RAPIDJSON_BINARY ${TVMOSAIC_INC_STAGE}/rapidjson/rapidjson.h)
add_custom_command(
    OUTPUT ${TVMOSAIC_RAPIDJSON_BINARY}
    WORKING_DIRECTORY ${TVMOSAIC_ANKER_DIR}/../../libs/rapidjson
    COMMAND ${TVMOSAIC_ANKER_DIR}/cmake_prepare_build_env.sh -p ${TVMOSAIC_STAGE_DIR} -s cmake_build_${TVMOSAIC_BUILD_CONFIG}.sh -c "${TVM_CROSS}"
)

add_custom_target(rapidjson ALL DEPENDS ${TVMOSAIC_RAPIDJSON_BINARY})

