#!/bin/bash

for sh in $( find . -maxdepth 1 -name 'build_*.sh' ); do
    echo ""
    echo "    $(tput bold)Starting $(tput setaf 2) $sh$(tput sgr0)..."
	./$sh
done
