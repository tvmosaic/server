#!/bin/bash

CROSS_COMPILER="/opt/sdk/tvmosaic/qnap/arm64/bin/aarch64-QNAP-linux-gnu-"

CONFIG_TYPE=qnap-arm64-release

mkdir ${CONFIG_TYPE}
cd ${CONFIG_TYPE}

cmake .. -DTVM_BUILD_TYPE="Release" -DTVM_CONFIG_TYPE="${CONFIG_TYPE}" \
 -DTVM_CROSS="${CROSS_COMPILER}"

CORES=`getconf _NPROCESSORS_ONLN`

make -j $CORES

cd ..
