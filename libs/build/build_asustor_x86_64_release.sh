#!/bin/bash

CROSS_COMPILER="/opt/sdk/tvmosaic/asustor/x86_64/bin/x86_64-asustor-linux-gnu-"

CONFIG_TYPE=asustor-x86_64-release

mkdir ${CONFIG_TYPE}
cd ${CONFIG_TYPE}

cmake .. -DTVM_BUILD_TYPE="Release" -DTVM_CONFIG_TYPE="${CONFIG_TYPE}" \
 -DTVM_CROSS="${CROSS_COMPILER}"

CORES=`getconf _NPROCESSORS_ONLN`

make -j $CORES

cd ..
