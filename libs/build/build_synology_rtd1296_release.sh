#!/bin/bash

CROSS_COMPILER="/opt/sdk/tvmosaic/synology/rtd1296/aarch64-unknown-linux-gnueabi/bin/aarch64-unknown-linux-gnueabi-"

CONFIG_TYPE=synology-rtd1296-release

mkdir ${CONFIG_TYPE}
cd ${CONFIG_TYPE}

cmake .. -DTVM_BUILD_TYPE="Release" -DTVM_CONFIG_TYPE="${CONFIG_TYPE}" \
 -DTVM_CROSS="${CROSS_COMPILER}"

CORES=`getconf _NPROCESSORS_ONLN`

make -j $CORES

cd ..
