#!/bin/bash

CROSS_COMPILER="/opt/sdk/tvmosaic/qnap/arm/bin/arm-none-linux-gnueabi-"

CONFIG_TYPE=qnap-arm-release

mkdir ${CONFIG_TYPE}
cd ${CONFIG_TYPE}

cmake .. -DTVM_BUILD_TYPE="Release" -DTVM_CONFIG_TYPE="${CONFIG_TYPE}" \
 -DTVM_USE_OPENSSL_098="1" -DTVM_CROSS="${CROSS_COMPILER}"

CORES=`getconf _NPROCESSORS_ONLN`

make -j $CORES

cd ..
