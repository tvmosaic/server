#!/bin/bash

CONFIG_TYPE=ubuntu-x86_64-release

mkdir ${CONFIG_TYPE}
cd ${CONFIG_TYPE}

cmake .. -DTVM_BUILD_TYPE="Release" -DTVM_CONFIG_TYPE="${CONFIG_TYPE}" \
 -DTVM_USE_DBUS="1"

CORES=`getconf _NPROCESSORS_ONLN`

make -j $CORES

cd ..
