#!/bin/bash

CROSS_COMPILER="/opt/sdk/tvmosaic/asustor/arm/bin/arm-marvell-linux-gnueabi-"

CONFIG_TYPE=asustor-arm-release

mkdir ${CONFIG_TYPE}
cd ${CONFIG_TYPE}

cmake .. -DTVM_BUILD_TYPE="Release" -DTVM_CONFIG_TYPE="${CONFIG_TYPE}" \
 -DTVM_CROSS="${CROSS_COMPILER}"

CORES=`getconf _NPROCESSORS_ONLN`

make -j $CORES

cd ..
