#!/bin/bash

mkdir -p /opt/sdk/netgear
ln -sf /opt/sdk/tvmosaic/netgear/x86_64 /opt/sdk/netgear/x86_64

CROSS_COMPILER="/opt/sdk/tvmosaic/netgear/x86_64/bin/x86_64-linux-gnu-"

CONFIG_TYPE=netgear-x86_64-release

mkdir ${CONFIG_TYPE}
cd ${CONFIG_TYPE}

cmake .. -DTVM_BUILD_TYPE="Release" -DTVM_CONFIG_TYPE="${CONFIG_TYPE}" \
 -DTVM_CROSS="${CROSS_COMPILER}"

CORES=`getconf _NPROCESSORS_ONLN`

make -j $CORES

cd ..
