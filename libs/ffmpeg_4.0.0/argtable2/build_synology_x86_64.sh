#!/bin/bash

export CFLAGS="-fPIC -O3"
export CXXFLAGS="-fPIC"

CROSS_COMPILER="/opt/sdk/tvmosaic/synology/x86_64/x86_64-pc-linux-gnu/bin/x86_64-pc-linux-gnu-"

rm -rf ./CMakeFiles
rm ./CMakeCache.txt

make clean

cmake -DCMAKE_INSTALL_PREFIX="/home/dev/work/ffmpeg/4.0.0" -DCMAKE_SYSTEM_NAME="Linux" -DCMAKE_C_COMPILER="${CROSS_COMPILER}gcc" -DCMAKE_CXX_COMPILER="${CROSS_COMPILER}g++" .

#make





