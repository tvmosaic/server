#!/bin/bash

export CFLAGS="-fPIC -O3"
export CXXFLAGS="-fPIC"

CROSS_COMPILER="/opt/sdk/tvmosaic/qnap/x86_64/cross-tools/bin/x86_64-QNAP-linux-gnu-"

rm -rf ./CMakeFiles
rm ./CMakeCache.txt

make clean

cmake -DCMAKE_INSTALL_PREFIX="/home/dev/work/ffmpeg/4.0.0" -DCMAKE_SYSTEM_NAME="Linux" -DCMAKE_C_COMPILER="${CROSS_COMPILER}gcc" -DCMAKE_CXX_COMPILER="${CROSS_COMPILER}g++" .

#make





