#!/bin/bash

export cross="/opt/cross-project/arm/linaro/bin/arm-linux-gnueabihf-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3"

make clean

./configure \
--host=arm-linux-gnueabi \
--cross-prefix=/opt/cross-project/arm/linaro/bin/arm-linux-gnueabihf- \
--enable-static \
--prefix=/home/dev/work/ffmpeg/4.0.0 \
--enable-pic

#make

#make install

