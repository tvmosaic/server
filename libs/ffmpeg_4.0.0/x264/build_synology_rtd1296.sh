#!/bin/bash

export cross="/opt/sdk/tvmosaic/synology/rtd1296/aarch64-unknown-linux-gnueabi/bin/aarch64-unknown-linux-gnueabi-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3"

make clean

./configure \
--host=aarch64-linux-gnu \
--cross-prefix=/opt/sdk/tvmosaic/synology/rtd1296/aarch64-unknown-linux-gnueabi/bin/aarch64-unknown-linux-gnueabi- \
--enable-static \
--prefix=/home/dev/work/ffmpeg/4.0.0 \
--enable-pic

#make

#make install

