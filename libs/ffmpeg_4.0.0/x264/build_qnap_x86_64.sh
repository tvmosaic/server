#!/bin/bash

export cross="/opt/sdk/tvmosaic/qnap/x86_64/cross-tools/bin/x86_64-QNAP-linux-gnu-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3"

make clean

./configure \
--host=x86_64-linux-gnu \
--cross-prefix=/opt/sdk/tvmosaic/qnap/x86_64/cross-tools/bin/x86_64-QNAP-linux-gnu- \
--enable-static \
--prefix=/home/dev/work/ffmpeg/4.0.0 \
--enable-pic

#make

#make install

