#!/bin/bash

export cross="/opt/sdk/tvmosaic/synology/armadaxp/arm-marvell-linux-gnueabi/bin/arm-marvell-linux-gnueabi-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3"

make clean

./configure \
--host=arm-linux-gnueabi \
--cross-prefix=/opt/sdk/tvmosaic/synology/armadaxp/arm-marvell-linux-gnueabi/bin/arm-marvell-linux-gnueabi- \
--enable-static \
--prefix=/home/dev/work/ffmpeg/4.0.0 \
--enable-pic

#make

#make install

