#!/bin/bash

export CFLAGS="-fPIC -O3"

make clean

./configure \
--host=x86_64-linux-gnu \
--enable-static \
--prefix=/home/dev/work/ffmpeg/4.0.0 \
--enable-pic

#make

#make install

