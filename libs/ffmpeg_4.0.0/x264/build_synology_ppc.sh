#!/bin/bash

export cross="/opt/sdk/tvmosaic/synology/ppc/bin/powerpc-e500v2-linux-gnuspe-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3"

make clean

./configure \
--host=powerpc-none-linux-gnuspe \
--cross-prefix=/opt/sdk/tvmosaic/synology/ppc/bin/powerpc-e500v2-linux-gnuspe- \
--enable-static \
--prefix=/home/dev/work/ffmpeg/4.0.0 \
--enable-pic

#make

#make install

