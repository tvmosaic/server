#!/bin/bash

export cross="/opt/sdk/tvmosaic/asustor/x86_64/bin/x86_64-asustor-linux-gnu-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3"

export PKG_CONFIG_PATH="/home/dev/work/ffmpeg/4.0.0/lib/pkgconfig"

make clean

./autogen.sh \
--host=x86_64-linux-gnu \
--enable-static \
--prefix=/home/dev/work/ffmpeg/4.0.0 \
--disable-nouveau \
--disable-vmwgfx \
--disable-freedreno \
--enable-amdgpu \
--enable-intel \
--enable-radeon

#make

#make install

