#!/bin/bash

export CFLAGS="-fPIC -O3"

export PKG_CONFIG_PATH="/home/dev/work/ffmpeg/4.0.0/lib/pkgconfig"

make clean

./autogen.sh \
--host=x86_64-linux-gnu \
--enable-static \
--prefix=/home/dev/work/ffmpeg/4.0.0 \
--disable-x11 \
--disable-wayland

#make

#make install

