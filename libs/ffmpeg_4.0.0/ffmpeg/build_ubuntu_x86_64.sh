#!/bin/bash

export CFLAGS="-fPIC -O3"

export PKG_CONFIG_PATH="/home/dev/work/ffmpeg/4.0.0/lib/pkgconfig"

make clean

./configure \
--prefix=/home/dev/work/ffmpeg/4.0.0 \
--pkg-config=pkg-config \
--arch=x86_64 \
--target-os=linux \
--enable-static \
--disable-shared \
--enable-version3 \
--enable-vaapi \
--enable-libdrm \
--enable-gpl \
--enable-libx264 \
--extra-cflags="-I/home/dev/work/ffmpeg/4.0.0/include" \
--extra-ldflags="-L/home/dev/work/ffmpeg/4.0.0/lib" \
--extra-libs="-ldl -lva -ldrm" \
--enable-pthreads \
--disable-iconv

#make

