#!/bin/bash

export cross="/opt/sdk/tvmosaic/synology/x86_64/x86_64-pc-linux-gnu/bin/x86_64-pc-linux-gnu-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3"

export PKG_CONFIG_PATH="/home/dev/work/ffmpeg/4.0.0/lib/pkgconfig"

make clean

./configure \
--enable-cross-compile \
--prefix=/home/dev/work/ffmpeg/4.0.0 \
--cross-prefix=/opt/sdk/tvmosaic/synology/x86_64/x86_64-pc-linux-gnu/bin/x86_64-pc-linux-gnu- \
--pkg-config=pkg-config \
--arch=x86_64 \
--target-os=linux \
--enable-static \
--disable-shared \
--enable-version3 \
--enable-vaapi \
--enable-libdrm \
--enable-gpl \
--enable-libx264 \
--extra-cflags="-I/home/dev/work/ffmpeg/4.0.0/include" \
--extra-ldflags="-L/home/dev/work/ffmpeg/4.0.0/lib" \
--extra-libs="-ldl -lva -ldrm" \
--enable-pthreads \
--disable-iconv

#make

