#!/bin/bash

export cross="/opt/sdk/tvmosaic/synology/ppc/bin/powerpc-e500v2-linux-gnuspe-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3"

make clean

./configure \
--enable-cross-compile \
--prefix=/home/dev/work/ffmpeg/4.0.0 \
--cross-prefix=/opt/sdk/tvmosaic/synology/ppc/bin/powerpc-e500v2-linux-gnuspe- \
--arch=powerpc \
--target-os=linux \
--enable-static \
--disable-shared \
--enable-version3 \
--enable-gpl \
--enable-libx264 \
--extra-libs=-ldl \
--extra-cflags="-I/home/dev/work/ffmpeg/4.0.0/include" \
--extra-ldflags="-L/home/dev/work/ffmpeg/4.0.0/lib" \
--enable-pthreads \
--disable-iconv

#make

