#!/bin/bash

export cross="/opt/sdk/tvmosaic/synology/x86_64/x86_64-pc-linux-gnu/bin/x86_64-pc-linux-gnu-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3"

export PKG_CONFIG_PATH="/home/dev/work/ffmpeg/4.0.0/lib/pkgconfig"

make clean

./autogen.sh \
--host=x86_64-linux-gnu \
--enable-static \
--prefix=/home/dev/work/ffmpeg/4.0.0 \
--enable-drm \
--disable-glx \
--disable-x11 \
--disable-wayland

#make

#make install

