#!/bin/bash

export cross="/opt/sdk/tvmosaic/asustor/x86_64/bin/x86_64-asustor-linux-gnu-"
export CC="${cross}gcc"
export AR="${cross}ar"
export RANLIB="${cross}ranlib"
export LD="${cross}ld"
export CFLAGS="-fPIC -O3 -DHAVE_PTHREADS"
export CPPFLAGS="-fPIC -O3 -DHAVE_PTHREADS"

export argtable2_CFLAGS="-I/home/dev/work/ffmpeg/4.0.0/include"
export argtable2_LIBS="-L/home/dev/work/ffmpeg/4.0.0/lib -largtable2"

export PKG_CONFIG_PATH="/home/dev/work/ffmpeg/4.0.0/lib/pkgconfig"

make clean

./configure \
--host=x86_64-linux-gnu \
--enable-static \
--disable-gui \
--prefix=/home/dev/work/ffmpeg/4.0.0


#make

#make install

