#!/bin/bash

export PREFIX="/opt/sdk/ubuntu/x86_64"
export CROSS="${PREFIX}/bin/x86_64-linux-gnu-"
export CC="${CROSS}gcc"
export AR="${CROSS}ar"
export RANLIB="${CROSS}ranlib"
export LD="${CROSS}ld"
export CFLAGS="-fPIC -O3  -D_FILE_OFFSET_BITS=64"

make clean
make
#make install
