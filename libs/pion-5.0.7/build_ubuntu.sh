#!/bin/bash

export PREFIX="/opt/sdk/ubuntu/x86_64"
export CROSS="${PREFIX}/bin/x86_64-linux-gnu-"
export CC="${CROSS}gcc -fPIC"
export CXX="${CROSS}g++ -fPIC -std=c++0x"
export AR="${CROSS}ar"
export RANLIB="${CROSS}ranlib"
export LD="${CROSS}ld"
export CFLAGS="-fPIC -O3"
export CXXFLAGS="-fPIC -O3 -std=c++0x"

export BOOST_ROOT="/home/user/projects/dvblex/libs/boost_1_59_0"
rm -R $BOOST_ROOT/stage/lib
mkdir $BOOST_ROOT/stage/lib
cp /home/user/projects/dvblex/lib/ubuntu/x86_64/release/boost/* $BOOST_ROOT/stage/lib


./configure \
--prefix=${PREFIX} \
--host=x86_64-linux-gnu \
--disable-tests \
--disable-doxygen-doc \
--disable-doxygen-html \
--enable-static=yes \
--enable-shared=no \
--disable-logging \
--without-openssl \
--without-bzlib

#make clean
make
#make install
