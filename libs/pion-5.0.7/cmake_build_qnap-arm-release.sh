#!/bin/bash

#if cross variable is set - assign it to build vars
if [ -n "$TVMOSAIC_CROSS" ]; then
    export CC="${TVMOSAIC_CROSS}gcc"
    export CXX="${TVMOSAIC_CROSS}g++"
    export AR="${TVMOSAIC_CROSS}ar"
    export RANLIB="${TVMOSAIC_CROSS}ranlib"
    export LD="${TVMOSAIC_CROSS}ld"
fi

export CFLAGS="-fPIC -O3"
export CXXFLAGS="-fPIC -O3"

. ./autogen.sh

make clean
rm ./Makefile

./configure \
--prefix=${TVMOSAIC_STAGE_DIR} \
--host=arm-linux-gnueabi \
--disable-tests \
--disable-doxygen-doc \
--disable-doxygen-html \
--enable-static=yes \
--enable-shared=no \
--disable-logging \
--with-boost=${TVMOSAIC_STAGE_DIR} \
--with-openssl=${TVMOSAIC_STAGE_DIR} \
--without-bzlib

make

if [ -n "$TVMOSAIC_STAGE_DIR" ]; then
    make install
fi

#build will fail on services, just copy the .a and .hpp files manually
cp -rf ./src/.libs/libpion.a ${TVMOSAIC_STAGE_DIR}/lib/

mkdir -p ${TVMOSAIC_STAGE_DIR}/include/pion/http
mkdir -p ${TVMOSAIC_STAGE_DIR}/include/pion/spdy
mkdir -p ${TVMOSAIC_STAGE_DIR}/include/pion/tcp
mkdir -p ${TVMOSAIC_STAGE_DIR}/include/pion/test

cp -rf ./include/pion/http/*.hpp ${TVMOSAIC_STAGE_DIR}/include/pion/http/
cp -rf ./include/pion/spdy/*.hpp ${TVMOSAIC_STAGE_DIR}/include/pion/spdy/
cp -rf ./include/pion/tcp/*.hpp ${TVMOSAIC_STAGE_DIR}/include/pion/tcp/
cp -rf ./include/pion/test/*.hpp ${TVMOSAIC_STAGE_DIR}/include/pion/test/
cp -rf ./include/pion/*.hpp ${TVMOSAIC_STAGE_DIR}/include/pion/
