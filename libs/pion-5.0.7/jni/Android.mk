LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LIB_ROOT := ..
LOCAL_MODULE := pion
LOCAL_CFLAGS := -fPIC

LOCAL_CPPFLAGS += -fexceptions
LOCAL_CPPFLAGS += -frtti

LOCAL_SRC_FILES := \
  $(LIB_ROOT)/src/admin_rights.cpp \
  $(LIB_ROOT)/src/algorithm.cpp \
  $(LIB_ROOT)/src/http_auth.cpp \
  $(LIB_ROOT)/src/http_basic_auth.cpp \
  $(LIB_ROOT)/src/http_cookie_auth.cpp \
  $(LIB_ROOT)/src/http_message.cpp \
  $(LIB_ROOT)/src/http_parser.cpp \
  $(LIB_ROOT)/src/http_plugin_server.cpp \
  $(LIB_ROOT)/src/http_reader.cpp \
  $(LIB_ROOT)/src/http_server.cpp \
  $(LIB_ROOT)/src/http_types.cpp \
  $(LIB_ROOT)/src/http_writer.cpp \
  $(LIB_ROOT)/src/logger.cpp \
  $(LIB_ROOT)/src/plugin.cpp \
  $(LIB_ROOT)/src/process.cpp \
  $(LIB_ROOT)/src/scheduler.cpp \
  $(LIB_ROOT)/src/spdy_decompressor.cpp \
  $(LIB_ROOT)/src/spdy_parser.cpp \
  $(LIB_ROOT)/src/tcp_server.cpp \
  $(LIB_ROOT)/src/tcp_timer.cpp

LOCAL_C_INCLUDES += \
  $(LIB_ROOT)/include/ \
  $(LIB_ROOT)/../boost_1_59_0/

LOCAL_EXPORT_C_INCLUDES :=  $(LIB_ROOT)/include
include $(BUILD_STATIC_LIBRARY)
