# TVMosaic CE build procedure

## Libraries
All required libraries can be found in the libs/ directory. 
This directory also contains sources/dependent libraries for ffmpeg, comskip and vlc-android.
Libraries for TVMosaic Windows build are pre-built and commited to the stage/ directory. For all other platforms the libraries have to be built before the main TVMosaic CE executable is built with the procedure described below.

# Build for Windows
Building TVMosaic CE for Windows is a manual process and requires MSVC 2008 SP2.
The MSVC solution file is dvblex.sln inside the dvblex/ directory.
All dependent libraries for Windows platform are already prebuilt and commited to the stage/ directory. The binary dependencies (ffmpeg, comskip and sqlite) are also prebuilt.
TVMosaic CE on Windows is available only as win32 executable because of the dependent tuner libraries, which are only available in 32 bit version.
Windows installer is built by running install/platforms/win32/build_installer.bat. Installer build process requires WiX installer, which canbe downloaded from https://drive.google.com/drive/folders/15miFbg2AFaruCQrkwDXrtg1wJ9kkf4qe

Built installer will be found inside the installation/ folder.

# Build for Ubuntu/Raspberry/all supported NAS models

Executables for linux-based systems are cross-compiled using docker container with all required tools. This is also largely manual process.
This docker container takes a tvmosaic source tree and a directory with platform specific toolchains as mounted volumes.
To build the docker image, execute 
````
docker build --tag dvblogic/tvm-build:1.0 .
````

Run docker container interactively with
````
docker run -it -v /var/run/docker.sock:/var/run/docker.sock -v <local directory with tvmosaic code tree>:/opt/tvmosaic -v <local directory with cross compilers>:/opt/sdk/tvmosaic dvblogic/tvm-build:1.0 bash
````

Download and unpack required toolchains to some directory on your system. The toolchains for all supported platforms can be downloaded from https://drive.google.com/drive/folders/17AmYChEOzH0AU6kLF3qn5iKwftaCw3rI

When running build for the first time, the dependent libraries have to be built first. To do that, execute the following commands **inside the build container**:
````
cd /opt/tvmosaic/libs/build

To build libraries for synology evansport, for example:
./build_synology_evansport_release.sh
or to build libraries for all platforms:
./rebuild_all.sh
````

Once finished, change to dvblex/ directory and build the server executable:
````
cd /opt/tvmosaic/dvblex

To build binaries for synology evansport, for example:
./build_synology_evansport_release.sh
or to build binaries for all platforms:
./rebuild_all.sh
````

When build is finished create installers by running ./build_installer.sh inside install/platforms/< platform/architecture of your choice >

All platform specific executables for ffmpeg, comskip and sqlite are pre-built and committed to the tree.

Built installer(s) will be found inside the installation/ folder.

# Build for MacOS

Build for MacOS needs to be done natively on the Mac with the build tools (gcc) installed via homebrew and awaits for someone to describe the exact procedure.
