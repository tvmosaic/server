#!/bin/bash
#

#location ankers
CURDIR="`pwd`"
SVN_ROOT="${CURDIR}/../../../.."

. ${SVN_ROOT}/install/platforms/global_vars.sh

#product definitions
TVM_PREFIX="android-all"

. ${SVN_ROOT}/install/platforms/common_vars.sh

#package files root
PACKAGE_ROOT="$TEMP_INSTALL_ROOT/tvmosaic"

. ${SVN_ROOT}/install/platforms/tvm_tree_build.sh

TVM_DEST_BIN_DIR=$PACKAGE_ROOT/bin
TVM_PRIVATE_BIN_DIR=$PACKAGE_ROOT/data
TVM_SHARED_BIN_DIR=$PACKAGE_ROOT/shared.inst

#copy hdhomerun.so and jni.so

mkdir -p $TVM_DEST_BIN_DIR
cp -rf $STAGE_ROOT/lib/libhdhomerun.so $TVM_DEST_BIN_DIR/
cp -rf $STAGE_ROOT/bin/tvmosaic/libtvmosaic_jni.so $TVM_DEST_BIN_DIR/

# reset file timestamps
#
echo "Updating timestamps..."
DATE=`date +%Y%m%d`
TIME="0000"
find $PACKAGE_ROOT/* -exec touch -t ${DATE}${TIME} {} \;

# packaging
echo "Packaging ..."
#TVM_ARCHIVE_NAME="$TVM_PRODUCT_NAME-raspberry-linux-armhf_openelec-$TVM_VERSION.tar.gz"

cd $TVM_DEST_BIN_DIR
zip -r ./bin.zip ./*
mv ./bin.zip ../../

cd $TVM_SHARED_BIN_DIR
zip -r ./shared.zip ./*
mv ./shared.zip ../../

cd $TVM_PRIVATE_BIN_DIR
zip -r ./private.zip ./*
mv ./private.zip ../../

cd $CURDIR

echo "Done!"
