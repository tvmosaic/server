#!/bin/sh

TVM_ROOT_DIR="/usr/local/bin/tvmosaic"
TVM_SHARE_PATH="/opt/TVMosaic"

#copy the default content of the shared dir to its actual place on each start-up
cp -rpaf ${TVM_ROOT_DIR}/shared.inst/* ${TVM_SHARE_PATH}/

#web directory
$TVM_ROOT_DIR/reg.sh -unziptodir "${TVM_ROOT_DIR}/data/web/root.zip" "${TVM_ROOT_DIR}/data/web/root"

if [ ! -f $TVM_SHARE_PATH/tvmosaic_configuration.xml ]; then
	#new install

#	chmod +rw ${TVM_SHARE_PATH}/xmltv
#	chmod +rw ${TVM_SHARE_PATH}/channel_logo

#because we can only persist data in a docker volume, two things need to be done:
# 1. generate tvmosaic_configuration.xml in TVM_ROOT_DIR, then move it to TVM_SHARE_PATH
# 2. make a link to config directory to actually reside in shared path
	$TVM_ROOT_DIR/reg.sh -preparenewinstall "${TVM_ROOT_DIR}" "${TVM_ROOT_DIR}/data" "${TVM_SHARE_PATH}"
	mv -f ${TVM_ROOT_DIR}/tvmosaic_configuration.xml ${TVM_SHARE_PATH}/
fi

#make a link to $TVM_SHARE_PATH/tvmosaic_configuration.xml from ${TVM_ROOT_DIR}
ln -sf ${TVM_SHARE_PATH}/tvmosaic_configuration.xml ${TVM_ROOT_DIR}/tvmosaic_configuration.xml

#remove config directory inside private root (if it exists), create one in shared dir and link aone to another
rm -rf ${TVM_ROOT_DIR}/data/config
mkdir -p ${TVM_SHARE_PATH}/config
ln -sf ${TVM_SHARE_PATH}/config ${TVM_ROOT_DIR}/data/config

/usr/local/bin/tvmosaic/start.sh

#bash

