#!/bin/sh

waittime=0
while true ; do
    IP=$(ip route | awk '/default/ { print $3 }')
    echo "Default gateay is $IP"

    ping -c 1 -w 2 $IP
    RETVAL=$?
    if [ $RETVAL -eq 0 ]; then
	echo "ping $IP: success"
	break
    else
	echo "Gateway $IP is not reachable"
	`sleep 1`
	waittime=`expr $waittime + 1`
	if [ $waittime -ge 10 ]; then
	    echo "timeout waiting for network availability"
    	    exit 1
	fi
    fi
done

exit 0