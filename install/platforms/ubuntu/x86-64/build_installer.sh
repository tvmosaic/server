#!/bin/bash
#

#check if running as a root
SUDO=''
if (( $EUID != 0 )); then
    SUDO='sudo'
fi

#get parameters
DOCKER_BUILD=$1

#location ankers
CURDIR="`pwd`"
SVN_ROOT="${CURDIR}/../../../.."

. ${SVN_ROOT}/install/platforms/global_vars.sh

#product definitions
TVM_PREFIX="ubuntu-x86_64"
TVM_ARCHIVE_NAME="$TVM_PRODUCT_NAME-pc-linux-$TVM_PREFIX-$TVM_VERSION.deb"

. ${SVN_ROOT}/install/platforms/common_vars.sh

#package files root
PACKAGE_ROOT="$TEMP_INSTALL_ROOT/app-root/usr/local/bin/tvmosaic"
TRANSCODING_ENABLED="true"
. ${SVN_ROOT}/install/platforms/tvm_tree_build.sh

#vaapi ffmpeg scripts
cp -Raf $SVN_ROOT/install/common/ffmpeg/linux/profiles/* $SHARED_ROOT/ffmpeg/profiles/

#local vars and actions
APP_ROOT="$TEMP_INSTALL_ROOT/app-root"
mkdir -p "$APP_ROOT"

#copy app-root files
cp -raf $RESOURCES_ROOT/app-root/* $APP_ROOT/

chmod 755 $APP_ROOT/DEBIAN/*
chmod 755 $APP_ROOT/etc/init.d/tvmosaic

#copy pakage root files
cp -raf $RESOURCES_ROOT/package/* $PACKAGE_ROOT/

chmod 644 $PACKAGE_ROOT/deb_control

# reset file timestamps
#
echo "Updating timestamps..."
DATE=`date +%Y%m%d`
TIME="0000"
find $APP_ROOT/* -exec touch -t ${DATE}${TIME} {} \;

# packaging
if [ "$DOCKER_BUILD" = "docker" ]; then
	echo "Creating a docker container..."

	#copy docker files
	cp -raf $RESOURCES_ROOT/docker/* $TEMP_INSTALL_ROOT/

	#remove previous latest build
	$SUDO docker image rm -f dvblogic/$TVM_PRODUCT_NAME:latest

	cd $TEMP_INSTALL_ROOT
	$SUDO docker build -t dvblogic/$TVM_PRODUCT_NAME:$TVM_VERSION.$SVN_REVISION .
	$SUDO docker build -t dvblogic/$TVM_PRODUCT_NAME:latest .
	cd $CURDIR
else
	echo "Packaging dpkg..."
	cd $TEMP_INSTALL_ROOT
	dpkg -b app-root $TVM_ARCHIVE_NAME
	mv $TVM_ARCHIVE_NAME ../../
	cd $CURDIR
fi

echo "Done!"
exit 0
