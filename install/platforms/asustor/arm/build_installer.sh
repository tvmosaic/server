#!/bin/bash
#

#check if running as a root
SUDO=''
if (( $EUID != 0 )); then
    SUDO='sudo'
fi

#location ankers
CURDIR="`pwd`"
SVN_ROOT="${CURDIR}/../../../.."

. ${SVN_ROOT}/install/platforms/global_vars.sh

#product definitions
TVM_PREFIX="asustor-arm"
TVM_ARCHIVE_NAME="$TVM_PRODUCT_NAME-asustor-linux-arm-$TVM_VERSION.apk"

. ${SVN_ROOT}/install/platforms/common_vars.sh

#package files root
PACKAGE_ROOT="$TEMP_INSTALL_ROOT/bin"

#asustor requires su permissions when building the tree
$SUDO rm -rf $TEMP_INSTALL_ROOT

. ${SVN_ROOT}/install/platforms/tvm_tree_build.sh


#copy package root files
cp -raf $RESOURCES_ROOT/package/* $PACKAGE_ROOT/

#copy package scripts
SCRIPT_DIR="adm"

cp -raf $RESOURCES_ROOT/$SCRIPT_DIR/CONTROL $TEMP_INSTALL_ROOT/

#stamp and copy app config file
. $SVN_ROOT/${SET_REVISION_SCRIPT} $SVN_ROOT $RESOURCES_ROOT/$SCRIPT_DIR/config.json $TEMP_INSTALL_ROOT/CONTROL/config.json

# reset file timestamps
#
echo "Updating timestamps..."
DATE=`date +%Y%m%d`
TIME="0000"
find $TEMP_INSTALL_ROOT/* -exec touch -t ${DATE}${TIME} {} \;

# taring
#
echo "Packaging..."
$SUDO python $CURDIR/../apkg-tools.py create $TEMP_INSTALL_ROOT --destination $TEMP_INSTALL_ROOT/..

$SUDO mv $TEMP_INSTALL_ROOT/../tvmosaic_$TVM_VERSION*.apk $TEMP_INSTALL_ROOT/../../$TVM_ARCHIVE_NAME

echo "Done!"

exit 0
