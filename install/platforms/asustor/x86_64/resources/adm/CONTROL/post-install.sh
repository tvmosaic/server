#!/bin/sh


TVMOSAIC_TMP_DIR="$APKG_TEMP_DIR/../${APKG_PKG_NAME}_temp"
TVMOSAIC_ROOT_DIR=$APKG_PKG_DIR/bin
SHARE_PATH="/volume1/TVMosaic"

do_install()
{
    ln -sf ${SHARE_PATH} ${TVMOSAIC_ROOT_DIR}/share

    #copy shared.inst to share and delete it afterwards
    cp -raf ${TVMOSAIC_ROOT_DIR}/shared.inst/* $SHARE_PATH/
    rm -rf ${TVMOSAIC_ROOT_DIR}/shared.inst

    #web directory
    $TVMOSAIC_ROOT_DIR/reg.sh -unziptodir "${TVMOSAIC_ROOT_DIR}/data/web/root.zip" "${TVMOSAIC_ROOT_DIR}/data/web/root"

    chmod +rw ${SHARE_PATH}/xmltv
    chmod +rw ${SHARE_PATH}/channel_logo

    $TVMOSAIC_ROOT_DIR/reg.sh -preparenewinstall "${TVMOSAIC_ROOT_DIR}" "${TVMOSAIC_ROOT_DIR}/data" "${SHARE_PATH}"
}


do_post_upgrade()
{
    ln -sf ${SHARE_PATH} ${TVMOSAIC_ROOT_DIR}/share

    #copy shared.inst to share and delete it afterwards
    cp -rpaf ${TVMOSAIC_ROOT_DIR}/shared.inst/* $SHARE_PATH/
    rm -rf ${TVMOSAIC_ROOT_DIR}/shared.inst

    #web directory
    rm -rf ${TVMOSAIC_ROOT_DIR}/data/web/root
    mkdir -p ${TVMOSAIC_ROOT_DIR}/data/web/root
    $TVMOSAIC_ROOT_DIR/reg.sh -unziptodir "${TVMOSAIC_ROOT_DIR}/data/web/root.zip" "${TVMOSAIC_ROOT_DIR}/data/web/root"

    #restore configuration files
    cp -rf $TVMOSAIC_TMP_DIR/config/* ${TVMOSAIC_ROOT_DIR}/data/config/
    cp -rf $TVMOSAIC_TMP_DIR/tvmosaic_configuration.xml ${TVMOSAIC_ROOT_DIR}/
    rm -rf $TVMOSAIC_TMP_DIR
}

case "$APKG_PKG_STATUS" in
        install)
                do_install
                ;;
        upgrade)
                do_post_upgrade
                ;;
        *)
                ;;
esac

exit 0
