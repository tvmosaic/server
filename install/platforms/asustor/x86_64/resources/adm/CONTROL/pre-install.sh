#!/bin/sh

TVMOSAIC_TMP_DIR="$APKG_TEMP_DIR/../${APKG_PKG_NAME}_temp"
TVMOSAIC_ROOT_DIR=$APKG_PKG_DIR/bin

do_pre_upgrade()
{
	rm -rf $TVMOSAIC_TMP_DIR
	mkdir -p $TVMOSAIC_TMP_DIR/config

	#copy existing configuration files to tvmosaic temp dir
	cp -rf $TVMOSAIC_ROOT_DIR/data/config/* $TVMOSAIC_TMP_DIR/config/
	cp -rf $TVMOSAIC_ROOT_DIR/tvmosaic_configuration.xml $TVMOSAIC_TMP_DIR/
}


case "$APKG_PKG_STATUS" in
	install)
		;;
	upgrade)
		do_pre_upgrade
		;;
	*)
		;;
esac

exit 0
