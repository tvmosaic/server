#!/bin/sh

APKG_PKG_DIR=/usr/local/AppCentral/tvmosaic

case "$1" in
    start)
        $APKG_PKG_DIR/bin/start2.sh
        ;;
    stop)
        $APKG_PKG_DIR/bin/stop.sh
        ;;
    restart)
        $APKG_PKG_DIR/bin/stop.sh
        $APKG_PKG_DIR/bin/start2.sh
        ;;
    *)
        echo "Usage: {start|stop|restart}"
        exit 2
        ;;
esac

exit 0

