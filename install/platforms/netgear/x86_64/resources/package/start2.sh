#!/bin/sh

. $(dirname $0)/incl

export LD_LIBRARY_PATH=${TVM_ROOT_DIR}/lib
export TVMOSAIC_ROOT_CONFIG_DIR=${TVM_ROOT_DIR}

${TVM_ROOT_DIR}/tvmosaic_server

process=`ps ax | grep ${SERVER_NAME} | grep -v grep`
pid=`echo $process | awk '{ print $1 }'`
echo ${pid} > ${TVM_ROOT_DIR}/pidfile

