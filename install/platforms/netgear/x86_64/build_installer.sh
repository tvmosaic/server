#!/bin/bash
#

#location ankers
CURDIR="`pwd`"
SVN_ROOT="${CURDIR}/../../../.."
SYNOLOGY_COMMON_DIR="${CURDIR}/../common"

. ${SVN_ROOT}/install/platforms/global_vars.sh

#product definitions
TVM_PREFIX="netgear-x86_64"
TVM_ARCHIVE_NAME="$TVM_PRODUCT_NAME-netgear-linux-r6-x86_64-$TVM_VERSION.deb"

. ${SVN_ROOT}/install/platforms/common_vars.sh

#package files root
PACKAGE_ROOT="$TEMP_INSTALL_ROOT/tvmosaic/apps/tvmosaic"
TRANSCODING_ENABLED="true"

. ${SVN_ROOT}/install/platforms/tvm_tree_build.sh

#vaapi ffmpeg scripts
cp -Raf $SVN_ROOT/install/common/ffmpeg/linux/profiles/* $SHARED_ROOT/ffmpeg/profiles/

APP_ROOT="$TEMP_INSTALL_ROOT/tvmosaic"

#copy app-root files
cp -raf $RESOURCES_ROOT/tvmosaic/* $APP_ROOT/

chmod 755 $APP_ROOT/DEBIAN/*

#copy package root files
cp -raf $RESOURCES_ROOT/package/* $PACKAGE_ROOT/

#stamp config.xml and ovewrite the destination file
. $SVN_ROOT/${SET_REVISION_SCRIPT} $SVN_ROOT $RESOURCES_ROOT/tvmosaic/apps/tvmosaic/config.xml $PACKAGE_ROOT/config.xml
. $SVN_ROOT/${SET_REVISION_SCRIPT} $SVN_ROOT $RESOURCES_ROOT/tvmosaic/DEBIAN/control $TEMP_INSTALL_ROOT/tvmosaic/DEBIAN/control

# reset file timestamps
#
echo "Updating timestamps..."
DATE=`date +%Y%m%d`
TIME="0000"
find $TEMP_INSTALL_ROOT/* -exec touch -t ${DATE}${TIME} {} \;

# taring
#
echo "Packaging..."

cd $TEMP_INSTALL_ROOT/tvmosaic
hashdeep -c md5 -r ./apps > ./DEBIAN/md5sums

cd $TEMP_INSTALL_ROOT
fakeroot dpkg-deb --build tvmosaic

mv tvmosaic.deb ../../$TVM_ARCHIVE_NAME

echo "Done!"

exit 0

