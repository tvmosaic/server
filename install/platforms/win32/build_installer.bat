

set CUR_DIR=%CD%
set SVN_ROOT=%CUR_DIR%\..\..\..
set SOLUTION_ROOT=%SVN_ROOT%
set DEST_ROOT=%CUR_DIR%\product_info
set PI_SRC_DIR=%CUR_DIR%\product_info
set SCANNERS_DIR=%SOLUTION_ROOT%\dvblex\scanners
set OUTPUT_DIR=%SOLUTION_ROOT%\installation

:: create product files
call %SOLUTION_ROOT%\set_revision.bat %SVN_ROOT% %PI_SRC_DIR%\tvmosaic.xm_ %DEST_ROOT%\tvmosaic.xml 20000

:: build wix installer - 32 bit (there is no 64 bit tvmosaic exe)
"%WIX%bin\candle.exe" tvmosaic_win32.xml -out "%OUTPUT_DIR%\tvmosaic_win32.wixobj" -ext WixUtilExtension -ext WiXNetFxExtension
"%WIX%bin\light.exe" "%OUTPUT_DIR%\tvmosaic_win32.wixobj" -sval -out "%OUTPUT_DIR%\tvmosaic_win32.msi" -ext WixUtilExtension -ext WiXNetFxExtension

:: copy update bat to installation dest
copy /Y tvmosaic_update.bat %OUTPUT_DIR%\
copy /Y tvmosaic_install.bat %OUTPUT_DIR%\
