#!/bin/bash
#

#location ankers
CURDIR="`pwd`"
SVN_ROOT="${CURDIR}/../../../.."
SYNOLOGY_COMMON_DIR="${CURDIR}/../common"

. ${SVN_ROOT}/install/platforms/global_vars.sh

#product definitions
TVM_PREFIX="synology-rtd1296"
TVM_ARCHIVE_NAME="$TVM_PRODUCT_NAME-synology-linux-arm64-$TVM_VERSION.spk"

. ${SVN_ROOT}/install/platforms/common_vars.sh

#package files root
PACKAGE_ROOT="$TEMP_INSTALL_ROOT/package"

. ${SVN_ROOT}/install/platforms/tvm_tree_build.sh


#copy package root files
cp -raf $RESOURCES_ROOT/package/* $PACKAGE_ROOT/

#copy package scripts
SCRIPT_DIR="tvmosaic"

cp -raf $RESOURCES_ROOT/$SCRIPT_DIR $TEMP_INSTALL_ROOT/
chmod 774 $TEMP_INSTALL_ROOT/$SCRIPT_DIR/WIZARD_UIFILES/*
chmod 774 $TEMP_INSTALL_ROOT/$SCRIPT_DIR/scripts/*

#stamp INFO and ovewrite the destination file
. $SVN_ROOT/${SET_REVISION_SCRIPT} $SVN_ROOT $RESOURCES_ROOT/$SCRIPT_DIR/INFO $TEMP_INSTALL_ROOT/$SCRIPT_DIR/INFO

#copy firmware files to share directory
cp -raf $SYNOLOGY_COMMON_DIR/firmware $SHARED_ROOT/

# reset file timestamps
#
echo "Updating timestamps..."
DATE=`date +%Y%m%d`
TIME="0000"
find $TEMP_INSTALL_ROOT/* -exec touch -t ${DATE}${TIME} {} \;

# taring
#
echo "Packaging..."
cd $PACKAGE_ROOT/
tar czvf package.tgz --exclude-vcs * > /dev/null
mv package.tgz ../$SCRIPT_DIR/

cd ../$SCRIPT_DIR/

tar cvf $TVM_ARCHIVE_NAME * > /dev/null
mv $TVM_ARCHIVE_NAME ../../..

echo "Done!"

exit 0
