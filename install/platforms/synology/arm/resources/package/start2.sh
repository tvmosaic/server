#!/bin/sh

. $(dirname $0)/incl

#copy firmware files to share directory
cp -raf $SYNOLOGY_COMMON_DIR/firmware $SHARED_ROOT/

export LD_LIBRARY_PATH=${TVM_ROOT_DIR}/lib
export TVMOSAIC_ROOT_CONFIG_DIR=${TVM_ROOT_DIR}

${TVM_ROOT_DIR}/tvmosaic_server

