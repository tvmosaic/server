#!/bin/bash
#

#location ankers
CURDIR="`pwd`"
SVN_ROOT="${CURDIR}/../../../.."

. ${SVN_ROOT}/install/platforms/global_vars.sh

#product definitions
TVM_PREFIX="darwin-x86_64"

. ${SVN_ROOT}/install/platforms/common_vars.sh

#overwrite macos-specific vars
SIGN_ROOT="$SVN_ROOT/signing/macos"
SO_EXTENSION="dylib"
SET_REVISION_SCRIPT="set_revision_macosx.sh"

#remove previous install dir
rm -rf $TEMP_INSTALL_ROOT

#local vars and actions
APP_ROOT="$TEMP_INSTALL_ROOT/app-root"

#package files root
PACKAGE_ROOT="$TEMP_INSTALL_ROOT/app-root/TVMosaic.app/Contents/server"
TRANSCODING_ENABLED="true"

. ${SVN_ROOT}/install/platforms/tvm_tree_build.sh

#copy app-root files
cp -Raf $RESOURCES_ROOT/root/* $PACKAGE_ROOT/

# reset file timestamps
#
echo "Updating timestamps..."
DATE=`date +%Y%m%d`
TIME="0000"
find $APP_ROOT/* -exec touch -t ${DATE}${TIME} {} \;

#find $APP_ROOT/TVMosaic.app/Contents/Frameworks -name "Info.plist" -type f -delete

# packaging  
TVM_PACKAGE_NAME="$TVM_PRODUCT_NAME-pc-macos-x86_64-$TVM_VERSION.pkg"
TVM_ARCHIVE_NAME="$TVM_PRODUCT_NAME-pc-macos-x86_64-$TVM_VERSION.dmg"

PKG_RESOURCES_ROOT=$RESOURCES_ROOT/tvmosaic

cd $TEMP_INSTALL_ROOT

#create an empty component list (because pkgbuild will throw away all compoennts inside Frameworks)
pkgbuild --analyze --root zzz nocomponents.plist

pkgbuild \
--root $APP_ROOT \
--sign 599D44JJ97 \
--component-plist nocomponents.plist \
--install-location /Applications/ \
--identifier com.dvblogic.tvmosaic.server \
--version $TVM_VERSION \
--scripts $PKG_RESOURCES_ROOT/scripts \
--ownership recommended \
tvmosaic_server_cmp.pkg

productbuild --distribution $PKG_RESOURCES_ROOT/xml/distribution.xml \
#--sign <your apple sign certificate id> \
--resources $PKG_RESOURCES_ROOT/resources \
--version $TVM_VERSION \
./${TVM_PACKAGE_NAME}

#make dmg file
hdiutil create -ov -volname "TVMosaic CE installer" -srcfolder ${TVM_PACKAGE_NAME} ../../${TVM_ARCHIVE_NAME}

cd $CURDIR

echo "Done!"
exit 0
