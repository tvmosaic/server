#!/bin/sh

. "$(dirname "$0")"/incl

stop_tvmosaic()
{
    process=`ps ax | grep ${SERVER_NAME} | grep -v grep`
    if [ -n "$process" ] ; then
        pid=`echo $process | awk '{ print $1 }'`
        if [ -n "$pid" ] ; then
            kill -2 $pid
            process=`ps ax | grep ${SERVER_NAME} | grep -v grep`
            waittime=0
            while [ -n "$process" ] ; do
                `sleep 1`
                waittime=$((waittime + 1))
                if [ $waittime -ge 30 ]; then
                    break
                fi
            process=`ps ax | grep ${SERVER_NAME} | grep -v grep`
            done
            kill -9 $pid
        fi
    fi
    `sleep 5`
}

stop_tvmosaic

