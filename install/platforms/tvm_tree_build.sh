#!/bin/bash
#

echo "Package root ..."

#(re)-create temp install dir
rm -rf "$TEMP_INSTALL_ROOT"
mkdir -p "$TEMP_INSTALL_ROOT"

mkdir -p "$PACKAGE_ROOT"

#copy and sign binaries
cp -rf $STAGE_ROOT/bin/tvmosaic/tvmosaic_server $PACKAGE_ROOT/
cp -rf $STAGE_ROOT/bin/tvmosaic/tvmosaic_reg $PACKAGE_ROOT/

chmod +x $PACKAGE_ROOT/tvmosaic_server
chmod +x $PACKAGE_ROOT/tvmosaic_reg

echo "Shared libraries"

mkdir -p $PACKAGE_ROOT/lib
cp -Raf $STAGE_ROOT/lib/*.${SO_EXTENSION} $PACKAGE_ROOT/lib/
cp -Raf $STAGE_ROOT/lib/*.${SO_EXTENSION}.* $PACKAGE_ROOT/lib/

#shared and private roots
SHARED_ROOT="$PACKAGE_ROOT/shared.inst"
PRIVATE_ROOT="$PACKAGE_ROOT/data"
mkdir -p "$SHARED_ROOT"
mkdir -p "$PRIVATE_ROOT"

#private folder
echo "Private folder/common"
mkdir -p $PRIVATE_ROOT/common

#create a directory for comskip, so that others can drop their comskip there
mkdir -p $PRIVATE_ROOT/common/comskip

#copy binaries - ffmpeg, comskip, sqlite
cp -Raf $RESOURCES_ROOT/bin/* $PRIVATE_ROOT/common/

#html resources
mkdir -p $PRIVATE_ROOT/common/resources
cp -Raf $SVN_ROOT/install/html_resources/* $PRIVATE_ROOT/common/resources/

#product info
mkdir -p $PRIVATE_ROOT/common/product_info

echo "Stamping revision..."
. $SVN_ROOT/${SET_REVISION_SCRIPT} $SVN_ROOT $RESOURCES_ROOT/product_info/tvmosaic.xm_ $PRIVATE_ROOT/common/product_info/tvmosaic.xml

dos2unix -q $PRIVATE_ROOT/common/product_info/*.xml
chmod 644 $PRIVATE_ROOT/common/product_info/*.xml

echo "Private folder/config"
mkdir -p $PRIVATE_ROOT/config

echo "Private folder/devices/dvb/file"
mkdir -p $PRIVATE_ROOT/devices/dvb/file

echo "Private folder/web"
mkdir -p $PRIVATE_ROOT/web
cp -Raf $SVN_ROOT/dvblex/src/dvblink_server/player/web/mime.xml $PRIVATE_ROOT/web/
cp -Raf $SVN_ROOT/dvblex/src/dvblink_server/player/web/root.zip $PRIVATE_ROOT/web/

echo "Shared folder/xmltv"
mkdir -p $SHARED_ROOT/xmltv

echo "Shared folder/licenses"
mkdir -p $SHARED_ROOT/licenses

echo "Shared folder/ssl"
mkdir -p $SHARED_ROOT/ssl

echo "Shared folder/common"
mkdir -p $SHARED_ROOT/common
cp -f $SVN_ROOT/dvblex/src/dvblink_server/components/xmltv/xmltv_categorymap.xml $SHARED_ROOT/common/

echo "Shared folder/language"
mkdir -p $SHARED_ROOT/language
cp -f $SVN_ROOT/dvblex/src/dvblink_server/language/* $SHARED_ROOT/language/
dos2unix -q $SHARED_ROOT/language/*.xml

echo "Shared folder/scanners"
mkdir -p $SHARED_ROOT/scanners
cp -Raf $SVN_ROOT/dvblex/scanners/* $SHARED_ROOT/scanners/

echo "Shared folder/channel_logo"
mkdir -p $SHARED_ROOT/channel_logo

echo "Shared folder/recorder_database"
mkdir -p $SHARED_ROOT/recorder_database
cp -f $RESOURCES_ROOT/bin/sqlite/dlrepair.sh $SHARED_ROOT/recorder_database/

echo "ffmpeg common scripts"
mkdir -p $SHARED_ROOT/ffmpeg
if [ -z "${TRANSCODING_ENABLED-}" ]; then
	echo "without transcoding"
	FFMPEG_SCRIPTS_DIR="without_transcoding"
else
	echo "with transcoding"
	FFMPEG_SCRIPTS_DIR="with_transcoding"
fi
cp -Raf $SVN_ROOT/install/common/ffmpeg/${FFMPEG_SCRIPTS_DIR}/* $SHARED_ROOT/ffmpeg/

#comskip profiles
mkdir -p $SHARED_ROOT/comskip/
cp -Raf $SVN_ROOT/install/common/comskip/* $SHARED_ROOT/comskip/

echo "platform specific shared files"
cp -Raf $RESOURCES_ROOT/shared/* $SHARED_ROOT/
