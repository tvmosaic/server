-- Raspbian installation --
Copy tvmosaic_tvmosaic-raspberry-linux-armhf_raspbian-x.x.x.deb to your Raspbian installation on a Raspberry Pi.
From the ssh command line execute
sudo dpkg -i tvmosaic-raspberry-linux-armhf_raspbian-x.x.x.deb


-- Raspberry Openelec installation --
Create a temporary directory on your Openelec Raspberry Pi system:
mkdir /storage/inst

Copy tvmosaic-raspberry-linux-armhf_openelec-x.x.x.tar.gz to this directory.
From the ssh command line extract the dvblink installation files:
cd /storage/inst
tar -xzvf tvmosaic-raspberry-linux-armhf_openelec-x.x.x.tar.gz

Then follow the installation instructions as described in the /storage/inst/README.FIRST file.
