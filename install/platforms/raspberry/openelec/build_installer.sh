#!/bin/bash
#

#location ankers
CURDIR="`pwd`"
SVN_ROOT="${CURDIR}/../../../.."

. ${SVN_ROOT}/install/platforms/global_vars.sh

#product definitions
TVM_PREFIX="raspberry-arm"

. ${SVN_ROOT}/install/platforms/common_vars.sh

#package files root
PACKAGE_ROOT="$TEMP_INSTALL_ROOT/tvmosaic"

. ${SVN_ROOT}/install/platforms/tvm_tree_build.sh

#copy pakage root files
cp -raf $RESOURCES_ROOT/package/* $PACKAGE_ROOT/

#copy install script and readme
cp -raf $RESOURCES_ROOT/README.FIRST $TEMP_INSTALL_ROOT/
cp -raf $RESOURCES_ROOT/install.sh $TEMP_INSTALL_ROOT/

# reset file timestamps
#
echo "Updating timestamps..."
DATE=`date +%Y%m%d`
TIME="0000"
find $PACKAGE_ROOT/* -exec touch -t ${DATE}${TIME} {} \;

# packaging
echo "Packaging ..."
TVM_ARCHIVE_NAME="$TVM_PRODUCT_NAME-raspberry-linux-armhf_openelec-$TVM_VERSION.tar.gz"

cd $TEMP_INSTALL_ROOT
tar -czvf ./$TVM_ARCHIVE_NAME ./*
mv $TVM_ARCHIVE_NAME ../../

cd $CURDIR

echo "Done!"
