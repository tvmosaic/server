#!/bin/bash
#

cd openelec
. ./build_installer.sh
cd ..

cd raspbian
. ./build_installer.sh
cd ..

#location ankers
CURDIR="`pwd`"
SVN_ROOT="${CURDIR}/../../.."

. ${SVN_ROOT}/install/platforms/global_vars.sh

#product definitions
TVM_PREFIX="raspberry-arm"

. ${SVN_ROOT}/install/platforms/common_vars.sh

TVM_INSTALLATION_DIR=$SVN_ROOT/installation

OPENELEC_ARCHIVE_NAME="$TVM_PRODUCT_NAME-raspberry-linux-armhf_openelec-$TVM_VERSION.tar.gz"
RASPBIAN_ARCHIVE_NAME="$TVM_PRODUCT_NAME-raspberry-linux-armhf_raspbian-$TVM_VERSION.deb"
RPI_ARCHIVE_NAME="$TVM_PRODUCT_NAME-raspberry-linux-arm-$TVM_VERSION.tar.gz"

cp -f ./README.txt $TVM_INSTALLATION_DIR/

#package both server installers into a single archive
cd $TVM_INSTALLATION_DIR

tar -czvf ./${RPI_ARCHIVE_NAME} ./${OPENELEC_ARCHIVE_NAME} ./${RASPBIAN_ARCHIVE_NAME} ./README.txt

cd $CURDIR


echo "Done!"
exit 0
