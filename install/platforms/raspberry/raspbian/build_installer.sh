#!/bin/bash
#

#location ankers
CURDIR="`pwd`"
SVN_ROOT="${CURDIR}/../../../.."

. ${SVN_ROOT}/install/platforms/global_vars.sh

#product definitions
TVM_PREFIX="raspberry-arm"

. ${SVN_ROOT}/install/platforms/common_vars.sh

#package files root
PACKAGE_ROOT="$TEMP_INSTALL_ROOT/app-root/usr/local/bin/tvmosaic"

. ${SVN_ROOT}/install/platforms/tvm_tree_build.sh

#local vars and actions
APP_ROOT="$TEMP_INSTALL_ROOT/app-root"
mkdir -p "$APP_ROOT"

#copy app-root files
cp -raf $RESOURCES_ROOT/app-root/* $APP_ROOT/

chmod 755 $APP_ROOT/DEBIAN/*
chmod 755 $APP_ROOT/etc/init.d/tvmosaic

#copy pakage root files
cp -raf $RESOURCES_ROOT/package/* $PACKAGE_ROOT/

chmod 644 $PACKAGE_ROOT/deb_control

# reset file timestamps
#
echo "Updating timestamps..."
DATE=`date +%Y%m%d`
TIME="0000"
find $APP_ROOT/* -exec touch -t ${DATE}${TIME} {} \;

# packaging
echo "Packaging ..."
TVM_ARCHIVE_NAME="$TVM_PRODUCT_NAME-raspberry-linux-armhf_raspbian-$TVM_VERSION.deb"

cd $TEMP_INSTALL_ROOT
dpkg -b app-root $TVM_ARCHIVE_NAME
mv $TVM_ARCHIVE_NAME ../../

cd $CURDIR

echo "Done!"
