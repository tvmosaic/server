#!/bin/sh

. $(dirname $0)/incl

stop_tvmosaic()
{
	pid=`pidof ${SERVER_NAME}`
	if [ -n "$pid" ] ; then
	    kill -2 $pid
	    waittime=0
	    while [ -d /proc/$pid ] ; do
		`sleep 1`
		waittime=`expr $waittime + 1`
		if [ $waittime -ge 30 ]; then
		    break
		fi
	    done
	    kill -9 $pid
	fi
	`sleep 5`
}

stop_tvmosaic

