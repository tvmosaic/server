#!/bin/bash
#

#location ankers
CURDIR="`pwd`"
SVN_ROOT="${CURDIR}/../../../.."

. ${SVN_ROOT}/install/platforms/global_vars.sh

#product definitions
TVM_PREFIX="qnap-armx41"
TVM_ARCHIVE_NAME="$TVM_PRODUCT_NAME-qnap-linux-x41-$TVM_VERSION.qpkg"

. ${SVN_ROOT}/install/platforms/common_vars.sh

#package files root
PACKAGE_ROOT="$TEMP_INSTALL_ROOT/shared"

. ${SVN_ROOT}/install/platforms/tvm_tree_build.sh

#copy package root files
cp -raf $RESOURCES_ROOT/package/* $PACKAGE_ROOT/

#copy package scripts
SCRIPT_DIR="qpkg"

cp -raf $RESOURCES_ROOT/$SCRIPT_DIR/* $TEMP_INSTALL_ROOT/
chmod 774 $TEMP_INSTALL_ROOT/package_routines

#stamp qpkg.cfg and ovewrite the destination file
. $SVN_ROOT/${SET_REVISION_SCRIPT} $SVN_ROOT $RESOURCES_ROOT/$SCRIPT_DIR/qpkg.cfg $TEMP_INSTALL_ROOT/qpkg.cfg

#copy icons
cp -raf $CURDIR/../icons $TEMP_INSTALL_ROOT/

# reset file timestamps
#
echo "Updating timestamps..."
DATE=`date +%Y%m%d`
TIME="0000"
find $TEMP_INSTALL_ROOT/* -exec touch -t ${DATE}${TIME} {} \;

# taring
#
echo "Packaging..."
cd ../QDK/bin

QPKG_ARCH="arm-x41"
QPKG_NAME="TVMosaic"

./qbuild --root $TEMP_INSTALL_ROOT/ --build-arch $QPKG_ARCH --build-dir ../ --bzip2
mv $TEMP_INSTALL_ROOT/../$QPKG_NAME_?*_$QPKG_ARCH.qpkg $TEMP_INSTALL_ROOT/../../$TVM_ARCHIVE_NAME

cd $CURDIR

echo "Done!"

exit 0
