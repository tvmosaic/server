#!/bin/sh

CONF=/etc/config/qpkg.conf
QPKG_NAME="TVMosaic"
SERVER_NAME="tvmosaic_server"
TVM_ROOT_DIR=$(/sbin/getcfg $QPKG_NAME Install_Path -d "" -f $CONF)

case "$1" in
  start)
    ENABLED=$(/sbin/getcfg $QPKG_NAME Enable -u -d FALSE -f $CONF)
    if [ "$ENABLED" != "TRUE" ]; then
        echo "$QPKG_NAME is disabled."
        exit 1
    fi
    : ADD START ACTIONS HERE
    ${TVM_ROOT_DIR}/start2.sh
    ;;

  stop)
    : ADD STOP ACTIONS HERE
    ${TVM_ROOT_DIR}/stop.sh
    ;;

  restart)
    $0 stop
    $0 start
    ;;

  *)
    echo "Usage: $0 {start|stop|restart}"
    exit 1
esac

exit 0
