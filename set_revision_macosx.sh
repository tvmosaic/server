#!/bin/bash

# $1 - path to a Subversion working copy.
# $2 - path to a template file containing keywords.
# $3 - path to save the resulting parsed file.


#REVISION=`svn info $1 | grep 'Revision:' | cut -d: -f2 | awk '{ print $1 }'`
R=`git rev-list --count master`
REVISION=$((R + 20000))

if [ "$2" != "$3" ]
then
	cp -f $2 $3
fi

sed -i '' "s/WCRANGE/$REVISION/g" $3

echo "REVISION = $REVISION"


