/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <openssl/aes.h>
#include <boost/foreach.hpp>
#include <dl_ts.h>
#include <dl_strings.h>
#include <dl_network_helper.h>
#include <dl_http_comm.curl.h>
#include <dl_common.h>
#include "playback_segments.h"

namespace dvblink {
namespace streaming {

playback_segments::playback_segments(const m3u8_media_segments& segments) :
    target_duration_(0), offset_(0), eof_(0), seek_size_(0), seek_offs_(0),
    seeking_(false), tmpbuf_(4096, '\0'), pool_(2), shutdown_(false)
{
    const std::vector<m3u8_media_segment>& vec = segments.segments;

    for (size_t j = 0; j < vec.size(); j++)
    {
        segment_info info;
        info.seg = vec[j];
        segment_list_.push_back(info);
    }

    if (segments.targetDuration > 0)
    {
        target_duration_ = segments.targetDuration;
    }
}

playback_segments::~playback_segments()
{
    shutdown();
}

bool playback_segments::initialize()
{
    bool success = false;

    do 
    {
        // begin scope
        {
            boost::mutex::scoped_lock lock(lock_);

            if (shutdown_ || segment_list_.empty() || thread_ptr_)
            {
                break;
            }

            shutdown_ = false;
            offset_ = 0;
            eof_ = 0;

            free_unused_segments(0);
        }
        // end scope

        // load the first segment
        bool ok = load_segment(0);

        if (!ok)
        {
            break;
        }

        // begin scope
        {
            boost::mutex::scoped_lock lock(lock_);

            const segment_info& info0 = segment_list_[0]; // first segment
            size_t size0 = info0.segment_size;

            if (size0 == 0)
            {
                break;
            }
            
            BOOST_FOREACH(segment_info& info, segment_list_)
            {
                info.estimated_size = size0;
            }

            eof_ = segment_list_.size() * size0;
            seek_size_ = eof_;

            thread_ptr_ = thread_ptr(new boost::thread(boost::bind(
                &playback_segments::segment_loader, this)));
        }
        // end scope

        success = true;
    }
    while (0);
  
    return success;
}

bool playback_segments::shutdown()
{
    shutdown_ = true;
    load_event_.signal();
    read_event_.signal();
 
    if (thread_ptr_)
    {
        thread_ptr_->join();
        thread_ptr_.reset();
    }

    return true;
}

bool playback_segments::load_segment(unsigned int seg_num)
{
    using namespace dvblink::engine;
    bool success = false;
    buffer_ptr buf_ptr;

    do
    {
        std::string url;
        size_t seg_size = 0;

        // begin scope
        {
            boost::mutex::scoped_lock lock(lock_);
            const size_t size = segment_list_.size();

            if (shutdown_ || (size == 0) || (seg_num >= size))
            {
                break;
            }

            segment_info& info = segment_list_[seg_num];
            info.load_failed_ = false;

            if (info.buf_ptr)
            {
                success = true;
                break; // segment already loaded
            }

            url = info.seg.url;

            if (url.empty())
            {
                break;
            }

            seg_size = info.segment_size;
        }
        // end scope

        std::string wurl;
        std::string addr;
        std::string user;
        std::string password;
        std::string suffix;
        unsigned short port = 0;
        EDL_NET_PROTOCOLS proto = DL_NET_PROTO_UNKNOWN;

        try
        {
            proto = network_helper::parse_net_url(url.c_str(), addr, user, password, port, suffix);

            if (proto == DL_NET_PROTO_UNKNOWN)
            {
                break;
            }
        }
        catch (...)
        {
            break;
        }

        http_comm_handler http(PRODUCT_NAME_UTF8, addr.c_str(), user.c_str(), password.c_str(), port);

        if (!http.Init())
        {
            break;
        }

        buf_ptr = pool_.get_buffer();

        if (!buf_ptr)
        {
            break; // there are no free buffers in the pool
        }

        if (seg_size > 0)
        {
            if (buf_ptr->capacity() < seg_size)
            {
                buf_ptr->reserve(seg_size);
            }
        }
            
        buf_ptr->resize(0);
        
        void* h = http.SendGetRequest(suffix.c_str(), proto == DL_NET_PROTO_HTTPS);

        if (!h)
        {
            http.Term();
            break;
        }

        while (!shutdown_)
        {
            unsigned long bytes = (unsigned long)tmpbuf_.size();

            bool ok = http.ReadRequestResponse(h,
                (unsigned char*)&tmpbuf_[0], bytes, &shutdown_);

            if (ok && (bytes > 0))
            {
                // copy data to the buffer
                buf_ptr->append(&tmpbuf_[0], size_t(bytes));
            }
            else
            {
                boost::mutex::scoped_lock lock(lock_);
                segment_info& info = segment_list_[seg_num];

                if (buf_ptr->empty())
                {
                    info.load_failed_ = true;
                    break; // error
                }

                info.buf_ptr = buf_ptr;

                if (info.seg.encrypted)
                {
                    ok = info.decrypt_data();

                    if (!ok)
                    {
                        info.buf_ptr.reset();
                        info.load_failed_ = true;                                            
                        break; // error
                    }
                }
                
                info.segment_size = info.buf_ptr->size();
                update_eof();
   
                success = true;
                break; // done
            }
        }

        http.CloseRequest(h);
        http.Term();
    }
    while (0);

    if (!success && buf_ptr)
    {
        pool_.put_buffer(buf_ptr);
    }

    return success;
}

void playback_segments::free_unused_segments(unsigned int cur_seg)
{
    for (unsigned int j = 0; j < segment_list_.size(); j++)
    {
        if ((j == cur_seg) || (j == (cur_seg + 1)))
        {
            continue;
        }

        segment_info& info = segment_list_[j];
        buffer_ptr ptr = info.buf_ptr;

        if (ptr)
        {
            pool_.put_buffer(ptr);
            info.buf_ptr.reset();
        }
    }
}

unsigned int playback_segments::find_segment_by_offset() const
{
    if ((offset_ == 0) || segment_list_.empty())
    {
        return 0;
    }

    boost::uint64_t size = 0;
    unsigned int seg_num = segment_list_.size();

    for (size_t j = 0; j < segment_list_.size(); j++)
    {
        const segment_info& info = segment_list_[j];

        if (info.segment_size > 0)
        {
            size += info.segment_size;
        }
        else
        {
            size += info.estimated_size;
        }

        if (size > offset_)
        {
            seg_num = (unsigned int)(j);
            break;
        }
    }

    return seg_num;
}

bool playback_segments::move_to_next_segment(unsigned int cur_seg)
{
    if ((offset_ >= eof_) || (cur_seg >= segment_list_.size()))
    {
        return false;
    }
    
    boost::uint64_t offs = 0;

    for (unsigned int j = 0; j <= cur_seg; j++)
    {
        const segment_info& info = segment_list_[j];

        if (info.segment_size > 0)
        {
            offs += info.segment_size;
        }
        else
        {
            offs += info.estimated_size;
        }
    }

    offset_ = offs;    
    return (offset_ < eof_);
}

size_t playback_segments::get_relative_offset(unsigned int cur_seg) const
{
    if ((offset_ >= eof_) || (cur_seg >= segment_list_.size()))
    {
        return 0; // error
    }

    boost::uint64_t size = 0;

    for (unsigned int j = 0; j < cur_seg; j++)
    {
        const segment_info& info = segment_list_[j];

        if (info.segment_size > 0)
        {
            size += info.segment_size;
        }
        else
        {
            size += info.estimated_size;
        }
    }

    assert(offset_ >= size);
    return size_t(offset_ - size);
}

void playback_segments::update_eof()
{
    boost::uint64_t size = 0;

    BOOST_FOREACH(const segment_info& info, segment_list_)
    {
        if (info.segment_size > 0)
        {
            size += info.segment_size;
        }
        else if (info.estimated_size > 0)
        {
            size += info.estimated_size;
        }
    }

    eof_ = size;
}

void playback_segments::segment_loader()
{
    while (!shutdown_)
    {
        unsigned int cur_seg = 0;
        unsigned int seg_index = 0;
        bool load = false;

        // begin scope
        {
            boost::mutex::scoped_lock lock(lock_);

            cur_seg = find_segment_by_offset();
            assert(cur_seg < segment_list_.size());

            free_unused_segments(cur_seg);
            segment_info& info0 = segment_list_[cur_seg];
            
            if (info0.buf_ptr && (info0.segment_size > 0))
            {
                // segment loaded - check next segment
                if ((cur_seg + 1) < segment_list_.size())
                {
                    segment_info& info1 = segment_list_[cur_seg + 1];

                    if (info1.buf_ptr && (info1.segment_size > 0))
                    {
                        // segment loaded
                    }
                    else
                    {
                        load = true;
                        seg_index = cur_seg + 1;
                    }
                }
            }
            else
            {
                load = true;
                seg_index = cur_seg;
            }
        }
        // end scope

        if (load)
        {
            load_segment(seg_index);
        }
        else
        {
            read_event_.wait(100);
        }
    }
}

bool playback_segments::read_data(unsigned char* buffer, size_t& len)
{
    assert(buffer);
    bool success = false;
    unsigned int cur_seg = 0;

    do 
    {
        // begin scope
        {
            boost::mutex::scoped_lock lock(lock_);

            if (!buffer || (len == 0) || shutdown_ || !thread_ptr_)
            {            
                break; // error
            }

            if (seeking_)
            {
                assert(seek_offs_ <= eof_);
                offset_ = seek_offs_;
                seeking_ = false;
            }

            cur_seg = find_segment_by_offset();
            assert(cur_seg < segment_list_.size());
        }
        // end scope

        while (!shutdown_)
        {
            bool segment_loaded = false;

            // begin scope
            {
                boost::mutex::scoped_lock lock(lock_);
                segment_info& info = segment_list_[cur_seg];

                if ((info.segment_size > 0) && info.buf_ptr)
                {
                    segment_loaded = true;
                }
                else if (info.load_failed_)
                {
                    break; // error
                }
            }
            // end scope

            if (segment_loaded)
            {
                boost::mutex::scoped_lock lock(lock_);
                segment_info& info = segment_list_[cur_seg];
                assert(info.buf_ptr && (info.segment_size > 0));

                size_t offs = get_relative_offset(cur_seg);

                if (!info.buf_ptr || (offs >= info.buf_ptr->size()))
                {
                    break; // unexpected error
                }

                const char* pdata = &(*(info.buf_ptr))[0] + offs;
                len = info.buf_ptr->size() - offs;
                memcpy(buffer, pdata, len);
                
                move_to_next_segment(cur_seg);                
                success = true;
                break; // done
            }
            else
            {
                load_event_.wait(100);
            }
        }
    }
    while (0);

    if (!success)
    {
        len = 0;
    }
    else
    {
        read_event_.signal();
    }

    return success;
}

bool playback_segments::seek(boost::uint64_t offset)
{
    boost::mutex::scoped_lock lock(lock_);

    if (shutdown_ || !thread_ptr_ || (seek_size_ == 0))
    {
        return false;
    }

    seeking_ = true;

    if (offset >= seek_size_)
    {
        seek_offs_ = eof_;
    }
    else
    {
        seek_offs_ = (offset * eof_) / seek_size_;
    }

    return true;
}

//-----------------------------------------------------------------------------

bool playback_segments::segment_info::decrypt_data()
{
    using namespace dvblink::engine;

    if (!buf_ptr || buf_ptr->empty())
    {
        return false;
    }

    segment_size = (buf_ptr->size() / TS_PACKET_SIZE) * TS_PACKET_SIZE;

    if (segment_size < TS_PACKET_SIZE)
    {
        return false;
    }

    const size_t key_size = 16;

    if (!seg.encrypted || (seg.aes_key.size() != key_size) ||
        (seg.aes_iv.size() != key_size))
    {
        return false;
    }

    AES_KEY key;

    AES_set_decrypt_key((unsigned char*)&(seg.aes_key[0]),
        8 * key_size, &key);

    AES_cbc_encrypt((unsigned char*)&(*buf_ptr)[0],
        (unsigned char*)&(*buf_ptr)[0], buf_ptr->size(),
        &key, (unsigned char*)&(seg.aes_iv[0]),
        AES_DECRYPT);

    buf_ptr->resize(segment_size);
    return true;
}

//-----------------------------------------------------------------------------

playback_segments::buffer_pool::buffer_pool(size_t size)
{
    for (size_t j = 0; j < size; j++)
    {
        buffer_ptr ptr(new buffer_type);
        pool_.push_back(ptr);
    }
}

playback_segments::buffer_ptr playback_segments::buffer_pool::get_buffer()
{
    boost::mutex::scoped_lock lock(lock_);
    buffer_ptr ptr;
    
    if (!pool_.empty())
    {
        ptr = pool_.back();
        pool_.pop_back();
    }

    return ptr;
}

void playback_segments::buffer_pool::put_buffer(playback_segments::buffer_ptr ptr)
{
    boost::mutex::scoped_lock lock(lock_);

    if (ptr)
    {
        pool_.push_back(ptr);
    }
}

} // namespace streaming
} // namespace dvblink

// $Id: playback_segments.cpp 7817 2013-02-07 16:03:05Z mike $
