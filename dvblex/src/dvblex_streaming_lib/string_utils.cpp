/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include "string_utils.h"

namespace dvblink {
namespace streaming {

void split_string(const std::string& str, string_list_t& list,
    const std::string sep, bool trim)
{
    std::vector<std::string> tmp_list;

    boost::split(tmp_list, str, boost::is_any_of(sep),
        boost::algorithm::token_compress_on);

    if (trim)
    {
        BOOST_FOREACH(std::string& t, tmp_list)
        {
            boost::trim(t);
        }
    }

    list.swap(tmp_list);
}

} // namespace streaming
} // namespace dvblink

// $Id: string_utils.cpp 6677 2012-10-18 14:27:57Z mike $
