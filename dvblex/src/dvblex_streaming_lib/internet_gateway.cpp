/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include <string>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include "internet_gateway.h"
#include "http_utils.h"
#include <dl_strings.h>
#include <dl_common.h>
#include <dl_network_helper.h>
#include <dl_http_comm.curl.h>
#include <dl_pugixml_helper.h>

using namespace dvblink::engine;
using namespace dvblink::pugixml_helpers;

namespace dvblink {
namespace streaming {

errcode_t get_igd_description(std::string& desc_url, igd_desc_t& igd_desc);

namespace pt = boost::posix_time;

static bool get_multicast_inerfaces(std::vector<sock_addr>& ifs)
{
    using namespace dvblink::engine;

    ifs.clear();
    TNetworkAdaptersInfo adapters;
    network_helper::get_local_net_adapters(adapters);

    sock_addr sa;

    for (size_t j = 0; j < adapters.size(); j++)
    {
        SNetworkAdaptersInfo& t = adapters[j];

        try
        {
            std::string addr = string_cast<EC_UTF8>(t.m_strAddress);
            errcode_t err = sa.set_address(addr);

            if ((err == err_none) && (addr != "127.0.0.1"))
            {
                ifs.push_back(sa);
            }
        }
        catch (...)
        {
        }
    }

    return true;
}

static bool wait_for_readable(const std::vector<udp_socket::sock_ptr>& v,
    dvblink::timeout_t to)
{
    if (v.empty())
    {
        return false;
    }

    fd_set set;
    FD_ZERO(&set);
    int fd_max = 0;

    for (size_t j = 0; j < v.size(); j++)
    {
        udp_socket::sock_ptr sptr = v[j];

        if (sptr)
        {
        #ifndef _WIN32
            basic_socket::handle_t fd = sptr->get_handle();

            if (fd >= FD_SETSIZE)
            {
                continue;
            }

            if (fd > fd_max)
            {
                fd_max = fd;
            }

            FD_SET(fd, &set);
        #else
            if (j < FD_SETSIZE)
            {                
                FD_SET(sptr->get_handle(), &set);
            }
        #endif
        }
    }

    int rc = -1;

    if (to.is_special()) // infinite timeout
    {
        rc = ::select(fd_max + 1, &set, NULL, NULL, NULL);
    }
    else
    {
        // prepare timeout value
        struct timeval tv;
        tv.tv_sec = long(to.total_milliseconds() / MILLI);
        tv.tv_usec = (to.total_milliseconds() % MILLI) * MILLI;

        rc = ::select(fd_max + 1, &set, NULL, NULL, &tv);
    }

    return (rc > 0);
}

errcode_t discover_internet_gateways(igd_list_t& igd_list, timeout_t to)
{
    errcode_t err = err_none;
    igd_list_t tmp_list;

    do 
    {
        std::vector<sock_addr> ifs;
        get_multicast_inerfaces(ifs);

        if (ifs.empty())
        {
            err = err_unexpected;
            break;
        }

        sock_addr group_addr;
        group_addr.set_port(1900);
        group_addr.set_address("239.255.255.250");

        std::vector<udp_socket::sock_ptr> sockets;

        for (size_t j = 0; j < ifs.size(); j++)
        {
            sock_addr local_addr;

            udp_socket::sock_ptr sptr = udp_socket::sock_ptr(new udp_socket);

            if (sptr->bind(local_addr) == err_none)
            {
                sptr->set_blocking_mode(false);
                sptr->set_ttl(1);
                sptr->set_multicast_if(ifs[j]);
                sptr->add_membership(group_addr, ifs[j]); // ?

                sockets.push_back(sptr);
            }
        }

        if (sockets.empty())
        {
            err = err_error;
            break;
        }

        const std::string req =
            "M-SEARCH * HTTP/1.1\r\n"
            "HOST: 239.255.255.250:1900\r\n"
            "MAN: \"ssdp:discover\"\r\n"
            "MX: 2\r\n"
            "ST: urn:schemas-upnp-org:device:InternetGatewayDevice:1\r\n"
            "\r\n";

        for (size_t j = 0; j < sockets.size(); j++)
        {
            udp_socket::sock_ptr sptr = sockets[j];
            sptr->send_datagram(&req[0], req.size(), group_addr);
        }

        // wait for results
        //
        std::string buf(4096, '\0');
        const timeout_t wait_timeout = pt::milliseconds(100);
        pt::ptime start = pt::microsec_clock::local_time();

        for ( ; ; )
        {
            bool readable = wait_for_readable(sockets, wait_timeout);
            pt::ptime now = pt::microsec_clock::local_time();

            if (!readable)
            {
                if ((now - start) > to)
                {
                    break; // timeout expired
                }

                continue;
            }

            udp_socket::sock_ptr sptr;

            sock_addr local;
            for (size_t j = 0; j < sockets.size(); j++)
            {
                sptr = sockets[j];

                if (sptr->is_readable())
                {
                    if (j < ifs.size())
                        local = ifs[j];
                        
                    break;
                }
            }

            if (!sptr)
            {
                continue;
            }

            sock_addr from;
            size_t resp_size = 0;
            errcode_t rc = sptr->receive_datagram(&buf[0],
                buf.size(), resp_size, from);

            if ((rc != err_none) || (resp_size == 0))
            {
                continue;
            }

            // parse received info
            //

            //HTTP/1.1 200 OK
            //CACHE-CONTROL: max-age=1800
            //DATE: Tue, 03 Feb 2015 13:20:07 GMT
            //EXT:
            //LOCATION: http://192.168.0.1:49153/gatedesc.xml
            //SERVER: Linux/2.6.21, UPnP/1.0, Portable SDK for UPnP devices/1.6.0
            //X-User-Agent: redsonic
            //ST: urn:schemas-upnp-org:device:InternetGatewayDevice:1
            //USN: uuid:74982a48-8b67-4a8d-9e5c-4f0af65d831f::urn:schemas-upnp-org:device:InternetGatewayDevice:1

            std::string status_line;
            http_headers headers;
            size_t body_offs = 0;
            const std::string msg(buf.begin(), buf.begin() + resp_size);

            bool ok = parse_http_message(msg, status_line, headers, body_offs);

            if (!ok || headers.empty())
            {
                continue;
            }

            std::string protocol;
            std::string version;
            std::string text;
            unsigned int status_code = 0;

            ok = parse_status_line(status_line, protocol,
                version, status_code, text);

            if ((status_code != 200) || (protocol != "HTTP"))
            {
                continue;
            }

            igd_info_t info;
            info.device_addr = from;
            info.local_addr = local;
            bool ig_device = false;

            BOOST_FOREACH(const header_field& x, headers)
            {
                try
                {
                    if (boost::iequals(x.name, "ST"))
                    {
                        if (boost::iequals(x.value,
                            "urn:schemas-upnp-org:device:InternetGatewayDevice:1"))
                        {
                            ig_device = true;
                        }
                    }
                    else if (boost::iequals(x.name, "LOCATION"))
                    {
                        info.descr_url = x.value;
                    }
                    else if (boost::iequals(x.name, "USN"))
                    {
                        info.usn = x.value;
                    }
                }
                catch (...)
                {
                }
            }

            if (ig_device)
            {
                bool new_device = true;

                for (size_t k = 0; k < tmp_list.size(); k++)
                {
                    if (tmp_list[k].usn == info.usn)
                    {
                        new_device = false;
                        break;
                    }
                }
                if (new_device)
                {
                    tmp_list.push_back(info);
                }
            }

            if ((now - start) > to)
            {
                break;
            }
        }
    }
    while (0);

    if (err == err_none)
    {
        igd_list.swap(tmp_list);
        
        //get device description for each found gateway
        for (size_t i=0; i<igd_list.size(); i++)
        igd_list[i].descr_processing_result = get_igd_description(igd_list[i].descr_url, igd_list[i].gateway_descr);
    }

    return err;
}

static pugi::xml_node get_device_from_list_by_type(pugi::xml_node& parent, const char* type_str)
{
    pugi::xml_node result;
    pugi::xml_node devicelist_node = parent.child("deviceList");
    if (devicelist_node != NULL)
    {
        pugi::xml_node device_node = devicelist_node.first_child();
        while (device_node != NULL)
        {
            if (boost::iequals(device_node.name(), "device"))
            {
                std::string str;
                if (get_node_value(device_node, "deviceType", str))
                {
                    if (boost::iequals(str, type_str))
                    {
                        result = device_node;
                        break;
                    }
                }
            }
            device_node = device_node.next_sibling();
        }
        
    }
    return result;
}

static pugi::xml_node get_wan_service(pugi::xml_node wan_con_device)
{
    pugi::xml_node result;
    pugi::xml_node servicelist_node = wan_con_device.child("serviceList");
    if (servicelist_node != NULL)
    {
        pugi::xml_node service_node = servicelist_node.first_child();
        while (service_node != NULL)
        {
            if (boost::iequals(service_node.name(), "service"))
            {
                std::string str;
                if (get_node_value(service_node, "serviceType", str))
                {
                    if (boost::iequals(str, "urn:schemas-upnp-org:service:WANIPConnection:1") ||
                        boost::iequals(str, "urn:schemas-upnp-org:service:WANPPPConnection:1") )
                    {
                        result = service_node;
                        break;
                    }
                }
            }
            service_node = service_node.next_sibling();
        }
        
    }
    return result;
}

errcode_t get_igd_description(std::string& desc_url, igd_desc_t& igd_desc)
{
    errcode_t res = err_error;

    std::string desc_xml;
    errcode_t err = get_description_xml(desc_url, desc_xml);

    if (err == err_none)
    {
        pugi::xml_document doc;
        if (doc.load_buffer(desc_xml.c_str(), desc_xml.length()).status == pugi::status_ok)
        {
            pugi::xml_node root_element = doc.first_child();
            if (root_element != NULL)
            {
                //base url
                std::string url_base;
                if (!get_node_value(root_element, "URLBase", url_base))
                {
                    std::string::size_type pos = desc_url.find("/", 7);
                    if (pos != std::string::npos )
                        url_base.assign(desc_url, 0, pos);
                }
                
                if (url_base.size() > 0)
                {
                    //remove last / (if present) from base url
                    if (url_base[url_base.size() - 1] == '/')
                        url_base = url_base.substr(0, url_base.size() - 1);
                    
                    pugi::xml_node device_node = root_element.child("device");
                    
                    if (device_node != NULL)
                    {
                        if (!get_node_value(device_node, "friendlyName", igd_desc.friendly_name))
                            igd_desc.friendly_name = "unknown";
                        
                        if (!get_node_value(device_node, "UDN", igd_desc.udn))
                            igd_desc.udn = "unknown";

                        pugi::xml_node wan_device_node = get_device_from_list_by_type(device_node, "urn:schemas-upnp-org:device:WANDevice:1");
                        if (wan_device_node != NULL)
                        {
                            pugi::xml_node wan_con_device_node = get_device_from_list_by_type(wan_device_node, "urn:schemas-upnp-org:device:WANConnectionDevice:1");
                            if (wan_con_device_node != NULL)
                            {
                                pugi::xml_node service_node = get_wan_service(wan_con_device_node);
                                if (service_node != NULL)
                                {
                                    if (get_node_value(service_node, "serviceType", igd_desc.service_type) &&
                                        get_node_value(service_node, "controlURL", igd_desc.service_url) )
                                    {
                                        //make complete service url
                                        EDL_NET_PROTOCOLS proto = network_helper::get_proto(igd_desc.service_url.c_str());
                                        if (proto == DL_NET_PROTO_UNKNOWN)
                                            igd_desc.service_url = url_base + igd_desc.service_url;
                                            
                                        res = err_none;
                                    }
                                } 
                                else
                                {
                                    res = (errcode_t)2;
                                    //no service node
                                }
                            } 
                            else
                            {
                                res = (errcode_t)3;
                                // no wan con device
                            }
                        } 
                        else
                        {
                            res = (errcode_t)4;
                            //no wan device
                        }
                    }
                    else
                    {
                        res = (errcode_t)5;
                        //no root device node
                    }
                }
                else
                {
                    res = (errcode_t)6;
                    //base url cannot be found/assigned;
                }
            }
            else
            {
                res = (errcode_t)7;
                //log_error(L"SAT2IP - get_server_props_from_description_xml: root node is not found");
            }
        }
        else
        {
            res = (errcode_t)8;
            //log_error(L"SAT2IP - get_server_props_from_description_xml: unable to parse description xml");
        }
    }
    else
    {
        res = (errcode_t)9;
        //get_description_xml failed;
    }

    return res;
}

static void fill_soap_envelope(const std::string& action, const std::string& params, const std::string& service_type, std::string& envelope)
{
    //form SOAP envelope for request
    std::stringstream soap_data;
	soap_data.clear(); soap_data.str("");
    soap_data << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
    soap_data << "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\" ";
    soap_data << "s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\r\n";
    soap_data << "<s:Body>\r\n";
    soap_data << "<u:" << action << " xmlns:u=\"" << service_type << "\">\r\n";
    soap_data << params;
    soap_data << "</u:" << action << ">\r\n";
    soap_data << "</s:Body>\r\n";
    soap_data << "</s:Envelope>\r\n";
    
    envelope = soap_data.str();
}

static errcode_t check_envelope_return(const std::string& action, const std::string& ret_envelope)
{
    errcode_t ret_val = err_error;
    pugi::xml_document doc;
    if (doc.load_buffer(ret_envelope.c_str(), ret_envelope.length()).status == pugi::status_ok)
    {
        pugi::xml_node root_element = doc.first_child();
        if (root_element != NULL)
        {
            //body element
            pugi::xml_node body_node = root_element.child("Body");
            
            if (body_node != NULL)
            {
                std::string tag = action + "Response";
                pugi::xml_node value_node = body_node.child(tag.c_str());
                if (value_node != NULL)
                    ret_val = err_none;
            }
        }
    }
    return ret_val;
}

static errcode_t send_soap_envelope(const std::string& url, const std::string& action, const std::string& service_type, const std::string& envelope,
    std::string& raw_response)
{
    errcode_t res = err_error;

    raw_response.clear();
    
    //send request
    std::string wurl;
    std::string waddr;
    std::string wuser;
    std::string wpass;
    std::string wsuff;
    unsigned short port = 0;

    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(url.c_str(), waddr, wuser, wpass, port, wsuff);

    http_comm_handler http_reader(PRODUCT_NAME_UTF8, waddr.c_str(), wuser.c_str(), wpass.c_str(), port);

    if (http_reader.Init())
    {
        http_comm_handler::http_headers_t headers;
        std::stringstream header_data;
        header_data << "SOAPACTION:\"" << service_type << "#" << action << "\"";
        headers.push_back(header_data.str());

        std::string str = "CONTENT-TYPE: text/xml ; charset=\"utf-8\"";
        headers.push_back(str);
        
        std::string response;
        bool ok = http_reader.ExecutePostWithResponse(wsuff.c_str(), envelope.c_str(), proto == DL_NET_PROTO_HTTPS, response, &headers);
        raw_response = response;
        
        if (ok)
            res = check_envelope_return(action, response);

        http_reader.Term();
    }
    
    return res;
}

errcode_t add_port_mapping(igd_info_t& igd_info, port_mapping_desc_t& port_mapping, std::string& raw_response)
{
    //get local address for port mapping
    std::string address_str;
    igd_info.local_addr.get_address(address_str);
    
    //action 
    std::string action = "AddPortMapping";

    //request data
    std::stringstream rec_data;
	rec_data.clear(); rec_data.str("");
    rec_data << "<NewRemoteHost></NewRemoteHost>\r\n";
    rec_data << "<NewExternalPort>" << port_mapping.ext_port << "</NewExternalPort>\r\n";
    rec_data << "<NewProtocol>" << port_mapping.protocol << "</NewProtocol>\r\n";
    rec_data << "<NewInternalPort>" << port_mapping.int_port << "</NewInternalPort>\r\n";
    rec_data << "<NewInternalClient>" << address_str << "</NewInternalClient>\r\n";
    rec_data << "<NewEnabled>1</NewEnabled>\r\n";
    rec_data << "<NewPortMappingDescription>" << port_mapping.description << "</NewPortMappingDescription>\r\n";
    rec_data << "<NewLeaseDuration>0</NewLeaseDuration>\r\n";
    
    //form SOAP envelope for request
    std::string envelope;
    fill_soap_envelope(action, rec_data.str(), igd_info.gateway_descr.service_type, envelope);

    //send request
    return send_soap_envelope(igd_info.gateway_descr.service_url, action, igd_info.gateway_descr.service_type, envelope, raw_response);
}

errcode_t remove_port_mapping(igd_info_t& igd_info, port_mapping_desc_t& port_mapping, std::string& raw_response)
{
    //get local address for port mapping
    std::string address_str;
    igd_info.local_addr.get_address(address_str);
    
    //action 
    std::string action = "DeletePortMapping";

    //request data
    std::stringstream rec_data;
	rec_data.clear(); rec_data.str("");
    rec_data << "<NewRemoteHost></NewRemoteHost>\r\n";
    rec_data << "<NewExternalPort>" << port_mapping.ext_port << "</NewExternalPort>\r\n";
    rec_data << "<NewProtocol>" << port_mapping.protocol << "</NewProtocol>\r\n";
    
    //form SOAP envelope for request
    std::string envelope;
    fill_soap_envelope(action, rec_data.str(), igd_info.gateway_descr.service_type, envelope);

    //send request
    return send_soap_envelope(igd_info.gateway_descr.service_url, action, igd_info.gateway_descr.service_type, envelope, raw_response);
}

} // namespace streaming
} // namespace dvblink
