/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <algorithm>
#include <dl_logger.h>
#include "m3u8segmentlist.h"

using namespace dvblink::logging;

m3u8_segment_list::m3u8_segment_list()
{
}

m3u8_segment_list::~m3u8_segment_list()
{
}

void m3u8_segment_list::AddSegments(m3u8_media_segment_list& segments)
{
	using namespace std;

    boost::unique_lock<boost::shared_mutex> lock(m_Lock);
	if (segments.size() > 0)
	{
		int start_segment = 0;
		//if this is live tv (e.g. no last segment) and there were no segments yet - take no more that three latest segments  
		if (!segments.back().last && m_SegmentList.size()  == 0)
		{
			start_segment = max(0, (int)segments.size() - 3);
			log_info(L"m3u8_segment_list::AddSegments. Start segment index %1% out of %2% segments") % start_segment % segments.size();
		}
		int count = 0;
		m3u8_media_segment_list::iterator it = segments.begin();
		while (it != segments.end())
		{
			if (count >= start_segment)
				CheckAndAddSegment(*it);

			it++;
			count += 1; 
		}
		//re-sort the list
		SortSegmentList();
	}
}

bool m3u8_segment_list::PopFirstPlayableSegment(m3u8_media_segment_ex& segment)
{
	bool ret_val = false;
    boost::unique_lock<boost::shared_mutex> lock(m_Lock);

	if (m_SegmentList.size() == 0)
		return false;

	int maxnum;
	GetMaxSeqNum(maxnum);

	bool bEndPresent = maxnum == -1;

	if ((bEndPresent && m_SegmentList.size() > 0) ||
		(!bEndPresent && m_SegmentList.size() > 1))
	{
		segment = m_SegmentList.front();
		m_SegmentList.pop_front();

		log_info(L"m3u8_segment_list::PopFirstPlayableSegment. Popped segment %d") % segment.sequenceBaseNum;

		ret_val = true;
	}

	return ret_val;
}

void m3u8_segment_list::Clear()
{
    boost::unique_lock<boost::shared_mutex> lock(m_Lock);
	m_SegmentList.clear();
}

void m3u8_segment_list::SortSegmentList()
{
	m_SegmentList.sort();
}

void m3u8_segment_list::CheckAndAddSegment(m3u8_media_segment_ex& segment)
{
	if (segment.last)
		segment.sequenceBaseNum = M3U8_LAST_SEGMENT_ID;

	int maxnum;
	GetMaxSeqNum(maxnum);

	if (m_SegmentList.size() == 0 ||
		(m_SegmentList.size() > 0 && maxnum != -1 && segment.sequenceBaseNum > maxnum))
	{
		m_SegmentList.push_back(segment);

		log_info(L"m3u8_segment_list::CheckAndAddSegment. Added segment %d") % segment.sequenceBaseNum;
	} else
	{
		/* Removed as impossible according to spec)
	    if (abs(segment.sequenceBaseNum - maxnum) > 16)
	    {
	        m_SegmentList.clear();
		    m_SegmentList.push_back(segment);
		    log_warning(L"m3u8_segment_list::CheckAndAddSegment. Sequence number wrap around? Added segment %d") % segment.sequenceBaseNum;
        }
		*/
	}
}

bool m3u8_segment_list::GetMaxSeqNum(int& maxnum)
{
    using namespace std;
	maxnum = -2;

	m3u8_media_segment_list::iterator it = m_SegmentList.begin();
	while (it != m_SegmentList.end())
	{
		if (it->last)
		{
			maxnum = -1;
			break;
		}

        maxnum = max(it->sequenceBaseNum, maxnum);
		it++;
	}
	return maxnum != -2;
}
