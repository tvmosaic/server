cmake_minimum_required(VERSION 3.0.0)

project(dvblex_streaming_lib)

include_directories(${TVMOSAIC_ANKER_DIR}/../include/dvblex_streaming_lib)
include_directories(${TVMOSAIC_ANKER_DIR}/../include/dvblex_net_lib)

file(GLOB SOURCES "*.cpp")

add_library(${PROJECT_NAME} STATIC ${SOURCES})

