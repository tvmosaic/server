/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <map>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <dl_http_comm.curl.h>
#include <dl_strings.h>
#include <dl_network_helper.h>
#include "m3u8parser.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace boost::algorithm;

namespace {    

typedef std::vector<std::string> param_list_t;

class name2tag
{
public:
    name2tag()
    {
        name2tag_map_[M3U8_EXTM3U_TAG] = tag_extm3u;
        name2tag_map_[M3U8_EXTINF_TAG] = tag_extinf;
        name2tag_map_[M3U8_TARGET_DURATION_TAG] = tag_ext_x_targetduration;
        name2tag_map_[M3U8_MEDIA_SEQUENCE_TAG] = tag_ext_x_media_sequence;
        name2tag_map_[M3U8_EXT_KEY_TAG] = tag_ext_x_key;
        name2tag_map_[M3U8_EXT_STREAM_INF_TAG] = tag_ext_x_stream_inf;
        name2tag_map_[M3U8_EXT_VERSION_TAG] = tag_ext_x_version;
        name2tag_map_[M3U8_EXT_END_LIST_TAG] = tag_ext_x_endlist;
    }

    m3u8_tag operator ()(const std::string& name) const
    {
        std::map<std::string, m3u8_tag>::const_iterator it =
            name2tag_map_.find(name);

        if (it != name2tag_map_.end())
        {
            return it->second;
        }

        return tag_unknown;
    }

private:
    std::map<std::string, m3u8_tag> name2tag_map_;
};

static bool _is_comma(char c)
{
	return (c == ',');
}

bool split_tag_line(const std::string& tag_line,
    m3u8_tag& tag, param_list_t& param_list)
{
    static const name2tag _tags;    
    param_list_t t;
    param_list.swap(t);
    tag = tag_unknown;

    if ((tag_line.size() <= 1) || (tag_line[0] != '#'))
    {
        return false;
    }

    size_t pos = tag_line.find(':');

    if (pos == std::string::npos)
    {
        tag = _tags(tag_line);
        return ((tag == tag_unknown) ? false : true);
    }
    else if (pos <= 1)
    {
        return false;
    }

    std::string tag_name(tag_line, 0, pos);
    tag = _tags(tag_name);

    if (tag == tag_unknown)
    {
        return false;
    }

    std::string params(tag_line, pos + 1);
    boost::algorithm::split(param_list, params, _is_comma, token_compress_off);

    return true;
}

bool parse_attr(const std::string& attr, std::string& name, std::string& value)
{
    if (attr.empty())
    {
        return false;
    }

    size_t pos = attr.find('=');

    if ((pos == std::string::npos) || (pos == 0))
    {
        return false;
    }

    name.assign(attr, 0, pos);
    boost::trim(name);

    value.assign(attr.c_str() + pos + 1);
    boost::trim(value);

    return true;
}

bool find_attr(param_list_t params, const std::string& name, std::string& value)
{
    if (params.empty() || name.empty())
    {
        return false;
    }

    BOOST_FOREACH(std::string& attr, params)
    {
        std::string _name;
        std::string _value;
        bool ok = parse_attr(attr, _name, _value);

        if (ok && (_name == name))
        {
            value = _value;
            return true;
        }
    }

    return false; // not found
}

void to_int(const std::string& s, int& n)
{
    try
    {
        n = boost::lexical_cast<int>(s);
    }
    catch (...)
    {
    }
}

bool get_key(const std::string& key_uri, std::string& key, const char* user_agent)
{
    if (key_uri.empty())
    {
        return false;
    }

    std::string address;
    std::string user;
    std::string password;
    std::string suffix;
    unsigned short port = 0;

    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(key_uri.c_str(), address, user, password, port, suffix);

    http_comm_handler http_reader(user_agent,
        address.c_str(), user.c_str(), password.c_str(), port);

    bool ok = http_reader.Init();
    
    if (!ok)
    {
        return false;
    }

    std::string response;
    
    ok = http_reader.ExecuteGetWithResponse(suffix.c_str(),
        proto == DL_NET_PROTO_HTTPS, response);

    http_reader.Term();

    if (!ok)
    {
        return false;
    }

    key.swap(response);
    return true;
}

void make_iv(unsigned int segment_no, std::string& iv)
{
    iv.assign(16, 0);
    iv.resize(16, 0);

    for (size_t j = 0, i = 15; j < sizeof(segment_no); j++, i--)
    {
        iv[i] = char(segment_no & 0xFF);
        segment_no >>= 8;
    }
}

}; // namespace

std::string hls_make_full_url(const std::string& root_url, std::string& suffix)
{
    std::string ret_val;

	std::string address;
	std::string user;
	std::string password;
	unsigned short portNum;
	std::string urlSuffix;
	//check whether url is already a full url
	if (network_helper::parse_net_url(suffix.c_str(), address, user, password, portNum, urlSuffix) == DL_NET_PROTO_UNKNOWN)
	{
        if (suffix[0] == '/')
        {
            std::string abs_root = root_url;
            //find first / after the address
            size_t start_pos = 0;
            size_t header_pos = root_url.find(url_header_separator);
            if (header_pos != std::string::npos)
                start_pos = header_pos + url_header_separator.size();

            size_t pos = root_url.find('/', start_pos);
            if (pos != std::string::npos)
                abs_root = root_url.substr(0, pos);

            ret_val = abs_root + suffix;
        } else
        {
            ret_val = root_url + "/" + suffix;
        }
    } else
    {
        ret_val = suffix;
    }

    return ret_val;
}

void make_iv_from_hex_string(const std::string& iv_hexstr, std::string& iv)
{
    iv.assign(16, 0);
    iv.resize(16, 0);

    std::string val = iv_hexstr;

    //decode hex representation of the vector: 0x12345467...
    if (boost::istarts_with(val, "0x"))
        val.erase(0, 2);

    for (size_t j = 0, i = 0; j < val.size()/2 && i < iv.size(); j++, i++)
    {
        iv[i] = network_helper::decode_hex_to_char(&val[j * 2]);
    }

}

bool m3u8_parse(const std::string& root_url, const char* contents, m3u8_media_segments& segments,
    m3u8_playlist_list& playlists, const char* user_agent)
{
	//split contents string into separate lines
	std::vector<std::string> lines;
	split(lines, contents, is_any_of("\r\n"), token_compress_on);

	//the first line must be #EXTM3U
	if (lines.empty() || (lines[0] != M3U8_EXTM3U_TAG))
    {
        return false;
    }
        
	segments.sequenceBaseNum = 1;
	segments.targetDuration = -1;

	bool bParsePlaylistUrl = false;
	bool bParseSegmentUrl = false;
    
    bool encrypted = false;
    std::string key_uri;
    std::string aes_iv;
    std::string aes_key;

	m3u8_media_segment segment;
	m3u8_play_list playlist;
	
    BOOST_FOREACH(std::string& line, lines)
	{
        boost::trim(line);

		if (line.empty())
        {
			continue; // skip empty lines
        }

		if (line[0] == '#')
		{
			//either comment or tag
            if (line.compare(1, 3, "EXT") != 0)
            {
                continue; // skip comment
            }

            m3u8_tag tag = tag_unknown;
            param_list_t params;
            bool ok = split_tag_line(line, tag, params);

            if (!ok || (tag == tag_unknown))
            {
                continue;
            }

            switch (tag)
            {
            case tag_extm3u:
                break;

            case tag_extinf:
                // streaming url
                segment.reset();

                try
                {
                    size_t nparams = params.size();

                    if (nparams > 0)
                    {
                        if (!params[0].empty())
                        {
                            double d = boost::lexical_cast<double>(params[0]);
                            segment.duration = (unsigned long)(d * 1000);
                        }

                        if (nparams > 1)
                        {
                            segment.title = params[1];
                        }
                    }
                }
                catch (...)
                {
                }

                bParseSegmentUrl = true;
                break;

            case tag_ext_x_targetduration:
                // target duration
                if (params.size() == 1)
                {
                    to_int(params[0], segments.targetDuration);
                }
                break;

            case tag_ext_x_media_sequence:
                // media sequence number
                if (params.size() == 1)
                {
                    to_int(params[0], segments.sequenceBaseNum);
                }
                break;

            case tag_ext_x_version:
                // version of the playlist file
                break;

            case tag_ext_x_key:
                {
                    encrypted = false;
                    std::string val;
                    bool ok = find_attr(params, "METHOD", val);

                    if (ok && (val == "AES-128"))
                    {
                        ok = find_attr(params, "URI", val);

                        if (ok && (val.size() > 2))
                        {
                            encrypted = true;
                            key_uri.assign(val.c_str(), 1, val.size() - 2);
                            key_uri = hls_make_full_url(root_url, key_uri);

                            get_key(key_uri, aes_key, user_agent);

                            ok = find_attr(params, "IV", val);

                            if (ok && val.size() > 0)
                            {
                                make_iv_from_hex_string(val, aes_iv);
                            }
                            else
                            {
                                aes_iv.clear();
                            }
                        }
                    }
                }
                break;

            case tag_ext_x_stream_inf:
                {
                    // playlist url
                    playlist.reset();
                    std::string val;
                    bool ok = find_attr(params, M3U8_SI_PROGRAM_ID, val);

                    if (ok)
                    {
                        to_int(val, playlist.programId);
                    }

                    ok = find_attr(params, M3U8_SI_BANDWIDTH, val);

                    if (ok)
                    {
                        to_int(val, playlist.bandwidth);
                    }

                    ok = find_attr(params, M3U8_SI_CODECS, val);

                    if (ok)
                    {
                        playlist.codecs = val;
                    }

                    ok = find_attr(params, M3U8_SI_RESOLUTION, val);

                    if (ok)
                    {
                        playlist.resolution = val;
                    }

                    bParsePlaylistUrl = true;
                }
                break;

            case tag_ext_x_endlist:
                //end list tag
                segment.reset();
                segment.last = true;
                segments.segments.push_back(segment);
                break;

            default:
                continue;
            }
		}
        else
		{
			//uri
			if (bParseSegmentUrl)
			{
				segment.url = line;

                if (encrypted)
                {
                    segment.encrypted = true;
                    segment.aes_iv = aes_iv;
                    segment.aes_key = aes_key;

                    if (segment.aes_iv.empty())
                    {
                        unsigned int segment_no =
                            (unsigned int)(segments.sequenceBaseNum +
                            segments.segments.size());
                        make_iv(segment_no, segment.aes_iv);
                    }
                }

				segments.segments.push_back(segment);
				bParseSegmentUrl = false;
			}
			
            if (bParsePlaylistUrl)
			{
				playlist.url = line;
				playlists.push_back(playlist);
				bParsePlaylistUrl = false;
			}
		}
	}
	
    return true;
}

