/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <dl_strings.h>
#include "sdp.h"

using namespace dvblink::engine;

namespace dvblink {
namespace streaming {

// An SDP session description consists of a number of pairs of text of
// the form <type>=<value> <type> is always exactly one character and is
// case-significant. <value> is a structured text string whose format
// depends on <type>. It also will be case-significant unless a
// specific field defines otherwise.  Whitespace is not permitted either
// side of the `=' sign. In general <value> is either a number of fields
// delimited by a single space character or a free format string.
//
bool parse_sdp(const std::string& sdp, sdp_pairs_t& pairs)
{
    if (sdp.empty() || (sdp.find('\0') != std::string::npos))
    {
        return false;
    }

    bool success = true;
    sdp_pairs_t tmp_pairs;
    
    size_t offs = 0;
    const size_t sdp_len = sdp.size();
    
    while (offs < sdp_len)
    {
        size_t i = sdp.find('\n', offs);

        if (i == std::string::npos)
        {
            //last line without cr\lf
            i = sdp_len - 1;
        }

        size_t line_len = i - offs;

        if (line_len < 2)
        {
            success = false;
            break;
        }

        char t = sdp[offs];
        
        bool valid_char =
            ((t >= 'a') && (t <= 'z')) ||
            ((t >= 'A') && (t <= 'Z'));
        
        if (!valid_char || (sdp[offs + 1] != '='))
        {
            success = false;
            break;
        }

        sdp_pair sp;
        sp.type = t;
        sp.value = std::string(sdp.begin() + offs + 2, sdp.begin() + i);
        boost::trim_right(sp.value);

        if (!sp.value.empty() && (sp.value[0] == ' '))
        {
            success = false;
            break;
        }
        
        tmp_pairs.push_back(sp);
        offs = i + 1;
    }

    pairs.swap(tmp_pairs);

    if (success)
    {        
        return !pairs.empty();
    }

    if (!pairs.empty())
    {
        return (sdp.find_first_not_of("\r\n", offs) == std::string::npos);
    }

    return false;
}

static void get_all_frontend_strings(std::string& instr, std::vector<std::string>& frontend_strings)
{
    std::string outstr;
    for (size_t i=0; i<instr.size(); i++)
    {
        if (ispunct(instr[i]) == 0 && isspace(instr[i]) == 0)
        {
            outstr += instr[i];
        } else
        {
            //reached punctuation character. skip it.
            if (outstr.size() > 0)
            {
                frontend_strings.push_back(outstr);
                outstr.clear();
            }
        }
    }
    //any remains
    if (outstr.size() > 0)
        frontend_strings.push_back(outstr);
}

bool get_frontends_number(const sdp_pairs_t& sdp_pairs,
    unsigned int& frontends)
{
    frontends = 0;

    if (sdp_pairs.empty())
    {
        return false;
    }

    bool found = false;

    BOOST_FOREACH(const sdp_pair& x, sdp_pairs)
    {
        if (x.type == 's')
        {
            //SatIPServer:1 4
            if (x.value.compare(0, 12, "SatIPServer:") == 0)
            {
                size_t offs = x.value.find(' ', 12);

                if (offs != std::string::npos)
                {
                    std::string s(x.value.begin() + offs, x.value.end());
                    boost::trim(s);

                    if (!s.empty())
                    {
                        //frontend number from different manufacturers can come as n or n,m or n:m etc.
                        //get only the first part
                        std::vector<std::string> frontend_str;
                        get_all_frontend_strings(s, frontend_str);
                        for (size_t i=0; i<frontend_str.size(); i++)
                        {
                            try
                            {
                                frontends += boost::lexical_cast<unsigned int>(frontend_str[i]);
                            }
                            catch (boost::bad_lexical_cast&)
                            {
                            }
                        }
                        if (frontends > 0)
                        {
                            found = true;
                            break;
                        }
                    }
                }
            }
        }
    }

    return found;
}

bool get_signal_params(const sdp_pairs_t& sdp_pairs, bool& lock, int& quality, int& level)
{
    lock = false;
    quality = 0;
    level = 0;

    if (sdp_pairs.empty())
    {
        return false;
    }

    bool found = false;

    BOOST_FOREACH(const sdp_pair& x, sdp_pairs)
    {
        if (x.type == 'a')
        {
            //fmtp:33 ver=1.0;src=1;tuner=1,0,0,0,10744,h,dvbs2,,off,,-858993460,34;pids=0
            std::string tuner_tag = "tuner=";
            size_t pos = x.value.find(tuner_tag);
            if (pos != std::string::npos)
            {
                std::string tuner_str = x.value.substr(pos + tuner_tag.size());
                //search for closing ;
                pos = tuner_str.find(";");
                if (pos != std::string::npos)
                    tuner_str.resize(pos);
                //split it into tokens
                typedef boost::tokenizer<boost::char_separator<char>, std::string::const_iterator, std::string> tokenizer_t;
                boost::char_separator<char> sep(",");
                tokenizer_t tokens(tuner_str, sep);

                bool b_complete = false;
                int token_count = 0;
                for (tokenizer_t::iterator tok_iter = tokens.begin(); tok_iter != tokens.end() && !b_complete; ++tok_iter)
                {
                    switch (token_count)
                    {
                    case 1:
                        {
                            string_conv::apply<int, int, char>((*tok_iter).c_str(), level, 0);
                            level = level * 100 / 255;
                        }
                        break;
                    case 2:
                        {
                            lock = boost::iequals(*tok_iter, "1");
                        }
                        break;
                    case 3:
                        {
                            string_conv::apply<int, int, char>((*tok_iter).c_str(), quality, 0);
                            quality = quality * 100 / 15;
                            //end sequence - we found all values
                            b_complete = true;
                        }
                        break;
                    }
                    ++token_count;
                }
                found = b_complete;
            }
        }
        if (found)
            break;
    }

    return found;
}

} // namespace streaming
} // namespace dvblink

// $Id: sdp.cpp 8994 2013-09-02 14:46:26Z pashab $
