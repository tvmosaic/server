/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include "rtsp_client.h"
#include "string_utils.h"
#include "http_utils.h"

namespace dvblink {
namespace streaming {

namespace pt = boost::posix_time;

rtsp_client::rtsp_client(const std::string& user_agent) :
    user_agent_(user_agent), cseq_(0), session_timeout_(0),
    buffer_(2048, '\0'), data_size_(0)
{
    boost::trim(user_agent_);
}

rtsp_client::~rtsp_client()
{
}

bool rtsp_client::connect(const std::string& rtsp_server_addr,
    timeout_t connect_timeout, unsigned short rtsp_server_port)
{
    bool success = false;

    do 
    {
        if (rtsp_server_addr.empty())
        {
            break;
        }

        if (rtsp_server_port == 0)
        {
            rtsp_server_port = 554;
        }

        sock_addr remote_addr;
        errcode_t err = remote_addr.set_address(rtsp_server_addr);

        if (err != err_none)
        {
            break;
        }

        remote_addr.set_port(rtsp_server_port);

        if (sock_ptr_)
        {
            disconnect();
        }

        tcp_socket::sock_ptr sock_ptr(new tcp_socket);
        
        sock_ptr->set_blocking_mode(false);
        sock_ptr->set_no_delay(true);
        sock_ptr->set_keep_alive(true);
        
        err = sock_ptr->connect(remote_addr, connect_timeout);

        if (err != err_none)
        {
            break;
        }

        sock_ptr_ = sock_ptr;
        success = true;
    }
    while (0);
    
    return success;
}

bool rtsp_client::disconnect()
{
    if (!sock_ptr_)
    {
        return false;
    }

    sock_ptr_->disconnect(pt::milliseconds(100));
    sock_ptr_.reset();
    
    cseq_ = 0;
    session_id_.clear();

    return true;
}

bool rtsp_client::is_connected() const
{
    return sock_ptr_ ? true : false;
}

bool rtsp_client::prepare_request(const std::string& req, std::string& new_req)
{
    bool success = false;

    do 
    {
        new_req.clear();

        if (req.empty())
        {
            break;
        }

        // parse input request
        //
        std::string start_line;
        http_headers headers;
        size_t body_offs = 0;

        bool ok = parse_http_message(req, start_line, headers, body_offs);

        if (!ok)
        {
            break;
        }

        new_req = start_line;
        new_req += CRLF;

        // add old headers to the request
        //
        if (!headers.empty())
        {
            BOOST_FOREACH(const header_field& x, headers)
            {
                if (boost::iequals(x.name, "CSeq") ||
                    boost::iequals(x.name, "Session") ||
                    boost::iequals(x.name, "User-Agent"))
                {
                    continue;
                }

                new_req += x.name;
                new_req += ": ";
                new_req += x.value;
                new_req += CRLF;
            }
        }

        // add new headers to the request
        //
        new_req += "CSeq: ";
        new_req += boost::lexical_cast<std::string>(cseq_ + 1);
        new_req += CRLF;

        if (!user_agent_.empty())
        {
            new_req += "User-Agent: ";
            new_req += user_agent_;
            new_req += CRLF;
        }

        if (!session_id_.empty())
        {
            new_req += "Session: ";
            new_req += session_id_;
            new_req += CRLF;
        }

        new_req += CRLF;
        success = true;
    }
    while (0);
    
    if (success)
    {
        cseq_++;
    }

    return success;
}

bool rtsp_client::send_request(const std::string& req)
{
    if (req.empty() || !sock_ptr_)
    {
        return false;
    }

    size_t bytes = 0;

    errcode_t err = sock_ptr_->send(&req[0], req.size(),
        bytes, pt::milliseconds(200));

    if (err != err_none)
    {
        disconnect(); // close connection
        return false;
    }

    return true;
}

bool rtsp_client::receive_response(std::string& status_line,
    http_headers& headers, std::string& body,
    timeout_t receive_timeout)
{
    status_line.clear();
    headers.clear();
    body.clear();

    if (!sock_ptr_)
    {
        return false;
    }

    bool success = true;
    const timeout_t wait_timeout = pt::milliseconds(100);
    pt::ptime start = pt::microsec_clock::local_time();
    size_t data_size = 0;
    size_t body_offs = std::string::npos;

    for ( ; ; )
    {
        errcode_t err = sock_ptr_->wait_for_readable(wait_timeout);
        pt::ptime now = pt::microsec_clock::local_time();

        if (err != err_none)
        {
            if ((receive_timeout != infinite_timeout) &&
                ((now - start) > receive_timeout))
            {
                success = false;
                break; // timeout expired
            }

            continue;
        }

        size_t bytes = 0;
        const size_t buf_size = buffer_.size();
        assert(data_size < buf_size);
        
        err = sock_ptr_->receive(&buffer_[0] + data_size,
            buf_size - data_size, bytes);

        if (err != err_none)
        {
            success = false;
            break;
        }

        data_size += bytes;

        if ((buf_size - data_size) <= 128)
        {
            buffer_.resize(buf_size + 1024, '\0');
        }

        // is header received?
        //
        buffer_.resize(data_size);
        body_offs = buffer_.find("\r\n\r\n");
        buffer_.resize(buf_size);

        if (body_offs != std::string::npos)
        {
            body_offs += 4;
            break;
        }
    }

    do 
    {
        if (!success || (data_size <= 4) || (body_offs == std::string::npos))
        {
            success = false;
            break;
        }

        // parse received header
        //
        std::string header(buffer_.begin(), buffer_.begin() + body_offs);
        success = parse_http_message(header, status_line, headers, body_offs);

        if (!success)
        {
            break;
        }

        std::string protocol;
        std::string version;
        std::string text;
        unsigned int status_code = 0;

        success = parse_status_line(status_line, protocol,
            version, status_code, text);

        if (!success)
        {
            break;
        }

        /*
        if (status_code != 200)
        {
            // no body
            success = true;
            break;
        }
        */

        if (session_id_.empty())
        {
            std::string val;
            bool found = find_http_header(headers, "Session", val);

            if (found && !val.empty())
            {
                // ..... ; timeout=60
                size_t i = val.find(';');

                if (i != std::string::npos)
                {
                    session_id_.assign(val.begin(), val.begin() + i);
                    size_t j = val.find('=', i + 1);

                    if (j != std::string::npos)
                    {
                        std::string t(val.begin() + j + 1, val.end());
                        boost::trim(t);

                        try
                        {
                            session_timeout_ = boost::lexical_cast<unsigned>(t);
                        }
                        catch (...)
                        {
                        }
                    }
                }
                else
                {
                    session_id_ = val;
                }

                boost::trim(session_id_);
            }
        }

        size_t content_len = 0;
        std::string val;

        bool has_body = find_http_header(headers, "Content-Length", val);

        if (has_body)
        {
            try
            {
                content_len = boost::lexical_cast<size_t>(val);
            }
            catch (...)
            {
            }

            if (content_len > (1024 * 1024)) // > 1 MB
            {
                success = false;
                break;
            }
        }

        if (content_len > 0)
        {
            size_t cur_body_size = data_size - header.size();

            if (cur_body_size >= content_len)
            {
                body.assign(buffer_.begin() + header.size(),
                    buffer_.begin() + data_size);
            }
            else
            {
                // receive body data
                //
                size_t data_left = content_len - cur_body_size;

                while (buffer_.size() < (data_size + data_left))
                {
                    buffer_.resize(buffer_.size() + 1024, '\0');
                }

                size_t bytes_received = 0;
                timeout_t to = infinite_timeout;

                if (receive_timeout != infinite_timeout)
                {
                    pt::ptime now = pt::microsec_clock::local_time();
                    pt::time_duration dt = now - start;

                    if (dt > receive_timeout)
                    {
                        to = zero_timeout;
                    }
                    else
                    {
                        to = receive_timeout - dt;
                    }
                }                

                errcode_t err = sock_ptr_->receive_max(&buffer_[0] + data_size,
                    data_left, bytes_received, to);

                if (err != err_none)
                {
                    success = false;
                    break;
                }

                std::string::iterator end_copy_ptr = header.size() + content_len > buffer_.size() ? buffer_.end() : buffer_.begin() + header.size() + content_len;
                body.assign(buffer_.begin() + header.size(), end_copy_ptr);
                
                success = true;
            }
        }
    }
    while (0);

    if (!success)
    {
        disconnect();
    }

    data_size_ = data_size;

    return success;
}

void rtsp_client::get_last_received_buffer(std::string& buffer)
{
    buffer.clear();

    buffer.assign(buffer_.begin(), buffer_.begin() + data_size_);
}

} // namespace streaming
} // namespace dvblink

// $Id: rtsp_client.cpp 8593 2013-06-21 10:06:42Z pashab $
