/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include <string>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>
#include "sat-ip.h"
#include "http_utils.h"

#include <dl_strings.h>
#include <dl_network_helper.h>
#include <dl_pugixml_helper.h>

#include <dl_http_comm.curl.h>

namespace dvblink {
namespace streaming {

namespace pt = boost::posix_time;

static bool get_multicast_inerfaces(std::vector<sock_addr>& ifs)
{
    using namespace dvblink::engine;

    ifs.clear();
    TNetworkAdaptersInfo adapters;
    network_helper::get_local_net_adapters(adapters);

    sock_addr sa;

#ifndef _WIN32
    ifs.push_back(sa);  // INADDR_ANY
#endif

    for (size_t j = 0; j < adapters.size(); j++)
    {
        SNetworkAdaptersInfo& t = adapters[j];

        try
        {
            std::string addr = string_cast<EC_UTF8>(t.m_strAddress);
            errcode_t err = sa.set_address(addr);

            if ((err == err_none) && (addr != "127.0.0.1"))
            {
                ifs.push_back(sa);
            }
        }
        catch (...)
        {
        }
    }

    return true;
}

static bool wait_for_readable(const std::vector<udp_socket::sock_ptr>& v,
    dvblink::timeout_t to)
{
    if (v.empty())
    {
        return false;
    }

    fd_set set;
    FD_ZERO(&set);
    int fd_max = 0;

    for (size_t j = 0; j < v.size(); j++)
    {
        udp_socket::sock_ptr sptr = v[j];

        if (sptr)
        {
        #ifndef _WIN32
            basic_socket::handle_t fd = sptr->get_handle();

            if (fd >= FD_SETSIZE)
            {
                continue;
            }

            if (fd > fd_max)
            {
                fd_max = fd;
            }

            FD_SET(fd, &set);
        #else
            if (j < FD_SETSIZE)
            {                
                FD_SET(sptr->get_handle(), &set);
            }
        #endif
        }
    }

    int rc = -1;

    if (to.is_special()) // infinite timeout
    {
        rc = ::select(fd_max + 1, &set, NULL, NULL, NULL);
    }
    else
    {
        // prepare timeout value
        struct timeval tv;
        tv.tv_sec = long(to.total_milliseconds() / MILLI);
        tv.tv_usec = (to.total_milliseconds() % MILLI) * MILLI;

        rc = ::select(fd_max + 1, &set, NULL, NULL, &tv);
    }

    return (rc > 0);
}

errcode_t discover_satip_servers(satip_servers_t& list, timeout_t to,
    sat2ip_new_server_cb_func cb_func, void* user_param)
{
    errcode_t err = err_none;
    satip_servers_t tmp_list;

    do 
    {
        std::vector<sock_addr> ifs;
        get_multicast_inerfaces(ifs);

        if (ifs.empty())
        {
            err = err_unexpected;
            break;
        }

        sock_addr group_addr;
        group_addr.set_port(1900);
        group_addr.set_address("239.255.255.250");

        std::vector<udp_socket::sock_ptr> sockets;

        for (size_t j = 0; j < ifs.size(); j++)
        {
        #if 0 //defined(_WIN32)
            sock_addr local_addr = ifs[j];
            local_addr.set_port(0);
        #else
            sock_addr local_addr;
        #endif

            udp_socket::sock_ptr sptr = udp_socket::sock_ptr(new udp_socket);

            if (sptr->bind(local_addr) == err_none)
            {
                sptr->set_blocking_mode(false);
                sptr->set_ttl(1);
                sptr->set_multicast_if(ifs[j]);
                sptr->add_membership(group_addr, ifs[j]); // ?

                sockets.push_back(sptr);
            }
        }

        if (sockets.empty())
        {
            err = err_error;
            break;
        }

        const std::string req =
            "M-SEARCH * HTTP/1.1\r\n"
            "HOST: 239.255.255.250:1900\r\n"
            "MAN: \"ssdp:discover\"\r\n"
            "MX: 2\r\n"
            "ST: urn:ses-com:device:SatIPServer:1\r\n"
            "\r\n";

        for (size_t j = 0; j < sockets.size(); j++)
        {
            udp_socket::sock_ptr sptr = sockets[j];
            sptr->send_datagram(&req[0], req.size(), group_addr);
        }

        // wait for results
        //
        std::string buf(4096, '\0');
        const timeout_t wait_timeout = pt::milliseconds(100);
        pt::ptime start = pt::microsec_clock::local_time();

        for ( ; ; )
        {
            bool readable = wait_for_readable(sockets, wait_timeout);
            pt::ptime now = pt::microsec_clock::local_time();

            if (!readable)
            {
                if ((now - start) > to)
                {
                    break; // timeout expired
                }

                continue;
            }

            udp_socket::sock_ptr sptr;

            for (size_t j = 0; j < sockets.size(); j++)
            {
                sptr = sockets[j];

                if (sptr->is_readable())
                {
                    break;
                }
            }

            if (!sptr)
            {
                continue;
            }

            sock_addr from;
            size_t resp_size = 0;
            errcode_t rc = sptr->receive_datagram(&buf[0],
                buf.size(), resp_size, from);

            if ((rc != err_none) || (resp_size == 0))
            {
                continue;
            }

            // parse received info
            //

            // HTTP/1.1 200 OK
            // CACHE-CONTROL: max-age=1800
            // DATE: Sat Jan  1 01:30:28 2000
            // EXT:
            // LOCATION: http://10.0.0.9:8080/desc.xml
            // SERVER: Linux/1.0 UPnP/1.1 IDL4K/1.0
            // ST: urn:ses-com:device:SatIPServer:1
            // USN: uuid:ad7bed05-2ae8-44e9-9e48-2a4514e2c22a::urn:ses-com:device:SatIPServer:1
            // BOOTID.UPNP.ORG: 70
            // CONFIGID.UPNP.ORG: 0
            // DEVICEID.SES.COM: 2

            std::string status_line;
            http_headers headers;
            size_t body_offs = 0;
            const std::string msg(buf.begin(), buf.begin() + resp_size);

            bool ok = parse_http_message(msg, status_line, headers, body_offs);

            if (!ok || headers.empty())
            {
                continue;
            }

            std::string protocol;
            std::string version;
            std::string text;
            unsigned int status_code = 0;

            ok = parse_status_line(status_line, protocol,
                version, status_code, text);

            if ((status_code != 200) || (protocol != "HTTP"))
            {
                continue;
            }

            satip_server_info info;
            info.device_addr = from;
            info.device_id = 0;
            info.boot_id = 0;
            info.config_id = 0;
            bool satip_server = false;

            BOOST_FOREACH(const header_field& x, headers)
            {
                try
                {
                    if (boost::iequals(x.name, "ST"))
                    {
                        if (boost::iequals(x.value,
                            "urn:ses-com:device:SatIPServer:1"))
                        {
                            satip_server = true;
                        }
                    }
                    else if (boost::iequals(x.name, "LOCATION"))
                    {
                        info.descr_url = x.value;
                    }
                    else if (boost::iequals(x.name, "USN"))
                    {
                        info.usn = x.value;
                    }
                    else if (boost::iequals(x.name, "DEVICEID.SES.COM"))
                    {
                        info.device_id = boost::lexical_cast<unsigned>(x.value);
                    }
                    else if (boost::iequals(x.name, "BOOTID.UPNP.ORG"))
                    {
                        info.boot_id = boost::lexical_cast<unsigned>(x.value);
                    }
                    else if (boost::iequals(x.name, "CONFIGID.UPNP.ORG"))
                    {
                        info.config_id = boost::lexical_cast<unsigned>(x.value);
                    }
                }
                catch (...)
                {
                }
            }

            if (satip_server)
            {
                bool new_device = true;

                for (size_t k = 0; k < tmp_list.size(); k++)
                {
                    if (boost::iequals(tmp_list[k].usn, info.usn))
                    {
                        new_device = false;
                        break;
                    }
                }

                if (new_device)
                {
                    tmp_list.push_back(info);

                    if (cb_func != NULL)
                    {
                        if (!cb_func(&tmp_list, user_param))
                        {
                            break;
                        }
                    }
                }
            }

            if ((now - start) > to)
            {
                break;
            }
        }
    }
    while (0);

    if (err == err_none)
    {
        list.swap(tmp_list);
    }

    return err;
}

errcode_t discover_satip_servers(dvblink::sock_addr sa, satip_servers_t& list,
    timeout_t to, sat2ip_new_server_cb_func cb_func, void* user_param)
{
    errcode_t err = err_none;

    {
        satip_servers_t empty_list;
        list.swap(empty_list);
    }

    do 
    {
        udp_socket sock;
        sock_addr local_addr;
        
        err = sock.bind(local_addr);

        if (err != err_none)
        {
            break;
        }

        sock.set_blocking_mode(false);
        sock.set_ttl(1);

        sock_addr group_addr;
        group_addr.set_port(1900);
        group_addr.set_address("239.255.255.250");

        sock.set_multicast_if(sa);
        
        // do we really need this?
        sock.add_membership(group_addr, sa);

        const std::string req =
            "M-SEARCH * HTTP/1.1\r\n"
            "HOST: 239.255.255.250:1900\r\n"
            "MAN: \"ssdp:discover\"\r\n"
            "MX: 2\r\n"
            "ST: urn:ses-com:device:SatIPServer:1\r\n"
            "\r\n";

        err = sock.send_datagram(&req[0], req.size(), group_addr);

        if (err != err_none)
        {
            break;
        }

        // wait for results
        //
        std::string buf(4096, '\0');
        const timeout_t wait_timeout = pt::milliseconds(100);
        pt::ptime start = pt::microsec_clock::local_time();

        for ( ; ; )
        {
            err = sock.wait_for_readable(wait_timeout);
            pt::ptime now = pt::microsec_clock::local_time();

            if (err != err_none)
            {
                err = err_none; // clear error

                if ((now - start) > to)
                {
                    break;
                }

                continue;
            }

            sock_addr from;
            size_t resp_size = 0;
            err = sock.receive_datagram(&buf[0], buf.size(), resp_size, from);

            if (err != err_none)
            {
                break;
            }

            if (resp_size == 0)
            {
                continue;
            }

            // parse received info
            //

            // HTTP/1.1 200 OK
            // CACHE-CONTROL: max-age=1800
            // DATE: Sat Jan  1 01:30:28 2000
            // EXT:
            // LOCATION: http://10.0.0.9:8080/desc.xml
            // SERVER: Linux/1.0 UPnP/1.1 IDL4K/1.0
            // ST: urn:ses-com:device:SatIPServer:1
            // USN: uuid:ad7bed05-2ae8-44e9-9e48-2a4514e2c22a::urn:ses-com:device:SatIPServer:1
            // BOOTID.UPNP.ORG: 70
            // CONFIGID.UPNP.ORG: 0
            // DEVICEID.SES.COM: 2

            std::string status_line;
            http_headers headers;
            size_t body_offs = 0;
            const std::string msg(buf.begin(), buf.begin() + resp_size);

            bool ok = parse_http_message(msg, status_line, headers, body_offs);

            if (!ok || headers.empty())
            {
                continue;
            }

            std::string protocol;
            std::string version;
            std::string text;
            unsigned int status_code = 0;

            ok = parse_status_line(status_line, protocol,
                version, status_code, text);

            if ((status_code != 200) || (protocol != "HTTP"))
            {
                continue;
            }

            satip_server_info info;
            info.device_addr = from;
            info.device_id = 0;
            info.boot_id = 0;
            info.config_id = 0;
            bool satip_server = false;
            
            BOOST_FOREACH(const header_field& x, headers)
            {
                try
                {
                    if (boost::iequals(x.name, "ST"))
                    {
                        if (boost::iequals(x.value,
                            "urn:ses-com:device:SatIPServer:1"))
                        {
                            satip_server = true;
                        }
                    }
                    else if (boost::iequals(x.name, "LOCATION"))
                    {
                        info.descr_url = x.value;
                    }
                    else if (boost::iequals(x.name, "USN"))
                    {
                        info.usn = x.value;
                    }
                    else if (boost::iequals(x.name, "DEVICEID.SES.COM"))
                    {
                        info.device_id = boost::lexical_cast<unsigned>(x.value);
                    }
                    else if (boost::iequals(x.name, "BOOTID.UPNP.ORG"))
                    {
                        info.boot_id = boost::lexical_cast<unsigned>(x.value);
                    }
                    else if (boost::iequals(x.name, "CONFIGID.UPNP.ORG"))
                    {
                        info.config_id = boost::lexical_cast<unsigned>(x.value);
                    }
                }
                catch (...)
                {
                }
            }

            if (satip_server)
            {
                list.push_back(info);

                if (cb_func != NULL)
                {
                    if (!cb_func(&list, user_param))
                        break;
                }
            }

            if ((now - start) > to)
            {
                break;
            }
        }
    }
    while (0);

    return err;
}

errcode_t get_server_props_from_description_xml(const std::string& desc_url, sat2ip_server_desc& desc)
{
    errcode_t res = err_error;

    std::string desc_xml;
    errcode_t err = get_description_xml(desc_url, desc_xml);

    if (err == err_none)
    {
        pugi::xml_document doc;
        if (doc.load_buffer(desc_xml.c_str(), desc_xml.size()).status == pugi::status_ok)
        {
            pugi::xml_node root_node = doc.first_child();
            if (root_node != NULL)
            {
                pugi::xml_node device_node = root_node.child("device");
                if (device_node != NULL)
                {
                    dvblink::pugixml_helpers::get_node_value(device_node, "deviceType", desc.device_type);
                    dvblink::pugixml_helpers::get_node_value(device_node, "modelName", desc.model_name);
                    dvblink::pugixml_helpers::get_node_value(device_node, "modelNumber", desc.model_number);
                    dvblink::pugixml_helpers::get_node_value(device_node, "manufacturer", desc.manufacturer);
                    dvblink::pugixml_helpers::get_node_value(device_node, "presentationURL", desc.uri);

                    if (!dvblink::pugixml_helpers::get_node_value(device_node, "friendlyName", desc.friendly_name))
                    {
                        desc.friendly_name = "unknown";
                    }
                    
                    //get number of frontends
                    std::string str;
                    if (dvblink::pugixml_helpers::get_node_value(device_node, "satip:X_SATIPCAP", str))
                        desc.parse_frontends(str);

                    if (dvblink::pugixml_helpers::get_node_value(device_node, "UDN", desc.udn))
                    {
                        //udn is essential. return success only if it is present
                        res = err_none;
                    }
                    else
                    {
                        res = (errcode_t)1;
                        //log_error(L"SAT2IP - get_server_props_from_description_xml: UDN node is not found");
                    }
                } else
                {
                    res = (errcode_t)2;
                }
            } else
            {
                res = (errcode_t)3;
            }
        } else
        {
            res = (errcode_t)4;
        }
    } else
    {
        res = (errcode_t)5;
    }

    return res;
}

void sat2ip_server_desc::parse_frontends(const std::string& fe_str)
{
    fe_num_map.clear();

    fe_num_map[dvbs_fe_key] = 0;
    fe_num_map[dvbc_fe_key] = 0;
    fe_num_map[dvbt_fe_key] = 0;

    typedef boost::tokenizer<boost::char_separator<char>, std::string::const_iterator, std::string> tokenizer_t;
    boost::char_separator<char> sep(",", 0, boost::keep_empty_tokens);
    tokenizer_t tokens(fe_str, sep);

    for (tokenizer_t::iterator tok_iter = tokens.begin(); tok_iter != tokens.end(); ++tok_iter)
    {
        size_t pos = tok_iter->find("-");
        if (pos != std::string::npos)
        {
            std::string system_str = tok_iter->substr(0, pos);
            int fe = atoi(tok_iter->substr(pos+1).c_str());

            if (boost::istarts_with(system_str, dvbs_fe_key))
                fe_num_map[dvbs_fe_key] += fe;

            if (boost::istarts_with(system_str, dvbc_fe_key))
                fe_num_map[dvbc_fe_key] += fe;

            if (boost::istarts_with(system_str, dvbt_fe_key))
                fe_num_map[dvbt_fe_key] += fe;
        }
    }
}

int sat2ip_server_desc::get_fe_num(const std::vector<std::string>& fe_to_combine)
{
    int ret_val = 0;

    //make a copy of the original map
    sat2ip_fe_num_map_t fe_map = fe_num_map;

    int combined_fe = 0;
    for (size_t i=0; i<fe_to_combine.size(); i++)
    {
        if (fe_map[fe_to_combine[i]] > combined_fe)
            combined_fe = fe_map[fe_to_combine[i]];

        fe_map.erase(fe_to_combine[i]);
    }

    ret_val += combined_fe;

    sat2ip_fe_num_map_t::iterator it = fe_map.begin();
    while (it != fe_map.end())
    {
        ret_val += it->second;
        ++it;
    }

    return ret_val;
}


} // namespace streaming
} // namespace dvblink

// $Id: sat-ip.cpp 12667 2016-04-13 13:58:38Z pashab $
