/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <dl_logger.h>
#include <dl_utils.h>
#include <dl_network_helper.h>
#include <dl_strings.h>
#include "m3u8loader.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

m3u8_loader::m3u8_loader(const char* user_agent) :
	m_SegmentList(NULL),
    m_ExitThread(false),
    m_UpdateThread(NULL)
{
    if (user_agent != NULL && strlen(user_agent) > 0)
        user_agent_ = user_agent;
}

m3u8_loader::~m3u8_loader()
{
}

bool m3u8_loader::Start(const char* url, m3u8_segment_list* segmentList)
{
	m_URL = url;
	m_SegmentList = segmentList;
    m_ExitThread = false;
	m_UpdateThread = new boost::thread(boost::bind(&m3u8_loader::UpdateThread, this));
	return true;
}

bool m3u8_loader::Stop()
{
    if (m_UpdateThread != NULL)
    {
        m_ExitThread = true;
		m_UpdateThread->join();
		delete m_UpdateThread;
        m_UpdateThread = NULL;

		m_SegmentList = NULL;
    }
	return true;
}

static void get_root_url(const char* url, std::string& root_url)
{
    root_url = url;

    //find last / symbol
    size_t slash_pos = root_url.rfind('/');
    if (slash_pos != std::string::npos)
    {
        //but not in the header ://
        size_t header_pos = root_url.find(url_header_separator);
        if (header_pos != std::string::npos)
        {
            if (slash_pos > header_pos + url_header_separator.size())
            {
                root_url = root_url.substr(0, slash_pos);
            }
        }
    }
}

bool m3u8_loader::GetActualPlaylist(const char* m3u8file, std::string& playlist_url)
{
    bool ret_val = false;
    playlist_url.clear();

    std::string address;
	std::string user;
	std::string password;
	unsigned short portNum;
	std::string urlSuffix;
	EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(m3u8file, address, user, password, portNum, urlSuffix);
	if (proto != DL_NET_PROTO_UNKNOWN)
	{
		//create http reader
		http_comm_handler http_reader(user_agent_.c_str(), address.c_str(),
            user.c_str(), password.c_str(), portNum);

		if (http_reader.Init())
		{

            std::string response;
	        http_reader.ExecuteGetWithResponse(urlSuffix.c_str(), proto == DL_NET_PROTO_HTTPS, response);
	        if (response.size() > 0)
	        {
                std::string effective_url = http_reader.get_last_effective_url();

		        std::string root_url;
                get_root_url(effective_url.c_str(), root_url);

                m3u8_media_segments media_segments; 
		        m3u8_playlist_list playlists;
		        m3u8_parse(root_url, response.c_str(), media_segments, playlists, user_agent_.c_str());

		        if (playlists.size() > 0)
		        {
			        //update url domain of each playlist
			        for (unsigned int i=0; i<playlists.size(); i++)
                        playlists[i].url = hls_make_full_url(root_url, playlists[i].url);

			        //find the playlist with the highest bandwidth
			        int idx = 0;
			        int bw = -1;
			        for (unsigned int i=0; i<playlists.size(); i++)
			        {
				        if (playlists[i].bandwidth > bw)
				        {
					        bw = playlists[i].bandwidth;
					        idx = i;
				        }
			        }
			        //execute this function recursively
			        ret_val = GetActualPlaylist(playlists[idx].url.c_str(), playlist_url);
		        } else
                {
                    playlist_url = effective_url;
                    ret_val = (playlist_url.size() > 0);
                }
	        }
			
            http_reader.Term();
        }
    }

    return ret_val;
}

void m3u8_loader::GetSegmentList(const char* m3u8file, m3u8_media_segment_list& segments)
{
    //parse address, port and suffix from the original url
    std::string address;
    std::string user;
    std::string password;
    unsigned short portNum;
    std::string urlSuffix;
    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(m3u8file, address, user, password, portNum, urlSuffix);
    if (proto != DL_NET_PROTO_UNKNOWN)
    {
	    //create http reader
	    http_comm_handler http_reader(user_agent_.c_str(), address.c_str(),
            user.c_str(), password.c_str(), portNum);

	    if (http_reader.Init())
	    {

            std::string response;
            http_reader.ExecuteGetWithResponse(urlSuffix.c_str(), proto == DL_NET_PROTO_HTTPS, response);
            if (response.size() > 0)
            {
                std::string effective_url = http_reader.get_last_effective_url();

                std::string root_url;
                get_root_url(effective_url.c_str(), root_url);

                m3u8_media_segments media_segments; 
	            m3u8_playlist_list playlists;
	            m3u8_parse(root_url, response.c_str(), media_segments, playlists, user_agent_.c_str());

                if (media_segments.segments.size() == 0)
                {
                    log_warning(L"m3u8_loader::GetSegmentList. Zero segments returned");
                }

	            for (unsigned int i=0; i<media_segments.segments.size(); i++)
	            {
                    media_segments.segments[i].url = hls_make_full_url(root_url, media_segments.segments[i].url);

		            m3u8_media_segment_ex segex(media_segments.sequenceBaseNum + i, media_segments.segments[i]);
		            segments.push_back(segex);
	            }
            }
	        http_reader.Term();
        }
    }
}

void m3u8_loader::UpdateThread()
{
    std::string playlist_url;
    if (GetActualPlaylist(m_URL.c_str(), playlist_url))
    {
	    int sleep_value = 100;
	    int counter = 0;
	    int scan_counter = 2000;
	    while (!m_ExitThread)
	    {
		    if (counter <= 0)
		    {
			    m3u8_media_segment_list segments;
			    GetSegmentList(playlist_url.c_str(), segments);
			    //Add segments to the list
			    if (m_SegmentList != NULL)
				    m_SegmentList->AddSegments(segments);
			    counter = scan_counter;
		    }
		    dvblink::engine::sleep(sleep_value);
		    counter -= sleep_value;
	    }
    } else
    {
		log_error(L"m3u8_loader::UpdateThread. GetActualPlaylist failed.");
    }
}

