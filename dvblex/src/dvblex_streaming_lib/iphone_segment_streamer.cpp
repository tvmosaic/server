/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <openssl/aes.h>
#include <dl_http_comm.curl.h>
#include <dl_network_helper.h>
#include <dl_logger.h>
#include <dl_strings.h>
#include "m3u8segmentlist.h"
#include "iphone_segment_streamer.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4355)
#endif

#define APPLE_IPAD_USER_AGENT_STR   "iTunes-iPad/6.1.2 (5; 16GB; dt:83)"

hls_segment_streamer::hls_segment_streamer(const char* user_agent)
    : packet_aligner_(AlignerCallback, this)
{
	m_StreamingThread = NULL;
	m_ReadThread = NULL;
	m_pmt_section = NULL;
	m_prebufferingTime = 3000;
	m_readBufferNodeSize = TS_PACKET_SIZE * 400; //256 kbit/s
	m_readBuffer = new ts_circle_buffer(64, m_readBufferNodeSize, L"hls_segment_streamer");
	m_recvBuf = new unsigned char[m_readBufferNodeSize];

    user_agent_ = APPLE_IPAD_USER_AGENT_STR;
    if (user_agent != NULL && strlen(user_agent) > 0)
        user_agent_ = user_agent;
}

#ifdef _MSC_VER
#pragma warning(pop)
#endif

hls_segment_streamer::~hls_segment_streamer()
{
    delete m_readBuffer;
    delete m_recvBuf;
}

bool hls_segment_streamer::StartReading(m3u8_segment_list* seglist, LP_IPHONE_STREAMER_TS_SEND_FUNC cbfunc, void* param)
{
	m_SegmentList = seglist;
	m_Callback = cbfunc;
	m_UserParam = param;
	m_readBuffer->reset();
	m_State = ESS_PAT;
	m_sectionParser.Init(0x00);
	m_pcrPid = 0;
    m_pmt_section = NULL;
    m_pmt_section_len = 0;
    m_NewPMTContinuityCounter = 0;
	
    m_ExitThread = false;
	m_ReadThread = new boost::thread(boost::bind(&hls_segment_streamer::ReadThread, this));

	return true;
}

bool hls_segment_streamer::StartStreaming()
{
    bool ret_val = false;
    
    if (m_ReadThread != NULL)
    {
		m_StreamingThread = new boost::thread(boost::bind(&hls_segment_streamer::StreamingThread, this));
        ret_val = true;
    }
    
	return ret_val;
}

bool hls_segment_streamer::Stop()
{
    m_ExitThread = true;
    if (m_StreamingThread != NULL)
    {
		m_StreamingThread->join();
		delete m_StreamingThread;
        m_StreamingThread = NULL;
    }
    if (m_ReadThread != NULL)
    {
		m_ReadThread->join();
		delete m_ReadThread;
        m_ReadThread = NULL;
    }

	if (m_pmt_section != NULL)
	{
	    delete m_pmt_section;
	    m_pmt_section = NULL;
    }
	    
	return true;
}

void hls_segment_streamer::ReadThread()
{
	while (!m_ExitThread)
	{
		m3u8_media_segment_ex segment;
		while (!m_ExitThread && m_SegmentList->PopFirstPlayableSegment(segment))
		{
			ReadSegment(&segment);
		}
	    boost::this_thread::sleep(boost::posix_time::milliseconds(100));
	}
}

__int64 hls_segment_streamer::GetBufferMaxPCR(unsigned char* buffer, int len)
{
    __int64 ret_val = -1;
   
    int c = len/188;
    for (int i=0; i<c; i++)
    {
        unsigned char* curpack = buffer + 188*i;
        unsigned short pid = ts_process_routines::GetPacketPID(curpack);
        if (pid != m_pcrPid)
            continue;
            
        boost::uint64_t pcr;
        if (ts_process_routines::GetPCRValue(curpack, pcr))
            ret_val = pcr;
    }
    return ret_val;
}

void hls_segment_streamer::ReadSegment(m3u8_media_segment_ex* segment)
{
	using namespace std;

	//parse segment url
    unsigned short portNum;
	std::string address, user, password, urlSuffix;
	EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(segment->url.c_str(), address, user, password, portNum, urlSuffix);

    if (proto != DL_NET_PROTO_UNKNOWN)
	{
		log_info(L"hls_segment_streamer::ReadSegment. Reading segment %d, %s") % segment->sequenceBaseNum % segment->url.c_str();

		http_comm_handler http_reader(user_agent_.c_str(), address.c_str(),
            user.c_str(), password.c_str(), portNum);

		if (http_reader.Init())
		{
            std::string buffer;
            int bytes_num = 0;
			void* hi = http_reader.SendGetRequest(urlSuffix.c_str(), proto == DL_NET_PROTO_HTTPS);
			
            if (hi != NULL)
			{
				while (!m_ExitThread)
				{
					unsigned long dwBytesRead = m_readBufferNodeSize;
					bool bRead = http_reader.ReadRequestResponse(hi, m_recvBuf, dwBytesRead, &m_ExitThread);
					
                    if (!bRead || dwBytesRead == 0)
                    {
                        if (!buffer.empty())
                        {
                            if ((segment->encrypted == true) &&
                                (segment->aes_key.size() == 16) &&
                                (segment->aes_iv.size() == 16))
                            {
                                // encrypted stream
                                AES_KEY key;
                                const size_t key_size = 16;

                                AES_set_decrypt_key(
                                    (unsigned char*)&(segment->aes_key[0]),
                                    8 * key_size, &key);

                                AES_cbc_encrypt((unsigned char*)&buffer[0],
                                    (unsigned char*)&buffer[0], buffer.size(),
                                    &key, (unsigned char*)&(segment->aes_iv[0]),
                                    AES_DECRYPT);

                                dwBytesRead = (buffer.size() / TS_PACKET_SIZE) * TS_PACKET_SIZE;
                            }
                            else
                            {
                                dwBytesRead = buffer.size();
                            }

							unsigned long l = dwBytesRead;
							unsigned char* buffer_ptr = (unsigned char*)&buffer[0];
                            while (!m_ExitThread && l > 0)
                            {
								if (m_readBuffer->get_free_nodes() > 0)
								{
									unsigned long write_portion = min((int)l, (int)m_readBuffer->get_node_size());

									packet_aligner_.write_stream(buffer_ptr, write_portion);

									buffer_ptr += write_portion;
									l -= write_portion;
								} else
								{
									//wait for place to be free
									boost::this_thread::sleep(boost::posix_time::milliseconds(50));
								}
                            }
                            bytes_num += dwBytesRead;
                            buffer.clear();
                        }
						//segment is read completely (or an error has happened). exit.
						break;
                    }
					else
					{
                        buffer.append((const char*)m_recvBuf, size_t(dwBytesRead));
					}
				}

				http_reader.CloseRequest(hi);
			}

			log_info(L"hls_segment_streamer::ReadSegment. Finished reading segment %d. %d bytes were read") % segment->sequenceBaseNum % bytes_num;
			http_reader.Term();
		}
	}
}

void hls_segment_streamer::SendStreamingData(const unsigned char* buf, int len, void* param)
{
    hls_segment_streamer* parent = (hls_segment_streamer*)param;
    parent->m_Callback((unsigned char*)buf, len, parent->m_UserParam);
}

void hls_segment_streamer::StreamingThread()
{
    //prebuffering extra time
    int delta = int(m_prebufferingTime);
    //time zero
    boost::posix_time::ptime t0;
    __int64 pcr0 = -1;
	while (!m_ExitThread)
	{
	    //wait until all stream parameters are known
	    if (m_State == ESS_STREAMING)
	    {
		    ts_circle_buffer::node_t stream_node = m_readBuffer->tear_node(0);
            if (stream_node != NULL)
            {
				unsigned char* stream_data = (unsigned char*)stream_node->data();
				int l = stream_node->size_to_read();

                __int64 buffer_pcr = GetBufferMaxPCR(stream_data, l);
                
				log_ext_info(L"hls_segment_streamer::StreamingThread. Pending buffer size %1%, pcr %2%") % l % buffer_pcr;

                if (pcr0 == -1)
                {
                    //there was no pcr assigned yet
                    if (buffer_pcr != -1)
                    {
                        pcr0 = buffer_pcr;
                        t0 = boost::posix_time::microsec_clock::local_time();
                    }
                    //send this buffer
                    SendBuffer(stream_data, l);
                }
                else
                {
                    if (buffer_pcr != -1)
                    {
                        while (!m_ExitThread)
                        {
                            //get current time
                            boost::posix_time::ptime time_now(boost::posix_time::microsec_clock::local_time());
                            //check the elapsed time
                            boost::posix_time::time_duration td = time_now - t0;
                            double stream_millisec = ((buffer_pcr - pcr0)/27000000.0)*1000;
                            int delta1 = int(td.total_milliseconds() + delta);
                            int delta2 = int(stream_millisec);
                            if (delta2 < 0 || (abs(delta1 - delta2) > 10000))
                            {
                                //pcr wrap around?
                                pcr0 = buffer_pcr;
                                t0 = boost::posix_time::microsec_clock::local_time();
                                //delta = 0;
		                        log_info(L"PCR wrap around? %d, %d") % delta1 % delta2;
                                SendBuffer(stream_data, l);
                                break;
                            }
                            else
                            {
                                if (delta1 > delta2)
                                {
									log_ext_info(L"hls_segment_streamer::StreamingThread: send buffer %d, %d") % delta1 % delta2;
                                    //send this buffer
                                    SendBuffer(stream_data, l);
                                    break;
                                }
                            }
						    boost::this_thread::sleep(boost::posix_time::milliseconds(10));
                        }
                    }
                    else
                    {
                        //send this buffer now
                        SendBuffer(stream_data, l);
                    }
                }
				m_readBuffer->put_node(stream_node);
            }
	    }
	    boost::this_thread::sleep(boost::posix_time::milliseconds(10));
	}
}

void hls_segment_streamer::SendBuffer(unsigned char* buf, int len)
{
	const unsigned char* start = buf;
	const unsigned char* cur = buf;
	const unsigned char* end = buf + len;
	size_t to_send = 0;

	while (cur < end)
	{
		unsigned short pid = ts_process_routines::GetPacketPID(cur);
		if (pid == m_pmtPid)
		{
			if (to_send)
			{
				m_Callback((unsigned char*)start, to_send, m_UserParam);
				to_send = 0;
			}

            //send our own pmt section
            m_PacketGen.SplitAndSendSectionBuffer(m_pmt_section, m_pmt_section_len, 
                &m_NewPMTContinuityCounter, m_pmtPid, SendStreamingData, this);

			cur += TS_PACKET_SIZE;
			start = cur;
		}
		else
		{
			cur += TS_PACKET_SIZE;
			to_send += TS_PACKET_SIZE;
		}
	}

	if (to_send)
	{
		m_Callback((unsigned char*)start, to_send, m_UserParam);
	}

	m_PacketGen.SplitAndSendSectionBuffer(m_pmt_section, m_pmt_section_len, &m_NewPMTContinuityCounter, m_pmtPid, SendStreamingData, this);
}

void __stdcall hls_segment_streamer::AlignerCallback(const unsigned char* buf, unsigned long len, void* user_param)
{
#if 0
    static FILE* f = fopen("/opt/dvblink/hls_stream.ts", "w+");

    if (f && buf && (len > 0))
    {
        fwrite(buf, len, 1, f);
        fflush(f);
    }
#endif

    hls_segment_streamer* parent = (hls_segment_streamer*)user_param;

    //process read segment to find out the parameters of the stream
    parent->ProcessReadSegment((unsigned char*)buf, len);
    parent->m_readBuffer->write_stream(buf, len);
}

void hls_segment_streamer::ProcessReadSegment(unsigned char* buf, int len)
{
    if (m_State != ESS_STREAMING)
    {
        int c = len / TS_PACKET_SIZE;
        for (int i = 0; i < c && m_State != ESS_STREAMING; i++)
        {
            unsigned char* curpack = buf + TS_PACKET_SIZE * i;
            unsigned short pid = ts_process_routines::GetPacketPID(curpack);
            
            switch (m_State)
            {
            case ESS_PAT:
                if (pid == 0x00)
                    ProcessPAT(curpack);
                break;
            case ESS_PMT:
                if (pid == m_pmtPid)
                    ProcessPMT(curpack);
                break;
            case ESS_STREAMING:
                break;
            }
        }
    }
}

void hls_segment_streamer::ProcessPAT(unsigned char* buf)
{
    ts_payload_parser::ts_section_list found_sections;
    if (m_sectionParser.AddPacket(buf, TS_PACKET_SIZE, found_sections) > 0)
    {
        //get PMT pid for our program from PAT
        std::vector<STSPATServiceInfo> services;
        ts_process_routines::GetPATStreamPIDs(found_sections[0].section, found_sections[0].length, services);
        if (services.size() == 1)
        {
            m_pmtPid = services[0].pmt_pid;
            m_sectionParser.Init(m_pmtPid);
            m_State = ESS_PMT;
        } else
        {
			log_warning(L"hls_segment_streamer::ProcessPAT. PAT contains more that one program");
        }
        m_sectionParser.ResetFoundSections(found_sections);
    }
}

void hls_segment_streamer::ProcessPMT(unsigned char* buf)
{
    ts_payload_parser::ts_section_list found_sections;
    if (m_sectionParser.AddPacket(buf, TS_PACKET_SIZE, found_sections) > 0)
    {
        //Get PCR PID
        ts_process_routines::GetPMTSectionPCRPID(found_sections[0].section, found_sections[0].length, m_pcrPid);
        //copy this section for the future reference
        m_pmt_section_len = found_sections[0].length;
        m_pmt_section = new unsigned char[m_pmt_section_len];
        memcpy(m_pmt_section, found_sections[0].section, m_pmt_section_len);

        m_sectionParser.ResetFoundSections(found_sections);

	    log_info(L"hls_segment_streamer::ProcessPMT. PAT and PMT processed successfully. PMT pid %d, PCR pid %d") % m_pmtPid % m_pcrPid;

        m_State = ESS_STREAMING;
    }
}
