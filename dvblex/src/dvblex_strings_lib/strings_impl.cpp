/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_strings.h>
#include <boost/algorithm/string.hpp>

namespace dvblink { namespace engine {

bool ConvertMultibyteToUC(ECodepage codepage, const char* src, std::wstring& dest)
{
    return ConvertMultibyteToUC(codepage, src, -1, dest);
}

bool IsStringEmpty(const wchar_t* str_to_test)
{
    bool ret_val = true;

    //walk through the string until first non-space character occurs
    int l = (int)wcslen(str_to_test);
    for (int i=0 ;i<l; i++)
    {
        if (str_to_test[i] != L' ' && 
            str_to_test[i] != L'\r' &&
            str_to_test[i] != L'\n' &&
            str_to_test[i] != L'\t')
        {
            ret_val = false;
            break;
        }
    }

    return ret_val;
}

bool IsStringEmpty(const char* str_to_test)
{
    bool ret_val = true;

    //walk through the string until first non-space character occurs
    int l = (int)strlen(str_to_test);
    for (int i=0 ;i<l; i++)
    {
        if (str_to_test[i] != ' ' && 
            str_to_test[i] != '\r' &&
            str_to_test[i] != '\n' &&
            str_to_test[i] != '\t')
        {
            ret_val = false;
            break;
        }
    }

    return ret_val;
}

void SubstSpecialChars(std::wstring& str, const wchar_t* str_to_subst, const wchar_t* subst_str)
{
    //find location of the character in question
    std::wstring::size_type idx = 0;
    idx = str.find(str_to_subst, idx);
    while (idx != std::wstring::npos)
    {
        //remove found character
        str.erase(idx, wcslen(str_to_subst));
        //insert substitute string
        str.insert(idx, subst_str);

        idx = str.find(str_to_subst, idx+1);
    }
}

void RemoveIllegalXMLCharacters(std::string& str)
{
    std::wstring wstr;
    if (ConvertMultibyteToUC(EC_UTF8, str.c_str(), wstr))
    {
        RemoveIllegalXMLCharacters(wstr);
        ConvertUCToMultibyte(EC_UTF8, wstr.c_str(), str);
    }
}

void RemoveIllegalXMLCharacters(std::wstring& str)
{
    /*
    The following characters are not allowed by XML parser and have to be removed:
    #x0 - #x8 (ASCII 0 - 8)
    #xB - #xC (ASCII 11 - 12)
    #xE - #x1F (ASCII 14 - 31)
    */

    wchar_t buf[2];
    buf[1] = L'\0';
    wchar_t i;
    const wchar_t nothing[] = L"";

    for (i = 0x01; i <= 0x08; i++)
    {
        buf[0] = i;
        SubstSpecialChars(str, buf, nothing);
    }

    for (i = 0x0B; i <= 0x0C; i++)
    {
        buf[0] = i;
        SubstSpecialChars(str, buf, nothing);
    }

    for (i = 0x0E; i <= 0x1F; i++)
    {
        buf[0] = i;
        SubstSpecialChars(str, buf, nothing);
    }
}

#ifdef WIN32

static void FixIso6937AccentChars(wchar_t* src, int& src_len)
{
	for (int i = 0; i < src_len; i++)
	{
        if (i > 0)
        {
            bool b_processed = false;
            if (src[i] == 0xB4)
            {
                //substitute previous character with accented version
                switch (src[i-1])
                {
                    case 0x20: src[i-1] = 0xB4; break;
                    case 0x41: src[i-1] = 0xC1; break;
                    case 0x43: src[i-1] = 0x106; break;
                    case 0x45: src[i-1] = 0xC9; break;
                    case 0x49: src[i-1] = 0xCD; break;
                    case 0x4C: src[i-1] = 0x139; break;
                    case 0x4E: src[i-1] = 0x143; break;
                    case 0x4F: src[i-1] = 0xD3; break;
                    case 0x52: src[i-1] = 0x154; break;
                    case 0x53: src[i-1] = 0x015A; break;
                    case 0x55: src[i-1] = 0xDA; break;
                    case 0x59: src[i-1] = 0xDD; break;
                    case 0x5A: src[i-1] = 0x179; break;
                    case 0x61: src[i-1] = 0xE1; break;
                    case 0x63: src[i-1] = 0x107; break;
                    case 0x65: src[i-1] = 0xE9; break;
                    case 0x69: src[i-1] = 0xED; break;
                    case 0x6C: src[i-1] = 0x013A; break;
                    case 0x6E: src[i-1] = 0x144; break;
                    case 0x6F: src[i-1] = 0xF3; break;
                    case 0x72: src[i-1] = 0x155; break;
                    case 0x73: src[i-1] = 0x015B; break;
                    case 0x75: src[i-1] = 0xFA; break;
                    case 0x79: src[i-1] = 0xFD; break;
                    case 0x7A: src[i-1] = 0x017A; break;
                    default: break;
                }
                b_processed = true;
            }
            if (src[i] == 0xF8F8)
            {
                //substitute previous character with accented version
                switch (src[i-1])
                {
                    case 0x5A: src[i-1] = 0x017B; break;
                    case 0x7A: src[i-1] = 0x017C; break;
                    default: break;
                }
                b_processed = true;
            }
            if (b_processed)
            {
                //remove prev character
                if (src_len - i >  1)
                {
                    wmemmove(src + i, src + i + 1, static_cast<size_t>(src_len - i) - 1);
                }
                //adjust i and length
                --src_len;
                --i;
            }
        }
	}
}

void CopyUCStr_be_to_le(wchar_t* dest_str, size_t str_len, const wchar_t* src_str)
{
    for (size_t j = 0; j < str_len; j++)
    {
        ((unsigned char*)dest_str)[2 * j] = ((const unsigned char*)src_str)[2 * j + 1];
        ((unsigned char*)dest_str)[2 * j + 1] = ((const unsigned char*)src_str)[2 * j];
    }
}

bool ConvertMultibyteToUC(ECodepage codepage, const char* src, int src_len, std::wstring& dest)
{
    bool ret_val = false;

    size_t t;
    if (src_len == -1)
    {
        t = strlen(src);
    }
    else
    {
        t = static_cast<size_t>(src_len);
    }

    if (codepage == EC_UTF16_BE)
    {
        //windows' MultiByteToWideChar does not handle this encoding conversion correctly
        
        //Bytes should be turned around (bigendian vs little endian)
        size_t new_buf_size = static_cast<size_t>(t)/sizeof(wchar_t) + 1; //account for extra zero terminator
        if (wchar_t* new_buf = static_cast<wchar_t*>(_malloca(sizeof(wchar_t) * new_buf_size)))
        {
            CopyUCStr_be_to_le(new_buf, new_buf_size - 1, reinterpret_cast<const wchar_t*>(src));
            new_buf[new_buf_size - 1] = L'\0';
            dest = new_buf;
            _freea(new_buf);
        }
        return true;
    }

    std::string str;
    if (codepage == static_cast<ECodepage>(20269)) //turn around controlling characters for ISO6937
    {
        for (size_t i = 0; i < t; i++)
        {
            if ((unsigned char)src[i] >= 0xC1 && (unsigned char)src[i] <= 0xCF)
            {
                if (i < t - 1)
                {
                    str += src[i + 1];
                }
                str += src[i];
                ++i;
            }
            else
            {
                str += src[i];
            }
        }
    }
    else
    {
        str.append(src, t);
    }

    dest = L"";
    int wsize = MultiByteToWideChar(codepage, 0, str.c_str(), static_cast<int>(str.size()), NULL, 0);
    if (wsize > 0)
    {
        wchar_t* wbuf = static_cast<wchar_t*>(_malloca(sizeof(wchar_t) * (static_cast<size_t>(wsize) + 1)));
        wsize = MultiByteToWideChar(codepage, 0, str.c_str(), static_cast<int>(str.size()), wbuf, wsize);
        if (wsize > 0)
        {
            if (codepage == static_cast<ECodepage>(20269))
            {
                FixIso6937AccentChars(wbuf, wsize);
            }

            wbuf[wsize] = L'\0';

            dest = wbuf;
            ret_val = true;
        }

        _freea(wbuf);
    }
    return ret_val;
}

bool ConvertUCToMultibyte(ECodepage codepage, const wchar_t* src, std::string& dest)
{
    bool ret_val = false;

    if (codepage == EC_UTF16_BE)
    {
        //windows' WideCharToMultiByte does not handle this encoding conversion correctly
        
        //Bytes should be turned around (bigendian vs little endian)
        size_t src_len = wcslen(src);
        if (wchar_t* new_buf = static_cast<wchar_t*>(_malloca(sizeof(wchar_t) * src_len)))
        {
            CopyUCStr_be_to_le(new_buf, src_len, src);
            dest.assign((char*)new_buf, src_len * sizeof(wchar_t));
            _freea(new_buf);
        }
        return true;
    }

    int size = WideCharToMultiByte(codepage, 0, src, -1, NULL, 0, NULL, NULL);
    if (size > 0)
    {
        char* buf = static_cast<char*>(_malloca(static_cast<size_t>(size) * sizeof(char)));
        size = WideCharToMultiByte(codepage, 0, src, -1, buf, size, NULL, NULL);
        if (size > 0)
        {
            dest = buf;
            ret_val = true;
        }
        _freea(buf);
    }
    return ret_val;
}

#else

bool ConvertMultibyteToUC(ECodepage codepage, const char* src, int src_len, std::wstring& dest)
{
    int len = src_len == -1 ? strlen(src) : src_len;
	return dvblink::iconv_helper::MultibyteToUnicode((dvblink::iconv_helper::ECodePageIDs)codepage, src, len, dest);
/*
    bool ret_val = false;
    int len = src_len == -1 ? strlen(src) : src_len;
    int size = mbstowcs(NULL, src, len);
    if (size > 0)
    {
        wchar_t* buf = new wchar_t[size + 1];

        size = mbstowcs(buf, src, len);
        if (size > 0)
        {
        	buf[size] = L'\0';
            dest = buf;
            ret_val = true;
        }
        delete buf;
    }
    return ret_val;
*/
}

bool ConvertUCToMultibyte(ECodepage codepage, const wchar_t* src, std::string& dest)
{
	return dvblink::iconv_helper::UnicodeToMultibyte((dvblink::iconv_helper::ECodePageIDs)codepage, src, wcslen(src), dest);
/*
    bool ret_val = false;
    int len = wcslen(src);
    int size = wcstombs(NULL, src, len);
    if (size > 0)
    {
        char* buf = new char[size+1];

        size = wcstombs(buf, src, len);
        if (size > 0)
        {
        	buf[size] = '\0';
            dest = buf;
            ret_val = true;
        }
        delete buf;
    }
    return ret_val;
*/
}

#endif

} //engine
} //dvblink

