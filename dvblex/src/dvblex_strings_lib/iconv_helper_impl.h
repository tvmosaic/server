/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <map>

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/locks.hpp>

#include <dl_iconv_helper.h>

#ifndef __APPLE__
    #undef LIBICONV_PLUG
    #include <dvblink_iconv.h>
#else
    #include<iconv.h>
#endif

namespace dvblink
{
namespace iconv_helper
{

typedef std::map<ECodePageIDs, std::string> TCodePageIDMap;

class CIconvCPConverter
{
public:
	CIconvCPConverter(const char* cpto, const char* cpfrom);
	~CIconvCPConverter();

	const char* Convert(const char* instr, int inlen, int& outlen);

protected:
	iconv_t m_iconvHandle;
	char* m_convBuffer;
	int m_convBufferSize;
};

typedef std::map<ECodePageIDs, CIconvCPConverter*> TCPConverterMap;

class CISO6937Converter
{
public:
	CISO6937Converter();
	~CISO6937Converter();

	bool Convert(const char* instr, int inlen, std::string& outstr);

protected:
	char* m_convBuffer;
	int m_convBufferSize;
};

class CIconvConverter
{
public:
	bool MultibyteToUnicode(ECodePageIDs codepage, const char* instr, int len, std::wstring& outstr);
	bool UnicodeToMultibyte(ECodePageIDs codepage, const wchar_t* instr, int len, std::string& outstr);

	CIconvConverter();
	~CIconvConverter();
protected:
	TCPConverterMap m_toUCConverters;
	TCPConverterMap m_fromUCConverters;
	TCodePageIDMap m_cpIDMap;

	bool MultibyteToUnicode_default(ECodePageIDs codepage, const char* src, int src_len, std::wstring& dest);
	bool UnicodeToMultibyte_default(ECodePageIDs codepage, const wchar_t* src, int len, std::string& dest);

	void DeleteConverters(TCPConverterMap* cp_map);
	CIconvCPConverter* GetConverter(ECodePageIDs codepage, bool toUC);
	boost::recursive_mutex m_lock;
	CISO6937Converter iso_6937_converter_;
};

} //iconv_helper
} //dvblink
