/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <string>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <dl_ini_file.h>
#include <dl_utils.h>
#include <dl_strings.h>

using namespace dvblink::engine;

namespace dvblink { namespace engine {

bool ini_file_reader::read_ini_file(const wchar_t* fpath, TDLIniFileTree& contents)
{
    std::string sfname = string_cast<EC_UTF8>(fpath);
    return read_ini_file(sfname.c_str(), contents);
}

bool ini_file_reader::read_ini_file(const char* fpath, TDLIniFileTree& contents)
{
    bool ret_val = true;
	contents.clear();

    //read ini file into a string
    std::ifstream t(fpath);
    std::string ini_file_contents((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
    
    //split string into lines
    typedef boost::tokenizer<boost::char_separator<char>, std::string::const_iterator, std::string> tokenizer_t;
    boost::char_separator<char> sep("\r\n");
    tokenizer_t tokens(ini_file_contents, sep);

    std::string section;
    for (tokenizer_t::iterator tok_iter = tokens.begin(); tok_iter != tokens.end(); ++tok_iter)
    {
        std::string cur_str = boost::trim_copy(*tok_iter);
        adjust_ini_string_for_comment(cur_str);
        if (cur_str.size() == 0)
            continue;
            
        //check if this is a section string
        if (cur_str[0] == '[')
        {
            section.assign(cur_str.begin() + 1, cur_str.end());
	        std::string::size_type pos = section.rfind(']');
	        if (pos != std::string::npos)
		        section.erase(pos);
		        
		    if (contents.find(section) == contents.end())
			    contents[section] = TDLIniFileLines();
        } else
        {
            //key/value string
            if (section.size() == 0)
                continue;
	        std::string::size_type pos = cur_str.find('=');
	        if (pos != std::string::npos)
	        {
                SDLIniFileLine dli;
                dli.key = cur_str.substr(0, pos);
                dli.value = cur_str.substr(pos + 1);
                if (dli.key.size() > 0)
				    contents[section].push_back(dli);
	        }
        }
    }
    return ret_val;
}

void ini_file_reader::adjust_ini_string_for_comment(std::string& ini_string)
{
	//search for ; symbol
	std::string::size_type pos = ini_string.find(';');
	if (pos != std::string::npos)
		ini_string.erase(pos);
}

} //settings
} //dvblink
