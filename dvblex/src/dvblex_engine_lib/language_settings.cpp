/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <dl_language_settings.h>
#include <dl_pugixml_helper.h>
#include <dl_utils.h>
#include <dl_common.h>
#include <dl_strings.h>


namespace dvblink { namespace engine {

std::auto_ptr<language_settings> language_settings::s_pInstance;

void language_settings::CreateInstance(const filesystem_path_t& pathToBinary)
{
    if (!GetInstance())
    {
        s_pInstance = std::auto_ptr<language_settings>(new language_settings(pathToBinary));
    }
}

void language_settings::DestroyInstance()
{
    if (GetInstance())
    {
        s_pInstance.reset();
    }
}

language_settings* language_settings::GetInstance()
{
    return s_pInstance.get();
}

language_settings::language_settings(const filesystem_path_t& pathToBinary) :
    m_pathToBinary(pathToBinary), m_strLangId("EN")
{
    Init();
}

language_settings::~language_settings()
{
    Reset();
}

void language_settings::Init()
{
    InitLangFileMap();
    InitItemMap();
}

void language_settings::Reset()
{
    m_mapItemIdToItemName.clear();
    m_mapLangToFileName.clear();
}

void language_settings::ReInitialize()
{
    boost::mutex::scoped_lock lock(m_lock);

    Reset();

    Init();
}

bool language_settings::get_path_by_language_id(const std::string& id, filesystem_path_t& p)
{
    bool ret_val = false;
    map_lang_name_to_lang_file_name_t::iterator it = m_mapLangToFileName.begin();
    while (it != m_mapLangToFileName.end())
    {
        if (boost::iequals(it->first, id))
        {
            //found!
            p = it->second.p;
            ret_val = true;
            break;
        }
        ++it;
    }
    return ret_val;
}

bool language_settings::InitItemMap()
{
    bool bRes = false;

    filesystem_path_t p = m_pathToBinary / LANGUAGES_DIRECTORY;
    filesystem_path_t p_lang;
    if (get_path_by_language_id(m_strLangId, p_lang))
    {
        p /= p_lang;
    } else
    {
        //not found
        //fall back to the first one
        if (m_mapLangToFileName.size() > 0)
        {
            p /= m_mapLangToFileName.begin()->second.p;
        } else
        {
            return false;
        }
    }

    pugi::xml_document doc;
    if (doc.load_file(p.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            pugi::xml_node child_node = root_node.first_child();
            while (child_node != NULL)
            {
                std::string strItemName = child_node.name();

                std::string strItemValue;
                if (dvblink::pugixml_helpers::get_node_attribute(child_node, LANGUAGE_ITEM_VALUE, strItemValue))
                    m_mapItemIdToItemName[item_id_t(strItemName)] = strItemValue;

                child_node = child_node.next_sibling();
            }
            bRes = true;
        }
    }
    return bRes;
}

void language_settings::InitLangFileMap()
{
    std::string strLangName;
    boost::filesystem::directory_iterator end_itr;

    try {
        filesystem_path_t p = m_pathToBinary / LANGUAGES_DIRECTORY;

        for (boost::filesystem::directory_iterator itr(p.to_boost_filesystem()); itr != end_itr; ++itr)
        {
            language_desc_t language_desc;
            if (!boost::filesystem::is_directory(itr->status()) && GetLangFromXml(itr->path(), language_desc))
            {
                language_desc.p = itr->path().filename();
                m_mapLangToFileName[language_desc.id] = language_desc;
            }
        }
    } catch(...) {}
}

bool language_settings::GetLangFromXml(const boost::filesystem::path& xmlFile, language_desc_t& language_desc) const
{
    bool bRes = false;

    filesystem_path_t p(xmlFile);

    pugi::xml_document doc;
    if (doc.load_file(p.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            bRes = dvblink::pugixml_helpers::get_node_attribute(root_node, LANGUAGE_NAME, language_desc.name) &&
                dvblink::pugixml_helpers::get_node_attribute(root_node, LANGUAGE_ID, language_desc.id);
        }
    }

    return bRes;
}
/*
void language_settings::GetLanguageList(std::vector<std::string>& vecLanguage)
{
    boost::mutex::scoped_lock lock(m_lock);

    InitLangFileMap();
    
    map_lang_name_to_lang_file_name_const_iter_t iter = m_mapLangToFileName.begin();
    while (iter != m_mapLangToFileName.end())
    {
        vecLanguage.push_back(iter->first);
        ++iter;
    }

    std::sort(vecLanguage.begin(), vecLanguage.end());
}
*/
bool language_settings::GetCurrentLanguageId(std::string& lang_id)
{
    lang_id = m_strLangId;
    return true;
}

bool language_settings::SetCurrentLanguageId(const std::string& lang_id)
{
    bool ret_val = false;

    boost::mutex::scoped_lock lock(m_lock);

    filesystem_path_t p;
    if (get_path_by_language_id(lang_id, p))
    {
        m_strLangId = lang_id;

        Reset();
        Init();

        ret_val = true;
    }

    return ret_val;
}

bool language_settings::GetItemName(const item_id_t& itemID, std::string& strItemName)
{
    boost::mutex::scoped_lock lock(m_lock);

    bool bRes = false;
    map_item_id_to_item_name_const_iter_t iter = m_mapItemIdToItemName.find(itemID);
    if (iter != m_mapItemIdToItemName.end())
    {
        strItemName = iter->second;
        bRes = true;
    }
    return bRes;
}

std::string language_settings::GetItemName(const item_id_t& itemID)
{
    std::string strItemName;
    if (!GetItemName(itemID, strItemName))
    {
        strItemName = itemID.get();
    }
    return strItemName;
}

} //engine
} //dvblink
