/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"

#ifdef WIN32

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>

#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <tuner.h>

#include <dl_dshow_enum.h>
#include <dl_logger.h>

using namespace dvblink::logging;

namespace dvblink { namespace engine {

TDSEnum * enum_create(REFCLSID clsid)
{
	TDSEnum *p = (TDSEnum *) malloc(sizeof(TDSEnum));

	HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC, IID_ICreateDevEnum,  (void **) &p->pICreateDevEnum);
   	if (FAILED (hr))
   	{
       	log_error(L"enum_create(): Cannot CoCreate ICreateDevEnum");
		free(p);
		p = NULL;
       	return NULL;
   	}

    // obtain the enumerator
    hr = p->pICreateDevEnum->CreateClassEnumerator(clsid, &p->pIEnumMoniker, 0);
    // the call can return S_FALSE if no moniker exists, so explicitly check S_OK
    if (FAILED (hr))
    {
        log_error(L"enum_create(): Cannot CreateClassEnumerator");
        p->pICreateDevEnum->Release();
		free(p);
		p = NULL;
        return NULL;
    }
    if (S_OK != hr)  // Class not found
    {
        log_error(L"enum_create(): Class not found, CreateClassEnumerator returned S_FALSE");
        p->pICreateDevEnum->Release();
		free(p);
		p = NULL;
        return NULL;
	}

	p->pIMoniker = NULL;

	return p;

}



HRESULT enum_next(TDSEnum *pEnum)
{	
	if (!pEnum) return S_FALSE;	

	if (pEnum->pIMoniker) 
    {
		pEnum->pIMoniker->Release();
		pEnum->pIMoniker = NULL;
	}

	return pEnum->pIEnumMoniker->Next(1, &pEnum->pIMoniker, 0);
}



HRESULT enum_free(TDSEnum *pEnum)
{	
	if (!pEnum) return S_OK;

	if (pEnum->pIMoniker) 
    {
		pEnum->pIMoniker->Release();
		pEnum->pIMoniker = NULL;
	}

	if (pEnum->pICreateDevEnum)
		pEnum->pICreateDevEnum->Release();
	
	if (pEnum->pIEnumMoniker)
		pEnum->pIEnumMoniker->Release();

	free(pEnum);

	return S_OK;
}

HRESULT enum_get_filter(TDSEnum *pEnum, IBaseFilter **ppFilter)
{
	*ppFilter = NULL;

//	IBaseFilter *pFilter = NULL;	

	// bind the filter        
    HRESULT hr = pEnum->pIMoniker->BindToObject(
                                    NULL, 
                                    NULL, 
                                    IID_IBaseFilter,
                                    reinterpret_cast<void**>(ppFilter)
                                    );
	if (!*ppFilter)
			return S_FALSE;



	

	//hr = pFilter->QueryInterface (IID_IBaseFilter, (void **) ppFilter);
	//*ppFilter = pFilter;
	return hr; 
}

HRESULT enum_get_name(TDSEnum *pEnum)
{
    //Obtain device path
    LPOLESTR device_path;
    HRESULT hr = pEnum->pIMoniker->GetDisplayName(NULL, NULL, &device_path);
    if (hr == S_OK)
    {
        //copy name
		wcscpy_s(pEnum->szDevicePathW, 511, device_path);
        //free name
        IMalloc* malloc_if = NULL;
        hr = CoGetMalloc(1, &malloc_if);
        if (hr == S_OK)
        {
            malloc_if->Free(device_path);
            malloc_if->Release();
        }
    }
    else
    {
        log_warning(L"enum_get_name(): GetDisplayName method failed %d") % hr;
        wcscpy_s(pEnum->szDevicePathW, L"Unknown device path");
    }
    // obtain filter's friendly name
    IPropertyBag *pBag;
    hr =pEnum->pIMoniker->BindToStorage(
                                NULL, 
                                NULL, 
                                IID_IPropertyBag,
                                reinterpret_cast<void**>(&pBag)
                                );

    if(hr == S_OK)
    {
   	    VARIANT varBSTR;
	    VariantInit(&varBSTR);			
	    varBSTR.vt = VT_BSTR;		

	    hr = pBag->Read(L"FriendlyName", &varBSTR, NULL);	

        if(FAILED(hr))
        {
            log_ext_info(L"enum_get_name(): IPropertyBag->Read method failed %d") % hr;
		    wcscpy_s(pEnum->szNameW, L"Unknown device");
		    strcpy_s(pEnum->szName, "Unknown device");
    //		pBag->Release();		
    //		VariantClear(&varBSTR);
    //		return hr;
        }
        else
	    {
			size_t sz;
		    wcstombs_s(&sz, pEnum->szName, sizeof(pEnum->szName), varBSTR.bstrVal, 255);
		    wcscpy_s(pEnum->szNameW, 255, varBSTR.bstrVal);
		    VariantClear(&varBSTR);
	    }
    	
    	
	    pBag->Release();		
    }
    else
    {
        log_error(L"enum_get_name(): Cannot BindToStorage");
    }

	return hr;
}

} //engine
} //dvblink

#endif
