/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <assert.h>
#include <boost/format.hpp>
#include <dl_ts_info.h>
#include <dl_utils.h>
#include <dl_stream_types.h>
#include <dl_logger.h>
#include <dl_strings.h>
#include <memory.h>

using namespace dvblink::logging;

namespace dvblink { namespace engine {

void SetDefaultDVBTChannelInfo(SDVBTChannelInfo* channel_info)
{
    channel_info->centerFreq = 52000000;
    channel_info->bandwidth = ETS_BT_7MHZ;
    channel_info->constellation = ETS_CT_QPSK;
    channel_info->hierarchyInfo = ETS_HIT_NON_HIERARCHICAL;
    channel_info->hpCodeRate = ETS_CR_12;
    channel_info->lpCodeRate = ETS_CR_12;
    channel_info->guardInterval = ETS_GI_132;
    channel_info->transmission_mode = ETS_TM_2K;
}

//******************* ts_crc_handler ***************************

ts_crc_handler* ts_crc_handler::GetCRCHandler()
{
    static ts_crc_handler crchandler;
    return &crchandler;
}

ts_crc_handler::ts_crc_handler() 
{
    boost::uint32_t i,j,k;
    for(i=0;i<256;i++) {
        k=0;
        for(j=((i<<24) & 0xFF000000) | 0x800000;j!=0x80000000;j<<=1)
            k=((k<<1) & 0xFFFFFFFE)^(((k^j)&0x80000000)?0x04C11DB7:0);
        m_CRC32Table[i]=k;
    }
}

boost::uint32_t ts_crc_handler::CalculateCRC(unsigned char* TSPacket, int len) {
    boost::uint32_t CRC32=0xffffffff;
    for(int i=0;i<len;i++)
        CRC32=(((CRC32<<8) & 0xFFFFFF00))^m_CRC32Table[(((CRC32>>24) & 0x000000FF))^TSPacket[i]];
    return CRC32;
}

void ts_crc_handler::AddCRC(unsigned char* buffer, int len, unsigned char* add_ptr)
{
	boost::uint32_t crc = CalculateCRC(buffer, len);

	add_ptr[0]=(unsigned char)((crc>>24)&0xFF); //CRC_32 Hi-Hi
    add_ptr[1]=(unsigned char)((crc>>16)&0xFF); //CRC_32 Hi-Lo
    add_ptr[2]=(unsigned char)((crc>>8)&0xFF); //CRC_32 Lo-Hi
    add_ptr[3]=(unsigned char)(crc&0xFF); //CRC_32 Lo-Lo
}

//--------------------------------------------------------------------

boost::uint32_t ts_process_routines::read_u32bit_from_memory(unsigned char* buffer, int bytes_to_read, unsigned char first_byte_mask)
{
    int shift_factor = (bytes_to_read - 1)*8;
    boost::uint32_t value = ((boost::uint32_t)(buffer[0] & first_byte_mask)) << shift_factor;
    for (int i=1; i<bytes_to_read; i++)
    {
        shift_factor = (bytes_to_read - 1 - i)*8;
        value |= ((boost::uint32_t)buffer[i]) << shift_factor;
    }
    return value;
}

void ts_process_routines::change_packet_to_null(unsigned char* packet)
{
    packet[0] = 0x47;
    packet[1] = 0x1F;
    packet[2] = 0xFF;
    packet[3] = 0x10;
}

void ts_process_routines::IncPacketVersion(unsigned char& version)
{
    version += 1;
    if (version > 32 || version == 0)
        version = 1;
}

unsigned short ts_process_routines::GetNextContinuityCounter(unsigned short counter)
{
    unsigned short ret_val = counter + 1;

    if (ret_val > 15)
    {
        ret_val = 0;
    }

    return ret_val;
}

int ts_process_routines::GetPayloadStartIndicator(const void* buffer)
{
    unsigned char* byte_buffer = (unsigned char*)buffer;
    return (((byte_buffer[1] & 0x40) >> 6) & 0x03);
}

int ts_process_routines::GetTSErrorIndicator(const void* buffer)
{
    unsigned char* byte_buffer = (unsigned char*)buffer;
    return (((byte_buffer[1] & 0x80) >> 7) & 0x01);
}

void ts_process_routines::SetContinuityCounter(const void* buffer, unsigned char new_counter)
{
    unsigned char* byte_buffer = (unsigned char*)buffer;
    byte_buffer[3] &= 0xF0;
    byte_buffer[3] |= (new_counter & 0x0F);
}

unsigned char ts_process_routines::GetContinuityCounter(const void* buffer)
{
    unsigned char* byte_buffer = (unsigned char*)buffer;
    return (byte_buffer[3] & 0x0F);
}

int ts_process_routines::GetAdaptationFieldValue(void* buffer)
{
    unsigned char* byte_buffer = (unsigned char*)buffer;
    return (((byte_buffer[3] & 0x30) >> 4) & 0x0F);
}

unsigned short ts_process_routines::GetPacketPID(const unsigned char* buffer)
{
    return (((((unsigned short)buffer[1]) << 8) & 0xFF00) | buffer[2]) & 0x1FFF;
}

bool ts_process_routines::packet_starts_with_keyframe(const unsigned char* buffer)
{
	bool ret_val = false;
	if( (buffer[3] & 0x20) && (buffer[4] > 0) ) 
	{
        // packet has adaptation field
        if ( buffer[5] & 0x40 ) 
			ret_val = true;
	}

	return ret_val;
}

bool ts_process_routines::SetPacketPID(void* packetBuffer, unsigned short newPid)
{
    bool retVal = false;

    if (packetBuffer != NULL)
    {
        unsigned char* byteBuf = (unsigned char*)packetBuffer;
        newPid = (((unsigned short)byteBuf[1] << 8) & 0xE000) | newPid;
        byteBuf[1] = (newPid >> 8) & 0xFF;
        byteBuf[2] = newPid & 0xFF;

        retVal = true;
    }

    return retVal;
}

int ts_process_routines::GetSectionLength(const void* buffer)
{
    unsigned char* buf = (unsigned char*)buffer;
    return ((((((unsigned short)buf[1]) << 8) & 0xFF00) | buf[2]) & 0x0FFF) + 3;
}

//This function return -1 if there is no section data in this packet or offset to it otherwise
int ts_process_routines::GetSectionDataOffset(const void* buffer)
{
    int ret_val = -1;
    unsigned char* byte_buffer = (unsigned char*)buffer;

    //Get the adaptation control flag
    int adapt_control = (((byte_buffer[3] & 0x30) >> 4) & 0x0F);
    //Filter cases when packet is filled with adaptation field only
    if (adapt_control == 1 || adapt_control == 3)
    {
        ret_val = 4; //offset to section offset field (in case of no adaptation payload present)
        if (adapt_control == 3)
        {
            //get the length of adaptation field and add it to the offset
            ret_val += byte_buffer[4] + 1; //add also the "adaptation field length" field
        }
        //offset field is only relevant for the packets with payload_start_indicator == 1
        if (GetPayloadStartIndicator(buffer) == 1)
        {
            if (ret_val >=0 && ret_val <= 187)
            {
                //get the pointer value and add it to the section data offset
                ret_val = ret_val + byte_buffer[ret_val] + 1; //account for the pointer field itself
            }
            else
            {
                ret_val = -1;
            }
        }
    }
    else
    {
        //log error
        log_error(L"ts_process_routines::GetSectionDataOffset. Warning. Only adaptation field is present in the payload.");
    }
    return ret_val;
}

bool ts_process_routines::CheckSectionCRC(const void* buffer)
{
	bool ret_val = false;

	try
	{
		unsigned char* byte_buf = (unsigned char*)buffer;

		int section_len = GetSectionLength(buffer);
		if (section_len > 4)
		{
			boost::uint32_t CRC32 = ts_crc_handler::GetCRCHandler()->CalculateCRC(byte_buf, section_len - 4);

			//read crc value from the section
			boost::uint32_t sectioncrc = ((boost::uint32_t)byte_buf[section_len - 4]) << 24;
			sectioncrc |= ((boost::uint32_t)byte_buf[section_len - 3]) << 16;
			sectioncrc |= ((boost::uint32_t)byte_buf[section_len - 2]) << 8;
			sectioncrc |= ((boost::uint32_t)byte_buf[section_len - 1]);

			if (sectioncrc == CRC32)
			{
				ret_val = true;
			}
		}

	} catch(...)
	{
		ret_val = false;
	}

	return ret_val;
}

int ts_process_routines::GetPayloadOffset(const void* buffer)
{
    int ret_val = -1;
    unsigned char* byte_buffer = (unsigned char*)buffer;

    //Get the adaptation control flag
    int adapt_control = (((byte_buffer[3] & 0x30) >> 4) & 0x0F);
    //Filter cases when packet is filled with adaptation field only
    if (adapt_control == 1 || adapt_control == 3)
    {
        ret_val = 4; //offset to section offset field (in case of no adaptation payload present)
        if (adapt_control == 3)
        {
            //get the length of adaptation field and add it to the offset
            ret_val += byte_buffer[4] + 1; //add also the "adaptation field length" field
        }
    }
    return ret_val;
}

boost::int64_t ts_process_routines::GetPTSValue(const void* buffer)
{
    boost::int64_t ret_val = -1;
    unsigned char* byte_buf = (unsigned char*)buffer;
    //check payload start indicator first
    if (GetPayloadStartIndicator(buffer) != 0)
    {
        //get offset to PES header
        int pes_hdr_offset = GetPayloadOffset(buffer);
        if (pes_hdr_offset != -1)
        {
            ret_val = ps_process_routines::GetPTSValueFromPES(byte_buf + pes_hdr_offset);
        }
    }
    return ret_val;
}

void ts_process_routines::SetPTSValue(const void* buffer, boost::int64_t value)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    //check payload start indicator first
    if (GetPayloadStartIndicator(buffer) != 0)
    {
        //get offset to PES header
        int pes_hdr_offset = GetPayloadOffset(buffer);
        if (pes_hdr_offset != -1)
        {
            ps_process_routines::SetPTSInPES(byte_buf + pes_hdr_offset, value);
        }
    }
}

boost::int64_t ts_process_routines::GetDTSValue(const void* buffer)
{
    boost::int64_t ret_val = -1;
    unsigned char* byte_buf = (unsigned char*)buffer;
    //check payload start indicator first
    if (GetPayloadStartIndicator(buffer) != 0)
    {
        //get offset to PES header
        int pes_hdr_offset = GetPayloadOffset(buffer);
        if (pes_hdr_offset != -1)
        {
            ret_val = ps_process_routines::GetDTSValueFromPES(byte_buf + pes_hdr_offset);
        }
    }
    return ret_val;
}

void ts_process_routines::SetDTSValue(const void* buffer, boost::int64_t value)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    //check payload start indicator first
    if (GetPayloadStartIndicator(buffer) != 0)
    {
        //get offset to PES header
        int pes_hdr_offset = GetPayloadOffset(buffer);
        if (pes_hdr_offset != -1)
        {
            ps_process_routines::SetDTSInPES(byte_buf + pes_hdr_offset, value);
        }
    }
}

unsigned char ts_process_routines::GetNITTableId(void* buffer, int /*sdt_length*/)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    return byte_buf[0];
}

bool ts_process_routines::GetNITSectionStats(unsigned char* section, int /*len*/, unsigned char& version, unsigned char& cur_next)
{
	version = ((section[5] >> 1) & 0x1F);
    cur_next = (section[5] & 0x01);
	return true;
}

bool ts_process_routines::GetNITSectionNumbers(unsigned char* section, int /*len*/, unsigned char& cur_section_num, unsigned char& max_section_num)
{
	cur_section_num = section[6];
    max_section_num = section[7];
	return true;
}

bool ts_process_routines::GetNetworkIDFromNIT(void* buffer, int nit_length, unsigned short& nid)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    nid = ((((unsigned short)byte_buf[3]) << 8) & 0xFF00) | byte_buf[4];
    return true;
}

bool ts_process_routines::GetTSInfoFromNIT(void* buffer, int nit_length, std::vector<STSNITTSInfo>& streams_info)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    
    streams_info.clear();

    unsigned short nid = ((((unsigned short)byte_buf[3]) << 8) & 0xFF00) | byte_buf[4];

    //length of first descriptor array
    int net_desc_length = ((((((unsigned short)byte_buf[8]) << 8) & 0xFF00) | byte_buf[9]) & 0x0FFF);

    //go through the second list of descriptors and extract all channels 
    int offs_to_second_desc = 10 + net_desc_length;
    int second_desc_length = ((((((unsigned short)byte_buf[offs_to_second_desc]) << 8) & 0xFF00) | byte_buf[offs_to_second_desc+1]) & 0x0FFF);
    offs_to_second_desc += 2; //account for the length itself
    int desc_offs = 0;
	while (desc_offs < second_desc_length)
	{
		unsigned char* cur_buf = byte_buf + offs_to_second_desc + desc_offs;
        unsigned short tid = ((((unsigned short)cur_buf[0]) << 8) & 0xFF00) | cur_buf[1];
        unsigned short onid = ((((unsigned short)cur_buf[2]) << 8) & 0xFF00) | cur_buf[3];

        STSNITTSInfo ts;
        ts.nid = nid;
        ts.onid = onid;
        ts.tid = tid;
        streams_info.push_back(ts);

        int ts_desc_len = ((((((unsigned short)cur_buf[4]) << 8) & 0xFF00) | cur_buf[5]) & 0x0FFF);
        desc_offs += ts_desc_len + 6;
	}
    return true;
}

static void AddLCNService(std::vector<SDVBTLCNDesc>& lcn_services, SDVBTLCNDesc& lcndesc)
{
    bool bFound = false;
    for (unsigned int i=0; i<lcn_services.size(); i++)
    {
        if (lcn_services[i].service_id == lcndesc.service_id && 
            lcn_services[i].ts_id == lcndesc.ts_id && 
            lcn_services[i].onid == lcndesc.onid && 
            lcn_services[i].network_id == lcndesc.network_id)
        {
            //found! - update the number if it not -1
            if (lcndesc.lcn != -1)
                lcn_services[i].lcn = lcndesc.lcn;

            bFound = true;
            break;
        }
    }
    if (!bFound)
        lcn_services.push_back(lcndesc);
}

bool ts_process_routines::GetLCNFromNIT(void* buffer, int nit_length, std::vector<SDVBTLCNDesc>& lcn_services)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    
    lcn_services.clear();

    unsigned short nid = ((((unsigned short)byte_buf[3]) << 8) & 0xFF00) | byte_buf[4];

    //length of first descriptor array
    int net_desc_length = ((((((unsigned short)byte_buf[8]) << 8) & 0xFF00) | byte_buf[9]) & 0x0FFF);

    //go through the second list of descriptors and extract all channels 
    int offs_to_second_desc = 10 + net_desc_length;
    int second_desc_length = ((((((unsigned short)byte_buf[offs_to_second_desc]) << 8) & 0xFF00) | byte_buf[offs_to_second_desc+1]) & 0x0FFF);
    offs_to_second_desc += 2; //account for the length itself
    int desc_offs = 0;
	while (desc_offs < second_desc_length)
	{
		unsigned char* cur_buf = byte_buf + offs_to_second_desc + desc_offs;
        unsigned short tid = ((((unsigned short)cur_buf[0]) << 8) & 0xFF00) | cur_buf[1];
        unsigned short onid = ((((unsigned short)cur_buf[2]) << 8) & 0xFF00) | cur_buf[3];

        SDVBTLCNDesc srv;
        srv.network_id = nid;
        srv.onid = onid;
        srv.ts_id = tid;

        int ts_desc_len = ((((((unsigned short)cur_buf[4]) << 8) & 0xFF00) | cur_buf[5]) & 0x0FFF);
        int ts_desc_offs = 0;
        while (ts_desc_offs < ts_desc_len)
        {
            unsigned char* ts_desc_buf = cur_buf + ts_desc_offs + 6;
		    int desc_len = ts_desc_buf[1];
            unsigned char desc_type = ts_desc_buf[0];
            switch (desc_type)
            {
                case 0x83:
                    {
                        //LCN descriptor
                        int lcn_offs = 0;
                        while (lcn_offs < desc_len)
                        {
                            unsigned char* cur_lcn = ts_desc_buf+2+lcn_offs;
                            srv.service_id = ((((unsigned short)cur_lcn[0]) << 8) & 0xFF00) | cur_lcn[1];
                            srv.lcn = ((((unsigned short)cur_lcn[2]) << 8) & 0x0300) | cur_lcn[3];
                            AddLCNService(lcn_services, srv);

                            lcn_offs += 4;
                        }
                    }
                    break;
                case 0x41:
                    {
                        //Service list descriptor
                        //The services in this list is a super set of services in LCN list (whatever comes first)
                        int sl_offs = 0;
                        while (sl_offs < desc_len)
                        {
                            unsigned char* cur_sl = ts_desc_buf + 2 + sl_offs;
                            srv.service_id = ((((unsigned short)cur_sl[0]) << 8) & 0xFF00) | cur_sl[1];
                            srv.lcn = -1;
                            AddLCNService(lcn_services, srv);

                            sl_offs += 3;
                        }
                    }
                    break;
                default:
                    break;
            }
		    ts_desc_offs += desc_len + 2;
        }

        desc_offs += ts_desc_len + 6;
	}
    return true;
}

bool ts_process_routines::GetDeliverySystemsFromNIT(void* buffer, int nit_length, std::vector<STSDeliverySystemInfo>& delivery_systems)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    unsigned short nid = ((((unsigned short)byte_buf[3]) << 8) & 0xFF00) | byte_buf[4];
    int net_desc_length = ((((((unsigned short)byte_buf[8]) << 8) & 0xFF00) | byte_buf[9]) & 0x0FFF);
    //go through the first list of descriptors for a network name
    std::wstring netName = L"";
    int desc_offs = 0;
    while (desc_offs < net_desc_length)
    {
        unsigned char* cur_buf = byte_buf + desc_offs + 10;
        int desc_len = cur_buf[1];
        if (cur_buf[0] == 0x40)
        {
            //network name descriptor
            ConvertAnnexATextToUnicode(cur_buf+2, desc_len, netName);
        }
		desc_offs += desc_len + 2;
    }

    //go through the second list of descriptors and extract all channels 
    int offs_to_second_desc = 10 + net_desc_length;
    int second_desc_length = ((((((unsigned short)byte_buf[offs_to_second_desc]) << 8) & 0xFF00) | byte_buf[offs_to_second_desc+1]) & 0x0FFF);
    offs_to_second_desc += 2; //account for the length itself
    desc_offs = 0;
	while (desc_offs < second_desc_length)
	{
		unsigned char* cur_buf = byte_buf + offs_to_second_desc + desc_offs;
        unsigned short tid = ((((unsigned short)cur_buf[0]) << 8) & 0xFF00) | cur_buf[1];
        unsigned short onid = ((((unsigned short)cur_buf[2]) << 8) & 0xFF00) | cur_buf[3];

        STSDeliverySystemInfo ds;
        ds.networkName = netName;
        ds.nid = nid;
        ds.tid = tid;
        ds.systemType = ETS_DS_UNKNOWN;

        STSServiceInfo si;
        si.onid = onid;
        si.tid = tid;

        int ts_desc_len = ((((((unsigned short)cur_buf[4]) << 8) & 0xFF00) | cur_buf[5]) & 0x0FFF);
        int ts_desc_offs = 0;
        while (ts_desc_offs < ts_desc_len)
        {
            unsigned char* ts_desc_buf = cur_buf + ts_desc_offs + 6;
		    int desc_len = ts_desc_buf[1];
            unsigned char desc_type = ts_desc_buf[0];
            switch (desc_type)
            {
            case 0x43:
                //satellite delivery desc
                GetSatDeliverySystemInfo(ts_desc_buf, desc_len, ds);
                break;
            case 0x44:
                //cable delivery desc
                GetCableDeliverySystemInfo(ts_desc_buf, desc_len, ds);
                break;
            case 0x5A:
                //terrestrial delivery desc
                GetTerrestrialDeliverySystemInfo(ts_desc_buf, desc_len, ds);
                break;
            case 0x41:
                {
                    //Service list descriptor
                    //The services in this list is a super set of services in LCN list (whatever comes first)
                    ds.bFilterServices = true;
                    si.ch_num = -1;
                    int sl_offs = 0;
                    while (sl_offs < desc_len)
                    {
                        unsigned char* cur_sl = ts_desc_buf + 2 + sl_offs;
                        si.sid = ((((unsigned short)cur_sl[0]) << 8) & 0xFF00) | cur_sl[1];
                        ds.add_service(si);

                        sl_offs += 3;
                    }
                }
                break;
            case 0x83:
                {
                    //LCN descriptor
                    int lcn_offs = 0;
                    while (lcn_offs < desc_len)
                    {
                        unsigned char* cur_lcn = ts_desc_buf+2+lcn_offs;
                        si.sid = ((((unsigned short)cur_lcn[0]) << 8) & 0xFF00) | cur_lcn[1];
                        si.ch_num = ((((unsigned short)cur_lcn[2]) << 8) & 0x0300) | cur_lcn[3];
                        ds.add_service(si);

                        lcn_offs += 4;
                    }
                }
                break;
            default:
                break;
            }
		    ts_desc_offs += desc_len + 2;
        }
        if (ds.systemType != ETS_DS_UNKNOWN)
            delivery_systems.push_back(ds);

        desc_offs += ts_desc_len + 6;
	}
    return true;
}

void ts_process_routines::GetSatDeliverySystemInfo(unsigned char* desc_ptr, int len, STSDeliverySystemInfo& ds_info)
{
    //frequency
    GetNumberFromBCD(desc_ptr + 2, 8, ds_info.frequency);
    //polarization
    unsigned char pol = (desc_ptr[8] & 0x60) >> 5;
    switch (pol)
    {
    case 0:
        ds_info.polarization = ETS_PT_HOR;
        break;
    case 1:
        ds_info.polarization = ETS_PT_VER;
        break;
    case 2:
        ds_info.polarization = ETS_PT_LEFT;
        break;
    case 3:
        ds_info.polarization = ETS_PT_RIGHT;
        break;
	default:
        ds_info.polarization = ETS_PT_HOR;
		break;
    }
	//dvb-s vs dvb-s2
	bool bdvbs2 = (desc_ptr[8] & 0x04) != 0;
	ds_info.systemType = bdvbs2 ? ETS_DS_SAT_S2 : ETS_DS_SAT;
    //modulation
    unsigned char modulation = (desc_ptr[8] & 0x03);
    switch (modulation)
    {
    case 0:
    case 1:
		ds_info.modulation = ETS_MT_QPSK;
        break;
    case 2:
        ds_info.modulation = ETS_MT_8PSK;
        break;
    case 3:
        ds_info.modulation = ETS_MT_QAM16;
        break;
	default:
        ds_info.modulation = ETS_MT_QPSK;
		break;
    }
    //symbol rate
    GetNumberFromBCD(desc_ptr + 9, 7, ds_info.symbol_rate);
    //FEC
    unsigned char fec = (desc_ptr[12] & 0x0F);
    switch (fec)
    {
    case 1:
        ds_info.fec = ETS_FT_12;
        break;
    case 2:
        ds_info.fec = ETS_FT_23;
        break;
    case 3:
        ds_info.fec = ETS_FT_34;
        break;
    case 4:
        ds_info.fec = ETS_FT_56;
        break;
    case 5:
        ds_info.fec = ETS_FT_78;
        break;
    case 6:
        ds_info.fec = ETS_FT_89;
        break;
    default:
        ds_info.fec = ETS_FT_AUTO;
        break;
    }
}

void ts_process_routines::GetCableDeliverySystemInfo(unsigned char* desc_ptr, int len, STSDeliverySystemInfo& ds_info)
{
    ds_info.systemType = ETS_DS_CABLE;
    //frequency
    GetNumberFromBCD(desc_ptr + 2, 8, ds_info.frequency);
    //modulation
    unsigned char modulation = desc_ptr[8];
    switch (modulation)
    {
    case 1:
        ds_info.modulation = ETS_MT_QAM16;
        break;
    case 2:
        ds_info.modulation = ETS_MT_QAM32;
        break;
    case 3:
        ds_info.modulation = ETS_MT_QAM64;
        break;
    case 4:
        ds_info.modulation = ETS_MT_QAM128;
        break;
    case 5:
        ds_info.modulation = ETS_MT_QAM256;
        break;
	default:
        ds_info.modulation = ETS_MT_QAM64;
		break;
    }
    //symbol rate
    GetNumberFromBCD(desc_ptr + 9, 7, ds_info.symbol_rate);
}

void ts_process_routines::GetTerrestrialDeliverySystemInfo(unsigned char* desc_ptr, int len, STSDeliverySystemInfo& ds_info)
{
    ds_info.systemType = ETS_DS_TERRESTRIAL;
    //frequency
    GetNumberFromBCD(desc_ptr + 2, 8, ds_info.frequency);
    //bandwidth
    unsigned char bnd = (desc_ptr[6] & 0xE0) >> 5;
    ds_info.bandwidth = 8-bnd;
}

void ts_process_routines::GetCADescriptorsFromCAT(void* catBuffer, int catLength, std::vector<STSCADescriptorInfo>& ca_desc_list)
{
    ca_desc_list.clear();

    unsigned char* byte_buf = (unsigned char*)catBuffer;
    int section_len = ((((((unsigned short)byte_buf[1]) << 8) & 0xFF00) | byte_buf[2]) & 0x0FFF) + 3;
    int desc_offs = 8;
    //scan PMT for PIDs
    while (desc_offs < section_len - 4)
    {
        unsigned char* cur_buf = byte_buf + desc_offs;
        int desc_len = cur_buf[1];
        if (cur_buf[0] == 0x09)
        {
            STSCADescriptorInfo desc_info;
            desc_info.CI_system_ID = ((((unsigned short)cur_buf[2]) << 8) & 0xFF00) | cur_buf[3];
            desc_info.PID = (((((unsigned short)cur_buf[4]) << 8) & 0xFF00) | cur_buf[5]) & 0x1FFF;
            ca_desc_list.push_back(desc_info);
        }
        desc_offs += desc_len + 2;
    }
}

bool ts_process_routines::GetPMTDescForPID(unsigned short pid_to_find, void* buffer, int pmt_len, unsigned char& stream_type, unsigned char** ret_ptr, int& desc_length)
{
    bool ret_val = false;
    unsigned char* byte_buf = (unsigned char*)buffer;
    //Get offset to the stream description part
    int program_info_len = (((((unsigned short)byte_buf[10]) << 8) & 0xFF00) | byte_buf[11]) & 0x0FFF;
    int streams_desc_offs = program_info_len + 12;
    //Search for a PID of our stream
    while (streams_desc_offs < pmt_len-4)
    {
        unsigned char* cur_buf = byte_buf + streams_desc_offs;
        unsigned short pid = (((((unsigned short)cur_buf[1]) << 8) & 0xFF00) | cur_buf[2]) & 0x1FFF;
        int stream_desc_len = (((((unsigned short)cur_buf[3]) << 8) & 0xFF00) | cur_buf[4]) & 0x0FFF;
        if (pid == pid_to_find)
        {
            //Found! 
            //stream type
            stream_type = cur_buf[0];
            //Copy descriptors
            desc_length = stream_desc_len;
            if (desc_length > 0)
            {
                *ret_ptr = new unsigned char[stream_desc_len];
                memcpy(*ret_ptr, cur_buf + 5, stream_desc_len);
            }
            else
            {
                *ret_ptr = NULL;
            }
            ret_val = true;
            break;
        }
        //Advance offset pointer
        streams_desc_offs += stream_desc_len + 5;
    }

    return ret_val;
}

unsigned char ts_process_routines::GetPMTVersion(void* buffer, int /*length*/)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    return ((byte_buf[5] >> 1) & 0x1F);
}

bool ts_process_routines::GetPMTStreamPIDs(unsigned char* pmt_section, int pmt_len, std::vector<unsigned short>& pids)
{
    bool ret_val = true;
    pids.clear();

    unsigned char* byte_buf = (unsigned char*)pmt_section;
    //Get offset to the stream description part
    int program_info_len = (((((unsigned short)byte_buf[10]) << 8) & 0xFF00) | byte_buf[11]) & 0x0FFF;
    int streams_desc_offs = program_info_len + 12;
    //scan PMT for PIDs
    while (streams_desc_offs < pmt_len - 4)
    {
        unsigned char* cur_buf = byte_buf + streams_desc_offs;
        unsigned short pid = (((((unsigned short)cur_buf[1]) << 8) & 0xFF00) | cur_buf[2]) & 0x1FFF;
        int stream_desc_len = (((((unsigned short)cur_buf[3]) << 8) & 0xFF00) | cur_buf[4]) & 0x0FFF;
        pids.push_back(pid);
        //Advance offset pointer
        streams_desc_offs += stream_desc_len + 5;
    }

    return ret_val;
}

bool ts_process_routines::GetPMTStreamsInfo(unsigned char* pmt_section, int pmt_len, std::vector<STSESInfo>& stream_info)
{
    bool ret_val = true;
    stream_info.clear();

    unsigned char* byte_buf = (unsigned char*)pmt_section;
    //Get offset to the stream description part
    int program_info_len = (((((unsigned short)byte_buf[10]) << 8) & 0xFF00) | byte_buf[11]) & 0x0FFF;
    int streams_desc_offs = program_info_len + 12;
    //scan PMT for PIDs
    while (streams_desc_offs < pmt_len - 4)
    {
        unsigned char* cur_buf = byte_buf + streams_desc_offs;
        STSESInfo si;
        si.PID = (((((unsigned short)cur_buf[1]) << 8) & 0xFF00) | cur_buf[2]) & 0x1FFF;
        si.type = cur_buf[0];
        int stream_desc_len = (((((unsigned short)cur_buf[3]) << 8) & 0xFF00) | cur_buf[4]) & 0x0FFF;
        //search CA descriptors
        int ca_desc_offs = 0;
        while (ca_desc_offs < stream_desc_len)
        {
            unsigned char* ca_cur_buf = cur_buf + ca_desc_offs + 5;
            int ca_desc_len = ca_cur_buf[1];
            if (ca_cur_buf[0] == 0x09)
            {
                STSCADescriptorInfo desc_info;
                desc_info.CI_system_ID = ((((unsigned short)ca_cur_buf[2]) << 8) & 0xFF00) | ca_cur_buf[3];
                desc_info.PID = (((((unsigned short)ca_cur_buf[4]) << 8) & 0xFF00) | ca_cur_buf[5]) & 0x1FFF;
                si.ca_descriptors.push_back(desc_info);
            }
            ca_desc_offs += ca_desc_len + 2;
        }

        stream_info.push_back(si);
        //Advance offset pointer
        streams_desc_offs += stream_desc_len + 5;
    }

    return ret_val;
}

void ts_process_routines::GetCADescriptorsFromPMT(void* pmtBuffer, int pmtLength, std::vector<STSCADescriptorInfo>& ca_desc_list)
{
    ca_desc_list.clear();

    unsigned char* byte_buf = (unsigned char*)pmtBuffer;
    //Get offset to the PMT descriptors part
    int program_info_len = (((((unsigned short)byte_buf[10]) << 8) & 0xFF00) | byte_buf[11]) & 0x0FFF;
    int pmt_desc_offs = 0;
    byte_buf += 12;
    while (pmt_desc_offs < program_info_len)
    {
        unsigned char* cur_buf = byte_buf + pmt_desc_offs;
        int desc_len = cur_buf[1];
        if (cur_buf[0] == 0x09)
        {
            STSCADescriptorInfo desc_info;
            desc_info.CI_system_ID = ((((unsigned short)cur_buf[2]) << 8) & 0xFF00) | cur_buf[3];
            desc_info.PID = (((((unsigned short)cur_buf[4]) << 8) & 0xFF00) | cur_buf[5]) & 0x1FFF;
            ca_desc_list.push_back(desc_info);
        }
        pmt_desc_offs += desc_len + 2;
    }
}

void ts_process_routines::GetCADescriptorsFromPMTWithDesc(void* pmtBuffer, int pmtLength, std::vector<TCA_PMT_CA_DESC>& ca_desc_list)
{
	ca_desc_list.clear();

	unsigned char* byte_buf = (unsigned char*)pmtBuffer;
	//Get offset to the PMT descriptors part
	int program_info_len = (((((unsigned short)byte_buf[10]) << 8) & 0xFF00) | byte_buf[11]) & 0x0FFF;
	int pmt_desc_offs = 0;
	byte_buf += 12;
	while (pmt_desc_offs < program_info_len)
	{
		unsigned char* cur_buf = byte_buf + pmt_desc_offs;
		int desc_len = cur_buf[1];
		if (cur_buf[0] == 0x09)
		{
            //be on the safe side
            if (desc_len + 2 < pmtLength)
            {
				TCA_PMT_CA_DESC desc_info;
				desc_info.assign(cur_buf, cur_buf + (desc_len + 2));
			    ca_desc_list.push_back(desc_info);
            }
		}
		pmt_desc_offs += desc_len + 2;
	}
}

bool ts_process_routines::GetPMTStreamsInfoWithDesc(unsigned char* pmt_section, int pmt_len, unsigned short& ServiceId, std::vector<TCA_PMT_ES_DESC>& stream_info)
{
	bool ret_val = true;
	stream_info.clear();

	unsigned char* byte_buf = pmt_section;
    //service ID == program number
	ServiceId = ((((unsigned short)byte_buf[3]) << 8) & 0xFF00) | byte_buf[4];
    //Get offset to the stream description part
	int program_info_len = (((((unsigned short)byte_buf[10]) << 8) & 0xFF00) | byte_buf[11]) & 0x0FFF;
	int streams_desc_offs = program_info_len + 12;
	//scan PMT for PIDs
	while (streams_desc_offs < pmt_len - 4)
	{
		unsigned char* cur_buf = byte_buf + streams_desc_offs;
		TCA_PMT_ES_DESC si;
		si.pid = (((((unsigned short)cur_buf[1]) << 8) & 0xFF00) | cur_buf[2]) & 0x1FFF;
		si.type = cur_buf[0];
		int stream_desc_len = (((((unsigned short)cur_buf[3]) << 8) & 0xFF00) | cur_buf[4]) & 0x0FFF;
        //collect ca descriptors
	    int pmt_ca_desc_offs = 0;
        unsigned char* ca_desc_buf_start = cur_buf + 5;
	    while (pmt_ca_desc_offs < stream_desc_len)
	    {
		    unsigned char* ca_desc_buf = ca_desc_buf_start + pmt_ca_desc_offs;
		    int ca_desc_len = ca_desc_buf[1];
            std::vector<TCA_PMT_CA_DESC>* list_ptr;
		    if (ca_desc_buf[0] == 0x09)
                list_ptr = &si.ca_desc_list;
            else
                list_ptr = &si.other_desc_list;

           //be on the safe side
            if (ca_desc_len + 2 < pmt_len)
            {
				TCA_PMT_CA_DESC desc_info;
				desc_info.assign(ca_desc_buf, ca_desc_buf + (ca_desc_len + 2));
		        list_ptr->push_back(desc_info);
            }

            pmt_ca_desc_offs += ca_desc_len + 2;
	    }
        //save resulting info
		stream_info.push_back(si);
		//Advance offset pointer
		streams_desc_offs += stream_desc_len + 5;
	}

	return ret_val;
}

void ts_process_routines::DescriptorsLinearToArray(SDataChunk& linear_desc, std::vector<TCA_PMT_CA_DESC>& vector_desc)
{
    vector_desc.clear();
    if (linear_desc.length > 0 && linear_desc.data != NULL)
    {
        unsigned char* cur_desc = linear_desc.data;
        while (cur_desc < linear_desc.data + linear_desc.length)
        {
            int desc_len = cur_desc[1] + 2;
            if (cur_desc + desc_len <= linear_desc.data + linear_desc.length)
            {
                TCA_PMT_CA_DESC desc;
                desc.assign(cur_desc, cur_desc + desc_len);
                vector_desc.push_back(desc);
            }
            cur_desc += desc_len;
        }
    }
}

bool ts_process_routines::IsAudioStream(unsigned char type, std::vector<TCA_PMT_CA_DESC>& stream_desc_list)
{
    bool ret_val = false;
    switch (type)
    {
      case 0x82://DTS 6 channel audio for Blu-ray
      case 0x83://Dolby TrueHD lossless audio for Blu-ray
      case 0x84://Dolby Digital Plus up to 16 channel audio for Blu-ray
      case 0x85://DTS 8 channel audio for Blu-ray
      case 0x86://DTS 8 channel lossless audio for Blu-ray
      case 0x87://Dolby Digital Plus up to 16 channel audio for ATSC
      case 0x3://MPEG-1 AUDIO ISO/IEC 11172 
      case 0x4://MPEG-3 AUDIO ISO/IEC 13818-3 
      case 0x81://AC3 AUDIO
      case 0x0f://AAC AUDIO
      case 0x11://LATM AAC AUDIO
          ret_val = true;
        break;
      case 0x06:
          //this can be ac3 audio
          //go through descriptors to see if it is so
          for (unsigned int i=0; i<stream_desc_list.size(); i++)
          {
              if (stream_desc_list[i].size() > 0 &&
                  (stream_desc_list[i].at(0) == 0x6a || stream_desc_list[i].at(0) == 0x7a)) //ac3/eac3 descriptor
              {
                  ret_val = true;
                  break;
              }
          }
          break;
      default:
          break;
    }
    return ret_val;
}

bool ts_process_routines::IsVideoStream(unsigned char type, std::vector<TCA_PMT_CA_DESC>& stream_desc_list)
{
    bool ret_val = false;
    switch (type)
    {
      case 0x80://DigiCipher II video
          {
              //this can be audio pcm or DCII video
              for (unsigned int i=0; i<stream_desc_list.size(); i++)
              {
                  if (stream_desc_list[i].size() > 0 &&
                      stream_desc_list[i].at(0) == VIDEO_STERAM_DESC_TAG )
                  {
                      ret_val = true;
                      break;
                  }
              }
          }
          break;
      case 0x24://ITU-T Rec. H.265 and ISO/IEC 23008-2 (Ultra HD video)
      case 0x1b://H.264
      case 0x10://MPEG4 ISO/IEC 14496-2
      case 0x1://MPEG-1 VIDEO ISO/IEC 11172 
      case 0x2://MPEG-2 VIDEO ITU-T Rec. H.262 | ISO/IEC 13818-2 Video or ISO/IEC 11172-2 constrained parameter video stream
          ret_val = true;
        break;
      default:
          break;
    }
    return ret_val;
}

bool ts_process_routines::IsSubtitlesStream(unsigned char type, std::vector<TCA_PMT_CA_DESC>& stream_desc_list)
{
    bool ret_val = false;
    switch (type)
    {
      case 0x06:
          //this can be subtitles or teletext stream
          //go through descriptors to see if it is so
          for (unsigned int i=0; i<stream_desc_list.size(); i++)
          {
              if (stream_desc_list[i].size() > 0 &&
                  (stream_desc_list[i].at(0) == TELETEXT_DESC_TAG ||
                  stream_desc_list[i].at(0) == SUBTITLING_DESC_TAG) )
              {
                  ret_val = true;
                  break;
              }
          }
        break;
      default:
          break;
    }
    return ret_val;
}

bool ts_process_routines::SetPMTSectionServiceID(void* pmt_buffer, int pmt_length, unsigned short ServiceId)
{
    unsigned char* byte_buf = (unsigned char*)pmt_buffer;
    //Update service id value
    if (ServiceId != 0)
    {
        byte_buf[3] = (ServiceId >> 8) & 0xFF;
        byte_buf[4] = ServiceId & 0xFF;
    }
    //Recalculate CRC
    unsigned long CRC32=ts_crc_handler::GetCRCHandler()->CalculateCRC(byte_buf, 
        pmt_length - 4);
    byte_buf[pmt_length - 4]=(unsigned char)((CRC32>>24)&0xFF); //CRC_32 Hi-Hi
    byte_buf[pmt_length - 3]=(unsigned char)((CRC32>>16)&0xFF); //CRC_32 Hi-Lo
    byte_buf[pmt_length - 2]=(unsigned char)((CRC32>>8)&0xFF); //CRC_32 Lo-Hi
    byte_buf[pmt_length - 1]=(unsigned char)(CRC32&0xFF); //CRC_32 Lo-Lo
    return true;
}

bool ts_process_routines::GetPMTSectionServiceID(void* pmt_buffer, int pmt_length, unsigned short& ServiceId)
{
    unsigned char* buffer = (unsigned char*)pmt_buffer;
    ServiceId = ((((unsigned short)buffer[3]) << 8) & 0xFF00) | buffer[4];
    return true;
}

bool ts_process_routines::GetPMTSectionPCRPID(void* pmtBuffer, int /*pmtLength*/, unsigned short& pcrPid)
{
    unsigned char* byteBuf = (unsigned char*)pmtBuffer;
    pcrPid = (((((unsigned short)byteBuf[8]) << 8) & 0xFF00) | byteBuf[9]) & 0x1FFF;

    return true;
}

bool ts_process_routines::SetPMTStreamType(void* pmt_buffer, int pmt_length, unsigned short stream_pid, unsigned char stream_type)
{
    bool retVal = false;

    if (stream_pid != 0 && pmt_buffer != NULL)
    {
        unsigned char* byteBuf = (unsigned char*)pmt_buffer;

        // Get offset to the stream description part
        int program_info_len = (((((unsigned short)byteBuf[10]) << 8) & 0xFF00) | byteBuf[11]) & 0x0FFF;
        int streams_desc_offs = program_info_len + 12;
        // Change PMT PID
        while (streams_desc_offs < pmt_length - 4)
        {
            unsigned char* curBuf = byteBuf + streams_desc_offs;
            unsigned short pid = (((((unsigned short)curBuf[1]) << 8) & 0xFF00) | curBuf[2]) & 0x1FFF;
            if (pid == stream_pid)
            {
                curBuf[0] = stream_type;
                retVal = true;
                break;
            }

            int stream_desc_len = (((((unsigned short)curBuf[3]) << 8) & 0xFF00) | curBuf[4]) & 0x0FFF;		
            // Advance offset pointer
            streams_desc_offs += stream_desc_len + 5;
        }
        if (retVal)
        {
            // Recalculate CRC
            unsigned long CRC32 = ts_crc_handler::GetCRCHandler()->CalculateCRC(byteBuf, pmt_length - 4);
            byteBuf[pmt_length - 4] = (unsigned char)((CRC32 >> 24) & 0xFF); // CRC_32 Hi-Hi
            byteBuf[pmt_length - 3] = (unsigned char)((CRC32 >> 16) & 0xFF); // CRC_32 Hi-Lo
            byteBuf[pmt_length - 2] = (unsigned char)((CRC32 >> 8) & 0xFF);  // CRC_32 Lo-Hi
            byteBuf[pmt_length - 1] = (unsigned char)(CRC32 & 0xFF);         // CRC_32 Lo-Lo
        }
    }
    return retVal;
}

bool ts_process_routines::SetPMTSectionPCRPID(void* pmtBuffer, int pmtLength, unsigned short pcrPid)
{
    bool retVal = false;

    // Update pcr id value
    if (pcrPid != 0 && pmtBuffer != NULL)
    {
        unsigned char* byteBuf = (unsigned char*)pmtBuffer;
        pcrPid = (((unsigned short)byteBuf[8] << 8) & 0xE000) | pcrPid;
        byteBuf[8] = (pcrPid >> 8) & 0xFF;
        byteBuf[9] = pcrPid & 0xFF;

        // Recalculate CRC
        unsigned long CRC32 = ts_crc_handler::GetCRCHandler()->CalculateCRC(byteBuf, pmtLength - 4);
        byteBuf[pmtLength - 4] = (unsigned char)((CRC32 >> 24) & 0xFF); // CRC_32 Hi-Hi
        byteBuf[pmtLength - 3] = (unsigned char)((CRC32 >> 16) & 0xFF); // CRC_32 Hi-Lo
        byteBuf[pmtLength - 2] = (unsigned char)((CRC32 >> 8) & 0xFF);  // CRC_32 Lo-Hi
        byteBuf[pmtLength - 1] = (unsigned char)(CRC32 & 0xFF);         // CRC_32 Lo-Lo

        retVal = true;
    }

    return retVal;
}

bool ts_process_routines::ChangePMTSectionStreamPID(void* pmtBuffer, int pmtLength, unsigned short origPid, unsigned short newPid)
{
    bool retVal = false;

    if (origPid != 0 && newPid != 0 && pmtBuffer != NULL)
    {
        unsigned char* byteBuf = (unsigned char*)pmtBuffer;

        // Get offset to the stream description part
        int program_info_len = (((((unsigned short)byteBuf[10]) << 8) & 0xFF00) | byteBuf[11]) & 0x0FFF;
        int streams_desc_offs = program_info_len + 12;
        // Change PMT PID
        while (streams_desc_offs < pmtLength - 4)
        {
            unsigned char* curBuf = byteBuf + streams_desc_offs;
            unsigned short pid = (((((unsigned short)curBuf[1]) << 8) & 0xFF00) | curBuf[2]) & 0x1FFF;
            if (pid == origPid)
            {
                newPid = (((unsigned short)curBuf[1] << 8) & 0xE000) | newPid;
                curBuf[1] = (newPid >> 8) & 0xFF;
                curBuf[2] = newPid & 0xFF;

                retVal = true;
                break;
            }

            int stream_desc_len = (((((unsigned short)curBuf[3]) << 8) & 0xFF00) | curBuf[4]) & 0x0FFF;		
            // Advance offset pointer
            streams_desc_offs += stream_desc_len + 5;
        }
        if (retVal)
        {
            // Recalculate CRC
            unsigned long CRC32 = ts_crc_handler::GetCRCHandler()->CalculateCRC(byteBuf, pmtLength - 4);
            byteBuf[pmtLength - 4] = (unsigned char)((CRC32 >> 24) & 0xFF); // CRC_32 Hi-Hi
            byteBuf[pmtLength - 3] = (unsigned char)((CRC32 >> 16) & 0xFF); // CRC_32 Hi-Lo
            byteBuf[pmtLength - 2] = (unsigned char)((CRC32 >> 8) & 0xFF);  // CRC_32 Lo-Hi
            byteBuf[pmtLength - 1] = (unsigned char)(CRC32 & 0xFF);         // CRC_32 Lo-Lo
        }
    }

    return retVal;
}

void ts_process_routines::FillDVBTNetworkDescriptor(unsigned char* buffer, SDVBTChannelInfo* ch_info)
{
    //Set center frequency
    unsigned long freq = ch_info->centerFreq / 10;
    buffer[0] = (unsigned char)((freq >> 24) & 0xFF);
    buffer[1] = (unsigned char)(((freq & 0x00FF0000) >> 16) & 0xFF);
    buffer[2] = (unsigned char)(((freq & 0x0000FF00) >> 8) & 0xFF);
    buffer[3] = (unsigned char)(freq & 0x000000FF);
    //bandwidth
    unsigned char bandwidth_mask = 0;
    switch (ch_info->bandwidth)
    {
    case ETS_BT_8MHZ:
        bandwidth_mask = 0;
        break;
    case ETS_BT_7MHZ:
        bandwidth_mask = 0x20;
        break;
    default:
        assert(false);
        break;
    }
    buffer[4] = bandwidth_mask;

    //Constellation, hierarchy information and code rate hp
    unsigned char constellation_mask = 0;
    unsigned char hierarchy_mask = 0;
    unsigned char coderate_hp_mask = 0;
    switch (ch_info->constellation)
    {
    case ETS_CT_QPSK:
        constellation_mask = 0;
        break;
    case ETS_CT_16QAM:
        constellation_mask = 0x40;
        break;
    case ETS_CT_64QAM:
        constellation_mask = 0x80;
        break;
    default:
        assert(false);
        break;
    }

    switch (ch_info->hierarchyInfo)
    {
    case ETS_HIT_NON_HIERARCHICAL:
        hierarchy_mask = 0;
        break;
    case ETS_HIT_ALPHA_1:
        hierarchy_mask = 0x08;
        break;
    case ETS_HIT_ALPHA_2:
        hierarchy_mask = 0x10;
        break;
    case ETS_HIT_ALPHA_4:
        hierarchy_mask = 0x18;
        break;
    default:
        assert(false);
        break;
    }

    switch (ch_info->hpCodeRate)
    {
    case ETS_CR_12:
        coderate_hp_mask = 0;
        break;
    case ETS_CR_23:
        coderate_hp_mask = 1;
        break;
    case ETS_CR_34:
        coderate_hp_mask = 2;
        break;
    case ETS_CR_56:
        coderate_hp_mask = 3;
        break;
    case ETS_CR_78:
        coderate_hp_mask = 4;
        break;
    default:
        assert(false);
        break;
    }

    buffer[5] = constellation_mask | hierarchy_mask | coderate_hp_mask;

    //hcode rate lp, guard interval and transmission mode
    unsigned char coderate_lp_mask = 0;
    unsigned char guard_interval_mask = 0;
    unsigned char transmission_mode_mask = 0;

    switch (ch_info->lpCodeRate)
    {
    case ETS_CR_12:
        coderate_lp_mask = 0;
        break;
    case ETS_CR_23:
        coderate_lp_mask = 0x20;
        break;
    case ETS_CR_34:
        coderate_lp_mask = 0x40;
        break;
    case ETS_CR_56:
        coderate_lp_mask = 0x60;
        break;
    case ETS_CR_78:
        coderate_lp_mask = 0x80;
        break;
    default:
        assert(false);
        break;
    }

    switch (ch_info->guardInterval)
    {
    case ETS_GI_132:
        guard_interval_mask = 0;
        break;
    case ETS_GI_116:
        guard_interval_mask = 0x08;
        break;
    case ETS_GI_18:
        guard_interval_mask = 0x10;
        break;
    case ETS_GI_14:
        guard_interval_mask = 0x18;
        break;
    default:
        assert(false);
        break;
    }

    switch (ch_info->transmission_mode)
    {
    case ETS_TM_2K:
        transmission_mode_mask = 0;
        break;
    case ETS_TM_8K:
        transmission_mode_mask = 0x02;
        break;
    default:
        assert(false);
        break;
    }

    buffer[6] = coderate_lp_mask | guard_interval_mask | transmission_mode_mask;
}

bool ts_process_routines::GetPCRValue(void* buffer, boost::uint64_t& pcr)
{
    bool ret_val = false;
    unsigned char* byte_buf = (unsigned char*)buffer;
    if (GetTSErrorIndicator(buffer) == 0)
    {
        //Check packet's adaptation field
        int adapt_field = GetAdaptationFieldValue(buffer);
        if (adapt_field == 2 || adapt_field == 3)
        {
            //check length of the adaptation field
            if (byte_buf[4] > 0)
            {
                //check adaptation flags for PCR
                if ((byte_buf[5] & 0x10) == 0x10)
                {
                    //retreive PCR value from this packet
                    boost::uint64_t pcr_base;
                    boost::uint64_t temp;
                    temp = byte_buf[6];
                    pcr_base  = (temp << 25) & UINT64_CONST(0x00000001FE000000);
                    temp = byte_buf[7];
                    pcr_base  |= (temp << 17) & UINT64_CONST(0x0000000001FE0000);
                    temp = byte_buf[8];
                    pcr_base  |= (temp << 9) & UINT64_CONST(0x000000000001FE00);
                    temp = byte_buf[9];
                    pcr_base  |= (temp << 1) & UINT64_CONST(0x00000000000001FE);
                    temp = (byte_buf[10] & 0x80);
                    pcr_base  |= (temp >> 7) & 0x01;
                    boost::uint64_t pcr_ext;
                    temp = (byte_buf[10] & 0x01);
                    pcr_ext  = (temp << 8) & UINT64_CONST(0x000000000000FF00);
                    temp = byte_buf[11];
                    pcr_ext |= temp;

                    pcr = pcr_base*300 + pcr_ext;
                    ret_val = true;
                }
            }
        }
    }
    return ret_val;
}

bool ts_process_routines::GetEITSectionIds(void* buffer, int /*eit_length*/, unsigned short& NetworkId, unsigned short& TSId, unsigned short& ServiceId)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    NetworkId = ((((unsigned short)byte_buf[10]) << 8) & 0xFF00) | byte_buf[11];
    TSId = ((((unsigned short)byte_buf[8]) << 8) & 0xFF00) | byte_buf[9];
    ServiceId = ((((unsigned short)byte_buf[3]) << 8)  & 0xFF00) | byte_buf[4];
    return true;
}

bool ts_process_routines::SetEITSectionIds(void* buffer, int eit_length, unsigned short NetworkId, unsigned short TSId, unsigned short ServiceId)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    //Update network id value
    if (NetworkId != 0)
    {
        byte_buf[10] = (NetworkId >> 8) & 0xFF;
        byte_buf[11] = NetworkId & 0xFF;
    }
    //Update transport stream id value
    if (TSId != 0)
    {
        byte_buf[8] = (TSId >> 8) & 0xFF;
        byte_buf[9] = TSId & 0xFF;
    }
    //Update service id value
    if (ServiceId != 0)
    {
        byte_buf[3] = (ServiceId >> 8) & 0xFF;
        byte_buf[4] = ServiceId & 0xFF;
    }
    //Recalculate CRC
    unsigned long CRC32=ts_crc_handler::GetCRCHandler()->CalculateCRC(byte_buf, 
        eit_length - 4);
    byte_buf[eit_length - 4]=(unsigned char)((CRC32>>24)&0xFF); //CRC_32 Hi-Hi
    byte_buf[eit_length - 3]=(unsigned char)((CRC32>>16)&0xFF); //CRC_32 Hi-Lo
    byte_buf[eit_length - 2]=(unsigned char)((CRC32>>8)&0xFF); //CRC_32 Lo-Hi
    byte_buf[eit_length - 1]=(unsigned char)(CRC32&0xFF); //CRC_32 Lo-Lo
    return true;
}

bool ts_process_routines::GetTSIDFromPATSection(void* buffer, int /*pat_section*/, unsigned short& TSID)
{
    bool ret_val = true;
    unsigned char* byte_buf = (unsigned char*)buffer;

    TSID = ((((unsigned short)byte_buf[3]) << 8) & 0xFF00) | byte_buf[4];

    return ret_val;
}

bool ts_process_routines::GetPATStreamPIDs(unsigned char* pat_section, int pat_len, std::vector<STSPATServiceInfo>& services)
{
    bool ret_val = true;
    services.clear();

    unsigned short tid;
    GetTSIDFromPATSection(pat_section, pat_len, tid);

    unsigned char* byte_buf = (unsigned char*)pat_section;
    //Get offset to the services description part
    int services_desc_offs = 8;
    //scan PAT for services
    while (services_desc_offs < pat_len - 4)
    {
        unsigned char* cur_buf = byte_buf + services_desc_offs;
        STSPATServiceInfo si;
        si.sid = ((((unsigned short)cur_buf[0]) << 8) & 0xFF00) | cur_buf[1];
        if (si.sid != 0) //skip special entry for NIT pid
        {
            si.pmt_pid = (((((unsigned short)cur_buf[2]) << 8) & 0xFF00) | cur_buf[3]) & 0x1FFF;
            si.tid = tid;
            services.push_back(si);
        }
        //Advance offset pointer
        services_desc_offs += 4;
    }

    return ret_val;
}

int ts_process_routines::GetTextDescriptorLength(unsigned char* name_buf)
{
    int ret_val = 0;

    char* str_buf;
    //determine type of the name encoding according to DVB standard
    char enc_type = name_buf[0];
    if (enc_type >= 0x20 /* && enc_type <= 0xFF */)
    {
        //this is just a normal ASCII string
        str_buf = (char*)name_buf;
        ret_val = (int)strlen(str_buf);
    }
    else
    {
        if (enc_type >= 0x01 && enc_type <= 0x05)
        {
            //first byte defines a coding table according to DVB standard and the rest are 8-bit values
            str_buf = (char*)(name_buf+1);
            ret_val = (int)strlen(str_buf) + 1;
        }
        else
        {
            if (enc_type == 0x10)
            {
                //this is ISO 8859 
                //two following bytes indicate part of the standard and the rest - 8-bit characters
                str_buf = (char*)(name_buf+3);
                ret_val = (int)strlen(str_buf) + 3;
            }
            else
            {
                if (enc_type == 0x11)
                {
                    //this is UNICODE string
                    wchar_t* u_buf = (wchar_t*)(name_buf + 1);
                    ret_val = (int)wcslen(u_buf) + 1;
                }
            }
        }
    }

    return ret_val;
}

int ts_process_routines::GetEITCurNextIndicator(void* buffer, int /*eit_length*/)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    return (byte_buf[5] & 0x01);
}

unsigned char ts_process_routines::GetEITVersion(void* buffer, int /*eit_length*/)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    return ((byte_buf[5] >> 1) & 0x1F);
}

unsigned char ts_process_routines::GetEITCurSectionNum(void* buffer, int /*eit_length*/)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    return byte_buf[6];
}

unsigned char ts_process_routines::GetEITMaxSectionNum(void* buffer, int /*eit_length*/)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    return byte_buf[7];
}

unsigned char ts_process_routines::GetEITMaxSegmentSectionNum(void* buffer, int /*eit_length*/)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    return byte_buf[12];
}

unsigned char ts_process_routines::GetEITLastTableId(void* buffer, int /*eit_length*/)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    return byte_buf[13];
}

unsigned char ts_process_routines::GetEITTableId(void* buffer, int /*eit_length*/)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    return byte_buf[0];
}

//-------------- VCT routines -----------------

bool ts_process_routines::GetTVCTSectionTSID(unsigned char* section, int /*len*/, unsigned short& tid)
{
    tid = ((((unsigned short)section[3]) << 8) & 0xFF00) | section[4];
	return true;
}

bool ts_process_routines::GetTVCTSectionStats(unsigned char* section, int /*len*/, unsigned char& version, unsigned char& cur_next)
{
	version = ((section[5] >> 1) & 0x1F);
    cur_next = (section[5] & 0x01);
	return true;
}

bool ts_process_routines::GetTVCTSectionNumbers(unsigned char* section, int /*len*/, unsigned char& cur_section_num, unsigned char& max_section_num)
{
	cur_section_num = section[6];
    max_section_num = section[7];
	return true;
}

unsigned char ts_process_routines::GetTVCTTableId(unsigned char* section, int /*len*/)
{
    return section[0];
}

bool ts_process_routines::GetTVCTServicesEx(unsigned char* section, int len, std::vector<STSTVCTServiceInfoEx>& services)
{
    bool ret_val = true;
    services.clear();

	//number of channels in the tvct
	unsigned char services_num = section[9];
    //Get offset to the services description part
    int services_desc_offs = 10;
	//walk through all services
	unsigned char cur_service = 0;
	while (cur_service < services_num && services_desc_offs < len - 4)
    {
        unsigned char* cur_buf = section + services_desc_offs;

        STSTVCTServiceInfoEx si;
		//name
		ConvertMultibyteToUC(dvblink::engine::EC_UTF16_BE, (const char*)cur_buf, 14, si.name);
		//ts id
		si.tid = ((((unsigned short)cur_buf[22]) << 8) & 0xFF00) | cur_buf[23];
		//program number
		si.sid = ((((unsigned short)cur_buf[24]) << 8) & 0xFF00) | cur_buf[25];
		//major ch number
		si.mj_num = (((unsigned short)(cur_buf[14] & 0x0F))<< 6) | (((unsigned short)(cur_buf[15] & 0xFC)) >> 2);
		//major ch number
		si.mn_num = (((unsigned short)(cur_buf[15] & 0x03))<< 8) | cur_buf[16];
		//channel type
		unsigned char chtype = (cur_buf[27] & 0x3F);
		if (chtype == 2)
			si.type = 1; //TV
		else if (chtype == 3)
			si.type = 2; //Radio
			else
				si.type = 0; //unknown
		//program source id
		si.source_id = ((((unsigned short)cur_buf[28]) << 8) & 0xFF00) | cur_buf[29];

		if (si.type != 0)
		{
			//add service
			services.push_back(si);
		}
        int service_desc_len = (((unsigned short)(cur_buf[30] & 0x03)) << 8) | cur_buf[31];
        //Advance offset pointer
        services_desc_offs += service_desc_len + 32;

		cur_service += 1;
    }

    return ret_val;
}

//-------------- MGT routines -----------------

bool ts_process_routines::GetMGTTables(unsigned char* section, int len, std::vector<STSMGTTablesInfo>& tables)
{
    bool ret_val = true;
    tables.clear();

	//number of tables in the mgt
	unsigned short tables_num = ((((unsigned short)section[9]) << 8) & 0xFF00) | section[10];
    //Get offset to the tables description part
    int tables_desc_offs = 11;
	//walk through all tables
	unsigned short cur_table = 0;
	while (cur_table < tables_num && tables_desc_offs < len - 4)
    {
        unsigned char* cur_buf = section + tables_desc_offs;

        STSMGTTablesInfo ti;
		//type
		ti.table_type = ((((unsigned short)cur_buf[0]) << 8) & 0xFF00) | cur_buf[1];
        //pid
		ti.table_pid = (((unsigned short)(cur_buf[2] & 0x1F)) << 8) | cur_buf[3];

        tables.push_back(ti);

        int table_desc_len = (((unsigned short)(cur_buf[9] & 0x0F)) << 8) | cur_buf[10];
        //Advance offset pointer
        tables_desc_offs += table_desc_len + 11;

		cur_table += 1;
    }

    return ret_val;
}

int ts_process_routines::GetMGTCurNextIndicator(unsigned char* buffer, int len)
{
    return (buffer[5] & 0x01);
}

//-------------- EIT ATSC routines -----------------

unsigned char ts_process_routines::GetEitAtscTableId(unsigned char* section, int len)
{
    return section[0];
}

int ts_process_routines::GetEitAtscCurNextIndicator(unsigned char* buffer, int len)
{
    return (buffer[5] & 0x01);
}

unsigned short ts_process_routines::GetEitAtscSourceId(unsigned char* section, int len)
{
    return ((((unsigned short)section[3]) << 8) & 0xFF00) | section[4];
}

unsigned char ts_process_routines::GetEitAtscSectionVersion(unsigned char* section, int len)
{
    return (section[5] & 0x3E);
}

unsigned char ts_process_routines::GetEitAtscMaxSectionNum(unsigned char* section, int len)
{
    return section[7];
}

unsigned char ts_process_routines::GetEitAtscSectionNum(unsigned char* section, int len)
{
    return section[6];
}

//-------------- STT routines -----------------
bool ts_process_routines::GetSTTGPSTimeAndOffset(unsigned char* section, int len, boost::uint32_t& gps_time, boost::uint32_t& gps_offset)
{
    gps_time = read_u32bit_from_memory(section + 9, 4, 0xFF);
    gps_offset = read_u32bit_from_memory(section + 13, 1, 0xFF);
    return true;
}

//-------------- ETT routines -----------------

unsigned char ts_process_routines::GetEttAtscTableId(unsigned char* section, int len)
{
    return section[0];
}

int ts_process_routines::GetEttAtscCurNextIndicator(unsigned char* buffer, int len)
{
    return (buffer[5] & 0x01);
}

//-------------- SDT routines -----------------

int ts_process_routines::GetSDTCurNextIndicator(void* buffer, int /*sdt_length*/)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    return (byte_buf[5] & 0x01);
}

unsigned char ts_process_routines::GetSDTVersion(void* buffer, int /*eit_length*/)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    return ((byte_buf[5] >> 1) & 0x1F);
}

unsigned char ts_process_routines::GetSDTCurSectionNum(void* buffer, int /*eit_length*/)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    return byte_buf[6];
}

unsigned char ts_process_routines::GetSDTMaxSectionNum(void* buffer, int /*sdt_length*/)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    return byte_buf[7];
}

unsigned char ts_process_routines::GetSDTTableId(void* buffer, int /*sdt_length*/)
{
    unsigned char* byte_buf = (unsigned char*)buffer;
    return byte_buf[0];
}

bool ts_process_routines::GetSDTSectionIds(void* buffer, int sdt_length, unsigned short& NetworkId, unsigned short& TSId)
{
    bool ret_val = true;
    unsigned char* byte_buf = (unsigned char*)buffer;

    //check if this is really SDT section
    if (GetSDTTableId(buffer, sdt_length) != 0x42 && GetSDTTableId(buffer, sdt_length) != 0x46)
        return false;

    NetworkId = ((((unsigned short)byte_buf[8]) << 8) & 0xFF00) | byte_buf[9];
    TSId = ((((unsigned short)byte_buf[3]) << 8) & 0xFF00) | byte_buf[4];

    return ret_val;
}

bool ts_process_routines::GetSDTServices(unsigned char* sdt_section, int sdt_len, std::vector<unsigned short>& services)
{
    bool ret_val = true;
    services.clear();

    //check if this is really SDT section
    if (GetSDTTableId(sdt_section, sdt_len) != 0x42 && GetSDTTableId(sdt_section, sdt_len) != 0x46)
        return false;

    unsigned char* byte_buf = (unsigned char*)sdt_section;
    //Get offset to the services description part
    int services_desc_offs = 11;
    //scan SDT for services
    while (services_desc_offs < sdt_len - 4)
    {
        unsigned char* cur_buf = byte_buf + services_desc_offs;
        unsigned short service_id = ((((unsigned short)cur_buf[0]) << 8) & 0xFF00) | cur_buf[1];
        int services_desc_len = ((((unsigned short)cur_buf[3]) << 8) & 0x0F00) | cur_buf[4];
        services.push_back(service_id);
        //Advance offset pointer
        services_desc_offs += services_desc_len + 5;
    }

    return ret_val;
}

unsigned char* ts_process_routines::FindDescriptor(unsigned char* desc_ptr, int desc_len, unsigned char desc_tag)
{
    unsigned char* ret_val = NULL;

    int l = desc_len;
    unsigned char* cur_desc = desc_ptr;
    while (l >= 0)
    {
        if (cur_desc[0] == desc_tag)
        {
            ret_val = cur_desc;
            break;
        }
        int cur_desc_len = cur_desc[1] + 2;
        l -= cur_desc_len;
        cur_desc += cur_desc_len;
    }

    return ret_val;
}

bool ts_process_routines::GetSDTServicesEx(unsigned char* sdt_section, int sdt_len, std::vector<STSServiceInfoEx>& services)
{
    bool ret_val = true;
    services.clear();

    //check if this is really SDT section
    if (GetSDTTableId(sdt_section, sdt_len) != 0x42 && GetSDTTableId(sdt_section, sdt_len) != 0x46)
        return false;

    unsigned short nid, tid;
    GetSDTSectionIds(sdt_section, sdt_len, nid, tid);

    unsigned char* byte_buf = (unsigned char*)sdt_section;
    //Get offset to the services description part
    int services_desc_offs = 11;
    //scan SDT for services
    while (services_desc_offs < sdt_len - 4)
    {
        unsigned char* cur_buf = byte_buf + services_desc_offs;

        STSServiceInfoEx si;
        si.nid = nid;
        si.tid = tid;
        si.sid = ((((unsigned short)cur_buf[0]) << 8) & 0xFF00) | cur_buf[1];
        si.encrypted = (cur_buf[3] >> 4) & 0x01;
        int services_desc_len = ((((unsigned short)cur_buf[3]) << 8) & 0x0F00) | cur_buf[4];
        unsigned char* desc_start = cur_buf + 5;

        //find Service description descriptor
        unsigned char* service_descriptor = FindDescriptor(desc_start, services_desc_len, 0x48);
        if (service_descriptor != NULL)
        {
            si.type = service_descriptor[2];
            int nlen = service_descriptor[3];
            ConvertAnnexATextToUnicode(service_descriptor + 4, nlen, si.provider);
            int plen = service_descriptor[4 + nlen];
            ConvertAnnexATextToUnicode(service_descriptor + 4 + nlen + 1, plen, si.name);
			RemoveIllegalXMLCharacters(si.provider);
            if (IsStringEmpty(si.provider.c_str()))
                si.provider = L"Unknown";
			RemoveIllegalXMLCharacters(si.name);
            if (IsStringEmpty(si.name.c_str()))
            {
                //wchar_t buf[255];
                //swprintf_s(buf, L"Service %d", si.sid);
                //si.name = buf;
                si.name = (boost::wformat(L"Service %1%") % si.sid).str();
            }
        }
        else
        {
            //Fill in some dummy values
            si.type = 0;
            si.provider = L"Unknown";
            //wchar_t buf[255];
            //swprintf_s(buf, L"Service %d", si.sid);
            //si.name = buf;
            si.name = (boost::wformat(L"Service %1%") % si.sid).str();
        }
        unsigned char* ca_descriptor = FindDescriptor(desc_start, services_desc_len, 0x53);
        if (ca_descriptor != NULL)
            si.encrypted = 1;
        //Add service
        services.push_back(si);
        //Advance offset pointer
        services_desc_offs += services_desc_len + 5;
    }

    return ret_val;
}

bool ts_process_routines::GetUTCTimeFromTDT(unsigned char* buffer, int length, time_t& retval)
{
    bool res = false;
    if (buffer[0] == TID_TDT && length >= 8) //length is always 8 bytes
    {
        //get start date
        WORD mjd = (((WORD)buffer[3]) << 8) | buffer[4];
        //get start time
        int total_sec;
        GetHHMMSSFromBCD(buffer+5, NULL, NULL, NULL, &total_sec);
        retval = MJD2time_t(mjd, total_sec);
        res = true;
    }
    return res;
}

bool ts_process_routines::GetUTCTimeFromTOT(unsigned char* buffer, int length, time_t& retval)
{
    bool res = false;
    if (buffer[0] == TID_TOT)
    {
        //get start date
        WORD mjd = (((WORD)buffer[3]) << 8) | buffer[4];
        //get start time
        int total_sec;
        GetHHMMSSFromBCD(buffer+5, NULL, NULL, NULL, &total_sec);
        retval = MJD2time_t(mjd, total_sec);
        res = true;
    }
    return res;
}

void ts_process_routines::InsertPCRInPacket(unsigned char* packet_ptr, boost::uint64_t pcr, bool update_adaptation_field)
{
    if (update_adaptation_field)
    {
        //set adaptation field
        packet_ptr[3] |= 0x30;
        //Adaptation field length
        packet_ptr[4] = 0x07;
        //Adaptation field flags
        packet_ptr[5] = 0x10;
    }
    //New PCR value
    boost::uint64_t pcr_base = pcr / 300;
    unsigned short pcr_ext = (unsigned short)(pcr % 300);
    pcr_base = pcr_base % UINT64_CONST(0x1FFFFFFFF); // 33 bit
    packet_ptr[6]=(unsigned char)((pcr_base>>(24+1)) & 0xFF); // PCR_base value 33 bit
    packet_ptr[7]=(unsigned char)((pcr_base>>(16+1))&0xFF);
    packet_ptr[8]=(unsigned char)((pcr_base>>(8+1))&0xFF);
    packet_ptr[9]=(unsigned char)((pcr_base>>1)&0xFF);
    packet_ptr[10]=(unsigned char)(((pcr_base&0x01)<<7) & 0x10);
    packet_ptr[10] |= (pcr_ext >> 8) & 0x01;
    packet_ptr[11] = pcr_ext & 0xFF;
}

//----------------------------------------------------------------

boost::int64_t ps_process_routines::GetPTSValueFromPES(void* buffer)
{
    boost::int64_t ret_val = -1;
    unsigned char* byte_buf = (unsigned char*)buffer;
    if (byte_buf[0] == 0x00 &&
        byte_buf[1] == 0x00 &&
        byte_buf[2] == 0x01)
    {
        //check if this header has PTS value
        if ((byte_buf[7] & 0x80) == 0x80)
        {
            //retreive PTS value
            boost::uint64_t t;
            //SCR base
            boost::uint64_t pts;
            t = (byte_buf[9]&0x0E);
            pts = (t << 29) & UINT64_CONST(0x0000001FE0000000);
            t = byte_buf[10];
            pts |= (t << 22) & UINT64_CONST(0x000000003FC00000);
            t = byte_buf[11] & 0xFE;
            pts |= (t << 14) & UINT64_CONST(0x00000000003FC000);
            t = byte_buf[12];
            pts |= (t << 7) &  UINT64_CONST(0x0000000000007F80);
            t = byte_buf[13] & 0xFE;
            pts |= (t >> 1) & 0x7F;

            ret_val = pts*300;

        }
    }
    return ret_val;
}

boost::int64_t ps_process_routines::GetDTSValueFromPES(void* buffer)
{
    boost::int64_t ret_val = -1;
    unsigned char* byte_buf = (unsigned char*)buffer;
    if (byte_buf[0] == 0x00 &&
        byte_buf[1] == 0x00 &&
        byte_buf[2] == 0x01)
    {
        //check if this header has DTS value
        if ((byte_buf[7] & 0x40) == 0x40)
        {
            //retreive DTS value
            boost::uint64_t t;
            //SCR base
            boost::uint64_t dts;
            t = (byte_buf[14]&0x0E);
            dts = (t << 29) & UINT64_CONST(0x0000001FE0000000);
            t = byte_buf[15];
            dts |= (t << 22) & UINT64_CONST(0x000000003FC00000);
            t = byte_buf[16] & 0xFE;
            dts |= (t << 14) & UINT64_CONST(0x00000000003FC000);
            t = byte_buf[17];
            dts |= (t << 7) &  UINT64_CONST(0x0000000000007F80);
            t = byte_buf[18] & 0xFE;
            dts |= (t >> 1) & 0x7F;

            ret_val = dts*300;

        }
    }
    return ret_val;
}

void ps_process_routines::SetPTSInPES(void* buffer, boost::int64_t value)
{
    unsigned char mask = 0x20;

    unsigned char* byte_buf = (unsigned char*)buffer;
    if ((byte_buf[7] & 0x40) == 0x40)
        mask = 0x30;

    SetxTSInPES(buffer, value, 9, mask);
}

void ps_process_routines::SetDTSInPES(void* buffer, boost::int64_t value)
{
    SetxTSInPES(buffer, value, 14, 0x10);
}

void ps_process_routines::SetxTSInPES(void* buffer, boost::int64_t value, int offset, unsigned char start_mask)
{
    value = value / 300;

    unsigned char* byte_buf = (unsigned char*)buffer;

    //set 5 bytes to zero as initial value
    memset(byte_buf + offset, 0, 5);

    //start_mask == 0x20 or 0x30
    byte_buf[offset] = (unsigned char)((value >> 29) & 0x0E) | start_mask | 0x01;
    byte_buf[offset + 1] = (unsigned char)((value >> 22) & 0xFF);
    byte_buf[offset + 2] = (unsigned char)((value >> 14) & 0xFE) | 0x01;
    byte_buf[offset + 3] = (unsigned char)((value >> 7) & 0xFF);
    byte_buf[offset + 4] = (unsigned char)((value << 1) & 0xFE) | 0x01;
}

int ps_process_routines::GetPackID(unsigned char* buffer)
{
    return buffer[3];
}

boost::uint64_t ps_process_routines::GetPSPackHeaderSCR(unsigned char* buffer)
{
    boost::uint64_t ret_val = static_cast<boost::uint64_t>(-1);

    boost::uint64_t t;
    //SCR base
    boost::uint64_t scr_base;
    t = (buffer[4]&0x38);
    scr_base = (t << 27) & UINT64_CONST(0x00000007F8000000);
    t = buffer[4]&0x03;
    scr_base |= (t << 28) & UINT64_CONST(0x0000000FF0000000);
    t = buffer[5];
    scr_base |= (t << 20) & UINT64_CONST(0x000000000FF00000);
    t = buffer[6] & 0xF8;
    scr_base |= (t << 12) & UINT64_CONST(0x00000000000FF000);
    t = buffer[6] & 0x03;
    scr_base |= (t << 13) & UINT64_CONST(0x00000000001FE000);
    t = buffer[7];
    scr_base |= (t << 5) & UINT64_CONST(0x0000000000001FE0);
    t = buffer[8] & 0xF8;
    scr_base |= (t >> 3) & 0x1F;
    //SCR extension
    boost::uint64_t scr_ext;
    t = buffer[8] & 0x03;
    scr_ext = (t << 7) & UINT64_CONST(0x0000000000007F80);
    t = buffer[9] & 0xFE;
    scr_ext |= (t >> 1) & 0x7F;

    ret_val = scr_base*300 + scr_ext;

    return ret_val;
}

int ps_process_routines::GetPSPackHeaderMuxRate(unsigned char* buffer)
{
    int ret_val;
    int t = buffer[10];
    ret_val = (t << 14) & 0x003FC000;
    t = buffer[11];
    ret_val |= (t << 6) & 0x00003FC0;
    t = buffer[12] & 0xFC;
    ret_val |= (t >> 2) & 0x3F;

    return ret_val*50;
}

int ps_process_routines::GetESDataOffset(unsigned char* buffer)
{
    //offset consists of the following parts:
    //1) startcode prefix + stream id + packet length + flags + header length (9 bytes in total)
    //2) PES standard data (depending on the flags)
    //3) PES extension data (depending on the flags)
    //4) PES extension data 2 (with explicit length field)
    int ret_val = 9;
    unsigned char flags = buffer[7];
    if ((flags & 0x80) == 0x80)
    {
        //Add PTS length
        ret_val += 5;
    }
    if ((flags & 0x40) == 0x40)
    {
        //Add DTS length
        ret_val += 5;
    }
    if ((flags & 0x20) == 0x20)
    {
        //Add ESCR length
        ret_val += 6;
    }
    if ((flags & 0x10) == 0x10)
    {
        //Add ES rate length
        ret_val += 3;
    }
    if ((flags & 0x08) == 0x08)
    {
        //Add DSM trick mode length
        ret_val += 1;
    }
    if ((flags & 0x04) == 0x04)
    {
        //Add additional copy length length
        ret_val += 1;
    }
    if ((flags & 0x02) == 0x02)
    {
        //Add PES CRC length
        ret_val += 2;
    }
    if ((flags & 0x01) == 0x01)
    {
        //PES extension flag is set
        int pes_ext_offset = ret_val;
        ret_val += 1; //account for flags field
        flags = buffer[pes_ext_offset];
        if ((flags & 0x80) == 0x80)
        {
            //Add PES private data length
            ret_val += 16;
        }
        if ((flags & 0x40) == 0x40)
        {
            //Add pack header field length
            int l = buffer[ret_val];
            ret_val += l + 1; //add also length field itself
        }
        if ((flags & 0x20) == 0x20)
        {
            //Add program packet seq counter length
            ret_val += 2;
        }
        if ((flags & 0x10) == 0x10)
        {
            //Add PSTD buffer length
            ret_val += 2;
        }
        if ((flags & 0x10) == 0x10)
        {
            //Add PES extension 2 length
            int l = buffer[ret_val] & 0x7F;
            ret_val += l + 1; //add also length field itself
        }
    }
    return ret_val;
}

bool ps_process_routines::IsValidPESPacketStart(unsigned char* buffer, int /*buf_len*/, EPS_PROC_STREAM_TYPES stream_type)
{
    bool ret_val = false;
    //check the start pattern first
    if (buffer[0] == 0x00 && buffer[1] == 0x00 && buffer[2] == 0x01)
    {
        //check the valid range (pattern) for stream id
        bool b = false;
        unsigned char stream_id = buffer[3];

        if (stream_type != EPS_PST_UNKNOWN)
        {
            switch (stream_type)
            {
            case EPS_PST_VIDEO_MPEG2:
                if (((stream_id >> 4) & 0x0F) == 0x0E)
                    b = true;
                break;
            case EPS_PST_VIDEO_MPEG4:
                assert(false);
                break;
            case EPS_PST_AUDIO_MPEG2:
                if (((stream_id >> 5) & 0x07) == 0x06)
                    b = true;
                break;
            case EPS_PST_AUDIO_AC3:
                if (stream_id == 0xBD)
                    b = true;
                break;
            case EPS_PST_TELETEXT:
                if (stream_id == 0xBD)
                    b = true;
                break;
            default:
                assert(false);
                break;
            }
        }
        else
        {
            //check all possible stream types
            b = (((stream_id >> 4) & 0x0F) == 0x0E) ||
                stream_id == 0xBD ||
                stream_id == 0xBE ||
                stream_id == 0xBF ||
                (((stream_id >> 5) & 0x07) == 0x06) ||
                stream_id == 0xF0 ||
                stream_id == 0xF1 ||
                stream_id == 0xF2 ||
                stream_id == 0xF3 ||
                stream_id == 0xF4 ||
                stream_id == 0xF5 ||
                stream_id == 0xF6 ||
                stream_id == 0xF7 ||
                stream_id == 0xF8 ||
                stream_id == 0xF9;
        }
        if (b)
        {
            //So far so good
            //Check '10' pattern in the flags byte now
            if (((buffer[6] >> 6) & 0x03) == 2)
            {
                //At this moment we are sure enough that this is a beginning od the PES packet
                ret_val = true;
                //But, do some extra checks just to be sure
                //If PTS or DTS is present, they also begin with a certain bit-pattern
                unsigned char pts_dts_flags = (buffer[7] >> 6) & 0x03;
                if (pts_dts_flags != 1)
                {
                    //check the PTS and DTS start patterns
                    if (pts_dts_flags == 2)
                    {
                        //PTS only
                        if (((buffer[9] >> 4) & 0x0F) != 2)
                        {
                            ret_val = false;
                            //log error
                            log_error(L"ps_process_routines::IsValidPESPacketStart. Warning. Possible PES packet start was dropped because of wrong PTS pattern");
                        }
                    }
                    if (pts_dts_flags == 3)
                    {
                        //PTS and DTS
                        if ((((buffer[9] >> 4) & 0x0F) != 3) || (((buffer[14] >> 4) & 0x0F) != 1))
                        {
                            ret_val = false;
                            //log error
                            log_error(L"ps_process_routines::IsValidPESPacketStart. Warning. Possible PES packet start was dropped because of wrong PTS and DTS pattern");
                        }
                    }
                }
                else
                {
                    //This value is not allowed
                    ret_val = false;
                    //log error
                    log_error(L"ps_process_routines::IsValidPESPacketStart. Warning. Possible PES packet start was dropped because of wrong PTS_DTS field value");
                }
            }
        }
    }

    return ret_val;
}

//-------------------------- ES Video ----------------------

bool pes_mpeg12_video_process_routines::CheckVideoSequenceHdr(unsigned char* buffer)
{
    bool ret_val = false;

    //Get ES data offset
    int es_data_offs = ps_process_routines::GetESDataOffset(buffer);
    //we should check this offset against length of PES itself - TODO
    //Check whether ES data starts with 0x000001B3
    unsigned char* es_data_ptr = buffer + es_data_offs;
    if (es_data_ptr[0] == 0x00 &&
        es_data_ptr[1] == 0x00 &&
        es_data_ptr[2] == 0x01 &&
        es_data_ptr[3] == 0xB3)
    {
        ret_val = true;
    }

    return ret_val;
}

//-------------------------- ES MPEG1/2 Audio ----------------------

bool pes_mpeg12_audio_process_routines::CheckAudioFrameStart(unsigned char* buffer)
{
    bool ret_val = false;

    //Get ES data offset
    int es_data_offs = ps_process_routines::GetESDataOffset(buffer);
    //TODO - add actual implementation of this function
    unsigned char* buf = buffer + es_data_offs;
    if (buf[0] == 0xFF && ((buf[1] & 0xF0) == 0xF0))
    {
        ret_val = true;
    }

    return ret_val;
}

//-------------------------- Payload parser ----------------------

ts_payload_parser::ts_payload_parser()
{
    m_Payload.m_PayloadData = NULL;
}

ts_payload_parser::~ts_payload_parser()
{
    delete [] m_Payload.m_PayloadData;
}

bool ts_payload_parser::Init(unsigned short pid)
{
    if (m_Payload.m_PayloadData != NULL)
    {
        delete [] m_Payload.m_PayloadData;
    }
    //allocate memory for payload
    int payload_size = GetPayloadMaxLength();
    m_Payload.m_PayloadData = new unsigned char[payload_size];
    //Initialize variables
    m_PID = pid;

    Reset();
    return 1;
}

void ts_payload_parser::Reset()
{
    m_Payload.m_Length = 0;
    m_State = BDAC_PPS_WAITING_FIRST_SECTION_PACKET;
}

void ts_payload_parser::ResetFoundSections(ts_payload_parser::ts_section_list& found_sections)
{
	ts_section_list::iterator it = found_sections.begin();
	while (it != found_sections.end())
	{
		delete [] (it->section);
		it++;
	}
	found_sections.clear();
}

int ts_payload_parser::AddPacket(const unsigned char* packet, int length, ts_payload_parser::ts_section_list& found_sections)
{
	ResetFoundSections(found_sections);
    //check PID first and error indocator
    if (ts_process_routines::GetPacketPID(packet) == m_PID &&
		ts_process_routines::GetTSErrorIndicator(packet) == 0)
    {
        switch (m_State)
        {
        case BDAC_PPS_IDLE:
        case BDAC_PPS_READY:
            //do nothing
            break;
        case BDAC_PPS_WAITING_FIRST_SECTION_PACKET:
            //check payload start indicator - it should be 1
            if (ts_process_routines::GetPayloadStartIndicator(packet) == 1)
            {
                //retrieve continuity counter
                m_TSContinuityCounter = ts_process_routines::GetContinuityCounter(packet);
                //search the amount of section data, present in this packet
                int data_start_offset = GetPayloadDataOffset(packet);
                if (data_start_offset != -1)
                {
                    //calculate data length
                    int data_length = TS_PACKET_SIZE - data_start_offset;
                    //take precaution not to overwrite the buffer tale
                    if (data_length > 0 && data_length <= TS_PACKET_SIZE)
                    {
                        //copy data into our internal buffer
                        memcpy(m_Payload.m_PayloadData, packet + data_start_offset, data_length);
                        m_Payload.m_Length = data_length;
						//indicate that we have initiali data and looking for the rest of the packets
                        m_State = BDAC_PPS_COLLECTING_ALL_SECTION_PACKETS;
                    }
                }
            }
			break;
        case BDAC_PPS_COLLECTING_ALL_SECTION_PACKETS:
			{
				//retrieve the continuity counter
				unsigned short cont_counter = ts_process_routines::GetContinuityCounter(packet);
				//check whether this is the same duplicated packet
				if (cont_counter != m_TSContinuityCounter)
				{
					//check whether this is a next packet
					if (cont_counter == ts_process_routines::GetNextContinuityCounter(m_TSContinuityCounter))
					{
						m_TSContinuityCounter = cont_counter;
						int payload_offs = GetPayloadFirstByteOffset(packet);
						if (payload_offs != -1)
						{
							if (payload_offs > 0 && payload_offs < TS_PACKET_SIZE)
							{
								int payload_size = TS_PACKET_SIZE - payload_offs;
								if (m_Payload.m_Length + payload_size <= GetPayloadMaxLength())
								{
									memcpy(m_Payload.m_PayloadData + m_Payload.m_Length, packet + payload_offs, payload_size);
									m_Payload.m_Length += payload_size;
								}
                                else
								{
									Reset();
								}
							}
                            else
							{
								Reset();
							}
						}
                        else
						{
							//log error
							log_error(L"ts_payload_parser::AddPacket(PID = %d). Error. GetPayloadFirstByteOffset returned -1") % m_PID;
							Reset();
						}
					}
                    else
					{
						//there is continuity error - restart parsing sequence
						Reset();
					}
				}
			}
			break;
		}
		//check for the completed sections
		while (m_Payload.m_Length > 0)
		{
			//check first whether the section size is ok
			int section_size = GetPayloadLength();
			if (section_size > GetPayloadMaxLength())
			{
				//bad packet. Simply reset the buffer
				Reset();
			}
            else
			{
				if (CheckIfPayloadComplete(section_size))
				{
					if (CheckFoundData(m_Payload.m_PayloadData, section_size))
					{
						ts_section_info si;
						si.length = section_size;
						si.section = new unsigned char[section_size];
						memcpy(si.section, m_Payload.m_PayloadData, section_size);
						found_sections.push_back(si);
					}

					m_Payload.m_Length -= section_size;
					if (m_Payload.m_Length > 0)
					{
						//if this packet has no "payload start indicator" it means that the rest can be discarded
						//(there can be no new section in this packet)
						if (ts_process_routines::GetPayloadStartIndicator(packet) == 1)
						{
							memmove(m_Payload.m_PayloadData, m_Payload.m_PayloadData + section_size, m_Payload.m_Length);
							if (m_Payload.m_PayloadData[0] == 0xFF) //discard data if it starts with 0xFF - there is no PES or section that starts with it
								Reset();
						}
						else
							Reset();
					}
					else
					{
						Reset();
					}
				}
                else
				{
					//the section is incomplete. break and wait for new packets
					break;
				}
			}
		}
	}
	return found_sections.size();
}

bool ts_payload_parser::CheckIfPayloadComplete(int& section_size)
{
    bool bRes = false;
	section_size = 0;

    int section_length = GetPayloadLength();
    if (section_length > 0)
    {
        //retrieve length of the section
        if (m_Payload.m_Length >= section_length)
        {
			section_size = section_length;
            bRes = true;
        }
    }
    return bRes;
}

//-------------------------- PES payload parser ----------------------

ts_pes_payload_parser::ts_pes_payload_parser() :
ts_payload_parser()
{
}

ts_pes_payload_parser::~ts_pes_payload_parser()
{
}

int ts_pes_payload_parser::GetPayloadMaxLength()
{
    return 65536 + 376;
}

int ts_pes_payload_parser::GetPayloadLength()
{
    int ret_val = 0;
    if (m_Payload.m_Length >= 6)
    {
        ret_val =  (((((unsigned short)m_Payload.m_PayloadData[4]) << 8) & 0xFF00) | m_Payload.m_PayloadData[5]) + 6;
    }
    return ret_val;
}

int ts_pes_payload_parser::GetPayloadDataOffset(const void* buffer)
{
    return ts_process_routines::GetPayloadOffset(buffer);
}

int ts_pes_payload_parser::GetPayloadFirstByteOffset(const void* buffer)
{
    return ts_process_routines::GetPayloadOffset(buffer);
}

bool ts_pes_payload_parser::CheckFoundData(void* buffer, int len)
{
	return true;
}

//-------------------------- Section payload parser ------------------
ts_section_payload_parser::ts_section_payload_parser() : 
ts_payload_parser()
{
}

ts_section_payload_parser::~ts_section_payload_parser()
{
}

int ts_section_payload_parser::GetPayloadLength()
{
    int ret_val = 0;
    if (m_Payload.m_Length >= 3)
    {
        ret_val =  ((((((unsigned short)m_Payload.m_PayloadData[1]) << 8) & 0xFF00) | m_Payload.m_PayloadData[2]) & 0x0FFF) + 3;
    }
    return ret_val;
}

int ts_section_payload_parser::GetPayloadMaxLength()
{
    return 8192;
}

int ts_section_payload_parser::GetPayloadDataOffset(const void* buffer)
{
    return ts_process_routines::GetSectionDataOffset(buffer);
}

int ts_section_payload_parser::GetPayloadFirstByteOffset(const void* buffer)
{
    int offs = ts_process_routines::GetPayloadOffset(buffer);
    if (offs != -1 && ts_process_routines::GetPayloadStartIndicator(buffer) == 1)
        offs += 1; //count section payload offset byte
    return offs;
}

bool ts_section_payload_parser::CheckFoundData(void* buffer, int len)
{
    bool check_crc = true;
    unsigned char* charbuf = (unsigned char*)buffer;
    if (len >= 1 && m_PID == PID_TDT && charbuf[0] == TID_TDT)
        check_crc = false; //this is TDT section, which does not have a CRC
	if (check_crc && !ts_process_routines::CheckSectionCRC(buffer))
	{
		log_ext_info(L"ts_section_payload_parser::CheckFoundData. Bad section CRC");
		return false;
	}
	return true;
}

//----------------------------- ts_packet_generator -------------------------------
ts_packet_generator::ts_packet_generator()
{
}

ts_packet_generator::~ts_packet_generator()
{
}

void ts_packet_generator::IncreaseVersionNumber(unsigned short& version)
{
    version += 1;
    if (version > 32)
        version = 1;
}

unsigned char* ts_packet_generator::CreatePATPacket(int& packet_length, unsigned char version, 
    unsigned short* cont_counter, unsigned short tid, unsigned short sid, unsigned short pmt_pid)
{
    packet_length = TS_PACKET_SIZE;
    //Fill the packet with 0xFFs
    memset(m_TSPacket, 0xFF, TS_PACKET_SIZE);

    int packet_total_length_counter = 0;
    //sync byte
    m_TSPacket[0] = SYNC_BYTE;
    packet_total_length_counter += 1;
    //error indicator, payload start indicator, transport priority and PID
    m_TSPacket[1] = 0x40;
    packet_total_length_counter += 1;
    m_TSPacket[2] = 0x00;
    packet_total_length_counter += 1;
    //scrambling control, adaptation field, continuity counter
    m_TSPacket[3] = 0x10 | (*cont_counter & 0x0F);
    packet_total_length_counter += 1;
    *cont_counter = ts_process_routines::GetNextContinuityCounter(*cont_counter);
    //offset
    m_TSPacket[4] = 0;
    packet_total_length_counter += 1;
    //Table Id == 0x00 (PAT)
    m_TSPacket[5] = 0x00;
    packet_total_length_counter += 1;
    //Next two bytes - do not forget to set whole section length afterwards!! (Bytes 6 and 7)
    packet_total_length_counter += 2;
    //start whole section counter.
    int whole_section_counter = packet_total_length_counter;
    //Transport stream ID (Hi)
    m_TSPacket[8] = (tid >> 8) & 0xFF;
    packet_total_length_counter += 1;
    //Transport stream ID (Lo)
    m_TSPacket[9] = tid & 0xFF;
    packet_total_length_counter += 1;
    //Reserved, Version number, Current-Next indicator (always set to 1)
    m_TSPacket[10]=(unsigned char)(0xC0|(((version&0x1F)<<1) & 0xFE) | 0x01);
    packet_total_length_counter += 1;
    //Section number
    m_TSPacket[11]=0x00;
    packet_total_length_counter += 1;
    //Last section number
    m_TSPacket[12]=0x00;
    packet_total_length_counter += 1;
    //Program number (hi)
    m_TSPacket[13]=(sid >> 8) & 0xFF;
    packet_total_length_counter += 1;
    //Program number (lo)
    m_TSPacket[14]= sid & 0xFF;
    packet_total_length_counter += 1;
    //PMT PID Hi
    m_TSPacket[15]=(unsigned char)(0xE0|((pmt_pid >> 8) & 0x1F));
    packet_total_length_counter += 1;
    //PMT PID Lo
    m_TSPacket[16]= pmt_pid & 0xFF;
    packet_total_length_counter += 1;
    //Add section length (bytes 6 and 7)
    whole_section_counter = packet_total_length_counter - whole_section_counter + 4; //+CRC
    m_TSPacket[6] = 0xB0 | (((whole_section_counter & 0x0F00) >> 8) & 0xFF);
    m_TSPacket[7] = whole_section_counter & 0x00FF;
    //Add CRC
    unsigned long CRC32=ts_crc_handler::GetCRCHandler()->CalculateCRC(m_TSPacket + 5, 
        packet_total_length_counter - 5);
    m_TSPacket[packet_total_length_counter]=(unsigned char)((CRC32>>24)&0xFF); //CRC_32 Hi-Hi
    m_TSPacket[packet_total_length_counter + 1]=(unsigned char)((CRC32>>16)&0xFF); //CRC_32 Hi-Lo
    m_TSPacket[packet_total_length_counter + 2]=(unsigned char)((CRC32>>8)&0xFF); //CRC_32 Lo-Hi
    m_TSPacket[packet_total_length_counter + 3]=(unsigned char)(CRC32&0xFF); //CRC_32 Lo-Lo

    return m_TSPacket;
}

unsigned char* ts_packet_generator::CreateNITPacket(int& packet_length, unsigned char version, unsigned short* cont_counter, 
                                          STSStreamInfo* stream_info, SDVBTChannelInfo* dvbt_info,
                                          TDVBTLCNList* lcn_list /*=NULL*/, unsigned char section_pid /*0x40*/)
{
    packet_length = TS_PACKET_SIZE;

    //Fill the packet with 0xFFs
    memset(m_TSPacket, 0xFF, TS_PACKET_SIZE);

    int packet_total_length_counter = 0;
    //sync byte
    m_TSPacket[packet_total_length_counter] = SYNC_BYTE;
    packet_total_length_counter += 1;
    //error indicator, payload start indicator, transport priority and PID
    m_TSPacket[packet_total_length_counter] = 0x40;
    packet_total_length_counter += 1;
    m_TSPacket[packet_total_length_counter] = 0x10;
    packet_total_length_counter += 1;
    //scrambling control, adaptation field, continuity counter
    m_TSPacket[packet_total_length_counter] = 0x10 | (*cont_counter & 0x0F);
    packet_total_length_counter += 1;
    *cont_counter = ts_process_routines::GetNextContinuityCounter(*cont_counter);
    //offset
    m_TSPacket[packet_total_length_counter] = 0;
    packet_total_length_counter += 1;
    //Table Id
    m_TSPacket[packet_total_length_counter] = section_pid; //0x40;
    packet_total_length_counter += 1;
    //Next two bytes - do not forget to set whole section length afterwards!! (Bytes 6 and 7)
    packet_total_length_counter += 2;
    //start whole section counter.
    int whole_section_counter = packet_total_length_counter;
    //original network ID (hi)
    m_TSPacket[packet_total_length_counter] = (stream_info->NetworkId >> 8) & 0xFF;
    packet_total_length_counter += 1;
    //original network ID (Lo)
    m_TSPacket[packet_total_length_counter] = stream_info->NetworkId & 0xFF;
    packet_total_length_counter += 1;
    //Reserved, Version number, Current-Next indicator (always set to 1)
    m_TSPacket[packet_total_length_counter]=(unsigned char)(0xC0|(((version&0x1F)<<1) & 0xFE) | 0x01);
    packet_total_length_counter += 1;
    //section number
    m_TSPacket[packet_total_length_counter]=0x00;
    packet_total_length_counter += 1;
    //last section number
    m_TSPacket[packet_total_length_counter]=0x00;
    packet_total_length_counter += 1;
    //do not forget to fill in network descriptors length (Bytes 13 and 14)
    packet_total_length_counter += 2;
    //Start network descriptors counter
    int net_desc_counter = packet_total_length_counter;
    //Network name descriptor
    //descriptor tag
    m_TSPacket[packet_total_length_counter] = 0x40;
    packet_total_length_counter += 1;
    const char* network_name = "DVBLink Network";

    //Descriptor length
    unsigned char name_desc_len = static_cast<unsigned char>(strlen(network_name));
    m_TSPacket[packet_total_length_counter] = name_desc_len;
    packet_total_length_counter += 1;

    //name itself
    memcpy(m_TSPacket + packet_total_length_counter, network_name, name_desc_len);
    packet_total_length_counter += name_desc_len;

    //LCN descriptor
    if (lcn_list != NULL)
    {
        //descriptor tag
        m_TSPacket[packet_total_length_counter] = 0x83;
        packet_total_length_counter += 1;
        int lcn_desc_len_offs = packet_total_length_counter;
        packet_total_length_counter += 1;
        //do not forget to fill the descriptor in afterwards!
        for (size_t i=0; i<lcn_list->size(); i++)
        {
            //service id hi
            m_TSPacket[packet_total_length_counter] = (lcn_list->at(i).service_id >> 8) & 0xFF;
            packet_total_length_counter += 1;
            //service id lo
            m_TSPacket[packet_total_length_counter] = lcn_list->at(i).service_id & 0xFF;
            packet_total_length_counter += 1;
            //lcn hi + visibility + reserved
            m_TSPacket[packet_total_length_counter] = 0xFC | ((lcn_list->at(i).lcn >> 8) & 0xFF);
            packet_total_length_counter += 1;
            //lcn lo
            m_TSPacket[packet_total_length_counter] = lcn_list->at(i).lcn & 0xFF;
            packet_total_length_counter += 1;
        }

        //descriptor length 
        unsigned char len = static_cast<unsigned char>(packet_total_length_counter - lcn_desc_len_offs - 1);
        m_TSPacket[lcn_desc_len_offs] = len;
    }
    net_desc_counter = packet_total_length_counter - net_desc_counter;
    //fill in network descriptor length
    m_TSPacket[13] = 0xF0 | (((net_desc_counter & 0x0F00) >> 8) & 0xFF);
    m_TSPacket[14] = net_desc_counter & 0x00FF;
    int transport_stream_loop_offset = packet_total_length_counter;
    //do not forget to fill in transport stream loop length (Bytes transport_stream_loop_offset 
    //and transport_stream_loop_offset + 1)
    packet_total_length_counter += 2;
    //Start transport stream loop counter
    int tr_stream_loop_counter = packet_total_length_counter;
    //Transport stream ID (Hi)
    m_TSPacket[packet_total_length_counter] = (stream_info->TSID >> 8) & 0xFF;
    packet_total_length_counter += 1;
    //Transport stream ID (Lo)
    m_TSPacket[packet_total_length_counter] = stream_info->TSID & 0xFF;
    packet_total_length_counter += 1;
    //original network ID (hi)
    m_TSPacket[packet_total_length_counter] = (stream_info->NetworkId >> 8) & 0xFF;
    packet_total_length_counter += 1;
    //original network ID (Lo)
    m_TSPacket[packet_total_length_counter] = stream_info->NetworkId & 0xFF;
    packet_total_length_counter += 1;
    //do not forget to fill in transport decriptors length (Bytes transport_stream_loop_offset + 6 and 
    //transport_stream_loop_offset + 7)
    packet_total_length_counter += 2;
    //start transport descriptors counter
    int tr_desc_counter = packet_total_length_counter;
    //Service list descriptor
    m_TSPacket[packet_total_length_counter] = 0x41;
    packet_total_length_counter += 1;
    //descriptor length
    m_TSPacket[packet_total_length_counter] = 3;//3 bytes - 2 bytes for id and 1 bytes for type
    packet_total_length_counter += 1;
    //Program ID (Hi)
    m_TSPacket[packet_total_length_counter] = (stream_info->currentProgram >> 8) & 0xFF;
    packet_total_length_counter += 1;
    //Program ID (Lo)
    m_TSPacket[packet_total_length_counter] = stream_info->currentProgram & 0xFF;
    packet_total_length_counter += 1;
    //Program Type
    m_TSPacket[packet_total_length_counter] = (unsigned char)stream_info->channelType;
    packet_total_length_counter += 1;
    //DVB-T network descriptor
    //Table Id
    m_TSPacket[packet_total_length_counter] = 0x5A;
    packet_total_length_counter += 1;
    //descriptor length
    m_TSPacket[packet_total_length_counter] = 11;
    packet_total_length_counter += 1;
    //Fill in the rest of the DVB-T network descriptor
    ts_process_routines::FillDVBTNetworkDescriptor(m_TSPacket+transport_stream_loop_offset + 15,
        dvbt_info);
    packet_total_length_counter += 11;

    //Write transport descriptors length
    tr_desc_counter = packet_total_length_counter - tr_desc_counter;
    m_TSPacket[transport_stream_loop_offset + 6] = 0xF0 | (((tr_desc_counter & 0x0F00) >> 8) & 0xFF);
    m_TSPacket[transport_stream_loop_offset + 7] = tr_desc_counter & 0x00FF;

    //Write transport stream loop counter
    tr_stream_loop_counter = packet_total_length_counter - tr_stream_loop_counter;
    m_TSPacket[transport_stream_loop_offset] = 0xF0 | (((tr_stream_loop_counter & 0x0F00) >> 8) & 0xFF);
    m_TSPacket[transport_stream_loop_offset + 1] = tr_stream_loop_counter & 0x00FF;

    //Write whole section counter
    whole_section_counter = packet_total_length_counter - whole_section_counter + 4; //+CRC
    m_TSPacket[6] = 0xF0 | (((whole_section_counter & 0x0F00) >> 8) & 0xFF);
    m_TSPacket[7] = whole_section_counter & 0x00FF;
    //add CRC
    unsigned long CRC32=ts_crc_handler::GetCRCHandler()->CalculateCRC(m_TSPacket + 5, 
        packet_total_length_counter - 5);
    m_TSPacket[packet_total_length_counter]=(unsigned char)((CRC32>>24)&0xFF); //CRC_32 Hi-Hi
    m_TSPacket[packet_total_length_counter + 1]=(unsigned char)((CRC32>>16)&0xFF); //CRC_32 Hi-Lo
    m_TSPacket[packet_total_length_counter + 2]=(unsigned char)((CRC32>>8)&0xFF); //CRC_32 Lo-Hi
    m_TSPacket[packet_total_length_counter + 3]=(unsigned char)(CRC32&0xFF); //CRC_32 Lo-Lo

    return m_TSPacket;
}

unsigned char* ts_packet_generator::CreatePMTSection(int& section_length, unsigned char version, STSPMTSectionInfo* stream_info)
{
    //PMT can only be created if audio stream descriptors are already parsed out of original PMT
    memset(m_SectionBuffer, 0xFF, sizeof(m_SectionBuffer));
    //Create whole PMT section first
    int packet_total_length_counter = 0;
    //Table Id == 0x02 (PMT)
    m_SectionBuffer[packet_total_length_counter] = 0x02;
    packet_total_length_counter += 1;
    //Next two bytes - do not forget to set whole section length afterwards!! (Bytes 1 and 2)
    packet_total_length_counter += 2;
    //start whole section counter.
    int whole_section_counter = packet_total_length_counter;
    //Program number (Hi)
    m_SectionBuffer[packet_total_length_counter] = (stream_info->ts_info.currentProgram >> 8) & 0xFF;
    packet_total_length_counter += 1;
    //Program number (Lo)
    m_SectionBuffer[packet_total_length_counter] = stream_info->ts_info.currentProgram & 0xFF;
    packet_total_length_counter += 1;
    //Reserved, Version number, Current-Next indicator (always set to 1)
    m_SectionBuffer[packet_total_length_counter]=(unsigned char)(0xC0|(((version&0x1F)<<1) & 0xFE) | 0x01);
    packet_total_length_counter += 1;
    //Section number
    m_SectionBuffer[packet_total_length_counter]=0x00;
    packet_total_length_counter += 1;
    //Last section number
    m_SectionBuffer[packet_total_length_counter]=0x00;
    packet_total_length_counter += 1;
    //PCR PID Hi
    m_SectionBuffer[packet_total_length_counter] = (stream_info->ts_info.PCRPid >> 8) & 0x1F;
    packet_total_length_counter += 1;
    //PCR PID Lo
    m_SectionBuffer[packet_total_length_counter] = stream_info->ts_info.PCRPid & 0xFF;
    packet_total_length_counter += 1;
    //Program info length (set to 0)
    m_SectionBuffer[packet_total_length_counter] = 0x00;
    packet_total_length_counter += 1;
    m_SectionBuffer[packet_total_length_counter] = 0x00;
    packet_total_length_counter += 1;
    //Video stream (if present)
    if (stream_info->ts_info.VideoPID != 0 && 
        stream_info->ts_info.VideoStreamType != 0)
    {
        //Fill in video part
        //stream type
        m_SectionBuffer[packet_total_length_counter] = stream_info->ts_info.VideoStreamType;
        packet_total_length_counter += 1;
        //elementary stream PID (Hi)
        m_SectionBuffer[packet_total_length_counter] = (stream_info->ts_info.VideoPID >> 8) & 0x1F;
        packet_total_length_counter += 1;
        //elementary stream PID (Lo)
        m_SectionBuffer[packet_total_length_counter] = stream_info->ts_info.VideoPID & 0xFF;
        packet_total_length_counter += 1;
        //ES info length Hi
        m_SectionBuffer[packet_total_length_counter] = (stream_info->video_stream_desc_len >> 8) & 0x0F;
        packet_total_length_counter += 1;
        //ES info length Lo
        m_SectionBuffer[packet_total_length_counter] = stream_info->video_stream_desc_len & 0xFF;
        packet_total_length_counter += 1;
        //if ES info length not equal to zero - copy the data as well
        if (stream_info->video_stream_desc_len > 0)
        {
            memcpy(m_SectionBuffer + packet_total_length_counter,
                stream_info->video_stream_desc, stream_info->video_stream_desc_len);
            packet_total_length_counter += stream_info->video_stream_desc_len;
        }
    }
    //Audio stream
    if (stream_info->ts_info.AudioPID != 0 && 
        stream_info->ts_info.AudioStreamType != 0)
    {
		//Stream type
		m_SectionBuffer[packet_total_length_counter] = stream_info->ts_info.AudioStreamType;
		packet_total_length_counter += 1;
		//elementary stream PID (Hi)
		m_SectionBuffer[packet_total_length_counter] = (stream_info->ts_info.AudioPID >> 8) & 0x1F;
		packet_total_length_counter += 1;
		//elementary stream PID (Lo)
		m_SectionBuffer[packet_total_length_counter] = stream_info->ts_info.AudioPID & 0xFF;
		packet_total_length_counter += 1;
		//ES info length Hi
		m_SectionBuffer[packet_total_length_counter] = (stream_info->audio_stream_desc_len >> 8) & 0x0F;
		packet_total_length_counter += 1;
		//ES info length Lo
		m_SectionBuffer[packet_total_length_counter] = stream_info->audio_stream_desc_len & 0xFF;
		packet_total_length_counter += 1;
		//if ES info length not equal to zero - copy the data as well
		if (stream_info->audio_stream_desc_len > 0)
		{
			memcpy(m_SectionBuffer + packet_total_length_counter,
				stream_info->audio_stream_desc, stream_info->audio_stream_desc_len);
			packet_total_length_counter += stream_info->audio_stream_desc_len;
		}
	}

    //teletext stream
    if (stream_info->ts_info.TeletextPID != 0 &&
        stream_info->ts_info.TeletextStreamType != 0)
    {
        //Stream type
        m_SectionBuffer[packet_total_length_counter] = stream_info->ts_info.TeletextStreamType;
        packet_total_length_counter += 1;
        //elementary stream PID (Hi)
        m_SectionBuffer[packet_total_length_counter] = (stream_info->ts_info.TeletextPID >> 8) & 0x1F;
        packet_total_length_counter += 1;
        //elementary stream PID (Lo)
        m_SectionBuffer[packet_total_length_counter] = stream_info->ts_info.TeletextPID & 0xFF;
        packet_total_length_counter += 1;
        //ES info length Hi
        m_SectionBuffer[packet_total_length_counter] = (stream_info->teletext_stream_desc_len >> 8) & 0x0F;
        packet_total_length_counter += 1;
        //ES info length Lo
        m_SectionBuffer[packet_total_length_counter] = stream_info->teletext_stream_desc_len & 0xFF;
        packet_total_length_counter += 1;
        //if ES info length not equal to zero - copy the data as well
        if (stream_info->teletext_stream_desc_len > 0)
        {
            memcpy(m_SectionBuffer + packet_total_length_counter,
                stream_info->teletext_stream_desc, stream_info->teletext_stream_desc_len);
            packet_total_length_counter += stream_info->teletext_stream_desc_len;
        }
    }

    //Add section length (bytes 6 and 7)
    whole_section_counter = packet_total_length_counter - whole_section_counter + 4; //+CRC
    m_SectionBuffer[1] = 0xB0 | (((whole_section_counter & 0x0F00) >> 8) & 0xFF);
    m_SectionBuffer[2] = whole_section_counter & 0x00FF;
    //Add CRC
    unsigned long CRC32=ts_crc_handler::GetCRCHandler()->CalculateCRC(m_SectionBuffer, 
        packet_total_length_counter);
    m_SectionBuffer[packet_total_length_counter]=(unsigned char)((CRC32>>24)&0xFF); //CRC_32 Hi-Hi
    m_SectionBuffer[packet_total_length_counter + 1]=(unsigned char)((CRC32>>16)&0xFF); //CRC_32 Hi-Lo
    m_SectionBuffer[packet_total_length_counter + 2]=(unsigned char)((CRC32>>8)&0xFF); //CRC_32 Lo-Hi
    m_SectionBuffer[packet_total_length_counter + 3]=(unsigned char)(CRC32&0xFF); //CRC_32 Lo-Lo

    section_length = whole_section_counter + 3;
    return m_SectionBuffer;
}

unsigned char* ts_packet_generator::CreateSDTPacket(int& packet_length, unsigned char version, unsigned short* cont_counter, 
                                          STSStreamInfo* stream_info, unsigned char section_pid /*0x42*/)
{
    //form two descriptors: for name and for provider
    ts_decriptor_gen name_descriptor(stream_info->ChannelName.c_str());
    ts_decriptor_gen provider_descriptor(stream_info->ProviderName.c_str());
    //Fill the packet with 0xFFs
    memset(m_TSPacket, 0xFF, TS_PACKET_SIZE);
    //set all fields of the packet one by one
    int packet_total_length_counter = 0;
    //sync byte
    m_TSPacket[0] = SYNC_BYTE;
    packet_total_length_counter += 1;
    //error indicator, payload start indicator, transport priority and PID
    m_TSPacket[1] = 0x40;
    packet_total_length_counter += 1;
    m_TSPacket[2] = 0x11;
    packet_total_length_counter += 1;
    //scrambling control, adaptation field, continuity counter
    m_TSPacket[3] = 0x10 | (*cont_counter & 0x0F);
    *cont_counter = ts_process_routines::GetNextContinuityCounter(*cont_counter);
    packet_total_length_counter += 1;
    //offset
    m_TSPacket[4] = 0;
    packet_total_length_counter += 1;
    //Table Id
    m_TSPacket[5] = section_pid; //0x42;
    packet_total_length_counter += 1;
    //Next two bytes - do not forget to set whole section length afterwards!! (Bytes 6 and 7)
    packet_total_length_counter += 2;
    //start whole section counter.
    int whole_section_counter = packet_total_length_counter;
    //Transport stream ID (Hi)
    m_TSPacket[8] = (stream_info->TSID >> 8) & 0xFF;
    packet_total_length_counter += 1;
    //Transport stream ID (Lo)
    m_TSPacket[9] = stream_info->TSID & 0xFF;
    packet_total_length_counter += 1;
    //Reserved, Version number, Current-Next indicator (always set to 1)
    m_TSPacket[10]=(unsigned char)(0xC0|(((version&0x1F)<<1) & 0xFE) | 0x01);
    packet_total_length_counter += 1;
    //section number
    m_TSPacket[11]=0x00;
    packet_total_length_counter += 1;
    //last section number
    m_TSPacket[12]=0x00;
    packet_total_length_counter += 1;
    //original network ID (hi)
    m_TSPacket[13] = (stream_info->NetworkId >> 8) & 0xFF;
    packet_total_length_counter += 1;
    //original network ID (Lo)
    m_TSPacket[14] = stream_info->NetworkId & 0xFF;
    packet_total_length_counter += 1;
    //Reserved
    m_TSPacket[15] = 0xFF;
    packet_total_length_counter += 1;
    //service ID (hi)
    m_TSPacket[16] = (stream_info->currentProgram >> 8) & 0xFF;
    packet_total_length_counter += 1;
    //Service ID (Lo)
    m_TSPacket[17] = stream_info->currentProgram & 0xFF;
    packet_total_length_counter += 1;
    //EIT schedule and present following flag (set it to available)
    m_TSPacket[18] = 0x03;
    packet_total_length_counter += 1;

    //Running status, free CA mode and Hi of descriptor length
    unsigned short desc_length = 5 + name_descriptor.GetDescriptorLength() + provider_descriptor.GetDescriptorLength();
    m_TSPacket[19] = (desc_length >> 8) & 0x0F;
    packet_total_length_counter += 1;

    //Lo of descriptor length
    m_TSPacket[20] = desc_length & 0xFF;
    packet_total_length_counter += 1;

    //Descriptor itself
    //Descriptor tag
    m_TSPacket[21] = 0x48;
    packet_total_length_counter += 1;
    //Descriptor length
    m_TSPacket[22] = 3 + name_descriptor.GetDescriptorLength() + provider_descriptor.GetDescriptorLength();
    packet_total_length_counter += 1;
    //Service type
    m_TSPacket[23] = (unsigned char)stream_info->channelType;
    packet_total_length_counter += 1;
    //Service provider name length
    m_TSPacket[24] = provider_descriptor.GetDescriptorLength();
    packet_total_length_counter += 1;
    //Service provider name
    memcpy(m_TSPacket + 25, provider_descriptor.GetDescriptor(), provider_descriptor.GetDescriptorLength());
    packet_total_length_counter += provider_descriptor.GetDescriptorLength();
    //Service name length
    m_TSPacket[packet_total_length_counter] = name_descriptor.GetDescriptorLength();
    packet_total_length_counter += 1;
    //Service name
    memcpy(m_TSPacket + packet_total_length_counter, name_descriptor.GetDescriptor(), 
        name_descriptor.GetDescriptorLength());
    packet_total_length_counter += name_descriptor.GetDescriptorLength();
    //Add section length (bytes 6 and 7)
    whole_section_counter = packet_total_length_counter - whole_section_counter + 4; //+CRC
    m_TSPacket[6] = 0xF0 | (((whole_section_counter & 0x0F00) >> 8) & 0xFF);
    m_TSPacket[7] = whole_section_counter & 0x00FF;
    //Add CRC
    unsigned long CRC32=ts_crc_handler::GetCRCHandler()->CalculateCRC(m_TSPacket + 5, 
        packet_total_length_counter - 5);
    m_TSPacket[packet_total_length_counter]=(unsigned char)((CRC32>>24)&0xFF); //CRC_32 Hi-Hi
    m_TSPacket[packet_total_length_counter + 1]=(unsigned char)((CRC32>>16)&0xFF); //CRC_32 Hi-Lo
    m_TSPacket[packet_total_length_counter + 2]=(unsigned char)((CRC32>>8)&0xFF); //CRC_32 Lo-Hi
    m_TSPacket[packet_total_length_counter + 3]=(unsigned char)(CRC32&0xFF); //CRC_32 Lo-Lo

    packet_length = TS_PACKET_SIZE;
    return m_TSPacket;
}

unsigned char* ts_packet_generator::CreateDiscontinuityPacket(int& packet_length, unsigned short PID, unsigned short* cont_counter)
{
    //This function creates a transport stream packet with 
    //adaptation field only where discontinuity flag is set to true
    //Fill the packet with 0xFFs
    memset(m_TSPacket, 0xFF, TS_PACKET_SIZE);

    //sync byte
    m_TSPacket[0] = SYNC_BYTE;
    //error indicator, payload start indicator(0), transport priority and PID
    m_TSPacket[1]=(unsigned char)(0x00|((PID >> 8) & 0x1F));
    m_TSPacket[2] = PID & 0xFF;
    //scrambling control, adaptation field only (no payload), continuity counter
    m_TSPacket[3] = 0x20 | (*cont_counter & 0x0F);
    *cont_counter = ts_process_routines::GetNextContinuityCounter(*cont_counter);
    //Adaptation field length
    unsigned char l = 183;
    m_TSPacket[4] = l;
    //Adaptation field flags - set discontinuity indicator
    m_TSPacket[5] = 0x80;

    packet_length = TS_PACKET_SIZE;
    return m_TSPacket;
}

void ts_packet_generator::SplitAndSendSectionBuffer(unsigned char* SectionBuf, int SectionBufLen, 
                                                   unsigned short* cont_counter, unsigned short pid, LP_PACKGEN_TS_SEND_FUNC send_func, void* param)
{
    //Split PMT section into TS packets and send it down the stream
    int written_packets = 0;
    int buf_offs = 0;
    while (buf_offs < SectionBufLen)
    {
        //Fill the packet with 0xFFs
        memset(m_TSSendPacket, 0xFF, TS_PACKET_SIZE);
        int offset_to_payload = 4;
        //add packet content
        m_TSSendPacket[0] = SYNC_BYTE;
        m_TSSendPacket[1] = (pid >> 8) & 0xFF;
        m_TSSendPacket[2] = pid & 0xFF;
        //scrambling control, adaptation field, continuity counter
        m_TSSendPacket[3] = 0x10 | (*cont_counter & 0x0F);
        *cont_counter = ts_process_routines::GetNextContinuityCounter(*cont_counter);
        if (written_packets == 0)
        {
            //first packet
            //Set "offset to section" field
            m_TSSendPacket[4] = 0x00;
            offset_to_payload += 1;
            //set payload start indicator for the first packet
            m_TSSendPacket[1] = m_TSSendPacket[1] | 0x40;
        }
        //copy the PMT data into newly created packet
        int copy_len = TS_PACKET_SIZE - offset_to_payload;
        if (SectionBufLen - buf_offs < TS_PACKET_SIZE - offset_to_payload)
        {
            copy_len = SectionBufLen - buf_offs;
        }

        memcpy(m_TSSendPacket + offset_to_payload, SectionBuf + buf_offs, copy_len);
        //Send the packet
        send_func(m_TSSendPacket, TS_PACKET_SIZE, param);

        written_packets += 1;
        buf_offs += copy_len;
    }
}

void ts_packet_generator::SplitAndSendPESBuffer(unsigned char* PESBuf,
    int PESBufLen, unsigned short* cont_counter, unsigned short pid,
    boost::int64_t pcr, bool payload_start,
    LP_PACKGEN_TS_SEND_FUNC send_func, void* param)
{
    //Split PES buffer into TS packets and send it down the stream
    int written_packets = 0;
    int buf_offs = 0;
    while (buf_offs < PESBufLen)
    {
        //Fill the packet with 0xFFs
        memset(m_TSSendPacket, 0xFF, TS_PACKET_SIZE);
        unsigned char offset_to_payload = 4;

        //add packet content
        m_TSSendPacket[0] = SYNC_BYTE;
        m_TSSendPacket[1] = ((pid >> 8) & 0xFF);
        m_TSSendPacket[2] = pid & 0xFF;

        //scrambling control, adaptation field, continuity counter
        m_TSSendPacket[3] = 0x10 | (*cont_counter & 0x0F);
        *cont_counter = ts_process_routines::GetNextContinuityCounter(*cont_counter);

        unsigned char adapt_field_size = 0;
        if (written_packets == 0)
        {
            //first packet
            //Pay_load start indicator
            if (payload_start)
            {
                m_TSSendPacket[1] = m_TSSendPacket[1] | 0x40;
            }

            //check if PCR has to be inserted
            if (pcr != -1)
            {
                adapt_field_size = 7;
                offset_to_payload += 8;
                ts_process_routines::InsertPCRInPacket(m_TSSendPacket, pcr);
            }
        }

        //copy data into newly created packet
        unsigned char copy_len = TS_PACKET_SIZE - offset_to_payload;
        if (PESBufLen - buf_offs < TS_PACKET_SIZE - offset_to_payload)
        {
            copy_len = static_cast<unsigned char>(PESBufLen - buf_offs);
        }

        if (copy_len < TS_PACKET_SIZE - offset_to_payload)
        {
            //put the adaptation field to stuff the packet

            //adding adaptation field will add 2 extra bytes
            //make sure that we are not shooting outside the buffer
            if (TS_PACKET_SIZE - offset_to_payload - copy_len < 16)
            {
                copy_len = TS_PACKET_SIZE - offset_to_payload - 16;
            }

            //update packet header
            m_TSSendPacket[3] |= 0x30;
            if (adapt_field_size == 0)
            {
                //There is no adaptation field in this packet yet
                adapt_field_size = TS_PACKET_SIZE - offset_to_payload - copy_len - 1;
                offset_to_payload += adapt_field_size + 1;
                m_TSSendPacket[5] = 0; //flags (none ;)
            }
            else
            {
                //There is already an adaptation field in this packet
                adapt_field_size += TS_PACKET_SIZE - offset_to_payload - copy_len;
                offset_to_payload += TS_PACKET_SIZE - offset_to_payload - copy_len;
                //Adaptation field flags are already set
            }

            m_TSSendPacket[4] = adapt_field_size; //length of adaptation packet
        }

        memcpy(m_TSSendPacket + offset_to_payload, PESBuf + buf_offs, copy_len);

        //Send the packet
        send_func(m_TSSendPacket, TS_PACKET_SIZE, param);

        written_packets += 1;
        buf_offs += copy_len;
    }
}

//********************** ts_decriptor_gen ************************
ts_decriptor_gen::ts_decriptor_gen(const wchar_t* desc_string)
{
    const wchar_t* str = desc_string;

    if (wcslen(desc_string) == 0)
        str = L"-";

    std::string outstr;
    ConvertUCToMultibyte(dvblink::engine::EC_UTF16_BE, str, outstr);
    int str_len = strlen(outstr.c_str());
    
    memcpy(m_Buffer + 1, outstr.c_str(), str_len);
    m_DescriptorLength = static_cast<unsigned char>(1 + str_len);
    m_Buffer[0] = 0x11;
}

}}
