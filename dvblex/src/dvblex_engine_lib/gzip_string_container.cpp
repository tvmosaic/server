/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <string.h>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <dl_gzip_string_container.h>

namespace dvblink { namespace engine {

gzip_string_container::gzip_string_container()
{
}

gzip_string_container::gzip_string_container(const std::string& str)
{
    add_string(str);
}

gzip_string_container::~gzip_string_container()
{
}

bool gzip_string_container::add_string(const std::string& str)
{
    bool ret_val = true;

    data_.clear();

    boost::iostreams::filtering_ostream os;
    os.push(boost::iostreams::gzip_compressor());
    os.push(boost::iostreams::back_inserter(data_));
    try {
        boost::iostreams::write(os, &str[0], str.size());
    } catch (...)
    {
        ret_val = false;
    }

    return ret_val;
}

bool gzip_string_container::get_string(std::string& str)
{
    bool ret_val = true;

    str.clear();

    boost::iostreams::filtering_ostream os;
    os.push(boost::iostreams::gzip_decompressor());
    os.push(boost::iostreams::back_inserter(str));
    try {
        boost::iostreams::write(os, &data_[0], data_.size());
    } catch (...)
    {
        ret_val = false;
    }

    return ret_val;
}

} //engine
} //dvblink
