/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#ifdef _WIN32

#include <stdexcept>
#include "dl_event.h"

using namespace dvblink;

struct dvblink::event::_impl
{
    HANDLE handle;
};

dvblink::event::event()
{
    // auto-reset event
    HANDLE handle = ::CreateEvent(NULL, FALSE, FALSE, NULL);
    
    if (!handle)
    {
        throw std::runtime_error("Cannot create event object");
    }

    std::auto_ptr<_impl> ptr(new _impl);
    ptr->handle = handle;
    pimpl_ = ptr;
}

dvblink::event::~event()
{
    ::CloseHandle(pimpl_->handle);
    pimpl_->handle = NULL;
}

errcode_t dvblink::event::wait(timeout_t to) const
{
    DWORD ret = ::WaitForSingleObject(pimpl_->handle,
        DWORD(to.total_milliseconds()));

    if (ret == WAIT_OBJECT_0)
    {
        // the state of the specified object is signaled
        return err_none;
    }

    if (ret == WAIT_TIMEOUT)
    {
        // time-out interval elapsed
        return err_timeout;        
    }

    return err_unexpected;
}

errcode_t dvblink::event::signal()
{
    ::SetEvent(pimpl_->handle);
    return err_none;
}

errcode_t dvblink::event::reset()
{
    ::ResetEvent(pimpl_->handle);
    return err_none;
}

bool dvblink::event::is_signaled() const
{
    return (err_none == dvblink::event::wait(zero_timeout));
}

#endif

// $Id: event.win32.cpp 4816 2012-03-22 12:48:38Z mike $
