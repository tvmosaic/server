/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#ifdef WIN32

#include <windows.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

namespace dvblink {

//
// Sends a string to the debugger for display
//
void _debug_print(const char* format, ...)
{
    const size_t _buf_size = 1024;
    char buf[_buf_size + 1];
    buf[_buf_size] = 0;

    unsigned long pid = GetCurrentProcessId();
    unsigned long tid = GetCurrentThreadId();
    int n = _snprintf_s(buf, _buf_size, _TRUNCATE, "(%lu:%lu) ", pid, tid);

    if (n > 0)
    {
        va_list args;
        va_start(args, format);
        _vsnprintf_s(buf + n, _buf_size - n, _TRUNCATE, format, args);
        va_end(args);

        OutputDebugStringA(buf);
    }
}

} // namespace dvblink

#endif

// $Id: debug.win32.cpp 2817 2011-06-18 15:25:29Z sab $
