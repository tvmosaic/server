/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <string>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_file_procedures.h>
#include <dl_xz.h>
#include "xz/xz.h"

namespace dvblink { namespace xz {

using namespace engine;
using namespace logging;

xzfile_reader::xzfile_reader(const dvblink::filesystem_path_t& filename) :
    xz_file_(filename)
{
}

xzfile_reader::~xzfile_reader()
{
}

bool xzfile_reader::decompress(const dvblink::filesystem_path_t& destination, dvblink::filesystem_path_t& uncompressed_filename)
{
    bool ret_val = false;

    uncompressed_filename = destination;
    boost::uuids::uuid new_guid = boost::uuids::random_generator()();
    uncompressed_filename /= boost::lexical_cast<std::string>(new_guid);

	struct xz_buf b;
	struct xz_dec *s;
	enum xz_ret ret;

    s = xz_dec_init(XZ_DYNALLOC, 1 << 26);
	if (s != NULL) 
    {
        xz_uint8_t in[BUFSIZ];
        xz_uint8_t out[BUFSIZ];

	    b.in = in;
	    b.in_pos = 0;
	    b.in_size = 0;
	    b.out = out;
	    b.out_pos = 0;
	    b.out_size = BUFSIZ;

        FILE* f_in = filesystem::universal_open_file(xz_file_, "r+b");
        FILE* f_out = filesystem::universal_open_file(uncompressed_filename, "w+b");
        if (f_in != NULL && f_out != NULL)
        {
	        while (true) {
		        if (b.in_pos == b.in_size) {
			        b.in_size = fread(in, 1, sizeof(in), f_in);
			        b.in_pos = 0;
		        }

		        ret = xz_dec_run(s, &b);

		        if (b.out_pos == sizeof(out)) 
                {
			        if (fwrite(out, 1, b.out_pos, f_out) != b.out_pos) 
                    {
				        log_error(L"xzfile_reader::decompress. Error writing output file");
				        break;
			        }
			        b.out_pos = 0;
		        }

		        if (ret == XZ_OK)
			        continue;

		        if (fwrite(out, 1, b.out_pos, f_out) != b.out_pos) 
                {
			        log_error(L"xzfile_reader::decompress. Error writing output file");
			        break;
		        }

                if (ret == XZ_STREAM_END)
                {
                    //success
                    ret_val = true;
                } else
                {
                    //log error
			        log_error(L"xzfile_reader::decompress. Decompression error %1%") % ret;
                }
                //exit loop in any case
                break;
	        }
        } else
        {
            log_error(L"xzfile_reader::decompress. Error opening input or output files");
        }

        if (f_in != NULL)
            fclose(f_in);

        if (f_out != NULL)
            fclose(f_out);

	    xz_dec_end(s);
    } else
    {
        log_error(L"xzfile_reader::decompress. Error initializing xz decoder");
    }

    return ret_val;
}

} // namespace xz
} // namespace dvblink
