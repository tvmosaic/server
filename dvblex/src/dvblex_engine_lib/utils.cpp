/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_common.h>
#include <boost/algorithm/string.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <dl_utils.h>
#include <dl_logger.h>
#include <dl_strings.h>
#include "bbc_huffman.h"

#if defined(_WIN32)
//add win32 headers here
#elif defined(__APPLE__)

#include <mach-o/dyld.h>    /* _NSGetExecutablePath */
#include <limits.h>     /* PATH_MAX */

#else
    #include <linux/limits.h>
#endif

using namespace dvblink::logging;

namespace dvblink {

#ifdef _WIN32
void sleep(timeout_t to)
{
    if (to.is_special())
    {
        ::Sleep(INFINITE);
    }
    else
    {
        ::Sleep(DWORD(to.total_milliseconds()));
    }    
}
#else
void sleep(timeout_t to)
{
    if (to.is_special())
    {
        ::select(0, NULL, NULL, NULL, NULL);
    }
    else
    {
        struct timeval tv;
        tv.tv_sec = long(to.total_milliseconds() / MILLI);      // seconds
        tv.tv_usec = (to.total_milliseconds() % MILLI) * MILLI; // micro-sec

        ::select(0, NULL, NULL, NULL, &tv);
    }
}
#endif

namespace engine {

void substitute_invalid_file_chars(std::wstring& str)
{
	std::wstring chars_to_substitute = L"\r\n []%\\/*?:\"\'<>|";
	std::wstring subst_char = L"_";
	while (1)
	{
		std::wstring::size_type i = str.find_first_of(chars_to_substitute);
		if (i != std::wstring::npos)
		{
			//substitute this character
			str.replace(i, 1, subst_char);
		}
        else
		{
			break;
		}
	}
}

static EDVBDefaultCodepage g_DefaultDVBCodepage = EDDC_ISO_8859_1;

void SetDefaultDVBCodepage(EDVBDefaultCodepage cp)
{
	g_DefaultDVBCodepage = cp;
}


#ifdef _WIN32

std::wstring format_error_message(unsigned long error)
{
    LPVOID lpMsgBuf;
    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        error,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
        (LPTSTR)&lpMsgBuf,
        0,
        NULL);

    std::wstring message = (TCHAR*)lpMsgBuf;
    LocalFree(lpMsgBuf);

    return message;
}

#endif

void ConvertAnnexAStringToUnicode(unsigned char* annexa_string, int string_len, unsigned char enc, std::wstring& out_str)
{
    out_str = L"";
    const char* annexa_string_s = (const char*)annexa_string;

    //Check text encoding
    if (enc >= 0x20)
    {
        switch (g_DefaultDVBCodepage)
        {
        case EDDC_ISO_8859_1:
	        ConvertMultibyteToUC(EC_ISO_8859_1, annexa_string_s, string_len, out_str); //ISO 8859-1
            break;
        case EDDC_ISO_8859_2:
			ConvertMultibyteToUC(EC_ISO_8859_2, annexa_string_s, string_len, out_str); //ISO 8859-2
            break;
        default:
			//(ISO 6937)
			ConvertMultibyteToUC(EC_ISO_6937, annexa_string_s, string_len, out_str);
			//fix for WinXP that cannot process sometimes CP 20269 correctly)
			if (out_str.size() == 0)
            {
				ConvertMultibyteToUC(EC_ISO_8859_1, annexa_string_s, string_len, out_str); //ISO 8859-1
            }
		}
    }
    else
    {
        if (enc >= 0x01 && enc <= 0x0F)
        {
            ECodepage codepage;
            switch (enc)
            {
            case 0x01:
                codepage = EC_ISO_8859_5;
                break;
            case 0x02:
                codepage = EC_ISO_8859_6;
                break;
            case 0x03:
                codepage = EC_ISO_8859_7;
                break;
            case 0x04:
                codepage = EC_ISO_8859_8;
                break;
            case 0x05:
                codepage = EC_ISO_8859_9;
                break;
            case 0x07:
                codepage = EC_ISO_8859_11;
                break;
            case 0x09:
                codepage = EC_ISO_8859_13;
                break;
            case 0x0B:
                codepage = EC_ISO_8859_15;
                break;
            default:
                //unsupported character set. Defaulting to latin1 (ISO 8859-1)
                codepage = EC_ISO_8859_1;
                //Log error
                break;
            }
            ConvertMultibyteToUC(codepage, annexa_string_s, string_len, out_str);
        }
        else
        {
            switch (enc)
            {
            case 0x10:
                if (string_len >= 2 && annexa_string_s[0] == 0x00)
                {
                    switch (annexa_string_s[1])
                    {
                    case 0x01:
                        ConvertMultibyteToUC(EC_ISO_8859_1, annexa_string_s+2, string_len - 2, out_str);
                        break;
                    case 0x02:
                        ConvertMultibyteToUC(EC_ISO_8859_2, annexa_string_s+2, string_len - 2, out_str);
                        break;
                    case 0x03:
                        ConvertMultibyteToUC(EC_ISO_8859_3, annexa_string_s+2, string_len - 2, out_str);
                        break;
                    case 0x04:
                        ConvertMultibyteToUC(EC_ISO_8859_4, annexa_string_s+2, string_len - 2, out_str);
                        break;
                    case 0x05:
                        ConvertMultibyteToUC(EC_ISO_8859_5, annexa_string_s+2, string_len - 2, out_str);
                        break;
                    case 0x06:
                        ConvertMultibyteToUC(EC_ISO_8859_6, annexa_string_s+2, string_len - 2, out_str);
                        break;
                    case 0x07:
                        ConvertMultibyteToUC(EC_ISO_8859_7, annexa_string_s+2, string_len - 2, out_str);
                        break;
                    case 0x08:
                        ConvertMultibyteToUC(EC_ISO_8859_8, annexa_string_s+2, string_len - 2, out_str);
                        break;
                    case 0x09:
                        ConvertMultibyteToUC(EC_ISO_8859_9, annexa_string_s+2, string_len - 2, out_str);
                        break;
                    case 0x0B:
                        ConvertMultibyteToUC(EC_ISO_8859_11, annexa_string_s+2, string_len - 2, out_str);
                        break;
                    case 0x0D:
                        ConvertMultibyteToUC(EC_ISO_8859_13, annexa_string_s+2, string_len - 2, out_str);
                        break;
                    case 0x0F:
                        ConvertMultibyteToUC(EC_ISO_8859_15, annexa_string_s+2, string_len - 2, out_str);
                        break;
                    default:
                        ConvertMultibyteToUC(EC_ISO_8859_1, annexa_string_s+2, string_len - 2, out_str);
                        break;
                    }
                }
                break;
            case 0x11:
                //Unicode (LE, 16 bit)
                ConvertMultibyteToUC(EC_UTF16_BE, annexa_string_s, string_len, out_str);
                break;
            case 0x12:
                //ks_c_5601-1987
                ConvertMultibyteToUC(EC_KS_C_5601_1987, annexa_string_s, string_len, out_str);
                break;
            case 0x13:
                //x-cp20936
                ConvertMultibyteToUC(EC_X_CP20936, annexa_string_s, string_len, out_str);
                break;
            case 0x14:
                //big5
                ConvertMultibyteToUC(EC_UTF16_BE, annexa_string_s, string_len, out_str);
                break;
            case 0x15:
                //utf-8
                ConvertMultibyteToUC(EC_UTF8, annexa_string_s, string_len, out_str);
                break;
            case 0x1F:
                //custom encoding by providers
                //the first byte is encoding, data starts from the second byte
                if (string_len >= 1)
                {
                    if (annexa_string_s[0] >= 0x01 && annexa_string_s[0] <= 0x04)
                    {
                        char* decoded = bbc_huffman_decode((const unsigned char*)annexa_string_s, string_len);
                        if (decoded != NULL)
                        {
                            ConvertMultibyteToUC(EC_ISO_8859_1, decoded, out_str);
                            free(decoded);
                        } else
                        {
                            //error? use 8859-1 as a fall back
                            ConvertMultibyteToUC(EC_ISO_8859_1, annexa_string_s+1, string_len-1, out_str);
                        }
                    } else
                    {
                        //default to 8859-1 for now
                        ConvertMultibyteToUC(EC_ISO_8859_1, annexa_string_s+1, string_len-1, out_str);
                    }
                }
                break;
            default:
                //Log error
                log_error(L"ConvertAnnexATextToUnicode. Unsupported character set (%1%)") % enc;
            }
        }
    }
}

bool ProcessControlChInAnnexAText(unsigned char* annexa_text, int length, unsigned char& enc, std::vector<std::string>& resText)
{
    bool ret_val = false;

    resText.clear();

    if (length > 0)
    {
        ret_val = true;

        enc = annexa_text[0];

        int start_idx = 0;
        if (enc >= 0x20 /* && enc <= 0xFF */)
            start_idx = 0;
        else
            start_idx = 1;
        //This function only applies to one byte character tables
        if (enc <= 0x0B || enc == 0x10 || enc >= 0x20)
        {
            int curstr_idx = 0;
            std::string str;
            resText.push_back(str);

            for (int i = start_idx; i < length; i++)
            {
                if (annexa_text[i] >= 0x80 && annexa_text[i] <= 0x9F)
                {
                    if (annexa_text[i] == 0x8A)
                    {
                        resText.push_back(str);
                        curstr_idx += 1;
                    }
                }
                else
                {
                    //add this symbol to the current string
                    resText[curstr_idx] += annexa_text[i];
                }
            }
        } else
        {
            resText.push_back(std::string((char*)annexa_text + start_idx, length - start_idx));
        }
    }

    return ret_val;
}

void ConvertAnnexATextToUnicode(unsigned char* annexa_text, int text_len, std::wstring& out_str)
{
    out_str = L"";

    if (text_len > 0)
    {
        unsigned char enc = annexa_text[0];
        if (enc == 0x11 || enc == 0x14)
        {
            //unicode
            ConvertAnnexAStringToUnicode(annexa_text+1, text_len-1, enc, out_str);
        }
        else
        {
            std::vector<std::string> resText;
            if (ProcessControlChInAnnexAText(annexa_text, text_len, enc, resText))
            {
                for (unsigned int i=0; i<resText.size(); i++)
                {
                    if (out_str.size() > 0)
                        out_str += L"\n";

                    if (resText[i].size() > 0)
                    {
                        std::wstring cur_str;
                        ConvertAnnexAStringToUnicode((unsigned char*)resText[i].c_str(), resText[i].size(), enc, cur_str);
                        out_str += cur_str;
                    }
                }
            }
        }
   }
}

void GetNumberFromBCD(unsigned char* bcd_num, int chnum, unsigned long& result)
{
    result = 0;
    unsigned long multiplier = 1;
    for (int i=0; i<chnum-1; i++)
        multiplier *= 10;
    for (int i=0; i<chnum; i++)
    {
        unsigned long nibble = 0;
        if ((i % 2) == 0)
            //even - take high nibble
            nibble = (*bcd_num & 0xF0) >> 4;
        else
        {
            //odd - take low nibble
            nibble = (*bcd_num & 0x0F);
            //advance bcd_num pointer
            bcd_num += 1;
        }
        result += nibble*multiplier;
        multiplier = multiplier/10;
    }
}

bool execute_script(const dvblink::filesystem_path_t& script_path,
    const dvblink::filesystem_path_t* current_directory, unsigned long wait_timeout)
{
    bool ret_val = true;

#ifdef _WIN32
    //start bat file
    wchar_t exe_buf[1024];
    wchar_t* pszComspec = _wgetenv(L"COMSPEC");
    if (pszComspec)
    {
        swprintf_s(exe_buf, L"\"%s\" /C \"%s\"", pszComspec, script_path.to_wstring().c_str());
    }
    else
    {
        swprintf_s(exe_buf, L"cmd.exe /C \"%s\"", script_path.to_wstring().c_str());
    }

    STARTUPINFO si = {0};
    PROCESS_INFORMATION pi = {0};

    si.cb = sizeof(si);
    if (CreateProcess(NULL, exe_buf, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS,
        NULL, current_directory ? current_directory->to_wstring().c_str() : NULL, &si, &pi))
    {
        //Wait until bat file is finished
        if (wait_timeout)
        {
            if (WaitForSingleObject(pi.hProcess, wait_timeout) == WAIT_TIMEOUT)
            {
                //Kill the process - we do not have time to wait any further
                TerminateProcess(pi.hProcess, 0);
            }
        }
        CloseHandle(pi.hProcess);
        CloseHandle(pi.hThread);
    }
    else
    {
        ret_val = false;
    }
#else
    if (current_directory)
    {
        chdir(current_directory->to_string().c_str());
    }

    int res = system(script_path.to_string().c_str());
    if (res == -1)
    {
        ret_val = false;
    }
#endif

    return ret_val;
}

const char* eng_codes[] = {"eng", "en", NULL};
const char* rus_codes[] = {"rus", "ru", NULL};
const char* ukr_codes[] = {"ukr", "uk", NULL};
const char* bul_codes[] = {"bul", "bg", NULL};
const char* ces_codes[] = {"ces", "cs", "cze", NULL};
const char* dan_codes[] = {"dan", "da", NULL};
const char* deu_codes[] = {"deu", "de", "ger", NULL};
const char* fin_codes[] = {"fin", "fe", NULL};
const char* fra_codes[] = {"fra", "fr", "fre", NULL};
const char* ell_codes[] = {"ell", "el", "gre", NULL};
const char* hrv_codes[] = {"hrv", "hr", NULL};
const char* hun_codes[] = {"hun", "hu", NULL};
const char* ita_codes[] = {"ita", "it", NULL};
const char* nor_codes[] = {"nor", "no", NULL};
const char* pol_codes[] = {"pol", "pl", NULL};
const char* slk_codes[] = {"slk", "slo", "sk", NULL};
const char* slv_codes[] = {"slv", "sl", NULL};
const char* spa_codes[] = {"spa", "es", NULL};
const char* swe_codes[] = {"swe", "sv", NULL};
const char* tur_codes[] = {"tur", "tr", NULL};
const char* nld_codes[] = {"nld", "nl", "dut", NULL};

static bool is_in_codes(const char* code, const char** codes)
{
    bool ret_val = false;
    
    int i=0;
    while (codes[i] != NULL)
    {
        if (boost::iequals(code, codes[i]))
        {
            ret_val = true;
            break;
        }
        i++;
    }
    return ret_val;
}

static void add_codes(const char** codes, std::vector<std::string>& code_vector)
{
    int i=0;
    while (codes[i] != NULL)
    {
        code_vector.push_back(codes[i]);
        i++;
    }
}

void GetISO_639_3_Synonyms(const char* iso_639_3_code, std::vector<std::string>& synonyms)
{
	synonyms.clear();
	if (is_in_codes(iso_639_3_code, eng_codes))
	    add_codes(eng_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, rus_codes))
	    add_codes(rus_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, ukr_codes))
	    add_codes(ukr_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, bul_codes))
	    add_codes(bul_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, ces_codes))
	    add_codes(ces_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, dan_codes))
	    add_codes(dan_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, deu_codes))
	    add_codes(deu_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, fin_codes))
	    add_codes(fin_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, fra_codes))
	    add_codes(fra_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, ell_codes))
	    add_codes(ell_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, hrv_codes))
	    add_codes(hrv_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, hun_codes))
	    add_codes(hun_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, ita_codes))
	    add_codes(ita_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, nor_codes))
	    add_codes(nor_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, pol_codes))
	    add_codes(pol_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, slk_codes))
	    add_codes(slk_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, slv_codes))
	    add_codes(slv_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, spa_codes))
	    add_codes(spa_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, swe_codes))
	    add_codes(swe_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, tur_codes))
	    add_codes(tur_codes, synonyms);
	else
	if (is_in_codes(iso_639_3_code, nld_codes))
	    add_codes(nld_codes, synonyms);
	else
	    synonyms.push_back(iso_639_3_code);
}

// The implementations that takes into account UTC 
boost::posix_time::ptime from_time_t(std::time_t t)
{ 
    static const boost::posix_time::time_duration OFFSET = boost::posix_time::second_clock::local_time() - boost::posix_time::second_clock::universal_time(); 
    static const boost::posix_time::ptime THE_EPOCH(boost::gregorian::date(1970,1,1), OFFSET); 
    return THE_EPOCH + boost::posix_time::seconds(static_cast<long>(t)); 
} 

std::time_t to_time_t(boost::posix_time::ptime t)
{ 
    static const boost::posix_time::time_duration OFFSET = boost::posix_time::second_clock::local_time() - boost::posix_time::second_clock::universal_time(); 
    static const boost::posix_time::ptime THE_EPOCH(boost::gregorian::date(1970,1,1), OFFSET); 
    return boost::posix_time::time_period(THE_EPOCH, t).length().total_seconds(); 
}

bool get_app_locations(dvblink::filesystem_path_t& full_path, dvblink::filesystem_path_t& working_dir)
{
    bool ret_val = false;

#if defined(_WIN32)
    wchar_t path[_MAX_PATH];

	GetModuleFileNameW(NULL, path, sizeof(path));
    full_path = path;
    ret_val = true;
#elif defined(__APPLE__)
    
    char buf[PATH_MAX];
    uint32_t size = PATH_MAX;

    if (_NSGetExecutablePath(buf, &size) == 0) 
    {
        full_path = buf;
        ret_val = true;
    }
#else
    char buf[PATH_MAX];
    int length = readlink("/proc/self/exe", buf, sizeof(buf));
    if (length > 0)
    {
        std::string str(buf, buf + length);
        full_path = str;
        ret_val = true;
    }
#endif

    if (ret_val)
    {
        boost::filesystem::path p = full_path.to_boost_filesystem();
        working_dir = p.remove_filename();
    }

    return ret_val;
}

} //engine
} //dvblink

