/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/device/file.hpp>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_gzip.h>
#include <dl_file_procedures.h>

namespace dvblink { namespace gzip {

using namespace engine;
using namespace logging;

static const boost::uint8_t id1_magic = 0x1f;
static const boost::uint8_t id2_magic = 0x8b;

#ifdef WIN32
	#pragma pack(push, 1)
#endif

struct gzip_file_header
{
    boost::uint8_t id1;
    boost::uint8_t id2;
    boost::uint8_t compression;
    boost::uint8_t flags;
    boost::uint32_t mtime;
    boost::uint8_t extra_flags;
    boost::uint8_t os;
};

#ifdef WIN32
	#pragma pack(pop)
#endif

class gzip_file_header_reader
{
public:
    gzip_file_header_reader()
    {}

    bool read(const dvblink::filesystem_path_t& file)
    {
        bool ret_val = false;

        FILE* f = filesystem::universal_open_file(file, "r+b");
        if (f != NULL)
        {
            size_t read = fread(&file_header, 1, sizeof(file_header), f);
            if (read == sizeof(file_header))
            {
                //check magic
                if (file_header.id1 == id1_magic && file_header.id2 == id2_magic)
                {
                    ret_val = true;

                    //calculate offset to a filename
                    if ((file_header.flags & 0x08) != 0)
                    {
                        size_t offset = sizeof(file_header);

                        boost::uint16_t extra_length = 0;
                        //read length of extra field
                        if ((file_header.flags & 0x04) != 0)
                        {
                            if (get_field_length(f, sizeof(file_header), extra_length))
                                offset += sizeof(extra_length) + extra_length;
                        }

                        //read bytes from file one by one until \0x00 is reached
                        if (fseek(f, offset, SEEK_SET) == 0)
                        {
                            boost::uint8_t ch;
                            do
                            {
                                read = fread(&ch, 1, sizeof(ch), f);
                                if (read == sizeof(ch))
                                {
                                    original_filename_ += ch;
                                } else
                                {
                                    original_filename_.clear();
                                    break;
                                }
                            } while (ch != '\0');
                        }
                    }
                } else
                {
                    log_error(L"gzip_file_header_reader::read. file header magic numbers do not match");
                }
            } else
            {
                log_error(L"gzip_file_header_reader::read. Cannot read file header");
            }
            fclose(f);
        } else
        {
            log_error(L"gzip_file_header_reader::read. Unable to open %1%") % file.to_wstring();
        }

        return ret_val;
    }

    bool get_field_length(FILE* f, boost::uint64_t offs, boost::uint16_t& length)
    {
        bool ret_val = false;

        if (fseek(f, offs, SEEK_SET) == 0)
        {
            boost::uint8_t b1;
            boost::uint8_t b2;
            if (fread(&b1, 1, sizeof(b1), f) == sizeof(b1) &&
                fread(&b2, 1, sizeof(b2), f) == sizeof(b2))
            {
                length = b2;
                length = (length << 8) + b1;
                ret_val = true;
            }
        }

        return ret_val;
    }

    gzip_file_header file_header;
    std::string original_filename_;

};


gzipfile_reader::gzipfile_reader(const dvblink::filesystem_path_t& filename)
    : filename_(filename)
{
}

gzipfile_reader::~gzipfile_reader()
{
}

bool gzipfile_reader::decompress(const dvblink::filesystem_path_t& destination, dvblink::filesystem_path_t& uncompressed_filename)
{
    bool ret_val = false;

    gzip_file_header_reader r;
    if (r.read(filename_))
    {
        uncompressed_filename = destination;
        if (r.original_filename_.size() > 0)
        {
            uncompressed_filename /= r.original_filename_;
        } else
        {
            boost::uuids::uuid new_guid = boost::uuids::random_generator()();
            uncompressed_filename /= boost::lexical_cast<std::string>(new_guid);
        }

        std::ifstream file(filename_.to_string().c_str(), std::ios_base::in | std::ios_base::binary);
        try {
            boost::iostreams::filtering_istream in;
            in.push(boost::iostreams::gzip_decompressor());
            in.push(file);

            FILE* fout = filesystem::universal_open_file(uncompressed_filename, "w+b");
            if (fout != NULL)
            {
                ret_val = true;

                const int buf_size = 512;
                char buf[buf_size];
                while (true)
                {
                    try 
                    {
        	            std::streamsize to_read = buf_size;
                        in.read(buf, to_read);

                        std::streamsize read_bytes = in.gcount();

                        if (read_bytes > 0)
                        {
                            size_t n = fwrite(buf, 1, read_bytes, fout);
                            if (n != read_bytes)
                            {
                                ret_val = false;
                                log_error(L"gzipfile_reader::decompress. File write error in %1%") % uncompressed_filename.to_wstring();
                                break;
                            }
                        } else
                        {
                            break;
                        }
                    }
                    catch(std::exception e)
                    {
                        ret_val = false;
                        log_error(L"gzipfile_reader::decompress. File read/write exception %1%") % string_cast<EC_UTF8>(e.what());
                    }
                }
                fclose(fout);
            } else
            {
                log_error(L"gzipfile_reader::decompress. Failed to create output file %1%") % uncompressed_filename.to_wstring();
            }
        } catch(const boost::iostreams::gzip_error& e) 
        {
            ret_val = false;
            log_error(L"gzipfile_reader::decompress. Exception %1%") % string_cast<EC_UTF8>(e.what());
        }
    } else
    {
        log_error(L"gzipfile_reader::decompress. Read error. File corrupt? %1%") % filename_.to_wstring();
    }

    return ret_val;
}

}
}
