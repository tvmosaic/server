/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_os_version.h>

#ifdef _WIN32
#ifndef VER_SUITE_WH_SERVER
#define VER_SUITE_WH_SERVER                 0x00008000
#endif
#endif


namespace dvblink { namespace engine {

#ifdef _WIN32

os_version::os_version() :
    major_version_(0),
    minor_version_(0),
    os_version_(os_undefined),
    product_type_(product_undefined),
    processor_architecture_(processor_architecture_unknown)
{
    OSVERSIONINFOEXW osvi = {0};
    osvi.dwOSVersionInfoSize = sizeof(osvi);
    if (GetVersionEx(reinterpret_cast<OSVERSIONINFO*>(&osvi)))
    {
        major_version_ = osvi.dwMajorVersion;
        minor_version_ = osvi.dwMinorVersion;
    }

    typedef void (__stdcall *get_native_system_info_t)(LPSYSTEM_INFO);

    SYSTEM_INFO si;
    if (get_native_system_info_t gnsi = (get_native_system_info_t)GetProcAddress(GetModuleHandle(TEXT("kernel32.dll")), "GetNativeSystemInfo"))
    {
        gnsi(&si);
    }
    else
    {
        GetSystemInfo(&si);
    }
    
    processor_architecture_ = static_cast<e_processor_architecture>(si.wProcessorArchitecture);

    switch (major_version_)
    {
    case 6:
        {
            typedef BOOL (__stdcall *get_product_info_t)(DWORD, DWORD, DWORD, DWORD, PDWORD);

            DWORD dwType = 0;
            if (get_product_info_t gpi = (get_product_info_t)GetProcAddress(GetModuleHandle(TEXT("kernel32.dll")), "GetProductInfo"))
            {
                gpi(osvi.dwMajorVersion, osvi.dwMinorVersion, 0, 0, &dwType);
                product_type_ = static_cast<e_product_type>(dwType);
            }

            switch (minor_version_)
            {
            case 1:
                os_version_ = osvi.wProductType == VER_NT_WORKSTATION ? windows_7 : windows_server_2008_r2;
                break;
            case 0:
                os_version_ = osvi.wProductType == VER_NT_WORKSTATION ? windows_vista : windows_server_2008;
                break;
            }
            break;
        }

    case 5:
        {
            switch (minor_version_)
            {
            case 2:
                if (GetSystemMetrics(SM_SERVERR2))                                      os_version_ = windows_server_2003_r2;
                else if (osvi.wSuiteMask & VER_SUITE_STORAGE_SERVER)                    os_version_ = windows_server_2003;
                else if (osvi.wSuiteMask & VER_SUITE_WH_SERVER)                         os_version_ = windows_home_server;
                else if (osvi.wProductType == VER_NT_WORKSTATION &&
                         si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)     os_version_ = windows_xp_professional_x64_edition;
                else                                                                    os_version_ = windows_server_2003;
                break;

            case 1:
                os_version_ = windows_xp;
                break;

            case 0:
                os_version_ = windows_2000;
                break;
            }
            break;
        }
    }
}

bool os_version::is_xp() const
{
    return version() == windows_xp ||
           version() == windows_xp_professional_x64_edition ||
           version() == windows_server_2003 ||
           version() == windows_home_server ||
           version() == windows_server_2003_r2;
}

bool os_version::is_vista() const
{
    return version() == windows_vista ||
           version() == windows_server_2008;
}

bool os_version::is_7() const
{
    return version() == windows_7 ||
           version() == windows_server_2008_r2;
}

#else

os_version::os_version() :
    major_version_(0),
    minor_version_(0),
    os_version_(os_undefined),
    product_type_(product_undefined),
    processor_architecture_(processor_architecture_unknown)
{
}

#endif

} //engine
} //dvblink
