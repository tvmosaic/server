/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include "dl_atsc_qam_helper.h"

namespace dvblink { namespace engine {

unsigned long atsc_channel_to_center_frequency(int channel)
{
	switch (channel)
	{
		case 2 : return  57028615;
		case 3 : return  63028615;
		case 4 : return  69028615;
		case 5 : return  79028615;
		case 6 : return  85028615;
		case 7 : return 177028615;
		case 8 : return 183028615;
		case 9 : return 189028615;
		case 10 : return 195028615;
		case 11 : return 201028615;
		case 12 : return 207028615;
		case 13 : return 213028615;
		case 14 : return 473028615;
		case 15 : return 479028615;
		case 16 : return 485028615;
		case 17 : return 491028615;
		case 18 : return 497028615;
		case 19 : return 503028615;
		case 20 : return 509028615;
		case 21 : return 515028615;
		case 22 : return 521028615;
		case 23 : return 527028615;
		case 24 : return 533028615;
		case 25 : return 539028615;
		case 26 : return 545028615;
		case 27 : return 551028615;
		case 28 : return 557028615;
		case 29 : return 563028615;
		case 30 : return 569028615;
		case 31 : return 575028615;
		case 32 : return 581028615;
		case 33 : return 587028615;
		case 34 : return 593028615;
		case 35 : return 599028615;
		case 36 : return 605028615;
		case 37 : return 611028615;
		case 38 : return 617028615;
		case 39 : return 623028615;
		case 40 : return 629028615;
		case 41 : return 635028615;
		case 42 : return 641028615;
		case 43 : return 647028615;
		case 44 : return 653028615;
		case 45 : return 659028615;
		case 46 : return 665028615;
		case 47 : return 671028615;
		case 48 : return 677028615;
		case 49 : return 683028615;
		case 50 : return 689028615;
		case 51 : return 695028615;
		case 52 : return 701028615;
		case 53 : return 707028615;
		case 54 : return 713028615;
		case 55 : return 719028615;
		case 56 : return 725028615;
		case 57 : return 731028615;
		case 58 : return 737028615;
		case 59 : return 743028615;
		case 60 : return 749028615;
		case 61 : return 755028615;
		case 62 : return 761028615;
		case 63 : return 767028615;
		case 64 : return 773028615;
		case 65 : return 779028615;
		case 66 : return 785028615;
		case 67 : return 791028615;
		case 68 : return 797028615;
		case 69 : return 803028615;
    }
    return 0;
}

int atsc_center_frequency_to_channel(unsigned long frequency)
{
    //remove eventual rounding
    unsigned long freq = frequency / 100000;
    switch (freq)
    {
        case 570: return 2;
        case 630: return 3;
        case 690: return 4;
        case 790: return 5;
        case 850: return 6;
        case 1770: return 7;
        case 1830: return 8;
        case 1890: return 9;
        case 1950: return 10;
        case 2010: return 11;
        case 2070: return 12;
        case 2130: return 13;
        case 4730: return 14;
        case 4790: return 15;
        case 4850: return 16;
        case 4910: return 17;
        case 4970: return 18;
        case 5030: return 19;
        case 5090: return 20;
        case 5150: return 21;
        case 5210: return 22;
        case 5270: return 23;
        case 5330: return 24;
        case 5390: return 25;
        case 5450: return 26;
        case 5510: return 27;
        case 5570: return 28;
        case 5630: return 29;
        case 5690: return 30;
        case 5750: return 31;
        case 5810: return 32;
        case 5870: return 33;
        case 5930: return 34;
        case 5990: return 35;
        case 6050: return 36;
        case 6110: return 37;
        case 6170: return 38;
        case 6230: return 39;
        case 6290: return 40;
        case 6350: return 41;
        case 6410: return 42;
        case 6470: return 43;
        case 6530: return 44;
        case 6590: return 45;
        case 6650: return 46;
        case 6710: return 47;
        case 6770: return 48;
        case 6830: return 49;
        case 6890: return 50;
        case 6950: return 51;
        case 7010: return 52;
        case 7070: return 53;
        case 7130: return 54;
        case 7190: return 55;
        case 7250: return 56;
        case 7310: return 57;
        case 7370: return 58;
        case 7430: return 59;
        case 7490: return 60;
        case 7550: return 61;
        case 7610: return 62;
        case 7670: return 63;
        case 7730: return 64;
        case 7790: return 65;
        case 7850: return 66;
        case 7910: return 67;
        case 7970: return 68;
        case 8030: return 69;
    }
    return 0;
}

unsigned long qam_channel_to_center_frequency(int channel)
{
    switch (channel)
    {
        case 2: return 57000000;
        case 3: return 63000000;
        case 4: return 69000000;
        case 1: return 75000000;
        case 5: return 79000000;
        case 6: return 85000000;
        case 95: return 93000000;
        case 96: return 99000000;
        case 97: return 105000000;
        case 98: return 111000000;
        case 99: return 117000000;
        case 14: return 123000000;
        case 15: return 129000000;
        case 16: return 135000000;
        case 17: return 141000000;
        case 18: return 147000000;
        case 19: return 153000000;
        case 20: return 159000000;
        case 21: return 165000000;
        case 22: return 171000000;
        case 7: return 177000000;
        case 8: return 183000000;
        case 9: return 189000000;
        case 10: return 195000000;
        case 11: return 201000000;
        case 12: return 207000000;
        case 13: return 213000000;
        case 23: return 219000000;
        case 24: return 225000000;
        case 25: return 231000000;
        case 26: return 237000000;
        case 27: return 243000000;
        case 28: return 249000000;
        case 29: return 255000000;
        case 30: return 261000000;
        case 31: return 267000000;
        case 32: return 273000000;
        case 33: return 279000000;
        case 34: return 285000000;
        case 35: return 291000000;
        case 36: return 297000000;
        case 37: return 303000000;
        case 38: return 309000000;
        case 39: return 315000000;
        case 40: return 321000000;
        case 41: return 327000000;
        case 42: return 333000000;
        case 43: return 339000000;
        case 44: return 345000000;
        case 45: return 351000000;
        case 46: return 357000000;
        case 47: return 363000000;
        case 48: return 369000000;
        case 49: return 375000000;
        case 50: return 381000000;
        case 51: return 387000000;
        case 52: return 393000000;
        case 53: return 399000000;
        case 54: return 405000000;
        case 55: return 411000000;
        case 56: return 417000000;
        case 57: return 423000000;
        case 58: return 429000000;
        case 59: return 435000000;
        case 60: return 441000000;
        case 61: return 447000000;
        case 62: return 453000000;
        case 63: return 459000000;
        case 64: return 465000000;
        case 65: return 471000000;
        case 66: return 477000000;
        case 67: return 483000000;
        case 68: return 489000000;
        case 69: return 495000000;
        case 70: return 501000000;
        case 71: return 507000000;
        case 72: return 513000000;
        case 73: return 519000000;
        case 74: return 525000000;
        case 75: return 531000000;
        case 76: return 537000000;
        case 77: return 543000000;
        case 78: return 549000000;
        case 79: return 555000000;
        case 80: return 561000000;
        case 81: return 567000000;
        case 82: return 573000000;
        case 83: return 579000000;
        case 84: return 585000000;
        case 85: return 591000000;
        case 86: return 597000000;
        case 87: return 603000000;
        case 88: return 609000000;
        case 89: return 615000000;
        case 90: return 621000000;
        case 91: return 627000000;
        case 92: return 633000000;
        case 93: return 639000000;
        case 94: return 645000000;
        case 100: return 651000000;
        case 101: return 657000000;
        case 102: return 663000000;
        case 103: return 669000000;
        case 104: return 675000000;
        case 105: return 681000000;
        case 106: return 687000000;
        case 107: return 693000000;
        case 108: return 699000000;
        case 109: return 705000000;
        case 110: return 711000000;
        case 111: return 717000000;
        case 112: return 723000000;
        case 113: return 729000000;
        case 114: return 735000000;
        case 115: return 741000000;
        case 116: return 747000000;
        case 117: return 753000000;
        case 118: return 759000000;
        case 119: return 765000000;
        case 120: return 771000000;
        case 121: return 777000000;
        case 122: return 783000000;
        case 123: return 789000000;
        case 124: return 795000000;
        case 125: return 801000000;
        case 126: return 807000000;
        case 127: return 813000000;
        case 128: return 819000000;
        case 129: return 825000000;
        case 130: return 831000000;
        case 131: return 837000000;
        case 132: return 843000000;
        case 133: return 849000000;
        case 134: return 855000000;
        case 135: return 861000000;
        case 136: return 867000000;
        case 137: return 873000000;
        case 138: return 879000000;
        case 139: return 885000000;
        case 140: return 891000000;
        case 141: return 897000000;
        case 142: return 903000000;
        case 143: return 909000000;
        case 144: return 915000000;
        case 145: return 921000000;
        case 146: return 927000000;
        case 147: return 933000000;
        case 148: return 939000000;
        case 149: return 945000000;
        case 150: return 951000000;
        case 151: return 957000000;
        case 152: return 963000000;
        case 153: return 969000000;
        case 154: return 975000000;
        case 155: return 981000000;
        case 156: return 987000000;
        case 157: return 993000000;
        case 158: return 999000000;
    }
    return 0;
}

int qam_center_frequency_to_channel(unsigned long frequency)
{
    //remove rounding
    unsigned long freq = frequency / 100000;
    switch (freq)
    {
        case 570 : return 2;
        case 630 : return 3;
        case 690 : return 4;
        case 750 : return 1;
        case 790 : return 5;
        case 850 : return 6;
        case 930 : return 95;
        case 990 : return 96;
        case 1050 : return 97;
        case 1110 : return 98;
        case 1170 : return 99;
        case 1230 : return 14;
        case 1290 : return 15;
        case 1350 : return 16;
        case 1410 : return 17;
        case 1470 : return 18;
        case 1530 : return 19;
        case 1590 : return 20;
        case 1650 : return 21;
        case 1710 : return 22;
        case 1770 : return 7;
        case 1830 : return 8;
        case 1890 : return 9;
        case 1950 : return 10;
        case 2010 : return 11;
        case 2070 : return 12;
        case 2130 : return 13;
        case 2190 : return 23;
        case 2250 : return 24;
        case 2310 : return 25;
        case 2370 : return 26;
        case 2430 : return 27;
        case 2490 : return 28;
        case 2550 : return 29;
        case 2610 : return 30;
        case 2670 : return 31;
        case 2730 : return 32;
        case 2790 : return 33;
        case 2850 : return 34;
        case 2910 : return 35;
        case 2970 : return 36;
        case 3030 : return 37;
        case 3090 : return 38;
        case 3150 : return 39;
        case 3210 : return 40;
        case 3270 : return 41;
        case 3330 : return 42;
        case 3390 : return 43;
        case 3450 : return 44;
        case 3510 : return 45;
        case 3570 : return 46;
        case 3630 : return 47;
        case 3690 : return 48;
        case 3750 : return 49;
        case 3810 : return 50;
        case 3870 : return 51;
        case 3930 : return 52;
        case 3990 : return 53;
        case 4050 : return 54;
        case 4110 : return 55;
        case 4170 : return 56;
        case 4230 : return 57;
        case 4290 : return 58;
        case 4350 : return 59;
        case 4410 : return 60;
        case 4470 : return 61;
        case 4530 : return 62;
        case 4590 : return 63;
        case 4650 : return 64;
        case 4710 : return 65;
        case 4770 : return 66;
        case 4830 : return 67;
        case 4890 : return 68;
        case 4950 : return 69;
        case 5010 : return 70;
        case 5070 : return 71;
        case 5130 : return 72;
        case 5190 : return 73;
        case 5250 : return 74;
        case 5310 : return 75;
        case 5370 : return 76;
        case 5430 : return 77;
        case 5490 : return 78;
        case 5550 : return 79;
        case 5610 : return 80;
        case 5670 : return 81;
        case 5730 : return 82;
        case 5790 : return 83;
        case 5850 : return 84;
        case 5910 : return 85;
        case 5970 : return 86;
        case 6030 : return 87;
        case 6090 : return 88;
        case 6150 : return 89;
        case 6210 : return 90;
        case 6270 : return 91;
        case 6330 : return 92;
        case 6390 : return 93;
        case 6450 : return 94;
        case 6510 : return 100;
        case 6570 : return 101;
        case 6630 : return 102;
        case 6690 : return 103;
        case 6750 : return 104;
        case 6810 : return 105;
        case 6870 : return 106;
        case 6930 : return 107;
        case 6990 : return 108;
        case 7050 : return 109;
        case 7110 : return 110;
        case 7170 : return 111;
        case 7230 : return 112;
        case 7290 : return 113;
        case 7350 : return 114;
        case 7410 : return 115;
        case 7470 : return 116;
        case 7530 : return 117;
        case 7590 : return 118;
        case 7650 : return 119;
        case 7710 : return 120;
        case 7770 : return 121;
        case 7830 : return 122;
        case 7890 : return 123;
        case 7950 : return 124;
        case 8010 : return 125;
        case 8070 : return 126;
        case 8130 : return 127;
        case 8190 : return 128;
        case 8250 : return 129;
        case 8310 : return 130;
        case 8370 : return 131;
        case 8430 : return 132;
        case 8490 : return 133;
        case 8550 : return 134;
        case 8610 : return 135;
        case 8670 : return 136;
        case 8730 : return 137;
        case 8790 : return 138;
        case 8850 : return 139;
        case 8910 : return 140;
        case 8970 : return 141;
        case 9030 : return 142;
        case 9090 : return 143;
        case 9150 : return 144;
        case 9210 : return 145;
        case 9270 : return 146;
        case 9330 : return 147;
        case 9390 : return 148;
        case 9450 : return 149;
        case 9510 : return 150;
        case 9570 : return 151;
        case 9630 : return 152;
        case 9690 : return 153;
        case 9750 : return 154;
        case 9810 : return 155;
        case 9870 : return 156;
        case 9930 : return 157;
        case 9990 : return 158;
    }
    return 0;
}

} //engine
} //dvblink
