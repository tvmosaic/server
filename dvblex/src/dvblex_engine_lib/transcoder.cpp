/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/asio.hpp>
#include <dl_logger.h>
#include <dl_language_settings.h>
#include <dl_transcoder.h>

using namespace dvblink::logging;
using namespace dvblink::engine;

namespace dvblink { namespace transcoder {

const std::string ffmpeg_param_src_id  = "%SRC%";
const std::string ffmpeg_param_dest_id  = "%DEST%";
const std::string ffmpeg_param_width_id  = "%WIDTH%";
const std::string ffmpeg_param_height_id  = "%HEIGHT%";
const std::string ffmpeg_param_bitrate_id  = "%BITRATE%";
const std::string ffmpeg_param_scale_id  = "%SCALE%";
const std::string ffmpeg_param_offsetsec_id  = "%OFFSETSEC%";
const std::string ffmpeg_param_extraparams_id  = "%EXTRAPARAMS%";

const std::string ffmpeg_condition_value_true  = "true";
const std::string ffmpeg_condition_value_false  = "false";

static const char* tv_usecase_ids[] = {tu_generate_thumb_from_file_id.c_str(), tu_transcode_live_to_ts_id.c_str(), tu_transcode_live_to_hls_id.c_str(),
                                        tu_transcode_live_to_mp4_id.c_str(), tu_transcode_recording_to_hls_id.c_str(), tu_transcode_sendto_transcode_id.c_str()};

static const char* tv_usecase_names[] = {tu_generate_thumb_from_file_name.c_str(), tu_transcode_live_to_ts_name.c_str(), tu_transcode_live_to_hls_name.c_str(),
                                        tu_transcode_live_to_mp4_name.c_str(), tu_transcode_recording_to_hls_name.c_str(), tu_transcode_sendto_transcode_name.c_str()};

static const size_t tv_usecase_num = sizeof(tv_usecase_names) / sizeof(char*);

void get_transcoder_usecase_list(transcoder_usecase_list_t& tu_list)
{
    for (size_t i=0; i<tv_usecase_num; i++)
    {
        transcoder_usecase_t uc;
        uc.id_ = tv_usecase_ids[i];
        uc.name_ = language_settings::GetInstance()->GetItemName(tv_usecase_names[i]);
        tu_list.push_back(uc);
    }
}

template <typename PARAM_TYPE>
static void substitute_parameter(const std::string& param, const PARAM_TYPE& value, bool use_quotes, std::string& param_template)
{
    std::string value_str;

    try
    {
        value_str = boost::lexical_cast<std::string>(value);
    } catch (boost::bad_lexical_cast& /*e*/) {}

#ifdef WIN32
    if (use_quotes)
        value_str = "\"" + value_str + "\"";
#endif

    boost::replace_all(param_template, param, value_str);
}

bool init_ffmpeg_launch_arguments(const ffmpeg_param_template_list_t& template_arguments, 
    const ffmpeg_param_template_init_t& init_info, std::vector<dvblink::launch_param_t>& arguments)
{
    for (size_t i=0; i<template_arguments.size(); i++)
    {
        ffmpeg_param_template_t param_template = template_arguments[i];
        std::string param = param_template.param_.to_string();

        ffmpeg_param_condition_map_t::iterator cnd_it;

        //condition dimension
        cnd_it = param_template.conditions_.find(ffmpeg_condition_dimensions);
        if (cnd_it != param_template.conditions_.end())
        {
            //check if condition is true
            if (boost::iequals(cnd_it->second.to_string(), ffmpeg_condition_value_true) && 
                    (init_info.width_ == ffmpeg_param_invalid_dimension ||
                    init_info.height_ == ffmpeg_param_invalid_dimension))
                continue;

            if (boost::iequals(cnd_it->second.to_string(), ffmpeg_condition_value_false) && 
                    (init_info.width_ != ffmpeg_param_invalid_dimension ||
                    init_info.height_ != ffmpeg_param_invalid_dimension))
                continue;
        }

        //condition bitrate
        cnd_it = param_template.conditions_.find(ffmpeg_condition_bitrate);
        if (cnd_it != param_template.conditions_.end())
        {
            //check if condition is true
            if (boost::iequals(cnd_it->second.to_string(), ffmpeg_condition_value_true) && 
                    init_info.bitrate_kbits_ == ffmpeg_param_invalid_bitrate)
                continue;

            if (boost::iequals(cnd_it->second.to_string(), ffmpeg_condition_value_false) && 
                    init_info.bitrate_kbits_ != ffmpeg_param_invalid_bitrate)
                continue;
        }

        //condition video scale
        cnd_it = param_template.conditions_.find(ffmpeg_condition_scale);
        if (cnd_it != param_template.conditions_.end())
        {
            //check if condition is true
            if (boost::iequals(cnd_it->second.to_string(), ffmpeg_condition_value_true) && 
                    init_info.video_scale_ == ffmpeg_param_invalid_dimension)
                continue;

            if (boost::iequals(cnd_it->second.to_string(), ffmpeg_condition_value_false) && 
                    init_info.video_scale_ != ffmpeg_param_invalid_dimension)
                continue;
        }

        //check if extra params are added
        if (boost::iequals(param, ffmpeg_param_extraparams_id))
        {
            arguments.insert(arguments.end(), init_info.extra_params.begin(), init_info.extra_params.end());
        } else
        {
            substitute_parameter(ffmpeg_param_src_id, init_info.src_, true, param);
            substitute_parameter(ffmpeg_param_dest_id, init_info.dest_, true, param);
            substitute_parameter(ffmpeg_param_offsetsec_id, init_info.offset_sec_, false, param);
            substitute_parameter(ffmpeg_param_bitrate_id, init_info.bitrate_kbits_, false, param);
            substitute_parameter(ffmpeg_param_width_id, init_info.width_, false, param);
            substitute_parameter(ffmpeg_param_height_id, init_info.height_, false, param);
            substitute_parameter(ffmpeg_param_scale_id, init_info.video_scale_, false, param);

            arguments.push_back(param);
        }
    }

    return true;
}

} //transcoder
} //dvblink
