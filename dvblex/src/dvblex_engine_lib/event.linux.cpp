/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifdef WIN32
#error Linux implementation
#endif

#include "stdafx.h"
#include <errno.h>
#include <stdexcept>
#include <pthread.h>
#include <sys/time.h>
#include "dl_event.h"

using namespace dvblink;

struct dvblink::event::_impl
{
    pthread_cond_t condvar;
    pthread_mutex_t mutex;
    bool signaled;
};

dvblink::event::event()
{
    std::auto_ptr<_impl> ptr(new _impl);
    ptr->signaled = false;

    int rc1 = pthread_mutex_init(&(ptr->mutex), NULL);
    int rc2 = pthread_cond_init(&(ptr->condvar), NULL);

    if ((rc1 != 0) || (rc2 != 0))
    {
        throw std::runtime_error("Cannot create event object");
    }

    pimpl_ = ptr;
}

dvblink::event::~event()
{
    pthread_cond_destroy(&(pimpl_->condvar));
    pthread_mutex_destroy(&(pimpl_->mutex));    
}

dvblink::errcode_t dvblink::event::wait(timeout_t ms) const
{
    dvblink::errcode_t err = err_none;

    // Lock the mutex
    int rc = pthread_mutex_lock(&(pimpl_->mutex));

    if (rc != 0)
    {
        switch (rc)
        {
        case EINVAL:
            // Invalid parameter
            err = err_invalid_param;
            break;

        case EDEADLK:
            // A deadlock would occur if the thread blocked
            // waiting for mutex
            err = err_deadlock;
            break;

        default:
            // Unexpected error
            err = err_unexpected;
        }
    }

    if (err != err_none)
    {
        return err; // error
    }

    if (pimpl_->signaled == false)
    {
        if (!ms.is_special())
        {
        	// Prepare timeout value
            struct timespec ts;

            unsigned long sec_add = long(ms.total_milliseconds() / MILLI);
            unsigned long nsec_add = long ((ms.total_milliseconds() % MILLI) * MICRO);

        #if defined (__FreeBSD__)
            // Get current time
            rc = clock_gettime(CLOCK_REALTIME, &ts);

            if (rc != 0)
            {
                // Timer error
                pthread_mutex_unlock(&(pimpl_->mutex));
                return err_invalid_param;
            }

            ts.tv_sec += sec_add;
            ts.tv_nsec += nsec_add;
        
        #else
            // Get current time
            struct timeval tv;

            rc = gettimeofday(&tv, NULL);

            if (rc != 0)
            {
                // Timer error
                pthread_mutex_unlock(&(pimpl_->mutex));
                return err_invalid_param;
            }

            ts.tv_sec = tv.tv_sec + sec_add;
            ts.tv_nsec = (tv.tv_usec * MILLI) + nsec_add;
        
        #endif

            // Normalize timeout value
            while ((unsigned long)ts.tv_nsec >= NANO)
            {
                ts.tv_sec++;
                ts.tv_nsec -= NANO;
            }

            // Waiting...
            rc = pthread_cond_timedwait(&(pimpl_->condvar),
                &(pimpl_->mutex), &ts);
        }
        else
        {
            // Waiting...
            rc = pthread_cond_wait(&(pimpl_->condvar), &(pimpl_->mutex));
        }

        if (rc == 0)
        {
            pimpl_->signaled = false;
        }
        else if (rc == ETIMEDOUT)
        {
            err = err_timeout;
        }
        else
        {
            err = err_unexpected;
        }
    }
    else
    {
        pimpl_->signaled = false;
    }

    pthread_mutex_unlock(&(pimpl_->mutex));

    return err;
}

dvblink::errcode_t dvblink::event::signal()
{
    pthread_mutex_lock(&(pimpl_->mutex));

    int rc = pthread_cond_signal(&(pimpl_->condvar));

    if (rc == 0)
    {
        pimpl_->signaled = true;
    }

    pthread_mutex_unlock(&(pimpl_->mutex));

    return (rc == 0) ? err_none : err_error;
}

dvblink::errcode_t dvblink::event::reset()
{
    pthread_mutex_lock(&(pimpl_->mutex));
    pimpl_->signaled = false;
    pthread_mutex_unlock(&(pimpl_->mutex));

    return err_none;
}

bool dvblink::event::is_signaled() const
{
    pthread_mutex_lock(&(pimpl_->mutex));
    bool signaled = pimpl_->signaled;
    pimpl_->signaled = false;    // reset event!
    pthread_mutex_unlock(&(pimpl_->mutex));

    return signaled;
}

// $Id: event.linux.cpp 2007 2011-03-03 11:11:21Z mike $
