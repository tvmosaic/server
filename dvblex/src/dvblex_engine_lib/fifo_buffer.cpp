/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_fifo_buffer.h>

#ifdef _MSC_VER
#pragma warning(disable : 4127)
#endif

namespace dvblink {
namespace engine {

fifo_buffer::fifo_buffer(size_t max_size, size_t block_size) :
    max_size_(max_size), block_size_(block_size), size_(0), read_offs_(0)
{
}

fifo_buffer::~fifo_buffer()
{
}

size_t fifo_buffer::size() const
{
    boost::mutex::scoped_lock lock(lock_);
    return size_;
}

bool fifo_buffer::empty() const
{
    boost::mutex::scoped_lock lock(lock_);
    return (size_ == 0);
}

void fifo_buffer::clear()
{
    boost::mutex::scoped_lock lock(lock_);
    queue_.clear();
    size_ = 0;
    read_offs_ = 0;
}

void fifo_buffer::swap(fifo_buffer& right)
{
    if (this == &right)
    {        
        return; // same object, do nothing
    }

    boost::mutex::scoped_lock lock(lock_);
    boost::mutex::scoped_lock rlock(right.lock_);

    block_size_ = right.block_size_;
    max_size_ = right.max_size_;
    size_ = right.size_;
    read_offs_ = right.read_offs_;

    queue_.swap(right.queue_);
    pool_.swap(right.pool_);
}

fifo_buffer::block_ptr fifo_buffer::_create_block()
{
    if (pool_.empty())
    {
        block_ptr ptr = block_ptr(new block_t);
        ptr->reserve(block_size_);
        return ptr;
    }

    block_ptr ptr = pool_.back();
    pool_.pop_back();

    if (ptr->size() > 0)
    {
        ptr->resize(0);
    }

    return ptr;
}

void fifo_buffer::_remove_block()
{
    block_ptr ptr = queue_.front();
    queue_.pop_front();
    pool_.push_back(ptr);
}

bool fifo_buffer::write(const void* data, size_t size)
{
    if (!data || (size == 0))
    {
        return false;   // invalid parameter
    }

    do
    {
        boost::mutex::scoped_lock lock(lock_);
        assert(_validate());

        if ((max_size_ > 0) && ((size_ + size) > max_size_))
        {
            return false;
        }

        size_ += size;
        const byte_t* p = static_cast<const byte_t*>(data);

        if (queue_.empty())
        {
            assert(read_offs_ == 0);
            block_ptr ptr = _create_block();
            queue_.push_back(ptr);

            if (size <= block_size_)
            {
                ptr->resize(size);
                memcpy(&(*ptr)[0], p, size);
                break;  // done
            }
            else
            {
                ptr->resize(block_size_);
                memcpy(&(*ptr)[0], p, block_size_);
                size -= block_size_;
                p += block_size_;
            }
        }
        else
        {
            block_ptr& last = queue_.back();
            size_t free_space = block_size_ - last->size();

            if (free_space > 0)
            {
                void* dst = &(*last)[0] + last->size();

                if (size <= free_space)
                {
                    last->resize(last->size() + size);
                    memcpy(dst, p, size);
                    break;  // done
                }
                else
                {
                    last->resize(block_size_);
                    memcpy(dst, p, free_space);
                    size -= free_space;
                    p += free_space;
                }
            }
        }

        size_t full_blocks = size / block_size_;
        size_t last_block_size = size % block_size_;

        for (size_t j = 0; j < full_blocks; j++)
        {
            block_ptr ptr = _create_block();
            ptr->resize(block_size_);
            memcpy(&(*ptr)[0], p, block_size_);
            p += block_size_;
            queue_.push_back(ptr);  // add full block
        }

        if (last_block_size > 0)
        {
            block_ptr ptr = _create_block();
            ptr->resize(last_block_size);
            memcpy(&(*ptr)[0], p, last_block_size);
            queue_.push_back(ptr);  // add last block
        }

        assert(_validate());
    }
    while (false);

    write_event_.signal();
    return true;
}

bool fifo_buffer::read(void* data, size_t size)
{
    if (!data || (size == 0))
    {
        return false;
    }

    do
    {
        boost::mutex::scoped_lock lock(lock_);
        assert(_validate());

        if (size_ < size)
        {
            return false;
        }

        assert(queue_.size() > 0);
        size_ -= size;

        block_ptr& first = queue_.front();
        assert(first->size() > read_offs_);

        size_t first_block_bytes = first->size() - read_offs_;
        byte_t* p = static_cast<byte_t*>(data);

        if (first_block_bytes >= size)
        {
            const void* src = &(*first)[0] + read_offs_;
            memcpy(p, src, size);

            if (first_block_bytes == size)
            {
                _remove_block();
                read_offs_ = 0;
            }
            else
            {
                read_offs_ += size;
                assert(read_offs_ < first->size());
            }

            assert(_validate());
            break;  // done
        }
        else
        {
            const void* src = &(*first)[0] + read_offs_;
            memcpy(p, src, first_block_bytes);
            p += first_block_bytes;
            size -= first_block_bytes;
            _remove_block();
            assert(queue_.size() > 0);
        }

        size_t full_blocks = size / block_size_;
        size_t from_last_block = size % block_size_;

        for (size_t j = 0; j < full_blocks; j++)
        {
            block_ptr ptr = queue_.front();
            memcpy(p, &(*ptr)[0], block_size_);
            p += block_size_;
            _remove_block();

            assert(size >= block_size_);
            size -= block_size_;
        }

        if (from_last_block > 0)
        {
            assert(queue_.size() > 0);
            assert(size == from_last_block);

            block_ptr ptr = queue_.front();
            assert(from_last_block <= ptr->size());
            memcpy(p, &(*ptr)[0], from_last_block);

            if (from_last_block < ptr->size())
            {
                read_offs_ = from_last_block;
            }
            else
            {
                _remove_block();
                read_offs_ = 0;
            }
        }
        else
        {
            read_offs_ = 0;
        }

        if (queue_.empty())
        {
            read_offs_ = 0;
        }

        assert(_validate());
    }
    while (false);

    read_event_.signal();
    return true;
}

bool fifo_buffer::wait_for_readable(dvblink::timeout_t to)
{
    dvblink::errcode_t err = write_event_.wait(to);
    return ((err == dvblink::err_none) ? true : false);
}

bool fifo_buffer::wait_for_writable(dvblink::timeout_t to)
{
    dvblink::errcode_t err = read_event_.wait(to);
    return ((err == dvblink::err_none) ? true : false);
}

bool fifo_buffer::_validate()
{
    const size_t qsize = queue_.size();

    if (qsize == 0)
    {
        if (read_offs_ != 0)
        {
            return false;
        }

        return true;
    }
 
    size_t first_bytes = 0;
    size_t last_bytes = 0;
    size_t middle_blocks = qsize;

    block_ptr& first = queue_.front();
    const size_t first_size = first->size();

    if ((first_size == 0) || (first_size > block_size_))
    {
        assert(0);
        return false;
    }

    if (read_offs_ >= first_size)
    {
        assert(0);
        return false;
    }

    first_bytes = first_size - read_offs_;

    if (qsize > 1)
    {
        block_ptr& last = queue_.back();
        const size_t last_size = last->size();

        if ((last_size == 0) || (last_size > block_size_))
        {
            assert(0);
            return false;
        }

        last_bytes = last_size;
    }

    if (first_bytes > 0)
    {
        middle_blocks--;
    }

    if (last_bytes > 0)
    {
        if (middle_blocks == 0)
        {
            assert(0);
            return false;
        }

        middle_blocks--;
    }

    size_t size = (middle_blocks * block_size_) + first_bytes + last_bytes;

    if (size_ != size)
    {
        assert(0);
        return false;
    }

    return true;
}

} // namespace engine
} // namespace dvblink

// $Id: fifo_buffer.cpp 6337 2012-09-25 07:53:29Z mike $
