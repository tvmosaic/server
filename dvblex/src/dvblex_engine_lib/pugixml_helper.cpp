/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <iostream>
#include <string.h>
#include <dl_strings.h>
#include <dl_pugixml_helper.h>

using namespace pugi;
using namespace dvblink::engine;

namespace dvblink { namespace pugixml_helpers {

xml_node new_child(pugi::xml_node& node, const std::string& name)
{
    return node.append_child(name.c_str());
}

xml_node new_child(xml_node& node, const std::string& name, const std::wstring& value)
{
    return new_child(node, name, value.c_str());
}

xml_node new_child(xml_node& node, const std::string& name, const std::string& value)
{
    return new_child(node, name, value.c_str());
}

xml_node new_child(xml_node& node, const std::string& name, const char* value)
{
    xml_node new_node = node.append_child(name.c_str());
    new_node.append_child(node_pcdata).set_value(value);
    return new_node;
}

xml_node new_child(xml_node& node, const std::string& name, const wchar_t* value)
{
    xml_node new_node = node.append_child(name.c_str());
    new_node.append_child(node_pcdata).set_value(string_cast<EC_UTF8>(value).c_str());
    return new_node;
}

void add_node_attribute(xml_node& node, const std::string& name, const std::wstring& value)
{
    add_node_attribute(node, name, string_cast<EC_UTF8>(value));
}

void add_node_attribute(xml_node& node, const std::string& name, const std::string& value)
{
    xml_attribute attr_node = node.append_attribute(name.c_str());
    attr_node.set_value(value.c_str());
}

bool get_node_value(const xml_node& parent_node, const std::string& name, std::string& value)
{
    if (xml_node node = parent_node.child(name.c_str()))
    {
        value = node.child_value();
        return true;
    }
    return false;
}

bool get_node_value(const xml_node& parent_node, const std::string& name, std::wstring& wvalue)
{
    std::string avalue;
    if (get_node_value(parent_node, name, avalue))
    {
        wvalue = string_cast<EC_UTF8>(avalue);
        return true;
    }
    return false;
}

bool get_node_attribute(const xml_node& node, const std::string& attribute_name, std::string& value)
{
    if (xml_attribute attr = node.attribute(attribute_name.c_str()))
    {
        value = attr.value();
        return true;
    }
    return false;
}

bool get_node_attribute(const pugi::xml_node& node, const std::string& attribute_name, std::wstring& wvalue)
{
    std::string avalue;
    if (get_node_attribute(node, attribute_name, avalue))
    {
        wvalue = string_cast<EC_UTF8>(avalue);
        return true;
    }
    return false;
}

struct xml_string_writer : public xml_writer
{
public:
    xml_string_writer(std::string& settings) :
        settings_(settings)
    {
    }

    xml_string_writer& operator = (const xml_string_writer& rhs)
    {
        settings_ = rhs.settings_;
        return *this;
    }

    void write(const void* data, size_t size)
    {
        settings_ += std::string(static_cast<const char*>(data), size);
    }

private:
    std::string& settings_;
};

void xmldoc_dump_to_string(const xml_document& doc, std::string& dump)
{
    xml_string_writer writer(dump);
    doc.save(writer, PUGIXML_TEXT("\t"), pugi::format_default, pugi::encoding_utf8);
}

bool xmldoc_dump_to_file(const pugi::xml_document& doc, const std::string& filename)
{
    return doc.save_file(filename.c_str(), PUGIXML_TEXT("\t"), pugi::format_default, pugi::encoding_utf8);
}

} //pugixml_helpers
} //dvblink
