/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_command_queue.h>


namespace dvblink { namespace engine {

command_queue::command_queue()
{
}

command_queue::~command_queue()
{
    //walk through all queue items and release them
    boost::mutex::scoped_lock lock(m_cs);
    while (m_Queue.size() > 0)
    {
        FinishCommand(&(m_Queue.front()));
        m_Queue.pop_front();
    }
}

bool command_queue::ExecuteCommand(unsigned long cmd_id, void* param)
{
    //check if this is modal command
    if (cmd_id >= post_command_offset)
    {
        return false;
    }

    //create new command structure and insert it into the list
    SDLCommandItem* item = new SDLCommandItem();
    item->id = cmd_id;
    item->param = param;

    {
        //submit event to the queue
        boost::mutex::scoped_lock lock(m_cs);
        m_Queue.push_back(item);
    }

    //wait on the event for command to be finished
    item->wait_event.wait(); //INFINITE waiting
    delete item;

    return true;
}

bool command_queue::PostCommand(unsigned long cmd_id, void* param)
{
    //check if this is post command
    if (cmd_id < post_command_offset)
    {
        return false;
    }

    //create new command structure, set post flag and insert it into the list
    SDLCommandItem* item = new SDLCommandItem();
    item->id = cmd_id;
    item->param = param;

    {
        //submit event to the queue
        boost::mutex::scoped_lock lock(m_cs);
        m_Queue.push_back(item);
    }

    return true;
}

bool command_queue::PeekCommand(SDLCommandItem** item)
{
    bool ret_val = false;
    boost::mutex::scoped_lock lock(m_cs);

    if (m_Queue.size() > 0)
    {
        *item = m_Queue.front();
        m_Queue.pop_front();
        ret_val = true;
    }

    return ret_val;
}

bool command_queue::FinishCommand(SDLCommandItem** item)
{
	bool bpost = (*item)->id >= post_command_offset;

    (*item)->wait_event.signal();

    //check if it is post command and delete it if it is
	// if it is not, the command will be deleted by a caller (send command)
    if ( bpost )
    {
        if ((*item)->param != NULL)
            delete (*item)->param;

        delete *item;
    }

    return true;
}

void command_queue::remove_commands(item_compare_f compare_func, void* param)
{
    boost::mutex::scoped_lock lock(m_cs);

    std::list<SDLCommandItem*>::iterator it = m_Queue.begin();
    while (it != m_Queue.end())
    {
        SDLCommandItem* item = (*it);
        if (compare_func(item, param))
        {
            FinishCommand(&item);
            m_Queue.erase(it);

            it = m_Queue.begin();
        } else
        {
            ++it;
        }
    }
}

} //engine
} //dvblink
