/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <dl_strings.h>
#include <dl_levenshtein.h>

using namespace dvblink::engine;

namespace dvblex { 

static const unsigned int LEVENSHTEIN_DISTANCE_MATCH = 0;
static const unsigned int LEVENSHTEIN_MIN_STRING_LEN = 3;
static const unsigned int LEVENSHTEIN_DISTANCE_LIMIT = 2;

levenshtein_distance_calc::levenshtein_distance_calc()
{
}

levenshtein_distance_result_e levenshtein_distance_calc::match(const std::string& first_str, const std::string& second_str)
{
    levenshtein_distance_result_e result = ldr_none;

    //string has to be converted to wstring as to_lower_copy does not work with non-ascii characters for std::string
    std::wstring first_str_cp = boost::algorithm::to_lower_copy(string_cast<EC_UTF8>(first_str));
    std::wstring second_str_cp = boost::algorithm::to_lower_copy(string_cast<EC_UTF8>(second_str));

    std::wstring::size_type first_str_len = first_str_cp.size();
    std::wstring::size_type second_str_len = second_str_cp.size();

    std::wstring::size_type type_len = levenshtein_distance(first_str_cp, second_str_cp);
    if (LEVENSHTEIN_DISTANCE_MATCH == type_len)
    {
        result = ldr_exact;
    }
    else
    {
        if (LEVENSHTEIN_MIN_STRING_LEN < first_str_len && LEVENSHTEIN_DISTANCE_LIMIT >= type_len)
        {
            result = ldr_partial;
        }
        else
        {
            //only do inclusion test if both strings are longer than minimum size
            if (first_str_len >= LEVENSHTEIN_MIN_STRING_LEN && second_str_len >= LEVENSHTEIN_MIN_STRING_LEN)
            {
                if (first_str_len >= second_str_len)
                {
                    if (std::string::npos != first_str_cp.find(second_str_cp))
                    {                   
                        result = ldr_partial;
                    }
                }
                else
                {
                    if (std::string::npos != second_str_cp.find(first_str_cp))
                    {
                        result = ldr_partial;
                    }
                }
            }
        }
    }

    return result;
}

}