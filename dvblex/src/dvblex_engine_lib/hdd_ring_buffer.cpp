/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/thread/mutex.hpp>
#include <boost/filesystem.hpp>
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_file_procedures.h>
#include "dl_hdd_ring_buffer.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

namespace dvblink {
namespace engine {

hdd_ring_buffer::hdd_ring_buffer()
    : f_write_(NULL)
{
}

hdd_ring_buffer::~hdd_ring_buffer()
{
}

bool hdd_ring_buffer::init(const dvblink::filesystem_path_t& temp_file, boost::uint64_t max_size)
{
    write_pos_ = 0;
    read_pos_ = 0;
    max_size_ = max_size;
    temp_file_ = temp_file;
    event_.reset();
    time(&start_time_);
    buffer_duration_ = -1;
    
    
    f_write_ = filesystem::universal_open_file(temp_file_, "wb+");
    bool res = f_write_ != NULL;
    
    if (!res)
    {
        log_error(L"hdd_ring_buffer::init. Unable to open ring buffer file %1%") % temp_file_.to_wstring();
        if (f_write_ != NULL)
            fclose(f_write_);
            
        f_write_ = NULL;
    }
    
    return res;
}

void hdd_ring_buffer::term()
{
    if (f_write_ != NULL)
        fclose(f_write_);
        
    f_write_ = NULL;
    
    //delete temp file
    try
    {
        boost::filesystem::remove(temp_file_.to_boost_filesystem());
    } catch(...)
    {
    }
}

boost::uint64_t hdd_ring_buffer::position_to_buffer(boost::uint64_t pos)
{
    return (pos % max_size_);
}

bool hdd_ring_buffer::write_stream(const unsigned char* buffer, size_t size)
{
    if (size > max_size_)
        return false;
        
    size_t write_first = size;
    size_t write_second = 0;

    boost::unique_lock<boost::shared_mutex> lock(lock_);
    
    //check if we have to wrap
    if (position_to_buffer(write_pos_) + size > max_size_)
    {
        write_first = max_size_ - position_to_buffer(write_pos_);
        write_second = size - write_first;
    }
    
    if (write_first > 0)
    {
        filesystem::universal_file_seek(f_write_, position_to_buffer(write_pos_), SEEK_SET);
        size_t res_size = fwrite(buffer, 1, write_first, f_write_);
        if (res_size != write_first)
        {
            //probably there is no disk space anymore - try to wrap around now and adjust max_size_
            log_ext_info(L"hdd_ring_buffer::write_stream. Written %1% bytes instead of planned %2%. No disk space?.. Doing wrap around now and adjusting max_size_") % res_size % write_first;
            write_second += (write_first - res_size);
            write_first = res_size;
            max_size_ = position_to_buffer(write_pos_) + res_size;
        }
        //advance write pointer
        write_pos_ += res_size;
    }
    
    //the reminder
    if (write_second > 0)
    {
        log_ext_info(L"hdd_ring_buffer::write_stream. Wrap around");
    
        filesystem::universal_file_seek(f_write_, 0, SEEK_SET);
        
        size_t res_size = fwrite(buffer + write_first, 1, write_second, f_write_);
        if (res_size != write_second)
        {
            log_ext_info(L"hdd_ring_buffer::write_stream. Written %1% bytes instead of planned %2% after wrap around. No disk space?.. Exiting now.") % res_size % write_first;
            return false;
        } else
        {
            //advance write pointer
            write_pos_ += res_size;
        }
        //update buffer duration
        if (buffer_duration_ == -1)
        {
            time_t now;
            time(&now);
            buffer_duration_ = now - start_time_;
            log_ext_info(L"hdd_ring_buffer::write_stream. Calculated buffer duration is %1%") % buffer_duration_;
        }
    }
    
    //adjust read pointer if it falls out of the window
    if (write_pos_ >= max_size_ && read_pos_ < write_pos_ - max_size_)
    {
        read_pos_ = write_pos_ - max_size_;
    }
    
    event_.signal();
    
    return true;
}

bool hdd_ring_buffer::read_stream(unsigned char* buffer, boost::uint64_t& size, size_t timeout_ms /*= 100*/)
{
    bool ret_val = false;
    
    lock_.lock();
    
    boost::uint64_t size_to_read = (write_pos_ - read_pos_) >= size ? size : (write_pos_ - read_pos_);
    if (size_to_read == 0)
    {
        //release lock and wait
        event_.reset();
        lock_.unlock();
        
        event_.wait(timeout_ms);
        
        lock_.lock();
    }
    
    size_to_read = (write_pos_ - read_pos_) >= size ? size : (write_pos_ - read_pos_);
    if (size_to_read > 0)
    {
        ret_val = true;
        size = size_to_read;

        //read data
        size_t read_first = size_to_read;
        size_t read_second = 0;
        
        //check if we have to wrap
        if (position_to_buffer(read_pos_) + size_to_read > max_size_)
        {
            read_first = max_size_ - position_to_buffer(read_pos_);
            read_second = size_to_read - read_first;
        }
        
        if (read_first > 0)
        {
            filesystem::universal_file_seek(f_write_, position_to_buffer(read_pos_), SEEK_SET);
            
            size_t res_size = fread(buffer, 1, read_first, f_write_);
            if (res_size != read_first)
            {
                ret_val = false;
            } else
            {
                //advance write pointer
                read_pos_ += res_size;
            }
        }
        
        //the reminder
        if (read_second > 0)
        {
            log_ext_info(L"hdd_ring_buffer::read_stream. Wrap around");
            
            filesystem::universal_file_seek(f_write_, 0, SEEK_SET);
            
            size_t res_size = fread(buffer + read_first, 1, read_second, f_write_);
            if (res_size != read_second)
            {
                ret_val = false;
            } else
            {
                //advance write pointer
                read_pos_ += res_size;
            }
        }
        
    }
    
    lock_.unlock();
    
    return ret_val;
}

boost::uint64_t hdd_ring_buffer::get_max_buffer_size()
{
    return max_size_;
}

//a * b / d
static boost::uint64_t lossy_mul_div_64(boost::uint64_t a, boost::uint64_t b, boost::uint64_t d)
{
    long double f = (long double)b/d;

    boost::uint64_t high_part = boost::uint64_t((a & ~0xffffffff) * f + 0.5);
    boost::uint64_t low_part = boost::uint64_t((a & 0xffffffff) * f + 0.5);

    return high_part + low_part;
}

void hdd_ring_buffer::get_stats(boost::uint64_t& position, boost::uint64_t& length, time_t& duration, time_t& position_sec)
{
    time_t now;
    time(&now);

    boost::shared_lock<boost::shared_mutex> lock(lock_);
    position = write_pos_ > max_size_ ? read_pos_ - (write_pos_ - max_size_) : read_pos_;
    length = write_pos_ > max_size_ ? max_size_ : write_pos_;
    duration = buffer_duration_ == -1 ? now - start_time_ : buffer_duration_;
    //we use average bitrate for now to calculate position in time units
    if (length > 0)
        position_sec = (time_t)lossy_mul_div_64(position, (boost::uint64_t)duration, length);
    else
        position_sec = 0;
}

bool hdd_ring_buffer::read_seek_time(time_t pos, int whence, boost::uint64_t& cur_pos)
{
    time_t now;
    time(&now);

    boost::uint64_t length = write_pos_ > max_size_ ? max_size_ : write_pos_;
    time_t duration = buffer_duration_ == -1 ? now - start_time_ : buffer_duration_;

    if (duration == 0)
        return false;

    //translate time into bytes using average bitrate
    boost::int64_t sign = pos < 0 ? -1 : 1;
    boost::uint64_t u_pos_bytes = lossy_mul_div_64((boost::uint64_t)(pos * sign), length, (boost::uint64_t)duration);
    boost::int64_t pos_bytes = u_pos_bytes * sign;

    log_ext_info(L"hdd_ring_buffer::read_seek_time. offset %1% sec, %2% bytes") % pos % pos_bytes;

    return read_seek_bytes(pos_bytes, whence, cur_pos);
}

bool hdd_ring_buffer::read_seek_bytes(boost::int64_t pos, int whence, boost::uint64_t& cur_pos)
{
    bool ret_val = false;

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    boost::uint64_t res_pos = 0;
    switch (whence)
    {
        case SEEK_SET:
            {
                if (pos >= 0)
                {
                    boost::uint64_t start = write_pos_ > max_size_ ? write_pos_ - max_size_ : 0;
                    res_pos = start + pos;
                    if (res_pos <= write_pos_)
                        ret_val = true;
                }
            }
            break;
        case SEEK_CUR:
            {
                if (pos < 0 && static_cast<boost::uint64_t>(((-1)*pos)) < read_pos_)
                {
                    res_pos = read_pos_ + pos;
                    boost::uint64_t start = write_pos_ > max_size_ ? write_pos_ - max_size_ : 0;
                    if ( res_pos >= start )
                        ret_val = true;
                } else
                {
                    res_pos = read_pos_ + pos;
                    if (res_pos <= write_pos_)
                        ret_val = true;
                }
            }
            break;
        case SEEK_END:
            {
                if (pos <= 0 && static_cast<boost::uint64_t>(((-1)*pos)) < write_pos_)
                {
                    res_pos = write_pos_ + pos;
                    
                    boost::uint64_t start = write_pos_ > max_size_ ? write_pos_ - max_size_ : 0;
                    if ( res_pos >= start )
                        ret_val = true;
                }
            }
            break;
    }
    if (ret_val)
    {
        read_pos_ = res_pos;
        
        cur_pos = write_pos_ > max_size_ ? read_pos_ - (write_pos_ - max_size_) : read_pos_;
    }
    
    return ret_val;
}

} // engine
} // dvblink
