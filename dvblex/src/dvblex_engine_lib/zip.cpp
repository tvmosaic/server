/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/filesystem.hpp>
#include <boost/crc.hpp>
#include <cassert>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <cstring>
#include <string>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_zip.h>

namespace dvblink { namespace zip {

using namespace engine;
using namespace logging;

const boost::uint32_t max_filename_length_bytes = 2048;

template<class T>
void Write_Primitive(std::ostream& stream, const T& x)
{
    if (sizeof(x) == sizeof (boost::uint8_t))
    {
        stream.write(&(char&)x, sizeof(T));
    }
    else
    {
        char val = static_cast<char>(x);
        stream.write(&val, 1);
        val = static_cast<char>(x >> 8);
        stream.write(&val, 1);

        if (sizeof(x) == sizeof (boost::uint32_t))
        {
            val = static_cast<char>(x >> 16);
            stream.write(&val, 1);
            val = static_cast<char>(x >> 24);
            stream.write(&val, 1);
        }
    }
}

template<class T>
void Read_Primitive(std::istream& stream,T& x)
{
    unsigned char buf[4];
    
    if (sizeof(x) == sizeof (boost::uint16_t))
    {
        //read 2 bytes value
        stream.read((char*)&buf[0], 1);
        stream.read((char*)&buf[1], 1);
        x = ((boost::uint32_t)buf[1] << 8) | buf[0];
    }
    else
    if (sizeof(x) == sizeof (boost::uint32_t))
    {
        //read 4 bytes value
        stream.read((char*)&buf[0], 1);
        stream.read((char*)&buf[1], 1);
        stream.read((char*)&buf[2], 1);
        stream.read((char*)&buf[3], 1);
        x = ((boost::uint32_t)buf[3] << 24) | ((boost::uint32_t)buf[2] << 16) | ((boost::uint32_t)buf[1] << 8) | buf[0];
    }
    else
    {
        stream.read(&(char&)x,sizeof(T));
    }
}

const boost::uint16_t compression_type_decompress = 8;
const boost::uint16_t compression_type_no_compression = 0;

///////////////////////////////////////////////////////////////////////////////

zipfile_header::zipfile_header()
{
    fill_defaults();
}

zipfile_header::zipfile_header(const std::string& filename_input) :
    version(20), flags(0), compression_type(8), stamp_date(0), stamp_time(0), crc(0),
    compressed_size(0), uncompressed_size(0), filename(filename_input), header_offset(0)
{}

void zipfile_header::fill_defaults()
{
    version = 20;
    flags = 0;
    compression_type = 8;
    stamp_date = 0;
    stamp_time = 0;
    crc = 0;
    compressed_size = 0;
    uncompressed_size = 0;
    header_offset = 0;
}

bool zipfile_header::is_directory()
{
    return (compressed_size == 0 && uncompressed_size == 0 && compression_type == 0) || boost::ends_with(filename, "\\") || boost::ends_with(filename, "/");
}

bool zipfile_header::is_zip_compressed()
{
    return compression_type == compression_type_decompress;
}

bool zipfile_header::is_not_compressed()
{
    return compression_type == compression_type_no_compression;
}

bool zipfile_header::read(std::istream& istream, bool global)
{
    try {
        boost:: uint32_t sig;
        boost:: uint16_t version, flags;

        // read and check for local/global magic
        if (global)
        {
            Read_Primitive(istream, sig);
            if(sig != central_file_header_signature_)
            {
                log_error(L"zipfile_header::read. File does not have global header signature (%1%)") % string_cast<EC_UTF8>(filename);
                return false;
            }
            Read_Primitive(istream, version);
        }
        else
        {
            Read_Primitive(istream, sig);
            if (sig != local_file_header_signature_)
            {
                log_error(L"zipfile_header::read. File does not have local header signature (%1%)") % string_cast<EC_UTF8>(filename);
                return false;
            }
        }
        // Read rest of header
        Read_Primitive(istream, version);
        Read_Primitive(istream, flags);
        Read_Primitive(istream, compression_type);
        Read_Primitive(istream, stamp_date);
        Read_Primitive(istream, stamp_time);
        Read_Primitive(istream, crc);
        Read_Primitive(istream, compressed_size);
        Read_Primitive(istream, uncompressed_size);
        boost::uint16_t filename_length,extra_length;
        Read_Primitive(istream, filename_length);
        Read_Primitive(istream, extra_length);
        boost::uint16_t comment_length = 0;

        if (global)
        {
            Read_Primitive(istream, comment_length); // file comment
            boost::uint16_t disk_number_start, int_file_attrib;
            boost::uint32_t ext_file_attrib;
            Read_Primitive(istream, disk_number_start); // disk# start
            Read_Primitive(istream, int_file_attrib); // internal file
            Read_Primitive(istream, ext_file_attrib); // ext final
            Read_Primitive(istream, header_offset); // rel offset
        }

        if (filename_length > max_filename_length_bytes)
            return false;

        filename.resize(filename_length);
        istream.read(&filename[0], filename_length);
        istream.ignore(extra_length);

        if(global)
            istream.ignore(comment_length);

        return true;
    }
    catch(std::exception e)
    {
        log_error(L"zipfile_header::read. Exception %1%") % string_cast<EC_UTF8>(e.what());
        return false;
    }
}

void zipfile_header::write(std::ostream& ostream, const bool global) const
{
    if (global)
    {
        Write_Primitive(ostream,(unsigned int)central_file_header_signature_); // central file header signature
        Write_Primitive(ostream,(unsigned short)00);
    }
    else
    {
        Write_Primitive(ostream,(unsigned int)local_file_header_signature_); // local file header signature
    }

    // put filename only in the archive without path
    //
    boost::filesystem::path p = filename;
    std::string file_name = p.filename().string();

    Write_Primitive(ostream, version);
    Write_Primitive(ostream, flags);
    Write_Primitive(ostream, compression_type);
    Write_Primitive(ostream, stamp_date);
    Write_Primitive(ostream, stamp_time);
    Write_Primitive(ostream, crc);
    Write_Primitive(ostream, compressed_size);
    Write_Primitive(ostream, uncompressed_size);
    Write_Primitive(ostream, (unsigned short)file_name.length());
    Write_Primitive(ostream, (unsigned short)0); // extra length

    if (global)
    {
        Write_Primitive(ostream, (unsigned short)0); // file comment
        Write_Primitive(ostream, (unsigned short)0); // disk# start
        Write_Primitive(ostream, (unsigned short)0); // internal file
        Write_Primitive(ostream, (unsigned int)0);   // ext final
        Write_Primitive(ostream, (unsigned int)header_offset);
    } // rel offset

    for (unsigned int i = 0; i < file_name.length(); i++)
    {
        Write_Primitive(ostream, file_name.c_str()[i]);
    }
}

///////////////////////////////////////////////////////////////////////////////

zipfile_reader::zipfile_reader(const dvblink::filesystem_path_t& filename) :
    zip_file_(filename)
{
}

zipfile_reader::~zipfile_reader()
{
    reset();
}

void zipfile_reader::reset()
{
    filename_to_header_map_t::iterator i = filename_to_header_.begin();
    for ( ; i != filename_to_header_.end(); ++i)
    {
        delete i->second;
    }
    filename_to_header_.clear();

    istream.close();
    istream.clear();
}

bool zipfile_reader::is_valid()
{
    reset();
    bool ret_val = load();
    reset();

    return ret_val;
}

bool zipfile_reader::load()
{
    try {
        istream.open(zip_file_.to_string().c_str(), std::ios::in | std::ios::binary);

        if (!istream)
        {
            log_error(L"zipfile_reader_t::load. Unable to open zip file (%1%)") % zip_file_.to_wstring();
            return false;
        }
        return find_and_read_central_header();
    }
    catch(std::exception e)
    {
        log_error(L"zipfile_reader_t::load. Exception %1%") % string_cast<EC_UTF8>(e.what());
        return false;
    }
}

bool zipfile_reader::find_and_read_central_header()
{
    // Find the header
    // NOTE: this assumes the zip file header is the last thing written to file...
    istream.seekg(0, std::ios_base::end);
    std::ios::streampos end_position = istream.tellg();

    boost::uint32_t max_comment_size = 0xffff; // max size of header
    boost::uint32_t read_size_before_comment = 22;
    std::ios::streamoff read_start = max_comment_size + read_size_before_comment;

    if(read_start > end_position)
    {
        read_start = end_position;
    }

    istream.seekg(end_position - read_start);
    if (read_start <= 0)
    {
        std::cerr<<"ZIP: Invalid read buffer size"<<std::endl;
        return false;
    }

    char *buf = new char[read_start];

    istream.read(buf, read_start);
    boost::int32_t found = -1;
    for(boost::int32_t i = 0; i < read_start - 3; i++)
    {
        if (buf[i] == 0x50 && buf[i+1] == 0x4b && buf[i+2] == 0x05 && buf[i+3] == 0x06)
        {
            found = i;
            break;
        }
    }
    delete [] buf;

    if (found == -1)
    {
        std::cerr<<"ZIP: Failed to find zip header"<<std::endl;
        return false;
    }
    // seek to end of central header and read
    istream.seekg(end_position - (read_start - found));
    boost::uint32_t word;
    boost::uint16_t disk_number1, disk_number2, num_files, num_files_this_disk;
    Read_Primitive(istream, word); // end of central
    Read_Primitive(istream, disk_number1); // this disk number
    Read_Primitive(istream, disk_number2); // this disk number
    if (disk_number1 != disk_number2 || disk_number1 != 0)
    {
        std::cerr << "ZIP: multiple disk zip files are not supported" << std::endl;
        return false;
    }
    Read_Primitive(istream, num_files); // one entry in center in this disk
    Read_Primitive(istream, num_files_this_disk); // one entry in center 
    if (num_files != num_files_this_disk)
    {
        std::cerr << "ZIP: multi disk zip files are not supported" << std::endl;
        return false;
    }

    boost::uint32_t size_of_header, header_offset;
    Read_Primitive(istream, size_of_header); // size of header
    Read_Primitive(istream, header_offset); // offset to header
    
    // go to header and read all file headers
    istream.seekg(header_offset);
    for(int i = 0; i < num_files; i++)
    {
        zipfile_header* header = new zipfile_header();
        if (header->read(istream,true))
        {
            filename_to_header_[header->filename] = header;
        }
        else
        {
            delete header;
        }
    }
    return true;
}

//
// destination directory MUST exist!!!
// 
bool zipfile_reader::decompress(const dvblink::filesystem_path_t& destination)
{
    if (!load())
    {
        return false;
    }

    bool ret_val = true;
    //create all destination directories first
    filename_to_header_map_t::iterator it = filename_to_header_.begin();
    while (it != filename_to_header_.end())
    {
        if (it->second->is_directory())
        {
            dvblink::filesystem_path_t dest_dir = destination / it->second->filename;
            try {
                boost::filesystem::create_directories(dest_dir.to_boost_filesystem());
            }catch (...){}
        }
        ++it;
    }

    //create all destination directories first
    it = filename_to_header_.begin();
    while (it != filename_to_header_.end() && ret_val)
    {
        if (!it->second->is_directory())
        {
            dvblink::filesystem_path_t dest_file = destination / it->second->filename;

            //normalize file path
            boost::filesystem::path p = dest_file.to_boost_filesystem();
            p = p.normalize();
            p.make_preferred();

            dest_file = p;

            //make sure that directory itself exists
            boost::filesystem::path file_dest_dir = dest_file.to_boost_filesystem();
            try {
                boost::filesystem::create_directories(file_dest_dir.parent_path());
            } catch(...){}

            if (!boost::filesystem::exists(file_dest_dir.parent_path()))
            {
                ret_val = false;
                break;
            }

            try {
                istream.seekg(it->second->header_offset);
            } catch(...)
            {
                ret_val = false;
                break;
            }

            zipfile_header hdr;
            if (hdr.read(istream, false))
            {
                if (hdr.is_zip_compressed() || hdr.is_not_compressed())
                {
                    boost::iostreams::filtering_ostream os;

                    boost::iostreams::zlib_params zp;
                    zp.noheader = true;
                    if (hdr.is_zip_compressed())
                    {
                        os.push(boost::iostreams::zlib_decompressor(zp));
                    }
                    //if (hdr.is_zip_compressed())
                    //{
                    //    os.push(boost::iostreams::zlib_decompressor());
                    //}

                    os.push(boost::iostreams::file_sink(dest_file.to_string(), BOOST_IOS::binary));

                    std::streamsize size = it->second->compressed_size;
                    const int buf_size = 512;
                    char buf[buf_size];
                    while (size > 0)
                    {
                        try {
                        	std::streamsize to_read = size < buf_size ? size : buf_size; // min

                            istream.read(buf, to_read);
                            std::streamsize read_bytes = istream.gcount();
                            if (read_bytes > 0)
                            {
                                std::streamsize n = boost::iostreams::write(os, buf, read_bytes);
                                if (n != read_bytes)
                                {
                                    ret_val = false;
                                    break;
                                }
                                size -= read_bytes;
                            } else
                            {
                                ret_val = false;
                                break;
                            }
                        }
                        catch (boost::iostreams::zlib_error& e)
                        {
                            std::cout << e.what() << std::endl;
                        }
                        catch(std::exception e)
                        {
                            log_error(L"zipfile_reader_t::decompress. Exception %1%") % string_cast<EC_UTF8>(e.what());
                            ret_val = false;
                        }
                    }
                } else
                {
                    log_warning(L"zipfile_reader_t::decompress. Skipped item with unsupported compression type %1% (%2%)") % string_cast<EC_UTF8>(hdr.filename) % hdr.compression_type;
                }
            } else
            {
                ret_val = false;
                break;
            }
        }
        ++it;
    }
    return ret_val;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////

zip_file_writer::zip_file_writer(const dvblink::filesystem_path_t& file_name) :
    flushed_(false)
{
    ostream_.open(file_name.to_string().c_str(), std::ios::out | std::ios::binary);
    
    if (!ostream_)
    {
        throw std::runtime_error("ZIP: Invalid file handle");
    }
}

void zip_file_writer::flush()
{
    // Write all file headers
    std::ios::streampos final_position = ostream_.tellp();
    for (unsigned int i = 0; i < files_.size(); i++)
    {
        files_[i]->write(ostream_, true);
        delete files_[i];
    }

    std::ios::streampos central_end = ostream_.tellp();

    // Write end of central
    Write_Primitive(ostream_, (unsigned int)end_of_central_signature_); // end of central
    Write_Primitive(ostream_, (unsigned short)0);                       // this disk number
    Write_Primitive(ostream_, (unsigned short)0);                       // this disk number
    Write_Primitive(ostream_, (unsigned short)files_.size());           // one entry in center in this disk
    Write_Primitive(ostream_, (unsigned short)files_.size());           // one entry in center 
    Write_Primitive(ostream_, (unsigned int)(central_end - final_position)); // size of header
    Write_Primitive(ostream_, (unsigned int)final_position);            // offset to header
    Write_Primitive(ostream_, (unsigned short)0);                       // zip comment

    ostream_.close();
    flushed_ = true;
}

zip_file_writer::~zip_file_writer()
{
    if (!flushed_)
    {
        flush();
    }
}

//
//  file name should be without path to it!!!
// 
bool zip_file_writer::add_file(const dvblink::filesystem_path_t& file_name)
{
    try
    {
        zipfile_header* hdr = new zipfile_header(file_name.to_string());

        std::stringbuf buf(std::ios_base::in | std::ios_base::out | std::ios_base::binary);
        boost::iostreams::file_source infile(file_name.to_string(), std::ios_base::in | std::ios_base::binary);
        boost::iostreams::filtering_istream fis;

        hdr->uncompressed_size = infile.seek(0, std::ios_base::end);
        infile.seek(0, std::ios_base::beg);

        boost::crc_32_type crc32;
        const long buffer_size = 1024;
        char buffer[buffer_size];
        long num = 0;
        do
        {
            num = infile.read(buffer, buffer_size);
            crc32.process_bytes(buffer, num);
        } while (num == buffer_size);
        hdr->crc = crc32.checksum();

        infile.seek(0, std::ios_base::beg);

        boost::iostreams::zlib_params zp;
        zp.noheader = true;
        fis.push(boost::iostreams::zlib_compressor(zp));

        fis.push(infile);

        hdr->compressed_size = boost::iostreams::copy(fis, buf);
        hdr->header_offset = ostream_.tellp();

        files_.push_back(hdr);
        hdr->write(ostream_, false);

        ostream_ << buf.str();
    }
    catch (boost::iostreams::zlib_error& err)
    {
        log_error(L"zip_file_writer::add_file(%1%) - error: %2%") % file_name.to_wstring() % string_cast<EC_UTF8>(err.what());
        return false;
    }
    catch (...)
    {
        log_error(L"zip_file_writer::add_file(%1%) - unknown error") % file_name.to_wstring();
        return false;
    }
    return true;
}


} // namespace unzip
} // namespace dvblink
