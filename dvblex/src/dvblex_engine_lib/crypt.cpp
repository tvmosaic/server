/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include "dl_crypt.h"
#include <cstring>
#include <cstdlib>

using namespace std;

namespace dvblink {
namespace engine {

#define F(x, y, z) ((z) ^ ((x) & ((y) ^ (z))))
#define G(x, y, z) ((x) ^ (y) ^ (z))
#define H(x, y, z) (((x) & (y)) | ((z) & ((x) | (y))))
#define I(x, y, z) ((x) ^ (y) ^ (z))
#define ROTATE_LEFT(x, n) (((x) << (n)) | ((x) >> (32-(n))))
#define W(i) ( tmp=x[(i-3)&15]^x[(i-8)&15]^x[(i-14)&15]^x[i&15], (x[i&15]=ROTATE_LEFT(tmp, 1)) )  

#define FF(a, b, c, d, e, w) { \
    (e) += F ((b), (c), (d)) + (w) + (unsigned int)(0x5A827999); \
    (e) += ROTATE_LEFT ((a), 5); \
    (b) = ROTATE_LEFT((b), 30); \
  }
#define GG(a, b, c, d, e, w) { \
    (e) += G ((b), (c), (d)) + (w) + (unsigned int)(0x6ED9EBA1); \
    (e) += ROTATE_LEFT ((a), 5); \
    (b) = ROTATE_LEFT((b), 30); \
  }
#define HH(a, b, c, d, e, w) { \
    (e) += H ((b), (c), (d)) + (w) + (unsigned int)(0x8F1BBCDC); \
    (e) += ROTATE_LEFT ((a), 5); \
    (b) = ROTATE_LEFT((b), 30); \
  }
#define II(a, b, c, d, e, w) { \
    (e) += I ((b), (c), (d)) + (w) + (unsigned int)(0xCA62C1D6); \
    (e) += ROTATE_LEFT ((a), 5); \
    (b) = ROTATE_LEFT((b), 30); \
  }

#ifndef _WIN32
void _ultoa(unsigned long val, char *buf, unsigned radix)
{
    char* p;            // pointer to traverse string
    char* firstdig;     // pointer to first digit
    char temp;          // temp char
    unsigned digval;    // value of digit

    p = buf;
    firstdig = p;   // save pointer to first digit

    do
    {
        digval = (unsigned)(val % radix);
        val /= radix;   // get next digit

        // convert to ascii and store
        if (digval > 9)
        {
            *p++ = (char) (digval - 10 + 'a');  // a letter
        }
        else
        {
            *p++ = (char) (digval + '0');   // a digit
        }
    }
    while (val > 0);

    // We now have the digit of the number in the buffer, but in reverse
    // order. Thus we reverse them now.

    *p-- = '\0';    // terminate string; p points to last digit

    do
    {
        temp = *p;
        *p = *firstdig;
        *firstdig = temp;   // swap *p and *firstdig
        --p;
        ++firstdig; // advance to next two digits
    }
    while (firstdig < p);   // repeat until halfway
}
#endif

char* str_reverse(char* str)
{
	char *e, *n, *p;
	int str_len = strlen(str);
	
	n = new char[str_len+1];
	p = n;
	
	e = str + str_len;
	
	while(--e >= str)
    {
		*p++ = *e;
	}
	
	*p = '\0';
	
	return n;
}

string str_reverse(string src)
{
    return string(src.rbegin(), src.rend());
}

string dec2hex(int dec)
{
    char hexDigits[] = {"0123456789abcdef"};
    unsigned char a = 0;
    string hex;

    while(dec != 0)
    {
        a = dec & 0xF;
        hex.insert(hex.begin(), hexDigits[a]);
        dec >>= 4;
    }

    return hex;
}

static void sha_encode32(unsigned char *output, unsigned int *input, unsigned int len)
{
	unsigned int i, j;

	for (i = 0, j = 0; j < len; i++, j += 4)
	{
		output[j] = (unsigned char) ((input[i] >> 24) & 0xff);
		output[j + 1] = (unsigned char) ((input[i] >> 16) & 0xff);
		output[j + 2] = (unsigned char) ((input[i] >> 8) & 0xff);
		output[j + 3] = (unsigned char) (input[i] & 0xff);
	}
}
static void sha_decode32(unsigned int *output, const unsigned char *input, unsigned int len)
{
	unsigned int i, j;

	for (i = 0, j = 0; j < len; i++, j += 4)
		output[i] = ((unsigned int) input[j + 3]) | (((unsigned int) input[j + 2]) << 8) | (((unsigned int) input[j + 1]) << 16) | (((unsigned int) input[j]) << 24);
}

void sha1_init(SHA1_CTX * context)
{
	context->count[0] = context->count[1] = 0;
	context->state[0] = 0x67452301;
	context->state[1] = 0xefcdab89;
	context->state[2] = 0x98badcfe;
	context->state[3] = 0x10325476;
	context->state[4] = 0xc3d2e1f0;
}

static void sha1_transform(unsigned int state[5], const unsigned char block[64])
{
	unsigned int a = state[0], b = state[1], c = state[2];
	unsigned int d = state[3], e = state[4], x[16], tmp;

	sha_decode32(x, block, 64);

	/* Round 1 */
	FF(a, b, c, d, e, x[0]);   /* 1 */
	FF(e, a, b, c, d, x[1]);   /* 2 */
	FF(d, e, a, b, c, x[2]);   /* 3 */
	FF(c, d, e, a, b, x[3]);   /* 4 */
	FF(b, c, d, e, a, x[4]);   /* 5 */
	FF(a, b, c, d, e, x[5]);   /* 6 */
	FF(e, a, b, c, d, x[6]);   /* 7 */
	FF(d, e, a, b, c, x[7]);   /* 8 */
	FF(c, d, e, a, b, x[8]);   /* 9 */
	FF(b, c, d, e, a, x[9]);   /* 10 */
	FF(a, b, c, d, e, x[10]);  /* 11 */
	FF(e, a, b, c, d, x[11]);  /* 12 */
	FF(d, e, a, b, c, x[12]);  /* 13 */
	FF(c, d, e, a, b, x[13]);  /* 14 */
	FF(b, c, d, e, a, x[14]);  /* 15 */
	FF(a, b, c, d, e, x[15]);  /* 16 */
	FF(e, a, b, c, d, W(16));  /* 17 */
	FF(d, e, a, b, c, W(17));  /* 18 */
	FF(c, d, e, a, b, W(18));  /* 19 */
	FF(b, c, d, e, a, W(19));  /* 20 */

	/* Round 2 */
	GG(a, b, c, d, e, W(20));  /* 21 */
	GG(e, a, b, c, d, W(21));  /* 22 */
	GG(d, e, a, b, c, W(22));  /* 23 */
	GG(c, d, e, a, b, W(23));  /* 24 */
	GG(b, c, d, e, a, W(24));  /* 25 */
	GG(a, b, c, d, e, W(25));  /* 26 */
	GG(e, a, b, c, d, W(26));  /* 27 */
	GG(d, e, a, b, c, W(27));  /* 28 */
	GG(c, d, e, a, b, W(28));  /* 29 */
	GG(b, c, d, e, a, W(29));  /* 30 */
	GG(a, b, c, d, e, W(30));  /* 31 */
	GG(e, a, b, c, d, W(31));  /* 32 */
	GG(d, e, a, b, c, W(32));  /* 33 */
	GG(c, d, e, a, b, W(33));  /* 34 */
	GG(b, c, d, e, a, W(34));  /* 35 */
	GG(a, b, c, d, e, W(35));  /* 36 */
	GG(e, a, b, c, d, W(36));  /* 37 */
	GG(d, e, a, b, c, W(37));  /* 38 */
	GG(c, d, e, a, b, W(38));  /* 39 */
	GG(b, c, d, e, a, W(39));  /* 40 */

	/* Round 3 */
	HH(a, b, c, d, e, W(40));  /* 41 */
	HH(e, a, b, c, d, W(41));  /* 42 */
	HH(d, e, a, b, c, W(42));  /* 43 */
	HH(c, d, e, a, b, W(43));  /* 44 */
	HH(b, c, d, e, a, W(44));  /* 45 */
	HH(a, b, c, d, e, W(45));  /* 46 */
	HH(e, a, b, c, d, W(46));  /* 47 */
	HH(d, e, a, b, c, W(47));  /* 48 */
	HH(c, d, e, a, b, W(48));  /* 49 */
	HH(b, c, d, e, a, W(49));  /* 50 */
	HH(a, b, c, d, e, W(50));  /* 51 */
	HH(e, a, b, c, d, W(51));  /* 52 */
	HH(d, e, a, b, c, W(52));  /* 53 */
	HH(c, d, e, a, b, W(53));  /* 54 */
	HH(b, c, d, e, a, W(54));  /* 55 */
	HH(a, b, c, d, e, W(55));  /* 56 */
	HH(e, a, b, c, d, W(56));  /* 57 */
	HH(d, e, a, b, c, W(57));  /* 58 */
	HH(c, d, e, a, b, W(58));  /* 59 */
	HH(b, c, d, e, a, W(59));  /* 60 */

	/* Round 4 */
	II(a, b, c, d, e, W(60));  /* 61 */
	II(e, a, b, c, d, W(61));  /* 62 */
	II(d, e, a, b, c, W(62));  /* 63 */
	II(c, d, e, a, b, W(63));  /* 64 */
	II(b, c, d, e, a, W(64));  /* 65 */
	II(a, b, c, d, e, W(65));  /* 66 */
	II(e, a, b, c, d, W(66));  /* 67 */
	II(d, e, a, b, c, W(67));  /* 68 */
	II(c, d, e, a, b, W(68));  /* 69 */
	II(b, c, d, e, a, W(69));  /* 70 */
	II(a, b, c, d, e, W(70));  /* 71 */
	II(e, a, b, c, d, W(71));  /* 72 */
	II(d, e, a, b, c, W(72));  /* 73 */
	II(c, d, e, a, b, W(73));  /* 74 */
	II(b, c, d, e, a, W(74));  /* 75 */
	II(a, b, c, d, e, W(75));  /* 76 */
	II(e, a, b, c, d, W(76));  /* 77 */
	II(d, e, a, b, c, W(77));  /* 78 */
	II(c, d, e, a, b, W(78));  /* 79 */
	II(b, c, d, e, a, W(79));  /* 80 */

	state[0] += a;
	state[1] += b;
	state[2] += c;
	state[3] += d;
	state[4] += e;

	memset((unsigned char*) x, 0, sizeof(x));
}

void sha1_update(SHA1_CTX * context, const unsigned char *input, unsigned int input_len)
{
	unsigned int i, index, part_len;

	index = (unsigned int) ((context->count[0] >> 3) & 0x3F);

	if((context->count[0] += ((unsigned int) input_len << 3)) < ((unsigned int) input_len << 3)) context->count[1]++;

	context->count[1] += ((unsigned int) input_len >> 29);

	part_len = 64 - index;

	if (input_len >= part_len)
	{
		memcpy((unsigned char*) & context->buffer[index], (unsigned char*) input, part_len);
		sha1_transform(context->state, context->buffer);

		for(i = part_len; i + 63 < input_len; i += 64)
            sha1_transform(context->state, &input[i]);

		index = 0;
	}
	else
	{
		i = 0;
	}

	memcpy((unsigned char*) & context->buffer[index], (unsigned char*) & input[i], input_len - i);
}

void sha1_final(unsigned char digest[20], SHA1_CTX * context)
{
	unsigned char bits[8];
	unsigned int index, pad_len;

	bits[7] = context->count[0] & 0xFF;
	bits[6] = (context->count[0] >> 8) & 0xFF;
	bits[5] = (context->count[0] >> 16) & 0xFF;
	bits[4] = (context->count[0] >> 24) & 0xFF;
	bits[3] = context->count[1] & 0xFF;
	bits[2] = (context->count[1] >> 8) & 0xFF;
	bits[1] = (context->count[1] >> 16) & 0xFF;
	bits[0] = (context->count[1] >> 24) & 0xFF;
	
	index = (unsigned int) ((context->count[0] >> 3) & 0x3f);
	pad_len = (index < 56) ? (56 - index) : (120 - index);
	sha1_update(context, PADDING, pad_len);
	sha1_update(context, bits, 8);
	sha_encode32(digest, context->state, 20);

	memset((unsigned char*) context, 0, sizeof(*context));
}

static inline void bin2hex(char *out, const unsigned char *in, int in_len)
{
	static const char hexits[17] = "0123456789abcdef";
	int i;

	for(i = 0; i < in_len; i++)
	{
		out[i * 2]       = hexits[in[i] >> 4];
		out[(i * 2) + 1] = hexits[in[i] &  0x0F];
	}
}

void make_sha1_digest(char *sha1str, unsigned char *digest)
{
	bin2hex(sha1str, digest, 20);
	sha1str[40] = '\0';
}

string sha1(const char *key)
{
    char sha1str[41];
    SHA1_CTX context;
	unsigned char digest[20];

    sha1str[0] = '\0';
    sha1_init(&context);
    sha1_update(&context, (const unsigned char*)key, strlen(key));
    sha1_final(digest, &context);

    make_sha1_digest(sha1str, digest);

    return sha1str;
}

string encode_xml(string xml, string key)
{
    string hash;
    string sha1_key = sha1(key.c_str());
    unsigned int sha1_key_len = sha1_key.length();
	unsigned int xml_len = xml.length();

    unsigned long ldec = 0;
    unsigned int j = 0;
    unsigned char cx, ck;
    char buffer[3];

	for(unsigned int i = 0; i < xml_len; i++)
	{
		cx = (unsigned char)xml[i];
		
		if(j == sha1_key_len) j = 0;
        ck = (unsigned char)sha1_key[j];
		j++;

        ldec = strtoul(dec2hex(cx + ck).c_str(), NULL, 16);
        _ultoa(ldec, buffer, 36);

        hash += str_reverse(buffer);
	}

    return hash;
}

string decode_xml(string hash, string key)
{
    string xml, buffer;
    string sha1_key = sha1(key.c_str());
    unsigned int sha1_key_len = sha1_key.length();
	unsigned int hash_len = hash.length();

    unsigned int j = 0;
    unsigned char cx, ck;

    for(unsigned int i = 0; i < hash_len; i+=2)
    {
        buffer = str_reverse(hash.substr(i, 2));
        cx = (unsigned char)strtoul(buffer.c_str(), NULL, 36);

        if(j == sha1_key_len) j = 0;
        ck = (unsigned char)sha1_key[j];
		j++;

        xml += cx - ck;
    }

    return xml;
}

} // namespace engine
} // namespace dvblink
