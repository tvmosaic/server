/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <dl_strptime.h>

namespace dvblink { namespace engine {
/*
 * We do not implement alternate representations. However, we always
 * check whether a given modifier is allowed for a certain conversion.
 */
#define ALT_E          0x01
#define ALT_O          0x02
//#define LEGAL_ALT(x)       { if (alt_format & ~(x)) return (0); }
#define LEGAL_ALT(x)       { ; }
#define TM_YEAR_BASE   (1900)

const static char gmt[] = { "GMT" };
const static char cet[] = { "CET" };
const static char eet[] = { "EET" };
const static char utc[] = { "UTC" };

static int conv_num(const char **, int *, int, int);
static int strncasecmp(const char *s1, const char *s2, size_t n);
 
static const char *day[7] = {
     "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
     "Friday", "Saturday"
};

static const char *abday[7] = {
     "Sun","Mon","Tue","Wed","Thu","Fri","Sat"
};

static const char *mon[12] = {
     "January", "February", "March", "April", "May", "June", "July",
     "August", "September", "October", "November", "December"
};

static const char *abmon[12] = {
     "Jan", "Feb", "Mar", "Apr", "May", "Jun",
     "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

static const char * const am_pm[2] = {
     "AM", "PM"
};
 
//static const char* find_string(const char *bp, int *tgt, const char ** n1, const char ** n2, int c)
//{
//	int i;
//	size_t len;
//
//	/* check full name - then abbreviated ones */
//	for (; n1 != NULL; n1 = n2, n2 = NULL) {
//		for (i = 0; i < c; i++, n1++) {
//			len = strlen(*n1);
//			if (strncasecmp((char*)*n1, (char *)bp, len) == 0) {
//				*tgt = i;
//				return bp + len;
//			}
//		}
//	}
//
//	/* Nothing matched */
//	return NULL;
//}

char * int_strptime(const char *buf, const char *fmt, struct tm* res_tm)
{
    char c;
    const char *bp;
    size_t len = 0;
    int alt_format, i, neg, offs, split_year = 0;
	time_t temp_time;

    bp = buf;

    while ((c = *fmt) != '\0') 
    {
        /* Clear `alternate' modifier prior to new conversion. */
        alt_format = 0;
     
        /* Eat up white-space. */
        if (isspace(c)) 
        {
            while (isspace(*bp))
                bp++;
 
            fmt++;
            continue;
        }
                  
         if ((c = *fmt++) != '%')
              goto literal;
 
 
again:  switch (c = *fmt++) 
        {
            case '%': /* "%%" is converted to "%". */
                literal:
                    if (c != *bp++)
                        return (0);
                    break;
 
                /* 
                 * "Alternative" modifiers. Just set the appropriate flag
                  * and start over again.
                   */
            case 'E': /* "%E?" alternative conversion modifier. */
                LEGAL_ALT(0);
                alt_format |= ALT_E;
                goto again;
 
            case 'O': /* "%O?" alternative conversion modifier. */
                LEGAL_ALT(0);
                alt_format |= ALT_O;
                goto again;
             
            /*
             * "Complex" conversion rules, implemented through recursion.
             */
            case 'c': /* Date and time, using the locale's format. */
                LEGAL_ALT(ALT_E);
                if ((bp = int_strptime(bp, "%x %X", res_tm)) == NULL)
                    return (0);
                break;
 
            case 'D': /* The date as "%m/%d/%y". */
                LEGAL_ALT(0);
                if ((bp = int_strptime(bp, "%m/%d/%y", res_tm)) == NULL)
                    return (0);
                break;

			case 'F':	/* The date as "%Y-%m-%d". */
                LEGAL_ALT(0);
                if ((bp = int_strptime(bp, "%Y-%m-%d", res_tm)) == NULL)
                    return (0);
				break;

            case 'R': /* The time as "%H:%M". */
                LEGAL_ALT(0);
                if ((bp = int_strptime(bp, "%H:%M", res_tm)) == NULL)
                    return (0);
                break;
         
            case 'r': /* The time in 12-hour clock representation. */
                LEGAL_ALT(0);
                if ((bp = int_strptime(bp, "%I:%M:%S %p", res_tm)) == NULL)
                    return (0);
                break;
 
            case 'T': /* The time as "%H:%M:%S". */
                LEGAL_ALT(0);
                if ((bp = int_strptime(bp, "%H:%M:%S", res_tm)) == NULL)
                    return (0);
                break;
 
            case 'X': /* The time, using the locale's format. */
                LEGAL_ALT(ALT_E);
                if ((bp = int_strptime(bp, "%H:%M:%S", res_tm)) == NULL)
                    return (0);
                break;
 
            case 'x': /* The date, using the locale's format. */
                LEGAL_ALT(ALT_E);
                if ((bp = int_strptime(bp, "%m/%d/%y", res_tm)) == NULL)
                    return (0);
                break;
 
            /*
             * "Elementary" conversion rules.
             */
            case 'A': /* The day of week, using the locale's form. */
            case 'a':
                LEGAL_ALT(0);
                for (i = 0; i < 7; i++) 
                {
                    /* Full name. */
                    len = strlen(day[i]);
                    if (strncasecmp((char *)(day[i]), (char *)bp, len) == 0)
                        break;
         
                    /* Abbreviated name. */
                    len = strlen(abday[i]);
                    if (strncasecmp((char *)(abday[i]), (char *)bp, len) == 0)
                        break;
                }
 
                /* Nothing matched. */
                if (i == 7)
                    return (0);
 
                res_tm->tm_wday = i;
                bp += len;
                break;
 
            case 'B': /* The month, using the locale's form. */
            case 'b':
            case 'h':
                LEGAL_ALT(0);
                for (i = 0; i < 12; i++) 
                {
                     /* Full name. */

                    len = strlen(mon[i]);
                    if (strncasecmp((char *)(mon[i]), (char *)bp, len) == 0)
                        break;
 
                    /* Abbreviated name. */
                    len = strlen(abmon[i]);
                    if (strncasecmp((char *)(abmon[i]),(char *) bp, len) == 0)
                        break;
                }
 
                /* Nothing matched. */
                if (i == 12)
                    return (0);
 
                res_tm->tm_mon = i;
                bp += len;
                break;
 
            case 'C': /* The century number. */
                LEGAL_ALT(ALT_E);
                if (!(conv_num(&bp, &i, 0, 99)))
                    return (0);
 
                if (split_year)
                {
                    res_tm->tm_year = (res_tm->tm_year % 100) + (i * 100);
                }
                else
                {
                    res_tm->tm_year = i * 100;
                    split_year = 1;
                }
                break;
 
            case 'd': /* The day of month. */
            case 'e':
                LEGAL_ALT(ALT_O);
                if (!(conv_num(&bp, &res_tm->tm_mday, 1, 31)))
                    return (0);
                break;
 
            case 'k': /* The hour (24-hour clock representation). */
                LEGAL_ALT(0);
                /* FALLTHROUGH */
            case 'H':
                LEGAL_ALT(ALT_O);
                if (!(conv_num(&bp, &res_tm->tm_hour, 0, 23)))
                    return (0);
                break;
 
            case 'l': /* The hour (12-hour clock representation). */
                LEGAL_ALT(0);
                /* FALLTHROUGH */
            case 'I':
                LEGAL_ALT(ALT_O);
                if (!(conv_num(&bp, &res_tm->tm_hour, 1, 12)))
                    return (0);
                if (res_tm->tm_hour == 12)
                    res_tm->tm_hour = 0;
                break;
         
            case 'j': /* The day of year. */
                LEGAL_ALT(0);
                if (!(conv_num(&bp, &i, 1, 366)))
                    return (0);
                res_tm->tm_yday = i - 1;
                break;
         
            case 'M': /* The minute. */
                LEGAL_ALT(ALT_O);
                if (!(conv_num(&bp, &res_tm->tm_min, 0, 59)))
                    return (0);
                break;
         
            case 'm': /* The month. */
                LEGAL_ALT(ALT_O);
                if (!(conv_num(&bp, &i, 1, 12)))
                    return (0);
                res_tm->tm_mon = i - 1;
                break;
 
//            case 'p': /* The locale's equivalent of AM/PM. */
//                LEGAL_ALT(0);
//                /* AM? */
//                if (strcasecmp(am_pm[0], bp) == 0) 
//                {
//                    if (res_tm->tm_hour > 11)
//                        return (0);
//
//                    bp += strlen(am_pm[0]);
//                    break;
//                }
//                /* PM? */
//                else if (strcasecmp(am_pm[1], bp) == 0) 
//                {
//                    if (res_tm->tm_hour > 11)
//                        return (0);
//
//                    res_tm->tm_hour += 12;
//                    bp += strlen(am_pm[1]);
//                    break;
//                }
//
//                /* Nothing matched. */
//                return (0);
         
            case 'S': /* The seconds. */
                LEGAL_ALT(ALT_O);
                if (!(conv_num(&bp, &res_tm->tm_sec, 0, 61)))
                    return (0);
                break;
         
            case 'U': /* The week of year, beginning on sunday. */
            case 'W': /* The week of year, beginning on monday. */
                LEGAL_ALT(ALT_O);
                /*
                 * XXX This is bogus, as we can not assume any valid
                 * information present in the tm structure at this
                 * point to calculate a real value, so just check the
                 * range for now.
                 */
                if (!(conv_num(&bp, &i, 0, 53)))
                    return (0);
                break;
 
            case 'w': /* The day of week, beginning on sunday. */
                LEGAL_ALT(ALT_O);
                if (!(conv_num(&bp, &res_tm->tm_wday, 0, 6)))
                    return (0);
                break;
 
            case 'Y': /* The year. */
                LEGAL_ALT(ALT_E);
                if (!(conv_num(&bp, &i, 0, 9999)))
                    return (0);
 
                res_tm->tm_year = i - TM_YEAR_BASE;
                break;
         
            case 'y': /* The year within 100 years of the epoch. */
                LEGAL_ALT(ALT_E | ALT_O);
                if (!(conv_num(&bp, &i, 0, 99)))
                    return (0);
 
                if (split_year) 
                {
                    res_tm->tm_year = ((res_tm->tm_year / 100) * 100) + i;
                    break;
                }
                split_year = 1;
                if (i <= 68)
                    res_tm->tm_year = i + 2000 - TM_YEAR_BASE;
                else
                    res_tm->tm_year = i + 1900 - TM_YEAR_BASE;
                break;
 

		case 'Z':
			offs = -1;
			//GMT
			if (strncasecmp((const char *)bp, gmt, 3) == 0) 
			{
				offs = 0;
				bp += 3;
			}
			//CET
			if (strncasecmp((const char *)bp, cet, 3) == 0) 
			{
				offs = 60;
				bp += 3;
			}
			//EET
			if (strncasecmp((const char *)bp, eet, 3) == 0) 
			{
				offs = 120;
				bp += 3;
			}
			if (offs != -1)
			{
				temp_time = _mkgmtime(res_tm);
				temp_time -= offs*60;
				*res_tm = *gmtime(&temp_time);
			}
            else
			{
				return NULL;
			}
			break;

		case 'z':
			/*
			 * We recognize all ISO 8601 formats:
			 * Z	= Zulu time/UTC
			 * [+-]hhmm
			 * [+-]hh:mm
			 * [+-]hh
			 */
			while (isspace(*bp))
				bp++;

			switch (*bp++) {
			case 'Z':
				res_tm->tm_isdst = 0;
				continue;
			case '+':
				neg = 0;
				break;
			case '-':
				neg = 1;
				break;
			default:
				return NULL;
			}
			offs = 0;
			for (i = 0; i < 4; ) {
				if (isdigit(*bp)) {
					offs = offs * 10 + (*bp++ - '0');
					i++;
					continue;
				}
				if (i == 2 && *bp == ':') {
					bp++;
					continue;
				}
				break;
			}
			switch (i) {
			case 2:
				offs *= 3600;
				break;
			case 4:
				i = offs % 100;
				if (i >= 60)
					return NULL;
				/* Convert minutes into decimal */
				offs = (offs / 100) * 3600 + (i * 60);
				break;
			default:
				return NULL;
			}
			if (neg == 0)
				offs = -offs;
			temp_time = _mkgmtime(res_tm);
			temp_time += offs;
			*res_tm = *gmtime(&temp_time);
			res_tm->tm_isdst = 0;	/* XXX */
			break;

                /*
                  * Miscellaneous conversions.
                  */
            case 'n': /* Any kind of white-space. */
            case 't':
                LEGAL_ALT(0);
                while (isspace(*bp))
                    bp++;
                break;
 
 
            default: /* Unknown/unsupported conversion. */
                return (0);
        }
 
 
    }
 
    /* LINTED functional specification */
    return ((char *)bp);
}

char * dl_strptime(const char *buf, const char *fmt, struct tm* res_tm)
{
	memset(res_tm, 0, sizeof(*res_tm));
    return int_strptime(buf, fmt, res_tm);
}
 
static int conv_num(const char **buf, int *dest, int llim, int ulim)
{
    int result = 0;
 
    /* The limit also determines the number of valid digits. */
    int rulim = ulim;
 
    if (**buf < '0' || **buf > '9')
        return (0);
 
    do {
        result *= 10;
        result += *(*buf)++ - '0';
        rulim /= 10;
    } while ((result * 10 <= ulim) && rulim && **buf >= '0' && **buf <= '9');
 
    if (result < llim || result > ulim)
        return (0);
 
    *dest = result;
    return (1);
}
 
int strncasecmp(const char *s1, const char *s2, size_t n)
{
    if (n == 0)
        return 0;
 
    while (n-- != 0 && tolower(*s1) == tolower(*s2))
    {
        if (n == 0 || *s1 == '\0' || *s2 == '\0')
            break;
        s1++;
        s2++;
    }
 
    return tolower(*(unsigned char *) s1) - tolower(*(unsigned char *) s2);
}

time_t GetDateTimeFromString(const char* dt_str)
{
	time_t ret_val = -1;
	struct tm dt_tm;
	char* conv_res = NULL;
	//try one by one a number of formats
	//YYYYMMDDHHMMSS +-z
	conv_res = dl_strptime(dt_str, "%Y%m%d%H%M%S%z", &dt_tm);
	if (conv_res == NULL)
	{
		//YYYYMMDDTHHMMSS +-z
		conv_res = dl_strptime(dt_str, "%Y%m%dT%H%M%S%z", &dt_tm);
	}
	if (conv_res == NULL)
	{
		//YYYYMMDDHHMMSS Z
		conv_res = dl_strptime(dt_str, "%Y%m%d%H%M%S %Z", &dt_tm);
	}
	if (conv_res == NULL)
	{
		//YYYY-MM-DDTHH:MM:SSZ
		conv_res = dl_strptime(dt_str, "%Y-%m-%dT%H:%M:%SZ", &dt_tm);
	}
	if (conv_res == NULL)
	{
		//YYYY-MM-DDTHH:MM:SS +-z
		conv_res = dl_strptime(dt_str, "%FT%T%z", &dt_tm);
	}
	if (conv_res == NULL)
	{
		//YYYY-MM-DD HH:MM:SS +-z
		conv_res = dl_strptime(dt_str, "%F %T%z", &dt_tm);
	}
	if (conv_res == NULL)
	{
		//YYYYMMDDHHMMSS
		conv_res = dl_strptime(dt_str, "%Y%m%d%H%M%S", &dt_tm);
	}
	if (conv_res == NULL)
	{
		//YYYYMMDDTHHMMSS +-z
		conv_res = dl_strptime(dt_str, "%Y%m%dT%H%M%S", &dt_tm);
	}
	if (conv_res == NULL)
	{
		//YYYY-MM-DDTHH:MM:SS
		conv_res = dl_strptime(dt_str, "%FT%T", &dt_tm);
	}
	if (conv_res == NULL)
	{
		//YYYY-MM-DD HH:MM:SS
		conv_res = dl_strptime(dt_str, "%F %T", &dt_tm);
	}
	if (conv_res == NULL)
	{
		//DD month YYYY HH:MM
		conv_res = dl_strptime(dt_str, "%d %B %Y %H:%M", &dt_tm);
	}
	if (conv_res != NULL)
	{
		ret_val = _mkgmtime(&dt_tm);
	}
	return ret_val;
}

//
// [YYYY]-[MM]-[DD]T[hh]:[mm]:[ss]Z 
//
void get_iso8601_time_string(const boost::posix_time::ptime& time, std::string& time_string)
{
    boost::gregorian::date d = time.date();
    boost::posix_time::time_duration t = time.time_of_day();

    char time_buf[64] = { 0 };
    sprintf(time_buf, "%04d-%02d-%02dT%02d:%02d:%02dZ", (int)d.year(), (int)d.month(), (int)d.day(), t.hours(), t.minutes(), t.seconds());
    time_string = time_buf;
}

//
// YYYYMMDDhhmmss
//
void get_iso8601_plain_time_string(const boost::posix_time::ptime& time, std::string& time_string)
{
    boost::gregorian::date d = time.date();
    boost::posix_time::time_duration t = time.time_of_day();

    char time_buf[64] = { 0 };
    sprintf(time_buf, "%04d%02d%02d%02d%02d%02d", (int)d.year(), (int)d.month(), (int)d.day(), t.hours(), t.minutes(), t.seconds());
    time_string = time_buf;
}

}
}

#ifndef _WIN32
//-----------------------------------------------------------------------------
// _mkgmtime (Linux implementation)
//-----------------------------------------------------------------------------

#ifndef NO_TIME_T_MAX
#ifndef TM_YEAR_MAX
#define TM_YEAR_MAX 2106
#endif
#ifndef TM_MON_MAX
#define TM_MON_MAX  1   // February
#endif
#ifndef TM_MDAY_MAX
#define TM_MDAY_MAX 7
#endif
#ifndef TM_HOUR_MAX
#define TM_HOUR_MAX 6
#endif
#ifndef TM_MIN_MAX
#define TM_MIN_MAX  28
#endif
#ifndef TM_SEC_MAX
#define TM_SEC_MAX  14
#endif
#endif // NO_TIME_T_MAX

// Adjusts out-of-range values for `tm' field `tm_member'
#define ADJUST_TM(tm_member, tm_carry, modulus) \
    if ((tm_member) < 0) { \
    tm_carry -= (1 - ((tm_member)+1) / (modulus)); \
    tm_member = (modulus-1) + (((tm_member)+1) % (modulus)); \
    } else if ((tm_member) >= (modulus)) { \
    tm_carry += (tm_member) / (modulus); \
    tm_member = (tm_member) % (modulus); \
    }

// Nonzero if `y' is a leap year, else zero
#define leap(y) (((y) % 4 == 0 && (y) % 100 != 0) || (y) % 400 == 0)

// Number of leap years from 1970 to `y' (not including `y' itself)
#define nleap(y) (((y) - 1969) / 4 - ((y) - 1901) / 100 + ((y) - 1601) / 400)

// Additional leapday in February of leap years
#define leapday(m, y) ((m) == 1 && leap (y))

// Length of month `m' (0 .. 11)
#define monthlen(m, y) (ydays[(m)+1] - ydays[m] + leapday (m, y))

// Accumulated number of days from 01-Jan up to start of current month
static const short ydays[] =
{
    0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365
};

// Return the equivalent in seconds past 12:00:00 a.m. Jan 1, 1970 GMT
// of the Greenwich Mean time and date in the exploded time structure `tm'.
// This function does always put back normalized values into the `tm' struct,
// parameter, including the calculated numbers for `tm->tm_yday',
// `tm->tm_wday', and `tm->tm_isdst'.
// Returns -1 if the time in the `tm' parameter cannot be represented
// as valid `time_t' number.
//
time_t _mkgmtime(struct tm *tm)
{
    int years, months, days, hours, minutes, seconds;

    years = tm->tm_year + 1900; // year - 1900 -> year
    months = tm->tm_mon;        // 0..11
    days = tm->tm_mday - 1;     // 1..31 -> 0..30
    hours = tm->tm_hour;        // 0..23
    minutes = tm->tm_min;       // 0..59
    seconds = tm->tm_sec;       // 0..61 in ANSI C

    ADJUST_TM(seconds, minutes, 60)
        ADJUST_TM(minutes, hours, 60)
        ADJUST_TM(hours, days, 24)
        ADJUST_TM(months, years, 12)

        if (days < 0)
        {
            do
            {
                if (--months < 0)
                {
                    --years;
                    months = 11;
                }            
                days += monthlen(months, years);
            }
            while (days < 0);
        }
        else
        {
            while (days >= monthlen(months, years))
            {
                days -= monthlen(months, years);
                if (++months >= 12)
                {
                    ++years;
                    months = 0;
                }
            }
        }

        // Restore adjusted values in tm structure
        tm->tm_year = years - 1900;
        tm->tm_mon = months;
        tm->tm_mday = days + 1;
        tm->tm_hour = hours;
        tm->tm_min = minutes;
        tm->tm_sec = seconds;

        // Set `days' to the number of days into the year
        days += ydays[months] + (months > 1 && leap (years));
        tm->tm_yday = days;

        // Now calculate `days' to the number of days since Jan 1, 1970
        days = (unsigned)days + 365 * (unsigned)(years - 1970) +
            (unsigned)(nleap (years));

        tm->tm_wday = ((unsigned)days + 4) % 7; // Jan 1, 1970 was Thursday
        tm->tm_isdst = 0;

        if (years < 1970)
        {
            return (time_t)-1; // error
        }

#if (defined(TM_YEAR_MAX) && defined(TM_MON_MAX) && defined(TM_MDAY_MAX))
#if (defined(TM_HOUR_MAX) && defined(TM_MIN_MAX) && defined(TM_SEC_MAX))
        if (years > TM_YEAR_MAX ||
            (years == TM_YEAR_MAX &&
            (tm->tm_yday > ydays[TM_MON_MAX] + (TM_MDAY_MAX - 1) +
            (TM_MON_MAX > 1 && leap (TM_YEAR_MAX)) ||
            (tm->tm_yday == ydays[TM_MON_MAX] + (TM_MDAY_MAX - 1) +
            (TM_MON_MAX > 1 && leap (TM_YEAR_MAX)) &&
            (hours > TM_HOUR_MAX ||
            (hours == TM_HOUR_MAX &&
            (minutes > TM_MIN_MAX ||
            (minutes == TM_MIN_MAX && seconds > TM_SEC_MAX) )))))))
        {
            return (time_t)-1; // error
        }
#endif
#endif

        return (time_t)(86400L * (unsigned long)(unsigned)days +
            3600L * (unsigned long)hours +
            (unsigned long)(60 * minutes + seconds));
}

#endif
