/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_dynlib.h>
#include <dl_strings.h>

#ifndef _WIN32
#include <dlfcn.h>
#else
#include <winbase.h>
#endif

namespace dvblink { namespace engine {

dyn_lib::dyn_lib() :
    handle_(NULL)
{
    SetNoError();
}

dyn_lib::dyn_lib(const std::string& lib_name) :
    handle_(NULL),
    lib_name_(lib_name)
{
    SetNoError();
}

dyn_lib::dyn_lib(const std::wstring& lib_name) :
    handle_(NULL)
{
	ConvertUCToMultibyte(dvblink::engine::EC_UTF8, lib_name.c_str(), lib_name_);
    SetNoError();
}

dyn_lib::~dyn_lib()
{
    Unload();
}

void dyn_lib::SetLibName(const std::string& lib_name)
{
    lib_name_ = lib_name;
}

void dyn_lib::SetLibName(const std::wstring& lib_name)
{
	ConvertUCToMultibyte(dvblink::engine::EC_UTF8, lib_name.c_str(), lib_name_);
}

bool dyn_lib::Load()
{
    bool res = false;
    if (!handle_)
    {
        handle_ = DynLibLoad(lib_name_.c_str());
        if (!handle_)
        {
            m_strError = DynLibGetError();
        }
        else
        {
            SetNoError();
            res = true;
        }
    }
    return res;
}

bool dyn_lib::Unload()
{
    bool bRes = false;
    if (handle_)
    {
        if (!DynLibUnload(handle_))
        {
            m_strError = DynLibGetError();
        }
        else
        {
            handle_ = NULL;
            SetNoError();
        }

        bRes = handle_ ? false : true;
    }

    return bRes;
}

void* dyn_lib::GetFuncAddr(const char* func_name) const
{
    void* pFunc = NULL;
    
    pFunc = DynLibGetProc(handle_, func_name);
    if (!pFunc)
    {
        m_strError = DynLibGetError();
    }
    else
    {
        SetNoError();
    }

    return pFunc;
}

void dyn_lib::SetNoError() const
{
    m_strError = "no error";
}

const char* dyn_lib::GetError() const
{
    return m_strError.c_str();
}

const void dyn_lib::GetError(std::wstring& werror) const
{
    ConvertMultibyteToUC(EC_UTF8, m_strError.c_str(), werror);
}

//////////////////////////////////////////////////////////////////////////

dynlib dyn_lib::DynLibLoad(const std::string& lib_name)
{
#ifdef _WIN32
    std::string str = lib_name;
    size_t pos = str.find('/');
    while (pos != std::string::npos)
    {
        str[pos] = '\\';
        pos = str.find('/');
    }
	//convert path from utf-8 to unicode - otherwise we will not be able to load sources with non-ansi characters in them
	std::wstring wstr_dll;
	ConvertMultibyteToUC(EC_UTF8, str.c_str(), wstr_dll);
    return LoadLibraryExW(wstr_dll.c_str(), NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
#elif defined(_DARWIN_X86_64)
    return dlopen(lib_name.c_str(), RTLD_LOCAL);
#else
    return dlopen(lib_name.c_str(), RTLD_NOW /*RTLD_LAZY*/);
#endif
}

//
// return:  true  - success
//          false - fail
//
bool dyn_lib::DynLibUnload(dynlib handle)
{
    bool bRes;

#ifdef _WIN32
    bRes = FreeLibrary(reinterpret_cast<HMODULE>(handle)) ? true : false;
#else
    bRes = dlclose(handle) ? false : true;
#endif

    return bRes;
}

void* dyn_lib::DynLibGetProc(dynlib handle, const char* proc_name) const
{
#ifdef _WIN32
    return (void*)GetProcAddress(reinterpret_cast<HMODULE>(handle), proc_name);
#else
    return dlsym(handle, proc_name);
#endif
    
}

const char* dyn_lib::DynLibGetError() const
{
	const char* szMessage;

#ifdef _WIN32
    unsigned long dwLastError = GetLastError(); 

    FormatMessageA(
        FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dwLastError,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        m_szErrorBuf,
        sizeof(m_szErrorBuf),
        NULL);

    szMessage = m_szErrorBuf;
#else
    szMessage = dlerror();
#endif

    return szMessage;
}

} //engine
} //dvblink
