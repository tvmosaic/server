/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <stdlib.h> 
#include <string.h>
#include <stdio.h>

struct hufftab 
{ 
    char from;
    unsigned int value; 
    short bits; 
    char next; 
}; 

#include "bbc_huffman_tables.h"
 
#define START   '\0' 
#define STOP    '\0' 
#define ESCAPE  '\1' 

static struct hufftab *tables[2][256];
static int             table_size[2][256];

void bbc_huffman_table_init()
{
  int i;
  int this_table = -1;
  unsigned char this_char = 0xff;
  static bool runonce = true;

  if (runonce) 
  {
    runonce = false;
  } else 
  {
    return;	// Initialisation to be done once only
  }

  for (i=0; i<256; i++) {
    tables[0][i] = NULL;
    tables[1][i] = NULL;
    table_size[0][i] = 0;
    table_size[1][i] = 0;
  }

  for (i=0; i<MEMTABLE_SIZE; i++) {
    if (memtable[i].from != this_char) {
      this_char = memtable[i].from;
      if (!this_char) { // Jumping to next table
        this_table++;
      }
      tables[this_table][this_char] = &memtable[i];
    }
    table_size[this_table][this_char]++;
  }
}

char* bbc_huffman_decode( const unsigned char* src_orig, size_t size_orig) 
{
    try
    {
        if (src_orig[0] == 1 || src_orig[0] == 2) 
        { 
            unsigned char* src = (unsigned char*)calloc(size_orig + 1, 1);
            src[0] = 0x1F;
            memcpy(src + 1, src_orig, size_orig);
            size_t size = size_orig + 1;
            int tableid;

            int    uncompressed_len = 30;
            char * uncompressed = (char *)calloc(1,uncompressed_len + 1);
            unsigned value = 0, byte = 2, bit = 0; 
            int p = 0; 
            int lastch = START; 

            tableid = src[1] - 1;
            while (byte < 6 && byte < size) {
                value |= src[byte] << ((5-byte) * 8); 
                byte++; 
            } 
      
            bbc_huffman_table_init();

            do {
                int found = 0; 
                unsigned bitShift = 0; 
                if (lastch == ESCAPE) {
                    char nextCh = (value >> 24) & 0xff; 
                    found = 1; 
                    // Encoded in the next 8 bits. 
                    // Terminated by the first ASCII character. 
                    bitShift = 8; 
                    if ((nextCh & 0x80) == 0) 
                        lastch = nextCh; 
                    if (p >= uncompressed_len) {
                        uncompressed_len += 10;
                        uncompressed = (char *)realloc(uncompressed, uncompressed_len + 1);
                    }
                    uncompressed[p++] = nextCh; 
                    uncompressed[p] = 0;
                } else {
                    int j;
                    for ( j = 0; j < table_size[tableid][lastch]; j++) {
                        unsigned mask = 0, maskbit = 0x80000000; 
                        short kk;
                        for ( kk = 0; kk < tables[tableid][lastch][j].bits; kk++) {
                            mask |= maskbit; 
                            maskbit >>= 1; 
                        } 
                        if ((value & mask) == tables[tableid][lastch][j].value) {
                            char nextCh = tables[tableid][lastch][j].next; 
                            bitShift = tables[tableid][lastch][j].bits; 
                            if (nextCh != STOP && nextCh != ESCAPE) {
                                if (p >= uncompressed_len) {
                                    uncompressed_len += 10;
                                    uncompressed = (char *)realloc(uncompressed, uncompressed_len + 1);
                                }
                                uncompressed[p++] = nextCh; 
                                uncompressed[p] = 0;
                            } 
                            found = 1;
                            lastch = nextCh; 
                            break; 
                        } 
                    } 
                } 
                if (found) {
                    // Shift up by the number of bits. 
                    unsigned b;
                    for ( b = 0; b < bitShift; b++) 
                    { 
                        value = (value << 1) & 0xfffffffe; 
                        if (byte < size) 
                            value |= (src[byte] >> (7-bit)) & 1; 
                        if (bit == 7) 
                        { 
                            bit = 0; 
                            byte++; 
                        } 
                        else bit++; 
                    } 
                } else 
                {
                    // Entry missing in table. 
                    free(src);
                    return uncompressed; 
                } 
            } while (lastch != STOP && value != 0); 
     
            free(src);
            return uncompressed;
        }
    } catch(...) {}
    return NULL; 
} 
