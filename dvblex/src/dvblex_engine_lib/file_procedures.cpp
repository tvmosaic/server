/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef _WIN32
#include <Windows.h>
#include <io.h>
#endif
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp> 
#include <dl_file_procedures.h>
#include <dl_strings.h>

namespace dvblink { namespace engine { namespace filesystem
{

bool create_directory(const boost::filesystem::path& dir)
{
    bool res = true;
    try
    {
        if (!boost::filesystem::exists(dir))
        {
            boost::filesystem::path create_dir;
            for (boost::filesystem::path::iterator iter = dir.begin(); iter != dir.end(); ++iter)
            {
                if (!boost::filesystem::exists(create_dir /= *iter))
                {
                    if ((res &= boost::filesystem::create_directory(create_dir)) == false)
                        break;
                }
            }
        }
    }
    catch (boost::filesystem::filesystem_error& /*e*/)
    {
        res = false;
    }

    return res;
}

bool copy_directory_recursive(const boost::filesystem::path& src, const boost::filesystem::path& dest)
{
    bool res = true;
    try
    {
        if ((res = dvblink::engine::filesystem::create_directory(dest)) == true)
        {
            boost::filesystem::directory_iterator end_itr;
            for (boost::filesystem::directory_iterator iter(src); iter != end_itr; ++iter)
            {
                if (boost::filesystem::is_regular_file(iter->status()))
                {
                    boost::filesystem::copy_file(iter->path(), dest / iter->path().filename(), boost::filesystem::copy_option::overwrite_if_exists);
                }
                else
                if (boost::filesystem::is_directory(iter->status()))
                {
                    boost::filesystem::path crdir(*iter);
                    boost::filesystem::path new_dest = dest / *(--crdir.end());
                    if ((res = dvblink::engine::filesystem::create_directory(new_dest)) == true)
                    {
                        if ((res = copy_directory_recursive(*iter, new_dest)) == false)
                            break;
                    }
                }
            }
        }
    }
    catch (boost::filesystem::filesystem_error& /*e*/)
    {
        res = false;
    }

    return res;
}


bool delete_directory_recursive(const boost::filesystem::path& dir)
{
    try
    {
        boost::filesystem::remove_all(dir);
    }
    catch (boost::filesystem::filesystem_error& /*e*/)
    {
        return false;
    }
    return true;
}

bool delete_directory_contents(const boost::filesystem::path& dir_path)
{
    namespace fs = boost::filesystem;

    if (!fs::exists(dir_path) || !fs::is_directory(dir_path))
    {
        return false;
    }

    try
    {
        const fs::directory_iterator end;

        for (fs::directory_iterator it(dir_path); it != end; ++it)
        {
            if (fs::is_regular_file(it->status()))
            {
                //file
                fs::remove(it->path());
            } else
            {
                //directory
                fs::remove_all(it->path());
            }
        }
    }
    catch (...)
    {
        return false;
    }
    return true;
}

bool find_files(const boost::filesystem::path& dir_path, ff_t& result,
    const std::wstring& file_ext)
{
    namespace fs = boost::filesystem;

    if (!fs::exists(dir_path) || !fs::is_directory(dir_path))
    {
        return false;
    }

    try
    {
        const fs::directory_iterator end;

        for (fs::directory_iterator it(dir_path); it != end; ++it)
        {
            if (!fs::is_regular_file(it->status()))
            {
                continue; // skip
            }

            const fs::path& fpath = it->path();

            if (!file_ext.empty())
            {
            #ifdef _WIN32
                if (!boost::iequals(fpath.extension().wstring(), file_ext))
            #else
                std::wstring wext = string_cast<EC_UTF8>(fpath.extension().string());
                if (!boost::iequals(wext, file_ext))
            #endif
                {
                    continue; // skip
                }
            }

            result.push_back(fpath);
        }
    }
    catch (...)
    {
        return false;
    }

    return true;
}

static boost::int64_t get_file_size(const wchar_t* fname)
{
    boost::int64_t ret_val = -1;
#ifdef _WIN32
    int fh = _wopen(fname, _O_RDONLY | _O_BINARY);

    if (fh != -1)
    {
        struct _stat64 stbuf;
        if (_fstat64(fh, &stbuf) != -1) 
            ret_val = stbuf.st_size;
        _close(fh);
    }
#else
    std::string fname_str = string_cast<EC_UTF8>(std::wstring(fname));
    int fh = open(fname_str.c_str(), O_RDONLY);

    if (fh != -1)
    {
        struct stat stbuf;
        if (fstat(fh, &stbuf) != -1) 
            ret_val = stbuf.st_size;
        close(fh);
    }
#endif
    return ret_val;
}

boost::int64_t get_file_size(const std::wstring& fname)
{
  return get_file_size(fname.c_str());
}

boost::int64_t get_file_size(const std::string& fname)
{
  return get_file_size(string_cast<EC_UTF8>(fname).c_str());
}

FILE* universal_open_file(const dvblink::filesystem_path_t& fname, const char* access_mode)
{
    FILE* f = NULL;
    #ifdef _WIN32
        f = _wfopen(fname.to_wstring().c_str(), string_cast<EC_UTF8>(access_mode).c_str());
    #else
        f = fopen(fname.to_string().c_str(), access_mode);
    #endif

    return f;
}

int universal_file_seek(FILE* f, boost::int64_t offset, int origin)
{
    int ret_val = -1;
#ifdef WIN32
        ret_val = _fseeki64(f, offset, origin);
#elif defined(_DARWIN_X86_64)
    	ret_val = fseeko(f, offset, origin);
#elif defined(__ANDROID__)
        ret_val = fseek(f, offset, origin);
#else
        ret_val = fseeko64(f, offset, origin);
#endif
    return ret_val;
}

#ifdef _WIN32
bool get_drives(directory_tree& dt)
{
    boost::uint32_t drives = GetLogicalDrives();

    char letter[2] = {0};
    for (char i = 0; i < 26; ++i)
    {
        if (drives & (1 << i))
        {
            letter[0] = 'A' + i;
            std::string drive(letter);
            dt.add_dir(dvblink::filesystem_path_t(drive + ":\\"));
        }
    }

    return true;
}
#endif

bool get_directory_tree(directory_tree& dt)
{
    bool res = true;
    dt.tree_.clear();

    try
    {
#ifndef _WIN32
        //on linux-based systems request for the first level from root coms with //. Change it to /
        if (boost::starts_with(dt.parent_path_.to_string(), "//"))
            dt.parent_path_ = dt.parent_path_.to_string().substr(1);
#endif

        boost::filesystem::path p = dt.parent_path_.to_boost_filesystem();
        p = p.normalize();
        p.make_preferred();
        dt.parent_path_ = p;

        bool b_root_request = boost::iequals(dt.parent_path_.to_string(), "/") || dt.parent_path_.empty();
#ifdef _WIN32
        // check if this is root of drive
        //
        if (!b_root_request)
        {
            dvblink::filesystem_path_t rp = p.root_path().string();
            dvblink::filesystem_path_t pp = p.parent_path().string();
            dvblink::filesystem_path_t fn = p.filename().string();

            b_root_request = (boost::iequals(rp.to_string(), pp.to_string()) && boost::iequals(fn.to_string(), "..") && !is_empty(p.root_name()));
        }
#endif
        if (b_root_request)
#ifdef _WIN32
            dt.parent_path_ = "";
#else
            dt.parent_path_ = "/";
#endif

    #ifdef _WIN32
        // request for root contents
        //
        if (b_root_request)
        {
            get_drives(dt);
        }
        else
        {
    #endif
            if (!b_root_request)
                dt.add_dir(dvblink::filesystem_path_t(".."));

            boost::filesystem::directory_iterator end_itr;
            for (boost::filesystem::directory_iterator iter(p); iter != end_itr; ++iter)
            {
                if (boost::filesystem::is_directory(iter->status()))
                {
                    boost::filesystem::path sub_dir(*iter);
                    dt.add_dir(sub_dir.filename());
                }
            }
    #ifdef _WIN32
        }
    #endif
        //    std::cout << std::endl;
    }
    catch (const boost::filesystem::filesystem_error& /*fe*/)
    {
        res = false;
    }
    catch (const boost::system::error_code& /*ec*/)
    {
        res = false;
    }
    catch (...)
    {
        res = false;
    }
    return res;
}


dvblink::filesystem_path_t make_preferred_dvblink_filepath(const dvblink::filesystem_path_t& fname)
{
    boost::filesystem::path p = fname.to_boost_filesystem();
    p = p.make_preferred();
    return dvblink::filesystem_path_t(p);
}

char* read_file_into_memory(const dvblink::filesystem_path_t& fname, boost::int64_t& size)
{
    char* ret_val = NULL;
    size = filesystem::get_file_size(fname.to_string());
    if (size > 0)
    {
        FILE* f = filesystem::universal_open_file(fname, "r+b");
        if (f != NULL)
        {
            ret_val = (char*) malloc(size);
            if (ret_val != NULL)
            {
                if (fread(ret_val, 1, size, f) != size)
                {
                    //not successfull
                    free(ret_val);
                    ret_val = NULL;
                }
            }
            fclose(f);
        }
    }
    return ret_val;
}

} // filesystem
} //engine
} //dvblink
