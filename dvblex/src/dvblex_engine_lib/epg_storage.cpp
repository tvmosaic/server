/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#include <boost/filesystem.hpp>

#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_url_encoding.h>
#include <dl_logger.h>
#include <dl_utils.h>
#include <dl_message_addresses.h>
#include <dl_epg_storage.h>
#include <dl_xml_serialization.h>
#include <dl_epg_source_commands.h>

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;

const std::string master_file = "channel_info.xml";
const std::string schedule_directory = "schedules";

namespace dvblex {

static time_t default_first_update_delay_sec = 5;

time_t epg_storage_t::get_first_update_delay_sec()
{
    return default_first_update_delay_sec;
}

epg_storage_t::epg_storage_t(dvblink::messaging::message_queue_t& message_queue, const dvblink::epg_source_name_t& name, const dvblink::filesystem_path_t& disk_path) :
    message_queue_(message_queue), 
    update_thread_(NULL), 
    exit_flag_(false), 
    id_(message_queue->get_id().get()), 
    name_(name), 
    has_settings_(false), 
    periodic_updates_enabled_(true),
    use_memory_storage_(true)
{
    epg_storage_path_ = disk_path / get_id().to_string();
    schedules_path_ = epg_storage_path_ / schedule_directory;
    channels_file_path_ = epg_storage_path_ / master_file;
}

epg_storage_t::epg_storage_t(const dvblink::epg_source_id_t& id, const dvblink::epg_source_name_t& name,
                             const dvblink::i_server_t& server, const dvblink::filesystem_path_t& disk_path) :
    server_(server), update_thread_(NULL), exit_flag_(false), id_(id), name_(name), has_settings_(false)
{
    epg_storage_path_ = disk_path / get_id().to_string();
    schedules_path_ = epg_storage_path_ / schedule_directory;
    channels_file_path_ = epg_storage_path_ / master_file;
}

epg_storage_t::~epg_storage_t()
{
}

bool epg_storage_t::start()
{
    if (!use_memory_storage_)
    {
        //create storage directory or remove its contents if it exists already
        if (boost::filesystem::exists(epg_storage_path_.to_boost_filesystem()))
        {
            remove_directory_content(epg_storage_path_);
        } else
        {
            try {
                boost::filesystem::create_directories(epg_storage_path_.to_boost_filesystem());
            } catch(...) {}
        }
    } else
    {
        memory_channel_map_storage_.clear();
        memory_epg_storage_map_.clear();
    }

    //register a message queue
    if (server_.get() != NULL)
    {
        message_queue_ = share_object_safely(new message_queue(id_.get()));
        message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));
        server_->register_queue(message_queue_);
    } else
    {
        message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));
    }

    exit_flag_ = false;
    update_thread_ = new boost::thread(boost::bind(&epg_storage_t::update_thread_func, this));

#ifdef WIN32
	    HANDLE thread_handle = update_thread_->native_handle();
	    SetThreadPriority(thread_handle, THREAD_PRIORITY_BELOW_NORMAL);
#endif

    return true;
}

void epg_storage_t::remove_directory_content(const dvblink::filesystem_path_t& dir)
{
    boost::filesystem::path path_to_remove(dir.to_boost_filesystem());
    for (boost::filesystem::directory_iterator end_dir_it, it(path_to_remove); it!=end_dir_it; ++it) 
    {
        try {
            boost::filesystem::remove_all(it->path());
        } catch(...){}
    }
}

void epg_storage_t::stop()
{
    if (update_thread_ != NULL)
    {
        exit_flag_ = true;

        update_thread_->join();
        delete update_thread_;
        update_thread_ = NULL;
    }

    //unregister epg source
    unregister_source();

    //unregister message queue
    if (server_.get() != NULL)
    {
        server_->unregister_queue(message_queue_->get_id());
        message_queue_->shutdown();
    }

    message_handler_.reset();

    if (!use_memory_storage_)
    {
        //remove epg_storage_path on stop - it is considered only to be a cache
        try {
            boost::filesystem::remove_all(epg_storage_path_.to_boost_filesystem());
        } catch(...) {}
    }
}

bool epg_storage_t::get_epg_channels(epg_source_channel_map_t& epg_channel_map)
{
    return read_channels_file(channels_file_path_, epg_channel_map);
}

bool epg_storage_t::read_channels_file(const dvblink::filesystem_path_t& f, epg_source_channel_map_t& channels)
{
    boost::mutex::scoped_lock lock(lock_);
    
    bool res = false;

    if (!use_memory_storage_)
    {
        res = read_epg_channels_from_file(f, channels);
    } else
    {
        channels = memory_channel_map_storage_;
        res = true;
    }

    return res;
}

bool epg_storage_t::write_channels_file(const epg_source_channel_map_t& channels, std::vector<dvblink::epg_channel_id_t>& non_existent_channels)
{
    //create a list of channels, which disappear after update
    epg_source_channel_map_t current_channel_map;
    if (get_epg_channels(current_channel_map))
    {
        epg_source_channel_map_t::iterator it = current_channel_map.begin();
        while (it != current_channel_map.end())
        {
            if (channels.find(it->first) == channels.end())
                non_existent_channels.push_back(it->first);

            ++it;
        }
    }

    boost::mutex::scoped_lock lock(lock_);

    bool res = false;

    if (!use_memory_storage_)
    {
        res = write_epg_channels_to_file(channels, channels_file_path_);
    } else 
    {
        memory_channel_map_storage_ = channels;
        res = true;
    }

    return res;
}

std::string epg_storage_t::get_encoded_channel_id(const dvblink::epg_channel_id_t& channel_id)
{
    //convert channel id into url_encoded form to avoid invalid characters in filepath
    std::string idstr = channel_id.to_string();
    std::string encoded_idstr;
    url_encode(idstr, encoded_idstr) ;
    return encoded_idstr;
}

epg_channel_id_t epg_storage_t::get_decoded_channel_id(const std::string& encoded_channel_id)
{
    std::string decoded_idstr;
    url_decode(encoded_channel_id.c_str(), decoded_idstr) ;
    return epg_channel_id_t(decoded_idstr);
}

bool epg_storage_t::get_epg_for_channel(const dvblink::epg_channel_id_t& channel_id, dvblink::engine::DLEPGEventList& events)
{
    dvblink::filesystem_path_t channel_epg_file_path = schedules_path_ / get_encoded_channel_id(channel_id);

    boost::mutex::scoped_lock lock(lock_);

    bool res = false;
    
    if (!use_memory_storage_)
    {
        res = EPGReadEventsFromFile(channel_epg_file_path, events);
    } else
    {
        if (memory_epg_storage_map_.find(channel_id) != memory_epg_storage_map_.end())
        {
            std::string events_str;
            if (memory_epg_storage_map_[channel_id].get_string(events_str))
                res = EPGReadEventsFromXML(events_str, events);
        }
    }

    return res;
}

bool epg_storage_t::store_channel_schedule(const dvblink::epg_channel_id_t& id, DLEPGEventList& schedule)
{
    dvblink::filesystem_path_t channel_epg_file_path = schedules_path_ / get_encoded_channel_id(id);

    boost::mutex::scoped_lock lock(lock_);
    bool res = false;

    if (!use_memory_storage_)
    {
        res = EPGWriteEventsToFile(schedule, channel_epg_file_path);
    } else
    {
        std::string events_str;
        if (EPGWriteEventsToXML(schedule, events_str))
        {
            memory_epg_storage_map_[id] = gzip_string_container(events_str);
            res = true;
        }
    }

    return res;
}

bool epg_storage_t::is_started()
{
    return (update_thread_ != NULL);
}

bool epg_storage_t::register_source()
{
    epg::register_epg_source_request req(epg_source_t(id_, name_, has_settings_));
    epg::register_epg_source_response resp;
    message_queue_->send(epg_manager_message_queue_addressee, req, resp);

    if (resp.result_)
        periodic_updates_enabled_ = resp.periodic_updates_enabled_;

    return resp.result_;
}

bool epg_storage_t::unregister_source()
{
    epg::unregister_epg_source_request req(id_);
    epg::unregister_epg_source_response resp;
    message_queue_->send(epg_manager_message_queue_addressee, req, resp);
    return resp.result_;
}

void epg_storage_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_channel_epg_request& request, dvblink::messaging::epg::get_channel_epg_response& response)
{
    response.result_ = get_epg_for_channel(request.channel_id_, response.epg_);
}

void epg_storage_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_epg_channels_request& request, dvblink::messaging::epg::get_epg_channels_response& response)
{
    response.channels_.clear();
    get_epg_channels(response.channels_);
    response.result_ = true;
}

void epg_storage_t::update_thread_func()
{
    time_t next_update_time;
    time(&next_update_time);
    next_update_time += get_first_update_delay_sec();

    log_info(L"epg_storage_t::update_thread_func. Started for source %1%. First update is scheduled for %2%") % get_id().to_wstring() % next_update_time;

    register_source();

    while (!exit_flag_)
    {
        time_t now;
        time(&now);

        if ((now >= next_update_time && periodic_updates_enabled_) || force_refresh_.is_signaled())
        {
            if (force_refresh_.is_signaled())
                force_refresh_.reset();

            log_info(L"epg_storage_t::update_thread_func. Starting update for source %1%") % get_id().to_wstring();

            bool bsuccess = false;
            //epg update time
            epg_source_channel_map_t channel_map;
            if (do_prefetch(channel_map))
            {
                std::vector<dvblink::epg_channel_id_t> non_existent_channels;
                write_channels_file(channel_map, non_existent_channels);
                log_info(L"epg_storage_t::update_thread_func. Received channels for source %1%") % get_id().to_wstring();

                epg::epg_updated_request req;
                req.epg_source_id_ = id_.get();

                //update epg for each channel
                if (download_epg())
                {
                    log_info(L"epg_storage_t::update_thread_func. Received epg for source %1%") % get_id().to_wstring();
                    if (!use_memory_storage_)
                    {
                        //create directory for schedules
                        try {
                            boost::filesystem::create_directories(schedules_path_.to_boost_filesystem());
                        } catch(...) {}
                    }

                    epg_source_channel_map_t::const_iterator iter = channel_map.begin(), end = channel_map.end();
                    while (iter != end && !exit_flag_)
                    {
                        DLEPGEventList schedule;
                        if (get_channel_epg(iter->first, schedule))
                        {
                            log_info(L"epg_storage_t::update_thread_func. Updating epg for channel %1% / source %2%") % string_cast<EC_UTF8>(iter->first.get()) % get_id().to_wstring();
                            store_channel_schedule(iter->first, schedule);

                            req.updated_epg_channels_.push_back(iter->first.get());
                        }

                        ++iter;
                    }

                    bsuccess = !exit_flag_;
                }

                if (bsuccess)
                    message_queue_->post(broadcast_addressee, req);

                cleanup();

                delete_non_existent_channels(non_existent_channels);

            } else
            {
                log_error(L"epg_storage_t::update_thread_func. EPG prefetch failed for source %1%") % get_id().to_wstring();
            }

            next_update_time = get_next_update_time(bsuccess);
            log_info(L"epg_storage_t::update_thread_func. Update finished for source %1%. Next update is scheduled for %2%") % get_id().to_wstring() % next_update_time;
        }

        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
    }
}

void epg_storage_t::delete_non_existent_channels(const std::vector<dvblink::epg_channel_id_t>& non_existent_channels)
{
    for (size_t i=0; i<non_existent_channels.size(); i++)
    {
        if (use_memory_storage_)
        {
            memory_epg_storage_map_.erase(non_existent_channels[i]);
        } else
        {
            dvblink::filesystem_path_t channel_epg_file_path = schedules_path_ / get_encoded_channel_id(non_existent_channels[i]);
            try {
                boost::filesystem::remove(channel_epg_file_path.to_boost_filesystem());
            } catch(...){}
        }
    }
}

void epg_storage_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response)
{
    response.result_ = dvblink::xmlcmd_result_error;

    if (boost::iequals(request.cmd_id_.get(), epg_source_refresh_cmd))
    {
        log_info(L"epg_storage_t::handle xml_command. Refresh");

        if (is_started())
            refresh();

        response.result_ = dvblink::xmlcmd_result_success;
    } else
    if (boost::iequals(request.cmd_id_.get(), epg_source_get_settings_cmd))
    {
        log_info(L"epg_storage_t::handle xml_command. Get settings");

        get_epg_source_settings_response_t resp;
        if (get_source_settings(resp.settings_))
        {
            std::string data;
            write_to_xml(resp, data);

            response.xml_ = data;
            response.result_ = dvblink::xmlcmd_result_success;
        }
    } else
    if (boost::iequals(request.cmd_id_.get(), epg_source_set_settings_cmd))
    {
        log_info(L"epg_storage_t::handle xml_command. Set settings");

        set_epg_source_settings_request_t req;
        if (read_from_xml(request.xml_.get(), req))
        {
            set_source_settings(req.settings_);

            if (is_started())
                refresh();

            response.result_ = dvblink::xmlcmd_result_success;
        }
    }
}

void epg_storage_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::force_epg_update_request& request)
{
    log_info(L"epg_storage_t::handle force_epg_update_request. Refresh");

    if (is_started())
        refresh();
}

void epg_storage_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::enable_periodic_epg_updates_request& request)
{
    periodic_updates_enabled_ = request.enable_;
    log_info(L"epg_storage_t::handle enable_periodic_epg_updates_request:%1%") % periodic_updates_enabled_;
}

} // dvblex
