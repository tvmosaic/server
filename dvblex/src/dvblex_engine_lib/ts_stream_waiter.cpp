/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#include <algorithm>
#include <boost/algorithm/string.hpp> 
#include <boost/thread/thread.hpp>
#include <dl_ts_stream_waiter.h>
#include <dl_logger.h>

using namespace dvblink::engine;
using namespace dvblink::logging;

CTSStreamWaiter::CTSStreamWaiter(dvblink::engine::LP_PACKGEN_TS_SEND_FUNC cb_func, void* param) 
	: m_state(ES_STOPPED), cb_func_(cb_func), param_(param)
{
}

CTSStreamWaiter::~CTSStreamWaiter()
{
}

void CTSStreamWaiter::Start(CTSPmtParser* pmt_parser)
{
	boost::unique_lock<boost::shared_mutex> lock(mutex_);

	pid_ = GetPidToCheck(pmt_parser);
	if (pid_ != INVALID_PACKET_PID)
		m_state = ES_WAITING_STREAM;
	else
		m_state = ES_STREAMING;

	video_pid_ = GetVideoPid(pmt_parser);
}

void CTSStreamWaiter::Stop()
{
	boost::unique_lock<boost::shared_mutex> lock(mutex_);
	m_state = ES_STOPPED;
}

CTSStreamWaiter::EState CTSStreamWaiter::GetState()
{
	boost::unique_lock<boost::shared_mutex> lock(mutex_);
	return m_state;
}

void CTSStreamWaiter::ProcessStream(unsigned char* buffer, int len)
{
    boost::unique_lock<boost::shared_mutex> lock(mutex_, boost::try_to_lock);
    if (lock.owns_lock())
    {
		switch (m_state)
		{
		case ES_WAITING_STREAM:
		case ES_WAITING_DECRYPTION:
			{
				int c = len / TS_PACKET_SIZE;
				for (int i=0; i<c && (m_state == ES_WAITING_STREAM || m_state == ES_WAITING_DECRYPTION); i++)
				{
					unsigned char* buf = (unsigned char*)buffer + i*TS_PACKET_SIZE;
					unsigned short pid = ts_process_routines::GetPacketPID(buf);
					if (pid == pid_)
					{
						if (!IsStreamEncrypted(buf))
						{
							//unfortunately not all channels have the random access bit set when packet contains I frame
							//so skip waiting for now
//							if (video_pid_ != INVALID_PACKET_PID)
							if (false)
							{
								m_state = ES_WAITING_KEY_FRAME;
								log_info(L"CTSStreamWaiter::ProcessStream. Found first not encrypted packet. Waiting for key frame");
							} else
							{
								m_state = ES_STREAMING;
								log_info(L"CTSStreamWaiter::ProcessStream. Found first not encrypted packet. Start streaming");
							}
						} else
						{
							m_state = ES_WAITING_DECRYPTION;
						}
					}
				}
			}
			break;
		case ES_WAITING_KEY_FRAME:
			{
				int c = len / TS_PACKET_SIZE;
				for (int i=0; i<c && m_state == ES_WAITING_KEY_FRAME; i++)
				{
					unsigned char* buf = (unsigned char*)buffer + i*TS_PACKET_SIZE;
					unsigned short pid = ts_process_routines::GetPacketPID(buf);
					if (pid == video_pid_)
					{
						if (ts_process_routines::packet_starts_with_keyframe(buf))
						{
							m_state = ES_STREAMING;
							log_info(L"CTSStreamWaiter::ProcessStream. Found key frame. Start streaming");
						}
					}
				}
			}
			break;
		case ES_STREAMING:
			if (cb_func_ != NULL)
				cb_func_(buffer, len, param_);
			break;
		case ES_STOPPED:
		default:
			break;
		}
	}
}

unsigned short CTSStreamWaiter::GetPid()
{
	unsigned short pid = INVALID_PACKET_PID;
	if (m_state == ES_STREAMING)
		pid = pid_;
	return pid;
}

unsigned short CTSStreamWaiter::GetPidToCheck(CTSPmtParser* pmt_parser)
{
	unsigned short pid = INVALID_PACKET_PID;

	unsigned short audio_pid = INVALID_PACKET_PID;
	unsigned short video_pid = INVALID_PACKET_PID;
	CTSPmtInfo* pmtInfo = pmt_parser->GetPmtInfo();
	if (pmtInfo != NULL)
	{
		std::vector<SESInfo> streams;
		pmtInfo->GetStreams(streams);
		for (unsigned int i=0; i<streams.size(); i++)
		{
            std::vector<TCA_PMT_CA_DESC> vector_desc;
            ts_process_routines::DescriptorsLinearToArray(streams[i].descriptors, vector_desc);

            if (ts_process_routines::IsAudioStream(streams[i].es_header.stream_type, vector_desc))
			{
				audio_pid = HILO_GET(streams[i].es_header.elementary_PID);
			}
            if (ts_process_routines::IsVideoStream(streams[i].es_header.stream_type, vector_desc))
			{
				video_pid = HILO_GET(streams[i].es_header.elementary_PID);
			}
		}
		if (video_pid != INVALID_PACKET_PID)
			pid = video_pid;
		else
			if (audio_pid != INVALID_PACKET_PID)
				pid = audio_pid;
	}
	
	if (pid != INVALID_PACKET_PID)
		log_info(L"CTSStreamWaiter::GetPidToCheck. PID to check %1%") % pid;

	return pid;
}

unsigned short CTSStreamWaiter::GetVideoPid(CTSPmtParser* pmt_parser)
{
	unsigned short pid = INVALID_PACKET_PID;

	CTSPmtInfo* pmtInfo = pmt_parser->GetPmtInfo();
	if (pmtInfo != NULL)
	{
		std::vector<SESInfo> streams;
		pmtInfo->GetStreams(streams);
		for (unsigned int i=0; i<streams.size(); i++)
		{
            std::vector<TCA_PMT_CA_DESC> vector_desc;
            ts_process_routines::DescriptorsLinearToArray(streams[i].descriptors, vector_desc);

            if (ts_process_routines::IsVideoStream(streams[i].es_header.stream_type, vector_desc))
			{
				pid = HILO_GET(streams[i].es_header.elementary_PID);
				break;
			}
		}
	}
	
	if (pid != INVALID_PACKET_PID)
		log_info(L"CTSStreamWaiter::GetVideoPid. Video PID %1%") % pid;
	else
		log_info(L"CTSStreamWaiter::GetVideoPid. No video PID found");

	return pid;
}

bool CTSStreamWaiter::IsStreamEncrypted(const unsigned char* pPacket)
{
    const STransportHeader* pTS = reinterpret_cast<const STransportHeader*>(pPacket);
    
    bool bRes = true;
    if (pTS->payload_unit_start_indicator)
    {
        int offset = ts_process_routines::GetPayloadOffset(const_cast<unsigned char*>(pPacket));

        bRes = ps_process_routines::IsValidPESPacketStart(const_cast<unsigned char*>(pPacket) + offset, TS_PACKET_SIZE - offset, EPS_PST_UNKNOWN) ? false : true;
    }

    return bRes;
}
