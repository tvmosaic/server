/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#include <algorithm>
#include <boost/algorithm/string.hpp> 
#include <boost/thread/thread.hpp>
#include <dl_ts_audio_selector.h>
#include <dl_utils.h>

using namespace dvblink::engine;
using std::min;

dvblink::engine::CTSAudioSelector::CTSAudioSelector(dvblink::engine::LP_PACKGEN_TS_SEND_FUNC cb_func, void* param) :
    m_contCounter(0),
    m_pmt_pid(INVALID_PACKET_PID),
    m_cb_func(cb_func),
    m_param(param),
    m_state(ES_STOPPED)
{
}

dvblink::engine::CTSAudioSelector::~CTSAudioSelector()
{
}

void CTSAudioSelector::Start(const char* lng, unsigned short pmt_pid)
{
    boost::unique_lock<boost::shared_mutex> lock(m_lock);

	pid_exclude_list_.clear();

    m_lng = lng;
    m_pmt_pid = pmt_pid;
    if (m_pmt_pid == INVALID_PACKET_PID)
    {
        m_sectionParser.Init(PID_PAT);
        m_state = ES_PAT;
    }
    else
    {
        m_sectionParser.Init(pmt_pid);
        m_state = ES_PMT;
    }
}

void CTSAudioSelector::Start(const char* lng, CTSPmtParser* pmt_parser)
{
    boost::unique_lock<boost::shared_mutex> lock(m_lock);

    m_lng = lng;

	CTSPmtInfo* pmtinfo = pmt_parser->GetPmtInfo();
	if (pmtinfo != NULL && pmt_parser->GetPMTPid() != INVALID_PACKET_PID)
	{
		ProcessPMTSection(*pmtinfo);
		m_pmt_pid = pmt_parser->GetPMTPid();
        m_sectionParser.Init(m_pmt_pid);
        m_state = ES_STREAMING;

	} else
	{
		m_pmt_pid = INVALID_PACKET_PID;
        m_sectionParser.Init(PID_PAT);
        m_state = ES_PAT;
	}
}

void dvblink::engine::CTSAudioSelector::Stop()
{
    boost::unique_lock<boost::shared_mutex> lock(m_lock);
    m_state = ES_STOPPED;
}

void dvblink::engine::CTSAudioSelector::ProcessStream(unsigned char* buffer, int len)
{
    boost::shared_lock<boost::shared_mutex> lock(m_lock, boost::try_to_lock);
    if (lock.owns_lock())
    {
    	if (m_state == ES_PAT)
    	{
			int c = len / TS_PACKET_SIZE;
			for (int i=0; i<c; i++)
			{
				unsigned char* buf = buffer + i*TS_PACKET_SIZE;
				unsigned short pid = dvblink::engine::ts_process_routines::GetPacketPID(buf);
				if (pid == PID_PAT)
				{
					ts_payload_parser::ts_section_list found_sections;
					if (m_sectionParser.AddPacket(buf, TS_PACKET_SIZE, found_sections) > 0)
					{
						//get PAT section
						for (unsigned int ii=0; ii<found_sections.size() && m_pmt_pid == INVALID_PACKET_PID; ii++)
						{
							ProcessPATSection(found_sections[ii].section, found_sections[ii].length);
						}
						m_sectionParser.ResetFoundSections(found_sections);
					}
				}
			}
    	} else
		if (m_state == ES_PMT)
		{
			int c = len / TS_PACKET_SIZE;
			for (int i=0; i<c; i++)
			{
				unsigned char* buf = buffer + i*TS_PACKET_SIZE;
				unsigned short pid = dvblink::engine::ts_process_routines::GetPacketPID(buf);
				if (pid == m_pmt_pid)
				{
					ts_payload_parser::ts_section_list found_sections;
					if (m_sectionParser.AddPacket(buf, TS_PACKET_SIZE, found_sections) > 0)
					{
						//get PMT section
						for (unsigned int ii=0; ii<found_sections.size(); ii++)
						{
							//this will fill also all exclude pids (if any)
							CTSPmtInfo pmtinfo;
							pmtinfo.Init(found_sections[ii].section, found_sections[ii].length);

							ProcessPMTSection(pmtinfo);
						}
						m_sectionParser.ResetFoundSections(found_sections);

						m_state = ES_STREAMING;
					}
				}
			}
		} else
		if (m_state == ES_STREAMING)
		{
			const unsigned char* start = buffer;
			const unsigned char* cur = buffer;
			const unsigned char* end = buffer + len;
			size_t to_send = 0;

			while (cur < end)
			{
				unsigned short pid = ts_process_routines::GetPacketPID(cur);
				if (pid == m_pmt_pid || pid_exclude_list_.find(pid) != pid_exclude_list_.end())
				{
					if (to_send)
					{
						m_cb_func(start, to_send, m_param);
						to_send = 0;
					}

					if (pid == m_pmt_pid)
					{
						ts_payload_parser::ts_section_list found_sections;
						if (m_sectionParser.AddPacket(cur, TS_PACKET_SIZE, found_sections) > 0)
						{
							//get PMT section
							for (unsigned int ii=0; ii<found_sections.size(); ii++)
							{
								SendPMTSection(found_sections[ii].section, found_sections[ii].length);
							}
							m_sectionParser.ResetFoundSections(found_sections);
						}
					}

					cur += TS_PACKET_SIZE;
					start = cur;
				}
				else
				{
					cur += TS_PACKET_SIZE;
					to_send += TS_PACKET_SIZE;
				}
			}

			if (to_send)
			{
				m_cb_func(start, to_send, m_param);
			}
		}
    }
}

void dvblink::engine::CTSAudioSelector::ProcessPATSection(unsigned char* pat_buffer, int pat_len)
{
    dvblink::engine::CTSPatInfo patinfo;
    patinfo.Init(pat_buffer, pat_len);

    std::vector<SPatProg> services;
    patinfo.GetServices(services);
    for (unsigned int i=0; i<services.size(); i++)
    {
        unsigned short service_num = HILO_GET(services[i].program_number);
        if (service_num != 0)
        {
            m_pmt_pid = HILO_GET(services[i].network_pid);
            m_sectionParser.Init(m_pmt_pid);
            m_state = ES_PMT;
            break;
        }
    }
}

void dvblink::engine::CTSAudioSelector::ProcessPMTSection(CTSPmtInfo& pmtinfo)
{
    //extract es info from the section
    std::vector<SESInfo> streams;
    pmtinfo.GetStreams(streams);
    
	//get all synonyms of a language code
	std::vector<std::string> lang_to_check;
	dvblink::engine::GetISO_639_3_Synonyms(m_lng.c_str(), lang_to_check);
    
    //select only one audio stream that matches required language
    int streamIdx = -1;
    int firstAudioStreamIdx = -1;
    for (unsigned int i=0; i<streams.size(); i++)
    {
        std::vector<TCA_PMT_CA_DESC> vector_desc;
        ts_process_routines::DescriptorsLinearToArray(streams[i].descriptors, vector_desc);
        
        //check stream type, but only if required language string is not empty
        if (m_lng.size() != 0 && ts_process_routines::IsAudioStream(streams[i].es_header.stream_type, vector_desc))
        {
            if (streamIdx == -1)
            {
                if (firstAudioStreamIdx == -1)
                    firstAudioStreamIdx = i;

                if (streams[i].descriptors.length > 0)
                {
                    //find language descriptor
                    unsigned char* lang_desc = dvblink::engine::ts_process_routines::FindDescriptor(streams[i].descriptors.data, 
                        streams[i].descriptors.length, ISO639_LANG_DESC_TAG);
                    if (lang_desc != NULL)
                    {
                        SDescriptorHeader* ld = (SDescriptorHeader*)lang_desc;
                        std::string lng;
                        lng.assign(reinterpret_cast<char*>(lang_desc + sizeof(SDescriptorHeader)), min(static_cast<u_char>(3), ld->descriptor_length));

						for (unsigned int j=0; j<lang_to_check.size() && streamIdx == -1; j++)
						{
							if (boost::iequals(lng, lang_to_check[j]))
							{
								streamIdx = i;
							}
						}
                    }
                }
            }
			//first - exclude all audio packets by default (we will remove required once later)
			pid_exclude_list_[HILO_GET(streams[i].es_header.elementary_PID)] = HILO_GET(streams[i].es_header.elementary_PID);
        }

    }
    //add found audio stream
    if (streamIdx != -1)
    {
		pid_exclude_list_.erase(HILO_GET(streams[streamIdx].es_header.elementary_PID));
    }
    else
    {
        //add first found audio stream
        if (firstAudioStreamIdx != -1)
        {
			pid_exclude_list_.erase(HILO_GET(streams[firstAudioStreamIdx].es_header.elementary_PID));
        }
    }
    //clean up streams info structure
    dvblink::engine::CTSPmtInfo::ClearESInfoVector(streams);
}

void dvblink::engine::CTSAudioSelector::SendPMTSection(unsigned char* pmt_buffer, int pmt_len)
{
    dvblink::engine::CTSPmtInfo pmtinfo;
    pmtinfo.Init(pmt_buffer, pmt_len);

    //extract es info from the section
    std::vector<SESInfo> streams;
    pmtinfo.GetStreams(streams);
    //make new pmt with all PIDs except for the excluded ones
    std::vector<SESInfo> newStreams;
    for (unsigned int i=0; i<streams.size(); i++)
    {
		unsigned short pid = HILO_GET(streams[i].es_header.elementary_PID);
		if (pid_exclude_list_.find(pid) == pid_exclude_list_.end())
			newStreams.push_back(streams[i]);
	}
    //create new PMT
    SPmt* pmt = (SPmt*)pmt_buffer;
    SDataChunk ext_descriptors;
    pmtinfo.GetExternalDescriptors(ext_descriptors);
    int new_pmt_len;
    unsigned char* new_pmt = m_packetGenerator.CreatePMTSection(new_pmt_len, pmt, &ext_descriptors, newStreams);
    //send new PMT down the stream
    m_packetGenerator.SplitAndSendSectionBuffer(new_pmt, new_pmt_len, &m_contCounter, m_pmt_pid, m_cb_func, m_param);
    //clean up streams info structure
    dvblink::engine::CTSPmtInfo::ClearESInfoVector(streams);
	//clean up external descriptors
	if (ext_descriptors.data != NULL)
		delete ext_descriptors.data;
}
