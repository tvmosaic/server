/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <vector>
#include <fstream>
#include <cstring>
#include <dl_md5.h>

#define S11 7
#define S12 12
#define S13 17
#define S14 22
#define S21 5
#define S22 9
#define S23 14
#define S24 20
#define S31 4
#define S32 11
#define S33 16
#define S34 23
#define S41 6
#define S42 10
#define S43 15
#define S44 21

#define ROTL(x, n) (((x) << (n)) | ((x) >> (32 - (n))))

#define FF(a, b, c, d, x, s, ac)        \
    a += ((b & c) | (~b & d)) + x + ac; \
    a = ROTL(a, s) + b;

#define GG(a, b, c, d, x, s, ac)        \
    a += ((b & d) | (c & ~d)) + x + ac; \
    a = ROTL(a, s) + b;

#define HH(a, b, c, d, x, s, ac)        \
    a += (b ^ c ^ d) + x + ac;          \
    a = ROTL(a, s) + b;

#define II(a, b, c, d, x, s, ac)        \
    a += (c ^ (b | ~d)) + x + ac;       \
    a = ROTL(a, s) + b;

#define BYTE0(x) (boost::uint8_t)((boost::uint32_t)((x      ) & 0xFF))
#define BYTE1(x) (boost::uint8_t)((boost::uint32_t)((x >>  8) & 0xFF))
#define BYTE2(x) (boost::uint8_t)((boost::uint32_t)((x >> 16) & 0xFF))
#define BYTE3(x) (boost::uint8_t)((boost::uint32_t)((x >> 24) & 0xFF))

// MD5 basic transformation
//
static void _transform(boost::uint8_t* block, boost::uint32_t* pstate)
{
    // convert 64 bytes of input to big-endian uint32_t array
    //
    int i = 0;
    boost::uint32_t x[16];

    boost::uint32_t a = *(pstate + 0);
    boost::uint32_t b = *(pstate + 1);
    boost::uint32_t c = *(pstate + 2);
    boost::uint32_t d = *(pstate + 3);

    for (int j = 0; j < 64; j += 4)
    {
        x[i++] = ((boost::uint32_t)block[j + 0]) |   
            (((boost::uint32_t)block[j + 1]) <<  8) |
            (((boost::uint32_t)block[j + 2]) << 16) |
            (((boost::uint32_t)block[j + 3]) << 24);
    }

    // Round 1
    FF(a, b, c, d, x[ 0], S11, 0xD76AA478); // 1
    FF(d, a, b, c, x[ 1], S12, 0xE8C7B756); // 2
    FF(c, d, a, b, x[ 2], S13, 0x242070DB); // 3
    FF(b, c, d, a, x[ 3], S14, 0xC1BDCEEE); // 4
    FF(a, b, c, d, x[ 4], S11, 0xF57C0FAF); // 5
    FF(d, a, b, c, x[ 5], S12, 0x4787C62A); // 6
    FF(c, d, a, b, x[ 6], S13, 0xA8304613); // 7
    FF(b, c, d, a, x[ 7], S14, 0xFD469501); // 8
    FF(a, b, c, d, x[ 8], S11, 0x698098D8); // 9
    FF(d, a, b, c, x[ 9], S12, 0x8B44F7AF); // 10
    FF(c, d, a, b, x[10], S13, 0xFFFF5BB1); // 11
    FF(b, c, d, a, x[11], S14, 0x895CD7BE); // 12
    FF(a, b, c, d, x[12], S11, 0x6B901122); // 13
    FF(d, a, b, c, x[13], S12, 0xFD987193); // 14
    FF(c, d, a, b, x[14], S13, 0xA679438E); // 15
    FF(b, c, d, a, x[15], S14, 0x49B40821); // 16

    // Round 2
    GG(a, b, c, d, x[ 1], S21, 0xF61E2562); // 17
    GG(d, a, b, c, x[ 6], S22, 0xC040B340); // 18
    GG(c, d, a, b, x[11], S23, 0x265E5A51); // 19
    GG(b, c, d, a, x[ 0], S24, 0xE9B6C7AA); // 20
    GG(a, b, c, d, x[ 5], S21, 0xD62F105D); // 21
    GG(d, a, b, c, x[10], S22, 0x02441453); // 22
    GG(c, d, a, b, x[15], S23, 0xD8A1E681); // 23
    GG(b, c, d, a, x[ 4], S24, 0xE7D3FBC8); // 24
    GG(a, b, c, d, x[ 9], S21, 0x21E1CDE6); // 25
    GG(d, a, b, c, x[14], S22, 0xC33707D6); // 26
    GG(c, d, a, b, x[ 3], S23, 0xF4D50D87); // 27
    GG(b, c, d, a, x[ 8], S24, 0x455A14ED); // 28
    GG(a, b, c, d, x[13], S21, 0xA9E3E905); // 29
    GG(d, a, b, c, x[ 2], S22, 0xFCEFA3F8); // 30
    GG(c, d, a, b, x[ 7], S23, 0x676F02D9); // 31
    GG(b, c, d, a, x[12], S24, 0x8D2A4C8A); // 32

    // Round 3
    HH(a, b, c, d, x[ 5], S31, 0xFFFA3942); // 33
    HH(d, a, b, c, x[ 8], S32, 0x8771F681); // 34
    HH(c, d, a, b, x[11], S33, 0x6D9D6122); // 35
    HH(b, c, d, a, x[14], S34, 0xFDE5380C); // 36
    HH(a, b, c, d, x[ 1], S31, 0xA4BEEA44); // 37
    HH(d, a, b, c, x[ 4], S32, 0x4BDECFA9); // 38
    HH(c, d, a, b, x[ 7], S33, 0xF6BB4B60); // 39
    HH(b, c, d, a, x[10], S34, 0xBEBFBC70); // 40
    HH(a, b, c, d, x[13], S31, 0x289B7EC6); // 41
    HH(d, a, b, c, x[ 0], S32, 0xEAA127FA); // 42
    HH(c, d, a, b, x[ 3], S33, 0xD4EF3085); // 43
    HH(b, c, d, a, x[ 6], S34, 0x04881D05); // 44
    HH(a, b, c, d, x[ 9], S31, 0xD9D4D039); // 45
    HH(d, a, b, c, x[12], S32, 0xE6DB99E5); // 46
    HH(c, d, a, b, x[15], S33, 0x1FA27CF8); // 47
    HH(b, c, d, a, x[ 2], S34, 0xC4AC5665); // 48

    // Round 4
    II(a, b, c, d, x[ 0], S41, 0xF4292244); // 49
    II(d, a, b, c, x[ 7], S42, 0x432AFF97); // 50
    II(c, d, a, b, x[14], S43, 0xAB9423A7); // 51
    II(b, c, d, a, x[ 5], S44, 0xFC93A039); // 52
    II(a, b, c, d, x[12], S41, 0x655B59C3); // 53
    II(d, a, b, c, x[ 3], S42, 0x8F0CCC92); // 54
    II(c, d, a, b, x[10], S43, 0xFFEFF47D); // 55
    II(b, c, d, a, x[ 1], S44, 0x85845DD1); // 56
    II(a, b, c, d, x[ 8], S41, 0x6FA87E4F); // 57
    II(d, a, b, c, x[15], S42, 0xFE2CE6E0); // 58
    II(c, d, a, b, x[ 6], S43, 0xA3014314); // 59
    II(b, c, d, a, x[13], S44, 0x4E0811A1); // 60
    II(a, b, c, d, x[ 4], S41, 0xF7537E82); // 61
    II(d, a, b, c, x[11], S42, 0xBD3AF235); // 62
    II(c, d, a, b, x[ 2], S43, 0x2AD7D2BB); // 63
    II(b, c, d, a, x[ 9], S44, 0xEB86D391); // 64

    *(pstate + 0) += a;
    *(pstate + 1) += b;
    *(pstate + 2) += c;
    *(pstate + 3) += d;
}

namespace dvblink {

md5::md5()
{
    reset();
}

md5::md5(const void* data, size_t size)
{
    reset();
    update(data, size);
}

md5::md5(const std::string& data)
{
    reset();
    update(data);
}

void md5::reset()
{
    state_[0] = 0x67452301;
    state_[1] = 0xEFCDAB89;
    state_[2] = 0x98BADCFE;
    state_[3] = 0x10325476;

    count_[0] = 0;
    count_[1] = 0;
    
    memset(buffer_, 0, 64);
    hash_.assign(0);

    finished_ = false;
}

// MD5 block update operation. Continues an MD5 message-digest operation,
// processing another message block, and updating the context.
//
bool md5::update(const void* data, size_t size)
{
    if (finished_)
    {
        return false;
    }

    if (!data || (size == 0))
    {
        return true;
    }

    // compute number of bytes mod 64
    unsigned buf_index = ((count_[0] >> 3) & 0x3F);

    // update number of bits
    boost::uint32_t bits = (boost::uint32_t)(size << 3);

    count_[0] += bits;

    if (count_[0] < bits)
    {
        count_[1]++;
    }

    count_[1] += static_cast<boost::uint32_t>(size >> 29);

    // how much space is left in buffer?
    unsigned buf_space = 64 - buf_index;
    unsigned input_index = 0;

    // transform as many times as possible
    if (size >= buf_space)
    { 
        // fill the rest of the buffer and transform
        memcpy(buffer_ + buf_index, data, buf_space);

        _transform(buffer_, state_);

        // transform each 64-byte piece of the input, bypassing the buffer
        for (input_index = buf_space; ; input_index += 64)
        {
            if ((input_index + 63) >= size)
            {
                break;
            }

            _transform((boost::uint8_t*)data + input_index, state_);
        }

        // so we can buffer remaining
        buf_index = 0;
    }

    // and here we do the buffering
    memcpy(buffer_ + buf_index, (boost::uint8_t*)data + input_index,
        size - input_index);

    return true;
}

bool md5::update(const std::string& data)
{
    return update(data.c_str(), data.size());
}

// MD5 finalization. Ends an MD5 message-digest operation and writing the
// the message digest.
//
const md5::hash_t& md5::operator()()
{
    if (!finished_)
    {
        boost::uint8_t bits[8];
        boost::uint8_t padding[64];
        boost::uint8_t* p = bits;

        memset(padding, 0, 64);
        padding[0] = 0x80;

        // save number of bits
        for (int i = 0; i < 2; i++)
        {
            boost::uint32_t dw = count_[i];

            *p++ = BYTE0(dw);
            *p++ = BYTE1(dw);
            *p++ = BYTE2(dw);
            *p++ = BYTE3(dw);
        }

        // pad out to 56 mod 64
        int index = (int)((count_[0] >> 3) & 0x3F);
        int pad_len = (index < 56) ? (56 - index) : (120 - index);

        update(padding, pad_len);

        // append length (before padding)
        update(bits, 8);

        // store state in digest
        p = hash_.c_array();

        for (int i = 0; i < 4; i++)
        {
            boost::uint32_t dw = state_[i];

            *p++ = BYTE0(dw);
            *p++ = BYTE1(dw);
            *p++ = BYTE2(dw);
            *p++ = BYTE3(dw);
        }

        finished_ = true;
    }

    return hash_;
}

std::string to_string(const md5::hash_t& hash)
{
    std::string s(32, '\0');    
    sprintf(&s[0],
        "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
        hash[0], hash[1], hash[2], hash[3], 
        hash[4], hash[5], hash[6], hash[7], 
        hash[8], hash[9], hash[10], hash[11], 
        hash[12], hash[13], hash[14], hash[15]);
    return s;
}

} // namespace dvblink

// $Id: md5.cpp 8705 2013-07-18 19:12:31Z mike $
