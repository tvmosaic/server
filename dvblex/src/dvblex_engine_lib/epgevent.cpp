/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <map>
#include <algorithm>
#include <sstream>
#include <cstring>
#include <dl_common.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_epgevent.h>
#include <dl_pugixml_helper.h>

namespace dvblink { namespace engine {

bool EPGItemStartTimeComp(const DLEPGEvent& first_item, const DLEPGEvent& second_item)
{
	return first_item.m_StartTime < second_item.m_StartTime;
}

void PostProcessEPGEvents(DLEPGEventList& epgEventList)
{
    //find and remove duplicate events (e.g. those that start on the same time
    DLEPGEventList new_epgEventList;
    std::map<boost::int64_t, boost::int64_t> event_start_map;
	DLEPGEventList::iterator it = epgEventList.begin();
	while (it != epgEventList.end())
	{
        if (event_start_map.find(it->m_StartTime) == event_start_map.end())
        {
            event_start_map[it->m_StartTime] = it->m_StartTime;
            new_epgEventList.push_back(*it);
        }
        it++;
    }
    if (epgEventList.size() != new_epgEventList.size())
    {
        //swap the contents
        epgEventList.clear();
        epgEventList = new_epgEventList;
    }
	//sort EPG array on start time
	std::sort(epgEventList.begin(), epgEventList.end(), EPGItemStartTimeComp);
	//walk through array and update duration if needed
	it = epgEventList.begin();
	while (it != epgEventList.end())
	{
		DLEPGEventList::iterator it_next = it + 1;
		if (it_next != epgEventList.end())
		{
            //check for 0 duration
			if (it->m_Duration == 0 && it_next->m_StartTime > it->m_StartTime)
				it->m_Duration = it_next->m_StartTime - it->m_StartTime;
            //check for overlapping programs
            if (it->m_StartTime + it->m_Duration > it_next->m_StartTime)
				it->m_Duration = it_next->m_StartTime - it->m_StartTime;
		}
        else
		{
			if (it->m_Duration == 0)
				it->m_Duration = 3600*3;
		}
		it++;
	}
}

void serialize(const DLEPGEvent& event, pugi::xml_node& program_node)
{
    std::string str;
    //id
    if (event.id_.size() > 0)
        pugixml_helpers::add_node_attribute(program_node, EPG_PROGRAM_ID, event.id_);
    //name
    str = event.m_Name;
    RemoveIllegalXMLCharacters(str);
    pugixml_helpers::add_node_attribute(program_node, EPG_PROGRAM_NAME, str);
    //description
    if (event.m_ShortDesc.size() > 0)
    {
        str = event.m_ShortDesc;
        RemoveIllegalXMLCharacters(str);
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_SHORT_DESC, str);
    }
    //start time
    std::stringstream wBuf;
    wBuf << event.m_StartTime;
    pugixml_helpers::new_child(program_node, EPG_PROGRAM_START_TIME, wBuf.str());
    //duration
    wBuf.clear(); wBuf.str("");
    wBuf << event.m_Duration;
    pugixml_helpers::new_child(program_node, EPG_PROGRAM_DURATION, wBuf.str());
    //subname
    if (event.m_SecondName.size() > 0)
    {
        str = event.m_SecondName;
        RemoveIllegalXMLCharacters(str);
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_SUBNAME, str);
    }
    //language
    if (event.m_Language.size() > 0)
    {
        str = event.m_Language;
        RemoveIllegalXMLCharacters(str);
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_LANGUAGE, str);
    }
    //Actors
    if (event.m_Actors.size() > 0)
    {
        str = event.m_Actors;
        RemoveIllegalXMLCharacters(str);
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_ACTORS, str);
    }
    //Directors
    if (event.m_Directors.size() > 0)
    {
        str = event.m_Directors;
        RemoveIllegalXMLCharacters(str);
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_DIRECTORS, str);
    }
    //Writers
    if (event.m_Writers.size() > 0)
    {
        str = event.m_Writers;
        RemoveIllegalXMLCharacters(str);
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_WRITERS, str);
    }
    //Producers
    if (event.m_Producers.size() > 0)
    {
        str = event.m_Producers;
        RemoveIllegalXMLCharacters(str);
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_PRODUCERS, str);
    }
    //Guests
    if (event.m_Guests.size() > 0)
    {
        str = event.m_Guests;
        RemoveIllegalXMLCharacters(str);
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_GUESTS, str);
    }
    //Image URL
    if (event.m_ImageURL.size() > 0)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_IMAGE, event.m_ImageURL);
    //Year
    if (event.m_Year > 0)
    {
        wBuf.clear(); wBuf.str("");
        wBuf << event.m_Year;
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_YEAR, wBuf.str());
    }
    //Episode number
    if (event.m_EpisodeNum > 0)
    {
        wBuf.clear(); wBuf.str("");
        wBuf << event.m_EpisodeNum;
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_EPISODE_NUM, wBuf.str());
    }
    //Season number
    if (event.m_SeasonNum > 0)
    {
        wBuf.clear(); wBuf.str("");
        wBuf << event.m_SeasonNum;
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_SEASON_NUM, wBuf.str());
    }
    //Star rating
    if (event.m_StarNum > 0 && event.m_StarNumMax > 0)
    {
        wBuf.clear(); wBuf.str("");
        wBuf << event.m_StarNum;
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_STARS_NUM, wBuf.str());
        wBuf.clear(); wBuf.str("");
        wBuf << event.m_StarNumMax;
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_STARSMAX_NUM, wBuf.str());
    }
    //HDTV
    if (event.m_IsHDTV)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_HDTV, "");
    //Premiere
    if (event.m_IsPremiere)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_PREMIERE, "");
    //Repeat
    if (event.m_IsRepeatFlag)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_REPEAT, "");
    //keywords
    pugixml_helpers::new_child(program_node, EPG_PROGRAM_CATEGORIES, event.m_Categories);
    //Action
    if (event.m_IsAction)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_ACTION, "");
    //Comedy
    if (event.m_IsComedy)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_COMEDY, "");
    //Documentary
    if (event.m_IsDocumentary)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_DOCUMENTARY, "");
    //Drama
    if (event.m_IsDrama)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_DRAMA, "");
    //Educational
    if (event.m_IsEducational)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_EDUCATIONAL, "");
    //Horror
    if (event.m_IsHorror)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_HORROR, "");
    //Kids
    if (event.m_IsKids)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_KIDS, "");
    //Movie
    if (event.m_IsMovie)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_MOVIE, "");
    //Music
    if (event.m_IsMusic)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_MUSIC, "");
    //News
    if (event.m_IsNews)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_NEWS, "");
    //Reality
    if (event.m_IsReality)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_REALITY, "");
    //Romance
    if (event.m_IsRomance)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_ROMANCE, "");
    //Science Fiction
    if (event.m_IsScienceFiction)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_SCIFI, "");
    //Serial
    if (event.m_IsSerial)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_SERIAL, "");
    //Soap
    if (event.m_IsSoap)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_SOAP, "");
    //Special
    if (event.m_IsSpecial)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_SPECIAL, "");
    //Sports
    if (event.m_IsSports)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_SPORTS, "");
    //Thriller
    if (event.m_IsThriller)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_THRILLER, "");
    //Adult
    if (event.m_IsAdult)
        pugixml_helpers::new_child(program_node, EPG_PROGRAM_CAT_ADULT, "");
}

void deserialize(const pugi::xml_node& program_node, DLEPGEvent& epg_event)
{
    pugixml_helpers::get_node_attribute(program_node, EPG_PROGRAM_ID, epg_event.id_);
    pugixml_helpers::get_node_attribute(program_node, EPG_PROGRAM_NAME, epg_event.m_Name);
    pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_SHORT_DESC, epg_event.m_ShortDesc);

    std::string str;
    if (pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_START_TIME, str))
        epg_event.m_StartTime = atol(str.c_str());

    if (pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_DURATION, str))
        epg_event.m_Duration = atol(str.c_str());

    pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_SUBNAME, epg_event.m_SecondName);
    pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_LANGUAGE, epg_event.m_Language);
    pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_ACTORS, epg_event.m_Actors);
    pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_DIRECTORS, epg_event.m_Directors);
    pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_WRITERS, epg_event.m_Writers);
    pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_PRODUCERS, epg_event.m_Producers);
    pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_GUESTS, epg_event.m_Guests);
    pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_IMAGE, epg_event.m_ImageURL);

    if (pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_YEAR, str))
        epg_event.m_Year = atol(str.c_str());

    if (pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_EPISODE_NUM, str))
        epg_event.m_EpisodeNum = atol(str.c_str());

    if (pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_SEASON_NUM, str))
        epg_event.m_SeasonNum = atol(str.c_str());

    if (pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_STARS_NUM, str))
        epg_event.m_StarNum = atol(str.c_str());

    if (pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_STARSMAX_NUM, str))
        epg_event.m_StarNumMax = atol(str.c_str());

    if (program_node.child(EPG_PROGRAM_HDTV) != NULL)
        epg_event.m_IsHDTV = true;

    if (program_node.child(EPG_PROGRAM_PREMIERE) != NULL)
        epg_event.m_IsPremiere = true;

    if (program_node.child(EPG_PROGRAM_REPEAT) != NULL)
        epg_event.m_IsRepeatFlag = true;

    pugixml_helpers::get_node_value(program_node, EPG_PROGRAM_CATEGORIES, epg_event.m_Categories);

    if (program_node.child(EPG_PROGRAM_CAT_ACTION) != NULL)
        epg_event.m_IsAction = true;

    if (program_node.child(EPG_PROGRAM_CAT_COMEDY) != NULL)
        epg_event.m_IsComedy = true;

    if (program_node.child(EPG_PROGRAM_CAT_DOCUMENTARY) != NULL)
        epg_event.m_IsDocumentary = true;

    if (program_node.child(EPG_PROGRAM_CAT_DRAMA) != NULL)
        epg_event.m_IsDrama = true;

    if (program_node.child(EPG_PROGRAM_CAT_EDUCATIONAL) != NULL)
        epg_event.m_IsEducational = true;

    if (program_node.child(EPG_PROGRAM_CAT_HORROR) != NULL)
        epg_event.m_IsHorror = true;

    if (program_node.child(EPG_PROGRAM_CAT_KIDS) != NULL)
        epg_event.m_IsKids = true;

    if (program_node.child(EPG_PROGRAM_CAT_MOVIE) != NULL)
        epg_event.m_IsMovie = true;

    if (program_node.child(EPG_PROGRAM_CAT_MUSIC) != NULL)
        epg_event.m_IsMusic = true;

    if (program_node.child(EPG_PROGRAM_CAT_NEWS) != NULL)
        epg_event.m_IsNews = true;

    if (program_node.child(EPG_PROGRAM_CAT_REALITY) != NULL)
        epg_event.m_IsReality = true;

    if (program_node.child(EPG_PROGRAM_CAT_ROMANCE) != NULL)
        epg_event.m_IsRomance = true;

    if (program_node.child(EPG_PROGRAM_CAT_SCIFI) != NULL)
        epg_event.m_IsScienceFiction = true;

    if (program_node.child(EPG_PROGRAM_CAT_SERIAL) != NULL)
        epg_event.m_IsSerial = true;

    if (program_node.child(EPG_PROGRAM_CAT_SOAP) != NULL)
        epg_event.m_IsSoap = true;

    if (program_node.child(EPG_PROGRAM_CAT_SPECIAL) != NULL)
        epg_event.m_IsSpecial = true;

    if (program_node.child(EPG_PROGRAM_CAT_SPORTS) != NULL)
        epg_event.m_IsSports = true;

    if (program_node.child(EPG_PROGRAM_CAT_THRILLER) != NULL)
        epg_event.m_IsThriller = true;

    if (program_node.child(EPG_PROGRAM_CAT_ADULT) != NULL)
        epg_event.m_IsAdult = true;
}

void EPGAddEventsToXMLDocument(const DLEPGEventList& eventList, pugi::xml_node& root_node)
{
    for (size_t i = 0; i < eventList.size(); i++)
    {
        pugi::xml_node program_node = pugixml_helpers::new_child(root_node, EPG_PROGRAM);

        if (program_node != NULL)
            serialize(eventList[i], program_node);
    }
}

bool EPGWriteEventsToXML(const DLEPGEventList& epgEventList, std::string& programs_xml)
{
    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(EPG_ROOT_NODE);
    if (root_node != NULL)
    {
        EPGAddEventsToXMLDocument(epgEventList, root_node);
        pugixml_helpers::xmldoc_dump_to_string(doc, programs_xml);
    }
    return true;
}

bool EPGReadEventsFromXMLDocument(const pugi::xml_node& root_node, DLEPGEventList& epgEventList)
{
    bool ret_val = false;

    epgEventList.clear();

    if (root_node != NULL)
    {
        pugi::xml_node program_node = root_node.first_child();
        while (program_node != NULL)
        {
            if (boost::iequals(program_node.name(), EPG_PROGRAM))
            {
                DLEPGEvent epg_event;
                deserialize(program_node, epg_event);
                epgEventList.push_back(epg_event);
            }
	        program_node = program_node.next_sibling();
        }

        PostProcessEPGEvents(epgEventList);
        ret_val = true;
    }

    return ret_val;
}

bool EPGReadEventsFromXML(const std::string& programs_xml, DLEPGEventList& epgEventList)
{
    bool ret_val = false;
    
    pugi::xml_document doc;
    if (doc.load_buffer(programs_xml.c_str(), programs_xml.length()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        ret_val = EPGReadEventsFromXMLDocument(root_node, epgEventList);
    }

    return ret_val;
}

bool EPGWriteEventsToFile(DLEPGEventList& epgEventList, const dvblink::filesystem_path_t& fname)
{
    bool ret_val = false;

    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(EPG_ROOT_NODE);
    if (root_node != NULL)
    {
        EPGAddEventsToXMLDocument(epgEventList, root_node);
        ret_val = pugixml_helpers::xmldoc_dump_to_file(doc, fname.to_string());
    }
    return ret_val;
}

bool EPGReadEventsFromFile(const dvblink::filesystem_path_t& fname, DLEPGEventList& epgEventList)
{
    bool ret_val = false;
    
    pugi::xml_document doc;
    if (doc.load_file(fname.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
            ret_val = EPGReadEventsFromXMLDocument(root_node, epgEventList);
    }

    return ret_val;
}

}
}
