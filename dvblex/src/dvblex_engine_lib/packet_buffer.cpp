/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <algorithm>
#include <string.h>
#include <dl_packet_buffer.h>
#include <dl_ts.h>

namespace dvblink { namespace engine {

ts_packet_buffer::ts_packet_buffer(int maxPacketNum) :
    m_PacketBuffer(NULL),
    m_PacketsNum(0),
    m_PacketsMax(maxPacketNum),
    m_BufferLength(m_PacketsMax * TS_PACKET_SIZE)
{
    m_PacketBuffer = new unsigned char[m_BufferLength];
}

ts_packet_buffer::~ts_packet_buffer()
{
	delete [] m_PacketBuffer;
}

bool ts_packet_buffer::AddPacket(const unsigned char* buffer)
{
	bool ret_val = false;
	if (m_PacketsNum < m_PacketsMax)
	{
		memcpy(m_PacketBuffer + m_PacketsNum * TS_PACKET_SIZE, buffer, TS_PACKET_SIZE);
		ret_val = (++m_PacketsNum == m_PacketsMax);
	}
	return ret_val;
}

bool ts_packet_buffer::AddPackets(const unsigned char* buffer, int length, int& added_length)
{
	using namespace std;
	bool ret_val = false;

	added_length =  m_PacketsNum < m_PacketsMax ? min((m_PacketsMax - m_PacketsNum)*TS_PACKET_SIZE, length - (length % TS_PACKET_SIZE)) : 0;

	if (added_length > 0)
	{
		memcpy(m_PacketBuffer + m_PacketsNum * TS_PACKET_SIZE, buffer, added_length);
		m_PacketsNum += added_length / TS_PACKET_SIZE;
		ret_val = (m_PacketsNum == m_PacketsMax);
	}
	return ret_val;
}

void ts_packet_buffer::Reset()
{
	m_PacketsNum = 0;
}

unsigned char* ts_packet_buffer::GetPacketBuffer(int& packet_num)
{
	packet_num = m_PacketsNum;
	return m_PacketBuffer;
}

}}
