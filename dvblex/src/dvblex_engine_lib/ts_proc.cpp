/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#include <boost/thread/thread.hpp>

#include <dl_ts_proc.h>
#include <dl_ts_info.h>

using namespace dvblink::engine;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

dvblink::engine::CTSPatInfo::CTSPatInfo()
{
}

dvblink::engine::CTSPatInfo::~CTSPatInfo()
{
}

void dvblink::engine::CTSPatInfo::Init(unsigned char* pat_section, int section_len)
{
	m_pat.clear();
	m_pat.assign(pat_section, section_len);
}

void dvblink::engine::CTSPatInfo::Clear()
{
	m_pat.clear();
}

bool dvblink::engine::CTSPatInfo::GetTSID(unsigned short& TSID)
{
	if (m_pat.size() == 0)
		return false;

	SPat* pat = (SPat*)m_pat.data();

	TSID = HILO_GET(pat->transport_stream_id);

    return true;
}

bool dvblink::engine::CTSPatInfo::GetServices(std::vector<SPatProg>& services)
{
	if (m_pat.size() == 0)
		return false;

	services.clear();

    unsigned short tid;
    GetTSID(tid);

	SPat* pat = (SPat*)m_pat.data();
	int services_loop_length = HILO_GET(pat->section_length) + 3 - sizeof(SPat) - CRC_SIZE_BYTES;
	const unsigned char* service_ptr = m_pat.data() + sizeof(SPat);

	while (services_loop_length > 0)
	{
		SPatProg* service = (SPatProg*)service_ptr;
		services.push_back(*service);

		service_ptr += sizeof(SPatProg);
		services_loop_length -= sizeof(SPatProg);
	}
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

dvblink::engine::CTSPmtInfo::CTSPmtInfo()
{
}

void dvblink::engine::CTSPmtInfo::Init(unsigned char* pmt_section, int section_len)
{
	m_pmt.clear();
	m_pmt.assign(pmt_section, section_len);
}

const unsigned char* dvblink::engine::CTSPmtInfo::GetPMTPointer(size_t& len)
{
    len = m_pmt.size();
    return m_pmt.data();
}

void dvblink::engine::CTSPmtInfo::Clear()
{
	m_pmt.clear();
}

bool dvblink::engine::CTSPmtInfo::GetSectionServiceID(unsigned short& ServiceId)
{
	if (m_pmt.size() == 0)
		return false;

	SPmt* pmt = (SPmt*)m_pmt.data();
	ServiceId = HILO_GET(pmt->program_number);
    return true;
}

bool dvblink::engine::CTSPmtInfo::GetExternalDescriptors(SDataChunk& ext_descriptors)
{
	if (m_pmt.size() == 0)
		return false;

	ext_descriptors.length = 0;
	ext_descriptors.data = NULL;

	SPmt* pmt = (SPmt*)m_pmt.data();
	//get external descriptor loop length
	ext_descriptors.length = HILO_GET(pmt->program_info_length);
	if (ext_descriptors.length > 0)
	{
		ext_descriptors.data = new unsigned char[ext_descriptors.length];
		memcpy(ext_descriptors.data, m_pmt.data() + sizeof(SPmt), ext_descriptors.length);
	}
    else
	{
		ext_descriptors.length = 0;
		ext_descriptors.data = NULL;
	}
	return true;
}

bool dvblink::engine::CTSPmtInfo::GetStreams(std::vector<SESInfo>& stream_info)
{
	if (m_pmt.size() == 0)
		return false;

	stream_info.clear();

	SPmt* pmt = (SPmt*)m_pmt.data();
	//pmt section length
	unsigned short pmt_len = HILO_GET(pmt->section_length) + 3; //plus tableid and section length itself
	//get external descriptor loop length
	unsigned short es_len = HILO_GET(pmt->program_info_length);
	int es_loop_len = pmt_len - CRC_SIZE_BYTES - es_len - sizeof(SPmt); //4 is crc size
	const unsigned char* es_start_ptr = m_pmt.data() + es_len + sizeof(SPmt);
	while (es_loop_len > 0)
	{
		SESHeader* es_header = (SESHeader*)es_start_ptr;

		SESInfo es_info;
		es_info.es_header = *es_header;
		es_info.descriptors.length = HILO_GET(es_header->ES_info_length);
		if (es_info.descriptors.length > 0)
		{
			es_info.descriptors.data = new unsigned char[es_info.descriptors.length];
			memcpy(es_info.descriptors.data, es_start_ptr + sizeof(SESHeader), es_info.descriptors.length);
		}
        else
		{
			es_info.descriptors.length = 0;
			es_info.descriptors.data = NULL;
		}
		stream_info.push_back(es_info);

		es_start_ptr += sizeof(SESHeader) + es_info.descriptors.length;
		es_loop_len -= sizeof(SESHeader) + es_info.descriptors.length;
	}

	return true;
}

bool dvblink::engine::CTSPmtInfo::ClearESInfoVector(std::vector<SESInfo>& stream_info)
{
	for (unsigned int i=0; i<stream_info.size(); i++)
		ClearESInfoStruct(stream_info[i]);

	stream_info.clear();

	return true;
}

bool dvblink::engine::CTSPmtInfo::ClearESInfoStruct(SESInfo& es_info)
{
	if (es_info.descriptors.length > 0)
		delete es_info.descriptors.data;
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

dvblink::engine::CTSPmtParser::CTSPmtParser() :
	m_serviceId(-1),
	m_pmtPid(INVALID_PACKET_PID),
    m_state(ES_STOPPED)
{
	m_sectionParser = new ts_section_payload_parser();
}

dvblink::engine::CTSPmtParser::~CTSPmtParser()
{
	delete m_sectionParser;
}

void dvblink::engine::CTSPmtParser::Init(int service_id)
{
	boost::unique_lock<boost::shared_mutex> lock(m_lock);

	m_serviceId = service_id;
	m_pmtPid = INVALID_PACKET_PID;

	m_sectionParser->Init(PID_PAT);
	m_state = ES_PAT;
}

void dvblink::engine::CTSPmtParser::Term()
{
	boost::unique_lock<boost::shared_mutex> lock(m_lock);

	m_state = ES_STOPPED;
	m_pmtInfo.Clear();
	m_patInfo.Clear();
}

dvblink::engine::CTSPmtInfo* dvblink::engine::CTSPmtParser::GetPmtInfo()
{
	boost::unique_lock<boost::shared_mutex> lock(m_lock);

	if (m_state != ES_READY)
		return NULL;

	return &m_pmtInfo;
}

dvblink::engine::CTSPatInfo* dvblink::engine::CTSPmtParser::GetPatInfo()
{
	boost::unique_lock<boost::shared_mutex> lock(m_lock);

	if (m_state != ES_READY ||
		m_state != ES_PMT)
		return NULL;

	return &m_patInfo;
}

void dvblink::engine::CTSPmtParser::ProcessStream(unsigned char* buffer, int len)
{
	//if state ready or stopped - do not even take a mutex
	if (m_state == ES_STOPPED || m_state == ES_READY)
		return;

	boost::shared_lock<boost::shared_mutex> lock(m_lock, boost::try_to_lock);
    if (lock.owns_lock())
    {
		int c = len / TS_PACKET_SIZE;
		for (int i=0; i<c; i++)
		{
			unsigned char* buf = buffer + i*TS_PACKET_SIZE;
			unsigned short pid = dvblink::engine::ts_process_routines::GetPacketPID(buf);
			switch (m_state)
			{
			case ES_PAT:
				if (pid == PID_PAT)
				{
					ts_payload_parser::ts_section_list found_sections;
					if (m_sectionParser->AddPacket(buf, TS_PACKET_SIZE, found_sections) > 0)
					{
						//get PAT section
						for (unsigned int ii=0; ii<found_sections.size() && m_state == ES_PAT; ii++)
						{
							ProcessPATSection(found_sections[ii].section, found_sections[ii].length);
						}
						m_sectionParser->ResetFoundSections(found_sections);
					}
				}
				break;
			case ES_PMT:
				if (pid == m_pmtPid)
				{
					ts_payload_parser::ts_section_list found_sections;
					if (m_sectionParser->AddPacket(buf, TS_PACKET_SIZE, found_sections) > 0)
					{
						//get PMT section
						for (unsigned int ii=0; ii<found_sections.size() && m_state == ES_PMT; ii++)
						{
							ProcessPMTSection(found_sections[ii].section, found_sections[ii].length);
						}
						m_sectionParser->ResetFoundSections(found_sections);
					}
				}
				break;
			case ES_STOPPED:
			case ES_READY:
			default:
				break;
			}
		}
	}
}

void dvblink::engine::CTSPmtParser::ProcessPATSection(unsigned char* pat_buffer, int pat_len)
{
	bool bFound = false;
	m_patInfo.Init(pat_buffer, pat_len);

	std::vector<SPatProg> services;
	m_patInfo.GetServices(services);
	for (unsigned int i=0; i<services.size(); i++)
	{
		unsigned short service_num = HILO_GET(services[i].program_number);
		if (service_num != 0 &&
			(m_serviceId == -1 || (m_serviceId != -1 && service_num == m_serviceId)))
		{
			m_pmtPid = HILO_GET(services[i].network_pid);
			m_sectionParser->Init(m_pmtPid);
			m_state = ES_PMT;
			bFound = true;
			break;
		}
	}
	if (!bFound)
		m_patInfo.Clear();
}

void dvblink::engine::CTSPmtParser::ProcessPMTSection(unsigned char* pmt_buffer, int pmt_len)
{
	m_pmtInfo.Init(pmt_buffer, pmt_len);
	bool bFound = false;
	unsigned short service_id = 0;

	m_pmtInfo.GetSectionServiceID(service_id);
	if (m_serviceId == -1 || 
		(m_serviceId != -1 && service_id == m_serviceId))
	{
		m_state = ES_READY;
		bFound = true;
	}

	if (!bFound)
		m_pmtInfo.Clear();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

dvblink::engine::CTSPacketGenerator::CTSPacketGenerator()
{
}

dvblink::engine::CTSPacketGenerator::~CTSPacketGenerator()
{
}

unsigned char* dvblink::engine::CTSPacketGenerator::CreatePMTSection(int& section_length, SPmt* pmt, SDataChunk* ext_decriptors, std::vector<SESInfo>& streams)
{
	section_length = 0;
	SPmt* new_pmt = (SPmt*)m_SectionBuffer;
	//copy header
	memcpy(m_SectionBuffer, pmt, sizeof(SPmt));
	section_length += sizeof(SPmt);
	//copy external descriptors (if any)
	int ext_desc_len = HILO_GET(pmt->program_info_length);
	if (ext_desc_len > 0 && ext_desc_len == ext_decriptors->length)
	{
		memcpy(m_SectionBuffer + section_length, ext_decriptors->data, ext_decriptors->length);
		section_length += ext_decriptors->length;
	}
	//copy elementary streams info
	for (unsigned int i=0; i<streams.size(); i++)
	{
		memcpy(m_SectionBuffer + section_length, &(streams[i].es_header), sizeof(SESHeader));
		section_length += sizeof(SESHeader);

		int desc_len = HILO_GET(streams[i].es_header.ES_info_length);
		if (desc_len > 0 && desc_len == streams[i].descriptors.length)
		{
			memcpy(m_SectionBuffer + section_length, streams[i].descriptors.data, streams[i].descriptors.length);
			section_length += streams[i].descriptors.length;
		}
	}
	unsigned char* crc_ptr = m_SectionBuffer + section_length;
	int crc_len = section_length;
	//count for crc
	section_length += CRC_SIZE_BYTES;
	//update section length
	HILO_PUT(new_pmt->section_length, (section_length - 3));
	//add crc
	dvblink::engine::ts_crc_handler::GetCRCHandler()->AddCRC(m_SectionBuffer, crc_len, crc_ptr);
	return m_SectionBuffer;
}

void dvblink::engine::CTSPacketGenerator::SplitAndSendSectionBuffer(unsigned char* section_buf, int section_len, 
                                                   unsigned char* cont_counter, unsigned short pid, LP_PACKGEN_TS_SEND_FUNC send_func, void* param)
{
    //Split PMT section into TS packets and send it down the stream
    int written_packets = 0;
    int buf_offs = 0;
    while (buf_offs < section_len)
    {
        //Fill the packet with 0xFFs
        memset(m_TSSendPacket, 0xFF, TS_PACKET_SIZE);
        int offset_to_payload = 4;
        //add packet content
		STransportHeader* ts_header = (STransportHeader*)m_TSSendPacket;
		ts_header->sync_byte = SYNC_BYTE;
		HILO_PUT(ts_header->PID, pid);
        //scrambling control, adaptation field, continuity counter
		ts_header->transport_priority = 0;
		ts_header->payload_unit_start_indicator = 0;
		ts_header->transport_error_indicator = 0;
		ts_header->adaptation_field_control = 1;
		ts_header->transport_scrambling_control = 0;
		ts_header->continuity_counter = *cont_counter;
        m_TSSendPacket[3] = 0x10 | (*cont_counter & 0x0F);
        *cont_counter = static_cast<unsigned char>(ts_process_routines::GetNextContinuityCounter(*cont_counter));
        if (written_packets == 0)
        {
            //first packet
            //Set "offset to section" field
            m_TSSendPacket[4] = 0x00;
            offset_to_payload += 1;
            //set payload start indicator for the first packet
			ts_header->payload_unit_start_indicator = 1;
        }
        //copy the section data into newly created packet
        int copy_len = TS_PACKET_SIZE - offset_to_payload;
        if (section_len - buf_offs < TS_PACKET_SIZE - offset_to_payload)
        {
            copy_len = section_len - buf_offs;
        }

        memcpy(m_TSSendPacket + offset_to_payload, section_buf + buf_offs, copy_len);
        //Send the packet
        send_func(m_TSSendPacket, TS_PACKET_SIZE, param);

        written_packets += 1;
        buf_offs += copy_len;
    }
}

