/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_circle_buffer.h>
#include <dl_logger.h>
#include <string.h>

using namespace dvblink::logging;

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

namespace dvblink { namespace engine {

///*
circular_node::circular_node(size_t node_size) :
    node_size_(node_size),
    written_length_(0),
    read_offset_(0)
{
    data_.resize(node_size_);
}

size_t circular_node::write_data(const unsigned char* buffer_in, size_t len_in)
{
    size_t free_length = node_size_ - written_length_;
    size_t to_write = len_in < free_length ? len_in : free_length;

    //std::copy(buffer_in, buffer_in + to_write, data_.begin() + written_length_);
    memcpy((void*)(&data_[0] + written_length_), buffer_in, to_write);
    written_length_ += to_write;

    return to_write;
}

size_t circular_node::read_data(unsigned char* buffer_out, size_t buffer_size)
{
    size_t ready_len = written_length_ - read_offset_;
    size_t to_read = (buffer_size >= ready_len) ? ready_len : buffer_size;

    //std::copy(data_.begin() + read_offset_, data_.begin() + read_offset_ + to_read, buffer_out);
    memcpy((void*)buffer_out, &data_[0] + read_offset_, to_read);
    read_offset_ += to_read;

    return to_read;
}

///////////////////////////////////////////////////////////////////////////////

ts_circle_buffer::ts_circle_buffer(size_t node_num, size_t node_size, const wchar_t* context, const boost::filesystem::path* data_file) :
    node_size_(node_size), stream_file_(NULL), data_trigger_amount_(0), 
    write_miss_count_(0), context_(context), node_num_(node_num), max_node_num_(node_num), expansion_block_node_num_(0)
{
    for (size_t i = 0; i < node_num; ++i)
    {
        node_t node = new circular_node(node_size);
        free_queue_.push_back(node);
    }

    if (data_file)
    {
        stream_file_ = new boost::filesystem::ofstream(data_file->wstring(), std::ios::out | std::ios::binary | std::ios::trunc);
    }
}

ts_circle_buffer::~ts_circle_buffer()
{
	reset();

    while (!free_queue_.empty())
    {
        node_t node = free_queue_.front();
        free_queue_.pop_front();

        delete node;
    }

    delete stream_file_;
}

void ts_circle_buffer::set_expansion_props(size_t max_node_num, size_t expansion_block_node_num)
{
    boost::mutex::scoped_lock lock(lock_);

    max_node_num_ = max_node_num;
    expansion_block_node_num_ = expansion_block_node_num;
}

void ts_circle_buffer::reset()
{
    boost::mutex::scoped_lock lock(lock_);

    while (!busy_queue_.empty())
    {
        node_t node = busy_queue_.front();
        busy_queue_.pop_front();

        node->reset();

        free_queue_.push_back(node);
    }
}

ts_circle_buffer::node_t ts_circle_buffer::tear_node(long timeout_ms)
{
    ts_circle_buffer::node_t node = NULL;
    {
        boost::mutex::scoped_lock lock(lock_);
        if (!busy_queue_.empty())
        {
            node = busy_queue_.front();
            busy_queue_.pop_front();
            return node;
        }
    }

    if (wait_read_.wait(timeout_ms))
    {
		boost::mutex::scoped_lock lock(lock_);
		if (!busy_queue_.empty())
		{
			node = busy_queue_.front();
			busy_queue_.pop_front();
		}
		wait_read_.reset();
    }

    return node;
}

void ts_circle_buffer::put_node(ts_circle_buffer::node_t node)
{
    boost::mutex::scoped_lock lock(lock_);

    node->reset();
    free_queue_.push_back(node);
}

bool ts_circle_buffer::is_data_available(size_t size)
{
	size_t l = 0;
	std::list<node_t>::iterator it = busy_queue_.begin();
	while (it != busy_queue_.end())
	{
		l += (*it)->size_to_read();
		if (l >= size)
			break;
		++it;
	}
	return (l >= size);
}

size_t ts_circle_buffer::read_stream(long timeout_ms, size_t expected_data, unsigned char* buffer, size_t buffer_size)
{
    node_t node = NULL;
    size_t written_len = 0, read_len = 0;

    bool b_available = false;
    {
        boost::mutex::scoped_lock lock(lock_);
        b_available = is_data_available(expected_data);
        if (!b_available)
        {
            data_trigger_amount_ = expected_data;
			wait_read_.reset();
        }
    }

    if (!b_available)
    {
		if (wait_read_.wait(timeout_ms))
		{
			wait_read_.reset();
		}
    }

    {
        boost::mutex::scoped_lock lock(lock_);
		while (!busy_queue_.empty())
		{
			node = busy_queue_.front();
			read_len = node->read_data(buffer + written_len, buffer_size - written_len);
			written_len += read_len;

			if (node->size_to_read() > 0)
			{
				break;
			}

			busy_queue_.pop_front();
			node->reset();
			free_queue_.push_back(node);
		}
    }

    return written_len;
}

size_t ts_circle_buffer::write_stream(const unsigned char* buffer_in, size_t len_in)
{
    boost::mutex::scoped_lock lock(lock_);

    if (stream_file_)
    {
        stream_file_->write(reinterpret_cast<const char*>(buffer_in), len_in);
    }

    size_t written = 0;
    if (!busy_queue_.empty())
    {
        written = busy_queue_.back()->write_data(buffer_in, len_in);
    }

    while (written != len_in)
    {
        node_t node = NULL;
        if (!free_queue_.empty())
        {
            node = free_queue_.front();
            free_queue_.pop_front();

            //reset write miss counter
            write_miss_count_ = 0;
        }
        else
        if (!busy_queue_.empty())
        {
            //check if we can expand
            if (node_num_ < max_node_num_)
            {
                //we can expand the queue
                size_t extra_node_num = std::min(expansion_block_node_num_, max_node_num_ - node_num_);

                log_info(L"ts_circle_buffer::write_stream. Context %1%. Expanding by %2% nodes") % context_ % extra_node_num;

                for (size_t i = 0; i < extra_node_num; ++i)
                {
                    node_t node = new circular_node(node_size_);
                    free_queue_.push_back(node);
                }

                node_num_ += extra_node_num;

                //take new node for writing
                node = free_queue_.front();
                free_queue_.pop_front();

                //reset write miss counter
                write_miss_count_ = 0;

            } else 
            {
                //we are overwriting existing data in the queue
                if (write_miss_count_ == 0)
                {
                    log_warning(L"ts_circle_buffer::write_stream. Circle buffer overflow. Context %1%") % context_;
                    write_miss_count_ += 1;
                }

                node = busy_queue_.front();
                busy_queue_.pop_front();
                node->reset();
            }
        }
        else
        {
            assert(0);
        }

        written += node->write_data(buffer_in + written, len_in - written);
        busy_queue_.push_back(node);
    }

    if (is_data_available(data_trigger_amount_))
    {
    	wait_read_.signal();
    }

    return written;
}

} //engine
} //dvblink
