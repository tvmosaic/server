/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <cstdlib>
#include <cassert>
#include <cstdio>
#include <fstream>
#include <cmath>
#include <iostream>
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/bzip2.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_file_procedures.h>
#include <dl_tar.h>

using namespace boost::iostreams;

namespace dvblink { namespace tar {

using namespace engine;
using namespace logging;

#define ASCII_TO_NUMBER(num) ((num)-48) //Converts an ascii digit to the corresponding number (assuming it is an ASCII digit)

static boost::uint64_t decodeTarOctal(char* data, size_t size = 12) 
{
    unsigned char* currentPtr = (unsigned char*) data + size;
    boost::uint64_t sum = 0;
    boost::uint64_t currentMultiplier = 1;
    //Skip everything after the last NUL/space character
    //In some TAR archives the size field has non-trailing NULs/spaces, so this is neccessary
    unsigned char* checkPtr = currentPtr; //This is used to check where the last NUL/space char is
    for (; checkPtr >= (unsigned char*) data; checkPtr--) {
        if ((*checkPtr) == 0 || (*checkPtr) == ' ') {
            currentPtr = checkPtr - 1;
        }
    }
    for (; currentPtr >= (unsigned char*) data; currentPtr--) {
        sum += ASCII_TO_NUMBER(*currentPtr) * currentMultiplier;
        currentMultiplier *= 8;
    }
    return sum;
}

struct TARFileHeader {
    char filename[100]; //NUL-terminated
    char mode[8];
    char uid[8];
    char gid[8];
    char fileSize[12];
    char lastModification[12];
    char checksum[8];
    char typeFlag; //Also called link indicator for none-UStar format
    char linkedFileName[100];
    //USTar-specific fields -- NUL-filled in non-USTAR version
    char ustarIndicator[6]; //"ustar" -- 6th character might be NUL but results show it doesn't have to
    char ustarVersion[2]; //00
    char ownerUserName[32];
    char ownerGroupName[32];
    char deviceMajorNumber[8];
    char deviceMinorNumber[8];
    char filenamePrefix[155];
    char padding[12]; //Nothing of interest, but relevant for checksum

    /**
     * @return true if and only if
     */
    bool isUSTAR() {
        return (memcmp("ustar", ustarIndicator, 5) == 0);
    }

    /**
     * @return The filesize in bytes
     */
    boost::uint64_t getFileSize() {
        return decodeTarOctal(fileSize);
    }

    /**
     * Return true if and only if the header checksum is correct
     * @return
     */
    bool checkChecksum() {
        //We need to set the checksum to zer
        char originalChecksum[8];
        memcpy(originalChecksum, checksum, 8);
        memset(checksum, ' ', 8);
        //Calculate the checksum -- both signed and unsigned
        boost::int64_t unsignedSum = 0;
        boost::int64_t signedSum = 0;
        for (int i = 0; i < sizeof (TARFileHeader); i++) {
            unsignedSum += ((unsigned char*) this)[i];
            signedSum += ((signed char*) this)[i];
        }
        //Copy back the checksum
        memcpy(checksum, originalChecksum, 8);
        //Decode the original checksum
        boost::uint64_t referenceChecksum = decodeTarOctal(originalChecksum);
        return (referenceChecksum == unsignedSum || referenceChecksum == signedSum);
    }
};

tarfile_reader::tarfile_reader(const dvblink::filesystem_path_t& filename, tar_file_type_e tar_file_type)
    : filename_(filename), tar_file_type_(tar_file_type)
{
}

tarfile_reader::~tarfile_reader()
{
}

bool tarfile_reader::decompress(const dvblink::filesystem_path_t& destination)
{
#ifdef min
#undef min
#endif

    bool ret_val = false;

    if (tar_file_type_ == tft_auto)
    {
        //try file type autodetection
        if (boost::algorithm::iends_with(filename_.to_string(), ".gz")) {
            tar_file_type_ = tft_tar_gz;
        } else if (boost::algorithm::iends_with(filename_.to_string(), ".bz2")) {
            tar_file_type_ = tft_tar_bz2;
        } else if (boost::algorithm::iends_with(filename_.to_string(), ".tar")) {
            tar_file_type_ = tft_tar;
        }
    }

    if (tar_file_type_ != tft_auto)
    {

#ifdef WIN32
        std::ifstream fin(filename_.to_wstring().c_str(), std::ios_base::in | std::ios_base::binary);
#else
        std::ifstream fin(filename_.to_string().c_str(), std::ios_base::in | std::ios_base::binary);
#endif
        filtering_istream in;
        //Depending on the compression format, select the correct decompressor
        switch (tar_file_type_)
        {
            case tft_tar:
                //uncompressed content
                break;
            case tft_tar_gz:
                in.push(gzip_decompressor());
                break;
            case tft_tar_bz2:
              in.push(bzip2_decompressor());
                break;
            default:
                break;
        }
        in.push(fin);

        //Initialize a zero-filled block we can compare against (zero-filled header block --> end of TAR archive)
        char zeroBlock[512];
        memset(zeroBlock, 0, 512);
        //Start reading
        bool nextEntryHasLongName = false;
        while (in) 
        { 
            //Stop if end of file has been reached or any error occured
            TARFileHeader currentFileHeader;
            //Read the file header.
            in.read((char*) &currentFileHeader, 512);
            //When a block with zeroes-only is found, the TAR archive ends here
            if(memcmp(&currentFileHeader, zeroBlock, 512) == 0) 
            {
                ret_val = true;
                break;
            }

            if (!currentFileHeader.checkChecksum())
                break;

            //Uncomment this to check for USTAR if you need USTAR features
            //assert(currentFileHeader.isUSTAR());

            //Convert the filename to a std::string to make handling easier
            //Filenames of length 100+ need special handling
            // (only USTAR supports 101+-character filenames, but in non-USTAR archives the prefix is 0 and therefore ignored)
            std::string filename(currentFileHeader.filename, std::min((size_t)100, strlen(currentFileHeader.filename)));

            //block below is needed to support long filenames---
//            size_t prefixLength = strlen(currentFileHeader.filenamePrefix);
//            if(prefixLength > 0) { //If there is a filename prefix, add it to the string. See `man ustar`LON
//                filename = std::string(currentFileHeader.filenamePrefix, min((size_t)155, prefixLength)) + "/" + filename; //min limit: Not needed by spec, but we want to be safe
//            }
            if (currentFileHeader.typeFlag == '0' || currentFileHeader.typeFlag == 0) 
            { //Normal file
                //Handle GNU TAR long filenames -- the current block contains the filename only whilst the next block contains metadata
                if(nextEntryHasLongName) 
                {
                    //Set the filename from the current header
                    filename = std::string(currentFileHeader.filename);
                    //The next header contains the metadata, so replace the header before reading the metadata
                    in.read((char*) &currentFileHeader, 512);
                    //Reset the long name flag
                    nextEntryHasLongName = false;
                }
                //Now the metadata in the current file header is valie -- we can read the values.
                boost::uint64_t size = currentFileHeader.getFileSize();
                
                //In the tar archive, entire 512-byte-blocks are used for each file
                //Therefore we now have to skip the padded bytes.
                size_t paddingBytes = (512 - (size % 512)) % 512; //How long the padding to 512 bytes needs to be

                dvblink::filesystem_path_t outfile = destination / filename;
                FILE* fout = filesystem::universal_open_file(outfile, "w+b");
                if (fout != NULL)
                {
                    bool read_op_res = true;

                    const int buf_size = 512;
                    char buf[buf_size];
                    while (size > 0)
                    {
                        try 
                        {
                    	    std::streamsize to_read = size < buf_size ? size : buf_size; // min
                            in.read(buf, to_read);
                            std::streamsize read_bytes = in.gcount();
                            if (read_bytes > 0)
                            {
                                size_t n = fwrite(buf, 1, read_bytes, fout);
                                if (n != read_bytes)
                                {
                                    read_op_res = false;
                                    break;
                                }
                                size -= read_bytes;
                            } else
                            {
                                read_op_res = false;
                                break;
                            }
                        }
                        catch(std::exception e)
                        {
                            log_error(L"tarfile_reader::decompress. Exception %1%") % string_cast<EC_UTF8>(e.what());
                            read_op_res = false;
                        }
                    }
                    fclose(fout);

                    if (!read_op_res)
                    {
                        //exit on not successful file read operation
                        break;
                    }

                } else
                {
                    log_error(L"tarfile_reader::decompress. Error creating output file %1%")% outfile.to_wstring();
                    break;
                }

                //Simply ignore the padding
                in.ignore(paddingBytes);
            } else if (currentFileHeader.typeFlag == '5') 
            {
                //A directory
                dvblink::filesystem_path_t dirname = destination / filename;
                try {
                    boost::filesystem::create_directories(dirname.to_boost_filesystem());
                } catch(...){}

            } else 
            if(currentFileHeader.typeFlag == 'L') 
            {
                nextEntryHasLongName = true;
            } else 
            {
                //Neither normal file nor directory (symlink etc.) -- currently ignored silently
            }
        }
        fin.close();
    } else
    {
        log_error(L"tarfile_reader::decompress. Unsupported file type %1% (%2%)") % tar_file_type_ % filename_.to_wstring();
    }

    return ret_val;
}

}
}
