/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <dl_ts_aligner.h>
#include <cstring>

using namespace dvblink::engine;

void ts_packet_aligner::write_stream(unsigned char* buf, unsigned long len) 
{	
    if (buf && (len > 0))
    {
        if (fragment_length_ > 0 && fragment_length_ + len < TS_PACKET_SIZE)
        {
            //this is another part of the same incomplete packet
            // Important! Contents of the packet_buffer_ always starts with 0x47
            memcpy(packet_buffer_ + fragment_length_, buf, len);
            fragment_length_ += len;
            return;
        }

        unsigned long i = 0;
        if (fragment_length_ > 0 && len >= TS_PACKET_SIZE - fragment_length_)
        {
            if (len == TS_PACKET_SIZE - fragment_length_ ||
                (len > TS_PACKET_SIZE - fragment_length_ && buf[TS_PACKET_SIZE - fragment_length_] == SYNC_BYTE))
            {
                // complete the remaining packet fragment
                memcpy(&packet_buffer_[fragment_length_], buf, TS_PACKET_SIZE - fragment_length_);
                callback_(packet_buffer_, TS_PACKET_SIZE, user_param_);
                i += TS_PACKET_SIZE - fragment_length_;
            }
        }

        size_t to_send = 0;
        size_t begin = i;

        while (i + TS_PACKET_SIZE < len)
        {
            if (buf[i] == SYNC_BYTE && buf[i + TS_PACKET_SIZE] == SYNC_BYTE)
            {
                to_send += TS_PACKET_SIZE;
                i += TS_PACKET_SIZE;
            }
            else
            {
                callback_(&buf[begin], to_send, user_param_);
                i++; // single step scan for valid sync bytes
                
                begin = i;
                to_send = 0;
            }
        }

        if (to_send)
        {
             // complete packet at the end?
            if (i + TS_PACKET_SIZE == len && buf[i] == SYNC_BYTE &&
                (len >= 2 * TS_PACKET_SIZE && buf[i - TS_PACKET_SIZE] == SYNC_BYTE))
            {
                to_send += TS_PACKET_SIZE;
                i += TS_PACKET_SIZE;
            }
           callback_(&buf[begin], to_send, user_param_);
        }

        // incomplete packet at the end?
        if (i < len && i + TS_PACKET_SIZE > len && buf[i] == SYNC_BYTE)
        {
            fragment_length_ = len - i;
            memcpy(packet_buffer_, &buf[i], fragment_length_);
        }
        else
        {
            // complete packet at the end or exactly one packet in buffer?
            if (i + TS_PACKET_SIZE == len && buf[i] == SYNC_BYTE)
            {
                callback_(&buf[i], TS_PACKET_SIZE, user_param_);
                i += TS_PACKET_SIZE;
            }
            fragment_length_ = 0;
        }
    }
}
