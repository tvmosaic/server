/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_epg_channels.h>
#include <dl_pugixml_helper.h>

using namespace dvblink;

namespace dvblex {

const std::string epg_channel_channels_tag = "channels";
const std::string epg_channel_channel_tag = "channel";
const std::string epg_channel_id_tag = "id";
const std::string epg_channel_name_tag = "name";
const std::string epg_channel_num_tag = "num";
const std::string epg_channel_subnum_tag = "subnum";
const std::string epg_channel_logo_tag = "logo_url";
const std::string epg_channel_nid_tag = "nid";
const std::string epg_channel_tid_tag = "tid";
const std::string epg_channel_sid_tag = "sid";

bool write_epg_channels_to_file(const epg_source_channel_map_t& channel_map, const dvblink::filesystem_path_t& fname)
{
    bool ret_val = false;

    std::string str;

    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(epg_channel_channels_tag.c_str());
    if (root_node != NULL)
    {
        epg_source_channel_map_t::const_iterator iter = channel_map.begin();
        while (iter != channel_map.end())
        {
            pugi::xml_node channel_node = pugixml_helpers::new_child(root_node, epg_channel_channel_tag);

            if (channel_node != NULL)
            {
			    if (iter->second.nid_.get() != epg_invalid_scan_id_.get())
                {
                    str = boost::lexical_cast<std::string>(iter->second.nid_.get());
				    pugixml_helpers::add_node_attribute(channel_node, epg_channel_nid_tag, str);
                }

			    if (iter->second.tid_.get() != epg_invalid_scan_id_.get())
                {
                    str = boost::lexical_cast<std::string>(iter->second.tid_.get());
				    pugixml_helpers::add_node_attribute(channel_node, epg_channel_tid_tag, str);
                }

			    if (iter->second.sid_.get() != epg_invalid_scan_id_.get())
                {
                    str = boost::lexical_cast<std::string>(iter->second.sid_.get());
				    pugixml_helpers::add_node_attribute(channel_node, epg_channel_sid_tag, str);
                }

                pugixml_helpers::new_child(channel_node, epg_channel_id_tag, iter->second.id_.to_string());
                pugixml_helpers::new_child(channel_node, epg_channel_name_tag, iter->second.name_.to_string());

                if (iter->second.number_.get() != epg_invalid_channel_number_.get())
                {
                    str = boost::lexical_cast<std::string>(iter->second.number_.get());
                    pugixml_helpers::new_child(channel_node, epg_channel_num_tag, str);
                    
                    if (iter->second.sub_number_.get() != epg_invalid_channel_number_.get())
                    {
                        str = boost::lexical_cast<std::string>(iter->second.sub_number_.get());
                        pugixml_helpers::new_child(channel_node, epg_channel_subnum_tag, str);
                    }
                }

			    if (iter->second.logo_.get().size() > 0)
				    pugixml_helpers::new_child(channel_node, epg_channel_logo_tag, iter->second.logo_.to_string());
            }
            ++iter;
        }
        ret_val = pugixml_helpers::xmldoc_dump_to_file(doc, fname.to_string());

    }

    return ret_val;
}

bool read_epg_channels_from_file(const dvblink::filesystem_path_t& fname, epg_source_channel_map_t& channel_map)
{
    bool res = false;

    channel_map.clear();

    pugi::xml_document doc;
    if (doc.load_file(fname.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            pugi::xml_node channel_node = root_node.first_child();
            while (channel_node != NULL)
            {
                if (boost::iequals(channel_node.name(), epg_channel_channel_tag))
                {
                    epg_source_channel_t ch;
                    std::string str;
                    boost::uint16_t i16;
                    boost::int32_t i32;

                    if (pugixml_helpers::get_node_value(channel_node, epg_channel_id_tag, str))
                        ch.id_ = str;

                    if (pugixml_helpers::get_node_value(channel_node, epg_channel_name_tag, str))
                        ch.name_ = str;

                    if (pugixml_helpers::get_node_value(channel_node, epg_channel_logo_tag, str))
                        ch.logo_ = str;

                    if (pugixml_helpers::get_node_attribute(channel_node, epg_channel_nid_tag, str))
                    {
                        dvblink::engine::string_conv::apply(str.c_str(), i16, epg_invalid_scan_id_.get());
                        ch.nid_ = i16;
                    }

                    if (pugixml_helpers::get_node_attribute(channel_node, epg_channel_tid_tag, str))
                    {
                        dvblink::engine::string_conv::apply(str.c_str(), i16, epg_invalid_scan_id_.get());
                        ch.tid_ = i16;
                    }

                    if (pugixml_helpers::get_node_attribute(channel_node, epg_channel_sid_tag, str))
                    {
                        dvblink::engine::string_conv::apply(str.c_str(), i16, epg_invalid_scan_id_.get());
                        ch.sid_ = i16;
                    }

                    if (pugixml_helpers::get_node_value(channel_node, epg_channel_num_tag, str))
                    {
                        dvblink::engine::string_conv::apply(str.c_str(), i32, epg_invalid_channel_number_.get());
                        ch.number_ = i32;
                    }

                    if (pugixml_helpers::get_node_value(channel_node, epg_channel_subnum_tag, str))
                    {
                        dvblink::engine::string_conv::apply(str.c_str(), i32, epg_invalid_channel_number_.get());
                        ch.sub_number_ = i32;
                    }

                    if (ch.id_.get().size() > 0)
                        channel_map[ch.id_] = ch;
                }
                channel_node = channel_node.next_sibling();
            }
            res = true;
        }
    }

    return res;
}

}
