/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"

#ifdef WIN32

#include <boost/algorithm/string.hpp>
#include <dshow.h>
#include <ks.h>
#include <ksmedia.h>
#include <Dvdmedia.h>
#include <dl_logger.h>
#include <dl_dshow_enum.h>
#include <dl_dshow_helpers.h>

using namespace dvblink::logging;

namespace dvblink { namespace engine {

IBaseFilter* get_preferred_filter(const IID& clsid, const wchar_t* device_path, std::wstring& name)
{
	IBaseFilter* ret_val = NULL;	

	log_info(L"Looking for preferred filter for %s") % device_path;

	TDSEnum* dsenum = enum_create(clsid);
	if(dsenum != NULL)
	{
		const wchar_t* ptr = wcsrchr(device_path, L'#');
		if (ptr != NULL)
		{
			int cmp_num = ptr - device_path;

			while (enum_next(dsenum) == S_OK)
			{
				enum_get_name(dsenum);
			
				if (_wcsnicmp(device_path, dsenum->szDevicePathW, cmp_num) == 0)
				{
					log_info(L"Found preferred filter: %s") % dsenum->szDevicePathW;
					//found preferred filter
					name = dsenum->szNameW;
					if (enum_get_filter(dsenum, &ret_val) != S_OK)
						ret_val = NULL;
			            
					break;
				}
			}
		}
		enum_free(dsenum);
	}
	return ret_val;
}

IBaseFilter* get_filter_by_name(const IID& clsid, const wchar_t* name_to_find, std::wstring& name)
{
	IBaseFilter* ret_val = NULL;	

	TDSEnum* dsenum = enum_create(clsid);
	if(dsenum != NULL)
	{
		while (enum_next(dsenum) == S_OK)
		{
			enum_get_name(dsenum);
		
            if (boost::icontains(dsenum->szNameW, name_to_find))
			{
				//found preferred filter
				name = dsenum->szNameW;
				if (enum_get_filter(dsenum, &ret_val) != S_OK)
					ret_val = NULL;
		            
				break;
			}
		}
		enum_free(dsenum);
	}
	return ret_val;
}

// Release the format block for a media type.
void _FreeMediaType(AM_MEDIA_TYPE& mt)
{
    if (mt.cbFormat != 0)
    {
        CoTaskMemFree((PVOID)mt.pbFormat);
        mt.cbFormat = 0;
        mt.pbFormat = NULL;
    }
    if (mt.pUnk != NULL)
    {
        // pUnk should not be used.
        mt.pUnk->Release();
        mt.pUnk = NULL;
    }
}

// Delete a media type structure that was allocated on the heap.
void _DeleteMediaType(AM_MEDIA_TYPE *pmt)
{
    if (pmt != NULL)
    {
        _FreeMediaType(*pmt); 
        CoTaskMemFree(pmt);
    }
}

static bool check_pin_media_type(IPin* pin, const GUID* major_type, const GUID* sub_type)
{
	bool ret_val = false;
	IEnumMediaTypes* media_enum = NULL;

	HRESULT hr = pin->EnumMediaTypes(&media_enum);
	if (hr == S_OK && media_enum != NULL)
	{
		media_enum->Reset();
		AM_MEDIA_TYPE* avmt;
		while (!ret_val && media_enum->Next(1, &avmt, NULL) == S_OK)
		{
//            VIDEOINFOHEADER2* vh2 = (VIDEOINFOHEADER2*)avmt->pbFormat;
//            VIDEOINFOHEADER* vh = (VIDEOINFOHEADER*)avmt->pbFormat;
			if ((major_type == NULL || (major_type != NULL && avmt->majortype == *major_type)) &&
				(sub_type == NULL || (sub_type != NULL && avmt->subtype == *sub_type)))
			{
				 ret_val = true;
			}
			 _DeleteMediaType(avmt);
		}
		media_enum->Release();
	}

	return ret_val;
}

IPin* get_pin(IBaseFilter * pFilter, PIN_DIRECTION dir, const GUID* major_type, const GUID* sub_type)
{
	IPin* res_pin = NULL;

    IEnumPins *pEnum;
	
    HRESULT hr = pFilter->EnumPins(&pEnum);
	if (hr == S_OK)
	{
		pEnum->Reset();

		IPin *pPin = NULL;
		hr = E_FAIL;

		while(pEnum->Next(1, &pPin, NULL) == S_OK)
		{
			PIN_DIRECTION pindir;
			hr = pPin->QueryDirection(&pindir);

			if(hr == S_OK && pindir == dir)
			{
				if ((major_type == NULL && sub_type == NULL) || check_pin_media_type(pPin, major_type, sub_type))
				{
					res_pin = pPin;  // Return the pin's interface
					break;
				}
			} 

			pPin->Release();
		}

		pEnum->Release();
	}
    return res_pin;
}

IPin* get_pin_byidx(IBaseFilter * pFilter, PIN_DIRECTION dir, long idx)
{
	IPin* res_pin = NULL;

    IEnumPins *pEnum;
	
    HRESULT hr = pFilter->EnumPins(&pEnum);
	if (hr == S_OK)
	{
		pEnum->Reset();

		IPin *pPin = NULL;
		hr = E_FAIL;

		long count = 0;
		while(pEnum->Next(1, &pPin, NULL) == S_OK)
		{
			PIN_DIRECTION pindir;
			hr = pPin->QueryDirection(&pindir);

			if(hr == S_OK && pindir == dir && idx == count)
			{
				res_pin = pPin;  // Return the pin's interface
				break;
			} 

			pPin->Release();
			++count;
		}

		pEnum->Release();
	}
    return res_pin;
}

bool ConnectUpFilterToPin(IGraphBuilder *pGraph, IBaseFilter* pFilterUpstream, IPin* pPin)
{
    bool bConnected = false;
    HRESULT         hr = E_FAIL;

    IPin *pIPinUpstream;

    PIN_INFO        PinInfoUpstream;
    PIN_INFO        PinInfoDownstream;

    // grab upstream filter's enumerator
    IEnumPins *pIEnumPinsUpstream;
    hr = pFilterUpstream->EnumPins(&pIEnumPinsUpstream);

    if(FAILED(hr))
    {
        log_error(L"Cannot Enumerate Upstream Filter's Pins");
        return false;
    }

    // iterate through upstream filter's pins
    while (!bConnected && pIEnumPinsUpstream->Next(1, &pIPinUpstream, 0) == S_OK)
    {
        hr = pIPinUpstream->QueryPinInfo (&PinInfoUpstream);
        if (hr == S_OK)
        {
            IPin *pPinDown = NULL;
            pIPinUpstream->ConnectedTo(&pPinDown);

            // bail if pins are connected
            // otherwise check direction and connect
            if ((PINDIR_OUTPUT == PinInfoUpstream.dir) && (pPinDown == NULL))
            {
                // make sure that pin to connect is an input pin
                hr = pPin->QueryPinInfo(&PinInfoDownstream);
                if(SUCCEEDED(hr))
                {
                    IPin *pPinUp = NULL;

                    // Determine if the pin is already connected.  Note that 
                    // VFW_E_NOT_CONNECTED is expected if the pin isn't yet connected.
                    hr = pPin->ConnectedTo(&pPinUp);
                    if(FAILED(hr) && hr != VFW_E_NOT_CONNECTED)
                    {
                        log_error(L"FAIL: Failed in pPin->ConnectedTo()!");
                    }
                    else
                    {
                        if ((PINDIR_INPUT == PinInfoDownstream.dir) && (pPinUp == NULL))
                        {
                            if (SUCCEEDED (pGraph->Connect(pIPinUpstream, pPin)))
                                bConnected = true;
                        }
                    }
                    if (pPinUp != NULL)
                        pPinUp->Release();
                    if (PinInfoDownstream.pFilter != NULL)
                        PinInfoDownstream.pFilter->Release();
                }
            }// if output pin

            if (pPinDown != NULL)
                pPinDown->Release();
            if (PinInfoUpstream.pFilter != NULL)
                PinInfoUpstream.pFilter->Release();
        }
        else
        {
            log_error(L"Cannot Obtain Upstream Filter's PIN_INFO");
        }
        pIPinUpstream->Release();
    } // while next upstream filter pin

    pIEnumPinsUpstream->Release();

    return bConnected;
}

bool ConnectDownFilterToPin(IGraphBuilder *pGraph, IPin* pPin, IBaseFilter* pFilterDownstream)
{
    bool bConnected = false;
    HRESULT         hr = E_FAIL;

    PIN_INFO        PinInfoUpstream;
    PIN_INFO        PinInfoDownstream;

    hr = pPin->QueryPinInfo(&PinInfoUpstream);
    if (hr == S_OK)
    {
        IPin *pPinDown = NULL;
        pPin->ConnectedTo(&pPinDown);

        // bail if pins are connected
        // otherwise check direction and connect
        if ((PINDIR_OUTPUT == PinInfoUpstream.dir) && (pPinDown == NULL))
        {
            // grab downstream filter's enumerator
            IEnumPins *pIEnumPinsDownstream;
            hr = pFilterDownstream->EnumPins(&pIEnumPinsDownstream);
            if (hr == S_OK)
            {
                // iterate through downstream filter's pins
                IPin *pIPinDownstream;
                while (!bConnected && pIEnumPinsDownstream->Next (1, &pIPinDownstream, 0) == S_OK)
                {
                    // make sure it is an input pin
                    hr = pIPinDownstream->QueryPinInfo(&PinInfoDownstream);
                    if(SUCCEEDED(hr))
                    {
                        IPin *pPinUp = NULL;

                        // Determine if the pin is already connected.  Note that 
                        // VFW_E_NOT_CONNECTED is expected if the pin isn't yet connected.
                        hr = pIPinDownstream->ConnectedTo(&pPinUp);
                        if(FAILED(hr) && hr != VFW_E_NOT_CONNECTED)
                        {
                            log_error(L"FAIL: Failed in pIPinDownstream->ConnectedTo()!");
                        }
                        else
                        {
                            if ((PINDIR_INPUT == PinInfoDownstream.dir) && (pPinUp == NULL))
                            {
                                if (SUCCEEDED (pGraph->Connect(pPin, pIPinDownstream)))
                                    bConnected = true;
                            }
                        }
                        if (pPinUp != NULL)
                            pPinUp->Release();
                        if (PinInfoDownstream.pFilter != NULL)
                            PinInfoDownstream.pFilter->Release();
                    }
                    pIPinDownstream->Release();
                } // while next downstream filter pin

                pIEnumPinsDownstream->Release();
                //We are now back into the upstream pin loop
            }
            else
            {
                log_error(L"Cannot enumerate pins on downstream filter!");
            }
        }// if output pin

        if (pPinDown != NULL)
            pPinDown->Release();
        if (PinInfoUpstream.pFilter != NULL)
            PinInfoUpstream.pFilter->Release();
    }
    else
    {
        log_error(L"Cannot Obtain Upstream Filter's PIN_INFO");
    }

    return bConnected;
}

bool ConnectFilters(IGraphBuilder *pGraph, IBaseFilter* pFilterUpstream, IBaseFilter* pFilterDownstream)
{
    bool bConnected = false;
    HRESULT         hr = E_FAIL;

    IPin *pIPinUpstream;

    PIN_INFO        PinInfoUpstream;
    PIN_INFO        PinInfoDownstream;

    // grab upstream filter's enumerator
    IEnumPins *pIEnumPinsUpstream;
    hr = pFilterUpstream->EnumPins(&pIEnumPinsUpstream);

    if(FAILED(hr))
    {
        log_error(L"Cannot Enumerate Upstream Filter's Pins");
        return false;
    }

    // iterate through upstream filter's pins
    while (!bConnected && pIEnumPinsUpstream->Next(1, &pIPinUpstream, 0) == S_OK)
    {
        hr = pIPinUpstream->QueryPinInfo (&PinInfoUpstream);
        if (hr == S_OK)
        {
            IPin *pPinDown = NULL;
            pIPinUpstream->ConnectedTo(&pPinDown);

            // bail if pins are connected
            // otherwise check direction and connect
            if ((PINDIR_OUTPUT == PinInfoUpstream.dir) && (pPinDown == NULL))
            {
                // grab downstream filter's enumerator
                IEnumPins *pIEnumPinsDownstream;
                hr = pFilterDownstream->EnumPins(&pIEnumPinsDownstream);
                if (hr == S_OK)
                {
                    // iterate through downstream filter's pins
                    IPin *pIPinDownstream;
                    while (!bConnected && pIEnumPinsDownstream->Next (1, &pIPinDownstream, 0) == S_OK)
                    {
                        // make sure it is an input pin
                        hr = pIPinDownstream->QueryPinInfo(&PinInfoDownstream);
                        if(SUCCEEDED(hr))
                        {
                            IPin *pPinUp = NULL;

                            // Determine if the pin is already connected.  Note that 
                            // VFW_E_NOT_CONNECTED is expected if the pin isn't yet connected.
                            hr = pIPinDownstream->ConnectedTo(&pPinUp);
                            if(FAILED(hr) && hr != VFW_E_NOT_CONNECTED)
                            {
                                log_error(L"FAIL: Failed in pIPinDownstream->ConnectedTo()!");
                            }
                            else
                            {
                                if ((PINDIR_INPUT == PinInfoDownstream.dir) && (pPinUp == NULL))
                                {
                                    if (SUCCEEDED (pGraph->Connect(pIPinUpstream, pIPinDownstream)))
                                        bConnected = true;
                                }
                            }
                            if (pPinUp != NULL)
                                pPinUp->Release();
                            if (PinInfoDownstream.pFilter != NULL)
                                PinInfoDownstream.pFilter->Release();
                        }
                        pIPinDownstream->Release();
                    } // while next downstream filter pin

                    pIEnumPinsDownstream->Release();
                    //We are now back into the upstream pin loop
                }
                else
                {
                    log_error(L"Cannot enumerate pins on downstream filter!");
                }
            }// if output pin

            if (pPinDown != NULL)
                pPinDown->Release();
            if (PinInfoUpstream.pFilter != NULL)
                PinInfoUpstream.pFilter->Release();
        }
        else
        {
            log_error(L"Cannot Obtain Upstream Filter's PIN_INFO");
        }
        pIPinUpstream->Release();
    } // while next upstream filter pin

    pIEnumPinsUpstream->Release();

    return bConnected;
}

bool start_ds_graph(IMediaControl* pmediactrl, unsigned long timeout)
{
	HRESULT hr = E_FAIL;

	if (pmediactrl != NULL)
	{
		hr = pmediactrl->Run();
		if (hr == S_OK)
		{
			//wait until running state
			DWORD elapsed_time = 0;
			FILTER_STATE fs;
			hr = pmediactrl->GetState(1, (OAFilterState*)&fs);
			while ((hr != S_OK || fs != State_Running) && elapsed_time < timeout)
			{
				Sleep(50);
				elapsed_time += 50;
				hr = pmediactrl->GetState(1, (OAFilterState*)&fs);
			}
			log_info(L"start_graph: hr=%d, state=%d") % hr % fs;
			hr = (hr == S_OK && fs == State_Running) ? S_OK : E_FAIL;
		}
		if (hr != S_OK)
			log_warning(L"Cannot start graph, %d") % hr;
	}
	return hr == S_OK;
}

bool stop_ds_graph(IMediaControl* pmediactrl, unsigned long timeout)
{
	HRESULT hr = E_FAIL;

	if (pmediactrl != NULL)
	{
		hr = pmediactrl->Stop();
		if (hr == S_OK)
		{
			//wait until running state
			DWORD elapsed_time = 0;
			FILTER_STATE fs;
			hr = pmediactrl->GetState(1, (OAFilterState*)&fs);
			while ((hr != S_OK || fs != State_Stopped) && elapsed_time < timeout)
			{
				Sleep(50);
				elapsed_time += 50;
				hr = pmediactrl->GetState(1, (OAFilterState*)&fs);
			}
			log_info(L"stop_graph: hr=%d, state=%d") % hr % fs;
			hr = (hr == S_OK && fs == State_Stopped) ? S_OK : E_FAIL;
		}
		if (hr != S_OK)
			log_warning(L"Cannot stop graph, %d") % hr;
	}
	return hr == S_OK;
}

bool pause_ds_graph(IMediaControl* pmediactrl, unsigned long timeout)
{
	HRESULT hr = E_FAIL;

	if (pmediactrl != NULL)
	{
		hr = pmediactrl->Pause();
		if (hr == S_OK)
		{
			//wait until running state
			DWORD elapsed_time = 0;
			FILTER_STATE fs;
			hr = pmediactrl->GetState(1, (OAFilterState*)&fs);
			while ((hr != S_OK || fs != State_Paused) && elapsed_time < timeout)
			{
				Sleep(50);
				elapsed_time += 50;
				hr = pmediactrl->GetState(1, (OAFilterState*)&fs);
			}
			log_info(L"pause_graph: hr=%d, state=%d") % hr % fs;
			hr = (hr == S_OK && fs == State_Paused) ? S_OK : E_FAIL;
		}
		if (hr != S_OK)
			log_warning(L"Cannot pause graph, %d") % hr;
	}
	return hr == S_OK;
}

// Adds a DirectShow filter graph to the Running Object Table,
// allowing GraphEdit to "spy" on a remote filter graph.
bool AddGraphToRot( IUnknown *pUnkGraph, unsigned long *pdwRegister )
{
    IMoniker * pMoniker;
    IRunningObjectTable *pROT;
    wchar_t wsz[128];
    size_t wsize = sizeof(wsz)/sizeof(wsz[0]);
    HRESULT hr;

    if( FAILED(GetRunningObjectTable(0, &pROT)) )
        return false;

#if (_MSC_VER < 1400)
    _snwprintf(wsz, wsize, L"FilterGraph %08x pid %08x", (DWORD_PTR)pUnkGraph, GetCurrentProcessId());
#else
    _snwprintf_s(wsz, wsize, wsize, L"FilterGraph %08x pid %08x", (DWORD_PTR)pUnkGraph, GetCurrentProcessId());
#endif


    hr = CreateItemMoniker(L"!", wsz, &pMoniker);
    if( SUCCEEDED(hr) )
    {
        hr = pROT->Register(ROTFLAGS_REGISTRATIONKEEPSALIVE, pUnkGraph, pMoniker, pdwRegister);
        if (hr == S_OK)
            printf("ROT: sucess\r\n");
        else
            printf("ROT: failure %d\r\n", hr);
        pMoniker->Release();
    }
    pROT->Release();
    return hr == S_OK;
}

// Removes a filter graph from the Running Object Table
void RemoveGraphFromRot( unsigned long pdwRegister )
{
    IRunningObjectTable *pROT;

    if( SUCCEEDED(GetRunningObjectTable(0, &pROT)) )
    {
        pROT->Revoke(pdwRegister);
        pROT->Release();
    }
}

// Opens a DirectShow filter's property page in a modal window
bool OpenPropertyPage( IBaseFilter *pFilter, HWND AppHandle )
{
    if( !pFilter )
        return false;

    ISpecifyPropertyPages *pProp = NULL;
    HRESULT hr = pFilter->QueryInterface( IID_ISpecifyPropertyPages, reinterpret_cast<void**>(&pProp) );
    if( SUCCEEDED(hr) && pProp )
    {
        // Get the filter's name and IUnknown pointer.
        FILTER_INFO FilterInfo;
        hr = pFilter->QueryFilterInfo(&FilterInfo);
        if (hr == S_OK)
        {
            IUnknown *pFilterUnk = NULL;
            hr = pFilter->QueryInterface( IID_IUnknown, reinterpret_cast<void**>(&pFilterUnk) );
            if (hr == S_OK)
            {
                // Show the page. 
                CAUUID caGUID;
                hr = pProp->GetPages(&caGUID);
                if (hr == S_OK)
                {
                    hr = OleCreatePropertyFrame(
                                               AppHandle,           // Parent window
                                               0, 0,                // Reserved
                                               FilterInfo.achName,  // Caption for the dialog box
                                               1,                   // Number of objects (just the filter)
                                               &pFilterUnk,         // Array of object pointers. 
                                               caGUID.cElems,       // Number of property pages
                                               caGUID.pElems,       // Array of property page CLSIDs
                                               0,                   // Locale identifier
                                               0, NULL              // Reserved
                                               );

                    CoTaskMemFree(caGUID.pElems);
                }
                // Clean up.
                pFilterUnk->Release();
            }
            FilterInfo.pGraph->Release();
        }
        pProp->Release();
    }

    return hr == S_OK;
}

void guid2str(GUID *g, char *s)
{
	unsigned char *x = (unsigned char *)g;
	sprintf_s(s, 255, "{%0.2X%0.2X%0.2X%0.2X-%0.2X%0.2X-%0.2X%0.2X-%0.2X%0.2X-%0.2X%0.2X%0.2X%0.2X%0.2X%0.2X}", x[3], x[2], x[1], x[0], x[5], x[4], x[7], x[6], x[8], x[9], x[10], x[11], x[12], x[13], x[14], x[15]);	
}

void get_filter_name(GUID *clsid, wchar_t* wsCLSID)
{
	char szCLSID[256];
	guid2str(clsid, &szCLSID[0]);

	wsCLSID[0] = 0;


	// Get version number
	HKEY   hkey;
	char szVal[256];
	char sz[256];
	DWORD lenVal;
	sprintf_s(sz, 255, "CLSID\\%s", szCLSID); 
	//AddLog(L"guid2str: %S", sz);
	if(RegOpenKeyExA(HKEY_CLASSES_ROOT, sz, 0, KEY_READ, &hkey)== ERROR_SUCCESS) 
    {
		/*AddLog(L"xxx:%S", sz);

		lenVal = 256;					
		if (RegQueryValueExA(hkey, "CLSID", NULL, NULL, (LPBYTE)&szVal[0], &lenVal)== ERROR_SUCCESS) { 
			AddLog(L"FriendlyName1");				
			wsprintf(wsCLSID, L"%S", szVal);
		}	
		lenVal = 256;					
		if (RegQueryValueExA(hkey, "FriendlyName", NULL, NULL, (LPBYTE)&szVal[0], &lenVal)== ERROR_SUCCESS) { 
			AddLog(L"FriendlyName2");				
			wsprintf(wsCLSID, L"%S", szVal);
		}	*/
		lenVal = 256;					
		if (RegQueryValueExA(hkey, "", NULL, NULL, (LPBYTE)&szVal[0], &lenVal)== ERROR_SUCCESS) { 
		//	AddLog(L"FriendlyName3");				
			swprintf_s(wsCLSID, 256, L"%S", szVal);
		}	

	/*	*/
		RegCloseKey(hkey);

	}
}

HRESULT get_filter_property_set(IBaseFilter *pfilter, IKsPropertySet **pkspropertyset)
{
	return pfilter->QueryInterface(IID_IKsPropertySet, (void **) pkspropertyset);
}

// -1  = ALL
//  0  = PINDIR_INPUT 
//  1  = PINDIR_OUTPUT
HRESULT get_pin_property_set(IBaseFilter *pfilter, int sdir, IKsPropertySet **pkspropertyset)
{
	HRESULT hr = E_FAIL;
	*pkspropertyset = NULL;
	IEnumPins* pPinEnum = NULL;
	pfilter->EnumPins(&pPinEnum);
	if (pPinEnum)
	{
		IPin* pPin=NULL;
		while (!(*pkspropertyset) && SUCCEEDED(pPinEnum->Next(1, &pPin, NULL)))
		{
			PIN_DIRECTION dir;
			if (SUCCEEDED(pPin->QueryDirection(&dir)) && ((sdir==0 && dir==PINDIR_INPUT) || (sdir==1 && dir==PINDIR_OUTPUT) || (sdir==-1))  )		
			{						
				hr = pPin->QueryInterface(IID_IKsPropertySet, (void **) pkspropertyset);										
			}
			pPin->Release();
		}
		pPinEnum->Release();
	}

	return hr;
}

} //engine
} //dvblink

#endif
