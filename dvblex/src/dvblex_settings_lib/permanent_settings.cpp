/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#ifdef WIN32
#include <shlobj.h>
#else
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#endif
#include <dl_utils.h>
#include <dl_common.h>
#include <dl_strings.h>
#include <dl_permanent_settings.h>
#include <dl_file_procedures.h>

using namespace dvblink::engine;

namespace dvblex { namespace settings {

boost::mutex permanent_settings_storage::lock_;
std::auto_ptr<permanent_settings_storage> permanent_settings_storage::instance_;

permanent_settings_storage::permanent_settings_storage() :
    storage_base(CONFIG_STORAGE_ROOT_NODE_NAME)
{

}

permanent_settings_storage::~permanent_settings_storage()
{
}

bool permanent_settings_storage::get_permanent_settings_file_directory(filesystem_path_t& folder)
{
#ifdef WIN32
    bool res = false;
    wchar_t buf[_MAX_PATH];
    HRESULT hr = SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA, NULL, SHGFP_TYPE_DEFAULT, buf);
    if (hr == S_OK)
    {
        folder = buf;
        res = true;
    }

    if (res)
    {
        res = !res;

        folder /= COMPANY_NAME;
        if (dvblink::engine::filesystem::create_directory(folder.to_boost_filesystem()))
        {
            folder /= PRODUCT_NAME;
            res = dvblink::engine::filesystem::create_directory(folder.to_boost_filesystem());
        }
    }
    return res;
#else
    const char* env_dir = getenv(TVMOSAIC_ROOT_CONFIG_DIR_ENV);

    if (env_dir && *env_dir)
    {
        try
        {
            folder = env_dir;
        }
        catch (...)
        {
            return false;
        }
    }
    else
    {
        folder = L"/usr/local/share/tvmosaic";
    }

    return true;
#endif
}

bool permanent_settings_storage::open()
{
    filesystem_path_t fs;
    if (get_permanent_settings_file_directory(fs))
    {
        fs /= PERMANENT_CONFIG_STORAGE_FILE_NAME;
        return storage_base::open(fs);
    }
    return false;
}

void permanent_settings_storage::shutdown()
{
    instance_.reset();
}

permanent_settings_storage& permanent_settings_storage::instance()
{
    if (!instance_.get())
    {
        boost::mutex::scoped_lock lock(lock_);
        if (!instance_.get())
        {
            permanent_settings_storage* tinstance = new permanent_settings_storage();
            tinstance->open();
            instance_ = std::auto_ptr<permanent_settings_storage>(tinstance);
        }
    }
    return *instance_.get();
}

filesystem_path_t permanent_settings::get_server_install_path()
{
    std::wstring path;
    permanent_settings_storage::instance().get_value(SERVER_INSTALL_PATH, path);
    return path;
}

void permanent_settings::set_server_install_path(filesystem_path_t& install_path)
{
    permanent_settings_storage::instance().set_value(SERVER_INSTALL_PATH, install_path.to_wstring(), storage_base::saving_at_once);
}

filesystem_path_t permanent_settings::get_private_root()
{
    std::wstring path;
    permanent_settings_storage::instance().get_value(PRIVATE_ROOT, path);
    return path;
}

void permanent_settings::set_private_root(filesystem_path_t& private_root)
{
    permanent_settings_storage::instance().set_value(PRIVATE_ROOT, private_root.to_wstring(), storage_base::saving_at_once);
}

filesystem_path_t permanent_settings::get_shared_root()
{
    std::wstring path;
    permanent_settings_storage::instance().get_value(SHARED_ROOT, path);
    return path;
}

void permanent_settings::set_shared_root(filesystem_path_t& shared_root)
{
    permanent_settings_storage::instance().set_value(SHARED_ROOT, shared_root.to_wstring(), storage_base::saving_at_once);
}

client_id_t permanent_settings::get_server_id()
{
    client_id_t cid;
    std::wstring id;
    if (permanent_settings_storage::instance().get_value(SERVER_ID, id))
    {
        cid = id;
    }
    return cid;
}

bool permanent_settings::set_server_id(const client_id_t& id)
{
    return permanent_settings_storage::instance().set_value(SERVER_ID, id.to_wstring(), storage_base::saving_at_once);
}

} //settings
} //dvblink
