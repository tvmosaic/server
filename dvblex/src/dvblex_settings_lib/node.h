/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

# pragma once

namespace dvblex { namespace settings {

class node
{
    friend class storage_base;
private:
    node_name_t name_;
    value_t value_;

    typedef std::map<node_name_t, node> map_nodes_t;
    map_nodes_t nodes_;

public:
    node() {}
    node(const node_name_t& name) : name_(name) {}
    ~node() {}

private:
    node_name_t name() const {return name_;}

    void set(const node& src)
    {
        value_ = src.value_;
        nodes_ = src.nodes_;
    }

    node* insert_node(const node& n)
    {
        std::pair<map_nodes_t::iterator, bool> res = nodes_.insert(std::make_pair(n.name(), n));
        if (res.second)
        {
            map_nodes_t::iterator it = res.first;
            return &it->second;
        }
        return NULL;
    }

    bool remove_node(const node_name_t& name)
    {
        map_nodes_t::iterator iter = nodes_.find(name);
        if (iter != nodes_.end())
        {
            nodes_.erase(iter);
            return true;
        }
        return false;
    }

    void set_value(const value_t value)
    {
        value_ = value;
    }

    node* get_node(const node_name_t& node_name)
    {
        map_nodes_t::iterator iter = nodes_.find(node_name);
        if (iter != nodes_.end())
        {
            return &iter->second;
        }
        return NULL;
    }

    bool get_value(value_t& val) const
    {
        if (!nodes_.size())
        {
            val = value_;
            return true;
        }
        return false;
    }

    template <typename Out>
    void enum_leaf(Out& result)
    {
        for (map_nodes_t::const_iterator iter = nodes_.begin(); iter != nodes_.end(); ++iter)
        {
            result.insert(result.end(), iter->first.get());
        }
    }
};

} //settings
} //dvblink

