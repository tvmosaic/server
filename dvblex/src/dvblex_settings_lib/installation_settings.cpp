/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_strings.h>
#include <dl_message_server.h>
#include <dl_message_addresses.h>
#include <dl_installation_settings.h>
#include <dl_file_procedures.h>

using namespace dvblink;
using namespace dvblink::messaging;

namespace dvblex { namespace settings {

void installation_settings_t::init(dvblink::messaging::message_queue_t& message_queue)
{
    message_queue_ = message_queue;

    //private paths
    //common path
    server::private_module_path_request req(common_component_name);
    server::private_module_path_response resp;
    if (message_queue->send(server_message_queue_addressee, req, resp) == success)
        private_common_path_ = resp.path_;

    //config path
    req.module_name_ = config_component_name;
    if (message_queue->send(server_message_queue_addressee, req, resp) == success)
        private_config_path_ = resp.path_;

    //shared paths
    //common path
    server::shared_module_path_request shared_req(common_component_name);
    server::shared_module_path_response shared_resp;
    if (message_queue->send(server_message_queue_addressee, shared_req, shared_resp) == success)
        shared_common_path_ = shared_resp.path_;

    //sqlite
    server::common_item_file_info_request cif_req(common_item_sqlite);
    server::common_item_file_info_response cif_resp;
    if (message_queue->send(server_message_queue_addressee, cif_req, cif_resp) == success)
    {
        sqlite_.dir_ = cif_resp.dir_;
        sqlite_.file_ = cif_resp.file_;
    }

    //temp dir
    cif_req.file_item_ = common_item_temp;
    if (message_queue->send(server_message_queue_addressee, cif_req, cif_resp) == success)
        shared_temp_path_ = cif_resp.dir_;

    //ffmpeg
    cif_req.file_item_ = common_item_ffmpeg;
    if (message_queue->send(server_message_queue_addressee, cif_req, cif_resp) == success)
    {
        ffmpeg_.dir_ = cif_resp.dir_;
        ffmpeg_.file_ = cif_resp.file_;
    }

    //ffprobe
    cif_req.file_item_ = common_item_ffprobe;
    if (message_queue->send(server_message_queue_addressee, cif_req, cif_resp) == success)
    {
        ffprobe_.dir_ = cif_resp.dir_;
        ffprobe_.file_ = cif_resp.file_;
    }

    //comskip
    cif_req.file_item_ = common_item_comskip;
    if (message_queue->send(server_message_queue_addressee, cif_req, cif_resp) == success)
    {
        comskip_.dir_ = cif_resp.dir_;
        comskip_.file_ = cif_resp.file_;
    }

    //xmltv category definitions
    cif_req.file_item_ = common_item_xmltv_cat_definitions;
    if (message_queue->send(server_message_queue_addressee, cif_req, cif_resp) == success)
    {
        xmltv_cat_defs_.dir_ = cif_resp.dir_;
        xmltv_cat_defs_.file_ = cif_resp.file_;
    }
}

const filesystem_path_t installation_settings_t::get_private_component_path(const std::string& component_name)
{
    filesystem_path_t ret_val;

    server::private_module_path_request req(component_name);
    server::private_module_path_response resp;
    if (message_queue_->send(server_message_queue_addressee, req, resp) == success)
    {
        ret_val = resp.path_;
    }

    return ret_val;
}

const filesystem_path_t installation_settings_t::get_shared_component_path(const std::string& component_name)
{
    filesystem_path_t ret_val;

    server::shared_module_path_request req(component_name);
    server::shared_module_path_response resp;
    if (message_queue_->send(server_message_queue_addressee, req, resp) == success)
    {
        ret_val = resp.path_;
    }

    return ret_val;
}

} //settings
} //dvblex
