/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <string>
#include <vector>
#ifndef _WIN32
#include <sys/types.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <paths.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <fcntl.h>
#endif
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_external_control.h>
#include <dl_zip.h>

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::zip;

//#define DVBLEX_USE_EXTERNAL_ZIP true

namespace dvblex { namespace settings {

bool external_control::unzip(installation_settings_t& ds, const dvblink::filesystem_path_t& zipfile, const dvblink::filesystem_path_t& dest_dir)
{
    bool ret_val = false;

    dvblink::logging::log_info(L"external_control::unzip: Starting unzip process for %1%") % zipfile.to_wstring();

#ifndef DVBLEX_USE_EXTERNAL_ZIP

    zipfile_reader zipfile_reader(zipfile);
    ret_val = zipfile_reader.decompress(dest_dir);

#else
    dvblink::filesystem_path_t exe_path = ds.get_unzip().file_;

    //create command line
	std::ostringstream ostr;
    ostr << "\"" << exe_path.to_string() << "\" -q -o \"" << zipfile.to_string() << "\"";
	ostr << " -d \"" << dest_dir.to_string() << "\"";

    bool success = false;

#ifdef WIN32
	PROCESS_INFORMATION piProcInfo; 
	STARTUPINFOA siStartInfo;
    
    // Set up members of the PROCESS_INFORMATION structure
	ZeroMemory(&piProcInfo, sizeof(PROCESS_INFORMATION));
    
    // Set up members of the STARTUPINFO structure
	ZeroMemory(&siStartInfo, sizeof(STARTUPINFO));
	siStartInfo.cb = sizeof(STARTUPINFO); 
	siStartInfo.dwFlags |= STARTF_USESHOWWINDOW;
	siStartInfo.wShowWindow = SW_NORMAL;

	success = CreateProcessA(NULL,
		(char*)ostr.str().c_str(),  // command line 
		NULL,           // process security attributes 
		NULL,           // primary thread security attributes 
		FALSE,          // handles are inherited 
		0,              // creation flags 
		NULL,           // use parent's environment 
		NULL,           // use parent's current directory
		&siStartInfo,   // STARTUPINFO pointer 
		&piProcInfo) ? true : false;   // receives PROCESS_INFORMATION
   
    DWORD exit_code = 0;
	if (success)
	{
		CloseHandle(piProcInfo.hThread);
        //wait until unzip process finishes
		WaitForSingleObject(piProcInfo.hProcess, INFINITE);

        //get exit code
        if (GetExitCodeProcess(piProcInfo.hProcess, &exit_code))
        {
            ret_val = (exit_code == 0);
        }
		CloseHandle(piProcInfo.hProcess);
    } else
	{
        dvblink::logging::log_error(L"external_control::unzip: CreateProcess() failed (%1%)") % string_cast<EC_UTF8>(ostr.str());
	}

#else
	std::string path = ostr.str();

	log_info(L"external_control::unzip. start unzip: %1%") % string_cast<EC_UTF8>(path);

	int exit_code = system(path.c_str());
	log_ext_info(L"external_control::unzip. unzip returned: %1%") % exit_code;
	log_ext_info(L"external_control::unzip. errno: %1%") % errno;

    success = true;
    ret_val = true;
#endif

    if (success)
    {
        if (ret_val)
        {
            dvblink::logging::log_info(L"external_control::unzip: unzip process finished successfully");
        }
        else
        {
            dvblink::logging::log_error(L"external_control::unzip: unzip process failed (%1%)") % exit_code;
        }
    }
#endif
    return ret_val;
}
/*
bool external_control::untar(dvblink::filesystem_path_t& tarfile, dvblink::filesystem_path_t& dest_dir)
{
    bool ret_val = false;

    dvblink::logging::log_info(L"external_control::untar: Starting untar process for %1%") % tarfile.get();

    //create command line
    std::wostringstream ostr;

    dvblink::filesystem_path_t exe_path = L"tar";
    ostr << L"\"" << exe_path.get() << L"\" xvzf \"" << tarfile.get() << "\"";
    ostr << L" -C \"" << dest_dir.get() << "\"";

    int exit_code = 0;
#ifdef WIN32
    ret_val = false;
#else
	std::string path = string_cast<EC_UTF8>(ostr.str().c_str());

	log_info(L"external_control::untar. start tar: %1%") % path.c_str();

	exit_code = system(path.c_str());
	log_info(L"external_control::untar. tar returned: %1%") % exit_code;
	log_ext_info(L"external_control::untar. errno: %1%") % errno;

    ret_val = true;//(exit_code == 0); //system() does not report correct exit status on linux. set it to true always
#endif

	if (ret_val)
	{
		dvblink::logging::log_info(L"external_control::untar: tar process finished successfully");
	}
	else
	{
		dvblink::logging::log_error(L"external_control::untar: tar process failed (%1%)") % exit_code;
	}

    return ret_val;
}
*/
#ifndef _WIN32

#ifndef OPEN_MAX_DESC
	#define OPEN_MAX_DESC 256
#endif

static int open_devnull(int fd)
{
  FILE *f = 0;

  if (!fd)
	  f = freopen(_PATH_DEVNULL, "rb", stdin);
  else
	  if (fd == 1)
		  f = freopen(_PATH_DEVNULL, "wb", stdout);
	  else
		  if (fd == 2)
			  f = freopen(_PATH_DEVNULL, "wb", stderr);
  return (f && fileno(f) == fd);
}

static void sanitize_open_files(void)
{
  int         fd, fds;
  struct stat st;

  /* Make sure all open descriptors other than the standard ones are closed */
#ifndef __ANDROID__
  if ((fds = getdtablesize()) == -1)
#else
    if ((fds = sysconf(_SC_OPEN_MAX)) == -1)
#endif
	  fds = OPEN_MAX_DESC;
  for (fd = 3;  fd < fds;  fd++)
	  close(fd);

  /* Verify that the standard descriptors are open.  If they're not, attempt to
   * open them using /dev/null.  If any are unsuccessful, abort.
   */
  for (fd = 0;  fd < 3;  fd++)
    if (fstat(fd, &st) == -1 && (errno != EBADF || !open_devnull(fd)))
    	abort();
}

#endif

boost::int64_t external_control::start_process(const dvblink::filesystem_path_t& exe_path, const dvblink::filesystem_path_t* exe_dir,
                                               const std::vector<launch_param_t>& params, process_priority_e priority,
                                               const std::vector<dvblink::env_var_t>* env_vars/* = NULL */)
{
    boost::int64_t pid = -1;
	dvblink::logging::log_info(L"external_control::start_process: Starting process %1%. Params num %2%") % exe_path.to_wstring() % params.size();

#ifdef WIN32
    std::wstring output_file_name;
    //create command line
	std::wostringstream ostr;
    ostr << L"\"" << exe_path.to_wstring() << L"\"";
	for (size_t i=0; i<params.size(); i++)
    {
        if(boost::iequals(params[i].to_string(), ">"))
        {
            //redirected output
            output_file_name = string_cast<EC_UTF8>(params[i+1].to_string());
            break;
        } else
        {
    		ostr << L" " << string_cast<EC_UTF8>(params[i].to_string());
        }
    }

    PROCESS_INFORMATION piProcInfo;
	STARTUPINFOW siStartInfo;
    
    // Set up members of the PROCESS_INFORMATION structure
	ZeroMemory(&piProcInfo, sizeof(PROCESS_INFORMATION));
    
    // Set up members of the STARTUPINFO structure
	ZeroMemory(&siStartInfo, sizeof(STARTUPINFO));
	siStartInfo.cb = sizeof(STARTUPINFO); 
	siStartInfo.dwFlags |= STARTF_USESHOWWINDOW;
	siStartInfo.wShowWindow = SW_NORMAL;

    if (!output_file_name.empty())
    {
        //remove first and last quotes
        if (output_file_name[0] == '"')
            output_file_name.erase(0, 1);
        if (output_file_name[output_file_name.size() - 1] == '"')
            output_file_name.erase(output_file_name.size() - 1, 1);

        //setup redirected output
        siStartInfo.dwFlags |= STARTF_USESTDHANDLES;

        SECURITY_ATTRIBUTES sa;
        sa.nLength = sizeof(SECURITY_ATTRIBUTES);
        sa.lpSecurityDescriptor = NULL;
        sa.bInheritHandle = TRUE;

        HANDLE hOut = CreateFileW(output_file_name.c_str(), GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, &sa, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
        DWORD err = GetLastError();
        siStartInfo.hStdOutput = hOut;
    }

    std::wstring run_cmd = ostr.str();

    logging::log_ext_info(L"external_control::start_process. Executing %1%") % run_cmd;

	bool success = CreateProcessW(
        NULL,
		const_cast<wchar_t*>(run_cmd.c_str()),  // command line 
		NULL,           // process security attributes 
		NULL,           // primary thread security attributes 
		!output_file_name.empty(),          // handles inherited ?
		priority,       // creation flags 
		NULL,           // use parent's environment 
        exe_dir != NULL ? exe_dir->to_wstring().c_str() : NULL,           // use parent's current directory
		&siStartInfo,   // STARTUPINFO pointer 
		&piProcInfo) ? true : false;   // receives PROCESS_INFORMATION
   
	if (success)
	{
        CloseHandle(piProcInfo.hThread);
        CloseHandle(piProcInfo.hProcess);
        pid = piProcInfo.dwProcessId;
    } else
	{
        dvblink::logging::log_error(L"external_control::start_process: CreateProcess() failed (%1%)") % ostr.str();
	}

#else
	std::string exe_path_str = exe_path.to_string();

	std::wostringstream bufstr;
    bufstr << exe_path.to_wstring() << L" ";

	std::vector<std::string> str_params;
	for (size_t i=0; i<params.size(); i++)
    {
		str_params.push_back(params[i].to_string());
        bufstr << params[i].to_wstring() << L" ";
    }

    logging::log_ext_info(L"external_control::start_process. Executing %1%") % bufstr.str();

	pid = fork();

	if (pid == -1)
	{
		log_error(L"external_control::run_package_installer. clone failed");
	}

	if (pid == 0)
	{
		//child process
    
        //set priority
        setpriority(PRIO_PROCESS, 0, (int)priority - 20);

		//close all open file descriptors, which are inherited from a parent process
		sanitize_open_files();

		char** arg_list = (char**)malloc(sizeof(char*)*(str_params.size() + 2));
		arg_list[0] = (char*)exe_path_str.c_str(); /* argv[0], the name of the program.  */

        int param_num = 1;
		for (size_t i=0; i<str_params.size(); i++)
        {
			arg_list[i+1] = (char*)str_params[i].c_str();
            ++param_num;
        }

		arg_list[str_params.size() + 1] = NULL;      /* The argument list must end with a NULL.  */

        //check if output redirect is needed
        for (int i=0; i<param_num; i++)
        {
            if(strcmp(arg_list[i], ">") == 0)
            {
                int fd1 = open(arg_list[i+1], O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
                if(fd1 >= 0)
                {
                    dup2(fd1, 1);
                    close(fd1);
                }
                arg_list[i] = NULL;
                break;
            }
        }

        int env_params_num = 0;
        if (exe_dir != NULL)
            env_params_num += 1;
        if (env_vars != NULL)
            env_params_num += env_vars->size();

        if (env_params_num > 0)
        {
		    char** env_var_list = (char**)malloc(sizeof(char*)*(env_params_num + 1));
            int env_start_idx = 0;

            std::string ld_libpath_str;
            if (exe_dir != NULL)
            {
                ld_libpath_str = exe_dir->to_string().c_str();
                ld_libpath_str = "LD_LIBRARY_PATH="+ ld_libpath_str;
                env_var_list[env_start_idx] = (char*)ld_libpath_str.c_str();
                env_start_idx += 1;
            }

        	std::vector<std::string> env_params;
            if (env_vars != NULL)
            {
                for (size_t i=0; i<env_vars->size(); i++)
            		env_params.push_back(env_vars->at(i).to_string());
            }

            for (size_t i=0; i<env_params.size(); i++)
                env_var_list[env_start_idx+i] = (char*)env_params[i].c_str();

            env_var_list[env_params_num] = NULL;

	        execve(exe_path_str.c_str(), arg_list, env_var_list);
        } else
        {
	        execvp(exe_path_str.c_str(), arg_list);
        }

		/* The execvp function returns only if an error occurs.  */
		fprintf (stderr, "an error occurred in execvp\n");
		abort ();
		return 0;
	}

#endif

    return pid;
}

bool external_control::is_process_running(boost::int64_t pid)
{
    bool res = false;
    if (pid != -1)
    {
#ifdef WIN32
    HANDLE h = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, pid);
    if (h != NULL)
    {
        unsigned long dw;
        if (GetExitCodeProcess(h, &dw) && dw == STILL_ACTIVE)
            res = true;
        CloseHandle(h);
    }
#else
    //do waitpid first to make sure that process is not zombi
    int process_stat;
    waitpid(pid, &process_stat, WNOHANG | WUNTRACED);

    int kill_res = kill(pid, 0);
    log_ext_info(L"external_control::is_process_running. kill(0) result %1% on pid %2%") % kill_res % pid;
    if (kill_res == 0)
    	res = true;
#endif
    }
    return res;
}

bool external_control::kill_process(boost::int64_t pid)
{
    bool res = false;
    if (pid != -1)
    {
#ifdef WIN32
    HANDLE h = OpenProcess(PROCESS_TERMINATE, FALSE, pid);
    if (h != NULL)
    {
        if (TerminateProcess(h, 0))
            res = true;
        CloseHandle(h);
    }
#else
    int kill_res = kill(pid, 9);
    if (kill_res == 0)
    	res = true;
#endif
    }
    return res;
}

} //settings
} //dvblex
