/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <iostream>
#include <exception>
#include <boost/algorithm/string.hpp>
#include <dl_xml_name_encoding.h>
#include <dl_utils.h>
#include <dl_strings.h>
#include <dl_storage_base.h>
#include "node.h"


using namespace pugi;
using namespace dvblink;
using namespace dvblink::pugixml_helpers;
using namespace dvblink::engine;

namespace dvblex { namespace settings {

storage_base::storage_base(const std::string& root_node_name, bool b_encode_tags/* = false*/) :
    node_(NULL),
    root_node_name_(root_node_name),
    initialized_(false),
    encode_tags_(b_encode_tags)
{
}

storage_base::~storage_base()
{
    //save();
    delete node_;
}

std::string storage_base::convert(const std::wstring& from)
{
    std::string to;
    ConvertUCToMultibyte(EC_UTF8, from.c_str(), to);
    return to;
}

std::string storage_base::convert_tag(const std::wstring& from)
{
    std::string to;
    ConvertUCToMultibyte(EC_UTF8, from.c_str(), to);
    std::string tag_to;
    if (encode_tags_)
        xml_name_encode(to, tag_to);
    else
        tag_to = to;
    return tag_to;
}

std::wstring storage_base::convert(const char* from)
{
    std::wstring to;
    ConvertMultibyteToUC(EC_UTF8, reinterpret_cast<const char*>(from), to);
    return to;
}

std::wstring storage_base::convert_tag(const char* from)
{
    std::string str;
    if (encode_tags_)
    {
        xml_name_decode(reinterpret_cast<const char*>(from), str);
    }
    else
    {
        str = reinterpret_cast<const char*>(from);
    }
    std::wstring to;
    ConvertMultibyteToUC(EC_UTF8, str.c_str(), to);
    return to;
}

std::wstring storage_base::convert(const std::string& from)
{
    std::wstring to;
    ConvertMultibyteToUC(EC_UTF8, from.c_str(), to);
    return to;
}

void storage_base::enum_leaf(node* n, std::vector<std::wstring>& collection)
{
    return n->enum_leaf(collection);
}

bool storage_base::create()
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    return do_create();
}

bool storage_base::do_create()
{
    if (node_ == NULL)
    {
        node_ = new node(node_name_t(root_node_name_));
        initialized_ = node_ ? true : false;
    }
    return initialized_;
}


bool storage_base::reload()
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    delete node_;
    node_ = NULL;
    return do_open(config_path_);
}

bool storage_base::open(const dvblink::filesystem_path_t& p)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    return do_open(p);
}

bool storage_base::do_open(const dvblink::filesystem_path_t& p)
{
    if (node_ == NULL)
    {
        config_path_ = p;

        if (boost::filesystem::exists(p.to_boost_filesystem()))
        {
            xml_document doc;
            if (doc.load_file(p.to_string().c_str()).status == status_ok)
            {
                xml_node root_node = doc.first_child();
                if (boost::iequals(root_node.name(), root_node_name_))
                {
                    node_ = new node(node_name_t(convert_tag(root_node.name())));
                    initialized_ = read_xml_node(root_node, node_);
                }
            }
        }
        else
        {
            //create empty storage if it does not exist yet
            do_create();
        }

        if (!initialized_)
        {
            std::ostringstream str;
            str << "Xml storage " << p.to_string() << " is corrupted";
            throw std::runtime_error(str.str().c_str());
        }
    }
    else
    {
        std::ostringstream str;
        str << "Xml storage " << p.to_string() << " has been initialized already";
        throw std::runtime_error(str.str().c_str());
    }

    return initialized_;
}

bool storage_base::write_xml_node(const node* src_node, xml_node& dst_node)
{
    if (src_node)
    {
        if (src_node->nodes_.size())
        {
            xml_node child = new_child(dst_node, src_node->name().to_string());

            node::map_nodes_t::const_iterator iter_end = src_node->nodes_.end();
            for (node::map_nodes_t::const_iterator iter = src_node->nodes_.begin(); iter != iter_end; ++iter)
            {
                write_xml_node(&iter->second, child);
            }
        }

        value_t value;
        if (src_node->get_value(value))
        {
            new_child(dst_node, src_node->name().to_string(), value.get());
        }
    }
    return true;
}

bool storage_base::do_save(xml_string_t* xml)
{
    bool res = false;

    xml_document doc;
    write_xml_node(node_, doc);

    if (xml)
    {
        std::string dump;
        xmldoc_dump_to_string(doc, dump);
        xml->set(dump);
    }

    doc.save_file(config_path_.to_string().c_str());

    return res;
}

bool storage_base::save(xml_string_t* xml)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    return do_save(xml);
}

bool storage_base::insert_xml(const xml_node& chapter_node, const storage_path& path, storage_base::save_type save_flag)
{
    bool res = false;
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    if (node* to = find_node(path))
    {
        res = read_xml_node(chapter_node, to);
        if (res && save_flag == saving_at_once)
        {
            do_save();
        }
    }
    return res;
}

bool storage_base::read_xml_node(const xml_node& parent_node, node* parent)
{
    bool res = false;

    xml_node child_node = parent_node.first_child();
    while (child_node)
    {
        switch (child_node.type())
        {
        case node_element:
            {
                //std::cout << "NODE. name: " << child_node->name << std::endl;
                node chnode(convert_tag(child_node.name()));
                if (node* chnode_res = parent->insert_node(chnode))
                {
                    res = read_xml_node(child_node, chnode_res);
                }
            }
            break;
        case node_pcdata: // text node
            //if (xmlStrcasecmp(child_node->name, (const xmlChar*)"text") == 0 && child_node->content && child_node->content[0] != 0xa)
            {
                std::string val = child_node.value();
                if (!val.empty())
                {
                    parent->set_value(convert(val));
                    res = true;
                    //std::cout << "VALUE. name: " << parent_node->name << ", value: \"" << value << "\"" << std::endl;
                }
            }
            break;
        default:
        	break;
        }
        child_node = child_node.next_sibling();
    }
    return res;
}

node* storage_base::find_node(const storage_path& path)
{
    std::vector<std::wstring> result;
    boost::algorithm::split(result, path.get(), &storage_path::is_delimeter);

    node* cur_node = NULL;
    if (node_ != NULL)
    {
        if (result[0].empty()) //from root
        {
            cur_node = node_;
            for (size_t i = 1; i < result.size(); ++i)
            {
                if ((cur_node = cur_node->get_node(result[i])) == NULL)
                {
                    break;
                }
            }
        }
    }
    return cur_node;
}

bool storage_base::set_value(const storage_path& path, const value_t& value)
{
    node* n = find_node(path);
    if (!n)
    {
        n = create_node(path);
    }
    n->set_value(value);

    return true;
}

bool storage_base::get_value(const storage_path& path, value_t& value)
{
    if (node* n = find_node(path))
    {
        n->get_value(value);
        return true;
    }
    return false;
}

node* storage_base::create_node(const storage_path& path)
{
    std::vector<std::wstring> result;
    boost::algorithm::split(result, path.get(), &storage_path::is_delimeter);

    node* next_node = NULL;
    if (result[0].empty() && node_) // from root
    {
        size_t i;
        node* cur_node = node_;
        for (i = 1 ; i < result.size(); ++i)
        {
            if ((next_node = cur_node->get_node(result[i])) == NULL)
            {
                break;
            }
            cur_node = next_node;
        }

        while (i < result.size())
        {
            node_name_t nnode(result[i++]);
            node chnode(nnode);
            next_node = cur_node = cur_node->insert_node(chnode);
        }
    }

    return next_node;
}

bool storage_base::rename(const storage_path& path, std::wstring new_name)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);

    bool res = false;
    if (node* n = find_node(path))
    {
        std::wstring prev_node_path = path.get();
        boost::iterator_range<std::wstring::iterator> result = boost::algorithm::find_last(prev_node_path, "/");

        prev_node_path.erase(result.begin(), prev_node_path.end());
        if (node* prev_node = find_node(prev_node_path))
        {
            // make a copy
            node last_node = *n;
            // rename
            last_node.name_ = new_name;

            prev_node->remove_node(n->name());
            prev_node->insert_node(last_node);

            res = true;
        }
    }

    return res;
}

bool storage_base::copy_recursive(const storage_path& src_path, const storage_path& dst_path)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);

    bool res = false;
    node* src_node = find_node(src_path);
    if (src_node)
    {
        node* dst_node = create_node(dst_path);
        if (dst_node)
        {
            dst_node->set(*src_node);
            res = true;
        }
    }

    return res;
}

bool storage_base::remove(const storage_path& path, storage_base::save_type save)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    bool b = remove_impl(path);
    if (b && save == saving_at_once)
    {   
        storage_base::do_save();
    }

    return b;
}

bool storage_base::remove_impl(const storage_path& path)
{
    std::vector<std::wstring> result;
    boost::algorithm::split(result, path.get(), &storage_path::is_delimeter);

    bool res = false;
    node* pred_end_node = NULL;
    if (result[0].empty() && node_) //from root
    {
        pred_end_node = node_;
        for (size_t i = 1; i < result.size() - 1; ++i)
        {
            if ((pred_end_node = pred_end_node->get_node(result[i])) == NULL)
            {
                break;
            }
        }

        if (pred_end_node)
        {
            res = pred_end_node->remove_node(result[result.size() - 1]);
        }
    }
    return res;
}

bool storage_base::read_node(const storage_path& path, storage_node_content_t& node_content)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    //bool res = false;

    //std::vector<std::wstring> collection;
    //if (enum_leaf_impl(path, collection))
    //{
    //    for (size_t i = 0; i < collection.size(); i++)
    //    {
    //        value_t value;
    //        if (get_value(path / collection[i], value))
    //        {
    //            node_content.push_back(node_description(collection[i], value));
    //        }
    //    }

    //    res = true;
    //}

    //return res;
    node_content.clear();
    return read_node_impl(path, L"", node_content);
}

bool storage_base::read_node_impl(const storage_path& path_root, const storage_path& path_middle, storage_node_content_t& node_content)
{
    bool res = false;

    std::vector<std::wstring> collection;
    storage_path full_path = path_root / path_middle;
    if (enum_leaf_impl(full_path, collection))
    {
        for (size_t i = 0; i < collection.size(); i++)
        {
            value_t value;
            storage_path p;
            if (get_value(full_path / collection[i], value))
            {
                if (!path_middle.empty())
                {
                    p = path_middle / collection[i];
                }
                else
                {
                    p = collection[i];
                }

                node_content.push_back(node_description(p, value));
            }
            read_node_impl(path_root, p, node_content);
        }

        res = true;
    }

    return res;
}

// path - absolute path from storage root
// pathes in the node_content - relative the path
bool storage_base::write_node(const storage_path& path, const storage_node_content_t& node_content, bool clean)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);

    if (clean)
    {
        remove_impl(path);
    }

    for (size_t i = 0; i < node_content.size(); i++)
    {
        set_value(path / node_content[i].path_, node_content[i].value_);
    }

    return true;
}

} //settings
} //dvblink
