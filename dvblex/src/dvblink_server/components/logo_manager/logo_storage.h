/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <dl_message_queue.h>
#include <dl_message_epg.h>
#include <dl_message_common.h>
#include <dl_filesystem_path.h>
#include <dl_installation_settings.h>
#include <dl_message_channels.h>
#include <dl_message_playback.h>
#include <dli_playback_source.h>
#include <dl_logo_cmd.h>

namespace dvblex { 

struct default_channel_logo_t
{
    default_channel_logo_t()
    {
        time(&last_update_time_);
    }

    time_t last_update_time_;
    dvblink::url_address_t default_logo_;
    dvblink::epg_source_id_t default_logo_src_;
};

typedef std::map<dvblink::channel_id_t, default_channel_logo_t> default_channel_logo_map_t;

class logo_storage_t
{
    class playback_handler : public dvblink::messaging::i_play_source_control, public boost::enable_shared_from_this<playback_handler>
    {
        public:
            playback_handler(logo_storage_t* logo_storage)
                : logo_storage_(logo_storage)
                {}

            dvblink::i_result __stdcall query_interface(const dvblink::base_id_t& requestor_id, const dvblink::i_guid& iid, dvblink::i_base_object_t& obj)
            {
                dvblink::i_result res = dvblink::i_error;
                return res;
            }

            virtual const boost::uuids::uuid& __stdcall get_uid(){return logo_storage_->get_uid();}

            virtual bool __stdcall read_data(const dvblink::object_handle_safe_t& handle, unsigned char* buffer, boost::uint32_t& buffer_length)
            {
                return logo_storage_->read_data(handle, buffer, buffer_length);
            }

    protected:
        logo_storage_t* logo_storage_;
    };

    class message_handler : 
        public dvblink::messaging::xml_message_request::subscriber,
        public dvblink::messaging::playback::open_item_request::subscriber,
        public dvblink::messaging::playback::seek_item_request::subscriber,
        public dvblink::messaging::playback::close_item_request::subscriber,
        public dvblink::messaging::playback::get_objects_request::subscriber,
        public dvblink::messaging::channels::channel_config_changed_request::subscriber,
        public dvblink::messaging::epg::epg_updated_request::subscriber,
        public dvblink::messaging::channels::get_channel_logo_request::subscriber
    {
    public:
        message_handler(logo_storage_t* logo_storage, dvblink::messaging::message_queue_t message_queue) :
            dvblink::messaging::xml_message_request::subscriber(message_queue),
            dvblink::messaging::playback::open_item_request::subscriber(message_queue),
            dvblink::messaging::playback::seek_item_request::subscriber(message_queue),
            dvblink::messaging::playback::close_item_request::subscriber(message_queue),
            dvblink::messaging::playback::get_objects_request::subscriber(message_queue),
            dvblink::messaging::channels::channel_config_changed_request::subscriber(message_queue),
            dvblink::messaging::epg::epg_updated_request::subscriber(message_queue),
            dvblink::messaging::channels::get_channel_logo_request::subscriber(message_queue),
            logo_storage_(logo_storage),
            message_queue_(message_queue)
        {
        }

        ~message_handler()
        {
        }

    protected:
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response)
        {
            logo_storage_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channel_logo_request& request, dvblink::messaging::channels::get_channel_logo_response& response)
        {
            logo_storage_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::channel_config_changed_request& request)
        {
            logo_storage_->handle(sender, request);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::epg_updated_request& request)
        {
            logo_storage_->handle(sender, request);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::open_item_request& request, dvblink::messaging::playback::open_item_response& response)
        {
            logo_storage_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::seek_item_request& request, dvblink::messaging::playback::seek_item_response& response)
        {
            logo_storage_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::close_item_request& request, dvblink::messaging::playback::close_item_response& response)
        {
            logo_storage_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_objects_request& request, dvblink::messaging::playback::get_objects_response& response)
        {
            logo_storage_->handle(sender, request, response);
        }

    private:
        logo_storage_t* logo_storage_;
        dvblink::messaging::message_queue_t message_queue_;
    };

public:
    logo_storage_t(dvblink::messaging::message_queue_t& message_queue, dvblex::settings::installation_settings_t& settings);

    virtual ~logo_storage_t();

    bool start();
    void stop();

    dvblink::i_base_object_t get_playback_interface(){return playback_handler_->shared_from_this();}

protected:
    dvblink::messaging::message_queue_t message_queue_;
    std::auto_ptr<message_handler> message_handler_;
    boost::shared_ptr<playback_handler> playback_handler_;
    dvblex::settings::installation_settings_t settings_;
    channel_logo_id_map_t channel_logo_map_;
    default_channel_logo_map_t default_channel_logo_map_;
    dvblink::filesystem_path_t settings_path_;
    dvblink::filesystem_path_t logo_packages_path_;
    dvblink::network_port_t streaming_port_;
    dvblink::network_port_t https_streaming_port_;
	boost::shared_mutex file_info_lock_;
	std::map<dvblink::object_handle_t, FILE*> file_info_map_;

    const boost::uuids::uuid& get_uid() {return message_queue_->get_id().get();}

    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channel_logo_request& request, dvblink::messaging::channels::get_channel_logo_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::channel_config_changed_request& request);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::epg_updated_request& request);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::open_item_request& request, dvblink::messaging::playback::open_item_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::seek_item_request& request, dvblink::messaging::playback::seek_item_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::close_item_request& request, dvblink::messaging::playback::close_item_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_objects_request& request, dvblink::messaging::playback::get_objects_response& response);
    void update_default_logos(const dvblink::epg_source_id_t* epg_source_id);
    void load_config();
    void save_config();
    void load_defaults();
    void save_defaults();
    void get_logo_packages_list(logo_package_list_t& packages);
    void get_logos_for_package(const dvblink::url_proto_t& proto, const dvblink::url_address_t& server_addr, const dvblink::logo_package_t& package, channel_package_logo_item_list_t& item_list);
    void get_id_for_logo(const dvblink::logo_package_t& package, const std::string& logo, dvblink::logo_id_t& logo_id);
    void update_streaming_port();
    dvblink::url_address_t generate_url_for_logo(const dvblink::logo_id_t& logo_id, const dvblink::url_proto_t& proto, const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port);
    void set_channel_logos(const channel_logo_id_map_t& channel_logo_map);
    void match_logos_to_channels(const channel_package_logo_item_list_t& item_list, 
        const match_logo_package_req& request, channel_logo_match_map_t& result);
    bool read_data(const dvblink::object_handle_safe_t& handle, unsigned char* buffer, boost::uint32_t& buffer_length);
    dvblink::filesystem_path_t get_logo_path_from_id(const std::string& id);
    dvblink::network_port_t get_port_from_proto(const dvblink::url_proto_t& proto);
    void assign_default_logo_from_channels(channel_logo_desc_map_t& logo_desc_map);
};

}
