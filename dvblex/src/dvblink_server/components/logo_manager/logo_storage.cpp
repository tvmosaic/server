/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_pugixml_helper.h>
#include <dl_message_addresses.h>
#include <dl_xml_serialization.h>
#include <dl_file_procedures.h>
#include <dl_levenshtein.h>
#include "logo_storage.h"

using namespace dvblex::settings;
using namespace dvblex::playback;
using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;
using namespace dvblink::pugixml_helpers;

namespace dvblex {

static const std::string logo_settings_file_name = "logo_settings.xml";
static const std::string default_logos_file_name = "default_logos.xml";
static const std::string channel_logo_component_name = "channel_logo";

static const time_t logo_invalidate_period_sec = 60*60*24;

logo_storage_t::logo_storage_t(dvblink::messaging::message_queue_t& message_queue, dvblex::settings::installation_settings_t& settings) :
    message_queue_(message_queue), settings_(settings), streaming_port_(0), https_streaming_port_(0)
{
    playback_handler_ = boost::shared_ptr<playback_handler>(new playback_handler(this));

    message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));
    settings_path_ = settings_.get_config_path() / logo_settings_file_name;
    logo_packages_path_ = settings_.get_shared_component_path(channel_logo_component_name);
}

logo_storage_t::~logo_storage_t()
{
}

bool logo_storage_t::start()
{
    load_config();
    load_defaults();
    return true;
}

void logo_storage_t::stop()
{
    //save defaults to a file. This will invalidate and remove old ones
    save_defaults();
}

dvblink::network_port_t logo_storage_t::get_port_from_proto(const dvblink::url_proto_t& proto)
{
    return boost::iequals(proto.get(), "https") ? https_streaming_port_ : streaming_port_;
}

void logo_storage_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channel_logo_request& request, dvblink::messaging::channels::get_channel_logo_response& response)
{
    update_streaming_port();

    //assign default logo's first
    default_channel_logo_map_t::iterator default_it = default_channel_logo_map_.begin();
    while (default_it != default_channel_logo_map_.end())
    {
        response.channel_logo_[default_it->first] = default_it->second.default_logo_;
        ++default_it;
    }

    //add/overwite this settings with assigned logos
    channel_logo_id_map_t::iterator it = channel_logo_map_.begin();
    while (it != channel_logo_map_.end())
    {
        response.channel_logo_[it->first] = generate_url_for_logo(it->second, request.proto_, request.server_address_, get_port_from_proto(request.proto_));;
        ++it;
    }
}

void logo_storage_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::channel_config_changed_request& request)
{
    if (request.changes_.has_epg_config_changes())
    {
        update_default_logos(NULL);
    }
}

void logo_storage_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::epg_updated_request& request)
{
    update_default_logos(&request.epg_source_id_);
}

void logo_storage_t::update_default_logos(const dvblink::epg_source_id_t* epg_source_id)
{
    time_t now;
    time(&now);

    epg::get_channel_epg_config_request req;
    epg::get_channel_epg_config_response resp;
    if (message_queue_->send(epg_manager_message_queue_addressee, req, resp) == success && resp.result_)
    {
        std::map<dvblink::epg_source_id_t, dvblink::epg_source_id_t> epg_sources;
        dvblex::channel_to_epg_source_map_t::iterator ch_it = resp.epg_config_.begin();
        while (ch_it != resp.epg_config_.end())
        {
            if (epg_source_id == NULL || *epg_source_id == ch_it->second.epg_source_id_)
                if (epg_sources.find(ch_it->second.epg_source_id_) == epg_sources.end())
                    epg_sources[ch_it->second.epg_source_id_] = ch_it->second.epg_source_id_;

            ++ch_it;
        }

        if (epg_sources.size() > 0)
        {
            //get channels from each detected epg source
            std::map<dvblink::epg_source_id_t, dvblink::epg_source_id_t>::iterator epg_src_it = epg_sources.begin();
            while (epg_src_it != epg_sources.end())
            {
                epg::get_epg_channels_request epg_ch_req;
                epg::get_epg_channels_response epg_ch_resp;
                if (message_queue_->send(epg_src_it->first.get(), epg_ch_req, epg_ch_resp) == success && epg_ch_resp.result_)
                {
                    ch_it = resp.epg_config_.begin();
                    while (ch_it != resp.epg_config_.end())
                    {
                        if (ch_it->second.epg_source_id_ == epg_src_it->first)
                            if (epg_ch_resp.channels_.find(ch_it->second.epg_channel_id_) != epg_ch_resp.channels_.end())
                            {
                                if (!epg_ch_resp.channels_[ch_it->second.epg_channel_id_].logo_.empty())
                                {
                                    default_channel_logo_map_[ch_it->first].default_logo_ = epg_ch_resp.channels_[ch_it->second.epg_channel_id_].logo_;
                                    default_channel_logo_map_[ch_it->first].default_logo_src_ = epg_src_it->first;
                                    default_channel_logo_map_[ch_it->first].last_update_time_ = now;
                                }
                            }

                        ++ch_it;
                    }
                }

                ++epg_src_it;
            }

            //walk through logo sources and reset default logo for channels not present in epg mappings
            channel_id_list_t ch_to_drop;
            default_channel_logo_map_t::iterator logo_ch_it = default_channel_logo_map_.begin();
            while (logo_ch_it != default_channel_logo_map_.end())
            {
                if (resp.epg_config_.find(logo_ch_it->first) == resp.epg_config_.end())
                {
                    ch_to_drop.push_back(logo_ch_it->first);
                }

                ++logo_ch_it;
            }

            for (size_t i=0; i<ch_to_drop.size(); i++)
                default_channel_logo_map_.erase(ch_to_drop[i]);

            //save resulting list
            save_defaults();
        }
    }
}

void logo_storage_t::set_channel_logos(const channel_logo_id_map_t& channel_logo_map)
{
    channel_logo_map_.clear();

    channel_logo_id_map_t::const_iterator it = channel_logo_map.begin();
    while (it != channel_logo_map.end())
    {
        if (!it->second.empty())
            channel_logo_map_[it->first] = it->second;
        ++it;
    }
    //save resulting list
    save_config();
}

void logo_storage_t::get_logo_packages_list(logo_package_list_t& packages)
{
    namespace fs = boost::filesystem;

	packages.clear();
    boost::filesystem::path dir_path = logo_packages_path_.to_boost_filesystem();

	if ( fs::exists(dir_path) && fs::is_directory(dir_path))
	{
		fs::directory_iterator end_iter;
		for( fs::directory_iterator dir_iter(dir_path) ; dir_iter != end_iter ; ++dir_iter)
		{
			if (fs::is_directory(dir_iter->status()) )
			{
#ifdef WIN32
				logo_package_t package = string_cast<EC_UTF8>(dir_iter->path().filename().wstring());
#else
				logo_package_t package = dir_iter->path().filename().string();
#endif
				packages.push_back(package);
			}
		}
	}
}

void logo_storage_t::get_logos_for_package(const dvblink::url_proto_t& proto, const dvblink::url_address_t& server_addr, const dvblink::logo_package_t& package, channel_package_logo_item_list_t& item_list)
{
	item_list.clear();

	dvblink::filesystem_path_t fpath = logo_packages_path_;
	fpath /= package.get();

	std::vector<boost::filesystem::path> file_list;
    filesystem::find_files(fpath.to_boost_filesystem(), file_list, L"");

	for (size_t i = 0; i < file_list.size(); i++)
	{
        channel_package_logo_item_t item;
        std::string filename;
#ifdef WIN32
        filename = string_cast<EC_UTF8>(file_list[i].filename().wstring());
		item.name_ = string_cast<EC_UTF8>(file_list[i].stem().wstring());
#else
        item.name_ = file_list[i].stem().string();
		filename = file_list[i].filename().string();
#endif
		get_id_for_logo(package, filename, item.id_);

        item.url_ = generate_url_for_logo(item.id_, proto, server_addr, get_port_from_proto(proto));

        item_list.push_back(item);
	}
}

void logo_storage_t::get_id_for_logo(const dvblink::logo_package_t& package, const std::string& logo, dvblink::logo_id_t& logo_id)
{
	logo_id = package.get() + "/" + logo;
}

void logo_storage_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response)
{
    response.result_ = dvblink::xmlcmd_result_error;

	update_streaming_port();

	if (boost::iequals(request.cmd_id_.get(), get_channel_logo_packages_cmd))
    {
        logo_package_list_t packages;
        get_logo_packages_list(packages);

        std::string response_str;
		write_to_xml(packages, response_str);
		response.xml_ = response_str;
        response.result_ = dvblink::xmlcmd_result_success;
    } else
	if (boost::iequals(request.cmd_id_.get(), get_channel_logo_package_items_cmd))
    {
		get_logo_package_items_req req;
		read_from_xml(request.xml_.get(), req);

        channel_package_logo_item_list_t item_list;

        get_logos_for_package(request.proto_, request.server_address_, req.package_, item_list);

        std::string response_str;
		write_to_xml(item_list, response_str);
		response.xml_ = response_str;
        response.result_ = dvblink::xmlcmd_result_success;
    } else
	if (boost::iequals(request.cmd_id_.get(), get_channel_logo_cmd))
    {
        channel_logo_desc_map_t logo_desc_map;

        //assign default logos from channels
        assign_default_logo_from_channels(logo_desc_map);

        //assign/overwrite default logo from epg sources
        default_channel_logo_map_t::iterator default_it = default_channel_logo_map_.begin();
        while (default_it != default_channel_logo_map_.end())
        {
            logo_desc_map[default_it->first].default_logo_ = default_it->second.default_logo_;
            ++default_it;
        }

        //add assigned logos
        channel_logo_id_map_t::iterator it = channel_logo_map_.begin();
        while (it != channel_logo_map_.end())
        {
            logo_desc_map[it->first].id_ = it->second;
            logo_desc_map[it->first].url_ = generate_url_for_logo(it->second, request.proto_, request.server_address_, get_port_from_proto(request.proto_));
            ++it;
        }

        std::string response_str;
		write_to_xml(logo_desc_map, response_str);
		response.xml_ = response_str;
        response.result_ = dvblink::xmlcmd_result_success;
    } else
	if (boost::iequals(request.cmd_id_.get(), set_channel_logo_cmd))
    {
		channel_logo_id_map_t channel_logo_map;
		read_from_xml(request.xml_.get(), channel_logo_map);

        set_channel_logos(channel_logo_map);

        response.result_ = dvblink::xmlcmd_result_success;
    } else
	if (boost::iequals(request.cmd_id_.get(), match_package_logo_cmd))
    {
		match_logo_package_req match_req;
		read_from_xml(request.xml_.get(), match_req);

        channel_package_logo_item_list_t item_list;
        get_logos_for_package(request.proto_, request.server_address_, match_req.package_, item_list);

        channel_logo_match_map_t match_res;
        match_logos_to_channels(item_list, match_req, match_res);

        std::string response_str;
		write_to_xml(match_res, response_str);
		response.xml_ = response_str;

        response.result_ = dvblink::xmlcmd_result_success;
    }
}

void logo_storage_t::assign_default_logo_from_channels(channel_logo_desc_map_t& logo_desc_map)
{
    channels::get_provider_channels_request req;
    channels::get_provider_channels_response resp;
    if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        for (size_t i=0; i<resp.headends_.size(); i++)
        {
            for (size_t j=0; j<resp.headends_[i].transponders_.size(); j++)
            {
                for (size_t k=0; k<resp.headends_[i].transponders_[j].channels_.size(); k++)
                {
                    concise_device_channel_t& ch = resp.headends_[i].transponders_[j].channels_[k];
                    if (!ch.logo_.empty())
                        logo_desc_map[ch.id_].default_logo_ = ch.logo_.get();
                }
            }
        }
    }
}

void logo_storage_t::match_logos_to_channels(const channel_package_logo_item_list_t& item_list, 
    const match_logo_package_req& request, channel_logo_match_map_t& result)
{

    levenshtein_distance_calc distance_calc;

    //walk through the list of requested channels and match them one by one
    for (size_t i=0; i<request.channels_.size(); i++)
    {
        //get device channels
        channels::get_channels_description_request ch_req;
        ch_req.channels_.push_back(request.channels_[i]);
        channels::get_channels_description_response ch_resp;
        if (message_queue_->send(source_manager_message_queue_addressee, ch_req, ch_resp) == success && ch_resp.result_ && ch_resp.channels_desc_.size() > 0)
        {
            channel_logo_match_t match_info;
            const channel_description_t& channel = ch_resp.channels_desc_[0];

            for (size_t j=0; j<item_list.size(); j++)
            {
                levenshtein_distance_result_e res = distance_calc.match(channel.name_.get(), item_list[j].name_.get());

                switch (res)
                {
                case ldr_exact:
                    match_info.exact_match_.push_back(item_list[j].id_);
                    break;
                case ldr_partial:
                    match_info.partial_match_.push_back(item_list[j].id_);
                    break;
                case ldr_none:
                default:
                    break;
                }
            }

            result[request.channels_[i]] = match_info;
        }
    }

}

dvblink::url_address_t logo_storage_t::generate_url_for_logo(const dvblink::logo_id_t& logo_id, const dvblink::url_proto_t& proto, const url_address_t& server_address, const network_port_t& server_port)
{
	object_id_t object_id =  playback::make_object_id(message_queue_->get_id().to_string(), logo_id.to_string());
	return playback::make_object_url(proto.get(), server_address, server_port, object_id.to_string());
}

void logo_storage_t::update_streaming_port()
{
    if (streaming_port_.get() == 0)
    {
        dvblink::messaging::playback::get_playback_streaming_port_request req;
        dvblink::messaging::playback::get_playback_streaming_port_response resp;
        if (message_queue_->send(network_server_message_queue_addressee, req, resp) == success && resp.result_)
        {
            streaming_port_ = resp.port_;
            https_streaming_port_ = resp.https_port_;
        }
    }
}

dvblink::filesystem_path_t logo_storage_t::get_logo_path_from_id(const std::string& id)
{
#ifdef WIN32
	boost::filesystem::path id_path = string_cast<EC_UTF8>(id);
#else
	boost::filesystem::path id_path = id;
#endif
    id_path.make_preferred();

	return logo_packages_path_ / id_path;
}

static boost::int64_t get_filesize(const dvblink::filesystem_path_t& fname)
{
    boost::int64_t ret_val = dvblink::engine::filesystem::get_file_size(fname.to_string());

    if (ret_val < 0)
        ret_val = 0;

    return ret_val;
}

void logo_storage_t::handle(const message_sender_t& sender, const messaging::playback::open_item_request& request, messaging::playback::open_item_response& response)
{
    response.result_ = false;

	std::string source_id;
	std::string object_id;
    if (dvblex::playback::parse_object_id(request.object_id_, source_id, object_id))
    {
        if (boost::iequals(source_id, message_queue_->get_id().to_string()))
        {
            dvblink::filesystem_path_t item_path = get_logo_path_from_id(object_id);

            log_ext_info(L"logo_storage_t::message_handler::handle. Opening file %1%") % item_path.to_wstring();

            //get size
            response.item_size_bytes_ = get_filesize(item_path);

            //open a file
            FILE* f = filesystem::universal_open_file(item_path, "r+b");

            if (f != NULL)
            {
                boost::unique_lock<boost::shared_mutex> lock(file_info_lock_);
                std::string str;
                uuid::gen_uuid(str);
                response.handle_ = str;
                file_info_map_[response.handle_] = f;

                response.result_ = true;
            } else
            {
                log_error(L"logo_storage_t::message_handler::handle. Error opening file %1%") % item_path.to_wstring();
            }
        } else
        {
	        log_error(L"logo_storage_t::message_handler::handle. Request for unknown source id %1%") % string_cast<EC_UTF8>(source_id);
        }
    }
}

void logo_storage_t::handle(const message_sender_t& sender, const messaging::playback::seek_item_request& request, messaging::playback::seek_item_response& response)
{
    response.result_ = false;

    FILE* f = NULL;

    {
        boost::shared_lock<boost::shared_mutex> lock(file_info_lock_);
        if (file_info_map_.find(request.handle_) != file_info_map_.end())
            f = file_info_map_[request.handle_];
    }

    if (f != NULL)
        response.result_ = (filesystem::universal_file_seek(f, request.offset_, SEEK_SET) == 0);
}

void logo_storage_t::handle(const message_sender_t& sender, const messaging::playback::close_item_request& request, messaging::playback::close_item_response& response)
{
    response.result_ = false;

    boost::unique_lock<boost::shared_mutex> lock(file_info_lock_);
    if (file_info_map_.find(request.handle_) != file_info_map_.end())
    {
        fclose(file_info_map_[request.handle_]);
        file_info_map_.erase(request.handle_);
        response.result_ = true;
    }
}

void logo_storage_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_objects_request& request, dvblink::messaging::playback::get_objects_response& response)
{
	response.result_ = false;
	//find out source and object id
    std::string source_id;
    std::string object_id;
    if (parse_object_id(request.object_id_, source_id, object_id))
	{
        std::string src_id = message_queue_->get_id().to_string();
        if (boost::iequals(source_id, src_id))
        {
            if (!request.is_children_request_)
            {
                //object info request
                dvblink::filesystem_path_t item_path = get_logo_path_from_id(object_id);

                boost::shared_ptr<pb_image_t> image_item(new pb_image_t());
                image_item->size_ = get_filesize(item_path);
                image_item->object_id_ = request.object_id_;
                image_item->parent_id_ = src_id;
                image_item->url_ = generate_url_for_logo(object_id, request.proto_, request.server_address_, get_port_from_proto(request.proto_));

                response.object_.item_list_.push_back(image_item);
                response.object_.actual_count_ = response.object_.item_list_.size();
                response.object_.total_count_ = response.object_.item_list_.size();

                response.result_ = true;
            }
        } else
        {
	        log_error(L"logo_storage_t::handle get_objects. Request for unknown source id %1%") % string_cast<EC_UTF8>(source_id);
        }
	} else
	{
		log_error(L"logo_storage_t::handle get_objects. Unable to parse object id %1%") % request.object_id_.to_wstring();
	}
}

bool logo_storage_t::read_data(const object_handle_safe_t& handle, unsigned char* buffer, boost::uint32_t& buffer_length)
{
    bool ret_val = false;

    object_handle_t object_handle(handle.get());

    FILE* f = NULL;

    {
        boost::shared_lock<boost::shared_mutex> lock(file_info_lock_);
        if (file_info_map_.find(object_handle) != file_info_map_.end())
            f = file_info_map_[object_handle];
    }

    if (f != NULL)
    {
        size_t ret = fread((void*)buffer, 1, buffer_length, f);
        //was there an error?
        if (ret < buffer_length)
            ret_val = (ferror(f) == 0);
        else
            ret_val = true;
        //number of actually read bytes
        buffer_length = static_cast<boost::uint32_t>(ret);
    }

    return ret_val;
}

static const std::string logo_manager_root_node = "logos";
static const std::string logo_manager_channel_node = "channel";
static const std::string logo_manager_channel_id_attr = "id";
static const std::string logo_manager_local_logo_attr = "logo";
static const std::string logo_manager_ts_id_attr = "ts";
static const std::string logo_manager_default_logo_attr = "default";
static const std::string logo_manager_default_src_attr = "default_src";

void logo_storage_t::save_config()
{
    //save
    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(logo_manager_root_node.c_str());
    if (root_node != NULL)
    {
        channel_logo_id_map_t::iterator it = channel_logo_map_.begin();
        while (it != channel_logo_map_.end())
        {
            pugi::xml_node ch_node = new_child(root_node, logo_manager_channel_node);
            if (ch_node != NULL)
            {
                add_node_attribute(ch_node, logo_manager_channel_id_attr, it->first.get());

                add_node_attribute(ch_node, logo_manager_local_logo_attr, it->second.get());
            }

            ++it;
        }

        pugixml_helpers::xmldoc_dump_to_file(doc, settings_path_.to_string());
    }
}

void logo_storage_t::load_config()
{
    std::string str;

    channel_logo_map_.clear();

    pugi::xml_document doc;
    if (doc.load_file(settings_path_.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            pugi::xml_node channel_node = root_node.first_child();
            while (channel_node != NULL)
            {
                std::string channel_id;
                if (get_node_attribute(channel_node, logo_manager_channel_id_attr, channel_id))
                {
                    std::string logo;
                    if (get_node_attribute(channel_node, logo_manager_local_logo_attr, logo))
                        channel_logo_map_[channel_id] = logo;
                }

                channel_node = channel_node.next_sibling();
            }
        }
    }
}

///////////////////// default logos

void logo_storage_t::load_defaults()
{
    std::string str;

    time_t now;
    time(&now);

    default_channel_logo_map_.clear();

    dvblink::filesystem_path_t p = logo_packages_path_ / default_logos_file_name;

    pugi::xml_document doc;
    if (doc.load_file(p.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            pugi::xml_node channel_node = root_node.first_child();
            while (channel_node != NULL)
            {
                std::string channel_id;
                if (get_node_attribute(channel_node, logo_manager_channel_id_attr, channel_id))
                {
                    default_channel_logo_t dcl;

                    if (get_node_attribute(channel_node, logo_manager_default_logo_attr, str))
                        dcl.default_logo_ = str;

                    if (get_node_attribute(channel_node, logo_manager_default_src_attr, str))
                        dcl.default_logo_src_ = str;

                    if (get_node_attribute(channel_node, logo_manager_ts_id_attr, str))
                        string_conv::apply(str.c_str(), dcl.last_update_time_, now);

                    default_channel_logo_map_[channel_id] = dcl;
                }

                channel_node = channel_node.next_sibling();
            }
        }
    }
}

void logo_storage_t::save_defaults()
{
    //drop all default logos that have expired (updated more tthan 24 hours ago)
    time_t now;
    time(&now);

    channel_id_list_t ch_to_drop;
    default_channel_logo_map_t::iterator drop_it = default_channel_logo_map_.begin();
    while (drop_it != default_channel_logo_map_.end())
    {
        if (now > drop_it->second.last_update_time_ + logo_invalidate_period_sec)
            ch_to_drop.push_back(drop_it->first);

        ++drop_it;
    }

    for (size_t i=0; i<ch_to_drop.size(); i++)
        default_channel_logo_map_.erase(ch_to_drop[i]);

    //save
    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(logo_manager_root_node.c_str());
    if (root_node != NULL)
    {
        default_channel_logo_map_t::iterator it = default_channel_logo_map_.begin();
        while (it != default_channel_logo_map_.end())
        {
            pugi::xml_node ch_node = new_child(root_node, logo_manager_channel_node);
            if (ch_node != NULL)
            {
                add_node_attribute(ch_node, logo_manager_channel_id_attr, it->first.get());

                add_node_attribute(ch_node, logo_manager_default_logo_attr, it->second.default_logo_.get());

                add_node_attribute(ch_node, logo_manager_default_src_attr, it->second.default_logo_src_.to_string());

                std::stringstream buf;
                buf << it->second.last_update_time_;
                add_node_attribute(ch_node, logo_manager_ts_id_attr, buf.str());
            }

            ++it;
        }

        dvblink::filesystem_path_t p = logo_packages_path_ / default_logos_file_name;
        pugixml_helpers::xmldoc_dump_to_file(doc, p.to_string());
    }
}


} // dvblex
