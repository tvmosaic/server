/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include "settings.h"
#include <dl_epg_storage.h>
#include <dl_parameters.h>
#include <dl_xmltv_processor_pugi.h>

namespace dvblex { 

class xmltv_instance_t : public epg_storage_t
{
public:
    xmltv_instance_t(dvblink::messaging::message_queue_t& message_queue, boost::shared_ptr<xmltv_settings_t>& settings);

    virtual ~xmltv_instance_t();

    virtual bool start();

    virtual bool do_prefetch(epg_source_channel_map_t& epg_channel_map);
    virtual bool download_epg();
    virtual bool get_channel_epg(const dvblink::epg_channel_id_t& channel_id, dvblink::engine::DLEPGEventList& events);
    virtual time_t get_next_update_time(bool bsuccess);
    virtual void cleanup();
    virtual bool get_source_settings(dvblex::parameters_container_t& settings);
    virtual bool set_source_settings(const dvblex::concise_param_map_t& settings);

protected:
    boost::shared_ptr<xmltv_settings_t> settings_;
    epg_source_channel_map_t tmp_epg_channels_map_;
    dvblink::engine::map_epg_channel_to_epg_t temp_epg_channel_events_map_;
    dvblink::engine::xmltv_category_keywords_t temp_keywords_map_;

    bool process_xmltv_feeds();
};

}
