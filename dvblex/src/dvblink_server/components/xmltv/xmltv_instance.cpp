/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#include <boost/filesystem.hpp>
#include <boost/algorithm/string/predicate.hpp>

#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_locale_strings.h>
#include <dl_language_settings.h>
#include <dl_xmltv_file_processor.h>
#include "xmltv_instance.h"

using namespace dvblex::settings;
using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;
using namespace dvblink::tar;

namespace dvblex {

const std::string xmltv_product_name = "XMLTV";
const std::string download_item_key = "xmltv_url";
const size_t download_item_max_num = 5;

xmltv_instance_t::xmltv_instance_t(dvblink::messaging::message_queue_t& message_queue, boost::shared_ptr<xmltv_settings_t>& settings) :
    epg_storage_t(message_queue, xmltv_product_name, settings->get_temp_path()), settings_(settings)
{
    has_settings_ = true;
}

xmltv_instance_t::~xmltv_instance_t()
{
}

bool xmltv_instance_t::start()
{
    bool ret_val = epg_storage_t::start();

    return ret_val;
}

bool xmltv_instance_t::do_prefetch(epg_source_channel_map_t& epg_channel_map)
{
    process_xmltv_feeds();
    epg_channel_map = tmp_epg_channels_map_;

    return true;
}

bool xmltv_instance_t::download_epg()
{
    return true;
}

bool xmltv_instance_t::get_channel_epg(const dvblink::epg_channel_id_t& channel_id, dvblink::engine::DLEPGEventList& events)
{
    bool ret_val = false;

    if (temp_epg_channel_events_map_.find(channel_id) != temp_epg_channel_events_map_.end())
    {
        events = temp_epg_channel_events_map_[channel_id];

        ret_val = true;
    }

    return ret_val;
}

void xmltv_instance_t::cleanup()
{
    tmp_epg_channels_map_.clear();
    temp_epg_channel_events_map_.clear();
    temp_keywords_map_.reset();
}

time_t xmltv_instance_t::get_next_update_time(bool bsuccess)
{
    time_t now;
    time(&now);

    return (now + settings_->get_update_timeout_seconds());
}

bool xmltv_instance_t::process_xmltv_feeds()
{
    cleanup();

    //read epg category map before each xmltv parsing cycle
    const common_file_item_desc_t& cat_defs = settings_->get_xmltv_cat_definitions();
    ReadCategoryKeywords(cat_defs.file_, &temp_keywords_map_);

    xmltv_file_processor xmltv_file_proc(temp_keywords_map_, tmp_epg_channels_map_, temp_epg_channel_events_map_);

    //process xmltv files
	xmltv_file_proc.read_directory(settings_->get_xmltv_input_dir(), NULL, exit_flag_);

    log_info(L"xmltv_instance_t::read_xmltv_files. Finished reading xmltv files");


    //process xmltv download items
    xmltv_download_item_list_t item_list;
    settings_->get_download_item_list(item_list);
    for (size_t i = 0; i<item_list.size() && !exit_flag_; i++)
    {
        if (!item_list[i].empty())
        {
            //check if this is a local file
            if (!xmltv_file_proc.read_xmltv_file(dvblink::filesystem_path_t(item_list[i].get()), NULL, exit_flag_))
            {
                //create temporary directory
                dvblink::filesystem_path_t tempdir = epg_storage_path_;
                boost::uuids::uuid tmp_uuid = boost::uuids::random_generator()();
                tempdir /= boost::lexical_cast<std::string>(tmp_uuid);

                try {
                    boost::filesystem::create_directories(tempdir.to_boost_filesystem());
                } catch(...) {}

                xmltv_file_proc.process_internet_file(item_list[i], tempdir, NULL, exit_flag_);

                //remove temp dir when we are finished
                try {
                    boost::filesystem::remove_all(tempdir.to_boost_filesystem());
                } catch(...) {}
            }
        }
    }
   
    return true;
}

const std::string xmltv_settings_container_id = "42549bae-d053-44e0-99ec-b67a08cc7db3";
const std::string url1_field_key = "26a8e6dcd8e6";
const std::string url2_field_key = "26a8e6dcd8e7";
const std::string update_period_field_key = "26a8e6dcd8e8";

bool xmltv_instance_t::get_source_settings(dvblex::parameters_container_t& settings)
{
    settings.id_ = xmltv_settings_container_id;
    settings.name_ = language_settings::GetInstance()->GetItemName(xmltv_settings_container_name);
    settings.desc_ = language_settings::GetInstance()->GetItemName(xmltv_settings_container_desc);

    //download items

    xmltv_download_item_list_t download_item_list;
    settings_->get_download_item_list(download_item_list);

    //xmltv url
    for (size_t i = 0; i<download_item_max_num; i++)
    {
        std::stringstream strbuf;
        strbuf << download_item_key << (i + 1);
        dvblink::parameter_key_t key(strbuf.str());

        editable_param_element_t xmltv_url_element;
        xmltv_url_element.key_ = key;

        strbuf.clear(); strbuf.str("");
        strbuf << language_settings::GetInstance()->GetItemName(xmltv_url_param_name) << " (" << (i + 1) << ")";
        xmltv_url_element.name_ = strbuf.str();
        xmltv_url_element.format_ = epf_string;
        xmltv_url_element.value_ = i < download_item_list.size() ? download_item_list.at(i).get() : "";

        settings.editable_params_.push_back(xmltv_url_element);
    }

    // update period
    std::stringstream buf;
    buf << (settings_->get_update_timeout_seconds() / 60);

    editable_param_element_t update_element;
    update_element.key_ = update_period_field_key;
    update_element.name_ = language_settings::GetInstance()->GetItemName(xmltv_settings_update_period_field_name);
    update_element.value_ = buf.str();
    update_element.format_ = epf_number;
    settings.editable_params_.push_back(update_element);

    return true;
}

bool xmltv_instance_t::set_source_settings(const dvblex::concise_param_map_t& settings)
{
    //download items
    xmltv_download_item_list_t download_item_list;

    for (size_t i = 0; i<download_item_max_num; i++)
    {
        std::stringstream strbuf;
        strbuf << download_item_key << (i + 1);

        dvblink::parameter_key_t key(strbuf.str());
        if (is_key_present(key, settings))
            download_item_list.push_back(get_value_for_param(key, settings).to_string());
    }

    settings_->set_download_item_list(download_item_list);

    //update period
    if (is_key_present(update_period_field_key, settings))
    {
        int period = atoi(get_value_for_param(update_period_field_key, settings).to_string().c_str());
        if (period > 0)
            settings_->set_update_timeout_seconds(period * 60);
        else
            log_warning(L"xmltv_instance_t::set_source_settings. Invalid update period value specified %1%") % get_value_for_param(update_period_field_key, settings).to_wstring();
    }

    settings_->save();

    return true;
}

} // dvblex
