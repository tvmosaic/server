/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_message_addresses.h>
#include <dl_message_server.h>
#include <components/xmltv.h>
#include "settings.h"
#include "xmltv_instance.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;

namespace dvblex {

xmltv_t::xmltv_t() :
    id_(xmltv_message_queue_addressee.get())
{
    settings_ = boost::shared_ptr<xmltv_settings_t>(new xmltv_settings_t());
}

xmltv_t::~xmltv_t()
{
    server_->unregister_queue(message_queue_->get_id());

    message_queue_->shutdown();
}

i_result xmltv_t::query_interface(const base_id_t& requestor_id, const i_guid& iid, i_base_object_t& obj)
{
    i_result res = i_error;

    //no other interfaces

    return res;
}

bool xmltv_t::init(const dvblink::i_server_t& server)
{
    server_ = server;

    message_queue_ = share_object_safely(new message_queue(id_.get()));
    message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));

    server_->register_queue(message_queue_);

    dvblink::filesystem_path_t disk_path;

    settings_->init(message_queue_);
    settings_->load();

    //create xmltv instance
    xmltv_instance_ = boost::shared_ptr<xmltv_instance_t>(new xmltv_instance_t(message_queue_, settings_));

    return true;
}

void xmltv_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response)
{
    if (!xmltv_instance_->is_started())
        xmltv_instance_->start();

    response.result_ = true;
}

void xmltv_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response)
{
    if (xmltv_instance_->is_started())
        xmltv_instance_->stop();

    response.result_ = true;
}

} // dvblex
