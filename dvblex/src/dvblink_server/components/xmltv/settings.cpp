/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_message_server.h>
#include <dl_message_addresses.h>
#include <dl_pugixml_helper.h>
#include "settings.h"

using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink;
using namespace dvblink::messaging;

namespace dvblex { 

static const std::string xmltv_settings_filename = "xmltv_settings.xml";
static const std::string xmltv_component_name = "xmltv";

static const boost::uint32_t default_update_timeout_sec = 3600*12; //12 hours

xmltv_settings_t::xmltv_settings_t() :
    settings::installation_settings_t()
{
}

xmltv_settings_t::~xmltv_settings_t()
{
}

void xmltv_settings_t::init(dvblink::messaging::message_queue_t& message_queue)
{
    settings::installation_settings_t::init(message_queue);

    config_file_ = get_config_path() / xmltv_settings_filename;

    server::shared_module_path_request shared_req(xmltv_component_name);
    server::shared_module_path_response shared_resp;
    if (message_queue->send(server_message_queue_addressee, shared_req, shared_resp) == success)
        default_xmltv_input_dir_ = shared_resp.path_;
}

void xmltv_settings_t::reset()
{
    xmltv_input_dir_ = default_xmltv_input_dir_;
    update_timeout_sec_ = default_update_timeout_sec;
    download_item_list_.clear();
}

static const std::string xmltv_settings_root                = "xmltv_settings";
static const std::string xmltv_settings_timeout_sec_node      = "update_timeout";
static const std::string xmltv_settings_input_dir_node      = "input_dir";
static const std::string xmltv_settings_download_items_node      = "download_items";
static const std::string xmltv_settings_download_item_node      = "item";
static const std::string xmltv_settings_url_node      = "url";

void xmltv_settings_t::load()
{
    reset();

    std::string str;

    pugi::xml_document doc;
    if (doc.load_file(config_file_.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            if (pugixml_helpers::get_node_value(root_node, xmltv_settings_timeout_sec_node, str))
                string_conv::apply(str.c_str(), update_timeout_sec_, default_update_timeout_sec);

            if (pugixml_helpers::get_node_value(root_node, xmltv_settings_input_dir_node, str))
                xmltv_input_dir_ = str;

            pugi::xml_node items_node = root_node.child(xmltv_settings_download_items_node.c_str());
            if (items_node != NULL)
            {
                pugi::xml_node item_node = items_node.first_child();
                while (item_node != NULL)
                {
                    if (pugixml_helpers::get_node_value(item_node, xmltv_settings_url_node, str))
                        download_item_list_.push_back(str);

                    item_node = item_node.next_sibling();
                }
            }
        }
    }
}

void xmltv_settings_t::save()
{
    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(xmltv_settings_root.c_str());
    if (root_node != NULL)
    {
        std::stringstream buf;
        buf << update_timeout_sec_;
        pugixml_helpers::new_child(root_node, xmltv_settings_timeout_sec_node, buf.str());

        if (!boost::iequals(xmltv_input_dir_.get(), default_xmltv_input_dir_.get()))
            pugixml_helpers::new_child(root_node, xmltv_settings_input_dir_node, xmltv_input_dir_.get());

        pugi::xml_node items_node = pugixml_helpers::new_child(root_node, xmltv_settings_download_items_node);
        if (items_node != NULL)
        {
            for (size_t i=0; i<download_item_list_.size(); i++)
            {
                pugi::xml_node item_node = pugixml_helpers::new_child(items_node, xmltv_settings_download_item_node);
                if (item_node != NULL)
                    pugixml_helpers::new_child(item_node, xmltv_settings_url_node, download_item_list_[i].get());
            }
        }

        pugixml_helpers::xmldoc_dump_to_file(doc, config_file_.to_string());
    }
}

}