/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <dl_installation_settings.h>

namespace dvblex { 

typedef std::vector<dvblink::url_address_t> xmltv_download_item_list_t;

class xmltv_settings_t : public settings::installation_settings_t
{
public:
    xmltv_settings_t();
    ~xmltv_settings_t();

    virtual void init(dvblink::messaging::message_queue_t& message_queue);

    void load();
    void save();

    dvblink::filesystem_path_t get_xmltv_input_dir() const {return xmltv_input_dir_;}
    void set_xmltv_input_dir(const dvblink::filesystem_path_t& xmltv_input_dir) {xmltv_input_dir_ = xmltv_input_dir;}

    boost::uint32_t get_update_timeout_seconds() const {return update_timeout_sec_;}
    void set_update_timeout_seconds(const boost::uint32_t& update_timeout_sec) {update_timeout_sec_ = update_timeout_sec;}

    void get_download_item_list(xmltv_download_item_list_t& download_item_list){download_item_list = download_item_list_;}
    void set_download_item_list(const xmltv_download_item_list_t& download_item_list){download_item_list_ = download_item_list;}

protected:
    dvblink::filesystem_path_t config_file_;
    dvblink::filesystem_path_t xmltv_input_dir_;
    boost::uint32_t update_timeout_sec_;
    xmltv_download_item_list_t download_item_list_;
    dvblink::filesystem_path_t default_xmltv_input_dir_;

    void reset();
};

}
