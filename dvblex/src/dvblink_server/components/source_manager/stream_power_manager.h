/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>
#include <boost/thread.hpp>
#include <dl_types.h>
#include <dl_command_queue.h>
#include <device_manager/device_manager.h>

namespace dvblex { 

class stream_power_manager_t
{
protected:
    enum stream_power_state_e
    {
        sps_standby_enabled,
        sps_standby_disabled
    };

    enum commands_e {e_report_active_device};

    struct s_report_active_device_params
    {
    };

public:
    stream_power_manager_t(boost::shared_ptr<device_manager_t>& device_manager, dvblink::messaging::message_queue_t message_queue);
    ~stream_power_manager_t();

    void init();
    void term();

    void report_active_device();

protected:
    dvblink::messaging::message_queue_t message_queue_;
    dvblink::engine::command_queue command_queue_;
    boost::thread* thread_;
    bool exit_flag_;
    dvblink::client_id_t id_;
    boost::shared_ptr<device_manager_t> device_manager_;

    void start();
    void stop();
    void thread_function();
};

}
