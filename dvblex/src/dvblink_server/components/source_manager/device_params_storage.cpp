/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/algorithm/string.hpp> 
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_pugixml_helper.h>
#include <dl_parameters_serializer.h>
#include "device_params_storage.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::pugixml_helpers;

namespace dvblex { 

device_params_storage_t::device_params_storage_t(source_manager_settings_obj_t& settings) :
    settings_(settings)
{
}

device_params_storage_t::~device_params_storage_t()
{
}

void device_params_storage_t::init()
{
    load();
}

void device_params_storage_t::term()
{
}

void device_params_storage_t::add_device_params(const dvblink::device_id_t& device_id, const concise_param_map_t& params)
{
    device_params_map_[device_id] = params;

    save();
}

void device_params_storage_t::delete_device_params(const dvblink::device_id_t& device_id)
{
    device_params_map_t::iterator it = device_params_map_.find(device_id);
    if (it != device_params_map_.end())
    {
        device_params_map_.erase(it);
        save();
    }
}

void device_params_storage_t::set_manual_devices(const manual_device_list_t& devices)
{
    manual_devices_ = devices;
    save();
}

void device_params_storage_t::reset()
{
    device_params_map_.clear();
    manual_devices_.clear();
}

static const std::string device_params_root_node                 = "device_params";
static const std::string device_params_manual_devices_node         = "manual_devices";
static const std::string device_params_device_node                 = "device";
static const std::string device_params_id_attr                     = "id";
static const std::string device_params_params_node                 = "params";
static const std::string device_params_device_settings_node        = "device_settings";
static const std::string device_params_settings_node        = "settings";

void device_params_storage_t::save()
{
    std::stringstream buf;

    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(device_params_root_node.c_str());
    if (root_node != NULL)
    {
        //manual devices
        pugi::xml_node manual_devices_node = new_child(root_node, device_params_manual_devices_node);
        if (manual_devices_node != NULL)
        {
            for (size_t i=0; i<manual_devices_.size(); i++)
            {
                pugi::xml_node manual_device_node = new_child(manual_devices_node, device_params_device_node);
                if (manual_device_node != NULL)
                {
                    add_node_attribute(manual_device_node, device_params_id_attr, manual_devices_[i].id_.get());
                    write_to_node(manual_devices_[i].params_, manual_device_node);
                }
            }
        }

        //device setttings
        pugi::xml_node device_settings_node = new_child(root_node, device_params_device_settings_node);
        if (device_settings_node != NULL)
        {
            device_params_map_t::iterator it = device_params_map_.begin();
            while (it != device_params_map_.end())
            {
                pugi::xml_node device_node = new_child(device_settings_node, device_params_device_node);
                if (device_node != NULL)
                {
                    add_node_attribute(device_node, device_params_id_attr, it->first.get());
                    concise_param_map_t& cpm  = it->second;

                    write_to_node(cpm, device_node);
                }

                ++it;
            }
        }

        pugixml_helpers::xmldoc_dump_to_file(doc, settings_->get_device_params_filepath().to_string());
    }
}

void device_params_storage_t::load()
{
    reset();

    std::string str;

    pugi::xml_document doc;
    if (doc.load_file(settings_->get_device_params_filepath().to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            //manual devices
            pugi::xml_node manual_devices_node = root_node.child(device_params_manual_devices_node.c_str());
            if (manual_devices_node != NULL)
            {
                pugi::xml_node manual_device_node = manual_devices_node.first_child();
                while (manual_device_node != NULL)
                {
                    manual_device_t md;
                    if (get_node_attribute(manual_device_node, device_params_id_attr, str))
                        md.id_ = str;

                    read_from_node(manual_device_node, md.params_);

                    manual_devices_.push_back(md);

                    manual_device_node = manual_device_node.next_sibling();
                }
            }

            //device setttings
            pugi::xml_node device_settings_node = root_node.child(device_params_device_settings_node.c_str());
            if (device_settings_node != NULL)
            {
                pugi::xml_node device_node = device_settings_node.first_child();
                while (device_node != NULL)
                {
                    if (boost::iequals(device_node.name(), device_params_device_node))
                    {
                        if (get_node_attribute(device_node, device_params_id_attr, str))
                        {
                            concise_param_map_t& cpm = device_params_map_[str];

                            read_from_node(device_node, cpm);
                        }
                    }

                    device_node = device_node.next_sibling();
                }
            }
        }
    }
}

}