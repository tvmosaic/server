/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>
#include <dl_types.h>
#include <device_manager/device_playback_src.h>
#include "settings.h"

namespace dvblex { 

class playback_source_manager_t
{
protected:

    typedef std::map<dvblink::device_id_t, device_playback_src_obj_t> device_to_playback_src_map_t;

public:
    playback_source_manager_t(source_manager_settings_obj_t& settings);
    ~playback_source_manager_t();

    void init();
    void term();

    void add_playback_src_for_device(const dvblink::device_id_t& device_id, const device_playback_src_obj_t& playback_src);
    void remove_playback_src_for_device(const dvblink::device_id_t& device_id);

protected:
    source_manager_settings_obj_t settings_;
    device_to_playback_src_map_t playback_sources_;
};

}
