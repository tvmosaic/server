/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>
#include <boost/thread.hpp>
#include <dl_types.h>
#include <dl_filesystem_path.h>
#include <dl_channels.h>
#include "settings.h"

namespace dvblex { 

typedef std::map<dvblink::headend_id_t, bool> headend_fav_auto_map_t;

class favorites_storage_t
{
public:
    favorites_storage_t(source_manager_settings_obj_t& settings);
    ~favorites_storage_t();

    void init();
    void term();

    bool get_favorites(const dvblink::favorite_id_t& favorite_id, channel_favorite_list_t& favorites);
    void set_favorites(const channel_favorite_list_t& favorites);
    void process_channel_changes(const channel_changes_desc_t& changes, const concise_headend_desc_list_t& headends_channels, const headend_fav_auto_map_t& headend_fav_auto_map);

protected:
    channel_favorite_list_t favorites_;
    source_manager_settings_obj_t settings_;

    void save();
    void load();
    void reset();
    void add_update_channel_type_favorites(channel_type_e ch_type, const concise_headend_desc_list_t& headends_channels);
    void drop_all_auto_favorites();
    void add_origin_favorites(const concise_headend_desc_list_t& headend_channels, const headend_fav_auto_map_t& headend_fav_auto_map);
    void add_headend_favorites(const concise_headend_desc_list_t& headend_channels);
};

}
