/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>
#include <boost/thread.hpp>
#include <dl_types.h>
#include <dl_filesystem_path.h>
#include <dl_channel_info.h>
#include <dl_headend.h>
#include <dl_channels.h>
#include <dl_devices.h>
#include "settings.h"

namespace dvblex { 

//device tuning structure
struct device_channel_tuning_t
{
    concise_param_map_t scanner_settings_;
    device_channel_t device_channel_;
};

typedef std::map<dvblink::device_id_t, device_channel_tuning_t> device_channel_tuning_map_t;

class channel_storage_t
{
    //headends and their channels

    typedef std::map<dvblink::transponder_id_t, transponder_t> transponder_channels_map_t;
    typedef std::map<dvblink::headend_id_t, transponder_channels_map_t> headends_map_t;

    //fast channel lookup

    struct channel_lookup_decriptor_t
    {
        dvblink::headend_id_t headend_id_;
        dvblink::transponder_id_t transponder_id_;
        size_t channel_idx_;
    };

    typedef std::map<dvblink::channel_id_t, channel_lookup_decriptor_t> channel_lookup_map_t;

    //devices
    typedef std::map<dvblink::headend_id_t, concise_param_map_t> device_headend_map_t;

    typedef std::map<dvblink::device_id_t, device_headend_map_t> device_map_t;

public:
    channel_storage_t(source_manager_settings_obj_t& settings);
    ~channel_storage_t();

    void init();
    void term();

    void add_headend_channels(const dvblink::device_id_t& device_id, 
        const headend_info_t& headend_info, const concise_param_map_t& scanner_settings, const transponder_list_t& transponders, channel_changes_desc_t& result);

    bool add_headend_to_device(const dvblink::device_id_t& device_id, const headend_info_t& headend_info, const concise_param_map_t& scanner_settings);

    void get_devices(device_map_to_headend_list_t& devices);
    bool is_device_present(const dvblink::device_id_t& device_id);
    void get_headends(headend_id_list_t& headends);
    bool get_channel_descriptions(const dvblink::headend_id_t& headend_id, concise_transponder_desc_list_t& transponders, const invisible_channel_map_t* invisible_map,
        bool with_channels = true, const dvblink::transponder_id_t* transponder_id = NULL);

    bool get_channel_descriptions(const dvblink::headend_id_t& headend_id, idonly_transponder_desc_list_t& transponders, const invisible_channel_map_t* invisible_map);

    bool get_device_channels(const dvblink::channel_id_t& channel_id, device_channel_tuning_map_t& device_channels);
    void get_headend_desc_for_device(const dvblink::device_id_t& device_id, headend_description_for_device_list_t& pdd_list);
    void get_device_channel_sets(const configurable_device_props_map_t& device_props, ts_source_TO_set_of_set_of_channels_set_t& device_channel_sets);

    void drop_headend_on_device(const dvblink::device_id_t& device_id, const dvblink::headend_id_t& headend_id, channel_changes_desc_t& result);

    bool can_add_concurrent_channel(const dvblink::device_id_t& device_id, const channel_id_list_t& current_channels, const configurable_device_props_t& device_props, 
        const dvblink::channel_id_t& new_channel);

    bool get_channel_descriptor(const dvblink::channel_id_t& channel_id, channel_description_t& channel_desc);
    void get_channel_descriptors(const channel_id_list_t& channels, channel_desc_list_t& channels_desc);

protected:
    channel_lookup_map_t channel_lookup_map_;
    headends_map_t headends_;
    device_map_t devices_;
    source_manager_settings_obj_t settings_;
    boost::shared_mutex lock_;

    void drop_headend_on_device_int(const dvblink::device_id_t& device_id, const dvblink::headend_id_t& headend_id, channel_changes_desc_t& result);
    void save();
    void load();
    void reset();
};

}
