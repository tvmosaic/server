/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/algorithm/string.hpp> 
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_pugixml_helper.h>
#include <dl_locale_strings.h>
#include <dl_language_settings.h>
#include "channel_visibility_storage.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::pugixml_helpers;

namespace dvblex { 

channel_visibility_storage_t::channel_visibility_storage_t(source_manager_settings_obj_t& settings) :
    settings_(settings)
{
}

channel_visibility_storage_t::~channel_visibility_storage_t()
{
}

void channel_visibility_storage_t::init()
{
    load();
}

void channel_visibility_storage_t::term()
{
}

const invisible_channel_map_t* channel_visibility_storage_t::get_invisible_channels()
{
    return &invisible_channels_;
}

void channel_visibility_storage_t::get_invisible_channels(invisible_channel_map_t& invisible_channels)
{
    invisible_channels = invisible_channels_;
}

void channel_visibility_storage_t::set_invisible_channels(const invisible_channel_map_t& invisible_channels, dvblex::channel_changes_desc_t& ccd)
{
    //channels, present in the new map and not present in the existing one will be hidden
    invisible_channel_map_t::const_iterator it = invisible_channels.begin();
    while (it != invisible_channels.end())
    {
        if (invisible_channels_.find(it->first) == invisible_channels_.end())
            ccd.hidden_channels_.push_back(it->first);
        ++it;
    }

    //channels, present in the existing map and not present in the new one will be shown
    it = invisible_channels_.begin();
    while (it != invisible_channels_.end())
    {
        if (invisible_channels.find(it->first) == invisible_channels.end())
            ccd.shown_channels_.push_back(it->first);
        ++it;
    }

    invisible_channels_ = invisible_channels;

    save();
}

void channel_visibility_storage_t::process_channel_changes(const channel_changes_desc_t& changes, const concise_headend_desc_list_t& headend_channels)
{
    //check if any of the invisible channels were deleted
    if (changes.deleted_channels_.size() > 0)
    {
        //create a channel id lookup map from the vector
        std::map<dvblink::channel_id_t, dvblink::channel_id_t> lookup_map;
        for (size_t i=0; i<changes.deleted_channels_.size(); i++)
            lookup_map[changes.deleted_channels_[i]] = changes.deleted_channels_[i];

        //check if any of the invisible channels was deleted
        channel_id_list_t deleted_channels;
        invisible_channel_map_t::iterator it = invisible_channels_.begin();
        while (it != invisible_channels_.end())
        {
            if (lookup_map.find(it->first) != lookup_map.end())
                deleted_channels.push_back(it->first);
            ++it;
        }
        
        //delete old channels
        for (size_t i=0; i<deleted_channels.size(); i++)
            invisible_channels_.erase(deleted_channels[i]);

        save();
    }
}

void channel_visibility_storage_t::hide_new_channels(dvblex::channel_changes_desc_t& ccd)
{
    while (ccd.added_channels_.size() > 0)
    {
        dvblink::channel_id_t& channel_id = ccd.added_channels_.at(0);
        invisible_channels_[channel_id] = channel_id;
        ccd.hidden_channels_.push_back(channel_id);
        //erase this first element from added channels
        ccd.added_channels_.erase(ccd.added_channels_.begin());
    }

    save();
}

void channel_visibility_storage_t::reset()
{
    invisible_channels_.clear();
}

static const std::string invisible_channels_root_node                 = "invisible_channels";
static const std::string invisible_channels_channel_node                 = "channel";
static const std::string invisible_channels_id_attr                     = "id";

void channel_visibility_storage_t::save()
{
    std::stringstream buf;

    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(invisible_channels_root_node.c_str());
    if (root_node != NULL)
    {
        invisible_channel_map_t::iterator it = invisible_channels_.begin();
        while (it != invisible_channels_.end())
        {
            pugi::xml_node ich_node = new_child(root_node, invisible_channels_channel_node);
            if (ich_node != NULL)
                add_node_attribute(ich_node, invisible_channels_id_attr, it->first.get()); 

            ++it;
        }

        pugixml_helpers::xmldoc_dump_to_file(doc, settings_->get_visibility_config_filepath().to_string());
    }
}

void channel_visibility_storage_t::load()
{
    reset();

    std::string str;

    pugi::xml_document doc;
    if (doc.load_file(settings_->get_visibility_config_filepath().to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            pugi::xml_node channel_node = root_node.first_child();
            while (channel_node != NULL)
            {
                if (boost::iequals(channel_node.name(), invisible_channels_channel_node))
                {
                    if (get_node_attribute(channel_node, invisible_channels_id_attr, str))
                        invisible_channels_[str] = str;
                }

                channel_node = channel_node.next_sibling();
            }
        }
    }
}

}