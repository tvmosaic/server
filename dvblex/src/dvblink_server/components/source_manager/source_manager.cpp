/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_message_addresses.h>
#include <dl_message_server.h>
#include <dl_message_epg.h>
#include <dl_language_settings.h>
#include <dl_locale_strings.h>
#include <dl_device_serializer.h>
#include <dl_epg_source_commands.h>
#include <components/source_manager.h>
#include <device_manager/device_manager.h>
#include "stream_power_manager.h"
#include "channel_storage.h"
#include "favorites_storage.h"
#include "channel_overwrites_storage.h"
#include "channel_visibility_storage.h"
#include "device_params_storage.h"
#include "playback_sources.h"
#include "settings.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;

namespace dvblex {

source_manager_t::source_manager_t() :
    id_(source_manager_message_queue_addressee.get())
{
}

source_manager_t::~source_manager_t()
{
    playback_source_manager_.reset();
    device_params_storage_.reset();
    channel_overwrites_storage_.reset();
    channel_visibility_storage_.reset();
    favorites_storage_.reset();
    channel_storage_.reset();
    stream_power_manager_.reset();
    device_manager_.reset();
    
	server_->unregister_object(id_.get());

    server_->unregister_queue(message_queue_->get_id());

    message_queue_->shutdown();
}

i_result source_manager_t::query_interface(const base_id_t& requestor_id, const i_guid& iid, i_base_object_t& obj)
{
    i_result res = i_error;

    if (iid == stream_source_control_interface)
    {
	    obj = stream_handler_->shared_from_this();
        res = i_success;
    }
    else
    {
        res = i_not_found;
    }

    return res;
}

bool source_manager_t::init(const dvblink::i_server_t& server)
{
    server_ = server;

    stream_handler_ = boost::shared_ptr<stream_handler>(new stream_handler(this));

	//register interface for queryinterface() functionality
	server_->register_object(this->shared_from_this());

    message_queue_ = share_object_safely(new message_queue(id_.get()));
    message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));

    server_->register_queue(message_queue_);

    settings_ = boost::shared_ptr<source_manager_settings_t>(new source_manager_settings_t());
    settings_->init(message_queue_);

    boost::shared_ptr<dvblex::settings::installation_settings_t> is = settings_;
    device_manager_ = boost::shared_ptr<device_manager_t>(new device_manager_t(is));
    channel_storage_ = boost::shared_ptr<channel_storage_t>(new channel_storage_t(settings_));
    favorites_storage_ = boost::shared_ptr<favorites_storage_t>(new favorites_storage_t(settings_));
    channel_visibility_storage_ = boost::shared_ptr<channel_visibility_storage_t>(new channel_visibility_storage_t(settings_));
    channel_overwrites_storage_ = boost::shared_ptr<channel_overwrites_storage_t>(new channel_overwrites_storage_t(settings_));
    device_params_storage_ = boost::shared_ptr<device_params_storage_t>(new device_params_storage_t(settings_));
    stream_power_manager_ = boost::shared_ptr<stream_power_manager_t>(new stream_power_manager_t(device_manager_, message_queue_));
    playback_source_manager_ = boost::shared_ptr<playback_source_manager_t>(new playback_source_manager_t(settings_));

    return true;
}

void source_manager_t::add_playback_src_for_device(const dvblink::device_id_t& device_id, const headend_id_list_t& headend_list)
{
    //fill in the playback src init structure
    device_playback_src_init_list_t device_playback_src_init_list;
    for (size_t i=0; i<headend_list.size(); i++)
    {
        device_playback_src_init_item_t init_item;

        headend_info_t headend_info;
        if (find_headend_info(device_id, headend_list[i], init_item.headend_info_))
        {
            if (device_manager_->get_provider_details(init_item.headend_info_.provider_id_, init_item.headend_info_.device_config_params_, 
                init_item.provider_info_, init_item.scan_list_))
            {
                device_playback_src_init_list.push_back(init_item);
            } else
            {
                log_error(L"source_manager_t::add_playback_src_for_device. Failed to get provider details for provider %1%") % init_item.headend_info_.provider_id_.to_wstring();
            }
        } else
        {
            log_error(L"source_manager_t::add_playback_src_for_device. Failed to get headend info for headend %1%") % headend_list[i].to_wstring();
        }
    }

    device_playback_src_obj_t playback_src = device_manager_->get_playback_src_for_device(device_id);
    if (playback_src != NULL)
    {
        //drop playback source for this device if it exists already
        playback_source_manager_->remove_playback_src_for_device(device_id);

        if (playback_src->init(device_id, device_playback_src_init_list, server_))
        {
            //add a new one
            playback_source_manager_->add_playback_src_for_device(device_id, playback_src);
        } else
        {
            log_error(L"source_manager_t::add_playback_src_for_device. Failed to initialize playback source for device %1%") % device_id.to_wstring();
            playback_src.reset();
        }
    }
}

void source_manager_t::init_device_manager()
{
    //init device params storage
    device_params_storage_->init();

    //initialize device manager
    manual_device_list_t& manual_devices = device_params_storage_->get_manual_devices();
    device_manager_->init(manual_devices);

    //apply device params
    device_params_storage_t::device_params_map_t& device_params = device_params_storage_->get_all_params();
    device_params_storage_t::device_params_map_t::iterator dp_it = device_params.begin();
    while (dp_it != device_params.end())
    {
        device_manager_->set_device_configurable_props(dp_it->first, dp_it->second);
        ++dp_it;
    }

    //init playback sources manager
    playback_source_manager_->init();

    //add playback sources for all devices from channel storage
    device_map_to_headend_list_t devices;
    channel_storage_->get_devices(devices);

    device_map_to_headend_list_t::iterator dev_it = devices.begin();
    while (dev_it != devices.end())
    {
        headend_id_list_t headend_id_list;
        for (size_t i=0; i<dev_it->second.size(); i++)
            headend_id_list.push_back(dev_it->second.at(i).headend_id_);

        add_playback_src_for_device(dev_it->first, headend_id_list);

        ++dev_it;
    }
}

void source_manager_t::term_device_manager()
{
    //terminate playback sources manager
    playback_source_manager_->term();

    //term device manager
    device_manager_->term();

    //term device params storage
    device_params_storage_->term();
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response)
{
    //initialize channel storage
    channel_storage_->init();

    //initialize favorites storage
    favorites_storage_->init();

    //init visibility storage
    channel_visibility_storage_->init();

    //init overwrites storage
    channel_overwrites_storage_->init();

    //initialize device manager
    init_device_manager();

    //initialize stream power manager
    stream_power_manager_->init();

    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response)
{

    //stop stream power manager
    stream_power_manager_->term();

    //stop device manager
    term_device_manager();

    //stop overwrites storage
    channel_overwrites_storage_->term();

    //stop channel visibility storage
    channel_visibility_storage_->term();

    //stop favorites storage
    favorites_storage_->term();

    //stop channel storage
    channel_storage_->term();

    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::standby_request& request, dvblink::messaging::standby_response& response)
{
    //stop device manager
    term_device_manager();

    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::resume_request& request, dvblink::messaging::resume_response& response)
{
    //initialize device manager
    init_device_manager();

    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::devices_info_request& request, dvblink::messaging::devices::devices_info_response& response)
{
    response.result_ = false;

    device_descriptor_list_t device_list;
    if (device_manager_->get_device_list(dvblink::device_uuid_t(), device_list))
    {
        response.result_ = true;

        //get device list from channel storage
        device_map_to_headend_list_t devices;
        channel_storage_->get_devices(devices);

        for (size_t i=0; i<device_list.size(); i++)
        {
            device_info_t di;
            di.descriptor_ = device_list[i];
            di.state_ = dis_new;
            
            if (devices.find(di.descriptor_.id_) != devices.end())
            {
                di.state_ = dis_active;
                //add providers of this device
                for (size_t j=0; j<devices[di.descriptor_.id_].size(); j++)
                {
                    headend_info_t headend_info;
                    if (device_manager_->get_headend_info(devices[di.descriptor_.id_].at(j), headend_info))
                        di.headends_.push_back(headend_info);
                }

                devices.erase(di.descriptor_.id_);
            }
            response.device_info_list_.push_back(di);
        }
        //all devices that are still in devices map have missing status
        device_map_to_headend_list_t::iterator it = devices.begin();
        while (it != devices.end())
        {
            device_info_t di;
            di.descriptor_.id_ = it->first;
            di.descriptor_.name_ = language_settings::GetInstance()->GetItemName(unknown_device_name);
            di.state_ = dis_missing;

            //add providers of this device
            for (size_t j=0; j<it->second.size(); j++)
            {
                headend_info_t headend_info;
                if (device_manager_->get_headend_info(it->second.at(j), headend_info))
                    di.headends_.push_back(headend_info);
            }

            response.device_info_list_.push_back(di);

            ++it;
        }
    } else
    {
        log_error(L"source_manager_t::handle devices_info_request. get_device_list failed");
    }
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_scanners_request& request, dvblink::messaging::devices::get_scanners_response& response)
{
    dvblink::standard_set_t standard = request.standard_;
    if (standard == st_unknown)
    {
        device_descriptor_t device_info;
        if (device_manager_->get_device_info(request.device_id_, device_info))
            standard = device_info.supported_standards_;
        else
            log_warning(L"source_manager_t::handle get_scanners_request. device_manager_->get_device_info returned error for device %1%") % string_cast<EC_UTF8>(request.device_id_.get());
    }
    device_manager_->get_providers(standard, response.providers_);
    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::start_scan_request& request, dvblink::messaging::devices::start_scan_response& response)
{
    response.scan_start_result_ = cssr_error;

    concise_param_map_t::const_iterator it;
    if ((it = request.parameters_.find(devices_provider_tag)) != request.parameters_.end())
    {
        //first scan
        provider_id_t provider_id = it->second.get();
        provider_info_t provider_info;
        provider_scan_list_t scan_list;
        if (device_manager_->get_provider_details(provider_id, request.parameters_, provider_info, scan_list))
        {
            bool b_already_scanned = false;
            //check if channels of this provider have been scanned already
            headend_info_t headend_info;
            if (device_manager_->get_headend_from_provider(provider_info, request.parameters_, headend_info))
                b_already_scanned = channel_storage_->add_headend_to_device(request.device_id_, headend_info, request.parameters_);

            if (b_already_scanned)
            {
                //create shared dir for this device
                device_descriptor_t dd;
                if (device_manager_->get_device_info(request.device_id_, dd))
                    device_manager_->create_shared_dir_for_device(dd);

                response.scan_start_result_ = cssr_scan_joined;
            } else
            {
                response.scan_start_result_ = device_manager_->scan_start(request.device_id_, provider_info, scan_list, request.parameters_) ? cssr_scan_started : cssr_error;
            }
        } else
        {
            log_error(L"source_manager_t::handle start_scan_request. device_manager_->get_provider_details return error");
        }
    } else
    if ((it = request.parameters_.find(devices_network_tag)) != request.parameters_.end())
    {
        //selected network scan
        scan_network_id_t network_id = it->second.get();
        response.scan_start_result_ = device_manager_->scan_network(request.device_id_, network_id) ? cssr_scan_started : cssr_error;
    } else
    {
        log_error(L"source_manager_t::handle start_scan_request. invalid parameters");
    }
}

bool source_manager_t::find_headend_info(const dvblink::device_id_t& device_id, const dvblink::headend_id_t& headend_id, headend_info_t& headend_info)
{
    bool ret_val = false;

    headend_description_for_device_list_t pdd_list;
    channel_storage_->get_headend_desc_for_device(device_id, pdd_list);

    headend_description_for_device_t headend;
    bool bfound = false;
    //find requested provider
    for (size_t i=0; i<pdd_list.size(); i++)
    {
        if (boost::iequals(pdd_list[i].headend_id_.get(), headend_id.get()))
        {
            headend = pdd_list[i];
            bfound = true;
            break;
        }
    }

    if (bfound)
    {
        ret_val = device_manager_->get_headend_info(headend, headend_info);
    } else
    {
        log_error(L"source_manager_t::find_headend_info. headend id %1% not found on device %2%") % headend_id.to_wstring() % device_id.to_wstring();
    }
    return ret_val;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_rescan_settings_request& request, dvblink::messaging::devices::get_rescan_settings_response& response)
{
    response.result_ = false;

    headend_info_t headend_info;
    if (find_headend_info(request.device_id_, request.headend_id_, headend_info))
    {
        response.result_ = device_manager_->get_rescan_settings(headend_info.provider_id_, headend_info.device_config_params_, response.settings_);
    }
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::rescan_provider_request& request, dvblink::messaging::devices::rescan_provider_response& response)
{
    response.result_ = false;

    headend_info_t headend_info;
    if (find_headend_info(request.device_id_, request.headend_id_, headend_info))
    {
        provider_info_t provider_info;
        provider_scan_list_t scan_list;
        if (device_manager_->get_provider_details(headend_info.provider_id_, headend_info.device_config_params_, provider_info, scan_list))
        {
            //merge new and existing parameters map
            concise_param_map_t merged_settings = headend_info.device_config_params_;
            concise_param_map_t::const_iterator it = request.settings_.begin();
            while (it != request.settings_.end())
            {
                merged_settings[it->first] = it->second;
                ++it;
            }
            
            response.result_ = device_manager_->scan_start(request.device_id_, provider_info, scan_list, merged_settings);
        } else
        {
            log_error(L"source_manager_t::handle rescan_provider_request. device_manager_->get_provider_details return error");
        }
    } else
    {
        log_error(L"source_manager_t::handle rescan_provider_request. device_manager_->get_headend_info return error");
    }
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::cancel_scan_request& request, dvblink::messaging::devices::cancel_scan_response& response)
{
    response.result_ = device_manager_->scan_cancel(request.device_id_);
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_networks_request& request, dvblink::messaging::devices::get_networks_response& response)
{
    response.result_ = device_manager_->scan_get_networks(request.device_id_, response.networks_);
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_device_status_request& request, dvblink::messaging::devices::get_device_status_response& response)
{
    response.result_ = device_manager_->get_device_status(request.device_id_, response.status_.device_status_);
    if (response.result_)
    {
        if (response.status_.device_status_.state_ == ds_channel_scan_in_progress ||
            response.status_.device_status_.state_ == ds_networks_scanned || response.status_.device_status_.state_ == ds_channel_scan_finished)
        {
            response.result_ = device_manager_->scan_get_log(request.device_id_, response.status_.scan_log_);
        } else 
        if (response.status_.device_status_.state_ == ds_streaming)
        {
            //fill in channel names for streamed channels
            for (size_t i=0; i<response.status_.device_status_.stream_channels_.size(); i++)
            {
                stream_channel_stats_t& sch = response.status_.device_status_.stream_channels_[i];

                channel_description_t device_channel;
                if (channel_storage_->get_channel_descriptor(sch.channel_id_, device_channel))
                    sch.channel_name_ = device_channel.name_;
            }
        }
    }
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::apply_scan_request& request, dvblink::messaging::devices::apply_scan_response& response)
{
    response.result_ = false;
    device_state_e state = device_manager_->get_device_state(request.device_id_);
    if (state == ds_channel_scan_finished)
    {
        headend_info_t headend_info;
        transponder_list_t channels;
        headend_description_for_device_t pdd;
        if (device_manager_->scan_get_channels(request.device_id_, headend_info, pdd.device_config_params_, channels))
        {
            pdd.headend_id_ = headend_info.id_;

            dvblink::messaging::channels::channel_config_changed_request req;

            //check and remove conflicting providers for this device
            headend_description_for_device_list_t pdd_list;
            channel_storage_->get_headend_desc_for_device(request.device_id_, pdd_list);

            headend_id_list_t conflicting_headends;
            device_manager_->find_conflicting_headends(request.device_id_, pdd_list, pdd, conflicting_headends);
            for (size_t i=0; i<conflicting_headends.size(); i++)
            {
                log_info(L"source_manager_t::handle apply_scan_request. Dropping conflicting headend %1% on device %2%") % conflicting_headends[i].to_wstring() % request.device_id_.to_wstring();
                channel_storage_->drop_headend_on_device(request.device_id_, conflicting_headends[i], req.changes_);
            }

            //add/update provider on device
            channel_storage_->add_headend_channels(request.device_id_, headend_info, pdd.device_config_params_, channels, req.changes_);
            //reset device to idle state
            device_manager_->reset_device(request.device_id_);

            //create shared dir for this device
            device_descriptor_t dd;
            if (device_manager_->get_device_info(request.device_id_, dd))
                device_manager_->create_shared_dir_for_device(dd);

            //process "hide new channels" flag
            dvblink::parameter_value_t val = get_value_for_param(provider_hide_new_channels_on_rescan_key, pdd.device_config_params_);
            bool hide_new_channels = boost::iequals(val.to_string(), PARAM_VALUE_YES);

            if (hide_new_channels)
                channel_visibility_storage_->hide_new_channels(req.changes_);

            if (req.changes_.has_changes())
                message_queue_->post(dvblink::messaging::broadcast_addressee, req);

            //re-read headend info for this device to add its playback sources
            channel_storage_->get_headend_desc_for_device(request.device_id_, pdd_list);

            //add playback source for a new/updated device
            headend_id_list_t headend_id_list;
            for (size_t i = 0; i<pdd_list.size(); i++)
                headend_id_list.push_back(pdd_list.at(i).headend_id_);

            add_playback_src_for_device(request.device_id_, headend_id_list);

            response.result_ = true;
        }
    } else
    {
        log_error(L"source_manager_t::handle apply_scan_request. Invalid device state %1%") % state;
    }
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::drop_headend_on_device_request& request, dvblink::messaging::devices::drop_headend_on_device_response& response)
{
    dvblink::messaging::channels::channel_config_changed_request req;
    channel_storage_->drop_headend_on_device(request.device_id_, request.headend_id_, req.changes_);

    //remove playback source if device is not used anymore
    if (!channel_storage_->is_device_present(request.device_id_))
        playback_source_manager_->remove_playback_src_for_device(request.device_id_);

    if (req.changes_.has_changes())
        message_queue_->post(dvblink::messaging::broadcast_addressee, req);

    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_device_settings_request& request, dvblink::messaging::devices::get_device_settings_response& response)
{
    configurable_device_props_t props;
    response.result_ = device_manager_->get_device_configurable_props(request.device_id_, props);
    response.settings_ = props.param_container_;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::set_device_settings_request& request, dvblink::messaging::devices::set_device_settings_response& response)
{
    set_device_props_result_e result = device_manager_->set_device_configurable_props(request.device_id_, request.settings_);
    response.result_ = (result != sdpr_error);

    //save new device settings
    device_params_storage_->add_device_params(request.device_id_, request.settings_);

    //initialize new epg scan if needed
    if ((result & sdpr_epg_rescan_needed) != 0)
    {
        xml_message_request req;
        req.cmd_id_ = epg_source_refresh_cmd;

        xml_message_response resp;

        message_queue_->send(epg_manager_message_queue_addressee, req, resp);
    }
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::delete_manual_device_request& request, dvblink::messaging::devices::delete_manual_device_response& response)
{
    response.result_ = device_manager_->delete_manual_device(request.device_id_);
    if (response.result_)
    {
        //update manual devices list
        manual_device_list_t manual_devices;
        device_manager_->get_manual_devices(manual_devices);
        device_params_storage_->set_manual_devices(manual_devices);
        //update device properties
        device_params_storage_->delete_device_params(request.device_id_);
        //update channel storage
        dvblink::messaging::channels::channel_config_changed_request req;
        device_map_to_headend_list_t devices;
        channel_storage_->get_devices(devices);
        device_map_to_headend_list_t::iterator device_it = devices.find(request.device_id_);
        if (device_it != devices.end())
        {
            //drop all providers from this device
            headend_description_for_device_list_t& headend_list = device_it->second;
            for (size_t i=0; i<headend_list.size(); i++)
                channel_storage_->drop_headend_on_device(request.device_id_, headend_list[i].headend_id_, req.changes_);
        }

        //remove playback source for this device
        playback_source_manager_->remove_playback_src_for_device(request.device_id_);

        if (req.changes_.has_changes())
            message_queue_->post(dvblink::messaging::broadcast_addressee, req);
    }
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::create_manual_device_request& request, dvblink::messaging::devices::create_manual_device_response& response)
{
    response.result_ = device_manager_->create_manual_device(request.params_);
    if (response.result_)
    {
        manual_device_list_t manual_devices;
        device_manager_->get_manual_devices(manual_devices);
        device_params_storage_->set_manual_devices(manual_devices);
    }
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_device_templates_request& request, dvblink::messaging::devices::get_device_templates_response& response)
{
    device_manager_->get_device_template_list(response.templates_);
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_device_headend_info_request& request, dvblink::messaging::channels::get_device_headend_info_response& response)
{
    channel_storage_->get_devices(response.devices_);
    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_scanned_channels_request& request, dvblink::messaging::channels::get_scanned_channels_response& response)
{
    get_headend_channels(request.headend_id_, response.headends_, NULL, request.info_level_, &request.transponder_id_);
    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_provider_channels_request& request, dvblink::messaging::channels::get_provider_channels_response& response)
{
    get_headend_channels(request.headend_id_, response.headends_, channel_visibility_storage_->get_invisible_channels());
    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_provider_channels_id_request& request, dvblink::messaging::channels::get_provider_channels_id_response& response)
{
    get_headend_channels(request.headend_id_, response.headends_, channel_visibility_storage_->get_invisible_channels());
    response.result_ = true;
}

void source_manager_t::get_headend_channels(const headend_id_t& headend_id, concise_headend_desc_list_t& headends_channels, const invisible_channel_map_t* invisible_map, 
        int info_level, const dvblink::transponder_id_t* transponder_id)
{
    std::map<dvblink::headend_id_t, headend_description_for_device_t> headend_description_map;

    device_map_to_headend_list_t devices;
    channel_storage_->get_devices(devices);

    device_map_to_headend_list_t::iterator it = devices.begin();
    while (it != devices.end())
    {
        for (size_t i=0; i<it->second.size(); i++)
            headend_description_map[it->second.at(i).headend_id_] = it->second.at(i);

        ++it;
    }

    std::map<dvblink::headend_id_t, headend_description_for_device_t>::iterator hd_it = headend_description_map.begin();
    while (hd_it != headend_description_map.end())
    {
        headend_description_for_device_t& hdd = hd_it->second;

        if (headend_id.empty() || boost::iequals(hdd.headend_id_.get(), headend_id.get()))
        {
            concise_channel_headend_t cp;
            cp.id_ = hdd.headend_id_;
            headend_info_t headend_info;
            if (device_manager_->get_headend_info(hdd, headend_info))
            {
                cp.name_ = headend_info.name_;
                cp.desc_ = headend_info.desc_;
            }

            if (info_level >= channels::get_scanned_channels_level_transponders)
            {
                channel_storage_->get_channel_descriptions(hdd.headend_id_, cp.transponders_, invisible_map, 
                    info_level == channels::get_scanned_channels_level_channels, transponder_id);
            }

            headends_channels.push_back(cp);
        }

        ++hd_it;
    }
}

void source_manager_t::get_headend_channels(const headend_id_t& headend_id, idonly_headend_desc_list_t& headend_channels, const invisible_channel_map_t* invisible_map)
{
    headend_id_list_t headends;
    channel_storage_->get_headends(headends);

    for (size_t i=0; i<headends.size(); i++)
    {
        if (headend_id.empty() || boost::iequals(headends[i].get(), headend_id.get()))
        {
            idonly_channel_headend_t cp;
            cp.id_ = headends[i];
            channel_storage_->get_channel_descriptions(headends[i], cp.transponders_, invisible_map);
            headend_channels.push_back(cp);
        }
    }
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channels_description_request& request, dvblink::messaging::channels::get_channels_description_response& response)
{
    channel_storage_->get_channel_descriptors(request.channels_, response.channels_desc_);
    //apply overwrites
    channel_overwrites_storage_->apply_overwrites(response.channels_desc_);

    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channel_favorites_request& request, dvblink::messaging::channels::get_channel_favorites_response& response)
{
    response.result_ = favorites_storage_->get_favorites(request.favorite_id_, response.favorites_);
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::set_channel_favorites_request& request, dvblink::messaging::channels::set_channel_favorites_response& response)
{
    favorites_storage_->set_favorites(request.favorites_);
    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channel_overwrites_request& request, dvblink::messaging::channels::get_channel_overwrites_response& response)
{
    channel_overwrites_storage_->get_channel_overwrites(response.overwrites_);
    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::set_channel_overwrites_request& request, dvblink::messaging::channels::set_channel_overwrites_response& response)
{
    channel_overwrites_storage_->set_channel_overwrites(request.overwrites_);
    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::channel_config_changed_request& request)
{
    //get device map 
    device_map_to_headend_list_t device_map;
    channel_storage_->get_devices(device_map);

    //get all headends with channels
    headend_id_t headend_id;
    concise_headend_desc_list_t headend_channels;
    get_headend_channels(headend_id, headend_channels, channel_visibility_storage_->get_invisible_channels());

    //get per-headend flag if a favorite has to be created for channel origin
    headend_fav_auto_map_t headend_fav_auto_map;
    for (size_t i=0; i<headend_channels.size(); i++)
    {
        dvblink::headend_id_t& headend_id = headend_channels[i].id_;
        bool bcreate_favorite = false;

        device_map_to_headend_list_t::iterator it = device_map.begin();
        while (it != device_map.end() && !bcreate_favorite)
        {
            for (size_t j=0; j<it->second.size(); j++)
            {
                if (boost::iequals(it->second.at(j).headend_id_.get(), headend_id.get()))
                {
                    dvblink::parameter_value_t val = get_value_for_param(provider_auto_origin_favorites_key, it->second.at(j).device_config_params_);
                    bcreate_favorite = boost::iequals(val.to_string(), PARAM_VALUE_YES);
                    break;
                }
            }
            ++it;
        }

        headend_fav_auto_map[headend_id] = bcreate_favorite;
    }

    favorites_storage_->process_channel_changes(request.changes_, headend_channels, headend_fav_auto_map);
    channel_visibility_storage_->process_channel_changes(request.changes_, headend_channels);
    channel_overwrites_storage_->process_channel_changes(request.changes_);
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_channel_visibility_request& request, dvblink::messaging::channels::get_channel_visibility_response& response)
{
    channel_visibility_storage_->get_invisible_channels(response.channel_visibility_);
    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::set_channel_visibility_request& request, dvblink::messaging::channels::set_channel_visibility_response& response)
{
    dvblink::messaging::channels::channel_config_changed_request req;
    channel_visibility_storage_->set_invisible_channels(request.channel_visibility_, req.changes_);

    if (req.changes_.has_changes())
        message_queue_->post(dvblink::messaging::broadcast_addressee, req);

    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::get_device_channel_sets_request& request, dvblink::messaging::channels::get_device_channel_sets_response& response)
{
    //build configurable props map first
    device_map_to_headend_list_t devices;
    channel_storage_->get_devices(devices);

    configurable_device_props_map_t device_props_map;
    device_map_to_headend_list_t::iterator it = devices.begin();
    while (it != devices.end())
    {
        configurable_device_props_t props;
        if (device_manager_->get_device_configurable_props(it->first, props))
            device_props_map[it->first] = props;
        ++it;
    }
    
    channel_storage_->get_device_channel_sets(device_props_map, response.device_channel_sets_);
    response.result_ = true;
}

static bool compare_streaming_statuses_func(const device_status_t& first_item, const device_status_t& second_item)
{
    size_t i1 = first_item.max_priority_*100 + first_item.stream_channels_.size();
    size_t i2 = second_item.max_priority_*100 + second_item.stream_channels_.size();
	return i1 < i2;
}

dvblink::EStatus source_manager_t::start_channel_int(const char* channel_id, streamer_object_t& streamer, void* device_channel_map, dvblink::device_id_t& device_id)
{
    dvblink::EStatus ret_val = DvbLink_StatusError;

    device_channel_tuning_map_t& device_channels = *((device_channel_tuning_map_t*)device_channel_map);

    concise_channel_tune_info_t tune_info;
    device_status_list_t device_status_list;

    device_channel_tuning_map_t::iterator device_it = device_channels.begin();
    while (device_it != device_channels.end())
    {
        const device_channel_tuning_t& channel = device_it->second;
        tune_info.tuning_params_ = channel.device_channel_.tune_params_;
        tune_info.scanner_settings_ = channel.scanner_settings_;

        device_status_t device_status;
        if (device_manager_->get_device_status(device_it->first, device_status) && device_status.state_ != ds_channel_scan_in_progress)
        {
            //first check if this channel is already streamed
            if (device_manager_->is_channel_present_on_device(device_it->first, channel_id))
            {
                device_id = device_it->first;
                log_info(L"source_manager_t::start_channel. Channel %1% is already present on device %2%") % string_cast<EC_UTF8>(channel_id) % string_cast<EC_UTF8>(device_id.get());
                break;
            } else
            {
                //check if new request can join the ongoing streaming on this device
                if (device_manager_->can_join_streaming(device_it->first, tune_info))
                {
                    configurable_device_props_t dp;
                    device_manager_->get_device_configurable_props(device_it->first, dp);
                    //check number of concurrent channels on this device
                    channel_id_list_t current_channels;
                    for (size_t i=0; i<device_status.stream_channels_.size(); i++)
                        current_channels.push_back(device_status.stream_channels_[i].channel_id_);

                    if (channel_storage_->can_add_concurrent_channel(device_it->first, current_channels, dp, channel_id))
                    {
                        device_id = device_it->first;
                        log_info(L"source_manager_t::start_channel. Channel %1% can join streaming on device %2%") % string_cast<EC_UTF8>(channel_id) % string_cast<EC_UTF8>(device_id.get());
                        break;
                    }
                }
            }

            //add device status to the selection list, ignoring actively scanning devices
            device_status_list.push_back(device_status);
        } else
        {
            log_info(L"source_manager_t::start_channel. get_device_status failed on device %1% or device is scanning channels") % string_cast<EC_UTF8>(device_id.get());
        }
        ++device_it;
    }

    if (device_id.empty())
    {
        //find first idle device
        for (size_t i=0; i<device_status_list.size(); i++)
        {
            if (device_status_list[i].state_ == ds_none ||
                device_status_list[i].state_ == ds_idle ||
                device_status_list[i].state_ == ds_networks_scanned ||
                device_status_list[i].state_ == ds_channel_scan_finished ||
                device_status_list[i].state_ == ds_epg_scan_in_progress ||
                device_status_list[i].state_ == ds_epg_scan_finished)
            {
                device_id = device_status_list[i].id_;

                const device_channel_tuning_t& channel = device_channels[device_id];
                tune_info.tuning_params_ = channel.device_channel_.tune_params_;
                tune_info.scanner_settings_ = channel.scanner_settings_;

                log_info(L"source_manager_t::start_channel. Channel %1% will start on device %2% (%3%)") % string_cast<EC_UTF8>(channel_id) % string_cast<EC_UTF8>(device_id.get()) % device_status_list[i].state_;

                break;
            }
        }
        //if device is still not found - check if we can disconnect one of the streaming devices, based on request priority
        //device_status_list has now only streaming devices
        if (device_id.empty())
        {
            //list is sorted on priority (first) and number of concurrent channels (second)
            std::sort(device_status_list.begin(), device_status_list.end(), compare_streaming_statuses_func);

            //check the first element in the list (lowest priority with least concurrent channels)
            if (device_status_list.size() > 0)
            {
                device_status_t& status = device_status_list[0];
                if (status.max_priority_ < streamer->get_priority())
                {
                    device_id = status.id_;

                    const device_channel_tuning_t& channel = device_channels[device_id];
                    tune_info.tuning_params_ = channel.device_channel_.tune_params_;
                    tune_info.scanner_settings_ = channel.scanner_settings_;

                    log_info(L"source_manager_t::start_channel. Channel %1% preempts streaming on on device %2% (%3%)") % string_cast<EC_UTF8>(channel_id) % string_cast<EC_UTF8>(device_id.get()) % status.max_priority_;
                    device_manager_->reset_device(device_id);
                }
            }
        }
    }

    if (!device_id.empty())
    {
        if (device_manager_->add_streamer(device_id, channel_id, tune_info, streamer))
            ret_val = DvbLink_StatusOk;
    } else
    {
        log_error(L"source_manager_t::handle start_channel. No free devices found for channel %1% request") % string_cast<EC_UTF8>(channel_id);
        ret_val = DvbLink_StatusNoFreeTuner;
    }

    return ret_val;
}

dvblink::EStatus source_manager_t::start_channel(const char* channel_id, streamer_object_t& streamer)
{
    dvblink::EStatus ret_val = DvbLink_StatusError;

    log_info(L"source_manager_t::start_channel. Channel %1%, priority %2%") % string_cast<EC_UTF8>(channel_id) % streamer->get_priority();

    //get device id and tuning params for the channel
    device_channel_tuning_map_t device_channels;
    if (channel_storage_->get_device_channels(channel_id, device_channels))
    {
        dvblink::device_id_t device_id;

        while (device_channels.size() > 0)
        {
            device_id.clear();
            ret_val = start_channel_int(channel_id, streamer, &device_channels, device_id);
            if (ret_val == DvbLink_StatusOk)
            {
                //success
                break;
            } else
            if (!device_id.empty())
            {
                //error changing channel on this device
                //remove this device from the list and try again
                if (device_channels.find(device_id) != device_channels.end())
                {
                    log_info(L"source_manager_t::start_channel. Removing device %1% from candidates and retrying") % device_id.to_wstring();
                    device_channels.erase(device_id);
                } else
                {
                    //do not know how this can happen, but to be sure
                    break;
                }
            } else
            {
                //no device can handle this request
                //return error
                break;
            }
        }

    } else
    {
        log_error(L"source_manager_t::handle start_channel. Channel %1% was not found in the channel storage") % string_cast<EC_UTF8>(channel_id);
    }

    if (ret_val == DvbLink_StatusOk)
        stream_power_manager_->report_active_device();

    return ret_val;
}

dvblink::EStatus source_manager_t::start_channel_on_device(const char* channel_id, const char* device_id, const char* tuning_params, streamer_object_t& streamer)
{
    concise_channel_tune_info_t tune_info;
    tune_info.tuning_params_ = tuning_params;
    dvblink::EStatus ret_val = device_manager_->add_streamer(device_id, channel_id, tune_info, streamer) ? DvbLink_StatusOk : DvbLink_StatusError;

    if (ret_val == DvbLink_StatusOk)
        stream_power_manager_->report_active_device();

    return ret_val;
}

void source_manager_t::stop_streamer(const char* streamer_id)
{
    device_manager_->remove_streamer(streamer_id_t(streamer_id));
}

start_scan_status_e source_manager_t::start_epg_scan(const char* provider_id, epg_box_obj_t& epg_box)
{
    start_scan_status_e ret_val = sss_error;

    //fill in channels tune info
    channel_id_list_t channels;
    epg_box->get_channels(channels);

    std::map<dvblink::device_id_t, epg_channel_tune_info_list_t> device_scan_channels;
    for (size_t i=0; i<channels.size(); i++)
    {
        device_channel_tuning_map_t device_channels;
        if (channel_storage_->get_device_channels(channels[i], device_channels))
        {
            device_channel_tuning_map_t::iterator device_it = device_channels.begin();
            while (device_it != device_channels.end())
            {
                if (device_scan_channels.find(device_it->first) == device_scan_channels.end())
                    device_scan_channels[device_it->first] = epg_channel_tune_info_list_t();

                epg_channel_tune_info_t ti;
                ti.channel_id_ = device_it->second.device_channel_.id_;
                ti.tuning_params_.tuning_params_ = device_it->second.device_channel_.tune_params_;
                ti.tuning_params_.scanner_settings_ = device_it->second.scanner_settings_;

                device_scan_channels[device_it->first].push_back(ti);

                ++device_it;
            }
        } else
        {
            log_warning(L"source_manager_t::start_epg_scan. Channel %1% does not have tune parameters") % string_cast<EC_UTF8>(channels[i].get());
        }
    }

    if (device_scan_channels.size() > 0)
    {
        ret_val = sss_no_free_device;
        //set device specific scan info
        std::map<dvblink::device_id_t, epg_channel_tune_info_list_t>::iterator it = device_scan_channels.begin();
        while (it != device_scan_channels.end())
        {
            epg_box->set_channels_to_scan(it->second);

            //try to start scan
            if (device_manager_->start_epg_scan(it->first, epg_box))
            {
                ret_val = sss_success;
                break;
            }

            ++it;
        }
    }

    if (ret_val != sss_error)
        stream_power_manager_->report_active_device();

    return ret_val;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::stream::stop_stream_request& request, dvblink::messaging::stream::stop_stream_response& response)
{
    device_manager_->remove_streamer(request.streamer_id_);
    response.result_ = true;
}

void source_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::devices::get_device_channel_url_request& request, dvblink::messaging::devices::get_device_channel_url_response& response)
{
    response.result_ = false;

    device_channel_tuning_map_t device_channels;
    if (channel_storage_->get_device_channels(request.channel_id_, device_channels))
    {
        if (device_channels.size() > 0)
        {
            device_channel_tuning_map_t::iterator it = device_channels.begin();

            concise_channel_tune_info_t ci;
            ci.scanner_settings_ = it->second.scanner_settings_;
            ci.tuning_params_ = it->second.device_channel_.tune_params_;
            response.result_ = device_manager_->get_channel_url_for_format(it->first, ci, request.format_, response.url_, response.mime_);
        }
    }
}

} // dvblink
