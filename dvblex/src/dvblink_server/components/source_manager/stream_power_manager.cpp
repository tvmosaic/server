/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/algorithm/string.hpp> 
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_message_addresses.h>
#include <dl_message_power.h>
#include "stream_power_manager.h"

using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex { 

stream_power_manager_t::stream_power_manager_t(boost::shared_ptr<device_manager_t>& device_manager, dvblink::messaging::message_queue_t message_queue) :
    message_queue_(message_queue), thread_(NULL), exit_flag_(false), id_("f5a66228-e8b7-499f-bfb7-2ad8e1079976"), device_manager_(device_manager)
{
}

stream_power_manager_t::~stream_power_manager_t()
{
}

void stream_power_manager_t::init()
{
    start();
}

void stream_power_manager_t::term()
{
    stop();
}

void stream_power_manager_t::report_active_device()
{
    s_report_active_device_params params;

    command_queue_.ExecuteCommand(e_report_active_device, &params);
}

void stream_power_manager_t::start()
{
    exit_flag_ = false;
    thread_ = new boost::thread(boost::bind(&stream_power_manager_t::thread_function, this));
}

void stream_power_manager_t::stop()
{
    if (thread_ != NULL)
    {
        exit_flag_ = true;
        thread_->join();
        delete thread_;
        thread_ = NULL;
    }
}

void stream_power_manager_t::thread_function()
{
    log_info(L"stream_power_manager_t::thread_function. Started");

    //requests
    power::disable_standby_request ds_req(id_);
    power::disable_standby_response ds_resp;

    power::enable_standby_request es_req(id_);
    power::enable_standby_response es_resp;

    //standby is enabled by default
    stream_power_state_e state = sps_standby_enabled;

    if (message_queue_->send(server_message_queue_addressee, es_req, es_resp) != success || !es_resp.result_)
        log_warning(L"stream_power_manager_t::thread_function. Enable standby failed");

    int idle_counter = 0;

    time_t check_period_seconds = 10;
    time_t next_check;
    time(&next_check);
    next_check += check_period_seconds;

    while (!exit_flag_)
    {
        time_t now;
        time(&now);

        SDLCommandItem* item;
        while (command_queue_.PeekCommand(&item))
        {
            switch (item->id)
            {
            case e_report_active_device:
                {
                    s_report_active_device_params* params = (s_report_active_device_params*)item->param;

                    if (state == sps_standby_enabled)
                    {
                        log_info(L"stream_power_manager_t::thread_function. Device manager is active. Disable standby");

                        if (message_queue_->send(server_message_queue_addressee, ds_req, ds_resp) != success || !ds_resp.result_)
                            log_warning(L"stream_power_manager_t::thread_function. Disable standby failed");

                        state = sps_standby_disabled;
                    }
                    //reset idle counter
                    idle_counter = 0;

                }
                break;
            }
            command_queue_.FinishCommand(&item);
        }

        if (now > next_check)
        {
            //check if device manager is idle
            if (state == sps_standby_disabled)
            {
                if (device_manager_->is_idle())
                {
                    idle_counter += 1;
                    if (idle_counter == 2)
                    {
                        log_info(L"stream_power_manager_t::thread_function. Device manager is idle. Enable standby");

                        //set standby enabled
                        if (message_queue_->send(server_message_queue_addressee, es_req, es_resp) != success || !es_resp.result_)
                            log_warning(L"stream_power_manager_t::thread_function. Enable standby failed");

                        state = sps_standby_enabled;
                    }
                }
            }
            next_check = now + check_period_seconds;
        }

        boost::this_thread::sleep(boost::posix_time::milliseconds(200));
    }

    //enable standby on thread exit
    if (message_queue_->send(server_message_queue_addressee, es_req, es_resp) != success || !es_resp.result_)
        log_warning(L"stream_power_manager_t::thread_function. Enable standby failed");

    log_info(L"stream_power_manager_t::thread_function. Stopped");
}

}

