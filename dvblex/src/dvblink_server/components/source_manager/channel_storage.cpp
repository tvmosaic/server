/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/algorithm/string.hpp> 
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_pugixml_helper.h>
#include "channel_storage.h"

using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::pugixml_helpers;

namespace dvblex { 

channel_storage_t::channel_storage_t(source_manager_settings_obj_t& settings) :
    settings_(settings)
{
}

channel_storage_t::~channel_storage_t()
{
}

void channel_storage_t::init()
{
    load();
}

void channel_storage_t::term()
{
}

bool channel_storage_t::add_headend_to_device(const dvblink::device_id_t& device_id, const headend_info_t& headend_info, const concise_param_map_t& scanner_settings)
{
    bool ret_val = false;
    log_info(L"channel_storage_t::add_headend_to_device. Adding headend %1% to device %2%") % headend_info.id_.to_wstring() % device_id.to_wstring();

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    //check if this headend already exists
    if (headends_.find(headend_info.id_) != headends_.end())
    {
        log_info(L"channel_storage_t::add_headend_to_device. Successfully added headend %1% to device %2%") % headend_info.id_.to_wstring() % device_id.to_wstring();
        ret_val = true;

        //check if device is in the map already and add it if not
        device_map_t::iterator devices_it = devices_.find(device_id);
        if (devices_it == devices_.end())
        {
            devices_[device_id] = device_headend_map_t();
            devices_it = devices_.find(device_id);
        }

        //add new headend and its channel tuning info on the device
        if (devices_it->second.find(headend_info.id_) == devices_it->second.end())
        {
            //add new device headend
            devices_it->second[headend_info.id_] = concise_param_map_t();
        }

        //assign scanner settings
        devices_it->second[headend_info.id_] = scanner_settings;

        //save new channel configuration
        save();
    } else
    {
        log_info(L"channel_storage_t::add_headend_to_device. Headend %1% does not exist yet") % headend_info.id_.to_wstring();
    }

    return ret_val;
}

void channel_storage_t::add_headend_channels(const dvblink::device_id_t& device_id,
                                              const headend_info_t& headend_info, const concise_param_map_t& scanner_settings,
                                              const transponder_list_t& transponders, channel_changes_desc_t& result)
{
    log_info(L"channel_storage_t::add_headend_channels. Adding channels of headend %1% on device %2%") % headend_info.id_.to_wstring() % device_id.to_wstring();

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    transponder_channels_map_t pcm;
    channel_lookup_map_t clm;

    for (size_t i=0; i<transponders.size(); i++)
    {
        pcm[transponders[i].tr_id_] = transponders[i];
        for (size_t j=0; j<transponders[i].channels_.size(); j++)
        {
            channel_lookup_decriptor_t cld;
            cld.headend_id_ = headend_info.id_;
            cld.transponder_id_ = transponders[i].tr_id_;
            cld.channel_idx_ = j;
            clm[transponders[i].channels_[j].id_] = cld;
           
        }
    }

    //check if this headend exists already
    headends_map_t::iterator headend_it = headends_.find(headend_info.id_);
    if (headend_it == headends_.end())
    {
        //add headend
        headends_[headend_info.id_] = pcm;
        channel_lookup_map_.insert(clm.begin(), clm.end());

        //update notification structures
        result.added_headends_.push_back(headend_info.id_);
        channel_lookup_map_t::iterator clm_it = clm.begin();
        while (clm_it != clm.end())
        {
            result.added_channels_.push_back(clm_it->first);
            ++clm_it;
        }
    } else
    {
        //update headend

        //added channels
        channel_lookup_map_t::iterator clm_it = clm.begin();
        while (clm_it != clm.end())
        {
            if (channel_lookup_map_.find(clm_it->first) == channel_lookup_map_.end())
            {
                channel_lookup_map_[clm_it->first] = clm[clm_it->first];
                result.added_channels_.push_back(clm_it->first);
            }

            ++clm_it;
        }

        //deleted channels
        transponder_channels_map_t::iterator tcm_iterator = headend_it->second.begin();
        while (tcm_iterator != headend_it->second.end())
        {
            for (size_t i=0; i<tcm_iterator->second.channels_.size(); i++)
            {
                if (clm.find(tcm_iterator->second.channels_[i].id_) == clm.end())
                {
                    channel_lookup_map_.erase(tcm_iterator->second.channels_[i].id_);
                    result.deleted_channels_.push_back(tcm_iterator->second.channels_[i].id_);
                }
            }
            ++tcm_iterator;
        }

        //swap headend info
        headends_[headend_info.id_] = pcm;
    }

    //check if device is in the map already and add it if not
    device_map_t::iterator devices_it = devices_.find(device_id);
    if (devices_it == devices_.end())
    {
        devices_[device_id] = device_headend_map_t();
        devices_it = devices_.find(device_id);
    }

    //add new headend and its channel tuning info on the device
    if (devices_it->second.find(headend_info.id_) == devices_it->second.end())
    {
        //add new device headend
        devices_it->second[headend_info.id_] = concise_param_map_t();
    }

    //assign scanner settings
    devices_it->second[headend_info.id_] = scanner_settings;

    //save new channel configuration
    save();
}

void channel_storage_t::drop_headend_on_device(const dvblink::device_id_t& device_id, const dvblink::headend_id_t& headend_id, channel_changes_desc_t& result)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);

    drop_headend_on_device_int(device_id, headend_id, result);
}

void channel_storage_t::drop_headend_on_device_int(const dvblink::device_id_t& device_id, const dvblink::headend_id_t& headend_id, channel_changes_desc_t& result)
{
    log_info(L"channel_storage_t::drop_headend_on_device_int. Deleting headend %1% on device %2%") % string_cast<EC_UTF8>(headend_id.get()) % string_cast<EC_UTF8>(device_id.get());

    //delete headend from this device
    if (devices_.find(device_id) != devices_.end())
    {
        devices_[device_id].erase(headend_id);
        //if device does not have any associated headends anymore - drop device as well
        log_info(L"channel_storage_t::drop_headend_on_device_int. Device %1% has no headends. Dropping it.") % string_cast<EC_UTF8>(device_id.get());
        if (devices_[device_id].size() == 0)
            devices_.erase(device_id);
    }
    //check if this headend is still used on other devices
    bool b_in_use = false;
    device_map_t::iterator device_it = devices_.begin();
    while (device_it != devices_.end())
    {
        if (device_it->second.find(headend_id) != device_it->second.end())
        {
            b_in_use = true;
            break;
        }
        ++device_it;
    }

    if (!b_in_use)
    {
        log_info(L"channel_storage_t::drop_headend_on_device_int. headend %1% has no associated devices. Dropping it.") % string_cast<EC_UTF8>(headend_id.get());

        headends_map_t::iterator headend_it = headends_.find(headend_id);
        if (headend_it != headends_.end())
        {
            transponder_channels_map_t::iterator tcm_iterator = headend_it->second.begin();
            while (tcm_iterator != headend_it->second.end())
            {
                for (size_t i=0; i<tcm_iterator->second.channels_.size(); i++)
                {
                    channel_lookup_map_.erase(tcm_iterator->second.channels_[i].id_);
                    result.deleted_channels_.push_back(tcm_iterator->second.channels_[i].id_);
                }
                ++tcm_iterator;
            }
        }

        headends_.erase(headend_id);
        result.deleted_headends_.push_back(headend_id);
    }

    //save new channel configuration
    save();
}

static const std::string channel_config_root_node                 = "channel_config";
static const std::string channel_config_devices_node                 = "devices";
static const std::string channel_config_device_node                 = "device";
static const std::string channel_config_id_node                     = "id";
static const std::string channel_config_headend_node                 = "headend";
static const std::string channel_config_headends_node                 = "headends";
static const std::string channel_config_transponder_node                 = "transponder";
static const std::string channel_config_channel_node                 = "channel";
static const std::string channel_config_channels_node                 = "channels";
static const std::string channel_config_settings_node                 = "settings";

static const std::string channel_config_name_node                 = "name";
static const std::string channel_config_origin_node                 = "origin";
static const std::string channel_config_tid_node                 = "tid";
static const std::string channel_config_nid_node                 = "nid";
static const std::string channel_config_sid_node                 = "sid";
static const std::string channel_config_enc_node                 = "enc";
static const std::string channel_config_num_node                 = "num";
static const std::string channel_config_subnum_node              = "subnum";
static const std::string channel_config_logo_node                 = "logo";
static const std::string channel_config_comment_node                 = "comment";
static const std::string channel_config_type_node                = "type";
static const std::string channel_config_params_node              = "params";
static const std::string channel_config_setting_node            = "setting";
static const std::string channel_config_key_node                 = "key";
static const std::string channel_config_value_node               = "value";

void channel_storage_t::save()
{
    std::stringstream buf;

    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(channel_config_root_node.c_str());
    if (root_node != NULL)
    {
        //headends and channels
        pugi::xml_node headends_node = new_child(root_node, channel_config_headends_node);
        if (headends_node != NULL)
        {
            headends_map_t::iterator headend_it = headends_.begin();
            while (headend_it != headends_.end())
            {
                pugi::xml_node headend_node = new_child(headends_node, channel_config_headend_node);
                if (headend_node != NULL)
                {
                    add_node_attribute(headend_node, channel_config_id_node, headend_it->first.to_string()); 
                    transponder_channels_map_t::iterator tr_it = headend_it->second.begin();
                    while (tr_it != headend_it->second.end())
                    {
                        pugi::xml_node transponder_node = new_child(headend_node, channel_config_transponder_node);
                        if (transponder_node != NULL)
                        {
                            add_node_attribute(transponder_node, channel_config_id_node, tr_it->second.tr_id_.to_string());
                            add_node_attribute(transponder_node, channel_config_name_node, tr_it->second.tr_name_.to_string());

                            for (size_t i=0; i<tr_it->second.channels_.size(); i++)
                            {
                                pugi::xml_node channel_node = new_child(transponder_node, channel_config_channel_node);
                                if (channel_node != NULL)
                                {
                                    new_child(channel_node, channel_config_id_node, tr_it->second.channels_[i].id_.to_string());
                                    new_child(channel_node, channel_config_name_node, tr_it->second.channels_[i].name_.get());
                                    new_child(channel_node, channel_config_origin_node, tr_it->second.channels_[i].origin_.get());
                                    new_child(channel_node, channel_config_logo_node, tr_it->second.channels_[i].logo_.get());
                                    new_child(channel_node, channel_config_comment_node, tr_it->second.channels_[i].comment_.get());

                                    buf.clear(); buf.str("");
                                    buf << tr_it->second.channels_[i].tid_.get();
                                    new_child(channel_node, channel_config_tid_node, buf.str());

                                    buf.clear(); buf.str("");
                                    buf << tr_it->second.channels_[i].nid_.get();
                                    new_child(channel_node, channel_config_nid_node, buf.str());

                                    buf.clear(); buf.str("");
                                    buf << tr_it->second.channels_[i].sid_.get();
                                    new_child(channel_node, channel_config_sid_node, buf.str());

                                    buf.clear(); buf.str("");
                                    buf << tr_it->second.channels_[i].encrypted_.get();
                                    new_child(channel_node, channel_config_enc_node, buf.str());

                                    buf.clear(); buf.str("");
                                    buf << tr_it->second.channels_[i].num_.get();
                                    new_child(channel_node, channel_config_num_node, buf.str());

                                    buf.clear(); buf.str("");
                                    buf << tr_it->second.channels_[i].sub_num_.get();
                                    new_child(channel_node, channel_config_subnum_node, buf.str());

                                    buf.clear(); buf.str("");
                                    buf << (unsigned long)(tr_it->second.channels_[i].type_);
                                    new_child(channel_node, channel_config_type_node, buf.str());

                                    new_child(channel_node, channel_config_params_node, tr_it->second.channels_[i].tune_params_.get());
                                }
                            }
                        }
                        ++tr_it;
                    }
                }
                ++headend_it;
            }
	    }

        //devices
        pugi::xml_node devices_node = new_child(root_node, channel_config_devices_node);
        if (devices_node != NULL)
        {
            device_map_t::iterator devices_it = devices_.begin();
            while (devices_it != devices_.end())
            {
                pugi::xml_node device_node = new_child(devices_node, channel_config_device_node);
                if (device_node != NULL)
                {
                    add_node_attribute(device_node, channel_config_id_node, devices_it->first.to_string()); 

                    //headends
                    device_headend_map_t::iterator headend_it = devices_it->second.begin();
                    while (headend_it != devices_it->second.end())
                    {
                        pugi::xml_node headend_node = new_child(device_node, channel_config_headend_node);
                        if (headend_node != NULL)
                        {
                            add_node_attribute(headend_node, channel_config_id_node, headend_it->first.to_string()); 

                            //scanner settings
                            pugi::xml_node settings_node = new_child(headend_node, channel_config_settings_node);
                            if (settings_node != NULL)
                            {
                                concise_param_map_t& cp_map = headend_it->second;
                                concise_param_map_t::iterator cp_it = cp_map.begin();
                                while (cp_it != cp_map.end())
                                {
                                    pugi::xml_node cp_node = new_child(settings_node, channel_config_setting_node);
                                    if (cp_node != NULL)
                                    {
                                        add_node_attribute(cp_node, channel_config_key_node, cp_it->first.get());
                                        add_node_attribute(cp_node, channel_config_value_node, cp_it->second.get());
                                    }
                                    ++cp_it;
                                }
                            }
                        }

                        ++headend_it;
                    }
                }
                ++devices_it;
            }
	    }

        xmldoc_dump_to_file(doc, settings_->get_channel_config_filepath().to_string());
    }
}

void channel_storage_t::reset()
{
    channel_lookup_map_.clear();
    headends_.clear();
    devices_.clear();
}

void channel_storage_t::load()
{
    reset();
    std::string str;

    pugi::xml_document doc;
    if (doc.load_file(settings_->get_channel_config_filepath().to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            //devices
            pugi::xml_node devices_node = root_node.child(channel_config_devices_node.c_str());
            if (devices_node != NULL)
            {
                pugi::xml_node device_node = devices_node.first_child();
                while (device_node != NULL)
                {
                    if (boost::iequals(device_node.name(), channel_config_device_node))
                    {
                        std::string device_id;
                        get_node_attribute(device_node, channel_config_id_node, device_id);
                        devices_[device_id] = device_headend_map_t();

                        //headends
                        pugi::xml_node headend_node = device_node.first_child();
                        while (headend_node != NULL)
                        {
                            if (boost::iequals(headend_node.name(), channel_config_headend_node))
                            {
                                //new version has headend id as attribute
                                if (get_node_attribute(headend_node, channel_config_id_node, str))
                                {
                                    devices_[device_id].insert(std::make_pair(str, concise_param_map_t()));
                                    concise_param_map_t& cp_map = devices_[device_id].find(str)->second;

                                    pugi::xml_node settings_node = headend_node.child(channel_config_settings_node.c_str());
                                    if (settings_node != NULL)
                                    {
                                        pugi::xml_node cp_node = settings_node.first_child();
                                        while (cp_node != NULL)
                                        {
                                            std::string str1;
                                            get_node_attribute(cp_node, channel_config_key_node, str);
                                            get_node_attribute(cp_node, channel_config_value_node, str1);
                                            cp_map[str] = str1;

                                            cp_node = cp_node.next_sibling();
                                        }
                                    }

                                } else
                                {
                                    //old version. insert empty headend detailed info
                                    str = headend_node.child_value();
                                    devices_[device_id].insert(std::make_pair(str, concise_param_map_t()));
                                }
                            }

                            headend_node = headend_node.next_sibling();
                        }
                    }
                    device_node = device_node.next_sibling();
                }

            }
            //headends and channels
            pugi::xml_node headends_node = root_node.child(channel_config_headends_node.c_str());
            if (headends_node != NULL)
            {
                pugi::xml_node headend_node = headends_node.first_child();
                while (headend_node != NULL)
                {
                    if (boost::iequals(headend_node.name(), channel_config_headend_node))
                    {
                        std::string headend_id;
                        get_node_attribute(headend_node, channel_config_id_node, headend_id);
                        headends_[headend_id] = transponder_channels_map_t();

                        pugi::xml_node transponder_node = headend_node.first_child();
                        while (transponder_node != NULL)
                        {
                            if (boost::iequals(transponder_node.name(), channel_config_transponder_node))
                            {
                                transponder_t tr;
                                get_node_attribute(transponder_node, channel_config_id_node, str);
                                tr.tr_id_ = str;
                                get_node_attribute(transponder_node, channel_config_name_node, str);
                                tr.tr_name_ = str;

                                pugi::xml_node channel_node = transponder_node.first_child();
                                while (channel_node != NULL)
                                {
                                    if (boost::iequals(channel_node.name(), channel_config_channel_node))
                                    {
                                        device_channel_t ch;
                                        if (get_node_value(channel_node, channel_config_id_node, str))
                                            ch.id_ = str;
                                        if (get_node_value(channel_node, channel_config_name_node, str))
                                            ch.name_ = str;
                                        if (get_node_value(channel_node, channel_config_origin_node, str))
                                            ch.origin_ = str;
                                        if (get_node_value(channel_node, channel_config_logo_node, str))
                                            ch.logo_ = str;
                                        if (get_node_value(channel_node, channel_config_comment_node, str))
                                            ch.comment_ = str;
                                        boost::uint16_t uint16_value;
                                        if (get_node_value(channel_node, channel_config_tid_node, str))
                                        {
                                            string_conv::apply(str.c_str(), uint16_value, (boost::uint16_t)0);
                                            ch.tid_ = uint16_value;
                                        }
                                        if (get_node_value(channel_node, channel_config_nid_node, str))
                                        {
                                            string_conv::apply(str.c_str(), uint16_value, (boost::uint16_t)0);
                                            ch.nid_ = uint16_value;
                                        }
                                        if (get_node_value(channel_node, channel_config_sid_node, str))
                                        {
                                            string_conv::apply(str.c_str(), uint16_value, (boost::uint16_t)0);
                                            ch.sid_ = uint16_value;
                                        }
                                        if (get_node_value(channel_node, channel_config_enc_node, str))
                                        {
                                            bool b;
                                            string_conv::apply(str.c_str(), b, false);
                                            ch.encrypted_ = b;
                                        }
                                        if (get_node_value(channel_node, channel_config_num_node, str))
                                        {
                                            int l;
                                            string_conv::apply(str.c_str(), l, invalid_channel_number_);
                                            ch.num_ = l;
                                        }
                                        if (get_node_value(channel_node, channel_config_subnum_node, str))
                                        {
                                            int l;
                                            string_conv::apply(str.c_str(), l, invalid_channel_number_);
                                            ch.sub_num_ = l;
                                        }
                                        if (get_node_value(channel_node, channel_config_type_node, str))
                                        {
                                            unsigned long ul;
                                            string_conv::apply(str.c_str(), ul, (unsigned long)ct_tv);
                                            ch.type_ = (channel_type_e)ul;
                                        }
                                        if (get_node_value(channel_node, channel_config_params_node, str))
                                            ch.tune_params_ = str;

                                        tr.channels_.push_back(ch);
                                    }
                                    channel_node = channel_node.next_sibling();
                                }

                                headends_[headend_id].insert(std::make_pair(tr.tr_id_, tr));
                            }
                            transponder_node = transponder_node.next_sibling();
                        }
                    }
                 
                    headend_node = headend_node.next_sibling();
                }
            }
        }
    }

    //recreate channel lookup map
    headends_map_t::iterator headend_it = headends_.begin();
    while (headend_it != headends_.end())
    {
        transponder_channels_map_t::iterator tcm_iterator = headend_it->second.begin();
        while (tcm_iterator != headend_it->second.end())
        {
            for (size_t i=0; i<tcm_iterator->second.channels_.size(); i++)
            {
                channel_lookup_decriptor_t desc;
                desc.headend_id_ = headend_it->first;
                desc.transponder_id_ = tcm_iterator->first;
                desc.channel_idx_ = i;
                channel_lookup_map_[tcm_iterator->second.channels_[i].id_] = desc;
            }
            ++tcm_iterator;
        }

        ++headend_it;
    }
}

void channel_storage_t::get_devices(device_map_to_headend_list_t& devices)
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);

    device_map_t::iterator device_it = devices_.begin();
    while (device_it != devices_.end())
    {
        devices[device_it->first] = headend_description_for_device_list_t();
        device_headend_map_t::iterator headend_it = device_it->second.begin();
        while (headend_it != device_it->second.end())
        {
            headend_description_for_device_t hdd;
            hdd.headend_id_ = headend_it->first;
            hdd.device_config_params_ = headend_it->second;

            devices[device_it->first].push_back(hdd);
            ++headend_it;
        }
        ++device_it;
    }
}

bool channel_storage_t::is_device_present(const dvblink::device_id_t& device_id)
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);

    return devices_.find(device_id) != devices_.end();
}

void channel_storage_t::get_headends(headend_id_list_t& headends)
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);

    headends_map_t::iterator headend_it = headends_.begin();
    while (headend_it != headends_.end())
    {
        headends.push_back(headend_it->first);

        ++headend_it;
    }
}


bool channel_storage_t::get_channel_descriptions(const dvblink::headend_id_t& headend_id, concise_transponder_desc_list_t& transponders, 
                                                 const invisible_channel_map_t* invisible_map, bool with_channels, const dvblink::transponder_id_t* transponder_id)
{
    bool ret_val = false;

    boost::shared_lock<boost::shared_mutex> lock(lock_);

    headends_map_t::iterator headend_it = headends_.begin();
    while (headend_it != headends_.end())
    {
        if (boost::iequals(headend_id.get(), headend_it->first.get()))
        {
            transponder_channels_map_t::iterator tr_it = headend_it->second.begin();
            while (tr_it != headend_it->second.end())
            {
                if (transponder_id == NULL || transponder_id->empty() || boost::iequals(tr_it->second.tr_id_.get(), transponder_id->get()))
                {
                    concise_transponder_description_t td;
                    td.tr_id_ = tr_it->second.tr_id_;
                    td.tr_name_ = tr_it->second.tr_name_;

                    if (with_channels)
                    {
                        for (size_t i=0; i<tr_it->second.channels_.size(); i++)
                        {
                            if (invisible_map == NULL || invisible_map->find(tr_it->second.channels_[i].id_) == invisible_map->end())
                            {
                                concise_device_channel_t cd;
                                cd.from_device_channel(tr_it->second.channels_[i]);
                                td.channels_.push_back(cd);
                            }
                        }
                    }

                    transponders.push_back(td);
                }

                ++tr_it;
            }

            ret_val = true;
        }
        ++headend_it;
    }

    return ret_val;
}

bool channel_storage_t::get_channel_descriptions(const dvblink::headend_id_t& headend_id, idonly_transponder_desc_list_t& transponders, const invisible_channel_map_t* invisible_map)
{
    bool ret_val = false;

    boost::shared_lock<boost::shared_mutex> lock(lock_);

    headends_map_t::iterator headend_it = headends_.begin();
    while (headend_it != headends_.end())
    {
        if (boost::iequals(headend_id.get(), headend_it->first.get()))
        {
            transponder_channels_map_t::iterator tr_it = headend_it->second.begin();
            while (tr_it != headend_it->second.end())
            {
                idonly_transponder_description_t td;
                td.id_ = tr_it->first;

                for (size_t i=0; i<tr_it->second.channels_.size(); i++)
                    if (invisible_map == NULL || invisible_map->find(tr_it->second.channels_[i].id_) == invisible_map->end())
                        td.channels_.push_back(tr_it->second.channels_[i].id_);

                transponders.push_back(td);

                ++tr_it;
            }

            ret_val = true;
        }
        ++headend_it;
    }

    return ret_val;
}

bool channel_storage_t::get_channel_descriptor(const dvblink::channel_id_t& channel_id, channel_description_t& channel_desc)
{
    bool result = false;

    boost::shared_lock<boost::shared_mutex> lock(lock_);

    device_channel_tuning_t channel;

    channel_lookup_map_t::iterator clm_it = channel_lookup_map_.find(channel_id);
    if (clm_it != channel_lookup_map_.end())
    {
        headends_map_t::iterator headend_it = headends_.find(clm_it->second.headend_id_);
        if (headend_it != headends_.end() && headend_it->second.find(clm_it->second.transponder_id_) != headend_it->second.end())
        {
            //find channel
            device_channel_t& dc = headend_it->second[clm_it->second.transponder_id_].channels_[clm_it->second.channel_idx_];
            concise_device_channel_t cc;
            cc.from_device_channel(dc);
            channel_desc.from_concise_channel(cc);

            result = true;
        }
    }
    return result;
}

void channel_storage_t::get_channel_descriptors(const channel_id_list_t& channels, channel_desc_list_t& channels_desc)
{
    for (size_t i=0; i<channels.size(); i++)
    {
        channel_description_t channel_desc;
        if (get_channel_descriptor(channels[i], channel_desc))
            channels_desc.push_back(channel_desc);
    }
}

bool channel_storage_t::get_device_channels(const dvblink::channel_id_t& channel_id, device_channel_tuning_map_t& device_channels)
{
    bool result = false;

    boost::shared_lock<boost::shared_mutex> lock(lock_);

    device_channel_tuning_t channel;

    channel_lookup_map_t::iterator clm_it = channel_lookup_map_.find(channel_id);
    if (clm_it != channel_lookup_map_.end())
    {
        headends_map_t::iterator headend_it = headends_.find(clm_it->second.headend_id_);
        if (headend_it != headends_.end() && 
            headend_it->second.find(clm_it->second.transponder_id_) != headend_it->second.end())
        {
            channel.device_channel_ = headend_it->second[clm_it->second.transponder_id_].channels_[clm_it->second.channel_idx_];

            //walk through all devices and fill in tuning info for each device
            device_map_t::iterator device_it = devices_.begin();
            while (device_it != devices_.end())
            {
                if (device_it->second.find(headend_it->first) != device_it->second.end())
                {
                    channel.scanner_settings_ = device_it->second[headend_it->first];
                    device_channels[device_it->first] = channel;
                }

                ++device_it;
            }

            result = !device_channels.empty();
        }
    }

    return result;
}

void channel_storage_t::get_headend_desc_for_device(const dvblink::device_id_t& device_id, headend_description_for_device_list_t& pdd_list)
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);

    //lookup devices with this headend
    device_map_t::iterator device_it = devices_.find(device_id);
    if (device_it != devices_.end())
    {
        device_headend_map_t::iterator headend_it = device_it->second.begin();
        while (headend_it != device_it->second.end())
        {
            headend_description_for_device_t pdd;
            pdd.headend_id_ = headend_it->first;
            pdd.device_config_params_ = headend_it->second;
            pdd_list.push_back(pdd);

            ++headend_it;
        }
    }
}

bool channel_storage_t::can_add_concurrent_channel(const dvblink::device_id_t& device_id, const channel_id_list_t& current_channels, 
                                                   const configurable_device_props_t& device_props, const dvblink::channel_id_t& new_channel)
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);

    bool ret_val = false;

    device_map_t::iterator device_it = devices_.find(device_id);
    if (device_it != devices_.end())
    {
        int fta_ch_num = 0;
        int enc_ch_num = 0;

        //check if new channel is already among current
        bool badd = true;
        for (size_t i=0; i<current_channels.size(); i++)
        {
            if (boost::iequals(current_channels[i].get(), new_channel.get()))
            {
                badd = false;
                break;
            }
        }

        channel_id_list_t cur_channels = current_channels;
        if (badd)
            cur_channels.push_back(new_channel);

        for (size_t j=0; j<cur_channels.size(); j++)
        {
            channel_lookup_map_t::iterator clm_it = channel_lookup_map_.find(cur_channels[j]);
            if (clm_it != channel_lookup_map_.end())
            {
                headends_map_t::iterator headend_it = headends_.find(clm_it->second.headend_id_);
                if (headend_it != headends_.end() && 
                    headend_it->second.find(clm_it->second.transponder_id_) != headend_it->second.end())
                {
                    device_channel_t& channel = headend_it->second[clm_it->second.transponder_id_].channels_[clm_it->second.channel_idx_];
                    if (channel.encrypted_.get())
                        enc_ch_num += 1;
                    else
                        fta_ch_num += 1;
                }
            }
        }

        ret_val = (device_props.enc_concurrent_streams_ == -1 || enc_ch_num <= device_props.enc_concurrent_streams_) &&
                  (device_props.fta_concurrent_streams_ == -1 || fta_ch_num <= device_props.fta_concurrent_streams_);

        if (!ret_val)
            log_info(L"channel_storage_t::can_add_concurrent_channel. Concurrency factor exceeded for channel %1% on device %2% (%3%, %4%, %5%, %6%)") % new_channel.to_wstring() % device_id.to_wstring() %
                enc_ch_num % device_props.enc_concurrent_streams_ % fta_ch_num % device_props.fta_concurrent_streams_;
    }

    return ret_val;
}

static boost::int32_t inc_seed(boost::int32_t& seed)
{
    seed += 2;

    return seed;
}

void channel_storage_t::get_device_channel_sets(const configurable_device_props_map_t& device_props, ts_source_TO_set_of_set_of_channels_set_t& device_channel_sets)
{
    typedef std::map<dvblink::headend_id_t, set_of_set_of_channels_set_t> headend_channel_sets_map_t;

    boost::shared_lock<boost::shared_mutex> lock(lock_);

    configurable_device_props_t default_device_props;

    boost::int32_t fta_id_seed = 0;
    boost::int32_t enc_id_seed = 1;

    headend_channel_sets_map_t headend_channel_sets_map;

    headends_map_t::iterator pm_it = headends_.begin();
    while (pm_it != headends_.end())
    {
        headend_channel_sets_map[pm_it->first] = set_of_set_of_channels_set_t();
        set_of_set_of_channels_set_t& sscs = headend_channel_sets_map[pm_it->first];

        transponder_channels_map_t::iterator tr_it = pm_it->second.begin();
        while (tr_it != pm_it->second.end())
        {
            sscs.push_back(set_of_channel_sets_t());
            set_of_channel_sets_t& tr_ch_set = sscs[sscs.size() - 1];

            tr_ch_set.push_back(channel_set_t());
            tr_ch_set.push_back(channel_set_t());

            channel_set_t& fta_channels = tr_ch_set[0];
            channel_set_t& enc_channels = tr_ch_set[1];

            fta_channels.channel_set_id_ = inc_seed(fta_id_seed);
            fta_channels.max_concurrency_num_ = default_device_props.fta_concurrent_streams_;

            enc_channels.channel_set_id_ = inc_seed(enc_id_seed);
            enc_channels.max_concurrency_num_ = default_device_props.enc_concurrent_streams_;

            for (size_t i=0; i<tr_it->second.channels_.size(); i++)
            {
                if (tr_it->second.channels_[i].encrypted_.get())
                    enc_channels.channels_.push_back(tr_it->second.channels_[i].id_);
                else
                    fta_channels.channels_.push_back(tr_it->second.channels_[i].id_);
            }

            if (fta_channels.channels_.size() == 0)
                tr_ch_set.erase(tr_ch_set.begin());
            else if (enc_channels.channels_.size() == 0)
                tr_ch_set.erase(tr_ch_set.begin() + 1);
                
            ++tr_it;
        }

        ++pm_it;
    }

    device_map_t::iterator device_it = devices_.begin();
    while (device_it != devices_.end())
    {
        device_channel_sets[device_it->first] = set_of_set_of_channels_set_t();
        set_of_set_of_channels_set_t& cur_ch_set = device_channel_sets[device_it->first];

        device_headend_map_t::iterator headend_it = device_it->second.begin();
        while (headend_it != device_it->second.end())
        {
            headend_channel_sets_map_t::iterator pcs_it = headend_channel_sets_map.find(headend_it->first);
            if (pcs_it != headend_channel_sets_map.end())
                cur_ch_set.insert(cur_ch_set.end(), pcs_it->second.begin(), pcs_it->second.end());

            ++headend_it;
        }

        configurable_device_props_map_t::const_iterator dp_it = device_props.find(device_it->first);
        if (dp_it != device_props.end())
        {
            //apply actual concurrency factors (if they are different from defaults)
            if (dp_it->second.fta_concurrent_streams_ != default_device_props.fta_concurrent_streams_ ||
                dp_it->second.enc_concurrent_streams_ != default_device_props.enc_concurrent_streams_)
            {
                for (size_t i=0; i<cur_ch_set.size(); i++)
                {
                    set_of_channel_sets_t& scs = cur_ch_set[i];
                    for (size_t j=0; j<scs.size(); j++)
                    {
                        channel_set_t& cs = scs[j];
                        if ((cs.channel_set_id_.get() % 2) == 0)
                        {
                            //even - this is fta channel set
                            cs.max_concurrency_num_ = dp_it->second.fta_concurrent_streams_;
                        } else
                        {
                            //odd - this is enc channel set
                            cs.max_concurrency_num_ = dp_it->second.enc_concurrent_streams_;
                        }
                    }
                }
            }
        }

        ++device_it;
    }

}

}