/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>
#include <boost/thread.hpp>
#include <dl_types.h>
#include <dl_filesystem_path.h>
#include <dl_parameters.h>
#include <dl_device_info.h>
#include "settings.h"

namespace dvblex { 

class device_params_storage_t
{
public:
    typedef std::map<dvblink::device_id_t, concise_param_map_t> device_params_map_t;

public:
    device_params_storage_t(source_manager_settings_obj_t& settings);
    ~device_params_storage_t();

    void init();
    void term();

    void add_device_params(const dvblink::device_id_t& device_id, const concise_param_map_t& params);
    void delete_device_params(const dvblink::device_id_t& device_id);
    device_params_map_t& get_all_params(){return device_params_map_;}

    void set_manual_devices(const manual_device_list_t& devices);
    manual_device_list_t& get_manual_devices(){return manual_devices_;}

protected:
    device_params_map_t device_params_map_;
    source_manager_settings_obj_t settings_;
    manual_device_list_t manual_devices_;

    void save();
    void load();
    void reset();
};

}
