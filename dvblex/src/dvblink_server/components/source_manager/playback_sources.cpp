/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/algorithm/string.hpp> 
#include <dl_strings.h>
#include <dl_logger.h>
#include "playback_sources.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

namespace dvblex { 

playback_source_manager_t::playback_source_manager_t(source_manager_settings_obj_t& settings) :
    settings_(settings)
{
}

playback_source_manager_t::~playback_source_manager_t()
{
}

void playback_source_manager_t::init()
{
}

void playback_source_manager_t::term()
{
    device_to_playback_src_map_t::iterator it = playback_sources_.begin();
    while (it != playback_sources_.end())
    {
        it->second->term();
        ++it;
    }

    playback_sources_.clear();
}

void playback_source_manager_t::add_playback_src_for_device(const dvblink::device_id_t& device_id, const device_playback_src_obj_t& playback_src)
{
    playback_sources_[device_id] = playback_src;
}

void playback_source_manager_t::remove_playback_src_for_device(const dvblink::device_id_t& device_id)
{
    if (playback_sources_.find(device_id) != playback_sources_.end())
    {
        playback_sources_[device_id]->term();
        playback_sources_.erase(device_id);
    }
}

}