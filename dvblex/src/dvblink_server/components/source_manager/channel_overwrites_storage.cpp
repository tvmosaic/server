/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/algorithm/string.hpp> 
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_pugixml_helper.h>
#include <dl_locale_strings.h>
#include "channel_overwrites_storage.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::pugixml_helpers;

namespace dvblex { 

channel_overwrites_storage_t::channel_overwrites_storage_t(source_manager_settings_obj_t& settings) :
    settings_(settings)
{
}

channel_overwrites_storage_t::~channel_overwrites_storage_t()
{
}

void channel_overwrites_storage_t::init()
{
    load();
}

void channel_overwrites_storage_t::term()
{
}

void channel_overwrites_storage_t::get_channel_overwrites(channel_overwrite_map_t& overwrites)
{
    overwrites = overwrites_;
}

void channel_overwrites_storage_t::set_channel_overwrites(const channel_overwrite_map_t& overwrites)
{
    overwrites_ = overwrites;

    //remove empty overwrites
    channel_id_list_t channels_to_remove;
    channel_overwrite_map_t::iterator it = overwrites_.begin();
    while (it != overwrites_.end())
    {
        if (it->second.is_empty())
            channels_to_remove.push_back(it->first);

        ++it;
    }

    for (size_t i=0; i<channels_to_remove.size(); i++)
        overwrites_.erase(channels_to_remove[i]);

    save();
}

void channel_overwrites_storage_t::process_channel_changes(const channel_changes_desc_t& changes)
{
    for (size_t i=0; i<changes.hidden_channels_.size(); i++)
        overwrites_.erase(changes.hidden_channels_.at(i));

    for (size_t i=0; i<changes.deleted_channels_.size(); i++)
        overwrites_.erase(changes.deleted_channels_.at(i));

    save();

}

void channel_overwrites_storage_t::apply_overwrites(channel_desc_list_t& channels_desc)
{
    for (size_t i=0; i<channels_desc.size(); i++)
    {
        channel_description_t& cd = channels_desc[i];
        channel_overwrite_map_t::iterator it = overwrites_.find(cd.id_);
        if (it != overwrites_.end())
        {
            if (!it->second.name_.empty())
                cd.name_ = it->second.name_;

            if (it->second.number_.get() > 0)
                cd.num_ = it->second.number_;

            if (it->second.subnumber_.get() > 0)
                cd.sub_num_ = it->second.subnumber_;
        }
    }
}

void channel_overwrites_storage_t::reset()
{
    overwrites_.clear();
}

static const std::string overwrites_config_root_node                 = "overwrites_config";
static const std::string overwrites_config_ov_node                 = "overwrite";
static const std::string overwrites_config_id_node                     = "id";
static const std::string overwrites_config_name_node                 = "name";
static const std::string overwrites_config_num_node                 = "num";
static const std::string overwrites_config_subnum_node                 = "subnum";

void channel_overwrites_storage_t::save()
{
    std::stringstream buf;
    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(overwrites_config_root_node.c_str());
    if (root_node != NULL)
    {
        channel_overwrite_map_t::iterator it = overwrites_.begin();
        while (it != overwrites_.end())
        {
            pugi::xml_node ov_node = new_child(root_node, overwrites_config_ov_node);
            if (ov_node != NULL)
            {
                add_node_attribute(ov_node, overwrites_config_id_node, it->first.get());
                if (!it->second.name_.empty())
                    add_node_attribute(ov_node, overwrites_config_name_node, it->second.name_.get()); 

                if (it->second.number_.get() > 0)
                {
                    buf.clear(); buf.str("");
                    buf << it->second.number_.get();
                    add_node_attribute(ov_node, overwrites_config_num_node, buf.str()); 

                    if (it->second.subnumber_.get() > 0)
                    {
                        buf.clear(); buf.str("");
                        buf << it->second.subnumber_.get();
                        add_node_attribute(ov_node, overwrites_config_subnum_node, buf.str()); 
                    }
                }
            }
            ++it;
        }

        pugixml_helpers::xmldoc_dump_to_file(doc, settings_->get_overwrites_config_filepath().to_string());
    }
}

void channel_overwrites_storage_t::load()
{
    reset();

    std::string str;

    pugi::xml_document doc;
    if (doc.load_file(settings_->get_overwrites_config_filepath().to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            pugi::xml_node ov_node = root_node.first_child();
            while (ov_node != NULL)
            {
                if (boost::iequals(ov_node.name(), overwrites_config_ov_node))
                {
                    dvblink::channel_id_t chid;
                    channel_overwrite_t ov;
                    if (get_node_attribute(ov_node, overwrites_config_id_node, str))
                    {
                        chid = str;

                        if (get_node_attribute(ov_node, overwrites_config_name_node, str))
                            ov.name_ = str;

                        if (get_node_attribute(ov_node, overwrites_config_num_node, str))
                        {
                            boost::int32_t i32;
                            string_conv::apply(str.c_str(), i32, ov.number_.get());
                            ov.number_ = i32;

                            if (get_node_attribute(ov_node, overwrites_config_subnum_node, str))
                            {
                                boost::int32_t i32;
                                string_conv::apply(str.c_str(), i32, ov.subnumber_.get());
                                ov.subnumber_ = i32;
                            }
                        }
                        overwrites_[chid] = ov;
                    }
                }

                ov_node = ov_node.next_sibling();
            }
        }
    }
}

}