/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/algorithm/string.hpp> 
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_pugixml_helper.h>
#include <dl_locale_strings.h>
#include <dl_language_settings.h>
#include "favorites_storage.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::pugixml_helpers;

namespace dvblex { 

static const std::string favorites_tv_channels_id                    = "c02ecf21-3b0c-4774-9bd8-6897592a902a";
static const std::string favorites_radio_channels_id                 = "4e5b2e1a-8e0e-4d49-b3b2-bb2e9826740b";

favorites_storage_t::favorites_storage_t(source_manager_settings_obj_t& settings) :
    settings_(settings)
{
}

favorites_storage_t::~favorites_storage_t()
{
}

void favorites_storage_t::init()
{
    load();
}

void favorites_storage_t::term()
{
}

bool favorites_storage_t::get_favorites(const dvblink::favorite_id_t& favorite_id, channel_favorite_list_t& favorites)
{
    for (size_t i=0; i<favorites_.size(); i++)
    {
        if (favorite_id.empty() || boost::iequals(favorite_id.get(), favorites_.at(i).id_.get()))
            favorites.push_back(favorites_.at(i));
    }

    //update localized favorites names for automatic favorites
    for (size_t i=0; i<favorites.size(); i++)
    {
        if (boost::iequals(favorites[i].id_.get(), favorites_tv_channels_id))
        {
            favorites[i].name_ = language_settings::GetInstance()->GetItemName(tv_channels_favorites_name);
        } else
        if (boost::iequals(favorites[i].id_.get(), favorites_radio_channels_id))
        {
            favorites[i].name_ = language_settings::GetInstance()->GetItemName(radio_channels_favorites_name);
        }
    }

    return favorite_id.empty() || (!favorite_id.empty() && favorites_.size() > 0);
}

static int find_fav(const channel_favorite_list_t& favorites, const dvblink::favorite_id_t& id)
{
    int ret_val = -1;
    for (size_t i=0; i<favorites.size(); i++)
    {
        if (boost::iequals(favorites[i].id_.get(), id.get()))
        {
            ret_val = i;
            break;
        }
    }
    return ret_val;
}

void favorites_storage_t::set_favorites(const channel_favorite_list_t& favorites)
{
    //add new or update existing
    for (size_t i=0; i<favorites.size(); i++)
    {
        //process manual favorites only
        if ((favorites[i].flags_.get() & cft_manual) != 0)
        {
            int idx = find_fav(favorites_, favorites[i].id_);
            if (idx != -1)
                favorites_.at(idx) = favorites[i];
            else
                favorites_.push_back(favorites[i]);
        }
    }

    //remove non-existent 
    size_t c = 0;
    while(c < favorites_.size())
    {
        //process manual favorites only
        if ((favorites_[c].flags_.get() & cft_manual) != 0)
        {
            int idx = find_fav(favorites, favorites_[c].id_);

            if (idx == -1)
                favorites_.erase(favorites_.begin() + c);
            else
                c += 1;
        } else
        {
            c += 1;
        }
    }

    save();
}

void favorites_storage_t::process_channel_changes(const channel_changes_desc_t& changes, const concise_headend_desc_list_t& headend_channels, const headend_fav_auto_map_t& headend_fav_auto_map)
{
    if (changes.has_config_changes() || changes.has_visibility_changes())
    {
        //drop all auto favorites first
        drop_all_auto_favorites();

        //add or update auto favorites for TV and Radio
        add_update_channel_type_favorites(ct_tv, headend_channels);
        add_update_channel_type_favorites(ct_radio, headend_channels);

        //add favorites, based on origin (aka channel group)
        add_origin_favorites(headend_channels, headend_fav_auto_map);

        //add auto favorite for each headend
        add_headend_favorites(headend_channels);

        //remove any hidden or deleted channels from manual favorites
        std::map<dvblink::channel_id_t, bool> channel_map;

        for (size_t i=0; i<changes.hidden_channels_.size(); i++)
            channel_map[changes.hidden_channels_.at(i)] = true;

        for (size_t i=0; i<changes.deleted_channels_.size(); i++)
            channel_map[changes.deleted_channels_.at(i)] = true;

        for (size_t i=0; i<favorites_.size(); i++)
        {
            if ((favorites_[i].flags_.get() & cft_manual) != 0)
            {
                size_t c = 0;
                while (c < favorites_[i].channels_.size())
                {
                    if (channel_map.find(favorites_[i].channels_[c]) != channel_map.end())
                    {
                        //remove this channel
                        favorites_[i].channels_.erase(favorites_[i].channels_.begin() + c);
                    } else
                    {
                        c += 1;
                    }
                }
            }
        }

        save();
    }
}

void favorites_storage_t::drop_all_auto_favorites()
{
    size_t c = 0;
    while(c < favorites_.size())
    {
        if ((favorites_[c].flags_.get() & cft_auto) != 0)
        {
            favorites_.erase(favorites_.begin() + c);
        } else
        {
            c += 1;
        }
    }
}

void favorites_storage_t::add_origin_favorites(const concise_headend_desc_list_t& headend_channels, const headend_fav_auto_map_t& headend_fav_auto_map)
{
    std::map<dvblink::favorite_name_t, channel_favorite_t> origin_favorites;

    for (size_t i=0; i<headend_channels.size(); i++)
    {
        const dvblink::headend_id_t& headend_id = headend_channels[i].id_;
        headend_fav_auto_map_t::const_iterator headend_it = headend_fav_auto_map.find(headend_id);
        if (headend_it == headend_fav_auto_map.end() || !headend_it->second)
            continue;

        for (size_t j=0; j<headend_channels[i].transponders_.size(); j++)
        {
            for (size_t k=0; k<headend_channels[i].transponders_[j].channels_.size(); k++)
            {
                const concise_device_channel_t& channel = headend_channels[i].transponders_[j].channels_[k];
                if (!channel.origin_.empty())
                {
                    dvblink::favorite_name_t fav_name(channel.origin_.get());
                    if (origin_favorites.find(fav_name) == origin_favorites.end())
                    {
                        std::string uuid_str;
                        uuid::gen_uuid(uuid_str);

                        //add this favorite
                        channel_favorite_t fav;
                        fav.id_ = uuid_str;
                        fav.name_ = fav_name;
                        fav.flags_ = cft_auto;
                        origin_favorites[fav_name] = fav;
                    }
                    origin_favorites[fav_name].channels_.push_back(channel.id_);
                }
            }
        }
    }

    std::map<dvblink::favorite_name_t, channel_favorite_t>::iterator it = origin_favorites.begin();
    while (it != origin_favorites.end())
    {
        favorites_.push_back(it->second);

        ++it;
    }
}

void favorites_storage_t::add_headend_favorites(const concise_headend_desc_list_t& headend_channels)
{
    std::map<dvblink::favorite_name_t, channel_favorite_t> origin_favorites;

    for (size_t i=0; i<headend_channels.size(); i++)
    {
        channel_favorite_t fav;
        std::string uuid_str;
        uuid::gen_uuid(uuid_str);
        fav.id_ = uuid_str;
        fav.name_ = headend_channels[i].name_.to_string();
        fav.flags_ = cft_auto;

        for (size_t j=0; j<headend_channels[i].transponders_.size(); j++)
        {
            for (size_t k=0; k<headend_channels[i].transponders_[j].channels_.size(); k++)
            {
                const concise_device_channel_t& channel = headend_channels[i].transponders_[j].channels_[k];
                fav.channels_.push_back(channel.id_);
            }
        }

        favorites_.push_back(fav);
    }
}

void favorites_storage_t::add_update_channel_type_favorites(channel_type_e ch_type, const concise_headend_desc_list_t& headend_channels)
{
    favorite_id_t id;
    favorite_name_t name;
    if (ch_type == ct_tv)
    {
        id = favorites_tv_channels_id;
        name = language_settings::GetInstance()->GetItemName(tv_channels_favorites_name);
    } else
    {
        id = favorites_radio_channels_id;
        name = language_settings::GetInstance()->GetItemName(radio_channels_favorites_name);
    }

    //check if this favorite already exists
    channel_favorite_t* favorite = NULL;
    for (size_t i=0; i<favorites_.size(); i++)
    {
        if (boost::iequals(favorites_[i].id_.get(), id.get()))
        {
            favorite = &(favorites_[i]);
            break;
        }
    }
    if (favorite == NULL)
    {
        //it does not exist yet
        channel_favorite_t fav;
        fav.id_ = id;
        fav.name_ = name;
        fav.flags_ = cft_auto;
        favorites_.push_back(fav);
        favorite = &(favorites_[favorites_.size() - 1]);
    }
    
    favorite->channels_.clear();

    for (size_t i=0; i<headend_channels.size(); i++)
    {
        for (size_t j=0; j<headend_channels[i].transponders_.size(); j++)
        {
            for (size_t k=0; k<headend_channels[i].transponders_[j].channels_.size(); k++)
            {
                const concise_device_channel_t& channel = headend_channels[i].transponders_[j].channels_[k];
                if (channel.type_ == ch_type)
                    favorite->channels_.push_back(channel.id_);
            }
        }
    }
}

void favorites_storage_t::reset()
{
    favorites_.clear();
}

static const std::string favorites_config_root_node                 = "favorites_config";
static const std::string favorites_config_favorite_node                 = "favorite";
static const std::string favorites_config_id_node                     = "id";
static const std::string favorites_config_name_node                 = "name";
static const std::string favorites_config_flags_node                 = "flags";
static const std::string favorites_config_channel_node                 = "channel";

void favorites_storage_t::save()
{
    std::stringstream buf;
    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(favorites_config_root_node.c_str());
    if (root_node != NULL)
    {
        for (size_t idx=0; idx<favorites_.size(); idx++)
        {
            pugi::xml_node fav_node = new_child(root_node, favorites_config_favorite_node);
            if (fav_node != NULL)
            {
                add_node_attribute(fav_node, favorites_config_id_node, favorites_[idx].id_.to_string()); 
                add_node_attribute(fav_node, favorites_config_name_node, favorites_[idx].name_.to_string()); 
                buf.clear(); buf.str("");
                buf << favorites_[idx].flags_;
                add_node_attribute(fav_node, favorites_config_flags_node, buf.str()); 
                for (size_t ch_idx=0; ch_idx<favorites_[idx].channels_.size(); ch_idx++)
                    new_child(fav_node, favorites_config_channel_node, favorites_[idx].channels_[ch_idx].get());
            }
        }

        pugixml_helpers::xmldoc_dump_to_file(doc, settings_->get_favorites_config_filepath().to_string());
    }
}

void favorites_storage_t::load()
{
    reset();

    std::string str;

    pugi::xml_document doc;
    if (doc.load_file(settings_->get_favorites_config_filepath().to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            pugi::xml_node favorite_node = root_node.first_child();
            while (favorite_node != NULL)
            {
                if (boost::iequals(favorite_node.name(), favorites_config_favorite_node))
                {
                    channel_favorite_t fav;
                    if (get_node_attribute(favorite_node, favorites_config_id_node, str))
                    {
                        fav.id_ = str;

                        if (get_node_attribute(favorite_node, favorites_config_name_node, str))
                            fav.name_ = str;
                        if (get_node_attribute(favorite_node, favorites_config_flags_node, str))
                        {
                            unsigned long ul;
                            string_conv::apply(str.c_str(), ul, (unsigned long)cft_none);
                            fav.flags_ = (channel_favorite_type_e)ul;
                        }
                    }

                    pugi::xml_node channel_node = favorite_node.first_child();
                    while (channel_node != NULL)
                    {
                        if (boost::iequals(channel_node.name(), favorites_config_channel_node))
                        {
                            str = channel_node.child_value();

                            if (!str.empty())
                                fav.channels_.push_back(str);
                        }

                        channel_node = channel_node.next_sibling();
                    }

                    favorites_.push_back(fav);
                }

                favorite_node = favorite_node.next_sibling();
            }
        }
    }
}

}