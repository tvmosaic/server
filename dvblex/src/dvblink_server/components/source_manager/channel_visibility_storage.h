/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>
#include <boost/thread.hpp>
#include <dl_types.h>
#include <dl_filesystem_path.h>
#include <dl_channels.h>
#include "settings.h"

namespace dvblex { 

class channel_visibility_storage_t
{
public:
    channel_visibility_storage_t(source_manager_settings_obj_t& settings);
    ~channel_visibility_storage_t();

    void init();
    void term();

    const invisible_channel_map_t* get_invisible_channels();
    void get_invisible_channels(invisible_channel_map_t& invisible_channels);
    void set_invisible_channels(const invisible_channel_map_t& invisible_channels, dvblex::channel_changes_desc_t& ccd);
    void process_channel_changes(const channel_changes_desc_t& changes, const concise_headend_desc_list_t& headend_channels);

    void hide_new_channels(dvblex::channel_changes_desc_t& ccd);

protected:
    invisible_channel_map_t invisible_channels_;
    source_manager_settings_obj_t settings_;

    void save();
    void load();
    void reset();
};

}
