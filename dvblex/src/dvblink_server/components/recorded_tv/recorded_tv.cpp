/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning (disable : 4100)
#pragma warning (disable : 4244)
#endif

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_types.h>
#include <dl_message_addresses.h>
#include <dl_message_server.h>
#include <dl_language_settings.h>
#include <dl_file_procedures.h>
#include <dl_send_to_cmd.h>
#include <dl_xml_serialization.h>
#include <components/recorded_tv.h>
#include "constants.h"
#include "rtv_content_storage.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;

namespace dvblex {

using namespace playback;

recorded_tv_t::recorded_tv_t() :
    id_(recorded_tv_message_queue_addressee.get()),
    content_storage_(NULL),
    shutting_down_(false),
    https_streaming_port_(0),
    streaming_port_(0)
{
}

recorded_tv_t::~recorded_tv_t()
{
	server_->unregister_object(id_.get());

    server_->unregister_queue(message_queue_->get_id());
    message_queue_->shutdown();

    delete content_storage_;
}

i_result recorded_tv_t::query_interface(const base_id_t& requestor_id, const i_guid& iid, i_base_object_t& obj)
{
    i_result res = i_error;

    if (iid == play_source_control_interface)
    {
        boost::shared_lock<boost::shared_mutex> lock(clients_lock_);
        if (!shutting_down_)
        {
            connected_clients_[requestor_id] = requestor_id;
    		obj = playback_handler_->shared_from_this();
            res = i_success;
        } else
        {
            res = i_not_found;
        }
    }
    else
    {
        res = i_not_found;
    }

    return res;
}

bool recorded_tv_t::init(const dvblink::i_server_t& server)
{
    server_ = server;

    playback_handler_ = boost::shared_ptr<playback_handler>(new playback_handler(this));

	//register interface for queryinterface() functionality
	server_->register_object(this->shared_from_this());

    message_queue_ = share_object_safely(new message_queue(id_.get()));
    message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));

    server_->register_queue(message_queue_);

    content_storage_ = new rtv_content_storage_t(message_queue_),

    settings_.init(message_queue_);

    return true;
}

dvblink::network_port_t recorded_tv_t::get_port_from_proto(const dvblink::url_proto_t& proto)
{
    return boost::iequals(proto.get(), "https") ? https_streaming_port_ : streaming_port_;
}

void recorded_tv_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response)
{
    shutting_down_ = false;

    //init content storage
	content_storage_->init();

    //register playback source
    dvblex::playback::pb_source_t pb_source;
    pb_source.id_ = id_.get();
    pb_source.name_ = language_settings::GetInstance()->GetItemName(item_id_t(IDS_RECTV_SOURCE_NAME));

    dvblink::messaging::playback::register_pb_source_request req(pb_source);
    dvblink::messaging::playback::register_pb_source_response resp;
    if (message_queue_->send(server_message_queue_addressee, req, resp) != success || !resp.result_)
    {
        log_warning(L"recorded_tv_t::handle start_request. Error, registering source");
    }

    response.result_ = true;
}

void recorded_tv_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response)
{
	content_storage_->term();

    //we are shutting down and do not accept any connections/query_interface from others
    {
        boost::unique_lock<boost::shared_mutex> lock(clients_lock_);
        shutting_down_ = true;
    }

    //ask all connected clients to release our interfaces. shutdown flag is already set, so the map will not change anymore
    std::map<dvblink::base_id_t, dvblink::base_id_t>::iterator it = connected_clients_.begin();
    while (it != connected_clients_.end())
    {
        release_me_request req;
        req.id_ = id_.get();
        release_me_response resp;
        message_error err = message_queue_->send(it->first.get(), req, resp);
        ++it;
    }
    connected_clients_.clear();
    //close all open files
    {
        boost::unique_lock<boost::shared_mutex> lock(file_info_lock_);
        std::map<dvblink::object_handle_t, FILE*>::iterator it = file_info_map_.begin();
        while (it != file_info_map_.end())
        {
            fclose(it->second);
            ++it;
        }
        file_info_map_.clear();
    }


    //unregister playback source
    dvblink::messaging::playback::unregister_pb_source_request req(id_.get());
    dvblink::messaging::playback::unregister_pb_source_response resp;
    if (message_queue_->send(server_message_queue_addressee, req, resp) != success || !resp.result_)
    {
        log_warning(L"recorded_tv_t::handle shutdown_request. Error, unregistering source");
    }

    response.result_ = true;
}

static bool parse_rtv_object_id(const object_id_t& object_id, std::string& src_id, std::string& container_id, std::string& id_level_1, std::string& id_level_2)
{
    bool ret_val = false;

    src_id.clear();
    container_id.clear();
    id_level_2.clear();
    id_level_1.clear();

    std::string rtv_object_id;
    if (dvblex::playback::parse_object_id(object_id, src_id, rtv_object_id))
    {
        //parse rtv_object_id into container-item_id
        if (boost::iequals(rtv_object_id, object_root_id.get()))
        {
            //source root object (container and id_level_x are empty)
            ret_val = true;
        } else
        {
            //search for a separator
            std::string::size_type pos = rtv_object_id.find(RTV_ID_SEPARATOR);
            if (pos == std::string::npos)
            {
                //container
                container_id = rtv_object_id;
                ret_val = true;
            } else
            {
                //container and id_level_x
                container_id = rtv_object_id.substr(0, pos);
                std::string::size_type level_2_pos = rtv_object_id.find(RTV_ID_SEPARATOR, pos+1);
                if (level_2_pos == std::string::npos)
                {
                    id_level_1 = rtv_object_id.substr(pos+1, rtv_object_id.size() - pos - 1);
                } else
                {
                    id_level_1 = rtv_object_id.substr(pos+1, level_2_pos - pos - 1);
                    id_level_2 = rtv_object_id.substr(level_2_pos+1, rtv_object_id.size() - level_2_pos - 1);
                }
                ret_val = true;
            }
        }
    }

    return ret_val;
}

static bool get_item_id_from_rtv_object_id(std::string& container_id, std::string& id_level_1, std::string& id_level_2, std::string& item_id)
{
    bool res = false;

    if ((boost::iequals(container_id, RTV_BY_NAME_CONTAINER_ID) || 
        boost::iequals(container_id, RTV_IMAGE_CONTAINER_ID) || 
        boost::iequals(container_id, RTV_BY_DATE_CONTAINER_ID)) &&
        id_level_1.size() > 0)
    {
        item_id = id_level_1;
        res = true;
    } else
    if ((boost::iequals(container_id, RTV_BY_GENRE_CONTAINER_ID) || 
        boost::iequals(container_id, RTV_BY_SERIES_CONTAINER_ID)) &&
        id_level_1.size() > 0 && id_level_2.size() > 0)
    {
        item_id = id_level_2;
        res = true;
    }
    return res;
}

std::string recorded_tv_t::get_source_id()
{
    //convert ID to upper case as some programs (kodi) rely on it being uppercase
    return boost::to_upper_copy(id_.to_string());
}

bool recorded_tv_t::add_container_to_container_list(const std::string& container_id, std::string& source_id, const dvblink::url_proto_t& proto_header,
                                                    const dvblink::url_address_t& server_address, 
                                                    const dvblink::network_port_t& server_port, dvblex::playback::pb_container_list_t& container_list)
{
    bool ret_val = true;

	object_id_t object_id;

    if (boost::iequals(container_id, object_root_id.get()))
    {
        //source itself
        std::string src_id = get_source_id();
	    pb_container_t rtv_container(pbct_container_source, pbit_item_unknown);
	    rtv_container.object_id_ = make_object_id(src_id, object_root_id.get());
	    rtv_container.parent_id_ = object_root_id.get();
	    rtv_container.source_id_ = src_id;
	    rtv_container.name_ = language_settings::GetInstance()->GetItemName(item_id_t(IDS_RECTV_SOURCE_NAME));
	    rtv_container.description_ = language_settings::GetInstance()->GetItemName(item_id_t(IDS_RECTV_SOURCE_DESC));

        object_id_t image_container_id = make_object_id(src_id, RTV_IMAGE_CONTAINER_ID);
        object_id_t logo_id(image_container_id.get() + RTV_ID_SEPARATOR + RTV_SOURCE_LOGO_ID);
	    rtv_container.logo_ = url_address_t(make_object_url(proto_header.get(), server_address, server_port, logo_id.get()));

        rtv_container.total_count_ = RTV_TOP_LEVEL_CONTAINER_COUNT;
        container_list.push_back(rtv_container);
    } else
    if (boost::iequals(container_id, RTV_BY_GENRE_CONTAINER_ID))
    {
        //by genre container
	    pb_container_t rtv_container(pbct_container_category_group, pbit_item_recorded_tv);
	    object_id = make_object_id(source_id, std::string(RTV_BY_GENRE_CONTAINER_ID));
	    rtv_container.object_id_ = object_id;
	    rtv_container.parent_id_ = make_object_id(source_id, object_root_id.get());
	    rtv_container.name_ = language_settings::GetInstance()->GetItemName(item_id_t(IDS_RECTV_BY_GENRE_CONTAINER_NAME));
	    rtv_container.description_ = language_settings::GetInstance()->GetItemName(item_id_t(IDS_RECTV_BY_GENRE_CONTAINER_DESC));

        object_id_t image_container_id = make_object_id(source_id, RTV_IMAGE_CONTAINER_ID);
        object_id_t logo_id(image_container_id.get() + RTV_ID_SEPARATOR + RTV_BY_GENRE_LOGO_ID);
	    rtv_container.logo_ = url_address_t(make_object_url(proto_header.get(), server_address, server_port, logo_id.get()));

        rtv_container.total_count_ = content_storage_->get_genre_container_count();
	    container_list.push_back(rtv_container);
    } else
    if (boost::iequals(container_id, RTV_BY_SERIES_CONTAINER_ID))
    {
        //by genre container
	    pb_container_t rtv_container(pbct_container_category_group, pbit_item_recorded_tv);
	    object_id = make_object_id(source_id, std::string(RTV_BY_SERIES_CONTAINER_ID));
	    rtv_container.object_id_ = object_id;
	    rtv_container.parent_id_ = make_object_id(source_id, object_root_id.get());
	    rtv_container.name_ = language_settings::GetInstance()->GetItemName(item_id_t(IDS_RECTV_BY_SERIES_CONTAINER_NAME));
	    rtv_container.description_ = language_settings::GetInstance()->GetItemName(item_id_t(IDS_RECTV_BY_SERIES_CONTAINER_DESC));

        object_id_t image_container_id = make_object_id(source_id, RTV_IMAGE_CONTAINER_ID);
        object_id_t logo_id(image_container_id.get() + RTV_ID_SEPARATOR + RTV_BY_SERIES_LOGO_ID);
	    rtv_container.logo_ = url_address_t(make_object_url(proto_header.get(), server_address, server_port, logo_id.get()));

        rtv_container.total_count_ = content_storage_->get_series_container_count();
	    container_list.push_back(rtv_container);
    } else
    if (boost::iequals(container_id, RTV_BY_DATE_CONTAINER_ID))
    {
        //by date container itself
	    pb_container_t rtv_container(pbct_container_category_sort, pbit_item_recorded_tv);
	    object_id = make_object_id(source_id, std::string(RTV_BY_DATE_CONTAINER_ID));
	    rtv_container.object_id_ = object_id;
	    rtv_container.parent_id_ = make_object_id(source_id, object_root_id.get());
	    rtv_container.name_ = language_settings::GetInstance()->GetItemName(item_id_t(IDS_RECTV_BY_DATE_CONTAINER_NAME));
	    rtv_container.description_ = language_settings::GetInstance()->GetItemName(item_id_t(IDS_RECTV_BY_DATE_CONTAINER_DESC));

        object_id_t image_container_id = make_object_id(source_id, RTV_IMAGE_CONTAINER_ID);
        object_id_t logo_id(image_container_id.get() + RTV_ID_SEPARATOR + RTV_BY_DATE_LOGO_ID);
	    rtv_container.logo_ = url_address_t(make_object_url(proto_header.get(), server_address, server_port, logo_id.get()));

        rtv_container.total_count_ = content_storage_->get_item_count();
	    container_list.push_back(rtv_container);
    } else
    if (boost::iequals(container_id, RTV_BY_NAME_CONTAINER_ID))
    {
        //by name container itself
	    pb_container_t rtv_container(pbct_container_category_sort, pbit_item_recorded_tv);
        object_id = make_object_id(source_id, std::string(RTV_BY_NAME_CONTAINER_ID));
	    rtv_container.object_id_ = object_id;
	    rtv_container.parent_id_ = make_object_id(source_id, object_root_id.get());
	    rtv_container.name_ = language_settings::GetInstance()->GetItemName(item_id_t(IDS_RECTV_BY_NAME_CONTAINER_NAME));
	    rtv_container.description_ = language_settings::GetInstance()->GetItemName(item_id_t(IDS_RECTV_BY_NAME_CONTAINER_DESC));

        object_id_t image_container_id = make_object_id(source_id, RTV_IMAGE_CONTAINER_ID);
        object_id_t logo_id(image_container_id.get() + RTV_ID_SEPARATOR + RTV_BY_NAME_LOGO_ID);
	    rtv_container.logo_ = url_address_t(make_object_url(proto_header.get(), server_address, server_port, logo_id.get()));

        rtv_container.total_count_ = content_storage_->get_item_count();
	    container_list.push_back(rtv_container);
    } else
    {
        ret_val = false;
    }

    return ret_val;
}

static boost::uint64_t get_filesize(const dvblink::filesystem_path_t& fname)
{
    boost::uint64_t ret_val = dvblink::engine::filesystem::get_file_size(fname.to_wstring());

    if (ret_val < 0)
        ret_val = 0;

    return ret_val;
}

void recorded_tv_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_objects_request& request, dvblink::messaging::playback::get_objects_response& response)
{
	response.result_ = false;

    update_streaming_port();

    dvblex::playback::pb_object_t& object_info = response.object_;
    dvblink::url_address_t server_address = request.server_address_;
    dvblink::network_port_t server_port = get_port_from_proto(request.proto_);

	//find out source and object id
	std::string source_id;
	std::string container_id;
    std::string id_level_1;
    std::string id_level_2;
    if (parse_rtv_object_id(request.object_id_, source_id, container_id, id_level_1, id_level_2))
	{
        if (boost::iequals(source_id, get_source_id()))
        {
            if (request.is_children_request_)
            {
		        //is this source root request
                if (container_id.size() == 0)
		        {
			        //at this level there are only containers
			        if (request.object_type_ == pot_object_unknown ||
				        request.object_type_ == pot_object_container)
			        {
				        //add containers
				        response.result_ = generate_root_response(request.proto_, server_address, server_port, object_info);
			        } else
			        {
                        //empty response
				        response.result_ = true;
			        }
		        } else
		        {
			        //is this by date/name container?
			        if ((boost::iequals(container_id, RTV_BY_DATE_CONTAINER_ID) || boost::iequals(container_id, RTV_BY_NAME_CONTAINER_ID)))
			        {
				        //we only have items
				        if (request.object_type_ == pot_object_unknown ||
					        request.object_type_ == pot_object_item)
				        {
					        //we only have recorded tv items
					        if (request.item_type_ == pbit_item_unknown || request.item_type_ == pbit_item_recorded_tv)
					        {
                                object_id_t parent_id = make_object_id(source_id, container_id);
                                object_id_t image_container_id = make_object_id(source_id, RTV_IMAGE_CONTAINER_ID);
						        if (boost::iequals(container_id, RTV_BY_DATE_CONTAINER_ID))
							        response.result_ = content_storage_->get_items_by_date(parent_id.get(), image_container_id, 
                                        request.start_position_, request.requested_count_, request.proto_, server_address, server_port, object_info);
						        if (boost::iequals(container_id, RTV_BY_NAME_CONTAINER_ID))
							        response.result_ = content_storage_->get_items_by_name(parent_id.get(), image_container_id, 
                                        request.start_position_, request.requested_count_, request.proto_, server_address, server_port, object_info);
					        } else
					        {
                                //empty response
						        response.result_ = true;
					        }
				        } else
				        {
                            //empty response
					        response.result_ = true;
				        }
			        } else
			        //is this by genre container?
			        if (boost::iequals(container_id, RTV_BY_GENRE_CONTAINER_ID))
			        {
                        if (id_level_1.size() == 0)
                        {
                            //request for genre containers
                            object_id_t parent_id = make_object_id(source_id, container_id);
                            object_id_t image_container_id = make_object_id(source_id, RTV_IMAGE_CONTAINER_ID);
					        response.result_ = content_storage_->add_genre_container(parent_id.get(), dvblink::object_id_t(), image_container_id, 
                                request.proto_, server_address, server_port, object_info);
                        } else
                        {
                            //request for items of this genre
                            std::string pid = container_id + RTV_ID_SEPARATOR + id_level_1;
                            object_id_t parent_id = make_object_id(source_id, pid);
                            object_id_t image_container_id = make_object_id(source_id, RTV_IMAGE_CONTAINER_ID);
					        response.result_ = content_storage_->add_genre_items(parent_id.get(), id_level_1, image_container_id, 
                                request.start_position_, request.requested_count_, request.proto_, server_address, server_port, object_info);
                        }
			        } else
			        if (boost::iequals(container_id, RTV_BY_SERIES_CONTAINER_ID))
			        {
                        if (id_level_1.size() == 0)
                        {
                            //request for series containers
                            object_id_t parent_id = make_object_id(source_id, container_id);
                            object_id_t image_container_id = make_object_id(source_id, RTV_IMAGE_CONTAINER_ID);
					        response.result_ = content_storage_->add_series_container(parent_id.get(), dvblink::object_id_t(), image_container_id, 
                                request.proto_, server_address, server_port, object_info);
                        } else
                        {
                            //request for items of this serie
                            std::string pid = container_id + RTV_ID_SEPARATOR + id_level_1;
                            object_id_t parent_id = make_object_id(source_id, pid);
                            object_id_t image_container_id = make_object_id(source_id, RTV_IMAGE_CONTAINER_ID);
					        response.result_ = content_storage_->add_series_items(parent_id.get(), id_level_1, image_container_id, 
                                request.start_position_, request.requested_count_, request.proto_, server_address, server_port, object_info);
                        }
			        } else
			        {
				        log_error(L"rtv_control_t::get_objects. Request for children of unknown object id %1%") % string_cast<EC_UTF8>(request.object_id_.get());
			        }
		        }
            } else
            {
                //object info request
                std::string item_id;
                if (get_item_id_from_rtv_object_id(container_id, id_level_1, id_level_2, item_id))
                {
                    //is this an image?
                    dvblink::filesystem_path_t item_path;
                    if (get_image_pathname(container_id, item_id, item_path))
                    {
		                boost::shared_ptr<pb_image_t> image_item(new pb_image_t());
                        image_item->size_ = get_filesize(item_path);
	                    object_info.item_list_.push_back(image_item);
                        object_info.actual_count_ = object_info.item_list_.size();
                        object_info.total_count_ = object_info.item_list_.size();

                        response.result_ = true;
                    } else
                    {
                        //this is item
                        std::string pid = container_id; //one level of items
                        if (boost::iequals(container_id, RTV_BY_GENRE_CONTAINER_ID) || boost::iequals(container_id, RTV_BY_SERIES_CONTAINER_ID))
                                pid = container_id + RTV_ID_SEPARATOR + id_level_1; //two levels of items

                        object_id_t parent_id = make_object_id(source_id, pid);
                        object_id_t image_container_id = make_object_id(source_id, RTV_IMAGE_CONTAINER_ID);
                        response.result_ = content_storage_->get_item_info(item_id, parent_id.get(), image_container_id, request.proto_, server_address, server_port, object_info);
                    }
                } else
                {
                    //this is container
                    std::string ws = get_source_id();
                    //top level container?
                    if (id_level_1.size() == 0)
                    {
                        if (add_container_to_container_list(container_id, ws, request.proto_, server_address, server_port, object_info.container_list_))
                        {
                            object_info.actual_count_ = object_info.container_list_.size();
                            object_info.total_count_ = object_info.container_list_.size();

                            response.result_ = true;
                        }
                    } else
                    {
                        //second level container
                        if (boost::iequals(container_id, RTV_BY_GENRE_CONTAINER_ID))
                        {
                            object_id_t parent_id = make_object_id(source_id, container_id);
                            object_id_t image_container_id = make_object_id(source_id, RTV_IMAGE_CONTAINER_ID);
					        response.result_ = content_storage_->add_genre_container(parent_id.get(), id_level_1, image_container_id, 
                                request.proto_, server_address, server_port, object_info);
                        } else
                        if (boost::iequals(container_id, RTV_BY_SERIES_CONTAINER_ID))
                        {
                            object_id_t parent_id = make_object_id(source_id, container_id);
                            object_id_t image_container_id = make_object_id(source_id, RTV_IMAGE_CONTAINER_ID);
					        response.result_ = content_storage_->add_series_container(parent_id.get(), id_level_1, image_container_id, 
                                request.proto_, server_address, server_port, object_info);
                        }
                    }
                }
            }
        } else
        {
	        log_error(L"rtv_control_t::get_objects. Request for unknown source id %1%") % string_cast<EC_UTF8>(source_id);
        }
	} else
	{
		log_error(L"rtv_control_t::get_objects. Unable to parse object id %1%") % string_cast<EC_UTF8>(request.object_id_.get());
	}
}

bool recorded_tv_t::generate_root_response(const dvblink::url_proto_t& proto, const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, dvblex::playback::pb_object_t& object_info)
{
    std::string s = get_source_id();
	add_container_to_container_list(RTV_BY_NAME_CONTAINER_ID, s, proto, server_address, server_port, object_info.container_list_);
    add_container_to_container_list(RTV_BY_DATE_CONTAINER_ID, s, proto, server_address, server_port, object_info.container_list_);
    add_container_to_container_list(RTV_BY_SERIES_CONTAINER_ID, s, proto, server_address, server_port, object_info.container_list_);
    add_container_to_container_list(RTV_BY_GENRE_CONTAINER_ID, s, proto, server_address, server_port, object_info.container_list_);

	object_info.actual_count_ = object_info.container_list_.size();
	object_info.total_count_ = object_info.container_list_.size();

    return true;
}

void recorded_tv_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_source_container_request& request, dvblink::messaging::playback::get_source_container_response& response)
{
    update_streaming_port();

    std::string src_id = get_source_id();
    dvblex::playback::pb_container_list_t cl;
    add_container_to_container_list(object_root_id.get(), src_id, request.proto_, request.server_address_, get_port_from_proto(request.proto_), cl);
    if (cl.size() > 0)
    {
        response.container_ = cl[0];
        response.result_ = true;
    } else
    {
        response.result_ = false;
    }
}

bool recorded_tv_t::get_image_pathname(std::string& container_id, std::string& item_id, dvblink::filesystem_path_t& path)
{
    bool ret_val = false;

    if (boost::iequals(container_id, RTV_IMAGE_CONTAINER_ID))
    {
        ret_val = true;
//        settings_->get_source_dir(path);

        if (boost::iequals(item_id, RTV_BY_NAME_LOGO_ID))
            path /= RTV_BY_NAME_CONTAINER_FILENAME;
        else if (boost::iequals(item_id, RTV_BY_DATE_LOGO_ID))
            path /= RTV_BY_DATE_CONTAINER_FILENAME;
        else if (boost::iequals(item_id, RTV_SOURCE_LOGO_ID))
            path /= RTV_SOURCE_CONTAINER_FILENAME;
        else if (boost::iequals(item_id, RTV_BY_SERIES_LOGO_ID))
            path /= RTV_BY_SERIES_CONTAINER_FILENAME;
        else if (boost::iequals(item_id, RTV_BY_SERIES_GROUP_LOGO_ID))
            path /= RTV_BY_SERIES_GROUP_CONTAINER_FILENAME;
        else if (boost::iequals(item_id, RTV_BY_GENRE_LOGO_ID))
            path /= RTV_BY_GENRE_CONTAINER_FILENAME;
        else if (boost::iequals(item_id, RTV_BY_GENRE_DOC_LOGO_ID))
            path /= RTV_BY_GENRE_DOC_CONTAINER_FILENAME;
        else if (boost::iequals(item_id, RTV_BY_GENRE_MUSIC_LOGO_ID))
            path /= RTV_BY_GENRE_MUSIC_CONTAINER_FILENAME;
        else if (boost::iequals(item_id, RTV_BY_GENRE_MOVIE_LOGO_ID))
            path /= RTV_BY_GENRE_MOVIE_CONTAINER_FILENAME;
        else if (boost::iequals(item_id, RTV_BY_GENRE_KIDS_LOGO_ID))
            path /= RTV_BY_GENRE_KIDS_CONTAINER_FILENAME;
        else if (boost::iequals(item_id, RTV_BY_GENRE_NEWS_LOGO_ID))
            path /= RTV_BY_GENRE_NEWS_CONTAINER_FILENAME;
        else if (boost::iequals(item_id, RTV_BY_GENRE_SPORTS_LOGO_ID))
            path /= RTV_BY_GENRE_SPORTS_CONTAINER_FILENAME;
        else if (boost::iequals(item_id, RTV_BY_GENRE_OTHER_LOGO_ID))
            path /= RTV_BY_GENRE_OTHER_CONTAINER_FILENAME;
        else
            ret_val = content_storage_->get_thumbnail_fname(item_id, path);
    }

    return ret_val;
}

void recorded_tv_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::local_object_info_request& request, dvblink::messaging::playback::local_object_info_response& response)
{
    response.result_ = false;
	std::string source_id;
	std::string container_id;
    std::string id_level_1;
    std::string id_level_2;
    if (parse_rtv_object_id(request.object_id_, source_id, container_id, id_level_1, id_level_2))
	{
        std::string item_id;
        if (get_item_id_from_rtv_object_id(container_id, id_level_1, id_level_2, item_id))
	    {
            dvblink::filesystem_path_t item_path;
            if (content_storage_->get_item_fname(item_id, item_path))
            {
				response.result_ = true;
				response.file_path_ = item_path;
			}
		}
	}
}

void recorded_tv_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::open_item_request& request, dvblink::messaging::playback::open_item_response& response)
{
    response.result_ = false;

	std::string source_id;
	std::string container_id;
    std::string id_level_1;
    std::string id_level_2;
    if (parse_rtv_object_id(request.object_id_, source_id, container_id, id_level_1, id_level_2))
	{
        std::string item_id;
        if (get_item_id_from_rtv_object_id(container_id, id_level_1, id_level_2, item_id))
	    {
            bool b_found = false;
            dvblink::filesystem_path_t item_path;
            //check first if this is an image
            b_found = get_image_pathname(container_id, item_id, item_path);
            //is this a video file itself?
            if (!b_found)
                b_found = content_storage_->get_item_fname(item_id, item_path);
            if (b_found)
            {
                log_info(L"recorded_tv_t::handle. Opening file %1%") % item_path.to_wstring();

                //get size
                response.item_size_bytes_ = get_filesize(item_path);

                //open a file
                FILE* f = filesystem::universal_open_file(item_path, "rb");

                if (f != NULL)
                {
                    boost::unique_lock<boost::shared_mutex> lock(file_info_lock_);
                    std::string str;
                    uuid::gen_uuid(str);
                    response.handle_ = str;
                    file_info_map_[response.handle_] = f;

                    response.result_ = true;
                }
                else
                {
                    log_error(L"recorded_tv_t::handle. Error opening file %1%") % item_path.get();
                }
            }
        }
    }
}

void recorded_tv_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::seek_item_request& request, dvblink::messaging::playback::seek_item_response& response)
{
    response.result_ = false;

    FILE* f = NULL;

    {
        boost::shared_lock<boost::shared_mutex> lock(file_info_lock_);
        if (file_info_map_.find(request.handle_) != file_info_map_.end())
            f = file_info_map_[request.handle_];
    }

    if (f != NULL)
        response.result_ = (filesystem::universal_file_seek(f, request.offset_, SEEK_SET) == 0);
}

void recorded_tv_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::close_item_request& request, dvblink::messaging::playback::close_item_response& response)
{
    response.result_ = false;
    boost::unique_lock<boost::shared_mutex> lock(file_info_lock_);
    if (file_info_map_.find(request.handle_) != file_info_map_.end())
    {
        fclose(file_info_map_[request.handle_]);
        file_info_map_.erase(request.handle_);
        response.result_ = true;
    }
}

bool __stdcall recorded_tv_t::read_data(const dvblink::object_handle_safe_t& handle, unsigned char* buffer, boost::uint32_t& buffer_length)
{
    bool ret_val = false;

    object_handle_t object_handle(handle.get());

    FILE* f = NULL;

    {
        boost::shared_lock<boost::shared_mutex> lock(file_info_lock_);
        if (file_info_map_.find(object_handle) != file_info_map_.end())
            f = file_info_map_[object_handle];
    }

    if (f != NULL)
    {
        size_t ret = fread((void*)buffer, 1, buffer_length, f);
        //was there an error?
        if (ret < buffer_length)
            ret_val = (ferror(f) == 0);
        else
            ret_val = true;
        //number of actually read bytes
        buffer_length = static_cast<boost::uint32_t>(ret);
    }

    return ret_val;
}

void recorded_tv_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::timer_recording_completed_request& request)
{
    if (!shutting_down_)
    {
        update_streaming_port();

        //force reload
        content_storage_->reload_content();

        //pretend that we archive object in "by date container" and find its object id by its timer_id
        object_id_t parent_id = playback::make_object_id(get_source_id(), RTV_BY_DATE_CONTAINER_ID);
        object_id_t object_id(parent_id.get() + RTV_ID_SEPARATOR + request.timer_id_.to_string());

        //submit sendto jobs for each target
        send_to_add_item_request req;
        for (size_t i=0; i<request.targets_.size(); i++)
        {
            send_to_work_item wi;
            wi.pb_object_id = object_id;
            wi.target_id = request.targets_[i];
            req.work_items_.push_back(wi);
        }

        std::string inparam;
        write_to_xml(req, inparam);

        url_address_t server_address; //empty - has no meaning here
        messaging::xml_message_post_request xml_req(server_address, SEND_TO_CMD_ADD_ITEM, inparam);
        message_queue_->post(messaging::social_message_queue_addressee, xml_req);
    }
}

void recorded_tv_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::remove_object_request& request, dvblink::messaging::playback::remove_object_response& response)
{
    std::string source_id;
    std::string container_id;
    std::string id_level_1;
    std::string id_level_2;
    if (parse_rtv_object_id(request.object_id_, source_id, container_id, id_level_1, id_level_2))
    {
        std::string item_id;
        if (get_item_id_from_rtv_object_id(container_id, id_level_1, id_level_2, item_id))
            response.result_ = content_storage_->delete_item(item_id);
    }
}

void recorded_tv_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::stop_recording_request& request, dvblink::messaging::playback::stop_recording_response& response)
{
    std::string source_id;
    std::string container_id;
    std::string id_level_1;
    std::string id_level_2;
    if (parse_rtv_object_id(request.object_id_, source_id, container_id, id_level_1, id_level_2))
    {
        std::string item_id;
        if (get_item_id_from_rtv_object_id(container_id, id_level_1, id_level_2, item_id))
            response.result_ = content_storage_->stop_recording(item_id);
    }
}

void recorded_tv_t::update_streaming_port()
{
    if (streaming_port_.get() == 0)
    {
        dvblink::messaging::playback::get_playback_streaming_port_request req;
        dvblink::messaging::playback::get_playback_streaming_port_response resp;
        if (message_queue_->send(network_server_message_queue_addressee, req, resp) == success && resp.result_)
        {
            streaming_port_ = resp.port_;
            https_streaming_port_ = resp.https_port_;
        }
    }
}

} // dvblink
