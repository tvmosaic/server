/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <dl_logger.h>
#include <dl_uuid.h>
#include <dl_message_recorder.h>
#include <dl_language_settings.h>
#include <dl_pb_types.h>
#include <dl_pb_object.h>
#include <dl_xml_serialization.h>
#include <dl_timer_serializer.h>
#include "constants.h"
#include "rtv_content_storage.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblex::playback;

rtv_content_storage_t::rtv_content_storage_t(const dvblink::messaging::message_queue_t& message_queue) :
    message_queue_(message_queue),
    reload_time_(boost::posix_time::not_a_date_time),
    reload_period_sec_(60)
{
}

rtv_content_storage_t::~rtv_content_storage_t()
{
}

bool rtv_content_storage_t::init()
{
    reload_time_ = boost::posix_time::not_a_date_time;
    //add genre cntainers
    rtv_genre_container_t genre_container;
    //movie
    genre_container.init(RTVGC_MOVIE, "IDS_RECTV_GENRE_MOVIE_CONTAINER_NAME", RTV_BY_GENRE_MOVIE_LOGO_ID, RTVGC_MOVIE);
    genre_container_list_.push_back(genre_container);
    //kids
    genre_container.init(RTVGC_KIDS, "IDS_RECTV_GENRE_KIDS_CONTAINER_NAME", RTV_BY_GENRE_KIDS_LOGO_ID, RTVGC_KIDS);
    genre_container_list_.push_back(genre_container);
    //sport
    genre_container.init(RTVGC_SPORT, "IDS_RECTV_GENRE_SPORT_CONTAINER_NAME", RTV_BY_GENRE_SPORTS_LOGO_ID, RTVGC_SPORT);
    genre_container_list_.push_back(genre_container);
    //Documentary
    genre_container.init(RTVGC_DOCUMENTARY, "IDS_RECTV_GENRE_DOC_CONTAINER_NAME", RTV_BY_GENRE_DOC_LOGO_ID, RTVGC_DOCUMENTARY);
    genre_container_list_.push_back(genre_container);
    //music
    genre_container.init(RTVGC_MUSIC, "IDS_RECTV_GENRE_MUSIC_CONTAINER_NAME", RTV_BY_GENRE_MUSIC_LOGO_ID, RTVGC_MUSIC);
    genre_container_list_.push_back(genre_container);
    //news
    genre_container.init(RTVGC_NEWS, "IDS_RECTV_GENRE_NEWS_CONTAINER_NAME", RTV_BY_GENRE_NEWS_LOGO_ID, RTVGC_NEWS);
    genre_container_list_.push_back(genre_container);
    //other
    genre_container.init(RTVGC_OTHER, "IDS_RECTV_GENRE_OTHER_CONTAINER_NAME", RTV_BY_GENRE_OTHER_LOGO_ID, RTVGC_OTHER);
    genre_container_list_.push_back(genre_container);
	return true;
}

bool rtv_content_storage_t::term()
{
	return true;
}

void rtv_content_storage_t::reload_content()
{
    boost::posix_time::ptime cur_time = boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time();
    reload_content(cur_time);
}

void rtv_content_storage_t::check_and_reload_content()
{
    boost::posix_time::ptime cur_time = boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time();

    bool b_reload = false;
    if (!reload_time_.is_not_a_date_time())
    {
	    boost::posix_time::time_duration duration(cur_time - reload_time_);
	    if (duration.total_seconds() > reload_period_sec_)
        {
            b_reload = true;
        }
    } else
    {
        b_reload = true;
    }

    if (b_reload)
    {
        log_info(L"rtv_content_storage_t::check_and_reload_content: Reading recorded content from recorder");
        reload_content(cur_time);
    }
}

void rtv_content_storage_t::reload_content(boost::posix_time::ptime cur_time)
{
    if (cur_time.is_not_a_date_time())
        cur_time = boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time();

    get_recorded_tv_items();

    reload_time_ = cur_time;
}

int rtv_content_storage_t::get_item_count()
{
	boost::unique_lock<boost::shared_mutex> lock(items_lock_);
    check_and_reload_content();

    return (int)recorded_tv_items_.size();
}

bool rtv_content_storage_t::delete_item(std::string& item_id)
{
    bool ret_val = false;
    messaging::recorder::remove_completed_recording_request req(item_id);
    messaging::recorder::remove_completed_recording_response resp;
    if (message_queue_->send(messaging::recorder_message_queue_addressee, req, resp) == messaging::success && resp.result_)
    {
        boost::unique_lock<boost::shared_mutex> lock(items_lock_);
        reload_content(boost::posix_time::not_a_date_time);
        ret_val = true;
    }

    return ret_val;
}

bool rtv_content_storage_t::stop_recording(std::string& item_id)
{
    bool ret_val = false;

    dvblex::rd_recording_remover_t recording_remover;
    recording_remover.set_recording_id(item_id);
    std::string recording_remover_str;
    if (dvblex::write_to_xml<dvblex::rd_recording_remover_t>(recording_remover, recording_remover_str))
    {
        messaging::recorder::remove_recording_request req(recording_remover_str);
        messaging::recorder::remove_recording_response resp;
        if (message_queue_->send(messaging::recorder_message_queue_addressee, req, resp) == messaging::success && resp.result_)
        {
            boost::unique_lock<boost::shared_mutex> lock(items_lock_);
            reload_content(boost::posix_time::not_a_date_time);
            ret_val = true;
        }
    }

    return ret_val;
}

bool rtv_content_storage_t::get_items_by_date(const dvblink::object_id_t& parent_id, const dvblink::object_id_t& image_container_id, int start, int count, const dvblink::url_proto_t& proto,
                                              const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, dvblex::playback::pb_object_t& object_info)
{
    boost::unique_lock<boost::shared_mutex> lock(items_lock_);

	return get_items_from_vector(items_by_date_, parent_id, image_container_id, start, count, proto, server_address, server_port, object_info);
}

bool rtv_content_storage_t::get_items_by_name(const dvblink::object_id_t& parent_id, const dvblink::object_id_t& image_container_id, int start, int count, const dvblink::url_proto_t& proto,
                                              const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, dvblex::playback::pb_object_t& object_info)
{
	boost::unique_lock<boost::shared_mutex> lock(items_lock_);

	return get_items_from_vector(items_by_name_, parent_id, image_container_id, start, count, proto, server_address, server_port, object_info);
}

bool rtv_content_storage_t::get_item_fname(std::string& item_id, dvblink::filesystem_path_t& item_path)
{
    bool ret_val = false;

	boost::unique_lock<boost::shared_mutex> lock(items_lock_);

    //check whether recorded content list is up to date
    check_and_reload_content();
    if (recorded_tv_items_.find(item_id) != recorded_tv_items_.end())
    {
    	pb_item_list_t pb_item_list;

        boost::shared_ptr<pb_recorded_tv_t> recorded_tv(new pb_recorded_tv_t());
		*recorded_tv = recorded_tv_items_[item_id];
        dvblink::url_address_t url = recorded_tv->url_;
        item_path = string_cast<EC_UTF8>(url.get());
        ret_val = true;
    }
    return ret_val;
}

bool rtv_content_storage_t::get_thumbnail_fname(std::string& item_id, dvblink::filesystem_path_t& item_path)
{
    bool ret_val = false;

	boost::unique_lock<boost::shared_mutex> lock(items_lock_);

    //check whether recorded content list is up to date
    check_and_reload_content();
    if (recorded_tv_items_.find(item_id) != recorded_tv_items_.end())
    {
    	pb_item_list_t pb_item_list;

        boost::shared_ptr<pb_recorded_tv_t> recorded_tv(new pb_recorded_tv_t());
		*recorded_tv = recorded_tv_items_[item_id];
        item_path = string_cast<EC_UTF8>(recorded_tv->video_info_.m_ImageURL);
        ret_val = true;
    }
    return ret_val;
}

bool rtv_content_storage_t::get_item_info(std::string& item_id, const dvblink::object_id_t& parent_id, const dvblink::object_id_t& image_container_id, const dvblink::url_proto_t& proto,
                                          const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, dvblex::playback::pb_object_t& object_info)
{
    bool ret_val = false;

	boost::unique_lock<boost::shared_mutex> lock(items_lock_);

    //check whether recorded content list is up to date
    check_and_reload_content();
    if (recorded_tv_items_.find(item_id) != recorded_tv_items_.end())
    {
        boost::shared_ptr<pb_recorded_tv_t> recorded_tv(new pb_recorded_tv_t());
		*recorded_tv = recorded_tv_items_[item_id];
		//set object id and url (relative) to object id
		object_id_t object_id(parent_id.get() + RTV_ID_SEPARATOR + item_id);
		recorded_tv->object_id_ = object_id;
		recorded_tv->parent_id_ = parent_id;
        recorded_tv->url_ = url_address_t(make_object_url(proto.get(), server_address, server_port, object_id.get()));

		//set image url (relative) to image container
        if (recorded_tv->video_info_.m_ImageURL.size() > 0)
        {
		    object_id_t image_id(image_container_id.get() + RTV_ID_SEPARATOR + item_id);
            url_address_t image_url(make_object_url(proto.get(), server_address, server_port, image_id.get()));
            recorded_tv->thumbnail_ = image_url;
            recorded_tv->video_info_.m_ImageURL = image_url.get();
        }

		object_info.item_list_.push_back(recorded_tv);

	    object_info.actual_count_ = object_info.item_list_.size();
	    object_info.total_count_ = object_info.item_list_.size();

        ret_val = true;
    }
    return ret_val;
}

bool rtv_content_storage_t::add_genre_container(const dvblink::object_id_t& parent_id, const dvblink::object_id_t& container_id,
    const dvblink::object_id_t& image_container_id, const dvblink::url_proto_t& proto, const dvblink::url_address_t& server_address, 
    const dvblink::network_port_t& server_port, dvblex::playback::pb_object_t& object_info)
{
	boost::unique_lock<boost::shared_mutex> lock(items_lock_);
    //check whether recorded content list is up to date
    check_and_reload_content();
    //add containers
    for (size_t i=0; i<genre_container_list_.size(); i++)
    {
        if (container_id.get().size() == 0 || 
            (container_id.get().size() != 0 && boost::iequals(genre_container_list_[i].container_id, container_id.get())))
        {
	        pb_container_t rtv_container(pbct_container_group, pbit_item_recorded_tv);
            object_id_t object_id(parent_id.get() + RTV_ID_SEPARATOR + genre_container_list_[i].container_id);
	        rtv_container.object_id_ = object_id;
	        rtv_container.parent_id_ = parent_id;
	        rtv_container.name_ = language_settings::GetInstance()->GetItemName(item_id_t(genre_container_list_[i].container_name_ids));

            object_id_t logo_id(image_container_id.get() + RTV_ID_SEPARATOR + genre_container_list_[i].logo_id);
	        rtv_container.logo_ = url_address_t(make_object_url(proto.get(), server_address, server_port, logo_id.get()));

            rtv_container.total_count_ = genre_container_list_[i].items.size();
	        object_info.container_list_.push_back(rtv_container);

            if (container_id.get().size() != 0)
                break;
        }
    }

    object_info.actual_count_ = object_info.container_list_.size();
    object_info.total_count_ = object_info.container_list_.size();

    return true;
}

bool rtv_content_storage_t::add_genre_items(const dvblink::object_id_t& parent_id, const dvblink::object_id_t& container_id, const dvblink::object_id_t& image_container_id, 
    int start, int count, const dvblink::url_proto_t& proto, const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, 
    dvblex::playback::pb_object_t& object_info)
{
    bool res = false;

    boost::unique_lock<boost::shared_mutex> lock(items_lock_);

    for (size_t i=0; i<genre_container_list_.size(); i++)
    {
        if (boost::iequals(genre_container_list_[i].container_id, container_id.get()))
        {
            res = get_items_from_vector(genre_container_list_[i].items, parent_id, image_container_id, start, count, proto, server_address, server_port, object_info);
            break;
        }
    }

	return res;
}

bool rtv_content_storage_t::add_series_container(const dvblink::object_id_t& parent_id, const dvblink::object_id_t& container_id,
    const dvblink::object_id_t& image_container_id, const dvblink::url_proto_t& proto, const dvblink::url_address_t& server_address, 
    const dvblink::network_port_t& server_port, dvblex::playback::pb_object_t& object_info)
{
	boost::unique_lock<boost::shared_mutex> lock(items_lock_);
    //check whether recorded content list is up to date
    check_and_reload_content();
    //add containers
    for (size_t i=0; i<series_container_list_.size(); i++)
    {
        if (container_id.get().size() == 0 || 
            (container_id.get().size() != 0 && boost::iequals(series_container_list_[i].container_id, container_id.get())))
        {
	        pb_container_t rtv_container(pbct_container_group, pbit_item_recorded_tv);
            object_id_t object_id(parent_id.get() + RTV_ID_SEPARATOR + series_container_list_[i].container_id);
	        rtv_container.object_id_ = object_id;
	        rtv_container.parent_id_ = parent_id;
	        rtv_container.name_ = series_container_list_[i].container_name;

            object_id_t logo_id(image_container_id.get() + RTV_ID_SEPARATOR + RTV_BY_SERIES_GROUP_LOGO_ID);
	        rtv_container.logo_ = url_address_t(make_object_url(proto.get(), server_address, server_port, logo_id.get()));

            rtv_container.total_count_ = series_container_list_[i].items.size();
	        object_info.container_list_.push_back(rtv_container);

            if (container_id.get().size() != 0)
                break;
        }
    }

    object_info.actual_count_ = object_info.container_list_.size();
    object_info.total_count_ = object_info.container_list_.size();

    return true;
}

bool rtv_content_storage_t::add_series_items(const dvblink::object_id_t& parent_id, const dvblink::object_id_t& container_id, const dvblink::object_id_t& image_container_id, 
    int start, int count, const dvblink::url_proto_t& proto, const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, 
    dvblex::playback::pb_object_t& object_info)
{
    bool res = false;

    boost::unique_lock<boost::shared_mutex> lock(items_lock_);

    for (size_t i=0; i<series_container_list_.size(); i++)
    {
        if (boost::iequals(series_container_list_[i].container_id, container_id.get()))
        {
            res = get_items_from_vector(series_container_list_[i].items, parent_id, image_container_id, start, count, proto, server_address, server_port, object_info);
            break;
        }
    }

	return res;
}

bool rtv_content_storage_t::get_items_from_vector(std::vector<rtv_item_desc_t>& items_list, const dvblink::object_id_t& parent_id, 
                                                  const dvblink::object_id_t& image_container_id, int start, int count, const dvblink::url_proto_t& proto,
                                                  const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, dvblex::playback::pb_object_t& object_info)
{
    //check whether recorded content list is up to date
    check_and_reload_content();
    //unsigned int item_count = count == dvblex::playback::OBJECT_COUNT_ALL ? (unsigned int)items_list.size() : count;

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

    size_t start_idx = std::max(start, 0);
	size_t end_idx = std::min(start_idx + count, items_list.size());

	for (size_t i=start_idx; i < end_idx; i++)
	{
		boost::shared_ptr<pb_recorded_tv_t> recorded_tv(new pb_recorded_tv_t());
		*recorded_tv = items_list[i].parent->recorded_tv_items_[items_list[i].item_id];
		//set object id and url (relative) to object id
		object_id_t object_id(parent_id.get() + RTV_ID_SEPARATOR + items_list[i].item_id);
		recorded_tv->object_id_ = object_id;
		recorded_tv->parent_id_ = parent_id;
        recorded_tv->url_ = url_address_t(make_object_url(proto.get(), server_address, server_port, object_id.get()));

		//set image url (relative) to image container
        if (recorded_tv->video_info_.m_ImageURL.size() > 0)
        {
		    object_id_t image_id(image_container_id.get() + RTV_ID_SEPARATOR + items_list[i].item_id);
            url_address_t image_url(make_object_url(proto.get(), server_address, server_port, image_id.get()));
            recorded_tv->thumbnail_ = image_url;
            recorded_tv->video_info_.m_ImageURL = image_url.get();
        }

        object_info.item_list_.push_back(recorded_tv);
	}

	object_info.actual_count_ = object_info.item_list_.size();
	object_info.total_count_ = items_list.size();

    return true;
}

bool rtv_content_storage_t::RTVItemStartTimeComp(const rtv_item_desc_t& first_item, const rtv_item_desc_t& second_item)
{
	return first_item.parent->recorded_tv_items_[first_item.item_id].video_info_.m_StartTime > second_item.parent->recorded_tv_items_[second_item.item_id].video_info_.m_StartTime;
}

bool rtv_content_storage_t::RTVItemNameComp(const rtv_item_desc_t& first_item, const rtv_item_desc_t& second_item)
{
	return boost::to_upper_copy(first_item.parent->recorded_tv_items_[first_item.item_id].video_info_.m_Name) < boost::to_upper_copy(second_item.parent->recorded_tv_items_[second_item.item_id].video_info_.m_Name);
}

bool rtv_content_storage_t::SeriesContainerNameComp(const rtv_series_container_t& first_item, const rtv_series_container_t& second_item)
{
	return boost::to_upper_copy(first_item.container_name) < boost::to_upper_copy(second_item.container_name);
}

bool rtv_content_storage_t::get_recorded_tv_items()
{
	bool ret_val = false;

	items_by_date_.clear();
	items_by_name_.clear();
	recorded_tv_items_.clear();
    series_container_list_.clear();
    reset_genre_containers();

    messaging::recorder::get_completed_recordings_request req;
    messaging::recorder::get_completed_recordings_response resp;
    if (message_queue_->send(messaging::recorder_message_queue_addressee, req, resp) == messaging::success)
    {
	    pb_item_list_t& item_list = resp.completed_recordings_;

	    for (unsigned int i=0; i<item_list.size(); i++)
	    {
            boost::shared_ptr<pb_recorded_tv_t> item = boost::static_pointer_cast<pb_recorded_tv_t>(item_list[i]);
		    recorded_tv_items_[item->object_id_.get()] = *(item);
		    rtv_item_desc_t rtv_item_desc;
		    rtv_item_desc.parent = this;
		    rtv_item_desc.item_id = item->object_id_.get();
            //by date container
		    items_by_date_.push_back(rtv_item_desc);
            //by name container
		    items_by_name_.push_back(rtv_item_desc);
            //genre containers
            add_item_to_genre_container(item.get());
            //seried container
            add_item_to_series_container(item.get());
	    }

	    //sort items
	    std::sort(items_by_date_.begin(), items_by_date_.end(), RTVItemStartTimeComp);
	    std::sort(items_by_name_.begin(), items_by_name_.end(), RTVItemNameComp);
        for (size_t i=0; i<genre_container_list_.size(); i++)
	        std::sort(genre_container_list_[i].items.begin(), genre_container_list_[i].items.end(), RTVItemStartTimeComp);
	    std::sort(series_container_list_.begin(), series_container_list_.end(), SeriesContainerNameComp);
        for (size_t i=0; i<series_container_list_.size(); i++)
	        std::sort(series_container_list_[i].items.begin(), series_container_list_[i].items.end(), RTVItemStartTimeComp);

	    ret_val = true;
    }

	return ret_val;
}

void rtv_content_storage_t::reset_genre_containers()
{
    for (size_t i=0; i<genre_container_list_.size(); i++)
        genre_container_list_[i].items.clear();
}

rtv_content_storage_t::rtv_item_genre_category_e rtv_content_storage_t::get_genre_category(dvblex::playback::pb_recorded_tv_t* item)
{
    if (item->video_info_.m_IsKids)
        return RTVGC_KIDS;
    if (item->video_info_.m_IsNews)
        return RTVGC_NEWS;
    if (item->video_info_.m_IsMusic)
        return RTVGC_MUSIC;
    if (item->video_info_.m_IsDocumentary)
        return RTVGC_DOCUMENTARY;
    if (item->video_info_.m_IsSports)
        return RTVGC_SPORT;
    if (item->video_info_.m_IsMovie)
        return RTVGC_MOVIE;

    return RTVGC_OTHER;
}

void rtv_content_storage_t::add_item_to_genre_container(dvblex::playback::pb_recorded_tv_t* item)
{
    rtv_content_storage_t::rtv_item_genre_category_e genre = get_genre_category(item);

    for (size_t i=0; i<genre_container_list_.size(); i++)
    {
        if (genre_container_list_[i].genre == genre)
        {
		    rtv_item_desc_t rtv_item_desc;
		    rtv_item_desc.parent = this;
		    rtv_item_desc.item_id = item->object_id_.get();
            genre_container_list_[i].items.push_back(rtv_item_desc);
            break;
        }
    }
}

void rtv_content_storage_t::add_item_to_series_container(dvblex::playback::pb_recorded_tv_t* item)
{
    if (!item->schedule_id_.empty() && item->series_schedule_)
    {
        //check if this container already exists
        rtv_content_storage_t::rtv_series_container_t* container = NULL;
        for (size_t i=0; i<series_container_list_.size(); i++)
        {
            if (boost::iequals(series_container_list_[i].schedule_id.get(), item->schedule_id_.get()))
            {
                container = &series_container_list_[i];
                break;
            }
        }
        if (container == NULL)
        {
            series_container_list_.push_back(rtv_content_storage_t::rtv_series_container_t());
            container = &series_container_list_[series_container_list_.size() - 1];
            container->container_id = item->schedule_id_.get();
            container->schedule_id = item->schedule_id_.get();
            container->container_name = item->schedule_name_.get();
        }

	    rtv_item_desc_t rtv_item_desc;
	    rtv_item_desc.parent = this;
	    rtv_item_desc.item_id = item->object_id_.get();
        container->items.push_back(rtv_item_desc);
    }
}
