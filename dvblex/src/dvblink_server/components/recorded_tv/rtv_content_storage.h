/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <boost/thread.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <dl_types.h>
#include <dl_filesystem_path.h>
#include <dl_message_queue.h>
#include <dl_pb_recorded_tv.h>

class rtv_content_storage_t
{
public:

    enum rtv_item_genre_category_e
    {
        RTVGC_OTHER,
        RTVGC_KIDS,
        RTVGC_MOVIE,
        RTVGC_SPORT,
        RTVGC_DOCUMENTARY,
        RTVGC_MUSIC,
        RTVGC_NEWS
    };

	struct rtv_item_desc_t
	{
		std::string item_id;
		rtv_content_storage_t* parent;
	};

    typedef std::vector<rtv_item_desc_t> rtv_item_desc_list_t;

	struct rtv_genre_container_t
    {
        void init(unsigned long id, const char* name, const std::string& logo, rtv_item_genre_category_e genre_cat)
        {
            container_id = boost::lexical_cast<std::string>(id);
            container_name_ids = name;
            genre = genre_cat;
            logo_id = logo;
            items.clear();
        }

		std::string container_id;
		std::string container_name_ids;
        rtv_item_genre_category_e genre;
        rtv_item_desc_list_t items;
        std::string logo_id;
    };

	struct rtv_series_container_t
    {
		std::string container_id;
        dvblink::schedule_id_t schedule_id;
		std::string container_name;
        rtv_item_desc_list_t items;
    };

    typedef std::vector<rtv_genre_container_t> rtv_genre_container_list_t;
    typedef std::vector<rtv_series_container_t> rtv_series_container_list_t;

public:
	rtv_content_storage_t(const dvblink::messaging::message_queue_t& message_queue);
	~rtv_content_storage_t();

	bool init();
	bool term();

    int get_item_count();
    void reload_content();

    bool get_item_fname(std::string& item_id, dvblink::filesystem_path_t& item_path);
    bool get_thumbnail_fname(std::string& item_id, dvblink::filesystem_path_t& item_path);
    bool get_item_info(std::string& item_id, const dvblink::object_id_t& parent_id, const dvblink::object_id_t& image_container_id, const dvblink::url_proto_t& proto,
        const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, dvblex::playback::pb_object_t& object_info);
	bool get_items_by_date(const dvblink::object_id_t& parent_id, const dvblink::object_id_t& image_container_id, int start, int count, const dvblink::url_proto_t& proto,
        const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, dvblex::playback::pb_object_t& object_info);
	bool get_items_by_name(const dvblink::object_id_t& parent_id, const dvblink::object_id_t& image_container_id, int start, int count, const dvblink::url_proto_t& proto,
        const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, dvblex::playback::pb_object_t& object_info);
    bool delete_item(std::string& item_id);
    bool stop_recording(std::string& item_id);

    int get_series_container_count(){return series_container_list_.size();};
    bool add_series_container(const dvblink::object_id_t& parent_id, const dvblink::object_id_t& container_id, const dvblink::object_id_t& image_container_id, 
        const dvblink::url_proto_t& proto, const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, 
        dvblex::playback::pb_object_t& object_info);
    bool add_series_items(const dvblink::object_id_t& parent_id, const dvblink::object_id_t& container_id, const dvblink::object_id_t& image_container_id, 
        int start, int count, const dvblink::url_proto_t& proto, const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, 
        dvblex::playback::pb_object_t& object_info);

    int get_genre_container_count(){return genre_container_list_.size();}
    bool add_genre_container(const dvblink::object_id_t& parent_id, const dvblink::object_id_t& container_id, const dvblink::object_id_t& image_container_id, 
        const dvblink::url_proto_t& proto, const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, 
        dvblex::playback::pb_object_t& object_info);
    bool add_genre_items(const dvblink::object_id_t& parent_id, const dvblink::object_id_t& container_id, const dvblink::object_id_t& image_container_id, 
        int start, int count, const dvblink::url_proto_t& proto, const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, 
        dvblex::playback::pb_object_t& object_info);

protected:
    dvblink::messaging::message_queue_t message_queue_;
	std::vector<rtv_item_desc_t> items_by_date_;
	std::vector<rtv_item_desc_t> items_by_name_;
    rtv_genre_container_list_t genre_container_list_;
    rtv_series_container_list_t series_container_list_;
	std::map<std::string, dvblex::playback::pb_recorded_tv_t> recorded_tv_items_;
	boost::shared_mutex items_lock_;
	boost::posix_time::ptime reload_time_;
    int reload_period_sec_;

	bool get_recorded_tv_items();
	static bool RTVItemStartTimeComp(const rtv_item_desc_t& first_item, const rtv_item_desc_t& second_item);
	static bool RTVItemNameComp(const rtv_item_desc_t& first_item, const rtv_item_desc_t& second_item);
    static bool SeriesContainerNameComp(const rtv_series_container_t& first_item, const rtv_series_container_t& second_item);
	bool get_items_from_vector(std::vector<rtv_item_desc_t>& items_list, const dvblink::object_id_t& parent_id, const dvblink::object_id_t& image_container_id,
        int start, int count, const dvblink::url_proto_t& proto, const dvblink::url_address_t& server_address, const dvblink::network_port_t& server_port, 
        dvblex::playback::pb_object_t& object_info);
    void check_and_reload_content();
    void reload_content(boost::posix_time::ptime cur_time);
    void reset_genre_containers();
    rtv_item_genre_category_e get_genre_category(dvblex::playback::pb_recorded_tv_t* item);
    void add_item_to_genre_container(dvblex::playback::pb_recorded_tv_t* item);
    void add_item_to_series_container(dvblex::playback::pb_recorded_tv_t* item);
};
