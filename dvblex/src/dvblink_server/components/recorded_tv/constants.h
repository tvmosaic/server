/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>

#define RTV_TOP_LEVEL_CONTAINER_COUNT   4

const std::string RTV_IMAGE_CONTAINER_ID = "DA5F661E-540E-47B1-9DCC-3F40F5CB85CA";

const std::string RTV_BY_NAME_LOGO_ID = "F13E752F-067F-4540-8D41-0922E1ACB06A";
const std::string RTV_BY_DATE_LOGO_ID = "64F1458B-CEBB-41e0-A37C-08C30938B521";
const std::string RTV_SOURCE_LOGO_ID = "B86A430E-A1D8-4f27-9015-F5CBF0B900F2";

const std::string RTV_BY_SERIES_LOGO_ID = "CC0E9054-F8C2-4145-BBC2-419DB0BF1BC1";
const std::string RTV_BY_SERIES_GROUP_LOGO_ID = "7A5FD3E6-F4A7-4738-8C45-CEDBD6DF913F";

const std::string RTV_BY_GENRE_LOGO_ID = "2D12C540-4856-41c7-92FB-7DD8B557AC86";
const std::string RTV_BY_GENRE_DOC_LOGO_ID = "31E0E156-223E-440f-9F40-311ACC0358ED";
const std::string RTV_BY_GENRE_MUSIC_LOGO_ID = "E25DAD55-202A-4445-89C5-A4C9123BA47C";
const std::string RTV_BY_GENRE_MOVIE_LOGO_ID = "6B04D14D-8831-4b2c-9B71-D0965ED9553C";
const std::string RTV_BY_GENRE_KIDS_LOGO_ID = "A5BB92EA-1358-4463-ABE7-71B0E153F05B";
const std::string RTV_BY_GENRE_NEWS_LOGO_ID = "E75E46BA-7DFA-49de-B389-6BF755176901";
const std::string RTV_BY_GENRE_SPORTS_LOGO_ID = "F87FB9FE-97FD-434f-90B1-DC06DFCFA2F1";
const std::string RTV_BY_GENRE_OTHER_LOGO_ID = "BDD006DE-8818-439d-AC1F-CA7EE5A2CB30";

const std::string RTV_BY_NAME_CONTAINER_FILENAME = "by_name_logo.png";
const std::string RTV_BY_DATE_CONTAINER_FILENAME = "by_date_logo.png";
const std::string RTV_SOURCE_CONTAINER_FILENAME = "rtv_source_logo.png";

const std::string RTV_BY_SERIES_CONTAINER_FILENAME = "by_series_logo.png";
const std::string RTV_BY_SERIES_GROUP_CONTAINER_FILENAME = "by_series_group_logo.png";

const std::string RTV_BY_GENRE_CONTAINER_FILENAME = "by_genre_logo.png";
const std::string RTV_BY_GENRE_DOC_CONTAINER_FILENAME = "by_genre_documentary_logo.png";
const std::string RTV_BY_GENRE_MUSIC_CONTAINER_FILENAME = "by_genre_music_logo.png";
const std::string RTV_BY_GENRE_MOVIE_CONTAINER_FILENAME = "by_genre_movie_logo.png";
const std::string RTV_BY_GENRE_KIDS_CONTAINER_FILENAME = "by_genre_kids_logo.png";
const std::string RTV_BY_GENRE_NEWS_CONTAINER_FILENAME = "by_genre_news_logo.png";
const std::string RTV_BY_GENRE_SPORTS_CONTAINER_FILENAME = "by_genre_sports_logo.png";
const std::string RTV_BY_GENRE_OTHER_CONTAINER_FILENAME = "by_genre_other_logo.png";


const std::string RTV_ID_SEPARATOR = "/";
const std::string RTV_ID_DOUBLE_SEPARATOR = "//";

const std::string IDS_RECTV_BY_NAME_CONTAINER_NAME = "IDS_RECTV_BY_NAME_CONTAINER_NAME";
const std::string IDS_RECTV_BY_NAME_CONTAINER_DESC = "IDS_RECTV_BY_NAME_CONTAINER_DESC";
const std::string IDS_RECTV_BY_DATE_CONTAINER_NAME = "IDS_RECTV_BY_DATE_CONTAINER_NAME";
const std::string IDS_RECTV_BY_DATE_CONTAINER_DESC = "IDS_RECTV_BY_DATE_CONTAINER_DESC";
const std::string IDS_RECTV_BY_GENRE_CONTAINER_NAME = "IDS_RECTV_BY_GENRE_CONTAINER_NAME";
const std::string IDS_RECTV_BY_GENRE_CONTAINER_DESC = "IDS_RECTV_BY_GENRE_CONTAINER_DESC";
const std::string IDS_RECTV_BY_SERIES_CONTAINER_NAME = "IDS_RECTV_BY_SERIES_CONTAINER_NAME";
const std::string IDS_RECTV_BY_SERIES_CONTAINER_DESC = "IDS_RECTV_BY_SERIES_CONTAINER_DESC";
const std::string IDS_RECTV_SOURCE_NAME = "IDS_RECTV_SOURCE_NAME";
const std::string IDS_RECTV_SOURCE_DESC = "IDS_RECTV_SOURCE_DESC";

const std::string RTV_BY_NAME_CONTAINER_ID = "E44367A7-6293-4492-8C07-0E551195B99F";
const std::string RTV_BY_DATE_CONTAINER_ID = "F6F08949-2A07-4074-9E9D-423D877270BB";
const std::string RTV_BY_GENRE_CONTAINER_ID = "CE482DD8-BC5E-47c3-9072-2554B968F27C";
const std::string RTV_BY_SERIES_CONTAINER_ID = "0E03FEB8-BD8F-46e7-B3EF-34F6890FB458";

