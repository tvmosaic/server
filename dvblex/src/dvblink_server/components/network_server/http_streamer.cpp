/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/xpressive/xpressive.hpp>
#include <dl_logger.h>
#include <dl_utils.h>
#include "constants.h"
#include "mime_types.h"
#include "http_streamer.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

namespace dvblex {

http_streamer_t::http_streamer_t(const std::string& id, stream_processor_base* processor, const char* mime_code, bool use_idle_packets) :
    network_streamer_base_t(id, processor),
	circle_buffer_(50, 256*188, L"http_streamer_t"),
	send_thread_ptr(NULL),
    use_idle_packets_(use_idle_packets),
	b_write_error_(false),
	no_stream_timeout_sec_(30),
    b_client_connected_(false)
{

    circle_buffer_.set_expansion_props(200, 50);

    if (mime_code != NULL)
         mime_code_ = mime_code;
    else
        mime_code_ = mpeg_mime_code;

    //prepare NULL packet
    memset(null_packet_, 0xFF, TS_PACKET_SIZE);
    null_packet_[0] = 0x47;
    null_packet_[1] = 0x1F;
    null_packet_[2] = 0xFF;
    null_packet_[3] = 0x10;

	time(&start_time_);
	b_stream_sent_ = false;

	idle_timer_ = new dvblink::engine::timer_procedure<http_streamer_t>(&http_streamer_t::idle_timer_func, *this, static_cast<long>(1000));
    client_checker_ = new dvblink::engine::timer_procedure<http_streamer_t>(&http_streamer_t::check_client_timer_procedure, *this, static_cast<long>(30000));
    start_http_streaming();
}

bool http_streamer_t::start(streamer_callbacks_t* cb)
{
    network_streamer_base_t::start(cb);
    //check if error has to be reported already
    return !b_write_error_;
}

http_streamer_t::~http_streamer_t()
{
    stop_processor();

    stop_http_streaming();
    delete idle_timer_;
    delete client_checker_;
}

void http_streamer_t::idle_timer_func(const boost::system::error_code& e)
{
	send_idle_packet_.check_and_set();
}

void http_streamer_t::check_client_timer_procedure(const boost::system::error_code& e)
{
    //if still there are no clients connected, break the line
    if (!b_client_connected_)
    {
        log_info(L"http_streamer_t::check_client_timer_procedure. No clients are connected within timeout");
        report_write_error();
    }
}

void http_streamer_t::on_received_stream(const unsigned char* data, size_t len)
{
	circle_buffer_.write_stream(data, len);
}

void http_streamer_t::stop_http_streaming()
{
	if (send_thread_ptr != NULL)
	{
		b_exit_flag_ = true;
		send_thread_ptr->join();
		delete send_thread_ptr;
		send_thread_ptr = NULL;
	}
}

void http_streamer_t::start_http_streaming()
{
	stop_http_streaming();
	//start new streaming thread
	b_stream_sent_ = false;
	time(&start_time_);
	b_exit_flag_ = false;
	send_thread_ptr = new boost::thread(boost::bind(&http_streamer_t::send_thread_func, this));
}

void http_streamer_t::add_client(pion::http::response_writer_ptr response_writer)
{
    b_client_connected_ = true;

    boost::unique_lock<boost::shared_mutex> lock(clients_lock_);
    pending_clients_.push_back(response_writer);
}

void http_streamer_t::send_thread_func()
{
	b_write_error_ = false;

	while (!b_exit_flag_ && !b_write_error_)
	{
		ts_circle_buffer::node_t node = circle_buffer_.tear_node(10);
        //add pending clients
        {
            boost::unique_lock<boost::shared_mutex> lock(clients_lock_);
            for (unsigned int i=0; i<pending_clients_.size(); i++)
            {
	            // set the Content-Type HTTP header using the file's MIME type
                pending_clients_[i]->get_response().set_content_type(mime_code_);

	            // use "200 OK" HTTP response
                pending_clients_[i]->get_response().set_status_code(pion::http::types::RESPONSE_CODE_OK);
                pending_clients_[i]->get_response().set_status_message(pion::http::types::RESPONSE_MESSAGE_OK);

                //add client to the main clients list
                http_client_desc_t client_desc;
                client_desc.writer = pending_clients_[i];
                client_desc.b_write_error = false;
                client_desc.first_error_time = boost::date_time::not_a_date_time;
                clients_.push_back(client_desc);
                log_info(L"http_streamer_t::send_thread_func: adding http client");
            }
            pending_clients_.clear();
        }
		if (node != NULL)
		{
            for (unsigned int i=0; i<clients_.size(); i++)
            {
                clients_[i].writer->write_no_copy((char*)(node->data()), node->size_to_read());

			    node_processed_.reset();
                first_error_time_ = &(clients_[i].first_error_time);
                b_client_write_error_ = &(clients_[i].b_write_error);
                clients_[i].writer->send_chunk(boost::bind(&http_streamer_t::handle_write, this, 
				    boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

			    node_processed_.wait();

                clients_[i].writer->clear();
            }
			circle_buffer_.put_node(node);
			b_stream_sent_ = true;
		} else
        {
            if (send_idle_packet_.check_and_reset())
            {
				if (use_idle_packets_)
				{
					//send a NULL packet to each client just to test the connection
					for (unsigned int i=0; i<clients_.size(); i++)
					{
						clients_[i].writer->write_no_copy(null_packet_, sizeof(null_packet_));

						node_processed_.reset();
						first_error_time_ = &(clients_[i].first_error_time);
						b_client_write_error_ = &(clients_[i].b_write_error);
						clients_[i].writer->send_chunk(boost::bind(&http_streamer_t::handle_write, this, 
							boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

						node_processed_.wait();

						clients_[i].writer->clear();
					}
				}
				//check for no stream condition
				time_t now;
				time(&now);

				if (b_stream_sent_)
				{
					b_stream_sent_ = false;
					start_time_ = now;
				} 
				else
				{
					if (now - start_time_ > no_stream_timeout_sec_)
					{
						log_info(L"http_streamer_t::send_thread_func: No stream timeout detected. Setting exit flag");
                        report_write_error();
					}
				}
            }
        }
        bool not_empty = clients_.size() > 0;
        //remove all clients with write error
        std::vector<http_client_desc_t>::iterator it = clients_.begin();
        while (it != clients_.end())
        {
            if (it->b_write_error || !it->writer->get_connection()->is_open())
            {
                log_info(L"http_streamer_t::send_thread_func: removing http client");
	            //close http connection
                it->writer->get_connection()->finish();
                clients_.erase(it);
                it = clients_.begin();
            } else
            {
                ++it;
            }
        }
        //if last client was deleted - report error
        if (not_empty && clients_.size() == 0)
        {
			log_info(L"http_streamer_t::send_thread_func: all clients are deleted. Setting exit flag");
            report_write_error();
        }
	}
	//walk through all actual and pending clients and close http connection
    for (unsigned int i=0; i<clients_.size(); i++)
        clients_[i].writer->get_connection()->finish();
    clients_.clear();

    {
        boost::unique_lock<boost::shared_mutex> lock(clients_lock_);
        for (unsigned int i=0; i<pending_clients_.size(); i++)
        {
            pending_clients_[i]->get_connection()->finish();
            pending_clients_[i]->get_connection()->close();
        }
        pending_clients_.clear();
    }
}

void http_streamer_t::handle_write(const boost::system::error_code& write_error, std::size_t /*bytes_written*/)
{
	if (write_error)
	{
		*b_client_write_error_ = true;
        log_info(L"http_streamer_t::handle_write. http write error");
        /*
		if (first_error_time_->is_not_a_date_time())
			*first_error_time_ = boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time();
		else
		{
			boost::posix_time::ptime now = boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time();
            boost::posix_time::time_duration duration(now - *first_error_time_);
			if (duration.total_milliseconds() > 2000)
			{
				*b_client_write_error_ = true;
				log_error(L"http_streamer_t: http write error");
			}
		}
        */
	}
	node_processed_.signal();
}

void http_streamer_t::report_write_error()
{
    b_write_error_ = true;
    if (callbacks_ != NULL)
        callbacks_->streamer_disconnected(get_id());
}

std::string http_streamer_t::get_streaming_url(const dvblink::channel_id_t& channel_id, const std::string& protocol_header, const std::string& server_address, 
        boost::uint16_t streaming_port, int handle)
{
    std::stringstream url;
	url << protocol_header << "://" << server_address << ":" << streaming_port << http_stream_full_prefix << handle;

    return url.str();
}

bool http_streamer_t::is_http_stream_request(const pion::http::request_ptr& request, int& handle)
{
    std::string resource_string = request->get_resource();
    //if . is present in the url - it is definitely not for us (some programs add extension to direct url, do not process it).
    std::string::size_type pos = resource_string.find('.');
    if (pos != std::string::npos)
        return false;

    boost::xpressive::sregex session_http_server_regex(boost::xpressive::sregex::compile(http_stream_name_mask.c_str()));
    boost::xpressive::sregex_token_iterator sregex_end;

    boost::xpressive::sregex_token_iterator cur(request->get_resource().begin(), request->get_resource().end(), session_http_server_regex, 1);

    if (cur != sregex_end)
    {
        handle = boost::lexical_cast<int>(*cur);
        return true;
    }
    return false;
}

bool http_streamer_t::process_request(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn)
{
    pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *http_request_ptr);
    add_client(writer);

    return true;
}

} // dvblex
