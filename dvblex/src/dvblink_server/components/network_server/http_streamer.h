/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning (disable : 4100)
#pragma warning (disable : 4244)
#endif

#include <pion/http/response_writer.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include <boost/date_time/posix_time/posix_time.hpp>

#include <dl_ts.h>
#include <dl_circle_buffer.h>
#include <dl_event.h>
#include <dl_flag.h>
#include <dl_timer_procedure.h>
#include "network_streamer_base.h"

namespace dvblex {

struct http_client_desc_t
{
    pion::http::response_writer_ptr writer;
	bool b_write_error;
    boost::posix_time::ptime first_error_time;
};

class http_streamer_t : public network_streamer_base_t
{
public:
    http_streamer_t(const std::string& id, stream_processor_base* processor = NULL, const char* mime_code = NULL, bool use_idle_packets = true);
	~http_streamer_t();

    virtual void on_received_stream(const unsigned char* data, size_t len);
    virtual bool start(dvblink::streamer_callbacks_t* cb);

    void add_client(pion::http::response_writer_ptr response_writer);

    virtual std::string get_streaming_url(const dvblink::channel_id_t& channel_id, const std::string& protocol_header, const std::string& server_address, 
        boost::uint16_t streaming_port, int handle);

    static bool is_http_stream_request(const pion::http::request_ptr& request, int& handle);
    bool process_request(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn);

private:
	dvblink::engine::ts_circle_buffer circle_buffer_;
	boost::thread* send_thread_ptr;
	std::string mime_code_;
	bool use_idle_packets_;

    boost::shared_mutex clients_lock_;
    std::vector<http_client_desc_t> clients_;
    std::vector<pion::http::response_writer_ptr> pending_clients_;
	dvblink::engine::timer_procedure<http_streamer_t>* idle_timer_;

    bool b_exit_flag_;
    dvblink::event node_processed_;
    dvblink::engine::atomic_switch send_idle_packet_;
	bool b_write_error_;
    bool* b_client_write_error_;
    boost::posix_time::ptime* first_error_time_;
    unsigned char null_packet_[dvblink::engine::TS_PACKET_SIZE];
	time_t start_time_;
	bool b_stream_sent_;
	int no_stream_timeout_sec_;
    dvblink::engine::timer_procedure<http_streamer_t>* client_checker_;
    bool b_client_connected_;

	void send_thread_func();
	void stop_http_streaming();
	void handle_write(const boost::system::error_code& write_error, std::size_t bytes_written);
	void idle_timer_func(const boost::system::error_code& e);
	void start_http_streaming();
    void report_write_error();
    void check_client_timer_procedure(const boost::system::error_code& e);
};

} // dvblink
