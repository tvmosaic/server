/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <sstream>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_xml_serialization.h>
#include <dl_url_encoding.h>
#include "constants.h"
#include "mime_types.h"
#include "playback_objects.h"

using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;
using namespace dvblink;

namespace dvblex {

using namespace playback;

const int transcoded_segment_duration_sec       = 6;

playback_objects_t::message_handler::message_handler(playback_objects_t* parent, messaging::message_queue_t message_queue) :
    release_me_request::subscriber(message_queue),
    parent_(parent),
    message_queue_(message_queue)
{

}

playback_objects_t::message_handler::~message_handler()
{

}

void playback_objects_t::message_handler::handle(const message_sender_t& /*sender*/, const release_me_request& request, release_me_response& /*response*/)
{
    parent_->invalidate_clients(request.id_);
}


playback_objects_t::playback_objects_t() : 
    client_watchdog_thread_(NULL)
{
}

playback_objects_t::~playback_objects_t()
{
}

bool playback_objects_t::init(network_server_settings_t* settings, const dvblink::i_server_t& server)
{
    settings_ = settings;
    server_ = server;

    //create temportary guid for message queue
    message_queue_ = share_object_safely(new dvblink::messaging::message_queue(engine::uuid::gen_uuid()));
    message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));

    server_->register_queue(message_queue_);

    //start clients watchdog thread
    b_watchdog_exit_ = false;
    client_watchdog_thread_ = new boost::thread(boost::bind(&playback_objects_t::client_watchdog_thread_func, this));

    return true;
}

void playback_objects_t::term()
{
    //stop clients watchdog thread
    if (client_watchdog_thread_ != NULL)
    {
        b_watchdog_exit_ = true;
        client_watchdog_thread_->join();
        delete client_watchdog_thread_;
        client_watchdog_thread_ = NULL;
    }

    //delete all remaining clients (if any)
    for (unsigned int i=0; i<clients_list_.size(); i++)
    {
        clients_list_[i]->stop();
        delete clients_list_[i];
    }
    clients_list_.clear();

    transcoded_clients_map_t::iterator it  = transcoded_clients_list_.begin();
    while (it != transcoded_clients_list_.end())
    {
        it->second->stop();
        ++it;
    }
    transcoded_clients_list_.clear();

    message_handler_.reset();

    server_->unregister_queue(message_queue_->get_id());
    message_queue_->shutdown();
}

bool playback_objects_t::start_object_streaming(const std::string& protocol_header, const std::string& object_id, const pion::http::request_ptr& request, const pion::tcp::connection_ptr& tcp_conn)
{
    bool success = false;

    pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *request);

    std::string source_id;
    std::string source_object_id;
    if (parse_object_id(object_id, source_id, source_object_id))
    {
        if (request->has_query("transcoder"))
        {
            if (request->has_query("segment"))
            {
                //request actual transcoded segment
                success = process_transcoded_segment_request(object_id, source_id, writer, request);
            } else
            {
                //request for m3u8 file of this object
                success = generate_m3u8_file(protocol_header, object_id, writer, request);
            }
        } else
        {
            //request for not-transcoded streaming
            log_info(L"playback_objects_t::start_object_streaming. Request for object %1% of source %2%") % string_cast<EC_UTF8>(source_object_id) % string_cast<EC_UTF8>(source_id);
            //get item itself - playback engine may need some of its metadata
            //call may fail (if this is "subresource" - thumbnail for example
            //in this case item will be NULL. It is normal - treat it as such.
            boost::shared_ptr<pb_item_t> item;
            get_playback_item(object_id_t(object_id), item);
            //get playback interface
            i_play_source_control_t play_interface;
            if (server_->query_object_interface(message_queue_->get_uid(), base_id_t(source_id), play_source_control_interface, play_interface) == dvblink::i_success)
            {
                playback_item_streamer_t* new_client = new playback_item_streamer_t(item, base_id_t(source_id), object_id, play_interface, message_queue_, writer, request);
                boost::unique_lock<boost::shared_mutex> lock(clients_lock_);
                clients_list_.push_back(new_client);

                success = true;
            } else
            {
                log_error(L"playback_objects_t::start_object_streaming. failed to get playback interface for source %1%") % string_cast<EC_UTF8>(source_id);
            }
        }
    } else
    {
        log_error(L"playback_objects_t::start_object_streaming. failed to parse object id from %1%") % string_cast<EC_UTF8>(object_id);
    }
    return success;
}

void playback_objects_t::invalidate_clients(const dvblink::base_id_t source_id)
{
    boost::unique_lock<boost::shared_mutex> lock(clients_lock_);

    std::vector<playback_item_streamer_t*>::iterator it = clients_list_.begin();
    while (it != clients_list_.end())
    {
        if ((*it)->get_source_id() == source_id)
        {
            (*it)->cancel();
            delete *it;
            clients_list_.erase(it);
            //restart from the beginning
            it = clients_list_.begin();
        } else
        {
            ++it;
        }
    }
    
    transcoded_clients_map_t::iterator it_tr  = transcoded_clients_list_.begin();
    while (it_tr != transcoded_clients_list_.end())
    {
        if (it_tr->second->get_source_id() == source_id)
        {
            it_tr->second->cancel();
            transcoded_clients_list_.erase(it_tr);
            //restart from the beginning
            it_tr  = transcoded_clients_list_.begin();
        } else
        {
            ++it_tr;
        }
    }
    
}

void playback_objects_t::client_watchdog_thread_func()
{
    while (!b_watchdog_exit_)
    {
        {
            boost::unique_lock<boost::shared_mutex> lock(clients_lock_);

            std::vector<playback_item_streamer_t*>::iterator it = clients_list_.begin();
            while (it != clients_list_.end())
            {
                if ((*it)->is_finished())
                {
                    (*it)->stop();
                    delete *it;
                    clients_list_.erase(it);
                    //restart from the beginning
                    it = clients_list_.begin();
                } else
                {
                    ++it;
                }
            }
            
            transcoded_clients_map_t::iterator it_tr  = transcoded_clients_list_.begin();
            while (it_tr != transcoded_clients_list_.end())
            {
                if (it_tr->second->is_finished())
                {
                    it_tr->second->stop();
                    transcoded_clients_list_.erase(it_tr);
                    //restart from the beginning
                    it_tr  = transcoded_clients_list_.begin();
                } else
                {
                    ++it_tr;
                }
            }
          
        }
		dvblink::engine::sleep(1000);
    }
}

bool playback_objects_t::get_playback_item(dvblink::object_id_t object_id, boost::shared_ptr<pb_item_t>& item)
{
    bool ret_val = false;

	//dummy server parameters - they are not needed/used for this call
    dvblink::url_address_t server_address = "localhost";
	dvblink::network_port_t port = 0;

	std::string source_id;
	std::string source_object_id;
	parse_object_id(object_id, source_id, source_object_id);

	messaging::playback::get_objects_request req;
    req.object_id_ = object_id;
    req.server_address_ = server_address;

	messaging::playback::get_objects_response resp;
	messaging::message_error err = message_queue_->send(source_id, req, resp);

    if (err == messaging::success)
    {
        if (resp.object_.item_list_.size() == 1)
        {
            item = resp.object_.item_list_[0];
            ret_val = true;
        }
    } else
    {
        log_warning(L"playback_objects_t::get_playback_item. get_objects_request returned NULL for object %1%") % object_id.to_wstring();
    }
    return ret_val;
}

bool playback_objects_t::is_playback_object_request(const pion::http::request_ptr& request, std::string& object_id)
{
	bool ret_val = false;
    object_id.clear();

	//expected url format is http://address:port/stream/playback?object=xxxx
	std::string resource = request->get_resource();
    if (boost::istarts_with(resource, playback_object_full_prefix))
	{
        std::string object_id_str = request->get_query("object");
        //do url decode on object id
        dvblink::url_decode(object_id_str.c_str(), object_id);

		ret_val = object_id.size() > 0;
	}
	
	return ret_val;
}

bool playback_objects_t::generate_m3u8_file(const std::string& protocol_header, const std::string& object_id_str, pion::http::response_writer_ptr writer, pion::http::request_ptr request)
{
    bool res = false;
    
    log_info(L"playback_objects_t::generate_m3u8_file. m3u8 requst for object %1%") % string_cast<EC_UTF8>(object_id_str);
    
    int segment_duration_sec = transcoded_segment_duration_sec;
    
    //recreate original query parameters string
    std::string original_query_str;
    pion::ihash_multimap query_params = request->get_queries();
    pion::ihash_multimap::iterator it = query_params.begin();
    while (it != query_params.end())
    {
        if (original_query_str.size() != 0)
            original_query_str += "&";
            
        original_query_str += it->first + "=" + it->second;
        
        ++it;
    }

    std::string host_str = request->get_header("Host");

    boost::shared_ptr<pb_item_t> item;
    if (get_playback_item(object_id_t(object_id_str), item))
    {
        if (item->item_type_ == pbit_item_recorded_tv)
        {
            boost::shared_ptr<pb_recorded_tv_t> recorded_tv = boost::static_pointer_cast<pb_recorded_tv_t>(item);
            time_t duration = recorded_tv->video_info_.m_Duration;
            if (duration > 0)
            {
                std::ostringstream list_stream;
                list_stream << "#EXTM3U\n" << "#EXT-X-TARGETDURATION:" << segment_duration_sec << "\n#EXT-X-MEDIA-SEQUENCE:0\n#EXT-X-PLAYLIST-TYPE:VOD\n#EXT-X-VERSION:3\n";

                int count = 0;
                while (duration >= segment_duration_sec)
                {
                    list_stream << "#EXTINF:" << segment_duration_sec << ",\n" <<
                        protocol_header << "://" << host_str << request->get_resource() << "?" << original_query_str << "&segment=" << count << "\n";
                        
                    duration -= segment_duration_sec;
                    count += 1;
                }
                list_stream << "#EXT-X-ENDLIST\n";
            
                writer->get_response().set_status_code(pion::http::types::RESPONSE_CODE_OK);
                writer->get_response().set_status_message(pion::http::types::RESPONSE_MESSAGE_OK);
                writer->get_response().set_content_type(m3u8_mime_code);
                writer->get_response().change_header("Content-Disposition", "inline; filename=index.m3u8");
                writer->get_response().change_header("Access-Control-Allow-Origin", access_control_allow_origin_hdr.c_str());
                writer->get_response().change_header("Access-Control-Expose-Headers", "*");
                writer->get_response().change_header("Access-Control-Allow-Credentials", "true");
                writer->write(list_stream.str().c_str(), list_stream.str().size());
	            writer->send();
                //close http connection
                writer->get_connection()->finish();
                
                res = true;
            } else
            {
                log_error(L"playback_objects_t::generate_m3u8_file. object %1% duration is unknown") % string_cast<EC_UTF8>(object_id_str);
            }
        } else
        {
            log_error(L"playback_objects_t::generate_m3u8_file. m3u8 generation is only supported for recorded TV content (object %1%) ") % string_cast<EC_UTF8>(object_id_str);
        }
    }
    
    return res;
}

bool playback_objects_t::process_transcoded_segment_request(const std::string& object_id_str, const std::string& source_id, pion::http::response_writer_ptr writer, 
    pion::http::request_ptr request)
{
    bool res = false;

    if (request->has_query("client_id") &&
        request->has_query("segment"))
    {
        //convert http request into stream/transcoder request structure
        transcoder::transcoder_params transcoder_params;
        if (transcoded_playback_provider::get_transcoder_params_from_http_request(transcoder_params, request))
        {
            std::string client_id = request->get_query("client_id");
            int segment_num = boost::lexical_cast<int>(request->get_query("segment"));
            
            transcoded_playback_provider_t provider;
            {
                //check if this client is already in the map    
                boost::unique_lock<boost::shared_mutex> lock(clients_lock_);
                if (transcoded_clients_list_.find(client_id) != transcoded_clients_list_.end())
                {
                    bool b_valid = false;
                    //check if request parameters and object id are the same
                    if (transcoded_clients_list_[client_id]->is_valid_transcoder_params(transcoder_params))
                    {
                        //check if this provider can handle the segment request (not too far in the future/past)
                        if (transcoded_clients_list_[client_id]->is_valid_segment_num(segment_num))
                        {
                            b_valid = true;
                        }
                    }
                    if (b_valid)
                    {
                        provider = transcoded_clients_list_[client_id];
                    } else
                    {
                        //this transcoder cannot handle the request. delete it and re-create
                        transcoded_clients_list_[client_id]->stop();
                        transcoded_clients_list_.erase(client_id);
                    }
                }
                
                if (!provider)
                {
                    //create new provider to handle the request
                    boost::shared_ptr<pb_item_t> item;
                    get_playback_item(object_id_t(object_id_str), item);
                    //get playback interface
                    i_play_source_control_t play_interface;
                    if (server_->query_object_interface(message_queue_->get_uid(), base_id_t(source_id), play_source_control_interface, play_interface) == dvblink::i_success)
                    {
                        provider = boost::shared_ptr<transcoded_playback_provider>(new transcoded_playback_provider(item, base_id_t(source_id), 
                            object_id_str, play_interface, message_queue_, transcoder_params, segment_num, transcoded_segment_duration_sec));
                        transcoded_clients_list_[client_id] = provider;
                    } else
                    {
                        log_error(L"playback_objects_t::start_object_streaming. failed to get playback interface for source %1%") % string_cast<EC_UTF8>(source_id);
                    }
                }
            }
            
            if (provider)
            {
                res = provider->process_segment_request(request, writer, segment_num);
            }
        } else
        {
            log_error(L"playback_objects_t::process_transcoded_segment_request. get_stream_from_http_request failed");
        }
    } else
    {
        log_error(L"playback_objects_t::process_transcoded_segment_request. request does not have all required params");
    }
    
    return res;
}

}