/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp> 
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <pion/http/response_writer.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_language_settings.h>
#include <dl_pugixml_helper.h>
#include "constants.h"
#include "mime_types.h"
#include "pion_helper.h"
#include "web/web_settings_general.h"
#include "web/web_settings_general_log.h"
#include "web/web_settings_general_checkupdates.h"
#include "web/web_settings_channels_visibility_channels.h"
#include "web/web_settings_channels_visibility_providers.h"
#include "web/web_settings_channels_visibility_transponders.h"
#include "web/web_settings_network.h"
#include "web/web_settings_network_checkports.h"
#include "web/web_settings_dlna.h"
#include "web/web_settings_backup.h"
#include "web/web_settings_transcoding.h"
#include "web/web_settings_epg_sources.h"
#include "web/web_settings_epg_sources_settings.h"
#include "web/web_settings_epg_sources_refresh.h"
#include "web/web_settings_recorder.h"
#include "web/web_settings_recorder_repairdb.h"
#include "web/web_settings_directories.h"
#include "web/web_settings_products.h"
#include "web/web_settings_products_file.h"
#include "web/web_settings_products_trial.h"
#include "web/web_settings_products_activate.h"
#include "web/web_settings_epg_mapping_channels.h"
#include "web/web_settings_epg_mapping_epgsources.h"
#include "web/web_settings_epg_mapping_epgsources_channels.h"
#include "web/web_settings_epg_mapping_favorites.h"
#include "web/web_settings_epg_mapping_favorites_channels.h"
#include "web/web_settings_epg_mapping_epgsearch.h"
#include "web/web_settings_channels_properties_channels.h"
#include "web/web_settings_channels_properties_favorites.h"
#include "web/web_settings_channels_properties_favorites_channels.h"
#include "web/web_settings_channels_logos_channels.h"
#include "web/web_settings_channels_logos_favorites.h"
#include "web/web_settings_channels_logos_favorites_channels.h"
#include "web/web_settings_channels_logos_logosearch.h"
#include "web/web_settings_channels_logos_packs.h"
#include "web/web_settings_channels_logos_packs_logos.h"
#include "web/web_settings_channels_scan_templates.h"
#include "web/web_settings_channels_scan_devices.h"
#include "web/web_settings_channels_scan_devices_settings.h"
#include "web/web_settings_channels_scan_devices_status.h"
#include "web/web_settings_channels_scan_providers.h"
#include "web/web_settings_channels_scan_scanners.h"
#include "web/web_settings_channels_scan_start.h"
#include "web/web_settings_channels_scan_cancel.h"
#include "web/web_settings_channels_scan_apply.h"
#include "web/web_settings_channels_scan_rescan_settings.h"
#include "web/web_settings_channels_scan_rescan_start.h"
#include "web/web_settings_channels_scan_networks.h"
#include "web/web_settings_channels_favorites.h"
#include "web/web_settings_channels_favorites_allchannels.h"
#include "web/web_settings_sendto_targets.h"
#include "web/web_settings_sendto_comskip.h"
#include "web/web_settings_sendto_comskip_default.h"
#include "web/web_settings_sendto_activateworkunit.h"
#include "web/web_connect_tvrecordings_categories.h"
#include "web/web_connect_tvrecordings_programs.h"
#include "web/web_connect_tvrecordings_stop.h"
#include "web/web_connect_tvrecordings_remove.h"
#include "web/web_connect_library_sources.h"
#include "web/web_connect_library_filters.h"
#include "web/web_connect_library_programs.h"
#include "web/web_connect_library_details.h"
#include "web/web_connect_stream.h"
#include "web/web_connect_tvguide_channels.h"
#include "web/web_connect_tvguide_favorites.h"
#include "web/web_connect_tvguide_programs.h"
#include "web/web_connect_search_genres.h"
#include "web/web_connect_schedules.h"
#include "web/web_connect_schedules_channels.h"
#include "web/web_connect_schedules_diskspace.h"
#include "web/web_connect_schedules_edit.h"
#include "web/web_connect_server_capabilities.h"
#include "web/web_connect_sendto_items_canceled.h"
#include "web/web_connect_sendto_items.h"
#include "web/web_connect_sendto_targets.h"
#include "web_service.h"

#ifdef _MSC_VER
#pragma warning (disable : 4100)
#endif

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

static const std::string web_root_dir      = "root";
static const std::string mime_file_name     = "mime.xml";
static const std::string index_file_name     = "index.html";

static const std::string network_server_mime_ext_attr = "ext";
static const std::string network_server_mime_attr = "mime";

web_service::web_service(dvblink::i_server_t server, dvblink::messaging::message_queue_t message_queue, unsigned short port, stream_service* stream_service, network_server_settings_t* settings) :
    server_(server), message_queue_(message_queue), port_(port), stream_service_(stream_service), settings_(settings)
{
    web_file_path_ = settings_->get_web_dir();
    web_root_file_path_ = web_file_path_ / web_root_dir;

    load_mime_types();

    build_handlers_map();    
}

web_service::~web_service()
{
}

void web_service::build_handlers_map()
{
    handlers_[web_settings_general_handler::get_resource()] = web_settings_general_handler::get_new_instance();
    handlers_[web_settings_general_log_handler::get_resource()] = web_settings_general_log_handler::get_new_instance();
    handlers_[web_settings_general_checkupdates_handler::get_resource()] = web_settings_general_checkupdates_handler::get_new_instance();
    handlers_[web_settings_channels_visibility_transponders_handler::get_resource()] = web_settings_channels_visibility_transponders_handler::get_new_instance();
    handlers_[web_settings_channels_visibility_providers_handler::get_resource()] = web_settings_channels_visibility_providers_handler::get_new_instance();
    handlers_[web_settings_channels_visibility_channels_handler::get_resource()] = web_settings_channels_visibility_channels_handler::get_new_instance();
    handlers_[web_settings_network_handler::get_resource()] = web_settings_network_handler::get_new_instance();
    handlers_[web_settings_network_checkports_handler::get_resource()] = web_settings_network_checkports_handler::get_new_instance();
    handlers_[web_settings_dlna_handler::get_resource()] = web_settings_dlna_handler::get_new_instance();
    handlers_[web_settings_backup_handler::get_resource()] = web_settings_backup_handler::get_new_instance();
    handlers_[web_settings_transcoding_handler::get_resource()] = web_settings_transcoding_handler::get_new_instance();
    handlers_[web_settings_epg_sources_handler::get_resource()] = web_settings_epg_sources_handler::get_new_instance();
    handlers_[web_settings_epg_sources_settings_handler::get_resource()] = web_settings_epg_sources_settings_handler::get_new_instance();
    handlers_[web_settings_epg_sources_refresh_handler::get_resource()] = web_settings_epg_sources_refresh_handler::get_new_instance();
    handlers_[web_settings_recorder_handler::get_resource()] = web_settings_recorder_handler::get_new_instance();
    handlers_[web_settings_recorder_repairdb_handler::get_resource()] = web_settings_recorder_repairdb_handler::get_new_instance();
    handlers_[web_settings_directories_handler::get_resource()] = web_settings_directories_handler::get_new_instance();
    handlers_[web_settings_products_handler::get_resource()] = web_settings_products_handler::get_new_instance();
    handlers_[web_settings_products_activate_handler::get_resource()] = web_settings_products_activate_handler::get_new_instance();
    handlers_[web_settings_products_file_handler::get_resource()] = web_settings_products_file_handler::get_new_instance();
    handlers_[web_settings_products_trial_handler::get_resource()] = web_settings_products_trial_handler::get_new_instance();
    handlers_[web_settings_epg_mapping_channels_handler::get_resource()] = web_settings_epg_mapping_channels_handler::get_new_instance();
    handlers_[web_settings_epg_mapping_epgsources_handler::get_resource()] = web_settings_epg_mapping_epgsources_handler::get_new_instance();
    handlers_[web_settings_epg_mapping_epgsources_channels_handler::get_resource()] = web_settings_epg_mapping_epgsources_channels_handler::get_new_instance();
    handlers_[web_settings_epg_mapping_favorites_handler::get_resource()] = web_settings_epg_mapping_favorites_handler::get_new_instance();
    handlers_[web_settings_epg_mapping_favorites_channels_handler::get_resource()] = web_settings_epg_mapping_favorites_channels_handler::get_new_instance();
    handlers_[web_settings_epg_mapping_epgsearch_handler::get_resource()] = web_settings_epg_mapping_epgsearch_handler::get_new_instance();
    handlers_[web_settings_channels_properties_channels_handler::get_resource()] = web_settings_channels_properties_channels_handler::get_new_instance();
    handlers_[web_settings_channels_properties_favorites_handler::get_resource()] = web_settings_channels_properties_favorites_handler::get_new_instance();
    handlers_[web_settings_channels_properties_favorites_channels_handler::get_resource()] = web_settings_channels_properties_favorites_channels_handler::get_new_instance();
    handlers_[web_settings_channels_logos_channels_handler::get_resource()] = web_settings_channels_logos_channels_handler::get_new_instance();
    handlers_[web_settings_channels_logos_favorites_handler::get_resource()] = web_settings_channels_logos_favorites_handler::get_new_instance();
    handlers_[web_settings_channels_logos_favorites_channels_handler::get_resource()] = web_settings_channels_logos_favorites_channels_handler::get_new_instance();
    handlers_[web_settings_channels_logos_logosearch_handler::get_resource()] = web_settings_channels_logos_logosearch_handler::get_new_instance();
    handlers_[web_settings_channels_logos_packs_handler::get_resource()] = web_settings_channels_logos_packs_handler::get_new_instance();
    handlers_[web_settings_channels_logos_packs_logos_handler::get_resource()] = web_settings_channels_logos_packs_logos_handler::get_new_instance();
    handlers_[web_settings_channels_scan_devices_handler::get_resource()] = web_settings_channels_scan_devices_handler::get_new_instance();
    handlers_[web_settings_channels_scan_templates_handler::get_resource()] = web_settings_channels_scan_templates_handler::get_new_instance();
    handlers_[web_settings_channels_scan_devices_settings_handler::get_resource()] = web_settings_channels_scan_devices_settings_handler::get_new_instance();
    handlers_[web_settings_channels_scan_devices_status_handler::get_resource()] = web_settings_channels_scan_devices_status_handler::get_new_instance();
    handlers_[web_settings_channels_scan_providers_handler::get_resource()] = web_settings_channels_scan_providers_handler::get_new_instance();
    handlers_[web_settings_channels_scan_scanners_handler::get_resource()] = web_settings_channels_scan_scanners_handler::get_new_instance();
    handlers_[web_settings_channels_scan_start_handler::get_resource()] = web_settings_channels_scan_start_handler::get_new_instance();
    handlers_[web_settings_channels_scan_cancel_handler::get_resource()] = web_settings_channels_scan_cancel_handler::get_new_instance();
    handlers_[web_settings_channels_scan_apply_handler::get_resource()] = web_settings_channels_scan_apply_handler::get_new_instance();
    handlers_[web_settings_channels_scan_rescan_settings_handler::get_resource()] = web_settings_channels_scan_rescan_settings_handler::get_new_instance();
    handlers_[web_settings_channels_scan_rescan_start_handler::get_resource()] = web_settings_channels_scan_rescan_start_handler::get_new_instance();
    handlers_[web_settings_channels_scan_networks_handler::get_resource()] = web_settings_channels_scan_networks_handler::get_new_instance();
    handlers_[web_settings_channels_favorites_handler::get_resource()] = web_settings_channels_favorites_handler::get_new_instance();
    handlers_[web_settings_channels_favorites_allchannels_handler::get_resource()] = web_settings_channels_favorites_allchannels_handler::get_new_instance();
    handlers_[web_settings_sendto_targets_handler::get_resource()] = web_settings_sendto_targets_handler::get_new_instance();
    handlers_[web_settings_sendto_comskip_default_handler::get_resource()] = web_settings_sendto_comskip_default_handler::get_new_instance();
    handlers_[web_settings_sendto_comskip_handler::get_resource()] = web_settings_sendto_comskip_handler::get_new_instance();
    handlers_[web_settings_sendto_activateworkunit_handler::get_resource()] = web_settings_sendto_activateworkunit_handler::get_new_instance();
    handlers_[web_connect_tvrecordings_categories_handler::get_resource()] = web_connect_tvrecordings_categories_handler::get_new_instance();
    handlers_[web_connect_tvrecordings_programs_handler::get_resource()] = web_connect_tvrecordings_programs_handler::get_new_instance();
    handlers_[web_connect_tvrecordings_stop_handler::get_resource()] = web_connect_tvrecordings_stop_handler::get_new_instance();
    handlers_[web_connect_tvrecordings_remove_handler::get_resource()] = web_connect_tvrecordings_remove_handler::get_new_instance();
    handlers_[web_connect_library_sources_handler::get_resource()] = web_connect_library_sources_handler::get_new_instance();
    handlers_[web_connect_library_filters_handler::get_resource()] = web_connect_library_filters_handler::get_new_instance();
    handlers_[web_connect_library_programs_handler::get_resource()] = web_connect_library_programs_handler::get_new_instance();
    handlers_[web_connect_library_details_handler::get_resource()] = web_connect_library_details_handler::get_new_instance();
    handlers_[web_connect_stream_handler::get_resource()] = web_connect_stream_handler::get_new_instance();
    handlers_[web_connect_tvguide_channels_handler::get_resource()] = web_connect_tvguide_channels_handler::get_new_instance();
    handlers_[web_connect_tvguide_favorites_handler::get_resource()] = web_connect_tvguide_favorites_handler::get_new_instance();
    handlers_[web_connect_tvguide_programs_handler::get_resource()] = web_connect_tvguide_programs_handler::get_new_instance();
    handlers_[web_connect_schedules_handler::get_resource()] = web_connect_schedules_handler::get_new_instance();
    handlers_[web_connect_search_genres_handler::get_resource()] = web_connect_search_genres_handler::get_new_instance();
    handlers_[web_connect_schedules_channels_handler::get_resource()] = web_connect_schedules_channels_handler::get_new_instance();
    handlers_[web_connect_schedules_diskspace_handler::get_resource()] = web_connect_schedules_diskspace_handler::get_new_instance();
    handlers_[web_connect_schedules_edit_handler::get_resource()] = web_connect_schedules_edit_handler::get_new_instance();
    handlers_[web_connect_server_capabilities_handler::get_resource()] = web_connect_server_capabilities_handler::get_new_instance();
    handlers_[web_connect_sendto_targets_handler::get_resource()] = web_connect_sendto_targets_handler::get_new_instance();
    handlers_[web_connect_sendto_items_handler::get_resource()] = web_connect_sendto_items_handler::get_new_instance();
    handlers_[web_connect_sendto_items_canceled_handler::get_resource()] = web_connect_sendto_items_canceled_handler::get_new_instance();
}

void web_service::process_accept_language_header(const pion::http::request_ptr& http_request_ptr)
{
    //we process only language codes without quality qualifiers
    //also, if several comma separated values are given, only the first one is taken
    std::string lang;
    if (http_request_ptr->has_header("Accept-Language"))
    {
        std::string contents = http_request_ptr->get_header("Accept-Language");
        size_t pos = contents.find(",");
        if (pos == std::string::npos)
            lang = contents;
        else
            lang = contents.substr(0, pos);
    }
    if (!lang.empty())
    {
        std::string cur_lang;
        language_settings::GetInstance()->GetCurrentLanguageId(cur_lang);
        if (!boost::iequals(cur_lang, lang))
        {
            log_ext_info(L"web_service::process_accept_language_header. Changing language to %1%") % string_cast<EC_UTF8>(lang);
            if (!language_settings::GetInstance()->SetCurrentLanguageId(lang))
            {
                log_ext_info(L"web_service::process_accept_language_header. Language change to %1% failed") % string_cast<EC_UTF8>(lang);
            }
        }
    }
}

void web_service::send_binary_response(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, const std::vector<char>* data, 
                                       const std::string& mime, bool add_gzip_header)
{
	pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *http_request_ptr);

    writer->get_response().set_status_code(pion::http::types::RESPONSE_CODE_OK);
    writer->get_response().set_status_message(pion::http::types::RESPONSE_MESSAGE_OK);
    writer->get_response().set_content_type(mime);
    writer->get_response().change_header("Access-Control-Allow-Origin", access_control_allow_origin_hdr.c_str());
    if (add_gzip_header)
        writer->get_response().change_header("Content-Encoding", "gzip");
    writer->write((void*)&(*data)[0], data->size());
    writer->send();
    writer->get_connection()->finish();
}

void web_service::send_error_response(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, ::boost::uint32_t code, const std::string& msg)
{
	pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *http_request_ptr);

    writer->get_response().set_status_code(code);
    writer->get_response().set_status_message(msg);
    writer->get_response().set_content_type(text_xml_mime_code);
    writer->send();
    writer->get_connection()->finish();
}

void web_service::operator()(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn)
{
    process_accept_language_header(http_request_ptr);

    std::string web_root_resource = web_service_resource_url + std::string("/");
    std::string resource = http_request_ptr->get_resource();
    std::string subresource = resource.size() > web_root_resource.size() ? resource.substr(web_root_resource.size()) : "";

    //check if one of the handlers should be envoked
    std::string handler_resource = "/" + subresource;
    if (boost::ends_with(handler_resource, "/"))
        handler_resource.erase(handler_resource.size() - 1);

    if (handlers_.find(handler_resource) != handlers_.end())
    {
        log_ext_info(L"web_service::operator(). Envoking handler for %1% (%2%)") % string_cast<EC_UTF8>(handler_resource) % string_cast<EC_UTF8>(http_request_ptr->get_method());

        web_request_context context;
        context.stream_service = stream_service_;
        context.settings = settings_;
        context.port = port_;

        //envoke handler
        if (boost::iequals(http_request_ptr->get_method(), "GET"))
            handlers_[handler_resource]->process_get(message_queue_, http_request_ptr, tcp_conn, context);
        else if (boost::iequals(http_request_ptr->get_method(), "POST"))
            handlers_[handler_resource]->process_post(message_queue_, http_request_ptr, tcp_conn, context);
        else if (boost::iequals(http_request_ptr->get_method(), "DELETE"))
            handlers_[handler_resource]->process_delete(message_queue_, http_request_ptr, tcp_conn, context);
        else
            web_request_handler::send_json_error_message(http_request_ptr, tcp_conn, "no handler for this http method");
    } else
    {
        //this is a local file request

        //return index.html if subresource is empty
        if (subresource.empty())
            subresource = index_file_name;

        dvblink::filesystem_path_t item_path = web_root_file_path_ / subresource;

        std::string mime = text_html_mime_code;
        std::string ext = item_path.to_boost_filesystem().extension().string();
        if (ext_to_mime_type_map_.find(ext) != ext_to_mime_type_map_.end())
            mime = ext_to_mime_type_map_[ext];

        log_ext_info(L"web_service::operator(). Request for %1% (%2%)") % string_cast<EC_UTF8>(subresource) % string_cast<EC_UTF8>(mime);

        if (boost::filesystem::exists(item_path.to_boost_filesystem()))
        {
            try {
                //send contents of the file back
                std::ifstream file(item_path.to_string().c_str(), std::ios::binary);
                std::vector<char> file_contents((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
                if (file_contents.size() > 0)
                {
                    std::vector<char> compressed_data;

                    if (boost::iequals(subresource, index_file_name))
                    {
                        //compress file to show client that gzip content-encoding is supported
                        boost::iostreams::filtering_ostream os;
                        os.push(boost::iostreams::gzip_compressor());
                        os.push(boost::iostreams::back_inserter(compressed_data));
                        try {
                            boost::iostreams::write(os, &file_contents[0], file_contents.size());
                        } catch (...){}
                    }

                    send_binary_response(http_request_ptr, tcp_conn, compressed_data.size() > 0 ? &compressed_data : &file_contents, mime, compressed_data.size() > 0);
                } else
                {
                    send_error_response(http_request_ptr, tcp_conn, pion::http::types::RESPONSE_CODE_NOT_FOUND, pion::http::types::RESPONSE_MESSAGE_NOT_FOUND);
                }
            } catch(...)
            {
                //send server error response
                send_error_response(http_request_ptr, tcp_conn, pion::http::types::RESPONSE_CODE_SERVER_ERROR, pion::http::types::RESPONSE_MESSAGE_SERVER_ERROR);
            }
        } else
        {
            //send 404 response
            send_error_response(http_request_ptr, tcp_conn, pion::http::types::RESPONSE_CODE_NOT_FOUND, pion::http::types::RESPONSE_MESSAGE_NOT_FOUND);
        }
    }
}

void web_service::load_mime_types()
{
    dvblink::filesystem_path_t fname = web_file_path_ / mime_file_name;

    std::string ext;
    std::string mime;

    pugi::xml_document doc;
    if (doc.load_file(fname.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            pugi::xml_node mime_node = root_node.first_child();
            while (mime_node != NULL)
            {
               if (pugixml_helpers::get_node_attribute(mime_node, network_server_mime_ext_attr, ext) &&
                   pugixml_helpers::get_node_attribute(mime_node, network_server_mime_attr, mime))
               {
                   ext_to_mime_type_map_[ext] = mime;
               }
                mime_node = mime_node.next_sibling();
            }
        }
    }

}


} // dvblex
