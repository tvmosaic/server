/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/shared_ptr.hpp>
#include <dl_types.h>
#include <dl_streamer.h>
#include "stream_processor_base.h"

namespace dvblex {

class streamer_service_callbacks_t
{
public:
    streamer_service_callbacks_t(){}

    virtual void streamer_stopped(const dvblink::streamer_id_t& /* id */){};
};

class network_streamer_base_t;
typedef boost::shared_ptr<network_streamer_base_t> network_streamer_base_object_t;

class network_streamer_base_t : public dvblink::streamer_t
{
public:
    network_streamer_base_t(const std::string& id, stream_processor_base* processor);
	virtual ~network_streamer_base_t();

    virtual void notify_streamer_deleted();
    void set_streamer_service_callbacks(streamer_service_callbacks_t* streamer_service_callbacks);

    virtual void on_received_stream(const unsigned char* data, size_t len){}

    virtual std::string get_streaming_url(const dvblink::channel_id_t& channel_id, const std::string& protocol_header, const std::string& server_address, 
        boost::uint16_t streaming_port, int handle){return "";}

    virtual void signal_exit(){}

protected:
    streamer_service_callbacks_t* streamer_service_callbacks_;
    stream_processor_base* processor_;

    static void processor_callback(const unsigned char* data, size_t len, void* user_param);
    virtual void write_stream(const unsigned char* data, size_t len);
    void stop_processor();
};

} // dvblink
