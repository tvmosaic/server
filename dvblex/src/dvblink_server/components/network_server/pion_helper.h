/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <boost/algorithm/string.hpp> 
#include <pion/http/plugin_service.hpp>
#include <dl_sockets.h>

namespace dvblex { 

inline std::string get_server_ip_from_request(const pion::http::request_ptr& http_request_ptr)
{
    std::string server_ip;
    if (http_request_ptr->has_header("Host"))
    {
        std::string host = http_request_ptr->get_header("Host");
        size_t pos = host.find(":");
        if (pos == std::string::npos)
            server_ip = host;
        else
            server_ip = host.substr(0, pos);
    }
    return server_ip;
}

inline bool is_local_pion_request(const pion::http::request_ptr& request, const pion::tcp::connection_ptr& tcp_conn)
{
    const SOCKET sock = tcp_conn->get_socket().native_handle();

    return socket_check_if_local_connection(sock);
/*
    std::string server_ip = get_server_ip_from_request(request);
    bool ret_val = boost::iequals(server_ip, "localhost") || boost::iequals(server_ip, "127.0.0.1");

    return ret_val;
*/
}

}
