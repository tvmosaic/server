/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp> 
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_message_addresses.h>
#include <dl_message_server.h>
#include <dl_url_encoding.h>
#include <dl_file_procedures.h>
#include <dl_platforms.h>
#include <dl_transcoder.h>
#include "constants.h"
#include "live_transcoder.h"
#include "iphone_segmentor_streamer.h"
#include "stream_service.h"
#include "http_streamer.h"
#include "mime_types.h"
#include "pion_helper.h"

#ifdef _MSC_VER
#pragma warning (disable : 4100)
#endif

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;
using namespace dvblink::transcoder;

namespace dvblex {

stream_service::stream_service(dvblink::i_server_t server, dvblink::messaging::message_queue_t message_queue, unsigned short port, network_server_settings_t* settings) :
    server_(server), message_queue_(message_queue), port_(port), stream_handle_seed_(1), settings_(settings)
{
    create_and_clean_timeshift_dir();
    playback_objects_.init(settings, server);
}

stream_service::~stream_service()
{
    playback_objects_.term();
    remove_all_streamers();
}

void stream_service::stop()
{
    playback_objects_.term();
    remove_all_streamers();

    pion::http::plugin_service::stop();
}

void stream_service::create_and_clean_timeshift_dir()
{
    //create temp directory
    filesystem_path_t p = settings_->get_timeshift_dir();
    if (!p.to_string().empty())
    {
        try
        {
            boost::filesystem::create_directories(p.to_boost_filesystem());
        } catch(...){}
        //clean up temp dir (if there are any old files left)
	    std::vector<boost::filesystem::path> temp_files;
        dvblink::engine::filesystem::find_files(p.get(), temp_files, L"");
	    for (unsigned int i = 0; i < temp_files.size(); i++)
	    {
            try
            {
                boost::filesystem::remove(temp_files[i]);
            } catch(...){}
	    }
    }
}

static void send_error_response(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, ::boost::uint32_t code, const std::string& msg)
{
	pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *http_request_ptr);

    writer->get_response().set_status_code(code);
    writer->get_response().set_status_message(msg);
    writer->get_response().set_content_type(text_xml_mime_code);
    writer->send();
    writer->get_connection()->finish();
}

bool stream_service::is_direct_request(const pion::http::request_ptr& http_request_ptr)
{
    std::string resource = http_request_ptr->get_resource();

    size_t n = resource.rfind('.');
	if (n != std::string::npos)
	    resource = resource.substr(0, n);

    return boost::iequals(direct_stream_full_prefix, resource);
}

void stream_service::operator()(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn)
{
    bool res = false;
  
    std::string client_addr = tcp_conn->get_remote_ip().to_string();
    if (is_valid_client_ip(client_addr))
    {
        std::string object_id;
        bool head_for_playback_object = boost::iequals(http_request_ptr->get_method(), "HEAD") && 
            playback_objects_.is_playback_object_request(http_request_ptr, object_id);

        if (boost::iequals(http_request_ptr->get_method(), "GET") ||
            head_for_playback_object)
        {
            int handle;
            unsigned long segment_id;
            bool is_audio_only;
            m3u8_list_type_e list_type;

            if (is_direct_request(http_request_ptr))
                res = process_direct_request(http_request_ptr, tcp_conn);
            else
            if (http_timeshifted_streamer_t::is_timeshift_request(http_request_ptr))
            {
                res = process_timeshift_request(http_request_ptr, tcp_conn);
            } else
            if (http_streamer_t::is_http_stream_request(http_request_ptr, handle))
            {
                log_ext_info(L"stream_service::operator(). http_stream_request %1%") % string_cast<EC_UTF8>(http_request_ptr->get_resource());

                boost::shared_ptr<http_streamer_t> http_streamer = boost::static_pointer_cast<http_streamer_t>(get_streamer_object_from_handle(handle));
	            if (http_streamer)
                    res = http_streamer->process_request(http_request_ptr, tcp_conn);
            } else
            if (iphone_segmentor::is_segment_request(http_request_ptr, handle, segment_id, is_audio_only))
            {
                log_ext_info(L"stream_service::operator(). process_segment_request %1%") % string_cast<EC_UTF8>(http_request_ptr->get_resource());

                boost::shared_ptr<iphone_segmentor> segmentor = boost::static_pointer_cast<iphone_segmentor>(get_streamer_object_from_handle(handle));
	            if (segmentor)
                    res = segmentor->process_segment_request(http_request_ptr, tcp_conn, segmentor, segment_id, is_audio_only);
            } else
            if (iphone_segmentor::is_m3u8_request(http_request_ptr, handle, list_type))
            {
                boost::shared_ptr<iphone_segmentor> segmentor = boost::static_pointer_cast<iphone_segmentor>(get_streamer_object_from_handle(handle));
	            if (segmentor)
                    res = segmentor->process_m3u8_request(get_http_prefix(), http_request_ptr, tcp_conn, handle, list_type);
            } else
            {
                if (playback_objects_.is_playback_object_request(http_request_ptr, object_id))
                {
                    res = playback_objects_.start_object_streaming(get_http_prefix(), object_id, http_request_ptr, tcp_conn);
                } else
                {
                    log_error(L"stream_service::operator(). Unknown resource %1%") % string_cast<EC_UTF8>(http_request_ptr->get_resource());
                }
            }
        } else
        if (boost::iequals(http_request_ptr->get_method(), "HEAD"))
        {
            log_info(L"stream_service::operator(): Sending generic response for HEAD request");

            pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *http_request_ptr);
            writer->get_response().set_status_code(pion::http::types::RESPONSE_CODE_OK);
            writer->get_response().set_status_message(pion::http::types::RESPONSE_MESSAGE_OK);
            writer->get_response().set_content_type(octet_stream_mime_code);
            writer->get_response().change_header("CONTENT-LENGTH", "*");
            writer->send();
            writer->get_connection()->finish();
            res = true;
        }
    } else
    {
        log_info(L"stream_service::operator(). Blocked unverified IP %1%") % string_cast<EC_UTF8>(client_addr);
    }

    if (!res)
        send_error_response(http_request_ptr, tcp_conn, pion::http::types::RESPONSE_CODE_NOT_FOUND, pion::http::types::RESPONSE_MESSAGE_NOT_FOUND);
}

stream_processor_base* stream_service::get_processor_from_http_request(const pion::http::request_ptr& request)
{
    stream_processor_base* ret_val = NULL;

    if (request->has_query(request_param_transcoder) && request->has_query(request_param_transcoder_bitrate))
    {
        std::string str;
        std::string stream_type = request->get_query(request_param_transcoder);

        transcoder::transcoder_params tp;

        if (request->has_query(request_param_transcoder_width))
            tp.tr_width_ = atoi(request->get_query(request_param_transcoder_width).c_str());

        if (request->has_query(request_param_transcoder_height))
            tp.tr_height_ = atoi(request->get_query(request_param_transcoder_height).c_str());

        if (request->has_query(request_param_transcoder_scale))
            tp.tr_video_scale_ = atoi(request->get_query(request_param_transcoder_scale).c_str());

        if (request->has_query(request_param_transcoder_bitrate))
            tp.tr_bitrate_ = atoi(request->get_query(request_param_transcoder_bitrate).c_str());

        if (request->has_query(request_param_transcoder_lng))
            tp.audio_track_ = request->get_query(request_param_transcoder_lng);

        ret_val = get_processor_from_stream_type(stream_type, tp);
    }

    return ret_val;
}

stream_processor_base* stream_service::get_processor_from_play_request(const play_channel_request_t& request)
{
    stream_processor_base* ret_val = NULL;

    if (request.transcoded_stream_)
    {
        transcoder::transcoder_params tp;

        tp.tr_width_ = request.tr_width_;
        tp.tr_height_ = request.tr_height_;
        tp.tr_bitrate_ = request.tr_bitrate_;
        tp.audio_track_ = request.audio_track_;
        tp.tr_video_scale_ = request.tr_video_scale_;

        ret_val = get_processor_from_stream_type(request.stream_type_, tp);
    }

    return ret_val;
}

stream_processor_base* stream_service::get_processor_from_stream_type(const std::string& stream_type, const transcoder::transcoder_params& tp)
{
    stream_processor_base* ret_val = NULL;

    std::string usecase_id;
    if (boost::iequals(stream_type, stream_type_h264ts) || boost::iequals(stream_type, stream_type_h264ts_http_timeshift))
        usecase_id = tu_transcode_live_to_ts_id;
    else
    if (boost::iequals(stream_type, stream_type_hls))
        usecase_id = tu_transcode_live_to_hls_id;
    else
    if (boost::iequals(stream_type, stream_type_mp4))
        usecase_id = tu_transcode_live_to_mp4_id;
    else
        log_warning(L"stream_service::get_processor_from_stream_type. Unknown stream type %1%") % string_cast<EC_UTF8>(stream_type);

    if (!usecase_id.empty())
    {
        server::ffmpeg_launch_params_request req(usecase_id);
        server::ffmpeg_launch_params_response resp;
        if (message_queue_->send(server_message_queue_addressee, req, resp) == success && resp.result_)
            ret_val = new live_transcoder(resp.launch_params_, tp);
        else
            log_warning(L"stream_service::get_processor_from_stream_type. Failed to get ffmpeg launch parameters for usecase %1%") % string_cast<EC_UTF8>(usecase_id);
    }

    return ret_val;
}

i_stream_source_control_t stream_service::get_stream_control_if()
{
    i_stream_source_control_t stream_interface;
    if (!server_->query_object_interface(message_queue_->get_uid(), base_id_t(source_manager_message_queue_addressee.get()), stream_source_control_interface, stream_interface) == dvblink::i_success)
    {
        log_error(L"stream_service::get_stream_control_if. Failed to get stream interface");
    }
    return stream_interface;
}

bool stream_service::process_direct_request(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn)
{
    bool ret_val = false;
    if (http_request_ptr->has_query(channel_param_name))
    {
        std::string e_channel_id = http_request_ptr->get_query(channel_param_name);
        std::string channel_id;
        url_decode(e_channel_id.c_str(), channel_id);

        std::string client_id;
        if (http_request_ptr->has_query(client_param_name))
        {
            std::string e_client_id = http_request_ptr->get_query(client_param_name);
            url_decode(e_client_id.c_str(), client_id);
        } else
        {
            client_id = tcp_conn->get_remote_ip().to_string();
        }

        log_info(L"stream_service::process_direct_request. Starting direct stream for channel %1%, client %2%") % string_cast<EC_UTF8>(channel_id) % string_cast<EC_UTF8>(client_id);

        //stop and remove existing streamer for this client id (if any)
        remove_active_streamer(client_id);

        stream_processor_base* processor = get_processor_from_http_request(http_request_ptr);
        boost::shared_ptr<http_streamer_t> ts = share_streamer_safely(new http_streamer_t(client_id, processor,
            processor != NULL ? processor->get_mime() : NULL, 
            processor != NULL ? processor->is_transport_stream() : true));

        pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *http_request_ptr);
        ts->add_client(writer);

        network_streamer_base_object_t net_streamer = ts;
        add_streamer(net_streamer);

        i_stream_source_control_t stream_control = get_stream_control_if();
        if (stream_control)
        {
            streamer_object_t obj = ts;
            dvblink::EStatus status = stream_control->start_channel(channel_id.c_str(), obj);
            if (status == DvbLink_StatusOk)
            {
                ret_val = true;
            } else
            {
                remove_streamer(ts->get_id());
                ts.reset();
                log_error(L"stream_service::operator(). server_->start_channel returned error %1% for channel %2%") % status % string_cast<EC_UTF8>(channel_id);
            }
            stream_control.reset();
        } else
        {
            remove_streamer(ts->get_id());
            ts.reset();
        }
    } else
    {
        //invalid command request
        //send error
        log_error(L"stream_service::process_direct_request. Invalid request format %1%") % string_cast<EC_UTF8>(http_request_ptr->get_query_string());
    }
    return ret_val;
}

bool stream_service::process_timeshift_request(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn)
{
    bool ret_val = false;
    if (http_request_ptr->has_query(handle_param_name))
    {
        std::string handle_str = http_request_ptr->get_query(handle_param_name);
        int handle = atoi(handle_str.c_str());
        http_timeshifted_streamer_object_t ts = get_timeshift_object_from_handle(handle);

        if (ts != NULL)
            ret_val = ts->process_request(http_request_ptr, tcp_conn);

    } else
    {
        log_error(L"stream_service::process_timeshift_request. Invalid request format %1%") % string_cast<EC_UTF8>(http_request_ptr->get_query_string());
    }
    return ret_val;
}

std::string stream_service::get_direct_streaming_url(const dvblink::channel_id_t& channel_id, const std::string& server_address, const std::string& client_id)
{
    std::stringstream strbuf;
    strbuf << get_http_prefix() << "://" << server_address << ":" << port_ << direct_stream_full_prefix << "?";

    if (!client_id.empty())  
        strbuf << client_param_name << "=" << client_id << "&";

    strbuf << channel_param_name << "=" << channel_id;

    return strbuf.str();
}

void stream_service::remove_active_streamer(const dvblink::streamer_id_t& id)
{
    remove_streamer(id);
    i_stream_source_control_t stream_control = get_stream_control_if();
    if (stream_control)
    {
        stream_control->stop_streamer(id.to_string().c_str());
        stream_control.reset();
    }
}

void stream_service::remove_active_streamer(int handle)
{
    streamer_id_t id;
    if (get_streamer_id(handle, id))
        remove_active_streamer(id);
}

void stream_service::streamer_stopped(const dvblink::streamer_id_t& id)
{
    log_info(L"stream_service::streamer_stopped. Reporting stopped streamer %1%") % id.to_wstring();

    remove_streamer(id);
}

int stream_service::add_streamer(network_streamer_base_object_t& streamer)
{
    boost::unique_lock<boost::mutex> lock(streamer_map_lock_);

    streamer->set_streamer_service_callbacks(this);
    streamer_desc_t sd;
    sd.handle = stream_handle_seed_++;
    sd.streamer = streamer;
    //stream_handle_seed_
    active_streamer_map_[streamer->get_id()] = sd;

    return sd.handle;
}

void stream_service::remove_streamer(const dvblink::streamer_id_t& id)
{
    boost::unique_lock<boost::mutex> lock(streamer_map_lock_);

    if (active_streamer_map_.find(id) != active_streamer_map_.end())
    {
        network_streamer_base_object_t streamer = active_streamer_map_[id].streamer;
        streamer->set_streamer_service_callbacks(NULL);
        active_streamer_map_.erase(id);
        streamer->signal_exit();
        streamer.reset();
    }
}

network_streamer_base_object_t stream_service::get_streamer(const dvblink::streamer_id_t& id)
{
    boost::unique_lock<boost::mutex> lock(streamer_map_lock_);

    network_streamer_base_object_t ret_val;

    if (active_streamer_map_.find(id) != active_streamer_map_.end())
        ret_val = active_streamer_map_[id].streamer;

    return ret_val;
}

bool stream_service::get_streamer_id(int handle, dvblink::streamer_id_t& id)
{
    boost::unique_lock<boost::mutex> lock(streamer_map_lock_);

    bool ret_val = false;

    network_streamer_map_t::iterator it = active_streamer_map_.begin();
    while (it != active_streamer_map_.end())
    {
        if (it->second.handle == handle)
        {
            id = it->first;
            ret_val = true;
            break;
        }
        ++it;
    }

    return ret_val;
}

void stream_service::remove_all_streamers()
{
    boost::unique_lock<boost::mutex> lock(streamer_map_lock_);

    network_streamer_map_t::iterator it = active_streamer_map_.begin();
    while (it != active_streamer_map_.end())
    {
        it->second.streamer->set_streamer_service_callbacks(NULL);
        ++it;
    }

    active_streamer_map_.clear();
}

network_streamer_base_object_t stream_service::get_streamer_from_play_request(const std::string& client_id, const std::string& server_address, 
                                                                              const play_channel_request_t& request, dvblink::EStatus& status)
{
    network_streamer_base_object_t ret_val;

    status = DvbLink_StatusError;

    if (boost::iequals(request.stream_type_, stream_type_raw_http_timeshift) || 
        boost::iequals(request.stream_type_, stream_type_h264ts_http_timeshift))
    {
        dvblink::filesystem_path_t disk_path = settings_->get_timeshift_dir();
        boost::uint64_t max_size = settings_->get_timeshift_buffer_max_size();

        boost::filesystem::space_info si;

        bool b_space = true;
        try {
            si = boost::filesystem::space(disk_path.to_boost_filesystem());
        } catch(...) 
        {
            b_space = false;
        }

        if (b_space)
        {
            boost::uint64_t slack_size = timeshift_disk_slack_size;

            boost::uint64_t min_size = si.available > slack_size ? si.available - slack_size : slack_size;
            max_size = si.available > max_size + slack_size ? max_size : min_size;

            log_info(L"stream_service::get_streamer_from_play_request. Timeshift drive (%1%) stats: available space %2%, buffer size %3%") % disk_path.to_wstring() % si.available % max_size;
        } else
        {
            log_warning(L"stream_service::get_streamer_from_play_request. boost::filesystem::space failed. Using default value (%1%) for buffer size") % max_size;
        }

        stream_processor_base* processor = get_processor_from_play_request(request);

        boost::shared_ptr<http_timeshifted_streamer_t> ts = share_streamer_safely(new http_timeshifted_streamer_t(client_id, disk_path, max_size, processor));
        status = DvbLink_StatusOk;
        ret_val = ts;
    } 
    else
    if (boost::iequals(request.stream_type_, stream_type_hls))
    {
        stream_processor_base* processor = get_processor_from_play_request(request);
        boost::shared_ptr<iphone_segmentor> ts = share_streamer_safely(new iphone_segmentor(client_id, port_, server_address, processor));
        status = DvbLink_StatusOk;
        ret_val = ts;
    } else
    if (boost::iequals(request.stream_type_, stream_type_h264ts) || boost::iequals(request.stream_type_, stream_type_raw_http)  
        || boost::iequals(request.stream_type_, stream_type_mp4))
    {
        stream_processor_base* processor = get_processor_from_play_request(request);
        boost::shared_ptr<http_streamer_t> ts = share_streamer_safely(new http_streamer_t(client_id, processor, 
            processor != NULL ? processor->get_mime() : NULL, 
            processor != NULL ? processor->is_transport_stream() : true));
        status = DvbLink_StatusOk;
        ret_val = ts;
    } else
        log_error(L"stream_service::get_streamer_from_play_request. Unsupported stream type %1%") % string_cast<EC_UTF8>(request.stream_type_);

    return ret_val;
}

dvblink::EStatus stream_service::process_play_channel_request(const play_channel_request_t& request, const std::string& server_address, const std::string& client_address, play_channel_response_t& response)
{
    dvblink::EStatus ret_val = DvbLink_StatusError;

    std::string client_id;
    if (!request.stream_client_id_.empty())
        client_id = request.stream_client_id_;
    else
        client_id = client_address;

    log_info(L"stream_service::process_play_channel_request. Starting %1% stream for channel %2%, client %3%") % string_cast<EC_UTF8>(request.stream_type_) % request.channel_id_.to_wstring() % string_cast<EC_UTF8>(client_id);

    //stop and remove existing streamer for this client id (if any)
    remove_active_streamer(client_id);

    network_streamer_base_object_t net_streamer = get_streamer_from_play_request(client_id, server_address, request, ret_val);

    if (net_streamer != NULL)
    {
        //reset return status variable
        ret_val = DvbLink_StatusError;

        response.channel_handle_ = add_streamer(net_streamer);
        response.url_ = net_streamer->get_streaming_url(request.channel_id_, get_http_prefix(), server_address, port_, response.channel_handle_);

        i_stream_source_control_t stream_control = get_stream_control_if();
        if (stream_control)
        {
            streamer_object_t obj = net_streamer;
            ret_val = stream_control->start_channel(request.channel_id_.to_string().c_str(), obj);
            if (ret_val != DvbLink_StatusOk)
            {
                remove_streamer(net_streamer->get_id());
                net_streamer.reset();
                log_error(L"stream_service::process_play_channel_request. server_->start_channel returned error for channel %1%") % request.channel_id_.to_wstring();
            }
            stream_control.reset();
        } else
        {
            remove_streamer(net_streamer->get_id());
            net_streamer.reset();
        }
    }

    return ret_val;
}

bool stream_service::process_stop_channel_request(const stop_channel_request_t& request)
{
    log_info(L"stream_service::process_stop_channel_request. Handle %1%, client %2%") % request.channel_handle_ % string_cast<EC_UTF8>(request.stream_client_id_);

    if (!request.stream_client_id_.empty())
        remove_active_streamer(request.stream_client_id_);
    else
        remove_active_streamer(request.channel_handle_);

    return true;
}

network_streamer_base_object_t stream_service::get_streamer_object_from_handle(int handle)
{
    network_streamer_base_object_t ret_val;
    dvblink::streamer_id_t id;
    if (get_streamer_id(handle, id))
    {
        ret_val = get_streamer(id);
    } else
    {
        log_error(L"stream_service::get_streamer_object_from_handle. Handle does not exist %1%") % handle;
    }
    return ret_val;
}

http_timeshifted_streamer_object_t stream_service::get_timeshift_object_from_handle(int handle)
{
    http_timeshifted_streamer_object_t ret_val = boost::static_pointer_cast<http_timeshifted_streamer_t>(get_streamer_object_from_handle(handle));
    if (ret_val == NULL)
    {
        log_error(L"stream_service::get_timeshift_object_from_handle. Timeshift streamer is NULL (%1%)") % handle;
    }
    return ret_val;
}

bool stream_service::process_timeshift_stats_request(const timeshift_stats_request_t& request, timeshift_stats_response_t& response)
{
    bool ret_val = false;

    http_timeshifted_streamer_object_t ts = get_timeshift_object_from_handle(request.channel_handle_);
    if (ts != NULL)
    {
        timeshift_stats_s stats;
        if (ts->get_timeshift_stats(stats))
        {
            response.max_buffer_length_ = stats.max_buffer_length;
            response.buffer_length_ = stats.buffer_length;
            response.cur_pos_bytes_ = stats.cur_pos_bytes;
            response.buffer_duration_ = stats.buffer_duration;
            response.cur_pos_sec_ = stats.cur_pos_sec;
            ret_val = true;
        } else
        {
            log_error(L"stream_service::process_timeshift_stats_request. get_timeshift_stats failed (%1%)") % request.channel_handle_;
        }
    }
    return ret_val;
}

bool stream_service::process_timeshift_seek_request(const timeshift_seek_request_t& request)
{
    bool ret_val = false;

    http_timeshifted_streamer_object_t ts = get_timeshift_object_from_handle(request.channel_handle_);
    if (ts != NULL)
    {
        boost::uint64_t cur_pos;

        if (request.type_ == 1)
            ret_val = ts->seek_by_time(request.offset_, request.whence_, cur_pos);
        else
            ret_val = ts->seek_by_bytes(request.offset_, request.whence_, cur_pos);
    }
    return ret_val;
}

bool stream_service::is_transcoding_supported()
{
    bool ret_val = false;
    server::ffmpeg_launch_params_request req(tu_transcode_live_to_ts_id);
    server::ffmpeg_launch_params_response resp;
    ret_val = message_queue_->send(server_message_queue_addressee, req, resp) == success && resp.result_;

    return ret_val;
}

void stream_service::fill_streaming_caps(server_capabilities_t& caps)
{
    caps.supports_timeshift_ = true;
    caps.timeshift_version_ = 1;
    
    if (is_transcoding_supported())
    {
        caps.protocols_ = lpe_http | lpe_hls;
        caps.transcoders_ = lte_raw | lte_h264 | lte_aac;

        caps.pb_protocols_ = ppe_http | ppe_hls;
        caps.pb_transcoders_ = pte_raw | pte_h264 | pte_aac;
    } else
    {
        caps.protocols_ = lpe_http;
        caps.transcoders_ = lte_raw;

        caps.pb_protocols_ = ppe_http;
        caps.pb_transcoders_ = pte_raw;
    }
}

static inline boost::uint32_t ip_parts_to_num(boost::uint32_t u1, boost::uint32_t u2, boost::uint32_t u3, boost::uint32_t u4)
{
    return ((u1 << 24) | (u2 << 16) | (u3 << 8) | u4);
}

static bool is_private_ip(const std::string& ip)
{
    bool ret_val = false;

    static boost::uint32_t r1_min = ip_parts_to_num(10, 0, 0, 0);
    static boost::uint32_t r1_max = ip_parts_to_num(10, 255, 255, 255);

    static boost::uint32_t r2_min = ip_parts_to_num(172, 16, 0, 0);
    static boost::uint32_t r2_max = ip_parts_to_num(172, 31, 255, 255);

    static boost::uint32_t r3_min = ip_parts_to_num(192, 168, 0, 0);
    static boost::uint32_t r3_max = ip_parts_to_num(192, 168, 255, 255);

    static boost::uint32_t r4_min = ip_parts_to_num(169, 254, 0, 0);
    static boost::uint32_t r4_max = ip_parts_to_num(169, 254, 255, 255);

    static boost::uint32_t loopback = ip_parts_to_num(127, 0, 0, 1);

    boost::uint32_t u1, u2, u3, u4;
    if (sscanf(ip.c_str(), "%u.%u.%u.%u", &u1, &u2, &u3, &u4) == 4)
    {
        boost::uint32_t ipnum = ip_parts_to_num(u1, u2, u3, u4);

        ret_val = (ipnum >= r1_min && ipnum <= r1_max) ||
                  (ipnum >= r2_min && ipnum <= r2_max) ||
                  (ipnum >= r3_min && ipnum <= r3_max) ||
                  (ipnum >= r4_min && ipnum <= r4_max) ||
                  ipnum == loopback;
    }
    return ret_val;
}

void stream_service::add_client_ip(const std::string& client_ip)
{
    boost::unique_lock<boost::mutex> lock(client_ip_map_lock_);
    if (client_ip_map_.find(client_ip) == client_ip_map_.end())
        log_info(L"stream_service::add_client_ip. Added safe client IP %1%") % string_cast<EC_UTF8>(client_ip);
    //if this ip already exists then its timestamp will be updated
    client_ip_map_[client_ip] = client_ip_desc_t();
}

bool stream_service::is_valid_client_ip(const std::string& client_ip)
{
    //if stream security is disabled, always return true
    if (settings_->is_stream_security_enabled())
    {
        //do not check this for private IP addresses - they are from trusted LAN
        bool ret_val = is_private_ip(client_ip);

        if (!ret_val)
        {
            boost::unique_lock<boost::mutex> lock(client_ip_map_lock_);

            ret_val = client_ip_map_.find(client_ip) != client_ip_map_.end();
        }

        return ret_val;
    }

    return true;
}

} // dvblink
