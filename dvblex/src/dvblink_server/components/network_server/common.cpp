/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_message_channels.h>
#include <dl_message_addresses.h>
#include "common.h"

using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;
using namespace dvblink;

namespace dvblex { 

bool ns_get_channel_descriptions(dvblink::favorite_id_t& favorite_id, message_queue_t& message_queue, network_server_settings_t* settings, 
                                               stream_service* stream_service, const std::string& server_address, channel_desc_list_t& cdl)
{
    bool ret_val = false;

    channels::get_provider_channels_request req;
    channels::get_provider_channels_response resp;
    if (message_queue->send(source_manager_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        ret_val = true;

        //get channel logos
        channels::get_channel_logo_request logo_req(server_address, stream_service->get_http_prefix());
        channels::get_channel_logo_response logo_resp;
        if (message_queue->send(logo_manager_message_queue_addressee, logo_req, logo_resp) != success)
            log_warning(L"command_service::get_channel_descriptions. get_channel_logo_request failed");

        //get channel overwrites
        channels::get_channel_overwrites_request ov_req;
        channels::get_channel_overwrites_response ov_resp;
        if (message_queue->send(source_manager_message_queue_addressee, ov_req, ov_resp) != success)
            log_warning(L"command_service::get_channel_descriptions. get_channel_overwrites_request failed");

        //get favorites (if requested) and build channel id map for the fast lookup
        std::map<channel_id_t, channel_id_t> fav_channel_map;
        if (!favorite_id.empty())
        {
            log_info(L"command_service::get_channel_descriptions. Getting channels for favorite %1%") % favorite_id.to_wstring();
            channels::get_channel_favorites_request fav_req;
            channels::get_channel_favorites_response fav_resp;
            if (message_queue->send(source_manager_message_queue_addressee, fav_req, fav_resp) == success &&
                resp.result_)
            {
                for (size_t i=0; i<fav_resp.favorites_.size(); i++)
                {
                    channel_favorite_t& fav = fav_resp.favorites_[i];
                    if (boost::iequals(fav.id_.get(), favorite_id.get()))
                    {
                        for (size_t j=0; j<fav.channels_.size(); j++)
                            fav_channel_map[fav.channels_.at(j)] = fav.channels_[j];

                        break;
                    }
                }
            }
        }

        std::map<dvblink::channel_id_t, boost::int8_t> processed_channels_map;

        for (size_t i=0; i<resp.headends_.size(); i++)
        {
            for (size_t j=0; j<resp.headends_[i].transponders_.size(); j++)
            {
                for (size_t k=0; k<resp.headends_[i].transponders_[j].channels_.size(); k++)
                {
                    concise_device_channel_t& ch = resp.headends_[i].transponders_[j].channels_[k];

                    //check if channel belongs to the requested favorite
                    if (!favorite_id.empty() && fav_channel_map.find(ch.id_) == fav_channel_map.end())
                        continue;

                    //it can happen that the same channels are present on different transponders (duplicate, similar scan parameters etc.)
                    //filterthose channels from the list
                    if (processed_channels_map.find(ch.id_) == processed_channels_map.end())
                    {
                        processed_channels_map[ch.id_] = 1;

                        channel_description_t cd;
                        cd.from_concise_channel(ch);

                        //channel overwrites
                        if (ov_resp.overwrites_.find(cd.id_) != ov_resp.overwrites_.end())
                        {
                            channel_overwrite_t& ov = ov_resp.overwrites_[cd.id_];
                            if (!ov.name_.empty())
                                cd.name_ = ov.name_;
                            if (ov.number_.get() > 0)
                            {
                                cd.num_ = ov.number_;

                                if (ov.subnumber_.get() > 0)
                                    cd.sub_num_ = ov.subnumber_;
                            }
                        }

                        //assign logo from logo manager, if any
                        if (logo_resp.channel_logo_.find(cd.id_) != logo_resp.channel_logo_.end())
                            cd.logo_ = logo_resp.channel_logo_[cd.id_];

                        cdl.push_back(cd);
                    }
                }
            }
        }
    }
    return ret_val;
}

bool ns_get_raw_channel_descriptions(dvblink::messaging::message_queue_t& message_queue, channel_desc_list_t& cdl)
{
    bool ret_val = false;

    channels::get_provider_channels_request req;
    channels::get_provider_channels_response resp;
    if (message_queue->send(source_manager_message_queue_addressee, req, resp) == success && resp.result_)
    {
        std::map<dvblink::channel_id_t, boost::int8_t> processed_channels_map;

        for (size_t i=0; i<resp.headends_.size(); i++)
        {
            for (size_t j=0; j<resp.headends_[i].transponders_.size(); j++)
            {
                for (size_t k=0; k<resp.headends_[i].transponders_[j].channels_.size(); k++)
                {
                    concise_device_channel_t& ch = resp.headends_[i].transponders_[j].channels_[k];

                    //it can happen that the same channels are present on different transponders (duplicate, similar scan parameters etc.)
                    //filterthose channels from the list
                    if (processed_channels_map.find(ch.id_) == processed_channels_map.end())
                    {
                        processed_channels_map[ch.id_] = 1;

                        channel_description_t cd;
                        cd.from_concise_channel(ch);
                        cdl.push_back(cd);
                    }
                }
            }
        }

        ret_val = true;
    }

    return ret_val;
}


}