/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <algorithm>
#include <dl_strings.h>
#include "mime_types.h"
#include "live_transcoder.h"

using namespace dvblink::logging;
using namespace dvblex::settings;
using namespace dvblink::engine;
using namespace dvblink::transcoder;

namespace dvblex {

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4355)
#endif

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

live_transcoder::live_transcoder(const dvblink::transcoder::ffmpeg_launch_params_t& ffmpeg_launch_params, const dvblink::transcoder::transcoder_params& tp) :
    stream_processor_base(), ffmpeg_(NULL), bitrate_(0), audio_selector_(&audio_selector_cb, this), 
    mime_(ffmpeg_launch_params.mime_.to_string()), 
    is_ts_(ffmpeg_launch_params.is_transport_stream_.get())
{
    std::string audio_track_ISO639 = tp.audio_track_;

    ffmpeg_param_template_init_t init_info;
    if (tp.tr_bitrate_ > 0)
    {
        init_info.bitrate_kbits_ = tp.tr_bitrate_;
        bitrate_ = tp.tr_bitrate_;
    }
    if (tp.tr_width_ > 0 && tp.tr_height_ > 0)
    {
        init_info.height_ = std::min(tp.tr_width_, tp.tr_height_);
        init_info.width_ = std::max(tp.tr_width_, tp.tr_height_);
    }
    if (tp.tr_video_scale_ > 0)
    {
        init_info.width_ = ffmpeg_param_invalid_dimension;
        init_info.height_ = ffmpeg_param_invalid_dimension;
        init_info.video_scale_ = tp.tr_video_scale_;
    }

	log_info(L"Starting live_transcoder. w %1%, h %2%, b %3%, s %4%, a (%5%)") % init_info.width_ % init_info.height_ % bitrate_ % init_info.video_scale_ % string_cast<EC_UTF8>(audio_track_ISO639);

    ffmpeg_ = new ffmpeg_wrapper_t(this, &live_transcoder::ffmpeg_callback, 
        ffmpeg_launch_params.ffmpeg_exepath_, ffmpeg_launch_params.ffmpeg_dir_, ffmpeg_launch_params.is_transport_stream_.get());

    init_info.src_ = ffmpeg_->get_in_pipe_name();
    init_info.dest_ = ffmpeg_->get_out_pipe_name();

    std::vector<dvblink::launch_param_t> args;
    init_ffmpeg_launch_arguments(ffmpeg_launch_params.arguments_, init_info, args);

    ffmpeg_->start(args, ffmpeg_launch_params.ffmpeg_env_);
	audio_selector_.Start(audio_track_ISO639.c_str());
}


#ifdef _MSC_VER
#pragma warning(pop)
#endif

live_transcoder::~live_transcoder()
{
	audio_selector_.Stop();
    ffmpeg_->stop();

    delete ffmpeg_;
}

void live_transcoder::write_stream(const unsigned char* data, size_t len)
{
	audio_selector_.ProcessStream((unsigned char*)data, len);
}

void live_transcoder::audio_selector_cb(const unsigned char* buf, int len, void* param)
{
	live_transcoder* parent = (live_transcoder*)param;
	parent->ffmpeg_->process_data(buf, len);
}

void live_transcoder::ffmpeg_callback(const boost::uint8_t* data, boost::uint32_t len)
{
    if (cb_ != NULL)
        cb_(data, len, user_param_);
}

} // dvblex
