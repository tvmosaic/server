/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <dl_logger.h>
#include <dl_message_playback.h>
#include "playback_item_streamer.h"
#include "mime_types.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging::playback;

using std::min;

namespace dvblex {

using namespace playback;

playback_item_streamer_t::playback_item_streamer_t(boost::shared_ptr<playback::pb_item_t> item, 
        const dvblink::base_id_t source_id,  const std::string& object_id, 
        const dvblink::messaging::i_play_source_control_t playback_interface, 
        const dvblink::messaging::message_queue_t message_queue, const pion::http::response_writer_ptr& response_writer, 
        const pion::http::request_ptr& request) :
    playback_interface_(playback_interface),
    message_queue_(message_queue),
    source_id_(source_id),
    object_id_(object_id),
    response_writer_(response_writer),
    request_(request),
    item_(item),
    play_thread_(NULL) 
{
    //start sending thread
    b_exit_flag_ = false;
    b_cancel_flag_ = false;
    play_thread_ = new boost::thread(boost::bind(&playback_item_streamer_t::playback_thread_func, this));
}

playback_item_streamer_t::~playback_item_streamer_t()
{
    delete play_thread_;
    playback_interface_.reset();
}

bool playback_item_streamer_t::is_finished()
{
    return play_thread_->timed_join(boost::posix_time::milliseconds(10));
}

void playback_item_streamer_t::stop()
{
    b_exit_flag_ = true;
    play_thread_->join();
}

void playback_item_streamer_t::cancel()
{
    b_cancel_flag_ = true;
    play_thread_->join();
}

void playback_item_streamer_t::playback_thread_func()
{
    b_write_error_ = false;

    bool bhead_request = boost::iequals(request_->get_method(), "HEAD");

    const boost::uint32_t read_buf_len = 200*1024;
    unsigned char* read_buf = new unsigned char[read_buf_len];
    //open playback item
    dvblink::object_handle_t object_handle;
    boost::uint64_t item_size_bytes = 0;
    if (open(object_id_, object_handle, item_size_bytes))
    {
	    // set the Content-Type HTTP header using the file's MIME type
        response_writer_->get_response().set_content_type(octet_stream_mime_code); //generic one
        if (item_.get() != NULL && (item_->item_type_ == pbit_item_recorded_tv || item_->item_type_ == pbit_item_video))
	        response_writer_->get_response().set_content_type(mpeg_mime_code);
        if (item_.get() != NULL && item_->item_type_ == pbit_item_image)
	        response_writer_->get_response().set_content_type(jpeg_mime_code);

        //set last modified date to the beginning of time (we do not know it anyway)
        response_writer_->get_response().set_last_modified(0);
        //server date/time
        time_t now;
        time(&now);
        response_writer_->get_response().change_header("DATE", pion::http::types::get_date_string(now));

	    // use "200 OK" HTTP response status message
	    response_writer_->get_response().set_status_message(pion::http::types::RESPONSE_MESSAGE_OK);
        //indicate that we accept range requests
        response_writer_->get_response().change_header("Accept-Ranges", "bytes");
        response_writer_->get_response().set_do_not_send_content_length();

        boost::uint64_t start_offset = 0;
        boost::uint64_t read_range = 0;
        if (get_read_range(start_offset, read_range))
        {
            seek(object_handle, start_offset);

            if (start_offset == 0 && read_range == 0) //Range: 0-
            {
                //set 200 response code (all content is served in one go)
	            response_writer_->get_response().set_status_code(pion::http::types::RESPONSE_CODE_OK);
            } else
            {
                //if seek was requested - set 206 response code (partial content)
                response_writer_->get_response().set_status_code(206);
            }
        } else
        {
            //set 200 response code (no seek or range arguments)
	        response_writer_->get_response().set_status_code(pion::http::types::RESPONSE_CODE_OK);
        }
        //find out the number of bytes to read
        boost::uint64_t bytes_available = item_size_bytes > 0 ? item_size_bytes - start_offset : 0;
        if (read_range == 0)
        {
            read_range = bytes_available;
        } else
        {
            read_range = bytes_available > 0 ? min(read_range, bytes_available) : read_range;
        }

        log_info(L"playback_item_streamer_t::playback_thread_func: reading starting at %1%, number of bytes %2%, HEAD: %3%") % start_offset % read_range % bhead_request;

        //set content-length header
        if (read_range > 0)
        {
            response_writer_->get_response().change_header("CONTENT-LENGTH", boost::lexical_cast<std::string>(read_range));
            if (bytes_available > 0 && item_size_bytes > 0)
            {
                std::stringstream strbuf;
                strbuf << "bytes " << start_offset << "-" << start_offset + read_range - 1 << "/" << item_size_bytes;
                response_writer_->get_response().change_header("CONTENT-RANGE", strbuf.str());
            }
        }
        else
        {
            response_writer_->get_response().change_header("CONTENT-LENGTH", "*");
        }

        if (!bhead_request)
        {
            boost::uint64_t bytes_read = 0;

            while (!b_exit_flag_ && !b_cancel_flag_)
            {
                boost::uint32_t len = read_range > 0 ? min(static_cast<boost::uint64_t>(read_buf_len), read_range - bytes_read) : read_buf_len;

                bool b = playback_interface_->read_data(object_handle.get().c_str(), read_buf, len);
                if (!b || len == 0)
                {
                    //error while reading (no data anymore?)
                    break;
                }

                bytes_read += len;

                //send data
                buffer_processed_.reset();

		        response_writer_->write_no_copy(read_buf, len);
		        response_writer_->send(boost::bind(&playback_item_streamer_t::handle_write, this, 
			        boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

                //wait for buffer to be sent to a client
                buffer_processed_.wait();

			    response_writer_->clear();
                //if there was an error while sending data - break.
                if (b_write_error_)
                    break;
                //if all required data was sent - send a response with zero length and break afterwards
                if (read_range > 0 && bytes_read >= read_range)
                {
                    response_writer_->send();
                    break;
                }
            }
        } else
        {
            //send headers without data for HEAD request
		    response_writer_->send();
        }

        if (!b_cancel_flag_) //do not close object if cancelled
            close(object_handle);
    } else
    {
        log_error(L"playback_item_streamer_t::playback_thread_func. failed to open %1%") % string_cast<EC_UTF8>(object_id_);
        //send error response to a client
		response_writer_->get_response().set_status_code(pion::http::types::RESPONSE_CODE_NOT_FOUND);
		response_writer_->get_response().set_status_message(pion::http::types::RESPONSE_MESSAGE_NOT_FOUND);
		response_writer_->send();
    }

    delete read_buf;

	//close http connection
    response_writer_->get_connection()->finish();
}

void playback_item_streamer_t::handle_write(const boost::system::error_code& write_error, std::size_t /*bytes_written*/)
{
	if (write_error)
	{
        b_write_error_ = true;
        log_info(L"playback_item_streamer_t::handle_write: http write error. connection will be closed");
	}
	buffer_processed_.signal();
}

bool playback_item_streamer_t::open(std::string& object_id, dvblink::object_handle_t& object_handle, boost::uint64_t& item_size_bytes)
{
    bool ret_val = false;

    open_item_request req;
    open_item_response resp;
    req.object_id_ = object_id;
    messaging::message_error err = message_queue_->send(playback_interface_->get_uid(), req, resp);
    if (err == messaging::success && resp.result_)
    {
        object_handle = resp.handle_;
        item_size_bytes = resp.item_size_bytes_;
        ret_val = true;
    }

    return ret_val;
}

void playback_item_streamer_t::close(dvblink::object_handle_t& object_handle)
{
    close_item_request req;
    close_item_response resp;
    req.handle_ = object_handle;
    /*message_error err = */message_queue_->send(playback_interface_->get_uid(), req, resp);
}

bool playback_item_streamer_t::get_read_range(boost::uint64_t& seek_position, boost::uint64_t& bytes_to_read)
{
    seek_position = 0;
    bytes_to_read = 0;
    bool ret_val = false;

    std::string val = request_->get_header("Range");
    if (val.size() > 0)
    {
        //find '='
        std::string::size_type pos = val.find('=');
        if (pos != std::string::npos)
        {
            std::string range_val = val.substr(pos+1, val.size() - pos - 1);
            //find '-'
            pos = range_val.find('-');
            if (pos != std::string::npos)
            {
                std::string start_val = range_val.substr(0, pos);
                try
                {
                    seek_position = boost::lexical_cast<boost::uint64_t>(start_val);
                } catch(...)
                {
                    seek_position = 0;
                }
                std::string read_val = range_val.substr(pos+1, range_val.size() - pos - 1);
                if (read_val.size() > 0)
                {
                    try
                    {
                        bytes_to_read = boost::lexical_cast<boost::uint64_t>(read_val);
                    } catch(...)
                    {
                        bytes_to_read = 0;
                    }
                    if (bytes_to_read > 0 && bytes_to_read > seek_position)
                        bytes_to_read = bytes_to_read - seek_position + 1;
                }
                ret_val = true;
                log_info(L"playback_item_streamer_t::get_start_offset: Seek position %1%, bytes to read %2%") % seek_position % bytes_to_read;
            }
        }
    }

    return ret_val;
}

void playback_item_streamer_t::seek(dvblink::object_handle_t& object_handle, boost::uint64_t offset)
{
    seek_item_request req;
    seek_item_response resp;
    req.handle_ = object_handle;
    req.offset_ = offset;
    /*message_error err = */message_queue_->send(playback_interface_->get_uid(), req, resp);
}

} // dvblex
