/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp> 
#include <algorithm>
#include <dl_utils.h>
#include <dl_ts_info.h>
#include "constants.h"
#include "mime_types.h"
#include "memory_buffer_sender.h"
#include "live_transcoder.h"
#include "playback_transcoder.h"
#include "iphone_segmentor_streamer.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

#ifdef AFTER_SEGMENTOR_WRITE_FILE
#define FILE_NAME "c:/data_after_segmentor.ts"
#endif

static const size_t max_allowed_segment_number         = 2;

namespace dvblex {

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

iphone_segmentor::iphone_segmentor(const std::string& id, const dvblink::network_port_t& server_streaming_port, const dvblink::url_address_t& server_ip, 
        stream_processor_base* processor, unsigned int segment_duration_sec, size_t segment_first_num) :
    network_streamer_base_t(id, processor),
    server_ip_(server_ip),
	server_streaming_port_(server_streaming_port),
    segment_duration_sec_(segment_duration_sec),
    segment_duration_(segment_duration_sec * one_second),
    prev_pts_(-1),
    bandwidth_(0),
    audio_bandwidth_(128000),
    segment_first_cnt_(0),
    segment_cnt_(segment_first_cnt_),
    last_checked_segment_cnt_(segment_cnt_),
    segment_first_num_(segment_first_num),
    max_segment_size_(0),
    quit_(false),
    stream_checker_(NULL)
#ifdef AFTER_SEGMENTOR_WRITE_FILE
    ,file_(NULL)
#endif
{

#ifdef AFTER_SEGMENTOR_WRITE_FILE
    file_ = fopen(FILE_NAME, "w+b");
    if (file_)
    {
        setbuf(file_, m_buffer);
    }
#endif

/*
    //Hack! :( Maybe move the settings to storage xml file?
    filesystem_path_t fpath = ns_cluster::get_instance()->disk_path();
    fpath /= L"audio_only.xml";
    b_do_audio_only_ = boost::filesystem::exists(fpath.get());
*/
    b_do_audio_only_ = false;

    //cast processor to live_transcoder
    live_transcoder* it = dynamic_cast<live_transcoder*>(processor);
    if (it != NULL)
    {
        bandwidth_ = it->get_bitrate() * 1024 + audio_bandwidth_; //round up for audio bitrate extra
    } else
    {
        //cast processor to playback_transcoder
        playback_transcoder* pt = dynamic_cast<playback_transcoder*>(processor);
        if (pt != NULL)
            bandwidth_ = pt->get_bitrate() * 1024 + audio_bandwidth_; //round up for audio bitrate extra
    }

    if (bandwidth_ == 0)
        bandwidth_ = 512000;

    bandwidth_ = bandwidth_ * 1.1;

    //these are default PIDs of ffmpeg (may be different later! Check!)
    pmt_pid_ = 0x1000;
    video_pid_ = 0x100;

    pmt_parser_.Init(pmt_pid_);
}

iphone_segmentor::~iphone_segmentor()
{
    stop_streaming();

#ifdef AFTER_SEGMENTOR_WRITE_FILE
    fclose(file_);
#endif
}

std::string iphone_segmentor::get_streaming_url(const dvblink::channel_id_t& channel_id, const std::string& protocol_header, const std::string& server_address, 
    boost::uint16_t streaming_port, int handle)
{
    std::stringstream url;
	url << protocol_header << "://" << server_address << ":" << streaming_port << service_iphone_path << top_m3u8_file_name_prefix << handle << m3u8_file_name_suffix;
    return url.str();
}

bool iphone_segmentor::start(streamer_callbacks_t* cb)
{
    network_streamer_base_t::start(cb);

    return start_streaming();
}

bool iphone_segmentor::start_streaming()
{
    quit_ = false;
    client_counter_.reset();
    
    last_checked_segment_cnt_ = segment_cnt_ = segment_first_cnt_ = segment_first_num_;

    prev_pts_ = -1;

    // start stream watchdog timer
    // first timeout (30 sec) with reserve for turn-on transients
    stream_checker_ = new timer_procedure<iphone_segmentor>(&iphone_segmentor::check_stream_timer_procedure, *this, 30 * 1000);
    return true;
}

void iphone_segmentor::stop_streaming()
{
    //stop incoming stream flow
    stop_processor();

    delete stream_checker_;
    stream_checker_ = NULL;

    //bool res = transcoder_->Stop();

    lock_.lock();
    quit_ = true;
	while (client_counter_.get_counter() > 0)
    {
        lock_.unlock();
        dvblink::engine::sleep(100);
        lock_.lock();
    }

    map_segment_name_data_.clear();
    cur_segment_.reset();

    lock_.unlock();
}

void iphone_segmentor::signal_exit()
{
    //thios function should unblock any blocking functions as a preparation for exit/deletion
    processor_->signal_exit();
    quit_ = true;
}

void iphone_segmentor::check_stream_timer_procedure(const boost::system::error_code& e)
{
    if (e != boost::asio::error::operation_aborted)
    {
        boost::mutex::scoped_lock lock(lock_);

        bool stream_flow_ok = true;
        if (last_checked_segment_cnt_ == segment_cnt_)
        {
            stream_flow_ok = false;
            log_info(L"iphone_segmentor::check_stream_timer_procedure: PROBLEMS WITH STREAM FLOW; STOP TRANSCODING");
        }
        else
        {
            last_checked_segment_cnt_ = segment_cnt_;
        }

        bool allowed_segment_number = map_segment_name_data_.size() <= max_allowed_segment_number + 1;
        if (!allowed_segment_number)
            log_info(L"iphone_segmentor::check_stream_timer_procedure: segment number %1% EXCEEDED ALLOWED QUANTITY %2%") % map_segment_name_data_.size() % max_allowed_segment_number;

        //if any of the errors happened, report it to streamer manager
        if (!allowed_segment_number || !stream_flow_ok)
            if (callbacks_ != NULL)
                callbacks_->streamer_disconnected(get_id());
    }
    else
    {
        dvblink::logging::log_ext_info(L"iphone_segmentor::check_stream_timer_procedure - aborted");
    }
}

void iphone_segmentor::on_received_stream(const unsigned char* buffer, size_t length)
{
//#ifdef AFTER_SEGMENTOR_WRITE_FILE
//    fwrite(buffer, 1, length, file_);
//#endif

    __int64 pts;
    for (size_t i = 0; i < length / TS_PACKET_SIZE; i++)
    {
        pts = ts_processor_.GetPTSValue(buffer + i * TS_PACKET_SIZE);
        if (pts != -1)
        {
            if (prev_pts_ == -1)
            {
                prev_pts_ = pts;
            }
            else
            {
                if (pts - prev_pts_ > segment_duration_)
                {
                    dvblink::logging::log_info(L"iphone_segmentor::callback: DeltaPTS > segment_duration");

                    boost::mutex::scoped_lock lock(lock_);
                    if (cur_segment_)
                    {
                        unsigned long segment_id = segment_cnt_++;
                        
                        cur_segment_->set_id(segment_id);
                        map_segment_name_data_.insert(std::make_pair(segment_id, cur_segment_));

                        log_ext_info(L"iphone_segmentor::send_stream: segment %1% has been put into the ready_list") % segment_id;

                        if (max_segment_size_ < cur_segment_->size())
                        {
                            max_segment_size_ = cur_segment_->size();
                        }

#ifdef AFTER_SEGMENTOR_WRITE_FILE
                        fwrite(&((*cur_segment_.get())[0]), 1, cur_segment_->size(), file_);
#endif
                        cur_segment_.reset();
                        cur_segment_ = get_segment();
                    }
                    else
                    {
                        log_ext_info(L"iphone_segmentor::send_stream: cur_segment_ = NULL");
                    }

                    prev_pts_ = pts;
                }
            }
        }

//#ifdef AFTER_SEGMENTOR_WRITE_FILE
//        fwrite(buffer + i * TS_PACKET_SIZE, 1, TS_PACKET_SIZE, file_);
//#endif
        add_data(buffer + i * TS_PACKET_SIZE, TS_PACKET_SIZE);
    }
}

iphone_segmentor::segment_t iphone_segmentor::get_segment()
{
    if (!cur_segment_)
    {
        if (free_segment_list_.size())
        {
            cur_segment_ = free_segment_list_.front();
            free_segment_list_.erase(free_segment_list_.begin());
        }
        else
        {
            cur_segment_ = segment_t(new segment);
        }
        cur_segment_->reserve(max_segment_size_);
    }
    return cur_segment_;
}

bool iphone_segmentor::get_m3u8_list(const std::string& protocol_header, int sid, std::string& m3u8, bool audio_only)
{
    boost::mutex::scoped_lock lock(lock_);

    std::ostringstream list_stream;
    list_stream << "#EXTM3U\n" << "#EXT-X-TARGETDURATION:" << segment_duration_sec_ << "\n#EXT-X-MEDIA-SEQUENCE:" << segment_first_cnt_ << "\n";

    for (unsigned int i = 0; i <= map_segment_name_data_.size() + 2; i++)
    {
        if (audio_only)
        {
            list_stream << "#EXTINF:" << segment_duration_sec_ << ",\n" <<
                protocol_header << "://" << server_ip_.get() << ":" << server_streaming_port_.get() << stream_service_resource_url"/" << segment_audio_name_prefix << sid << "-" << i + segment_first_cnt_ << segment_name_suffix << "\n";
        } else
        {
            list_stream << "#EXTINF:" << segment_duration_sec_ << ",\n" <<
                protocol_header << "://" << server_ip_.get() << ":" << server_streaming_port_.get() << stream_service_resource_url"/" << segment_name_prefix << sid << "-" << i + segment_first_cnt_ << segment_name_suffix << "\n";
        }
    }

    //
    // list is without tag #EXT-X-ENDLIST because it's endless
    //

    m3u8 = list_stream.str();

    return true;
}

bool iphone_segmentor::get_top_level_m3u8_list(const std::string& protocol_header, int sid, std::string& m3u8)
{
    boost::mutex::scoped_lock lock(lock_);

    std::ostringstream list_stream;
    list_stream << "#EXTM3U\n" << "#EXT-X-STREAM-INF:BANDWIDTH=" << bandwidth_ << ",CODECS=\"avc1.42001e,mp4a.40.2\"\n";
	list_stream << protocol_header << "://" << server_ip_.get() << ":" << server_streaming_port_.get() << service_iphone_path << m3u8_file_name_prefix << sid << m3u8_file_name_suffix << "\n";
    if (b_do_audio_only_)
    {
        list_stream << "#EXT-X-STREAM-INF:BANDWIDTH=" << audio_bandwidth_ << ",CODECS=\"mp4a.40.2\"\n";
	    list_stream << protocol_header << "://" << server_ip_.get() << ":" << server_streaming_port_.get() << service_iphone_path << audio_m3u8_file_name_prefix << sid << m3u8_file_name_suffix << "\n";
    }

    m3u8 = list_stream.str();

    return true;
}

class counter_holder
{
public:
	counter_holder(client_counter* cc)
	{
		cc_ = cc;
		cc_->increase();
	}

	~counter_holder()
	{
		cc_->decrease();
	}
protected:
	client_counter* cc_;
};

iphone_segmentor::segment_t iphone_segmentor::get_segment(unsigned long segment_id, bool audio_only)
{
    segment_t res;

	counter_holder ch(&client_counter_);

    log_ext_info(L"iphone_segmentor::get_segment: segment #%1%") % segment_id;
    lock_.lock();

    if (segment_id >= segment_first_cnt_ && segment_id <= segment_cnt_ + 3)
    {
        const long wait_step = 500;
        const long wait_period = static_cast<long>(segment_duration_sec_ * 1000 * 2.0);
        long period_cnt = 0;

		if (segment_first_cnt_ == 0)
		{
			//wait until there are at least two segments in the queue
			while (!quit_ && get_segments_num() < 2)
			{
				lock_.unlock();
				dvblink::engine::sleep(wait_step);
				lock_.lock();
			}
		}

		map_segment_name_data_iter_t iter = map_segment_name_data_.find(segment_id);
		while (!quit_ && iter == map_segment_name_data_.end() && period_cnt < wait_period)
		{
			lock_.unlock();
			dvblink::engine::sleep(wait_step);
			period_cnt += wait_step;

			lock_.lock();
			iter = map_segment_name_data_.find(segment_id);
		}

		if (iter != map_segment_name_data_.end())
		{
			res = iter->second;
		}

		if (period_cnt >= wait_period)
		{
			log_ext_info(L"iphone_segmentor::get_segment: TIME PERIOD EXCEEDED: %1% ms") % period_cnt;
		}
		else
		if (quit_)
		{
			log_ext_info(L"iphone_segmentor::get_segment: QUIT signal");
		}
		else
		{
			log_ext_info(L"iphone_segmentor::get_segment: segment %1% is READY FOR SENDING") % segment_id;
		}
	}
    else
    {
        log_ext_info(L"iphone_segmentor::get_segment: WRONG segment %1% required") % segment_id;
    }

    lock_.unlock();

    if (res)
    {
        if (audio_only)
        {
			//make a copy of the segment as it will be modified
            segment_t copy_segment = segment_t(new segment);
			copy_segment->copy(res->data(), res->id());
			res = copy_segment;

            std::vector<char>* data = &res->data();
            unsigned int pos = 0;
            while (pos < data->size())
            {
                //update PMT packets
                ts_payload_parser::ts_section_list found_sections;
                if (pmt_parser_.AddPacket((const unsigned char*)&(data->at(0)) + pos, TS_PACKET_SIZE, found_sections) > 0)
                {
                    ts_process_routines::SetPMTStreamType(found_sections[0].section, found_sections[0].length, video_pid_, USER_PRIVATE_DESCRIPTOR); //set "some" private stream type
                    ts_process_routines::ChangePMTSectionStreamPID(found_sections[0].section, found_sections[0].length, video_pid_, NULL_PACKET_PID);
                    //write section back to the ts packet (assuming that it only takes a single packet!)
                    memcpy(&(data->at(0)) + pos + 5, found_sections[0].section, std::min(TS_PACKET_SIZE - 5, found_sections[0].length));
                    pmt_parser_.ResetFoundSections(found_sections);
                }
                if (ts_process_routines::GetPacketPID((const unsigned char*)&(data->at(0)) + pos) == NULL_PACKET_PID)
                {
                    //remove this packet
                    data->erase(data->begin() + pos, data->begin() + pos + TS_PACKET_SIZE);
                } else
                {
                    //remove non-PCR video packets
                    boost::uint64_t pcr;
                    if (ts_process_routines::GetPacketPID((const unsigned char*)&(data->at(0)) + pos) == video_pid_ &&
                        !ts_process_routines::GetPCRValue(&(data->at(0)) + pos, pcr))
                    {
                        //remove this packet
                        data->erase(data->begin() + pos, data->begin() + pos + TS_PACKET_SIZE);
                    } else
                    {
                        pos += TS_PACKET_SIZE;
                    }
                }
            }
        }
        log_ext_info(L"iphone_segmentor::get_segment: segment %1% returned, size %2%") % segment_id % res.get()->size();
    }

    return res;
}

void iphone_segmentor::free_segment(segment_t segment)
{
    boost::mutex::scoped_lock lock(lock_);

    // this function frees always previous segment
    //  current segment can be requested by client again
    size_t cur_id = segment->id();
    if (cur_id > 0)
    {
        for (size_t i = segment_first_cnt_; i < cur_id; i++)
        {
            size_t prev_id = i;
            map_segment_name_data_iter_t iter_prev = map_segment_name_data_.find(prev_id);
            if (iter_prev != map_segment_name_data_.end())
            {
                segment_t prev_segment = iter_prev->second;
                map_segment_name_data_.erase(iter_prev);

                prev_segment->clear();
                free_segment_list_.push_back(prev_segment);
            }
        }
        segment_first_cnt_ = cur_id;
    }
}

void iphone_segmentor::add_data(const boost::uint8_t* data, size_t len)
{
    boost::mutex::scoped_lock lock(lock_);

    segment_t segment = get_segment();
    assert(segment);

//#ifdef AFTER_SEGMENTOR_WRITE_FILE
//    fwrite(data, 1, len, file_);
//#endif
    segment->insert_to_tail(data, data + len);
}

bool iphone_segmentor::is_valid_segment_id(size_t segment_id)
{
    return (segment_id >= segment_first_cnt_ && segment_id <= segment_cnt_ + 1);
}

size_t iphone_segmentor::get_segments_num()
{
    return (segment_cnt_ - segment_first_cnt_);
}

bool iphone_segmentor::process_segment_request(const pion::http::request_ptr& request, const pion::tcp::connection_ptr& tcp_conn, boost::shared_ptr<iphone_segmentor> segmentor,
                                               unsigned long segment_id, bool audio_only)
{
    bool res = false;

	if (segment_t segment = get_segment(segment_id, audio_only))
	{
		boost::shared_ptr<memory_buffer_sender> sender_ptr(memory_buffer_sender::Create(segmentor, segment, ts_data_mime_code, request, tcp_conn));
		sender_ptr->send();
		res = true;
	} else
    {
        log_error(L"iphone_segmentor::process_segment_request. Segment %1% does not exist (%2%)") % segment_id % audio_only;
    }
	return res;
}

bool iphone_segmentor::process_m3u8_request(const std::string& protocol_header, const pion::http::request_ptr& request, const pion::tcp::connection_ptr& tcp_conn, int sid, m3u8_list_type_e list_type)
{
    bool res = false;

	std::string m3u8_list_content;
    switch (list_type)
    {
    case mlt_top:
	    res = get_top_level_m3u8_list(protocol_header, sid, m3u8_list_content);
        break;
    case mlt_normal:
	    res = get_m3u8_list(protocol_header, sid, m3u8_list_content, false);
        break;
    case mlt_audio:
	    res = get_m3u8_list(protocol_header, sid, m3u8_list_content, true);
        break;
    default:
        log_error(L"iphone_segmentor::process_m3u8_request. unknown m3u8 type");
        res = false;
        break;
    }

	if (res)
	{
		log_ext_info(L"iphone_segmentor::process_m3u8_request:\n%1%") % string_cast<EC_UTF8>(m3u8_list_content);

		boost::shared_ptr<memory_buffer_sender> sender_ptr(memory_buffer_sender::Create(
            (const unsigned char*)&m3u8_list_content[0], m3u8_list_content.size(), m3u8_mime_code, request, tcp_conn));
		sender_ptr->send();
	}

    return res;
}

bool iphone_segmentor::is_m3u8_request(const pion::http::request_ptr& request, int& sid, m3u8_list_type_e& list_type)
{
    boost::xpressive::sregex session_m3u8_regex(boost::xpressive::sregex::compile(m3u8_name_mask.c_str()));
    boost::xpressive::sregex session_top_m3u8_regex(boost::xpressive::sregex::compile(top_m3u8_name_mask.c_str()));
    boost::xpressive::sregex session_audio_m3u8_regex(boost::xpressive::sregex::compile(audio_m3u8_name_mask.c_str()));
    boost::xpressive::sregex_token_iterator sregex_end;

    boost::xpressive::sregex_token_iterator cur(request->get_resource().begin(), request->get_resource().end(), session_m3u8_regex, 1);
    if (cur != sregex_end)
    {
        sid = boost::lexical_cast<unsigned long>(*cur);
        list_type = mlt_normal;
        return true;
    }
    boost::xpressive::sregex_token_iterator top_cur(request->get_resource().begin(), request->get_resource().end(), session_top_m3u8_regex, 1);
    if (top_cur != sregex_end)
    {
        sid = boost::lexical_cast<unsigned long>(*top_cur);
        list_type = mlt_top;
        return true;
    }
    boost::xpressive::sregex_token_iterator audio_cur(request->get_resource().begin(), request->get_resource().end(), session_audio_m3u8_regex, 1);
    if (audio_cur != sregex_end)
    {
        sid = boost::lexical_cast<unsigned long>(*audio_cur);
        list_type = mlt_audio;
        return true;
    }
    return false;
}

bool iphone_segmentor::is_segment_request(const pion::http::request_ptr& request, int& sid, unsigned long& segment_id, bool& is_audio_only)
{
    boost::xpressive::sregex session_ts_segment_regex(boost::xpressive::sregex::compile(segment_name_mask.c_str()));
    boost::xpressive::sregex session_ts_audio_segment_regex(boost::xpressive::sregex::compile(segment_audio_name_mask.c_str()));
    boost::xpressive::sregex_token_iterator sregex_end;

    boost::xpressive::sregex_token_iterator cur(request->get_resource().begin(), request->get_resource().end(), session_ts_segment_regex, 1);
    if (cur != sregex_end)
    {
        sid = boost::lexical_cast<int>(*cur);
        is_audio_only = false;
        //get also segment number
        boost::xpressive::sregex_token_iterator segment_cur(request->get_resource().begin(), request->get_resource().end(), session_ts_segment_regex, 2);
        if (segment_cur != sregex_end)
            segment_id = boost::lexical_cast<unsigned long>(*segment_cur);

        return true;
    }
    boost::xpressive::sregex_token_iterator cur_audio(request->get_resource().begin(), request->get_resource().end(), session_ts_audio_segment_regex, 1);
    if (cur_audio != sregex_end)
    {
        sid = boost::lexical_cast<int>(*cur_audio);
        is_audio_only = true;
        //get also segment number
        boost::xpressive::sregex_token_iterator segment_cur(request->get_resource().begin(), request->get_resource().end(), session_ts_audio_segment_regex, 2);
        if (segment_cur != sregex_end)
            segment_id = boost::lexical_cast<unsigned long>(*segment_cur);
        return true;
    }
    return false;
}

} // dvblex
