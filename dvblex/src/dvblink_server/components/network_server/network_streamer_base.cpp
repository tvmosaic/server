/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include "network_streamer_base.h"

using namespace dvblink;
using namespace dvblink::logging;

namespace dvblex {

network_streamer_base_t::network_streamer_base_t(const std::string& id, stream_processor_base* processor) :
    streamer_t(id),
    streamer_service_callbacks_(NULL),
    processor_(processor)
{
    if (processor_ != NULL)
        processor_->init(&network_streamer_base_t::processor_callback, this);
}

network_streamer_base_t::~network_streamer_base_t()
{
    stop_processor();
}

void network_streamer_base_t::stop_processor()
{
    if (processor_ != NULL)
    {
        delete processor_;
        processor_ = NULL;
    }
}

void network_streamer_base_t::set_streamer_service_callbacks(streamer_service_callbacks_t* streamer_service_callbacks)
{
    streamer_service_callbacks_ = streamer_service_callbacks;
}

void network_streamer_base_t::notify_streamer_deleted()
{
    streamer_t::notify_streamer_deleted();

    if (streamer_service_callbacks_ != NULL)
        streamer_service_callbacks_->streamer_stopped(streamer_id_);
}

void network_streamer_base_t::processor_callback(const unsigned char* data, size_t len, void* user_param)
{
    network_streamer_base_t* parent = (network_streamer_base_t*)user_param;
    parent->on_received_stream(data, len);
}

void network_streamer_base_t::write_stream(const unsigned char* data, size_t len)
{
    if (processor_ != NULL)
    {
        processor_->write_stream(data, len);
    } else
    {
        processor_callback(data, len, this);
    }
}

} // dvblex
