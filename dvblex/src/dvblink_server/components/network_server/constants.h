/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>

namespace dvblex {

static const std::string access_control_allow_origin_hdr      = "*";

const std::string network_server_settings_filename       = "network_server_settings.xml";

const std::string timeshift_dir_name              = "c5dcc59f-3776-4576-8276-7035841f872f";

static const std::string stream_type_rtp      = "rtp";
static const std::string stream_type_hls      = "hls";
static const std::string stream_type_asf      = "asf";
static const std::string stream_type_raw_http      = "raw_http";
static const std::string stream_type_webm      = "webm";
static const std::string stream_type_mp4      = "mp4";
static const std::string stream_type_h264ts      = "h264ts";
static const std::string stream_type_h264ts_http_timeshift      = "h264ts_http_timeshift";
static const std::string stream_type_raw_http_timeshift      = "raw_http_timeshift";

static const std::string client_param_name                   = "client";
static const std::string channel_param_name                   = "channel";
static const std::string handle_param_name                   = "handle";
static const std::string sort_mode_param_name                 = "sort";
static const std::string channel_type_param_name                 = "type";

static const std::string sort_mode_param_on_name                 = "name";
static const std::string sort_mode_param_on_number                 = "number";

static const std::string sort_mode_param_tv                 = "tv";
static const std::string sort_mode_param_radio                 = "radio";

#define command_service_resource_url    "/mobile"
#define stream_service_resource_url     "/stream"
#define web_service_resource_url    "/web"

const std::string cmd_service_command_tag              = "command";
const std::string cmd_service_xmlparam_tag              = "xml_param";

static const std::string request_param_transcoder = "transcoder";
static const std::string request_param_transcoder_bitrate = "bitrate";
static const std::string request_param_transcoder_width = "width";
static const std::string request_param_transcoder_height = "height";
static const std::string request_param_transcoder_lng = "lng";
static const std::string request_param_transcoder_scale = "scale";

const boost::uint32_t default_http_port = 9270;
const boost::uint32_t default_discovery_port = 65332;

const std::string default_auth_user              = "";
const std::string default_auth_password              = "";

#if defined(_ANDROID_ALL)
    const boost::uint64_t default_timeshift_buffer_max_size = 800000000;
    const boost::uint64_t timeshift_disk_slack_size = 100*1024*1024;
#else
    const boost::uint64_t default_timeshift_buffer_max_size = 4000000000ULL;
    const boost::uint64_t timeshift_disk_slack_size = 1000000000ULL;
#endif

const std::string service_http_stream_path      = stream_service_resource_url"/";
const std::string http_stream_name_prefix       = "http_stream_";
const std::string http_stream_full_prefix       = service_http_stream_path + http_stream_name_prefix;

const std::string service_direct_http_stream_path      = stream_service_resource_url"/";
const std::string direct_stream_prefix           = "direct";
const std::string direct_stream_full_prefix      = service_direct_http_stream_path + direct_stream_prefix;

const std::string service_timeshift_http_stream_path      = stream_service_resource_url"/";
const std::string timeshift_stream_prefix           = "timeshift";
const std::string timeshift_stream_full_prefix      = service_timeshift_http_stream_path + timeshift_stream_prefix;

const std::string service_iphone_path           = stream_service_resource_url"/";
const std::string m3u8_file_name_prefix         = "tvm_index_";
const std::string top_m3u8_file_name_prefix     = "tvm_top_index_";
const std::string audio_m3u8_file_name_prefix   = "tvm_audio_index_";
const std::string m3u8_file_name_suffix         = ".m3u8";

const std::string segment_name_prefix           = "tvm_segment_";
const std::string segment_audio_name_prefix     = "tvm_audio_segment_";
const std::string segment_name_suffix           = ".ts";

const std::string service_playback_object_path   = stream_service_resource_url"/";
const std::string playback_object_prefix         = "playback";
const std::string playback_object_full_prefix    = service_playback_object_path + playback_object_prefix;

const std::string segment_name_mask             = service_iphone_path + segment_name_prefix + "(\\d+)" + "-(\\d+)" + segment_name_suffix;
const std::string segment_audio_name_mask       = service_iphone_path + segment_audio_name_prefix + "(\\d+)" + "-(\\d+)" + segment_name_suffix;
const std::string m3u8_name_mask                = service_iphone_path + m3u8_file_name_prefix + "(\\d+)" + m3u8_file_name_suffix;
const std::string top_m3u8_name_mask            = service_iphone_path + top_m3u8_file_name_prefix + "(\\d+)" + m3u8_file_name_suffix;
const std::string audio_m3u8_name_mask          = service_iphone_path + audio_m3u8_file_name_prefix + "(\\d+)" + m3u8_file_name_suffix;
const std::string http_stream_name_mask         = service_http_stream_path + http_stream_name_prefix + "(\\d+)";

}
