/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <pion/http/response_writer.hpp>

#include <dl_message_queue.h>
#include <dl_event.h>
#include <dli_playback_source.h>
#include <dl_pb_types.h>

namespace dvblex {

class playback_item_streamer_t
{
public:
    playback_item_streamer_t(boost::shared_ptr<playback::pb_item_t> item, const dvblink::base_id_t source_id, 
        const std::string& object_id, const dvblink::messaging::i_play_source_control_t playback_interface, 
        const dvblink::messaging::message_queue_t message_queue, const pion::http::response_writer_ptr& response_writer, 
        const pion::http::request_ptr& request);
	virtual ~playback_item_streamer_t();

    bool is_finished();
    void stop();
    void cancel();

    dvblink::base_id_t get_source_id(){return source_id_;};

protected:
    dvblink::messaging::i_play_source_control_t playback_interface_;
    dvblink::messaging::message_queue_t message_queue_;
    dvblink::base_id_t source_id_;
    std::string object_id_;
    pion::http::response_writer_ptr response_writer_;
    pion::http::request_ptr request_;
    boost::shared_ptr<playback::pb_item_t> item_;

    boost::thread* play_thread_;
    bool b_exit_flag_;
    bool b_cancel_flag_;
    dvblink::event buffer_processed_;
    bool b_write_error_;

    void playback_thread_func();
    bool open(std::string& object_id, dvblink::object_handle_t& object_handle, boost::uint64_t& item_size_bytes);
    void close(dvblink::object_handle_t& object_handle);
    void seek(dvblink::object_handle_t& object_handle, boost::uint64_t offset);
    void handle_write(const boost::system::error_code& write_error, std::size_t /*bytes_written*/);
    bool get_read_range(boost::uint64_t& seek_position, boost::uint64_t& bytes_to_read);
};

}
