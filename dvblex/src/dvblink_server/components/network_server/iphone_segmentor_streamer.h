/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once
#include <map>
#include <boost/thread/mutex.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <pion/http/response_writer.hpp>
#include <dl_timer_procedure.h>
#include <dl_ts_info.h>
#include "network_streamer_base.h"

//#define AFTER_SEGMENTOR_WRITE_FILE

namespace dvblink { namespace engine {
    template <typename T> class timer_procedure;
}
}

namespace dvblex {

const unsigned int default_segment_duration_sec = 6;

class stream_processor_base;

enum m3u8_list_type_e
{
    mlt_top,
    mlt_normal,
    mlt_audio,
};

class client_counter
{
public:
	client_counter()
	{
		counter_ = 0;
	}

	void reset()
	{
		boost::unique_lock<boost::mutex> lock(lock_);
		counter_ = 0;
	}

	long get_counter()
	{
		boost::unique_lock<boost::mutex> lock(lock_);
		return counter_;
	}

	void increase()
	{
		boost::unique_lock<boost::mutex> lock(lock_);
		counter_++;
	}

	void decrease()
	{
		boost::unique_lock<boost::mutex> lock(lock_);
		counter_--;
	}

protected:
	boost::mutex lock_;
	long volatile counter_;
};

class iphone_segmentor : public network_streamer_base_t
{
    friend class transcoded_playback_provider;
    friend class memory_buffer_sender;

    static const unsigned int one_second = 27000000;

    template <class T>
    struct segment_base
    {
        size_t id() const {return id_;}
        void set_id(size_t id) {id_ = id;}
        void clear() {data_.clear();}
        size_t size() const {return data_.size();}
        void reserve(size_t n) {data_.reserve(n);}
        T& data() {return data_;}

        template <class InputIterator>
        void insert_to_tail(InputIterator first, InputIterator last)
        {
            data_.insert(data_.end(), first, last);
        }

		void copy(const T& data, size_t id)
		{
			data_.clear();
            data_.insert(data_.end(), data.begin(), data.end());

			id_ = id;
		}

    private:
        size_t id_;
        T data_;
    };

    typedef segment_base<std::vector<char> > segment;
    typedef boost::shared_ptr<segment> segment_t;

    typedef std::map<unsigned long, segment_t> map_segment_name_data_t;
    typedef map_segment_name_data_t::iterator map_segment_name_data_iter_t;
    typedef std::vector<segment_t> segment_list;
    typedef segment_list::iterator segment_list_iter_t;

public:
    iphone_segmentor(const std::string& id, const dvblink::network_port_t& server_streaming_port, const dvblink::url_address_t& server_ip, 
        stream_processor_base* processor, unsigned int segment_duration_sec = default_segment_duration_sec, size_t segment_first_num = 0);
    virtual ~iphone_segmentor();

    virtual bool start(dvblink::streamer_callbacks_t* cb);
    virtual void on_received_stream(const unsigned char* data, size_t len);

    static bool is_m3u8_request(const pion::http::request_ptr& request, int& sid, m3u8_list_type_e& list_type);
    static bool is_segment_request(const pion::http::request_ptr& request, int& sid, unsigned long& segment_id, bool& is_audio_only);

    virtual std::string get_streaming_url(const dvblink::channel_id_t& channel_id, const std::string& protocol_header, const std::string& server_address, 
        boost::uint16_t streaming_port, int handle);

    bool process_segment_request(const pion::http::request_ptr& request, const pion::tcp::connection_ptr& tcp_conn, boost::shared_ptr<iphone_segmentor> segmentor,
        unsigned long segment_id, bool audio_only);

    bool process_m3u8_request(const std::string& protocol_header, const pion::http::request_ptr& request, const pion::tcp::connection_ptr& tcp_conn, int sid, m3u8_list_type_e list_type);

    virtual void signal_exit();

private:
    segment_t get_segment();
    void add_data(const boost::uint8_t* data, size_t len);
    bool start_streaming();
    void stop_streaming();
    void check_stream_timer_procedure(const boost::system::error_code& e);

    bool get_m3u8_list(const std::string& protocol_header, int sid, std::string& m3u8, bool audio_only);
    bool get_top_level_m3u8_list(const std::string& protocol_header, int sid, std::string& m3u8);

    segment_t get_segment(unsigned long segment_id, bool audio_only);
    void free_segment(segment_t segment);
    bool is_valid_segment_id(size_t segment_id);
    size_t get_segments_num();

private:
    dvblink::url_address_t server_ip_;
    dvblink::network_port_t server_streaming_port_;
    unsigned int segment_duration_sec_;
    unsigned int segment_duration_;
    dvblink::engine::ts_process_routines ts_processor_;
    __int64 prev_pts_;
    unsigned long bandwidth_;
    unsigned long audio_bandwidth_;
	dvblink::engine::ts_section_payload_parser pmt_parser_;
    unsigned short pmt_pid_;
    unsigned short video_pid_;
    bool b_do_audio_only_;

    size_t segment_first_cnt_, segment_cnt_, last_checked_segment_cnt_, segment_first_num_;
    size_t max_segment_size_;

    boost::mutex lock_;
    segment_t cur_segment_;
    map_segment_name_data_t map_segment_name_data_;
    segment_list free_segment_list_;

    volatile bool quit_;
	client_counter client_counter_;

    dvblink::engine::timer_procedure<iphone_segmentor>* stream_checker_;

#ifdef AFTER_SEGMENTOR_WRITE_FILE
    FILE* file_;
    char m_buffer[BUFSIZ * 10];
#endif
};

} // dvblex
