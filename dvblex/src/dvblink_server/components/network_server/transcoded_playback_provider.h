/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <pion/http/response_writer.hpp>

#include <dl_message_queue.h>
#include <dl_event.h>
#include <dl_ts_aligner.h>
#include <dl_ts_proc.h>
#include <dl_pb_types.h>
#include <dli_playback_source.h>

#include "iphone_segmentor_streamer.h"
#include <tc_types.h>

namespace dvblex {

class transcoded_playback_provider
{
public:
    transcoded_playback_provider(boost::shared_ptr<playback::pb_item_t> item, const dvblink::base_id_t source_id, const std::string& object_id, 
        const dvblink::messaging::i_play_source_control_t playback_interface, const dvblink::messaging::message_queue_t message_queue, 
        dvblink::transcoder::transcoder_params& transcoder_params, size_t start_segment_num, size_t segment_duration);
    
    ~transcoded_playback_provider();
    
    bool is_valid_transcoder_params(dvblink::transcoder::transcoder_params& transcoder_params);
    bool is_valid_segment_num(size_t segment_num);
    bool process_segment_request(const pion::http::request_ptr& request, pion::http::response_writer_ptr& writer, size_t segment_num);
    dvblink::base_id_t get_source_id(){return source_id_;};
    bool is_finished();
    
    void cancel();
    void stop();
    static bool get_transcoder_params_from_http_request(dvblink::transcoder::transcoder_params& transcoder_params, const pion::http::request_ptr& request);
    
protected:
    void playback_thread_func();
    void seek(dvblink::object_handle_t& object_handle, boost::uint64_t offset);
    void close(dvblink::object_handle_t& object_handle);
    bool open(std::string& object_id, dvblink::object_handle_t& object_handle, boost::uint64_t& item_size_bytes);
    boost::uint64_t get_start_offset();
    static void stream_preproc_cb(const unsigned char* buf, int len, void* param);
    static void __stdcall aligner_callback(const unsigned char* buf, unsigned long len, void* user_param);
    boost::uint64_t get_time_stamp_offset();

    boost::shared_ptr<iphone_segmentor> segmentor_;
    
    boost::thread* play_thread_;
    bool b_exit_flag_;
    bool b_cancel_flag_;
    dvblink::messaging::i_play_source_control_t playback_interface_;
    dvblink::messaging::message_queue_t message_queue_;
    dvblink::base_id_t source_id_;
    std::string object_id_;
    boost::shared_ptr<playback::pb_item_t> item_;
    dvblink::transcoder::transcoder_params transcoder_params_;
    time_t last_request_time_;
    time_t inactivity_timeout_;
    size_t segment_duration_;
    size_t segment_num_threshold_;
    size_t start_segment_num_;
	dvblink::engine::ts_packet_aligner packet_aligner_;
};

typedef boost::shared_ptr<transcoded_playback_provider> transcoded_playback_provider_t;

} // dvblex
