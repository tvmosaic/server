/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp> 
#include <pion/http/response_writer.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_status.h>
#include <dl_commands.h>
#include <dl_message_recorder.h>
#include <dl_message_addresses.h>
#include <dl_url_encoding.h>
#include <dl_xml_serialization.h>
#include <dl_xml_command.h>
#include "constants.h"
#include "pion_helper.h"
#include "command_service.h"

#ifdef _MSC_VER
#pragma warning (disable : 4100)
#endif

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void command_service::process_get_recordings_cmd(std::string& xml_response)
{
    recorder::get_recordings_request req;
    recorder::get_recordings_response resp;
    if (message_queue_->send(recorder_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        write_to_xml(command_response_t(DvbLink_StatusOk, &resp.recordings_.get()), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_add_schedule_cmd(const std::string& request, std::string& xml_response)
{
    recorder::add_schedule_request req(request);
    recorder::add_schedule_response resp;
    if (message_queue_->send(recorder_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_search_epg_cmd(const std::string& request, const std::string& server_address, std::string& xml_response)
{
    recorder::search_epg_request req(request, server_address, network_port_t(stream_service_->get_streaming_port()));
    recorder::search_epg_response resp;
    if (message_queue_->send(recorder_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        write_to_xml(command_response_t(DvbLink_StatusOk, &(resp.epg_.get())), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_repair_database_cmd(std::string& xml_response)
{
    recorder::repair_database_request req;
    recorder::repair_database_response resp;
    if (message_queue_->send(recorder_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_get_recording_settings_cmd(std::string& xml_response)
{
    recorder::get_recording_options_request req;
    recorder::get_recording_options_response resp;
    if (message_queue_->send(recorder_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        write_to_xml(command_response_t(DvbLink_StatusOk, &resp.options_.get()), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_set_recording_settings_cmd(const std::string& request, std::string& xml_response)
{
    recorder::set_recording_options_request req(request);
    recorder::set_recording_options_response resp;
    if (message_queue_->send(recorder_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_get_schedules_cmd(std::string& xml_response)
{
    recorder::get_schedule_request req;
    recorder::get_schedule_response resp;
    if (message_queue_->send(recorder_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        write_to_xml(command_response_t(DvbLink_StatusOk, &resp.schedule_.get()), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_update_schedule_cmd(const std::string& request, std::string& xml_response)
{
    recorder::update_schedule_request req(request);
    recorder::update_schedule_response resp;
    if (message_queue_->send(recorder_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_remove_schedule_cmd(const std::string& request, std::string& xml_response)
{
    recorder::remove_schedule_request req(request);
    recorder::remove_schedule_response resp;
    if (message_queue_->send(recorder_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_remove_recording_cmd(const std::string& request, std::string& xml_response)
{
    recorder::remove_recording_request req(request);
    recorder::remove_recording_response resp;
    if (message_queue_->send(recorder_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

} // dvblink
