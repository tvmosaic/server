/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning (disable : 4100)
#pragma warning (disable : 4244)
#endif

#include <pion/http/plugin_service.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include <boost/cstdint.hpp>
#include <dli_server.h>
#include <dl_message_queue.h>
#include "stream_service.h"
#include "web/web_request_handler.h"

namespace dvblex {

class web_service : public http_server_type
{

    typedef std::map<std::string, std::string> ext_to_mime_type_map_t;

    typedef std::map<std::string, web_request_handler_obj_t> web_service_handler_map_t;

public:
    web_service(dvblink::i_server_t server, dvblink::messaging::message_queue_t message_queue, unsigned short port, stream_service* stream_service, network_server_settings_t* settings);
    virtual ~web_service();

    virtual void operator()(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn);

    virtual bool is_streaming_service(){return false;}

private:
    dvblink::messaging::message_queue_t message_queue_;
    dvblink::i_server_t server_;
    unsigned short port_;
    stream_service* stream_service_;
    network_server_settings_t* settings_;
    ext_to_mime_type_map_t ext_to_mime_type_map_;
    dvblink::filesystem_path_t web_file_path_;
    dvblink::filesystem_path_t web_root_file_path_;
    web_service_handler_map_t handlers_;

    void process_accept_language_header(const pion::http::request_ptr& http_request_ptr);
    void load_mime_types();
    void build_handlers_map();
    void send_error_response(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, ::boost::uint32_t code, const std::string& msg);
    void send_binary_response(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, const std::vector<char>* data, 
        const std::string& mime, bool add_gzip_header);
};

class web_service_https : public web_service
{
public:
    web_service_https(dvblink::i_server_t server, dvblink::messaging::message_queue_t message_queue, unsigned short port, stream_service* stream_service, network_server_settings_t* settings)
        : web_service(server, message_queue, port, stream_service, settings)
    {}

    virtual bool is_https(){return true;}
};

} // dvblex
