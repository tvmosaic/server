/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <pion/http/response_writer.hpp>

#include <dli_server.h>
#include <dl_message_playback.h>
#include <dl_message_common.h>
#include <dl_message_queue.h>
#include <dl_pb_types.h>
#include "transcoded_playback_provider.h"
#include "playback_item_streamer.h"
#include "settings.h"

namespace dvblex {

class playback_objects_t
{
    class message_handler : 
        public dvblink::messaging::release_me_request::subscriber
    {
    public:
        message_handler(playback_objects_t* parent, dvblink::messaging::message_queue_t message_queue);
        ~message_handler();

    protected:
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::release_me_request& request, dvblink::messaging::release_me_response& response);

    private:
        playback_objects_t* parent_;
        dvblink::messaging::message_queue_t message_queue_;
    };

public:
    playback_objects_t();
    ~playback_objects_t();

    bool init(network_server_settings_t* settings, const dvblink::i_server_t& server);
    void term();

    bool start_object_streaming(const std::string& protocol_header, const std::string& object_id, const pion::http::request_ptr& request, const pion::tcp::connection_ptr& tcp_conn);
    bool is_playback_object_request(const pion::http::request_ptr& request, std::string& object_id);

protected:
    void invalidate_clients(const dvblink::base_id_t source_id);
    bool get_playback_item(dvblink::object_id_t object_id, boost::shared_ptr<playback::pb_item_t>& item);
    void client_watchdog_thread_func();
    bool generate_m3u8_file(const std::string& protocol_header, const std::string& object_id_str, pion::http::response_writer_ptr writer, pion::http::request_ptr request);
    bool process_transcoded_segment_request(const std::string& object_id_str, const std::string& source_id, pion::http::response_writer_ptr writer, pion::http::request_ptr request);

    dvblink::messaging::message_queue_t message_queue_;
    std::auto_ptr<message_handler> message_handler_;
    bool b_watchdog_exit_;
    boost::thread* client_watchdog_thread_;
    dvblink::i_server_t server_;
    network_server_settings_t* settings_;

    boost::shared_mutex clients_lock_;
    std::vector<playback_item_streamer_t*> clients_list_;
    
    typedef std::map<std::string, transcoded_playback_provider_t> transcoded_clients_map_t;
    transcoded_clients_map_t transcoded_clients_list_;
};

} // dvblex
