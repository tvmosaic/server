/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp> 
#include <pion/http/response_writer.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_message_server.h>
#include <dl_message_devices.h>
#include <dl_message_channels.h>
#include <dl_message_common.h>
#include <dl_message_recorder.h>
#include <dli_message_driven.h>
#include <dl_message_license.h>
#include <dl_message_epg.h>
#include <dl_message_addresses.h>
#include <dl_message_playback.h>
#include <dl_url_encoding.h>
#include <dl_xml_serialization.h>
#include <dl_server_info.h>
#include <dl_stream.h>
#include <dl_commands.h>
#include <dl_status.h>
#include <dl_server_serializer.h>
#include <dl_device_serializer.h>
#include <dl_channel_serializer.h>
#include <dl_stream_serializer.h>
#include <dl_epg_config_serializer.h>
#include <dl_products_serializer.h>
#include <dl_language_settings.h>
#include <dl_xml_command.h>
#include "constants.h"
#include "mime_types.h"
#include "pion_helper.h"
#include "common.h"
#include "command_service.h"

#ifdef _MSC_VER
#pragma warning (disable : 4100)
#endif

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void command_service::process_get_favorites_cmd(std::string& xml_response)
{
    channels::get_channel_favorites_request req;
    channels::get_channel_favorites_response resp;
    if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        std::string str;
        write_to_xml(resp.favorites_, str);
        write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_set_favorites_cmd(const std::string& request, std::string& xml_response)
{
    channels::set_channel_favorites_request req;
    if (read_from_xml(request, req.favorites_))
	{
        channels::set_channel_favorites_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
            resp.result_)
        {
		    write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_channel_overwrites_cmd(std::string& xml_response)
{
    channels::get_channel_overwrites_request req;
    channels::get_channel_overwrites_response resp;
    if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        std::string str;
        write_to_xml(resp.overwrites_, str);
        write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_set_channel_overwrites_cmd(const std::string& request, std::string& xml_response)
{
    channels::set_channel_overwrites_request req;
    if (read_from_xml(request, req.overwrites_))
	{
        channels::set_channel_overwrites_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
            resp.result_)
        {
		    write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_raw_channels_cmd(const std::string& server_address, std::string& xml_response)
{
    channel_desc_list_t cdl;
    if (ns_get_raw_channel_descriptions(message_queue_, cdl))
    {
        std::string str;
        write_to_xml(cdl, str);
        write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

bool command_service::get_channel_descriptions(const std::string& request, const std::string& server_address, channel_desc_list_t& cdl)
{
    get_channels_request_t ch_request;
    if (request.size() > 0)
        read_from_xml(request, ch_request);

    return ns_get_channel_descriptions(ch_request.favorite_id_, message_queue_, settings_, stream_service_, server_address, cdl);
}

void command_service::process_get_channels_cmd(const std::string& request, const std::string& server_address, const std::string& client_address, std::string& xml_response)
{
    //add safe IP for streaming requests
    stream_service_->add_client_ip(client_address);

    channel_desc_list_t cdl;
    if (get_channel_descriptions(request, server_address, cdl))
    {
        std::string str;
        write_to_xml(cdl, str);
        write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_get_scanned_channels_cmd(std::string& xml_response)
{
    channels::get_scanned_channels_request req;
    channels::get_scanned_channels_response resp;
    if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        std::string str;
        write_to_xml(resp.headends_, str);
        write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_get_channels_visibility_cmd(std::string& xml_response)
{
    channels::get_channel_visibility_request req;
    channels::get_channel_visibility_response resp;
    if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        std::string str;
        write_to_xml(resp.channel_visibility_, str);
        write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_set_channels_visibility_cmd(const std::string& request, std::string& xml_response)
{
    channels::set_channel_visibility_request req;
    if (read_from_xml(request, req.channel_visibility_))
	{

        channels::set_channel_visibility_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
            resp.result_)
        {
		    write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_drop_provider_on_device_cmd(const std::string& request, std::string& xml_response)
{
    drop_provider_on_device_request_t r;
    if (read_from_xml(request, r))
	{
        devices::drop_headend_on_device_request req;
        req.device_id_ = r.device_id_;
        req.headend_id_ = r.headend_id_;

        devices::drop_headend_on_device_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
            resp.result_)
        {
		    write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_oob_channel_url_cmd(const std::string& request, std::string& xml_response)
{
    get_oob_channel_url_request_t cmd_req;
    if (read_from_xml(request, cmd_req))
	{
        devices::get_device_channel_url_request req(cmd_req.channel_id_, cmd_req.format_);
        devices::get_device_channel_url_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success && resp.result_)
        {
            get_oob_channel_url_response_t cmd_resp;
            cmd_resp.url_ = resp.url_;
            cmd_resp.mime_ = resp.mime_;

            std::string str;
            write_to_xml(cmd_resp, str);
            write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}


} // dvblex
