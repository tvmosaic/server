/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning (disable : 4100)
#pragma warning (disable : 4244)
#endif

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include <dl_logger.h>
#include <dl_common.h>
#include <dl_strings.h>
#include <dl_message_addresses.h>
#include <dl_message_server.h>
#include <dl_permanent_settings.h>
#include <dl_discovery.h>
#include <dl_network_helper.h>
#include <dl_network_server_params.h>
#include <dl_xml_serialization.h>
#include <pion/http/plugin_server.hpp>
#include <pion/http/basic_auth.hpp>
#include <components/network_server.h>
#include "settings.h"
#include "constants.h"
#include "command_service.h"
#include "stream_service.h"
#include "web_service.h"
#include "port_mapper.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;
using namespace dvblex::settings;

static const size_t tvm_max_http_content_length = 8*1024*1024;

namespace pion {
namespace http {

class PION_API dvblex_auth :
    public basic_auth
{
public:
    
    /// default constructor
    dvblex_auth(user_manager_ptr userManager, const std::string& realm="PION") :
      basic_auth(userManager, realm),
      io_service_(1)
      {
          tcp_conn_ = boost::shared_ptr<tcp::connection>(new tcp::connection(io_service_));
      }

protected:
    virtual bool handle_request(const http::request_ptr& http_request_ptr, const tcp::connection_ptr& tcp_conn)
    {
        bool authorized = true;
        try {
            authorized = basic_auth::handle_request(http_request_ptr, tcp_conn_);
        } catch (...)
        {
            //there was an attempt to send something
            authorized = false;
        }

        if (!authorized)
        {
            //send our own response

            // authentication failed, send 401.....
            static const std::string CONTENT =
                " <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\""
                "\"http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd\">"
                "<HTML>"
                "<HEAD>"
                "<TITLE>Error</TITLE>"
                "<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=ISO-8859-1\">"
                "</HEAD>"
                "<BODY><H1>401 Unauthorized.</H1></BODY>"
                "</HTML> ";
            http::response_writer_ptr writer(http::response_writer::create(tcp_conn, *http_request_ptr,
                                                                           boost::bind(&tcp::connection::finish, tcp_conn)));
            writer->get_response().set_status_code(http::types::RESPONSE_CODE_UNAUTHORIZED);
            writer->get_response().set_status_message(http::types::RESPONSE_MESSAGE_UNAUTHORIZED);
            writer->get_response().add_header("WWW-Authenticate", "Basic realm=\"" + std::string(PRODUCT_NAME_UTF8) + "\"");
            writer->get_response().add_header("Access-Control-Allow-Origin", dvblex::access_control_allow_origin_hdr.c_str());
            writer->write(CONTENT);
            writer->send();
        }

        return authorized;
    }

    boost::asio::io_service io_service_;
    tcp::connection_ptr tcp_conn_;
};

class PION_API sync_plugin_server :
    public plugin_server
{
public:
    sync_plugin_server(const boost::asio::ip::tcp::endpoint& endpoint, bool enabled = true)
        : plugin_server(endpoint), pending_auth_enabled_(enabled)
    {
    }

    void set_pending_auth(pion::http::auth_ptr pending_auth)
    {
        if (pending_auth_enabled_)
        {
            boost::unique_lock<boost::mutex> lock(pending_auth_lock_);
            pending_auth_list_.push_back(pending_auth);
        }
    }

protected:
    virtual void handle_request(const http::request_ptr& http_request_ptr,
        const tcp::connection_ptr& tcp_conn, const boost::system::error_code& ec)
    {
        //this is a hack for monitoring to work (it has to be processed without authorization)
#ifdef _DEBUG
        if (boost::iequals(http_request_ptr->get_method(), "OPTIONS"))
        {
	        pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *http_request_ptr);

            writer->get_response().set_status_code(pion::http::types::RESPONSE_CODE_OK);
            writer->get_response().set_status_message(pion::http::types::RESPONSE_MESSAGE_OK);
            writer->get_response().change_header("Access-Control-Allow-Origin", "*");
            writer->get_response().change_header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
            writer->get_response().change_header("Access-Control-Allow-Headers", "X-Requested-With, content-type");
            writer->send();
            writer->get_connection()->finish();

        } else
#else
        if (boost::iequals(http_request_ptr->get_method(), "OPTIONS") &&
            http_request_ptr->has_header("Access-Control-Request-Headers") &&
            boost::icontains(http_request_ptr->get_header("Access-Control-Request-Headers"), "authorization"))
        {
	        pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *http_request_ptr);

            writer->get_response().set_status_code(pion::http::types::RESPONSE_CODE_OK);
            writer->get_response().set_status_message(pion::http::types::RESPONSE_MESSAGE_OK);
            writer->get_response().change_header("Access-Control-Allow-Origin", dvblex::access_control_allow_origin_hdr.c_str());
            writer->get_response().change_header("Access-Control-Allow-Headers", "authorization");
            writer->get_response().change_header("Access-Control-Allow-Methods", "GET, OPTIONS");
            writer->get_response().change_header("Access-Control-Allow-Credentials", "true");
            writer->send();
            writer->get_connection()->finish();

        } else
#endif
        {
            if (pending_auth_enabled_)
            {
                //check if pending auth has to be applied
                bool b = false;
                pion::http::auth_ptr pending_auth;

                {
                    boost::unique_lock<boost::mutex> lock(pending_auth_lock_);
                    if (pending_auth_list_.size() > 0)
                    {
                        b = true;
                        pending_auth = pending_auth_list_.at(0);
                        pending_auth_list_.erase(pending_auth_list_.begin());
                    }
                }

                if (b)
                {
                    boost::unique_lock<boost::shared_mutex> lock(apply_auth_lock_);
                    set_authentication(pending_auth);
                }

                {
                    boost::shared_lock<boost::shared_mutex> lock(apply_auth_lock_);
                    plugin_server::handle_request(http_request_ptr, tcp_conn, ec);
                }
            } else
            {
                plugin_server::handle_request(http_request_ptr, tcp_conn, ec);
            }
        }
    }

    std::vector<pion::http::auth_ptr> pending_auth_list_;
    boost::mutex pending_auth_lock_;

    boost::shared_mutex apply_auth_lock_;
    bool pending_auth_enabled_;
};

}}

namespace dvblex {

network_server_t::network_server_t() :
    settings_(NULL), id_(network_server_message_queue_addressee.get()), streaming_port_(0), https_streaming_port_(0)
{
}

network_server_t::~network_server_t()
{
    server_->unregister_queue(message_queue_->get_id());
    message_queue_->shutdown();

    ext_port_mapper_.reset();

    if (settings_ != NULL)
        delete settings_;
}

i_result network_server_t::query_interface(const base_id_t& /*requestor_id*/, const i_guid& /*iid*/, i_base_object_t& /*obj*/)
{
    i_result res = i_error;

    //no other interfaces

    return res;
}

bool network_server_t::init(const dvblink::i_server_t& server)
{
    server_ = server;

    message_queue_ = share_object_safely(new message_queue(id_.get()));
    message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));

    server_->register_queue(message_queue_);

    settings_ = new network_server_settings_t();
    settings_->init(message_queue_);
    settings_->load();

    ext_port_mapper_ = boost::shared_ptr<ext_port_mapper>(new ext_port_mapper());

    return true;
}

void network_server_t::handle(const dvblink::message_sender_t& /*sender*/, const dvblink::messaging::start_request& /*request*/, dvblink::messaging::start_response& response)
{
    response.result_ = start();
}

void network_server_t::handle(const dvblink::message_sender_t& /*sender*/, const dvblink::messaging::shutdown_request& /*request*/, dvblink::messaging::shutdown_response& response)
{
    stop();
    response.result_ = true;
}

plugin_server_obj_t network_server_t::start_web_server(unsigned short port, std::vector<network_server_t::web_service_desc_t> web_services)
{
    boost::asio::ip::tcp::endpoint cfg_endpoint(boost::asio::ip::tcp::v4(), port);

    //all of the services have to be of the same https/streaming type
    bool is_streaming = web_services.at(0).web_service_->is_streaming_service();
    bool is_https = web_services.at(0).web_service_->is_https();
    std::string http_prefix = web_services.at(0).web_service_->get_http_prefix();

    plugin_server_obj_t srv = boost::shared_ptr<pion::http::sync_plugin_server>(new pion::http::sync_plugin_server(cfg_endpoint, !is_streaming));
    srv->set_max_content_length(tvm_max_http_content_length);
    for (size_t i=0; i<web_services.size(); i++)
        srv->add_service(web_services[i].resource_.c_str(), web_services[i].web_service_);
    if (is_https)
    {
        srv->set_ssl_flag();
        dvblink::filesystem_path_t cert_file = settings_->get_local_certificate_path();
        log_info(L"network_server_t::start. Applying SSL certificate file %1%") % cert_file.to_wstring();
        try {
            srv->get_ssl_context_type().use_certificate_chain_file(cert_file.to_string());
            srv->set_ssl_key_file(cert_file.to_string());
        } catch(...)
        {
            log_warning(L"network_server_t::start. Exception while reading certificate file %1%") % cert_file.to_wstring();
        }
    }

    log_info(L"network_server_t::start. Starting %1% web server on port %2%") % string_cast<EC_UTF8>(http_prefix) % port;
    try
    {
        srv->start();
    }
    catch (std::exception& x)
    {
        log_error(L"network_server_t: %1%") % x.what();
        log_error(L"network_server_t: port %1% is not available") % port;
        srv.reset();
    }

    if (srv != NULL)
        servers_.push_back(srv);

    return srv;
}

static boost::uint16_t get_stream_port_from_command(boost::uint16_t command_port)
{
    return command_port + 1;
}

void network_server_t::apply_authentication()
{
    pion::http::auth_ptr auth;

    if (settings_->is_authentication_enabled())
    {
	    pion::user_manager_ptr user_man(new pion::user_manager());

	    auth = pion::http::auth_ptr( new pion::http::dvblex_auth(user_man, PRODUCT_NAME_UTF8) );
	    auth->add_restrict("/");
	    auth->add_user(settings_->get_auth_user(), settings_->get_auth_password());
    }

    for (size_t i=0; i<servers_.size(); i++)
    {
        if (servers_[i] != NULL)
        {
            pion::http::sync_plugin_server* srv = (pion::http::sync_plugin_server*)servers_[i].get();
            srv->set_pending_auth(auth);            
        }
    }

}

bool network_server_t::start()
{
    ext_port_desc_list_t http_ports;
    ext_port_desc_list_t https_ports;

    //http servers
    boost::uint16_t http_port = settings_->get_http_port();

    std::vector<web_service_desc_t> web_services;

    streaming_port_ = get_stream_port_from_command(http_port);
    stream_service* ss = new stream_service(server_, message_queue_, streaming_port_, settings_);
    web_services.push_back(web_service_desc_t(ss, stream_service_resource_url));
    plugin_server_obj_t http_stream_web_server = start_web_server(streaming_port_, web_services);

    web_services.clear();
    command_service* cs = new command_service(server_, message_queue_, http_port, ss, settings_);
    web_services.push_back(web_service_desc_t(cs, command_service_resource_url));
    web_service* ws = new web_service(server_, message_queue_, http_port, ss, settings_);
    web_services.push_back(web_service_desc_t(ws, web_service_resource_url));

    plugin_server_obj_t http_command_web_server = start_web_server(http_port, web_services);

    if (http_command_web_server == NULL || http_stream_web_server == NULL)
        log_error(L"network_server_t::start. Error starting http server on port %1%") % http_port;

    http_ports.push_back(ext_port_desc(http_port, "TVMosaic http control port"));
    http_ports.push_back(ext_port_desc(streaming_port_, "TVMosaic http stream port"));

    //https servers
    boost::uint16_t https_port = 0;
    dvblink::filesystem_path_t cert_file = settings_->get_local_certificate_path();
    if (!cert_file.empty() && boost::filesystem::exists(cert_file.to_boost_filesystem()))
    {
        unsigned short https_port = settings_->get_https_port();

        web_services.clear();

        https_streaming_port_ = get_stream_port_from_command(https_port);
        stream_service_https* https_stream_service = new stream_service_https(server_, message_queue_, https_streaming_port_, settings_);
        web_services.push_back(web_service_desc_t(https_stream_service, stream_service_resource_url));
        plugin_server_obj_t http_stream_web_server = start_web_server(https_streaming_port_, web_services);

        web_services.clear();
        command_service_https* https_command_service = new command_service_https(server_, message_queue_, https_port, https_stream_service, settings_);
        web_services.push_back(web_service_desc_t(https_command_service, command_service_resource_url));
        web_service_https* https_web_service = new web_service_https(server_, message_queue_, https_port, https_stream_service, settings_);
        web_services.push_back(web_service_desc_t(https_web_service, web_service_resource_url));

        plugin_server_obj_t https_command_web_server = start_web_server(https_port, web_services);

        if (https_command_web_server == NULL || http_stream_web_server == NULL)
            log_error(L"network_server_t::start. Error starting https server on port %1%") % https_port;

        https_ports.push_back(ext_port_desc(https_port, "TVMosaic https control port"));
        https_ports.push_back(ext_port_desc(https_streaming_port_, "TVMosaic https stream port"));
    }

    apply_authentication();

    //start port mapper
    ext_port_mapper_->start(http_ports, settings_->is_port_forwarding_enabled(), https_ports, settings_->is_port_forwarding_enabled());

    //start discovery server
#ifndef _ANDROID_ALL
    
    try
    {
        std::string hostname;
        network_helper::get_local_hostname(hostname);

        discovery_server::server_settings ss;
        ss.server_id = permanent_settings::get_server_id().to_string();
        ss.server_name = TVMOSAIC_SERVER_NAME;
        if (!hostname.empty())
            ss.server_name += " (" + hostname + ")";

        ss.http_port = http_port;
        ss.https_port = https_port;
        ss.discovery_port = settings_->get_discovery_port();

        discovery_server_ = boost::shared_ptr<discovery_server>(new discovery_server(ss));
        log_info(L"network_server_t::start(). discovery_server started");
    }
    catch (const std::runtime_error& e)
    {
	    std::wstring msg = string_cast<EC_UTF8>(e.what());
        log_error(L"network_server_t::start(): %1%") % msg.c_str();
    }
    catch (...)
    {
        log_error(L"network_server_t::start(): discovery_server could not be created");
    }

#endif

    return true;
}

void network_server_t::stop()
{
    discovery_server_.reset();

    ext_port_mapper_->stop();

    for (size_t i=0; i<servers_.size(); i++)
    {
        if (servers_[i] != NULL)
        {
            servers_[i]->stop();
            servers_[i].reset();
        }
    }

    servers_.clear();

    log_info(L"network_server_t::stop. Web server is stopped");

}

void network_server_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_playback_streaming_port_request& request, dvblink::messaging::playback::get_playback_streaming_port_response& response)
{
    response.port_ = streaming_port_;
    response.https_port_ = https_streaming_port_;
    response.result_ = true;
}

void network_server_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response)
{
    if (boost::iequals(request.cmd_id_.get(), get_net_server_params_cmd))
    {
        log_info(L"network_server_t::handle xml_command. Get network server parameters");
        network_server_params sp;
        sp.forward_ports_ = settings_->is_port_forwarding_enabled();
        sp.use_authentication_ = settings_->is_authentication_enabled();
        sp.user_ = settings_->get_auth_user();
        sp.password_ = settings_->get_auth_password();

        //ports
        sp.ports_.push_back(settings_->get_http_port());
        sp.ports_.push_back(streaming_port_);

        std::string res;
        if (write_to_xml(sp, res))
        {
            response.xml_ = res;
            response.result_ = dvblink::xmlcmd_result_success;
        } else
        {
            response.result_ = dvblink::xmlcmd_result_error;
        }
    }
    else
    if (boost::iequals(request.cmd_id_.get(), set_net_server_params_cmd))
    {
        log_info(L"network_server_t::handle xml_command. Set network server parameters");
        network_server_params sp;
        if (read_from_xml(request.xml_.get(), sp))
        {
            //do basic checks
            if (sp.use_authentication_ && (sp.user_.empty() || sp.password_.empty()))
            {
                log_error(L"network_server_::set network server parameters. User and password must be non-empty when authentication is enabled");
                response.result_ = dvblink::xmlcmd_result_error;
            } else
            {
                settings_->set_port_forwarding_enabled(sp.forward_ports_);
                settings_->set_authentication_enabled(sp.use_authentication_);
                settings_->set_auth_user(sp.user_);
                settings_->set_auth_password(sp.password_);
                settings_->save();

                //apply new settings
                apply_authentication();
                ext_port_mapper_->change_forwarding(settings_->is_port_forwarding_enabled(), settings_->is_port_forwarding_enabled());

                response.result_ = dvblink::xmlcmd_result_success;
            }
        }
        else
        {
            response.result_ = dvblink::xmlcmd_result_error;
        }
    } else
    {
        response.result_ = dvblink::xmlcmd_result_error;
    }
}

} // dvblink
