/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <algorithm>
#include <dl_strings.h>
#include <dl_ts.h>
#include "playback_transcoder.h"

using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblex::settings;
using namespace dvblink::transcoder;

namespace dvblex {

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4355)
#endif

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

playback_transcoder::playback_transcoder(const ffmpeg_launch_params_t& ffmpeg_launch_params, 
                                         const dvblink::transcoder::transcoder_params& tp,
                                         boost::uint64_t timestamp_offset) :
    stream_processor_base(), ffmpeg_(NULL), bitrate_(0),
    audio_selector_(&audio_selector_cb, this), timestamp_offset_(timestamp_offset), b_exit_flag_(false)
{
    std::string audio_track_ISO639 = tp.audio_track_;

    pmt_parser_.Init();

    ffmpeg_param_template_init_t init_info;
    if (tp.tr_bitrate_ > 0)
    {
        init_info.bitrate_kbits_ = tp.tr_bitrate_;
        bitrate_ = tp.tr_bitrate_;
    }
    if (tp.tr_width_ > 0 && tp.tr_height_ > 0)
    {
        init_info.height_ = std::min(tp.tr_width_, tp.tr_height_);
        init_info.width_ = std::max(tp.tr_width_, tp.tr_height_);
    }
    if (tp.tr_video_scale_ > 0)
    {
        init_info.width_ = ffmpeg_param_invalid_dimension;
        init_info.height_ = ffmpeg_param_invalid_dimension;
        init_info.video_scale_ = tp.tr_video_scale_;
    }

	log_info(L"Starting live_transcoder. w %1%, h %2%, b %3%, s %4%, a (%5%)") % init_info.width_ % init_info.height_ % bitrate_ % init_info.video_scale_ % string_cast<EC_UTF8>(audio_track_ISO639);

    ffmpeg_ = new ffmpeg_wrapper_t(this, &playback_transcoder::ffmpeg_callback, 
        ffmpeg_launch_params.ffmpeg_exepath_, ffmpeg_launch_params.ffmpeg_dir_);

    init_info.src_ = ffmpeg_->get_in_pipe_name();
    init_info.dest_ = ffmpeg_->get_out_pipe_name();

    std::vector<dvblink::launch_param_t> args;
    init_ffmpeg_launch_arguments(ffmpeg_launch_params.arguments_, init_info, args);

    ffmpeg_->start(args, ffmpeg_launch_params.ffmpeg_env_);
	audio_selector_.Start(audio_track_ISO639.c_str());
}

#ifdef _MSC_VER
#pragma warning(pop)
#endif

playback_transcoder::~playback_transcoder()
{
    b_exit_flag_ = true;
    ffmpeg_->stop();

    pmt_parser_.Term();
    pes_pids_.clear();

	audio_selector_.Stop();

    delete ffmpeg_;
}

void playback_transcoder::write_stream(const unsigned char* data, size_t len)
{
	audio_selector_.ProcessStream((unsigned char*)data, len);
}

void playback_transcoder::audio_selector_cb(const unsigned char* buf, int len, void* param)
{
	playback_transcoder* parent = (playback_transcoder*)param;

    while (!parent->b_exit_flag_)
    {
        bool b_processed = parent->ffmpeg_->process_data(buf, len, false);

        if (b_processed)
        {
            break;
        } else
        {
            boost::this_thread::sleep(boost::posix_time::milliseconds(50));
        }
    }
}

void playback_transcoder::ffmpeg_callback(const boost::uint8_t* buffer, boost::uint32_t length)
{

    if (pmt_parser_.GetPmtInfo() == NULL)
    {
        pmt_parser_.ProcessStream((unsigned char*)buffer, length);
        if (pmt_parser_.GetPmtInfo() != NULL)
        {
            size_t pmt_len = 0;
            const unsigned char* pmt_buf = pmt_parser_.GetPmtInfo()->GetPMTPointer(pmt_len);
            //pcr pid
            ts_process_routines::GetPMTSectionPCRPID((void*)pmt_buf, pmt_len, pcr_pid_);
            //elementary streams
            std::vector<SESInfo> stream_info;
            pmt_parser_.GetPmtInfo()->GetStreams(stream_info);
            for (size_t i=0; i<stream_info.size(); i++)
            {
                unsigned short pid = HILO_GET(stream_info[i].es_header.elementary_PID);
                pes_pids_[pid] = pid;
            }
        }
    }

    if (pmt_parser_.GetPmtInfo() != NULL)
    {
        for (size_t i = 0; i < length / TS_PACKET_SIZE; i++)
        {
            const boost::uint8_t* curpack = buffer + i * TS_PACKET_SIZE;

            unsigned short pid = ts_process_routines::GetPacketPID(curpack);

            if (pid == pcr_pid_)
            {
                boost::uint64_t pcr;
                if (ts_process_routines::GetPCRValue((void*)curpack, pcr))
                {
                    pcr += timestamp_offset_;
                    ts_process_routines::InsertPCRInPacket((unsigned char*)curpack, pcr, false);
                }
            }
            if (pes_pids_.find(pid) != pes_pids_.end())
            {
                __int64 dts = ts_process_routines::GetDTSValue(curpack);
                if (dts != -1)
                {
                    dts += timestamp_offset_;
                    ts_process_routines::SetDTSValue(curpack, dts);
                }

                __int64 pts = ts_process_routines::GetPTSValue(curpack);
                if (pts != -1)
                {
                    pts += timestamp_offset_;
                    ts_process_routines::SetPTSValue(curpack, pts);
                }
            }
        }

        if (cb_ != NULL)
            cb_(buffer, length, user_param_);
    }
}


} // dvblex
