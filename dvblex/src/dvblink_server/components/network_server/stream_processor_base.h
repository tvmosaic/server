/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/shared_ptr.hpp>
#include <dl_types.h>
#include "mime_types.h"

namespace dvblex {

class stream_processor_base
{
    friend class network_streamer_base_t;

public:
    typedef void (*stream_proc_callback_f)(const unsigned char* data, size_t len, void* user_param);

public:
    stream_processor_base() 
        : cb_(NULL), user_param_(NULL)
    {}

    virtual ~stream_processor_base(){}
    virtual void signal_exit(){}

    virtual bool is_transport_stream() {return true;}
    virtual const char* get_mime() {return mpeg_mime_code.c_str();}

protected:
    stream_proc_callback_f cb_;
    void* user_param_;

    virtual void write_stream(const unsigned char* /*data*/, size_t /*len*/){}
    virtual void init(stream_proc_callback_f cb, void* user_param){cb_ = cb; user_param_ = user_param;}
};

} // dvblink
