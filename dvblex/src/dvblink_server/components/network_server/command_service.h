/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning (disable : 4100)
#pragma warning (disable : 4244)
#endif

#include <pion/http/plugin_service.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include <boost/cstdint.hpp>
#include <dli_server.h>
#include <dl_message_queue.h>
#include "stream_service.h"

namespace dvblex {

class command_service : public http_server_type
{
public:
    command_service(dvblink::i_server_t server, dvblink::messaging::message_queue_t message_queue, unsigned short port, stream_service* stream_service, network_server_settings_t* settings);
    virtual ~command_service();

    virtual void operator()(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn);

    virtual bool is_streaming_service(){return false;}

private:
    dvblink::messaging::message_queue_t message_queue_;
    dvblink::i_server_t server_;
    unsigned short port_;
    stream_service* stream_service_;
    network_server_settings_t* settings_;

    void send_binary_response(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, const std::vector<char>& data, const std::string& mime);
    void send_xml_response(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, const std::string& data, const std::string& mime);
    void send_error_response(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, ::boost::uint32_t code, const std::string& msg);
    void process_accept_language_header(const pion::http::request_ptr& http_request_ptr);
    std::string process_command(const pion::http::request_ptr& http_request_ptr, const std::string& command, const std::string& request, const std::string& server_address, const std::string& client_addr, std::string& xml_response);
    void process_get_streaming_caps_cmd(const std::string& request, std::string& xml_response);
    void process_server_info_cmd(std::string& xml_response);
    void process_get_devices_cmd(std::string& xml_response);
    void process_get_scanners_cmd(const std::string& request, std::string& xml_response);
    void process_start_scan_cmd(const std::string& request, std::string& xml_response);
    void process_rescan_provider_cmd(const std::string& request, std::string& xml_response);
    void process_get_rescan_settings_cmd(const std::string& request, std::string& xml_response);
    void process_cancel_scan_cmd(const std::string& request, std::string& xml_response);
    void process_apply_scan_cmd(const std::string& request, std::string& xml_response);
    void process_get_networks_cmd(const std::string& request, std::string& xml_response);
    void process_get_device_status_cmd(const std::string& request, std::string& xml_response);
    void process_get_favorites_cmd(std::string& xml_response);
    void process_set_favorites_cmd(const std::string& request, std::string& xml_response);    
    void process_get_channels_cmd(const std::string& request, const std::string& server_address, const std::string& client_address, std::string& xml_response);
    void process_get_raw_channels_cmd(const std::string& server_address, std::string& xml_response);
    void process_get_scanned_channels_cmd(std::string& xml_response);
    void process_search_epg_cmd(const std::string& request, const std::string& server_address, std::string& xml_response);
    void process_get_recordings_cmd(std::string& xml_response);
    void process_execute_command_cmd(const std::string& request, const std::string& server_address, std::string& xml_response);
    void process_get_stream_info_cmd(const std::string& request, const std::string& server_address, const std::string& client_address, std::string& xml_response);
    void process_repair_database_cmd(std::string& xml_response);
    void process_force_epg_update_cmd(std::string& xml_response);
    void process_enable_epg_updates_cmd(const std::string& request, std::string& xml_response);
    void process_get_channels_visibility_cmd(std::string& xml_response);
    void process_set_channels_visibility_cmd(const std::string& request, std::string& xml_response);
    void process_get_object_resume_info_cmd(const std::string& request, std::string& xml_response);
    void process_set_object_resume_info_cmd(const std::string& request, std::string& xml_response);
    void process_get_channel_overwrites_cmd(std::string& xml_response);
    void process_set_channel_overwrites_cmd(const std::string& request, std::string& xml_response);
    void process_play_channel_cmd(const std::string& request, const std::string& server_address, const std::string& client_addr, std::string& xml_response);
    void process_stop_channel_cmd(const std::string& request, std::string& xml_response);
    void process_timeshift_get_stats_cmd(const std::string& request, std::string& xml_response);
    void process_timeshift_seek_cmd(const std::string& request, std::string& xml_response);
    void process_get_epg_sources_cmd(std::string& xml_response);
    void process_get_epg_channels_cmd(const std::string& request, std::string& xml_response);
    void process_get_epg_channel_config_cmd(const std::string& request, std::string& xml_response);
    void process_set_epg_channel_config_cmd(const std::string& request, std::string& xml_response);
    void process_match_epg_channels_cmd(const std::string& request, std::string& xml_response);
    void process_get_installed_products_cmd(std::string& xml_response);
    void process_activate_product_cmd(const std::string& request, std::string& xml_response);
    void process_activate_product_trial_cmd(const std::string& request, std::string& xml_response);
    void process_add_schedule_cmd(const std::string& request, std::string& xml_response);
    void process_set_recording_settings_cmd(const std::string& request, std::string& xml_response);
    void process_get_recording_settings_cmd(std::string& xml_response);
    void process_get_schedules_cmd(std::string& xml_response);
    void process_update_schedule_cmd(const std::string& request, std::string& xml_response);
    void process_remove_schedule_cmd(const std::string& request, std::string& xml_response);
    void process_remove_recording_cmd(const std::string& request, std::string& xml_response);
    void process_get_playlist_m3u_cmd(const pion::http::request_ptr& http_request_ptr, const std::string& server_address, const std::string& client_address, std::string& xml_response);
    void process_get_xmltv_epg_cmd(const pion::http::request_ptr& http_request_ptr, const std::string& server_address, std::string& xml_response);
    void process_get_objects_cmd(const std::string& request, const std::string& server_address, const std::string& client_address, std::string& xml_response);
    void process_remove_object_cmd(const std::string& request, std::string& xml_response);
    void process_stop_recording_cmd(const std::string& request, std::string& xml_response);
    void process_drop_provider_on_device_cmd(const std::string& request, std::string& xml_response);
    void process_get_device_settings_cmd(const std::string& request, std::string& xml_response);
    void process_set_device_settings_cmd(const std::string& request, std::string& xml_response);
    void process_get_device_templates_cmd(const std::string& request, std::string& xml_response);
    void process_create_manual_device_cmd(const std::string& request, std::string& xml_response);
    void process_delete_manual_device_cmd(const std::string& request, std::string& xml_response);
    void process_search_objects_cmd(const std::string& request, const std::string& server_address, std::string& xml_response);
    bool get_channel_descriptions(const std::string& request, const std::string& server_address, channel_desc_list_t& cdl);
    void process_get_oob_channel_url_cmd(const std::string& request, std::string& xml_response);
};

class command_service_https : public command_service
{
public:
    command_service_https(dvblink::i_server_t server, dvblink::messaging::message_queue_t message_queue, unsigned short port, stream_service* stream_service, network_server_settings_t* settings)
        : command_service(server, message_queue, port, stream_service, settings)
    {}

    virtual bool is_https(){return true;}
};

} // dvblink
