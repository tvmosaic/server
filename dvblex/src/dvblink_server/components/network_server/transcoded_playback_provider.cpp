/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_utils.h>
#include <dl_installation_settings.h>
#include <dl_message_playback.h>
#include <dl_message_server.h>
#include <dl_message_addresses.h>
#include <dl_ts_info.h>
#include <dl_ts.h>
#include <dl_transcoder.h>
#include "constants.h"
#include "mime_types.h"
#include "memory_buffer_sender.h"
#include "playback_transcoder.h"
#include "transcoded_playback_provider.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblex::settings;
using namespace dvblink::messaging;
using namespace dvblink::transcoder;
using namespace dvblink::logging;
using namespace dvblink::messaging::playback;
using namespace dvblex::playback;

namespace dvblex {

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

transcoded_playback_provider::transcoded_playback_provider(boost::shared_ptr<pb_item_t> item, const base_id_t source_id, const std::string& object_id, 
                                           const i_play_source_control_t playback_interface, const message_queue_t message_queue, 
                                           dvblink::transcoder::transcoder_params& transcoder_params, size_t start_segment_num, size_t segment_duration) :
    playback_interface_(playback_interface),
    message_queue_(message_queue),
    source_id_(source_id),
    object_id_(object_id),
    play_thread_(NULL),
    item_(item),
    segment_duration_(segment_duration),
    segment_num_threshold_(3),
    transcoder_params_(transcoder_params),
    start_segment_num_(start_segment_num),
    packet_aligner_(aligner_callback, this)
{
    inactivity_timeout_ = std::min(2 * segment_duration, (size_t)10);
    
    time(&last_request_time_);

    dvblex::settings::installation_settings_t install_settings;
    install_settings.init(message_queue_);

    server::ffmpeg_launch_params_request req(tu_transcode_recording_to_hls_id);
    server::ffmpeg_launch_params_response resp;
    bool b = message_queue_->send(server_message_queue_addressee, req, resp) == success && resp.result_;

    if (!b)
        log_error(L"transcoded_playback_provider. No transcoding profile exists");

    boost::uint64_t time_stamp_offset = get_time_stamp_offset();
    stream_processor_base* processor = new playback_transcoder(resp.launch_params_, transcoder_params_, time_stamp_offset);

    network_port_t server_streaming_port;
    url_address_t server_ip;
    std::string id;
    segmentor_ = boost::shared_ptr<iphone_segmentor>(new iphone_segmentor(id, server_streaming_port, server_ip, processor, segment_duration, start_segment_num));
    segmentor_->start(NULL);

    b_exit_flag_ = false;
    b_cancel_flag_ = false;
    play_thread_ = new boost::thread(boost::bind(&transcoded_playback_provider::playback_thread_func, this));
}

transcoded_playback_provider::~transcoded_playback_provider()
{
    if (play_thread_ != NULL)
        delete play_thread_;

    playback_interface_.reset();
}

void transcoded_playback_provider::stop()
{
    if (play_thread_ != NULL)
    {
        segmentor_->signal_exit();
        b_exit_flag_ = true;

        play_thread_->join();
        segmentor_.reset();

        delete play_thread_;
        play_thread_ = NULL;
    }
    
    log_info(L"transcoded_playback_provider::stop. Object id %1%") % string_cast<EC_UTF8>(object_id_);
}

void transcoded_playback_provider::cancel()
{
    b_cancel_flag_ = true;
    play_thread_->join();

    segmentor_.reset();
    log_info(L"transcoded_playback_provider::cancel. Object id %1%") % string_cast<EC_UTF8>(object_id_);
}

boost::uint64_t transcoded_playback_provider::get_start_offset()
{
    boost::uint64_t offs = 0;
    
    //get item duration
    time_t duration = 0;
    switch (item_->item_type_)
    {
        case pbit_item_recorded_tv:
            {
                boost::shared_ptr<pb_recorded_tv_t> recorded_tv = boost::static_pointer_cast<pb_recorded_tv_t>(item_);
                duration = recorded_tv->video_info_.m_Duration;
            } 
            break;
        default:
            break;
    }
    if (duration > 0)
    {
        size_t total_segments = duration / segment_duration_;
        offs = (item_->size_ * start_segment_num_) / total_segments;
    } else
    {
        log_error(L"transcoded_playback_provider::get_start_offset. Unable to calculate playback item duration %1%") % string_cast<EC_UTF8>(object_id_);
    }
    
    return offs;
}

boost::uint64_t transcoded_playback_provider::get_time_stamp_offset()
{
    boost::uint64_t ret_val = 0;

    ret_val = start_segment_num_ * segment_duration_;
    //convert it into pcr/pts/dts domain
    ret_val = ret_val * 27000000;

    return ret_val;
}

void transcoded_playback_provider::playback_thread_func()
{
    const boost::uint32_t read_buf_len = 200*1024;
    unsigned char* read_buf = new unsigned char[read_buf_len];
    //open playback item
    dvblink::object_handle_t object_handle;
    boost::uint64_t item_size_bytes = 0;
    if (open(object_id_, object_handle, item_size_bytes))
    {
        boost::uint64_t start_offset = get_start_offset();
        seek(object_handle, start_offset);
        
        log_info(L"transcoded_playback_provider::playback_thread_func. Starting playback of %1% from offset %2%") % string_cast<EC_UTF8>(object_id_) % start_offset;
        
        bool b_finished_reading = false;
        while (!b_exit_flag_ && !b_cancel_flag_)
        {
            if (b_finished_reading)
            {
                //simply wait until this client is closed
                boost::this_thread::sleep(boost::posix_time::milliseconds(50));
            } else
            {
                boost::uint32_t len = read_buf_len;

                bool b = playback_interface_->read_data(object_handle.get().c_str(), read_buf, len);
                if (!b || len == 0)
                {
                    //error while reading (no data anymore?)
                    b_finished_reading = true;
                    log_info(L"transcoded_playback_provider::playback_thread_func. Finished reading %1%. Waiting for termination") % string_cast<EC_UTF8>(object_id_);
                } else
                {
                    packet_aligner_.write_stream(read_buf, len);
                }
            }
        }

        if (!b_cancel_flag_) //do not close object if cancelled
            close(object_handle);
    }

    delete read_buf;
}

bool transcoded_playback_provider::open(std::string& object_id, dvblink::object_handle_t& object_handle, boost::uint64_t& item_size_bytes)
{
    bool ret_val = false;

    open_item_request req;
    open_item_response resp;
    req.object_id_ = object_id;
    message_error err = message_queue_->send(playback_interface_->get_uid(), req, resp);
    if (err == success && resp.result_)
    {
        object_handle = resp.handle_;
        item_size_bytes = resp.item_size_bytes_;
        ret_val = true;
    }

    return ret_val;
}

void transcoded_playback_provider::close(dvblink::object_handle_t& object_handle)
{
    close_item_request req;
    close_item_response resp;
    req.handle_ = object_handle;
    /*message_error err = */message_queue_->send(playback_interface_->get_uid(), req, resp);
}

void transcoded_playback_provider::seek(dvblink::object_handle_t& object_handle, boost::uint64_t offset)
{
    seek_item_request req;
    seek_item_response resp;
    req.handle_ = object_handle;
    req.offset_ = offset;
    /*message_error err = */message_queue_->send(playback_interface_->get_uid(), req, resp);
}

bool transcoded_playback_provider::process_segment_request(const pion::http::request_ptr& request, pion::http::response_writer_ptr& writer, size_t segment_num)
{
    bool res = false;

    //remember time of the last segment request - for timeout checking
    time(&last_request_time_);

    log_ext_info(L"transcoded_playback_provider::process_segment_request. Segment %1%") % segment_num;
    
	if (iphone_segmentor::segment_t segment = segmentor_->get_segment(segment_num, false))
	{
		boost::shared_ptr<memory_buffer_sender> sender_ptr(
            memory_buffer_sender::Create(segmentor_, segment, ts_data_mime_code, request, writer->get_connection()));
		sender_ptr->send();
		res = true;
	}
    return res;
}

bool transcoded_playback_provider::is_finished()
{
    time_t now;
    time(&now);
    return (now - last_request_time_) > inactivity_timeout_;
}

bool transcoded_playback_provider::is_valid_segment_num(size_t segment_num)
{
    return segmentor_->is_valid_segment_id(segment_num);
}

bool transcoded_playback_provider::get_transcoder_params_from_http_request(dvblink::transcoder::transcoder_params& transcoder_params, const pion::http::request_ptr& request)
{
    bool res = false;
    
    if (request->has_query(request_param_transcoder) && request->has_query(request_param_transcoder_bitrate))
    {
        if (request->has_query(request_param_transcoder_width))
            transcoder_params.tr_width_ = atoi(request->get_query(request_param_transcoder_width).c_str());

        if (request->has_query(request_param_transcoder_height))
            transcoder_params.tr_height_ = atoi(request->get_query(request_param_transcoder_height).c_str());

        if (request->has_query(request_param_transcoder_scale))
            transcoder_params.tr_video_scale_ = atoi(request->get_query(request_param_transcoder_scale).c_str());

        transcoder_params.tr_bitrate_ = atoi(request->get_query(request_param_transcoder_bitrate).c_str());

        if (request->has_query(request_param_transcoder_lng))
            transcoder_params.audio_track_ = request->get_query(request_param_transcoder_lng);

        res = true;
    }

    return res;
}

bool transcoded_playback_provider::is_valid_transcoder_params(dvblink::transcoder::transcoder_params& transcoder_params)
{
    return (transcoder_params_.tr_width_ == transcoder_params.tr_width_ &&
            transcoder_params_.tr_height_ == transcoder_params.tr_height_ &&
            transcoder_params_.tr_bitrate_ == transcoder_params.tr_bitrate_ &&
            transcoder_params_.audio_track_ == transcoder_params.audio_track_);
}

void __stdcall transcoded_playback_provider::aligner_callback(const unsigned char* buf, unsigned long len, void* user_param)
{
	transcoded_playback_provider* parent = (transcoded_playback_provider*)user_param;

    while (!parent->b_exit_flag_ && !parent->b_cancel_flag_)
    {
        bool b_processed = false;
        //do not send new data if number of pending segments is more than threshold
        if (parent->segmentor_->get_segments_num() < parent->segment_num_threshold_)
        {
            parent->segmentor_->write_stream(buf, len);
            b_processed = true;
        }
                
        if (b_processed)
        {
            break;
        } else
        {
            boost::this_thread::sleep(boost::posix_time::milliseconds(50));
        }
    }
}

} // dvblex
