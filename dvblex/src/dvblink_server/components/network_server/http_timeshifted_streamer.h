/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning (disable : 4100)
#pragma warning (disable : 4244)
#endif

#include <pion/http/response_writer.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include <boost/thread.hpp>
#include <dl_ts.h>
#include <dl_circle_buffer.h>
#include <dl_timer_procedure.h>
#include <dl_event.h>
#include <dl_packet_buffer.h>
#include <dl_hdd_ring_buffer.h>
#include <dl_ts_aligner.h>
#include <dl_smooth_streamer.h>
#include <dl_flag.h>
#include <dl_timer_procedure.h>
#include "network_streamer_base.h"

namespace dvblex {

struct timeshift_stats_s
{
    boost::uint64_t max_buffer_length;
    boost::uint64_t buffer_length;
    time_t buffer_duration;
    boost::uint64_t cur_pos_bytes;
    time_t cur_pos_sec;
};

class http_timeshifted_streamer_t;
typedef boost::shared_ptr<http_timeshifted_streamer_t> http_timeshifted_streamer_object_t;

class http_timeshifted_streamer_t : public network_streamer_base_t
{
public:
    http_timeshifted_streamer_t(const std::string& id, const dvblink::filesystem_path_t& disk_path, boost::uint64_t max_size, stream_processor_base* processor = NULL);
	~http_timeshifted_streamer_t();

    bool process_request(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn);

    virtual bool start(dvblink::streamer_callbacks_t* cb);
    virtual void on_received_stream(const unsigned char* data, size_t len);

    bool get_timeshift_stats(timeshift_stats_s& stats);
    bool seek_by_bytes(boost::int64_t offset, int whence, boost::uint64_t& cur_pos);
    bool seek_by_time(time_t offset, int whence, boost::uint64_t& cur_pos);

    static int get_timeshift_version();
    static bool is_timeshift_request(const pion::http::request_ptr& http_request_ptr);

    virtual std::string get_streaming_url(const dvblink::channel_id_t& channel_id, const std::string& protocol_header, const std::string& server_address, 
        boost::uint16_t streaming_port, int handle);

private:
    dvblink::engine::hdd_ring_buffer ring_buffer_;
    bool b_initialized_;
    bool b_exit_flag_;
	boost::thread* send_thread_;
	time_t last_disconnect_;
	int disconnect_threshold_;
	dvblink::engine::timer_procedure<http_timeshifted_streamer_t>* disconnect_timer_;

    pion::http::response_writer_ptr client_;
    dvblink::event node_processed_;
    dvblink::engine::ts_packet_buffer packet_buffer_;
    bool b_write_error_;
	dvblink::engine::timer_procedure<http_timeshifted_streamer_t>* idle_timer_;
    dvblink::engine::atomic_switch send_idle_packet_;
    unsigned char null_packet_[dvblink::engine::TS_PACKET_SIZE];
	time_t start_time_;
	bool b_stream_sent_;
	int no_stream_timeout_sec_;

    dvblink::engine::ts_packet_aligner aligner_;
    dvblink::smooth_streamer_t smooth_streamer_;

	void send_thread_func();    
    void handle_write(const boost::system::error_code& write_error, std::size_t /*bytes_written*/, bool* b_error_flag);
    void stop_send_thread();
	void disconnect_timer_func(const boost::system::error_code& e);
    static void smooth_streamer_callback(const unsigned char* buf, size_t len, void* user_param);
    static void __stdcall aligner_callback(const unsigned char* buf, unsigned long len, void* user_param);
	void idle_timer_func(const boost::system::error_code& e);

};

} // dvblex
