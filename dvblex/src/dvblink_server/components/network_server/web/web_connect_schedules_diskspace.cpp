/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_rec_options_serializer.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_message_recorder.h>
#include <dl_xml_serialization.h>
#include <dl_filename_pattern.h>
#include <dl_locale_strings.h>
#include "web_connect_schedules_diskspace.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_connect_schedules_diskspace_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    dvblink::messaging::recorder::get_recording_options_request req;
    dvblink::messaging::recorder::get_recording_options_response resp;
    if (message_queue->send(recorder_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        rd_recording_settings_t rs;
        read_from_xml(resp.options_.to_string(), rs);

        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

        writer.StartObject();

        writer.Key("total");
        writer.Int64(rs.get_total_space_kb());
        writer.Key("free");
        writer.Int64(rs.get_avail_space_kb());

        writer.EndObject();

        send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
    } else
    {
        log_error(L"web_connect_schedules_diskspace_handler::process_get. get_recording_options_request failed");
        send_json_error_message(http_request_ptr, tcp_conn, "get_recording_options_request failed");
    }
}

} // dvblex
