/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_server_params.h>
#include <dl_message_addresses.h>
#include <dl_message_license.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "web_settings_products_file.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_products_file_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    if (http_request_ptr->has_query("id"))
    {
        std::string product_id = http_request_ptr->get_query("id");

        license::get_installed_products_request req;
        license::get_installed_products_response resp;
        if (message_queue->send(server_message_queue_addressee, req, resp) == success && resp.result_.get())
        {
            dvblink::product_file_t product_file;
            for (size_t i=0; i<resp.products_.size(); i++)
            {
                if (boost::iequals(product_id, resp.products_[i].id_.to_string()))
                {
                    product_file = resp.products_[i].product_file_;
                    break;
                }
            }

            if (!product_file.empty())
            {
                std::ostringstream stream;
                stream << "attachment; filename=" << product_id << ".dat";

	            pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *http_request_ptr);

                writer->get_response().set_status_code(pion::http::types::RESPONSE_CODE_OK);
                writer->get_response().set_status_message(pion::http::types::RESPONSE_MESSAGE_OK);
                writer->get_response().set_content_type("application/octet-stream");
                writer->get_response().change_header("Access-Control-Allow-Origin", access_control_allow_origin_hdr.c_str());
                writer->get_response().change_header("Content-Disposition", stream.str());
                writer->write(product_file.to_string());
                writer->send();
                writer->get_connection()->finish();
            } else
            {
                log_error(L"web_settings_products_file_handler::process_get. product file not found");
                send_json_error_message(http_request_ptr, tcp_conn, "product file not found");
            }
        } else
        {
            log_error(L"web_settings_products_file_handler::process_get. get_server_log_cmd failed");
            send_json_error_message(http_request_ptr, tcp_conn, "get_server_params_cmd failed");
        }
    } else
    {
        log_error(L"web_settings_products_file_handler::process_get. missing products id parameter");
        send_json_error_message(http_request_ptr, tcp_conn, "missing products id parameter");
    }
}

} // dvblex
