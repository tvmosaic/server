/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_rec_options_serializer.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_message_recorder.h>
#include <dl_xml_serialization.h>
#include <dl_filename_pattern.h>
#include <dl_locale_strings.h>
#include "web_settings_recorder.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_recorder_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;
    if (http_request_ptr->get_content_length())
    {
        std::string response_str = http_request_ptr->get_content();

        rapidjson::LookaheadParser r(&response_str[0]);
        if (r.IsValid())
        {
            rd_recording_settings_t rs;
            filename_pattern_t filename_pattern;

            r.EnterObject();
            while (const char* key = r.NextObjectKey())
            {
                if (strcmp(key, "path") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    rs.set_recording_path(filesystem_path_t(r.GetString()));
                } else
                if (strcmp(key, "before_margin") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    rs.set_before_margin(r.GetInt());
                } else
                if (strcmp(key, "after_margin") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    rs.set_after_margin(r.GetInt());
                } else
                if (strcmp(key, "check_deleted_rec") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    rs.set_check_deleted_recordings(r.GetBool());
                } else
                if (strcmp(key, "new_only_algo_type") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    rs.set_new_only_algo((recorder_new_only_algo_type_e)r.GetInt());
                } else
                if (strcmp(key, "new_only_default_value") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    rs.set_new_only_default_value((recorder_new_only_default_value_e)r.GetInt());
                } else
                if (strcmp(key, "auto_disk_space_mode") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    rs.set_auto_disk_space_mode(r.GetBool());
                } else
                if (strcmp(key, "man_disk_space_mb") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    rs.set_manual_disk_space_kb(r.GetInt64() * 1024);
                } else
                if (strcmp(key, "autodelete_algo") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    rs.set_autodelete_algo(r.GetBool());
                } else
                if (strcmp(key, "filename_pattern") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    r.EnterObject();
                    while (const char* fp_key = r.NextObjectKey())
                    {
                        if (strcmp(fp_key, "separator") == 0 && r.PeekType() != rapidjson::kNullType)
                        {
                            filename_pattern.separator = r.GetString();
                        } else
                        if (strcmp(fp_key, "current") == 0 && r.PeekType() != rapidjson::kNullType)
                        {
                            r.EnterArray();
                            while (r.NextArrayValue()) 
                            {
                                filename_pattern.fields.push_back((filename_pattern_fields_e)r.GetInt());
                            }
                        } else
                        {
                            r.SkipValue();
                        }
                    }
                } else
                {
                    r.SkipValue();
                }
            }

            std::string fp_string;
            filename_pattern_to_string(filename_pattern, fp_string);
            rs.set_filename_pattern(fp_string);

            std::string str;
            write_to_xml(rs, str);

            dvblink::messaging::recorder::set_recording_options_request req(str);
            dvblink::messaging::recorder::set_recording_options_response resp;
            if (message_queue->send(recorder_message_queue_addressee, req, resp) == success && resp.result_)
            {
                ok = true;
            } else
            {
                log_error(L"web_settings_recorder_handler::process_set. set_recording_options_request failed");
            }
        } else
        {
            log_error(L"web_settings_recorder_handler::process_set. unable to deserialize json request");
        }
    } else
    {
        log_error(L"web_settings_recorder_handler::process_set. post content of 0 length");
    }

    if (ok)
        send_json_ok_message(http_request_ptr, tcp_conn);
    else
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

void web_settings_recorder_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    dvblink::messaging::recorder::get_recording_options_request req;
    dvblink::messaging::recorder::get_recording_options_response resp;
    if (message_queue->send(recorder_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        rd_recording_settings_t rs;
        read_from_xml(resp.options_.to_string(), rs);

        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

        writer.StartObject();

        writer.Key("path");
        writer.String(rs.get_recording_path().to_string().c_str());
        writer.Key("before_margin");
        writer.Int(rs.get_before_margin());
        writer.Key("after_margin");
        writer.Int(rs.get_after_margin());
        writer.Key("check_deleted_rec");
        writer.Bool(rs.check_deleted_recordings());
        writer.Key("new_only_algo_type");
        writer.Int((int)rs.get_new_only_algo());
        writer.Key("new_only_default_value");
        writer.Int((int)rs.get_new_only_default_value());
        writer.Key("auto_disk_space_mode");
        writer.Int(rs.use_auto_disk_space_mode());
        writer.Key("man_disk_space_mb");
        writer.Int64(rs.get_manual_disk_space_kb() / 1024);
        writer.Key("autodelete_algo");
        writer.Bool(rs.use_autodelete_algo());

        writer.Key("filename_pattern");
        writer.StartObject();

        filename_pattern_t filename_pattern;
        string_to_filename_pattern(rs.get_filename_pattern(), filename_pattern);
        writer.Key("current");
        writer.StartArray();
        for (size_t i=0; i<filename_pattern.fields.size(); i++)
            writer.Int((int)filename_pattern.fields[i]);
        writer.EndArray();
        writer.Key("available");
        writer.StartArray();
        add_json_name_intvalue_pair_localized(e_fpf_channel_name, IDS_SERVER_PATTERN_CHANNEL_NAME, writer);
        add_json_name_intvalue_pair_localized(e_fpf_channel_number, IDS_SERVER_PATTERN_CHANNEL_NUM, writer);
        add_json_name_intvalue_pair_localized(e_fpf_program_name, IDS_SERVER_PATTERN_PROGRAM_NAME, writer);
        add_json_name_intvalue_pair_localized(e_fpf_program_subname, IDS_SERVER_PATTERN_PROGRAM_SUBNAME, writer);
        add_json_name_intvalue_pair_localized(e_fpf_program_season_episode, IDS_SERVER_PATTERN_SEASON_EPISODE, writer);
        add_json_name_intvalue_pair_localized(e_fpf_program_date_time, IDS_SERVER_PATTERN_DATE_TIME, writer);
        add_json_name_intvalue_pair_localized(e_fpf_program_date, IDS_SERVER_PATTERN_DATE, writer);
        add_json_name_intvalue_pair_localized(e_fpf_program_time, IDS_SERVER_PATTERN_TIME, writer);
        writer.EndArray();
        writer.Key("separator");
        writer.String(filename_pattern.separator.c_str());
        writer.Key("separators");
        writer.StartArray();
        writer.String("-");
        writer.String("_");
        writer.String(".");
        writer.EndArray();

        writer.EndObject();

        writer.Key("after_margins");
        writer.StartArray();
        add_json_name_intvalue_pair_localized(0, IDS_SERVER_MARGIN_ON_TIME, writer);
        add_json_name_intvalue_pair_raw(60, "1 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_LATER), writer);
        add_json_name_intvalue_pair_raw(120, "2 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_LATER), writer);
        add_json_name_intvalue_pair_raw(180, "3 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_LATER), writer);
        add_json_name_intvalue_pair_raw(240, "4 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_LATER), writer);
        add_json_name_intvalue_pair_raw(300, "5 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_LATER), writer);
        add_json_name_intvalue_pair_raw(600, "10 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_LATER), writer);
        add_json_name_intvalue_pair_raw(900, "15 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_LATER), writer);
        add_json_name_intvalue_pair_raw(1800, "30 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_LATER), writer);
        writer.EndArray();

        writer.Key("before_margins");
        writer.StartArray();
        add_json_name_intvalue_pair_localized(0, IDS_SERVER_MARGIN_ON_TIME, writer);
        add_json_name_intvalue_pair_raw(60, "1 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_EARLIER), writer);
        add_json_name_intvalue_pair_raw(120, "2 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_EARLIER), writer);
        add_json_name_intvalue_pair_raw(180, "3 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_EARLIER), writer);
        add_json_name_intvalue_pair_raw(240, "4 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_EARLIER), writer);
        add_json_name_intvalue_pair_raw(300, "5 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_EARLIER), writer);
        add_json_name_intvalue_pair_raw(600, "10 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_EARLIER), writer);
        add_json_name_intvalue_pair_raw(900, "15 " + language_settings::GetInstance()->GetItemName(IDS_SERVER_MARGIN_MIN_EARLIER), writer);
        writer.EndArray();

        writer.Key("new_only_algo_types");
        writer.StartArray();
        add_json_name_intvalue_pair_localized(rec_noat_not_seen_before, IDS_SERVER_NEWONLY_NOT_SEEN_BEFORE, writer);
        add_json_name_intvalue_pair_localized(rec_noat_epg_repeat_flag, IDS_SERVER_NEWONLY_REPEAT_FLAG, writer);
        add_json_name_intvalue_pair_localized(rec_noat_epg_premiere_flag, IDS_SERVER_NEWONLY_PREMIERE_FLAG, writer);
        writer.EndArray();

        writer.Key("new_only_default_values");
        writer.StartArray();
        add_json_name_intvalue_pair_localized(rec_nodv_not_set, IDS_SERVER_NEWONLY_DEFAULT_NOT_SET, writer);
        add_json_name_intvalue_pair_localized(rec_nodv_true, IDS_SERVER_NEWONLY_DEFAULT_TRUE, writer);
        add_json_name_intvalue_pair_localized(rec_nodv_false, IDS_SERVER_NEWONLY_DEFAULT_FALSE, writer);
        writer.EndArray();

        writer.EndObject();

        send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
    } else
    {
        log_error(L"web_settings_recorder_handler::process_get. get_recording_options_request failed");
        send_json_error_message(http_request_ptr, tcp_conn, "get_recording_options_request failed");
    }
}

} // dvblex
