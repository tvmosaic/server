/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_logo_cmd.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "../common.h"
#include "../pion_helper.h"
#include "web_settings_channels_logos_packs.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_channels_logos_packs_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    dvblink::messaging::xml_message_request request;
    request.cmd_id_ = get_channel_logo_packages_cmd;
    request.server_address_ = get_server_ip_from_request(http_request_ptr);
    request.proto_ = context.stream_service->get_http_prefix();

    dvblink::messaging::xml_message_response response;
    if (message_queue->send(logo_manager_message_queue_addressee, request, response) == dvblink::messaging::success &&
        response.result_.to_string().compare(xmlcmd_result_success) == 0)
    {
        logo_package_list_t logo_package_list;
        read_from_xml(response.xml_.to_string(), logo_package_list);

        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

        writer.StartArray();
        for (size_t i=0; i<logo_package_list.size(); i++)
        {
            add_json_id_label_pair_raw(logo_package_list[i].to_string().c_str(), logo_package_list[i].to_string().c_str(), writer);
        }
        writer.EndArray();

        send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
    } else
    {
        log_error(L"web_settings_channels_logos_packs_handler::process_get. get_channel_logo_packages_cmd failed");
        send_json_error_message(http_request_ptr, tcp_conn, "get_channel_logo_packages_cmd failed");
    }
}

} // dvblex
