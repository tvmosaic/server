/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_server.h>
#include "web_settings_general_checkupdates.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

bool web_settings_general_checkupdates_handler::get_available_server_revision(const std::string& server_id, std::string& revision)
{
    //TODO: Implement in the future a check if new TVMosaicc CE version is available for download
    revision = "0";
    return true;
}

void web_settings_general_checkupdates_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    server::server_info_request si_req;
    server::server_info_response si_resp;
    if (message_queue->send(server_message_queue_addressee, si_req, si_resp) == success)
    {
        std::string revision;
        if (get_available_server_revision(si_resp.server_info_.product_id_.to_string(), revision))
        {
            bool update_available = atoi(si_resp.server_info_.build_.to_string().c_str()) < atoi(revision.c_str());

            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            writer.StartObject();
            writer.Key("available");
            writer.Bool(update_available);
            writer.Key("localRevision");
            writer.String(si_resp.server_info_.build_.to_string().c_str());
            writer.Key("remoteRevision");
            writer.String(revision.c_str());
            writer.EndObject();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
        } else
        {
            send_json_error_message(http_request_ptr, tcp_conn, "get_available_server_revision failed");
        }
    } else
    {
        log_error(L"web_settings_general_checkupdates_handler::process_get. server_info_request failed");
        send_json_error_message(http_request_ptr, tcp_conn, "server_info_request failed");
    }
}

} // dvblex
