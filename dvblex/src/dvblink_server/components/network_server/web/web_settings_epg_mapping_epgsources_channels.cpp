/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_epg.h>
#include <dl_message_common.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "web_settings_epg_mapping_epgsources_channels.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_epg_mapping_epgsources_channels_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    if (http_request_ptr->has_query("id"))
    {
        std::string epg_source_id = http_request_ptr->get_query("id");

        epg::get_epg_channels_request ch_req;
        epg::get_epg_channels_response ch_resp;
        if (message_queue->send(dvblink::message_addressee_t(epg_source_id), ch_req, ch_resp) == success && ch_resp.result_)
        {
            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            writer.StartArray();
            epg_source_channel_map_t::iterator it = ch_resp.channels_.begin();
            while (it != ch_resp.channels_.end())
            {
                writer.StartObject();
                writer.Key("id");
                writer.String(it->second.id_.to_string().c_str());
                writer.Key("name");
                writer.String(it->second.name_.to_string().c_str());
                if (it->second.number_.get() > 0)
                {
                    writer.Key("number");
                    writer.Int(it->second.number_.get());
                    if (it->second.sub_number_.get() > 0)
                    {
                        writer.Key("subnumber");
                        writer.Int(it->second.sub_number_.get());
                    }
                }
                if (!it->second.logo_.empty())
                {
                    writer.Key("logo");
                    writer.String(it->second.logo_.to_string().c_str());
                }

                writer.EndObject();

                ++it;
            }
            writer.EndArray();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
        } else
        {
            dvblink::logging::log_error(L"web_settings_epg_mapping_epgsources_channels_handler. get_epg_channels_request failed");
            send_json_error_message(http_request_ptr, tcp_conn, "get_epg_channels_request failed");
        }
    } else
    {
        dvblink::logging::log_error(L"web_settings_epg_mapping_epgsources_channels_handler. missing epg source id parameter");
        send_json_error_message(http_request_ptr, tcp_conn, "missing epg source id parameter");
    }
}

} // dvblex
