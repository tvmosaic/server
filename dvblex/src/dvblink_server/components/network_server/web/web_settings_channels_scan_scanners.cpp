/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_message_devices.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "../common.h"
#include "../pion_helper.h"
#include "web_settings_channels_scan_scanners.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_channels_scan_scanners_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    if (http_request_ptr->has_query("device"))
    {
        std::string device_id = http_request_ptr->get_query("device");
        dvblink::standard_set_t standard = st_unknown;

        devices::get_scanners_request req(device_id, standard);
        devices::get_scanners_response resp;
        if (message_queue->send(source_manager_message_queue_addressee, req, resp) == success && resp.result_)
        {
            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            writer.StartArray();
            provider_info_map_t::const_iterator pim_it = resp.providers_.begin();
            while (pim_it != resp.providers_.end())
            {
                writer.StartObject();
                writer.Key("name");
                writer.String(pim_it->second.standard_name_.to_string().c_str());

                writer.Key("standard");
                writer.String(get_source_type_string_from_value(pim_it->second.standard_).c_str());

                if (!pim_it->second.parameters_.is_empty())
                    add_container_to_writer(pim_it->second.parameters_, "parameters", writer);

                writer.Key("providers");
                writer.StartArray();
                for (size_t i=0; i<pim_it->second.providers_.size(); i++)
                {
                    const provider_info_t& pi = pim_it->second.providers_[i];

                    writer.StartObject();
                    writer.Key("id");
                    writer.String(pi.id_.to_string().c_str());
                    writer.Key("name");
                    writer.String(pi.name_.to_string().c_str());
                    writer.Key("country");
                    writer.String(pi.country_.to_string().c_str());
                    writer.Key("description");
                    writer.String(pi.desc_.to_string().c_str());
                    if (!pi.param_container_.is_empty())
                        add_container_to_writer(pi.param_container_, "parameters", writer);
                    writer.EndObject();
                }
                writer.EndArray();

                writer.EndObject();
                ++pim_it;
            }
            writer.EndArray();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
        } else
        {
            log_error(L"web_settings_channels_scan_scanners_handler::process_get. devices_info_request failed");
            send_json_error_message(http_request_ptr, tcp_conn, "devices_info_request failed");
        }
    } else
    {
        log_error(L"web_settings_channels_scan_scanners_handler::process_get. missing id parameter");
        send_json_error_message(http_request_ptr, tcp_conn, "missing device parameter");
    }
}

} // dvblex
