/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_playback.h>
#include <dl_message_common.h>
#include <dl_locale_strings.h>
#include "../pion_helper.h"
#include "web_connect_tvrecordings_categories.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

struct playback_container_desc
{
    std::string id;
    std::string name;
    playback::pb_container_list_t container_list;
};

typedef std::vector<playback_container_desc> playback_container_desc_list_t;

static void add_containers_to_response(const playback_container_desc& cont_desc, rapidjson::Writer<rapidjson::StringBuffer>& writer)
{
    writer.StartObject();
    writer.Key("name");
    writer.String(cont_desc.name.c_str());
    writer.Key("items");

    writer.StartArray();

    for (size_t i=0; i<cont_desc.container_list.size(); i++)
    {
        writer.StartObject();
        writer.Key("id");
        writer.String(cont_desc.container_list[i].object_id_.to_string().c_str());
        writer.Key("name");
        writer.String(cont_desc.container_list[i].name_.to_string().c_str());
        writer.Key("length");
        writer.Int(cont_desc.container_list[i].total_count_);

        writer.EndObject();
    }

    writer.EndArray();

    writer.EndObject();
}

void web_connect_tvrecordings_categories_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    dvblink::messaging::playback::get_objects_request req;
    req.object_id_ = playback::make_object_id(recorded_tv_message_queue_addressee.to_string(), playback::object_root_id.get());
    req.object_type_ = playback::pot_object_container;
    req.is_children_request_ = true;
    req.server_address_ = get_server_ip_from_request(http_request_ptr);
    req.proto_ = context.stream_service->get_http_prefix();

    dvblink::messaging::playback::get_objects_response resp;
    if (message_queue->send(recorded_tv_message_queue_addressee, req, resp) == success && resp.result_)
    {
        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

        playback_container_desc sort_by_container;
        playback_container_desc_list_t second_level_containers;

        sort_by_container.id = "c769486c-561b-48b8-86a9-51965c141cf7";
        sort_by_container.name = language_settings::GetInstance()->GetItemName(IDS_CONNECT_TVRECORDINGS_SORT_BY);

        //walk through each first level item and ask if it has containers inside
        for (size_t i=0; i<resp.object_.container_list_.size(); i++)
        {
            req.object_id_ = resp.object_.container_list_[i].object_id_;
            req.object_type_ = playback::pot_object_container;
            req.is_children_request_ = true;
            req.server_address_ = get_server_ip_from_request(http_request_ptr);
            req.proto_ = context.stream_service->get_http_prefix();
            dvblink::messaging::playback::get_objects_response resp_2;
            if (message_queue->send(recorded_tv_message_queue_addressee, req, resp_2) == success && resp_2.result_)
            {
                if (resp_2.object_.container_list_.size() == 0)
                {
                    //add to "sort by" container
                    sort_by_container.container_list.push_back(resp.object_.container_list_[i]);
                } else
                {
                    //add to its own container
                    playback_container_desc cont_desc;
                    cont_desc.id = resp.object_.container_list_[i].object_id_.to_string();
                    cont_desc.name = resp.object_.container_list_[i].name_.to_string();
                    cont_desc.container_list = resp_2.object_.container_list_;
                    second_level_containers.push_back(cont_desc);
                }
            } else
            {
                log_warning(L"web_connect_tvrecordings_categories_handler::process_get. get_objects_request second level failed");
            }
        }
        writer.StartArray();
        if (sort_by_container.container_list.size() > 0)
            add_containers_to_response(sort_by_container, writer);

        for (size_t i=0; i<second_level_containers.size(); i++)
            add_containers_to_response(second_level_containers[i], writer);

        writer.EndArray();

        send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);

    } else
    {
        log_error(L"web_connect_tvrecordings_categories_handler::process_get. get_objects_request first level failed");
        send_json_error_message(http_request_ptr, tcp_conn, "get_objects_request first level failed");
    }
}

} // dvblex
