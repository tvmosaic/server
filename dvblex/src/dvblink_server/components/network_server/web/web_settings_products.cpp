/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_license.h>
#include <dl_message_common.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "web_settings_products.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

static const char* license_state_names[] = {"wrong_fingerprint", "free", "trial", "registered", "expired", "no_license_file", 
    "no_subscription", "subscribed", "subscription_expired", "subscription_wrong_fingerprint", "no_coupon", "coupon_wrong_fingerprint"};

void web_settings_products_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    license::get_installed_products_request req;
    license::get_installed_products_response resp;
    if (message_queue->send(server_message_queue_addressee, req, resp) == success && resp.result_.get())
    {
        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

        writer.StartArray();
        for (size_t i=0; i<resp.products_.size(); i++)
        {
            product_info_t& pi = resp.products_[i];

            writer.StartObject();
            writer.Key("id");
            writer.String(pi.id_.to_string().c_str());
            writer.Key("name");
            writer.String(pi.name_.to_string().c_str());
            writer.Key("url");
            writer.String(pi.url_.to_string().c_str());
            writer.Key("version");
            writer.String(pi.version_.to_string().c_str());
            writer.Key("revision");
            writer.String(pi.revision_.to_string().c_str());
            writer.Key("description");
            writer.String(pi.description_.to_string().c_str());
            writer.Key("trial_available");
            writer.Bool(pi.trial_available_.get());
            writer.Key("requires_registration");
            writer.Bool(pi.requires_registration_.get());
            writer.Key("requires_subscription");
            writer.Bool(pi.requires_subscription_.get());
            writer.Key("requires_coupon");
            writer.Bool(pi.requires_coupon_.get());
            writer.Key("activation_in_progress");
            writer.Bool(pi.activation_in_progress_.get());

            writer.Key("days_left");
            writer.Int(pi.days_left_);
            writer.Key("license_state");
            writer.String(license_state_names[pi.license_state_]);
            writer.Key("license_name");
            writer.String(pi.license_name_.to_string().c_str());
            writer.Key("license_key");
            writer.String(pi.license_key_.to_string().c_str());
            writer.Key("hw_fingerprint");
            writer.String(pi.hw_fingerprint_.to_string().c_str());
            writer.Key("machine_id");
            writer.String(pi.machine_id_.to_string().c_str());
//            writer.Key("product_file");
//            writer.String(pi.product_file_.to_string().c_str());

            writer.EndObject();
        }
        writer.EndArray();

        send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
    } else
    {
        log_error(L"web_settings_products_handler::process_get. get_installed_products_request failed");
        send_json_error_message(http_request_ptr, tcp_conn, "get_installed_products_request failed");
    }
}

} // dvblex
