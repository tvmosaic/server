/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_playback.h>
#include <dl_message_common.h>
#include <dl_locale_strings.h>
#include "../pion_helper.h"
#include "web_connect_library_programs.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_connect_library_programs_handler::add_pb_object_to_json(const playback::pb_object_t& result_object, rapidjson::Writer<rapidjson::StringBuffer>& writer)
{
    writer.StartObject();

    writer.Key("meta");
    writer.StartObject();
    writer.Key("total_entries");
    writer.Int(result_object.total_count_);
    writer.EndObject();

    writer.Key("programs");
    writer.StartArray();

    for (size_t i=0; i<result_object.item_list_.size(); i++)
    {
        boost::shared_ptr<playback::pb_item_t> item = result_object.item_list_[i];
        if (item->item_type_ == playback::pbit_item_video)
        {
            boost::shared_ptr<playback::pb_video_t> video = boost::static_pointer_cast<playback::pb_video_t>(item);

            writer.StartObject();

            //add pbitem info
            add_pbitem_to_json(item, writer);

            //add video info
            add_videoinfo_to_json(video->video_info_, writer);

            writer.EndObject();
        }
    }

    writer.EndArray();

    writer.EndObject();
}

void web_connect_library_programs_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    if (http_request_ptr->has_query("id"))
    {
        std::string category_id = http_request_ptr->get_query("id");

        std::string source_id;
        std::string source_object_id;
        if (!dvblex::playback::parse_object_id(category_id, source_id, source_object_id))
            log_warning(L"web_connect_library_programs_handler::process_get. failed to parse source id from filter %1%") % string_cast<EC_UTF8>(category_id);


        //all other parameters are optional
        std::string search_query = http_request_ptr->get_query("search");
        boost::int32_t start_position = playback::object_start_position;
        boost::int32_t requested_count = playback::object_count_all;

        try {
            if (http_request_ptr->has_query("count"))
                requested_count = boost::lexical_cast<boost::int32_t>(http_request_ptr->get_query("count"));
        } catch(...){}

        try {
            if (http_request_ptr->has_query("offset"))
                start_position = boost::lexical_cast<boost::int32_t>(http_request_ptr->get_query("offset"));
        } catch(...){}

        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

        if (search_query.empty())
        {
            dvblink::messaging::playback::get_objects_request req;
            req.object_id_ = category_id;
            req.object_type_ = playback::pot_object_item;
            req.is_children_request_ = true;
            req.requested_count_ = requested_count;
            req.start_position_ = start_position;
            req.server_address_ = get_server_ip_from_request(http_request_ptr);
            req.proto_ = context.stream_service->get_http_prefix();

            dvblink::messaging::playback::get_objects_response resp;
            if (message_queue->send(source_id, req, resp) == success && resp.result_)
            {
                add_pb_object_to_json(resp.object_, writer);
                send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
            } else
            {
                log_error(L"web_connect_library_programs_handler::process_get. get_objects_request failed");
                send_json_error_message(http_request_ptr, tcp_conn, "get_objects_request failed");
            }
        } else
        {
            dvblink::messaging::playback::search_objects_request req;
            req.object_id_ = category_id;
            req.object_type_ = playback::pot_object_item;
            req.search_string_ = search_query;
            req.server_address_ = get_server_ip_from_request(http_request_ptr);
            req.proto_ = context.stream_service->get_http_prefix();

            dvblink::messaging::playback::search_objects_response resp;
            if (message_queue->send(source_id, req, resp) == success && resp.result_)
            {
                add_pb_object_to_json(resp.object_, writer);
                send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
            } else
            {
                log_error(L"web_connect_library_programs_handler::process_get. search_objects_request failed");
                send_json_error_message(http_request_ptr, tcp_conn, "search_objects_request failed");
            }

        }
    } else
    {
        dvblink::logging::log_error(L"web_connect_library_programs_handler. missing category id parameter");
        send_json_error_message(http_request_ptr, tcp_conn, "missing category id parameter");
    }
}

} // dvblex
