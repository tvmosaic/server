/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_message_devices.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "../pion_helper.h"
#include "web_settings_channels_scan_rescan_start.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_channels_scan_rescan_start_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;
    if (http_request_ptr->get_content_length())
    {
        std::string response_str = http_request_ptr->get_content();

        rapidjson::LookaheadParser r(&response_str[0]);
        if (r.IsValid())
        {
            devices::rescan_provider_request req;

            r.EnterObject();
            while (const char* key = r.NextObjectKey())
            {
                if (strcmp(key, "device") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    req.device_id_ = r.GetString();
                } else
                if (strcmp(key, "provider") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    req.headend_id_ = r.GetString();
                } else
                if (strcmp(key, "values") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    read_concise_param_map_from_json(r, req.settings_);
                } else
                {
                    r.SkipValue();
                }
            }

            devices::rescan_provider_response resp;
            if (message_queue->send(source_manager_message_queue_addressee, req, resp) == success && resp.result_)
            {
                rapidjson::StringBuffer s;
                rapidjson::Writer<rapidjson::StringBuffer> writer(s);

                writer.StartObject();
                writer.Key("state");
                writer.String("started");
                writer.EndObject();

                send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);

                ok = true;
            } else
            {
                log_error(L"web_settings_channels_scan_rescan_start_handler::process_set. rescan_provider_request failed");
            }
        } else
        {
            log_error(L"web_settings_channels_scan_rescan_start_handler::process_set. unable to deserialize json request");
        }
    } else
    {
        log_error(L"web_settings_channels_scan_rescan_start_handler::process_set. post content of 0 length");
    }

    if (!ok)
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

} // dvblex
