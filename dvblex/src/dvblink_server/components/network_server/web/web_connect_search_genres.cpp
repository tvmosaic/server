/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_programs.h>
#include "web_connect_search_genres.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

using namespace recorder;

void web_connect_search_genres_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

        writer.StartArray();
        add_json_id_label_pair_localized((int)DRGC_NEWS, IDS_GENRE_NEWS, writer);
        add_json_id_label_pair_localized((int)DRGC_KIDS, IDS_GENRE_KIDS, writer);
        add_json_id_label_pair_localized((int)DRGC_MOVIE, IDS_GENRE_MOVIE, writer);
        add_json_id_label_pair_localized((int)DRGC_SPORT, IDS_GENRE_SPORT, writer);
        add_json_id_label_pair_localized((int)DRGC_DOCUMENTARY, IDS_GENRE_DOCUMENTARY, writer);
        add_json_id_label_pair_localized((int)DRGC_ACTION, IDS_GENRE_ACTION, writer);
        add_json_id_label_pair_localized((int)DRGC_COMEDY, IDS_GENRE_COMEDY, writer);
        add_json_id_label_pair_localized((int)DRGC_DRAMA, IDS_GENRE_DRAMA, writer);
        add_json_id_label_pair_localized((int)DRGC_EDU, IDS_GENRE_EDU, writer);
        add_json_id_label_pair_localized((int)DRGC_HORROR, IDS_GENRE_HORROR, writer);
        add_json_id_label_pair_localized((int)DRGC_MUSIC, IDS_GENRE_MUSIC, writer);
        add_json_id_label_pair_localized((int)DRGC_REALITY, IDS_GENRE_REALITY, writer);
        add_json_id_label_pair_localized((int)DRGC_ROMANCE, IDS_GENRE_ROMANCE, writer);
        add_json_id_label_pair_localized((int)DRGC_SCIFI, IDS_GENRE_SCIFI, writer);
        add_json_id_label_pair_localized((int)DRGC_SERIAL, IDS_GENRE_SERIAL, writer);
        add_json_id_label_pair_localized((int)DRGC_SOAP, IDS_GENRE_SOAP, writer);
        add_json_id_label_pair_localized((int)DRGC_SPECIAL, IDS_GENRE_SPECIAL, writer);
        add_json_id_label_pair_localized((int)DRGC_THRILLER, IDS_GENRE_THRILLER, writer);
        add_json_id_label_pair_localized((int)DRGC_ADULT, IDS_GENRE_ADULT, writer);
        writer.EndArray();

        send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
}

} // dvblex
