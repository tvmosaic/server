/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_network_server_params.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_xml_serialization.h>
#include <dl_network_helper.h>
#include <dl_http_comm.curl.h>
#include "../pion_helper.h"
#include "web_settings_network_checkports.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_network_checkports_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;

    //request current settings
    dvblink::messaging::xml_message_request request;
    request.server_address_ = get_server_ip_from_request(http_request_ptr);
    request.proto_ = context.stream_service->get_http_prefix();
    request.cmd_id_ = get_net_server_params_cmd;

    dvblink::messaging::xml_message_response response;

    if (message_queue->send(network_server_message_queue_addressee, request, response) == dvblink::messaging::success &&
        response.result_.to_string().compare(xmlcmd_result_success) == 0)
    {
        network_server_params nsp;
        read_from_xml(response.xml_.to_string(), nsp);

        if (nsp.ports_.size() > 0)
        {
            //TODO: create an external webpage for port checker. Check nsp.ports_[0] and nsp.ports_[1] - being base/http port and extension stream respectively

            log_error(L"web_settings_network_checkports_handler::process_get. Port check functionality is not implemented in TVMosaic CE");
        } else
        {
            log_error(L"web_settings_network_checkports_handler::process_get. no ports to check");
        }
    } else
    {
        log_error(L"web_settings_network_checkports_handler::process_get. get_net_server_params_cmd failed");
    }

    if (!ok)
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

} // dvblex
