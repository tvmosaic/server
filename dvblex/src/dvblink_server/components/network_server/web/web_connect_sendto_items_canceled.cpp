/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_send_to_target_cmd.h>
#include <dl_send_to_cmd.h>
#include <dl_message_common.h>
#include <dl_xml_serialization.h>
#include "../pion_helper.h"
#include "web_request_constants.h"
#include "web_connect_sendto_items_canceled.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;
using namespace dvblink::pugixml_helpers;

namespace dvblex {

void web_connect_sendto_items_canceled_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    if (http_request_ptr->get_content_length())
    {
        std::string response_str = http_request_ptr->get_content();

        //format: json array of strings
        rapidjson::LookaheadParser r(&response_str[0]);
        if (r.IsValid())
        {

            r.EnterArray();
            while (r.NextArrayValue())
            {
                send_to_cancel_item_request scir;
                scir.work_item_id_ = r.GetString();

                dvblink::messaging::xml_message_request request;
                request.server_address_ = get_server_ip_from_request(http_request_ptr);
                request.proto_ = context.stream_service->get_http_prefix();
                request.cmd_id_ = SEND_TO_CMD_CANCEL_ITEM;
                std::string str;
                write_to_xml(scir, str);
                request.xml_ = str;

                dvblink::messaging::xml_message_response response;

                if (message_queue->send(social_message_queue_addressee, request, response) != dvblink::messaging::success ||
                    response.result_.to_string().compare(xmlcmd_result_success) != 0)
                {
                    dvblink::logging::log_warning(L"web_connect_sendto_items_handler::process_post. SEND_TO_CMD_DELETE_ITEMS failed");
                }
            }
            send_json_ok_message(http_request_ptr, tcp_conn);
        } else
        {
            dvblink::logging::log_error(L"web_connect_sendto_items_handler::process_post. unable to parse input data");
            send_json_error_message(http_request_ptr, tcp_conn, "unable to parse input data");
        }
    } else
    {
        log_error(L"web_connect_sendto_items_handler::process_post. post content of 0 length");
        send_json_error_message(http_request_ptr, tcp_conn, "missing parameters");
    }
}

} // dvblex
