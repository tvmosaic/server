/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_logo_cmd.h>
#include <dl_message_common.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "../pion_helper.h"
#include "web_settings_channels_logos_logosearch.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_channels_logos_logosearch_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    if (http_request_ptr->has_query("pack") && http_request_ptr->has_query("channel"))
    {
        dvblink::logo_package_t package_id = http_request_ptr->get_query("pack");
        dvblink::channel_id_t channel_id = http_request_ptr->get_query("channel");

        match_logo_package_req rq;
        rq.package_ = package_id;
        rq.channels_.push_back(channel_id);

        std::string tmp;
        write_to_xml(rq, tmp);

        dvblink::messaging::xml_message_request request;
        request.cmd_id_ = match_package_logo_cmd;
        request.xml_ = tmp;
        request.server_address_ = get_server_ip_from_request(http_request_ptr);
        request.proto_ = context.stream_service->get_http_prefix();

        dvblink::messaging::xml_message_response response;
        if (message_queue->send(logo_manager_message_queue_addressee, request, response) == dvblink::messaging::success &&
            response.result_.to_string().compare(xmlcmd_result_success) == 0)
        {
            channel_logo_match_map_t match_info;
            read_from_xml(response.xml_.to_string(), match_info);

            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            writer.StartObject();
            channel_logo_match_map_t::iterator it = match_info.find(channel_id);
            if (it != match_info.end())
            {
                if (it->second.exact_match_.size() > 0)
                {
                    writer.Key("exact_match");
                    writer.StartObject();
                    writer.Key("id");
                    writer.String(it->second.exact_match_.at(0).to_string().c_str());
                    writer.EndObject();
                } else
                if (it->second.partial_match_.size() > 0)
                {
                    writer.Key("partial_match");
                    writer.StartArray();
                    for (size_t i=0; i<it->second.partial_match_.size(); i++)
                    {
                        writer.StartObject();
                        writer.Key("id");
                        writer.String(it->second.partial_match_.at(i).to_string().c_str());
                        writer.EndObject();
                    }
                    writer.EndArray();
                }
            }
            writer.EndObject();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
        } else
        {
            log_error(L"web_settings_channels_logos_logosearch_handler::process_get. match_package_logo_cmd failed");
            send_json_error_message(http_request_ptr, tcp_conn, "match_package_logo_cmd failed");
        }
    } else
    {
        dvblink::logging::log_error(L"web_settings_channels_logos_logosearch_handler. missing package and/or channel id parameter");
        send_json_error_message(http_request_ptr, tcp_conn, "missing package  and/or channel id parameter");
    }
}

} // dvblex
