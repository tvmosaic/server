/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_server_params.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_message_channels.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "web_settings_channels_visibility_channels.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_channels_visibility_channels_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;
    if (http_request_ptr->get_content_length())
    {
        std::string response = http_request_ptr->get_content();

        rapidjson::LookaheadParser r(&response[0]);
        if (r.IsValid())
        {
            channels::set_channel_visibility_request req;

            r.EnterObject();
            while (const char* key = r.NextObjectKey())
            {
                if (strcmp(key, "invisible") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    r.EnterArray();
                    while (r.NextArrayValue()) 
                    {
                        r.EnterObject();
                        while (const char* invisible_key = r.NextObjectKey())
                        {
                            if (strcmp(invisible_key, "id") == 0 && r.PeekType() != rapidjson::kNullType)
                            {
                                std::string str = r.GetString();
                                req.channel_visibility_[str] = str;
                            } else
                            {
                                r.SkipValue();
                            }
                        }
                    }
                } else
                {
                    r.SkipValue();
                }
            }

            channels::set_channel_visibility_response resp;
            if (message_queue->send(source_manager_message_queue_addressee, req, resp) == success && resp.result_)
            {
                ok = true;
            } else
            {
                log_error(L"web_settings_channels_visibility_channels_handler::process_set. set_channel_visibility_response failed");
            }
        } else
        {
            log_error(L"web_settings_channels_visibility_channels_handler::process_set. unable to deserialize json request");
        }
    } else
    {
        log_error(L"web_settings_channels_visibility_channels_handler::process_set. post content of 0 length");
    }

    if (ok)
        send_json_ok_message(http_request_ptr, tcp_conn);
    else
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

void web_settings_channels_visibility_channels_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    std::string headend_id;
    if (http_request_ptr->has_query("provider"))
        headend_id = http_request_ptr->get_query("provider");

    std::string transponder_id;
    if (http_request_ptr->has_query("transponder"))
        transponder_id = http_request_ptr->get_query("transponder");

    channels::get_scanned_channels_request req;
    req.info_level_ = channels::get_scanned_channels_level_channels;
    req.headend_id_ = headend_id;
    req.transponder_id_ = transponder_id;

    channels::get_scanned_channels_response resp;
    if (message_queue->send(source_manager_message_queue_addressee, req, resp) == success && resp.result_)
    {
        channels::get_channel_visibility_request vis_req;
        channels::get_channel_visibility_response vis_resp;
        if (message_queue->send(source_manager_message_queue_addressee, vis_req, vis_resp) == success && vis_resp.result_)
        {
            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            writer.StartArray();
            for (size_t i=0; i<resp.headends_.size(); i++)
            {
                concise_channel_headend_t& headend = resp.headends_.at(i);
                for (size_t j=0; j<headend.transponders_.size(); j++)
                {
                    concise_transponder_description_t& transponder = headend.transponders_.at(j);
                    for (size_t k=0; k<transponder.channels_.size(); k++)
                    {
                        concise_device_channel_t& channel = transponder.channels_.at(k);

                        writer.StartObject();
                        writer.Key("comment");
                        writer.String(channel.comment_.to_string().c_str());
                        writer.Key("encrypted");
                        writer.Bool(channel.encrypted_.get());
                        writer.Key("id");
                        writer.String(channel.id_.to_string().c_str());
                        writer.Key("name");
                        writer.String(channel.name_.to_string().c_str());
                        writer.Key("origin");
                        writer.String(channel.origin_.to_string().c_str());
                        writer.Key("provider");
                        writer.String(headend.id_.to_string().c_str());
                        std::string numstr;
                        if (get_number_as_string(channel.num_, channel.sub_num_, numstr))
                        {
                            writer.Key("number");
                            writer.String(numstr.c_str());
                        }
                        writer.Key("transponder");
                        writer.String(transponder.tr_id_.to_string().c_str());
                        writer.Key("type");
                        if (channel.type_ == ct_tv)
                            writer.String("TV");
                        else
                            writer.String("Radio");

                        writer.Key("visible");
                        if (vis_resp.channel_visibility_.find(channel.id_) == vis_resp.channel_visibility_.end())
                            writer.Bool(true);
                        else
                            writer.Bool(false);

                        writer.EndObject();

                    }
                }
            }
            writer.EndArray();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
        } else
        {
            log_error(L"web_settings_channels_visibility_channels_handler::process_get. get_channel_visibility_request failed");
            send_json_error_message(http_request_ptr, tcp_conn, "get_channel_visibility_request failed");
        }
    } else
    {
        log_error(L"web_settings_channels_visibility_channels_handler::process_get. get_scanned_channels_request failed");
        send_json_error_message(http_request_ptr, tcp_conn, "get_scanned_channels_request failed");
    }
}

} // dvblex
