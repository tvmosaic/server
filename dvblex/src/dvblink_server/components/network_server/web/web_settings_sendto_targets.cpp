/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_send_to_target_cmd.h>
#include <dl_message_common.h>
#include <dl_xml_serialization.h>
#include "../pion_helper.h"
#include "web_settings_sendto_targets.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;
using namespace dvblink::pugixml_helpers;

namespace dvblex {

void web_settings_sendto_targets_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;
    if (http_request_ptr->get_content_length())
    {
        std::string response_str = http_request_ptr->get_content();

        rapidjson::LookaheadParser r(&response_str[0]);
        if (r.IsValid())
        {
            send_to_set_targets_request sstr;

            r.EnterArray();
            while (r.NextArrayValue()) 
            {
                send_to_target_info sti;

                r.EnterObject();
                while (const char* key = r.NextObjectKey())
                {
                    if (strcmp(key, "id") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        sti.id = r.GetString();
                    } else
                    if (strcmp(key, "name") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        sti.name = r.GetString();
                    } else
                    if (strcmp(key, "default") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        sti.default_ = r.GetBool();
                    } else
                    if (strcmp(key, "delete_on_success") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        sti.delete_on_success = r.GetBool();
                    } else
                    if (strcmp(key, "use_comskip") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        sti.use_comskip = r.GetBool();
                    } else
                    if (strcmp(key, "comskip_params") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        read_concise_param_map_from_json(r, sti.comskip_params);
                    } else
                    if (strcmp(key, "dst_id") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        sti.dest_info.id = r.GetString();
                    } else
                    if (strcmp(key, "dst_params") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        convert_json_to_xml(r, sti.dest_info.params);
                    } else
                    if (strcmp(key, "fmt_id") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        sti.formatter_info.id = r.GetString();
                    } else
                    if (strcmp(key, "fmt_params") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        convert_json_to_xml(r, sti.formatter_info.params);
                    } else
                    {
                        r.SkipValue();
                    }
                }
                sstr.targets_.push_back(sti);
            }

            dvblink::messaging::xml_message_request request;
            request.server_address_ = get_server_ip_from_request(http_request_ptr);
            request.proto_ = context.stream_service->get_http_prefix();
            request.cmd_id_ = SEND_TO_TARGET_CMD_SET_TARGETS;
            std::string str;
            write_to_xml(sstr, str);
            request.xml_ = str;

            dvblink::messaging::xml_message_response response;

            if (message_queue->send(social_message_queue_addressee, request, response) == dvblink::messaging::success &&
                response.result_.to_string().compare(xmlcmd_result_success) == 0)
            {
                ok = true;
            } else
            {
                log_error(L"web_settings_sendto_targets_handler::process_post. SEND_TO_TARGET_CMD_SET_TARGETS failed");
            }

        } else
        {
            log_error(L"web_settings_sendto_targets_handler::process_post. unable to deserialize json request");
        }
    } else
    {
        log_error(L"web_settings_sendto_targets_handler::process_post. post content of 0 length");
    }

    if (ok)
        send_json_ok_message(http_request_ptr, tcp_conn);
    else
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

void web_settings_sendto_targets_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    get_sendto_targets(message_queue, http_request_ptr, tcp_conn, context);
}

} // dvblex
