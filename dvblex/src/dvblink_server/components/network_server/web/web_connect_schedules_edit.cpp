/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_recorder.h>
#include <dl_message_common.h>
#include <dl_locale_strings.h>
#include <dl_program_serializer.h>
#include <dl_timer_serializer.h>
#include <dl_schedule_serializer.h>
#include <dl_schedules.h>
#include <dl_xml_serialization.h>
#include <dl_programs.h>
#include "web_request_constants.h"
#include "../pion_helper.h"
#include "web_connect_schedules_edit.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_connect_schedules_edit_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;
    if (http_request_ptr->get_content_length())
    {
        std::string response_str = http_request_ptr->get_content();

        rapidjson::LookaheadParser r(&response_str[0]);
        if (r.IsValid())
        {
            recorder::update_schedule_item_info usi;
            bool return_timers = false;

            r.EnterObject();
            while (const char* key = r.NextObjectKey())
            {
                if (strcmp(key, "schedule") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    usi.schedule_item_id_ = boost::lexical_cast<boost::int32_t>(r.GetString());
                } else
                if (strcmp(key, "targets") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    r.EnterArray();
                    while (r.NextArrayValue()) 
                    {
                        usi.targets_.push_back(r.GetString());
                    }
                } else
                if (strcmp(key, "marginAfter") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    usi.margin_after_ = r.GetInt();
                } else
                if (strcmp(key, "marginBefore") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    usi.margin_before_ = r.GetInt();
                } else
                if (strcmp(key, "priority") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    usi.priority_ = r.GetInt();
                } else
                if (strcmp(key, "active") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    usi.active_ = r.GetBool();
                } else
                if (strcmp(key, "recordingsToKeep") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    usi.number_of_recordings_to_keep_ = r.GetInt();
                } else
                if (strcmp(key, "recordNewOnly") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    usi.record_series_new_only_ = r.GetBool();
                } else
                if (strcmp(key, "startBefore") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    usi.start_before_sec_ = r.GetInt();
                } else
                if (strcmp(key, "startAfter") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    usi.start_after_sec_ = r.GetInt();
                } else
                if (strcmp(key, "days") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    r.EnterArray();
                    while (r.NextArrayValue()) 
                    {
                        int mask = r.GetInt();
                        if (mask > 0)
                            usi.day_mask_ |= (1 << (mask - 1));
                    }
                } else
                {
                    r.SkipValue();
                }
            }

            std::string str;
            write_to_xml(usi, str);

            dvblink::messaging::recorder::update_schedule_request req(str);
            dvblink::messaging::recorder::update_schedule_response resp;
            ok = message_queue->send(recorder_message_queue_addressee, req, resp) == success && resp.result_;

            if (ok)
            {
                if (return_timers)
                {
                    messaging::recorder::get_recordings_request req;
                    messaging::recorder::get_recordings_response resp;
                    if (message_queue->send(recorder_message_queue_addressee, req, resp) == success && resp.result_)
                    {
                        rd_recording_list_t recordings;
                        read_from_xml(resp.recordings_.to_string(), recordings);

                        rapidjson::StringBuffer s;
                        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

                        add_timers_as_array(recordings, NULL, writer);

                        send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
                    } else
                    {
                        log_error(L"web_connect_schedules_edit_handler::process_post. get_recordings_request failed");
                    }
                } else
                {
                    send_json_ok_message(http_request_ptr, tcp_conn);
                }
            } else
            {
                log_error(L"web_connect_schedules_edit_handler::process_post. update_schedule_request failed");
            }
        } else
        {
            log_error(L"web_connect_schedules_edit_handler::process_post. unable to deserialize json request");
        }
    } else
    {
        log_error(L"web_connect_schedules_edit_handler::process_post. post content of 0 length");
    }

    if (!ok)
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}


} // dvblex
