/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_logo_cmd.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "../common.h"
#include "../pion_helper.h"
#include "web_settings_channels_logos_channels.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_channels_logos_channels_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;
    if (http_request_ptr->get_content_length())
    {
        std::string response_str = http_request_ptr->get_content();

        rapidjson::LookaheadParser r(&response_str[0]);
        if (r.IsValid())
        {
            channel_logo_id_map_t logo_id_map;

            r.EnterArray();
            while (r.NextArrayValue()) 
            {
                std::string channel_id;
                std::string logo_id;

                r.EnterObject();
                while (const char* key = r.NextObjectKey())
                {
                    if (strcmp(key, "id") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        channel_id = r.GetString();
                    } else
                    if (strcmp(key, "logo_id") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        logo_id = r.GetString();
                    } else
                    {
                        r.SkipValue();
                    }
                }
                logo_id_map[channel_id] = logo_id;
            }

            std::string tmp;
            write_to_xml(logo_id_map, tmp);

            dvblink::messaging::xml_message_request request;
            request.server_address_ = get_server_ip_from_request(http_request_ptr);
            request.proto_ = context.stream_service->get_http_prefix();
            request.cmd_id_ = set_channel_logo_cmd;
            request.xml_ = tmp;

            dvblink::messaging::xml_message_response response;
            if (message_queue->send(logo_manager_message_queue_addressee, request, response) == dvblink::messaging::success &&
                response.result_.to_string().compare(xmlcmd_result_success) == 0)
            {
                ok = true;
            } else
            {
                log_error(L"web_settings_channels_logos_channels_handler::process_set. set_channel_logo_cmd failed");
            }
        } else
        {
            log_error(L"web_settings_channels_logos_channels_handler::process_set. unable to deserialize json request");
        }
    } else
    {
        log_error(L"web_settings_channels_logos_channels_handler::process_set. post content of 0 length");
    }

    if (ok)
        send_json_ok_message(http_request_ptr, tcp_conn);
    else
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

void web_settings_channels_logos_channels_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    std::string server_address = get_server_ip_from_request(http_request_ptr);

    //get channels
    channel_desc_list_t cdl;
    dvblink::favorite_id_t favorite_id;
    if (ns_get_channel_descriptions(favorite_id, message_queue, context.settings, context.stream_service, server_address, cdl))
    {
        dvblink::messaging::xml_message_request request;
        request.server_address_ = get_server_ip_from_request(http_request_ptr);
        request.proto_ = context.stream_service->get_http_prefix();
        request.cmd_id_ = get_channel_logo_cmd;

        dvblink::messaging::xml_message_response response;
        if (message_queue->send(logo_manager_message_queue_addressee, request, response) == dvblink::messaging::success &&
            response.result_.to_string().compare(xmlcmd_result_success) == 0)
        {
            channel_logo_desc_map_t channel_logo_map;
            read_from_xml(response.xml_.to_string(), channel_logo_map);

            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            writer.StartArray();
            for (size_t i=0; i<cdl.size(); i++)
            {
                writer.StartObject();
                add_channel_to_object(cdl[i], writer);

                writer.Key("logo");
                writer.StartObject();
                //find epg source for this channel
                channel_logo_desc_map_t::iterator logo_it = channel_logo_map.find(cdl[i].id_);
                if (logo_it != channel_logo_map.end())
                {
                    if (!logo_it->second.default_logo_.empty())
                    {
                        writer.Key("default");
                        writer.String(logo_it->second.default_logo_.to_string().c_str());
                    }
                    writer.Key("id");
                    writer.String(logo_it->second.id_.to_string().c_str());
                    writer.Key("url");
                    writer.String(logo_it->second.url_.to_string().c_str());
                }
                writer.EndObject();

                writer.EndObject();
            }
            writer.EndArray();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
        } else
        {
            log_error(L"web_settings_channels_logos_channels_handler::process_get. get_channel_logo_cmd failed");
            send_json_error_message(http_request_ptr, tcp_conn, "get_channel_logo_cmd failed");
        }
    } else
    {
        log_error(L"web_settings_channels_logos_channels_handler::process_get. ns_get_channel_descriptions failed");
        send_json_error_message(http_request_ptr, tcp_conn, "ns_get_channel_descriptions failed");
    }
}

} // dvblex
