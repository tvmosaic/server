/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_playback.h>
#include <dl_message_common.h>
#include <dl_locale_strings.h>
#include "../pion_helper.h"
#include "web_connect_stream.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

std::string web_connect_stream_handler::get_channel_url(web_request_context& context, const std::string& channel_id, const std::string& server_address, const std::string& client_id)
{
    return context.stream_service->get_direct_streaming_url(channel_id, server_address, client_id);
}

std::string web_connect_stream_handler::get_object_url(dvblink::messaging::message_queue_t& message_queue, web_request_context& context, const std::string& object_id, const std::string& server_address, const std::string& client_id)
{
    std::string url;

    std::string source_id;
    std::string source_object_id;
    if (dvblex::playback::parse_object_id(object_id, source_id, source_object_id))
    {
        dvblink::messaging::playback::get_objects_request req;
        req.object_id_ = object_id;
        req.object_type_ = playback::pot_object_item;
        req.is_children_request_ = false;
        req.server_address_ = server_address;
        req.proto_ = context.stream_service->get_http_prefix();

        dvblink::messaging::playback::get_objects_response resp;
        if (message_queue->send(source_id, req, resp) == success && resp.result_ && resp.object_.item_list_.size() == 1)
        {
            boost::shared_ptr<playback::pb_item_t> item = resp.object_.item_list_[0];

            url = item->url_.to_string();
        } else
        {
            log_error(L"web_connect_stream_handler::get_object_url. get_objects_request failed");
        }
    } else
    {
        log_error(L"web_connect_stream_handler::get_object_url. failed to parse source id from object_id %1%") % string_cast<EC_UTF8>(object_id);
    }
    return url;
}

void web_connect_stream_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;

    std::string client_addr = tcp_conn->get_remote_ip().to_string();
    context.stream_service->add_client_ip(client_addr);

    if (http_request_ptr->get_content_length())
    {
        std::string response_str = http_request_ptr->get_content();

        rapidjson::LookaheadParser r(&response_str[0]);
        if (r.IsValid())
        {
            std::string client_id;
            std::string channel_id;
            std::string object_id;
            boost::int32_t bitrate = 0;
            boost::int32_t scale_factor = 0;
            std::string lang;
            std::string format;

            r.EnterObject();
            while (const char* key = r.NextObjectKey())
            {
                if (strcmp(key, "client") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    client_id = r.GetString();
                } else
                if (strcmp(key, "channel") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    channel_id = r.GetString();
                } else
                if (strcmp(key, "recording") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    object_id = r.GetString();
                } else

                if (strcmp(key, "params") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    r.EnterObject();
                    while (const char* params_key = r.NextObjectKey())
                    {
                        if (strcmp(params_key, "bitrate") == 0 && r.PeekType() != rapidjson::kNullType)
                        {
                            bitrate = r.GetInt();
                        } else
                        if (strcmp(params_key, "scaleFactor") == 0 && r.PeekType() != rapidjson::kNullType)
                        {
                            scale_factor = r.GetInt();
                        } else
                        if (strcmp(params_key, "format") == 0 && r.PeekType() != rapidjson::kNullType)
                        {
                            format = r.GetString();
                        } else
                        if (strcmp(params_key, "lang") == 0 && r.PeekType() != rapidjson::kNullType)
                        {
                            lang = r.GetString();
                        } else
                        {
                            r.SkipValue();
                        }
                    }
                } else
                {
                    r.SkipValue();
                }
            }

            if (!client_id.empty() && (!channel_id.empty() || !object_id.empty()) && !format.empty())
            {
                std::string server_address = get_server_ip_from_request(http_request_ptr);

                //get object / live url 
                std::string url;
                if (!channel_id.empty())
                    url = get_channel_url(context, channel_id, server_address, client_id);
                else
                    url = get_object_url(message_queue, context, object_id, server_address, client_id);

                if (!url.empty())
                {
                    //form transcoded url request
                    std::stringstream buffer;
                    buffer << url << "&" << request_param_transcoder << "=" << format << "&client_id=" << client_id;
                    if (bitrate > 0)
                        buffer << "&" << request_param_transcoder_bitrate << "=" << bitrate;
                    if (scale_factor > 0)
                        buffer << "&" << request_param_transcoder_scale << "=" << scale_factor;
                    if (!lang.empty())
                        buffer << "&" << request_param_transcoder_lng << "=" << lang;

                    rapidjson::StringBuffer s;
                    rapidjson::Writer<rapidjson::StringBuffer> writer(s);

                    writer.StartObject();
                    writer.Key("url");
                    writer.String(buffer.str().c_str());
                    writer.EndObject();

                    send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
                    ok = true;
                }
            } else
            {
                //error
                log_error(L"web_connect_stream_handler::process_set. One of the required parameters is empty (%1%, %2%, %3%, %4%)") % string_cast<EC_UTF8>(client_id) % string_cast<EC_UTF8>(channel_id) % string_cast<EC_UTF8>(object_id) % string_cast<EC_UTF8>(format);
            }
        } else
        {
            log_error(L"web_connect_stream_handler::process_set. unable to deserialize json request");
        }
    } else
    {
        log_error(L"web_connect_stream_handler::process_set. post content of 0 length");
    }

    if (!ok)
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

} // dvblex
