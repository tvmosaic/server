/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_playback.h>
#include <dl_message_common.h>
#include "web_connect_tvrecordings_stop.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_connect_tvrecordings_stop_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    if (http_request_ptr->has_query("id"))
    {
        std::string recording_id = http_request_ptr->get_query("id");

        dvblink::messaging::playback::stop_recording_request req;
        req.object_id_ = recording_id;

        dvblink::messaging::playback::stop_recording_response resp;
        if (message_queue->send(recorded_tv_message_queue_addressee, req, resp) == success && resp.result_)
        {
            send_json_ok_message(http_request_ptr, tcp_conn);
        } else
        {
            log_error(L"web_connect_tvrecordings_stop_handler::process_get. stop_recording_request failed");
            send_json_error_message(http_request_ptr, tcp_conn, "stop_recording_request failed");
        }
    } else
    {
        dvblink::logging::log_error(L"web_connect_tvrecordings_stop_handler. missing recording id parameter");
        send_json_error_message(http_request_ptr, tcp_conn, "missing recording id parameter");
    }
}

} // dvblex
