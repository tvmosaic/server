/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_playback.h>
#include <dl_message_common.h>
#include <dl_locale_strings.h>
#include "../pion_helper.h"
#include "web_connect_library_details.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_connect_library_details_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    if (http_request_ptr->has_query("id"))
    {
        std::string program_id = http_request_ptr->get_query("id");

        std::string source_id;
        std::string source_object_id;
        if (!dvblex::playback::parse_object_id(program_id, source_id, source_object_id))
            log_warning(L"web_connect_library_details_handler::process_get. failed to parse source id from program %1%") % string_cast<EC_UTF8>(program_id);


        dvblink::messaging::playback::get_objects_request req;
        req.object_id_ = program_id;
        req.object_type_ = playback::pot_object_item;
        req.is_children_request_ = false;
        req.server_address_ = get_server_ip_from_request(http_request_ptr);
        req.proto_ = context.stream_service->get_http_prefix();

        dvblink::messaging::playback::get_objects_response resp;
        if (message_queue->send(source_id, req, resp) == success && resp.result_ && resp.object_.item_list_.size() == 1)
        {
            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            boost::shared_ptr<playback::pb_item_t> item = resp.object_.item_list_[0];
            if (item->item_type_ == playback::pbit_item_video)
            {
                boost::shared_ptr<playback::pb_video_t> video = boost::static_pointer_cast<playback::pb_video_t>(item);

                writer.StartObject();

                //add pbitem info
                add_pbitem_to_json(item, writer);

                //add video info
                add_videoinfo_to_json(video->video_info_, writer);

                writer.EndObject();
            }

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
        } else
        {
            log_error(L"web_connect_library_details_handler::process_get. get_objects_request failed");
            send_json_error_message(http_request_ptr, tcp_conn, "get_objects_request failed");
        }
    } else
    {
        dvblink::logging::log_error(L"web_connect_library_details_handler. missing program id parameter");
        send_json_error_message(http_request_ptr, tcp_conn, "missing program id parameter");
    }
}

} // dvblex
