/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_send_to_target_cmd.h>
#include <dl_send_to_cmd.h>
#include <dl_message_common.h>
#include <dl_xml_serialization.h>
#include "../pion_helper.h"
#include "web_request_constants.h"
#include "web_connect_sendto_items.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;
using namespace dvblink::pugixml_helpers;

namespace dvblex {

void web_connect_sendto_items_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;
    if (http_request_ptr->get_content_length())
    {
        std::string response_str = http_request_ptr->get_content();

        rapidjson::LookaheadParser r(&response_str[0]);
        if (r.IsValid())
        {
            dvblink::base_id_t target_id;
            send_to_add_item_request sair;

            r.EnterObject();
            while (const char* key = r.NextObjectKey())
            {
                if (strcmp(key, "target") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    target_id = r.GetString();
                } else
                if (strcmp(key, "recordings") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    r.EnterArray();
                    while (r.NextArrayValue()) 
                    {
                        send_to_work_item swi;
                        swi.pb_object_id = r.GetString();
                        sair.work_items_.push_back(swi);
                    }
                } else
                {
                    r.SkipValue();
                }
            }

            for (size_t i=0; i<sair.work_items_.size(); i++)
                sair.work_items_[i].target_id = target_id;

            dvblink::messaging::xml_message_request request;
            request.server_address_ = get_server_ip_from_request(http_request_ptr);
            request.proto_ = context.stream_service->get_http_prefix();
            request.cmd_id_ = SEND_TO_CMD_ADD_ITEM;
            std::string str;
            write_to_xml(sair, str);
            request.xml_ = str;

            dvblink::messaging::xml_message_response response;

            if (message_queue->send(social_message_queue_addressee, request, response) == dvblink::messaging::success &&
                response.result_.to_string().compare(xmlcmd_result_success) == 0)
            {
                ok = true;
            } else
            {
                log_error(L"web_connect_sendto_items_handler::process_post. SEND_TO_CMD_ADD_ITEM failed");
            }

        } else
        {
            log_error(L"web_connect_sendto_items_handler::process_post. unable to deserialize json request");
        }
    } else
    {
        log_error(L"web_connect_sendto_items_handler::process_post. post content of 0 length");
    }

    if (ok)
        send_json_ok_message(http_request_ptr, tcp_conn);
    else
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

void web_connect_sendto_items_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    send_to_get_items_request sgir;

    dvblink::messaging::xml_message_request request;
    request.server_address_ = get_server_ip_from_request(http_request_ptr);
    request.proto_ = context.stream_service->get_http_prefix();
    request.cmd_id_ = SEND_TO_CMD_GET_ITEMS;

    std::string xml_req;
    write_to_xml(sgir, xml_req);
    request.xml_ = xml_req;

    dvblink::messaging::xml_message_response response;

    if (message_queue->send(dvblink::messaging::social_message_queue_addressee, request, response) == dvblink::messaging::success &&
        response.result_.to_string().compare(dvblink::xmlcmd_result_success) == 0)
    {
        send_to_get_items_response stgi;
        read_from_xml(response.xml_.to_string(), stgi);

        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

        writer.StartArray();

        for (size_t i=0; i<stgi.work_items_.size(); i++)
        {
            const send_to_work_item& swi = stgi.work_items_.at(i);

            writer.StartObject();

            writer.Key("id");
            writer.String(swi.item_id.to_string().c_str());
            writer.Key("pb_object_id");
            writer.String(swi.pb_object_id.to_string().c_str());
            writer.Key("description");
            writer.String(swi.description.c_str());
            writer.Key("created");
            writer.Int64(swi.creation_time);
            writer.Key("completed");
            writer.Int64(swi.completion_time);
            writer.Key("target_id");
            writer.String(swi.target_id.to_string().c_str());
            writer.Key("status");
            writer.String(sendto_status_names[swi.status]);

            writer.EndObject();
        }

        writer.EndArray();

        send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
    } else
    {
        dvblink::logging::log_error(L"web_connect_sendto_items_handler::get_sendto_items. SEND_TO_CMD_GET_ITEMS failed");
        send_json_error_message(http_request_ptr, tcp_conn, "SEND_TO_CMD_GET_ITEMS failed");
    }
}

void web_connect_sendto_items_handler::process_delete(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    if (http_request_ptr->get_content_length())
    {
        std::string response_str = http_request_ptr->get_content();

        //format: json array of strings
        rapidjson::LookaheadParser r(&response_str[0]);
        if (r.IsValid())
        {
            send_to_delete_item_request sdir;

            r.EnterArray();
            while (r.NextArrayValue()) 
                sdir.item_ids_.push_back(r.GetString());

            dvblink::messaging::xml_message_request request;
            request.server_address_ = get_server_ip_from_request(http_request_ptr);
            request.proto_ = context.stream_service->get_http_prefix();
            request.cmd_id_ = SEND_TO_CMD_DELETE_ITEMS;
            std::string str;
            write_to_xml(sdir, str);
            request.xml_ = str;

            dvblink::messaging::xml_message_response response;

            if (message_queue->send(social_message_queue_addressee, request, response) == dvblink::messaging::success &&
                response.result_.to_string().compare(xmlcmd_result_success) == 0)
            {
                send_json_ok_message(http_request_ptr, tcp_conn);
            } else
            {
                dvblink::logging::log_error(L"web_connect_sendto_items_handler::process_delete. SEND_TO_CMD_DELETE_ITEMS failed");
                send_json_error_message(http_request_ptr, tcp_conn, "SEND_TO_CMD_DELETE_ITEMS failed");
            }
        } else
        {
            dvblink::logging::log_error(L"web_connect_sendto_items_handler::process_delete. unable to parse input data");
            send_json_error_message(http_request_ptr, tcp_conn, "unable to parse input data");
        }
    } else
    {
        log_error(L"web_connect_sendto_items_handler::process_delete. delete content of 0 length");
        send_json_error_message(http_request_ptr, tcp_conn, "missing parameters");
    }
}

} // dvblex
