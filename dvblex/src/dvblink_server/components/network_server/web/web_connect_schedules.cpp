/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_recorder.h>
#include <dl_message_common.h>
#include <dl_locale_strings.h>
#include <dl_program_serializer.h>
#include <dl_timer_serializer.h>
#include <dl_schedule_serializer.h>
#include <dl_schedules.h>
#include <dl_xml_serialization.h>
#include <dl_programs.h>
#include "web_request_constants.h"
#include "../pion_helper.h"
#include "web_connect_schedules.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_connect_schedules_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;

    //request recording timers
    messaging::recorder::get_recordings_request req;
    messaging::recorder::get_recordings_response resp;
    if (message_queue->send(recorder_message_queue_addressee, req, resp) == success && resp.result_)
    {
        rd_recording_list_t recordings;
        read_from_xml(resp.recordings_.to_string(), recordings);
        
        //request schedules
        messaging::recorder::get_schedule_request req;
        messaging::recorder::get_schedule_response resp;
        if (message_queue->send(recorder_message_queue_addressee, req, resp) == success && resp.result_)
        {
            recorder::schedule_list_t schedules;
            read_from_xml(resp.schedule_.to_string(), schedules);

            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            writer.StartArray();

            for (size_t i=0; i<schedules.size(); i++)
            {
                writer.StartObject();

                writer.Key("id");
                std::string str = boost::lexical_cast<std::string>(schedules[i].schedule_item_id_.get());
                writer.String(str.c_str());
                writer.Key("type");
                writer.String(schedule_type_names[schedules[i].type_]);
                writer.Key("channel");
                writer.String(schedules[i].channel_.to_string().c_str());
                writer.Key("recordingsToKeep");
                writer.Int(schedules[i].number_of_recordings_to_keep_);
                writer.Key("marginBefore");
                writer.Int(schedules[i].margin_before_);
                writer.Key("marginAfter");
                writer.Int(schedules[i].margin_after_);
                writer.Key("startBefore");
                writer.Int(schedules[i].start_before_sec_);
                writer.Key("startAfter");
                writer.Int(schedules[i].start_after_sec_);
                writer.Key("active");
                writer.Bool(schedules[i].active_);
                writer.Key("priority");
                writer.Int(schedules[i].priority_);
                
                writer.Key("targets");
                writer.StartArray();
                for (size_t t=0; t<schedules[i].targets_.size(); t++)
                    writer.String(schedules[i].targets_[t].to_string().c_str());
                writer.EndArray();

                writer.Key("name");
                writer.String(schedules[i].get_schedule_name().to_string().c_str());
                writer.Key("days");
                writer.StartArray();
                {
                    int c = 1;
                    for (int d=1; d<=7; d++)
                    {
                        if ((schedules[i].day_mask_ & c) != 0)
                            writer.Int(d);

                        c = c << 1;
                    }
                }
                writer.EndArray();

                switch (schedules[i].type_)
                {
                case recorder::sitManual:
                    {
                        writer.Key("startTime");
                        writer.Int(schedules[i].start_time_);
                        writer.Key("endTime");
                        writer.Int(schedules[i].start_time_ + schedules[i].duration_);
                    }
                    break;
                case recorder::sitEpgBased:
                    {
                        writer.Key("program");
                        writer.String(schedules[i].epg_event_id_.to_string().c_str());
                        writer.Key("repeat");
                        writer.Bool(schedules[i].record_series_);
                        writer.Key("recordNewOnly");
                        writer.Bool(schedules[i].record_series_new_only_);
                    }
                    break;
                case recorder::sitPatternBased:
                    {
                        writer.Key("query");
                        writer.String(schedules[i].key_phrase_.to_string().c_str());
                        writer.Key("genres");
                        writer.StartArray();
                        int g = recorder::DRGC_NEWS;
                        while (g <= recorder::DRGC_LAST)
                        {
                            if ((schedules[i].genre_mask_ & g) != 0)
                            {
                                str = boost::lexical_cast<std::string>(g);
                                writer.String(str.c_str());
                            }
                            g = g << 1;
                        }
                        writer.EndArray();
                    }
                    break;
                default:
                    log_error(L"web_connect_schedules_handler::process_set. Unknown schedule type");
                    break;
                }

                writer.Key("recordings");
                add_timers_as_array(recordings, &schedules[i].schedule_item_id_, writer);

                writer.EndObject();
            }

            writer.EndArray();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);

            ok = true;
        } else
        {
            log_error(L"web_connect_schedules_handler::process_set. get_schedule_request failed");
        }
    } else
    {
        log_error(L"web_connect_schedules_handler::process_set. get_recordings_request failed");
    }

    if (!ok)
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

void web_connect_schedules_handler::process_delete(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;

    if (http_request_ptr->get_content_length())
    {
        std::string response_str = http_request_ptr->get_content();

        rapidjson::LookaheadParser r(&response_str[0]);
        if (r.IsValid())
        {
            std::string schedule_id;
            std::string timer_id;
            bool return_timers = false;

            r.EnterObject();
            while (const char* key = r.NextObjectKey())
            {
                if (strcmp(key, "schedule") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    schedule_id = r.GetString();
                } else
                if (strcmp(key, "recording") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    timer_id = r.GetString();
                } else
                if (strcmp(key, "full") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    return_timers = r.GetBool();
                } else
                {
                    r.SkipValue();
                }
            }

            if (!schedule_id.empty())
            {
                //remove schedule
                schedule_remover_t sr;
                sr.schedule_item_id_ = boost::lexical_cast<boost::int32_t>(schedule_id);

                std::string str;
                write_to_xml(sr, str);

                dvblink::messaging::recorder::remove_schedule_request req(str);
                dvblink::messaging::recorder::remove_schedule_response resp;
                ok = message_queue->send(recorder_message_queue_addressee, req, resp) == success && resp.result_;

            } else if (!timer_id.empty())
            {
                //cancel timer
                rd_recording_remover_t rr;
                rr.set_recording_id(timer_id);

                std::string str;
                write_to_xml(rr, str);

                dvblink::messaging::recorder::remove_recording_request req(str);
                dvblink::messaging::recorder::remove_recording_response resp;
                ok = message_queue->send(recorder_message_queue_addressee, req, resp) == success && resp.result_;
            } else
            {
                log_error(L"web_connect_schedules_handler::process_delete. missing schdule or timer id parameter");
            }

            if (ok)
            {
                if (return_timers)
                {
                    messaging::recorder::get_recordings_request req;
                    messaging::recorder::get_recordings_response resp;
                    if (message_queue->send(recorder_message_queue_addressee, req, resp) == success && resp.result_)
                    {
                        rd_recording_list_t recordings;
                        read_from_xml(resp.recordings_.to_string(), recordings);

                        rapidjson::StringBuffer s;
                        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

                        add_timers_as_array(recordings, NULL, writer);

                        send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
                    } else
                    {
                        log_error(L"web_connect_schedules_handler::process_delete. get_recordings_request failed");
                    }
                } else
                {
                    send_json_ok_message(http_request_ptr, tcp_conn);
                }
            } else
            {
                log_error(L"web_connect_schedules_handler::process_delete. remove_schedule_request/remove_recording_request failed");
            }
        } else
        {
            log_error(L"web_connect_schedules_handler::process_delete. unable to deserialize json request");
        }
    } else
    {
        log_error(L"web_connect_schedules_handler::process_delete. post content of 0 length");
    } 

    if (!ok)
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

void web_connect_schedules_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;

    if (http_request_ptr->get_content_length())
    {
        std::string response_str = http_request_ptr->get_content();

        rapidjson::LookaheadParser r(&response_str[0]);
        if (r.IsValid())
        {
            dvblex::recorder::schedule_item si;
            //this will set the default margins
            si.margin_after_ = -1;
            si.margin_before_ = -1;
            time_t end_time = 0;
            bool return_timers = false;

            r.EnterObject();
            while (const char* key = r.NextObjectKey())
            {
                if (strcmp(key, "type") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    std::string str = r.GetString();
                    int type = find_name_array_index(schedule_type_names, schedule_type_num, str.c_str());
                    if (type >= 0)
                        si.type_ = (recorder::schedule_item_type_e)type;
                    else
                        log_error(L"web_connect_schedules_handler::process_post. unknown schedule type %1%") % string_cast<EC_UTF8>(str);
                } else
                if (strcmp(key, "channel") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    si.channel_ = r.GetString();
                } else
                if (strcmp(key, "endTime") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    end_time = r.GetInt();
                } else
                if (strcmp(key, "startTime") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    si.start_time_ = r.GetInt();
                } else
                if (strcmp(key, "name") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    si.name_ = r.GetString();
                } else
                if (strcmp(key, "days") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    r.EnterArray();
                    while (r.NextArrayValue()) 
                    {
                        int mask = r.GetInt();
                        if (mask > 0)
                            si.day_mask_ |= (1 << (mask - 1));
                    }
                } else
                if (strcmp(key, "query") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    si.key_phrase_ = r.GetString();
                } else
                if (strcmp(key, "genres") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    r.EnterArray();
                    while (r.NextArrayValue()) 
                    {
                        int genre = atoi(r.GetString());
                        si.genre_mask_ = (si.genre_mask_ | genre);
                    }
                } else
                if (strcmp(key, "program") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    si.epg_event_id_ = r.GetString();
                } else
                if (strcmp(key, "repeat") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    si.record_series_ = r.GetBool();
                } else
                if (strcmp(key, "full") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    return_timers = r.GetBool();
                } else
                {
                    r.SkipValue();
                }
            }

            //correct duration from start/end times
            if (si.type_ == recorder::sitManual)
                si.duration_ = end_time - si.start_time_;

            std::string str;
            write_to_xml(si, str);

            dvblink::messaging::recorder::add_schedule_request req(str);
            dvblink::messaging::recorder::add_schedule_response resp;
            ok = message_queue->send(recorder_message_queue_addressee, req, resp) == success && resp.result_;

            if (ok)
            {
                if (return_timers)
                {
                    messaging::recorder::get_recordings_request req;
                    messaging::recorder::get_recordings_response resp;
                    if (message_queue->send(recorder_message_queue_addressee, req, resp) == success && resp.result_)
                    {
                        rd_recording_list_t recordings;
                        read_from_xml(resp.recordings_.to_string(), recordings);

                        rapidjson::StringBuffer s;
                        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

                        add_timers_as_array(recordings, NULL, writer);

                        send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
                    } else
                    {
                        log_error(L"web_connect_schedules_handler::process_post. get_recordings_request failed");
                    }
                } else
                {
                    send_json_ok_message(http_request_ptr, tcp_conn);
                }
            } else
            {
                log_error(L"web_connect_schedules_handler::process_post. add_schedule_request failed");
            }
        } else
        {
            log_error(L"web_connect_schedules_handler::process_post. unable to deserialize json request");
        }
    } else
    {
        log_error(L"web_connect_schedules_handler::process_post. post content of 0 length");
    }

    if (!ok)
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}


} // dvblex
