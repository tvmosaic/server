/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_epg.h>
#include <dl_message_common.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "web_settings_epg_mapping_epgsearch.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_epg_mapping_epgsearch_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    if (http_request_ptr->has_query("epg_source") && http_request_ptr->has_query("channel"))
    {
        dvblink::epg_source_id_t epg_source_id = http_request_ptr->get_query("epg_source");
        dvblink::channel_id_t channel_id = http_request_ptr->get_query("channel");

        epg::match_epg_channels_request req;
        req.epg_source_id_ = epg_source_id;
        req.channels_.push_back(channel_id);

        epg::match_epg_channels_response resp;
        if (message_queue->send(epg_manager_message_queue_addressee, req, resp) == success && resp.result_)
        {
            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            writer.StartObject();
            epg_channel_match_map_t::iterator it = resp.match_info_.find(channel_id);
            if (it != resp.match_info_.end())
            {
                if (it->second.exact_match_.size() > 0)
                {
                    writer.Key("exact_match");
                    writer.StartObject();
                    writer.Key("epg_channel_id");
                    writer.String(it->second.exact_match_.at(0).epg_channel_id_.to_string().c_str());
                    writer.EndObject();
                } else
                if (it->second.partial_match_.size() > 0)
                {
                    writer.Key("partial_match");
                    writer.StartArray();
                    for (size_t i=0; i<it->second.partial_match_.size(); i++)
                    {
                        writer.StartObject();
                        writer.Key("epg_channel_id");
                        writer.String(it->second.partial_match_.at(i).epg_channel_id_.to_string().c_str());
                        writer.EndObject();
                    }
                    writer.EndArray();
                }
            }
            writer.EndObject();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
        } else
        {
            log_error(L"web_settings_epg_mapping_epgsearch_handler::process_get. match_epg_channels_request failed");
            send_json_error_message(http_request_ptr, tcp_conn, "match_epg_channels_request failed");
        }
    } else
    {
        dvblink::logging::log_error(L"web_settings_epg_mapping_epgsearch_handler. missing epg source and/or channel id parameter");
        send_json_error_message(http_request_ptr, tcp_conn, "missing epg source and/or channel id parameter");
    }
}

} // dvblex
