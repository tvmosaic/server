/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_epg_source_commands.h>
#include <dl_message_addresses.h>
#include <dl_message_license.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "web_request_constants.h"
#include "web_settings_products_trial.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_products_trial_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;
    if (http_request_ptr->has_query("id"))
    {
        license::activate_product_trial_request req;
        req.product_id_ = http_request_ptr->get_query("id");

        license::activate_product_trial_response resp;
        if (message_queue->send(server_message_queue_addressee, req, resp) == success)
        {
            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            writer.StartObject();
            writer.Key("result");
            writer.String(activation_result_names[resp.result_]);
            writer.EndObject();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);

            ok = true;
        } else
        {
            log_error(L"web_settings_products_trial_handler::process_set. activate_product_trial_request failed");
        }
    } else
    {
        log_error(L"web_settings_products_trial_handler::process_get. missing product id parameter");
    }

    if (!ok)
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

} // dvblex
