/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_dlna_params.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "../pion_helper.h"
#include "web_settings_dlna.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

static const std::string key_for_default_value = "";
static const std::string value_for_default_value = "Auto";

void web_settings_dlna_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;
    if (http_request_ptr->get_content_length())
    {
        dlna_params dp;

        std::string response_str = http_request_ptr->get_content();
        rapidjson::LookaheadParser r(&response_str[0]);
        if (r.IsValid())
        {
            r.EnterObject();
            while (const char* key = r.NextObjectKey())
            {
                if (strcmp(key, "audio_track") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    dp.audio_track_ = r.GetString();
                    if (boost::iequals(dp.audio_track_, key_for_default_value))
                        dp.audio_track_.clear();
                } else
                if (strcmp(key, "network_adapter") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    dp.network_adapter_ = r.GetString();
                    if (boost::iequals(dp.network_adapter_, key_for_default_value))
                        dp.network_adapter_.clear();
                } else
                if (strcmp(key, "timeshift") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    dp.is_timeshift_enabled_ = r.GetBool();
                } else
                if (strcmp(key, "dlna_disabled") == 0 && r.PeekType() != rapidjson::kNullType)
                {
                    dp.is_dlna_disabled_ = r.GetBool();
                } else
                {
                    r.SkipValue();
                }
            }

            dvblink::messaging::xml_message_request request;
            request.server_address_ = get_server_ip_from_request(http_request_ptr);
            request.proto_ = context.stream_service->get_http_prefix();
            request.cmd_id_ = set_dlna_params_cmd;
            std::string str;
            write_to_xml(dp, str);
            request.xml_ = str;

            //applying dlna settings has been removed as DLNA component is not present in the open-source version
            ok = true;
        } else
        {
            log_error(L"web_settings_dlna_handler::process_set. unable to deserialize json request");
        }
    } else
    {
        log_error(L"web_settings_dlna_handler::process_set. post content of 0 length");
    }

    if (ok)
        send_json_ok_message(http_request_ptr, tcp_conn);
    else
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

void web_settings_dlna_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    dvblink::messaging::xml_message_request request;
    request.server_address_ = get_server_ip_from_request(http_request_ptr);
    request.proto_ = context.stream_service->get_http_prefix();
    request.cmd_id_ = get_dlna_params_cmd;

    dlna_params dp;
    //reading settings was removed as DLNA is not part of the open-source version

    rapidjson::StringBuffer s;
    rapidjson::Writer<rapidjson::StringBuffer> writer(s);

    writer.StartObject();

    writer.Key("network_adapter");
    writer.String(dp.network_adapter_.empty() ? key_for_default_value.c_str() : dp.network_adapter_.c_str());
    writer.Key("audio_track");
    writer.String(dp.audio_track_.empty() ? key_for_default_value.c_str() : dp.audio_track_.c_str());
    writer.Key("timeshift");
    writer.Bool(dp.is_timeshift_enabled_);
    writer.Key("dlna_disabled");
    writer.Bool(dp.is_dlna_disabled_);

    writer.Key("adapters");
    writer.StartArray();
    add_json_id_label_pair_raw(key_for_default_value, value_for_default_value, writer, "value");

    for (size_t i=0; i<dp.network_adapters_.size(); i++)
    {
        std::string name = string_cast<EC_UTF8>(dp.network_adapters_[i].m_strName) + " (" + string_cast<EC_UTF8>(dp.network_adapters_[i].m_strAddress) + ")";
        add_json_id_label_pair_raw(string_cast<EC_UTF8>(dp.network_adapters_[i].m_strGUID), name, writer, "value");
    }

    writer.EndArray();

    writer.Key("audios");
    writer.StartArray();
    add_json_id_label_pair_raw(key_for_default_value, value_for_default_value, writer, "value");
    add_json_id_label_pair_raw("hun", "hun", writer, "value");
    add_json_id_label_pair_raw("ita", "ita", writer, "value");
    add_json_id_label_pair_raw("pol", "pol", writer, "value");
    add_json_id_label_pair_raw("nld", "nld", writer, "value");
    add_json_id_label_pair_raw("spa", "spa", writer, "value");
    add_json_id_label_pair_raw("fra", "fra", writer, "value");
    add_json_id_label_pair_raw("ell", "ell", writer, "value");
    add_json_id_label_pair_raw("eng", "eng", writer, "value");
    add_json_id_label_pair_raw("hrv", "hrv", writer, "value");
    add_json_id_label_pair_raw("swe", "swe", writer, "value");
    add_json_id_label_pair_raw("ukr", "ukr", writer, "value");
    add_json_id_label_pair_raw("fin", "fin", writer, "value");
    add_json_id_label_pair_raw("tur", "tur", writer, "value");
    add_json_id_label_pair_raw("dan", "dan", writer, "value");
    add_json_id_label_pair_raw("ces", "ces", writer, "value");
    add_json_id_label_pair_raw("bul", "bul", writer, "value");
    add_json_id_label_pair_raw("rus", "rus", writer, "value");
    add_json_id_label_pair_raw("nor", "nor", writer, "value");
    add_json_id_label_pair_raw("slv", "slv", writer, "value");
    add_json_id_label_pair_raw("deu", "deu", writer, "value");
    add_json_id_label_pair_raw("slk", "slk", writer, "value");
    writer.EndArray();

    writer.EndObject();

    send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
}

} // dvblex
