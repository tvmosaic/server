/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_server_params.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "../pion_helper.h"
#include "web_settings_backup.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_backup_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;
    if (http_request_ptr->get_content_length())
    {
        const char* content = http_request_ptr->get_content();
        size_t content_length = http_request_ptr->get_content_length();

        //check if this a multipart form and extract data from it
        if (boost::istarts_with(content, "------"))
        {
            //find first 0d 0a 0d 0a
            char sign[] = { 0x0D, 0x0A, 0x0D, 0x0A };
            std::string needle(sign, sign + sizeof(sign));
            std::string haystack(content, content + content_length);
            std::size_t n = haystack.find(needle);

            if (n != std::string::npos)
            {
                content += (n + sizeof(sign));
                content_length -= (n + sizeof(sign));
            }
        }

        std::vector<boost::uint8_t> data;
        for (size_t i = 0; i < content_length; i++) 
            data.push_back(content[i]);

        server_binary_data_param slr(data, "application/zip");

        std::string tmp;
        write_to_xml(slr, tmp);

        dvblink::messaging::xml_message_request request;
        request.cmd_id_ = do_installation_restore_cmd;
        request.server_address_ = get_server_ip_from_request(http_request_ptr);
        request.proto_ = context.stream_service->get_http_prefix();
        request.xml_ = tmp;

        dvblink::messaging::xml_message_response response;

        if (message_queue->send(server_message_queue_addressee, request, response) == dvblink::messaging::success &&
            response.result_.to_string().compare(xmlcmd_result_success) == 0)
        {
            ok = true;
        } else
        {
            log_error(L"web_settings_backup_handler::process_set. do_installation_restore_cmd failed");
        }
    } else
    {
        log_error(L"web_settings_backup_handler::process_set. post content of 0 length");
    }

    if (ok)
        send_json_ok_message(http_request_ptr, tcp_conn);
    else
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

void web_settings_backup_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    dvblink::messaging::xml_message_request request;
    request.cmd_id_ = do_installation_backup_cmd;
    request.server_address_ = get_server_ip_from_request(http_request_ptr);
    request.proto_ = context.stream_service->get_http_prefix();

    dvblink::messaging::xml_message_response response;

    if (message_queue->send(server_message_queue_addressee, request, response) == dvblink::messaging::success &&
        response.result_.to_string().compare(xmlcmd_result_success) == 0)
    {
        server_binary_data_param sbd;
        read_from_xml(response.xml_.to_string(), sbd);

        std::vector<boost::uint8_t> data;
        sbd.get_binary_data(data);

        boost::posix_time::time_facet* facet = new boost::posix_time::time_facet("%d%m%Y_%H%M%S");
        std::ostringstream stream;
        stream.imbue(std::locale(stream.getloc(), facet));
        stream << "attachment; filename=tvmosaic_backup_" << boost::posix_time::second_clock::local_time() << ".zip";

	    pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *http_request_ptr);

        writer->get_response().set_status_code(pion::http::types::RESPONSE_CODE_OK);
        writer->get_response().set_status_message(pion::http::types::RESPONSE_MESSAGE_OK);
        writer->get_response().set_content_type(sbd.mime_);
        writer->get_response().change_header("Access-Control-Allow-Origin", access_control_allow_origin_hdr.c_str());
        writer->get_response().change_header("Content-Disposition", stream.str());
        writer->write(&data[0], data.size());
        writer->send();
        writer->get_connection()->finish();

    } else
    {
        log_error(L"web_settings_backup_handler::process_get. do_installation_backup_cmd failed");
        send_json_error_message(http_request_ptr, tcp_conn, "do_installation_backup_cmd failed");
    }
}

} // dvblex
