/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_message_devices.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "../common.h"
#include "../pion_helper.h"
#include "web_request_constants.h"
#include "web_settings_channels_scan_devices_status.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_channels_scan_devices_status_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    if (http_request_ptr->has_query("id"))
    {
        std::string device_id = http_request_ptr->get_query("id");

        devices::get_device_status_request req(device_id);
        devices::get_device_status_response resp;
        if (message_queue->send(source_manager_message_queue_addressee, req, resp) == success && resp.result_)
        {
            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            device_status_ex_t& dse = resp.status_;

            writer.StartObject();
            writer.Key("state");
            writer.String(device_status_names[dse.device_status_.state_]);

            writer.Key("scanning");
            writer.StartObject();
            if (dse.device_status_.state_ == ds_channel_scan_in_progress || dse.device_status_.state_ == ds_networks_scanned || 
                dse.device_status_.state_ == ds_channel_scan_finished)
            {
                writer.Key("progress");
                writer.Uint(dse.device_status_.scan_progress_.progress_);
                writer.Key("channels_found");
                writer.Uint(dse.device_status_.scan_progress_.channels_found_);
                writer.Key("transponders");
                writer.StartArray();
                for (size_t i=0; i<dse.scan_log_.size(); i++)
                {
                    writer.StartObject();
                    writer.Key("id");
                    writer.String(dse.scan_log_[i].scan_data_.to_string().c_str());
                    writer.Key("channels_found");
                    writer.Uint(dse.scan_log_[i].channels_found_);
                    writer.Key("quality");
                    writer.Uint(dse.scan_log_[i].signal_info_.signal_quality_);
                    writer.Key("strength");
                    writer.Uint(dse.scan_log_[i].signal_info_.signal_strength_);
                    writer.Key("lock");
                    writer.Bool(dse.scan_log_[i].signal_info_.lock_.get());
                    writer.Key("comment");
                    writer.String(dse.scan_log_[i].comment_.to_string().c_str());
                    writer.EndObject();
                }
                writer.EndArray();
            }
            writer.EndObject();

            writer.Key("streaming");
            writer.StartArray();
            if (dse.device_status_.state_ == ds_streaming)
            {
                for (size_t i=0; i<dse.device_status_.stream_channels_.size(); i++)
                {
                    const stream_channel_stats_t& sch = dse.device_status_.stream_channels_[i];

                    writer.StartObject();
                    writer.Key("id");
                    writer.String(sch.channel_id_.to_string().c_str());
                    writer.Key("name");
                    writer.String(sch.channel_name_.to_string().c_str());
                    writer.Key("lock");
                    writer.Bool(dse.device_status_.signal_info_.lock_.get());
                    writer.Key("strength");
                    writer.Uint(dse.device_status_.signal_info_.signal_strength_);
                    writer.Key("quality");
                    writer.Uint(dse.device_status_.signal_info_.signal_quality_);
                    writer.Key("consumers");
                    writer.Uint(sch.consumer_num_);
                    writer.Key("bitrate");
                    writer.Uint(sch.stream_stats_.bitrate_);
                    writer.Key("packets");
                    writer.Uint(sch.stream_stats_.packet_count_);
                    writer.Key("errors");
                    writer.Uint(sch.stream_stats_.error_packet_count_);
                    writer.Key("dscnt");
                    writer.Uint(sch.stream_stats_.discontinuity_count_);
                    writer.EndObject();
                }
            }
            writer.EndArray();

            writer.EndObject();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
        } else
        {
            log_error(L"web_settings_channels_scan_devices_status_handler::process_get. devices_info_request failed");
            send_json_error_message(http_request_ptr, tcp_conn, "devices_info_request failed");
        }
    } else
    {
        log_error(L"web_settings_channels_scan_devices_status_handler::process_get. missing id parameter");
        send_json_error_message(http_request_ptr, tcp_conn, "missing id parameter");
    }
}

} // dvblex
