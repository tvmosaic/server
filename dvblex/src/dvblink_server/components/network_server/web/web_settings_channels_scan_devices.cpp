/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_message_devices.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "../common.h"
#include "../pion_helper.h"
#include "web_request_constants.h"
#include "web_settings_channels_scan_devices.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_channels_scan_devices_handler::process_delete(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;
    if (http_request_ptr->has_query("id"))
    {
        std::string device_id = http_request_ptr->get_query("id");

        devices::delete_manual_device_request req(device_id);
        devices::delete_manual_device_response resp;
        if (message_queue->send(source_manager_message_queue_addressee, req, resp) == success && resp.result_)
        {
            ok = true;
        } else
        {
            log_error(L"web_settings_channels_scan_devices_handler::process_delete. delete_manual_device_request failed");
        }
    } else
    {
        log_error(L"web_settings_channels_scan_devices_handler::process_delete. missing id parameter");
    }

    if (ok)
        send_json_ok_message(http_request_ptr, tcp_conn);
    else
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

void web_settings_channels_scan_devices_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;
    if (http_request_ptr->get_content_length())
    {
        std::string response_str = http_request_ptr->get_content();

        rapidjson::LookaheadParser r(&response_str[0]);
        if (r.IsValid())
        {
            devices::create_manual_device_request req;
            read_concise_param_map_from_json(r, req.params_);

            devices::create_manual_device_response resp;
            if (message_queue->send(source_manager_message_queue_addressee, req, resp) == success && resp.result_)
            {
                ok = true;
            } else
            {
                log_error(L"web_settings_channels_scan_devices_handler::process_set. create_manual_device_request failed");
            }
        } else
        {
            log_error(L"web_settings_channels_scan_devices_handler::process_set. unable to deserialize json request");
        }
    } else
    {
        log_error(L"web_settings_channels_scan_devices_handler::process_set. post content of 0 length");
    }

    if (ok)
        send_json_ok_message(http_request_ptr, tcp_conn);
    else
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

void web_settings_channels_scan_devices_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    devices::devices_info_request req;
    devices::devices_info_response resp;
    if (message_queue->send(source_manager_message_queue_addressee, req, resp) == success && resp.result_)
    {
        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

        writer.StartArray();
        for (size_t i=0; i<resp.device_info_list_.size(); i++)
        {
            const device_info_t& di = resp.device_info_list_[i];

            writer.StartObject();
            writer.Key("status");
            writer.String(device_status_names[di.descriptor_.state_]);
            writer.Key("state");
            writer.String(device_state_names[di.state_]);
            writer.Key("has_settings");
            writer.Bool(di.descriptor_.has_settings_);
            writer.Key("name");
            writer.String(di.descriptor_.name_.to_string().c_str());
            writer.Key("id");
            writer.String(di.descriptor_.id_.to_string().c_str());
            writer.Key("standards");
            writer.Uint(di.descriptor_.supported_standards_.get());
            writer.Key("can_be_deleted");
            writer.Bool(di.descriptor_.can_be_deleted_);

            writer.Key("providers");
            writer.StartArray();
            for (size_t j=0; j<di.headends_.size(); j++)
            {
                const headend_info_t& hi = di.headends_[j];
                writer.StartObject();
                writer.Key("id");
                writer.String(hi.id_.to_string().c_str());
                writer.Key("name");
                writer.String(hi.name_.to_string().c_str());
                writer.Key("description");
                writer.String(hi.desc_.to_string().c_str());
                writer.Key("standard");
                writer.Uint((unsigned int)hi.standard_);
                writer.EndObject();
            }
            writer.EndArray();

            writer.EndObject();
        }
        writer.EndArray();

        send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
    } else
    {
        log_error(L"web_settings_channels_scan_devices_handler::process_get. devices_info_request failed");
        send_json_error_message(http_request_ptr, tcp_conn, "devices_info_request failed");
    }
}

} // dvblex
