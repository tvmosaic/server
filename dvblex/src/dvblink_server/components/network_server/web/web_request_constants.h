/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_types.h>

namespace dvblex {

static const char* activation_result_names[] = {"success", "no_server_connection", "no_activations_available", "already_activated", "invalid_login", "in_progress", 
    "file_write_error", "error", "invalid_xml", "invalid_data", "invalid_coupon", "already_used_coupon", "email_already_in_use",
    "other_product_coupon", "need_user_info"};

static const char* device_status_names[] = {"none", "idle", "streaming", "channel_scan_in_progress", "networks_scanned", "channel_scan_finished", 
    "epg_scan_in_progress", "epg_scan_finished"};

static const char* device_state_names[] = {"unknown", "new", "active", "missing", "error"};

static const char* sendto_status_names[] = {"pending", "fetching", "formatting", "sending", "success", "error", "canceled"};

static const char* schedule_type_names[] = {"manual", "epg", "pattern"};
static const size_t schedule_type_num = sizeof(schedule_type_names) / sizeof(char*);

inline int find_name_array_index(const char** names, size_t size, const char* name)
{
    int ret_val = -1;

    for (size_t i=0; i<size; i++)
    {
        if (strcmp(names[i], name) == 0)
        {
            ret_val = i;
            break;
        }
    }

    return ret_val;
}

} // dvblex
