/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/shared_ptr.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>

#include <pion/http/plugin_service.hpp>
#include <pion/http/response_writer.hpp>

#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include <dl_rapidjson_laparser.h>

#include <dl_types.h>
#include <dl_logger.h>
#include <dl_message_addresses.h>
#include <dl_message_queue.h>
#include <dl_message_channels.h>
#include <dl_message_epg.h>
#include <dl_language_settings.h>
#include <dl_parameters.h>
#include <dl_channels.h>
#include <dl_pugixml_helper.h>
#include <dl_pb_item.h>
#include <dl_pb_video_info.h>
#include <dl_locale_strings.h>
#include <dl_timer_serializer.h>
#include <dl_send_to_target_cmd.h>
#include <dl_xml_serialization.h>
#include "../settings.h"
#include "../constants.h"
#include "../stream_service.h"
#include "../pion_helper.h"

namespace dvblex {

const std::string application_json_mime_code		    = "application/json";

struct web_request_context
{
    dvblex::stream_service* stream_service;
    dvblex::network_server_settings_t* settings;
    boost::uint16_t port;
};

class web_request_handler
{
public:
    web_request_handler(){}
    virtual ~web_request_handler(){}

    virtual void process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
    {
        send_text_response(http_request_ptr, tcp_conn, "no POST handler", 0, application_json_mime_code, pion::http::types::RESPONSE_CODE_SERVER_ERROR, pion::http::types::RESPONSE_MESSAGE_SERVER_ERROR);
    }

    virtual void process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
    {
        send_text_response(http_request_ptr, tcp_conn, "no GET handler", 0, application_json_mime_code, pion::http::types::RESPONSE_CODE_SERVER_ERROR, pion::http::types::RESPONSE_MESSAGE_SERVER_ERROR);
    }

    virtual void process_delete(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
    {
        send_text_response(http_request_ptr, tcp_conn, "no DELETE handler", 0, application_json_mime_code, pion::http::types::RESPONSE_CODE_SERVER_ERROR, pion::http::types::RESPONSE_MESSAGE_SERVER_ERROR);
    }

    static void send_json_ok_message(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn)
    {
        send_text_response(http_request_ptr, tcp_conn, "{ \"success\": true }", 0, application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
    }

    static void send_json_error_message(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, const std::string& message)
    {
        std::stringstream strbuf;
        strbuf << "{\"error\": \"" << message << "\"}";

        send_text_response(http_request_ptr, tcp_conn, strbuf.str().c_str(), 0, application_json_mime_code, pion::http::types::RESPONSE_CODE_SERVER_ERROR, pion::http::types::RESPONSE_MESSAGE_SERVER_ERROR);
    }

    static void send_text_response(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, 
        const char* data, size_t data_size, const std::string& mime, unsigned int http_code, const std::string& http_message)
    {
        if (data_size == 0)
            data_size = strlen(data);

	    pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *http_request_ptr);

        bool supports_gzip = false;
        if (http_request_ptr->has_header("Accept-Encoding"))
        {
            std::string encodings = http_request_ptr->get_header("Accept-Encoding");
            supports_gzip = boost::icontains(encodings, "gzip");
        }

        std::vector<char> compressed_data;
        if (supports_gzip)
        {
            boost::iostreams::filtering_ostream os;
            os.push(boost::iostreams::gzip_compressor());
            os.push(boost::iostreams::back_inserter(compressed_data));
            try {
                boost::iostreams::write(os, data, data_size);
            } catch (...){}
        }

        writer->get_response().set_status_code(http_code);
        writer->get_response().set_status_message(http_message);
        writer->get_response().set_content_type(mime);
        writer->get_response().change_header("Access-Control-Allow-Origin", access_control_allow_origin_hdr.c_str());
        if (compressed_data.size() > 0)
        {
            writer->get_response().change_header("Content-Encoding", "gzip");
            writer->write((void*)&compressed_data[0], compressed_data.size());
        } else
        {
            writer->write((void*)data, data_size);
        }
        writer->send();
        writer->get_connection()->finish();
    }

protected:
    void add_json_name_intvalue_pair_localized(int value, const std::string& localized_name, rapidjson::Writer<rapidjson::StringBuffer>& writer)
    {
        add_json_name_intvalue_pair_raw(value, dvblink::engine::language_settings::GetInstance()->GetItemName(localized_name), writer);
    }

    void add_json_name_intvalue_pair_raw(int value, const std::string& name, rapidjson::Writer<rapidjson::StringBuffer>& writer)
    {
        writer.StartObject();
        writer.Key("name");
        writer.String(name.c_str());
        writer.Key("value");
        writer.Int(value);
        writer.EndObject();
    }

    template <typename T> void add_json_id_label_pair_localized(const T& id, const std::string& localized_value, rapidjson::Writer<rapidjson::StringBuffer>& writer, const char* id_tag = "id")
    {
        add_json_id_label_pair_raw(id, dvblink::engine::language_settings::GetInstance()->GetItemName(localized_value), writer, id_tag);
    }

    template <typename T> void add_json_id_label_pair_raw(const T& id, const std::string& raw_value, rapidjson::Writer<rapidjson::StringBuffer>& writer, const char* id_tag = "id")
    {
        writer.StartObject();
        writer.Key(id_tag);
        writer.String(boost::lexical_cast<std::string>(id).c_str());
        writer.Key("name");
        writer.String(raw_value.c_str());
        writer.EndObject();
    }

    void read_concise_param_map_from_json(rapidjson::LookaheadParser& parser, concise_param_map_t& param_map)
    {
        parser.EnterObject();
        while (const char* key = parser.NextObjectKey())
        {
            param_map[key] = parser.GetString();
        }
    }

    void add_concise_param_map_to_writer(const concise_param_map_t& param_map, const std::string& key, rapidjson::Writer<rapidjson::StringBuffer>& writer)
    {
        if (!key.empty())
            writer.Key(key.c_str());

        writer.StartObject();
        concise_param_map_t::const_iterator it = param_map.begin();
        while (it != param_map.end())
        {
            writer.Key(it->first.to_string().c_str());
            writer.String(it->second.to_string().c_str());
            ++it;
        }
        writer.EndObject();
    }

    void add_container_to_writer(const parameters_container_t& container, const std::string& key, rapidjson::Writer<rapidjson::StringBuffer>& writer)
    {
        if (!key.empty())
            writer.Key(key.c_str());

        writer.StartObject();
        writer.Key("id");
        writer.String(container.id_.to_string().c_str());
        writer.Key("name");
        writer.String(container.name_.to_string().c_str());
        writer.Key("description");
        writer.String(container.desc_.to_string().c_str());

        writer.Key("selectable");
        writer.StartArray();
        for (size_t i=0; i<container.selectable_params_.size(); i++)
        {
            const selectable_param_description_t& sel = container.selectable_params_[i];
            writer.StartObject();

            writer.Key("id");
            writer.String(sel.key_.to_string().c_str());
            writer.Key("name");
            writer.String(sel.name_.to_string().c_str());
            writer.Key("selected_idx");
            writer.Int(sel.selected_param_.get());
            writer.Key("values");

            writer.StartArray();
            for (size_t j=0; j<sel.parameters_list_.size(); j++)
                add_json_id_label_pair_raw(sel.parameters_list_[j].value_.to_string(), sel.parameters_list_[j].name_.to_string(), writer, "value");
            writer.EndArray();

            writer.EndObject();
        }
        writer.EndArray();

        writer.Key("editable");
        writer.StartArray();
        for (size_t i=0; i<container.editable_params_.size(); i++)
        {
            const editable_param_element_t& ed = container.editable_params_[i];
            writer.StartObject();
            writer.Key("id");
            writer.String(ed.key_.to_string().c_str());
            writer.Key("name");
            writer.String(ed.name_.to_string().c_str());
            writer.Key("value");
            writer.String(ed.value_.to_string().c_str());
            writer.Key("format");
            writer.String(ed.format_ == epf_string ? "epf_string" : "epf_number");
            writer.EndObject();
        }
        writer.EndArray();

        writer.EndObject();
    }

    void add_channel_to_object(const channel_description_t& cd, rapidjson::Writer<rapidjson::StringBuffer>& writer)
    {
        writer.Key("id");
        writer.String(cd.id_.to_string().c_str());
        writer.Key("name");
        writer.String(cd.name_.to_string().c_str());
        writer.Key("origin");
        writer.String(cd.origin_.to_string().c_str());
        writer.Key("encrypted");
        writer.Bool(cd.encrypted_.get());
        std::string numstr;
        if (get_number_as_string(cd.num_, cd.sub_num_, numstr))
        {
            writer.Key("number");
            writer.String(numstr.c_str());
        }
        writer.Key("type");
        if (cd.type_ == ct_tv)
            writer.String("TV");
        else
            writer.String("Radio");
        if (!cd.logo_.empty())
        {
            writer.Key("logo");
            writer.String(cd.logo_.to_string().c_str());
        }
        writer.Key("lock");
        writer.Bool(cd.child_lock_.get());
    }

    void get_epg_sources(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
    {
        dvblink::messaging::epg::get_epg_sources_request req;
        dvblink::messaging::epg::get_epg_sources_response resp;
        if (message_queue->send(dvblink::messaging::epg_manager_message_queue_addressee, req, resp) == dvblink::messaging::success && resp.result_)
        {
            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            writer.StartArray();
            epg_source_map_t::iterator it = resp.epg_sources_.begin();
            while (it != resp.epg_sources_.end())
            {
                writer.StartObject();
                writer.Key("id");
                writer.String(it->second.id_.to_string().c_str());
                writer.Key("name");
                writer.String(it->second.name_.to_string().c_str());
                writer.Key("has_settings");
                writer.Bool(it->second.has_settings_.get());
                writer.Key("is_default");
                writer.Bool(it->second.is_default_.get());

                writer.EndObject();

                ++it;
            }
            writer.EndArray();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
        } else
        {
            dvblink::logging::log_error(L"get_epg_sources. get_epg_sources_request failed");
            send_json_error_message(http_request_ptr, tcp_conn, "get_epg_sources_request failed");
        }
    }

    void get_favorites(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context, bool with_channels = false)
    {
        dvblink::messaging::channels::get_channel_favorites_request req;
        dvblink::messaging::channels::get_channel_favorites_response resp;
        if (message_queue->send(dvblink::messaging::source_manager_message_queue_addressee, req, resp) == dvblink::messaging::success && resp.result_)
        {
            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            writer.StartArray();
            for (size_t i=0; i<resp.favorites_.size(); i++)
            {
                writer.StartObject();
                writer.Key("id");
                writer.String(resp.favorites_[i].id_.to_string().c_str());
                writer.Key("name");
                writer.String(resp.favorites_[i].name_.to_string().c_str());
                writer.Key("auto");
                writer.Bool((resp.favorites_[i].flags_.get() & cft_auto) != 0);

                if (with_channels)
                {
                    writer.Key("channels");
                    writer.StartArray();
                    for (size_t chidx = 0; chidx<resp.favorites_[i].channels_.size(); chidx++)
                    {
                        writer.StartObject();
                        writer.Key("id");
                        writer.String(resp.favorites_[i].channels_[chidx].to_string().c_str());
                        writer.EndObject();
                    }
                    writer.EndArray();
                }

                writer.EndObject();
            }
            writer.EndArray();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
        } else
        {
            dvblink::logging::log_error(L"get_favorites. get_channel_favorites_request failed");
            send_json_error_message(http_request_ptr, tcp_conn, "get_channel_favorites_requestfailed");
        }
    }

    void get_favorite_channels(const std::string& favorite_id, dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
    {
        dvblink::favorite_id_t fv_id = favorite_id;
        dvblink::messaging::channels::get_channel_favorites_request req(fv_id);
        dvblink::messaging::channels::get_channel_favorites_response resp;
        if (message_queue->send(dvblink::messaging::source_manager_message_queue_addressee, req, resp) == dvblink::messaging::success && resp.result_ && resp.favorites_.size() > 0)
        {
            channel_favorite_t& ch_fav = resp.favorites_.at(0);

            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            writer.StartArray();
            for (size_t i=0; i<ch_fav.channels_.size(); i++)
            {
                writer.StartObject();
                writer.Key("id");
                writer.String(ch_fav.channels_[i].to_string().c_str());

                writer.EndObject();
            }
            writer.EndArray();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
        } else
        {
            dvblink::logging::log_error(L"get_favorites. get_channel_favorites_request failed");
            send_json_error_message(http_request_ptr, tcp_conn, "get_channel_favorites_requestfailed");
        }
    }

    bool get_number_as_string(const dvblink::channel_number_t& num, const dvblink::channel_subnumber_t& subnum, std::string& numstr)
    {
        numstr.clear();

        if (num.get() > 0)
        {
            std::stringstream strbuf;
            strbuf << num.get();
            if (subnum.get() > 0)
                strbuf << "." << subnum.get();

            numstr = strbuf.str();
        }

        return !numstr.empty();
    }

    void parse_number_from_string(const std::string& numstr, dvblink::channel_number_t& num, dvblink::channel_subnumber_t& subnum)
    {
        int e1, e2;
        if (sscanf(numstr.c_str(), "%d.%d", &e1, &e2) == 2)
        {
            num = e1;
            subnum = e2;
        } else
        {
            int d = 0;
            dvblink::engine::string_conv::apply(numstr.c_str(), d, 0);
            if (d > 0)
                num = d;
        }
    }

    void convert_json_to_xml(rapidjson::LookaheadParser& r, dvblink::xml_string_t& params)
    {
        pugi::xml_document doc;

        pugi::xml_node root_node = doc.append_child("params");
        if (root_node != NULL)
        {
            r.EnterArray();
            while (r.NextArrayValue()) 
            {
                std::string id;
                std::string value;

                r.EnterObject();
                while (const char* key = r.NextObjectKey())
                {
                    if (strcmp(key, "id") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        id = r.GetString();
                    } else
                    if (strcmp(key, "value") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        value = r.GetString();
                    } else
                    {
                        r.SkipValue();
                    }
                }
                dvblink::pugixml_helpers::new_child(root_node, id, value);
            }

            std::string str;
            dvblink::pugixml_helpers::xmldoc_dump_to_string(doc, str);
            params = str;
        }
    }

    void add_pbitem_to_json(boost::shared_ptr<playback::pb_item_t>& item, rapidjson::Writer<rapidjson::StringBuffer>& writer)
    {
        writer.Key("id");
        writer.String(item->object_id_.to_string().c_str());
        writer.Key("url");
        writer.String(item->url_.to_string().c_str());
        if (!item->thumbnail_.empty())
        {
            writer.Key("image");
            writer.String(item->thumbnail_.to_string().c_str());
        }
        writer.Key("can_be_deleted");
        writer.Bool(item->can_be_deleted_);
        if (item->size_ > 0)
        {
            writer.Key("size");
            writer.Int64(item->size_);
        }
    }

    void add_slash_sep_string_array_to_json(const std::string& key, const std::string& values, rapidjson::Writer<rapidjson::StringBuffer>& writer)
    {
        if (!values.empty())
        {
            typedef boost::tokenizer<boost::char_separator<char>, std::string::const_iterator, std::string> tokenizer_t;
            boost::char_separator<char> sep("/");
            tokenizer_t tokens(values, sep);

            if ( tokens.begin()!= tokens.end())
            {
                writer.Key(key.c_str());
                writer.StartArray();

                for (tokenizer_t::iterator tok_iter = tokens.begin(); tok_iter != tokens.end(); ++tok_iter)
                    writer.String((*tok_iter).c_str());

                writer.EndArray();
            }
        }
    }

    void add_videoinfo_to_json(playback::pb_video_info_t& vi, rapidjson::Writer<rapidjson::StringBuffer>& writer)
    {
        writer.Key("name");
        writer.String(vi.m_Name.c_str());
        writer.Key("description");
        writer.String(vi.m_ShortDesc.c_str());
        writer.Key("start");
        writer.Int(vi.m_StartTime);
        writer.Key("duration");
        writer.Int(vi.m_Duration);
        if (!vi.m_SecondName.empty())
        {
            writer.Key("sub_name");
            writer.String(vi.m_SecondName.c_str());
        }
        if (!vi.m_Language.empty())
        {
            writer.Key("lang");
            writer.String(vi.m_Language.c_str());
        }

        add_slash_sep_string_array_to_json("actors", vi.m_Actors, writer);
        add_slash_sep_string_array_to_json("directors", vi.m_Directors, writer);
        add_slash_sep_string_array_to_json("writers", vi.m_Writers, writer);
        add_slash_sep_string_array_to_json("producers", vi.m_Producers, writer);
        add_slash_sep_string_array_to_json("guests", vi.m_Guests, writer);

        writer.Key("genres");
        writer.StartArray();

        if (vi.m_IsAction)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_ACTION).c_str());
        if (vi.m_IsComedy)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_COMEDY).c_str());
        if (vi.m_IsDocumentary)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_DOCUMENTARY).c_str());
        if (vi.m_IsDrama)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_DRAMA).c_str());
        if (vi.m_IsEducational)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_EDU).c_str());
        if (vi.m_IsHorror)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_HORROR).c_str());
        if (vi.m_IsKids)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_KIDS).c_str());
        if (vi.m_IsMovie)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_MOVIE).c_str());
        if (vi.m_IsMusic)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_MUSIC).c_str());
        if (vi.m_IsNews)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_NEWS).c_str());
        if (vi.m_IsReality)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_REALITY).c_str());
        if (vi.m_IsRomance)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_ROMANCE).c_str());
        if (vi.m_IsScienceFiction)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_SCIFI).c_str());
        if (vi.m_IsSerial)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_SERIAL).c_str());
        if (vi.m_IsSoap)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_SOAP).c_str());
        if (vi.m_IsSpecial)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_SPECIAL).c_str());
        if (vi.m_IsSports)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_SPORT).c_str());
        if (vi.m_IsThriller)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_THRILLER).c_str());
        if (vi.m_IsAdult)
            writer.String(dvblink::engine::language_settings::GetInstance()->GetItemName(IDS_GENRE_ADULT).c_str());

        writer.EndArray();

        if (!vi.m_ImageURL.empty())
        {
            writer.Key("image");
            writer.String(vi.m_ImageURL.c_str());
        }

        if (vi.m_Year > 0)
        {
            writer.Key("year");
            writer.Int(vi.m_Year);
        }

        if (vi.m_EpisodeNum > 0)
        {
            writer.Key("episode");
            writer.Int(vi.m_EpisodeNum);
        }

        if (vi.m_SeasonNum > 0)
        {
            writer.Key("season");
            writer.Int(vi.m_SeasonNum);
        }

        if (vi.m_StarNum > 0)
        {
            writer.Key("rating");
            writer.Int(vi.m_StarNum);

            writer.Key("rating_max");
            writer.Int(vi.m_StarNumMax);
        }

        add_slash_sep_string_array_to_json("categories", vi.m_Categories, writer);

        writer.Key("hdtv");
        writer.Bool(vi.m_IsHDTV);

        writer.Key("premiere");
        writer.Bool(vi.m_IsPremiere);

        writer.Key("repeat");
        writer.Bool(vi.m_IsRepeatFlag);
    }

    void add_timers_as_array(const rd_recording_list_t& recordings, const dvblink::schedule_item_id_t* schedule_id, rapidjson::Writer<rapidjson::StringBuffer>& writer)
    {
        std::string str;

        writer.StartArray();

        for (size_t i=0; i<recordings.size(); i++)
        {
            if (schedule_id == NULL || recordings[i].get_schedule_id().get() == schedule_id->get())
            {
                writer.StartObject();
                writer.Key("recording");
                writer.String(recordings[i].get_recording_id().to_string().c_str());
                writer.Key("schedule");
                std::string str = boost::lexical_cast<std::string>(recordings[i].get_schedule_id().get());
                writer.String(str.c_str());
                writer.Key("channel");
                writer.String(recordings[i].get_channel_id().to_string().c_str());
                writer.Key("active");
                writer.Bool(recordings[i].is_active());
                writer.Key("conflicting");
                writer.Bool(recordings[i].is_conflicting());

                dvblex::recorder::epg_item_ex program = recordings[i].get_program();
                writer.Key("program");
                writer.StartObject();
                add_videoinfo_to_json(program.event_, writer);
                writer.Key("id");
                writer.String(program.event_.id_.c_str());
                writer.Key("isRecord");
                writer.Bool(program.is_record_);
                writer.Key("isSeries");
                writer.Bool(program.is_series_);
                writer.Key("isRepeatRecord");
                writer.Bool(program.is_repeat_record_);
                writer.Key("isRecordConflict");
                writer.Bool(program.is_record_conflict_);
                writer.EndObject();

                writer.EndObject();
            }
        }

        writer.EndArray();
    }

    void convert_xml_to_writer(const std::string& key, const dvblink::xml_string_t& params, rapidjson::Writer<rapidjson::StringBuffer>& writer)
    {
        writer.Key(key.c_str());

        writer.StartArray();

        pugi::xml_document doc;
        if (doc.load_buffer(params.to_string().c_str(), params.to_string().length()).status == pugi::status_ok)
        {
            pugi::xml_node root_node = doc.first_child();
            if (root_node != NULL)
            {
                pugi::xml_node node = root_node.first_child();
                while (node != NULL)
                {
                    writer.StartObject();
                    writer.Key("id");
                    writer.String(node.name());
                    writer.Key("value");
                    writer.String(node.child_value());
                    writer.EndObject();

                    node = node.next_sibling();
                }
            }
        }
        writer.EndArray();
    }

    void get_sendto_targets(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
    {
        dvblink::messaging::xml_message_request request;
        request.server_address_ = get_server_ip_from_request(http_request_ptr);
        request.proto_ = context.stream_service->get_http_prefix();
        request.cmd_id_ = SEND_TO_TARGET_CMD_GET_TARGETS;

        dvblink::messaging::xml_message_response response;

        if (message_queue->send(dvblink::messaging::social_message_queue_addressee, request, response) == dvblink::messaging::success &&
            response.result_.to_string().compare(dvblink::xmlcmd_result_success) == 0)
        {
            send_to_get_targets_response stt;
            read_from_xml(response.xml_.to_string(), stt);

            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            writer.StartArray();

            for (size_t i=0; i<stt.targets_.size(); i++)
            {
                const send_to_target_info& sti = stt.targets_.at(i);

                writer.StartObject();

                writer.Key("id");
                writer.String(sti.id.to_string().c_str());
                writer.Key("name");
                writer.String(sti.name.c_str());
                writer.Key("default");
                writer.Bool(sti.default_);
                writer.Key("delete_on_success");
                writer.Bool(sti.delete_on_success);
                writer.Key("use_comskip");
                writer.Bool(sti.use_comskip);
                add_concise_param_map_to_writer(sti.comskip_params, "comskip_params", writer);

                //formatter
                writer.Key("fmt_id");
                writer.String(sti.formatter_info.id.c_str());
                convert_xml_to_writer("fmt_params", sti.formatter_info.params, writer);

                //destination
                writer.Key("dst_id");
                writer.String(sti.dest_info.id.c_str());
                convert_xml_to_writer("dst_params", sti.dest_info.params, writer);

                writer.EndObject();
            }

            writer.EndArray();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
        } else
        {
            dvblink::logging::log_error(L"web_request_handler::get_sendto_targets. SEND_TO_TARGET_CMD_GET_TARGETS failed");
            send_json_error_message(http_request_ptr, tcp_conn, "SEND_TO_TARGET_CMD_GET_TARGETS failed");
        }
    }

};

typedef boost::shared_ptr<web_request_handler> web_request_handler_obj_t;

} // dvblex
