/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_epg_source_commands.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_message_epg.h>
#include <dl_xml_serialization.h>
#include <dl_locale_strings.h>
#include "../common.h"
#include "../pion_helper.h"
#include "web_settings_epg_mapping_channels.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void web_settings_epg_mapping_channels_handler::process_post(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    bool ok = false;
    if (http_request_ptr->get_content_length())
    {
        std::string response_str = http_request_ptr->get_content();

        rapidjson::LookaheadParser r(&response_str[0]);
        if (r.IsValid())
        {
            std::string source_id;
            epg::set_channel_epg_config_request req;

            r.EnterArray();
            while (r.NextArrayValue()) 
            {
                epg_source_channel_id_t epg_config;
                channel_id_t channel_id;

                r.EnterObject();
                while (const char* key = r.NextObjectKey())
                {
                    if (strcmp(key, "id") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        channel_id = r.GetString();
                    } else
                    if (strcmp(key, "epg_source_id") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        epg_config.epg_source_id_ = r.GetString();
                    } else
                    if (strcmp(key, "epg_channel_id") == 0 && r.PeekType() != rapidjson::kNullType)
                    {
                        epg_config.epg_channel_id_ = r.GetString();
                    } else
                    {
                        r.SkipValue();
                    }
                }
                req.epg_config_[channel_id] = epg_config;
            }

            epg::set_channel_epg_config_response resp;
            if (message_queue->send(epg_manager_message_queue_addressee, req, resp) == success && resp.result_)
            {
                ok = true;
            } else
            {
                log_error(L"web_settings_epg_mapping_channels_handler::process_set. set_channel_epg_config_request failed");
            }
        } else
        {
            log_error(L"web_settings_epg_mapping_channels_handler::process_set. unable to deserialize json request");
        }
    } else
    {
        log_error(L"web_settings_epg_mapping_channels_handler::process_set. post content of 0 length");
    }

    if (ok)
        send_json_ok_message(http_request_ptr, tcp_conn);
    else
        send_json_error_message(http_request_ptr, tcp_conn, "error");
}

void web_settings_epg_mapping_channels_handler::process_get(dvblink::messaging::message_queue_t& message_queue, const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, web_request_context& context)
{
    std::string server_address = get_server_ip_from_request(http_request_ptr);

    //get channels
    channel_desc_list_t cdl;
    dvblink::favorite_id_t favorite_id;
    if (ns_get_channel_descriptions(favorite_id, message_queue, context.settings, context.stream_service, server_address, cdl))
    {
        //get channels epg config
        epg::get_channel_epg_config_request req;
        epg::get_channel_epg_config_response resp;
        if (message_queue->send(epg_manager_message_queue_addressee, req, resp) == success && resp.result_)
        {
            //get support information
            epg_source_full_desc_map_t epg_sources;
            get_epg_source_full_desc(message_queue, epg_sources);

            rapidjson::StringBuffer s;
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);

            writer.StartArray();
            for (size_t i=0; i<cdl.size(); i++)
            {
                writer.StartObject();
                add_channel_to_object(cdl[i], writer);

                writer.Key("epg_mapping");
                writer.StartObject();
                //find epg source for this channel
                channel_to_epg_source_map_t::iterator epg_it = resp.epg_config_.find(cdl[i].id_);
                if (epg_it == resp.epg_config_.end())
                {
                    writer.Key("default");
                    writer.Bool(true);
                } else
                {
                    writer.Key("default");
                    writer.Bool(false);
                    writer.Key("epg_source_id");
                    writer.String(epg_it->second.epg_source_id_.to_string().c_str());
                    writer.Key("epg_channel_id");
                    writer.String(epg_it->second.epg_channel_id_.to_string().c_str());

                    std::string epg_source_name = language_settings::GetInstance()->GetItemName(IDS_SERVER_UNKNOWN_VALUE);
                    std::string epg_channel_name = language_settings::GetInstance()->GetItemName(IDS_SERVER_UNKNOWN_VALUE);

                    epg_source_full_desc_map_t::iterator epg_src_it = epg_sources.find(epg_it->second.epg_source_id_);
                    if (epg_src_it != epg_sources.end())
                    {
                        epg_source_name = epg_src_it->second.source_.name_.to_string();
                        epg_source_channel_map_t::iterator ch_it = epg_src_it->second.channels_.find(epg_it->second.epg_channel_id_);
                        if (ch_it != epg_src_it->second.channels_.end())
                            epg_channel_name = ch_it->second.name_.to_string();
                    }

                    writer.Key("epg_source_name");
                    writer.String(epg_source_name.c_str());
                    writer.Key("epg_channel_name");
                    writer.String(epg_channel_name.c_str());
                }
                writer.EndObject();

                writer.EndObject();
            }
            writer.EndArray();

            send_text_response(http_request_ptr, tcp_conn, s.GetString(), s.GetSize(), application_json_mime_code, pion::http::types::RESPONSE_CODE_OK, pion::http::types::RESPONSE_MESSAGE_OK);
        } else
        {
            log_error(L"web_settings_epg_mapping_channels_handler::process_get. get_channel_epg_config_request failed");
            send_json_error_message(http_request_ptr, tcp_conn, "get_channel_epg_config_request failed");
        }
    } else
    {
        log_error(L"web_settings_epg_mapping_channels_handler::process_get. ns_get_channel_descriptions failed");
        send_json_error_message(http_request_ptr, tcp_conn, "ns_get_channel_descriptions failed");
    }
}

void web_settings_epg_mapping_channels_handler::get_epg_source_full_desc(dvblink::messaging::message_queue_t& message_queue, web_settings_epg_mapping_channels_handler::epg_source_full_desc_map_t& epg_sources)
{
    epg::get_epg_sources_request req;
    epg::get_epg_sources_response resp;
    if (message_queue->send(epg_manager_message_queue_addressee, req, resp) == success && resp.result_)
    {
        epg_source_map_t::iterator it = resp.epg_sources_.begin();
        while (it != resp.epg_sources_.end())
        {
            epg_source_full_desc_t ed;
            ed.source_ = it->second;

            epg::get_epg_channels_request ch_req;
            epg::get_epg_channels_response ch_resp;
            if (message_queue->send(dvblink::message_addressee_t(ed.source_.id_.get()), ch_req, ch_resp) == success && resp.result_)
                ed.channels_ = ch_resp.channels_;

            epg_sources[ed.source_.id_] = ed;

            ++it;
        }
    }
}

} // dvblex
