/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>

namespace dvblex { 

const std::string ts_data_mime_code             = "video/MP2T";
const std::string m3u8_mime_code                = "application/x-mpegURL";
const std::string mpeg_mime_code				= "video/mpeg";
const std::string mp4_mime_code				    = "video/mp4";
const std::string webm_mime_code				= "video/webm";
const std::string text_xml_mime_code			= "text/xml";
const std::string m3u_playlist_mime_code		= "audio/mpegurl";
const std::string text_html_mime_code			= "text/html";
const std::string png_mime_code				    = "image/png";
const std::string jpeg_mime_code				= "image/jpeg";
const std::string image_mime_code				= "image/*";
const std::string octet_stream_mime_code	    = "application/octet-stream";
const std::string application_json_code		    = "application/json";

}
