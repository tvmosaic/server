/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include "port_mapper.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::streaming;
using namespace dvblink::engine;

namespace dvblex {

#define PORT_MAPPING_BROADCAST_PERIOD_MSEC  60000

ext_port_mapper::ext_port_mapper() :
    timer_(NULL),
    mapping_mask_(epp_mask_none)
{
}

ext_port_mapper::~ext_port_mapper()
{
    stop();
}

unsigned long ext_port_mapper::get_mask_from_flags(bool forward_http, bool forward_https)
{
    unsigned long mm = epp_mask_none;

    if (forward_http)
        mm |= epp_mask_http;

    if (forward_https)
        mm |= epp_mask_https;

    return mm;
}

void ext_port_mapper::start(const ext_port_desc_list_t& http_ports, bool forward_http, const ext_port_desc_list_t& https_ports, bool forward_https)
{
    http_ports_ = http_ports;
    https_ports_ = https_ports;

    mapping_mask_ = get_mask_from_flags(forward_http, forward_https);

    log_info(L"Starting external port mapper. Mapping mask %1%") % mapping_mask_;
    if (mapping_mask_ != epp_mask_none)
        timer_ = new dvblink::engine::timer_procedure<ext_port_mapper>(&ext_port_mapper::timer_func, *this, PORT_MAPPING_BROADCAST_PERIOD_MSEC, true);
}

void ext_port_mapper::stop()
{
    if (timer_ != NULL)
    {
        delete timer_;
        timer_ = NULL;
    }
}

void ext_port_mapper::change_forwarding(bool forward_http, bool forward_https)
{
    unsigned long new_mask = get_mask_from_flags(forward_http, forward_https);

    if (mapping_mask_ != new_mask)
    {
        //stop update timer (if it is running)
        if (timer_ != NULL)
        {
            delete timer_;
            timer_ = NULL;
        }

        //save new port mapping
		long prev_mask = mapping_mask_;
        mapping_mask_ = new_mask;
        log_info(L"ext_port_mapper::set_port_mapping. New mapping mask %1%") % mapping_mask_;
        
        apply_port_mapping(true, prev_mask, true);

        if (mapping_mask_ != epp_mask_none)
            timer_ = new dvblink::engine::timer_procedure<ext_port_mapper>(&ext_port_mapper::timer_func, *this, PORT_MAPPING_BROADCAST_PERIOD_MSEC, true);
    }
}

bool ext_port_mapper::apply_port_mapping(bool broadcast_delete, unsigned long prev_mask, bool log_errors)
{
    bool res = false;
    
    igd_list_t igd_list;
    errcode_t err = discover_internet_gateways(igd_list);
    if (err == err_none)
    {
        if (igd_list.size() > 0)
        {
            res = true;
            for (size_t i=0; i< igd_list.size(); i++)
            {
                if (igd_list[i].descr_processing_result == err_none)
                {
                    //apply new mapping mask: http
                    if ((mapping_mask_ & epp_mask_http) != 0)
                    {
                        for (size_t idx = 0; idx < http_ports_.size() ; idx++)
                            res &= broadcast_add_port(igd_list[i], http_ports_[idx].port_, http_ports_[idx].desc_.c_str(), log_errors);
                    } else
                    {
                        if (broadcast_delete)
						{
							if ((prev_mask & epp_mask_http) != 0)
                                for (size_t idx = 0; idx < http_ports_.size() ; idx++)
    								res &= broadcast_delete_port(igd_list[i], http_ports_[idx].port_, http_ports_[idx].desc_.c_str(), log_errors);
						}
                    }

                    //apply new mapping mask: https
                    if ((mapping_mask_ & epp_mask_https) != 0)
                    {
                        for (size_t idx = 0; idx < https_ports_.size() ; idx++)
                            res &= broadcast_add_port(igd_list[i], https_ports_[idx].port_, https_ports_[idx].desc_.c_str(), log_errors);
                    } else
                    {
                        if (broadcast_delete)
						{
							if ((prev_mask & epp_mask_https) != 0)
                                for (size_t idx = 0; idx < https_ports_.size() ; idx++)
    								res &= broadcast_delete_port(igd_list[i], https_ports_[idx].port_, https_ports_[idx].desc_.c_str(), log_errors);
						}
                    }
                } else
                {
                    if (log_errors)
                    {
                        std::wstring wstr = string_cast<EC_UTF8>(igd_list[i].descr_url);
                        log_error(L"ext_port_mapper::apply_port_mapping. Get gateway description (%1%) returned an error %2%") % wstr % (int)igd_list[i].descr_processing_result;
                    }
                }
            }
        } else
        {
            if (log_errors)
                log_warning(L"ext_port_mapper::apply_port_mapping. No internet gateways have been found");
        }
    } else
    {
        if (log_errors)
            log_error(L"ext_port_mapper::apply_port_mapping. discover_internet_gateways returned an error %1%") % (int)err;
    }
    
    return res;
}

bool ext_port_mapper::broadcast_add_port(dvblink::streaming::igd_info_t& igd_info, network_port_t& port, const char* description, bool log_errors)
{
    bool res = false;
    
    if (igd_info.descr_processing_result == err_none)
    {
        port_mapping_desc_t pm;
        pm.description = description;
        pm.ext_port = port.get();
        pm.int_port = port.get();
        pm.protocol = "TCP";
        std::string response;
        errcode_t err = add_port_mapping(igd_info, pm, response);
        res = (err == err_none);

        if (log_errors && err != err_none)
            log_error(L"ext_port_mapper::broadcast_add_port. Error returned: %1%") % string_cast<EC_UTF8>(response);
    }

    return res;
}

bool ext_port_mapper::broadcast_delete_port(dvblink::streaming::igd_info_t& igd_info, network_port_t& port, const char* description, bool log_errors)
{
    bool res = false;

    if (igd_info.descr_processing_result == err_none)
    {
        port_mapping_desc_t pm;
        pm.description = description;
        pm.ext_port = port.get();
        pm.int_port = port.get();
        pm.protocol = "TCP";
        std::string response;
        errcode_t err = remove_port_mapping(igd_info, pm, response);
        res = (err == err_none);

        if (log_errors && err != err_none)
            log_error(L"ext_port_mapper::broadcast_delete_port. Error returned: %1%") % string_cast<EC_UTF8>(response);
    }

    return res;
}

void ext_port_mapper::timer_func(const boost::system::error_code& e)
{
    if (e != boost::asio::error::operation_aborted)
    {
        apply_port_mapping(false, 0, false);
    }
}

} // dvblex

