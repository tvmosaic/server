/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <pion/http/response_writer.hpp>
#include <boost/shared_ptr.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include "iphone_segmentor_streamer.h"

namespace dvblex {

class memory_buffer_sender : public boost::enable_shared_from_this<memory_buffer_sender>, private boost::noncopyable
{
public:
    static inline boost::shared_ptr<memory_buffer_sender> Create(const unsigned char* data_buffer, size_t size,
        const std::string& mime_type, const pion::http::request_ptr& request, const pion::tcp::connection_ptr& tcp_conn, unsigned long max_chunk_size = 0) 
    {
        return boost::shared_ptr<memory_buffer_sender>(new memory_buffer_sender(data_buffer, size, mime_type, request, tcp_conn, max_chunk_size));
    }

    static inline boost::shared_ptr<memory_buffer_sender> Create(
        boost::shared_ptr<iphone_segmentor> segmentor, iphone_segmentor::segment_t segment,
        const std::string& mime_type, const pion::http::request_ptr& request, const pion::tcp::connection_ptr& tcp_conn, unsigned long max_chunk_size = 0) 
    {
        return boost::shared_ptr<memory_buffer_sender>(new memory_buffer_sender(segmentor, segment, mime_type, request, tcp_conn, max_chunk_size));
    }

protected:
    memory_buffer_sender(const unsigned char* data_buffer, size_t size, const std::string& mime_type,
            const pion::http::request_ptr& request, const pion::tcp::connection_ptr& tcp_conn, unsigned long max_chunk_size) :
        mime_type_(mime_type),
        writer_(pion::http::response_writer::create(tcp_conn, *request)),
        max_chunk_size_(max_chunk_size),
        file_bytes_to_send_(0),
        bytes_sent_(0)
    {
        init(data_buffer, size);
    }

    memory_buffer_sender(boost::shared_ptr<iphone_segmentor> segmentor, iphone_segmentor::segment_t segment,
            const std::string& mime_type, const pion::http::request_ptr& request, const pion::tcp::connection_ptr& tcp_conn, unsigned long max_chunk_size) :
        mime_type_(mime_type),
        writer_(pion::http::response_writer::create(tcp_conn, *request)),
        max_chunk_size_(max_chunk_size),
        file_bytes_to_send_(0),
        bytes_sent_(0),
        segmentor_(segmentor),
        segment_(segment)
    {
        const unsigned char* data_buffer = (const unsigned char*)&(segment->data().at(0));
        init(data_buffer, segment.get()->size());
    }

    void init(const unsigned char* data_buffer, size_t size)
    {
        //copy data
        data_buffer_.assign(data_buffer, data_buffer + size);
        // set the Content-Type HTTP header using the file's MIME type
        writer_->get_response().set_content_type(mime_type_);

        // use "200 OK" HTTP response
        writer_->get_response().set_status_code(pion::http::types::RESPONSE_CODE_OK);
        writer_->get_response().set_status_message(pion::http::types::RESPONSE_MESSAGE_OK);

        //add CORS headers (required by chromecast)
        writer_->get_response().change_header("Access-Control-Allow-Origin", access_control_allow_origin_hdr.c_str());
        writer_->get_response().change_header("Access-Control-Expose-Headers", "*");
        writer_->get_response().change_header("Access-Control-Allow-Credentials", "true");
    }

    void handle_write(const boost::system::error_code& write_error, std::size_t bytes_written)
    {
        bool finished_sending = true, free = true;

        if (write_error)
        {
            dvblink::logging::log_error(L"memory_buffer_sender::handle_write callback: encountered error sending response data: %1% %2%!!!!!!!!")
                % write_error.value() % dvblink::engine::string_cast<dvblink::engine::EC_UTF8>(write_error.message());

            // encountered error sending response data
            writer_->get_connection()->set_lifecycle(pion::tcp::connection::LIFECYCLE_CLOSE);	// make sure it will get closed
            free = false;
        }
        else
        {
            // response data sent OK
            //
            // use file_bytes_to_send_ instead of bytes_written; bytes_written
            // includes bytes for HTTP headers and chunking headers
            dvblink::logging::log_ext_info(L"memory_buffer_sender::handle_write callback: file_bytes_to_send_ = %1% bytes_written = %2%") % file_bytes_to_send_ % bytes_written;

            bytes_sent_ += file_bytes_to_send_;
            if (bytes_sent_ < data_buffer_.size()) 
            {
                // NOT finished sending
                finished_sending = false;
                writer_->clear();
            }
        }

        if (finished_sending) 
        {
            // TCPConnection::finish() calls TCPServer::finishConnection, which will either:
            // a) call HTTPServer::handleConnection again if keep-alive is true; or,
            // b) close the socket and remove it from the server's connection pool
            writer_->get_connection()->finish();
            if (free && segmentor_ && segment_)
            {
                segmentor_->free_segment(segment_);
            }
        }
        else
        {
            send();
        }
    }

public:
    void send()
    {
        // check if we have nothing to send (send 0 byte response content)
        if (data_buffer_.size() <= bytes_sent_) 
        {
            writer_->send();
            return;
        }

        // calculate the number of bytes to send (file_bytes_to_send_)
        file_bytes_to_send_ = data_buffer_.size() - bytes_sent_;
        if (max_chunk_size_ && file_bytes_to_send_ > max_chunk_size_)
        {
            file_bytes_to_send_ = max_chunk_size_;
        }

        // get the content to send (file_content_ptr)
        const char* file_content_ptr = &(data_buffer_[0]) + bytes_sent_;

        // send the content
        writer_->write_no_copy(const_cast<char*>(file_content_ptr), file_bytes_to_send_);
        if (bytes_sent_ + file_bytes_to_send_ >= data_buffer_.size()) 
        {
            // this is the last piece of data to send
            if (bytes_sent_) 
            {
                // send last chunk in a series
                writer_->send_final_chunk(boost::bind(&memory_buffer_sender::handle_write,
                    shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
            }
            else
            {
                // sending entire file at once
                writer_->send(boost::bind(&memory_buffer_sender::handle_write,
                    shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
            }
        }
        else
        {
            // there will be more data -> send a chunk
            writer_->send_chunk(boost::bind(&memory_buffer_sender::handle_write,
                shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
        }
    }

private:
	std::vector<char> data_buffer_;
    std::string mime_type_;

    pion::http::response_writer_ptr writer_;

    //maximum chunk size (in bytes): buffer larger than this size will be
    //delivered to clients using HTTP chunked responses.  A value of
    //zero means that the size is unlimited (chunking is disabled).
    unsigned long max_chunk_size_;
    // the number of file bytes send in the last operation
    unsigned long file_bytes_to_send_;
    // the number of bytes we have sent so far
    unsigned long bytes_sent_;

    boost::shared_ptr<iphone_segmentor> segmentor_;
    iphone_segmentor::segment_t segment_;
};

} // dvblex
