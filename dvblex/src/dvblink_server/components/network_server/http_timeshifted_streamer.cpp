/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/filesystem.hpp>
#include <dl_logger.h>
#include <dl_utils.h>
#include <dl_ts.h>
#include "constants.h"
#include "mime_types.h"
#include "http_timeshifted_streamer.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

static const int packet_buffer_size = 42;

namespace dvblex {

http_timeshifted_streamer_t::http_timeshifted_streamer_t(const std::string& id, const dvblink::filesystem_path_t& disk_path, 
                                                         boost::uint64_t max_size, stream_processor_base* processor) :
	network_streamer_base_t(id, processor),
	b_initialized_(false),
	send_thread_(NULL),
	last_disconnect_(-1),
	disconnect_threshold_(15),
    packet_buffer_(packet_buffer_size),
    b_stream_sent_(false),
    no_stream_timeout_sec_(30),
    aligner_(aligner_callback, this),
    smooth_streamer_(smooth_streamer_callback, this, &b_exit_flag_)
{

	time(&start_time_);

    //prepare NULL packet
    memset(null_packet_, 0xFF, TS_PACKET_SIZE);
    null_packet_[0] = 0x47;
    null_packet_[1] = 0x1F;
    null_packet_[2] = 0xFF;
    null_packet_[3] = 0x10;

    //start disconnect timer 
	disconnect_timer_ = new dvblink::engine::timer_procedure<http_timeshifted_streamer_t>(&http_timeshifted_streamer_t::disconnect_timer_func, *this, static_cast<long>(3000));
	idle_timer_ = new dvblink::engine::timer_procedure<http_timeshifted_streamer_t>(&http_timeshifted_streamer_t::idle_timer_func, *this, static_cast<long>(1000));

    //initialize ring buffer file
    std::string fname = boost::uuids::to_string(engine::uuid::gen_uuid());
    filesystem_path_t p = disk_path / fname;

    log_info(L"http_timeshifted_streamer_t::http_timeshifted_streamer_t. Buffer max length: %1%, buffer path %2%") % max_size % p.to_wstring();

    b_initialized_ = ring_buffer_.init(p, max_size);
}

http_timeshifted_streamer_t::~http_timeshifted_streamer_t()
{
    log_info(L"http_timeshifted_streamer_t::~http_timeshifted_streamer_t. Streamer %1% deleted") % get_id().to_wstring();

    stop_processor();

    stop_send_thread();

    if (b_initialized_)
        ring_buffer_.term();

    delete idle_timer_;
    delete disconnect_timer_;
}

void http_timeshifted_streamer_t::idle_timer_func(const boost::system::error_code& e)
{
	send_idle_packet_.check_and_set();
}

int http_timeshifted_streamer_t::get_timeshift_version()
{
    return 1;
}

void http_timeshifted_streamer_t::disconnect_timer_func(const boost::system::error_code& e)
{
    if (last_disconnect_ != -1)
    {
        time_t now;
        time(&now);
        
        if (now - last_disconnect_ >= disconnect_threshold_)
        {
            if (callbacks_ != NULL)
                callbacks_->streamer_disconnected(get_id());
        }
    }
}

bool http_timeshifted_streamer_t::start(streamer_callbacks_t* cb)
{
    network_streamer_base_t::start(cb);
    //check if error has to be reported already
    return b_initialized_;
}

void http_timeshifted_streamer_t::on_received_stream(const unsigned char* data, size_t len)
{
	b_stream_sent_ = true;

    if (b_initialized_)
    {
        if (len / TS_PACKET_SIZE > packet_buffer_size)
        {
            //if incoming packet size is greater than packet buffer size, write it immediately

            //first flush any packets in packet buffer
            int packet_num = 0;
            unsigned char* packet_buf = packet_buffer_.GetPacketBuffer(packet_num);
            if (packet_num > 0)
            {
                ring_buffer_.write_stream(packet_buf, packet_num*TS_PACKET_SIZE);
                packet_buffer_.Reset();
            }

            //write incoming packet
            ring_buffer_.write_stream(data, len);
        } else
        {
		    int remaining_length = len;
		    while (remaining_length > 0)
		    {
			    int added_length = 0;
			    bool b = packet_buffer_.AddPackets(data + (len - remaining_length), remaining_length, added_length);
			    if (b)
			    {
				    int packet_num = 0;
				    unsigned char* buf = packet_buffer_.GetPacketBuffer(packet_num);
                    ring_buffer_.write_stream(buf, packet_num*TS_PACKET_SIZE);
				    packet_buffer_.Reset();
			    }
			    remaining_length -= added_length;
		    }
        }
    }
}

bool http_timeshifted_streamer_t::process_request(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn)
{
    log_ext_info(L"(re)started reading from timeshift buffer");

	stop_send_thread();

	client_ = pion::http::response_writer::create(tcp_conn, *http_request_ptr);

    //start data sending thread
    b_exit_flag_ = false;
    send_thread_ = new boost::thread(boost::bind(&http_timeshifted_streamer_t::send_thread_func, this));    
    
    return true;
}

void http_timeshifted_streamer_t::stop_send_thread()
{
	if (send_thread_ != NULL)
	{
		b_exit_flag_ = true;
		send_thread_->join();
		delete send_thread_;
		send_thread_ = NULL;
	}
}

bool http_timeshifted_streamer_t::get_timeshift_stats(timeshift_stats_s& stats)
{
    ring_buffer_.get_stats(stats.cur_pos_bytes, stats.buffer_length, stats.buffer_duration, stats.cur_pos_sec);
    stats.max_buffer_length = ring_buffer_.get_max_buffer_size();
    log_ext_info(L"http_timeshifted_streamer_t::get_timeshift_stats. Buffer length: %1%, buffer max length: %2%, buffer duration: %3%, cur pos (bytes): %4%, cur pos (sec): %5%") % stats.buffer_length % stats.max_buffer_length % stats.buffer_duration % stats.cur_pos_bytes % stats.cur_pos_sec;
    return true;
}

bool http_timeshifted_streamer_t::seek_by_bytes(boost::int64_t offset, int whence, boost::uint64_t& cur_pos)
{
    log_ext_info(L"http_timeshifted_streamer_t::seek_by_bytes. offset: %1%, whence: %2%") % offset % whence;

    //stop any ongoing streaming
    stop_send_thread();

    cur_pos = 0;
    return ring_buffer_.read_seek_bytes(offset, whence, cur_pos);
}

bool http_timeshifted_streamer_t::seek_by_time(time_t offset, int whence, boost::uint64_t& cur_pos)
{
    log_ext_info(L"http_timeshifted_streamer_t::seek_by_time. offset: %1%, whence: %2%") % offset % whence;

    //stop any ongoing streaming
    stop_send_thread();

    cur_pos = 0;
    return ring_buffer_.read_seek_time(offset, whence, cur_pos);
}

void http_timeshifted_streamer_t::send_thread_func()
{
    smooth_streamer_.init();

    boost::uint64_t buffer_size = 188*1024;
    unsigned char* buffer = (unsigned char*)malloc(buffer_size);

    last_disconnect_ = -1;

    // set the Content-Type HTTP header using the file's MIME type
    client_->get_response().set_content_type(mpeg_mime_code);

    // use "200 OK" HTTP response
    client_->get_response().set_status_code(pion::http::types::RESPONSE_CODE_OK);
    client_->get_response().set_status_message(pion::http::types::RESPONSE_MESSAGE_OK);

	while (!b_exit_flag_)
	{
	    boost::uint64_t read_size = buffer_size;
	    bool b_read_data = ring_buffer_.read_stream(buffer, read_size, 100);
	    
	    b_write_error_ = false;
		if (b_read_data)
		{
            aligner_.write_stream(buffer, read_size);
		} else
        {
            if (send_idle_packet_.check_and_reset())
            {
                //send null packet while waiting for data
                //send it directly as smooth streamer might have its own state at this time
                node_processed_.reset();
                client_->write_no_copy(null_packet_, sizeof(null_packet_));
                client_->send_chunk(boost::bind(&http_timeshifted_streamer_t::handle_write, this, 
                    boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, &b_write_error_));
                node_processed_.wait();
                client_->clear();

				//check for no stream condition
				time_t now;
				time(&now);

                //check stream start timeout
				if (!b_stream_sent_)
				{
					if (now - start_time_ > no_stream_timeout_sec_)
					{
						log_info(L"http_timeshifted_streamer_t::send_thread_func: No stream timeout detected. Setting exit flag");
                        b_write_error_ = true;
                        if (callbacks_ != NULL)
                            callbacks_->streamer_disconnected(get_id());
					}
				}
			}
        }
        if (b_write_error_ || !client_->get_connection()->is_open())
        {
//            log_info(L"http_timeshifted_streamer_t::send_thread_func: removing http client");
            //break the loop
            break;
        }
	}

    //close http connection
    client_->get_connection()->finish();
    client_->get_connection()->close();
    
    client_.reset();
    
    free(buffer);

    //start client disconnect timer
    time(&last_disconnect_);

    smooth_streamer_.term();

    log_ext_info(L"http_timeshifted_streamer_t::send_thread_func. thread finished");
}

void http_timeshifted_streamer_t::handle_write(const boost::system::error_code& write_error, std::size_t /*bytes_written*/, bool* b_error_flag)
{
	if (write_error)
	{
		*b_error_flag = true;
//		log_error(L"http_timeshifted_streamer_t: http write error");
	}
	node_processed_.signal();
}

void __stdcall http_timeshifted_streamer_t::aligner_callback(const unsigned char* buf, unsigned long len, void* user_param)
{
    http_timeshifted_streamer_t* parent = (http_timeshifted_streamer_t*)user_param;
    parent->smooth_streamer_.process_stream(buf, len);
}

void http_timeshifted_streamer_t::smooth_streamer_callback(const unsigned char* buf, size_t len, void* user_param)
{
    http_timeshifted_streamer_t* parent = (http_timeshifted_streamer_t*)user_param;

    parent->node_processed_.reset();

    parent->client_->write_no_copy((char*)(buf), len);

    parent->client_->send_chunk(boost::bind(&http_timeshifted_streamer_t::handle_write, parent, 
        boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, &parent->b_write_error_));

    parent->node_processed_.wait();

    parent->client_->clear();
}

std::string http_timeshifted_streamer_t::get_streaming_url(const dvblink::channel_id_t& channel_id, const std::string& protocol_header, const std::string& server_address, 
    boost::uint16_t streaming_port, int handle)
{
    std::stringstream strbuf;
    strbuf << protocol_header << "://" << server_address << ":" << streaming_port << timeshift_stream_full_prefix << "?" << handle_param_name << "=" << handle;

    return strbuf.str();
}

bool http_timeshifted_streamer_t::is_timeshift_request(const pion::http::request_ptr& http_request_ptr)
{
    std::string resource = http_request_ptr->get_resource();

    size_t n = resource.rfind('.');
	if (n != std::string::npos)
	    resource = resource.substr(0, n);

    return boost::iequals(timeshift_stream_full_prefix, resource);
}

} // dvblex
