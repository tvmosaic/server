/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp> 
#include <pion/http/response_writer.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_message_server.h>
#include <dl_message_devices.h>
#include <dl_message_channels.h>
#include <dl_message_common.h>
#include <dl_message_recorder.h>
#include <dli_message_driven.h>
#include <dl_message_license.h>
#include <dl_message_epg.h>
#include <dl_message_addresses.h>
#include <dl_message_playback.h>
#include <dl_url_encoding.h>
#include <dl_xml_serialization.h>
#include <dl_server_info.h>
#include <dl_stream.h>
#include <dl_commands.h>
#include <dl_status.h>
#include <dl_server_serializer.h>
#include <dl_device_serializer.h>
#include <dl_channel_serializer.h>
#include <dl_stream_serializer.h>
#include <dl_epg_config_serializer.h>
#include <dl_products_serializer.h>
#include <dl_language_settings.h>
#include <dl_xml_command.h>
#include "constants.h"
#include "mime_types.h"
#include "pion_helper.h"
#include "command_service.h"

#ifdef _MSC_VER
#pragma warning (disable : 4100)
#endif

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

command_service::command_service(dvblink::i_server_t server, dvblink::messaging::message_queue_t message_queue, unsigned short port, stream_service* stream_service, network_server_settings_t* settings) :
    server_(server), message_queue_(message_queue), port_(port), stream_service_(stream_service), settings_(settings)
{
}

command_service::~command_service()
{
}

void command_service::send_binary_response(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, const std::vector<char>& data, const std::string& mime)
{
	pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *http_request_ptr);

    writer->get_response().set_status_code(pion::http::types::RESPONSE_CODE_OK);
    writer->get_response().set_status_message(pion::http::types::RESPONSE_MESSAGE_OK);
    writer->get_response().set_content_type(mime);
    writer->get_response().change_header("Access-Control-Allow-Origin", access_control_allow_origin_hdr.c_str());
    writer->write((void*)&data[0], data.size());
    writer->send();
    writer->get_connection()->finish();
}

void command_service::send_xml_response(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, const std::string& data, const std::string& mime)
{
	pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *http_request_ptr);

    writer->get_response().set_status_code(pion::http::types::RESPONSE_CODE_OK);
    writer->get_response().set_status_message(pion::http::types::RESPONSE_MESSAGE_OK);
    writer->get_response().set_content_type(mime);
    writer->get_response().change_header("Access-Control-Allow-Origin", access_control_allow_origin_hdr.c_str());
    writer->write(data);
    writer->send();
    writer->get_connection()->finish();
}

void command_service::send_error_response(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn, ::boost::uint32_t code, const std::string& msg)
{
	pion::http::response_writer_ptr writer = pion::http::response_writer::create(tcp_conn, *http_request_ptr);

    writer->get_response().set_status_code(code);
    writer->get_response().set_status_message(msg);
    writer->get_response().set_content_type(text_xml_mime_code);
    writer->send();
    writer->get_connection()->finish();
}

void command_service::process_accept_language_header(const pion::http::request_ptr& http_request_ptr)
{
    //we process only language codes without quality qualifiers
    //also, if several comma separated values are given, only the first one is taken
    std::string lang;
    if (http_request_ptr->has_header("Accept-Language"))
    {
        std::string contents = http_request_ptr->get_header("Accept-Language");
        size_t pos = contents.find(",");
        if (pos == std::string::npos)
            lang = contents;
        else
            lang = contents.substr(0, pos);
    }
    if (!lang.empty())
    {
        std::string cur_lang;
        language_settings::GetInstance()->GetCurrentLanguageId(cur_lang);
        if (!boost::iequals(cur_lang, lang))
        {
            log_ext_info(L"command_service::process_accept_language_header. Changing language to %1%") % string_cast<EC_UTF8>(lang);
            if (!language_settings::GetInstance()->SetCurrentLanguageId(lang))
            {
                log_ext_info(L"command_service::process_accept_language_header. Language change to %1% failed") % string_cast<EC_UTF8>(lang);
            }
        }
    }
}

void command_service::operator()(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn)
{
    process_accept_language_header(http_request_ptr);

    if (http_request_ptr->has_query(cmd_service_command_tag))
    {
        std::string e_cmd = http_request_ptr->get_query(cmd_service_command_tag);
        std::string cmd;
        url_decode(e_cmd.c_str(), cmd);

        std::string xml_param;
        if (http_request_ptr->has_query(cmd_service_xmlparam_tag))
            xml_param = http_request_ptr->get_query(cmd_service_xmlparam_tag);

        std::string server_addr = get_server_ip_from_request(http_request_ptr);
        std::string client_addr = tcp_conn->get_remote_ip().to_string();

        std::string result;
        std::string mime = process_command(http_request_ptr, cmd, xml_param, server_addr, client_addr, result);

        send_xml_response(http_request_ptr, tcp_conn, result, mime);
    } else
    {
        std::string cs_root_resource = command_service_resource_url + std::string("/");
        std::string resource = http_request_ptr->get_resource();
        std::string subresource = resource.size() > cs_root_resource.size() ? resource.substr(cs_root_resource.size()) : "";

        if (subresource.empty())
        {
            log_info(L"command_service::operator(). Generated error response for root request");
            send_error_response(http_request_ptr, tcp_conn, pion::http::types::RESPONSE_CODE_NOT_FOUND, pion::http::types::RESPONSE_MESSAGE_NOT_FOUND);
        } else
        {
            log_info(L"command_service::operator(). Serving %1%") % string_cast<EC_UTF8>(subresource);
            dvblink::filesystem_path_t item_path = settings_->get_resources_path() / subresource;

            std::ifstream file(item_path.to_string().c_str(), std::ios::binary);
            std::vector<char> file_contents((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
            if (file_contents.size() > 0)
                send_binary_response(http_request_ptr, tcp_conn, file_contents, image_mime_code);
            else
                send_error_response(http_request_ptr, tcp_conn, pion::http::types::RESPONSE_CODE_NOT_FOUND, pion::http::types::RESPONSE_MESSAGE_NOT_FOUND);
        }
    }
}

std::string command_service::process_command(const pion::http::request_ptr& http_request_ptr, const std::string& command, const std::string& request, const std::string& server_address, const std::string& client_addr, std::string& xml_response)
{
    std::string res_mime = text_xml_mime_code;

    log_info(L"command_service::process_command. Procesing command %1%") % string_cast<EC_UTF8>(command);

    if (boost::iequals(command, get_server_info_cmd))
    {
        process_server_info_cmd(xml_response);
    } else
    if (boost::iequals(command, get_devices_cmd))
    {
        process_get_devices_cmd(xml_response);
    } else
    if (boost::iequals(command, get_scanners_cmd))
    {
        process_get_scanners_cmd(request, xml_response);
    } else
    if (boost::iequals(command, start_scan_cmd))
    {
        process_start_scan_cmd(request, xml_response);
    } else
    if (boost::iequals(command, rescan_provider_cmd))
    {
        process_rescan_provider_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_rescan_settings_cmd))
    {
        process_get_rescan_settings_cmd(request, xml_response);
    } else
    if (boost::iequals(command, cancel_scan_cmd))
    {
        process_cancel_scan_cmd(request, xml_response);
    } else
    if (boost::iequals(command, apply_scan_cmd))
    {
        process_apply_scan_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_networks_cmd))
    {
        process_get_networks_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_device_status_cmd))
    {
        process_get_device_status_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_streaming_caps_cmd))
	{
        process_get_streaming_caps_cmd(request, xml_response);
    } else
    if (boost::iequals(command, search_epg_cmd))
	{
        process_search_epg_cmd(request, server_address, xml_response);
    } else
    if (boost::iequals(command, add_schedule_cmd))
	{
        process_add_schedule_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_schedules_cmd))
	{
        process_get_schedules_cmd(xml_response);
    } else
    if (boost::iequals(command, update_schedule_cmd))
	{
        process_update_schedule_cmd(request, xml_response);
    } else
    if (boost::iequals(command, remove_schedule_cmd))
	{
        process_remove_schedule_cmd(request, xml_response);
    } else
    if (boost::iequals(command, remove_recording_cmd))
	{
        process_remove_recording_cmd(request, xml_response);
    } else
    if (boost::iequals(command, set_recording_settings_cmd))
	{
        process_set_recording_settings_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_recording_settings_cmd))
    {
        process_get_recording_settings_cmd(xml_response);
    } else
    if (boost::iequals(command, set_device_settings_cmd))
	{
        process_set_device_settings_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_device_settings_cmd))
    {
        process_get_device_settings_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_device_templates_cmd))
	{
        process_get_device_templates_cmd(request, xml_response);
    } else
    if (boost::iequals(command, create_manual_device_cmd))
	{
        process_create_manual_device_cmd(request, xml_response);
    } else
    if (boost::iequals(command, delete_manual_device_cmd))
	{
        process_delete_manual_device_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_favorites_cmd))
    {
        process_get_favorites_cmd(xml_response);
    } else
    if (boost::iequals(command, set_favorites_cmd))
    {
        process_set_favorites_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_channels_visibility_cmd))
    {
        process_get_channels_visibility_cmd(xml_response);
    } else
    if (boost::iequals(command, set_channels_visibility_cmd))
    {
        process_set_channels_visibility_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_object_resume_info_cmd))
    {
        process_get_object_resume_info_cmd(request, xml_response);
    } else
    if (boost::iequals(command, set_object_resume_info_cmd))
    {
        process_set_object_resume_info_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_channel_overwrites_cmd))
    {
        process_get_channel_overwrites_cmd(xml_response);
    } else
    if (boost::iequals(command, set_channel_overwrites_cmd))
    {
        process_set_channel_overwrites_cmd(request, xml_response);
    } else
    if (boost::iequals(command, drop_provider_on_device_cmd))
    {
        process_drop_provider_on_device_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_channels_cmd))
    {
        process_get_channels_cmd(request, server_address, client_addr, xml_response);
    } else
    if (boost::iequals(command, get_scanned_channels_cmd))
    {
        process_get_scanned_channels_cmd(xml_response);
    } else
    if (boost::iequals(command, get_oob_channel_url_cmd))
    {
        process_get_oob_channel_url_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_recordings_cmd))
    {
        process_get_recordings_cmd(xml_response);
    } else
    if (boost::iequals(command, execute_command_cmd))
	{
        process_execute_command_cmd(request, server_address, xml_response);
    } else
    if (boost::iequals(command, get_stream_info_cmd))
	{
        process_get_stream_info_cmd(request, server_address, client_addr, xml_response);
    } else
    if (boost::iequals(command, play_channel_cmd))
	{
        process_play_channel_cmd(request, server_address, client_addr, xml_response);
    } else
    if (boost::iequals(command, stop_channel_cmd))
	{
        process_stop_channel_cmd(request, xml_response);
    } else
    if (boost::iequals(command, timeshift_get_stats_cmd))
	{
        process_timeshift_get_stats_cmd(request, xml_response);
    } else
    if (boost::iequals(command, timeshift_seek_cmd))
	{
        process_timeshift_seek_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_raw_channels_cmd))
	{
        process_get_raw_channels_cmd(server_address, xml_response);
    } else
    if (boost::iequals(command, get_playlist_m3u_cmd))
	{
        process_get_playlist_m3u_cmd(http_request_ptr, server_address, client_addr, xml_response);
        res_mime = m3u_playlist_mime_code;
    } else
    if (boost::iequals(command, get_xmltv_epg_cmd))
	{
        process_get_xmltv_epg_cmd(http_request_ptr, server_address, xml_response);
        res_mime = text_xml_mime_code;
    } else
    if (boost::iequals(command, repair_database_cmd))
    {
        process_repair_database_cmd(xml_response);
    } else
    if (boost::iequals(command, force_epg_update_cmd))
    {
        process_force_epg_update_cmd(xml_response);
    } else
    if (boost::iequals(command, enable_epg_updates_cmd))
    {
        process_enable_epg_updates_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_installed_products_cmd))
	{
        process_get_installed_products_cmd(xml_response);
    } else
    if (boost::iequals(command, activate_product_cmd))
	{
        process_activate_product_cmd(request, xml_response);
    } else
    if (boost::iequals(command, activate_product_trial_cmd))
	{
        process_activate_product_trial_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_epg_sources_cmd))
	{
        process_get_epg_sources_cmd(xml_response);
    } else
    if (boost::iequals(command, get_epg_channels_cmd))
	{
        process_get_epg_channels_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_epg_channel_config_cmd))
	{
        process_get_epg_channel_config_cmd(request, xml_response);
    } else
    if (boost::iequals(command, set_epg_channel_config_cmd))
	{
        process_set_epg_channel_config_cmd(request, xml_response);
    } else
    if (boost::iequals(command, get_objects_cmd))
	{
        process_get_objects_cmd(request, server_address, client_addr, xml_response);
    } else
    if (boost::iequals(command, search_objects_cmd))
	{
        process_search_objects_cmd(request, server_address, xml_response);
    } else
    if (boost::iequals(command, remove_object_cmd))
	{
        process_remove_object_cmd(request, xml_response);
    } else
    if (boost::iequals(command, stop_recording_cmd))
	{
        process_stop_recording_cmd(request, xml_response);
    } else
    if (boost::iequals(command, match_epg_channels_cmd))
	{
        process_match_epg_channels_cmd(request, xml_response);
    } else
    {
        log_error(L"command_service::process_command. Unknown command %1%") % string_cast<EC_UTF8>(command);
        //unknown command
        write_to_xml(command_response_t(DvbLink_StatusNotImplemented), xml_response);
    }

    return res_mime;
}

} // dvblink
