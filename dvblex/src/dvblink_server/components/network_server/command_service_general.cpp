/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp> 
#include <pion/http/response_writer.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_message_server.h>
#include <dl_message_devices.h>
#include <dl_message_channels.h>
#include <dl_message_common.h>
#include <dl_message_recorder.h>
#include <dli_message_driven.h>
#include <dl_message_license.h>
#include <dl_message_epg.h>
#include <dl_message_addresses.h>
#include <dl_message_playback.h>
#include <dl_url_encoding.h>
#include <dl_xml_serialization.h>
#include <dl_server_info.h>
#include <dl_stream.h>
#include <dl_commands.h>
#include <dl_status.h>
#include <dl_m3u.h>
#include <dl_server_serializer.h>
#include <dl_device_serializer.h>
#include <dl_channel_serializer.h>
#include <dl_stream_serializer.h>
#include <dl_epg_config_serializer.h>
#include <dl_products_serializer.h>
#include <dl_recommender_serializer.h>
#include <dl_language_settings.h>
#include <dl_xml_command.h>
#include <dl_epg_source_commands.h>
#include "constants.h"
#include "mime_types.h"
#include "pion_helper.h"
#include "command_service.h"

#ifdef _MSC_VER
#pragma warning (disable : 4100)
#endif

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;

namespace dvblex {

void command_service::process_server_info_cmd(std::string& xml_response)
{
    server::server_info_request req;
    server::server_info_response resp;
    if (message_queue_->send(server_message_queue_addressee, req, resp) == success)
    {
        std::string str;
        write_to_xml(resp.server_info_, str);
        write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_get_devices_cmd(std::string& xml_response)
{
    devices::devices_info_request req;
    devices::devices_info_response resp;
    if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        std::string str;
        write_to_xml(resp.device_info_list_, str);
        write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_get_streaming_caps_cmd(const std::string& request, std::string& xml_response)
{
	server_caps_request_t server_caps_request;
	if (read_from_xml(request, server_caps_request))
	{
		server_capabilities_t server_caps;

        //stream service is responsible for streaming caps/timeshifting
        stream_service_->fill_streaming_caps(server_caps);

#ifdef _ANDROID_ALL
        server_caps.can_record_ = false;
#else
        server_caps.can_record_ = true;
#endif

	    server_caps.supports_device_mgmt_ = true;

        //
        // check if addressees from addressees_to_check are exist
        //
        for (size_t i = 0; i < server_caps_request.addressees_to_check_.size(); ++i)
        {
            messaging::server::server_info_request req;
            messaging::server::server_info_response resp;
            messaging::message_error err = message_queue_->send(server_caps_request.addressees_to_check_[i], req, resp);
            if (err != messaging::addressee_not_found)
                server_caps.addressees_.push_back(server_caps_request.addressees_to_check_[i]);
        }

		std::string str;
		write_to_xml(server_caps, str);
		write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_scanners_cmd(const std::string& request, std::string& xml_response)
{
	get_scanners_request_t get_scanners_request;
	if (read_from_xml(request, get_scanners_request))
	{
        devices::get_scanners_request req(get_scanners_request.device_id_, get_scanners_request.standard_);
        devices::get_scanners_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
            resp.result_)
        {
		    std::string str;
    		write_to_xml(resp.providers_, str);
		    write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_start_scan_cmd(const std::string& request, std::string& xml_response)
{
	start_scan_request_t start_scan_request;
	if (read_from_xml(request, start_scan_request) && !start_scan_request.device_id_.get().empty())
	{

        devices::start_scan_request req(start_scan_request.device_id_, start_scan_request.parameters_);
        devices::start_scan_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success)
        {
            start_scan_response_t r;
            r.result_ = resp.scan_start_result_;

		    std::string str;
    		write_to_xml(r, str);

            write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_rescan_settings_cmd(const std::string& request, std::string& xml_response)
{
	get_rescan_provider_settings_request_t rescan_settings_request;
	if (read_from_xml(request, rescan_settings_request) && !rescan_settings_request.device_id_.get().empty() && !rescan_settings_request.headend_id_.get().empty())
	{
        devices::get_rescan_settings_request req(rescan_settings_request.device_id_, rescan_settings_request.headend_id_);
        devices::get_rescan_settings_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
            resp.result_)
        {
            get_rescan_provider_settings_response_t r;
            r.settings_ = resp.settings_;

		    std::string str;
            write_to_xml(r, str);

		    write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_rescan_provider_cmd(const std::string& request, std::string& xml_response)
{
	rescan_provider_request_t rescan_provider_request;
	if (read_from_xml(request, rescan_provider_request) && !rescan_provider_request.device_id_.get().empty() && !rescan_provider_request.device_id_.get().empty())
	{
        devices::rescan_provider_request req(rescan_provider_request.device_id_, rescan_provider_request.headend_id_, rescan_provider_request.settings_);
        devices::rescan_provider_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
            resp.result_)
        {
		    write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_cancel_scan_cmd(const std::string& request, std::string& xml_response)
{
	cancel_scan_request_t cancel_scan_request;
	if (read_from_xml(request, cancel_scan_request))
	{

        devices::cancel_scan_request req(cancel_scan_request.device_id_);
        devices::cancel_scan_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
            resp.result_)
		    write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
        else
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_apply_scan_cmd(const std::string& request, std::string& xml_response)
{
	apply_scan_request_t apply_scan_request;
	if (read_from_xml(request, apply_scan_request))
	{

        devices::apply_scan_request req(apply_scan_request.device_id_);
        devices::apply_scan_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
            resp.result_)
		    write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
        else
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_networks_cmd(const std::string& request, std::string& xml_response)
{
	get_networks_request_t get_networks_request;
	if (read_from_xml(request, get_networks_request))
	{

        devices::get_networks_request req(get_networks_request.device_id_);
        devices::get_networks_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
            resp.result_)
        {
		    std::string str;
    		write_to_xml(resp.networks_, str);
		    write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_device_status_cmd(const std::string& request, std::string& xml_response)
{
	get_device_status_request_t get_device_status_request;
	if (read_from_xml(request, get_device_status_request))
	{

        devices::get_device_status_request req(get_device_status_request.device_id_);
        devices::get_device_status_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
            resp.result_)
        {
		    std::string str;
    		write_to_xml(resp.status_, str);
		    write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

//this is dummy TVAdviser command processor, which allows legacy clients to get EPG
//if it is not present then get_mapped_channels request returns an error and that aborts epg loading sequence
static bool dummy_tvadviser_cmd_processor(const dvblink::message_addressee_t& addressee,
                                          const xml_message_request& request, 
                                          xml_message_response& response)
{
    bool ret_val = false;
    if (addressee == tvadviser_message_queue_addressee)
    {
        if (request.cmd_id_ == dvblex::tvadviser::get_mapped_sources_cmd)
        {
            dvblink::ncanto::map_provider_to_map_dvblink_ch_ncanto_ch_t channels;
            get_mapped_channels_response resp(channels);
            std::string tmp;
            write_to_xml(resp, tmp);
            response.xml_ = tmp;
            response.result_ = dvblink::xmlcmd_result_success;

            ret_val = true;
        }
    }
    return ret_val;
}

void command_service::process_execute_command_cmd(const std::string& request, const std::string& server_address, std::string& xml_response)
{
    xml_command_base xml_cmd;
	if (read_from_xml(request, xml_cmd))
	{
        xml_message_request req(stream_service_->get_http_prefix(), server_address, xml_cmd.get_command_id(), xml_cmd.get_in_params());
        xml_message_response resp;

        if (dummy_tvadviser_cmd_processor(xml_cmd.get_addressee(), req, resp) ||
            message_queue_->send(xml_cmd.get_addressee(), req, resp) == dvblink::messaging::success)
        {
            xml_response_base xml_resp(resp.result_, resp.xml_);

            std::string str;
            write_to_xml(xml_resp, str);
            write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_stream_info_cmd(const std::string& request, const std::string& server_address, const std::string& client_address, std::string& xml_response)
{
	get_stream_info_request_t get_stream_info_request;
	if (read_from_xml(request, get_stream_info_request))
	{
        //add safe IP for streaming requests
        stream_service_->add_client_ip(client_address);

        url_stream_info_list_t stream_info;

        for (size_t i=0; i<get_stream_info_request.channels_.size(); i++)
        {
            dvblink::url_address_t url;
            url_stream_info_t si;
            si.channel_id_ = get_stream_info_request.channels_[i];
            si.stream_url_ = stream_service_->get_direct_streaming_url(get_stream_info_request.channels_[i], server_address, get_stream_info_request.stream_client_id_);
            stream_info.push_back(si);
        }

	    std::string str;
		write_to_xml(stream_info, str);
	    write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_force_epg_update_cmd(std::string& xml_response)
{
    epg::force_epg_update_request req;
    message_queue_->post(dvblink::messaging::broadcast_addressee, req);

    write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
}

void command_service::process_enable_epg_updates_cmd(const std::string& request, std::string& xml_response)
{
	enable_epg_updates_request_t enable_req;
	if (read_from_xml(request, enable_req))
	{
        epg::enable_periodic_epg_updates_request req;
        req.enable_ = enable_req.enable_;
        message_queue_->post(dvblink::messaging::broadcast_addressee, req);

        write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
    }
}

void command_service::process_play_channel_cmd(const std::string& request, const std::string& server_address, const std::string& client_addr, std::string& xml_response)
{
	play_channel_request_t req;
	if (read_from_xml(request, req))
	{
        play_channel_response_t resp;
        dvblink::EStatus status = stream_service_->process_play_channel_request(req, server_address, client_addr, resp);
        if (status == DvbLink_StatusOk)
        {
            std::string str;
            write_to_xml(resp, str);
		    write_to_xml(command_response_t(status, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(status), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_stop_channel_cmd(const std::string& request, std::string& xml_response)
{
	stop_channel_request_t req;
	if (read_from_xml(request, req))
	{
        if (stream_service_->process_stop_channel_request(req))
        {
		    write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_timeshift_get_stats_cmd(const std::string& request, std::string& xml_response)
{
	timeshift_stats_request_t req;
	if (read_from_xml(request, req))
	{
        timeshift_stats_response_t resp;
        if (stream_service_->process_timeshift_stats_request(req, resp))
        {
            std::string str;
            write_to_xml(resp, str);
		    write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_timeshift_seek_cmd(const std::string& request, std::string& xml_response)
{
	timeshift_seek_request_t req;
	if (read_from_xml(request, req))
	{
        if (stream_service_->process_timeshift_seek_request(req))
        {
		    write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_epg_sources_cmd(std::string& xml_response)
{
    epg::get_epg_sources_request req;
    epg::get_epg_sources_response resp;
    if (message_queue_->send(epg_manager_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        std::string str;
        write_to_xml(resp.epg_sources_, str);
        write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_get_epg_channels_cmd(const std::string& request, std::string& xml_response)
{
	get_epg_channels_request_t r;
	if (read_from_xml(request, r))
	{
        epg::get_epg_channels_request req;
        epg::get_epg_channels_response resp;
        if (message_queue_->send(dvblink::message_addressee_t(r.epg_source_id_.get()), req, resp) == success &&
            resp.result_)
        {
            std::string str;
            write_to_xml(resp.channels_, str);
            write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_epg_channel_config_cmd(const std::string& request, std::string& xml_response)
{
	get_epg_channel_config_request_t r;
	if (read_from_xml(request, r))
	{
        epg::get_channel_epg_config_request req;
        req.channels_ = r.channels_;
        epg::get_channel_epg_config_response resp;
        if (message_queue_->send(epg_manager_message_queue_addressee, req, resp) == success &&
            resp.result_)
        {
            std::string str;
            write_to_xml(resp.epg_config_, str);
            write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_set_epg_channel_config_cmd(const std::string& request, std::string& xml_response)
{
    epg::set_channel_epg_config_request req;
	if (read_from_xml(request, req.epg_config_))
	{
        epg::set_channel_epg_config_response resp;
        if (message_queue_->send(epg_manager_message_queue_addressee, req, resp) == success &&
            resp.result_)
        {
            write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_match_epg_channels_cmd(const std::string& request, std::string& xml_response)
{
	match_epg_channels_request_t r;
	if (read_from_xml(request, r))
	{
        epg::match_epg_channels_request req(r.epg_source_id_, r.channels_);
        epg::match_epg_channels_response resp;
        if (message_queue_->send(epg_manager_message_queue_addressee, req, resp) == success &&
            resp.result_)
        {
            std::string str;
            write_to_xml(resp.match_info_, str);
            write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_installed_products_cmd(std::string& xml_response)
{
    license::get_installed_products_request req;
    license::get_installed_products_response resp;
    if (message_queue_->send(server_message_queue_addressee, req, resp) == success &&
        resp.result_.get())
    {
        std::string str;
        write_to_xml(resp.products_, str);
        write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_activate_product_cmd(const std::string& request, std::string& xml_response)
{
	activate_product_request_t r;
	if (read_from_xml(request, r))
	{
        license::activate_product_request req(r.activation_info_);
        license::activate_product_response resp;
        if (message_queue_->send(server_message_queue_addressee, req, resp) == success)
        {
            activate_product_response_t rr;
            rr.result_ = resp.result_;
            std::string str;
            write_to_xml(rr, str);
            write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_activate_product_trial_cmd(const std::string& request, std::string& xml_response)
{
	activate_product_request_t r;
	if (read_from_xml(request, r))
	{
        license::activate_product_trial_request req(r.activation_info_.id_);
        license::activate_product_trial_response resp;
        if (message_queue_->send(server_message_queue_addressee, req, resp) == success)
        {
            activate_product_response_t rr;
            rr.result_ = resp.result_;
            std::string str;
            write_to_xml(rr, str);
            write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_xmltv_epg_cmd(const pion::http::request_ptr& http_request_ptr, const std::string& server_address, std::string& xml_response)
{
    recorder::get_xmltv_epg_request xmltv_request;
    std::string empty_request;
    channel_desc_list_t cdl;
    if (get_channel_descriptions(empty_request, server_address, xmltv_request.channels_))
    {
        //number of days
        if (http_request_ptr->has_query("days"))
            xmltv_request.days_ = atoi(http_request_ptr->get_query("days").c_str());

        if (http_request_ptr->has_query("dt_tz_form"))
            xmltv_request.simple_time_format_ = false;

        recorder::get_xmltv_epg_response xmltv_response;
        if (message_queue_->send(recorder_message_queue_addressee, xmltv_request, xmltv_response) == success &&
            xmltv_response.result_)
        {
            xml_response = xmltv_response.epg_;
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

static void add_transcoder_tags(const pion::http::request_ptr& http_request_ptr, std::string& url)
{
    static const char* transcoder_params[] = {request_param_transcoder.c_str(), request_param_transcoder_bitrate.c_str(), request_param_transcoder_width.c_str(), request_param_transcoder_height.c_str(), request_param_transcoder_lng.c_str(), request_param_transcoder_scale.c_str()};
    static const size_t transcoder_params_num = sizeof(transcoder_params) / sizeof(char*);

    for (size_t i=0; i<transcoder_params_num; i++)
    {
        std::string param = transcoder_params[i];
        if (http_request_ptr->has_query(param))
        {
            std::string value = http_request_ptr->get_query(param);
            url += "&" + param + "=" + value;
        }
    }
}

static bool chanel_desc_compare_num(const channel_description_t& first_item, const channel_description_t& second_item)
{
    if (first_item.num_.get() == -1)
        return false;

    if (second_item.num_.get() == -1)
        return true;

    int num1 = first_item.num_.get() == -1 ? 0 : first_item.num_.get();
    int num2 = second_item.num_.get() == -1 ? 0 : second_item.num_.get();

    int sub_num1 = first_item.sub_num_.get() == -1 ? 0 : first_item.sub_num_.get();
    int sub_num2 = second_item.sub_num_.get() == -1 ? 0 : second_item.sub_num_.get();

	return (num1 * 1000 + sub_num1) < (num2 * 1000 + sub_num2);
}

static bool chanel_desc_compare_name(const channel_description_t& first_item, const channel_description_t& second_item)
{
	return boost::to_upper_copy(first_item.name_.get()) < boost::to_upper_copy(second_item.name_.get());
}

enum m3u_channel_filter_e
{
    m3u_cf_none,
    m3u_cf_tv,
    m3u_cf_radio
};

void command_service::process_get_playlist_m3u_cmd(const pion::http::request_ptr& http_request_ptr, const std::string& server_address, const std::string& client_address, std::string& xml_response)
{
    //add safe IP for streaming requests
    stream_service_->add_client_ip(client_address);

    std::string client_id; //use empty string - id will be generated from client's ip address
    if (http_request_ptr->has_query(client_param_name))
    {
        std::string e_client_id = http_request_ptr->get_query(client_param_name);
        url_decode(e_client_id.c_str(), client_id);
    }

    bool sort_on_name = true;
    if (http_request_ptr->has_query(sort_mode_param_name))
    {
        std::string mode = http_request_ptr->get_query(sort_mode_param_name);
        sort_on_name = !boost::iequals(mode, sort_mode_param_on_number);
    }

    m3u_channel_filter_e channel_filter = m3u_cf_none;
    if (http_request_ptr->has_query(channel_type_param_name))
    {
        std::string type = http_request_ptr->get_query(channel_type_param_name);

        if (boost::iequals(type, sort_mode_param_tv))
            channel_filter = m3u_cf_tv;
        else if (boost::iequals(type, sort_mode_param_radio))
            channel_filter = m3u_cf_radio;
    }

    std::string str;
    std::string empty_request;
    channel_desc_list_t cdl;
    if (get_channel_descriptions(empty_request, server_address, cdl))
    {
        //sort channels 
        std::sort(cdl.begin(), cdl.end(), sort_on_name ? chanel_desc_compare_name : chanel_desc_compare_num);

        std::stringstream str_out;
        str_out << m3u_playlist_header << std::endl;

        for (size_t i=0; i<cdl.size(); i++)
        {
            channel_description_t& channel = cdl[i];

            if (channel_filter == m3u_cf_tv && channel.type_ != ct_tv)
                continue;

            if (channel_filter == m3u_cf_radio && channel.type_ != ct_radio)
                continue;

            std::string url = stream_service_->get_direct_streaming_url(channel.id_, server_address, client_id);
            add_transcoder_tags(http_request_ptr, url);

            str_out << m3u_playlist_inf_section << ":-1";
            
            //add tvg-id tag with a channel id
            str_out << " " << m3u_plus_tvg_id << "=\"" << channel.id_.to_string() << "\"";
            
            //channel name
            str = channel.name_.to_string();
            boost::replace_all(str, "\"", "");
            str_out << " " << m3u_plus_tvg_name << "=\"" << str << "\"";

            //channel number
            if (channel.num_.get() > 0)
            {
                str_out << " " << m3u_plus_tvg_chno << "=\"" << channel.num_.get();
                if (channel.sub_num_.get() > 0)
                    str_out << "." << channel.sub_num_.get();
                
                str_out << "\"";
            }

            //channel logo
            if (!channel.logo_.empty())
                str_out << " " << m3u_plus_tvg_logo << "=\"" << channel.logo_.to_string() << "\"";

            //channel type
            str = channel.type_ == ct_radio ? m3u_plus_tvg_type_radio : m3u_plus_tvg_type_tv;
            str_out << " " << m3u_plus_tvg_type << "=\"" << str << "\"";

            //radio=true for radio channels
            if (channel.type_ == ct_radio)
                str_out << " radio=true";

            //standard channel name
            str_out << "," << channel.name_.get() << std::endl;
            str_out << url << std::endl;
        }

        xml_response = str_out.str();
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_get_objects_cmd(const std::string& request, const std::string& server_address, const std::string& client_address, std::string& xml_response)
{
    dvblex::playback::pb_object_requester_t object_requester;
	if (read_from_xml(request, object_requester))
	{
        //add safe IP for streaming requests
        stream_service_->add_client_ip(client_address);

        dvblink::message_addressee_t addressee = boost::uuids::nil_uuid();
        if (object_requester.is_root_request())
        {
            addressee = server_message_queue_addressee;
            logging::log_ext_info(L"process_get_objects_cmd. Root object request");
        } else
        {
            std::string source_id;
            std::string source_object_id;
            if (dvblex::playback::parse_object_id(object_requester.object_id_, source_id, source_object_id))
            {
                addressee = source_id;
                logging::log_ext_info(L"process_get_objects_cmd. Request for object(s) from source %1%") % addressee.to_wstring();
            }
        }

        if (!addressee.empty())
        {
            dvblink::messaging::playback::get_objects_request req;
            req.object_id_ = object_requester.object_id_;
            req.object_type_ = object_requester.object_type_;
            req.item_type_ = object_requester.item_type_;
            req.start_position_ = object_requester.start_position_;
            req.requested_count_ = object_requester.requested_count_;
            req.is_children_request_ = object_requester.is_children_request_;
            req.server_address_ = server_address;
            req.proto_ = stream_service_->get_http_prefix();

            dvblink::messaging::playback::get_objects_response resp;
            if (message_queue_->send(addressee, req, resp) == success &&
                resp.result_)
            {
		        std::string str;
    		    write_to_xml(resp.object_, str);
		        write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
            } else
            {
                write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
            }
        } else
        {
            logging::log_error(L"command_service::process_get_objects_cmd. Object request from unknown source (%1%)") % object_requester.object_id_.to_wstring();
            write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_remove_object_cmd(const std::string& request, std::string& xml_response)
{
    playback::pb_object_remover_t orm;
	if (read_from_xml(request, orm))
	{
        std::string source_id;
        std::string source_object_id;
        if (playback::parse_object_id(orm.object_id_, source_id, source_object_id))
        {
            dvblink::messaging::playback::remove_object_request request(orm.object_id_);
            dvblink::messaging::playback::remove_object_response response;
            if (message_queue_->send(dvblink::message_addressee_t(source_id), request, response) == success)
            {
                write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
            } else
            {
                write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
            }
	    } else
	    {
            logging::log_error(L"command_service::process_remove_object_cmd. unable to parse object id %1%") % orm.object_id_.to_wstring();
            write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	    }
	} else
	{
        logging::log_error(L"command_service::process_remove_object_cmd. unable to deserialize request object");
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_search_objects_cmd(const std::string& request, const std::string& server_address, std::string& xml_response)
{
    dvblex::playback::pb_object_searcher_t object_searcher;
	if (read_from_xml(request, object_searcher))
	{
        dvblink::message_addressee_t addressee = boost::uuids::nil_uuid();
        if (object_searcher.is_root_request())
        {
            addressee = server_message_queue_addressee;
            logging::log_ext_info(L"process_search_objects_cmd. Search root object request");
        } else
        {
            std::string source_id;
            std::string source_object_id;
            if (dvblex::playback::parse_object_id(object_searcher.object_id_, source_id, source_object_id))
            {
                addressee = source_id;
                logging::log_ext_info(L"process_search_objects_cmd. Search request for object(s) from source %1%") % addressee.to_wstring();
            }
        }

        if (!addressee.empty())
        {
            dvblink::messaging::playback::search_objects_request req;
            req.object_id_ = object_searcher.object_id_;
            req.object_type_ = object_searcher.object_type_;
            req.item_type_ = object_searcher.item_type_;
            req.search_string_ = object_searcher.search_string_;
            req.server_address_ = server_address;
            req.proto_ = stream_service_->get_http_prefix();

            dvblink::messaging::playback::search_objects_response resp;
            if (message_queue_->send(addressee, req, resp) == success &&
                resp.result_)
            {
		        std::string str;
    		    write_to_xml(resp.object_, str);
		        write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
            } else
            {
                write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
            }
        } else
        {
            logging::log_error(L"command_service::process_search_objects_cmd. Search objects request from unknown source (%1%)") % object_searcher.object_id_.to_wstring();
            write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_stop_recording_cmd(const std::string& request, std::string& xml_response)
{
    playback::pb_stop_recording_t sr;
	if (read_from_xml(request, sr))
	{
        std::string source_id;
        std::string source_object_id;
        if (playback::parse_object_id(sr.object_id_, source_id, source_object_id))
        {
            dvblink::messaging::playback::stop_recording_request request(sr.object_id_);
            dvblink::messaging::playback::stop_recording_response response;
            if (message_queue_->send(dvblink::message_addressee_t(source_id), request, response) == success)
            {
                write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
            } else
            {
                write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
            }
	    } else
	    {
            logging::log_error(L"command_service::process_stop_recording_cmd. unable to parse object id %1%") % sr.object_id_.to_wstring();
            write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	    }
	} else
	{
        logging::log_error(L"command_service::process_stop_recording_cmd. unable to deserialize request object");
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_device_settings_cmd(const std::string& request, std::string& xml_response)
{
	get_device_settings_request_t r;
	if (read_from_xml(request, r))
	{
        devices::get_device_settings_request req(r.device_id_);
        devices::get_device_settings_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success && resp.result_)
        {
            get_device_settings_response_t rr;
            rr.settings_ = resp.settings_;
            std::string str;
            write_to_xml(rr, str);
            write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_set_device_settings_cmd(const std::string& request, std::string& xml_response)
{
	set_device_settings_request_t r;
	if (read_from_xml(request, r))
	{
        devices::set_device_settings_request req(r.device_id_, r.settings_);
        devices::set_device_settings_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success && resp.result_)
        {
            write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_device_templates_cmd(const std::string& request, std::string& xml_response)
{
    devices::get_device_templates_request req;
    devices::get_device_templates_response resp;
    if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success)
    {
        get_device_templates_response_t rr;
        rr.templates_ = resp.templates_;
        std::string str;
        write_to_xml(rr, str);
        write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
    } else
    {
        write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
    }
}

void command_service::process_create_manual_device_cmd(const std::string& request, std::string& xml_response)
{
	create_manual_device_request_t r;
	if (read_from_xml(request, r))
	{
        devices::create_manual_device_request req(r.params_);
        devices::create_manual_device_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success && resp.result_)
        {
            write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_delete_manual_device_cmd(const std::string& request, std::string& xml_response)
{
	delete_manual_device_request_t r;
	if (read_from_xml(request, r))
	{
        devices::delete_manual_device_request req(r.device_id_);
        devices::delete_manual_device_response resp;
        if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success && resp.result_)
        {
            write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_get_object_resume_info_cmd(const std::string& request, std::string& xml_response)
{
	object_get_resume_info_request r;
	if (read_from_xml(request, r))
	{
        dvblink::messaging::playback::get_pb_resume_info_request req(r.object_id_);
        dvblink::messaging::playback::get_pb_resume_info_response resp;
        if (message_queue_->send(server_message_queue_addressee, req, resp) == success)
        {
            object_get_resume_info_response rr;
            rr.pos_ = resp.pos_;
            std::string str;
            write_to_xml(rr, str);
            write_to_xml(command_response_t(DvbLink_StatusOk, &str), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

void command_service::process_set_object_resume_info_cmd(const std::string& request, std::string& xml_response)
{
	object_set_resume_info_request r;
	if (read_from_xml(request, r))
	{
        dvblink::messaging::playback::set_pb_resume_info_request req(r.object_id_, r.pos_);
        dvblink::messaging::playback::set_pb_resume_info_response resp;
        if (message_queue_->send(server_message_queue_addressee, req, resp) == success && resp.result_)
        {
            write_to_xml(command_response_t(DvbLink_StatusOk), xml_response);
        } else
        {
            write_to_xml(command_response_t(DvbLink_StatusError), xml_response);
        }
	} else
	{
        write_to_xml(command_response_t(DvbLink_StatusInvalidData), xml_response);
	}
}

} // dvblink
