/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <map>
#include <boost/thread/mutex.hpp>
#include <dl_types.h>
#include <dl_timer_procedure.h>
#include <dvblex_streaming_lib/internet_gateway.h>

namespace dvblex {

struct ext_port_desc
{
    ext_port_desc(const dvblink::network_port_t& port, const char* desc) :
        port_(port), desc_(desc)
    {}

    dvblink::network_port_t port_;
    std::string desc_;
};

typedef std::vector<ext_port_desc> ext_port_desc_list_t;

class ext_port_mapper
{
protected:
    enum ext_port_mapper_mask_e
    {
        epp_mask_none = 0x00000000,
        epp_mask_http = 0x00000001,
        epp_mask_https = 0x00000002
    };

public:
    ext_port_mapper();
    ~ext_port_mapper();

    void start(const ext_port_desc_list_t& http_ports, bool forward_http, const ext_port_desc_list_t& https_ports, bool forward_https);
    void stop();

    void change_forwarding(bool forward_http, bool forward_https);

protected:
    boost::mutex lock_;
    dvblink::engine::timer_procedure<ext_port_mapper>* timer_;
    unsigned long mapping_mask_;
    ext_port_desc_list_t http_ports_;
    ext_port_desc_list_t https_ports_;

    void timer_func(const boost::system::error_code& e);
    bool apply_port_mapping(bool broadcast_delete, unsigned long prev_mask, bool log_errors);
    bool broadcast_add_port(dvblink::streaming::igd_info_t& igd_info, dvblink::network_port_t& port, const char* description, bool log_errors);
    bool broadcast_delete_port(dvblink::streaming::igd_info_t& igd_info, dvblink::network_port_t& port, const char* description, bool log_errors);
    unsigned long get_mask_from_flags(bool forward_http, bool forward_https);
};

} // dvblex
