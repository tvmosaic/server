/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <dl_types.h>
#include <dl_message_queue.h>
#include <dl_channels.h>
#include "settings.h"
#include "stream_service.h"

namespace dvblex { 

bool ns_get_channel_descriptions(dvblink::favorite_id_t& favorite_id, dvblink::messaging::message_queue_t& message_queue, network_server_settings_t* settings, 
                                               stream_service* stream_service, const std::string& server_address, channel_desc_list_t& cdl);

bool ns_get_raw_channel_descriptions(dvblink::messaging::message_queue_t& message_queue, channel_desc_list_t& cdl);

}
