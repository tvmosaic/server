/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning (disable : 4100)
#pragma warning (disable : 4244)
#endif

#include <pion/http/plugin_service.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include <map>
#include <boost/thread.hpp>
#include <dli_server.h>
#include <dli_stream_source.h>
#include <dl_message_queue.h>
#include "network_streamer_base.h"
#include <dl_stream_serializer.h>
#include <dl_server_info.h>
#include <tc_types.h>
#include "settings.h"
#include "http_timeshifted_streamer.h"
#include "playback_objects.h"

namespace dvblex {

class http_server_type : public pion::http::plugin_service
{
public:
    http_server_type()
        : pion::http::plugin_service()
    {}

    virtual ~http_server_type()
    {}

    virtual bool is_https(){return false;}

    virtual bool is_streaming_service(){return false;}

    std::string get_http_prefix()
    {
        if (is_https())
            return "https";

        return "http";
    }
};

class stream_service : public streamer_service_callbacks_t, public http_server_type
{
protected:

    struct client_ip_desc_t
    {
        client_ip_desc_t()
            {time(&added);}

        time_t added;
    };

    typedef std::map<std::string, client_ip_desc_t> client_ip_map_t;

    struct streamer_desc_t
    {
        network_streamer_base_object_t streamer;
        int handle;
    };

    typedef std::map<dvblink::streamer_id_t, streamer_desc_t> network_streamer_map_t;

public:
    stream_service(dvblink::i_server_t server, dvblink::messaging::message_queue_t message_queue, unsigned short port, network_server_settings_t* settings);
    virtual ~stream_service();

    virtual void operator()(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn);

    virtual void stop();

    std::string get_direct_streaming_url(const dvblink::channel_id_t& channel_id, const std::string& server_address, const std::string& client_id);
    dvblink::EStatus process_play_channel_request(const play_channel_request_t& request, const std::string& server_address, const std::string& client_address, play_channel_response_t& response);
    bool process_stop_channel_request(const stop_channel_request_t& request);
    bool process_timeshift_stats_request(const timeshift_stats_request_t& request, timeshift_stats_response_t& response);
    bool process_timeshift_seek_request(const timeshift_seek_request_t& request);

    void fill_streaming_caps(dvblex::server_capabilities_t& caps);

    unsigned short get_streaming_port() {return port_;}

    virtual bool is_streaming_service(){return true;}

    void add_client_ip(const std::string& client_ip);

protected:
    dvblink::messaging::message_queue_t message_queue_;
    dvblink::i_server_t server_;
    unsigned short port_;

    network_streamer_map_t active_streamer_map_;
    int stream_handle_seed_;
    boost::mutex streamer_map_lock_;
    network_server_settings_t* settings_;
    playback_objects_t playback_objects_;
    client_ip_map_t client_ip_map_;
    boost::mutex client_ip_map_lock_;

protected:
    virtual void streamer_stopped(const dvblink::streamer_id_t& id);

    int add_streamer(network_streamer_base_object_t& streamer);
    void remove_streamer(const dvblink::streamer_id_t& id);
    network_streamer_base_object_t get_streamer(const dvblink::streamer_id_t& id);
    network_streamer_base_object_t get_streamer_object_from_handle(int handle);
    bool get_streamer_id(int handle, dvblink::streamer_id_t& id);
    void remove_all_streamers();
    bool is_transcoding_supported();

    void remove_active_streamer(const dvblink::streamer_id_t& id);
    void remove_active_streamer(int handle);
    std::string get_timeshifting_streaming_url(const dvblink::channel_id_t& channel_id, const std::string& server_address, int handle);
    bool is_direct_request(const pion::http::request_ptr& http_request_ptr);
    bool process_direct_request(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn);
    bool process_timeshift_request(const pion::http::request_ptr& http_request_ptr, const pion::tcp::connection_ptr& tcp_conn);
    void create_and_clean_timeshift_dir();
    http_timeshifted_streamer_object_t get_timeshift_object_from_handle(int handle);
    dvblink::messaging::i_stream_source_control_t get_stream_control_if();
    stream_processor_base* get_processor_from_http_request(const pion::http::request_ptr& request);
    stream_processor_base* get_processor_from_stream_type(const std::string& stream_type, const dvblink::transcoder::transcoder_params& tp);
    stream_processor_base* get_processor_from_play_request(const play_channel_request_t& request);
    network_streamer_base_object_t get_streamer_from_play_request(const std::string& client_id, const std::string& server_address, 
        const play_channel_request_t& request, dvblink::EStatus& status);
    bool is_valid_client_ip(const std::string& client_ip);
};

class stream_service_https : public stream_service
{
public:
    stream_service_https(dvblink::i_server_t server, dvblink::messaging::message_queue_t message_queue, unsigned short port, network_server_settings_t* settings)
        : stream_service(server, message_queue, port, settings)
    {}

    virtual ~stream_service_https()
    {}

    virtual bool is_https(){return true;}
};

} // dvblink
