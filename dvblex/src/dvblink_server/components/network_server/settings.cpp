/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_permanent_settings.h>
#include <dl_pugixml_helper.h>
#include <dl_file_procedures.h>
#include <dl_crypt.h>
#include "constants.h"
#include "settings.h"

using namespace dvblex::settings;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink;

namespace dvblex { 

static const std::string network_server_settings_root                = "network_server_settings";
static const std::string network_server_settings_http_port_node      = "http_port";
static const std::string network_server_settings_discovery_port_node      = "discovery_port";
static const std::string network_server_settings_forward_node           = "forward_ports";
static const std::string network_server_settings_use_auth_node          = "use_authentication";
static const std::string network_server_settings_stream_security_node   = "stream_security";
static const std::string network_server_settings_user_node              = "user";
static const std::string network_server_settings_password_node          = "password";

static const std::string certificate_component      = "ssl";
static const std::string resources_dir      = "resources";
static const std::string web_component      = "web";

network_server_settings_t::network_server_settings_t() :
    settings::installation_settings_t()
{
}

network_server_settings_t::~network_server_settings_t()
{
}

void network_server_settings_t::init(dvblink::messaging::message_queue_t& message_queue)
{
    settings::installation_settings_t::init(message_queue);

    config_file_ = get_config_path() / network_server_settings_filename;
    certificate_dir_ = get_shared_component_path(certificate_component);

    //use local installation server id as a password encryption key
    encryption_key_ = permanent_settings::get_server_id().to_string();
}

dvblink::filesystem_path_t network_server_settings_t::get_timeshift_dir()
{
    return get_temp_path() / timeshift_dir_name;
}

dvblink::filesystem_path_t network_server_settings_t::get_web_dir()
{
    return get_private_component_path(web_component);
}

void network_server_settings_t::reset()
{
    http_port_ = default_http_port;
    discovery_port_ = default_discovery_port;
    timeshift_buffer_max_size_ = default_timeshift_buffer_max_size;

    port_forwarding_enabled_ = false;
    authentication_enabled_ = false;
    stream_security_enabled_ = true;
    auth_user_ = default_auth_user;
    auth_password_ = default_auth_password;
}

dvblink::filesystem_path_t network_server_settings_t::get_local_certificate_path()
{
    dvblink::filesystem_path_t cert_file;

    if (boost::filesystem::exists(certificate_dir_.to_boost_filesystem()))
    {
        std::vector<boost::filesystem::path> pem_files;
        filesystem::find_files(certificate_dir_.to_boost_filesystem(), pem_files, L".pem");

        //take first pem file
        if (pem_files.size() > 0)
            cert_file = pem_files[0];
    }

    return cert_file;
}

dvblink::filesystem_path_t network_server_settings_t::get_resources_path()
{
    return get_private_common_path() / resources_dir;
}

void network_server_settings_t::load()
{
    reset();

    std::string str;

    pugi::xml_document doc;
    if (doc.load_file(config_file_.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            if (pugixml_helpers::get_node_value(root_node, network_server_settings_http_port_node, str))
                string_conv::apply(str.c_str(), http_port_, default_http_port);

            if (pugixml_helpers::get_node_value(root_node, network_server_settings_discovery_port_node, str))
                string_conv::apply(str.c_str(), discovery_port_, default_discovery_port);

            if (pugixml_helpers::get_node_value(root_node, network_server_settings_forward_node, str) &&
                    boost::iequals(str, pugixml_helpers::xmlnode_value_true))
                port_forwarding_enabled_ = true;

            if (pugixml_helpers::get_node_value(root_node, network_server_settings_use_auth_node, str) &&
                    boost::iequals(str, pugixml_helpers::xmlnode_value_true))
                authentication_enabled_ = true;

            if (pugixml_helpers::get_node_value(root_node, network_server_settings_stream_security_node, str))
                stream_security_enabled_ = boost::iequals(str, pugixml_helpers::xmlnode_value_true);

            if (pugixml_helpers::get_node_value(root_node, network_server_settings_user_node, str))
                auth_user_ = str;

            if (pugixml_helpers::get_node_value(root_node, network_server_settings_password_node, str))
                auth_password_ = decode_xml(str, encryption_key_.c_str());
        }
    }
}

void network_server_settings_t::save()
{
    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(network_server_settings_root.c_str());
    if (root_node != NULL)
    {
        std::stringstream buf;
        buf << http_port_;
        pugixml_helpers::new_child(root_node, network_server_settings_http_port_node, buf.str());

        buf.clear(); buf.str("");
        buf << discovery_port_;
        pugixml_helpers::new_child(root_node, network_server_settings_discovery_port_node, buf.str());

        pugixml_helpers::new_child(root_node, network_server_settings_forward_node, port_forwarding_enabled_? pugixml_helpers::xmlnode_value_true : pugixml_helpers::xmlnode_value_false);
        pugixml_helpers::new_child(root_node, network_server_settings_use_auth_node, authentication_enabled_? pugixml_helpers::xmlnode_value_true : pugixml_helpers::xmlnode_value_false);
        pugixml_helpers::new_child(root_node, network_server_settings_stream_security_node, stream_security_enabled_ ? pugixml_helpers::xmlnode_value_true : pugixml_helpers::xmlnode_value_false);

        pugixml_helpers::new_child(root_node, network_server_settings_user_node, auth_user_);
        pugixml_helpers::new_child(root_node, network_server_settings_password_node, encode_xml(auth_password_, encryption_key_.c_str()));

        pugixml_helpers::xmldoc_dump_to_file(doc, config_file_.to_string());
    }
}

}