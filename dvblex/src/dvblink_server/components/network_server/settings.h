/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <dl_installation_settings.h>

namespace dvblex { 

class network_server_settings_t : public settings::installation_settings_t
{
public:
    network_server_settings_t();
    ~network_server_settings_t();

    virtual void init(dvblink::messaging::message_queue_t& message_queue);

    void load();
    void save();

    boost::uint32_t get_http_port(){return http_port_;}
    boost::uint32_t get_https_port(){return http_port_ + 100;}

    void set_http_port(boost::uint32_t http_port){http_port_ = http_port;}

    boost::uint32_t get_discovery_port(){return discovery_port_;}
    void set_discovery_port(boost::uint32_t discovery_port){discovery_port_ = discovery_port;}

    boost::uint64_t get_timeshift_buffer_max_size() {return timeshift_buffer_max_size_;}
    void set_timeshift_buffer_max_size(boost::uint64_t timeshift_buffer_max_size) {timeshift_buffer_max_size_ = timeshift_buffer_max_size;}

    dvblink::filesystem_path_t get_timeshift_dir();

    dvblink::filesystem_path_t get_local_certificate_path();
    dvblink::filesystem_path_t get_resources_path();

    bool is_port_forwarding_enabled(){return port_forwarding_enabled_;}
    void set_port_forwarding_enabled(bool enabled){port_forwarding_enabled_ = enabled;}

    bool is_authentication_enabled(){return authentication_enabled_;}
    void set_authentication_enabled(bool enabled){authentication_enabled_ = enabled;}

    std::string get_auth_user(){return auth_user_;}
    void set_auth_user(const std::string& user){auth_user_ = user;}

    std::string get_auth_password(){return auth_password_;}
    void set_auth_password(const std::string& password){auth_password_ = password;}

    dvblink::filesystem_path_t get_web_dir();

    bool is_stream_security_enabled(){return stream_security_enabled_;}
    void set_stream_security_enabled(bool enabled){stream_security_enabled_ = enabled;}

protected:
    dvblink::filesystem_path_t config_file_;
    boost::uint32_t http_port_;
    boost::uint32_t discovery_port_;
    boost::uint64_t timeshift_buffer_max_size_;
    bool port_forwarding_enabled_;
    bool authentication_enabled_;
    std::string auth_user_;
    std::string auth_password_;
    dvblink::filesystem_path_t certificate_dir_;
    bool stream_security_enabled_;
    std::string encryption_key_;

    void reset();
};

}
