/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_types.h>
#include <dl_ts_audio_selector.h>
#include <dl_installation_settings.h>
#include <tc_ffmpeg_wrapper.h>
#include <tc_types.h>
#include <dl_transcoder.h>
#include "stream_processor_base.h"

namespace dvblex {

class live_transcoder : public stream_processor_base
{
public:
    live_transcoder(const dvblink::transcoder::ffmpeg_launch_params_t& ffmpeg_launch_params, const dvblink::transcoder::transcoder_params& tp);
    ~live_transcoder();

    void send_stream(const boost::uint8_t* buffer, boost::uint32_t length);

    boost::uint32_t get_bitrate() {return bitrate_;}

    virtual bool is_transport_stream() {return is_ts_;}
    virtual const char* get_mime() {return mime_.c_str();}

private:
    typedef dvblink::transcoder::ffmpeg_wrapper<live_transcoder> ffmpeg_wrapper_t;
    ffmpeg_wrapper_t* ffmpeg_;
    boost::uint32_t bitrate_;
	dvblink::engine::CTSAudioSelector audio_selector_;
    std::string mime_;
    bool is_ts_;

	static void audio_selector_cb(const unsigned char* buf, int len, void* param);
    virtual void write_stream(const unsigned char* /*data*/, size_t /*len*/);
    void ffmpeg_callback(const boost::uint8_t*, boost::uint32_t);
};

} // dvblex
