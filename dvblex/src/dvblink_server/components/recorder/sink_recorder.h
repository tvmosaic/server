/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/unordered_map.hpp>
#include <dli_server.h>
#include <dl_message_recorder.h>
#include <dl_message_queue.h>
#include <dl_message_common.h>
#include <dl_message_channels.h>
#include <dl_tuple_serialization.h>
#include "recorder_engine.h"

namespace dvblex { namespace recorder {

class sink_recorder
{
    class message_handler :
        public dvblink::messaging::start_request::subscriber,
        public dvblink::messaging::standby_request::subscriber,
        public dvblink::messaging::resume_request::subscriber,
        public dvblink::messaging::shutdown_request::subscriber,
        public dvblink::messaging::recorder::get_recorder_info_request::subscriber,
        public dvblink::messaging::recorder::force_update_request::subscriber,
        public dvblink::messaging::recorder::set_recording_options_request::subscriber,
        public dvblink::messaging::recorder::get_recording_options_request::subscriber,
        public dvblink::messaging::recorder::search_epg_request::subscriber,
        public dvblink::messaging::recorder::add_schedule_request::subscriber,
        public dvblink::messaging::recorder::get_schedule_request::subscriber,
        public dvblink::messaging::recorder::update_schedule_request::subscriber,
        public dvblink::messaging::recorder::remove_schedule_request::subscriber,
        public dvblink::messaging::recorder::get_recordings_request::subscriber,
        public dvblink::messaging::recorder::remove_recording_request::subscriber,
        public dvblink::messaging::recorder::get_completed_recordings_request::subscriber,
        public dvblink::messaging::recorder::update_recorder_epg_request::subscriber,
        public dvblink::messaging::recorder::remove_completed_recording_request::subscriber,
        public dvblink::messaging::recorder::get_schedules_in_csv_request::subscriber,
        public dvblink::messaging::recorder::repair_database_request::subscriber,
        public dvblink::messaging::recorder::get_xmltv_epg_request::subscriber,
        public dvblink::messaging::channels::channel_config_changed_request::subscriber,
        public dvblink::messaging::xml_message_request::subscriber
    {
    public:
        message_handler(const dvblink::i_server_t& server, dvblink::messaging::message_queue_t message_queue);
        ~message_handler();

    protected:
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::standby_request& request, dvblink::messaging::standby_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::resume_request& request, dvblink::messaging::resume_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response);

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::get_recorder_info_request& request, dvblink::messaging::recorder::get_recorder_info_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::force_update_request& request);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::set_recording_options_request& request, dvblink::messaging::recorder::set_recording_options_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::get_recording_options_request& request, dvblink::messaging::recorder::get_recording_options_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::search_epg_request& request, dvblink::messaging::recorder::search_epg_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::add_schedule_request& request, dvblink::messaging::recorder::add_schedule_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::get_schedule_request& request, dvblink::messaging::recorder::get_schedule_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::update_schedule_request& request, dvblink::messaging::recorder::update_schedule_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::remove_schedule_request& request, dvblink::messaging::recorder::remove_schedule_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::get_recordings_request& request, dvblink::messaging::recorder::get_recordings_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::remove_recording_request& request, dvblink::messaging::recorder::remove_recording_response& response);

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::get_completed_recordings_request& request, dvblink::messaging::recorder::get_completed_recordings_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::update_recorder_epg_request& request, dvblink::messaging::recorder::update_recorder_epg_response& response);

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::remove_completed_recording_request& request, dvblink::messaging::recorder::remove_completed_recording_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::get_schedules_in_csv_request& request, dvblink::messaging::recorder::get_schedules_in_csv_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::repair_database_request& request, dvblink::messaging::recorder::repair_database_response& response);

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::recorder::get_xmltv_epg_request& request, dvblink::messaging::recorder::get_xmltv_epg_response& response);

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::channel_config_changed_request& request);

		void fill_cmds();
        void process(const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response);
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response);

        bool find_programs(const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response);
        void channel_config_changed_handler();

        template <typename INPARAM>
        void decode(const dvblink::messaging::xml_message_request& indata, INPARAM& inparam)
        {
            std::istringstream archive_stream(indata.xml_.get());
            boost::archive::text_iarchive archive(archive_stream);
            archive >> inparam;
        }

        template <typename OUTPARAM>
        void encode(const OUTPARAM& outparam, dvblink::messaging::xml_message_response& outdata)
        {
            std::ostringstream archive_stream;
            boost::archive::text_oarchive archive(archive_stream);
            archive << outparam;
            outdata.xml_ = archive_stream.str();
        }

    private:
        dvblink::i_server_t server_;
        recorder_engine recorder_engine_;
        dvblink::messaging::message_queue_t message_queue_;

        typedef bool (sink_recorder::message_handler::*cmd_handler_func_t)(const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response);
        typedef boost::unordered_map<std::string, cmd_handler_func_t> cmd_handler_t;
        typedef cmd_handler_t::const_iterator citer_cmd_handler_t;
        
        static cmd_handler_t cmd_handler_;
        static std::string recommender_cmd_[];
        static cmd_handler_func_t cmd_handler_func_[];

        struct rec_cmd_pair
        {
            std::string cmd_;
            sink_recorder::message_handler::cmd_handler_func_t handler_;
        };
        static rec_cmd_pair cmd_handler_pairs_[];
    };

public:
    sink_recorder(const dvblink::i_server_t& server, const dvblink::base_id_t& id);
    ~sink_recorder();

protected:
    dvblink::i_server_t server_;
    std::auto_ptr<message_handler> message_handler_;
    dvblink::base_id_t id_;
    dvblink::messaging::message_queue_t message_queue_;

};

} //recorder
} //dvblex
