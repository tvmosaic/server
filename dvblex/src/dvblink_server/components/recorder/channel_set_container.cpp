/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include "channel_set_container.h"

using namespace dvblink;

namespace dvblex { namespace recorder {

void transponder_data::add(const set_of_set_of_channels_set_t& transponder_list)
{
    size_t channel_set_cnt = 0;
    for (size_t i = 0; i < transponder_list.size(); ++i)
    {
        const set_of_channel_sets_t& transponder = transponder_list[i];
        for (size_t j = 0; j < transponder.size(); ++j)
        {
            const channel_set_t& ch_set = transponder[j];
            const std::vector<channel_id_t>& vec_ch_phy = ch_set.channels_;

            std::stringstream buf;
            buf << i;
            map_channel_set_[channel_set_cnt] = channel_set(transponder_id_t(buf.str()), channel_set_cnt, ch_set.max_concurrency_num_);
            for (size_t ch = 0; ch < vec_ch_phy.size(); ++ch)
            {
                map_ts_ch_phy_id_[vec_ch_phy[ch]] = channel_set_cnt;
            }
            ++channel_set_cnt;
        }
    }
}

bool transponder_data::get_phys_channel_data(const channel_id_t& ph_ch, transponder_id_t& transponder_id, channel_set_id_t& channel_set_id, concurrency_num_t& max_concurency_number) const
{
    bool res = false;

    map_chphy_id_TO_channel_sets_t::const_iterator it_ph_ch = map_ts_ch_phy_id_.find(ph_ch);
    if (it_ph_ch != map_ts_ch_phy_id_.end())
    {
        const channel_set_id_t& ch_set_id = it_ph_ch->second;
        map_channel_set_id_TO_channel_set_t::const_iterator it_ch_set = map_channel_set_.find(ch_set_id);
        if (it_ch_set != map_channel_set_.end())
        {
            const channel_set& ch_set = it_ch_set->second;
            transponder_id = ch_set.transponder_id();
            channel_set_id = ch_set.channel_set_id();
            max_concurency_number = ch_set.max_concurrency_num();
            res = true;
        }
    }

    return res;
}

///////////////////////////////////////////////////////////////////////////////

void channel_set_container::init(const ts_source_TO_set_of_set_of_channels_set_t& channel_set_data)
{
    set_collection_.clear();
    map_log_channel_data_.clear();

    // fill set_collection_
    //
    ts_source_TO_set_of_set_of_channels_set_t::const_iterator it_set = channel_set_data.begin();
    while (it_set != channel_set_data.end())
    {
        const device_id_t& source_id = it_set->first;
        const set_of_set_of_channels_set_t& transponder_list = it_set->second;

        set_collection_[source_id] = transponder_data();
        transponder_data& tr_data = set_collection_[source_id];

        tr_data.add(transponder_list);

        //fill map_log_channel_data_
        for (size_t i=0; i<transponder_list.size(); i++)
        {
            const set_of_channel_sets_t& set_of_channels = transponder_list[i];
            for (size_t j=0; j<set_of_channels.size(); j++)
            {
                const std::vector<dvblink::channel_id_t>& channels = set_of_channels[j].channels_;
                for (size_t k=0; k<channels.size(); k++)
                {
                    if (map_log_channel_data_.find(channels[k]) == map_log_channel_data_.end())
                        map_log_channel_data_[ channels[k] ] = map_source_TO_phys_channel_id_t();

                    map_source_TO_phys_channel_id_t& map = map_log_channel_data_[ channels[k] ];
                    map[source_id] = channels[k];
                }
            }
        }

        ++it_set;
    }
}

void channel_set_container::get_ts_sources(vec_ts_sources_t& sources) const
{
    map_ts_source_control_TO_transponders_t::const_iterator it_set = set_collection_.begin();
    while (it_set != set_collection_.end())
    {
        sources.push_back(it_set->first);
        ++it_set;
    }
}

bool channel_set_container::get_phys_channel_id(const device_id_t& ts_control_id, const channel_id_t& log_ch_id, channel_id_t& phy_ch_id) const
{
    bool res = false;
    map_log_ch_TO_map_source_phys_channel_id_t::const_iterator it_log_ch = map_log_channel_data_.find(log_ch_id);
    if (it_log_ch != map_log_channel_data_.end())
    {
        const map_source_TO_phys_channel_id_t& map_source_TO_phys_channel_id = it_log_ch->second;
        map_source_TO_phys_channel_id_t::const_iterator it_map = map_source_TO_phys_channel_id.find(ts_control_id);
        if (it_map != map_source_TO_phys_channel_id.end())
        {
            phy_ch_id = it_map->second;
            res = true;
        }
    }
    return res;
}

bool channel_set_container::get_phys_channel_data(const device_id_t& source_id,
    const channel_id_t& ph_ch, transponder_id_t& transponder_id, channel_set_id_t& channel_set_id, concurrency_num_t& max_concurency_number) const
{
    bool res = false;
    map_ts_source_control_TO_transponders_t::const_iterator it_set = set_collection_.find(source_id);
    if (it_set != set_collection_.end())
    {
        const transponder_data& tdata = it_set->second;
        res = tdata.get_phys_channel_data(ph_ch, transponder_id, channel_set_id, max_concurency_number);
    }
    return res;
}

bool channel_set_container::is_concurrent_channel(const device_id_t& ts_control_id,
    const channel_id_t& phy_id1, const channel_id_t& phy_id2, channel_set_id_t& channel_set_id, concurrency_num_t& max_concurency_number)
{
    bool res = false;
    map_ts_source_control_TO_transponders_t::const_iterator it_set = set_collection_.find(ts_control_id);
    if (it_set != set_collection_.end())
    {
        const transponder_data& tdata = it_set->second;

        channel_set_id_t set_id2;
        transponder_id_t transponder_id1, transponder_id2;
        concurrency_num_t max_concurency_number_2;
        if (tdata.get_phys_channel_data(phy_id1, transponder_id1, channel_set_id, max_concurency_number) && tdata.get_phys_channel_data(phy_id2, transponder_id2, set_id2, max_concurency_number_2))
        {
            res = (transponder_id1 == transponder_id2 && channel_set_id == set_id2); // the same channel set (on the same transponder)
        }
    }
    return res;
}

size_t channel_set_container::get_channel_quantity(const device_id_t& source_id) const
{
    size_t num = 0;
    map_ts_source_control_TO_transponders_t::const_iterator it_set = set_collection_.find(source_id);
    if (it_set != set_collection_.end())
    {
        const transponder_data& tr_data = it_set->second;
        num = tr_data.get_channel_number();
    }
    return num;
}

} //recorder
} //dvblex
