/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_message_addresses.h>
#include <components/recorder.h>
#include "sink_recorder.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::messaging;
using namespace dvblex::recorder;

namespace dvblex {

recorder_t::recorder_t() :
    id_(recorder_message_queue_addressee.get()),
    recorder_(NULL)
{
}

recorder_t::~recorder_t()
{
    if (recorder_ !=NULL)
        delete recorder_;
}

i_result recorder_t::query_interface(const base_id_t& requestor_id, const i_guid& iid, i_base_object_t& obj)
{
    i_result res = i_error;

    //no other interfaces

    return res;
}

bool recorder_t::init(const dvblink::i_server_t& server)
{
    recorder_ = new sink_recorder(server, id_);

    return true;
}


} // dvblex
