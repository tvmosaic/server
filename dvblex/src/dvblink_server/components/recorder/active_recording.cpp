/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_file_procedures.h>
#include <dl_logger.h>
#include <dl_uuid.h>
#include <http_utils.h>
#include "recorder_settings.h"

#include <dli_stream_source.h>
#include <dl_http_comm.curl.h>
#include <dl_network_helper.h>
#include <dl_message_server.h>
#include <dl_message_stream.h>
#include <dl_message_addresses.h>
#include <dl_filename_pattern.h>
#include <dl_installation_settings.h>
#include <dl_file_procedures.h>
#include <dl_external_control.h>
#include <dl_transcoder.h>
#include "active_recording.h"

using namespace dvblex::settings;
using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::messaging;
using namespace dvblink::logging;
using namespace dvblink::transcoder;

namespace dvblex { namespace recorder {

active_recording::active_recording(recorder_settings& rs, const recording_timer& rec_timer,
        const logical_channel_desc& channel, dvblink::i_server_t server, dvblink::messaging::message_queue_t message_queue) :
    server_(server),
    message_queue_(message_queue),
    recording_timer_(rec_timer),
    state_(rcsWaiting),
    channel_(channel),
    actual_start_(-1),
    actual_stop_(-1),
    thumb_thread_(NULL),
    thumb_invalidated_(false),
    thumb_tolerance_period_(THUMB_TOLERANCE_PERIOD),
    thumb_vert_size_(THUMB_VERT_SIZE),
    thumb_hor_size_(THUMB_HOR_SIZE),
    recorder_settings_(rs)
{
    //generate a filename
    filesystem_path_t rec_path = recorder_settings_.get_record_path();

	std::wstring rname;
	filename_pattern_string_to_filename(recorder_settings_.get_recording_filename_pattern(), 
		rec_timer.event_info(), channel.name_.get(), channel.number_.get(), channel.sub_number_.get(), true, rname);

    //check if file exists already
    filesystem_path_t file_tmp = rec_path / (rname + L".ts");
    bool b_exists = false;
    try {
        b_exists = boost::filesystem::exists(file_tmp.to_boost_filesystem());
    } catch(...) {b_exists = true;}
    if (b_exists)
    {
        //add timer id to the file name to be unique
	    rname += L"_" + string_cast<EC_UTF8>(rec_timer.timer_id().get());
    }

    rname += L".ts";
    filename_ = rec_path / rname;

    //update recording state to the actual value
    update_state();
}

void active_recording::cancel_recording()
{
    if (state_ == rcsPreRecording || state_ == rcsRecording || state_ == rcsPostRecording)
    {
        stop_recording();
        state_ = rcsCompleted;
    }
}

void active_recording::update_state()
{
    //check if the thumbnail generation thread has finished already
    if (thumb_thread_ && thumb_thread_->timed_join(boost::posix_time::milliseconds(10)))
    {
        thumb_thread_exit_ = true;
        thumb_thread_->join();
        delete thumb_thread_;
        thumb_thread_ = NULL;
    }

    if (state_ == rcsError || state_ == rcsCompleted || state_ == rcsInterrupted)
    {
        //there is nothing we can do here
        //just exit
        return;
    }

    //check if recording was interrupted (by another recording with higher priority)
    if (file_writer_ != NULL && file_writer_->is_deleted())
    {
        stop_recording();
        state_ = rcsInterrupted;
        return;
    }

    time_t now; time(&now);
    if (now >= recording_timer_.real_start_time() + recording_timer_.real_duration())
    {
        //we are outside the recording schedule. stop recording if it is ongoing
        switch (state_)
        {
        case rcsWaiting:
            state_ = rcsError;
            break;
        case rcsPreRecording:
        case rcsRecording:
        case rcsPostRecording:
            stop_recording();
            state_ = rcsCompleted;
            break;
        default:
            break;            
        }
    }
    else
    if (now >= recording_timer_.main_start_time() + recording_timer_.main_duration())
    {
        //we are in post recording margin part
        switch (state_)
        {
        case rcsWaiting:
            if (start_recording())
                state_ = rcsPostRecording;
            else
                state_ = rcsError;
            break;
        case rcsPreRecording:
        case rcsRecording:
            state_ = rcsPostRecording;
            break;
        default:
            break;            
        }
    }
    else
    if (now >= recording_timer_.main_start_time())
    {
        //we are in the main recording part
        switch (state_)
        {
        case rcsWaiting:
            if (start_recording())
                state_ = rcsRecording;
            else
                state_ = rcsError;
            break;
        case rcsPreRecording:
        case rcsPostRecording:  //do not know how this can happen ???
            state_ = rcsRecording;
            break;
        default:
            break;
        }          
    }
    else
    if (now >= recording_timer_.real_start_time())
    {
        //we are in the prerecording margin part
        switch (state_)
        {
        case rcsWaiting:
            if (start_recording())
                state_ = rcsPreRecording;
            else
                state_ = rcsError;
            break;
        case rcsRecording:      //do not know how this can happen ???
        case rcsPostRecording:  //do not know how this can happen ???
            state_ = rcsPreRecording;
            break;
        default:
            break;
        }
    }
    else
    if (now < recording_timer_.real_start_time())
    {
        //we are waiting for the recording to happen
    }
}

void active_recording::stop_recording()
{
    if (!streamer_id_.get().empty())
    {
        stream::stop_stream_request req(streamer_id_);
        stream::stop_stream_response resp;
        message_queue_->send(source_manager_message_queue_addressee, req, resp);
    }

    //delete local writer object
    file_writer_.reset();

	time(&actual_stop_);

    //if thumbnail generation thread is running - stop it now
    if (thumb_thread_)
    {
        thumb_thread_exit_ = true;
        thumb_thread_->join();
        delete thumb_thread_;
        thumb_thread_ = NULL;
    }
}

bool active_recording::start_recording()
{
    bool ret = false;

    i_stream_source_control_t stream_interface;
    if (server_->query_object_interface(message_queue_->get_uid(), base_id_t(source_manager_message_queue_addressee.get()), stream_source_control_interface, stream_interface) == dvblink::i_success)
    {
        //generate a unique guid for this streamer
        streamer_id_ = boost::uuids::to_string(engine::uuid::gen_uuid());

        file_writer_ = share_streamer_safely(new file_writer_t(streamer_id_.to_string(), filename_, recording_timer_.get_priority()));
        streamer_object_t obj = file_writer_;
        dvblink::EStatus status = stream_interface->start_channel(recording_timer_.channel().to_string().c_str(), obj);
        ret = (status == DvbLink_StatusOk);
        //server
	    time(&actual_start_);
        if (ret)
        {
            //start thumbnail generation thread
            thumb_thread_exit_ = false;
            thumb_thread_ = new boost::thread(boost::bind(&active_recording::thumb_thread_func, this));
        } else
        {
            log_error(L"active_recording::start_recording. Start channel %1% failed") % recording_timer_.channel().to_wstring();
        }

        stream_interface.reset();

    } else
    {
        log_error(L"active_recording::start_recording. Failed to get stream control interface");
    }
    return ret;
}

bool active_recording::get_thumb_path(std::wstring& filename) const
{
    bool res = (thumb_thread_ == NULL && thumb_filename_.get().size() && !thumb_invalidated_);
    if (res)
    {
        filename = thumb_filename_.get();
    }
    return res;
}

void active_recording::thumb_thread_func()
{
    //check whether thumbnail is available as a part of program description
    if (recording_timer_.event_info().m_ImageURL.size())
    {
        thumb_filename_ = recorder_settings_.get_thumbnail_path();
        thumb_filename_ /= recording_timer_.timer_id().get();

        streaming::download_http_image(recording_timer_.event_info().m_ImageURL, thumb_filename_, thumb_filename_);
    }
    else
    {
        //wait until recording of the actual content starts
#ifdef max
#undef max
#endif
        time_t actual_start = std::max(actual_start_, recording_timer_.main_start_time()) + thumb_tolerance_period_;
        time_t now; time(&now);
        while (!thumb_thread_exit_ && now < actual_start)
        {
            boost::this_thread::sleep(boost::posix_time::milliseconds(500));
            time(&now);
        }
        //run ffmpeg to generate a thumbnail - even if exit was signaled (it will not take a long time)
        size_t offset = now - actual_start_ > 15 ? now - actual_start_ - 10 : 0;
        generate_thumb_with_ffmpeg(offset);
    }
}

void active_recording::generate_thumb_with_ffmpeg(size_t sec_offset)
{
#ifndef _ANDROID_ALL

    std::wstring tid = string_cast<EC_UTF8>(recording_timer_.timer_id().get());

    //thumbnail filename
    thumb_filename_ = recorder_settings_.get_thumbnail_path();
    thumb_filename_ /= string_cast<EC_UTF8>(recording_timer_.timer_id().get());
    thumb_filename_ += L".jpg";

    bool success = false;

    server::ffmpeg_launch_params_request req(tu_generate_thumb_from_file_id);
    server::ffmpeg_launch_params_response resp;
    if (message_queue_->send(server_message_queue_addressee, req, resp) == messaging::success && resp.result_)
    {
        ffmpeg_param_template_init_t init_info;
        init_info.src_ = filename_.to_string();
        init_info.dest_ = thumb_filename_.to_string();
        init_info.width_ = thumb_hor_size_;
        init_info.height_ = thumb_vert_size_;
        init_info.offset_sec_ = sec_offset;

        std::vector<dvblink::launch_param_t> args;
        init_ffmpeg_launch_arguments(resp.launch_params_.arguments_, init_info, args);

        boost::int64_t pid = external_control::start_process(resp.launch_params_.ffmpeg_exepath_, 
            resp.launch_params_.ffmpeg_dir_.empty() ? NULL : &resp.launch_params_.ffmpeg_dir_,
            args, external_control::NormalPriority,
            resp.launch_params_.ffmpeg_env_.size() == 0 ? NULL : &resp.launch_params_.ffmpeg_env_);

        if (pid != -1)
        {
            //wait until process finishes
            while (external_control::is_process_running(pid))
            {
            	boost::this_thread::sleep(boost::posix_time::milliseconds(100));
            }

            success = true;
        } else
        {
            logging::log_error(L"active_recording: start_process returned error while generating thumb for (%1%)") % tid;
        }

    } else
    {
        log_warning(L"active_recording::generate_thumb_with_ffmpeg. Failed to get ffmpeg launch parameters");
    }
   
	if (success)
	{
        //check thumbnail on validity (e.g. file size > 0)
        boost::int64_t fsize = engine::filesystem::get_file_size(thumb_filename_.to_wstring());

        if (fsize == -1 || fsize == 0)
        {
            logging::log_error(L"active_recording: Generated thumbnail for (%1%) is invalid and deleted") % tid;
            //file is invalid
            try
            {
                boost::filesystem::remove(thumb_filename_.to_boost_filesystem());
            }
            catch(...)
            {
            }

            thumb_filename_ = L"";
        }
        else
        {
            logging::log_info(L"active_recording: Generated thumbnail for (%1%)") % tid;
        }
    }
    else
	{
        thumb_filename_ = L"";
	}
#endif
}

} //recorder
} //dvblex
