/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/filesystem.hpp>
#include <dl_logger.h>
#include <dl_message_power.h>
#include <dl_message_recorder.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_send_to_target_cmd.h>
#include <dl_xml_serialization.h>
#include "recorder_engine_impl.h"
#include "recorder_database.h"
#include "timeline_builder.h"
#include "recorder_settings.h"

using namespace dvblink; 
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::messaging;
using namespace dvblink::messaging::recorder;

namespace dvblex { namespace recorder {

static bool is_bit_set(int bit_num, unsigned char bit_field)
{
    unsigned char mask = (0x01 << bit_num);
    return (bit_field & mask) != 0;
}

bool recorder_engine_impl::is_enough_free_disk_space()
{
    //check for free disk space before starting a new timer recording
    execute_autodelete_procedure();

    //check disk space now
    boost::int64_t total_space;
    boost::int64_t available_space;
    recorder_settings_.get_rec_disk_space_stats_kb(total_space, available_space);
    
    return available_space > 0;
}

void recorder_engine_impl::execute_autodelete_procedure()
{
    if (recorder_settings_.is_autodelete_enabled())
    {
        log_ext_info(L"recorder_engine_impl::execute_autodelete_procedure()");
        //check disk space
        boost::int64_t total_space;
        boost::int64_t available_space;
        if (recorder_settings_.get_rec_disk_space_stats_kb(total_space, available_space))
        {
            if (available_space <= recorder_settings_.get_ds_threshold_kb())
            {
                log_info(L"recorder_engine_impl::execute_autodelete_procedure(). Starting auto delete algorithm. Required disk space: %1% KB. Actually available: %2% KB") % recorder_settings_.get_ds_threshold_kb() % available_space;
                //get list of recordings
                completed_recordings_list_t recordings;
                if (recorder_database_.get_completed_recordings(recordings))
                {
                    //recordings are sorted on start_time - the first - the oldest
                    //go through thenm and delete one by one until required anount of disk space is available
                    for (size_t i=0; i<recordings.size(); i++)
                    {
                        std::wstring wstr = string_cast<EC_UTF8>(recordings[i].timer_id().get());
                        log_info(L"recorder_engine_impl::execute_autodelete_procedure(). Deleting recording %1% (start time %2%)") % wstr % recordings[i].event_info().m_StartTime;
                        
                        delete_completed_recording(recordings[i].timer_id());
                        //check disk space
                        recorder_settings_.get_rec_disk_space_stats_kb(total_space, available_space);
                        log_info(L"recorder_engine_impl::execute_autodelete_procedure(). Available disk space: %1% KB") % available_space;
                        
                        if (available_space > recorder_settings_.get_ds_threshold_kb())
                            break;
                    }
                }
            }
        }
        
    }
}

recorder_engine_impl::recorder_engine_impl(recorder_settings& rs, recorder_database& rd, dvblink::i_server_t server, dvblink::messaging::message_queue_t message_queue) :
    recorder_settings_(rs),
    recorder_database_(rd),
    message_queue_(message_queue),
    server_(server),
    completed_rec_man_(rd),
    timeline_builder_(&completed_rec_man_, rs, channel_set_container_),
    cancelled_rec_man_(rd),
    active_rec_man_(this, rd, channel_set_container_),
    next_wakeup_timer_(invalid_time_value)
{
}

void recorder_engine_impl::reset(const source_to_logical_map_t& logical_channel_map, const ts_source_TO_set_of_set_of_channels_set_t& channel_set_data)
{
    log_channel_map_ = logical_channel_map;
    channel_set_container_.init(channel_set_data);
    
    timeline_builder_.reset();
    cancelled_rec_man_.reset();
    completed_rec_man_.reset();

    //put active recordings on the timelines of their tuners
    timeline_builder_.add(active_rec_man_.get_recordings());

    //read next wakeup timer
    next_wakeup_timer_ = invalid_time_value;
    time_t wakeup_time;
    if (get_wakeup_time(wakeup_time))
    {
        next_wakeup_timer_ = wakeup_time;
        log_info(L"recorder_engine_impl::reset. Current wakeup timer is set for %1%") % next_wakeup_timer_;
    }
    else
    {
        log_info(L"recorder_engine_impl::reset. There is no wakeup timer currently set");
    }
}

void recorder_engine_impl::generate_timers_for_schedule(const schedule_item& sch_item, recording_timers_list_t& timer_list)
{
    timer_list.clear();
    //timers are generated from "now" until "now + DL_RECORDER_RECORDING_WINDOWS_SEC"
    if (sch_item.is_manual())
    {
        time_t now; time(&now);
        //original recording
        if (timeline_builder_.is_program_in_wnd(sch_item.main_start_time(), sch_item.main_duration(), now, now + DL_RECORDER_RECORDING_WINDOWS_SEC))
        {
            recording_timer timer(sch_item, sch_item.main_start_time());
            if (!cancelled_rec_man_.is_timer_cancelled(timer))
            {
                timer_list.push_back(timer);                
            }
        }
        //recurring recordings
        if (sch_item.day_mask() != 0)
        {
            time_t timer_start_time = sch_item.main_start_time() + DL_RECORDER_24_HOURS_SEC;
            //find first repeating timer from original date and right before "now"
            if (timer_start_time < now)
            {
                timer_start_time = timer_start_time + DL_RECORDER_24_HOURS_SEC * ((now - timer_start_time) / DL_RECORDER_24_HOURS_SEC);
            }
            //start with this time and increment it every 24 hours to see if the timer should be recorded
            while (timer_start_time < now + DL_RECORDER_RECORDING_WINDOWS_SEC)
            {
                if (check_start_time_on_weekday(sch_item.day_mask(), timer_start_time))
                {
                    recording_timer timer(sch_item, timer_start_time);
                    if (!cancelled_rec_man_.is_timer_cancelled(timer))
                    {
                        timer_list.push_back(timer);
                    }
                }
                timer_start_time += DL_RECORDER_24_HOURS_SEC;
            }
        }
    }
    else
    if (sch_item.is_epg_based())
    {
        generate_timers_for_epg_schedule(sch_item, timer_list);
    }
    else
    if (sch_item.is_pattern_based())
    {
        generate_timers_for_pattern_schedule(sch_item, timer_list);
    }
    
    log_info(L"recorder_engine_impl::generate_timers_for_schedule. %1% timers were generated for schedule %2%") % timer_list.size() % sch_item.schedule_item_id();
}

void recorder_engine_impl::get_keyphrase_from_name(const std::string& name, std::string& keyphrase)
{
    keyphrase.clear();
    //substitute everything inside the brackets with wild card
    //add wildcard at the start and end
    bool b_inside_bracket = false;
    keyphrase = '%';
    for (size_t i = 0; i < name.size(); i++)
    {
        if (!b_inside_bracket)
        {
            keyphrase += name[i];
            if (name[i] == '(')
            {
                keyphrase += '%';
                b_inside_bracket = true;
            }
        } else
        {
            if (name[i] == ')')
            {
                keyphrase += name[i];
                b_inside_bracket = false;
            }
        }
    }
    keyphrase += '%';
}

bool recorder_engine_impl::check_start_time_in_before_after_margins(const schedule_item& sch_item, time_t start_time)
{
    bool ret_val = false;

    //convert start_time to local time
    struct tm* start_time_local = localtime(&start_time);
    int start_sec = start_time_local->tm_sec + start_time_local->tm_min*60 + start_time_local->tm_hour*3600;

    //determine start and end interval ends that fall into 00:00:00 - 23:59:59 of this day
    int max_seconds_per_day = 23*3600 + 59*60 + 59;
    int start_interval = 0;
    int end_interval = max_seconds_per_day;

    if (sch_item.start_after_sec_ != -1)
        start_interval = sch_item.start_after_sec_;

    if (sch_item.start_before_sec_ != -1)
        end_interval = sch_item.start_before_sec_;

    //check if interval crosses midnight
    if (start_interval <= end_interval)
        ret_val = start_interval <= start_sec && start_sec <= end_interval;
    else
        ret_val = (end_interval <= start_sec && start_sec <= max_seconds_per_day) || (0 <= start_sec && start_sec <= start_interval);

    return ret_val;
}

bool recorder_engine_impl::check_start_time_on_weekday(boost::uint8_t day_mask, time_t start_time)
{
    bool ret_val = false;

    if (day_mask == 0)
    {
        //any day
        ret_val = true;
    } else
    {
        tm* local_start_time = localtime(&start_time);
        ret_val = is_bit_set(local_start_time->tm_wday, day_mask);
    }

    return ret_val;
}

bool recorder_engine_impl::check_recurring_timer_for_boundaries(const schedule_item& sch_item, time_t start_time)
{
    bool ret_val = true;

    //only recursive epg/search schedules are considered
    if ((sch_item.is_epg_based() && sch_item.record_series()) || sch_item.is_pattern_based())
    {
        ret_val = check_start_time_in_before_after_margins(sch_item, start_time) &&
            check_start_time_on_weekday(sch_item.day_mask(), start_time);
    }

    return ret_val;
}

bool recorder_engine_impl::check_timer_for_duplicate(const schedule_item& sch_item, const recording_timer& timer, recorder_duplicate_checker& duplicate_checker)
{
    bool ret_val = sch_item.record_series() && sch_item.record_series_new_only() && duplicate_checker.was_recorded_before(timer);
    if (ret_val)
    {
        std::wstring wname = string_cast<EC_UTF8>(timer.event_info().m_Name);
        std::wstring wepisode = string_cast<EC_UTF8>(timer.event_info().m_SecondName);
        log_ext_info(L"recorder_engine_impl::check_timer_for_duplicate. Item %1%, %2% (%3%, %4%) was already recorded before") % wname % wepisode % timer.event_info().m_SeasonNum % timer.event_info().m_EpisodeNum;
    }
    return !ret_val;
}

void recorder_engine_impl::generate_timers_for_epg_schedule(const schedule_item& sch_item, recording_timers_list_t& timer_list)
{
    //initialize duplicate checker
    recorder_duplicate_checker duplicate_checker(recorder_database_, recorder_settings_);
    if (sch_item.record_series())
    {
        duplicate_checker.init(sch_item);
    }

    time_t now; time(&now);

    //get original epg item from database
    epg_item eitem;
    bool bfound = recorder_database_.get_epg_item(sch_item.channel(), sch_item.epg_event_id(), eitem);
    if (!bfound)
    {
        log_info(L"recorder_engine_impl::generate_timers_for_epg_schedule. EPG event %1% not found in database. Using original event instead") % sch_item.epg_event_id().to_wstring();
        //epg id is not present in the recording database anymore
        //deserialize the original epg item
        DLEPGEventList epgEventList;
        if (EPGReadEventsFromXML(sch_item.epg_program_info(), epgEventList) && epgEventList.size() == 1)
        {
            eitem.event_ = epgEventList[0];
            eitem.make_id();
            bfound = true;
        }
    }

    if (bfound)
    {
        //check its start time and place on timeline
        if (timeline_builder_.is_program_in_wnd(eitem.event_.m_StartTime, eitem.event_.m_Duration, now, now + DL_RECORDER_RECORDING_WINDOWS_SEC))
        {
            //check whether this epg id was "re-used" in the future for another event
            DLEPGEventList epgEventList;
            if (EPGReadEventsFromXML(sch_item.epg_program_info(), epgEventList) && epgEventList.size() == 1)
            {
                if (timeline_builder_.is_program_in_wnd(eitem.event_.m_StartTime, eitem.event_.m_Duration, epgEventList[0].m_StartTime - DL_RECORDER_ID_REUSE_TOLERANCE, epgEventList[0].m_StartTime + DL_RECORDER_ID_REUSE_TOLERANCE))
                {
                    recording_timer timer(sch_item, sch_item.epg_event_id(), eitem.event_);
                    if (!cancelled_rec_man_.is_timer_cancelled(timer))
                    {
                        bool add = check_timer_for_duplicate(sch_item, timer, duplicate_checker) && check_recurring_timer_for_boundaries(sch_item, eitem.event_.m_StartTime);
                        if (add)
                            timer_list.push_back(timer);
                    }
                }
            }
        }
    } else
    {
        log_warning(L"recorder_engine_impl::generate_timers_for_epg_schedule. EPG event %1% not found") % sch_item.epg_event_id().to_wstring();
    }

    //check if series have to be recorded
    if (sch_item.record_series())
    {
        //deserialize event info from schedule item
        DLEPGEventList epgEventList;
        if (EPGReadEventsFromXML(sch_item.epg_program_info(), epgEventList) && epgEventList.size() == 1)
        {
            //query all events like this from database
            epg_channel_info_map_t epg_channel_events;
            std::string keyphrase;
            get_keyphrase_from_name(epgEventList[0].m_Name, keyphrase);
            //ignore repeat flag from the database for "only new programs" and use duplicate checker instead
            if (recorder_database_.get_epg_for_keyphrase(sch_item.channel(), keyphrase, false, epg_channel_events))
            {
                epg_channel_info_map_t::iterator ch_it = epg_channel_events.begin();
                while (ch_it != epg_channel_events.end())
                {
                    for (size_t i = 0; i < ch_it->second.size(); i++)
                    {
                    	using namespace std;

                        if (timeline_builder_.is_program_in_wnd(ch_it->second[i].event_.m_StartTime, ch_it->second[i].event_.m_Duration, 
                            max(epgEventList[0].m_StartTime, now), now + DL_RECORDER_RECORDING_WINDOWS_SEC))
                        {
                            if (check_start_time_in_before_after_margins(sch_item, ch_it->second[i].event_.m_StartTime))
                            {
                                //do not schedule original epg event again
                                if (!boost::iequals(ch_it->second[i].event_.id_, sch_item.epg_event_id().get()))
                                {
                                    recording_timer timer(sch_item, ch_it->second[i].event_.id_, ch_it->second[i].event_);
                                    if (!cancelled_rec_man_.is_timer_cancelled(timer))
                                    {
                                        bool add = check_timer_for_duplicate(sch_item, timer, duplicate_checker) && 
                                            check_recurring_timer_for_boundaries(sch_item, ch_it->second[i].event_.m_StartTime);

                                        if (add)
                                            timer_list.push_back(timer);
                                    }
                                }
                            }
                        }
                    }
                    ++ch_it;
                }
            }
        }
        else
        {
            log_error(L"recorder_engine_impl::generate_timers_for_epg_schedule. Failed to deserialize epg event info");
        }
    }
}

void recorder_engine_impl::generate_timers_for_pattern_schedule(const schedule_item& sch_item, recording_timers_list_t& timer_list)
{
    time_t now; time(&now);
    
    //query all events for pattern from database
    server_channel_list_t channel_list;
    if (!sch_item.channel().empty())
        channel_list.push_back(sch_item.channel());
    std::string keyphrase = sch_item.get_key_phrase().get();
    time_t time_from = now;
    time_t time_to = now + DL_RECORDER_RECORDING_WINDOWS_SEC;

    epg_channel_info_map_t epg_channel_events;
    if (recorder_database_.browse_epg(channel_list, time_from, time_to, keyphrase, sch_item.get_genre_mask(), 
        epg_full_info, DL_RECORDER_MAX_SCHED_EVENT_NUM, epg_channel_events))
    {
        epg_channel_info_map_t::iterator ch_it = epg_channel_events.begin();
        while (ch_it != epg_channel_events.end())
        {
            for (size_t i = 0; i < ch_it->second.size(); i++)
            {
                recording_timer timer(sch_item, ch_it->first, ch_it->second[i].event_.id_, ch_it->second[i].event_);
                if (!cancelled_rec_man_.is_timer_cancelled(timer) && check_recurring_timer_for_boundaries(sch_item, ch_it->second[i].event_.m_StartTime))
                {
                    timer_list.push_back(timer);
                }
            }
            ++ch_it;
        }
    }
}

bool recorder_engine_impl::add_timer_to_timeline(const schedule_item& sch_item, const recording_timer& timer, recording_timers_list_t* conflict_list)
{
	//check whether this timer is already on a timeline
	//this can happen if reschedule is done during active recording
	recording_timer existing_timer;
	if (timeline_builder_.get_timer(timer.timer_id(), existing_timer))
	{
		log_info(L"recorder_engine_impl::add_timer_to_timeline. Timer %1% is already on a timeline (active recording?)") % string_cast<EC_UTF8>(timer.timer_id().get());
		return true;
	}

    //check if this timer should not be put on a timeline as a duplicate
    if (sch_item.record_series() && sch_item.record_series_new_only() && timer.is_by_epg() && 
        recorder_duplicate_checker::is_already_on_timeline(recorder_settings_, timeline_builder_, timer))
    {
        log_ext_info(L"recorder_engine_impl::add_timer_to_timeline. Duplicate check. Item %1%, %2% (%3%, %4%) is already on timeline") %
            string_cast<EC_UTF8>(timer.event_info().m_Name) %
            string_cast<EC_UTF8>(timer.event_info().m_SecondName) %
            timer.event_info().m_SeasonNum %
            timer.event_info().m_EpisodeNum;
        return false;
    }

    return timeline_builder_.add_timer(timer, conflict_list);
}

static bool timer_compare_on_time(const recording_timer& first_item, const recording_timer& second_item)
{
	return first_item.main_start_time() < second_item.main_start_time();
}

bool recorder_engine_impl::add_schedule(const schedule_item& item, recording_timers_list_t* conflict_list)
{
    if (conflict_list)
    {
        conflict_list->clear();
    }

    //generate timers for schedule
    recording_timers_list_t new_timers;
    generate_timers_for_schedule(item, new_timers);

    //sort timers on date/time
	std::sort(new_timers.begin(), new_timers.end(), timer_compare_on_time);

    size_t max_timers_per_schedule = 100;
    if (new_timers.size() > max_timers_per_schedule)
    {
	    log_info(L"recorder_engine_impl::add_schedule. Using only first %1% timers (were %2%) for the new schedule") % max_timers_per_schedule % new_timers.size();
        new_timers.resize(max_timers_per_schedule);
    }

    for (size_t i = 0; i < new_timers.size(); i++)
    {
        add_timer_to_timeline(item, new_timers[i], conflict_list);
    }

    bool res = true;
    if (conflict_list && conflict_list->size() != 0)
    {
        //there are errors or conflicts: delete already placed timers
        remove_schedule(item.schedule_item_id());
        log_error(L"recorder_engine_impl::add_schedule. There are errors or conflicts: delete already placed timers");
        res = false;
    }
    return res;
}

void recorder_engine_impl::remove_schedule(const schedule_item_id_t& schedule_id, bool delete_from_db /* = false */)
{
	log_info(L"recorder_engine_impl::remove_schedule. Removing recording schedule %1%") % schedule_id;

    //stop and complete all active recordings of this schedule
    active_rec_man_.remove_schedule(schedule_id);
    //remove all timers, which belong to this schedule
    timeline_builder_.remove_schedule(schedule_id);
    //also cancelled timers
    cancelled_rec_man_.remove_schedule(schedule_id);

	//remove schedule from database
    if (delete_from_db)
    {
        recorder_database_.remove_schedule(schedule_id);
    }
    else
    {
        recorder_database_.disable_schedule(schedule_id);
    }
}

void recorder_engine_impl::update_wakeup_timers()
{
    time_t wakeup_time = timeline_builder_.get_first_future_timer();
    if (wakeup_time == invalid_time_value)
    {
        //there are no future recording timers
        if (next_wakeup_timer_ != invalid_time_value)
        {
            //clean saved wakeup timer
            log_info(L"recorder_engine_impl::update_wakeup_timers. Clearing recording wake-up timer. No recordings are planned.");
            if (!remove_wakeup_time())
            {
                log_error(L"recorder_engine_impl::update_wakeup_timers. Unable to clear wakeup timer");
            }
            else
            {
                log_info(L"recorder_engine_impl::update_wakeup_timers. Wakeup timer removed (%1%)") % wakeup_time;
                next_wakeup_timer_ = wakeup_time;
            }
        }
    }
    else
    if (next_wakeup_timer_ != wakeup_time)
    {
        log_info(L"recorder_engine_impl::update_wakeup_timers. Setting wakeup timer to fire at %1%") % wakeup_time;

        if (!add_wakeup_time(wakeup_time))
        {
            log_error(L"recorder_engine_impl::update_wakeup_timers. Unable to schedule wakeup timer for %1%") % wakeup_time;
        }
        else
        {
            log_info(L"recorder_engine_impl::update_wakeup_timers. Wakeup timer successfully set to %1%") % wakeup_time;
            next_wakeup_timer_ = wakeup_time;
        }
    }
}

void recorder_engine_impl::complete_active_recording(const dvblink::device_id_t& src_id, active_recording_t& recording, bool delete_schedule /*= true*/)
{
    active_recording_t rec = recording;
    const recording_timer& timer = rec->get_timer();

    active_rec_man_.complete_active_recording(src_id, recording);


    completed_rec_man_.add_timer(timer.timer_id());
    //remove timer from  a timeline
    timeline_builder_.remove_timer(timer.timer_id());

    //broadcast recording completion message
    timer_recording_completed_request req;
    req.timer_id_ = timer.timer_id();

	//if this is a one shot recording - remove a schedule as well
    schedule_list_t schedule_list;
    if (recorder_database_.get_schedules(&schedule_list, timer.schedule_item_id()) && schedule_list.size() == 1)
    {
        if (schedule_list[0].get_sendto_targets().size() > 0)
        {
            req.targets_ = schedule_list[0].get_sendto_targets();
        } else
        {
            send_to_target_list_t targets;
            if (get_targets(targets))
            {
                for (size_t i=0; i<targets.size(); i++)
                {
                    if (targets[i].default_)
                    {
                        req.targets_.push_back(targets[i].id);
                        log_info(L"recorder_engine_impl::complete_active_recording. Using default target %1% on completed timer %2%") % targets[i].id.to_wstring() % timer.timer_id().to_wstring();
                    }
                }
            }
        }

	    if (delete_schedule && !schedule_list[0].is_recursive())
	    {
		    remove_schedule(timer.schedule_item_id());
	    }
    }

    //send message
    message_queue_->post(broadcast_addressee, req);

/*
    //enable standby mode if this was the last active recording
    if (active_rec_man_.get_quantity() == 0)
    {
        log_info(L"recorder_engine_impl::complete_active_recording. Enabling standby");
        enable_standby();
    }
*/
}

bool recorder_engine_impl::get_targets(send_to_target_list_t& targets)
{
    bool ret_val = false;

    xml_message_request req;
    req.cmd_id_ = SEND_TO_TARGET_CMD_GET_TARGETS;
    xml_message_response resp;

    if (message_queue_->send(social_message_queue_addressee, req, resp) == dvblink::messaging::success &&
        resp.result_.to_string().compare(xmlcmd_result_success) == 0)
    {
        send_to_get_targets_response r;
        read_from_xml(resp.xml_.to_string(), r);

        targets = r.targets_;
        ret_val = true;
    } else
    {
        log_error(L"recorder_engine_impl::get_targets. Get targets failed");
    }
    return ret_val;
}

const size_t max_interrupted_timers_num = 30;

void recorder_engine_impl::start_active_recording(const device_id_t& src_id, const recording_timer* timer)
{
    log_info(L"recorder_engine_impl::start_active_recording. Starting recording for timer %1% on channel %2%") % string_cast<EC_UTF8>(timer->timer_id().get()) % string_cast<EC_UTF8>(timer->channel().get());

    //prepare completed recording structure
    completed_recording cr;

    //delete a recording with the same timer id (should not happen, only in case of a crash?)
	if (recorder_database_.get_completed_recording(timer->timer_id(), cr))
	{
        bool bchanged = false;
        //change id of that completed recording, so that it does not clash with a new one
        std::string timer_id_base = timer->timer_id().to_string();
        for (size_t i=0; i<max_interrupted_timers_num; i++)
        {
            std::stringstream strbuf;
            strbuf << timer_id_base << "-" << i;

            timer_id_t new_timer_id(strbuf.str());
        	if (!recorder_database_.get_completed_recording(new_timer_id, cr))
            {
                //found a new slot
                log_info(L"recorder_engine_impl::start_active_recording. Renaming previously interrupted recording for timer %1% to part %2%") % timer->timer_id().to_wstring() % i;
                if (recorder_database_.update_completed_recording_timer_id(timer->timer_id(), new_timer_id))
                {
                    bchanged = true;
                    break;
                }
            }
        }

        //if all attempts failed - delete the existing recording, so that the new one can continue
        if (!bchanged)
        {
            log_warning(L"recorder_engine_impl::start_active_recording. Number of interrupted recordings for timer %1% exceeded maximum (%2%)") % timer->timer_id().to_wstring() % max_interrupted_timers_num;
            delete_completed_recording(timer->timer_id());
        }
    }

    cr.set(*timer);

    if (is_enough_free_disk_space())
    {
	    if (log_channel_map_.find(src_id) != log_channel_map_.end() && 
		    log_channel_map_[src_id].find(timer->channel()) != log_channel_map_[src_id].end())
	    {
            //purge extra series recordings (if needed)
            purge_extra_series_recordings(timer->schedule_item_id());

/*
            //disable standby mode if this is the first active recording
            if (active_rec_man_.get_quantity() == 0)
            {
                log_info(L"recorder_engine_impl::start_active_recording. Disabling standby");
                disable_standby();
            }
*/
            recording_timer new_active_timer = *timer;
            new_active_timer.set_image_url(timer->event_info().m_ImageURL);

            cr.set_channel(log_channel_map_[src_id][timer->channel()]);
            active_recording_t ar = boost::shared_ptr<active_recording>(new active_recording(recorder_settings_, new_active_timer, cr.channel(), server_, message_queue_));
            active_rec_man_.add_rec(src_id, ar);

            //get filename 
            cr.set_filename(ar->get_filename());
	    }
        else
	    {
		    log_error(L"recorder_engine_impl::start_active_recording. Failed to start recording for timer %1% on channel %2%. Channel is not present at the server anymore") % string_cast<EC_UTF8>(timer->timer_id().get()) % string_cast<EC_UTF8>(timer->channel().get());
		    cr.set_state(creError);
	    }
    }
    else
    {
        log_error(L"recorder_engine_impl::start_active_recording. Failed to start recording for timer %1% on channel %2%. Not enough free disk space is available") % string_cast<EC_UTF8>(timer->timer_id().get()) % string_cast<EC_UTF8>(timer->channel().get());
        cr.set_state(creError);
    }

    //update database (either error or in progress)
    recorder_database_.add_completed_recording(cr);    
    completed_rec_man_.add_timer(timer->timer_id());
}

void recorder_engine_impl::purge_extra_series_recordings(const schedule_item_id_t& schedule_id)
{
	//get the schedule itself
	schedule_list_t schedule_list;
	if (recorder_database_.get_schedules(&schedule_list, schedule_id) && schedule_list.size() > 0)
	{
		if (schedule_list[0].number_of_recordings_to_keep() != DL_RECORDER_KEEP_ALL && 
			schedule_list[0].number_of_recordings_to_keep() > 0)
		{
			//get all completed recordings of this schedule
			completed_recordings_list_t recordings;
			if (recorder_database_.get_completed_recording_for_schedule(schedule_id, recordings))
			{
				if (static_cast<int>(recordings.size()) > schedule_list[0].number_of_recordings_to_keep() - 1) //account for a new recording that's about to happen
				{
                    log_info(L"recorder_engine_impl::purge_extra_series_recordings. Keep %1% recordings for schedule %2%. Deleting extra %3% recording(s)") % schedule_list[0].number_of_recordings_to_keep() % schedule_id % (recordings.size() - schedule_list[0].number_of_recordings_to_keep());
					for (int i = 0; i < static_cast<int>(recordings.size()) - schedule_list[0].number_of_recordings_to_keep() + 1; i++)
					{
						delete_completed_recording(recordings[i].timer_id());
					}
				}
			}
		}
	}
}

bool recorder_engine_impl::delete_completed_recording(const timer_id_t& timer_id)
{
	bool res = false;
	//there can be situations (due to recently fixed bug) when there are more than one recordings for the same timer_id
	completed_recordings_list_t recording_list;
    if (recorder_database_.get_completed_recordings_for_timer(timer_id, recording_list))
	{
	    for (size_t i=0; i<recording_list.size(); i++)
	    {
	        completed_recording& recording = recording_list[i];
		    //see if this is recording in progress. if it is - it cannot be deleted. recording must be stopped first
		    if (recording.state() != creInProgress)
		    {
			    //try to delete a file first
                boost::filesystem::path file_path = recording.filename().to_boost_filesystem();
			    bool deleted = false;
			    try {
                    deleted = file_path.empty() || !boost::filesystem::exists(file_path) || (boost::filesystem::exists(file_path) && boost::filesystem::remove(file_path));
                } catch (...) {deleted = false;}
			    if (deleted)
			    {
                    //delete thumbnail (if any)
                    if (recording.event_info().m_ImageURL.size() > 0)
                    {
#ifdef WIN32
                boost::filesystem::path image_url(string_cast<EC_UTF8>(recording.event_info().m_ImageURL));
#else
                boost::filesystem::path image_url(recording.event_info().m_ImageURL);
#endif
                        if (boost::filesystem::exists(image_url))
                        {
							try {
	                            boost::filesystem::remove(image_url);
							} catch (...) {}
                        }
                    }
				    //delete a record in a database
				    res = recorder_database_.delete_completed_recording(timer_id);
				    log_info(L"recorder_engine_impl::delete_completed_recording. Completed recording for timer %1% was deleted") % string_cast<EC_UTF8>(timer_id.get());
			    }
                else
			    {
				    log_error(L"recorder_engine_impl::delete_completed_recording. Recorded file cannot be deleted");
			    }
		    }
            else
		    {
			    log_error(L"recorder_engine_impl::delete_completed_recording. Attempt to delete a recording in progress");
		    }
        }
	}
    else
    {
        log_error(L"recorder_engine_impl::delete_completed_recording. Recording for timer %1% was not found. Already deleted?") % string_cast<EC_UTF8>(timer_id.get());
    }
	return res;
}

void recorder_engine_impl::analyze_conflicts(map_tuner_timeline_t& now_timers)
{
    recording_timers_list_t timer_list;
    get_scheduled_timers(timer_list);
    for (size_t i = 0; i < timer_list.size(); ++i)
    {
        const recording_timer& timer = timer_list[i];
        if (timer.conflict())
        {
            // find this timer and remove it from now_timers
            map_tuner_timeline_t::iterator it_map = now_timers.begin();
            while (it_map != now_timers.end())
            {
                timeline_timer_list_t& list = it_map->second;
                timeline_timer_list_t::iterator it_list = list.begin();
                while (it_list != list.end())
                {
                    recording_timer& rec = *it_list;
                    if (rec.timer_id() == timer.timer_id())
                    {
                        it_list = list.erase(it_list);
                    }
                    else
                    {
                        ++it_list;
                    }
                }
                ++it_map;
            }
        }
    }
}

void recorder_engine_impl::process_timers()
{
    // check free disk space
    if (active_rec_man_.get_quantity() && !is_enough_free_disk_space())
    {
        log_warning(L"recorder_engine_impl::process_timers. Not enough disk space. Cancelling all ongoing timer recordings.");
        active_rec_man_.cancel_all_recs();
    }

    // update state of ongoing recordings and remove them from the list when they are finished
    active_rec_man_.update_state();

    // get timers for window: "now" until "now + prerecording time" - these are all potential timers that may start at this point of time
    // or already in recording
    time_t now; time(&now);
    map_tuner_timeline_t now_timers;
    timeline_builder_.get_timers_for_wnd(now, timeline_builder_.get_largest_before_margin(), now_timers);
    active_rec_man_.process_now_timers(now_timers);

    
    //process wakeup timers
    update_wakeup_timers();
}

void recorder_engine_impl::cancel_timer(timer_id_t& timer_id)
{
    //check active timers
    active_rec_man_.cancel_timer(timer_id);

    //find this timer in the timeline and cancel it
	recording_timer removed_timer;
	if (timeline_builder_.remove_timer(timer_id, &removed_timer))
	{
		//insert cancelled timer into database
		recorder_database_.add_cancelled_timer(removed_timer);

		//insert cancelled timer into map
        cancelled_rec_man_.add_timer(removed_timer);
	}

	log_info(L"recorder_engine_impl::cancel_timer. Timer %1% was cancelled") % string_cast<EC_UTF8>(timer_id.get());
}

bool recorder_engine_impl::get_scheduled_timers_in_csv(csv_string_t& schedule_csv) const
{
    return timeline_builder_.get_scheduled_timers_in_csv(schedule_csv);
}

bool recorder_engine_impl::are_conflicts_present()
{
    bool ret_val = false;
    
    recording_timers_list_t timer_list;
    get_scheduled_timers(timer_list);
    for (size_t i = 0; i < timer_list.size(); ++i)
    {
        if (timer_list[i].conflict())
        {
            ret_val = true;
            break;
        }
    }
    
    return ret_val;
}

bool recorder_engine_impl::get_wakeup_time(time_t& wakeup_time)
{
    bool ret_val = false;
    power::get_wakeup_time_request req(boost::uuids::to_string(message_queue_->get_uid()));
    power::get_wakeup_time_response resp;
    if (message_queue_->send(server_message_queue_addressee, req, resp) == success && resp.result_)
    {
        wakeup_time = resp.wakeup_time_;
        ret_val = true;
    }
    return ret_val;
}

bool recorder_engine_impl::remove_wakeup_time()
{
    bool ret_val = false;
    power::remove_wakeup_time_request req(boost::uuids::to_string(message_queue_->get_uid()));
    power::remove_wakeup_time_response resp;
    if (message_queue_->send(server_message_queue_addressee, req, resp) == success && resp.result_)
    {
        ret_val = true;
    }
    return ret_val;
}

bool recorder_engine_impl::add_wakeup_time(time_t wakeup_time)
{
    bool ret_val = false;
    power::add_wakeup_time_request req(boost::uuids::to_string(message_queue_->get_uid()), wakeup_time);
    power::add_wakeup_time_response resp;
    if (message_queue_->send(server_message_queue_addressee, req, resp) == success && resp.result_)
    {
        ret_val = true;
    }
    return ret_val;
}

} //recorder
} //dvblex
