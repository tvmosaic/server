/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_strings.h>
#include "completed_recording.h"

using namespace dvblink;
using namespace dvblink::engine;

namespace dvblex { namespace recorder {

void completed_recording::set(char** values, std::map<std::string, int>& value_map)
{
    if (value_map.find("schedule_id") != value_map.end() && values[value_map["schedule_id"]] != NULL)
    {
        schedule_item_id_t::type_t temp = schedule_item_id().get();
        engine::string_conv::apply(values[value_map["schedule_id"]], temp, static_cast<schedule_item_id_t::type_t>(DL_RECORDER_INVALID_ID));
        set_schedule_item_id(temp);
    }
    if (value_map.find("timer_id") != value_map.end() && values[value_map["timer_id"]] != NULL)
    {
        set_timer_id(values[value_map["timer_id"]]);
    }
    if (value_map.find("state") != value_map.end() && values[value_map["state"]] != NULL)
    {
        engine::string_conv::apply<int>(values[value_map["state"]], state_, creError);
    }
    if (value_map.find("filename") != value_map.end() && values[value_map["filename"]] != NULL)
    {
        filename_ = string_cast<EC_UTF8>(values[value_map["filename"]]);
    }

    ////read event info
    //epg_item epg_item;
    //fill_epg_event_info(epg_item, epg_full_info, values, value_map);
    //event_info_ = epg_item.event_;

    //read channel info
    int int_val;
    if (value_map.find("channel_id") != value_map.end() && values[value_map["channel_id"]] != NULL)
    {
        channel_.id_ = values[value_map["channel_id"]];
    }
    if (value_map.find("channel_num") != value_map.end() && values[value_map["channel_num"]] != NULL)
    {
        engine::string_conv::apply(values[value_map["channel_num"]], int_val, -1);
        channel_.number_ = int_val;
    }
    if (value_map.find("channel_subnum") != value_map.end() && values[value_map["channel_subnum"]] != NULL)
    {
        engine::string_conv::apply(values[value_map["channel_subnum"]], int_val, -1);
        channel_.sub_number_ = int_val;
    }
    if (value_map.find("channel_name") != value_map.end() && values[value_map["channel_name"]] != NULL)
    {
        channel_.name_ = values[value_map["channel_name"]];
    }
}

void completed_recording::set(const recording_timer& rec_timer)
{
    time_t now; time(&now);

    set_schedule_item_id(rec_timer.schedule_item_id());
    set_timer_id(rec_timer.timer_id());
    event_info_ = rec_timer.event_info();
    state_ = creInProgress;

    if (event_info_.m_Name.empty())
    {
        char dt_buf[1024];
        if (struct tm* ti = localtime(&now))
        {
            dt_buf[0] = '\0';
            sprintf(dt_buf, "%0.2d%0.2d_%d%0.2d%0.2d", ti->tm_hour, ti->tm_min, ti->tm_year + 1900, ti->tm_mon + 1, ti->tm_mday);
        }
        event_info_.m_Name = dt_buf;
    }

    event_info_.m_StartTime = now;
    event_info_.m_Duration = -1;
    channel_.id_ = rec_timer.channel();
}

} //recorder
} //dvblex

