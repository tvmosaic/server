/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include "recorder_database.h"
#include "completed_rec_man.h"

using namespace dvblink;

namespace dvblex { namespace recorder {

// read completed timers for the last 24 hours
//
void completed_rec_man::reset()
{
    recorder_db_.get_completed_recordings(rec_list_, 24);

    rec_map_.clear();
    completed_recordings_list_t::iterator it = rec_list_.begin();
    while (it != rec_list_.end())
    {
        rec_map_[it->timer_id()] = it->timer_id();
        ++it;
    }
}

void completed_rec_man::add_timer(const timer_id_t& timer_id)
{
    //save information in map
    rec_map_[timer_id] = timer_id;
}

bool completed_rec_man::is_exist(const timer_id_t& timer_id) const
{
    return rec_map_.find(timer_id) != rec_map_.end();
}

} //recorder
} //dvblex
