/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <map>
#include <boost/thread.hpp>
#include <dl_event.h>
#include <dl_types.h>
#include <dl_message_queue.h>
#include <dl_channels.h>
#include <dl_epg_channels.h>
#include "epg.h"

namespace dvblex { namespace recorder {

class recorder_database;

typedef std::map<dvblink::channel_id_t, dvblink::channel_id_t> map_channel_id_t;

class epg_updater
{
public:
    epg_updater(recorder_database& db, dvblink::messaging::message_queue_t& message_queue, dvblink::event& ev, map_channel_id_t& epg_update_channels);
    ~epg_updater();
    
    bool is_finished() {return is_finished_;}

protected:
    void update_thread_func();
    bool get_epg_channels_from_server(epg_channels_list_t& epg_channel_list, channel_to_epg_source_map_t& channel_to_epg_source_map);
    bool get_channel_epg_from_server(epg_channel& channel, const epg_source_channel_id_t& epg_source, epg_event_list_t& event_list);
    bool get_current_channels_map(epg_channel_map_t& channel_map);
    void clear_old_channels(epg_channel_map_t& current_channel_map, epg_channels_list_t& new_channel_list);

protected:
    bool is_finished_;
    recorder_database& recorder_db_;
    dvblink::event& finished_event_;
    bool exit_flag_;
    boost::thread* update_thread_;
    map_channel_id_t epg_update_channels_;
    dvblink::messaging::message_queue_t message_queue_;
};

} //recorder
} //dvblex
