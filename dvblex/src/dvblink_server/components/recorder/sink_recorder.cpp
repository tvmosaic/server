/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef _WIN32
#include <io.h>
#endif
#include <stdio.h>
#include <dl_logger.h>
#include <dl_utils.h>
#include <dl_status.h>
#include <dl_common.h>
#include <dl_xml_serialization.h>
#include <dl_file_procedures.h>
#include <dl_message_server.h>
#include <dl_message_addresses.h>
#include <dl_channel_serializer.h>
#include <dl_pb_recorded_tv.h>
#include <dl_pb_types.h>
#include <dl_recommender_cmd.h>
#include <dl_program_serializer.h>
#include <dl_timer_serializer.h>
#include <dl_rec_options_serializer.h>
#include <dl_schedule_serializer.h>
#include <dl_xmltv_writer.h>
#include "constants.h"
#include "recorder_engine.h"
#include <dl_schedules.h>
#include "sink_recorder.h"

using namespace dvblex;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblex::playback;
using namespace dvblink::messaging;
using namespace dvblink::messaging::recorder;
using namespace dvblink;

namespace dvblex { namespace recorder {

sink_recorder::message_handler::cmd_handler_t sink_recorder::message_handler::cmd_handler_;
sink_recorder::message_handler::rec_cmd_pair sink_recorder::message_handler::cmd_handler_pairs_[] =
{
    {dvblex::tvadviser::find_programs_cmd, &sink_recorder::message_handler::find_programs},
};

sink_recorder::message_handler::message_handler(const i_server_t& server, messaging::message_queue_t message_queue) :
    start_request::subscriber(message_queue),
    standby_request::subscriber(message_queue),
    resume_request::subscriber(message_queue),
    shutdown_request::subscriber(message_queue),
    get_recorder_info_request::subscriber(message_queue),
    force_update_request::subscriber(message_queue),
    set_recording_options_request::subscriber(message_queue),
    get_recording_options_request::subscriber(message_queue),
    search_epg_request::subscriber(message_queue),
    add_schedule_request::subscriber(message_queue),
    get_schedule_request::subscriber(message_queue),
    update_schedule_request::subscriber(message_queue),
    remove_schedule_request::subscriber(message_queue),
    get_recordings_request::subscriber(message_queue),
    remove_recording_request::subscriber(message_queue),
    get_completed_recordings_request::subscriber(message_queue),
    update_recorder_epg_request::subscriber(message_queue),
    remove_completed_recording_request::subscriber(message_queue),
    get_schedules_in_csv_request::subscriber(message_queue),
    repair_database_request::subscriber(message_queue),
    get_xmltv_epg_request::subscriber(message_queue),
    channels::channel_config_changed_request::subscriber(message_queue),
    xml_message_request::subscriber(message_queue),
    server_(server),
    message_queue_(message_queue)
{
    fill_cmds();
}

sink_recorder::message_handler::~message_handler()
{

}

void sink_recorder::message_handler::fill_cmds()
{
    for (size_t i = 0; i < sizeof(cmd_handler_pairs_)/sizeof(cmd_handler_pairs_[0]); ++i)
    {
        cmd_handler_[cmd_handler_pairs_[i].cmd_] = cmd_handler_pairs_[i].handler_;
    }
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const start_request& request, start_response& response)
{
    response.result_ = recorder_engine_.run(server_, message_queue_);
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const standby_request& request, standby_response& response)
{
    recorder_engine_.standby();
    response.result_ = true;
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const resume_request& request, resume_response& response)
{
    recorder_engine_.resume();
    response.result_ = true;
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const shutdown_request& request, shutdown_response& response)
{
    recorder_engine_.stop();
    response.result_ = true;
}

void sink_recorder::message_handler::channel_config_changed_handler()
{
    if (recorder_engine_.is_running())
    {
        recorder_engine_.force_update();
    }
}

void sink_recorder::message_handler::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::channel_config_changed_request& request)
{
    if (request.changes_.has_epg_config_changes())
    {
        recorder_engine_.update_channels_epg(request.changes_.epg_config_channels_);
    } else
    {
        channel_config_changed_handler();
    }
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const get_recorder_info_request& request, get_recorder_info_response& response)
{
    response.result_ = true;
    response.info_ =
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" \
        "<RecorderInfo>" \
        "   <name>TV Mosaic recorder</name>" \
        "   <version>7.0.0</version>" \
        "   <info>(c) DVBLogic 2017</info>" \
        "</RecorderInfo>";
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const force_update_request& request)
{
    recorder_engine_.force_update();
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const set_recording_options_request& request, set_recording_options_response& response)
{
    response.result_ = false;

    rd_recording_settings_t settings;
    if (read_from_xml<rd_recording_settings_t>(request.options_.get(), settings))
    {
        recorder_settings rec_settings;
        if (recorder_engine_.get_settings(rec_settings))
        {
            rec_settings.set_before_margin(settings.get_before_margin());
            rec_settings.set_after_margin(settings.get_after_margin());
            rec_settings.set_check_deleted_recordings(settings.check_deleted_recordings());
            rec_settings.set_ds_threshold_params(settings.use_auto_disk_space_mode(), settings.get_manual_disk_space_kb());
            rec_settings.set_autodelete_enabled(settings.use_autodelete_algo());
            rec_settings.set_new_only_algo_type((new_only_algo_type_e)settings.get_new_only_algo());
			if (settings.get_filename_pattern().size() > 0)
				rec_settings.set_recording_filename_pattern(settings.get_filename_pattern());
            rec_settings.set_new_only_default_value((new_only_default_value_e)settings.get_new_only_default_value());

            rec_settings.set_record_path(filesystem::make_preferred_dvblink_filepath(settings.get_recording_path()));

            if (recorder_engine_.set_settings(rec_settings))
                response.result_ = true;
        }
    }
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const get_recording_options_request& request, get_recording_options_response& response)
{
    response.result_ = false;

    recorder_settings rec_settings;
    if (recorder_engine_.get_settings(rec_settings))
    {
        rd_recording_settings_t settings;
        settings.set_after_margin(rec_settings.get_after_margin());
        settings.set_before_margin(rec_settings.get_before_margin());
        settings.set_check_deleted_recordings(rec_settings.check_deleted_recordings());

        settings.set_new_only_algo((recorder_new_only_algo_type_e)rec_settings.get_new_only_algo_type());
        settings.set_new_only_default_value((recorder_new_only_default_value_e)rec_settings.get_new_only_default_value());

        settings.set_recording_path(rec_settings.get_record_path());

        //available/total disk space
        boost::int64_t total_space;
        boost::int64_t available_space;
        rec_settings.get_rec_disk_space_stats_kb(total_space, available_space);
        
        settings.set_total_space_kb(total_space);
        settings.set_avail_space_kb(available_space);

        settings.set_auto_disk_space_mode(rec_settings.get_ds_threshold_mode());
        settings.set_manual_disk_space_kb(rec_settings.get_ds_threshold_kb());
        settings.set_autodelete_algo(rec_settings.is_autodelete_enabled());

		settings.set_filename_pattern(rec_settings.get_recording_filename_pattern());

        std::string epg_string;
        if (write_to_xml<rd_recording_settings_t>(settings, epg_string))
        {
            response.options_ = epg_string;
            response.result_ = true;
        }
    }
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const messaging::recorder::repair_database_request& request, messaging::recorder::repair_database_response& response)
{
    recorder_engine_.repair_database();

	response.result_ = true;
}

bool sink_recorder::message_handler::find_programs(const messaging::xml_message_request& request, messaging::xml_message_response& response)
{
    dvblex::tvadviser::map_channel_programs_t channels;
    boost::tuples::tuple<dvblex::tvadviser::map_channel_programs_t&> inparam(boost::ref(channels));
    decode(request, inparam);

    std::string keyphrase;
    dvblex::tvadviser::map_channel_programs_t::iterator iter = channels.begin();
    while (iter != channels.end())
    {
        channel_id_t chid = iter->first;
        dvblex::tvadviser::mixed_program_info_t& infos = iter->second;

        server_channel_list_t channel_list;
        channel_list.push_back(chid);

        time_t start, end;
        if (infos.size())
        {
            start = infos[0].start_; end = infos[0].end_;
            for (size_t i = 1; i < infos.size(); ++i)
            {
                dvblex::tvadviser::mixed_program_info& info = infos[i];
                if (info.start_ < start) start = info.start_;
                if (info.end_ > end)     end = info.end_;
            }

            epg_ex_channel_info_map_t epg_channel_events;
            bool query_success = recorder_engine_.browse_epg(channel_list,
                start, end, keyphrase, DRGC_ANY, epg_full_info, -1, epg_channel_events);
            if (query_success && epg_channel_events.size())
            {
                epg_ex_channel_info_map_t::const_iterator iiter = epg_channel_events.begin();
                const epg_event_ex_list_t& list = iiter->second;

                for (size_t n = 0; n < infos.size(); ++n)
                {
                    dvblex::tvadviser::mixed_program_info& info = infos[n];
                    for (size_t j = 0; j < list.size(); ++j)
                    {
                        const epg_item& item = list[j];
                        if (item.event_.m_StartTime == info.start_)
                        {
//                            info.dvblink_id_ = item.event_.id_;
                            info.event_ = item.event_;
                            info.is_series_ = true;
                            break;
                        }
                    }
                }
            }
        }
        ++iter;
    }

    boost::tuples::tuple<const dvblex::tvadviser::map_channel_programs_t&> outparam(boost::cref(channels));
    encode(outparam, response);
    return true;
}


void sink_recorder::message_handler::handle(const message_sender_t& sender, const search_epg_request& request, search_epg_response& response)
{
    response.result_ = false;

    std::string search_data_string(request.search_data_.get());
    search_epg_request_t es;
    if (read_from_xml<search_epg_request_t>(search_data_string, es))
    {
        timed_epg_ex_channel_info_map_t timed_epg_map;

        //common search properties
        epg_level_e info_level = epg_short_info;
        if (!es.short_epg_)
            info_level = epg_full_info;

        server_channel_list_t channel_list = es.channels_;

        bool query_success = false;
        if (es.is_program_info_request())
        {
            if (channel_list.size() == 1)
            {
                epg_event_id_t s = es.program_id_;

                epg_item_ex item;
                query_success = recorder_engine_.get_epg_event(channel_list[0], s, info_level, item);

                //even if epg item was not found - return success and empty result list
                timed_epg_map.epg_map_[channel_list[0]] = epg_event_ex_list_t();
                if (query_success)
                    timed_epg_map.epg_map_[channel_list[0]].push_back(item);

                query_success = true;
            }
        }
        else
        {
            int time_from = -1;
            int time_to = -1;
            std::string keyphrase;
            //get all search parameters

            if (es.is_keyword_search())
                keyphrase = es.keywords_.get();

            if (es.is_time_search())
            {
                time_from = es.start_time_;
                time_to = es.end_time_;
            }

            boost::int32_t max_count = es.requested_count_;

            if (time_from == -1)
            {
                //do not return any epg from the past when not explicitely requested
                time_t now; time(&now);
                time_from = now;
            }
            query_success = recorder_engine_.browse_epg(channel_list,
                time_from, time_to, keyphrase, es.genre_mask_, info_level, max_count, timed_epg_map.epg_map_);
        }

        if (query_success)
        {
            recorder_engine_.get_last_epg_update_time(timed_epg_map.time_stamp_);
			std::string epg_string;
			if (write_to_xml(timed_epg_map, epg_string))
			{
				response.epg_ = epg_string;
				response.result_ = true;
			}
        }
    }
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const get_xmltv_epg_request& request, get_xmltv_epg_response& response)
{
    server_channel_list_t channel_list; //all channels in epg
    std::string keyphrase;
    epg_level_e info_level = epg_full_info;
    boost::int32_t max_count = -1;
    unsigned long genre_mask = 0;

    time_t now; time(&now);
    int time_from = now; //epg from now onwards
    int time_to = request.days_ > 0 ? time_from + request.days_*3600*24 : -1;

    epg_ex_channel_info_map_t epg_map;
    if (recorder_engine_.browse_epg(channel_list, time_from, time_to, keyphrase, genre_mask, info_level, max_count, epg_map))
    {
        xmltv_creator xc;
        if (xc.start(request.simple_time_format_ ? xmltv_creator::dtf_simple : xmltv_creator::dtf_with_tz))
        {
            for (size_t i=0; i<request.channels_.size(); i++)
                xc.add_channel(request.channels_[i]);

            epg_ex_channel_info_map_t::iterator it = epg_map.begin();
            while (it != epg_map.end())
            {
                for (size_t i=0; i<it->second.size(); i++)
                    xc.add_event(it->first, it->second.at(i).event_);

                ++it;
            }

            xc.finish_and_create_string(response.epg_);
        }

        response.result_ = true;
    }
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const add_schedule_request& request, add_schedule_response& response)
{
    response.result_ = false;
    schedule_item new_schedule_item;

    schedule_item item;
    if (read_from_xml(request.schedule_.get(), item))
    {
        response.result_ = recorder_engine_.add_schedule(item, NULL);
    }
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const get_schedule_request& request, get_schedule_response& response)
{
    response.result_ = false;

    schedule_list_t schedule_list;
    if (recorder_engine_.get_schedules(schedule_list))
    {
        std::string epg_string;
        if (write_to_xml(schedule_list, epg_string))
        {
            response.schedule_ = epg_string;
            response.result_ = true;
        }
    }
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const update_schedule_request& request, update_schedule_response& response)
{
    response.result_ = false;

    update_schedule_item_info update_info;
    if (read_from_xml(request.schedule_.get(), update_info))
    {
        response.result_ = recorder_engine_.update_schedule(update_info);
    }
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const remove_schedule_request& request, remove_schedule_response& response)
{
    response.result_ = false;
    schedule_remover_t schedule_remover;
    if (read_from_xml(request.schedule_.get(), schedule_remover))
    {
        if (schedule_remover.schedule_item_id_ != DL_RECORDER_INVALID_ID)
            response.result_ = recorder_engine_.delete_schedule(schedule_remover.schedule_item_id_);
    }
}

static void AddTimerToRecordingList(recording_timer& timer, bool b_cancelled, rd_recording_list_t& recordings, schedule_map_t& schedule_map, timers_id_map_t active_timer_map)
{
    std::wstringstream strbuf;
    rd_recording_t rec;
    rec.set_recording_id(string_cast<EC_UTF8>(timer.timer_id().get()));

    rec.set_schedule_id(timer.schedule_item_id());

    rec.set_active(active_timer_map.find(timer.timer_id()) != active_timer_map.end());
    rec.set_conflicting(timer.conflict());

    rec.set_channel_id(timer.channel());

    epg_item_ex ei;
    ei.event_ = timer.event_info();
    ei.is_series_ = true;
    ei.is_record_ = !b_cancelled;
    if (schedule_map.find(timer.schedule_item_id()) != schedule_map.end() && schedule_map[timer.schedule_item_id()].is_recursive())
        ei.is_repeat_record_ = true;
	ei.is_record_conflict_ = !b_cancelled && timer.conflict();

    rec.set_program(ei);

    recordings.push_back(rec);
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const get_recordings_request& request, get_recordings_response& response)
{
    response.result_ = false;

    std::wstring wstr;
    std::wstringstream strbuf;
    recording_timers_list_t scheduled_timer_list, active_timer_list, cancelled_timer_list;
    schedule_list_t schedule_list;
    if (recorder_engine_.get_timers(scheduled_timer_list, active_timer_list, cancelled_timer_list) &&
        recorder_engine_.get_schedules(schedule_list))
    {
        //make schedule map
        schedule_map_t schedule_map;
        make_schedule_map_from_list(schedule_list, schedule_map);

        //make active timer map
        timers_id_map_t active_timer_map;
        get_map_from_timers_list(active_timer_list, active_timer_map);

        rd_recording_list_t recordings;
        while (scheduled_timer_list.size())
        {
            AddTimerToRecordingList(scheduled_timer_list[0], false, recordings, schedule_map, active_timer_map);
            scheduled_timer_list.erase(scheduled_timer_list.begin());
        }

        while (cancelled_timer_list.size())
        {
            AddTimerToRecordingList(cancelled_timer_list[0], true, recordings, schedule_map, active_timer_map);
            cancelled_timer_list.erase(cancelled_timer_list.begin());
        }

        std::string recordings_string;
        if (write_to_xml<rd_recording_list_t>(recordings, recordings_string))
        {
            response.recordings_ = recordings_string;
            response.result_ = true;
        }
    }
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const remove_recording_request& request, remove_recording_response& response)
{
    response.result_ = false;
    rd_recording_remover_t rec_item;
    if (read_from_xml<rd_recording_remover_t>(request.recording_.get(), rec_item))
    {
        timer_id_t s = string_cast<EC_UTF8>(rec_item.get_recording_id().get());
        response.result_  = recorder_engine_.delete_timer(s);
    }
}

void sink_recorder::message_handler::handle(const message_sender_t& sender,
    const messaging::recorder::get_completed_recordings_request& request, messaging::recorder::get_completed_recordings_response& response)
{
    response.result_ = false;

    schedule_list_t schedule_list;
    if (recorder_engine_.get_schedules(schedule_list, false))
    {
        schedule_map_t schedule_map;
        make_schedule_map_from_list(schedule_list, schedule_map);

	    completed_recordings_list_t completed_recordings_list;
	    if (recorder_engine_.get_completed_recordings(completed_recordings_list))
	    {
            time_t now; time(&now);

		    for (size_t i = 0; i < completed_recordings_list.size(); i++)
		    {
			    boost::shared_ptr<pb_recorded_tv_t> rtv_item(new pb_recorded_tv_t());

                //update duration for items that are under recording
                if (completed_recordings_list[i].event_info().m_Duration <= 0 && completed_recordings_list[i].state() == creInProgress)
                {
                    completed_recordings_list[i].event_info().m_Duration = now - completed_recordings_list[i].event_info().m_StartTime;
                }

                rtv_item->video_info_ = completed_recordings_list[i].event_info();

			    rtv_item->channel_id_ = completed_recordings_list[i].channel().id_;
			    rtv_item->channel_name_ = completed_recordings_list[i].channel().name_;
			    rtv_item->channel_number_ = completed_recordings_list[i].channel().number_;
			    rtv_item->channel_subnumber_ = completed_recordings_list[i].channel().sub_number_;
                rtv_item->object_id_ = completed_recordings_list[i].timer_id().get();
                rtv_item->can_be_deleted_ = true;

			    rtv_item->url_ = completed_recordings_list[i].filename().to_string();

			    pb_recorded_tv_state_e state;
			    switch (completed_recordings_list[i].state())
			    {
			    case creInProgress:
				    state = rtvs_in_progress;
                    rtv_item->can_be_deleted_ = false;
				    break;
			    case creError:
				    state = rtvs_error;
				    break;
			    case creForcedToCompletion:
				    state = rtvs_forced_to_completion;
				    break;
			    case creCompleted:
				    state = rtvs_completed;
				    break;
			    default:
				    state = rtvs_completed;
				    break;
			    }

			    rtv_item->state_ = state;
                //find this schedule in the schedule list
                if (schedule_map.find(completed_recordings_list[i].schedule_item_id()) != schedule_map.end())
                {
                    rtv_item->series_schedule_ = schedule_map[completed_recordings_list[i].schedule_item_id()].is_recursive();
                    rtv_item->schedule_id_ = boost::lexical_cast<std::string>(completed_recordings_list[i].schedule_item_id());
                    rtv_item->schedule_name_ = schedule_map[completed_recordings_list[i].schedule_item_id()].get_schedule_name();
                }
                //file size
                boost::int64_t fsize = filesystem::get_file_size(completed_recordings_list[i].filename().to_wstring());
                if (fsize != -1)
                    rtv_item->size_ = fsize;

                response.completed_recordings_.push_back(rtv_item);
		    }

            response.result_ = true;
	    }
    }
    
}

void sink_recorder::message_handler::handle(const message_sender_t& sender,
    const messaging::recorder::remove_completed_recording_request& request, messaging::recorder::remove_completed_recording_response& response)
{
    timer_id_t timer_id = request.recording_id_.get();
    response.result_  = recorder_engine_.delete_completed_recording(timer_id);
}

void sink_recorder::message_handler::handle(const message_sender_t& sender,
    const messaging::recorder::update_recorder_epg_request& request, messaging::recorder::update_recorder_epg_response& response)
{
    response.result_  = recorder_engine_.update_channels_epg(request.channel_list_);
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const get_schedules_in_csv_request& request, get_schedules_in_csv_response& response)
{
    response.result_ = recorder_engine_.get_scheduls_in_csv(response.schedules_);
}

void sink_recorder::message_handler::handle(const message_sender_t& sender, const messaging::xml_message_request& request, messaging::xml_message_response& response)
{
    return process(request, response);
}

void sink_recorder::message_handler::process(const messaging::xml_message_request& request, messaging::xml_message_response& response)
{
    bool res = false;
    citer_cmd_handler_t iter = cmd_handler_.find(request.cmd_id_.get());
    if (iter != cmd_handler_.end())
    {
        //std::wstring fname = string_cast<EC_UTF8>(iter->first);
        //log_info(L"     >>>>>>>>>>>>>>>>> %1%:: - begin...") % fname;
        res = (this->*(iter->second))(request, response);
        //log_info(L"     <<<<<<<<<<<<<<<<< %1%:: - ...end") % fname;
    }
    response.result_ = res ? dvblex::tvadviser::success : dvblex::tvadviser::fail;
}

///////////////////////////////////////////////////////////////////////////////

sink_recorder::sink_recorder(const i_server_t& server, const dvblink::base_id_t& id) :
    id_(id)
{
    server_ = server;

    message_queue_ = share_object_safely(new message_queue(id_.get()));
    message_handler_ = std::auto_ptr<message_handler>(new message_handler(server_, message_queue_));

    server->register_queue(message_queue_);
}

sink_recorder::~sink_recorder()
{
    server_->unregister_queue(message_queue_->get_id());

    message_queue_->shutdown();
    message_handler_.reset();
}

} //recorder
} //dvblex
