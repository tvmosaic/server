/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <dl_filesystem_path.h>
#include <dl_installation_settings.h>
#include "constants.h"

namespace dvblex { namespace recorder {

enum new_only_algo_type_e
{
    noat_not_seen_before,
    noat_epg_repeat_flag,
    noat_epg_premiere_flag
};

enum new_only_default_value_e
{
    nodv_not_set,
    nodv_true,
    nodv_false
};

class recorder_settings
{
public:
    recorder_settings() {}

    recorder_settings(recorder_settings& settings) {copy_settings(settings);}
    
    void copy_settings(const recorder_settings& from_settings);
    bool init(dvblex::settings::installation_settings_t& install_settings);
    void term() {}
    void save() {save_settings();}
    void load() {load_settings();}

    dvblink::filesystem_path_t get_record_path() {return record_path_;}
    dvblink::filesystem_path_t get_thumbnail_path() {return record_path_ / DL_RECORDER_THUMBNAILS_DIR; }

    time_t get_before_margin() const {return recording_margin_before_;}
    time_t get_after_margin() const {return recording_margin_after_;}
    dvblink::filesystem_path_t get_record_db_path() {return recorder_db_path_;}
    bool check_deleted_recordings() const {return check_deleted_recordings_;}
    bool get_ds_threshold_mode(){return ds_threshold_auto_;}
    boost::int64_t get_ds_threshold_kb();
    bool is_autodelete_enabled(){return autodelete_enabled_;}
    new_only_algo_type_e get_new_only_algo_type() const {return new_only_algo_type_;}
	const std::string get_recording_filename_pattern() const {return filename_pattern_;}
    new_only_default_value_e get_new_only_default_value() const {return new_only_default_value_;}

    void set_before_margin(int margin){recording_margin_before_ = margin;}
    void set_after_margin(int margin){recording_margin_after_ = margin;}
    void set_record_path(const dvblink::filesystem_path_t& record_path){record_path_ = record_path;}
    void set_check_deleted_recordings(bool check) {check_deleted_recordings_ = check;}
    void set_ds_threshold_params(bool mode_auto, boost::uint64_t threshold_kb);
    void set_autodelete_enabled(bool autodelete_enabled){autodelete_enabled_ = autodelete_enabled;}
    void set_new_only_algo_type(new_only_algo_type_e algo_type) {new_only_algo_type_ = algo_type;}
	void set_recording_filename_pattern(const std::string& pattern) {filename_pattern_ = pattern;}
    void set_new_only_default_value(new_only_default_value_e value) {new_only_default_value_ = value;}

    bool get_rec_disk_space_stats_kb(boost::int64_t& total_space, boost::int64_t& available_space);

protected:
    boost::int64_t get_auto_disk_space_threshold();
    dvblink::filesystem_path_t get_settings_pathname() {return recorder_settings_pathname_;}
    void load_settings();
    void save_settings();
    void reset_settings();
    bool read_default_recorder_path(dvblink::filesystem_path_t& default_path) const;
    void setup_record_path();

protected:
    time_t recording_margin_before_; //in seconds
    time_t recording_margin_after_; //in seconds
    boost::int64_t min_free_disk_space_;
    bool check_deleted_recordings_;
    bool ds_threshold_auto_;
    boost::int64_t manual_disk_threshold_kb_;
    bool autodelete_enabled_;
    new_only_algo_type_e new_only_algo_type_;
	std::string filename_pattern_;
    dvblink::filesystem_path_t record_path_;
    dvblink::filesystem_path_t recorder_db_path_;
    dvblink::filesystem_path_t recorder_settings_pathname_;
    dvblink::filesystem_path_t default_record_path_;
    new_only_default_value_e new_only_default_value_;
};

} //recorder
} //dvblex
