/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <dl_types.h>
#include <dl_epgevent.h>
#include <dl_programs.h>

namespace dvblex { namespace recorder {

typedef std::vector<dvblink::channel_id_t> server_channel_list_t;

enum epg_level_e
{
	epg_short_info,
	epg_full_info
};

typedef std::vector<epg_item> epg_event_list_t;

class epg_channel
{
public:
    epg_channel() {}
    ~epg_channel() {}
    
public:
    dvblink::channel_id_t channel_id_;
    dvblink::epg_channel_id_t epg_channel_id_;
};

class epg_channel_info
{
public:
    epg_channel_info() {}
    ~epg_channel_info() {}

public:
    epg_channel channel_;
    epg_event_list_t epg_list_;
};


typedef std::vector<epg_channel> epg_channels_list_t;
typedef std::vector<epg_channel_info> epg_channel_info_list_t;
typedef std::map<dvblink::channel_id_t, epg_event_list_t> epg_channel_info_map_t;
typedef std::map<dvblink::channel_id_t, epg_channel> epg_channel_map_t;

} //recorder
} //dvblex
