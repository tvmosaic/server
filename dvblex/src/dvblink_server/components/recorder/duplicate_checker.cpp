/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include "duplicate_checker.h"
#include "recorder_database.h"
#include "completed_recording.h"


using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink;

namespace dvblex { namespace recorder {

void recorder_duplicate_checker::init(const schedule_item& item)
{
    episode_name_map_.clear();
    episode_num_map_.clear();

    if (item.is_epg_based() && item.record_series())
    {
        completed_recordings_list_t recordings;
        recorder_db_.get_completed_recording_for_schedule(item.schedule_item_id(), recordings);
        
        //add already deleted recordings to the list
        completed_recordings_list_t deleted_recordings;
        recorder_db_.get_deleted_recording_for_schedule(item.schedule_item_id(), deleted_recordings);

        recordings.insert(recordings.end(), deleted_recordings.begin(), deleted_recordings.end());

        for (size_t i = 0; i < recordings.size(); i++)
        {
            std::string name_lower_case = make_episode_name_key(recordings[i].event_info());
            if (is_valid_name_key(name_lower_case))
            {
                episode_name_map_[name_lower_case] = name_lower_case;
            }

            unsigned long key = make_episode_key(recordings[i].event_info());
            if (is_valid_num_key(key))
            {
                episode_num_map_[key] = key;
            }
        }
    }
}

bool recorder_duplicate_checker::was_recorded_before(const recording_timer& timer)
{
	switch (recorder_settings_.get_new_only_algo_type())
	{
	case noat_epg_repeat_flag:
        return timer.event_info().m_IsRepeatFlag;
		break;
	case noat_epg_premiere_flag:
        return !timer.event_info().m_IsPremiere;
		break;
	default:
		break;
	}

    std::string name_lower_case = make_episode_name_key(timer.event_info());
    unsigned long key = make_episode_key(timer.event_info());
    return episode_name_map_.find(name_lower_case) != episode_name_map_.end() || episode_num_map_.find(key) != episode_num_map_.end();
}

std::string recorder_duplicate_checker::make_episode_name_key(const DLEPGEvent& event)
{
    return string_cast<EC_UTF8>(boost::algorithm::to_lower_copy(string_cast<EC_UTF8>(event.m_SecondName)));
}

bool recorder_duplicate_checker::is_already_on_timeline(recorder_settings& rs, const timeline_builder& timeline, const recording_timer& timer)
{
	switch (rs.get_new_only_algo_type())
	{
	case noat_epg_repeat_flag:
        return timer.event_info().m_IsRepeatFlag;
		break;
	case noat_epg_premiere_flag:
        return !timer.event_info().m_IsPremiere;
		break;
	default:
		break;
	}
    
    return timeline.is_already_on_timeline(timer);
}

bool recorder_duplicate_checker::is_series_program(const recording_timer& timer)
{
    //program is series if a) is_serial is set or b)episode name is not empty or c) episode/season number is not empty
    return timer.event_info().m_IsSerial || timer.event_info().m_SecondName.size() > 0 || timer.event_info().m_SeasonNum > 0 || timer.event_info().m_EpisodeNum > 0;
}

} //recorder
} //dvblex
