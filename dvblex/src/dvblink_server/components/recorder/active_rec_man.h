/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include "active_recording.h"
#include "channel_set_container.h"

namespace dvblex { namespace recorder {

class recorder_database;
class recording_timer_t;
class recorder_engine_impl;

class active_rec_man
{
public:
    active_rec_man(recorder_engine_impl* parent, recorder_database& db, channel_set_container& csc) :
        parent_(parent), recorder_db_(db), channel_set_container_(csc)
    {}
    ~active_rec_man() {}

    size_t get_quantity() const;

    void add_rec(const dvblink::device_id_t& src_id, active_recording_t& rec);

    void process_now_timers(const map_tuner_timeline_t& now_map);
    void update_state();

    active_recordings_map_t get_recordings() const {return active_recordings_;}
    void get_active_timers(recording_timers_list_t& timer_list) const;

    void start_recording(const dvblink::device_id_t& src_id, recording_timer_t* timer);

    void update_recording_thumbnail(active_recording_t& recording);
    void complete_active_recording(const dvblink::device_id_t& src_id, active_recording_t& recording);

    void cancel_timer(const dvblink::timer_id_t& timer_id);
    void cancel_all_recs();
    void complete_all_recs();
    void remove_schedule(const dvblink::schedule_item_id_t& schedule_id);

protected:
    void process_now_timers(const dvblink::device_id_t& src_id, const timeline_timer_list_t& timer_list);
    void remove_rec(const dvblink::device_id_t& src_id, const active_recording_t& rec);
    void convert(const active_recordings_list_t& list, timeline_timer_list_t& timer_list);

private:
    recorder_engine_impl* parent_;
    active_recordings_map_t active_recordings_;
    recorder_database& recorder_db_;
    channel_set_container& channel_set_container_;
};

} //recorder
} //dvblex
