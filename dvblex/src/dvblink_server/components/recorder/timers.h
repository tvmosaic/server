/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <sstream>
#include <string>
#include <map>
#include <list>
#include <boost/algorithm/string.hpp> 
#include <dl_types.h>
#include <dl_epgevent.h>
#include <dl_platforms.h>
#include "constants.h"
#include <dl_schedules.h>


namespace dvblex { namespace recorder {

class logical_channel_desc
{
public:
    logical_channel_desc();
    logical_channel_desc(const dvblink::channel_id_t& id, const dvblink::channel_name_t& name, const dvblink::channel_number_t& number, const dvblink::channel_subnumber_t& sub_number);

    void set(const dvblink::channel_id_t& id, const dvblink::channel_name_t& name, const dvblink::channel_number_t& number, const dvblink::channel_subnumber_t& sub_number);
    void reset();

public:
	dvblink::channel_id_t     id_;
	dvblink::channel_name_t   name_;
    dvblink::channel_number_t            number_;
    dvblink::channel_subnumber_t         sub_number_;
};

class timer
{
public:
    timer() {}
    timer(const timer& t) :
        timer_id_(t.timer_id()),
        schedule_item_id_(t.schedule_item_id())
    {}
    timer(const dvblink::timer_id_t& timer_id, const dvblink::schedule_item_id_t& schedule_item_id) :
        timer_id_(timer_id),
        schedule_item_id_(schedule_item_id)
    {}
    virtual ~timer() {}

    void set_timer_id(const dvblink::timer_id_t& timer_id) {timer_id_ = timer_id;}
    void set_schedule_item_id(const dvblink::schedule_item_id_t& schedule_item_id) {schedule_item_id_ = schedule_item_id;}

    const dvblink::timer_id_t& timer_id() const {return timer_id_;}
    const dvblink::schedule_item_id_t& schedule_item_id() const {return schedule_item_id_;}

private:
    dvblink::timer_id_t                  timer_id_;
    dvblink::schedule_item_id_t          schedule_item_id_;
};

class recording_timer : public timer
{
public:
    recording_timer() {}
    recording_timer(const schedule_item& sch_item);
    recording_timer(const schedule_item& sch_item, const time_t& start_time);
    recording_timer(const schedule_item& sch_item, const dvblink::epg_event_id_t& epg_event_id, const dvblink::engine::DLEPGEvent& event_info);
    recording_timer(const schedule_item& sch_item, const dvblink::channel_id_t& channel, const dvblink::epg_event_id_t& epg_event_id, const dvblink::engine::DLEPGEvent& event_info);
    ~recording_timer() {}
    
	bool is_by_epg() const {return epg_event_id_.get().size() > 0;}
    void make_id();
    std::wstring generate_record_name() const;
    
    time_t real_start_time() const {return start_time_ - margin_before_;}
    time_t real_duration() const {return duration_ + margin_after_ + margin_before_;}
    time_t main_start_time() const {return start_time_;}
    time_t main_duration() const {return duration_;}
    time_t margin_before() const {return margin_before_;}
    time_t margin_after() const {return margin_after_;}

    int get_priority() const {return priority_;}

    const dvblink::engine::DLEPGEvent& event_info() const {return event_info_;}
    const dvblink::channel_id_t& channel() const {return channel_;}
    const dvblink::epg_event_id_t& epg_event_id() const {return epg_event_id_;}
    bool conflict() const {return conflict_;}

    void set_conflict(bool conflict) {conflict_ = conflict;}
    void set_image_url(const std::string& url) {event_info_.m_ImageURL = url;}

private:
    time_t                      start_time_;
    time_t                      duration_;

    time_t                      margin_before_;
    time_t                      margin_after_;

    dvblink::channel_id_t                channel_;
    dvblink::epg_event_id_t              epg_event_id_;
    dvblink::engine::DLEPGEvent          event_info_;
    bool                        conflict_;     //used for get operation only. not persistent.
    int priority_;
};

typedef std::vector<recording_timer>     recording_timers_list_t;
typedef std::vector<timer>               cancelled_timers_list_t;
typedef std::map<dvblink::timer_id_t, dvblink::timer_id_t> timers_id_map_t;

class recording_epg_timer_map
{
public:
    recording_epg_timer_map(const recording_timers_list_t& timer_list);
    ~recording_epg_timer_map() {}
    
    const recording_timer* find_timer(const dvblink::channel_id_t& channel, std::string& epg_event_id);
    
protected:
    dvblink::epg_event_id_t make_id(const dvblink::channel_id_t& channel, const dvblink::epg_event_id_t& epg_event_id);

protected:
    typedef std::map<dvblink::epg_event_id_t, recording_timer> recordings_map_t;
    recordings_map_t recordings_map_;
};

typedef std::list<recording_timer> timeline_timer_list_t;
typedef std::map<dvblink::device_id_t, timeline_timer_list_t> map_tuner_timeline_t;

inline bool operator == (const dvblink::timer_id_t& ltimer, const dvblink::timer_id_t& rtimer)
{
    return boost::iequals(ltimer.get(), rtimer.get());
}

inline bool operator == (const recording_timer& rec, const dvblink::timer_id_t& rtimer)
{
    return boost::iequals(rec.timer_id().get(), rtimer.get());
}

inline bool operator == (const recording_timer& lrec, const recording_timer& rrec)
{
    return boost::iequals(lrec.timer_id().get(), rrec.timer_id().get());
}

inline bool operator == (const timer& lrec, const timer& rrec)
{
    return boost::iequals(lrec.timer_id().get(), rrec.timer_id().get());
}

template <typename T>
inline void get_map_from_timers_list(T& timer_list, timers_id_map_t& timer_map)
{
    timer_map.clear();
    typename T::iterator it = timer_list.begin();
    while (it != timer_list.end())
    {
        timer_map[it->timer_id()] = it->timer_id();
        ++it;
    }
}

} //recorder
} //dvblex
