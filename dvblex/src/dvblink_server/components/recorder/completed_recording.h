/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_strings.h>
#include <dl_filesystem_path.h>
#include "timers.h"

namespace dvblex { namespace recorder {

enum completed_recording_state_e
{
    creInProgress = 0,
    creCompleted = 1,
    creError = 2,
    creForcedToCompletion = 3
};

class completed_recording : public timer
{
public:
    completed_recording() {}
    ~completed_recording() {}
    
    dvblink::engine::DLEPGEvent& event_info() {return event_info_;}
    completed_recording_state_e state() const {return state_;}
    const dvblink::filesystem_path_t& filename() const {return filename_;}
    const logical_channel_desc& channel() const {return channel_;}

    void set(char** values, std::map<std::string, int>& value_map);
    void set(const recording_timer& rec_timer);

    void set_epg_info(const dvblink::engine::DLEPGEvent& event) {event_info_ = event;}

    void set_state(completed_recording_state_e state) {state_ = state;}
    void set_event_info(const dvblink::engine::DLEPGEvent& info) {event_info_ = info;}
    void set_filename(const dvblink::filesystem_path_t& filename) {filename_ = filename;}
    void set_channel(const logical_channel_desc& channel) {channel_ = channel;}

private:
    dvblink::filesystem_path_t filename_;
    completed_recording_state_e state_;
    dvblink::engine::DLEPGEvent event_info_;
	logical_channel_desc channel_;
};

typedef std::vector<completed_recording> completed_recordings_list_t;

} //recorder
} //dvblex
