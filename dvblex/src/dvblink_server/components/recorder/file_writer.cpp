/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_file_procedures.h>
#include "file_writer.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

namespace dvblex { namespace recorder {

file_writer_t::file_writer_t(const std::string& id, dvblink::filesystem_path_t& filename, int rel_prio) :
    streamer_t(id),
    f_(NULL),
    filename_(filename),
    priority_(rel_prio),
    deleted_(false)
{
}

file_writer_t::~file_writer_t()
{
}

void file_writer_t::notify_streamer_deleted()
{
    streamer_t::notify_streamer_deleted();

    if (f_ != NULL)
        fclose(f_);

    deleted_ = true;
}

bool file_writer_t::start(streamer_callbacks_t* cb)
{
    f_ = filesystem::universal_open_file(filename_, "w+b");

    if (!f_)
	{
		log_error(L"file_writer_t::start. Cannot create output file %1%") % filename_.get();
        return false;
	}

    return dvblink::streamer_t::start(cb);
}

void file_writer_t::write_stream(const unsigned char* data, size_t len)
{
    fwrite(data, len, 1, f_);
}

} //recorder
} //dvblex
