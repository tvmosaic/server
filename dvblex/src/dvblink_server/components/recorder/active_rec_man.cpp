/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include "active_rec_man.h"
#include "recorder_database.h"
#include "recorder_engine_impl.h"
#include "timeline_builder.h"


using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

namespace dvblex { namespace recorder {

void active_rec_man::add_rec(const device_id_t& src_id, active_recording_t& rec)
{
    active_recordings_map_t::iterator it = active_recordings_.find(src_id);
    if (it == active_recordings_.end())
    {
        active_recordings_[src_id] = active_recordings_list_t();
        it = active_recordings_.find(src_id);
    }
    active_recordings_list_t& list = it->second;
    list.push_back(rec);
}

void active_rec_man::process_now_timers(const map_tuner_timeline_t& now_map)
{
    map_tuner_timeline_t::const_iterator it_map = now_map.begin();
    while (it_map != now_map.end())
    {
        const device_id_t& source_id = it_map->first;
        const timeline_timer_list_t& timer_list = it_map->second;
        process_now_timers(source_id, timer_list);
        ++it_map;
    }
}

static active_recording_t get_active_recording_from_timer(const active_recordings_list_t& active_rec_list, recording_timer& timer)
{
    active_recording_t ret_val;
    active_recordings_list_t::const_iterator it_active_list = active_rec_list.begin();
    while (it_active_list != active_rec_list.end())
    {
        const active_recording_t& active_rec = *it_active_list;
        if (active_rec->get_timer() == timer)
        {
            ret_val = active_rec;
            break;
        }

        ++it_active_list;
    }
    return ret_val;
}

void active_rec_man::process_now_timers(const device_id_t& src_id, const timeline_timer_list_t& now_timer_list)
{
    timeline_timer_list_t timer_list = now_timer_list;
    //first of all - remove all now timers, which are already active, from the now timer list
    if (now_timer_list.size() > 0)
    {
        if (active_recordings_.find(src_id) != active_recordings_.end())
        {
            active_recordings_list_t active_rec_list = active_recordings_[src_id];
            
            active_recordings_list_t::iterator it_active_list = active_rec_list.begin();
            while (it_active_list != active_rec_list.end())
            {
                active_recording_t& active_rec = *it_active_list;

                // check if timer is in active list already
                timeline_timer_list_t::iterator it_timer = timer_list.begin();
                while (it_timer != timer_list.end())
                {
                    recording_timer& now_timer = *it_timer;
                    if (active_rec->get_timer() == now_timer)
                    {
                        it_timer = timer_list.erase(it_timer);
                    } else
                    {
                        ++it_timer;
                    }
                }
            
                ++it_active_list;
            }
        }
    }

    if (timer_list.size() > 0)
    {
        time_t now;
        time(&now);
        
        timeline_timer_list_t::iterator it_now_timer = timer_list.begin();
        while (it_now_timer != timer_list.end())
        {
            //start this timer already (taking into account before margin)?
            if (now >= it_now_timer->real_start_time())
            {
                const active_recordings_list_t& active_rec_list = active_recordings_[src_id];
                timeline_timer_list_t active_timeline_timer_list;
                convert(active_rec_list, active_timeline_timer_list);

                //if there are no conflicts with the currently ongoing recordings - start a new recording
                recording_timers_list_t conflict_timer_list;
                if (!timeline_builder::is_conflicted(src_id, active_timeline_timer_list, *it_now_timer, channel_set_container_, &conflict_timer_list))
                {
                    parent_->start_active_recording(src_id, &*it_now_timer);
                } else
                {
                    //split conflicting timers into lower/equal/higher priority
                    recording_timers_list_t higher_prio_timers;
                    recording_timers_list_t equal_prio_timers;
                    recording_timers_list_t lower_prio_timers;
                    for (size_t i=0; i<conflict_timer_list.size(); i++)
                    {
                        recording_timer& tmr = conflict_timer_list.at(i);
                        if (tmr.get_priority() > it_now_timer->get_priority())
                            higher_prio_timers.push_back(tmr);
                        else if (tmr.get_priority() < it_now_timer->get_priority())
                            lower_prio_timers.push_back(tmr);
                        else
                            equal_prio_timers.push_back(tmr);
                    }

                    //if there are higher priority timers, do not start the new one under any circumstances
                    if (higher_prio_timers.size() == 0)
                    {
                        bool can_start = true;
                        //if there are conflicting timer with the same priority - respect the before margin
                        if (equal_prio_timers.size() != 0)
                        {
                            //should this recording start even without before margin?
                            if (now >= it_now_timer->main_start_time())
                            {
                                //check if conflicting active recordings are in the post-recording/completed/error phase
                                recording_timers_list_t::iterator conflict_it = equal_prio_timers.begin();
                                while (conflict_it != equal_prio_timers.end())
                                {
                                    active_recording_t active_rec = get_active_recording_from_timer(active_rec_list, *conflict_it);
                                    if (active_rec.get())
                                    {
                                        active_recording_state_e state = active_rec->get_state();
                                        if (state != rcsPostRecording && state != rcsCompleted && state != rcsError && state != rcsInterrupted)
                                        {
                                            can_start = false;
                                            break;
                                        }
                                    }
                                    ++conflict_it;
                                }
                            }
                        }
                        //start new recording by completing all other conflicting recordings
                        if (can_start)
                        {
                            recording_timers_list_t::iterator conflict_it = conflict_timer_list.begin();
                            while (conflict_it != conflict_timer_list.end())
                            {
                                active_recording_t active_rec = get_active_recording_from_timer(active_rec_list, *conflict_it);
                                if (active_rec.get())
                                {
                                    parent_->complete_active_recording(src_id, active_rec);
                                }
                                ++conflict_it;
                            }
                            parent_->start_active_recording(src_id, &*it_now_timer);
                        }

                    }
                }
            }
            ++it_now_timer;
        }
    }
}

void active_rec_man::convert(const active_recordings_list_t& list, timeline_timer_list_t& timer_list)
{
    active_recordings_list_t::const_iterator it_active = list.begin();
    while (it_active != list.end())
    {
        active_recording_t rec = *it_active;
        timer_list.push_back(rec->get_timer());
        ++it_active;
    }
}

void active_rec_man::update_state()
{
    vec_ts_sources_t sources;
    channel_set_container_.get_ts_sources(sources);

    active_recordings_map_t::iterator it_rec = active_recordings_.begin();
    while (it_rec != active_recordings_.end())
    {
        const device_id_t& source_id = it_rec->first;
        active_recordings_list_t& rec_list = it_rec->second;
        active_recordings_list_t::iterator it_list = rec_list.begin();

        bool exist = (std::find(sources.begin(), sources.end(), source_id) != sources.end());
        while (it_list != rec_list.end())
        {
            active_recording_t& rec = *it_list;
            rec->update_state();

            //update thumbnail
            update_recording_thumbnail(rec);
            //check for error or completed state            
            if (rec->is_completed() || !exist)
            {
                parent_->complete_active_recording(source_id, rec);
                it_list = rec_list.begin();
            }
            else
            {
                ++it_list;
            }
        }
        ++it_rec;
    }
}

void active_rec_man::update_recording_thumbnail(active_recording_t& recording)
{
    std::wstring thumb_path;
    if (recording->get_thumb_path(thumb_path))
    {
        //update recording in database
        if (!recorder_db_.update_thumbnail(recording->get_timer().timer_id(), thumb_path))
        {
            log_error(L"recorder_timeline_t::update_active_recording_thumbnail: failed to save thumbnail for timer %1%") % string_cast<EC_UTF8>(recording->get_timer().timer_id().get());
        }
        //invalidate thumbnail
        recording->invalidate_thumb();
    }
}

size_t active_rec_man::get_quantity() const
{
    size_t res = 0;
    active_recordings_map_t::const_iterator it = active_recordings_.begin();
    while (it != active_recordings_.end())
    {
        const active_recordings_list_t& active_rec_list = it->second;
        res += active_rec_list.size();
        ++it;
    }
    return res;
}

void active_rec_man::complete_active_recording(const device_id_t& src_id, active_recording_t& recording)
{
    active_recording_t rec = recording;
    const recording_timer& timer = rec->get_timer();
    active_recording_state_e state = rec->get_state();

    log_info(L"Completing recording for timer %1%, started at %2% on channel %3%. Recording state %4%") %
        string_cast<EC_UTF8>(timer.timer_id().get()) % timer.main_start_time() % string_cast<EC_UTF8>(timer.channel().get()) % state;

    //write info about completed recording to database
    completed_recording_state_e crs;
    switch (state)
    {
    case rcsError:
        crs = creError;
        break;
    case rcsCompleted:
        crs = creCompleted;
        break;
    default:
        crs = creForcedToCompletion;
        break;
    }

    //cancel/stop recording if it is still ongoing
    rec->cancel_recording();
    //update recording thumbnail (if it was not updated before)
    update_recording_thumbnail(rec);

    time_t now; time(&now);
    recorder_db_.update_completed_recording(
        timer.timer_id(), rec->get_actual_start() == -1 ? now : rec->get_actual_start(), rec->get_actual_stop() == -1 ? now : rec->get_actual_stop(), crs);

    remove_rec(src_id, rec);
}

void active_rec_man::remove_rec(const device_id_t& src_id, const active_recording_t& rec)
{
    active_recordings_map_t::iterator it_map = active_recordings_.find(src_id);
    if (it_map != active_recordings_.end())
    {
        active_recordings_list_t& rec_list = it_map->second;
        active_recordings_list_t::iterator it_rec = rec_list.begin();
        while (it_rec != rec_list.end())
        {
            active_recording_t& cur = *it_rec;
            if (cur && cur->get_timer() == rec->get_timer())
            {
                rec_list.erase(it_rec);
                break;
            }
            ++it_rec;
        }
        ++it_map;
    }
}

void active_rec_man::cancel_timer(const timer_id_t& timer_id)
{
    //check active timers
    active_recordings_map_t::iterator it_map = active_recordings_.begin();
    while (it_map != active_recordings_.end())
    {
        const device_id_t& source_id = it_map->first;
        active_recordings_list_t& rec_list = it_map->second;
        active_recordings_list_t::iterator it_rec = rec_list.begin();
        while (it_rec != rec_list.end())
        {
            active_recording_t& rec = *it_rec;
            if (rec && rec->get_timer() == timer_id)
            {
                complete_active_recording(source_id, rec);
                it_rec = rec_list.begin();
            }
            else
            {
                ++it_rec;
            }
        }
        ++it_map;
    }    
}

void active_rec_man::cancel_all_recs()
{
    active_recordings_map_t::iterator it_map = active_recordings_.begin();
    while (it_map != active_recordings_.end())
    {
        active_recordings_list_t& rec_list = it_map->second;
        active_recordings_list_t::iterator it_rec = rec_list.begin();
        while (it_rec != rec_list.end())
        {
            active_recording_t& rec = *it_rec;
            if (rec)
            {
                rec->cancel_recording();
            }
            ++it_rec;
        }
        ++it_map;
    }
}

void active_rec_man::complete_all_recs()
{
    //complete all active recordings on exit
    active_recordings_map_t::iterator it_map = active_recordings_.begin();
    while (it_map != active_recordings_.end())
    {
        const device_id_t& source_id = it_map->first;
        active_recordings_list_t& rec_list = it_map->second;
        active_recordings_list_t::iterator it_rec = rec_list.begin();
        while (it_rec != rec_list.end())
        {
            active_recording_t& rec = *it_rec;
            parent_->complete_active_recording(source_id, rec, false); //do not delete schedule as we force recording to completion here and it may resume later
            it_rec = rec_list.begin();
        }
        ++it_map;
    }
    active_recordings_.clear();
}

void active_rec_man::remove_schedule(const schedule_item_id_t& schedule_id)
{
    active_recordings_map_t::iterator it_map = active_recordings_.begin();
    while (it_map != active_recordings_.end())
    {
        const device_id_t& source_id = it_map->first;
        active_recordings_list_t& rec_list = it_map->second;
        active_recordings_list_t::iterator it_rec = rec_list.begin();
        while (it_rec != rec_list.end())
        {
            active_recording_t& rec = *it_rec;
            if (rec->get_timer().schedule_item_id() == schedule_id)
            {
                parent_->complete_active_recording(source_id, rec);
                it_rec = rec_list.begin();
            }
            else
            {
                ++it_rec;
            }
        }
        ++it_map;
    }
}

void active_rec_man::get_active_timers(recording_timers_list_t& timer_list) const
{
    timer_list.clear();
    active_recordings_map_t::const_iterator rec_it = active_recordings_.begin();
    while (rec_it != active_recordings_.end())
    {
        const active_recordings_list_t& tuner_list = rec_it->second;
        active_recordings_list_t::const_iterator it_list = tuner_list.begin();
        while (it_list != tuner_list.end())
        {
            active_recording_t rec = *it_list;
            if (rec)
            {
                timer_list.push_back(rec->get_timer());
            }
            ++it_list;
        }
        ++rec_it;
    }
}

} //recorder
} //dvblex
