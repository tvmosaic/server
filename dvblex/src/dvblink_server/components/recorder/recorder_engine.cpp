/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_common.h>
#include <dl_message_server.h>
#include <dl_message_channels.h>
#include <dl_message_addresses.h>
#include <dl_installation_settings.h>
#include "recorder_engine.h"
#include "constants.h"

using namespace dvblex;
using namespace dvblex::settings;
using namespace dvblink; 
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;

namespace dvblex { namespace recorder {

static std::wstring __cmd_name[] =
{
    L"recAddSchedule",
    L"recGetSchedules",
    L"recUpdateSchedule",
    L"recDeleteSchedule",
    L"recGetTimers",
    L"recDeleteTimer",
    L"recForceUpdate",
    L"recBrowseEPG",
    L"recGetEPGEvent",
    L"recGetCompletedRecordings",
    L"recDeleteCompletedRecording",
    L"recUpdateChannelsEpg",
    L"recGetSettings",
    L"recSetSettings",
    L"recGetScheduledTimersCSV",
    L"recSwitchToActive",
    L"recSwitchToIdle",
    L"recRepairDatabase"
};

std::wstring recorder_engine::get_cmd_name(recorder_cmd_e cmd)
{
    if (static_cast<size_t>(cmd) > sizeof(__cmd_name) / sizeof(__cmd_name[0]))
        return L"Unknown recorder command";
    return __cmd_name[cmd];
}

recorder_engine::recorder_engine() :
    recordings_check_thread_(NULL),
    recorder_thread_(NULL),
    recorder_engine_impl_(NULL),
    epg_updater_(NULL),
    last_epg_update_time_(0),
    mode_(rem_unknown)
{
}

bool recorder_engine::run(dvblink::i_server_t server, messaging::message_queue_t message_queue)
{
    bool res = false;
    server_ = server;
    message_queue_ = message_queue;

    if (!recorder_thread_)
    {
        installation_settings_t install_settings;
        install_settings.init(message_queue_);
        //read settings
        if (recorder_settings_.init(install_settings))
        {
            //open recorder database
            filesystem_path_t record_db_path = recorder_settings_.get_record_db_path();
            if (recorder_database_.init(record_db_path, &recorder_settings_))
            {
                stop_flag_ = false;
                recorder_thread_ = new boost::thread(boost::bind(&recorder_engine::recorder_thread_func, this));
                
                if (recorder_settings_.check_deleted_recordings())
                    start_deleted_rec_check_thread();
                    
                res = true;
            }
            else
            {
                log_error(L"Error initializing DVBLink recorder database");
            }
        }
        else
        {
            log_error(L"Error reading DVBLink recorder settings");
        }
    }
    return res;
}

void recorder_engine::standby()
{
    if (recorder_thread_)
    {
        rec_switch_to_idle cmd_info;
        command_queue_.ExecuteCommand(recSwitchToIdle, &cmd_info);
    }
}

void recorder_engine::resume()
{
    if (recorder_thread_)
    {
        rec_switch_to_active cmd_info;
        command_queue_.ExecuteCommand(recSwitchToActive, &cmd_info);
    }
}

void recorder_engine::stop()
{
    stop_deleted_rec_check_thread();
    
    if (recorder_thread_)
    {
        stop_flag_ = true;

        recorder_thread_->join();
        delete recorder_thread_;
        recorder_thread_ = NULL;

        recorder_settings_.term();
        recorder_database_.term();
    }
}

void recorder_engine::start_deleted_rec_check_thread()
{
    if (recordings_check_thread_ == NULL)
    {
        recordings_check_stop_flag_ = false;
        recordings_check_thread_ = new boost::thread(boost::bind(&recorder_engine::recordings_check_func, this));        
    }
}

void recorder_engine::stop_deleted_rec_check_thread()
{
    if (recordings_check_thread_ != NULL)
    {
        recordings_check_stop_flag_ = true;
        recordings_check_thread_->join();
        delete recordings_check_thread_;
        recordings_check_thread_ = NULL;
    }
}

void recorder_engine::recordings_check_func()
{
    size_t scan_repeat_period_sec = 30 * 60;    //every 30 minutes
    size_t first_scan_delay_sec = 5 * 60;       //5 minutes

    boost::posix_time::ptime next_scan_time = boost::posix_time::second_clock::local_time() + boost::posix_time::seconds(first_scan_delay_sec);
    while (!recordings_check_stop_flag_)
    {
        if (next_scan_time <= boost::posix_time::second_clock::local_time())
        {
            //scan time!
            log_ext_info(L"recorder_engine::recordings_check_func: starting scan for non-existent recording files");
            completed_recordings_list_t recordings;
            if (get_completed_recordings(recordings))
            {
                for (size_t i = 0; i < recordings.size() && !recordings_check_stop_flag_; i++)
                {
                    //do not process recordings that are still in progress
                    if (recordings[i].state() != creInProgress)
                    {
                        if (!boost::filesystem::exists(recordings[i].filename().to_boost_filesystem()))
                        {
                            log_info(L"recorder_engine::recordings_check_func. Recording file (%1%) does not exist for recording %2%. Deleting recording from db") %
                                recordings[i].filename().get() % string_cast<EC_UTF8>(recordings[i].timer_id().get());
                            delete_completed_recording(recordings[i].timer_id());
                        }
                    }
                }
            }

            log_ext_info(L"recorder_engine::recordings_check_func: starting scan for deleted schedules without recordings");
            schedule_list_t schedule_list;
            if (get_schedules(schedule_list, false))
            {
                for (size_t i = 0; i < schedule_list.size(); i++)
                {
                    if (schedule_list[i].disabled())
                    {
                        completed_recordings_list_t recordings;
                        bool b = get_completed_recordings(recordings, schedule_list[i].schedule_item_id());
                        if (!b || recordings.size() == 0)
                        {
                            //remove this schedule
                            log_info(L"recorder_engine::recordings_check_func. Deleted schedule (%1%) does not have any recordings. Removing recording from db") % schedule_list[i].schedule_item_id();
                            delete_schedule(schedule_list[i].schedule_item_id(), true);
                        }
                    }
                }
            }

            next_scan_time = boost::posix_time::second_clock::local_time() + boost::posix_time::seconds(scan_repeat_period_sec);
        }
    	boost::this_thread::sleep(boost::posix_time::milliseconds(100));
    }
}

bool recorder_engine::browse_epg(server_channel_list_t& channel_list, int time_from, int time_to,
    std::string& keyphrase, unsigned long genre_mask, epg_level_e info_level, boost::int32_t max_count, epg_ex_channel_info_map_t& epg_channel_events)
{
    rec_browse_epg_info cmd_info;
    cmd_info.channel_list_ = &channel_list;
    cmd_info.time_from_ = time_from;
    cmd_info.time_to_ = time_to;
    cmd_info.keyphrase_ = &keyphrase;
    cmd_info.genre_mask_ = genre_mask;
	cmd_info.info_level_ = info_level;
    cmd_info.event_list_ = &epg_channel_events;
    cmd_info.max_count_ = max_count;
    command_queue_.ExecuteCommand(recBrowseEPG, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::get_epg_event(channel_id_t channel_id, epg_event_id_t& event_id, epg_level_e info_level, epg_item_ex& epg_item)
{
    rec_get_epg_event cmd_info;
    cmd_info.channel_id_ = &channel_id;
    cmd_info.event_id_ = &event_id;
	cmd_info.info_level_ = info_level;
    cmd_info.epg_item_ = &epg_item;
    command_queue_.ExecuteCommand(recGetEPGEvent, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::add_schedule(schedule_item& schedule_item, recording_timers_list_t* conflict_list)
{
    rec_add_schedule_info cmd_info;
    cmd_info.schedule_item_ = &schedule_item;
    cmd_info.conflict_list_ = conflict_list;
    command_queue_.ExecuteCommand(recAddSchedule, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::get_schedules(schedule_list_t& schedule_list, bool active_only /*= true*/)
{
    rec_get_schedules_info cmd_info;
    cmd_info.schedule_list_ = &schedule_list;
    cmd_info.active_only_ = active_only;
    command_queue_.ExecuteCommand(recGetSchedules, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::update_schedule(update_schedule_item_info& update_info)
{
    rec_update_schedule_info cmd_info;
    cmd_info.update_info_ = &update_info;
    command_queue_.ExecuteCommand(recUpdateSchedule, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::delete_schedule(const schedule_item_id_t& schedule_item_id, bool delete_from_db /*= false*/)
{
    rec_delete_schedule_info cmd_info;
    cmd_info.schedule_item_id_ = schedule_item_id;
    cmd_info.delete_from_db_ = delete_from_db;
    command_queue_.ExecuteCommand(recDeleteSchedule, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::get_timers(recording_timers_list_t& scheduled_timer_list, recording_timers_list_t& active_timer_list, recording_timers_list_t& cancelled_timer_list)
{
    rec_get_timers_info cmd_info;
    cmd_info.scheduled_timer_list_ = &scheduled_timer_list;
    cmd_info.active_timer_list_ = &active_timer_list;
	cmd_info.cancelled_timer_list_ = &cancelled_timer_list;
    command_queue_.ExecuteCommand(recGetTimers, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::delete_timer(timer_id_t& timer_id)
{
    rec_delete_timer_info cmd_info;
    cmd_info.timer_id_ = &timer_id;
    command_queue_.ExecuteCommand(recDeleteTimer, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::force_update()
{
    rec_force_update_info cmd_info;
    command_queue_.ExecuteCommand(recForceUpdate, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::get_completed_recordings(completed_recordings_list_t& completed_recordings_list, const schedule_item_id_t& schedule_item_id /* = DL_RECORDER_INVALID_SCHEDULE_ID*/)
{
    rec_get_completed_recordings_info cmd_info;
    cmd_info.completed_recordings_list_ = &completed_recordings_list;
    cmd_info.schedule_item_id_ = schedule_item_id;
    command_queue_.ExecuteCommand(recGetCompletedRecordings, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::delete_completed_recording(const timer_id_t& timer_id)
{
    rec_delete_completed_recordings_info cmd_info(timer_id);
    command_queue_.ExecuteCommand(recDeleteCompletedRecording, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::update_channels_epg(const std::vector<channel_id_t>& channel_list)
{
    if (stop_flag_)
        return false;

    rec_update_channels_epg_info cmd_info;
    cmd_info.channel_list_ = &channel_list;
    command_queue_.ExecuteCommand(recUpdateChannelsEpg, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::get_settings(recorder_settings& settings)
{
    rec_get_settings_info cmd_info;
    cmd_info.settings_ = &settings;
    command_queue_.ExecuteCommand(recGetSettings, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::set_settings(recorder_settings& settings)
{
    rec_set_settings_info cmd_info;
    cmd_info.settings_ = &settings;
    command_queue_.ExecuteCommand(recSetSettings, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::get_scheduls_in_csv(csv_string_t& csv)
{
    rec_scheduled_timers_in_csv cmd_info;
    cmd_info.csv_timers_ = &csv;
    command_queue_.ExecuteCommand(recGetScheduledTimersCSV, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::repair_database()
{
    repair_db_info cmd_info;
    command_queue_.ExecuteCommand(recRepairDatabase, &cmd_info);
    return cmd_info.result_;
}

bool recorder_engine::get_last_epg_update_time(time_t& last_update)
{
    last_update = last_epg_update_time_;
    return true;
}

void recorder_engine::recorder_thread_func()
{
    log_info(L"recorder_engine::recorder_thread_func(). Started.");

    //always start in active mode
    mode_ = rem_active;
    
    while (!stop_flag_)
    {
        switch (mode_)
        {
            case rem_active:
                run_active();
                break;
            case rem_idle:
                run_idle();
                break;
            default:
                break;
        }
    }
    log_info(L"recorder_engine::recorder_thread_func(). Finished.");
}

void recorder_engine::run_idle()
{
    log_info(L"recorder_engine::run_idle(). Switched to idle mode");

    while (!stop_flag_ && mode_ == rem_idle)
    {
        //check command queue
        SDLCommandItem* item;
        while (command_queue_.PeekCommand(&item))
        {
            recorder_cmd_e cmd = static_cast<recorder_cmd_e>(item->id);
            log_ext_info(L"recorder_engine::run_idle: PeekCommand: %1%") % get_cmd_name(cmd);
            switch (cmd)
            {
            case recSwitchToActive:
                {
                    mode_ = rem_active;
                }
                break;
            default:
                {
                    rec_cmd_base* params = (rec_cmd_base*)item->param;
                    params->result_ = false;
                }
                break;
            }
            command_queue_.FinishCommand(&item);
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(2));
    }
}

void recorder_engine::run_active()
{
    log_info(L"recorder_engine::run_active(). Switched to active mode");
    
	time_t now; time(&now);
	time_t prev_process_time = now;
    time_t epg_update_time = now + DL_RECORDER_INITIAL_EPG_SCAN_DELAY_SEC;

    recorder_engine_impl_ = new recorder_engine_impl(recorder_settings_, recorder_database_, server_, message_queue_);

	//clean-up database
	recorder_database_.complete_pending_completed_recordings();

    //wait until dvblink server is operational
    log_info(L"recorder_engine::run_active(). Waiting for server to become operational");
    while (!stop_flag_)
    {
        if (check_server_connection())
        {
            log_info(L"recorder_engine::run_active(). Server is operational. Continue with rescheduling");
            break;
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(200));
    }

	//do reschedule on start. that will clear b_reschedule_requested_ flag. EPG update will follow later    
    reschedule();

    b_full_epg_update_ = true; //set it to true to force the first update after start
    while (!stop_flag_ && mode_ == rem_active)
    {
        time(&now);

        //check whether epg updater has finished
        if (epg_updater_ != NULL && epg_updater_->is_finished())
        {
            delete epg_updater_;
            epg_updater_ = NULL;
            //update last epg update timestamp
            time(&last_epg_update_time_);
        }

        //check whether it is time to check for epg update
        if (now >= epg_update_time)
        {
            //if there are things to update
            if (b_full_epg_update_ || epg_update_channels_.size() > 0)
            {
                if (b_full_epg_update_)
                {
                    //if there is fill update pending and epg update is still ongoing - stop it
                    if (epg_updater_ != NULL)
                    {
                        delete epg_updater_;
                        epg_updater_ = NULL;
                    }
                    //if there specific channels to update in the map - clear it because full update will update them all
                    epg_update_channels_.clear();
                }
                //do new epg update only if previous update was finished or cancelled
                if (epg_updater_ == NULL)
                {
                    reschedule_event_.reset();
                    epg_updater_ = new epg_updater(recorder_database_, message_queue_, reschedule_event_, epg_update_channels_); //epg_update_channels_.size() == 0 means full update
                    //reset update flags
                    b_full_epg_update_ = false;
                    epg_update_channels_.clear();
                }
            }
            epg_update_time = now + DL_RECORDER_REPEAT_EPG_SCAN_DELAY_SEC;
        }
        //if reschedule event is signalled - it means that epg updater has finished its job
        //do reschedule now
        if (reschedule_event_.is_signaled())
        {
            reschedule();
            reschedule_event_.reset();
        }
        
        if (now - prev_process_time >= DL_RECORDER_PROCESS_PERIOD_SEC)
        {
            recorder_engine_impl_->process_timers();
            prev_process_time = now;
        }
        
        //check command queue
        SDLCommandItem* item;
        while (command_queue_.PeekCommand(&item))
        {
            recorder_cmd_e cmd = static_cast<recorder_cmd_e>(item->id);
            log_ext_info(L"recorder_engine::run_active: PeekCommand: %1%") % get_cmd_name(cmd);
            switch (cmd)
            {
            case recAddSchedule:
                {
                    rec_add_schedule_info* params = (rec_add_schedule_info*)item->param;
                    params->result_ = int_add_schedule(params->schedule_item_, params->conflict_list_);
                }
                break;
            case recGetSchedules:
                {
                    rec_get_schedules_info* params = (rec_get_schedules_info*)item->param;
                    params->result_ = int_get_schedules(params->schedule_list_, params->active_only_);
                }
                break;
            case recUpdateSchedule:
                {
                    rec_update_schedule_info* params = (rec_update_schedule_info*)item->param;
                    params->result_ = int_update_schedule(params->update_info_);
                }
                break;
            case recDeleteSchedule:
                {
                    rec_delete_schedule_info* params = (rec_delete_schedule_info*)item->param;
                    params->result_ = int_delete_schedule(params->schedule_item_id_, params->delete_from_db_);
                }
                break;
            case recGetTimers:
                {
                    rec_get_timers_info* params = (rec_get_timers_info*)item->param;
					params->result_ = int_get_timers(params->scheduled_timer_list_, params->active_timer_list_, params->cancelled_timer_list_);
                }
                break;
            case recDeleteTimer:
                {
                    rec_delete_timer_info* params = (rec_delete_timer_info*)item->param;
                    params->result_ = int_delete_timer(params->timer_id_);
                }
                break;
            case recGetCompletedRecordings:
                {
                    rec_get_completed_recordings_info* params = (rec_get_completed_recordings_info*)item->param;
                    params->result_ = int_get_completed_recordings(params->completed_recordings_list_, params->schedule_item_id_);
                }
                break;
            case recDeleteCompletedRecording:
                {
                    rec_delete_completed_recordings_info* params = (rec_delete_completed_recordings_info*)item->param;
                    params->result_ = int_delete_completed_recording(params->timer_id_);
                }
                break;
            case recForceUpdate:
                {
                    rec_force_update_info* params = (rec_force_update_info*)item->param;
                    params->result_ = int_force_update();
                }
                break;
            case recBrowseEPG:
                {
                    rec_browse_epg_info* params = (rec_browse_epg_info*)item->param;
                    params->result_ = int_browse_epg(params->channel_list_, params->time_from_, params->time_to_, params->keyphrase_,
						params->genre_mask_, params->info_level_, params->max_count_, params->event_list_);
                }
                break;
            case recGetEPGEvent:
                {
                    rec_get_epg_event* params = (rec_get_epg_event*)item->param;
					params->result_ = int_get_epg_event(params->channel_id_, params->event_id_, params->info_level_, params->epg_item_);
                }
                break;
            case recUpdateChannelsEpg:
                {
                    rec_update_channels_epg_info* params = (rec_update_channels_epg_info*)item->param;
					params->result_ = int_update_channels_epg(params->channel_list_);
                }
                break;
            case recGetSettings:
                {
                    rec_get_settings_info* params = (rec_get_settings_info*)item->param;
					params->result_ = int_get_settings(params->settings_);
                }
                break;
            case recSetSettings:
                {
                    rec_set_settings_info* params = (rec_set_settings_info*)item->param;
					params->result_ = int_set_settings(params->settings_);
                }
                break;
            case recGetScheduledTimersCSV:
                {
                    rec_scheduled_timers_in_csv* params = (rec_scheduled_timers_in_csv*)item->param;
                    params->result_ = int_get_scheduled_timers_in_csv(*params->csv_timers_);
                }
                break;
            case recRepairDatabase:
                {
                    repair_db_info* params = (repair_db_info*)item->param;
                    params->result_ = int_repair_database();
                }
                break;
            case recSwitchToIdle:
                {
                    mode_ = rem_idle;
                }
                break;
            default:
                break;
            }
            command_queue_.FinishCommand(&item);
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(2));
    }

    //stop epg updater if it is running
    if (epg_updater_ != NULL)
    {
        delete epg_updater_;
        epg_updater_ = NULL;
    }
    
    recorder_engine_impl_->term();
    delete recorder_engine_impl_;
    recorder_engine_impl_ = NULL;
}

bool recorder_engine::prepare_new_schedule(schedule_item& sch_item)
{
    bool res = true;
    //if this is schedule by EPG - prepare schedule item (e.g. find and serialize event info for the future reference)
    if (sch_item.is_epg_based())
    {
        res = false;
        epg_item eitem;
        if (recorder_database_.get_epg_item(sch_item.channel(), sch_item.epg_event_id(), eitem))
        {
            //serialize epg info into xml
            DLEPGEventList list;
            list.push_back(eitem.event_);

            std::string info = sch_item.epg_program_info();
            res = EPGWriteEventsToXML(list, info);
            sch_item.set_epg_program_info(info);
        }
        else
        {
            log_error(L"recorder_engine::prepare_new_schedule. Unable to get epg event %1% info (channel %2%) from database") %
                string_cast<EC_UTF8>(sch_item.epg_event_id().get()) % string_cast<EC_UTF8>(sch_item.channel().get());
        }
    }

    //set default new only flag
    if (recorder_settings_.get_new_only_default_value() != nodv_not_set)
        sch_item.record_series_new_only_ = recorder_settings_.get_new_only_default_value() == nodv_true ? true : false;

    //update default margins if needed
    if (sch_item.margin_before_ == DL_RECORDER_DEFAULT_MARGIN)
        sch_item.margin_before_ = recorder_settings_.get_before_margin();

    if (sch_item.margin_after_ == DL_RECORDER_DEFAULT_MARGIN)
        sch_item.margin_after_ = recorder_settings_.get_after_margin();

    return res;
}

bool recorder_engine::int_add_schedule(schedule_item* sch_item, recording_timers_list_t* conflict_list)
{
    bool res = false;
    
    //if reschedule has been requested, but not completed yet - do it now to update the channel lists and concurrency maps
    if (b_reschedule_requested_)
        reschedule();
    
    if (prepare_new_schedule(*sch_item))
    {
        if (recorder_engine_impl_->add_schedule(*sch_item, conflict_list))
        {
            if (conflict_list == NULL || (conflict_list != NULL && conflict_list->size() == 0))
            {
                res = recorder_database_.add_schedule(*sch_item);
                //all new timers were inserted with the temporary schedule id (DL_RECORDER_INVALID_ID)
                //update them now to the actual ones
                if (res)
                {
                    recorder_engine_impl_->update_temp_timer_ids(sch_item->schedule_item_id());
                }
                else
                {
                    recorder_engine_impl_->remove_schedule(DL_RECORDER_INVALID_ID);
                }
            }
        }
    }
    return res;
}

bool recorder_engine::int_get_schedules(schedule_list_t* schedule_list, bool active_only /*=true*/)
{
    return recorder_database_.get_schedules(schedule_list, DL_RECORDER_INVALID_ID, active_only);
}

bool recorder_engine::int_update_schedule(update_schedule_item_info* update_info)
{
    bool res = false;

    //if reschedule has been requested, but not completed yet - do it now to update the channel lists and concurrency maps
    if (b_reschedule_requested_)
        reschedule();
    
    //adjust start before/after if needed
    if (!update_info->supports_v2_schedule_ && 
        (update_info->start_after_sec_ == DL_RECORDER_INVALID_START_MARGIN_VALUE || 
        update_info->start_before_sec_ == DL_RECORDER_INVALID_START_MARGIN_VALUE))
    {
        //it means that we have an old client (that knows only "record series anytime" setting and it is set to false
        //in this case we need either to preserve existing start before/after settings or generate default ones
        schedule_list_t schedule_list;
        if (recorder_database_.get_schedules(&schedule_list, update_info->schedule_item_id_) &&
            schedule_list.size() == 1)
        {
            schedule_item si = schedule_list.at(0);

            if (si.start_after_sec_ == -1 && si.start_before_sec_ == -1)
            {
                //generate default start before/after
                si.generate_default_start_margins();
            }
            update_info->start_after_sec_ = si.start_after_sec_;
            update_info->start_before_sec_ = si.start_before_sec_;
        } else
        {
            log_warning(L"recorder_engine::int_update_schedule. Unable to get original schedule (%1%) from database") % update_info->schedule_item_id_;
            update_info->start_after_sec_ = -1;
            update_info->start_before_sec_ = -1;
        }
    }

    if (recorder_database_.update_schedule(update_info))
    {
        reschedule();
        res = true;
    }
    return res;
}

bool recorder_engine::int_repair_database()
{
    server::common_item_file_info_request req(common_item_sqlite);
    server::common_item_file_info_response resp;
    message_queue_->send(server_message_queue_addressee, req, resp);

    recorder_database_.repair_database(resp.dir_);

#ifdef _ANDROID_ALL
    //on Android - because database was actually deleted - force epg rebuild
    int_force_update();
#endif

	return true;
}

bool recorder_engine::int_delete_schedule(const schedule_item_id_t& schedule_item_id_, bool delete_from_db)
{
    bool current_conflicts = recorder_engine_impl_->are_conflicts_present();
    
    recorder_engine_impl_->remove_schedule(schedule_item_id_, delete_from_db);
    
    //if there were timer conflicts before deleting schedule - do reschedule now. It may solve the conflicts
    if (current_conflicts)
        reschedule();
        
    return true;
}

bool recorder_engine::int_get_timers(recording_timers_list_t* scheduled_timer_list, recording_timers_list_t* active_timer_list, recording_timers_list_t* cancelled_timer_list)
{
    recorder_engine_impl_->get_scheduled_timers(*scheduled_timer_list);
    recorder_engine_impl_->get_active_timers(*active_timer_list);
    recorder_engine_impl_->get_cancelled_timers(*cancelled_timer_list);
    return true;
}

bool recorder_engine::int_delete_timer(timer_id_t* timer_id)
{
    bool res = false;
    //find timer
    recording_timer timer;
    if (recorder_engine_impl_->get_timer(*timer_id, timer))
    {
        //find schedule of this timer
        schedule_list_t schedule_list;
        if (recorder_database_.get_schedules(&schedule_list, timer.schedule_item_id()) && schedule_list.size() > 0)
        {
            bool current_conflicts = recorder_engine_impl_->are_conflicts_present();
            
            //check whether this is a recursive schedule
            if (schedule_list[0].is_recursive())
            {
                recorder_engine_impl_->cancel_timer(*timer_id);
            }
            else
            {
                //this is one shot timer - remove whole schedule
                recorder_engine_impl_->remove_schedule(timer.schedule_item_id());
                recorder_database_.disable_schedule(timer.schedule_item_id());
            }
            
            //if there were timer conflicts before deleting timer - do reschedule now. It may solve the conflicts
            if (current_conflicts)
                reschedule();
            
            res = true;
        }            
    }
    
    return res;
}

bool recorder_engine::int_get_completed_recordings(completed_recordings_list_t* completed_recordings_list, const schedule_item_id_t& schedule_item_id)
{
	return recorder_database_.get_completed_recording_for_schedule(schedule_item_id, *completed_recordings_list);
}

bool recorder_engine::int_delete_completed_recording(const timer_id_t& timer_id)
{
	return recorder_engine_impl_->delete_completed_recording(timer_id);
}

void recorder_engine::postprocess_epg_item_list(epg_channel_info_map_t& epg_channel_events, epg_ex_channel_info_map_t& ece)
{
	//get also schedules and timers - we need them to fill all information in
	schedule_list_t schedule_list;
	recording_timers_list_t scheduled_timer_list, active_timer_list, cancelled_timer_list;
	if (int_get_schedules(&schedule_list) &&
		int_get_timers(&scheduled_timer_list, &active_timer_list, &cancelled_timer_list))
	{
		schedule_map_t schedule_map;
		make_schedule_map_from_list(schedule_list, schedule_map);

		//make cancelled timer map
		timers_id_map_t cancelled_timer_map;
        
		get_map_from_timers_list(cancelled_timer_list, cancelled_timer_map);

		recording_epg_timer_map epg_timer_map(scheduled_timer_list);
		recording_epg_timer_map epg_cancelled_timer_map(cancelled_timer_list);

		//walk through all events and add them to the resulting map
		epg_channel_info_map_t::iterator ch_it = epg_channel_events.begin();
		while (ch_it != epg_channel_events.end())
		{
            ece.insert(std::make_pair(ch_it->first, epg_event_ex_list_t()));

			while (ch_it->second.size())
			{
                epg_item_ex item;
                item.from_item(ch_it->second[0]);

				const recording_timer* rec_timer = epg_timer_map.find_timer(ch_it->first, ch_it->second[0].event_.id_);
				//try cancelled timers of it is not present in the scheduled timers map
				if (rec_timer == NULL)
				{
					rec_timer = epg_cancelled_timer_map.find_timer(ch_it->first, ch_it->second[0].event_.id_);
				}

				item.is_record_ = (rec_timer != NULL) && (cancelled_timer_map.find(rec_timer->timer_id()) == cancelled_timer_map.end());
				item.is_record_conflict_ = (rec_timer != NULL) && item.is_record_ && rec_timer->conflict();
				if (rec_timer != NULL)
				{
					if (schedule_map.find(rec_timer->schedule_item_id()) != schedule_map.end())
					{
						item.is_repeat_record_ = schedule_map[rec_timer->schedule_item_id()].is_recursive();
					}
				}
                item.is_series_ = true;

                ece.find(ch_it->first)->second.push_back(item);

				ch_it->second.erase(ch_it->second.begin());
			}
			++ch_it;
		}
	}
}

bool recorder_engine::int_browse_epg(server_channel_list_t* channel_list, int time_from,
    int time_to, std::string* keyphrase, unsigned long genre_mask, epg_level_e info_level, boost::int32_t max_count, epg_ex_channel_info_map_t* ece)
{
    bool ret_val = false;
    epg_channel_info_map_t epg_channel_events;
    if (recorder_database_.browse_epg(*channel_list, time_from, time_to, *keyphrase, genre_mask, info_level, max_count, epg_channel_events))
    {
        postprocess_epg_item_list(epg_channel_events, *ece);
        ret_val = true;
    }
    return ret_val;
}

bool recorder_engine::int_get_epg_event(channel_id_t* channel_id, epg_event_id_t* event_id, epg_level_e info_level, epg_item_ex* ei)
{
    bool ret_val = false;
    epg_item item;
    if (recorder_database_.get_epg_item(*channel_id, *event_id, item))
    {
        epg_channel_info_map_t epg_channel_events;
        epg_channel_events[*channel_id] = epg_event_list_t();
        epg_channel_events[*channel_id].push_back(item);
        epg_ex_channel_info_map_t ece;
        postprocess_epg_item_list(epg_channel_events, ece);

        if (ece.size() > 0)
        {
            *ei = ece.begin()->second[0];
            ret_val = true;
        }
    }
    return ret_val;
}

bool recorder_engine::int_force_update()
{
    //the flag will be picked up the next update check sycle
    b_full_epg_update_ = true;
    //set also the flag that reschedule has been rquested (new/deleted channels/sources)
    b_reschedule_requested_ = true;
    //epg updater will trigger reschedule() when it finishes
    return true;
}

bool recorder_engine::int_update_channels_epg(const std::vector<channel_id_t>* channel_list)
{
    for (size_t i = 0; i < channel_list->size(); i++)
    {
        epg_update_channels_[channel_list->at(i)] = channel_list->at(i);
    }
    return true;
}

bool recorder_engine::int_get_scheduled_timers_in_csv(csv_string_t& schedule_csv)
{
    return recorder_engine_impl_->get_scheduled_timers_in_csv(schedule_csv);
}

bool recorder_engine::get_tuners_info(source_to_logical_map_t& map_source_TO_log_ch, ts_source_TO_set_of_set_of_channels_set_t& ch_set_data) const
{
    bool ret_val = false;

    channels::get_device_headend_info_request device_req;
    channels::get_device_headend_info_response device_resp;
    if (message_queue_->send(source_manager_message_queue_addressee, device_req, device_resp) == success &&
        device_resp.result_)
    {
        channels::get_provider_channels_request channel_req;
        channels::get_provider_channels_response channel_resp;
        if (message_queue_->send(source_manager_message_queue_addressee, channel_req, channel_resp) == success &&
            channel_resp.result_)
        {
            device_map_to_headend_list_t::iterator device_it = device_resp.devices_.begin();
            while (device_it != device_resp.devices_.end())
            {
                map_source_TO_log_ch[device_it->first] = std::map<channel_id_t, logical_channel_desc>();
                for (size_t i=0; i<device_it->second.size(); i++)
                {
                    for (size_t j=0; j<channel_resp.headends_.size(); j++)
                    {
                        if (boost::iequals(channel_resp.headends_[j].id_.get(), device_it->second[i].headend_id_.get()))
                        {
                            //add all channels of this provider
                            concise_transponder_desc_list_t& tr_list = channel_resp.headends_[j].transponders_;
                            for (size_t tr_idx = 0; tr_idx<tr_list.size(); tr_idx++)
                            {
                                for (size_t ch_idx = 0; ch_idx<tr_list[tr_idx].channels_.size(); ch_idx++)
                                {
                                    concise_device_channel_t& ch = tr_list[tr_idx].channels_[ch_idx];
                                    logical_channel_desc lch(ch.id_, ch.name_, ch.num_, ch.sub_num_);
                                    map_source_TO_log_ch[device_it->first].insert(std::make_pair(ch.id_, lch));
                                }
                            }
                        }
                    }
                }

                ++device_it;
            }
            ret_val = true;
        } else
        {
            log_error(L"recorder_engine::get_tuners_info. get_device_provider_info_request returned error");
        }
    } else
    {
        log_error(L"recorder_engine::get_tuners_info. get_provider_channels_request returned error");
    }

    channels::get_device_channel_sets_request ch_sets_req;
    channels::get_device_channel_sets_response ch_sets_resp;
    if (message_queue_->send(source_manager_message_queue_addressee, ch_sets_req, ch_sets_resp) == success &&
        ch_sets_resp.result_)
    {
        ch_set_data = ch_sets_resp.device_channel_sets_;
    } else
    {
        log_error(L"recorder_engine::get_tuners_info. get_device_channel_sets_request returned error");
    }

    return ret_val;
}

void recorder_engine::reschedule()
{
    //update epg event id in schedules (for backward compatibility, because of moving from artificial to real event ids in 4.5.3)
    update_epg_events_id_for_schedules();
    
    //get physical tuner mapping
    source_to_logical_map_t map_source_TO_log_ch;
    ts_source_TO_set_of_set_of_channels_set_t channel_set_data;
    if (get_tuners_info(map_source_TO_log_ch, channel_set_data))
    {
        //initialize timeline
        recorder_engine_impl_->reset(map_source_TO_log_ch, channel_set_data);
        
        //purge old and impossible schedules
        purge_schedules(map_source_TO_log_ch);
        //purge deleted recordings without actual schedules
        purge_deleted_recordings();
        
        //get all schedules from database
        schedule_list_t schedule_list;
        if (recorder_database_.get_schedules(&schedule_list))
        {
            for (size_t i = 0; i < schedule_list.size(); i++)
            {
                if (schedule_list[i].active_)
                {
                    //add schedule to the timeline. By giving NULL as conflict list we enforce timeline creation even if there are conflicts
                    if (!recorder_engine_impl_->add_schedule(schedule_list[i], NULL))
                    {
                        log_warning(L"Failed to add schedule %1% to the timeline") % schedule_list[i].schedule_item_id();
                    }
                } else
                {
                    log_info(L"recorder_engine::reschedule. Skipped inactive schedule %1%") % schedule_list[i].schedule_item_id();
                }
            }
        }
        else
        {
            log_error(L"Failed to get schedules from database");
        }
    }
    //clear reschedule requested flag
    b_reschedule_requested_ = false;
}

void recorder_engine::purge_schedules(source_to_logical_map_t& channel_map)
{
    log_info(L"recorder_engine::purge_schedules.");
    schedule_list_t schedule_list;
    if (recorder_database_.get_schedules(&schedule_list, DL_RECORDER_INVALID_ID, false))
    {
        for (size_t i=0; i<schedule_list.size(); i++)
        {
            //we delete only disabled schedules
            bool bdelete = schedule_list[i].disabled();
            if (bdelete)
            {
                //check if there are completed recordings that belong to this schedule
                completed_recordings_list_t recordings;
                if (recorder_database_.get_completed_recording_for_schedule(schedule_list[i].schedule_item_id(), recordings))
                    bdelete = (recordings.size() == 0);
            }
            if (bdelete)
            {
                log_ext_info(L"recorder_engine::purge_schedules. Deleting schedule %1%") % schedule_list[i].schedule_item_id().get();
                recorder_database_.remove_schedule(schedule_list[i].schedule_item_id());
            }
        }
    }
}

void recorder_engine::purge_deleted_recordings()
{
    log_info(L"recorder_engine::purge_deleted_recordings.");
    completed_recordings_list_t recordings;
    if (recorder_database_.get_deleted_recordings(recordings))
    {
        schedule_list_t schedule_list;
        if (recorder_database_.get_schedules(&schedule_list, DL_RECORDER_INVALID_ID, false))
        {
            schedule_map_t schedule_map;
            make_schedule_map_from_list(schedule_list, schedule_map);
            for (size_t i=0; i<recordings.size(); i++)
            {
                //check if schedule for this deleted recording a)exists and b)is enabled
                bool bdelete = true;
                if (schedule_map.find(recordings[i].schedule_item_id()) != schedule_map.end())
                {
                    if (!schedule_map[recordings[i].schedule_item_id()].disabled())
                        bdelete  = false;
                }
                if (bdelete)
                {
                    std::wstring wstr = string_cast<EC_UTF8>(recordings[i].timer_id().get());
                    log_ext_info(L"recorder_engine::purge_deleted_recordings. Deleting recording for timer %1%") % wstr;
                    recorder_database_.delete_deleted_recording(recordings[i].timer_id());
                }
            }
        }
    }
}

void recorder_engine::update_epg_events_id_for_schedules()
{
    schedule_list_t schedule_list;
    if (recorder_database_.get_schedules(&schedule_list))
    {
        for (size_t i = 0; i < schedule_list.size(); i++)
        {
            if (schedule_list[i].is_epg_based())
            {
                //try finding the epg event by its id
                epg_item epg_item;
                if (!recorder_database_.get_epg_item(schedule_list[i].channel(), schedule_list[i].epg_event_id(), epg_item))
                {
                    //try finding the epg event by its start time and duration
                    DLEPGEventList epgEventList;
                    if (EPGReadEventsFromXML(schedule_list[i].epg_program_info(), epgEventList) && epgEventList.size() == 1)
                    {
                        if (recorder_database_.get_epg_item(schedule_list[i].channel(), epgEventList[0].m_StartTime, epgEventList[0].m_Duration, epg_item))
                        {
                            //update epg event id in the schedule and save it back to the database
                            std::string epg_program_info;

                            epgEventList.clear();
                            epgEventList.push_back(epg_item.event_);
                            if (EPGWriteEventsToXML(epgEventList, epg_program_info))
                            {
                                if (!recorder_database_.update_schedule_event(schedule_list[i].schedule_item_id(), epg_item.event_.id_, epg_program_info))
                                {
                                    log_warning(L"recorder_engine::update_epg_events_id_for_schedules. Could not update event id for schedule %1%") % schedule_list[i].schedule_item_id();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

bool recorder_engine::int_get_settings(recorder_settings* settings)
{
    settings->copy_settings(recorder_settings_);
    return true;
}

bool recorder_engine::int_set_settings(recorder_settings* settings)
{
    recorder_settings_.copy_settings(*settings);
    recorder_settings_.save();
    //load settings will apply the new values
    recorder_settings_.load();
    //start checking deleted recordings if needed
    if (recorder_settings_.check_deleted_recordings() && recordings_check_thread_ == NULL)
        start_deleted_rec_check_thread();
    //or shut it down if not needed anymore
    if (!recorder_settings_.check_deleted_recordings() && recordings_check_thread_ != NULL)
        stop_deleted_rec_check_thread();

    //apply new settings
    reschedule();
    return true;
}

bool recorder_engine::check_server_connection()
{
    server::server_state_request req;
    server::server_state_response resp;
    message_queue_->send(server_message_queue_addressee, req, resp);
    return resp.state_ == dss_active;
}

} //recorder
} //dvblex
