/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <boost/thread.hpp>
#include <dl_event.h>
#include <dli_server.h>
#include <dl_message_queue.h>
#include <dl_command_queue.h>
#include "recorder_settings.h"
#include "recorder_database.h"
#include "recorder_engine_impl.h"
#include "timers.h"
#include <dl_schedules.h>
#include "recorder_engine_impl.h"
#include "epg.h"
#include "epg_updater.h"
#include "recorder_cmd.h"

namespace dvblex { namespace recorder {

class recorder_database;

class recorder_engine
{
public:
    enum rec_engine_mode_e
    {
        rem_unknown,
        rem_active,
        rem_idle
    };
    
public:
    recorder_engine();
    ~recorder_engine() {}
    
    bool run(dvblink::i_server_t server, dvblink::messaging::message_queue_t message_queue);
    void standby();
    void resume();
    void stop();

    bool browse_epg(server_channel_list_t& channel_list, int time_from, int time_to,
        std::string& keyphrase, unsigned long genre_mask, epg_level_e info_level, boost::int32_t max_count, epg_ex_channel_info_map_t& epg_channel_events);
	bool get_epg_event(dvblink::channel_id_t channel_id, dvblink::epg_event_id_t& event_id, epg_level_e info_level, epg_item_ex& epg_item);

    bool add_schedule(schedule_item& item, recording_timers_list_t* conflict_list);
    bool get_schedules(schedule_list_t& schedule_list, bool active_only = true);
    bool update_schedule(update_schedule_item_info& update_info);
    bool delete_schedule(const dvblink::schedule_item_id_t& schedule_item_id, bool delete_from_db = false);

    bool get_timers(recording_timers_list_t& scheduled_timer_list, recording_timers_list_t& active_timer_list, recording_timers_list_t& cancelled_timer_list);
    bool delete_timer(dvblink::timer_id_t& timer_id);
    
    bool force_update();

	bool get_completed_recordings(completed_recordings_list_t& completed_recordings_list, const dvblink::schedule_item_id_t& schedule_item_id = DL_RECORDER_INVALID_SCHEDULE_ID);
	bool delete_completed_recording(const dvblink::timer_id_t& timer_id);

	bool update_channels_epg(const std::vector<dvblink::channel_id_t>& channel_list);

    bool get_settings(recorder_settings& settings);
    bool set_settings(recorder_settings& settings);

    bool get_scheduls_in_csv(dvblink::csv_string_t& csv);

	bool repair_database();
    bool get_last_epg_update_time(time_t& last_update);

    bool is_running() {return recorder_thread_ != NULL;}

protected:
    void recorder_thread_func();
    void recordings_check_func();
    void reschedule();
    void update_epg_events_id_for_schedules();
    void run_idle();
    void run_active();

    bool get_tuners_info(source_to_logical_map_t& channel_map, ts_source_TO_set_of_set_of_channels_set_t& ch_set_data) const;

    void purge_schedules(source_to_logical_map_t& channel_map);
    void purge_deleted_recordings();
    bool prepare_new_schedule(schedule_item& item);

    //command handlers
    bool int_add_schedule(schedule_item* item, recording_timers_list_t* conflict_list);
    bool int_get_schedules(schedule_list_t* schedule_list, bool active_only = true);
    bool int_update_schedule(update_schedule_item_info* update_info);
    bool int_delete_schedule(const dvblink::schedule_item_id_t& schedule_item_id_, bool delete_from_db);
    bool int_get_timers(recording_timers_list_t* scheduled_timer_list, recording_timers_list_t* active_timer_list, recording_timers_list_t* cancelled_timer_list);
    bool int_delete_timer(dvblink::timer_id_t* timer_id);
    bool int_force_update();
    bool int_browse_epg(server_channel_list_t* channel_list, int time_from, int time_to, std::string* keyphrase,
        unsigned long genre_mask, epg_level_e info_level, boost::int32_t max_count, epg_ex_channel_info_map_t* epg_channel_events);
	bool int_get_epg_event(dvblink::channel_id_t* channel_id, dvblink::epg_event_id_t* event_id, epg_level_e info_level, epg_item_ex* epg_item);
	bool int_get_completed_recordings(completed_recordings_list_t* completed_recordings_list, const dvblink::schedule_item_id_t& schedule_item_id);
	bool int_delete_completed_recording(const dvblink::timer_id_t& timer_id);
	bool int_update_channels_epg(const std::vector<dvblink::channel_id_t>* channel_list);
    bool int_get_settings(recorder_settings* settings);
    bool int_set_settings(recorder_settings* settings);
    bool int_get_scheduled_timers_in_csv(dvblink::csv_string_t& schedule_csv);
	bool int_repair_database();
    bool check_server_connection();
    void stop_deleted_rec_check_thread();
    void start_deleted_rec_check_thread();
    void postprocess_epg_item_list(epg_channel_info_map_t& epg_channel_events, epg_ex_channel_info_map_t& ece);

    static std::wstring get_cmd_name(recorder_cmd_e cmd);

private:
    boost::thread* recordings_check_thread_;
    boost::thread* recorder_thread_;
    bool stop_flag_;
    bool recordings_check_stop_flag_;
    dvblink::engine::command_queue command_queue_;
    rec_engine_mode_e mode_;

    recorder_engine_impl* recorder_engine_impl_;
    epg_updater* epg_updater_;
    dvblink::event reschedule_event_;
    time_t last_epg_update_time_;
    
    //epg update flags
    bool b_reschedule_requested_;
    bool b_full_epg_update_;
    map_channel_id_t epg_update_channels_;

    dvblink::messaging::message_queue_t message_queue_;
    dvblink::i_server_t server_;
    recorder_settings recorder_settings_;
    recorder_database recorder_database_;
};

} //recorder
} //dvblex
