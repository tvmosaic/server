/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
//#include <stdlib.h>
#include <iomanip>
#include <dl_hash.h>
#include <dl_utils.h>
#include "timers.h"

using namespace dvblink;

namespace dvblex { namespace recorder {

logical_channel_desc::logical_channel_desc()
{
    reset();
}

logical_channel_desc::logical_channel_desc(const channel_id_t& id, const channel_name_t& name, const channel_number_t& number, const channel_subnumber_t& sub_number)
{
    set(id, name, number, sub_number);
}

void logical_channel_desc::set(const channel_id_t& id, const channel_name_t& name, const channel_number_t& number, const channel_subnumber_t& sub_number)
{
    id_ = id;
    name_= name;
    number_ = number;
    sub_number_ = sub_number;
}

void logical_channel_desc::reset()
{
    number_ = -1;
    sub_number_ = -1;
    id_ = "";
};

///////////////////////////////////////////////////////////////////////////////

recording_timer::recording_timer(const schedule_item& sch_item) :
    timer(timer_id_t(), sch_item.schedule_item_id()),
    start_time_(sch_item.main_start_time()),
    duration_(sch_item.main_duration()),
    margin_before_(sch_item.margin_before()),
    margin_after_(sch_item.margin_after()),
    channel_(sch_item.channel()),
    conflict_(false),
    priority_(sch_item.priority_)
{
    event_info_.m_Name = sch_item.name().to_string();
    event_info_.m_StartTime = sch_item.main_start_time();
    event_info_.m_Duration = sch_item.main_duration();
    make_id();
}

recording_timer::recording_timer(const schedule_item& sch_item, const time_t& start_time) :
    timer(timer_id_t(), sch_item.schedule_item_id()),
    start_time_(start_time),
    duration_(sch_item.main_duration()),
    margin_before_(sch_item.margin_before()),
    margin_after_(sch_item.margin_after()),
    channel_(sch_item.channel()),
    conflict_(false),
    priority_(sch_item.priority_)
{
    event_info_.m_Name = sch_item.name().to_string();
    event_info_.m_StartTime = start_time;
    event_info_.m_Duration = sch_item.main_duration();
    make_id();
}

recording_timer::recording_timer(const schedule_item& sch_item, const epg_event_id_t& epg_event_id, const engine::DLEPGEvent& event_info) :
    timer(timer_id_t(), sch_item.schedule_item_id()),
    start_time_(event_info.m_StartTime),
    duration_(event_info.m_Duration),
    margin_before_(sch_item.margin_before()),
    margin_after_(sch_item.margin_after()),
    channel_(sch_item.channel()),
    epg_event_id_(epg_event_id),
    event_info_(event_info),
    conflict_(false),
    priority_(sch_item.priority_)
{
    make_id();
}

recording_timer::recording_timer(const schedule_item& sch_item, const channel_id_t& channel, const epg_event_id_t& epg_event_id, const engine::DLEPGEvent& event_info) :
    timer(timer_id_t(), sch_item.schedule_item_id()),
    start_time_(event_info.m_StartTime),
    duration_(event_info.m_Duration),
    margin_before_(sch_item.margin_before()),
    margin_after_(sch_item.margin_after()),
    channel_(channel),
    epg_event_id_(epg_event_id),
    event_info_(event_info),
    conflict_(false),
    priority_(sch_item.priority_)
{
    make_id();
}

void recording_timer::make_id()
{
    std::stringstream strbuf;
    strbuf << schedule_item_id() << "-" << dvblink::engine::calculate_64bit_string_hash(channel_.get()) << "-" << start_time_;
    set_timer_id(strbuf.str());
}

std::wstring recording_timer::generate_record_name() const
{
    //recording name
    std::string rname;
    //season and episode if they exist
    if (event_info_.m_SeasonNum != 0 || event_info_.m_EpisodeNum != 0)
    {
        std::stringstream ss;
        if (event_info_.m_SeasonNum != 0)
        {
            ss << "S" << std::setw(2) << std::setfill('0') << event_info_.m_SeasonNum;
        }
        if (event_info_.m_EpisodeNum != 0)
        {
            ss << "E" << std::setw(2) << std::setfill('0') << event_info_.m_EpisodeNum;
        }
        rname += ss.str() + "-";
    }

    for (size_t i = 0; i < event_info_.m_Name.size(); i++)
    {
        if (event_info_.m_Name[i] > 0)
        {
            if ( isalnum(event_info_.m_Name[i]) )
            {
                rname += event_info_.m_Name[i];
            }
            else
            if (isspace(event_info_.m_Name[i]))
            {
                rname += '_';
            }
        }
    }

    //recording date-time
    char dt_buf[1024]; dt_buf[0] = '\0';
    if (struct tm* ti = localtime(&start_time_))
    {
        sprintf(dt_buf, "%0.2d%0.2d_%d%0.2d%0.2d", ti->tm_hour, ti->tm_min, ti->tm_year + 1900, ti->tm_mon + 1, ti->tm_mday);
    }

    std::stringstream rec_file_stream;
    rec_file_stream << rname << "-" << dt_buf << "-" << timer_id();

    std::wstring wrec_name = engine::string_cast<engine::EC_UTF8>(rec_file_stream.str());
    engine::substitute_invalid_file_chars(wrec_name);

    return wrec_name;
}

///////////////////////////////////////////////////////////////////////////////

recording_epg_timer_map::recording_epg_timer_map(const recording_timers_list_t& timer_list)
{
    for (size_t i = 0; i < timer_list.size(); i++)
    {
        if (timer_list[i].is_by_epg())
        {
            epg_event_id_t id = make_id(timer_list[i].channel(), timer_list[i].epg_event_id());
            recordings_map_[id] = timer_list[i];
        }
    }
}

const recording_timer* recording_epg_timer_map::find_timer(const channel_id_t& channel, std::string& epg_event_id)
{
    recordings_map_t::const_iterator it = recordings_map_.find(make_id(channel, epg_event_id));
    if (it != recordings_map_.end())
    {
        return &it->second;
    }
    return NULL;
}

epg_event_id_t recording_epg_timer_map::make_id(const channel_id_t& channel, const epg_event_id_t& epg_event_id)
{
    std::stringstream strbuf;
    strbuf << channel.get() << "-" << epg_event_id;
    return strbuf.str();
}


} //recorder
} //dvblex
