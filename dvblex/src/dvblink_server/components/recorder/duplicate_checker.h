/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <map>
#include <list>
#include <dl_types.h>
#include <dl_schedules.h>
#include "timers.h"
#include "timeline_builder.h"
#include "recorder_settings.h"


namespace dvblex { namespace recorder {

class recorder_database;

typedef std::map<std::string, std::string> episode_name_map_t;
typedef std::map<unsigned long, unsigned long> episode_number_map_t;

class recorder_duplicate_checker
{
public:
    recorder_duplicate_checker(recorder_database& db, recorder_settings& rs) :
        recorder_db_(db), recorder_settings_(rs)
    {}

    ~recorder_duplicate_checker() {}
    
    void init(const schedule_item& item);
    bool was_recorded_before(const recording_timer& timer);
    static bool is_already_on_timeline(recorder_settings& rs, const timeline_builder& timeline, const recording_timer& timer);

    static unsigned long make_episode_key(const dvblink::engine::DLEPGEvent& event) {return (event.m_SeasonNum << 16) | event.m_EpisodeNum;}
    static std::string make_episode_name_key(const dvblink::engine::DLEPGEvent& event);

    static bool is_valid_name_key(const std::string& name_key) {return name_key.size() > 3;}
    static bool is_valid_num_key(unsigned long num_key) {return num_key != 0;}

protected:
    episode_name_map_t episode_name_map_;
    episode_number_map_t episode_num_map_;
    recorder_database& recorder_db_;
    recorder_settings& recorder_settings_;
    
    static bool is_series_program(const recording_timer& timer);
};

} //recorder
} //dvblex
