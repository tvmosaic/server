/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <sstream>
#include <string>
#include <dl_types.h>
#include <dl_strings.h>
#include <dl_network_helper.h>
#include <pb_object.h>

namespace dvblex { namespace recorder {

inline bool get_adjusted_resource_url(const std::string& original_url,
    const ip_address_t& server_address, const network_port_t& server_port, std::string& new_url)
{
    bool adjusted = false;
    new_url = original_url;
    if (!original_url.empty())
    {
        std::wstring wurl = engine::string_cast<engine::EC_UTF8>(original_url);
        //parse original url
        EDL_NET_PROTOCOLS proto = network_helper::get_proto(wurl.c_str());
        if (proto == DL_NET_PROTO_UNKNOWN)
        {
            //this is relative url, currently containing a playback object id
            //make a full url out of it
            new_url = playback::make_object_url(server_address, server_port, wurl);
            adjusted = true;
        }
    }
    return adjusted;
}

} //recorder
} //dvblex
