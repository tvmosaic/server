/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

namespace dvblex { namespace recorder {

const std::string recorder_db_dir_name           = "recorder_database";
const std::string recorded_tv_dir_name           = "RecordedTV";

const time_t invalid_time_value = -1;

#define THUMB_VERT_SIZE                         576
#define THUMB_HOR_SIZE                         720
#define THUMB_TOLERANCE_PERIOD                  90

#define DL_RECORDER_SETTINGS_FILENAME		    L"recorder_settings.xml"
#define DL_RECORDER_THUMBNAILS_DIR		        L"thumbnails"

#define DL_RECORDER_SETTINGS_PATH_NODE		    "record_path"
#define DL_RECORDER_SETTINGS_MARGIN_BEFORE_NODE	"margin_before"
#define DL_RECORDER_SETTINGS_MARGIN_AFTER_NODE	"margin_after"
#define DL_RECORDER_SETTINGS_ROOT_NODE		    "dlrecorder_settings"
#define DL_RECORDER_SETTINGS_MIN_FREE_SPACE_NODE "minimum_free_space"
#define DL_RECORDER_SETTINGS_NO_DUP_ALGO_NODE   "do_not_detect_duplicates"
#define DL_RECORDER_SETTINGS_CHECK_DELETED_NODE "check_deleted_recordings"
#define DL_RECORDER_SETTINGS_DS_MODE_NODE       "ds_auto_calc_mode"
#define DL_RECORDER_SETTINGS_DS_MAN_VALUE_NODE  "ds_manual_threshold_kb"
#define DL_RECORDER_SETTINGS_USE_AUTODELETE_NODE "use_autodelete"
#define DL_RECORDER_SETTINGS_NEW_ONLY_ALGO_NODE "new_only_algo_type"
#define DL_RECORDER_SETTINGS_NEW_ONLY_DEFAULT_NODE "new_only_default_value"
#define DL_RECORDER_SETTINGS_PATTERN_NODE		"filename_pattern"

#define DL_RECORDER_DEFAULT_MARGIN_BEFORE       time_t(60)
#define DL_RECORDER_DEFAULT_MARGIN_AFTER        time_t(300)

#define DL_RECORDER_DEFAULT_MIN_FREE_SPACE      4000000000UL
#define DL_THRESHOLD_MIN_DISK_SPACE_KB          (50000000)

#define DL_RECORDER_DEFAULT_FILENAME_PATTERN	"%program.season/episode%-%program.name%-%program.subname%-%program.date/time%"

#define DL_RECORDER_INVALID_SCHEDULE_ID         (-1)
#define DL_RECORDER_MAX_SCHED_EVENT_NUM	        (50)
#define DL_RECORDER_DEFAULT_MARGIN              (-1)

#define DL_RECORDER_RECORDING_WINDOWS_SEC       time_t(14 * 24 * 3600)
#define DL_RECORDER_24_HOURS_SEC                time_t(24 * 3600)
#define DL_RECORDER_ID_REUSE_TOLERANCE          time_t(4 * 60 * 60)
#define DL_RECORDER_PAST_EPG_COPY_FRAME         time_t(6 * 60 * 60)

#define DL_RECORDER_INITIAL_EPG_SCAN_DELAY_SEC  time_t(120)
#define DL_RECORDER_REPEAT_EPG_SCAN_DELAY_SEC   time_t(120)

#define DL_RECORDER_PROCESS_PERIOD_SEC          time_t(5)

#define DL_RECORDER_DATABASE_FILENAME           L"recorder_database.db"

#ifdef WIN32
	#define DL_RECORDER_DATABASE_REPAIR_SCRIPT      L"dlrepair.bat"
#else
	#define DL_RECORDER_DATABASE_REPAIR_SCRIPT      L"dlrepair.sh"
#endif

#define DL_RECORDER_CREATE_DLTD_REC_STMT        "CREATE TABLE deleted_recording (id integer PRIMARY KEY AUTOINCREMENT, schedule_id integer, timer_id varchar(255), state integer, "\
											    	"filename varchart(255), " \
											    	"channel_id varchar(255), channel_num integer, channel_subnum integer, channel_name varchart(255), " \
											    	"name varchar(255), short_desc varchar(255), start_time integer, duration integer, second_name varchar(255), " \
											    	"language varchar(255), actors varchar(255), directors varchar(255), writers varchar(255), producers varchar(255), guests varchar(255), image_url varchar(255), " \
											    	"year integer, episode_num integer, season_num integer, star_num integer, star_max integer, categories varchar(255), " \
											    	" is_hdtv integer, is_premiere integer, is_repeat integer, is_action integer, is_comedy integer, is_doc integer, is_drama integer, " \
											    	" is_edu integer, is_horror integer, is_kids integer, is_movie integer, is_music integer, is_news integer, is_reality integer, " \
											    	" is_romance integer, is_scifi integer, is_serial integer, is_soap integer, is_special integer, is_sports integer, " \
											    	" is_thriller integer, is_adult integer );"


#define DL_RECORDER_TABLES_CREATE_STMT          "CREATE TABLE schedule (id integer PRIMARY KEY AUTOINCREMENT, type integer, extra_param varchar(255), channel varchar(255), name varchar(255), " \
                                                    "start_time integer, duration integer, margin_before integer, margin_after integer, day_mask integer, epg_event_id varchar(255), epg_program_info varchar(255), " \
                                                    "series integer, series_anytime integer, series_new_only integer, number_to_keep integer, disabled integer, key_phrase varchar(255), genre_mask integer, sendto_targets varchar(255), " \
                                                    "start_after_sec integer, start_before_sec integer, priority integer, active integer);" \
                                                "CREATE TABLE cancelled_timer (id integer PRIMARY KEY AUTOINCREMENT, schedule_id integer references schedule(id), timer_id varchar(255));" \
                                                "CREATE TABLE epg_channel (channel varchar(255), epg_channel_id varchar(255) );" \
                                                "CREATE TABLE epg_event (event_id varchar(255), channel varchar(255) references epg_channel(channel), name varchar(255), name_lower_case varchar(255), short_desc varchar(255), short_desc_lower_case varchar(255), start_time integer, duration integer, second_name varchar(255), second_name_lower_case varchar(255), " \
											    	"language varchar(255), actors varchar(255), directors varchar(255), writers varchar(255), producers varchar(255), guests varchar(255), image_url varchar(255), " \
											    	"year integer, episode_num integer, season_num integer, star_num integer, star_max integer, categories varchar(255), " \
											    	" is_hdtv integer, is_premiere integer, is_repeat integer, is_action integer, is_comedy integer, is_doc integer, is_drama integer, " \
											    	" is_edu integer, is_horror integer, is_kids integer, is_movie integer, is_music integer, is_news integer, is_reality integer, " \
											    	" is_romance integer, is_scifi integer, is_serial integer, is_soap integer, is_special integer, is_sports integer, " \
											    	" is_thriller integer, is_adult integer );" \
                                                "CREATE TABLE epg_event_tmp (event_id varchar(255), channel varchar(255), name varchar(255), name_lower_case varchar(255), short_desc varchar(255), short_desc_lower_case varchar(255), start_time integer, duration integer, second_name varchar(255), second_name_lower_case varchar(255), " \
											    	"language varchar(255), actors varchar(255), directors varchar(255), writers varchar(255), producers varchar(255), guests varchar(255), image_url varchar(255), " \
											    	"year integer, episode_num integer, season_num integer, star_num integer, star_max integer, categories varchar(255), " \
											    	" is_hdtv integer, is_premiere integer, is_repeat integer, is_action integer, is_comedy integer, is_doc integer, is_drama integer, " \
											    	" is_edu integer, is_horror integer, is_kids integer, is_movie integer, is_music integer, is_news integer, is_reality integer, " \
											    	" is_romance integer, is_scifi integer, is_serial integer, is_soap integer, is_special integer, is_sports integer, " \
											    	" is_thriller integer, is_adult integer );" \
											    "CREATE TABLE completed_recording (id integer PRIMARY KEY AUTOINCREMENT, schedule_id integer, timer_id varchar(255), state integer, "\
											    	"filename varchart(255), " \
											    	"channel_id varchar(255), channel_num integer, channel_subnum integer, channel_name varchart(255), " \
											    	"name varchar(255), short_desc varchar(255), start_time integer, duration integer, second_name varchar(255), " \
											    	"language varchar(255), actors varchar(255), directors varchar(255), writers varchar(255), producers varchar(255), guests varchar(255), image_url varchar(255), " \
											    	"year integer, episode_num integer, season_num integer, star_num integer, star_max integer, categories varchar(255), " \
											    	" is_hdtv integer, is_premiere integer, is_repeat integer, is_action integer, is_comedy integer, is_doc integer, is_drama integer, " \
											    	" is_edu integer, is_horror integer, is_kids integer, is_movie integer, is_music integer, is_news integer, is_reality integer, " \
											    	" is_romance integer, is_scifi integer, is_serial integer, is_soap integer, is_special integer, is_sports integer, " \
											    	" is_thriller integer, is_adult integer );"
} //recorder
} //dvblex
