/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <list>
#include "timers.h"
#include "active_recording.h"
#include "recorder_settings.h"
#include "channel_set_container.h"

namespace dvblex { namespace recorder {

typedef std::vector<dvblink::channel_id_t> phys_channel_list_t;
typedef std::map<dvblink::channel_set_id_t, phys_channel_list_t> map_ch_set_id_t;
typedef std::map<dvblink::transponder_id_t, map_ch_set_id_t> map_transponder_id_t;

typedef std::map<dvblink::channel_set_id_t, dvblink::concurrency_num_t> map_ch_set_concurrency_t;

class completed_rec_man;

struct channel_concurr_detailed_info
{
    channel_concurr_detailed_info() :
        current_concurency_number_(0),
        channel_number_(0)
    {}

    channel_concurr_detailed_info(const dvblink::device_id_t& source_id, const dvblink::channel_id_t& phy_ch_id,
            const dvblink::transponder_id_t& transp_id, const dvblink::channel_set_id_t& ch_set_id, const dvblink::concurrency_num_t& current_concurency_number, const dvblink::concurrency_num_t& max_concurency_number, size_t channel_number) :
        source_id_(source_id),
        phy_ch_id_(phy_ch_id),
        transp_id_(transp_id),
        ch_set_id_(ch_set_id),
        current_concurency_number_(current_concurency_number),
        max_concurency_number_(max_concurency_number),
        channel_number_(channel_number)
    {
    }

    dvblink::device_id_t source_id_;
    dvblink::channel_id_t phy_ch_id_;
    dvblink::transponder_id_t transp_id_;
    dvblink::channel_set_id_t ch_set_id_;
    dvblink::concurrency_num_t current_concurency_number_;
    dvblink::concurrency_num_t max_concurency_number_;
    size_t channel_number_;
    recording_timers_list_t conflict_list_;

    static bool comp_channel_quantity(const channel_concurr_detailed_info& cs1, const channel_concurr_detailed_info& cs2)
    {
        return cs1.channel_number_ < cs2.channel_number_;
    }
};

typedef std::vector<channel_concurr_detailed_info> vec_source_concurr_data_t;

class concurrent_source_comp_channel_num
{
public:
    bool operator()(const channel_concurr_detailed_info& cs1, const channel_concurr_detailed_info& cs2)
    {
        return cs1.channel_number_ < cs2.channel_number_;
    }
};

class timeline_builder
{
public:
    timeline_builder(completed_rec_man* completed_r_man, recorder_settings& rs, channel_set_container& csc) :
        completed_rec_man_(completed_r_man), recorder_settings_(rs), channel_set_container_(csc)
    {}
    ~timeline_builder() {}

    static bool is_program_in_wnd(time_t start_time, time_t duration, time_t wnd_start, time_t wnd_end);
    static bool is_program_overlap(time_t p1_start, time_t p1_duration, time_t p2_start, time_t p2_duration);
    static std::wstring form_time(const time_t t);

    void reset();
    void add(const active_recordings_map_t& act_rec_map);

    bool add_timer(const recording_timer& timer, recording_timers_list_t* conflict_list);
    bool is_already_on_timeline(const recording_timer& timer) const;

    bool get_timer(const dvblink::timer_id_t& timer_id, recording_timer& timer) const;
    time_t get_first_future_timer() const;

    void get_scheduled_timers(recording_timers_list_t& timer_list) const;
    bool get_scheduled_timers_in_csv(dvblink::csv_string_t& schedule_csv) const;

    bool remove_timer(const dvblink::timer_id_t& timer_id, recording_timer* removed_timer = NULL);
    void remove_schedule(const dvblink::schedule_item_id_t& schedule_id);

    void update_temp_timer_ids(const dvblink::schedule_item_id_t& new_id);
    void get_timers_for_wnd(time_t wnd_start, time_t wnd_duration, map_tuner_timeline_t& timer_list);

    time_t get_largest_before_margin();

    enum margins_policy
    {
        dont_use_margins,
        use_margins
    };

    template <typename ITER>
    static ITER check_timeline_conflicts(const ITER& timer_list_begin, const ITER& timer_list_end, const recording_timer& timer, recording_timers_list_t* conflict_list, margins_policy policy);

    static void put_timer_in_list(timeline_timer_list_t& timer_list, const recording_timer& timer, recording_timers_list_t* conflict_list = NULL, margins_policy policy = use_margins);
    static bool is_conflicted(const dvblink::device_id_t& source_id,
        const timeline_timer_list_t& timeline_timer_list, const recording_timer& timer, 
        channel_set_container& csc, recording_timers_list_t* conflict_list = NULL, margins_policy policy = use_margins);

protected:
    bool find_timer_source_id(const timer& timer, dvblink::device_id_t& source_id) const;

    static bool get_channel_concurr_detailed_info(const dvblink::device_id_t& source_id, const dvblink::channel_id_t& log_ch, channel_set_container& csc, channel_concurr_detailed_info& info);
    void get_concurrent_sources(const recording_timer& timer, vec_source_concurr_data_t& concurr_list, vec_source_concurr_data_t& non_concurr_list, margins_policy policy = use_margins) const;

    bool put_timer_in_concurrent_source(const recording_timer& timer, vec_source_concurr_data_t& vec_concurrent_sources, dvblink::device_id_t& tuner_id, recording_timers_list_t& conflict_list);
    bool put_timer_in_non_concurrent_source(const recording_timer& timer, vec_source_concurr_data_t& vec_non_concurrent_sources, dvblink::device_id_t& tuner_id, recording_timers_list_t& conflict_list);

private:
    map_tuner_timeline_t timeline_;
    completed_rec_man* completed_rec_man_;
    recorder_settings& recorder_settings_;
    channel_set_container& channel_set_container_;
};

template <typename ITER>
ITER timeline_builder::check_timeline_conflicts(
    const ITER& timer_list_begin, const ITER& timer_list_end, const recording_timer& timer, recording_timers_list_t* conflict_list, margins_policy policy)
{
    ITER placing_it = timer_list_end;
    ITER it = timer_list_begin;
    ITER prev_it = it;
    time_t timer_start_time, it_start_time, timer_duration, it_duration, prev_it_start_time;

    if (policy == use_margins)
    {
        timer_start_time = timer.real_start_time();
        timer_duration = timer.real_duration();
    }
    else
    {
        timer_start_time = timer.main_start_time();
        timer_duration = timer.main_duration();
    }

    while (it != timer_list_end)
    {
        if (policy == use_margins)
        {
            it_start_time = it->real_start_time();
            it_duration = it->real_duration();
            prev_it_start_time = prev_it->real_start_time();
        }
        else
        {
            it_start_time = it->main_start_time();
            it_duration = it->main_duration();
            prev_it_start_time = prev_it->main_start_time();
        }

        //find first timer that is earlier than the new one
        if ((it == timer_list_begin && it_start_time >= timer_start_time) || (prev_it_start_time < timer_start_time && it_start_time >= timer_start_time))
        {
            placing_it = it;
        }

        if (conflict_list && is_program_overlap(timer_start_time, timer_duration, it_start_time, it_duration))
        {
            conflict_list->push_back(*it);
        }

        prev_it = it;
        ++it;
    }
    return placing_it;
}


} //recorder
} //dvblex
