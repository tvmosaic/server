/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread.hpp>
#include <dl_filesystem_path.h>
#include <dli_server.h>
#include <dl_message_queue.h>
#include "recorder_settings.h"
#include "timers.h"
#include "file_writer.h"

namespace dvblex { namespace recorder {

enum active_recording_state_e
{
    rcsError,
    rcsWaiting,
    rcsPreRecording,
    rcsRecording,
    rcsPostRecording,
    rcsCompleted,
    rcsInterrupted
};

class active_recording
{
public:
    active_recording(recorder_settings& rs, const recording_timer& rec_timer, const logical_channel_desc& channel, dvblink::i_server_t server, dvblink::messaging::message_queue_t message_queue);
    virtual ~active_recording() {cancel_recording();}

    void update_state();
    void cancel_recording();

    bool is_completed() {return state_ == rcsError || state_ == rcsCompleted || state_ == rcsInterrupted;}

    const recording_timer& get_timer() const {return recording_timer_;}
    active_recording_state_e get_state() const {return state_;}
    const dvblink::filesystem_path_t& get_filename() const {return filename_;}
	
    time_t get_actual_start() const {return actual_start_;}
	time_t get_actual_stop() const {return actual_stop_;}
    
    bool get_thumb_path(std::wstring& filename) const;
    void invalidate_thumb() {thumb_invalidated_ = true;}

    time_t real_start_time() const {return recording_timer_.real_start_time();}
    time_t real_duration() const {return recording_timer_.real_duration();}

protected:
    void stop_recording();
    bool start_recording();
    void thumb_thread_func();
    void generate_thumb_with_ffmpeg(size_t sec_offset);

private:
    dvblink::i_server_t server_;
    dvblink::messaging::message_queue_t message_queue_;
    dvblink::streamer_id_t streamer_id_;
    recording_timer recording_timer_;
    active_recording_state_e state_;
    dvblink::filesystem_path_t filename_;
	logical_channel_desc channel_;
	time_t actual_start_;
	time_t actual_stop_;

    dvblink::filesystem_path_t thumb_filename_;
    boost::thread* thumb_thread_;
    bool thumb_thread_exit_;
    bool thumb_invalidated_;
    int thumb_tolerance_period_;
    int thumb_vert_size_;
    int thumb_hor_size_;
    recorder_settings& recorder_settings_;
    boost::shared_ptr<file_writer_t> file_writer_;
};

typedef boost::shared_ptr<active_recording> active_recording_t;

typedef std::list<active_recording_t> active_recordings_list_t;
typedef std::map<dvblink::device_id_t, active_recordings_list_t> active_recordings_map_t;

} //recorder
} //dvblex
