/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#include <boost/filesystem.hpp>
#include <dl_strings.h>
#include <dl_pugixml_helper.h>
#include <dl_utils.h>
#include <dl_file_procedures.h>
#include "recorder_settings.h"

using namespace dvblink; 
using namespace dvblink::engine;
using namespace dvblink::pugixml_helpers;

namespace dvblex { namespace recorder {

bool recorder_settings::init(dvblex::settings::installation_settings_t& install_settings)
{
    recorder_db_path_ = install_settings.get_shared_component_path(recorder_db_dir_name);
    default_record_path_ = install_settings.get_shared_component_path(recorded_tv_dir_name);
    recorder_settings_pathname_ = install_settings.get_config_path() / DL_RECORDER_SETTINGS_FILENAME;
    
    //make sure that recorder db directory exists
    try {
        boost::filesystem::create_directories(recorder_db_path_.to_boost_filesystem());
    } catch(...) {}

    load_settings();
	return true;
}

void recorder_settings::copy_settings(const recorder_settings& from_settings)
{
    record_path_ = from_settings.record_path_;
    recorder_db_path_ = from_settings.recorder_db_path_;
    recorder_settings_pathname_ = from_settings.recorder_settings_pathname_;
    default_record_path_ = from_settings.default_record_path_;

    recording_margin_before_ = from_settings.recording_margin_before_;
    recording_margin_after_ = from_settings.recording_margin_after_;
    min_free_disk_space_ = from_settings.min_free_disk_space_;
    check_deleted_recordings_ = from_settings.check_deleted_recordings_;
    ds_threshold_auto_ = from_settings.ds_threshold_auto_;
    manual_disk_threshold_kb_ = from_settings.manual_disk_threshold_kb_;
    autodelete_enabled_ = from_settings.autodelete_enabled_;
    new_only_algo_type_ = from_settings.new_only_algo_type_;
	filename_pattern_ = from_settings.filename_pattern_;
    new_only_default_value_ = from_settings.new_only_default_value_;
}

void recorder_settings::setup_record_path()
{
    //make sure that directory exists
    if (!boost::filesystem::exists(record_path_.to_boost_filesystem()))
    {
        try {
            boost::filesystem::create_directories(record_path_.to_boost_filesystem());
        } catch(...) {}
    }

    filesystem_path_t thumbnail_path = get_thumbnail_path();
    //make sure that thumbnail directory exists
    if (!boost::filesystem::exists(thumbnail_path.to_boost_filesystem()))
    {
        try {
            boost::filesystem::create_directories(thumbnail_path.to_boost_filesystem());
        } catch(...) {}
    }
}

bool recorder_settings::get_rec_disk_space_stats_kb(boost::int64_t& total_space, boost::int64_t& available_space)
{
    bool ret_val = true;
    boost::filesystem::space_info si = boost::filesystem::space(record_path_.to_boost_filesystem());
    
    total_space = si.capacity / 1024;
    if (si.available > (boost::uintmax_t)min_free_disk_space_)
        available_space = (si.available - (boost::uintmax_t)min_free_disk_space_) / 1024;
    else
        available_space = 0;
    
    return ret_val;
}

boost::int64_t recorder_settings::get_auto_disk_space_threshold()
{
    boost::int64_t total_space = 0;
    boost::int64_t available_space = 0;
    
    try
    {
    	get_rec_disk_space_stats_kb(total_space, available_space);
    } catch(...)
    {
    	total_space = 0;
    	available_space = 0;
    }

    //minimum of 10% of the disk space or MIN_DISK_SPACE_KB
    boost::int64_t threshold = total_space / 10;

    if (threshold > DL_THRESHOLD_MIN_DISK_SPACE_KB)
        threshold = DL_THRESHOLD_MIN_DISK_SPACE_KB;

    return threshold;
}

void recorder_settings::reset_settings()
{
    record_path_ = default_record_path_;
        
    recording_margin_before_ = DL_RECORDER_DEFAULT_MARGIN_BEFORE;
    recording_margin_after_ = DL_RECORDER_DEFAULT_MARGIN_AFTER;

    min_free_disk_space_ = DL_RECORDER_DEFAULT_MIN_FREE_SPACE;

    check_deleted_recordings_ = false;
    
    ds_threshold_auto_ = true;
    manual_disk_threshold_kb_ = DL_RECORDER_DEFAULT_MIN_FREE_SPACE; //will be recalculated after loading settings
    autodelete_enabled_ = false;
    
    new_only_algo_type_ = noat_not_seen_before;
    new_only_default_value_ = nodv_not_set;

	filename_pattern_ = DL_RECORDER_DEFAULT_FILENAME_PATTERN;
}

void recorder_settings::load_settings()
{
    reset_settings();

    filesystem_path_t settings_pathname = get_settings_pathname();

    pugi::xml_document doc;
    if (doc.load_file(settings_pathname.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            std::string wstr;
            if (get_node_value(root_node, DL_RECORDER_SETTINGS_PATH_NODE, wstr))
                record_path_ = wstr;
            if (get_node_value(root_node, DL_RECORDER_SETTINGS_MARGIN_BEFORE_NODE, wstr))
                string_conv::apply(wstr.c_str(), recording_margin_before_, DL_RECORDER_DEFAULT_MARGIN_BEFORE);
            if (get_node_value(root_node, DL_RECORDER_SETTINGS_MARGIN_AFTER_NODE, wstr))
                string_conv::apply(wstr.c_str(), recording_margin_after_, DL_RECORDER_DEFAULT_MARGIN_AFTER);
            if (get_node_value(root_node, DL_RECORDER_SETTINGS_MIN_FREE_SPACE_NODE, wstr))
                string_conv::apply(wstr.c_str(), min_free_disk_space_, static_cast<boost::int64_t>(DL_RECORDER_DEFAULT_MIN_FREE_SPACE));
            if (get_node_value(root_node, DL_RECORDER_SETTINGS_NEW_ONLY_ALGO_NODE, wstr))
            {
                int l;
                string_conv::apply(wstr.c_str(), l, (int)noat_not_seen_before);
                new_only_algo_type_ = (new_only_algo_type_e)l;
            }
            if (get_node_value(root_node, DL_RECORDER_SETTINGS_NEW_ONLY_DEFAULT_NODE, wstr))
            {
                int l;
                string_conv::apply(wstr.c_str(), l, (int)nodv_not_set);
                new_only_default_value_ = (new_only_default_value_e)l;
            }
            if (root_node.child(DL_RECORDER_SETTINGS_CHECK_DELETED_NODE) != NULL)
                check_deleted_recordings_ = true;
            if (root_node.child(DL_RECORDER_SETTINGS_DS_MODE_NODE) != NULL)
            {
                ds_threshold_auto_ = false;
                if (get_node_value(root_node, DL_RECORDER_SETTINGS_DS_MAN_VALUE_NODE, wstr))
                {
                    boost::int64_t default_value = get_auto_disk_space_threshold();
                    string_conv::apply(wstr.c_str(), manual_disk_threshold_kb_, static_cast<boost::int64_t>(default_value));
                }
            }
            if (root_node.child(DL_RECORDER_SETTINGS_USE_AUTODELETE_NODE) != NULL)
                autodelete_enabled_ = true;

			get_node_value(root_node, DL_RECORDER_SETTINGS_PATTERN_NODE, filename_pattern_);
        }
    }

    //make sure that record directory exists 
    setup_record_path();

    //recalculate the auto disk space threshold if needed
    if (ds_threshold_auto_)
    	manual_disk_threshold_kb_ = get_auto_disk_space_threshold();

}

void recorder_settings::save_settings()
{
    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(DL_RECORDER_SETTINGS_ROOT_NODE);
    if (root_node != NULL)
    {
        
        new_child(root_node, DL_RECORDER_SETTINGS_PATH_NODE, record_path_.to_string());

        std::stringstream strbuf;
        strbuf << recording_margin_before_;
        new_child(root_node, DL_RECORDER_SETTINGS_MARGIN_BEFORE_NODE, strbuf.str());

        strbuf.clear(); strbuf.str("");
        strbuf << recording_margin_after_;
        new_child(root_node, DL_RECORDER_SETTINGS_MARGIN_AFTER_NODE, strbuf.str());

        strbuf.clear(); strbuf.str("");
        strbuf << min_free_disk_space_;
        new_child(root_node, DL_RECORDER_SETTINGS_MIN_FREE_SPACE_NODE, strbuf.str());

        strbuf.clear(); strbuf.str("");
        strbuf << new_only_default_value_;
        new_child(root_node, DL_RECORDER_SETTINGS_NEW_ONLY_DEFAULT_NODE, strbuf.str());

        strbuf.clear(); strbuf.str("");
        strbuf << new_only_algo_type_;
        new_child(root_node, DL_RECORDER_SETTINGS_NEW_ONLY_ALGO_NODE, strbuf.str());

        new_child(root_node, DL_RECORDER_SETTINGS_PATTERN_NODE, filename_pattern_);

        strbuf.clear(); strbuf.str("");
        strbuf << check_deleted_recordings_;
        if (check_deleted_recordings_)
            new_child(root_node, DL_RECORDER_SETTINGS_CHECK_DELETED_NODE, strbuf.str());

        if (!ds_threshold_auto_)
        {
            strbuf.clear(); strbuf.str("");
            strbuf << ds_threshold_auto_;
            new_child(root_node, DL_RECORDER_SETTINGS_DS_MODE_NODE, strbuf.str());

            strbuf.clear(); strbuf.str("");
            strbuf << manual_disk_threshold_kb_;
            new_child(root_node, DL_RECORDER_SETTINGS_DS_MAN_VALUE_NODE, strbuf.str());
        }

        strbuf.clear(); strbuf.str("");
        strbuf << autodelete_enabled_;
        if (autodelete_enabled_)
            new_child(root_node, DL_RECORDER_SETTINGS_USE_AUTODELETE_NODE, strbuf.str());

        filesystem_path_t settings_pathname = get_settings_pathname();

        pugixml_helpers::xmldoc_dump_to_file(doc, settings_pathname.to_string());
    }
}

boost::int64_t recorder_settings::get_ds_threshold_kb()
{
    boost::int64_t ret_val = manual_disk_threshold_kb_;
    if (ds_threshold_auto_)
        ret_val = get_auto_disk_space_threshold();
        
    return ret_val;
}

void recorder_settings::set_ds_threshold_params(bool mode_auto, boost::uint64_t threshold_kb)
{
    ds_threshold_auto_ = mode_auto;
    manual_disk_threshold_kb_ = threshold_kb;
    if (mode_auto)
        manual_disk_threshold_kb_ = get_auto_disk_space_threshold();
}

} //recorder
} //dvblex
