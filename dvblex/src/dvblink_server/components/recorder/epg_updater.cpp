/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_message_addresses.h>
#include <dl_message_channels.h>
#include <dl_message_epg.h>
#include "epg_updater.h"
#include "recorder_database.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;

namespace dvblex { namespace recorder {

epg_updater::epg_updater(recorder_database& db, dvblink::messaging::message_queue_t& message_queue, dvblink::event& ev, map_channel_id_t& epg_update_channels) :
    is_finished_(false),
    recorder_db_(db),
    finished_event_(ev),
    exit_flag_(false),
    update_thread_(NULL),
    epg_update_channels_(epg_update_channels),
    message_queue_(message_queue)
{
    update_thread_ = new boost::thread(boost::bind(&epg_updater::update_thread_func, this));
}

epg_updater::~epg_updater()
{
    exit_flag_ = true;
    update_thread_->join();
    delete update_thread_;
}

bool epg_updater::get_epg_channels_from_server(epg_channels_list_t& epg_channel_list, channel_to_epg_source_map_t& channel_to_epg_source_map)
{
    bool ret_val = false;

    epg_channel_list.clear();

    channels::get_provider_channels_id_request req;
    channels::get_provider_channels_id_response resp;
    if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        channel_id_list_t channel_id_list;

        for (size_t i=0; i<resp.headends_.size(); i++)
        {
            for (size_t j=0; j<resp.headends_[i].transponders_.size(); j++)
            {
                for (size_t k=0; k<resp.headends_[i].transponders_[j].channels_.size(); k++)
                {
                    epg_channel ch;
                    ch.channel_id_ = resp.headends_[i].transponders_[j].channels_[k];
                    //ch.epg_channel_id_ will be filled in later
                    epg_channel_list.push_back(ch);

                    channel_id_list.push_back(ch.channel_id_);
                }
            }
        }

        epg::get_channel_epg_config_request epg_req;
        epg_req.channels_ = channel_id_list;
        epg::get_channel_epg_config_response epg_resp;
        if (message_queue_->send(epg_manager_message_queue_addressee, epg_req, epg_resp) == success &&
            epg_resp.result_)
        {
            channel_to_epg_source_map = epg_resp.epg_config_;
            for (size_t i=0; i<epg_channel_list.size(); i++)
            {
                channel_to_epg_source_map_t::iterator it = channel_to_epg_source_map.find(epg_channel_list[i].channel_id_);
                if (it != channel_to_epg_source_map.end())
                {
                    std::stringstream strbuf;
                    strbuf << it->second.epg_source_id_.get() << ":" << it->second.epg_channel_id_.get();
                    epg_channel_list[i].epg_channel_id_ = strbuf.str();
                } else
                {
                    log_warning(L"epg_updater::get_epg_channels_from_server. Channel %1% does not have any assigned epg source information") % string_cast<EC_UTF8>(epg_channel_list[i].channel_id_.get());
                }
            }
        }

        ret_val = true;

    } else
    {
        log_error(L"epg_updater_t::get_epg_channels_from_server. get_provider_channels_id_request has returned an error");
    }

    return ret_val;
}

bool epg_updater::get_channel_epg_from_server(epg_channel& channel, const epg_source_channel_id_t& epg_source, epg_event_list_t& event_list)
{
    bool ret_val = false;

    event_list.clear();

    epg::get_channel_epg_request req(epg_source.epg_channel_id_);
    epg::get_channel_epg_response resp;
    if (message_queue_->send(epg_source.epg_source_id_.get(), req, resp) == success &&
        resp.result_)
    {

        bool event_duplicates = false;
        std::map<std::string, std::string> idmap;
        while (resp.epg_.size() > 0)
        {
            epg_item epg_item;
            epg_item.event_ = resp.epg_[0];
            epg_item.make_id();
            if (idmap.find(epg_item.event_.id_) == idmap.end())
                event_list.push_back(epg_item);
            else
                event_duplicates = true;
            idmap[epg_item.event_.id_] = epg_item.event_.id_;
            
            resp.epg_.erase(resp.epg_.begin());
        }
        if (event_duplicates)
            log_warning(L"epg_updater_t::get_channel_epg_from_server. Not all of the event ids are unique for channel %1%") % string_cast<EC_UTF8>(channel.channel_id_.get());
        //no need to sort the event_list, because original epgEventList was already sorted on start time

        ret_val = true;
    } else
    {
        log_info(L"epg_updater_t::get_channel_epg_from_server. get_channel_epg_request has returned an error");
    }

    return ret_val;
}

bool epg_updater::get_current_channels_map(epg_channel_map_t& channel_map)
{
    bool ret_val = false;
    channel_map.clear();
    
    epg_channels_list_t cur_channel_list;
    if (recorder_db_.get_epg_channels(cur_channel_list))
    {
        ret_val = true;
        //make a map out of this info
        for (unsigned int i=0; i<cur_channel_list.size(); i++)
        {
            channel_map[cur_channel_list[i].channel_id_] = cur_channel_list[i];
        }
    } else
    {
        log_error(L"epg_updater_t::get_current_channels_map. Failed to get list of current epg channels from database");
    }
    
    return ret_val;
}

void epg_updater::clear_old_channels(epg_channel_map_t& current_channel_map, epg_channels_list_t& new_channel_list)
{
    //make a map out of new channels
    epg_channel_map_t new_channel_map;
    for (unsigned int i=0; i<new_channel_list.size(); i++)
        new_channel_map[new_channel_list[i].channel_id_] = new_channel_list[i];
    
    //walk through current channels and delete non existent ones
    epg_channel_map_t::iterator ch_it = current_channel_map.begin();
    while (ch_it != current_channel_map.end() && !exit_flag_)
    {
        if (new_channel_map.find(ch_it->first) == new_channel_map.end())
            recorder_db_.delete_epg_channel(ch_it->second.channel_id_);
        
        ++ch_it;
    }
}

void epg_updater::update_thread_func()
{
    time_t now; time(&now);
    epg_channel_map_t current_channel_map;
    if (get_current_channels_map(current_channel_map))
    {
        epg_channels_list_t epg_channel_list;
        channel_to_epg_source_map_t channel_to_epg_source_map;
        if (get_epg_channels_from_server(epg_channel_list, channel_to_epg_source_map))
        {
            //walk through all channels and process them one by one
            for (unsigned int i=0; i<epg_channel_list.size() && !exit_flag_; i++)
            {
                if (epg_update_channels_.size() == 0 ||
                    (epg_update_channels_.size() > 0 && epg_update_channels_.find(epg_channel_list[i].channel_id_) != epg_update_channels_.end()))
                {
                    log_info(L"epg_updater_t::update_thread_func. Updating epg data for channel %1%") % string_cast<EC_UTF8>(epg_channel_list[i].channel_id_.get());

                    //if epg_channel_id has changed or this is a newly added channel - wipe the whole epg event table out for this channel
                    bool b_clear_table = true;
                    if (current_channel_map.find(epg_channel_list[i].channel_id_) != current_channel_map.end() &&
                        boost::iequals(epg_channel_list[i].epg_channel_id_.get(), current_channel_map[epg_channel_list[i].channel_id_].epg_channel_id_.get()))
                    {
                            b_clear_table = false;
                    }
                            
                    channel_to_epg_source_map_t::iterator epg_source_it = channel_to_epg_source_map.find(epg_channel_list[i].channel_id_);
                    if (epg_source_it != channel_to_epg_source_map.end())
                    {
                        //get epg from the server for this channel
                        epg_event_list_t event_list;
                        if (get_channel_epg_from_server(epg_channel_list[i], epg_source_it->second, event_list))
                        {
                            //get data for temp table preparation.
                            //we copy all existing events that are no earlier than "now - 6 hours" and the first start time of the new list.
                            int time_from = now - DL_RECORDER_PAST_EPG_COPY_FRAME;
                            int time_to = event_list.size() == 0 ? -1 : event_list[0].event_.m_StartTime;
                            if (recorder_db_.prepare_epg_update(epg_channel_list[i].channel_id_, !b_clear_table, time_from, time_to))
                            {
                                //find the insert start index
                                int insert_start_idx = -1;
                                for (unsigned int ev_idx=0; ev_idx<event_list.size() && !exit_flag_; ev_idx++)
                                {
                                    if (event_list[ev_idx].event_.m_StartTime > now - DL_RECORDER_24_HOURS_SEC)
                                    {
                                        insert_start_idx = ev_idx;
                                        break;
                                    }
                                }
                                //insert events into database
                                if (insert_start_idx != -1)
                                {
                                    recorder_db_.add_epg_events(epg_channel_list[i], event_list, insert_start_idx);
                                }
                                //commit the update for this channel when finished
                                if (!exit_flag_)
                                {
                                    log_info(L"epg_updater_t::update_thread_func. Commiting epg update for channel %1%") % string_cast<EC_UTF8>(epg_channel_list[i].channel_id_.get());
                                    if (!recorder_db_.commit_epg_update(epg_channel_list[i]))
                                    {
                                        log_warning(L"epg_updater_t::update_thread_func. Failed to commit epg update for channel %1%") % string_cast<EC_UTF8>(epg_channel_list[i].channel_id_.get());
                                    }
                                }
                            }
                            else
                            {
                                log_error(L"epg_updater_t::update_thread_func. Failed to prepare temp epg table for channel %1%") % string_cast<EC_UTF8>(epg_channel_list[i].channel_id_.get());
                            }
                        } else
                        {
                            //there are no new events for this channel, but event table has to be cleared
                            if (b_clear_table)
                            {
                                recorder_db_.prepare_epg_update(epg_channel_list[i].channel_id_, !b_clear_table, -1, -1);
                                recorder_db_.commit_epg_update(epg_channel_list[i]);
                            }
                        }
                    } else
                    {
                        //there is no epg source for this channel
                        recorder_db_.prepare_epg_update(epg_channel_list[i].channel_id_, false, -1, -1);
                        recorder_db_.commit_epg_update(epg_channel_list[i]);
                    }
                }
            }
            //clear also all non existent channels from database (but only in case of full update (e.g. epg_update_channels_.size() == 0!)
            if (!exit_flag_ && epg_update_channels_.size() == 0)
            {
                clear_old_channels(current_channel_map, epg_channel_list);
            }
        }
        else
        {
            log_error(L"epg_updater_t::update_thread_func. Failed to get list of EPG channels from server");
        }
    }

    //if exit was not set - signal the event
    if (!exit_flag_)
    {
        finished_event_.signal();
    }

    is_finished_ = true;
    log_info(L"epg_updater_t::update_thread_func. epg updater thread has finished");
}

} //recorder
} //dvblex
