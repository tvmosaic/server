/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread/mutex.hpp>
#include <map>
#include <string>
#include <dl_types.h>
#include <dl_schedules.h>
#include "timers.h"
#include "epg.h"
#include "completed_recording.h"
#include "recorder_settings.h"

struct sqlite3;

namespace dvblex { namespace recorder {

typedef int (*recorder_execute_cb_t)(void* param, int num_values, char** values, std::map<std::string, int>& value_map);

class recorder_database
{
public:
    recorder_database();
    ~recorder_database() {term();}

    bool init(const dvblink::filesystem_path_t& recorder_directory, recorder_settings* rs);
    void term();
	void repair_database(const dvblink::filesystem_path_t& sqlite_path);
    
    bool add_schedule(const schedule_item& item);
    bool get_schedules(schedule_list_t* schedule_list, const dvblink::schedule_item_id_t& schedule_id = DL_RECORDER_INVALID_ID, bool enabled_only = true);
    bool disable_schedule(const dvblink::schedule_item_id_t& schedule_id);
    bool remove_schedule(const dvblink::schedule_item_id_t& schedule_id);
    bool update_schedule(update_schedule_item_info* update_info);
    bool update_schedule_event(const dvblink::schedule_item_id_t& schedule_id, std::string& epg_event_id, std::string& epg_event_info);

    bool add_completed_recording(completed_recording& recording, bool b_deleted_recording = false);
    bool update_thumbnail(const dvblink::timer_id_t& timer_id, std::wstring& thumb_path);
	bool update_completed_recording(const dvblink::timer_id_t& timer_id, time_t actual_start, time_t actual_stop, completed_recording_state_e state);
    bool get_completed_recordings(completed_recordings_list_t& recordings, int completed_within_hours = -1);
	bool complete_pending_completed_recordings();
    bool get_completed_recordings_for_timer(const dvblink::timer_id_t& timer_id, completed_recordings_list_t& recording_list, bool lock_db = true);
    bool get_completed_recording(const dvblink::timer_id_t& timer_id, completed_recording& recording);
    bool get_completed_recording_for_schedule(const dvblink::schedule_item_id_t& schedule_id, completed_recordings_list_t& recordings);
	bool delete_completed_recording(const dvblink::timer_id_t& timer_id);
    bool update_completed_recording_timer_id(const dvblink::timer_id_t& old_timer_id, const dvblink::timer_id_t& new_timer_id);

    bool get_deleted_recordings(completed_recordings_list_t& recordings);
	bool delete_deleted_recording(const dvblink::timer_id_t& timer_id);
    bool get_deleted_recording_for_schedule(const dvblink::schedule_item_id_t& schedule_id, completed_recordings_list_t& recordings);
    
    bool add_cancelled_timer(const timer& t);
    bool get_cancelled_timers(cancelled_timers_list_t& timer_list);

    bool get_epg_channels(epg_channels_list_t& channels_list);
    bool prepare_epg_update(dvblink::channel_id_t& channel_id, bool b_copy, int time_from, int time_to);
    bool add_epg_events(epg_channel& epg_channel, epg_event_list_t& epg_events, int start_idx = 0, int count = -1);
    bool commit_epg_update(epg_channel& epg_channel);
    bool delete_epg_channel(dvblink::channel_id_t& channel_id);
    
    bool get_epg_item(const dvblink::channel_id_t& channel_id, time_t start_time, time_t duration, epg_item& eitem);
    bool get_epg_item(const dvblink::channel_id_t& channel_id, const dvblink::epg_event_id_t& epg_event_id, epg_item& eitem);
    
    bool get_epg(const dvblink::channel_id_t& channel_id, const dvblink::epg_event_id_t& epg_event_id, epg_event_list_t& event_list);
    bool get_epg_for_keyphrase(const dvblink::channel_id_t& channel_id, std::string& keyphrase, bool new_only, epg_channel_info_map_t& epg_channel_events);
    bool browse_epg(server_channel_list_t& channel_list, int time_from, int time_to,
        std::string& keyphrase, unsigned long genre_mask, epg_level_e info_level, boost::int32_t max_count, epg_channel_info_map_t& epg_channel_events);

    bool execute(const char* sql_stmt, recorder_execute_cb_t cb, void* user_param);

protected:
    bool initialize_database();
    void create_search_columns();
    void add_disabled_to_schedule();
    void add_margins_to_schedule();
    void add_pattern_params_to_schedule();
    void add_deleted_recordings_table();
    void add_sendto_targets_to_schedule();
    void add_v2_fields_to_schedule();
    void postprocess_recordings_path(completed_recordings_list_t& recordings);
    bool get_schedule_record_anytime_flag(const dvblink::schedule_item_id_t& schedule_id);

    bool browse_epg_int(server_channel_list_t& channel_list, int time_from, int time_to,
        std::string& keyphrase, unsigned long genre_mask, epg_level_e info_level, boost::int32_t max_count, epg_channel_info_map_t& epg_channel_events);

protected:
	dvblink::filesystem_path_t recorder_db_dir_;
	sqlite3* sqlite_handle_;
    boost::mutex mutex_;
    recorder_settings* recorder_settings_;
};

} //recorder
} //dvblex
