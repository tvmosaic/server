/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include "timers.h"

namespace dvblex { namespace recorder {

class recorder_database;

class cancelled_rec_man
{
public:
    cancelled_rec_man(recorder_database& db) :
        recorder_db_(db)
    {}

    ~cancelled_rec_man() {}

    void reset();
    void add_timer(const recording_timer& timer);

    bool is_timer_cancelled(const recording_timer& timer) const;
    void remove_schedule(const dvblink::schedule_item_id_t& schedule_id);
    void get_cancelled_timers(recording_timers_list_t& timer_list) const;

private:
    typedef std::map<dvblink::timer_id_t, recording_timer> cancelled_recordings_map_t;

    recorder_database& recorder_db_;
    timers_id_map_t cancelled_timers_;
    cancelled_recordings_map_t cancelled_recordings_;
};

} //recorder
} //dvblex
