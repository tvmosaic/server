/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include "cancelled_rec_man.h"
#include "recorder_database.h"

using namespace dvblink;

namespace dvblex { namespace recorder {

void cancelled_rec_man::reset()
{
    cancelled_timers_.clear();
    cancelled_recordings_.clear();

    // read cancelled timers
    //
    cancelled_timers_list_t cancelled_timer_list;
    recorder_db_.get_cancelled_timers(cancelled_timer_list);
    get_map_from_timers_list(cancelled_timer_list, cancelled_timers_);
}

void cancelled_rec_man::add_timer(const recording_timer& timer)
{
    cancelled_timers_[timer.timer_id()] = timer.timer_id();
    cancelled_recordings_[timer.timer_id()] = timer;
}

bool cancelled_rec_man::is_timer_cancelled(const recording_timer& timer) const
{
    bool res = false;
    if (cancelled_timers_.find(timer.timer_id()) != cancelled_timers_.end())
    {
//        cancelled_recordings_[timer.timer_id()] = timer;
        res = true;
    }
    return res;
}

void cancelled_rec_man::remove_schedule(const schedule_item_id_t& schedule_id)
{
    cancelled_recordings_map_t::iterator it = cancelled_recordings_.begin();
    cancelled_recordings_map_t::iterator it_end = cancelled_recordings_.end();
    while (it != it_end)
    {
        recording_timer& rec = it->second;
        if (rec.schedule_item_id() == schedule_id)
        {
            cancelled_recordings_.erase(it);
            it = cancelled_recordings_.begin();
            it_end = cancelled_recordings_.end();
        }
        else
        {
            ++it;
        }
    }
}

void cancelled_rec_man::get_cancelled_timers(recording_timers_list_t& timer_list) const
{
    timer_list.clear();
    cancelled_recordings_map_t::const_iterator it = cancelled_recordings_.begin();
    while (it != cancelled_recordings_.end())
    {
        timer_list.push_back(it->second);
        ++it;
    }
}

} //recorder
} //dvblex
