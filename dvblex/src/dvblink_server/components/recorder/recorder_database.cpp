/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/filesystem.hpp>
#include <boost/range/algorithm/remove_if.hpp>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_utils.h>
#include <dl_filesystem_path.h>
#include "sqlite/sqlite3.h"
#include "constants.h"
#include "recorder_database.h"

using namespace dvblink; 
using namespace dvblink::engine;
using namespace dvblink::logging;

namespace dvblex { namespace recorder {

static void fill_epg_event_info(epg_item& item, epg_level_e info_level, char** values, std::map<std::string, int>& value_map);

static std::string _esc(const std::string& s)
{
    std::string out;
    const size_t len = s.size();

    for (size_t j = 0; j < len; j++)
    {
        char c = s[j];

        switch (c)
        {
        case '\'': // single quotation mark
            out.push_back('\'');
            out.push_back('\'');
            break;
        default:
            out.push_back(c);
        }
    }

    return out;
}

recorder_database::recorder_database()
    : sqlite_handle_(NULL)
{
}

bool recorder_database::init(const filesystem_path_t& recorder_directory, recorder_settings* rs)
{
    bool res = false;
    
    recorder_settings_ = rs;

	recorder_db_dir_ = recorder_directory;
    filesystem_path_t dbfile = recorder_db_dir_  / DL_RECORDER_DATABASE_FILENAME;

    bool b_file_exists = boost::filesystem::exists(dbfile.to_boost_filesystem());
    log_info(L"recorder_database_t::init(): Opening database file %1%") % dbfile.to_wstring();

    std::string dfile_str = dbfile.to_string();
    //try to open/create database
    int err = sqlite3_open(dfile_str.c_str(), &sqlite_handle_);
    if (err == SQLITE_OK)
    {
        res = true;
        if (!b_file_exists)
        {
            log_info(L"recorder_database_t::init(): Initializing newly created database");
            //this is newly created database - create its tables
            if (!initialize_database())
            {
                log_error(L"recorder_database_t::init(): Failed to initialize database");
                res = false;
            }
        }
        if (!execute("PRAGMA synchronous=NORMAL;", NULL, NULL))
            log_warning(L"recorder_database_t::init(): sqlite pragma synchronous = normal failed");
		if (!execute("PRAGMA temp_store=MEMORY;", NULL, NULL))
            log_warning(L"recorder_database_t::init(): sqlite pragma temp_store = MEMORY failed");
        if (!execute("PRAGMA journal_mode=MEMORY;", NULL, NULL))
            log_warning(L"recorder_database_t::init(): sqlite pragma journal_mode = MEMORY failed");
		if (!execute("PRAGMA cache_size=-2000;", NULL, NULL))
            log_warning(L"recorder_database_t::init(): sqlite pragma cache_size=-2000 failed");
        //create additional lower case columns for search purpose if they do not exist yet (that might happen in beta software)
        create_search_columns();
        //create additional "disabled" column for schedule in case of upgrade
        add_disabled_to_schedule();
        //create additional "margin_before" and "margin_after" columns for schedule in case of upgrade
        add_margins_to_schedule();
        //create additional "key_phrase" and "genre_mask" columns for schedule in case of upgrade
        add_pattern_params_to_schedule();
        //create additional "sendto_targets" column for schedule in case of upgrade
        add_sendto_targets_to_schedule();
        //add new flags and fields to schedule
        add_v2_fields_to_schedule();
        //create index for epg_event::channel and start_time
        execute("create index idx_epg_event on epg_event(channel, start_time);", NULL, NULL);
        //add deleted recordings table
        add_deleted_recordings_table();
    }
    else
    {
        log_error(L"recorder_database_t::init(): Error %1% opening database file %2%") % err % dbfile.c_str();
    }
    return res;
}

void recorder_database::term()
{
    if (sqlite_handle_)
    {
        sqlite3_close(sqlite_handle_);
        sqlite_handle_ = NULL;
    }
}

void recorder_database::create_search_columns()
{
    if (!execute("select name_lower_case from epg_event", NULL, NULL))
    {
        bool res = true;
        res &= execute("alter table epg_event add name_lower_case varchar(255);", NULL, NULL);
        res &= execute("alter table epg_event add short_desc_lower_case varchar(255);", NULL, NULL);
        res &= execute("alter table epg_event add second_name_lower_case varchar(255);", NULL, NULL);
        res &= execute("alter table epg_event_tmp add name_lower_case varchar(255);", NULL, NULL);
        res &= execute("alter table epg_event_tmp add short_desc_lower_case varchar(255);", NULL, NULL);
        res &= execute("alter table epg_event_tmp add second_name_lower_case varchar(255);", NULL, NULL);
        if (!res)
        {
            log_warning(L"recorder_database_t::create_search_columns. Failed to add search columns to epg event tables");
        }
    }
}

void recorder_database::add_disabled_to_schedule()
{
    if (!execute("select disabled from schedule", NULL, NULL))
    {
        bool res = execute("alter table schedule add disabled integer;", NULL, NULL);
        res &= execute("UPDATE schedule SET disabled = 0;", NULL, NULL);

        if (!res)
        {
            log_warning(L"recorder_database_t::add_disabled_to_schedule. Failed to add disabled flags to schedule table");
        }
    }
}

static int record_anytime_schedule_row_cb(void* param, int num_values, char** values, std::map<std::string, int>& value_map)
{
    bool* anytime = (bool*)param;
    if (value_map.find("series_anytime") != value_map.end() && values[value_map["series_anytime"]] != NULL)
        dvblink::engine::string_conv::apply(values[value_map["series_anytime"]], *anytime, true);
    return SQLITE_OK;
}

bool recorder_database::get_schedule_record_anytime_flag(const schedule_item_id_t& schedule_id)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    bool ret_val = true;

    std::stringstream strbuf;
	strbuf << "SELECT * FROM schedule WHERE id = " << schedule_id << ";";
    
    bool b = execute(strbuf.str().c_str(), &record_anytime_schedule_row_cb, &ret_val);
    return ret_val;
}

void recorder_database::add_v2_fields_to_schedule()
{
    if (!execute("select start_after_sec from schedule", NULL, NULL))
    {
        bool res = execute("alter table schedule add start_after_sec integer;", NULL, NULL);
        res &= execute("UPDATE schedule SET start_after_sec = -1;", NULL, NULL);
        res = execute("alter table schedule add start_before_sec integer;", NULL, NULL);
        res &= execute("UPDATE schedule SET start_before_sec = -1;", NULL, NULL);
        res = execute("alter table schedule add priority integer;", NULL, NULL);
        res &= execute("UPDATE schedule SET priority = 0;", NULL, NULL);
        res = execute("alter table schedule add active integer;", NULL, NULL);
        res &= execute("UPDATE schedule SET active = 1;", NULL, NULL);

        if (!res)
        {
            log_warning(L"recorder_database_t::add_v2_fields_to_schedule. Failed to add new flags to schedule table");
        } else 
        {
            //get all schedules and generate default start_before_sec_ and start_after_sec_ where needed
            schedule_list_t schedule_list;
            if (get_schedules(&schedule_list, DL_RECORDER_INVALID_ID, false))
            {
                for (size_t i=0; i<schedule_list.size(); i++)
                {
                    schedule_item& si = schedule_list[i];
                    if (!get_schedule_record_anytime_flag(si.schedule_item_id_))
                    {
                        si.generate_default_start_margins();

                        std::stringstream strbuf;
                        strbuf << "UPDATE schedule SET start_after_sec = " << si.start_after_sec_ << " WHERE id = " << si.schedule_item_id_ << ";";
                        res &= execute(strbuf.str().c_str(), NULL, NULL);

                    	strbuf.clear(); strbuf.str("");
                        strbuf << "UPDATE schedule SET start_before_sec = " << si.start_before_sec_ << " WHERE id = " << si.schedule_item_id_ << ";";
                        res &= execute(strbuf.str().c_str(), NULL, NULL);
                    }
                    if (!res)
                        log_warning(L"recorder_database_t::add_v2_fields_to_schedule. Failed to update schedule before/after start margins");
                }
            } else
            {
                log_warning(L"recorder_database_t::add_v2_fields_to_schedule. get_schedules failed");
            }
        }
    }
}

void recorder_database::add_sendto_targets_to_schedule()
{
    if (!execute("select sendto_targets from schedule", NULL, NULL))
    {
        bool res = execute("alter table schedule add sendto_targets varchar(255);", NULL, NULL);

        if (!res)
        {
            log_warning(L"recorder_database_t::add_sendto_targets_to_schedule. Failed to add sendto_targets to schedule table");
        }
    }
}

void recorder_database::add_margins_to_schedule()
{
    if (!execute("select margin_before from schedule", NULL, NULL))
    {
        //bool res = execute("alter table schedule add margin_before integer, add margin_after integer;", NULL, NULL);
        bool res = execute("alter table schedule add margin_before integer;", NULL, NULL);
        res &= execute("alter table schedule add margin_after integer;", NULL, NULL);

        std::stringstream strbuf;
        strbuf << "UPDATE schedule SET margin_before = " << recorder_settings_->get_before_margin() << ";";
        res &= execute(strbuf.str().c_str(), NULL, NULL);

        strbuf.clear();
        strbuf << "UPDATE schedule SET margin_after = " << recorder_settings_->get_after_margin() << ";";
        res &= execute(strbuf.str().c_str(), NULL, NULL);

        if (!res)
        {
            log_warning(L"recorder_database_t::add_margins_to_schedule. Failed to add margins to schedule table");
        }
    }
}

void recorder_database::add_pattern_params_to_schedule()
{
    if (!execute("select key_phrase from schedule", NULL, NULL))
    {
        bool res = execute("alter table schedule add genre_mask integer;", NULL, NULL);
        res &= execute("alter table schedule add key_phrase varchar(255);", NULL, NULL);
        res &= execute("UPDATE schedule SET genre_mask = 0;", NULL, NULL);

        if (!res)
        {
            log_warning(L"recorder_database_t::add_pattern_params_to_schedule. Failed to add pattern search params to schedule table");
        }
    }
}

void recorder_database::add_deleted_recordings_table()
{
    if (!execute("select * from deleted_recording", NULL, NULL))
    {
        bool res = execute(DL_RECORDER_CREATE_DLTD_REC_STMT, NULL, NULL);

        if (!res)
        {
            log_warning(L"recorder_database_t::add_deleted_recordings_table. Failed to add deleted recordings table");
        }
    }
}

bool recorder_database::initialize_database()
{
    return execute(DL_RECORDER_TABLES_CREATE_STMT, NULL, NULL) && execute(DL_RECORDER_CREATE_DLTD_REC_STMT, NULL, NULL);
}

void recorder_database::repair_database(const filesystem_path_t& sqlite_path)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

	//close database connection
	term();

#ifdef _ANDROID_ALL
    //on Android we just delete the whole database
    //it has to be changed when sqlite for android will be available
    //OR!! When we start recording on Android
    filesystem_path_t dbfile = recorder_db_dir_  / DL_RECORDER_DATABASE_FILENAME;
    try {
        boost::filesystem::remove(dbfile.to_boost_filesystem());
    } catch(...) {}
#else
	filesystem_path_t p = recorder_db_dir_ / DL_RECORDER_DATABASE_REPAIR_SCRIPT;
    filesystem_path_t dbfile = recorder_db_dir_;//  / DL_RECORDER_DATABASE_FILENAME;

	//script format is: script <sqlite exe> <db file>
	std::string str = "\"" + p.to_string() + "\" \"" + sqlite_path.to_string() + "\" \"" + dbfile.to_string() + "\"";
	filesystem_path_t script_path(str);

    log_info(L"recorder_database_t::repair_database. Executing %1%") % script_path.to_wstring();
	//execute repair script
	execute_script(script_path);
#endif
	//re-open database
	init(recorder_db_dir_, recorder_settings_);

}

typedef int (*default_sqlite_execute_callback_t)(void*,int,char**,char**);

struct recorder_internal_cb_info
{
    recorder_execute_cb_t cb;
    void* user_param;
    std::map<std::string, int> value_map;
};

static int recorder_internal_cb(void* param, int count, char** values, char** column_names)
{
    recorder_internal_cb_info* cb_info = (recorder_internal_cb_info*)param;
    if (cb_info->value_map.size() == 0 && count != 0)
    {
        //this is probably first invocation - build name-column map
        for (int i = 0; i < count; i++)
        {
            if (strlen(column_names[i]) > 0)
            {
                cb_info->value_map[column_names[i]] = i;
            }
        }
    }
    return cb_info->cb(cb_info->user_param, count, values, cb_info->value_map);
}

bool recorder_database::execute(const char* sql_stmt, recorder_execute_cb_t cb, void* user_param)
{
    recorder_internal_cb_info cb_info;
    cb_info.cb = cb;
    cb_info.user_param = user_param;
    default_sqlite_execute_callback_t func_ptr = cb == NULL ? NULL : &recorder_internal_cb;
    int ret_val = sqlite3_exec(sqlite_handle_, sql_stmt, func_ptr, &cb_info, NULL);
    return ret_val == SQLITE_ABORT || ret_val == SQLITE_OK;
}

static int last_row_id_cb(void* param, int num_values, char** values, std::map<std::string, int>& value_map)
{
    int ret_val = SQLITE_OK;
    schedule_item_id_t::type_t* id = (schedule_item_id_t::type_t*)param;
    if (num_values > 0 && values[0] != NULL)
    {
        string_conv::apply(values[0], *id, static_cast<schedule_item_id_t::type_t>(DL_RECORDER_INVALID_ID));
    }
    else
    {
        ret_val = SQLITE_ABORT;
    }
    return ret_val;
}

static std::string prepare_search_string(const std::string& str)
{
	//get only alpha-numeric characters
	std::wstring wstr = string_cast<EC_UTF8>(str);

	wstr.erase(boost::remove_if(wstr, boost::is_any_of(L"/\n\r\t\f`~?#*\\<>|\":")), wstr.end());

	return string_cast<EC_UTF8>(boost::algorithm::to_lower_copy(wstr));
}

bool recorder_database::add_schedule(const schedule_item& item)
{
    boost::unique_lock<boost::mutex> lock(mutex_);
    
    bool ret_val = false;
    int daymask = item.day_mask();
    int rec_series = item.record_series() ? 1 : 0;
    int rec_new_series = item.record_series_new_only() ? 1 : 0;
    int active = item.active_ ? 1 : 0;
    
    std::stringstream strbuf;
    strbuf << "INSERT INTO schedule (disabled, type, extra_param, channel, name, start_time, duration, margin_before, margin_after, day_mask, epg_event_id, epg_program_info, series, " \
        "series_new_only, number_to_keep, key_phrase, genre_mask, sendto_targets, start_after_sec, start_before_sec, priority, active) VALUES(";
    strbuf << "0," << item.type() << ",'" << _esc(item.extra_param()) << "','" << _esc(item.channel().get()) << "','" << _esc(item.name().to_string()) << "'," << item.main_start_time() << ",";
	strbuf << item.main_duration() << "," << item.margin_before() << "," << item.margin_after() << "," << daymask << ",'" << _esc(item.epg_event_id().get()) << "','" << _esc(item.epg_program_info()) << "'," << rec_series << ",";
    strbuf << rec_new_series << "," << item.number_of_recordings_to_keep() << ",'" << _esc(item.get_key_phrase().to_string()) << "'," << item.get_genre_mask() << ",'" << _esc(item.get_targets_as_string()) << "',";
    strbuf << item.start_after_sec_ << "," << item.start_before_sec_ << "," << item.priority_ << "," << active << ");";
    
    ret_val = execute(strbuf.str().c_str(), NULL, NULL);
    if (ret_val)
    {
        //get id       
        ret_val = execute("select last_insert_rowid()", &last_row_id_cb, const_cast<schedule_item_id_t*>(&item.schedule_item_id()));
    }
    
    return ret_val;
}

bool recorder_database::update_schedule_event(const schedule_item_id_t& schedule_id, std::string& epg_event_id, std::string& epg_event_info)
{
    boost::unique_lock<boost::mutex> lock(mutex_);
    
    bool ret_val = false;
    std::stringstream strbuf;
    strbuf << "UPDATE schedule SET epg_event_id = '" << _esc(epg_event_id) << "', epg_program_info = '" << _esc(epg_event_info) << "'";
    strbuf << " WHERE id = " << schedule_id << ";";
    
    ret_val = execute(strbuf.str().c_str(), NULL, NULL);
    return ret_val;
}

bool recorder_database::update_schedule(update_schedule_item_info* update_info)
{
    boost::unique_lock<boost::mutex> lock(mutex_);
    
    bool ret_val = false;
    int rec_new_series = update_info->record_series_new_only_ ? 1 : 0;
    int active = update_info->active_ ? 1 : 0;

    std::stringstream strbuf;
    strbuf << "UPDATE schedule SET series_new_only = " << rec_new_series << ", number_to_keep = " << update_info->number_of_recordings_to_keep_;
    strbuf << ", sendto_targets = '" << _esc(update_info->get_targets_as_string()) << "'";

    if (update_info->margin_after_ != DL_RECORDER_INVALID_TIME)
        strbuf << ", margin_after = " << update_info->margin_after_;
    if (update_info->margin_before_ != DL_RECORDER_INVALID_TIME)
        strbuf << ", margin_before = " << update_info->margin_before_;

    strbuf << ", start_after_sec = " << update_info->start_after_sec_;
    strbuf << ", start_before_sec = " << update_info->start_before_sec_;

    if (update_info->supports_v2_schedule_)
    {
        strbuf << ", day_mask = " << (int)update_info->day_mask_;
        strbuf << ", priority = " << update_info->priority_;
        strbuf << ", active = " << active;
    }

    strbuf << " WHERE id = " << update_info->schedule_item_id_ << ";";
    
    ret_val = execute(strbuf.str().c_str(), NULL, NULL);
    return ret_val;
}

static int schedule_row_cb(void* param, int num_values, char** values, std::map<std::string, int>& value_map)
{
    schedule_list_t* schedule_list = (schedule_list_t*)param;
    
    schedule_item item;
    item.set(values, value_map);

    schedule_list->push_back(item);
    
    return SQLITE_OK;
}

bool recorder_database::get_schedules(schedule_list_t* schedule_list, const schedule_item_id_t& schedule_id/* = DL_RECORDER_INVALID_ID*/, bool enabled_only/* = true */)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    bool ret_val = false;
    std::stringstream strbuf;
	strbuf << "SELECT * FROM schedule ";
    if (enabled_only || schedule_id != DL_RECORDER_INVALID_ID)
    {
	    strbuf << " WHERE ";
    }

    bool prev = false;
    if (enabled_only)
    {
	    strbuf << " disabled = 0 ";
        prev = true;
    }

	if (schedule_id != DL_RECORDER_INVALID_ID)
    {
        if (prev)
        {
	        strbuf << " AND ";
        }
	    strbuf << " id = " << schedule_id;
    }
	strbuf << ";";
    
    ret_val = execute(strbuf.str().c_str(), &schedule_row_cb, schedule_list);
    return ret_val;
}

bool recorder_database::disable_schedule(const schedule_item_id_t& schedule_id)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    std::stringstream strbuf;
    //remove cancelled timers first (if any)
    strbuf << "DELETE FROM cancelled_timer WHERE schedule_id = " << schedule_id << ";";
    execute(strbuf.str().c_str(), NULL, NULL);

    //remove the schedule itself as well
	strbuf.clear(); strbuf.str("");
    strbuf << "UPDATE schedule SET disabled = 1 WHERE id = " << schedule_id << ";";

    execute(strbuf.str().c_str(), NULL, NULL);
    return true;
}

bool recorder_database::remove_schedule(const schedule_item_id_t& schedule_id)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    std::stringstream strbuf;
    //remove cancelled timers first (if any)
    strbuf << "DELETE FROM cancelled_timer WHERE schedule_id = " << schedule_id << ";";
    execute(strbuf.str().c_str(), NULL, NULL);

    //remove the schedule itself as well
	strbuf.clear(); strbuf.str("");
    strbuf << "DELETE FROM schedule WHERE id = " << schedule_id << ";";

    execute(strbuf.str().c_str(), NULL, NULL);
    return true;
}

bool recorder_database::add_completed_recording(completed_recording& recording, bool b_deleted_recording)
{
    std::string table_name = "deleted_recording";
    
    if (!b_deleted_recording)
    {
        mutex_.lock();
        table_name = "completed_recording";        
    }

    bool ret_val = false;
    int recording_state = recording.state();
    
    std::stringstream strbuf;
    strbuf << "INSERT INTO " << table_name.c_str() << " (schedule_id, timer_id, filename, state, ";
    strbuf << "channel_id, channel_num, channel_subnum, channel_name, ";
    strbuf << "name, short_desc, start_time, duration, ";
    strbuf << "second_name, language, actors, directors, writers, producers, guests, image_url, ";
    strbuf << "year, episode_num, season_num, star_num, star_max, categories, is_hdtv, is_premiere, ";
    strbuf << "is_repeat, is_action, is_comedy, is_doc, is_drama, is_edu, is_horror, is_kids, is_movie, is_music, ";
    strbuf << "is_news, is_reality, is_romance, is_scifi, is_serial, is_soap, is_special, is_sports, is_thriller, is_adult ";	
	strbuf << ") VALUES(";
    strbuf << recording.schedule_item_id() << ",'" << _esc(recording.timer_id().get()) << "','" << _esc(recording.filename().to_string()) << "',";
    strbuf << recording_state << ",'";
    strbuf << _esc(recording.channel().id_.get()) << "'," << recording.channel().number_.get() << "," << recording.channel().sub_number_.get() << ",'" << _esc(recording.channel().name_.to_string()) << "','";
    strbuf << _esc(recording.event_info().m_Name) << "','";
    strbuf << _esc(recording.event_info().m_ShortDesc) << "'," << recording.event_info().m_StartTime << "," << recording.event_info().m_Duration << ",'";
	strbuf << _esc(recording.event_info().m_SecondName) << "','" << _esc(recording.event_info().m_Language) << "','" << _esc(recording.event_info().m_Actors) << "','";
	strbuf << _esc(recording.event_info().m_Directors) << "','" << _esc(recording.event_info().m_Writers) << "','" << _esc(recording.event_info().m_Producers) << "','";
	strbuf << _esc(recording.event_info().m_Guests) << "','" << _esc(recording.event_info().m_ImageURL) << "'," << recording.event_info().m_Year << ",";
	strbuf << recording.event_info().m_EpisodeNum << "," << recording.event_info().m_SeasonNum << "," << recording.event_info().m_StarNum << ",";
	strbuf << recording.event_info().m_StarNumMax << ",'" << _esc(recording.event_info().m_Categories) << "'," << (int)recording.event_info().m_IsHDTV << ",";
	strbuf << (int)recording.event_info().m_IsPremiere << "," << (int)recording.event_info().m_IsRepeatFlag << "," << (int)recording.event_info().m_IsAction << ",";
	strbuf << (int)recording.event_info().m_IsComedy << "," << (int)recording.event_info().m_IsDocumentary << "," << (int)recording.event_info().m_IsDrama << ",";
	strbuf << (int)recording.event_info().m_IsEducational << "," << (int)recording.event_info().m_IsHorror << "," << (int)recording.event_info().m_IsKids << ",";
	strbuf << (int)recording.event_info().m_IsMovie << "," << (int)recording.event_info().m_IsMusic << "," << (int)recording.event_info().m_IsNews << ",";
	strbuf << (int)recording.event_info().m_IsReality << "," << (int)recording.event_info().m_IsRomance  << "," << (int)recording.event_info().m_IsScienceFiction << ",";
	strbuf << (int)recording.event_info().m_IsSerial << "," << (int)recording.event_info().m_IsSoap << "," << recording.event_info().m_IsSpecial << ",";
	strbuf << (int)recording.event_info().m_IsSports << "," << (int)recording.event_info().m_IsThriller << "," << (int)recording.event_info().m_IsAdult << ");";
    
    ret_val = execute(strbuf.str().c_str(), NULL, NULL);

    if (!b_deleted_recording)
        mutex_.unlock();

    return ret_val;
}

bool recorder_database::complete_pending_completed_recordings()
{
    boost::unique_lock<boost::mutex> lock(mutex_);

	bool ret_val = false;
    std::stringstream strbuf;

	//update duration first
    strbuf << "UPDATE completed_recording SET duration = 60 WHERE duration = -1;";
    ret_val = execute(strbuf.str().c_str(), NULL, NULL);

	strbuf.clear(); strbuf.str("");
    strbuf << "UPDATE completed_recording SET state = " << creForcedToCompletion << " WHERE state = " << creInProgress << ";";

    ret_val = execute(strbuf.str().c_str(), NULL, NULL);

    return ret_val;
}

bool recorder_database::update_completed_recording(const timer_id_t& timer_id, time_t actual_start, time_t actual_stop, completed_recording_state_e state)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

	bool ret_val = false;
	int duration = (actual_stop - actual_start) > 0 ? actual_stop - actual_start : 60; //to be on the safe side

    std::stringstream strbuf;
    strbuf << "UPDATE completed_recording SET start_time = " << actual_start << ", duration = " << duration;
	strbuf << ", state = " << state << " WHERE timer_id = '" << _esc(timer_id.get()) << "';";
    
    ret_val = execute(strbuf.str().c_str(), NULL, NULL);

    return ret_val;
}

bool recorder_database::update_thumbnail(const timer_id_t& timer_id, std::wstring& thumb_path)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

	bool ret_val = false;
    std::string thumb_path_str = string_cast<EC_UTF8>(thumb_path);

    std::stringstream strbuf;
    strbuf << "UPDATE completed_recording SET image_url = '" << _esc(thumb_path_str) << "'";
	strbuf << " WHERE timer_id = '" << _esc(timer_id.get()) << "';";
    
    ret_val = execute(strbuf.str().c_str(), NULL, NULL);

    return ret_val;
}

static int completed_recording_row_cb(void* param, int num_values, char** values, std::map<std::string, int>& value_map)
{
    completed_recordings_list_t* recordings = (completed_recordings_list_t*)param;
    
    completed_recording item;
    item.set(values, value_map);

	//read event info
	epg_item eitem;
	fill_epg_event_info(eitem, epg_full_info, values, value_map);
    item.set_epg_info(eitem.event_);

    recordings->push_back(item);
    return SQLITE_OK;
}

void recorder_database::postprocess_recordings_path(completed_recordings_list_t& recordings)
{
	filesystem_path_t record_path = recorder_settings_->get_record_path();
	filesystem_path_t thumbnail_path = recorder_settings_->get_thumbnail_path();
	
	for (size_t i=0; i<recordings.size(); i++)
	{
        if (!recordings[i].filename().empty())
        {
            boost::filesystem::path p_file(recordings[i].filename().to_boost_filesystem());
		    recordings[i].set_filename(record_path / p_file.filename());
        }

		if (recordings[i].event_info().m_ImageURL.size() > 0)
		{
#ifdef WIN32
			boost::filesystem::path p_thumb(string_cast<EC_UTF8>(recordings[i].event_info().m_ImageURL));
#else
			boost::filesystem::path p_thumb(recordings[i].event_info().m_ImageURL);
#endif
			filesystem_path_t thumb = thumbnail_path / p_thumb.filename();
			recordings[i].event_info().m_ImageURL = thumb.to_string();
		}
	}
}

bool recorder_database::get_completed_recordings(completed_recordings_list_t& recordings, int completed_within_hours/* = -1*/)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    bool ret_val = false;
    recordings.clear();
    std::stringstream strbuf;
	strbuf << "SELECT * FROM completed_recording";
	if (completed_within_hours != -1)
	{
        time_t now; time(&now);
        now -= completed_within_hours * 3600;
	    strbuf << " WHERE end_time >= " << now;
    }
	strbuf << " ORDER BY start_time;";
    ret_val = execute(strbuf.str().c_str(), &completed_recording_row_cb, &recordings);
	if (ret_val)
		postprocess_recordings_path(recordings);
    return ret_val;
}

bool recorder_database::update_completed_recording_timer_id(const timer_id_t& old_timer_id, const timer_id_t& new_timer_id)
{
    std::stringstream strbuf;
    strbuf << "UPDATE completed_recording SET timer_id = '" << new_timer_id.to_string() << "' WHERE timer_id = '" << old_timer_id.to_string() << "';";
    return execute(strbuf.str().c_str(), NULL, NULL);
}

bool recorder_database::get_completed_recording(const timer_id_t& timer_id, completed_recording& recording)
{
	completed_recordings_list_t recordings;
	bool res = get_completed_recordings_for_timer(timer_id, recordings);
	if (res && recordings.size() == 1)
	{
		recording = recordings[0];
	}
    else
	{
		res = false;
	}
    return res;
}

bool recorder_database::get_deleted_recordings(completed_recordings_list_t& recordings)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    bool ret_val = false;
    recordings.clear();
    std::stringstream strbuf;
	strbuf << "SELECT * FROM deleted_recording ORDER BY start_time;";
    ret_val = execute(strbuf.str().c_str(), &completed_recording_row_cb, &recordings);
	if (ret_val)
		postprocess_recordings_path(recordings);

    return ret_val;
}

bool recorder_database::get_completed_recordings_for_timer(const timer_id_t& timer_id, completed_recordings_list_t& recording_list, bool lock_db)
{
    if (lock_db)
        mutex_.lock();

    std::stringstream strbuf;
	strbuf << "SELECT * FROM completed_recording";
    strbuf << " WHERE timer_id = '" << timer_id.get() << "'";
	strbuf << " ORDER BY start_time;";
    
    bool res = execute(strbuf.str().c_str(), &completed_recording_row_cb, &recording_list);

    if (lock_db)
        mutex_.unlock();

	if (res)
		postprocess_recordings_path(recording_list);

    return res;
}

bool recorder_database::get_completed_recording_for_schedule(const schedule_item_id_t& schedule_id, completed_recordings_list_t& recordings)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    recordings.clear();
    std::stringstream strbuf;
	strbuf << "SELECT * FROM completed_recording";
    if (schedule_id != DL_RECORDER_INVALID_SCHEDULE_ID)
    {
        strbuf << " WHERE schedule_id = " << schedule_id;
    }
	strbuf << " ORDER BY start_time;";
    bool ret_val = execute(strbuf.str().c_str(), &completed_recording_row_cb, &recordings);
	if (ret_val)
		postprocess_recordings_path(recordings);
	return ret_val;
}

bool recorder_database::get_deleted_recording_for_schedule(const schedule_item_id_t& schedule_id, completed_recordings_list_t& recordings)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    recordings.clear();
    std::stringstream strbuf;
	strbuf << "SELECT * FROM deleted_recording";
    if (schedule_id != DL_RECORDER_INVALID_SCHEDULE_ID)
    {
        strbuf << " WHERE schedule_id = " << schedule_id;
    }
	strbuf << " ORDER BY start_time;";
    bool ret_val = execute(strbuf.str().c_str(), &completed_recording_row_cb, &recordings);

	if (ret_val)
		postprocess_recordings_path(recordings);

	return ret_val;
}

bool recorder_database::delete_deleted_recording(const timer_id_t& timer_id)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    std::stringstream strbuf;
    strbuf << "DELETE FROM deleted_recording WHERE timer_id = '" << timer_id.get() << "';";
    return execute(strbuf.str().c_str(), NULL, NULL);
}

bool recorder_database::delete_completed_recording(const timer_id_t& timer_id)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    //delete completed recording actually moves recording recordin in db to deleted recordings table
	completed_recordings_list_t recording_list;
    get_completed_recordings_for_timer(timer_id, recording_list, false);
    for (size_t i=0; i<recording_list.size(); i++)
    {
        add_completed_recording(recording_list[i], true);
    }

    //delete recordings from completed_recording table
    std::stringstream strbuf;
    strbuf << "DELETE FROM completed_recording WHERE timer_id = '" << timer_id.get() << "';";
    return execute(strbuf.str().c_str(), NULL, NULL);
}

bool recorder_database::add_cancelled_timer(const timer& t)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    std::stringstream strbuf;
    strbuf << "INSERT INTO cancelled_timer (schedule_id, timer_id) VALUES(";
    strbuf << t.schedule_item_id() << ",'" << _esc(t.timer_id().get()) << "');";
    
    return execute(strbuf.str().c_str(), NULL, NULL);
}

static int cancelled_timer_row_cb(void* param, int num_values, char** values, std::map<std::string, int>& value_map)
{
    cancelled_timers_list_t* timers = (cancelled_timers_list_t*)param;

    timer item;
    if (value_map.find("schedule_id") != value_map.end() && values[value_map["schedule_id"]] != NULL)
    {
        schedule_item_id_t::type_t temp = item.schedule_item_id().get();
        string_conv::apply(values[value_map["schedule_id"]], temp, static_cast<schedule_item_id_t::type_t>(DL_RECORDER_INVALID_ID));
        item.set_schedule_item_id(temp);
    }

    if (value_map.find("timer_id") != value_map.end() && values[value_map["timer_id"]] != NULL)
    {
        item.set_timer_id(values[value_map["timer_id"]]);
    }

    timers->push_back(item);
    return SQLITE_OK;
}

bool recorder_database::get_cancelled_timers(cancelled_timers_list_t& timer_list)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    timer_list.clear();
    std::stringstream strbuf;
	strbuf << "SELECT * FROM cancelled_timer;";
    return execute(strbuf.str().c_str(), &cancelled_timer_row_cb, &timer_list);
}

static int epg_channel_row_cb(void* param, int num_values, char** values, std::map<std::string, int>& value_map)
{
    epg_channels_list_t* channel_list = (epg_channels_list_t*)param;
    epg_channel item;
    if (value_map.find("channel") != value_map.end() && values[value_map["channel"]] != NULL)
    {
        item.channel_id_ = values[value_map["channel"]];
    }
    if (value_map.find("epg_channel_id") != value_map.end() && values[value_map["epg_channel_id"]] != NULL)
    {
        item.epg_channel_id_ = values[value_map["epg_channel_id"]];
    }
    channel_list->push_back(item);
    return SQLITE_OK;
}

bool recorder_database::get_epg_channels(epg_channels_list_t& channels_list)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    channels_list.clear();
    std::stringstream strbuf;
	strbuf << "SELECT * FROM epg_channel;";
    return execute(strbuf.str().c_str(), &epg_channel_row_cb, &channels_list);
}

bool recorder_database::prepare_epg_update(channel_id_t& channel_id, bool copy, int time_from, int time_to)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    //delete all info in the temp table
    bool res = execute("DELETE FROM epg_event_tmp", NULL, NULL);
    if (res)
    {
        //copy contents from the existing events table into temp one
        if (copy)
        {
            std::stringstream strbuf;
	        strbuf.clear(); strbuf.str("");
	        strbuf << "INSERT INTO epg_event_tmp (";
			strbuf << "event_id, channel, name, name_lower_case, short_desc, short_desc_lower_case, start_time, duration, second_name, second_name_lower_case, language, actors, directors, writers, producers, guests, image_url, ";
			strbuf << "year, episode_num, season_num, star_num, star_max, categories, is_hdtv, is_premiere, is_repeat, is_action, is_comedy, is_doc, ";
			strbuf << "is_drama, is_edu, is_horror, is_kids, is_movie, is_music, is_news, is_reality, is_romance, is_scifi, is_serial, is_soap, ";
			strbuf << "is_special, is_sports, is_thriller, is_adult ";
			strbuf << ") SELECT ";
			strbuf << "event_id, channel, name, name_lower_case, short_desc, short_desc_lower_case, start_time, duration, second_name, second_name_lower_case, language, actors, directors, writers, producers, guests, image_url, ";
			strbuf << "year, episode_num, season_num, star_num, star_max, categories, is_hdtv, is_premiere, is_repeat, is_action, is_comedy, is_doc, ";
			strbuf << "is_drama, is_edu, is_horror, is_kids, is_movie, is_music, is_news, is_reality, is_romance, is_scifi, is_serial, is_soap, ";
			strbuf << "is_special, is_sports, is_thriller, is_adult ";
			strbuf << "FROM epg_event WHERE channel = '" << _esc(channel_id.get());
	        strbuf << "' AND start_time > " << time_from;
	        if (time_to != -1)
            {
	            strbuf << " AND start_time < " << time_to;
            }
            strbuf << ";";
            res = execute(strbuf.str().c_str(), NULL, NULL);
        }
    }
    return res;
}

bool recorder_database::add_epg_events(epg_channel& epg_channel, epg_event_list_t& epg_events, int start_idx /*= 0*/, int count /*= -1*/)
{
    if (start_idx < 0 || (epg_events.size() > 0 && start_idx > (int)epg_events.size() - 1))
    {
        return false;
    }
    if (count < 0)
    {
        count = epg_events.size() - start_idx;
    }
    if (start_idx + count > (int)epg_events.size())
    {
        return false;
    }

	std::stringstream strbuf;
    int event_count = 0;
    bool res = true;
    for (size_t i = start_idx; i < (size_t)(start_idx + count) && res; i++)
    {
		std::string name_lower_case = prepare_search_string(epg_events[i].event_.m_Name);
		std::string second_name_lower_case = prepare_search_string(epg_events[i].event_.m_SecondName);
		std::string short_desc_lower_case = prepare_search_string(epg_events[i].event_.m_ShortDesc);
        if (event_count == 0)
        {
	        strbuf.clear(); strbuf.str("");
            strbuf << "INSERT INTO epg_event_tmp (event_id, channel, name, name_lower_case, short_desc, short_desc_lower_case, start_time, duration, ";
            strbuf << "second_name, second_name_lower_case, language, actors, directors, writers, producers, guests, image_url, ";
            strbuf << "year, episode_num, season_num, star_num, star_max, categories, is_hdtv, is_premiere, ";
            strbuf << "is_repeat, is_action, is_comedy, is_doc, is_drama, is_edu, is_horror, is_kids, is_movie, is_music, ";
            strbuf << "is_news, is_reality, is_romance, is_scifi, is_serial, is_soap, is_special, is_sports, is_thriller, is_adult) VALUES ";
        }
	    strbuf << "( '";
        strbuf << _esc(epg_events[i].event_.id_) << "','" << _esc(epg_channel.channel_id_.get()) << "','" << _esc(epg_events[i].event_.m_Name) << "','" << _esc(name_lower_case) << "','";
        strbuf << _esc(epg_events[i].event_.m_ShortDesc) << "','" << _esc(short_desc_lower_case) <<  "'," << epg_events[i].event_.m_StartTime << "," << epg_events[i].event_.m_Duration << ",'";
	    strbuf << _esc(epg_events[i].event_.m_SecondName) << "','" << _esc(second_name_lower_case) << "','" << _esc(epg_events[i].event_.m_Language) << "','" << _esc(epg_events[i].event_.m_Actors) << "','";
	    strbuf << _esc(epg_events[i].event_.m_Directors) << "','" << _esc(epg_events[i].event_.m_Writers) << "','" << _esc(epg_events[i].event_.m_Producers) << "','";
	    strbuf << _esc(epg_events[i].event_.m_Guests) << "','" << _esc(epg_events[i].event_.m_ImageURL) << "'," << epg_events[i].event_.m_Year << ",";
	    strbuf << epg_events[i].event_.m_EpisodeNum << "," << epg_events[i].event_.m_SeasonNum << "," << epg_events[i].event_.m_StarNum << ",";
	    strbuf << epg_events[i].event_.m_StarNumMax << ",'" << _esc(epg_events[i].event_.m_Categories) << "'," << (int)epg_events[i].event_.m_IsHDTV << ",";
	    strbuf << (int)epg_events[i].event_.m_IsPremiere << "," << (int)epg_events[i].event_.m_IsRepeatFlag << "," << (int)epg_events[i].event_.m_IsAction << ",";
	    strbuf << (int)epg_events[i].event_.m_IsComedy << "," << (int)epg_events[i].event_.m_IsDocumentary << "," << (int)epg_events[i].event_.m_IsDrama << ",";
	    strbuf << (int)epg_events[i].event_.m_IsEducational << "," << (int)epg_events[i].event_.m_IsHorror << "," << (int)epg_events[i].event_.m_IsKids << ",";
	    strbuf << (int)epg_events[i].event_.m_IsMovie << "," << (int)epg_events[i].event_.m_IsMusic << "," << (int)epg_events[i].event_.m_IsNews << ",";
	    strbuf << (int)epg_events[i].event_.m_IsReality << "," << (int)epg_events[i].event_.m_IsRomance  << "," << (int)epg_events[i].event_.m_IsScienceFiction << ",";
	    strbuf << (int)epg_events[i].event_.m_IsSerial << "," << (int)epg_events[i].event_.m_IsSoap << "," << epg_events[i].event_.m_IsSpecial << ",";
	    strbuf << (int)epg_events[i].event_.m_IsSports << "," << (int)epg_events[i].event_.m_IsThriller << "," << (int)epg_events[i].event_.m_IsAdult<< ")";

        //define what to do with the buffer
        if (event_count == 100 || i == (size_t)(start_idx + count - 1))
        {
            //execute this transaction
	        strbuf << ";";
            {
                boost::unique_lock<boost::mutex> lock(mutex_);
                res &= execute(strbuf.str().c_str(), NULL, NULL);
            }
            //restart building a new statement
	        strbuf.clear(); strbuf.str("");
            event_count = 0;
        }
        else
        {
            event_count += 1;
            //add , and continue
	        strbuf << ", ";
        }
    }

    if (!res)
    {
        log_error(L"recorder_database_t::add_epg_events(): INSERT failed");
    }

    return res;
}

bool recorder_database::commit_epg_update(epg_channel& epg_channel)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    std::stringstream strbuf;
    //delete all events of this channel from the actual epg table
    strbuf.clear(); strbuf.str("");
    strbuf << "DELETE FROM epg_event WHERE channel = '" << _esc(epg_channel.channel_id_.get()) << "';";
    bool res = execute(strbuf.str().c_str(), NULL, NULL);
    if (res)
    {
        //delete-add channel
        strbuf.clear(); strbuf.str("");
        strbuf << "DELETE FROM epg_channel WHERE channel = '" << _esc(epg_channel.channel_id_.get()) << "';";
        res = execute(strbuf.str().c_str(), NULL, NULL);

        strbuf.clear(); strbuf.str("");
        strbuf << "insert INTO epg_channel (channel, epg_channel_id) VALUES( '";
        strbuf << _esc(epg_channel.channel_id_.get()) << "','" << epg_channel.epg_channel_id_.to_string() << "');";
        res = execute(strbuf.str().c_str(), NULL, NULL);

        //copy contents from the temp events table into actual one
        strbuf.clear(); strbuf.str("");
        strbuf << "INSERT INTO epg_event ( ";
		strbuf << "event_id, channel, name, name_lower_case, short_desc, short_desc_lower_case, start_time, duration, second_name, second_name_lower_case, language, actors, directors, writers, producers, guests, image_url, ";
		strbuf << "year, episode_num, season_num, star_num, star_max, categories, is_hdtv, is_premiere, is_repeat, is_action, is_comedy, is_doc, ";
		strbuf << "is_drama, is_edu, is_horror, is_kids, is_movie, is_music, is_news, is_reality, is_romance, is_scifi, is_serial, is_soap, ";
		strbuf << "is_special, is_sports, is_thriller, is_adult ";
		strbuf << ") SELECT ";
		strbuf << "event_id, channel, name, name_lower_case, short_desc, short_desc_lower_case, start_time, duration, second_name, second_name_lower_case, language, actors, directors, writers, producers, guests, image_url, ";
		strbuf << "year, episode_num, season_num, star_num, star_max, categories, is_hdtv, is_premiere, is_repeat, is_action, is_comedy, is_doc, ";
		strbuf << "is_drama, is_edu, is_horror, is_kids, is_movie, is_music, is_news, is_reality, is_romance, is_scifi, is_serial, is_soap, ";
		strbuf << "is_special, is_sports, is_thriller, is_adult ";
		strbuf << "FROM epg_event_tmp WHERE channel = '" << _esc(epg_channel.channel_id_.get()) << "';";

        res = execute(strbuf.str().c_str(), NULL, NULL);
    }
    return res;
}

bool recorder_database::delete_epg_channel(channel_id_t& channel_id)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    bool ret_val = false;
    
    std::stringstream strbuf;
    //delete all events of this channel from the actual epg table
    strbuf.clear(); strbuf.str("");
    strbuf << "DELETE FROM epg_event WHERE channel = '" << _esc(channel_id.get()) << "';";
    ret_val = execute(strbuf.str().c_str(), NULL, NULL);

    //delete channel
    strbuf.clear(); strbuf.str("");
    strbuf << "DELETE FROM epg_channel WHERE channel = '" << _esc(channel_id.get()) << "';";
    ret_val = execute(strbuf.str().c_str(), NULL, NULL);
    
    return ret_val;
}

static void fill_epg_event_info(epg_item& item, epg_level_e info_level, char** values, std::map<std::string, int>& value_map)
{
	//event id, name, start time and duration are always filled. The rest depends on the info_level flag
	if (value_map.find("event_id") != value_map.end() && values[value_map["event_id"]] != NULL)
        item.event_.id_ = values[value_map["event_id"]];

    if (value_map.find("name") != value_map.end() && values[value_map["name"]] != NULL)
        item.event_.m_Name = values[value_map["name"]];

    if (value_map.find("start_time") != value_map.end() && values[value_map["start_time"]] != NULL)
        string_conv::apply<int, time_t>(values[value_map["start_time"]], item.event_.m_StartTime, 0);

    if (value_map.find("duration") != value_map.end() && values[value_map["duration"]] != NULL)
        string_conv::apply<int, time_t>(values[value_map["duration"]], item.event_.m_Duration, 0);

	if (value_map.find("is_action") != value_map.end() && values[value_map["is_action"]] != NULL)
		string_conv::apply(values[value_map["is_action"]], item.event_.m_IsAction, false);

	if (value_map.find("is_comedy") != value_map.end() && values[value_map["is_comedy"]] != NULL)
		string_conv::apply(values[value_map["is_comedy"]], item.event_.m_IsComedy, false);

	if (value_map.find("is_doc") != value_map.end() && values[value_map["is_doc"]] != NULL)
		string_conv::apply(values[value_map["is_doc"]], item.event_.m_IsDocumentary, false);

	if (value_map.find("is_drama") != value_map.end() && values[value_map["is_drama"]] != NULL)
		string_conv::apply(values[value_map["is_drama"]], item.event_.m_IsDrama, false);

	if (value_map.find("is_edu") != value_map.end() && values[value_map["is_edu"]] != NULL)
		string_conv::apply(values[value_map["is_edu"]], item.event_.m_IsEducational, false);

	if (value_map.find("is_horror") != value_map.end() && values[value_map["is_horror"]] != NULL)
		string_conv::apply(values[value_map["is_horror"]], item.event_.m_IsHorror, false);

	if (value_map.find("is_kids") != value_map.end() && values[value_map["is_kids"]] != NULL)
		string_conv::apply(values[value_map["is_kids"]], item.event_.m_IsKids, false);

	if (value_map.find("is_movie") != value_map.end() && values[value_map["is_movie"]] != NULL)
		string_conv::apply(values[value_map["is_movie"]], item.event_.m_IsMovie, false);

	if (value_map.find("is_music") != value_map.end() && values[value_map["is_music"]] != NULL)
		string_conv::apply(values[value_map["is_music"]], item.event_.m_IsMusic, false);

	if (value_map.find("is_news") != value_map.end() && values[value_map["is_news"]] != NULL)
		string_conv::apply(values[value_map["is_news"]], item.event_.m_IsNews, false);

	if (value_map.find("is_reality") != value_map.end() && values[value_map["is_reality"]] != NULL)
		string_conv::apply(values[value_map["is_reality"]], item.event_.m_IsReality, false);

	if (value_map.find("is_romance") != value_map.end() && values[value_map["is_romance"]] != NULL)
		string_conv::apply(values[value_map["is_romance"]], item.event_.m_IsRomance, false);

	if (value_map.find("is_scifi") != value_map.end() && values[value_map["is_scifi"]] != NULL)
		string_conv::apply(values[value_map["is_scifi"]], item.event_.m_IsScienceFiction, false);

	if (value_map.find("is_serial") != value_map.end() && values[value_map["is_serial"]] != NULL)
		string_conv::apply(values[value_map["is_serial"]], item.event_.m_IsSerial, false);

	if (value_map.find("is_soap") != value_map.end() && values[value_map["is_soap"]] != NULL)
		string_conv::apply(values[value_map["is_soap"]], item.event_.m_IsSoap, false);

	if (value_map.find("is_special") != value_map.end() && values[value_map["is_special"]] != NULL)
		string_conv::apply(values[value_map["is_special"]], item.event_.m_IsSpecial, false);

	if (value_map.find("is_sports") != value_map.end() && values[value_map["is_sports"]] != NULL)
		string_conv::apply(values[value_map["is_sports"]], item.event_.m_IsSports, false);

	if (value_map.find("is_thriller") != value_map.end() && values[value_map["is_thriller"]] != NULL)
		string_conv::apply(values[value_map["is_thriller"]], item.event_.m_IsThriller, false);

	if (value_map.find("is_adult") != value_map.end() && values[value_map["is_adult"]] != NULL)
		string_conv::apply(values[value_map["is_adult"]], item.event_.m_IsAdult, false);

	if (value_map.find("is_hdtv") != value_map.end() && values[value_map["is_hdtv"]] != NULL)
		string_conv::apply(values[value_map["is_hdtv"]], item.event_.m_IsHDTV, false);

	if (value_map.find("is_premiere") != value_map.end() && values[value_map["is_premiere"]] != NULL)
		string_conv::apply(values[value_map["is_premiere"]], item.event_.m_IsPremiere, false);

	if (value_map.find("is_repeat") != value_map.end() && values[value_map["is_repeat"]] != NULL)
		string_conv::apply(values[value_map["is_repeat"]], item.event_.m_IsRepeatFlag, false);

	if (value_map.find("second_name") != value_map.end() && values[value_map["second_name"]] != NULL)
		item.event_.m_SecondName = values[value_map["second_name"]];

	if (value_map.find("episode_num") != value_map.end() && values[value_map["episode_num"]] != NULL)
		string_conv::apply(values[value_map["episode_num"]], item.event_.m_EpisodeNum, long(0));

	if (value_map.find("season_num") != value_map.end() && values[value_map["season_num"]] != NULL)
		string_conv::apply(values[value_map["season_num"]], item.event_.m_SeasonNum, long(0));

	if (value_map.find("short_desc") != value_map.end() && values[value_map["short_desc"]] != NULL)
		item.event_.m_ShortDesc = values[value_map["short_desc"]];

	if (value_map.find("language") != value_map.end() && values[value_map["language"]] != NULL)
		item.event_.m_Language = values[value_map["language"]];

	if (value_map.find("actors") != value_map.end() && values[value_map["actors"]] != NULL)
		item.event_.m_Actors = values[value_map["actors"]];

	if (value_map.find("directors") != value_map.end() && values[value_map["directors"]] != NULL)
		item.event_.m_Directors = values[value_map["directors"]];

	if (value_map.find("writers") != value_map.end() && values[value_map["writers"]] != NULL)
		item.event_.m_Writers = values[value_map["writers"]];

	if (value_map.find("producers") != value_map.end() && values[value_map["producers"]] != NULL)
		item.event_.m_Producers = values[value_map["producers"]];

	if (value_map.find("guests") != value_map.end() && values[value_map["guests"]] != NULL)
		item.event_.m_Guests = values[value_map["guests"]];

	if (value_map.find("image_url") != value_map.end() && values[value_map["image_url"]] != NULL)
		item.event_.m_ImageURL = values[value_map["image_url"]];

	if (value_map.find("year") != value_map.end() && values[value_map["year"]] != NULL)
		string_conv::apply(values[value_map["year"]], item.event_.m_Year, long(0));

	if (value_map.find("star_num") != value_map.end() && values[value_map["star_num"]] != NULL)
		string_conv::apply(values[value_map["star_num"]], item.event_.m_StarNum, long(0));

	if (value_map.find("star_max") != value_map.end() && values[value_map["star_max"]] != NULL)
		string_conv::apply(values[value_map["star_max"]], item.event_.m_StarNumMax, long(0));

	if (value_map.find("categories") != value_map.end() && values[value_map["categories"]] != NULL)
		item.event_.m_Categories = values[value_map["categories"]];
}

static int epg_event_row_cb(void* param, int num_values, char** values, std::map<std::string, int>& value_map)
{
    epg_event_list_t* event_list = (epg_event_list_t*)param;
    epg_item item;
    fill_epg_event_info(item, epg_full_info, values, value_map);
    event_list->push_back(item);
    return SQLITE_OK;
}

bool recorder_database::get_epg_item(const channel_id_t& channel_id, time_t start_time, time_t duration, epg_item& epg_item)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    bool ret_val = false;
    std::stringstream strbuf;
	strbuf << "SELECT * FROM epg_event WHERE channel = '" << _esc(channel_id.get()) << "' ";
    strbuf << " AND start_time = " << start_time << " AND duration = " << duration;
	strbuf << " ORDER BY start_time;";
    
    epg_event_list_t event_list;
    if (execute(strbuf.str().c_str(), &epg_event_row_cb, &event_list))
    {
        if (event_list.size() == 1)
        {
            epg_item = event_list[0];
            ret_val = true;
        }
    }
    return ret_val;
}

bool recorder_database::get_epg(const channel_id_t& channel_id, const epg_event_id_t& epg_event_id, epg_event_list_t& event_list)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    event_list.clear();
    std::stringstream strbuf;
	strbuf << "SELECT * FROM epg_event WHERE channel = '" << _esc(channel_id.get()) << "' ";
	if (epg_event_id.get().size() > 0)
    {
	    strbuf << " AND event_id = '" << _esc(epg_event_id.get()) << "'";
    }
	strbuf << " ORDER BY start_time;";
    return execute(strbuf.str().c_str(), &epg_event_row_cb, &event_list);
}

bool recorder_database::get_epg_item(const channel_id_t& channel_id, const epg_event_id_t& epg_event_id, epg_item& item)
{
    bool ret_val = false;
    epg_event_list_t event_list;
    if (get_epg(channel_id, epg_event_id, event_list))
    {
        if (event_list.size() == 1)
        {
            item = event_list[0];
            ret_val = true;
        }
    }
    return ret_val;
}

struct epg_channel_event_row_cb_param
{
	epg_channel_info_map_t* epg_channel_events;
	epg_level_e info_level;
    boost::int32_t max_count;
};

static int epg_channel_event_row_cb(void* param, int num_values, char** values, std::map<std::string, int>& value_map)
{
    epg_channel_event_row_cb_param* info = (epg_channel_event_row_cb_param*)param;
    epg_item item;

    fill_epg_event_info(item, info->info_level, values, value_map);

    channel_id_t channel_id;
    if (value_map.find("channel") != value_map.end() && values[value_map["channel"]] != NULL)
    {
        channel_id = values[value_map["channel"]];
    }
    if (info->epg_channel_events->find(channel_id) == info->epg_channel_events->end())
    {
        (*info->epg_channel_events)[channel_id] = epg_event_list_t();
    }
    if (info->max_count == -1 || (info->max_count != -1 && static_cast<boost::int32_t>((*info->epg_channel_events)[channel_id].size()) < info->max_count))
    {
        (*info->epg_channel_events)[channel_id].push_back(item);
    }
    return SQLITE_OK;
}

bool recorder_database::get_epg_for_keyphrase(const channel_id_t& channel_id, std::string& keyphrase, bool new_only, epg_channel_info_map_t& epg_channel_events)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    epg_channel_events.clear();
    std::stringstream strbuf;
	strbuf << "SELECT * FROM epg_event WHERE (name LIKE '" << _esc(keyphrase) << "') ";
	if (!channel_id.get().empty())
    {
    	strbuf << " AND (channel = '" << _esc(channel_id.get()) << "') ";
    }
	if (new_only)
    {
    	strbuf << " AND (is_repeat = 0) ";
    }

	strbuf << " ORDER BY start_time;";
    
	epg_channel_event_row_cb_param cb_info;
	cb_info.epg_channel_events = &epg_channel_events;
	cb_info.info_level = epg_full_info;
    cb_info.max_count = -1;
    bool res = execute(strbuf.str().c_str(), &epg_channel_event_row_cb, &cb_info);
    return res;
}

bool recorder_database::browse_epg(server_channel_list_t& channel_list, int time_from,
    int time_to, std::string& keyphrase, unsigned long genre_mask, epg_level_e info_level, boost::int32_t max_count, epg_channel_info_map_t& epg_channel_events)
{
    bool res = true;
    epg_channel_events.clear();
    if (channel_list.size() == 0)
    {
        res = browse_epg_int(channel_list, time_from, time_to, keyphrase, genre_mask, info_level, max_count, epg_channel_events);
    }
    else
    {
	    size_t max_channels_per_request = 200;
	    server_channel_list_t tmp_channel_list;
	    //split request for no more than max_num channels at a time
	    for (size_t i = 0; i < channel_list.size(); i++)
	    {
		    tmp_channel_list.push_back(channel_list[i]);
		    if (tmp_channel_list.size() >= max_channels_per_request)
		    {
			    res &= browse_epg_int(tmp_channel_list, time_from, time_to, keyphrase, genre_mask, info_level, max_count, epg_channel_events);
			    tmp_channel_list.clear();
		    }
	    }
	    //and the reminder now
	    if (tmp_channel_list.size() > 0)
        {
		    res &= browse_epg_int(tmp_channel_list, time_from, time_to, keyphrase, genre_mask, info_level, max_count, epg_channel_events);
        }
    }
    return res;
}

static bool check_and_strip_hash_keyword(std::string& keyword)
{
	bool res = false;
	if (keyword.size() > 2)
	{
		if (boost::algorithm::starts_with(keyword, "#"))
		{
			res = true;
			keyword = keyword.substr(1);
		}
	}
	return res;
}

static bool check_and_strip_quotes_keyword(std::string& keyword)
{
	bool res = false;
	if (keyword.size() > 3)
	{
		if (boost::algorithm::starts_with(keyword, "\"") && boost::algorithm::ends_with(keyword, "\""))
		{
			res = true;
			keyword = keyword.substr(1, keyword.size() - 2);
		}
	}
	return res;
}

bool recorder_database::browse_epg_int(server_channel_list_t& channel_list, int time_from,
    int time_to, std::string& keyphrase, unsigned long genre_mask, epg_level_e info_level, boost::int32_t max_count, epg_channel_info_map_t& epg_channel_events)
{
    boost::unique_lock<boost::mutex> lock(mutex_);

    bool ret_val = false;
    std::stringstream strbuf;

	if (info_level == epg_full_info)
    {
		strbuf << "SELECT * FROM epg_event";
    }
	else
    {
		strbuf << "SELECT event_id, channel, name, start_time, duration, episode_num, season_num, is_hdtv, is_premiere, is_repeat, second_name, is_action, is_comedy, is_doc, is_drama, is_edu, is_horror, is_kids, is_movie, is_music, is_news, is_reality, is_romance, is_scifi, is_serial, is_soap, is_special, is_sports, is_thriller, is_adult FROM epg_event";
    }
	if (keyphrase.size() > 0 || time_from > 0 || time_to > 0 || channel_list.size() > 0 || genre_mask != DRGC_ANY)
    {
		strbuf << " WHERE ";
    }

	bool b_prev = false;
	//keyphrase
	if (keyphrase.size() > 0)
	{
		bool search_title_only = check_and_strip_hash_keyword(keyphrase);
		bool search_exact_match = check_and_strip_quotes_keyword(keyphrase);

		std::string keyphrase_lower_case = prepare_search_string(keyphrase);

		if (keyphrase_lower_case.size() > 0)
		{
			std::string wildcard_char = "%";
			if (search_exact_match)
				wildcard_char = "";

			strbuf << " (name_lower_case LIKE '" << wildcard_char << _esc(keyphrase_lower_case) << wildcard_char << "' ";
			if (!search_title_only)
			{
				strbuf << "OR second_name_lower_case LIKE '" << wildcard_char << _esc(keyphrase_lower_case) << wildcard_char << "' ";
				strbuf << "OR short_desc_lower_case LIKE '" << wildcard_char << _esc(keyphrase_lower_case) << wildcard_char << "' ";
			}
			strbuf << ") ";
			b_prev = true;
		}
	}

	//start-end time
	if (time_from > 0 || time_to > 0)
	{
		if (b_prev)
        {
			strbuf << " AND ";
        }
        if (time_from < 0 && time_to > 0)
        {
	        strbuf << "( start_time <= " << time_to << ") ";
        }
        else
        {
		    strbuf << " ( (start_time >= " << time_from << " ";
            if (time_to > 0)
            {
		        strbuf << " AND start_time <= " << time_to << ") ";
            }
            else
            {
		        strbuf << ") ";
            }
		    strbuf << " OR (start_time < " << time_from << " AND start_time+duration > " << time_from << ") ) ";
        }
		b_prev = true;
	}

    //genre mask
    if (genre_mask != DRGC_ANY)
    {
		if (b_prev)
        {
			strbuf << " AND ";
        }

        std::string mask_string;
        unsigned long mask = 1;
        do
        {
            if ((genre_mask & mask) != 0)
            {
		        if (mask_string.size() > 0)
                {
	    	        mask_string += " OR ";
                }

                switch (mask)
                {
                case DRGC_NEWS:
	    	        mask_string += "is_news <> 0";
                    break;
                case DRGC_KIDS:
	    	        mask_string += "is_kids <> 0";
                    break;
                case DRGC_MOVIE:
	    	        mask_string += "is_movie <> 0";
                    break;
                case DRGC_SPORT:
	    	        mask_string += "is_sports <> 0";
                    break;
                case DRGC_DOCUMENTARY:
	    	        mask_string += "is_doc <> 0";
                    break;
                case DRGC_ACTION:
	    	        mask_string += "is_action <> 0";
                    break;
                case DRGC_COMEDY:
	    	        mask_string += "is_comedy <> 0";
                    break;
                case DRGC_DRAMA:
	    	        mask_string += "is_drama <> 0";
                    break;
                case DRGC_EDU:
	    	        mask_string += "is_edu <> 0";
                    break;
                case DRGC_HORROR:
	    	        mask_string += "is_horror <> 0";
                    break;
                case DRGC_MUSIC:
	    	        mask_string += "is_music <> 0";
                    break;
                case DRGC_REALITY:
	    	        mask_string += "is_reality <> 0";
                    break;
                case DRGC_ROMANCE:
	    	        mask_string += "is_romance <> 0";
                    break;
                case DRGC_SCIFI:
	    	        mask_string += "is_scifi <> 0";
                    break;
                case DRGC_SERIAL:
	    	        mask_string += "is_serial <> 0";
                    break;
                case DRGC_SOAP:
	    	        mask_string += "is_soap <> 0";
                    break;
                case DRGC_SPECIAL:
	    	        mask_string += "is_special <> 0";
                    break;
                case DRGC_THRILLER:
	    	        mask_string += "is_thriller <> 0";
                    break;
                case DRGC_ADULT:
	    	        mask_string += "is_adult <> 0";
                    break;
                default:
                    //error! force exit
                    log_error(L"recorder_database_t::browse_epg. Invalid genre mask value %1%") % genre_mask;
                    mask_string = "";
                    mask = DRGC_LAST;
                    break;
                }
            }
            mask = (mask << 1);
        } while (mask <= DRGC_LAST);

        strbuf << " ( " << mask_string << " ) ";
        b_prev = true;
    }

	//channels
	if (channel_list.size() > 0)
	{
		if (b_prev)
        {
			strbuf << " AND ";
        }
    	strbuf << " ( ";
		for (size_t i=0; i<channel_list.size(); i++)
		{
			if (i > 0)
            {
		    	strbuf << " OR ";
            }
	    	strbuf << "channel = '" << _esc(channel_list[i].get()) << "'";
		}
    	strbuf << " ) ";
	}

	strbuf << " ORDER BY start_time;";

	epg_channel_event_row_cb_param cb_info;
	cb_info.epg_channel_events = &epg_channel_events;
	cb_info.info_level = info_level;
    cb_info.max_count = max_count;
    ret_val = execute(strbuf.str().c_str(), &epg_channel_event_row_cb, &cb_info);
    
    return ret_val;
}

} //recorder
} //dvblex
