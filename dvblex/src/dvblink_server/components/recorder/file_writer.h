/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_streamer.h>
#include <dl_filesystem_path.h>

namespace dvblex { namespace recorder {

#define STREAMER_PRIORITY_RECORDER   250

class file_writer_t : public dvblink::streamer_t
{
public:
    file_writer_t(const std::string& id, dvblink::filesystem_path_t& filename, int rel_prio);
	~file_writer_t();

    virtual void write_stream(const unsigned char* data, size_t len);
    virtual bool start(dvblink::streamer_callbacks_t* cb);
    virtual void notify_streamer_deleted();

    virtual boost::uint32_t get_priority(){return STREAMER_PRIORITY_RECORDER + priority_;}  //maximum priority for a recorder

    bool is_deleted() {return deleted_;}

private:
    FILE* f_;
    dvblink::filesystem_path_t filename_;
    int priority_;
    bool deleted_;
};

} //recorder
} //dvblex
