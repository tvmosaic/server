/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_message_server.h>
#include "timeline_builder.h"
#include "channel_set_container.h"
#include "duplicate_checker.h"
#include "completed_rec_man.h"
#include "recorder_settings.h"

using namespace dvblink; 
using namespace dvblink::engine;
using namespace dvblink::logging;

namespace dvblex { namespace recorder {

bool timeline_builder::is_program_overlap(time_t p1_start, time_t p1_duration, time_t p2_start, time_t p2_duration)
{
    return !(p1_start + p1_duration <= p2_start || p2_start + p2_duration <= p1_start);
}

bool timeline_builder::is_program_in_wnd(time_t start_time, time_t duration, time_t wnd_start, time_t wnd_end)
{
    return (start_time >= wnd_start && start_time <= wnd_end) || 
        (start_time + duration >= wnd_start && start_time + duration <= wnd_end) ||
        (start_time <= wnd_start && start_time + duration >= wnd_end);
}

std::wstring timeline_builder::form_time(const time_t t)
{
    char dt_buf[32];
    dt_buf[0] = '\0';
    tm* start_time = localtime(&t);
    sprintf(dt_buf, "%0.2d:%0.2d:%0.2d %d.%0.2d.%0.2d", start_time->tm_hour, start_time->tm_min,
        start_time->tm_sec, start_time->tm_year + 1900, start_time->tm_mon + 1, start_time->tm_mday);
    return string_cast<EC_UTF8>(dt_buf);
}

///////////////////////////////////////////////////////////////////////////////

void timeline_builder::reset()
{
    timeline_.clear();

    // initialize timeline with tuners
    //
    vec_ts_sources_t source_list;
    channel_set_container_.get_ts_sources(source_list);
    for (size_t i = 0; i < source_list.size(); ++i)
    {
        timeline_[source_list[i]] = timeline_timer_list_t();
    }
}

void timeline_builder::update_temp_timer_ids(const schedule_item_id_t& new_id)
{
    map_tuner_timeline_t::iterator tuner_it = timeline_.begin();
    while (tuner_it != timeline_.end())
    {
        timeline_timer_list_t::iterator timer_it = tuner_it->second.begin();
        while (timer_it != tuner_it->second.end())
        {
            recording_timer& rec = *timer_it;
            if (rec.schedule_item_id() == DL_RECORDER_INVALID_ID)
            {
                rec.set_schedule_item_id(new_id);
                rec.make_id();
            }
            ++timer_it;
        }
        ++tuner_it;
    }
}

bool timeline_builder::find_timer_source_id(const timer& t, device_id_t& source_id) const
{
    bool res = false;
    //walk through all tuners
    map_tuner_timeline_t::const_iterator timeline_it = timeline_.begin();
    while (timeline_it != timeline_.end() && !res)
    {
        //walk through all timers of this tuner
        timeline_timer_list_t::const_iterator timer_it = timeline_it->second.begin();
        while (timer_it != timeline_it->second.end() && !res)
        {
            if (*timer_it == t)
            {
                source_id = timeline_it->first;
                res = true;
                break;
            }
            ++timer_it;
        }
        ++timeline_it;
    }
    return res;
}

bool timeline_builder::get_timer(const timer_id_t& timer_id, recording_timer& timer) const
{
    bool res = false;
    //walk through all tuners
    map_tuner_timeline_t::const_iterator timeline_it = timeline_.begin();
    while (timeline_it != timeline_.end() && !res)
    {
        //walk through all timers of this tuner
        timeline_timer_list_t::const_iterator timer_it = timeline_it->second.begin();
        while (timer_it != timeline_it->second.end() && !res)
        {
            if (*timer_it == timer_id)
            {
                timer = *timer_it;
                res = true;
                break;
            }
            ++timer_it;
        }
        ++timeline_it;
    }
    return res;
}

bool timeline_builder::is_already_on_timeline(const recording_timer& timer) const
{
    std::string name_key_1 = recorder_duplicate_checker::make_episode_name_key(timer.event_info());
    unsigned long num_key_1 = recorder_duplicate_checker::make_episode_key(timer.event_info());
    if (!recorder_duplicate_checker::is_valid_name_key(name_key_1) && !recorder_duplicate_checker::is_valid_num_key(num_key_1))
    {
        return false;
    }

    map_tuner_timeline_t::const_iterator it_tuner = timeline_.begin();
    while (it_tuner != timeline_.end())
    {
        const timeline_timer_list_t& timer_list = it_tuner->second;
        timeline_timer_list_t::const_iterator it_timer = timer_list.begin();
        while (it_timer != timer_list.end())
        {
            const recording_timer& rec = *it_timer;
            if (rec.schedule_item_id() == timer.schedule_item_id())
            {
                std::string name_key_2 = recorder_duplicate_checker::make_episode_name_key(rec.event_info());
                unsigned long num_key_2 = recorder_duplicate_checker::make_episode_key(rec.event_info());

                if (recorder_duplicate_checker::is_valid_name_key(name_key_1) && recorder_duplicate_checker::is_valid_name_key(name_key_2) && boost::iequals(name_key_2, name_key_1))
                    return true;
                if (recorder_duplicate_checker::is_valid_num_key(num_key_1) && recorder_duplicate_checker::is_valid_num_key(num_key_2) && num_key_2 == num_key_1)
                    return true;
            }
            ++it_timer;
        }
        ++it_tuner;
    }
    return false;
}

bool timeline_builder::remove_timer(const timer_id_t& timer_id, recording_timer* removed_timer)
{
    bool removed = false;
    map_tuner_timeline_t::iterator timeline_it = timeline_.begin();
    while (timeline_it != timeline_.end() && !removed)
    {
        //walk through all timers of this tuner
        timeline_timer_list_t& timer_list = timeline_it->second;
        timeline_timer_list_t::iterator timer_it = timer_list.begin();
        while (timer_it != timer_list.end() && !removed)
        {
            if (*timer_it == timer_id)
            {
                if (removed_timer)
                {
                    *removed_timer = *timer_it;
                }
                //remove it from timeline
                timer_list.erase(timer_it);

                log_info(L"timeline_builder::remove_timer. Timer %1% was removed from timeline") % string_cast<EC_UTF8>(timer_id.get());
                removed = true;
                break;
            }
            ++timer_it;
        }
        ++timeline_it;
    }
    return removed;
}

void timeline_builder::remove_schedule(const schedule_item_id_t& schedule_id)
{
    //remove all timers, which belong to this schedule
    map_tuner_timeline_t::iterator tuner_it = timeline_.begin();
    while (tuner_it != timeline_.end())
    {
        timeline_timer_list_t temp_list = tuner_it->second;
        tuner_it->second.clear();

        timeline_timer_list_t::iterator timer_it = temp_list.begin();
        while (timer_it != temp_list.end())
        {
            if (timer_it->schedule_item_id() != schedule_id)
            {
                tuner_it->second.push_back(*timer_it);
            }
            ++timer_it;
        }
        ++tuner_it;
    }
}

void timeline_builder::add(const active_recordings_map_t& act_rec_map)
{
    active_recordings_map_t::const_iterator rec_it = act_rec_map.begin();
    while (rec_it != act_rec_map.end())
    {
        const device_id_t& source_id = rec_it->first;
        map_tuner_timeline_t::iterator it_timeline = timeline_.find(source_id);
        if (it_timeline != timeline_.end())
        {
            const active_recordings_list_t& active_recordings = rec_it->second;
            active_recordings_list_t::const_iterator it_act = active_recordings.begin();

            while (it_act != active_recordings.end())
            {
                timeline_timer_list_t& timeline_timer_list = it_timeline->second;
                const active_recording_t& rec = *it_act;
                if (rec)
                {
                    put_timer_in_list(timeline_timer_list, rec->get_timer(), NULL);
                }
                ++it_act;
            }
        }
        ++rec_it;
    }
}

bool timeline_builder::get_channel_concurr_detailed_info(const device_id_t& source_id, const channel_id_t& log_ch, 
                                                         channel_set_container&csc, channel_concurr_detailed_info& info)
{
    bool res = false;
    if (csc.get_phys_channel_id(source_id, log_ch, info.phy_ch_id_))
    {
        res = csc.get_phys_channel_data(source_id, info.phy_ch_id_, info.transp_id_, info.ch_set_id_, info.max_concurency_number_);
        info.channel_number_ = csc.get_channel_quantity(source_id);
        info.source_id_ = source_id;
    }
    return res;
}

void timeline_builder::get_concurrent_sources(const recording_timer& timer, vec_source_concurr_data_t& concurr_list, vec_source_concurr_data_t& non_concurr_list, margins_policy policy /*= use_margins*/) const
{
    map_tuner_timeline_t::const_iterator it_map = timeline_.begin();
    while (it_map != timeline_.end())
    {
        const device_id_t& source_id = it_map->first;
        channel_concurr_detailed_info timer_ch_info;
        if (get_channel_concurr_detailed_info(source_id, timer.channel(), channel_set_container_, timer_ch_info))
        {
            // check if the source is concurrent by program time
            const timeline_timer_list_t& timeline = it_map->second;
            recording_timers_list_t conflict_list;
            check_timeline_conflicts(timeline.begin(), timeline.end(), timer, &conflict_list, policy);

            if (conflict_list.size())
            {
                //
                // IMPORTANT!!! conflict_list could contain timers from different transponders and channel sets but with the same source, of course
                //
                timer_ch_info.conflict_list_ = conflict_list;
                concurr_list.push_back(timer_ch_info);
            }
            else
            {
                non_concurr_list.push_back(timer_ch_info);
            }
        }
        else
        {
            // it's Ok, channel doesn't belong to this source
        }
        ++it_map;
    }
}

bool timeline_builder::put_timer_in_concurrent_source(const recording_timer& timer,
    vec_source_concurr_data_t& vec_concurrent_sources, device_id_t& tuner_id, recording_timers_list_t& conflict_list)
{
    bool inserted = false;
    tuner_id.clear();
    if (vec_concurrent_sources.size())
    {
        // sort concurrent sources by channel quantity - source with less channel quantity is more prioritized
        //
        std::sort(vec_concurrent_sources.begin(), vec_concurrent_sources.end(), channel_concurr_detailed_info::comp_channel_quantity);
        for (size_t i = 0; i < vec_concurrent_sources.size(); ++i)
        {
            const channel_concurr_detailed_info& source = vec_concurrent_sources[i];
            timeline_timer_list_t& timeline = timeline_[source.source_id_];

            channel_concurr_detailed_info timer_ch_info;
            get_channel_concurr_detailed_info(source.source_id_, timer.channel(), channel_set_container_, timer_ch_info);

            concurrency_num_t::type_t concurr_cnt = 0;
            const recording_timers_list_t& cl = source.conflict_list_;
            bool conflict = false;
            for (size_t j = 0; j < cl.size(); ++j)
            {
                channel_concurr_detailed_info info;
                get_channel_concurr_detailed_info(source.source_id_, cl[j].channel(), channel_set_container_, info);

                if (timer_ch_info.transp_id_ != info.transp_id_)
                {
                    // this is conflict!!!
                    // streaming from different transponders is not possible!!!
                    conflict = true;
                    break;
                }
                else
                if (timer_ch_info.ch_set_id_ == info.ch_set_id_)
                {
                    // calc concurrent number for channel sets
                    ++concurr_cnt;
                }
            }

            if (!conflict &&
                (timer_ch_info.max_concurency_number_ == unlimited_concurrency_channel_num || concurr_cnt + 1 <= timer_ch_info.max_concurency_number_.get()))
            {
                put_timer_in_list(timeline, timer, NULL, use_margins);
                inserted = true;
                break;
            }

            if (!inserted)
            {
                recording_timers_list_t cl;
                check_timeline_conflicts(timeline.begin(), timeline.end(), timer, &cl, use_margins);
                if (tuner_id.empty() || cl.size() < conflict_list.size())
                {
                    tuner_id = source.source_id_;
                    conflict_list = cl;
                }
            }
        }
    }
    return inserted;
}

timeline_timer_list_t::const_iterator find_timer(const timeline_timer_list_t& timer_list, const recording_timer& timer)
{
    timeline_timer_list_t::const_iterator it = timer_list.begin();
    while (it != timer_list.end())
    {
        const recording_timer& t = *it;
        if (t.timer_id() == timer.timer_id())
        {
            break;
        }
        ++it;
    }
    return it;
}

bool timeline_builder::is_conflicted(const device_id_t& source_id,
    const timeline_timer_list_t& timeline_timer_list, const recording_timer& timer, 
    channel_set_container& csc, recording_timers_list_t* out_conflict_list, margins_policy policy /*= use_margins*/)
{
    channel_concurr_detailed_info timer_info;
    get_channel_concurr_detailed_info(source_id, timer.channel(), csc, timer_info);

    recording_timers_list_t conflict_list;
    check_timeline_conflicts(timeline_timer_list.begin(), timeline_timer_list.end(), timer, &conflict_list, policy);

    bool conflict = false;
    concurrency_num_t::type_t concurr_cnt = 0;
    for (size_t i = 0; i < conflict_list.size(); ++i)
    {
        recording_timer conflict_timer = conflict_list[i];
        channel_concurr_detailed_info conflict_timer_info;
        get_channel_concurr_detailed_info(source_id, conflict_timer.channel(), csc, conflict_timer_info);

        if (conflict_timer_info.transp_id_ != timer_info.transp_id_)
        {
            conflict = true;
        }
        else
        if (conflict_timer_info.ch_set_id_ == timer_info.ch_set_id_)
        {
            // calc concurrent number for channel sets
            ++concurr_cnt;
            if (timer_info.max_concurency_number_ != unlimited_concurrency_channel_num && concurr_cnt + 1> timer_info.max_concurency_number_.get())
            {
                conflict = true;
            }
        }

        if (conflict && out_conflict_list)
        {
            out_conflict_list->push_back(conflict_timer);
        }
    }
    return conflict;
}

bool timeline_builder::get_scheduled_timers_in_csv(csv_string_t& schedule_csv) const
{
    recording_timers_list_t timer_list;
    get_scheduled_timers(timer_list);

    std::wstringstream out;
    // header
    out << L"record_name, source, channel_name, begin, end, conflicted\n";

    if (timer_list.size())
    {
        for (size_t i = 0; i < timer_list.size(); ++i)
        {
            device_id_t source_id;
            const recording_timer& rec = timer_list[i];
            find_timer_source_id(rec, source_id);

            out << rec.generate_record_name() << L",";
            out << string_cast<EC_UTF8>(source_id.get());
            out << L",";

            out << string_cast<EC_UTF8>(rec.channel().get()) << L",";
            out << form_time(rec.real_start_time()) << L",";
            out << form_time(rec.real_start_time() + rec.real_duration()) << L",";
            out << (rec.conflict() ? L"yes" : L"no");
            out << std::endl;
        }
    }
    schedule_csv = out.str();
    return schedule_csv.get().size() ? true : false;
}

void timeline_builder::get_scheduled_timers(recording_timers_list_t& timer_list) const
{
    timer_list.clear();

    map_tuner_timeline_t::const_iterator it_timeline = timeline_.begin();
    while (it_timeline != timeline_.end())
    {
        timeline_timer_list_t out_timer_list;
        const timeline_timer_list_t& source_timeline = it_timeline->second;
        const device_id_t& source_id = it_timeline->first;

        // walk through all timers of this source
        timeline_timer_list_t::const_iterator it_source_timeline = source_timeline.begin();
        timeline_timer_list_t::const_iterator it_source_timeline_end = source_timeline.end();
        while (it_source_timeline != it_source_timeline_end)
        {
            recording_timer timer = *it_source_timeline;
            recording_timers_list_t conflict_list, conflict_list2;
            if (is_conflicted(source_id, out_timer_list, timer, channel_set_container_, &conflict_list, use_margins))
            {
                // try to not use margins
                is_conflicted(source_id, out_timer_list, timer, channel_set_container_, &conflict_list2, dont_use_margins);
                if (conflict_list2.size() < conflict_list.size())
                {
                    conflict_list = conflict_list2;
                }

                if (conflict_list.size())
                {
                    timer.set_conflict(true);

                    for (size_t i = 0; i < conflict_list.size(); ++i)
                    {
                        // find conflicted tuner in the common list and mark it as conflicted
                        recording_timer t = conflict_list[i];
                        timeline_timer_list_t::const_iterator cit = find_timer(out_timer_list, conflict_list[i]);
                        if (cit != out_timer_list.end())
                        {
                            // just mark
                            timeline_timer_list_t::iterator it = out_timer_list.begin();
                            std::advance(it, std::distance<timeline_timer_list_t::const_iterator>(it, cit));
                            it->set_conflict(true);
                        }
                    }
                }
            }

            //add timer to the list
            out_timer_list.push_back(timer);
            ++it_source_timeline;
        }
        timer_list.insert(timer_list.end(), out_timer_list.begin(), out_timer_list.end());
        ++it_timeline;
    }
}

bool timeline_builder::put_timer_in_non_concurrent_source(const recording_timer& timer,
    vec_source_concurr_data_t& vec_non_concurrent_sources, device_id_t& tuner_id, recording_timers_list_t& conflict_list)
{
    // sort non concurrent sources by channel quantity - source with less channel quantity is more prioritized
    std::sort(vec_non_concurrent_sources.begin(), vec_non_concurrent_sources.end(), channel_concurr_detailed_info::comp_channel_quantity);

    tuner_id = vec_non_concurrent_sources[0].source_id_;
    timeline_timer_list_t& timeline = timeline_[tuner_id];

    recording_timers_list_t cl;
    put_timer_in_list(timeline, timer, &cl);
    assert(!cl.size());

    return true;
}

bool timeline_builder::add_timer(const recording_timer& timer, recording_timers_list_t* conflict_list)
{
    // get tuner on which a channel is concurrent
    // get tuner with less quantity of channels

    // let's try to find concurrent and non concurrent sources
    //
    vec_source_concurr_data_t concurr_list, non_concurr_list;
    get_concurrent_sources(timer, concurr_list, non_concurr_list, use_margins);

    bool inserted = false;
    recording_timers_list_t tuner_conflict_list;
    device_id_t tuner_id = boost::uuids::to_string(boost::uuids::nil_uuid());
    if (concurr_list.size())
    {
        inserted = put_timer_in_concurrent_source(timer, concurr_list, tuner_id, tuner_conflict_list);
    }
    if (!inserted && non_concurr_list.size())
    {
        inserted = put_timer_in_non_concurrent_source(timer, non_concurr_list, tuner_id, tuner_conflict_list);
    }

    //if (!inserted)
    //{
    //    concurr_list.clear();
    //    non_concurr_list.clear();
    //    get_concurrent_sources(timer, concurr_list, non_concurr_list, false);

    //}

    bool res = true;
    if (!inserted)
    {
        if (tuner_conflict_list.size() == 0)
        {
            //strange - timer is not placed and there are no conflicts
            //probably it means that timer is for a channel, which is not present in the map
            log_error(L"timeline_builder::add_timer. Timer starting at %1% for channel %2% was not placed because its channel not found in the map") % timer.real_start_time() % string_cast<EC_UTF8>(timer.channel().get());
            res = false;
        }
        else
        if (conflict_list)
        {
            //return conflict list to a user for manual resolution
            conflict_list->insert(conflict_list->end(), tuner_conflict_list.begin(), tuner_conflict_list.end());
        }
        else
        {
            //force the timeline with conflicts
            //
            timeline_timer_list_t* timeline = &(timeline_[tuner_id]);
            
            //try to find possibility without margins first
            vec_source_concurr_data_t no_margin_concurr_list, no_margin_non_concurr_list;
            get_concurrent_sources(timer, no_margin_concurr_list, no_margin_non_concurr_list, dont_use_margins);
            
            if (no_margin_non_concurr_list.size() > 0)
                timeline = &(timeline_[no_margin_non_concurr_list[0].source_id_]);
                
            put_timer_in_list(*timeline, timer, NULL);
            log_info(L"timeline_builder::add_timer. Timer starting at %1% was forced onto a timeline of tuner %2%") % timer.real_start_time() % string_cast<EC_UTF8>(tuner_id.get());
        }
    }
    return res;
}

void timeline_builder::put_timer_in_list(timeline_timer_list_t& timer_list, const recording_timer& timer, recording_timers_list_t* conflict_list, margins_policy policy /*= use_margins*/)
{
    timeline_timer_list_t::iterator placing_it = check_timeline_conflicts(timer_list.begin(), timer_list.end(), timer, conflict_list, policy);
    //add timer if there are no conflicts
    if (!conflict_list || (conflict_list && conflict_list->size() == 0))
    {
        timer_list.insert(placing_it, timer);
    }
}

time_t timeline_builder::get_largest_before_margin()
{
    time_t ret_val = recorder_settings_.get_before_margin();
    
    map_tuner_timeline_t::iterator it_timeline = timeline_.begin();
    while (it_timeline != timeline_.end())
    {
        //walk through all timers of this tuner
        const timeline_timer_list_t& timeline_timer_list = it_timeline->second;
        timeline_timer_list_t::const_iterator it_timer = timeline_timer_list.begin();
        while (it_timer != timeline_timer_list.end())
        {
            if (it_timer->margin_before() > ret_val)
                ret_val = it_timer->margin_before();
            
            ++it_timer;
        }
        ++it_timeline;
    }
    
    return ret_val;
}

void timeline_builder::get_timers_for_wnd(time_t wnd_start, time_t wnd_duration, map_tuner_timeline_t& timer_list)
{
    timer_list.clear();
    map_tuner_timeline_t::iterator it_timeline = timeline_.begin();
    while (it_timeline != timeline_.end())
    {
        const device_id_t& source_id = it_timeline->first;
        const timeline_timer_list_t& timeline_timer_list = it_timeline->second;

        timer_list[source_id] = timeline_timer_list_t();
        timeline_timer_list_t& timeline_timer_list_out = timer_list[source_id];

        //walk through all timers of this tuner
        timeline_timer_list_t::const_iterator it_timer = timeline_timer_list.begin();
        while (it_timer != timeline_timer_list.end())
        {
            const recording_timer& rec = *it_timer;
            if (is_program_in_wnd(rec.real_start_time(), rec.real_duration(), wnd_start, wnd_start + wnd_duration))
            {
                //filter already completed recordings from the list
                if (!completed_rec_man_->is_exist(rec.timer_id()))
                {
                    timeline_timer_list_out.push_back(rec);
                }
            }
            ++it_timer;
        }
        ++it_timeline;
    }
}

time_t timeline_builder::get_first_future_timer() const
{
    time_t res = invalid_time_value;

    time_t now; time(&now);
    std::vector<time_t> timers;
    map_tuner_timeline_t::const_iterator timeline_it = timeline_.begin();
    while (timeline_it != timeline_.end())
    {
        //walk through all timers of this tuner
        timeline_timer_list_t::const_iterator timer_it = timeline_it->second.begin();
        while (timer_it != timeline_it->second.end())
        {
            time_t start_time = timer_it->real_start_time();// - recorder_settings::instance()->get_before_margin();
            if (start_time > now)
            {
                timers.push_back(start_time);
                break;
            }
            ++timer_it;
        }
        ++timeline_it;
    }

    //walk through timers list to find the closest
    for (size_t i = 0; i < timers.size(); i++)
    {
        if (res == invalid_time_value)
        {
            res = timers[i];
        }
        else
        if (timers[i] < res)
        {
            res = timers[i];
        }
    }

    return res;
}

} //recorder
} //dvblex
