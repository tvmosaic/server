/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <dl_types.h>
#include "epg.h"
#include <dl_schedules.h>
#include "timers.h"

namespace dvblex { namespace recorder {

enum recorder_cmd_e
{
    recAddSchedule,
    recGetSchedules,
    recUpdateSchedule,
    recDeleteSchedule,
    recGetTimers,
    recDeleteTimer,
    recForceUpdate,
    recBrowseEPG,
	recGetEPGEvent,
	recGetCompletedRecordings,
	recDeleteCompletedRecording,
    recUpdateChannelsEpg,
    recGetSettings,
    recSetSettings,
    recGetScheduledTimersCSV,
    recSwitchToActive,
    recSwitchToIdle,
	recRepairDatabase
};

struct rec_cmd_base
{
    bool result_;
};

struct rec_add_schedule_info : public rec_cmd_base
{
    schedule_item* schedule_item_;
    recording_timers_list_t* conflict_list_;
};

struct rec_get_schedules_info : public rec_cmd_base
{
    schedule_list_t* schedule_list_;
    bool active_only_;
};

struct rec_update_schedule_info : public rec_cmd_base
{
    update_schedule_item_info* update_info_;
};

struct rec_delete_schedule_info : public rec_cmd_base
{
    dvblink::schedule_item_id_t schedule_item_id_;
    bool delete_from_db_;
};

struct rec_get_timers_info : public rec_cmd_base
{
    recording_timers_list_t* scheduled_timer_list_;
    recording_timers_list_t* active_timer_list_;
	recording_timers_list_t* cancelled_timer_list_;
};

struct rec_delete_timer_info : public rec_cmd_base
{
    dvblink::timer_id_t* timer_id_;
};

struct rec_force_update_info : public rec_cmd_base
{
};

struct repair_db_info : public rec_cmd_base
{
};

struct rec_browse_epg_info : public rec_cmd_base
{
    server_channel_list_t* channel_list_;
    int time_from_;
    int time_to_;
    std::string* keyphrase_;
    unsigned long genre_mask_;
	epg_level_e info_level_;
    boost::int32_t max_count_;
    epg_ex_channel_info_map_t* event_list_;
};

struct rec_get_epg_event : public rec_cmd_base
{
	dvblink::channel_id_t* channel_id_;
	dvblink::epg_event_id_t* event_id_;
	epg_level_e info_level_;
    epg_item_ex* epg_item_;
};

struct rec_get_completed_recordings_info : public rec_cmd_base
{
    completed_recordings_list_t* completed_recordings_list_;
    dvblink::schedule_item_id_t schedule_item_id_;
};

struct rec_delete_completed_recordings_info : public rec_cmd_base
{
    rec_delete_completed_recordings_info(const dvblink::timer_id_t& timer_id) :
        timer_id_(timer_id)
    {}
	dvblink::timer_id_t timer_id_;
};

struct rec_update_channels_epg_info : public rec_cmd_base
{
	const std::vector<dvblink::channel_id_t>* channel_list_;
};

struct rec_get_settings_info : public rec_cmd_base
{
	recorder_settings* settings_;
};

struct rec_set_settings_info : public rec_cmd_base
{
	recorder_settings* settings_;
};

struct rec_scheduled_timers_in_csv : public rec_cmd_base
{
    dvblink::csv_string_t* csv_timers_;
};

struct rec_switch_to_idle : public rec_cmd_base
{
};

struct rec_switch_to_active : public rec_cmd_base
{
};

} //recorder
} //dvblex
