/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <map>
#include <dl_types.h>
#include <dl_channels.h>
#include "timers.h"

namespace dvblex { namespace recorder {

class channel_set
{
public:
    channel_set() {}
    channel_set(const dvblink::transponder_id_t& transponder_id, const dvblink::channel_set_id_t& channel_set_id, const dvblink::concurrency_num_t& max_concurrency_num) :
        transponder_id_(transponder_id),
        channel_set_id_(channel_set_id),
        max_concurrency_num_(max_concurrency_num)
    {
    }
    const dvblink::transponder_id_t& transponder_id() const {return transponder_id_;}
    const dvblink::channel_set_id_t& channel_set_id() const {return channel_set_id_;}
    const dvblink::concurrency_num_t& max_concurrency_num() const {return max_concurrency_num_;}

private:
    dvblink::transponder_id_t transponder_id_;
    dvblink::channel_set_id_t channel_set_id_;
    dvblink::concurrency_num_t max_concurrency_num_;
};

class transponder_data
{
public:
    void add(const set_of_set_of_channels_set_t& transponder_list);
    bool get_phys_channel_data(const dvblink::channel_id_t& ph_ch,
        dvblink::transponder_id_t& transponder_id, dvblink::channel_set_id_t& channel_set_id, dvblink::concurrency_num_t& max_concurency_number) const;
    size_t get_channel_number() const {return map_ts_ch_phy_id_.size();}

private:
    typedef std::map<dvblink::channel_set_id_t, channel_set> map_channel_set_id_TO_channel_set_t;
    map_channel_set_id_TO_channel_set_t map_channel_set_;

    typedef std::map<dvblink::channel_set_id_t, dvblink::transponder_id_t> map_channel_set_TO_transponder_id_t;
    map_channel_set_TO_transponder_id_t map_transponder_id_;

    typedef std::map<dvblink::channel_id_t, dvblink::channel_set_id_t> map_chphy_id_TO_channel_sets_t;
    map_chphy_id_TO_channel_sets_t map_ts_ch_phy_id_;
};

typedef std::vector<dvblink::device_id_t> vec_ts_sources_t;

class channel_set_container
{
public:
    channel_set_container() {}

    void init(const ts_source_TO_set_of_set_of_channels_set_t& channel_set_data);

    void get_ts_sources(vec_ts_sources_t& sources) const;

    bool get_phys_channel_id(const dvblink::device_id_t& ts_control_id, const dvblink::channel_id_t& log_ch_id, dvblink::channel_id_t& phy_id) const;
    bool get_phys_channel_data(const dvblink::device_id_t& ts_control_id, const dvblink::channel_id_t& ph_ch,
        dvblink::transponder_id_t& transponder_id, dvblink::channel_set_id_t& channel_set_id, dvblink::concurrency_num_t& max_concurency_number) const;

    bool is_concurrent_channel(const dvblink::device_id_t& ts_control_id, const dvblink::channel_id_t& cur_phy_id,
        const dvblink::channel_id_t& new_phy_id, dvblink::channel_set_id_t& channel_set_id, dvblink::concurrency_num_t& max_concurency_number);

    size_t get_channel_quantity(const dvblink::device_id_t& source_id) const;

private:
    typedef std::map<dvblink::device_id_t, transponder_data> map_ts_source_control_TO_transponders_t;
    map_ts_source_control_TO_transponders_t set_collection_;

    typedef std::map<dvblink::device_id_t, dvblink::channel_id_t> map_source_TO_phys_channel_id_t;
    typedef std::map<dvblink::channel_id_t, map_source_TO_phys_channel_id_t> map_log_ch_TO_map_source_phys_channel_id_t;
    map_log_ch_TO_map_source_phys_channel_id_t map_log_channel_data_;
};

} //recorder
} //dvblex
