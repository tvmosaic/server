/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_message_queue.h>
#include <dli_server.h>
#include <dl_send_to_item.h>
#include "channel_set_container.h"
#include "completed_rec_man.h"
#include "cancelled_rec_man.h"
#include "active_rec_man.h"
#include "timeline_builder.h"
#include "duplicate_checker.h"

namespace dvblex { namespace recorder {

class sink_factory;
class recorder_database;
class active_recording;
class schedule_item;

typedef std::map<dvblink::channel_id_t, logical_channel_desc> log_channel_map_t;
typedef std::map<dvblink::device_id_t, log_channel_map_t> source_to_logical_map_t;

class recorder_engine_impl
{
    friend class active_rec_man;
public:
    recorder_engine_impl(recorder_settings& rs, recorder_database& rd, dvblink::i_server_t server, dvblink::messaging::message_queue_t message_queue);
    ~recorder_engine_impl() {}
    
    void reset(const source_to_logical_map_t& logical_channel_map, const ts_source_TO_set_of_set_of_channels_set_t& channel_set_data);
    void term() {active_rec_man_.complete_all_recs();}

    timeline_builder& get_timeline_builder() {return timeline_builder_;}

    bool add_schedule(const schedule_item& item, recording_timers_list_t* conflict_list);
    void update_temp_timer_ids(const dvblink::schedule_item_id_t& new_id) {timeline_builder_.update_temp_timer_ids(new_id);}
    void remove_schedule(const dvblink::schedule_item_id_t& schedule_id, bool delete_from_db = false);

    void get_scheduled_timers(recording_timers_list_t& timer_list) const {timeline_builder_.get_scheduled_timers(timer_list);}
    void get_active_timers(recording_timers_list_t& timer_list) const {active_rec_man_.get_active_timers(timer_list);}
    void get_cancelled_timers(recording_timers_list_t& timer_list) const {cancelled_rec_man_.get_cancelled_timers(timer_list);}
    void cancel_timer(dvblink::timer_id_t& timer_id);

    bool get_timer(const dvblink::timer_id_t& timer_id, recording_timer& timer) const {return timeline_builder_.get_timer(timer_id, timer);}

    void process_timers();
	bool delete_completed_recording(const dvblink::timer_id_t& timer_id);

    bool get_scheduled_timers_in_csv(dvblink::csv_string_t& schedule_csv) const;

    bool are_conflicts_present();

    void execute_autodelete_procedure();

protected:
    void generate_timers_for_schedule(const schedule_item& item, recording_timers_list_t& timer_list);

    void analyze_conflicts(map_tuner_timeline_t& now_timers);
    bool add_timer_to_timeline(const schedule_item& item, const recording_timer& timer, recording_timers_list_t* conflict_list);

    void complete_active_recording(const dvblink::device_id_t& src_id, active_recording_t& recording, bool delete_schedule = true);
    void start_active_recording(const dvblink::device_id_t& src_id, const recording_timer* timer);
    void generate_timers_for_epg_schedule(const schedule_item& item, recording_timers_list_t& timer_list);
    void generate_timers_for_pattern_schedule(const schedule_item& item, recording_timers_list_t& timer_list);
	void purge_extra_series_recordings(const dvblink::schedule_item_id_t& schedule_id);
    time_t get_first_future_timer();
    void update_wakeup_timers();
    bool is_enough_free_disk_space();
    void get_keyphrase_from_name(const std::string& name, std::string& keyphrase);
    bool check_start_time_in_before_after_margins(const schedule_item& sch_item, time_t start_time);
    bool check_start_time_on_weekday(boost::uint8_t day_mask, time_t start_time);
    bool check_timer_for_duplicate(const schedule_item& sch_item, const recording_timer& timer, recorder_duplicate_checker& duplicate_checker);
    bool check_recurring_timer_for_boundaries(const schedule_item& sch_item, time_t start_time);
    bool get_targets(send_to_target_list_t& targets);

    //power management
    bool get_wakeup_time(time_t& wakeup_time);
    bool remove_wakeup_time();
    bool add_wakeup_time(time_t wakeup_time);

private:
    recorder_settings& recorder_settings_;
    recorder_database& recorder_database_;
    dvblink::messaging::message_queue_t message_queue_;
    dvblink::i_server_t server_;
    source_to_logical_map_t log_channel_map_;

    completed_rec_man completed_rec_man_;
    timeline_builder timeline_builder_;
    cancelled_rec_man cancelled_rec_man_;
    active_rec_man active_rec_man_;
    
    time_t next_wakeup_timer_;
    channel_set_container channel_set_container_;
};

} //recorder
} //dvblex
