/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <map>
#include <vector>
#include <dl_pb_item.h>
#include <dl_message_common.h>
#include <dl_send_to_item.h>
#include "send_to_settings.h"
#include "send_to_work_unit.h"

namespace dvblex {

typedef std::map<dvblink::base_id_t, send_to_target_info> send_to_target_map_t;
typedef std::map<send_to_work_unit_id, dvblink::xml_string_t> send_to_work_unit_activation_info_map_t;

class send_to_target_broker
{
public:
    send_to_target_broker(boost::shared_ptr<send_to_settings>& settings, const dvblink::messaging::message_queue_t& message_queue);
    ~send_to_target_broker();

	bool process_cmd(const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response);

	send_to_work_unit* create_work_unit(send_to_work_unit_init_info& info);
    send_to_work_unit* create_comskip_work_unit(send_to_target_info& target_info);
	bool get_target(const dvblink::base_id_t& target_id, send_to_target_info& target_info);

protected:
	send_to_target_map_t targets_;
	send_to_work_unit_activation_info_map_t activation_info_;
	boost::shared_ptr<send_to_settings> settings_;
	dvblink::filesystem_path_t settings_path_;
    dvblink::messaging::message_queue_t message_queue_;

	void write_data();
	void read_data();
	void reset();
	void fill_work_unit(const char* id, bool activation_required, send_to_work_unit_info& unit_info);

	void get_formatters(send_to_work_unit_info_list_t& formatters);
	void get_destinations(send_to_work_unit_info_list_t& destinations);
	void activate_work_unit(const send_to_work_unit_id& id, const dvblink::xml_string_t& activation_info);
	bool get_activation_info(const send_to_work_unit_id& id, dvblink::xml_string_t& activation_info);

	void get_targets(send_to_target_list_t& targets);
	void set_targets(const send_to_target_list_t& targets);

    bool get_comskip_setings(const concise_param_map_t& cur_values, parameters_container_t& settings, concise_param_map_t& default_values);

    bool is_transcoding_supported();
};

} // dvblex
