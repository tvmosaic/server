/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_file_procedures.h>
#include <dl_message_addresses.h>
#include <dl_message_recorder.h>
#include <dl_message_power.h>
#include <dl_message_server.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_xml_serialization.h>
#include <dl_message_playback.h>
#include <dl_pb_object.h>
#include <dl_rec_options_serializer.h>
#include <dl_send_to_cmd.h>
#include <dl_pugixml_helper.h>
#include "send_to_manager.h"
#include "send_to_common.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;
using namespace dvblink::pugixml_helpers;

namespace dvblex {

using namespace playback;

send_to_manager::send_to_manager() :
	manager_thread_(NULL),
	work_objects_(NULL),
	work_object_num_(0),
	target_broker_(NULL),
    processing_start_delay_sec_(60),
    power_man_client_id_("F679D368-B308-42a7-8F2F-82614AE59518"),
    completed_items_to_keep_(20)
{
}

send_to_manager::~send_to_manager()
{
}

size_t send_to_manager::get_max_work_objects()
{
	if (settings_ != NULL)
		return settings_->get_max_work_objects();

	return 1;
}

bool send_to_manager::init(boost::shared_ptr<send_to_settings>& settings, const messaging::message_queue_t& message_queue)
{
    settings_ = settings;
	message_queue_ = message_queue;

	target_broker_ = new send_to_target_broker(settings_, message_queue_);

	return true;
}

void send_to_manager::term()
{
	if (target_broker_ != NULL)
	{
		delete target_broker_;
		target_broker_ = NULL;
	}

	message_queue_.reset();
}

bool send_to_manager::start()
{
    if (manager_thread_ == NULL)
    {
	    exit_flag_ = false;
	    manager_thread_ = new boost::thread(boost::bind(&send_to_manager::manager_thread_function, this));
    }
	return true;
}

void send_to_manager::stop()
{
	if (manager_thread_ != NULL)
	{
		exit_flag_ = true;
		manager_thread_->join();
		delete manager_thread_;
		manager_thread_ = NULL;
	}
}

bool send_to_manager::process_cmd(const messaging::xml_message_request& request, messaging::xml_message_response& response)
{
	bool ret_val = true;

    if (boost::iequals(request.cmd_id_.get(), SEND_TO_CMD_ADD_ITEM))
    {
	    send_to_add_item_request req;
	    if (read_from_xml(request.xml_.get(), req))
	    {
		    s_add_item_params params;
		    params.items = &req.work_items_;

		    send_to_add_item_response resp;
		    params.item_ids = &resp.item_ids_;

		    command_queue_.ExecuteCommand(e_add_item, &params);

		    std::string str;
		    write_to_xml(resp, str);

		    response.result_ = params.result ? dvblink::xmlcmd_result_success : dvblink::xmlcmd_result_error;
		    response.xml_ = str;
	    }
    } else
    if (boost::iequals(request.cmd_id_.get(), SEND_TO_CMD_CANCEL_ITEM))
	{
		send_to_cancel_item_request req;
		if (read_from_xml(request.xml_.get(), req))
		{
			s_cancel_item_params params;
			params.work_item_id = &req.work_item_id_;

			command_queue_.ExecuteCommand(e_cancel_item, &params);

			response.result_ = params.result ? dvblink::xmlcmd_result_success : dvblink::xmlcmd_result_error;
		}
	} else
    if (boost::iequals(request.cmd_id_.get(), SEND_TO_CMD_DELETE_ITEMS))
	{
		send_to_delete_item_request req;
		if (read_from_xml(request.xml_.get(), req))
		{
			s_remove_item_params params;
			params.work_item_id_list = &req.item_ids_;

			command_queue_.ExecuteCommand(e_remove_item, &params);

			response.result_ = params.result ? dvblink::xmlcmd_result_success : dvblink::xmlcmd_result_error;
		}
	} else
    if (boost::iequals(request.cmd_id_.get(), SEND_TO_CMD_GET_ITEMS))
	{
		send_to_get_items_request req;
		if (read_from_xml(request.xml_.get(), req))
		{
			s_get_work_items_params params;
			params.item_type = req.item_type_;

			send_to_get_items_response resp;
			params.result_items = &resp.work_items_;

			command_queue_.ExecuteCommand(e_get_work_items, &params);

			std::string str;
			write_to_xml(resp, str);

			response.result_ = params.result ? dvblink::xmlcmd_result_success : dvblink::xmlcmd_result_error;
			response.xml_ = str;
		}
	} else
    if (boost::iequals(request.cmd_id_.get(), SEND_TO_CMD_LOOKUP_ITEMS))
	{
		send_to_lookup_items_request req;
		if (read_from_xml(request.xml_.get(), req))
		{

			s_lookup_work_items params;
			params.object_ids = &req.object_ids_;

			send_to_lookup_items_response resp;
			params.result_items = &resp.work_items_;

			command_queue_.ExecuteCommand(e_lookup_work_items, &params);

			std::string str;
			write_to_xml(resp, str);

			response.result_ = params.result ? dvblink::xmlcmd_result_success : dvblink::xmlcmd_result_error;
			response.xml_ = str;
		}
	} else 
	if (!target_broker_->process_cmd(request, response))
	{
		ret_val = false;
	}

	return ret_val;
}

void send_to_manager::manager_thread_function()
{
    log_info(L"send_to_manager::manager_thread_function. Starting main processing thread. Waiting for server to become operational");

    //start actual processing not immediately, but after some delay
    bool processing_allowed = false;

    time_t thread_start_timestamp;
    time(&thread_start_timestamp);

    //wait until dvblink server is operational
    while (!exit_flag_)
    {
        if (is_server_operational())
        {
            log_info(L"send_to_manager::manager_thread_function. Server is operational");
            break;
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(200));
    }

	read_items();

    //apply initial power state
    apply_power_state(get_power_state());

	work_objects_ = (send_to_work_object**)malloc(sizeof(send_to_work_object*) * get_max_work_objects());
	for (size_t i=0; i<get_max_work_objects(); i++)
		work_objects_[i] = NULL;

    while (!exit_flag_)
    {
        SDLCommandItem* item;
        while (command_queue_.PeekCommand(&item))
        {
            switch (item->id)
            {
            case e_add_item:
                {
                    s_add_item_params* params = (s_add_item_params*)item->param;
					          params->result = add_work_item_int(params->items, params->item_ids);
                }
                break;
            case e_remove_item:
                {
                    s_remove_item_params* params = (s_remove_item_params*)item->param;
					          params->result = remove_work_item_int(params->work_item_id_list);
                }
                break;
            case e_get_work_items:
                {
                    s_get_work_items_params* params = (s_get_work_items_params*)item->param;
					          params->result = get_work_items_int(params->item_type, params->result_items);
                }
                break;
            case e_lookup_work_items:
                {
                    s_lookup_work_items* params = (s_lookup_work_items*)item->param;
					          params->result = lookup_work_items_int(params->object_ids, params->result_items);
                }
                break;
            case e_cancel_item:
                {
                    s_cancel_item_params* params = (s_cancel_item_params*)item->param;
					          params->result = cancel_work_item_int(params->work_item_id);
                }
                break;
			default:
				break;
            }
            command_queue_.FinishCommand(&item);
        }

		  //process completed work objects
		  for (size_t i=0; i<get_max_work_objects(); i++)
		  {
			  if (work_objects_[i] != NULL && work_objects_[i]->is_completed())
			  {
				  send_to_work_item work_item;
				  work_objects_[i]->get_work_item(work_item);

				  delete_work_object(i);

				  complete_item(work_item);
			  }
		  }

		  //process pending work items
		  if (pending_items_.size() > 0 && processing_allowed)
		  {
			  while (pending_items_.size() > 0 && work_object_num_ < get_max_work_objects())
			  {
				  if (work_items_.find(pending_items_[0]) != work_items_.end())
				  {
					  send_to_work_item wi = work_items_.find(pending_items_[0])->second;
					  //take the first object from the queue
					  work_object_init_info init_info;
					  if (init_work_object_info(wi, init_info))
					  {
						  send_to_work_object* work_object = new send_to_work_object(init_info);
						  //add it to the work objects
						  if (!add_work_object(work_object))
							  log_error(L"send_to_manager::manager_thread_function. Failed to add work object %1%") % wi.item_id.to_wstring();
					  } else
					  {
						  log_error(L"send_to_manager::manager_thread_function. Failed to create init info for object %1%") % wi.item_id.to_wstring();
					  }
				  } else
				  {
					  log_error(L"send_to_manager::manager_thread_function. Pending work item does not exist");
				  }
				  //delete it from the pending queue
				  pending_items_.erase(pending_items_.begin());
			  }
		  }

      if (!processing_allowed)
      {
        time_t ts;
        time(&ts);
        if (ts - thread_start_timestamp > processing_start_delay_sec_)
          processing_allowed = true;
      }

      //update power state
      if (current_power_state_ != get_power_state())
        apply_power_state(get_power_state());

      boost::this_thread::sleep(boost::posix_time::milliseconds(10));
    }

	//cancel all active work objects on exit
	for (size_t i=0; i<get_max_work_objects(); i++)
	{
		if (work_objects_[i] != NULL)
		{
			log_info(L"send_to_manager::manager_thread_function. Waiting for work object to abort on exit");
			work_objects_[i]->cancel();
			//wait until it is completed
			while (!work_objects_[i]->is_completed())
				boost::this_thread::sleep(boost::posix_time::milliseconds(50));

			log_info(L"send_to_manager::manager_thread_function. Work object was aborted");

			delete_work_object(i);
		}
	}

	free(work_objects_);

    log_info(L"send_to_manager::manager_thread_function. Main processing thread has finished");
}

bool send_to_manager::add_work_item_int(send_to_work_item_list_t* items, send_to_work_item_id_list_t* item_ids)
{
	for (size_t i=0; i<items->size(); i++)
	{
		work_item_id_t item_id = add_item(&items->at(i));
		item_ids->push_back(item_id);
	}
	return true;
}

bool send_to_manager::remove_work_item_int(const send_to_work_item_id_list_t* work_item_id_list)
{
	send_to_work_item work_item;
	//cancel pending jobs if they are on the list
	for (size_t i=0; i<work_item_id_list->size(); i++)
		cancel_work_object(work_item_id_list->at(i), work_item);

	remove_items(work_item_id_list);
	return true;
}

bool send_to_manager::cancel_work_item_int(const work_item_id_t* work_item_id)
{
	bool ret_val = false;
	send_to_work_item work_item;

	for (size_t i=0; i<pending_items_.size(); i++)
	{
		if (boost::iequals(pending_items_[i].get(), work_item_id->get()) && work_items_.find(work_item_id->get()) != work_items_.end())
		{
			work_item = work_items_[work_item_id->get()];
			work_item.status = e_stwis_canceled;
			time(&work_item.completion_time);
			ret_val = true;
		}
	}

	if (!ret_val)
		ret_val = cancel_work_object(*work_item_id, work_item);

	if (ret_val)
		complete_item(work_item);

	return ret_val;
}

void send_to_manager::get_work_object_items(send_to_work_item_map_t& work_object_items)
{
	for (size_t i=0; i<get_max_work_objects(); i++)
	{
		if (work_objects_[i] != NULL)
		{
			send_to_work_item work_item;
			work_objects_[i]->get_work_item(work_item);
			work_object_items[work_item.item_id] = work_item;
		}
	}
}

bool send_to_manager::get_work_items_int(send_to_get_items_type_e item_type, send_to_work_item_list_t* result_items)
{
	//collect statuses of work object items
	send_to_work_item_map_t work_object_items;
	get_work_object_items(work_object_items);

	send_to_work_item_map_t::iterator it = work_items_.begin();
	while (it != work_items_.end())
	{
		send_to_work_item work_item = it->second;
		if (work_object_items.find(it->first) != work_object_items.end())
			work_item = work_object_items[it->first];

		switch (item_type)
		{
		case e_stgit_all:
			result_items->push_back(work_item);
			break;
		case e_stgit_active:
			if (!is_work_item_completed(work_item.status))
				result_items->push_back(work_item);
			break;
		case e_stgit_completed:
			if (is_work_item_completed(work_item.status))
				result_items->push_back(work_item);
			break;
		}
		++it;
	}

	return true;
}

bool send_to_manager::lookup_work_items_int(const send_to_object_id_list_t* object_ids, send_to_work_item_list_t* result_items)
{
	//collect statuses of work object items
	send_to_work_item_map_t work_object_items;
	get_work_object_items(work_object_items);

	for (size_t i=0; i<object_ids->size(); i++)
	{
		if (object_map_.find(object_ids->at(i)) != object_map_.end())
		{
			work_item_id_t item_id = object_map_[object_ids->at(i)];
			if (work_items_.find(item_id) != work_items_.end())
			{
				send_to_work_item work_item = work_items_[item_id];
				if (work_object_items.find(item_id) != work_object_items.end())
					work_item = work_object_items[item_id];

				result_items->push_back(work_item);
			}
		}
	}

	return true;
}

bool send_to_manager::add_work_object(send_to_work_object* work_object)
{
	bool ret_val = false;

	for (size_t i=0; i<get_max_work_objects(); i++)
	{
		if (work_objects_[i] == NULL)
		{
			work_objects_[i] = work_object;
			work_object_num_ += 1;
			ret_val = true;

			break;
		}
	}

	return ret_val;
}

void send_to_manager::delete_work_object(int idx)
{
	if (work_objects_[idx] != NULL)
	{
		delete work_objects_[idx];
		work_objects_[idx] = NULL;
		work_object_num_ -= 1;
	}
}

bool send_to_manager::cancel_work_object(const work_item_id_t work_item_id, send_to_work_item& work_item)
{
	bool ret_val = false;

	for (size_t i=0; i<get_max_work_objects(); i++)
	{
		if (work_objects_[i] != NULL)
		{
			work_objects_[i]->get_work_item(work_item);
			if (boost::iequals(work_item_id.get(), work_item.item_id.get()))
			{
				log_info(L"send_to_manager::cancel_work_item_int. Waiting for work object to abort");
				work_objects_[i]->cancel();
				//wait until it is completed
				while (!work_objects_[i]->is_completed())
					boost::this_thread::sleep(boost::posix_time::milliseconds(50));

				log_info(L"send_to_manager::cancel_work_item_int. Work object was aborted");

				//get work item to refresh its status
				work_objects_[i]->get_work_item(work_item);

				delete_work_object(i);

				ret_val = true;
				break;
			}
		}
	}
	return ret_val;
}

void send_to_manager::complete_item(const send_to_work_item& work_item)
{
	work_items_[work_item.item_id] = work_item;
	//no need to update pending queue - it has not been changed

	//delete original playback item if needed
	if (work_item.status == e_stwis_success)
	{
		send_to_target_info target_info;
		if (target_broker_->get_target(work_item.target_id, target_info))
		{
			if (target_info.delete_on_success)
			{
				log_info(L"send_to_manager::complete_item. Deleting playback object %1% on success") % work_item.pb_object_id.to_wstring();
				delete_playback_item(work_item.pb_object_id);
			}
		}
	}

    //purge old completed items if needed
    purge_items();

	//write items to disk
	write_items();
}

work_item_id_t send_to_manager::add_item(send_to_work_item* item)
{
	std::wstring wstr;
	uuid::gen_uuid(wstr);

	work_item_id_t ret_id(wstr);
	item->item_id = ret_id;

	//check and generate ddescription if needed
	if (item->description.size() == 0)
	{
		boost::shared_ptr<dvblex::playback::pb_item_t> pbitem;
		if (get_playback_item(item->pb_object_id, pbitem))
		{
			std::wstring rname;
			generate_filename_for_pbitem(pbitem, S2_DEFAULT_FILENAME_PATTERN, rname);
			item->description = string_cast<EC_UTF8>(rname);
		}
	}

	item->status = e_stwis_pending;
	time(&item->creation_time);

	work_items_[ret_id] = *item;
	//add new items to the back of the pending queue
	pending_items_.push_back(ret_id);

	//to the object map
	object_map_[item->pb_object_id] = ret_id;

	//write items to disk
	write_items();

	return ret_id;
}

void send_to_manager::remove_items(const send_to_work_item_id_list_t* work_item_id_list)
{
	//convert items to map for easy lookup
	std::map<work_item_id_t, work_item_id_t> id_map;
	for (size_t i=0; i<work_item_id_list->size(); i++)
	{
		id_map[work_item_id_list->at(i)] = work_item_id_list->at(i);
		//remove item from the general map
		work_items_.erase(work_item_id_list->at(i));
	}
	//remove items from sorted list
	size_t i=0;
	while (i < pending_items_.size())
	{
		if (id_map.find(pending_items_[i]) != id_map.end())
			pending_items_.erase(pending_items_.begin() + i);
		else
			++i;
	}

	//and from lookup map
	object_to_work_item_id_map_t::iterator it_map = object_map_.begin();
	while (it_map != object_map_.end())
	{
		if (id_map.find(it_map->second) != id_map.end())
			object_map_.erase(it_map++);
		else
			++it_map;
	}

	//write items to disk
	write_items();
}

class item_sorter
{
public:
	item_sorter(send_to_work_item_map_t* work_items) : work_items_(work_items) {}
	bool operator()(work_item_id_t const o1, work_item_id_t const o2) const 
	{
		if (work_items_->find(o1) != work_items_->end() && work_items_->find(o2) != work_items_->end())
			return work_items_->find(o1)->second.creation_time > work_items_->find(o2)->second.creation_time;

		return false;
	}
protected:
	send_to_work_item_map_t* work_items_;
};

void send_to_manager::purge_items()
{
    //create a list of the completed items and sort it on creation date
    work_item_id_list_t completed_items;
    send_to_work_item_map_t::iterator it = work_items_.begin();
    while (it != work_items_.end())
    {
        if (is_work_item_completed(it->second.status))
            completed_items.push_back(it->first);

        ++it;
    }
    if (completed_items.size() > 0)
    {
        send_to_work_item_id_list_t items_to_delete;
	    //sort items
	    std::sort(completed_items.begin(), completed_items.end(), item_sorter(&work_items_) );

        int counter = completed_items_to_keep_;
        for (size_t i=0; i<completed_items.size(); i++)
        {
            if (counter <=0)
                items_to_delete.push_back(completed_items[i]);

            counter -= 1;
        }

        if (items_to_delete.size() > 0)
            remove_items(&items_to_delete);
    }
}

bool send_to_manager::init_work_object_info(const send_to_work_item& work_item, work_object_init_info& init_info)
{
	bool ret_val = false;

	log_info(L"send_to_manager::init_work_object_info. Object id %1%, target id %2%") % work_item.pb_object_id.to_wstring() % work_item.target_id.to_wstring();

	init_info.work_item = work_item;
	//get target information
	send_to_target_info target_info;
	if (target_broker_->get_target(work_item.target_id, target_info))
	{
		ret_val = true;

		init_info.delete_on_success = target_info.delete_on_success;
		//query local object filename
		object_id_t object_id(work_item.pb_object_id);
		std::string source_id;
		std::string source_object_id;
        playback::parse_object_id(object_id, source_id, source_object_id);

		messaging::playback::local_object_info_request req(object_id);
		messaging::playback::local_object_info_response resp;
		messaging::message_error err = message_queue_->send(source_id, req, resp);

        if (err == messaging::success && resp.result_)
        {
			init_info.item_filepath = resp.file_path_;
		} else
		{
			//TODO: query playback interface in the future for remote items
			log_error(L"send_to_manager::init_work_object_info. local_object_info_request on %1% return error") % object_id.to_wstring();
			ret_val = false;
		}

		if (ret_val)
		{
			ret_val = get_playback_item(object_id, init_info.playback_item);
			if (!ret_val)
				log_error(L"send_to_manager::init_work_object_info. get_playback_item failed for object %1%") % object_id.to_wstring();
		}

		if (ret_val)
		{
			messaging::recorder::get_recording_options_request rec_req;
			messaging::recorder::get_recording_options_response rec_resp;
			messaging::message_error err = message_queue_->send(messaging::recorder_message_queue_addressee, rec_req, rec_resp);

			ret_val = (err == messaging::success && rec_resp.result_);
			if (ret_val)
			{
				rd_recording_settings_t rec_settings;
				read_from_xml(rec_resp.options_.get(), rec_settings);
				init_info.filename_pattern = rec_settings.get_filename_pattern();
				if (init_info.filename_pattern.size() == 0)
				{
					log_warning(L"send_to_manager::init_work_object_info. Filename pattern is epmty. Using default.");
					init_info.filename_pattern = S2_DEFAULT_FILENAME_PATTERN;
				}
			} else
			{
				log_error(L"send_to_manager::init_work_object_info. get_playback_item failed for object %1%") % object_id.to_wstring();
			}
		}

		if (ret_val)
		{
			//create comskip work unit
			init_info.comskip = target_broker_->create_comskip_work_unit(target_info);

			//create formatter
			init_info.formatter = target_broker_->create_work_unit(target_info.formatter_info);

			//create destination
			init_info.destination = target_broker_->create_work_unit(target_info.dest_info);

			if (init_info.formatter == NULL)
				log_error(L"send_to_manager::init_work_object_info. Failed to create formatter %1%") % string_cast<EC_UTF8>(target_info.formatter_info.id);

			if (init_info.destination == NULL)
				log_error(L"send_to_manager::init_work_object_info. Failed to create destination %1%") % string_cast<EC_UTF8>(target_info.dest_info.id);

			ret_val = init_info.formatter != NULL && init_info.destination != NULL;

			if (!ret_val)
			{
				delete init_info.comskip;
				delete init_info.formatter;
				delete init_info.destination;
			}
		}
	} else
	{
		log_error(L"send_to_manager::init_work_object_info. Target %1% does not exist") % work_item.target_id.to_wstring();
	}

	return ret_val;
}

bool send_to_manager::get_playback_item(dvblink::object_id_t object_id, boost::shared_ptr<pb_item_t>& item)
{
    bool ret_val = false;

    pb_object_requester_t object_requester(false);
    object_requester.object_id_ = object_id;

    dvblink::xml_string_t request;
    if (write_to_xml(object_requester, *(request.object())))
    {
		std::string source_id;
		std::string source_object_id;
		parse_object_id(object_id, source_id, source_object_id);

		messaging::playback::get_objects_request req;
        req.object_id_ = object_requester.object_id_;
        req.object_type_ = object_requester.object_type_;
        req.item_type_ = object_requester.item_type_;
        req.start_position_ = object_requester.start_position_;
        req.requested_count_ = object_requester.requested_count_;
        req.is_children_request_ = object_requester.is_children_request_;
		//dummy server parameters - they are not needed/used for this call
        req.server_address_ = "127.0.0.1";

		messaging::playback::get_objects_response resp;
		messaging::message_error err = message_queue_->send(source_id, req, resp);

        if (err == messaging::success)
        {
            if (resp.object_.item_list_.size() == 1)
            {
                item = resp.object_.item_list_[0];
                ret_val = true;
            }
        }
    }
    return ret_val;
}

bool send_to_manager::delete_playback_item(const dvblink::object_id_t& object_id)
{
    bool ret_val = false;

    pb_object_remover_t object_remover;
    object_remover.object_id_ = object_id;

    dvblink::xml_string_t request;
    if (write_to_xml(object_remover, *(request.object())))
    {
		std::string source_id;
		std::string source_object_id;
		parse_object_id(object_id, source_id, source_object_id);

		messaging::playback::remove_object_request req(object_id);
		messaging::playback::remove_object_response resp;
		messaging::message_error err = message_queue_->send(source_id, req, resp);

		if (err == messaging::success && resp.result_)
        {
			ret_val = true;
        } else
		{
			log_error(L"send_to_manager::delete_playback_item. remove_object_request failed on object %1%") % object_id.to_wstring();
		}
    }
    return ret_val;
}

bool send_to_manager::is_server_operational()
{
	//check the state of a server
    server::server_state_request req;
    server::server_state_response resp;
    message_error error = message_queue_->send(server_message_queue_addressee, req, resp);
    
    bool ret_val = error == success && resp.state_ == dss_active;

	return ret_val;
}

send_to_manager::power_state_e send_to_manager::get_power_state()
{
    power_state_e ret_val = ps_idle;

    if (pending_items_.size() > 0 || work_object_num_ > 0)
        ret_val = ps_no_standby;

    return ret_val;
}

void send_to_manager::apply_power_state(power_state_e power_state)
{
    switch (power_state)
    {
        case ps_idle:
            {
                log_info(L"send_to_manager::apply_power_state. Enabling standby");

		        messaging::power::enable_standby_request req(power_man_client_id_);
		        messaging::power::enable_standby_response resp;
		        messaging::message_error err = message_queue_->send(server_message_queue_addressee, req, resp);
            }
            break;
        case ps_no_standby:
            {
                log_info(L"send_to_manager::apply_power_state. Disabling standby");

		        messaging::power::disable_standby_request req(power_man_client_id_);
		        messaging::power::disable_standby_response resp;
		        messaging::message_error err = message_queue_->send(server_message_queue_addressee, req, resp);
            }
            break;
        default:
            break;
    }
    current_power_state_ = power_state;
}

void send_to_manager::write_items()
{
    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(S2_QUEUE_ROOT_NODE.c_str());
    if (root_node != NULL)
    {
        std::string str;
        send_to_work_item_map_t::iterator it = work_items_.begin();
        while (it != work_items_.end())
        {
            pugi::xml_node item_node = new_child(root_node, S2_QUEUE_ITEM_NODE);
            if (item_node != NULL)
	        {
		        new_child(item_node, S2_QUEUE_ITEM_ID_NODE, it->second.item_id.get());
		        new_child(item_node, S2_QUEUE_OBJECT_ID_NODE, it->second.pb_object_id.get());
		        new_child(item_node, S2_QUEUE_DESCRIPTION_NODE, it->second.description);
		        str = boost::lexical_cast<std::string>(it->second.creation_time);
		        new_child(item_node, S2_QUEUE_CREATION_TIME_NODE, str);
		        new_child(item_node, S2_QUEUE_TARGET_ID_NODE, it->second.target_id.to_string());
		        str = boost::lexical_cast<std::string>(it->second.status);
		        new_child(item_node, S2_QUEUE_STATUS_NODE, str);
		        str = boost::lexical_cast<std::string>(it->second.completion_time);
		        new_child(item_node, S2_QUEUE_COMPLETED_NODE, str);
	        }

	        ++it;
        }

        pugixml_helpers::xmldoc_dump_to_file(doc, settings_->get_send_to_queue_filename().to_string());
    }
}

void send_to_manager::read_items()
{
	work_items_.clear();
	object_map_.clear();
	pending_items_.clear();

    pugi::xml_document doc;
    if (doc.load_file(settings_->get_send_to_queue_filename().to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            pugi::xml_node item_node = root_node.first_child();
            while (item_node != NULL)
            {
				send_to_work_item item;
				std::string str;

				if (get_node_value(item_node, S2_QUEUE_ITEM_ID_NODE, str))
					item.item_id = str;

				if (get_node_value(item_node, S2_QUEUE_OBJECT_ID_NODE, str))
					item.pb_object_id = str;

				get_node_value(item_node, S2_QUEUE_DESCRIPTION_NODE, item.description);

				if (get_node_value(item_node, S2_QUEUE_CREATION_TIME_NODE, str))
					item.creation_time = boost::lexical_cast<time_t>(str);

				if (get_node_value(item_node, S2_QUEUE_TARGET_ID_NODE, str))
					item.target_id = str;

				if (get_node_value(item_node, S2_QUEUE_STATUS_NODE, str))
					item.status = (send_to_work_item_status_e)boost::lexical_cast<unsigned long>(str);

				if (get_node_value(item_node, S2_QUEUE_COMPLETED_NODE, str))
					item.completion_time = boost::lexical_cast<time_t>(str);

				work_items_[item.item_id] = item;

				object_map_[item.pb_object_id] = item.item_id;

				if (!is_work_item_completed(item.status))
					pending_items_.push_back(item.item_id);

                item_node = item_node.next_sibling();
            }
        }
    }
	//sort items
	std::sort(pending_items_.begin(), pending_items_.end(), item_sorter(&work_items_) );
}


} //dvblex
