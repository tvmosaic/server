/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_crypt.h>
#include <dl_strings.h>
#include <dl_utils.h>
#include <dl_logger.h>
#include <tc_types.h>
#include <dl_xml_serialization.h>
#include <dl_send_to_target_cmd.h>
#include <dl_transcoder.h>
#include <dl_message_server.h>
#include <dl_message_addresses.h>
#include <dl_parameters_serializer.h>
#include <dl_file_procedures.h>
#include <dl_language_settings.h>
#include <dl_locale_strings.h>
#include "send_to_common.h"
#include "send_to_comskip.h"
#include "send_to_target.h"
#include "send_to_format_copy.h"
#include "send_to_format_transcode.h"
#include "send_to_format_transmux_mp4.h"
#include "send_to_format_transmux_mkv.h"
#include "send_to_dest_copy_local.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::transcoder;
using namespace dvblink::messaging;
using namespace dvblink::pugixml_helpers;
using namespace dvblex::settings;

namespace dvblex { 

send_to_target_broker::send_to_target_broker(boost::shared_ptr<send_to_settings>& settings, 
                                             const dvblink::messaging::message_queue_t& message_queue) :
	settings_(settings), 
    message_queue_(message_queue)
{
	settings_path_ = settings_->get_send_to_config_filename();
	read_data();
}

send_to_target_broker::~send_to_target_broker()
{
}

void send_to_target_broker::fill_work_unit(const char* id, bool activation_required, send_to_work_unit_info& unit_info)
{
	unit_info.reset();

	unit_info.id = id;
	if (activation_required)
	{
		if (activation_info_.find(unit_info.id) != activation_info_.end())
		{
			unit_info.activation_info = activation_info_[unit_info.id];
			unit_info.is_activated = true;
		} else
		{
			unit_info.is_activated = false;
		}
	} else
	{
		unit_info.is_activated = true;
	}
}

bool send_to_target_broker::is_transcoding_supported()
{
    bool ret_val = false;
    server::ffmpeg_launch_params_request req(tu_transcode_sendto_transcode_id);
    server::ffmpeg_launch_params_response resp;
    ret_val = message_queue_->send(server_message_queue_addressee, req, resp) == success && resp.result_;

    return ret_val;
}

void send_to_target_broker::get_formatters(send_to_work_unit_info_list_t& formatters)
{
	send_to_work_unit_info unit_info;

	fill_work_unit(send_to_formatter_copy::get_id(), send_to_formatter_copy::activation_required(), unit_info);
	formatters.push_back(unit_info);

	fill_work_unit(send_to_formatter_transmux_mp4::get_id(), send_to_formatter_transmux_mp4::activation_required(), unit_info);
	formatters.push_back(unit_info);

	fill_work_unit(send_to_formatter_transmux_mkv::get_id(), send_to_formatter_transmux_mkv::activation_required(), unit_info);
	formatters.push_back(unit_info);

    if (is_transcoding_supported())
    {
	    fill_work_unit(send_to_formatter_transcode::get_id(), send_to_formatter_transcode::activation_required(), unit_info);
	    formatters.push_back(unit_info);
    }
}

void send_to_target_broker::get_destinations(send_to_work_unit_info_list_t& destinations)
{
	send_to_work_unit_info unit_info;

	fill_work_unit(send_to_dest_copy_local::get_id(), send_to_dest_copy_local::activation_required(), unit_info);
	destinations.push_back(unit_info);
}

void send_to_target_broker::activate_work_unit(const send_to_work_unit_id& id, const xml_string_t& activation_info)
{
	activation_info_[id] = activation_info;

	write_data();
}

void send_to_target_broker::get_targets(send_to_target_list_t& targets)
{
	send_to_target_map_t::iterator it = targets_.begin();
	while (it != targets_.end())
	{
		targets.push_back(it->second);
		++it;
	}
}

void send_to_target_broker::set_targets(const send_to_target_list_t& targets)
{
	targets_.clear();
	for (size_t i=0; i<targets.size(); i++)
	{
		targets_[targets[i].id] = targets[i];
	}

	write_data();
}

bool send_to_target_broker::get_target(const base_id_t& target_id, send_to_target_info& target_info)
{
	bool ret_val = false;

	if (targets_.find(target_id) != targets_.end())
	{
		target_info = targets_[target_id];
		ret_val = true;
	}

	return ret_val;
}

send_to_work_unit* send_to_target_broker::create_comskip_work_unit(send_to_target_info& target_info)
{
    if (target_info.use_comskip)
    {
        return new send_to_comskip_work_unit(settings_, target_info.comskip_params);
    }
    return NULL;
}

send_to_work_unit* send_to_target_broker::create_work_unit(send_to_work_unit_init_info& info)
{
	send_to_work_unit* ret_val = NULL;

	if (boost::iequals(info.id, send_to_formatter_copy::get_id()))
	{
		ret_val = new send_to_formatter_copy();
	} else
	if (boost::iequals(info.id, send_to_dest_copy_local::get_id()))
	{
		ret_val = new send_to_dest_copy_local(info.params);
	} else
	if (boost::iequals(info.id, send_to_formatter_transmux_mp4::get_id()))
	{
		ret_val = new send_to_formatter_transmux_mp4(settings_, info.params);
	} else
	if (boost::iequals(info.id, send_to_formatter_transmux_mkv::get_id()))
	{
		ret_val = new send_to_formatter_transmux_mkv(settings_, info.params);
	} else
	if (boost::iequals(info.id, send_to_formatter_transcode::get_id()))
	{
		ret_val = new send_to_formatter_transcode(settings_, info.params, message_queue_);
	}

	return ret_val;
}

bool send_to_target_broker::get_activation_info(const send_to_work_unit_id& id, xml_string_t& activation_info)
{
	bool ret_val = false;

	if (activation_info_.find(id) != activation_info_.end())
	{
		activation_info = activation_info_[id];
		ret_val = true;
	}

	return ret_val;
}

bool send_to_target_broker::get_comskip_setings(const concise_param_map_t& cur_values, parameters_container_t& settings, concise_param_map_t& default_values)
{
    bool enabled = boost::filesystem::exists(settings_->get_comskip().file_.to_boost_filesystem());

    //default values
    default_values = get_default_comskip_values();

    //profiles
    parameter_value_t current_profile = get_value_for_param(sendto_comskip_profile_key, cur_values);

    filesystem_path_t profiles_path = settings_->get_comskip_profiles_dir();
    filesystem::ff_t ini_files;
    filesystem::find_files(profiles_path.to_boost_filesystem(), ini_files);

    settings.id_ = "27ce49a9-986b-4382-b0f7-bae5d5049cd1";
    settings.name_ = language_settings::GetInstance()->GetItemName(IDS_SENDTO_COMSKIP_SETTINGS_NAME);
    settings.desc_ = language_settings::GetInstance()->GetItemName(IDS_SENDTO_COMSKIP_SETTINGS_DESC);

    selectable_param_description_t profiles_element;
    profiles_element.key_ = sendto_comskip_profile_key;
    profiles_element.name_ = language_settings::GetInstance()->GetItemName(IDS_SENDTO_COMSKIP_SETTINGS_PROFILE);
    for (size_t i=0; i<ini_files.size(); i++)
    {
        std::string name = ini_files[i].filename().string();
        profiles_element.parameters_list_.push_back(selectable_param_element_t(name, name));

        if (!current_profile.empty() && boost::iequals(name, current_profile.to_string()))
            profiles_element.selected_param_ = profiles_element.parameters_list_.size() - 1;
    }
    settings.selectable_params_.push_back(profiles_element);

    return enabled;
}

bool send_to_target_broker::process_cmd(const messaging::xml_message_request& request, messaging::xml_message_response& response)
{
	bool ret_val = true;

    if (boost::iequals(request.cmd_id_.get(), SEND_TO_TARGET_CMD_GET_FORMATTERS))
    {
        send_to_get_formatters_response resp;
		get_formatters(resp.formatters_);

		std::string str;
		write_to_xml(resp, str);

		response.result_ = dvblink::xmlcmd_result_success;
		response.xml_ = str;
    } else
    if (boost::iequals(request.cmd_id_.get(), SEND_TO_TARGET_CMD_GET_DESTINATIONS))
	{
		send_to_get_destinations_response resp;
		get_destinations(resp.destinations_);

		std::string str;
		write_to_xml(resp, str);

		response.result_ = dvblink::xmlcmd_result_success;
		response.xml_ = str;
	} else
    if (boost::iequals(request.cmd_id_.get(), SEND_TO_TARGET_CMD_ACTIVATE_WORK_UNIT))
	{
		send_to_activate_work_unit_request req;
		if (read_from_xml(request.xml_.get(), req))
		{
			activate_work_unit(req.wu_id_, req.activation_info_);
			response.result_ = dvblink::xmlcmd_result_success;
		}
	} else
    if (boost::iequals(request.cmd_id_.get(), SEND_TO_TARGET_CMD_GET_TARGETS))
    {
		send_to_get_targets_response resp;
		get_targets(resp.targets_);

		std::string str;
		write_to_xml(resp, str);

		response.result_ = dvblink::xmlcmd_result_success;
		response.xml_ = str;
    } else
    if (boost::iequals(request.cmd_id_.get(), SEND_TO_TARGET_CMD_SET_TARGETS))
	{
		send_to_set_targets_request req;
		if (read_from_xml(request.xml_.get(), req))
		{
			set_targets(req.targets_);
			response.result_ = dvblink::xmlcmd_result_success;
		}
	} else
    if (boost::iequals(request.cmd_id_.get(), SEND_TO_TARGET_CMD_GET_COMSKIP_SETTINGS))
    {
		send_to_get_comskip_settings_request req;
		if (read_from_xml(request.xml_.get(), req))
		{
		    send_to_get_comskip_settings_response resp;
            resp.comskip_enabled_ = get_comskip_setings(req.params_, resp.settings_, resp.default_params_);

		    std::string str;
		    write_to_xml(resp, str);

		    response.result_ = dvblink::xmlcmd_result_success;
		    response.xml_ = str;
        }
    } else
	{
		ret_val = false;
		response.result_ = dvblink::xmlcmd_result_error;
	}

	return ret_val;
}

void send_to_target_broker::reset()
{
	targets_.clear();
	activation_info_.clear();
}

void send_to_target_broker::read_data()
{
    reset();

    std::string str;

    pugi::xml_document doc;
    if (doc.load_file(settings_path_.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            //targets
            pugi::xml_node targets_node = root_node.child(S2_TARGETS_NODE.c_str());
            if (targets_node != NULL)
            {
	            pugi::xml_node target_node = targets_node.first_child();
	            while (target_node != NULL)
	            {
		            send_to_target_info target_info;
                    target_info.comskip_params = get_default_comskip_values();

		            std::string str;

		            if (get_node_value(target_node, S2_TARGET_ID_NODE, str))
			            target_info.id = str;

		            get_node_value(target_node, S2_TARGET_NAME_NODE, target_info.name);

		            if (get_node_value(target_node, S2_TARGET_FMT_ID_NODE, str))
			            target_info.formatter_info.id = str;

		            if (get_node_value(target_node, S2_TARGET_FMT_INFO_NODE, str))
			            target_info.formatter_info.params = str;

		            if (get_node_value(target_node, S2_TARGET_DST_ID_NODE, str))
			            target_info.dest_info.id = str;

		            if (get_node_value(target_node, S2_TARGET_DST_INFO_NODE, str))
			            target_info.dest_info.params = str;

		            if (get_node_value(target_node, S2_TARGET_DELETE_NODE, str))
			            target_info.delete_on_success = boost::lexical_cast<bool>(str);

                    pugi::xml_node comskip_params_node = target_node.child(S2_TARGET_COMSKIP_PARAMS_NODE.c_str());
                    if (comskip_params_node != NULL)
                        read_from_node(comskip_params_node, target_info.comskip_params);

		            if (get_node_value(target_node, S2_TARGET_USE_COMSKIP_NODE, str))
			            target_info.use_comskip = boost::lexical_cast<bool>(str);

		            if (get_node_value(target_node, S2_TARGET_DEFAULT_NODE, str))
			            target_info.default_ = boost::lexical_cast<bool>(str);

		            targets_[target_info.id] = target_info;

                    target_node = target_node.next_sibling();
	            }
            }
            //activation info
            pugi::xml_node activations_node = root_node.child(S2_ACTIVATIONS_NODE.c_str());
            if (activations_node != NULL)
            {
	            pugi::xml_node activation_node = activations_node.first_child();
	            while (activation_node != NULL)
	            {
		            send_to_target_info target_info;
		            std::string str_id;
		            std::string str;

		            get_node_value(activation_node, S2_ACTIVATION_ID_NODE, str_id);
		            get_node_value(activation_node, S2_ACTIVATION_INFO_NODE, str);

		            activation_info_[str_id] = str;

                    activation_node = activation_node.next_sibling();
	            }
            }
        }
    }
}


void send_to_target_broker::write_data()
{
    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(S2_TARGET_CONFIG_ROOT_NODE.c_str());
    if (root_node != NULL)
    {
        //tagrets
        pugi::xml_node targets_node = new_child(root_node, S2_TARGETS_NODE);
        if (targets_node != NULL)
        {
	        std::string str;
	        send_to_target_map_t::iterator it = targets_.begin();
	        while (it != targets_.end())
	        {
                pugi::xml_node target_node = new_child(targets_node, S2_TARGET_NODE);
                if (target_node != NULL)
		        {
			        new_child(target_node, S2_TARGET_ID_NODE, it->second.id.to_string());
			        new_child(target_node, S2_TARGET_NAME_NODE, it->second.name);
			        new_child(target_node, S2_TARGET_FMT_ID_NODE, it->second.formatter_info.id);
			        new_child(target_node, S2_TARGET_FMT_INFO_NODE, it->second.formatter_info.params.get());
			        new_child(target_node, S2_TARGET_DST_ID_NODE, it->second.dest_info.id);
			        new_child(target_node, S2_TARGET_DST_INFO_NODE, it->second.dest_info.params.get());

			        str = boost::lexical_cast<std::string>(it->second.delete_on_success);
			        new_child(target_node, S2_TARGET_DELETE_NODE, str);

			        str = boost::lexical_cast<std::string>(it->second.use_comskip);
			        new_child(target_node, S2_TARGET_USE_COMSKIP_NODE, str);

                    pugi::xml_node comskip_params_node = new_child(target_node, S2_TARGET_COMSKIP_PARAMS_NODE);
                    if (comskip_params_node != NULL)
                        write_to_node(it->second.comskip_params, comskip_params_node);

			        str = boost::lexical_cast<std::string>(it->second.default_);
			        new_child(target_node, S2_TARGET_DEFAULT_NODE, str);
                }

		        ++it;
	        }
        }
        //activation info
        pugi::xml_node activations_node = new_child(root_node, S2_ACTIVATIONS_NODE);
        if (activations_node != NULL)
        {
	        std::string str;
	        send_to_work_unit_activation_info_map_t::iterator it = activation_info_.begin();
	        while (it != activation_info_.end())
	        {
                pugi::xml_node activation_node = new_child(activations_node, S2_ACTIVATION_NODE);
                if (activation_node != NULL)
		        {
			        new_child(activation_node, S2_ACTIVATION_ID_NODE, it->first);
			        new_child(activation_node, S2_ACTIVATION_INFO_NODE, it->second.get());
		        }

		        ++it;
	        }
        }

        pugixml_helpers::xmldoc_dump_to_file(doc, settings_path_.to_string());
    }
}

} // dvblex
