/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_strings.h>
#include "send_to_common.h"
#include "send_to_format_copy.h"

using namespace dvblink;

namespace dvblex {

send_to_formatter_copy::send_to_formatter_copy()
{
}

send_to_formatter_copy::~send_to_formatter_copy()
{
}

bool send_to_formatter_copy::init(const work_unit_pb_item_info& pb_item, const work_unit_media_info& media_in)
{
	media_ = media_in;
	return true;
}

send_to_work_unit_step_result_e send_to_formatter_copy::do_step(work_unit_media_info& media_out)
{
	media_out = media_;
	return e_swusr_finished; 
}

void send_to_formatter_copy::term() 
{
}

void send_to_formatter_copy::cleanup()
{
}

} // dvblex
