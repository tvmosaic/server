/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning (disable : 4100)
#pragma warning (disable : 4244)
#endif

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_common.h>
#include <dl_message_addresses.h>
#include <dl_message_server.h>
#include <components/social.h>
#include "send_to_settings.h"
#include "send_to_manager.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;

namespace dvblex {

social_t::social_t() :
    id_(social_message_queue_addressee.get())
{
    settings_ = boost::shared_ptr<send_to_settings>(new send_to_settings());
    send_to_manager_ = boost::shared_ptr<send_to_manager>(new send_to_manager());
}

social_t::~social_t()
{
    send_to_manager_->term();
    send_to_manager_.reset();

    if (server_ && message_queue_)
    {
        server_->unregister_queue(message_queue_->get_id());
        message_queue_->shutdown();
    }
}

i_result social_t::query_interface(const base_id_t& requestor_id, const i_guid& iid, i_base_object_t& obj)
{
    i_result res = i_error;

    //no other interfaces

    return res;
}

bool social_t::init(const dvblink::i_server_t& server)
{
    server_ = server;

    message_queue_ = share_object_safely(new message_queue(id_.get()));
    message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));

    server_->register_queue(message_queue_);

    settings_->init(message_queue_);
    send_to_manager_->init(settings_, message_queue_);

    return true;
}

void social_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response)
{
    send_to_manager_->start();
    response.result_ = true;
}

void social_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response)
{
    send_to_manager_->stop();
    response.result_ = true;
}

void social_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response)
{
	if (!send_to_manager_->process_cmd(request, response))
    {
        log_error(L"social_cluster::process_cmd_request. Unknown command %1%") % string_cast<EC_UTF8>(request.cmd_id_.get());
		response.result_ = dvblink::xmlcmd_result_error;
    }
}

} // dvblex
