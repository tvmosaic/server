/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread.hpp>
#include <dl_command_queue.h>
#include <dl_send_to_item.h>
#include "send_to_work_unit.h"

namespace dvblex { 

struct work_object_init_info
{
	send_to_work_item work_item;
	send_to_work_unit* comskip;
	send_to_work_unit* formatter;
	send_to_work_unit* destination;
	dvblink::filesystem_path_t item_filepath;
	bool delete_on_success;
	boost::shared_ptr<dvblex::playback::pb_item_t> playback_item;
	std::string filename_pattern;
};

class send_to_work_object
{
private:
	enum commands_e {e_cancel};

    struct s_cancel
    {
    };

public:
    send_to_work_object(const work_object_init_info& init_info);
    ~send_to_work_object();

	void cancel();

	bool is_completed();

	void get_work_item(send_to_work_item& work_item);

protected:
	dvblink::engine::command_queue command_queue_;
	boost::thread* worker_thread_;
	bool exit_flag_;
	work_object_init_info init_info_;

    send_to_work_item work_item_;
    boost::shared_mutex lock_;

	void worker_thread_function();
	void cancel_int();
};

} //dvblex
