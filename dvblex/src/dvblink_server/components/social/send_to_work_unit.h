/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <map>
#include <vector>
#include <dl_pb_item.h>
#include <dl_send_to_item.h>
#include "send_to_common.h"

namespace dvblex {

enum send_to_work_unit_step_result_e
{
	e_swusr_in_progress = 0,
	e_swusr_error,
	e_swusr_finished
};

typedef std::vector<boost::uint8_t> edl_info_t;

struct work_unit_pb_item_info
{
	work_unit_pb_item_info() {}

	work_unit_pb_item_info(const pb_item_info_t& pbi, const std::string& fp) :
		pb_item(pbi), filename_pattern(fp)
	{}

	pb_item_info_t pb_item;
	std::string filename_pattern;
};

struct work_unit_media_info
{
	work_unit_media_info(){}

	work_unit_media_info(const dvblink::filesystem_path_t& fp, const std::string& f) :
		file_path(fp), format(f)
	{}

	dvblink::filesystem_path_t file_path;
	std::string format; //as extension, e.g. mp4, ts etc.
    edl_info_t edl;
};

class send_to_work_unit
{
public:
	send_to_work_unit(){}
	virtual ~send_to_work_unit(){}

	virtual bool init(const work_unit_pb_item_info& pb_item, const work_unit_media_info& media_in) {return false; }

	virtual send_to_work_unit_step_result_e do_step(work_unit_media_info& media_out){return e_swusr_error; };

	virtual void term() {}

	virtual void cleanup() {}
};

} // dvblex
