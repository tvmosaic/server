/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_pugixml_helper.h>
#include <dl_external_control.h>
#include <dl_common.h>
#include "send_to_common.h"
#include "send_to_format_transmux.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::transcoder;
using namespace dvblink::logging;
using namespace dvblink::pugixml_helpers;
using namespace dvblex::settings;

namespace dvblex { 

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

static const std::string send_to_fmt_encode_audio_aac_node = "encode_audio_aac";

send_to_formatter_transmux::send_to_formatter_transmux(boost::shared_ptr<send_to_settings>& settings, const xml_string_t& params) :
	settings_(settings),
	pid_(-1),
    encode_audio_aac_(false)
{
    pugi::xml_document doc;
    if (doc.load_buffer(params.c_str(), params.get().size()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
			std::string str;
			if (get_node_value(root_node, send_to_fmt_encode_audio_aac_node, str))
            {
                int i = 1;
				string_conv::apply(str.c_str(), i, 1);
                encode_audio_aac_ = i == 1 ? true : false;
            }
		    log_info(L"send_to_formatter_transmux(). Encode audio to aac (%1%)") % encode_audio_aac_;
        }
    } else
	{
		log_warning(L"send_to_formatter_transmux(). Can not parse params xml.");
	}
}

send_to_formatter_transmux::~send_to_formatter_transmux()
{
}

boost::int64_t send_to_formatter_transmux::start_transmuxer(const work_unit_pb_item_info& pb_item, const work_unit_media_info& media_in)
{
	log_info(L"send_to_formatter_transmux::start_transmuxer");

    //find the stream types
    bool convert_to_aac = false;
    media_info_ = media_in;

    tc_ffprobe_wrapper ffprobe(settings_->get_ffprobe().file_, settings_->get_ffprobe().dir_, settings_->get_temp_path());
    ffprobe_stream_desc_list_t stream_desc_list;
    if (ffprobe.get_stream_descriptions(media_in.file_path, stream_desc_list))
    {
        for (size_t i=0; i<stream_desc_list.size(); i++)
        {
            ffprobe_stream_desc_t& desc = stream_desc_list[i];

            if (boost::iequals(desc.codec, stream_codec_aac_latm))
                convert_to_aac = true;
        }

    } else
    {
	    log_warning(L"send_to_formatter_transmux::start_transmuxer. ffprobe failed");
    }

	const char* quote_sym = "";
#ifdef _WIN32
	quote_sym = "\"";
#endif

	std::ostringstream ostr;
    std::vector<dvblink::launch_param_t> args;

    args.push_back("-i");
	ostr.clear(); ostr.str("");
	ostr << quote_sym << media_in.file_path.to_string() << quote_sym;
	args.push_back(ostr.str());

    args.push_back("-f");
    args.push_back(get_ffmpeg_format());

    args.push_back("-acodec");
    if (encode_audio_aac_ || convert_to_aac)
    {
        args.push_back("aac");
#if defined(_QNAP_ARM) || defined(_SYNOLOGY_EVANSPORT) || defined(_SYNOLOGY_PPC)
        //these are using (very) old ffmpeg
        args.push_back("-strict");
        args.push_back("-2");
#endif
    } else
    {
        args.push_back("copy");
    }

    args.push_back("-vcodec");
    args.push_back("copy");

    args.push_back("-map");
    args.push_back("0:v:?");

    args.push_back("-map");
    args.push_back("0:a");

    add_extra_ffmpeg_args(stream_desc_list, args);

    //metadata
    mp4_metadata_t mp4_meta;

    if (get_mp4_metadata_from_pb_item(pb_item.pb_item, mp4_meta))
    {
        std::string metadata;

        args.push_back("-metadata");
        metadata = "encoding_tool=" + escape_string_cl_argument(PRODUCT_NAME_UTF8);
        args.push_back(metadata);

        args.push_back("-metadata");
        metadata = "title=" + escape_string_cl_argument(mp4_meta.title_);
        args.push_back(metadata);

        if (mp4_meta.description_.size() > 0)
        {
            args.push_back("-metadata");
            metadata = "description=" + escape_string_cl_argument(mp4_meta.description_);
            args.push_back(metadata);
        }

        if (mp4_meta.channel_name_.size() > 0)
        {
            args.push_back("-metadata");
            metadata = "network=" + escape_string_cl_argument(mp4_meta.channel_name_);
            args.push_back(metadata);
        }

        if (mp4_meta.episode_generated_name_.size() > 0)
        {
            args.push_back("-metadata");
            metadata = "episode_id=" + escape_string_cl_argument(mp4_meta.episode_generated_name_);
            args.push_back(metadata);
        }

        if (mp4_meta.genre_.size() > 0)
        {
            args.push_back("-metadata");
            metadata = "genre=" + escape_string_cl_argument(mp4_meta.genre_);
            args.push_back(metadata);
        }

        if (mp4_meta.year_ > 0)
        {
            args.push_back("-metadata");
    	    ostr.clear(); ostr.str("");
            ostr << mp4_meta.year_;
            metadata = "date=" + escape_string_cl_argument(ostr.str());
            args.push_back(metadata);
        }

        if (mp4_meta.season_num_> 0)
        {
            args.push_back("-metadata");
    	    ostr.clear(); ostr.str("");
            ostr << mp4_meta.season_num_;
            metadata = "season_number=" + escape_string_cl_argument(ostr.str());
            args.push_back(metadata);
        }

        if (mp4_meta.episode_num_> 0)
        {
            args.push_back("-metadata");
    	    ostr.clear(); ostr.str("");
            ostr << mp4_meta.episode_num_;
            metadata = "episode_sort=" + escape_string_cl_argument(ostr.str());
            args.push_back(metadata);
        }

        if (mp4_meta.is_hd_)
        {
            args.push_back("-metadata");
            metadata = "hd_video=" + escape_string_cl_argument("1");
            args.push_back(metadata);
        }

    }

    args.push_back("-y");

	//output file
	ostr.clear(); ostr.str("");
	ostr << quote_sym << output_filepath_.to_string() << quote_sym;
	args.push_back(ostr.str());

    return external_control::start_process(settings_->get_ffmpeg().file_, &settings_->get_ffmpeg().dir_, args, external_control::BelowNormalPriority);
}

bool send_to_formatter_transmux::init(const work_unit_pb_item_info& pb_item, const work_unit_media_info& media_in)
{
	bool ret_val = false;

	//generate temp file name for a transcoded item
	std::string str;
	uuid::gen_uuid(str);
	output_filepath_ = settings_->get_send_to_temp_dir() / str;

	//start transcoder
	pid_ = start_transmuxer(pb_item, media_in);
	if (pid_ > 0)
	{
		ret_val = true;
	} else
	{
		log_error(L"send_to_formatter_transmux::init. Failed to start transmuxer on %1%") % media_in.file_path.to_wstring();
	}

	return ret_val;
}

send_to_work_unit_step_result_e send_to_formatter_transmux::do_step(work_unit_media_info& media_out)
{
	send_to_work_unit_step_result_e step_res = e_swusr_error;

	if (external_control::is_process_running(pid_))
	{
		step_res = e_swusr_in_progress;
		boost::this_thread::sleep(boost::posix_time::milliseconds(10));
	} else
	{
        media_out = media_info_;
		media_out.file_path = output_filepath_;
		media_out.format = get_extension();
		step_res = e_swusr_finished;
	}

	return step_res;
}

void send_to_formatter_transmux::term()
{
	if (pid_ != -1)
	{
		external_control::kill_process(pid_);
		pid_ = -1;
	}
}

void send_to_formatter_transmux::cleanup()
{
	if (output_filepath_.get().size() > 0)
	{
		try {
			boost::filesystem::remove(output_filepath_.to_boost_filesystem());
		} catch(...){}
	}
}

} // dvblex
