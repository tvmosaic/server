/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <map>
#include <vector>
#include "send_to_format_transmux.h"

namespace dvblex {

class send_to_formatter_transmux_mp4 : public send_to_formatter_transmux
{
public:
	static const char* get_id() {return "format-transmux";}
	static bool activation_required() {return false;}

    send_to_formatter_transmux_mp4(boost::shared_ptr<send_to_settings>& settings, const dvblink::xml_string_t& params) :
        send_to_formatter_transmux(settings, params)
        {}

protected:
    virtual std::string get_ffmpeg_format(){return "mp4";}
    virtual std::string get_extension(){return "mp4";}

    virtual void add_extra_ffmpeg_args(const dvblink::transcoder::ffprobe_stream_desc_list_t& stream_desc_list, std::vector<dvblink::launch_param_t>& args)
    {
        for (size_t i=0; i<stream_desc_list.size(); i++)
        {
            const dvblink::transcoder::ffprobe_stream_desc_t& desc = stream_desc_list[i];

            if (boost::iequals(desc.codec, dvblink::transcoder::stream_codec_hevc))
            {
                args.push_back("-tag:v");
                args.push_back("hvc1");
                break;
            }
        }
    }
};

} // dvblex
