/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_file_procedures.h>
#include "send_to_work_object.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

namespace dvblex { 

send_to_work_object::send_to_work_object(const work_object_init_info& init_info) :
  init_info_(init_info), work_item_(init_info.work_item)
{
	exit_flag_ = false;
	worker_thread_ = new boost::thread(boost::bind(&send_to_work_object::worker_thread_function, this));
}

send_to_work_object::~send_to_work_object()
{
	exit_flag_ = true;
	worker_thread_->join();
	delete worker_thread_;
}

bool send_to_work_object::is_completed()
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);
	return is_work_item_completed(work_item_.status);
}

void send_to_work_object::get_work_item(send_to_work_item& work_item)
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);
	work_item = work_item_;
}

void send_to_work_object::cancel()
{
	s_cancel params;
	command_queue_.ExecuteCommand(e_cancel, &params);
}

void send_to_work_object::worker_thread_function()
{
	log_info(L"send_to_work_object::worker_thread_function. Worker thread started (%1%)") % init_info_.work_item.item_id.to_wstring();

	while (!exit_flag_)
	{
		SDLCommandItem* item;
		while (command_queue_.PeekCommand(&item))
		{
			switch (item->id)
			{
			case e_cancel:
				{
					cancel_int();
				}
				break;
			default:
				break;
			}
			command_queue_.FinishCommand(&item);
		}

		switch (init_info_.work_item.status)
		{
		case e_stwis_pending:
			//assume currently that local file is always available
			//in the future we first would need to fetch file locally, using playback interface

            //at the moment fetchin state is misused for doing comskip
            if (init_info_.comskip != NULL)
            {
		        log_info(L"send_to_work_object::worker_thread_function. Init comskip for %1% (%2%)") % init_info_.work_item.item_id.to_wstring() % init_info_.item_filepath.to_wstring();
		        if (init_info_.comskip->init(work_unit_pb_item_info(init_info_.playback_item, init_info_.filename_pattern), 
				        work_unit_media_info(init_info_.item_filepath, std::string("ts"))))
			        init_info_.work_item.status = e_stwis_fetching;
		        else
			        init_info_.work_item.set_finished_status(e_stwis_error);
            } else
            {
			    init_info_.work_item.status = e_stwis_fetching;
            }
			break;
		case e_stwis_fetching:
            if (init_info_.comskip != NULL)
            {
				work_unit_media_info media_info;
				send_to_work_unit_step_result_e res = init_info_.comskip->do_step(media_info);
				switch (res)
				{
				case e_swusr_finished:
					{
						init_info_.comskip->term();
						log_info(L"send_to_work_object::worker_thread_function. Init formatter for %1% (%2%)") % init_info_.work_item.item_id.to_wstring() % media_info.file_path.to_wstring();
						if (init_info_.formatter->init(work_unit_pb_item_info(init_info_.playback_item, init_info_.filename_pattern), media_info))
							init_info_.work_item.status = e_stwis_formatting;
						else
							init_info_.work_item.set_finished_status(e_stwis_error);
					}
					break;
				case e_swusr_error:
					init_info_.work_item.set_finished_status(e_stwis_error);
					break;
				default:
					break;
				}
            } else
            {
			    log_info(L"send_to_work_object::worker_thread_function. Init formatter for %1% (%2%)") % init_info_.work_item.item_id.to_wstring() % init_info_.item_filepath.to_wstring();
			    if (init_info_.formatter->init(work_unit_pb_item_info(init_info_.playback_item, init_info_.filename_pattern), 
					    work_unit_media_info(init_info_.item_filepath, std::string("ts"))))
				    init_info_.work_item.status = e_stwis_formatting;
			    else
				    init_info_.work_item.set_finished_status(e_stwis_error);
            }
			break;
		case e_stwis_formatting:
			{
				work_unit_media_info media_info;
				send_to_work_unit_step_result_e res = init_info_.formatter->do_step(media_info);
				switch (res)
				{
				case e_swusr_finished:
					{
						init_info_.formatter->term();
						log_info(L"send_to_work_object::worker_thread_function. Init destination for %1% (%2%)") % init_info_.work_item.item_id.to_wstring() % media_info.file_path.to_wstring();
						if (init_info_.destination->init(work_unit_pb_item_info(init_info_.playback_item, init_info_.filename_pattern), media_info))
							init_info_.work_item.status = e_stwis_sending;
						else
							init_info_.work_item.set_finished_status(e_stwis_error);
					}
					break;
				case e_swusr_error:
					init_info_.work_item.set_finished_status(e_stwis_error);
					break;
				default:
					break;
				}
			}
			break;
		case e_stwis_sending:
			{
				work_unit_media_info media_info;
				send_to_work_unit_step_result_e res = init_info_.destination->do_step(media_info);
				switch (res)
				{
				case e_swusr_finished:
					init_info_.destination->term();
					init_info_.work_item.set_finished_status(e_stwis_success);
					log_info(L"send_to_work_object::worker_thread_function. Work object %1% finished with success") % init_info_.work_item.item_id.to_wstring();
					break;
				case e_swusr_error:
					init_info_.work_item.set_finished_status(e_stwis_error);
					break;
				default:
					break;
				}
			}
			break;
		default:
			//wait for termination
			boost::this_thread::sleep(boost::posix_time::milliseconds(20));
			break;
		}

        //updated cached work item status
        {
          boost::unique_lock<boost::shared_mutex> lock(lock_);
          work_item_ = init_info_.work_item;
        }
	}

	//reset and delete init_info_ data on exit
    if (init_info_.comskip != NULL)
    {
	    init_info_.comskip->cleanup();
	    delete init_info_.comskip;
    }

    init_info_.formatter->cleanup();
	delete init_info_.formatter;

	init_info_.destination->cleanup();
	delete init_info_.destination;

	log_info(L"send_to_work_object::worker_thread_function. Worker thread finished (%1%)") % init_info_.work_item.item_id.to_wstring();
}

void send_to_work_object::cancel_int()
{
	log_info(L"send_to_work_object::cancel_int. Cancel work object (%1%)") % init_info_.work_item.item_id.to_wstring();

	switch (init_info_.work_item.status)
	{
	case e_stwis_fetching:
        if (init_info_.comskip != NULL)
		    init_info_.comskip->term();
		break;
	case e_stwis_formatting:
		init_info_.formatter->term();
		break;
	case e_stwis_sending:
		init_info_.destination->term();
		break;
	case e_stwis_pending:
		break;
	default:
		break;
	}
	init_info_.work_item.set_finished_status(e_stwis_canceled);
}

} //dvblex
