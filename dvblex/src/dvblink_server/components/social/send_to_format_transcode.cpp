/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_pugixml_helper.h>
#include <dl_external_control.h>
#include <dl_transcoder.h>
#include <dl_message_server.h>
#include <dl_message_addresses.h>
#include "send_to_common.h"
#include "send_to_format_transcode.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::messaging;
using namespace dvblink::transcoder;
using namespace dvblink::logging;
using namespace dvblink::pugixml_helpers;
using namespace dvblex::settings;

namespace dvblex { 

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

static const std::string send_to_fmt_width_node = "width";
static const std::string send_to_fmt_height_node = "height";
static const std::string send_to_fmt_bitrate_node = "bitrate";
static const std::string send_to_fmt_all_tracks_node = "all_audio_tracks";

send_to_formatter_transcode::send_to_formatter_transcode(boost::shared_ptr<send_to_settings>& settings, const xml_string_t& params,
                                                         const dvblink::messaging::message_queue_t& message_queue) :
	settings_(settings),
	pid_(-1),
	width_(-1),
	height_(-1),
	bitrate_(-1),
    message_queue_(message_queue)
{
    pugi::xml_document doc;
    if (doc.load_buffer(params.c_str(), params.get().size()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
			std::string str;
			if (get_node_value(root_node, send_to_fmt_width_node, str))
				string_conv::apply(str.c_str(), width_, -1);

			if (get_node_value(root_node, send_to_fmt_height_node, str))
				string_conv::apply(str.c_str(), height_, -1);

			if (get_node_value(root_node, send_to_fmt_bitrate_node, str))
				string_conv::apply(str.c_str(), bitrate_, -1);
        }
    } else
	{
		log_error(L"send_to_formatter_transcode. Can not parse params xml.");
	}
}

send_to_formatter_transcode::~send_to_formatter_transcode()
{
}

boost::int64_t send_to_formatter_transcode::start_transcoder(const work_unit_pb_item_info& pb_item, int width, int height, int bitrate, const work_unit_media_info& media_in)
{
	log_info(L"send_to_formatter_transcode::start_transcoder. w %1%, h %2%, b %3%") % width % height % bitrate;

    boost::int64_t pid = -1;
    media_info_ = media_in;

    server::ffmpeg_launch_params_request req(tu_transcode_sendto_transcode_id);
    server::ffmpeg_launch_params_response resp;
    if (message_queue_->send(server_message_queue_addressee, req, resp) == messaging::success && resp.result_)
    {
        ffmpeg_param_template_init_t init_info;

        init_info.src_ = media_in.file_path.to_string();
        init_info.dest_ = output_filepath_.to_string();

        if (bitrate > 0)
            init_info.bitrate_kbits_ = bitrate;
        if (width > 0 && height > 0)
        {
            init_info.height_ = std::min(width, height);
            init_info.width_ = std::max(width, height);
        }

        //metadata
        mp4_metadata_t mp4_meta;

        if (get_mp4_metadata_from_pb_item(pb_item.pb_item, mp4_meta))
        {
            std::string metadata;
        	std::ostringstream ostr;

            init_info.extra_params.push_back("-metadata");
            metadata = "encoding_tool=" + escape_string_cl_argument(PRODUCT_NAME_UTF8);
            init_info.extra_params.push_back(metadata);

            init_info.extra_params.push_back("-metadata");
            metadata = "title=" + escape_string_cl_argument(mp4_meta.title_);
            init_info.extra_params.push_back(metadata);

            if (mp4_meta.description_.size() > 0)
            {
                init_info.extra_params.push_back("-metadata");
                metadata = "description=" + escape_string_cl_argument(mp4_meta.description_);
                init_info.extra_params.push_back(metadata);
            }

            if (mp4_meta.channel_name_.size() > 0)
            {
                init_info.extra_params.push_back("-metadata");
                metadata = "network=" + escape_string_cl_argument(mp4_meta.channel_name_);
                init_info.extra_params.push_back(metadata);
            }

            if (mp4_meta.episode_generated_name_.size() > 0)
            {
                init_info.extra_params.push_back("-metadata");
                metadata = "episode_id=" + escape_string_cl_argument(mp4_meta.episode_generated_name_);
                init_info.extra_params.push_back(metadata);
            }

            if (mp4_meta.genre_.size() > 0)
            {
                init_info.extra_params.push_back("-metadata");
                metadata = "genre=" + escape_string_cl_argument(mp4_meta.genre_);
                init_info.extra_params.push_back(metadata);
            }

            if (mp4_meta.year_ > 0)
            {
                init_info.extra_params.push_back("-metadata");
    	        ostr.clear(); ostr.str("");
                ostr << mp4_meta.year_;
                metadata = "date=" + escape_string_cl_argument(ostr.str());
                init_info.extra_params.push_back(metadata);
            }

            if (mp4_meta.season_num_> 0)
            {
                init_info.extra_params.push_back("-metadata");
    	        ostr.clear(); ostr.str("");
                ostr << mp4_meta.season_num_;
                metadata = "season_number=" + escape_string_cl_argument(ostr.str());
                init_info.extra_params.push_back(metadata);
            }

            if (mp4_meta.episode_num_> 0)
            {
                init_info.extra_params.push_back("-metadata");
    	        ostr.clear(); ostr.str("");
                ostr << mp4_meta.episode_num_;
                metadata = "episode_sort=" + escape_string_cl_argument(ostr.str());
                init_info.extra_params.push_back(metadata);
            }

            if (mp4_meta.is_hd_)
            {
                init_info.extra_params.push_back("-metadata");
                metadata = "hd_video=" + escape_string_cl_argument("1");
                init_info.extra_params.push_back(metadata);
            }

        }

        std::vector<dvblink::launch_param_t> args;
        init_ffmpeg_launch_arguments(resp.launch_params_.arguments_, init_info, args);

        pid = external_control::start_process(resp.launch_params_.ffmpeg_exepath_, 
            resp.launch_params_.ffmpeg_dir_.empty() ? NULL : &resp.launch_params_.ffmpeg_dir_,
            args, external_control::BelowNormalPriority,
            resp.launch_params_.ffmpeg_env_.size() == 0 ? NULL : &resp.launch_params_.ffmpeg_env_);
    } else
    {
        log_error(L"send_to_formatter_transcode::start_transcoder. Failed to get ffmpeg launch params");
    }

    return pid;
}

bool send_to_formatter_transcode::init(const work_unit_pb_item_info& pb_item, const work_unit_media_info& media_in)
{
	bool ret_val = false;

	//generate temp file name for a transcoded item
	std::string str;
	uuid::gen_uuid(str);
	output_filepath_ = settings_->get_send_to_temp_dir() / str;

	//start transcoder
	pid_ = start_transcoder(pb_item, width_, height_, bitrate_, media_in);
	if (pid_ > 0)
	{
		ret_val = true;
	} else
	{
		log_error(L"send_to_formatter_transcode::init. Failed to start transcoder on %1%") % media_in.file_path.to_wstring();
	}

	return ret_val;
}

send_to_work_unit_step_result_e send_to_formatter_transcode::do_step(work_unit_media_info& media_out)
{
	send_to_work_unit_step_result_e step_res = e_swusr_error;

	if (external_control::is_process_running(pid_))
	{
		step_res = e_swusr_in_progress;
		boost::this_thread::sleep(boost::posix_time::milliseconds(10));
	} else
	{
        media_out = media_info_;
		media_out.file_path = output_filepath_;
		media_out.format = "mp4";
		step_res = e_swusr_finished;
	}

	return step_res;
}

void send_to_formatter_transcode::term()
{
	if (pid_ != -1)
	{
		external_control::kill_process(pid_);
		pid_ = -1;
	}
}

void send_to_formatter_transcode::cleanup()
{
	if (output_filepath_.get().size() > 0)
	{
		try {
			boost::filesystem::remove(output_filepath_.to_boost_filesystem());
		} catch(...){}
	}
}

} // dvblex
