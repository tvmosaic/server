/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/thread.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_external_control.h>
#include <dl_common.h>
#include <dl_file_procedures.h>
#include "send_to_common.h"
#include "send_to_comskip.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblex::settings;

static const std::string comskip_basename = "tvmosaic_12345";

namespace dvblex { 

send_to_comskip_work_unit::send_to_comskip_work_unit(boost::shared_ptr<send_to_settings>& settings, const concise_param_map_t& params) :
	settings_(settings),
	pid_(-1)
{
    profile_id_ = get_value_for_param(sendto_comskip_profile_key, params).to_string();
}

send_to_comskip_work_unit::~send_to_comskip_work_unit()
{
}

boost::int64_t send_to_comskip_work_unit::start_comskip(const work_unit_media_info& media_in)
{
	log_info(L"send_to_comskip_work_unit::start_comskip on %1%") % media_in.file_path.to_wstring();

    if (profile_id_.empty())
    {
		log_error(L"send_to_comskip_work_unit::start_comskip. Profile id cannot be empty");

        return false;
    }

    filesystem_path_t profile_path = settings_->get_comskip_profiles_dir() / profile_id_;


	const char* quote_sym = "";
#ifdef _WIN32
	quote_sym = "\"";
#endif

	std::ostringstream ostr;
    std::vector<dvblink::launch_param_t> args;

    args.push_back("--ini");
	ostr.clear(); ostr.str("");
	ostr << quote_sym << profile_path.to_string() << quote_sym;
	args.push_back(ostr.str());

    args.push_back("--output");
	ostr.clear(); ostr.str("");
	ostr << quote_sym << working_dir_.to_string() << quote_sym;
	args.push_back(ostr.str());

    args.push_back("--output-filename");
	ostr.clear(); ostr.str("");
	ostr << quote_sym << comskip_basename << quote_sym;
	args.push_back(ostr.str());

    args.push_back("--threads");
    unsigned int nthreads = boost::thread::hardware_concurrency();
	ostr.clear(); ostr.str("");
	ostr << nthreads;
	args.push_back(ostr.str());

	ostr.clear(); ostr.str("");
	ostr << quote_sym << media_in.file_path.to_string() << quote_sym;
	args.push_back(ostr.str());

    return external_control::start_process(settings_->get_comskip().file_, &settings_->get_comskip().dir_, args, external_control::BelowNormalPriority);
}

bool send_to_comskip_work_unit::init(const work_unit_pb_item_info& pb_item, const work_unit_media_info& media_in)
{
	bool ret_val = false;

    media_info_ = media_in;

	//generate temp file name for a transcoded item
	std::string str;
	uuid::gen_uuid(str);
	working_dir_ = settings_->get_temp_path() / str;

    //create directory
    try {
        boost::filesystem::create_directories(working_dir_.to_boost_filesystem());
    } catch(...) {}

	//start transcoder
	pid_ = start_comskip(media_in);
	if (pid_ > 0)
	{
		ret_val = true;
	} else
	{
		log_error(L"send_to_comskip_work_unit::init. Failed to start comskip on %1%") % media_in.file_path.to_wstring();
	}

	return ret_val;
}

send_to_work_unit_step_result_e send_to_comskip_work_unit::do_step(work_unit_media_info& media_out)
{
	send_to_work_unit_step_result_e step_res = e_swusr_error;

	if (external_control::is_process_running(pid_))
	{
		step_res = e_swusr_in_progress;
		boost::this_thread::sleep(boost::posix_time::milliseconds(10));
	} else
	{
        media_out = media_info_;

        //read edl file 
        dvblink::filesystem_path_t edl_path = working_dir_ / (comskip_basename + ".edl");

        FILE* f = filesystem::universal_open_file(edl_path, "r+b");
        if (f != NULL)
        {
            boost::int64_t file_size = filesystem::get_file_size(edl_path.to_string());
            if (file_size > 0)
            {
                media_out.edl.resize(file_size);
                fread(&media_out.edl[0], file_size, 1, f);
            } else
            {
    		    log_warning(L"send_to_comskip_work_unit::do_step. edl file size is 0 (%1%)") % edl_path.to_wstring();
            }
            fclose(f);
        } else
        {
    		log_warning(L"send_to_comskip_work_unit::do_step. Cannot read edl file %1%") % edl_path.to_wstring();
        }

		step_res = e_swusr_finished;
	}

	return step_res;
}

void send_to_comskip_work_unit::term()
{
	if (pid_ != -1)
	{
		external_control::kill_process(pid_);
		pid_ = -1;
	}
}

void send_to_comskip_work_unit::cleanup()
{
	if (!working_dir_.empty())
	{
		try {
            boost::filesystem::remove_all(working_dir_.to_boost_filesystem());
		} catch(...){}
	}
}

} // dvblex
