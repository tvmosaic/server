/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_installation_settings.h>

namespace dvblex {

class send_to_settings : public settings::installation_settings_t
{
public:
    send_to_settings();
    ~send_to_settings() {}

    virtual void init(dvblink::messaging::message_queue_t& message_queue);

    void load();
    void save();

    const dvblink::filesystem_path_t get_send_to_temp_dir();
	const dvblink::filesystem_path_t get_send_to_queue_filename();
    const dvblink::filesystem_path_t get_send_to_config_filename(){return config_filename_;}
    const dvblink::filesystem_path_t get_comskip_profiles_dir();
    
	size_t get_max_work_objects();

private:
    dvblink::filesystem_path_t temp_dir_;
    dvblink::filesystem_path_t component_path_;
    dvblink::filesystem_path_t config_filename_;
};

} // dvblex
