/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_common.h>
#include <dl_crypt.h>
#include <dl_strings.h>
#include <dl_installation_settings.h>
#include "send_to_settings.h"

using namespace dvblink;
using namespace dvblex::settings;
using namespace dvblink::engine;

static const std::string SEND_TO_TEMP_DIR = "65e20fbd-4460-4c0d-a278-53db17a844bd";
static const std::string SEND_TO_QUEUE_FILENAME = "queue.xml";
static const std::string SEND_TO_SETTINGS_FILENAME = "send_to_settings.xml";
static const std::string SEND_TO_COMPONENT_NAME = "send_to";
static const std::string COMSKIP_COMPONENT_NAME = "comskip";
static const std::string COMSKIP_PROFILES_DIR = "profiles";

namespace dvblex {

#define DEFAULT_MAX_WORK_OBJECTS	(1)

send_to_settings::send_to_settings() :
    settings::installation_settings_t()
{
}

void send_to_settings::init(dvblink::messaging::message_queue_t& message_queue)
{
    settings::installation_settings_t::init(message_queue);

    temp_dir_ = get_temp_path() / SEND_TO_TEMP_DIR;
    component_path_ = get_shared_component_path(SEND_TO_COMPONENT_NAME) / SEND_TO_COMPONENT_NAME;

    config_filename_ = get_config_path() / SEND_TO_SETTINGS_FILENAME;

    //make both paths exist
    try {
        boost::filesystem::create_directories(temp_dir_.to_boost_filesystem());
    } catch(...){}

    try {
        boost::filesystem::create_directories(component_path_.to_boost_filesystem());
    } catch(...){}
}

void send_to_settings::load()
{
}

void send_to_settings::save()
{
}

const filesystem_path_t send_to_settings::get_send_to_temp_dir()
{
	return temp_dir_;
}

const filesystem_path_t send_to_settings::get_send_to_queue_filename()
{
	return component_path_ / SEND_TO_QUEUE_FILENAME;
}

const filesystem_path_t send_to_settings::get_comskip_profiles_dir()
{
    dvblink::filesystem_path_t profiles_dir = get_shared_component_path(COMSKIP_COMPONENT_NAME);
    return profiles_dir / COMSKIP_PROFILES_DIR;
}

size_t send_to_settings::get_max_work_objects()
{
	return DEFAULT_MAX_WORK_OBJECTS;
}

} // dvblex
