/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread.hpp>
#include <dl_command_queue.h>
#include <dl_message_common.h>
#include <dl_pb_item.h>
#include <dl_send_to_item.h>
#include <dl_send_to_cmd.h>
#include "send_to_work_object.h"
#include "send_to_settings.h"
#include "send_to_target.h"

namespace dvblex {

typedef std::map<dvblink::work_item_id_t, send_to_work_item> send_to_work_item_map_t;

typedef std::vector<dvblink::work_item_id_t> work_item_id_list_t;

typedef std::map<dvblink::object_id_t, dvblink::work_item_id_t> object_to_work_item_id_map_t;

class send_to_manager
{
protected:
    enum commands_e {e_add_item, e_remove_item, e_get_work_items, e_lookup_work_items, e_cancel_item};

    enum power_state_e {ps_idle, ps_no_standby};

    struct s_add_item_params
    {
        send_to_work_item_list_t* items;
		send_to_work_item_id_list_t* item_ids;
        bool result;
    };

    struct s_remove_item_params
    {
		send_to_work_item_id_list_t* work_item_id_list;
        bool result;
    };

    struct s_get_work_items_params
    {
		send_to_get_items_type_e item_type;
		send_to_work_item_list_t* result_items;
        bool result;
    };

    struct s_lookup_work_items
    {
		send_to_object_id_list_t* object_ids;
		send_to_work_item_list_t* result_items;
        bool result;
    };

    struct s_cancel_item_params
    {
		dvblink::work_item_id_t* work_item_id;
        bool result;
    };

public:
    send_to_manager();
    virtual ~send_to_manager();

    bool init(boost::shared_ptr<send_to_settings>& settings, const dvblink::messaging::message_queue_t& message_queue);
	void term();

	bool start();
    void stop();

    bool process_cmd(const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response);

protected:
	send_to_work_item_map_t work_items_;
	work_item_id_list_t pending_items_;
	object_to_work_item_id_map_t object_map_;

    boost::shared_ptr<send_to_settings> settings_;
    dvblink::engine::command_queue command_queue_;
	boost::thread* manager_thread_;
	bool exit_flag_;
	send_to_work_object** work_objects_;
	size_t work_object_num_;
	send_to_target_broker* target_broker_;
    dvblink::messaging::message_queue_t message_queue_;
    time_t processing_start_delay_sec_;
    dvblink::client_id_t power_man_client_id_;
    power_state_e current_power_state_;
    size_t completed_items_to_keep_;

	size_t get_max_work_objects();
	void manager_thread_function();
	bool add_work_item_int(send_to_work_item_list_t* items, send_to_work_item_id_list_t* item_ids);
	bool remove_work_item_int(const send_to_work_item_id_list_t* work_item_id_list);
	bool get_work_items_int(send_to_get_items_type_e item_type, send_to_work_item_list_t* result_items);
	bool lookup_work_items_int(const send_to_object_id_list_t* object_ids, send_to_work_item_list_t* result_items);
    bool cancel_work_item_int(const dvblink::work_item_id_t* work_item_id);

	bool add_work_object(send_to_work_object* work_object);
	void delete_work_object(int idx);
    bool cancel_work_object(const dvblink::work_item_id_t work_item_id, send_to_work_item& work_item);
	void get_work_object_items(send_to_work_item_map_t& work_object_items);

	dvblink::work_item_id_t add_item(send_to_work_item* item);
	void remove_items(const send_to_work_item_id_list_t* work_item_id_list);
	void complete_item(const send_to_work_item& work_item);
	void write_items();
	void read_items();
	bool init_work_object_info(const send_to_work_item& work_item, work_object_init_info& inti_info);
	bool get_playback_item(dvblink::object_id_t object_id, boost::shared_ptr<dvblex::playback::pb_item_t>& item);
	bool delete_playback_item(const dvblink::object_id_t& object_id);
	bool is_server_operational();
	void purge_items();

    void apply_power_state(power_state_e power_state);
    power_state_e get_power_state();
};

} //dvblex
