/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <map>
#include <vector>
#include <tc_ffprobe_wrapper.h>
#include "send_to_work_unit.h"
#include "send_to_settings.h"

namespace dvblex {

class send_to_formatter_transmux : public send_to_work_unit
{
public:
    send_to_formatter_transmux(boost::shared_ptr<send_to_settings>& settings, const dvblink::xml_string_t& params);
	virtual ~send_to_formatter_transmux();

	virtual bool init(const work_unit_pb_item_info& pb_item, const work_unit_media_info& media_in);

	virtual send_to_work_unit_step_result_e do_step(work_unit_media_info& media_out);

	virtual void term();

	virtual void cleanup();
protected:
	dvblink::filesystem_path_t output_filepath_;
	boost::shared_ptr<send_to_settings> settings_;
	boost::int64_t pid_;
    bool encode_audio_aac_;
    work_unit_media_info media_info_;

	boost::int64_t start_transmuxer(const work_unit_pb_item_info& pb_item, const work_unit_media_info& media_in);

    virtual std::string get_ffmpeg_format(){return "";}
    virtual std::string get_extension(){return "";}
    virtual void add_extra_ffmpeg_args(const dvblink::transcoder::ffprobe_stream_desc_list_t& stream_desc_list, std::vector<dvblink::launch_param_t>& args){}
};

} // dvblex
