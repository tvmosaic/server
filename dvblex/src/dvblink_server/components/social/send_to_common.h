/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <map>
#include <dl_pb_item.h>

namespace dvblex {

const std::string S2_QUEUE_ROOT_NODE = "queue";
const std::string S2_QUEUE_ITEM_NODE = "item";
const std::string S2_QUEUE_ITEM_ID_NODE = "item_id";
const std::string S2_QUEUE_OBJECT_ID_NODE = "object_id";
const std::string S2_QUEUE_DESCRIPTION_NODE = "description";
const std::string S2_QUEUE_CREATION_TIME_NODE = "created";
const std::string S2_QUEUE_TARGET_ID_NODE = "target";
const std::string S2_QUEUE_STATUS_NODE = "status";
const std::string S2_QUEUE_COMPLETED_NODE = "completed";

const std::string S2_TARGET_CONFIG_ROOT_NODE = "targets_config";
const std::string S2_TARGETS_NODE = "targets";
const std::string S2_TARGET_NODE = "target";
const std::string S2_TARGET_ID_NODE = "id";
const std::string S2_TARGET_NAME_NODE = "name";
const std::string S2_TARGET_FMT_ID_NODE = "fmt_id";
const std::string S2_TARGET_FMT_INFO_NODE = "fmt_info";
const std::string S2_TARGET_DST_ID_NODE = "dst_id";
const std::string S2_TARGET_DST_INFO_NODE = "dst_info";
const std::string S2_TARGET_DELETE_NODE = "delete";
const std::string S2_TARGET_DEFAULT_NODE = "default";
const std::string S2_TARGET_USE_COMSKIP_NODE = "use_comskip";
const std::string S2_TARGET_COMSKIP_PARAMS_NODE = "comskip_params";

const std::string S2_ACTIVATIONS_NODE = "activations";
const std::string S2_ACTIVATION_NODE = "activation";
const std::string S2_ACTIVATION_ID_NODE = "id";
const std::string S2_ACTIVATION_INFO_NODE = "info";

const std::string S2_DEFAULT_FILENAME_PATTERN = "%program.season/episode%-%program.name%-%program.subname%-%program.date/time%";

typedef boost::shared_ptr<dvblex::playback::pb_item_t> pb_item_info_t;

void generate_filename_for_pbitem(const pb_item_info_t& pb_item, const std::string& filename_pattern, std::wstring& rname);

std::string escape_string_cl_argument(const std::string& src);

typedef std::map<std::string, std::string> genre_resource_to_name_map_t;

void fill_in_genre_names_for_current_locale(genre_resource_to_name_map_t& genre_names);

struct mp4_metadata_t
{
    mp4_metadata_t() :
        year_(0), episode_num_(0), season_num_(0), is_hd_(false)
    {}

    std::string title_;
    boost::uint32_t year_;
    std::string genre_;
    std::string episode_name_;
    boost::uint32_t episode_num_;
    boost::uint32_t season_num_;
    std::string episode_generated_name_;
    std::string description_;
    std::string channel_name_;
    bool is_hd_;
};

bool get_mp4_metadata_from_pb_item(const pb_item_info_t& pb_item, mp4_metadata_t& mp4_meta);

} // dvblex
