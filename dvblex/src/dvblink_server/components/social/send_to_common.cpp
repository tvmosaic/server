/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_strings.h>
#include <dl_pb_item.h>
#include <dl_pb_video.h>
#include <dl_pb_recorded_tv.h>
#include <dl_filename_pattern.h>
#include <dl_language_settings.h>
#include <dl_locale_strings.h>
#include "send_to_common.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

using namespace dvblex;
using namespace dvblex::playback;

void dvblex::generate_filename_for_pbitem(const pb_item_info_t& pb_item, const std::string& filename_pattern, std::wstring& rname)
{
	switch (pb_item->item_type_)
	{
	case pbit_item_recorded_tv:
		{
            boost::shared_ptr<pb_recorded_tv_t> recorded_tv = boost::static_pointer_cast<pb_recorded_tv_t>(pb_item);

			filename_pattern_string_to_filename(filename_pattern, recorded_tv->video_info_, 
                recorded_tv->channel_name_.get(), recorded_tv->channel_number_.get(), recorded_tv->channel_subnumber_.get(), true, rname);
		}
        break;
	case pbit_item_video:
		{
            boost::shared_ptr<pb_video_t> pb = boost::static_pointer_cast<pb_video_t>(pb_item);

            filename_pattern_string_to_filename(filename_pattern, pb->video_info_, std::string(), -1, -1, true, rname);
		}
		break;
	default:
		{
			log_warning(L"send_to_dest_copy_local::init. Unsupported item type %1%. Using generated output filename") % pb_item->item_type_;
			time_t now;
			time(&now);
			rname = boost::lexical_cast<std::wstring>(now);
		}
		break;
	}
}

void dvblex::fill_in_genre_names_for_current_locale(genre_resource_to_name_map_t& genre_names)
{
    genre_names[EPG_PROGRAM_CAT_MOVIE] = language_settings::GetInstance()->GetItemName(IDS_GENRE_MOVIE);
    genre_names[EPG_PROGRAM_CAT_ACTION] = language_settings::GetInstance()->GetItemName(IDS_GENRE_ACTION);
    genre_names[EPG_PROGRAM_CAT_ADULT] = language_settings::GetInstance()->GetItemName(IDS_GENRE_ADULT);
    genre_names[EPG_PROGRAM_CAT_COMEDY] = language_settings::GetInstance()->GetItemName(IDS_GENRE_COMEDY);
    genre_names[EPG_PROGRAM_CAT_DOCUMENTARY] = language_settings::GetInstance()->GetItemName(IDS_GENRE_DOCUMENTARY);
    genre_names[EPG_PROGRAM_CAT_DRAMA] = language_settings::GetInstance()->GetItemName(IDS_GENRE_DRAMA);
    genre_names[EPG_PROGRAM_CAT_EDUCATIONAL] = language_settings::GetInstance()->GetItemName(IDS_GENRE_EDU);
    genre_names[EPG_PROGRAM_CAT_HORROR] = language_settings::GetInstance()->GetItemName(IDS_GENRE_HORROR);
    genre_names[EPG_PROGRAM_CAT_KIDS] = language_settings::GetInstance()->GetItemName(IDS_GENRE_KIDS);
    genre_names[EPG_PROGRAM_CAT_MUSIC] = language_settings::GetInstance()->GetItemName(IDS_GENRE_MUSIC);
    genre_names[EPG_PROGRAM_CAT_NEWS] = language_settings::GetInstance()->GetItemName(IDS_GENRE_NEWS);
    genre_names[EPG_PROGRAM_CAT_REALITY] = language_settings::GetInstance()->GetItemName(IDS_GENRE_REALITY);
    genre_names[EPG_PROGRAM_CAT_ROMANCE] = language_settings::GetInstance()->GetItemName(IDS_GENRE_ROMANCE);
    genre_names[EPG_PROGRAM_CAT_SCIFI] = language_settings::GetInstance()->GetItemName(IDS_GENRE_SCIFI);
    genre_names[EPG_PROGRAM_CAT_SERIAL] = language_settings::GetInstance()->GetItemName(IDS_GENRE_SERIAL);
    genre_names[EPG_PROGRAM_CAT_SOAP] = language_settings::GetInstance()->GetItemName(IDS_GENRE_SOAP);
    genre_names[EPG_PROGRAM_CAT_SPECIAL] = language_settings::GetInstance()->GetItemName(IDS_GENRE_SPECIAL);
    genre_names[EPG_PROGRAM_CAT_SPORTS] = language_settings::GetInstance()->GetItemName(IDS_GENRE_SPORT);
    genre_names[EPG_PROGRAM_CAT_THRILLER] = language_settings::GetInstance()->GetItemName(IDS_GENRE_THRILLER);
}

std::string dvblex::escape_string_cl_argument(const std::string& src)
{
#ifdef WIN32
    std::string ret_val;

	const char quote_sym = '"';

    //walk through string and
    //- substitute \ by \\
    //- substitute " by \"
    //- substitute ' by \'
    for (size_t i=0; i<src.length(); i++)
    {
        if (src[i] == '\\' || src[i] == quote_sym)
            ret_val += '\\' + src[i];
        else 
            ret_val += src[i];
    }

    return quote_sym + ret_val + quote_sym;

#else
    return src;
#endif
}

static void add_string_for_flag(bool flag, const std::string& name, std::string& str)
{
    if (flag)
    {
        if (str.size() > 0)
            str += "/";

        str += name;
    }
}

static void fill_mp4_metadata_from_video_info(const pb_video_info_t& video_info, mp4_metadata_t& mp4_meta)
{
    mp4_meta.title_ = video_info.m_Name;

    //copy first 150 characters of the description
    //use wstring for it as it has a fixed size
    if (video_info.m_ShortDesc.size() > 0)
    {
        std::wstring wstr = string_cast<EC_UTF8>(video_info.m_ShortDesc);
        std::wstring wstr1 = wstr.substr(0, 150);
        if (wstr1.size() < wstr.size())
            wstr1 += L"...";
        mp4_meta.description_ = string_cast<EC_UTF8>(wstr1);
    }

    mp4_meta.episode_name_ = video_info.m_SecondName;
    mp4_meta.episode_generated_name_ = mp4_meta.episode_name_;
    mp4_meta.episode_num_ = video_info.m_EpisodeNum;
    mp4_meta.season_num_ = video_info.m_SeasonNum;

	if (mp4_meta.episode_num_ != 0 || mp4_meta.season_num_ != 0)
	{
		std::stringstream ss;
		if (mp4_meta.season_num_ != 0)
			ss << "S" << std::setw(2) << std::setfill('0') << mp4_meta.season_num_;

		if (mp4_meta.episode_num_ != 0)
			ss << "E" << std::setw(2) << std::setfill('0') << mp4_meta.episode_num_;

        if (ss.str().size() > 0)
        {
            if (mp4_meta.episode_generated_name_.size() > 0)
                mp4_meta.episode_generated_name_ = "(" + ss.str() + ") " + mp4_meta.episode_generated_name_;
            else
                mp4_meta.episode_generated_name_ = ss.str();
        }
	}

    mp4_meta.year_ = video_info.m_Year;
    mp4_meta.is_hd_ = video_info.m_IsHDTV;

    genre_resource_to_name_map_t genre_names;
    dvblex::fill_in_genre_names_for_current_locale(genre_names);

    add_string_for_flag(video_info.m_IsMovie, genre_names[EPG_PROGRAM_CAT_MOVIE], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsAction, genre_names[EPG_PROGRAM_CAT_ACTION], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsAdult, genre_names[EPG_PROGRAM_CAT_ADULT], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsComedy, genre_names[EPG_PROGRAM_CAT_COMEDY], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsDocumentary, genre_names[EPG_PROGRAM_CAT_DOCUMENTARY], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsDrama, genre_names[EPG_PROGRAM_CAT_DRAMA], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsEducational, genre_names[EPG_PROGRAM_CAT_EDUCATIONAL], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsHorror, genre_names[EPG_PROGRAM_CAT_HORROR], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsKids, genre_names[EPG_PROGRAM_CAT_KIDS], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsMusic, genre_names[EPG_PROGRAM_CAT_MUSIC], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsNews, genre_names[EPG_PROGRAM_CAT_NEWS], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsReality, genre_names[EPG_PROGRAM_CAT_REALITY], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsRomance, genre_names[EPG_PROGRAM_CAT_ROMANCE], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsScienceFiction, genre_names[EPG_PROGRAM_CAT_SCIFI], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsSerial, genre_names[EPG_PROGRAM_CAT_SERIAL], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsSoap, genre_names[EPG_PROGRAM_CAT_SOAP], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsSpecial, genre_names[EPG_PROGRAM_CAT_SPECIAL], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsSports, genre_names[EPG_PROGRAM_CAT_SPORTS], mp4_meta.genre_);
    add_string_for_flag(video_info.m_IsThriller, genre_names[EPG_PROGRAM_CAT_THRILLER], mp4_meta.genre_);
}

bool dvblex::get_mp4_metadata_from_pb_item(const pb_item_info_t& pb_item, mp4_metadata_t& mp4_meta)
{
    bool ret_val = true;

	switch (pb_item->item_type_)
	{
	case pbit_item_recorded_tv:
		{
            boost::shared_ptr<pb_recorded_tv_t> recorded_tv = boost::static_pointer_cast<pb_recorded_tv_t>(pb_item);

            fill_mp4_metadata_from_video_info(recorded_tv->video_info_, mp4_meta);
            mp4_meta.channel_name_ = recorded_tv->channel_name_.to_string();
		}
        break;
	case pbit_item_video:
		{
            boost::shared_ptr<pb_video_t> pb = boost::static_pointer_cast<pb_video_t>(pb_item);

            fill_mp4_metadata_from_video_info(pb->video_info_, mp4_meta);
		}
		break;
	default:
		{
            ret_val = false;
		}
		break;
	}

    return ret_val;
}
