/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_strings.h>
#include <dl_logger.h>
#include <http_utils.h>
#include <dl_pugixml_helper.h>
#include <dl_file_procedures.h>
#include <dl_pb_nfo_writer.h>
#include "send_to_common.h"
#include "send_to_dest_copy_local.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::pugixml_helpers;

static const std::string send_to_dest_dir_node = "dest_dir";
static const std::string send_to_dest_generate_nfo_node = "generate_nfo";

namespace dvblex {

send_to_dest_copy_local::send_to_dest_copy_local(const xml_string_t& params) :
	fsrc_(NULL), fdst_(NULL), buffer_(NULL), buffer_size_(512*1024), generate_nfo_(false)
{
    pugi::xml_document doc;
    if (doc.load_buffer(params.get().c_str(), params.get().length()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            std::string str;
            if (get_node_value(root_node, send_to_dest_dir_node, str))
                dest_dir_ = str;

			if (get_node_value(root_node, send_to_dest_generate_nfo_node, str))
            {
                int i = 0;
				string_conv::apply(str.c_str(), i, 0);
                generate_nfo_ = i == 1 ? true : false;
            }
		    log_info(L"send_to_dest_copy_local(). Generate nfo file (%1%)") % generate_nfo_;
        }
    }

    dest_dir_ = filesystem::make_preferred_dvblink_filepath(dest_dir_);

    if (dest_dir_.to_string().empty())
		log_error(L"send_to_dest_copy_local. Can not parse params xml.");
}

send_to_dest_copy_local::~send_to_dest_copy_local()
{
}

bool send_to_dest_copy_local::init(const work_unit_pb_item_info& pb_item, const work_unit_media_info& media_in)
{
	if (dest_dir_.get().size() == 0)
		return false;

    pb_item_ = pb_item.pb_item;
    media_info_ = media_in;

	dvblink::filesystem_path_t dst_path = dest_dir_;

	generate_filename_for_pbitem(pb_item.pb_item, pb_item.filename_pattern, output_file_name_);
	std::wstring rname = output_file_name_;

    rname += L"." + string_cast<EC_UTF8>(media_in.format);
	dst_path /= rname;

    fsrc_ = filesystem::universal_open_file(media_in.file_path, "rb");
    fdst_ = filesystem::universal_open_file(dst_path, "wb");

	bool bret = fdst_ != NULL && fsrc_ != NULL;

	if (!bret)
	{
		if (fsrc_ == NULL)
			log_error(L"send_to_dest_copy_local::init. Cannot open source file %1%") % media_in.file_path.to_wstring();
		else
			fclose(fsrc_);

		if (fdst_ == NULL)
			log_error(L"send_to_dest_copy_local::init. Cannot open dest file %1%") % dst_path.to_wstring();
		else
			fclose(fdst_);
	} else
	{
		//allocate transfer buffer
		buffer_ = new char[buffer_size_];
	}

	return bret;
}

send_to_work_unit_step_result_e send_to_dest_copy_local::do_step(work_unit_media_info& media_out)
{
	send_to_work_unit_step_result_e ret_val = e_swusr_error;

    size_t ret_read = fread((void*)buffer_, 1, buffer_size_, fsrc_);
	if (ret_read == buffer_size_)
		ret_val = e_swusr_in_progress;
	else if (feof(fsrc_) != 0)
		ret_val = e_swusr_finished;
	else if (ferror(fsrc_) != 0)
		ret_val = e_swusr_error;

	if (ret_val != e_swusr_error && ret_read > 0)
	{
		size_t ret_write = fwrite(buffer_, 1, ret_read, fdst_);
		if (ret_write != ret_read)
			ret_val = e_swusr_error;
	}

    //write thumbnail file and nfo file
    if (ret_val == e_swusr_finished)
    {
        if (generate_nfo_)
        {
            //thumbnail
            dvblink::filesystem_path_t thumb_local_file;

            dvblink::filesystem_path_t base_image_path = dest_dir_;
            base_image_path /= output_file_name_;
            dvblink::filesystem_path_t outfile_path;
            if (streaming::download_http_image(pb_item_->thumbnail_, base_image_path, outfile_path) && !outfile_path.empty())
                thumb_local_file = outfile_path.to_boost_filesystem().filename();

            //nfo file
            genre_resource_to_name_map_t genre_names;
            dvblex::fill_in_genre_names_for_current_locale(genre_names);

            std::string nfo;
            dvblex::playback::create_nfo_from_pb_item(pb_item_, genre_names, thumb_local_file.to_string(), nfo);

	        dvblink::filesystem_path_t dst_path = dest_dir_;

            std::wstring outfile = output_file_name_ + L".nfo";
            dst_path /= outfile;
            FILE* f = filesystem::universal_open_file(dst_path, "wb");
            if (f != NULL)
            {
                fwrite(nfo.c_str(), nfo.size(), 1, f);
                fclose(f);
            }
        }

        if (media_info_.edl.size() > 0)
        {
            std::wstring outfile = output_file_name_ + L".edl";
	        dvblink::filesystem_path_t dst_path = dest_dir_;
            dst_path /= outfile;
            FILE* f = filesystem::universal_open_file(dst_path, "wb");
            if (f != NULL)
            {
                fwrite(&media_info_.edl[0], media_info_.edl.size(), 1, f);
                fclose(f);
            }
        }
    }

	return ret_val;
}

void send_to_dest_copy_local::term()
{
	delete buffer_;
	fclose(fsrc_);
	fclose(fdst_);
}

void send_to_dest_copy_local::cleanup()
{
}

} // dvblex
