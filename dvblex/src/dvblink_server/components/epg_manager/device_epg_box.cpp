/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_uuid.h>
#include "device_epg_box.h"

using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink;

namespace dvblex { 

device_epg_box_t::device_epg_box_t(const dvblink::headend_id_t& headend_id) :
    id_(uuid::gen_uuid()), callbacks_(NULL), headend_id_(headend_id), pending_(true)
{
}

device_epg_box_t::~device_epg_box_t()
{
}

void device_epg_box_t::process_epg_data(const dvblink::channel_id_t& channel_id, const dvblink::engine::DLEPGEventList& epg_data)
{
    boost::unique_lock<boost::mutex> lock(lock_);
    if (callbacks_ != NULL)
        callbacks_->process_epg_data(id_, channel_id, epg_data);
}

void device_epg_box_t::scan_finished()
{
    boost::unique_lock<boost::mutex> lock(lock_);
    if (callbacks_ != NULL)
        callbacks_->scan_finished(id_);
}

void device_epg_box_t::scan_aborted()
{
    boost::unique_lock<boost::mutex> lock(lock_);
    if (callbacks_ != NULL)
        callbacks_->scan_aborted(id_);
}

void device_epg_box_t::set_callbacks(device_epg_box_callbacks_t* callbacks)
{
    boost::unique_lock<boost::mutex> lock(lock_);
    callbacks_ = callbacks;
}

}