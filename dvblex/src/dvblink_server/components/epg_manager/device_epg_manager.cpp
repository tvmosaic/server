/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_message_server.h>
#include <dl_message_channels.h>
#include <dl_message_addresses.h>
#include <dl_epg_source_commands.h>
#include <dl_xml_serialization.h>
#include <dl_pugixml_helper.h>
#include <dl_language_settings.h>
#include "device_epg_manager.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;

static const time_t first_epg_scan_delay_sec = 60; //1 minute
static const time_t epg_scan_delay_sec = 6*60*60; //6 hours
static const time_t schedule_attempt_delay_sec = 60; //1 minute
static const time_t scan_period_start_shift_sec = 2*60; //2 minutes

static const std::string device_epg_settings_root                = "device_epg_settings";
static const std::string device_epg_settings_scan_time_node      = "scan_time";

namespace dvblex {

device_epg_manager_t::device_epg_manager_t(const dvblink::i_server_t& server, dvblink::messaging::message_queue_t& message_queue, epg_manager_settings_t* settings) :
server_(server), message_queue_(message_queue), epg_manager_thread_(NULL), periodic_updates_enabled_(true), device_epg_update_time_(deut_default)
{
    settings_file_ = settings->get_device_epg_settings_file();
    load_settings();

    message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));

    device_epg_storage_ = boost::shared_ptr<device_epg_storage_t>(new device_epg_storage_t());
}

device_epg_manager_t::~device_epg_manager_t()
{
    device_epg_storage_.reset();
    message_handler_.reset();
}

void device_epg_manager_t::start()
{
    exit_flag_ = false;
    epg_manager_thread_ = new boost::thread(boost::bind(&device_epg_manager_t::epg_manager_thread_func, this));
}

void device_epg_manager_t::stop()
{
    if (epg_manager_thread_ != NULL)
    {
        exit_flag_ = true;
        epg_manager_thread_->join();
        delete epg_manager_thread_;
        epg_manager_thread_ = NULL;
    }
}

i_stream_source_control_t device_epg_manager_t::get_stream_control_if()
{
    i_stream_source_control_t stream_interface;
    if (!server_->query_object_interface(message_queue_->get_uid(), base_id_t(source_manager_message_queue_addressee.get()), stream_source_control_interface, stream_interface) == dvblink::i_success)
    {
        log_error(L"device_epg_manager_t::get_stream_control_if. Failed to get stream interface");
    }
    return stream_interface;
}

void device_epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_channel_epg_request& request, dvblink::messaging::epg::get_channel_epg_response& response)
{
    response.result_ = device_epg_storage_->get_epg(request.channel_id_.get(), response.epg_);
}

void device_epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::channel_config_changed_request& request)
{
    //ignore hidden channels
    if (request.changes_.has_config_changes() || request.changes_.shown_channels_.size() > 0)
    {
        ch_config_changed_params params;
        params.changes_ = &(request.changes_);
        command_queue_.ExecuteCommand(ch_config_changed_cmd, &params);
    }
}

void device_epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response)
{
    response.result_ = dvblink::xmlcmd_result_error;

    if (boost::iequals(request.cmd_id_.get(), epg_source_refresh_cmd))
    {
        log_info(L"device_epg_manager_t::handle xml_command. Refresh");

        command_queue_.ExecuteCommand(full_rescan_rescan_cmd, NULL);

        response.result_ = dvblink::xmlcmd_result_success;
    } else
    if (boost::iequals(request.cmd_id_.get(), epg_source_get_settings_cmd))
    {
        log_info(L"device_epg_manager_t::handle xml_command. Get settings");

        get_epg_source_settings_response_t resp;
        if (get_source_settings(resp.settings_))
        {
            std::string data;
            write_to_xml(resp, data);

            response.xml_ = data;
            response.result_ = dvblink::xmlcmd_result_success;
        }
    } else
    if (boost::iequals(request.cmd_id_.get(), epg_source_set_settings_cmd))
    {
        log_info(L"device_epg_manager_t::handle xml_command. Set settings");

        set_epg_source_settings_request_t req;
        if (read_from_xml(request.xml_.get(), req))
        {
            apply_new_src_settings_params params;
            params.settings = &req.settings_;
            command_queue_.ExecuteCommand(apply_new_src_settings, &params);

            response.result_ = dvblink::xmlcmd_result_success;
        }
    }
}

void device_epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::force_epg_update_request& request)
{
    command_queue_.ExecuteCommand(full_rescan_rescan_cmd, NULL);
}

void device_epg_manager_t::scan_finished(const dvblink::epg_box_id_t& id)
{
    boost::unique_lock<boost::mutex> lock(boxes_lock_);
    finished_boxes_.push_back(id);
}

void device_epg_manager_t::scan_aborted(const dvblink::epg_box_id_t& id)
{
    boost::unique_lock<boost::mutex> lock(boxes_lock_);
    aborted_boxes_.push_back(id);
}

void device_epg_manager_t::process_epg_data(const dvblink::epg_box_id_t& id, const dvblink::channel_id_t& channel_id, const dvblink::engine::DLEPGEventList& epg_data)
{
    device_epg_storage_->add_epg_for_channel(channel_id, epg_data);

    //notify the epg manager
    epg::epg_updated_request req;
    req.epg_source_id_ = epg_manager_message_queue_addressee.get();
    req.updated_epg_channels_.push_back(channel_id.get());

    message_queue_->post(broadcast_addressee, req);
}

void device_epg_manager_t::epg_manager_thread_func()
{
    log_info(L"device_epg_manager_t::epg_manager_thread_func is started");

    //wait until dvblink server is operational
    log_info(L"device_epg_manager_t::epg_manager_thread_func. Waiting for server to become operational");
    while (!exit_flag_)
    {
        if (check_server_connection())
        {
            log_info(L"device_epg_manager_t::epg_manager_thread_func. Server is operational");
            break;
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(200));
    }

    //fill in channels map
    rebuild_channel_map();

    time_t now;
    time(&now);
    //schedule first epg scan
    time_t first_scan = now + first_epg_scan_delay_sec;
    time_t next_scan = get_next_scan_from_scan_settings(true);
    time_t next_schedule_attempt = now + schedule_attempt_delay_sec;

    bool b_scan = false; //periodic scans
    bool b_force = false; //forced epg scans

    while (!exit_flag_)
    {
        time(&now);

        if (periodic_updates_enabled_ && (device_epg_update_time_ != deut_disabled) && (next_scan != -1 && now > next_scan))
        {
            b_scan = true;
            next_scan = -1;
        }

        //make sure that first scan happens no earlier than after start-up delay, even if force epg rescan is requested
        if ((b_scan || b_force) && now > first_scan)
        {
            b_force = false;

            //scan time!
            log_info(L"device_epg_manager_t::epg_manager_thread_func. Initiating device epg scan");

            //cancel all ongoing scans and delete epg boxes
            cancel_all_epg_boxes();

            //create new epg boxes for each headend
            std::map<headend_id_t, channel_id_list_t> headend_to_channel_map;
            channel_to_headend_map_t::iterator ch_it = channel_headend_map_.begin();
            while (ch_it != channel_headend_map_.end())
            {
                if (headend_to_channel_map.find(ch_it->second) == headend_to_channel_map.end())
                    headend_to_channel_map[ch_it->second] = channel_id_list_t();

                headend_to_channel_map[ch_it->second].push_back(ch_it->first);

                ++ch_it;
            }

            std::map<headend_id_t, channel_id_list_t>::iterator headend_it = headend_to_channel_map.begin();
            while (headend_it != headend_to_channel_map.end())
            {
                boost::shared_ptr<device_epg_box_t> epg_box = share_epg_box_safely(new device_epg_box_t(headend_it->first));

                epg_box->set_callbacks(this);
                epg_box->set_channels(headend_it->second);
                epg_boxes_[epg_box->id()] = epg_box;
                ++headend_it;
            }

            b_scan = false;
        }

        if (now > next_schedule_attempt)
        {
            bool b_restart = false;
            device_epg_box_map_t::iterator eb_it = epg_boxes_.begin();
            while (eb_it != epg_boxes_.end())
            {
                if (eb_it->second->is_pending())
                {
                    epg_box_obj_t obj = eb_it->second;
                    start_scan_status_e result = sss_error;
                    i_stream_source_control_t stream_control = get_stream_control_if();
                    if (stream_control)
                    {
                        result = stream_control->start_epg_scan(eb_it->second->headend_id().get().c_str(), obj);
                        stream_control.reset();
                    }
                    if (result == sss_success)
                    {
                        eb_it->second->set_pending(false);
                    } else
                    if (result == sss_no_free_device)
                    {
                        //just wait next turn
                    } else
                    if (result == sss_error)
                    {
                        //remove epg box
                        eb_it->second->set_callbacks(NULL);
                        epg_boxes_.erase(eb_it->second->id());
                        //restart
                        b_restart = true;
                    }
                }

                if (b_restart)
                    eb_it = epg_boxes_.begin();
                else
                    ++eb_it;

                b_restart = false;
            }

            next_schedule_attempt = now + schedule_attempt_delay_sec;
        }

        SDLCommandItem* item;
        while (command_queue_.PeekCommand(&item))
        {
            switch (item->id)
            {
            case ch_config_changed_cmd:
                {
                    ch_config_changed_params* params = (ch_config_changed_params*)item->param;
                    //cancel ongoing epg scan
                    bool b_ongoing_scan = epg_boxes_.size() > 0;
                    cancel_all_epg_boxes();
                    //drop deleted epg channels
                    if (params->changes_->deleted_channels_.size() > 0)
                        device_epg_storage_->remove_channels(params->changes_->deleted_channels_);
                    //drop hidden epg channels
                    if (params->changes_->hidden_channels_.size() > 0)
                        device_epg_storage_->remove_channels(params->changes_->hidden_channels_);
                    //rebuild local channel map
                    rebuild_channel_map();
                    //initiate a new full epg scan (only if there were added or shown channels or scan was ongoing and channels/headends were deleted/hidden)
                    if (params->changes_->added_channels_.size() > 0 || params->changes_->shown_channels_.size() > 0 ||
                        ((params->changes_->hidden_channels_.size() > 0 || params->changes_->deleted_channels_.size() > 0 || params->changes_->deleted_headends_.size() > 0) && b_ongoing_scan))
                        b_force = true;
                }
                break;
            case full_rescan_rescan_cmd:
                b_force = true;
                break;
            case apply_new_src_settings:
                {
                    apply_new_src_settings_params* params = (apply_new_src_settings_params*)item->param;
                    set_source_settings(*(params->settings));
                    //if there is no onging epg scan recalculate next scan time
                    if (next_scan != -1)
                        next_scan = get_next_scan_from_scan_settings(false);
                }
                break;
            default:
                break;
            }
            command_queue_.FinishCommand(&item);
        }

        //process finished boxes
        if (process_finished_boxes())
        {
            //epg scan is completed

            /*
            //notify the epg manager
            epg::epg_updated_request req;
            req.epg_source_id_ = epg_manager_message_queue_addressee.get();
            channel_id_list_t channels;
            device_epg_storage_->get_channel_list(channels);
            for (size_t i=0; i<channels.size(); i++)
                req.updated_epg_channels_.push_back(channels[i].get());

            message_queue_->post(broadcast_addressee, req);
            */

            log_info(L"device_epg_manager_t::epg_manager_thread_func. EPG scan is finished.");
            //schedule next epg scan
            next_scan = get_next_scan_from_scan_settings(false);
        }

        //process aborted boxes
        process_aborted_boxes();

        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
    }

    //cancel and clear all epg boxes on exit
    cancel_all_epg_boxes();

    log_info(L"device_epg_manager_t::epg_manager_thread_func is finished");
}

void device_epg_manager_t::process_aborted_boxes()
{
    epg_box_id_t aborted_eb_id;
    {
        boost::unique_lock<boost::mutex> lock(boxes_lock_);
        if (aborted_boxes_.size() > 0)
        {
            aborted_eb_id = aborted_boxes_[0];
            aborted_boxes_.erase(aborted_boxes_.begin());
        }
    }
    if (!aborted_eb_id.empty())
    {
        log_ext_info(L"device_epg_manager_t::epg_manager_thread_func. EPG scan for epg box %1% was aborted") % aborted_eb_id.to_wstring();

        device_epg_box_map_t::iterator it = epg_boxes_.find(aborted_eb_id);
        if (it != epg_boxes_.end())
        {
            //check if this is active box and reset it back to pending
            //it will be picked up again by the next scan attempt
            if (!it->second->is_pending())
                it->second->set_pending(true);
        } else
        {
            //the box was deleted already
        }
    }
}

bool device_epg_manager_t::process_finished_boxes()
{
    bool ret_val = false;

    epg_box_id_t finished_eb_id;
    {
        boost::unique_lock<boost::mutex> lock(boxes_lock_);
        if (finished_boxes_.size() > 0)
        {
            finished_eb_id = finished_boxes_[0];
            finished_boxes_.erase(finished_boxes_.begin());
        }
    }
    if (!finished_eb_id.empty())
    {
        //find and delete this epg box from the map
        device_epg_box_map_t::iterator it = epg_boxes_.find(finished_eb_id);
        if (it != epg_boxes_.end())
        {
            log_info(L"device_epg_manager_t::epg_manager_thread_func. EPG scan for headend %1% is finished") % string_cast<EC_UTF8>(it->second->headend_id().get());

            it->second->set_callbacks(NULL);
            epg_boxes_.erase(it->second->id());

            //check if it was the last epg box
            ret_val = (epg_boxes_.size() == 0);
        }
    }

    return ret_val;
}

void device_epg_manager_t::cancel_all_epg_boxes()
{
    device_epg_box_map_t::iterator eb_it = epg_boxes_.begin();
    while (eb_it != epg_boxes_.end())
    {
        eb_it->second->set_callbacks(NULL);
        eb_it->second->cancel();
        ++eb_it;
    }
    epg_boxes_.clear();
}

bool device_epg_manager_t::check_server_connection()
{
    server::server_state_request req;
    server::server_state_response resp;
    message_queue_->send(server_message_queue_addressee, req, resp);
    return resp.state_ == dss_active;
}

void device_epg_manager_t::rebuild_channel_map()
{
    channel_headend_map_.clear();

    channels::get_provider_channels_id_request req;
    channels::get_provider_channels_id_response resp;
    if (message_queue_->send(source_manager_message_queue_addressee, req, resp) == success &&
        resp.result_)
    {
        channel_desc_list_t cdl;

        for (size_t i=0; i<resp.headends_.size(); i++)
        {
            for (size_t j=0; j<resp.headends_[i].transponders_.size(); j++)
            {
                for (size_t k=0; k<resp.headends_[i].transponders_[j].channels_.size(); k++)
                    channel_headend_map_[resp.headends_[i].transponders_[j].channels_[k]] = resp.headends_[i].id_;
            }
        }
    } else
    {
        log_error(L"device_epg_manager_t::rebuild_channel_map. get_headend_channels_id_request returned an error");
    }
}

void device_epg_manager_t::save_settings()
{
    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(device_epg_settings_root.c_str());
    if (root_node != NULL)
    {
        std::stringstream buf;
        buf << device_epg_update_time_;
        pugixml_helpers::new_child(root_node, device_epg_settings_scan_time_node, buf.str());

        pugixml_helpers::xmldoc_dump_to_file(doc, settings_file_.to_string());
    }
}

void device_epg_manager_t::load_settings()
{
    device_epg_update_time_ = deut_default;

    std::string str;

    pugi::xml_document doc;
    if (doc.load_file(settings_file_.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            if (pugixml_helpers::get_node_value(root_node, device_epg_settings_scan_time_node, str))
            {
                int l;
                string_conv::apply(str.c_str(), l, (int)deut_default);
                device_epg_update_time_ = (device_epg_scan_time_e)l;
            }
        }
    }
}

const std::string device_epg_settings_container_id = "2945aba9-3a94-48da-a84d-3c3f0f4d77ff";
const std::string device_epg_settings_scan_time_key = "ac3f0f4d77f0";

static std::string device_epg_scan_time_enum_to_string(device_epg_scan_time_e value)
{
    return boost::lexical_cast<std::string>((int)value);
}

bool device_epg_manager_t::get_source_settings(dvblex::parameters_container_t& settings)
{
    settings.id_ = device_epg_settings_container_id;
    settings.name_ = language_settings::GetInstance()->GetItemName("IDS_DEVICE_EPG_SETTINGS_CNT_NAME");
    settings.desc_ = language_settings::GetInstance()->GetItemName("IDS_DEVICE_EPG_SETTINGS_CNT_DESC");

    selectable_param_description_t scan_time_element;
    scan_time_element.key_ = device_epg_settings_scan_time_key;
    scan_time_element.name_ = language_settings::GetInstance()->GetItemName("IDS_DEVICE_EPG_SETTINGS_SCAN_TIME_NAME");

    scan_time_element.parameters_list_.push_back(selectable_param_element_t(device_epg_scan_time_enum_to_string(deut_default), language_settings::GetInstance()->GetItemName("IDS_DEVICE_EPG_SETTINGS_SCAN_TIME_DEFAULT")));

    scan_time_element.parameters_list_.push_back(selectable_param_element_t(device_epg_scan_time_enum_to_string(deut_disabled), language_settings::GetInstance()->GetItemName("IDS_DEVICE_EPG_SETTINGS_SCAN_TIME_DISABLED")));
    if (device_epg_update_time_ == deut_disabled)
        scan_time_element.selected_param_ = scan_time_element.parameters_list_.size() - 1;

    for (size_t i=deut_00_03; i<=deut_21_24; i++)
    {
        std::stringstream buf;
        buf << "IDS_DEVICE_EPG_SETTINGS_SCAN_TIME_VAL_" << i;
        scan_time_element.parameters_list_.push_back(selectable_param_element_t(device_epg_scan_time_enum_to_string((device_epg_scan_time_e)i), language_settings::GetInstance()->GetItemName(buf.str())));

        if (device_epg_update_time_ == (device_epg_scan_time_e)i)
            scan_time_element.selected_param_ = scan_time_element.parameters_list_.size() - 1;
    }

    settings.selectable_params_.push_back(scan_time_element);

    return true;
}

bool device_epg_manager_t::set_source_settings(const dvblex::concise_param_map_t& settings)
{
    if (is_key_present(device_epg_settings_scan_time_key, settings))
    {
        dvblink::parameter_value_t value = get_value_for_param(device_epg_settings_scan_time_key, settings);
        int l;
        string_conv::apply(value.to_string().c_str(), l, (int)deut_default);
        device_epg_update_time_ = (device_epg_scan_time_e)l;
        save_settings();
    }
    return true;
}

time_t device_epg_manager_t::get_next_scan_from_scan_settings(bool first_scan)
{
    static const time_t scan_settings_to_period[] = {0, 0, 0, 1, 2, 3, 4, 5, 6, 7};
    static const time_t scan_settings_period_num = 8;
    time_t period_duration_sec = 3*3600;

    time_t now;
    time(&now);

    time_t ret_val = now;

    switch (device_epg_update_time_)
    {
    case deut_default:
        ret_val = first_scan ? now : now + epg_scan_delay_sec;
        break;
    case deut_00_03:
    case deut_03_06:
    case deut_06_09:
    case deut_09_12:
    case deut_12_15:
    case deut_15_18:
    case deut_18_21:
    case deut_21_24:
        {
            struct tm* local_now = localtime(&now);
            time_t local_now_sec = local_now->tm_sec + local_now->tm_min*60 + local_now->tm_hour*3600;

            time_t period_idx = local_now_sec / period_duration_sec;
            time_t period_offs = local_now_sec % period_duration_sec;
            time_t scan_settings_period = scan_settings_to_period[device_epg_update_time_];

            //if this is the first scan and we are on scan period - do scan now
            if (first_scan && period_idx == scan_settings_period)
                ret_val = now;
            else
                ret_val = (now - period_offs) + (scan_settings_period - period_idx + (period_idx >= scan_settings_period ? scan_settings_period_num : 0)) * period_duration_sec + scan_period_start_shift_sec;
        }
        break;
    case deut_disabled:
    default:
        break;
    }

    if (device_epg_update_time_ != deut_disabled)
        log_info(L"device_epg_manager_t::get_next_scan_from_scan_settings. EPG scan time settings: %1%, %2%. Next scan will be at %3% (in %4% seconds)") % device_epg_update_time_ % first_scan % ret_val % (ret_val - now);
    else
        log_info(L"device_epg_manager_t::get_next_scan_from_scan_settings. EPG scan is disabled");

    return ret_val;
}

} // dvblink
