/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>

#include <boost/thread.hpp>

#include <dl_channels.h>
#include <dl_epgevent.h>
#include <dl_gzip_string_container.h>

namespace dvblex { 

class device_epg_storage_t
{
    typedef std::map<dvblink::channel_id_t, dvblink::engine::gzip_string_container> memory_epg_storage_map_t;
public:
    device_epg_storage_t();
    ~device_epg_storage_t();

    void add_epg_for_channel(const dvblink::channel_id_t& channel_id, const dvblink::engine::DLEPGEventList& events);
    void remove_channels(const channel_id_list_t& channels);
    bool get_epg(const dvblink::channel_id_t& channel_id, dvblink::engine::DLEPGEventList& events);
    void get_channel_list(channel_id_list_t& channels);

protected:
    boost::shared_mutex lock_;
    memory_epg_storage_map_t epg_channels_;

    void drop_epg_channel(const dvblink::channel_id_t& channel_id);
};

}
