/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <dl_levenshtein.h>
#include "mapper.h"

namespace dvblex { 

epg_mapper_t::epg_mapper_t()
{
}

bool epg_mapper_t::map_device_channel_to_epg_channel(const channel_description_t& channel, const dvblink::epg_source_id_t& epg_source_id, 
                                                     const epg_source_channel_map_t& epg_channels, epg_channel_match_t& match_info)
{
    //in the future look up first by nid-tid-sid combination

    //do channel name match
    map_by_channel_name(channel, epg_source_id, epg_channels, match_info);

    return match_info.exact_match_.size() > 0 || match_info.partial_match_.size() > 0;
}

bool epg_mapper_t::map_by_channel_name(const channel_description_t& channel, const dvblink::epg_source_id_t& epg_source_id, 
                                              const epg_source_channel_map_t& epg_channels, epg_channel_match_t& match_info)
{
    match_info.exact_match_.clear();
    match_info.partial_match_.clear();

    levenshtein_distance_calc distance_calc;

    epg_source_channel_map_t::const_iterator it = epg_channels.begin();
    while (it != epg_channels.end())
    {
        levenshtein_distance_result_e res = distance_calc.match(channel.name_.get(), it->second.name_.get());

        switch (res)
        {
        case ldr_exact:
            match_info.exact_match_.push_back(epg_source_channel_id_t(epg_source_id, it->first));
            break;
        case ldr_partial:
            match_info.partial_match_.push_back(epg_source_channel_id_t(epg_source_id, it->first));
            break;
        case ldr_none:
        default:
            break;
        }

        ++it;
    }

    return true;
}

}