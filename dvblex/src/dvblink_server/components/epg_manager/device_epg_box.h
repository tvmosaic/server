/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <boost/thread.hpp>
#include <dl_types.h>
#include <dl_epg_box.h>

namespace dvblex { 

class device_epg_box_t : public dvblink::epg_box_t
{
public:
    class device_epg_box_callbacks_t
    {
        public:
            virtual void scan_finished(const dvblink::epg_box_id_t& id) = 0;
            virtual void scan_aborted(const dvblink::epg_box_id_t& id) = 0;
            virtual void process_epg_data(const dvblink::epg_box_id_t& id, const dvblink::channel_id_t& channel_id, const dvblink::engine::DLEPGEventList& epg_data) = 0;
    };

public:
    device_epg_box_t(const dvblink::headend_id_t& headend_id);
    ~device_epg_box_t();

    virtual void process_epg_data(const dvblink::channel_id_t& channel_id, const dvblink::engine::DLEPGEventList& epg_data);
    virtual void scan_finished();
    virtual void scan_aborted();

    dvblink::epg_box_id_t id(){return id_;}
    dvblink::headend_id_t headend_id(){return headend_id_;}
    void set_callbacks(device_epg_box_callbacks_t* callbacks);
    bool is_pending(){return pending_;}
    void set_pending(bool pending){pending_ = pending;}

protected:
    dvblink::epg_box_id_t id_;
    device_epg_box_callbacks_t* callbacks_;
    boost::mutex lock_;
    dvblink::headend_id_t headend_id_;
    bool pending_;
};

typedef boost::shared_ptr<device_epg_box_t> device_epg_box_obj_t;

}
