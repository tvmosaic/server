/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_message_addresses.h>
#include <dl_message_server.h>
#include <dl_message_recorder.h>
#include <dl_message_channels.h>
#include <dl_pugixml_helper.h>
#include <dl_language_settings.h>
#include <dl_locale_strings.h>
#include <components/epg_manager.h>
#include "settings.h"
#include "mapper.h"
#include "device_epg_manager.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::messaging;
using namespace dvblink::engine;
using namespace dvblink::pugixml_helpers;

namespace dvblex {

epg_manager_t::epg_manager_t() :
    id_(epg_manager_message_queue_addressee.get()),
    default_epg_source_id_(epg_manager_message_queue_addressee.get()),
#ifndef __ANDROID__
    periodic_updates_enabled_(true)
#else
    periodic_updates_enabled_(false)
#endif
{
}

epg_manager_t::~epg_manager_t()
{
    server_->unregister_queue(message_queue_->get_id());

    device_epg_manager_.reset();

    message_queue_->shutdown();
}

i_result epg_manager_t::query_interface(const base_id_t& requestor_id, const i_guid& iid, i_base_object_t& obj)
{
    i_result res = i_error;

    //no other interfaces

    return res;
}

bool epg_manager_t::init(const dvblink::i_server_t& server)
{
    server_ = server;

    message_queue_ = share_object_safely(new message_queue(id_.get()));
    message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));

    server_->register_queue(message_queue_);

    settings_ = boost::shared_ptr<epg_manager_settings_t>(new epg_manager_settings_t());
    settings_->init(message_queue_);

    device_epg_manager_ = boost::shared_ptr<device_epg_manager_t>(new device_epg_manager_t(server_, message_queue_, settings_.get()));

    return true;
}

void epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::start_request& request, dvblink::messaging::start_response& response)
{
    load_config();

    device_epg_manager_->set_periodic_epg_enabled(periodic_updates_enabled_);
    device_epg_manager_->start();
    response.result_ = true;
}

void epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::shutdown_request& request, dvblink::messaging::shutdown_response& response)
{
    device_epg_manager_->stop();
    response.result_ = true;
}

void epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::enable_periodic_epg_updates_request& request)
{
    periodic_updates_enabled_ = request.enable_;
    device_epg_manager_->set_periodic_epg_enabled(periodic_updates_enabled_);
    log_info(L"epg_manager_t::handle enable_periodic_epg_updates_request:%1%") % periodic_updates_enabled_;
}

void epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_channel_epg_config_request& request, dvblink::messaging::epg::get_channel_epg_config_response& response)
{
    if (request.channels_.size() > 0)
    {
        //return mappings for requested channels
        for (size_t i=0; i<request.channels_.size(); i++)
        {
            epg_source_channel_id_t ces;
            if (epg_channels_.find(request.channels_[i]) != epg_channels_.end())
            {
                ces = epg_channels_[ request.channels_[i] ];
            } else
            {
                //assign default epg source
                ces.epg_source_id_ = default_epg_source_id_;
                ces.epg_channel_id_ = request.channels_[i].get();
            }
            response.epg_config_[request.channels_[i]] = ces;
        }
    } else
    {
        //return mappings for all non-default channels
        channel_to_epg_source_map_t::iterator it = epg_channels_.begin();
        while (it != epg_channels_.end())
        {
            response.epg_config_[it->first] = it->second;
            ++it;
        }
    }
    response.result_ = true;
}

void epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::set_channel_epg_config_request& request, dvblink::messaging::epg::set_channel_epg_config_response& response)
{
    //build update epg channel config request
    dvblink::messaging::channels::channel_config_changed_request req;
    channel_to_epg_source_map_t prev_epg_channels = epg_channels_;

    epg_channels_.clear();

    channel_to_epg_source_map_t::const_iterator it = request.epg_config_.begin();
    while (it != request.epg_config_.end())
    {
        //skip all default epg sources from map
        if (it->second.epg_source_id_.get() != default_epg_source_id_.get())
        {
            epg_channels_[it->first] = it->second;

            //check if epg config has changed for this channel
            if (prev_epg_channels.find(it->first) == prev_epg_channels.end() ||
                (prev_epg_channels.find(it->first) != prev_epg_channels.end() && 
                (get_epg_source_channel_hash(prev_epg_channels[it->first]) != get_epg_source_channel_hash(it->second) ) ) )
            {
                req.changes_.epg_config_channels_.push_back(it->first);
            }
            prev_epg_channels.erase(it->first);
        }

        ++it;
    }

    rebuild_inverse_lookup_map();

    save_config();

    //for channels, remaining in prev_epg_channels teh epg config was reset to default
    it = prev_epg_channels.begin();
    while (it != prev_epg_channels.end())
    {
        req.changes_.epg_config_channels_.push_back(it->first);
        ++it;
    }

    //send notification
    if (req.changes_.has_changes())
        message_queue_->post(dvblink::messaging::broadcast_addressee, req);

    response.result_ = true;
}

void epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::epg_updated_request& request)
{
    recorder::update_recorder_epg_request req;

    for (size_t i=0; i<request.updated_epg_channels_.size(); i++)
    {
        channel_id_lookup_map_t channel_ids;
        if (get_channel_id_from_epg_source(request.epg_source_id_, request.updated_epg_channels_[i], channel_ids))
        {
            channel_id_lookup_map_t::iterator it = channel_ids.begin();
            while (it != channel_ids.end())
            {
                req.channel_list_.push_back(it->first);
                ++it;
            }
        }
    }

    if (req.channel_list_.size() > 0)
        message_queue_->post(recorder_message_queue_addressee, req);
}

void epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::register_epg_source_request& request, dvblink::messaging::epg::register_epg_source_response& response)
{
    epg_sources_[request.epg_source_.id_] = request.epg_source_;
    response.periodic_updates_enabled_ = periodic_updates_enabled_;
    response.result_ = true;
}

void epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::unregister_epg_source_request& request, dvblink::messaging::epg::unregister_epg_source_response& response)
{
    epg_sources_.erase(request.id_);
    response.result_ = true;
}

void epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_epg_sources_request& request, dvblink::messaging::epg::get_epg_sources_response& response)
{
    response.epg_sources_ = epg_sources_;
    //add itself as a default EPG source
    epg_source_t default_epg_source;
    default_epg_source.id_ = default_epg_source_id_;
    default_epg_source.name_= language_settings::GetInstance()->GetItemName(item_id_t(default_epg_source_name_name));
    default_epg_source.is_default_ = true;
    default_epg_source.has_settings_ = true;
    response.epg_sources_[default_epg_source_id_] = default_epg_source;

    response.result_ = true;
}

void epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::match_epg_channels_request& request, dvblink::messaging::epg::match_epg_channels_response& response)
{
    response.result_ = false;

    epg_source_id_list_t required_epg_sources;
    if (request.epg_source_id_.get() != default_epg_source_id_.get())
    {
        //if it is not default, use it for mapping
        required_epg_sources.push_back(request.epg_source_id_);
    } else
    {
        //otherwise, match against all available epg sources
        epg_source_map_t::iterator epg_src_it = epg_sources_.begin();
        while (epg_src_it != epg_sources_.end())
        {
            required_epg_sources.push_back(epg_src_it->first);
            ++epg_src_it;
        }
    }

    for (size_t idx=0; idx<required_epg_sources.size(); idx++)
    {
        const epg_source_id_t& epg_source_id = required_epg_sources[idx];

        //get list of epg channels from the source
        epg::get_epg_channels_request req;
        epg::get_epg_channels_response resp;
        if (message_queue_->send(epg_source_id.get(), req, resp) == success && resp.result_)
        {
            epg_mapper_t epg_mapper;

            //walk through the list of requested channels and match them one by one
            for (size_t i=0; i<request.channels_.size(); i++)
            {
                epg_channel_match_t match_info;

                //get device channels
                channels::get_channels_description_request ch_req;
                ch_req.channels_.push_back(request.channels_[i]);
                channels::get_channels_description_response ch_resp;
                if (message_queue_->send(source_manager_message_queue_addressee, ch_req, ch_resp) == success && ch_resp.result_ && ch_resp.channels_desc_.size() > 0)
                {
                    const channel_description_t& channel = ch_resp.channels_desc_[0];
                    epg_mapper.map_device_channel_to_epg_channel(channel, epg_source_id, resp.channels_, match_info);
                }

                response.match_info_[ request.channels_[i] ] = match_info;
            }

            response.result_ = true;

        } else
        {
            log_error(L"epg_manager_t::handle match_epg_channels_request. Failed to get epg channels from %1%") % epg_source_id.to_wstring();
        }
    }
}

void epg_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::channel_config_changed_request& request)
{
    device_epg_manager_->handle(sender, request);

    //remove all deleted/hidden channels from channel mapping
    bool bchanges = false;

    channel_id_list_t channels;
    channels.insert(channels.end(), request.changes_.deleted_channels_.begin(), request.changes_.deleted_channels_.end());
    channels.insert(channels.end(), request.changes_.hidden_channels_.begin(), request.changes_.hidden_channels_.end());
    for (size_t i=0; i<channels.size(); i++)
    {
        if (epg_channels_.find(channels[i]) != epg_channels_.end())
        {
            bchanges = true;

            std::string hash = get_epg_source_channel_hash(epg_channels_[ channels[i] ]);
            epg_channels_.erase(channels[i]);
            //delete channel from the inverse lookup table
            channel_id_lookup_map_t& lm = epg_source_to_channel_map_[hash];
            lm.erase(channels[i]);
            if (lm.size() == 0)
                epg_source_to_channel_map_.erase(hash);
        }
    }
    if (bchanges)
        save_config();
}

bool epg_manager_t::get_channel_id_from_epg_source(const epg_source_id_t& epg_source_id, const epg_channel_id_t& epg_channel_id, channel_id_lookup_map_t& channel_ids)
{
    bool ret_val = false;
    if (epg_source_id.get() == default_epg_source_id_.get())
    {
        //for default epg source channel id and epg channel id are the same
        channel_ids.clear();
        channel_ids[epg_channel_id.get()] = epg_channel_id.get();
        ret_val = true;
    } else
    {
        //lookup mapping for this epg channel
        epg_source_channel_id_t id;
        id.epg_source_id_ = epg_source_id;
        id.epg_channel_id_ = epg_channel_id;
        std::string hash = get_epg_source_channel_hash(id);
        if (epg_source_to_channel_map_.find(hash) != epg_source_to_channel_map_.end())
        {
            channel_ids = epg_source_to_channel_map_[hash];
            ret_val = true;
        }
    }

    return ret_val;
}

static const std::string epg_manager_root_node = "epg_mappings";
static const std::string epg_manager_channel_node = "channel";
static const std::string epg_manager_channel_id_attr = "id";
static const std::string epg_manager_epg_source_id_attr = "epg_source_id";
static const std::string epg_manager_epg_channel_id_attr = "epg_channel_id";

void epg_manager_t::save_config()
{
    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(epg_manager_root_node.c_str());
    if (root_node != NULL)
    {
        channel_to_epg_source_map_t::iterator it = epg_channels_.begin();
        while (it != epg_channels_.end())
        {
            pugi::xml_node ch_node = new_child(root_node, epg_manager_channel_node);
            if (ch_node != NULL)
            {
                add_node_attribute(ch_node, epg_manager_channel_id_attr, it->first.get());
                new_child(ch_node, epg_manager_epg_source_id_attr, it->second.epg_source_id_.to_string());
                new_child(ch_node, epg_manager_epg_channel_id_attr, it->second.epg_channel_id_.to_string());
            }

            ++it;
        }

        pugixml_helpers::xmldoc_dump_to_file(doc, settings_->get_epg_mappings_file().to_string());
    }
}

void epg_manager_t::load_config()
{
    std::string str;

    epg_channels_.clear();

    pugi::xml_document doc;
    if (doc.load_file(settings_->get_epg_mappings_file().to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            pugi::xml_node channel_node = root_node.first_child();
            while (channel_node != NULL)
            {
                std::string channel_id;
                if (get_node_attribute(channel_node, epg_manager_channel_id_attr, channel_id))
                {
                    epg_source_channel_id_t epg_source;
                    if (get_node_value(channel_node, epg_manager_epg_source_id_attr, str))
                        epg_source.epg_source_id_ = str;
                    if (get_node_value(channel_node, epg_manager_epg_channel_id_attr, str))
                        epg_source.epg_channel_id_ = str;

                    epg_channels_[channel_id] = epg_source;
                }

                channel_node = channel_node.next_sibling();
            }
        }
    }

    rebuild_inverse_lookup_map();
}

std::string epg_manager_t::get_epg_source_channel_hash(const epg_source_channel_id_t& es)
{
    return es.epg_source_id_.to_string() + "/" + es.epg_channel_id_.get();
}

void epg_manager_t::rebuild_inverse_lookup_map()
{
    epg_source_to_channel_map_.clear();

    channel_to_epg_source_map_t::iterator it = epg_channels_.begin();
    while (it != epg_channels_.end())
    {
        std::string hash = get_epg_source_channel_hash(it->second);
        if (epg_source_to_channel_map_.find(hash) == epg_source_to_channel_map_.end())
            epg_source_to_channel_map_[hash] = channel_id_lookup_map_t();

        channel_id_lookup_map_t& lm = epg_source_to_channel_map_[hash];
        lm[it->first] = it->first.get();

        ++it;
    }
}

} // dvblink
