/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_channels.h>
#include <dl_epg_channels.h>

namespace dvblex { 

class epg_mapper_t
{
public:
    epg_mapper_t();

    bool map_device_channel_to_epg_channel(const channel_description_t& channel, const dvblink::epg_source_id_t& epg_source_id, 
        const epg_source_channel_map_t& epg_channels, epg_channel_match_t& match_info);

protected:

    bool map_by_channel_name(const channel_description_t& channel, const dvblink::epg_source_id_t& epg_source_id, 
        const epg_source_channel_map_t& epg_channels, epg_channel_match_t& match_info);
};

}
