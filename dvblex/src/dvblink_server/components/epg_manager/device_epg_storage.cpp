/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <dl_strings.h>
#include <dl_logger.h>
#include "device_epg_storage.h"

using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink;

namespace dvblex { 

device_epg_storage_t::device_epg_storage_t()
{
}

device_epg_storage_t::~device_epg_storage_t()
{
}

void device_epg_storage_t::add_epg_for_channel(const channel_id_t& channel_id, const DLEPGEventList& events)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);

    drop_epg_channel(channel_id);

    std::string events_str;
    EPGWriteEventsToXML(events, events_str);
    epg_channels_[channel_id] = gzip_string_container(events_str);
}

void device_epg_storage_t::remove_channels(const channel_id_list_t& channels)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);

    for (size_t i=0; i<channels.size(); i++)
        drop_epg_channel(channels[i]);
}

void device_epg_storage_t::drop_epg_channel(const channel_id_t& channel_id)
{
    epg_channels_.erase(channel_id);
}

bool device_epg_storage_t::get_epg(const dvblink::channel_id_t& channel_id, dvblink::engine::DLEPGEventList& events)
{
    bool ret_val = false;

    boost::shared_lock<boost::shared_mutex> lock(lock_);

    memory_epg_storage_map_t::iterator it = epg_channels_.find(channel_id);
    if (it != epg_channels_.end())
    {
        std::string events_str;
        if (it->second.get_string(events_str))
        {
            EPGReadEventsFromXML(events_str, events);

            ret_val = true;
        }
    }

    return ret_val;
}

void device_epg_storage_t::get_channel_list(channel_id_list_t& channels)
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);

    memory_epg_storage_map_t::iterator it = epg_channels_.begin();
    while (it != epg_channels_.end())
    {
        channels.push_back(it->first);
        ++it;
    }
}


}