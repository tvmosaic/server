/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <dl_installation_settings.h>

namespace dvblex { 

class epg_manager_settings_t : public settings::installation_settings_t
{
public:
    epg_manager_settings_t() :
      settings::installation_settings_t()
      {}

    virtual void init(dvblink::messaging::message_queue_t& message_queue);
    const dvblink::filesystem_path_t get_epg_mappings_file() { return epg_mappings_file_;}
    const dvblink::filesystem_path_t get_device_epg_settings_file() { return device_epg_settings_file_;}

protected:
    dvblink::filesystem_path_t epg_mappings_file_;
    dvblink::filesystem_path_t device_epg_settings_file_;
};

}
