/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread.hpp>
#include <dl_types.h>
#include <dli_server.h>
#include <dli_stream_source.h>
#include <dl_message_queue.h>
#include <dl_message_epg.h>
#include <dl_message_common.h>
#include <dl_message_channels.h>
#include <dl_command_queue.h>
#include "settings.h"
#include "device_epg_box.h"
#include "device_epg_storage.h"

namespace dvblex {

enum device_epg_scan_time_e
{
    deut_default,
    deut_disabled,
    deut_00_03,
    deut_03_06,
    deut_06_09,
    deut_09_12,
    deut_12_15,
    deut_15_18,
    deut_18_21,
    deut_21_24
};

class device_epg_manager_t : public device_epg_box_t::device_epg_box_callbacks_t
{
    class message_handler : 
        public dvblink::messaging::epg::force_epg_update_request::subscriber,
        public dvblink::messaging::epg::get_channel_epg_request::subscriber,
        public dvblink::messaging::xml_message_request::subscriber
    {
    public:
        message_handler(device_epg_manager_t* device_epg_manager, dvblink::messaging::message_queue_t message_queue) :
            dvblink::messaging::epg::get_channel_epg_request::subscriber(message_queue),
            dvblink::messaging::epg::force_epg_update_request::subscriber(message_queue),
            dvblink::messaging::xml_message_request::subscriber(message_queue),
            device_epg_manager_(device_epg_manager),
            message_queue_(message_queue)
        {
        }

        ~message_handler()
        {
        }

    protected:
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_channel_epg_request& request, dvblink::messaging::epg::get_channel_epg_response& response)
        {
            device_epg_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::force_epg_update_request& request)
        {
            device_epg_manager_->handle(sender, request);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response)
        {
            device_epg_manager_->handle(sender, request, response);
        }

    private:
        device_epg_manager_t* device_epg_manager_;
        dvblink::messaging::message_queue_t message_queue_;
    };

protected:

    typedef std::map<dvblink::channel_id_t, dvblink::headend_id_t> channel_to_headend_map_t;
    
    typedef std::map<dvblink::epg_box_id_t, device_epg_box_obj_t> device_epg_box_map_t;

    enum queue_commands_e {ch_config_changed_cmd = 1, full_rescan_rescan_cmd = 2, apply_new_src_settings = 3};

    struct ch_config_changed_params
    {
        const dvblex::channel_changes_desc_t* changes_;
    };

    struct full_rescan_rescan_params
    {
        bool result_;
    };

    struct apply_new_src_settings_params
    {
        const dvblex::concise_param_map_t* settings;
    };

public:
    device_epg_manager_t(const dvblink::i_server_t& server, dvblink::messaging::message_queue_t& message_queue, epg_manager_settings_t* settings);
    ~device_epg_manager_t();

    void start();
    void stop();

    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::channels::channel_config_changed_request& request);
    void set_periodic_epg_enabled(bool enabled){periodic_updates_enabled_ = enabled;}

private:
    dvblink::i_server_t server_;
    dvblink::messaging::message_queue_t message_queue_;
    std::auto_ptr<message_handler> message_handler_;
    boost::shared_ptr<device_epg_storage_t> device_epg_storage_;
    boost::thread* epg_manager_thread_;
    bool exit_flag_;
    dvblink::engine::command_queue command_queue_;
    channel_to_headend_map_t channel_headend_map_;
    device_epg_box_map_t epg_boxes_;
    std::vector<dvblink::epg_box_id_t> finished_boxes_;
    std::vector<dvblink::epg_box_id_t> aborted_boxes_;
    boost::mutex boxes_lock_;
    bool periodic_updates_enabled_;
    dvblink::filesystem_path_t settings_file_;
    device_epg_scan_time_e device_epg_update_time_;

    void epg_manager_thread_func();
    bool check_server_connection();
    void rebuild_channel_map();
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::get_channel_epg_request& request, dvblink::messaging::epg::get_channel_epg_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::epg::force_epg_update_request& request);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response);
    dvblink::messaging::i_stream_source_control_t get_stream_control_if();

    virtual void scan_finished(const dvblink::epg_box_id_t& id);
    virtual void scan_aborted(const dvblink::epg_box_id_t& id);
    virtual void process_epg_data(const dvblink::epg_box_id_t& id, const dvblink::channel_id_t& channel_id, const dvblink::engine::DLEPGEventList& epg_data);
    void cancel_all_epg_boxes();
    void process_aborted_boxes();
    bool process_finished_boxes();
    bool get_source_settings(dvblex::parameters_container_t& settings);
    bool set_source_settings(const dvblex::concise_param_map_t& settings);
    void save_settings();
    void load_settings();
    time_t get_next_scan_from_scan_settings(bool first_scan);
};

} // dvblex
