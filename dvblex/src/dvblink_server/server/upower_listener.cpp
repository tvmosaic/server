/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#if defined(_UBUNTU_X86_64)

// Build options:
// -I/usr/include/dbus-1.0 -I/usr/lib/i386-linux-gnu/dbus-1.0/include 
// -ldbus-1

//  apt-get install libdbus-1-dev

#include "stdafx.h"
#include <cstring>
#include <stdexcept>
#include "upower_listener.h"

namespace dvblink {

upower_listener::upower_listener(callbacks_ptr cb_ptr) :
    shutdown_(false), cb_ptr_(cb_ptr), connection_(0)
{
    DBusError error;
    dbus_error_init(&error);

    // connect to D-Bus daemon
    connection_ = dbus_bus_get(DBUS_BUS_SYSTEM, &error);

    if (!connection_)
    {
        dbus_error_free(&error);
        throw std::runtime_error("upower_listener::upower_listener()");
    }

    const char* filter = "type='signal',interface='org.freedesktop.UPower'";
    dbus_bus_add_match(connection_, filter, &error);

    dbus_connection_add_filter(connection_,
        &upower_listener::dbus_filter_func, this, 0);

    dbus_error_init(&error);

    thread_ = thread_ptr(new boost::thread(boost::bind(
        &upower_listener::thread_func, this)));
}

upower_listener::~upower_listener()
{
    shutdown_ = true;

    if (thread_)
    {
        thread_->join();
    }

    if (connection_)
    {
        dbus_connection_unref(connection_);
    }
}

DBusHandlerResult upower_listener::dbus_filter_func(
    DBusConnection* connection,
    DBusMessage* message,
    void* user_data)
{
    if (user_data && message)
    {
        upower_listener* this_ = static_cast<upower_listener*>(user_data);
        this_->handle_message(message);
    }

    return DBUS_HANDLER_RESULT_HANDLED;
}

bool upower_listener::handle_message(DBusMessage* message)
{
    if (!message)
    {
        return false;
    }

    const char* iface = dbus_message_get_interface(message);

    if (!iface || strcmp(iface, "org.freedesktop.UPower"))
    {
        return false;
    }

    const char* member = dbus_message_get_member(message);

    if (!member)
    {
        return false;
    }

    if (!strcmp(member, "Sleeping"))
    {
        // This signal is sent when the session is about to be suspended or
        // hibernated. Session and system programs have one second to do
        // anything required before the sleep action is taken. 
        //
        if (cb_ptr_)
        {
            cb_ptr_->on_standby();
        }
    }
    else if (!strcmp(member, "Resuming"))
    {
        // This signal is sent when the session has just returned from
        // Suspend() or Hibernate()
        //
        if (cb_ptr_)
        {
            cb_ptr_->on_resume();
        }
    }
    else
    {
        return false;
    }

    return true; // processed
}

bool upower_listener::dispatch_messages(int timeout_ms)
{
    if (dbus_connection_read_write_dispatch(connection_, timeout_ms))
    {
        return true;
    }

    return false; // error
}

void upower_listener::thread_func()
{
    const int timeout_ms = 200;

    while (!shutdown_)
    {
        if (!dispatch_messages(timeout_ms))
        {
            break;
        }
    }
}

} // namespace dvblink

#endif

// $Id: upower_listener.cpp 9464 2013-12-07 13:40:02Z pashab $
