/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <signal.h>
#include "application.h"
#include "sleep_listener.macos.h"

namespace dvblex { 

class application_macos_t : public application_t<application_macos_t>
{
public:
    application_macos_t();
    ~application_macos_t();

private:
    class sleep_callbacks : public sleep_listener::callbacks
    {
    public:
        explicit sleep_callbacks(application_macos_t* app) :
            app_(app)
        {
        }

        void on_standby();
        void on_resume();

    private:
        application_macos_t* app_;
    };

    bool start_command_line();
    bool daemonize();
    bool daemonize(bool& bChild, pid_t& pidChild);

    bool set_signal_handlers();
    static void signal_handler(int iSignal);

    dvblex_core_t* create_core(bool command_line);
};

}
