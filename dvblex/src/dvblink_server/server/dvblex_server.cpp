/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <stdio.h>
#include <dl_logger.h>


#if defined(_WIN32)
#include "application_windows.h"
typedef dvblex::application_windows_t app;
#elif defined(__APPLE__)
#include "application_macos.h"
typedef dvblex::application_macos_t app;
#else
#include "application_linux.h"
typedef dvblex::application_linux_t app;
#endif

using namespace dvblex;
using namespace dvblink;


int main(int argc, char* argv[])
{
    try
    {
        if (app::instance()->init(argc, argv))
        {
            app::instance()->run();
        }
        dvblex::settings::permanent_settings_storage::instance().shutdown();
        app::shutdown();
    }
    catch (std::exception& e)
    {
        std::cout << e.what() << std::endl;
#ifdef _WIN32
        OutputDebugStringA(e.what());
#else
        std::cerr << e.what() << std::endl;
#endif
    }

    return 0;
}
