/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#include <memory>
#include <errno.h>
#include <signal.h>
#include <execinfo.h>
#include <sys/stat.h>

#include <boost/thread.hpp>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_dvblex_core.h>
#include "application_macos.h"

using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink;


namespace dvblex { 

typedef application_t<application_macos_t> app_macos_t;
template<> application_macos_t* app_macos_t::instance_ = NULL;

application_macos_t::application_macos_t() :
    app_macos_t()
{
}

application_macos_t::~application_macos_t()
{
}

dvblex_core_t* application_macos_t::create_core(bool command_line)
{
    return new dvblex_core_t(private_root_.to_string().c_str(), shared_root_.to_string().c_str(), &restart_event_);
}

bool application_macos_t::daemonize()
{
    bool child = false;
    pid_t pid_child = 0;

    daemonize(child, pid_child);

    if (child == false)
    {
        exit(0); // Exit parent process
    }

    dvblex_core_ = create_core(false);
    log_info(L"tvmosaic_server daemon started (pid=%1%)") % getpid();

    set_signal_handlers();
    quit_ = !dvblex_core_->start();

    sleep_listener::callbacks_ptr cb = sleep_listener::callbacks_ptr(new sleep_callbacks(this));

    sleep_listener pm_listener(cb);
    log_info(L"sleep_listener started");

    while (!quit_)
    {
        if (restart_event_.is_signaled())
        {
            dvblex_core_->stop();
            restart_event_.reset();
            dvblex_core_->start();
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
    }

    dvblex_core_->stop();
    return true;
}

bool application_macos_t::start_command_line()
{
    set_signal_handlers();
    dvblex_core_ = create_core(true);

    quit_ = !dvblex_core_->start();

    sleep_listener::callbacks_ptr cb = sleep_listener::callbacks_ptr(new sleep_callbacks(this));

    sleep_listener pm_listener(cb);
    log_info(L"sleep_listener started");

    while (!quit_)
    {
        if (restart_event_.is_signaled())
        {
            dvblex_core_->stop();
            restart_event_.reset();
            dvblex_core_->start();
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(50));
    }

    dvblex_core_->stop();
    return true;
}

void report_trouble()
{
    void *callstack[128];
    int frames = backtrace(callstack, 128);
    char **strs = backtrace_symbols(callstack, frames);

    FILE *f = fopen("tvm_crash_report.txt", "w");
    
    if (f)
    {
        for(int i = 0; i < frames; ++i)
        {
            fprintf(f, "%s\n", strs[i]);
        }
        fclose(f);
    }

    free(strs);
}

void application_macos_t::signal_handler(int signum)
{
    bool exit = false;

    switch (signum)
    {
    case SIGHUP:    // terminal line hangup
        break;

    case SIGINT:    // interrupt program
        log_info(L"Received SIGINT: Interrupt from keyboard.");
        exit = true;
        break;

    case SIGQUIT:   // quit program
        log_info(L"Received SIGQUIT: Quit from keyboard.");
        exit = true;
        break;

    case SIGTSTP:   // stop signal generated from keyboard
        log_info(L"Received SIGTSTP: Stop from keyboard.");
        exit = true;
        break;

    case SIGTERM:   // software termination signal
        log_info(L"Received SIGTERM: Software termination signal.");
        exit = true;
        break;

    case SIGSEGV:   // segmentation violation
        log_error(L"Error. Received SIGSEGV: Segmentation Violation.");
        report_trouble();
        signal(signum, SIG_DFL);
        //exit(1);
        break;

    case SIGKILL:   // kill program
        log_info(L"Received SIGKILL: Kill program.");
        exit = true;
        break;

    case SIGPIPE:   // write on a pipe with no reader
        log_info(L"Received SIGPIPE: Write on a pipe with no reader.");
        break;      // ignore

    case SIGUSR1:   // User defined signal 1
        break;

    case SIGUSR2:   // User defined signal 2
        break;

    default:
    	log_info(L"Received signal: %1%") % signal;
    }

    if (exit == true)
    {
    	app_macos_t::instance()->signal_quit();
    }
}

bool application_macos_t::set_signal_handlers()
{
    //
    // Install signal handlers
    //
    sigset_t sSigSet;
    sigemptyset(&sSigSet);

    struct sigaction sSigAction;

    sSigAction.sa_mask = sSigSet;
    sSigAction.sa_flags = 0;
    sSigAction.sa_handler = signal_handler;

    int a_signals[] =
    {
        SIGHUP,     // Terminal line hangup
        SIGINT,     // Interrupt program
        SIGQUIT,    // Quit program
        SIGTSTP,    // Stop signal generated from keyboard
        SIGSEGV,    // Segmentation violation
        SIGPIPE,    // Write on a pipe with no reader
        SIGTERM,    // Software termination signal
        SIGUSR1,    // User defined signal 1
        SIGUSR2     // User defined signal 2
    };

    for (size_t i = 0; i < sizeof(a_signals)/sizeof(a_signals[0]); i++)
    {
        sSigAction.sa_handler = signal_handler;

        int nRet = sigaction(a_signals[i], &sSigAction, NULL);

        if (nRet != 0)
        {
            return false;
        }
    }   

    return true;
}

bool application_macos_t::daemonize(bool& child, pid_t& pid_child)
{
    bool res = true;

    if (getppid() == 1)
    {
        //
        // Already a daemon
        //
        child = true;
        pid_child = getpid();

        return true;
    }

    //
    // fork() so the parent can exit, this returns control to the command
    // line or shell invoking your program. This step is required so that
    // the new process is guaranteed not to be a process group leader.
    //
    pid_t pid = fork();

    if (pid == -1)
    {
        switch (errno)
        {
        case EAGAIN:
            //
            // The system-imposed limit on the total number of
            // processes under execution would be exceeded.
            //
            res = false;
            break;

        case ENOMEM:
            //
            // There is insufficient swap space for the new process.
            //
            res = false;
            break;

        default:
            res = false;
            break;
        }

        return res;
    }

    pid_child = pid;

    if (pid > 0)
    {
        //
        // Parent process
        //
        child = false;
        return res;
    }
    else
    {
        //
        // Child process
        //
        child = true;
    }

    //
    // setsid() to become a process group and session group leader.
    // Since a controlling terminal is associated with a session,
    // and this new session has not yet acquired a controlling terminal
    // our process now has no controlling terminal, which is a Good Thing
    // for daemons.
    //
    setsid();

    //
    // chdir("/") to ensure that our process doesn't keep any directory in
    // use. Failure to do this could make it so that an administrator
    // couldn't unmount a filesystem, because it was our current directory.
    //
    chdir("/");

    //
    // Set file permissions (write access for the owner only)
    //
    umask(022);

    //
    // Release the standard in, out, and error we
    // inherited from our parent process
    //
    close(0);
    close(1);
    close(2);

    //
    // Establish new open descriptors for stdin, stdout and stderr
    //
    open("/dev/null", O_RDWR);  // stdin
    dup(0);                     // stdout
    dup(0);                     // stderr

    //
    // Ignore some signals
    //
    signal(SIGCHLD, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);
    signal(SIGTTIN, SIG_IGN);

    return res;
}

void application_macos_t::sleep_callbacks::on_standby()
{
    if (app_)
    {
        log_info(L"sleep_listener::on_standby()");
        app_->core()->standby();
    }
}

void application_macos_t::sleep_callbacks::on_resume()
{
    if (app_)
    {
        log_info(L"sleep_listener::on_resume()");
        app_->core()->resume();
    }
}

}
