/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <stdexcept>
#include <dl_logger.h>
#include "sleep_listener.macos.h"

namespace dvblink {

using namespace logging;

sleep_listener::sleep_listener(callbacks_ptr cb_ptr) :
    cb_ptr_(cb_ptr), root_port_(0)
{
    // register to receive system sleep notifications
    root_port_ = IORegisterForSystemPower(this, &notify_port_ref_,
        &sleep_listener::sleep_callback, &notifier_object_);

    if (root_port_ != 0)
    {
        // start the worker thread
        thread_ = thread_ptr(new boost::thread(boost::bind(
            &sleep_listener::thread_func, this)));
    }
}

sleep_listener::~sleep_listener()
{
    if (thread_)
    {
        // unblock the CFRunLoopRun() call
        CFRunLoopStop(run_loop_ref_);

        // stop the worker thread
        boost::posix_time::time_duration delay(0, 0, 8, 0); // 8 sec
        thread_->timed_join(delay);
    }
}

void sleep_listener::sleep_callback(void* ref_con, io_service_t service,
    natural_t message_type, void* message_arg)
{
    if (ref_con)
    {
        sleep_listener* this_ = static_cast<sleep_listener*>(ref_con);
        this_->handle_message(message_type, message_arg);
    }
}

void sleep_listener::handle_message(natural_t message_type, void* message_arg)
{
    switch (message_type)
    {
    case kIOMessageCanSystemSleep:
        log_info(L"sleep_listener: kIOMessageCanSystemSleep");

        // Idle sleep is about to kick in. This message will not be sent for
        // forced sleep. Applications have a chance to prevent sleep by 
        // calling IOCancelPowerChange. Most applications should not prevent
        // idle sleep. Power Management waits up to 30 seconds for you to
        // either allow or deny idle sleep. If you don't acknowledge this
        // power change by calling either IOAllowPowerChange or 
        // IOCancelPowerChange, the system will wait 30 seconds then go to
        // sleep.
        //
        #if 0
            // uncomment to cancel idle sleep
            IOCancelPowerChange(root_port_, (long)message_arg);
        #else
            // we will allow idle sleep
            IOAllowPowerChange(root_port_, (long)message_arg);
        #endif
        break;

    case kIOMessageSystemWillSleep:
        log_info(L"sleep_listener: kIOMessageSystemWillSleep");

        // The system WILL go to sleep. If you do not call IOAllowPowerChange
        // or IOCancelPowerChange to acknowledge this message, sleep will be
        // delayed by 30 seconds.
        //
        // NOTE: If you call IOCancelPowerChange to deny sleep it returns
        // kIOReturnSuccess, however the system WILL still go to sleep.
        //
        if (cb_ptr_)
        {
            cb_ptr_->on_standby();
        }
        
        IOAllowPowerChange(root_port_, (long)message_arg);
        break;

    case kIOMessageSystemWillPowerOn:
        // System has started the wake up process...
        log_info(L"sleep_listener: kIOMessageSystemWillPowerOn");
        break;

    case kIOMessageSystemHasPoweredOn:
        // System has finished waking up...
        log_info(L"sleep_listener: kIOMessageSystemHasPoweredOn");

        if (cb_ptr_)
        {
            cb_ptr_->on_resume();
        }
        break;

    default:
        break;
    }
}

void sleep_listener::thread_func()
{
    // start the run loop to receive sleep notifications
    //log_info(L"sleep_listener::thread_func() started");

    run_loop_ref_ = CFRunLoopGetCurrent();

    // add the notification port to the application runloop
    CFRunLoopAddSource(run_loop_ref_,
        IONotificationPortGetRunLoopSource(notify_port_ref_),
        kCFRunLoopCommonModes);

    CFRunLoopRun(); // blocking call    
    //log_info(L"sleep_listener::thread_func() terminated");
}

} // namespace dvblink

// $Id: sleep_listener.macos.cpp 11661 2015-07-02 11:37:57Z pashab $
