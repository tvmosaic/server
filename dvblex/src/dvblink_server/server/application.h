/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <dl_strings.h>
#include <dl_filesystem_path.h>
#include <dl_dvblex_core.h>
#include <dl_permanent_settings.h>
#include <dl_event.h>

namespace dvblex { 

template <typename App>
class application_t
{
#if defined(_WIN32)
friend class application_windows_t;
friend bool CtrlHandler(unsigned long fdwCtrlType);
#elif defined(_DARWIN_X86_64)
friend class application_macos_t;
#else
friend class application_linux_t;
#endif

public:
    static application_t* instance()
    {
        if (!instance_)
        {
            instance_ = new App();
        }
        return instance_;
    }

    static void shutdown()
    {
        delete instance_;
        instance_ = NULL;
    }

    virtual bool init(int argc, char** argv);
    virtual void run();

    void signal_quit() {quit_ = true;}

protected:
    //virtual bool init() = 0;
    virtual void check_start_params() {}
    virtual bool daemonize() = 0;
    virtual bool start_command_line() = 0;

    dvblex_core_t* core() const {return dvblex_core_;}

private:
    application_t();
    virtual ~application_t();

    // Avoid copying
    application_t(const application_t&);
    application_t& operator = (const application_t&);

protected:
    static App* instance_;
    dvblex_core_t* dvblex_core_;

    bool cmd_line_mode_;
    bool quit_;
    dvblink::event restart_event_;

    dvblink::filesystem_path_t private_root_;
    dvblink::filesystem_path_t shared_root_;
};


template <typename App>
application_t<App>::application_t() :
    dvblex_core_(NULL),
    cmd_line_mode_(false),
    quit_(false)
{
    dvblex::settings::permanent_settings_storage::instance();
}

template <typename App>
application_t<App>::~application_t()
{
    delete dvblex_core_;
}

template <typename App>
bool application_t<App>::init(int argc, char** argv)
{
    private_root_ = settings::permanent_settings::get_private_root();
    shared_root_ = settings::permanent_settings::get_shared_root();

    if (argc > 1)
    {
        if (boost::algorithm::equals(argv[1], L"-command_line_mode") ||
            boost::algorithm::equals(argv[1], L"--command_line_mode"))
        {
            dvblink::logging::log_info(L"command line mode chosen");
            cmd_line_mode_ = true;
        }
    }
    return true;
}

template <typename App>
void application_t<App>::run()
{
    check_start_params();

    if (cmd_line_mode_)
    {
        start_command_line();
    }
    else
    {
        daemonize();
    }
}

}
