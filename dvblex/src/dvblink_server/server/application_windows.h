/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include "application.h"

namespace dvblex { 

class application_windows_t : public application_t<application_windows_t>
{
public:
    application_windows_t();
    ~application_windows_t();

private:
    //bool init();

    dvblex_core_t* create_core(bool command_line);
    bool start_command_line();
    bool daemonize();

    virtual void check_start_params();

    void update_status(unsigned long state, unsigned long exit_code);
    void report_to_event_log(const unsigned long event_id, const unsigned short error_type, const unsigned short inserts_count, const char* message);

    bool add_event_source(wchar_t* szServicePathName);

    static unsigned long WINAPI service_control(unsigned long control, unsigned long event_type, void* event_data, void* context);
    static void WINAPI service_main(unsigned long argc, wchar_t** argv);

private:
    std::wstring service_name_;

#define EVTLOG_MESSAGE  0x00000001L
    static const unsigned long eventID = EVTLOG_MESSAGE;

    bool start_;
    unsigned long os_version_;

    SERVICE_STATUS service_status_;
    SERVICE_STATUS_HANDLE status_handle_;
};

}
