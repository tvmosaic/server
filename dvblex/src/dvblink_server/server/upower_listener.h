/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once
#include <dbus/dbus.h>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

namespace dvblink {

class upower_listener
{
public:
    struct callbacks
    {
        virtual void on_standby() = 0;
        virtual void on_resume() = 0;
    };

    typedef boost::shared_ptr<callbacks> callbacks_ptr;

public:
    explicit upower_listener(callbacks_ptr cb_ptr);
    ~upower_listener();

private:
    static DBusHandlerResult dbus_filter_func(
        DBusConnection* connection,
        DBusMessage* message,
        void* user_data);

    bool dispatch_messages(int timeout_ms = -1);
    bool handle_message(DBusMessage* message);
    void thread_func();

    typedef boost::shared_ptr<boost::thread> thread_ptr;

private:
    bool shutdown_;
    callbacks_ptr cb_ptr_;
    DBusConnection* connection_;
    thread_ptr thread_;
};

} // namespace dvblink

// $Id: upower_listener.h 9464 2013-12-07 13:40:02Z pashab $
