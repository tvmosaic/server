/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#if defined(_WIN32)

#include <boost/thread.hpp>
#include <dl_common.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_dvblex_core.h>
#include <dl_network_helper.h>
#include "application_windows.h"

using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink;

const wchar_t* service_name = L"tvmosaic_server";

namespace dvblex { 

typedef application_t<application_windows_t> app_win_t;
template<> application_windows_t* app_win_t::instance_ = NULL;

bool CtrlHandler(unsigned long fdwCtrlType)
{ 
    switch (fdwCtrlType)
    { 
    case CTRL_C_EVENT:
        log_info(L"Signal Control-C was received");
        app_win_t::instance()->signal_quit();
        return true;
    case CTRL_BREAK_EVENT:
        {
            log_info(L"Signal Control-Break was received");
            app_win_t::instance()->signal_quit();
         
            static bool flag = false;
            if (!flag)
            {
                app_win_t::instance()->core()->standby();
                flag = true;
            }
            else
            {
                app_win_t::instance()->core()->resume();
                flag = false;
            }
            return true;
        }
    case CTRL_CLOSE_EVENT:
    case CTRL_LOGOFF_EVENT:
    case CTRL_SHUTDOWN_EVENT:
    default:
        return false;
    } 
}

///////////////////////////////////////////////////////////////////////////////

application_windows_t::application_windows_t() :
    application_t(),
    service_name_(service_name),
    start_(true),
    os_version_(5)
{
    service_status_.dwServiceType = SERVICE_WIN32;
    service_status_.dwCurrentState = SERVICE_STOPPED;
    service_status_.dwControlsAccepted = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_POWEREVENT;
    service_status_.dwWin32ExitCode = NO_ERROR;
    service_status_.dwServiceSpecificExitCode = 0;
    service_status_.dwCheckPoint = 0;
    service_status_.dwWaitHint = 0;

    CoInitializeEx(NULL, COINIT_MULTITHREADED);

    OSVERSIONINFO osinfo;
    osinfo.dwOSVersionInfoSize = sizeof(osinfo);

    if (GetVersionEx(&osinfo))
    {
        os_version_ = osinfo.dwMajorVersion;
    }
}

application_windows_t::~application_windows_t()
{
    CoUninitialize();
}

//bool application_windows_t::init()
//{
//    return application_t::init();
//}

unsigned long application_windows_t::service_control(unsigned long dwControl, unsigned long dwEventType, void* /*lpEventData*/, void* lpContext)
{
    //server_base_interface* server = reinterpret_cast<server_base_interface*>(lpContext);
    application_windows_t* app_win = reinterpret_cast<application_windows_t*>(application_windows_t::instance());

    unsigned long result = NO_ERROR;

    switch (dwControl)
    {
    case SERVICE_CONTROL_PAUSE:
        //We do not support pausing the source
        result = ERROR_CALL_NOT_IMPLEMENTED;
        //		UpdateStatus(SERVICE_PAUSED, NO_ERROR);
        break;
    case SERVICE_CONTROL_CONTINUE:
        //We do not support continuing the source
        result = ERROR_CALL_NOT_IMPLEMENTED;
        //		UpdateStatus(SERVICE_RUNNING, NO_ERROR);
        break;
    case SERVICE_CONTROL_INTERROGATE:
        app_win->update_status(app_win->service_status_.dwCurrentState, NO_ERROR);
        break;
    case SERVICE_CONTROL_SHUTDOWN:
    case SERVICE_CONTROL_STOP:
        {
            log_info(L"TVMosaic service. Stopping the source program because of StopService or System Shutdown");
            //pServer->Stop();
            app_win_t::instance()->quit_ = true;
            app_win->update_status(SERVICE_STOP_PENDING, NO_ERROR);
        }
        break;
    case SERVICE_CONTROL_POWEREVENT:
        switch (dwEventType)
        {
        case PBT_APMSUSPEND :
            if (app_win->os_version_ > 5)
            {
                log_info(L"TVMosaic service. Suspending source in response to PBT_APMSUSPEND command");
                app_win_t::instance()->core()->standby();
            }
            else
            {
                log_info(L"TVMosaic service. PBT_APMSUSPEND command is not processed in WinXP");
            }
            break;
        case PBT_APMQUERYSUSPEND :
            if (app_win->os_version_ < 6)
            {
                log_info(L"TVMosaic service. Suspending source in response to PBT_APMQUERYSUSPEND command");
                app_win_t::instance()->core()->standby();
            }
            else
            {
                log_info(L"TVMosaic service. PBT_APMQUERYSUSPEND command is not processed in Vista");
            }
            break;
        case PBT_APMRESUMECRITICAL:
            log_info(L"TVMosaic service. Resuming source in response to PBT_APMRESUMECRITICAL command");
            app_win_t::instance()->core()->resume();
            break;
        case PBT_APMRESUMEAUTOMATIC:
            log_info(L"TVMosaic service. Resuming source in response to PBT_APMRESUMEAUTOMATIC command");
            app_win_t::instance()->core()->resume();
            break;
        case PBT_APMQUERYSUSPENDFAILED :
            if (app_win->os_version_ < 6)
            {
                log_info(L"TVMosaic service. Resuming source in response to PBT_APMQUERYSUSPENDFAILED command");
                app_win_t::instance()->core()->resume();
            }
            break;
        case PBT_APMRESUMESUSPEND :
            log_info(L"TVMosaic service. PBT_APMRESUMESUSPEND");
            break;
        default:
            log_info(L"TVMosaic service. Default case: %1%") % dwEventType;
            break;
        }
        break;
    default:
        log_error(L"TVMosaic service::service_control. Unrecognized code %1%") % dwControl;

        app_win->update_status(app_win->service_status_.dwCurrentState, ERROR_CALL_NOT_IMPLEMENTED);
        result = ERROR_CALL_NOT_IMPLEMENTED;
        break;
    }

    return result;
}

void application_windows_t::update_status(unsigned long state, unsigned long exitcode)
{
    service_status_.dwCurrentState = state;
    service_status_.dwWin32ExitCode = exitcode;

    if (!SetServiceStatus(status_handle_, &service_status_))
    {
        log_error(L"TVMosaic service::UpdateStatus. SetServiceStatus error: %1%") % GetLastError();
    }
}

void application_windows_t::check_start_params()
{
    if (!cmd_line_mode_)
    {
        log_info(L"Service mode chosen");
    }
}

void application_windows_t::service_main(unsigned long /*argc*/, wchar_t** /*argv*/)
{
    application_windows_t* app_win = reinterpret_cast<application_windows_t*>(application_windows_t::instance());

    app_win->status_handle_ = RegisterServiceCtrlHandlerEx(service_name, &application_windows_t::service_control, application_windows_t::instance()->core());
    if (!app_win->status_handle_)
    {
        log_error(L"TVMosaic service. RegisterServiceCtrlHandlerEx failed: %1%") % GetLastError();
        app_win_t::instance()->quit_ = true;
    } 

    app_win->update_status(SERVICE_START_PENDING, NO_ERROR);

    app_win_t::instance()->quit_ = !application_windows_t::instance()->core()->start();

    if (!app_win_t::instance()->quit_)
    {
        app_win->update_status(SERVICE_RUNNING, NO_ERROR);

        while (!app_win_t::instance()->quit_)
        {
            if (app_win_t::instance()->restart_event_.is_signaled())
            {
                application_windows_t::instance()->core()->stop();
                app_win_t::instance()->restart_event_.reset();
                application_windows_t::instance()->core()->start();
            }
            Sleep(100);
        }

        application_windows_t::instance()->core()->stop();

        app_win->update_status(SERVICE_STOPPED, NO_ERROR);
    }
}

dvblex_core_t* application_windows_t::create_core(bool command_line)
{
    // wait until at least one network interface becomes active
    network_helper::wait_for_network_initialization(NETWORK_CHECK_TIMEOUT_DEFAULT_VAL);
    return new dvblex_core_t(private_root_.to_string().c_str(), shared_root_.to_string().c_str(), &restart_event_);
}

bool application_windows_t::start_command_line()
{
    dvblex_core_ = create_core(true);
    bool res = SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, true) ? true : false;

    if (dvblex_core_->start())
    {
        while (!quit_)
        {
            if (restart_event_.is_signaled())
            {
                dvblex_core_->stop();
                restart_event_.reset();
                dvblex_core_->start();
            }
            boost::this_thread::sleep(boost::posix_time::milliseconds(50));
        }
        dvblex_core_->stop();
    }
    return res;
}

bool application_windows_t::daemonize()
{
    SERVICE_TABLE_ENTRY dispatchTable[] =
    {
        {const_cast<wchar_t*>(service_name), service_main},
        {0, 0}
    };

    dvblex_core_ = create_core(false);

    bool res = StartServiceCtrlDispatcher(dispatchTable) ? true : false;
    if (!res)
    {
        DWORD err = GetLastError();
        log_error(L"TVMosaic service. StartServiceCtrlDispatcher error: %1%") % err;

        std::stringstream message;
        message << "StartServiceCtrlDispatcher error: " << err;
        report_to_event_log(eventID, EVENTLOG_ERROR_TYPE, 1, message.str().c_str());
    }

    return res;
}

//bool application_windows_t::add_event_source(wchar_t* szServicePathName)
//{
//    HKEY hk; 
//    unsigned long   dwData, dwDisp; 
//
//    // Create the event source as a subkey of the log. 
//    std::wstring strSource(L"SYSTEM\\CurrentControlSet\\Services\\EventLog\\Application\\");
//    strSource += service_name_;
//
//    if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
//        strSource.c_str(), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hk, &dwDisp)) 
//    {
//        log_error(L"TVMosaic service::add_event_source. Could not create the registry key (%1%)") % strSource;
//        return false;
//    }
//
//    // Set the name of the message file. 
//    if (RegSetValueEx(hk, L"EventMessageFile", 0, REG_EXPAND_SZ,
//        (unsigned char*)szServicePathName, (unsigned long)(wcslen(szServicePathName) * sizeof(szServicePathName[0]) + 1)))
//    {
//        log_error(L"TVMosaic service::add_event_source. Could not set the event message file");
//        return false;
//    }
//
//    // Set the supported event types. 
//    dwData = EVENTLOG_ERROR_TYPE | EVENTLOG_WARNING_TYPE | EVENTLOG_INFORMATION_TYPE; 
//
//    if (RegSetValueEx(hk, L"TypesSupported", 0, REG_DWORD, (unsigned char*)&dwData, sizeof(dwData)))
//    {
//        log_error(L"TVMosaic service::add_event_source. Could not set the supported types");
//        return false;
//    }
//
//    RegCloseKey(hk); 
//    return true;
//}

void application_windows_t::report_to_event_log(const unsigned long localEventID, const unsigned short errorType, const unsigned short insertsCount, const char* message)
{
    HANDLE handle = NULL;
    LPCSTR lpszMessage[1];

    lpszMessage[0] = message;

    // Get a handle to the event log.
    handle = RegisterEventSource(NULL, service_name_.c_str());
    if (!handle) 
    {
        log_error(L"TVMosaic service::ReportToEventLog. Could not register the event source");
        return;
    }

    if (!ReportEventA(handle, errorType, 0, localEventID, NULL, insertsCount, 0, (LPCSTR*)&lpszMessage[0], NULL))
    {
        log_error(L"TVMosaic service::ReportToEventLog. Could not report the event (%1%)") % GetLastError();
    }

    DeregisterEventSource(handle); 
}

}

#endif
