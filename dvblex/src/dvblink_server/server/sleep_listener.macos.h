/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>

#include <mach/mach_port.h>
#include <mach/mach_interface.h>
#include <mach/mach_init.h>

#include <IOKit/pwr_mgt/IOPMLib.h>
#include <IOKit/IOMessage.h>

#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

namespace dvblink {

class sleep_listener
{
public:
    struct callbacks
    {
        virtual void on_standby() = 0;
        virtual void on_resume() = 0;
    };

    typedef boost::shared_ptr<callbacks> callbacks_ptr;

public:
    explicit sleep_listener(callbacks_ptr cb_ptr);
    ~sleep_listener();

private:
    static void sleep_callback(void* ref_con, io_service_t service,
        natural_t message_type, void* message_arg);

    void thread_func();
    void handle_message(natural_t message_type, void* message_arg);

private:
    callbacks_ptr cb_ptr_;

    typedef boost::shared_ptr<boost::thread> thread_ptr;
    thread_ptr thread_;

    io_connect_t root_port_; // a reference to the Root Power Domain IOService
    IONotificationPortRef notify_port_ref_;
    CFRunLoopRef run_loop_ref_;
    io_object_t notifier_object_;
};

} // namespace dvblink


// $Id: sleep_listener.macos.h 11661 2015-07-02 11:37:57Z pashab $
