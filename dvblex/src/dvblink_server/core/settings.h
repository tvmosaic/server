/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <dl_logger.h>
#include <dl_common.h>
#include <dl_filesystem_path.h>

namespace dvblex { 

class dvblex_settings_t
{
public:
    dvblex_settings_t(const dvblink::filesystem_path_t& config_file);
    ~dvblex_settings_t();

    void load();
    void save();

    dvblink::logging::e_log_level get_log_level(){return log_level_;}
    void set_log_level(dvblink::logging::e_log_level log_level){log_level_ = log_level;}

    dvblink::EDVBDefaultCodepage get_default_codepage(){return default_codepage_;}
    void set_default_codepage(dvblink::EDVBDefaultCodepage default_codepage){default_codepage_ = default_codepage;}

    boost::uint64_t get_max_logfile_size() {return max_logfile_size_;}
    void set_max_logfile_size(boost::uint64_t max_logfile_size) {max_logfile_size_ = max_logfile_size;}

    dvblink::filesystem_path_t get_temp_dir(){return temp_dir_;}

protected:
    dvblink::filesystem_path_t config_file_;
    dvblink::logging::e_log_level log_level_;
    dvblink::EDVBDefaultCodepage default_codepage_;
    boost::uint64_t max_logfile_size_;
    dvblink::filesystem_path_t temp_dir_;

    void reset();

};

}
