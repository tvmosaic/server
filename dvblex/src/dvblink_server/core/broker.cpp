/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_message_addresses.h>
#include <dl_message_common.h>
#include <dl_server_params.h>
#include <dl_language_settings.h>
#include <dl_locale_strings.h>
#include <dl_installation_settings.h>
#include <dl_permanent_settings.h>
#include <dl_xml_serialization.h>
#include <dl_file_procedures.h>
#include <dl_zip.h>
#include <dl_transcoder.h>
#include "transcoder_config.h"
#include "licensed_products.h"
#include "playback_manager.h"
#include "broker.h"

using namespace dvblex::settings;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;
using namespace dvblink::zip;
using namespace dvblink::transcoder;
using namespace dvblink;

namespace dvblex { 

broker_t::broker_t(const char* private_root, const char* shared_root, dvblink::event* restart_event) :
    state_(dss_unknown), restart_event_(restart_event)
{
    directory_config_ = boost::shared_ptr<directory_config_t>(new directory_config_t(private_root, shared_root));
    settings_ = boost::shared_ptr<dvblex_settings_t>(new dvblex_settings_t(directory_config_->get_settings_filepath()));
    licensed_products_ = boost::shared_ptr<licensed_products_t>(new licensed_products_t(*directory_config_));
    playback_manager_ = boost::shared_ptr<playback_manager_t>(new playback_manager_t());
    power_manager_ = boost::shared_ptr<power_manager_t>(new power_manager_t());
}

broker_t::~broker_t()
{
    settings_.reset();
    directory_config_.reset();
    licensed_products_.reset();
    playback_manager_.reset();
    power_manager_.reset();
}

bool broker_t::start()
{
    settings_->load();

    //start logging
    logger_file_writer_ = boost::shared_ptr<dvblink::logging::logger_file_writer>(new logger_file_writer(directory_config_->get_log_filepath(), settings_->get_log_level(), true));
    logger_file_writer_->truncate_log_file(settings_->get_max_logfile_size());

    logger_file_writer_->start();
    logger::instance()->set_interface(logger_file_writer_);

    //set default codepage
    SetDefaultDVBCodepage(settings_->get_default_codepage());

    log_info(L"broker_t::start...");

    //initialize language settings
    language_settings::CreateInstance(directory_config_->get_shared_root());

    //start iserver
    iserver_instance_ = share_object_safely(new dvblink::iserver(this));

    //create and register own message queue
    message_queue_ = share_object_safely(new message_queue(server_message_queue_addressee.get()));
    message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));

    iserver_instance_->register_queue(message_queue_);

    set_and_broadcast_server_state(dss_init);

    //power manager
    power_manager_->init(message_queue_);

    //initialize licensing engine
    licensed_products_->init(message_queue_);

    //initialize playback manager
    playback_manager_->init(message_queue_, directory_config_);

    //initialize components
    component_manager_.init(message_queue_, iserver_instance_);

    //log server info
    dump_server_info();

    set_and_broadcast_server_state(dss_starting);

    log_info(L"broker_t::starting components");

    //start all components
    component_manager_.start();

    //broadcast state change
    set_and_broadcast_server_state(dss_active);

    log_info(L"broker_t::start. Active.");

    return true;
}

void broker_t::shutdown()
{
    log_info(L"broker_t::shutdown...");

    set_and_broadcast_server_state(dss_shutting_down);

    //stop all components
    component_manager_.stop();
    component_manager_.term();

    //stop playback manager
    playback_manager_->term();

    //stop licensing engine
    licensed_products_->term();

    //power manager
    power_manager_->term();

    set_and_broadcast_server_state(dss_shut_down);

    log_info(L"broker_t::shutdown. Shutdown is completed.");

    //stop and deregister message queue
    iserver_instance_->unregister_queue(message_queue_->get_id());
    message_queue_->shutdown();

    //stop iserver
    iserver_instance_.reset();

    //shutdown language settings
    language_settings::DestroyInstance();

    //shutdown logger
    logger::instance()->shutdown();
    
	logger_file_writer_->stop();
	logger_file_writer_.reset();
}

void broker_t::standby()
{
    log_info(L"broker_t::standby...");

    set_and_broadcast_server_state(dss_going_to_standby);

    component_manager_.standby();

    set_and_broadcast_server_state(dss_standby);
}

void broker_t::resume()
{
    log_info(L"broker_t::resume...");

    set_and_broadcast_server_state(dss_waking_up);

    component_manager_.resume();

    set_and_broadcast_server_state(dss_active);
}

void broker_t::set_and_broadcast_server_state(dvblink_server_state_e state)
{
    dvblink_server_state_e old_state = get_server_state();
    dvblink_server_state_e new_state = state;
    
    set_server_state(new_state);

    dvblink::messaging::server_state_change_request req(old_state, new_state);
    message_queue_->post(dvblink::messaging::broadcast_addressee, req);
}

void broker_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::server_state_request& request, dvblink::messaging::server::server_state_response& response)
{
    response.state_ = get_server_state();
}

void broker_t::handle(const message_sender_t& sender, const server::shared_module_path_request& request, server::shared_module_path_response& response)
{
    response.path_ = directory_config_->get_shared_directory(request.module_name_.get());
}

void broker_t::handle(const message_sender_t& sender, const server::private_module_path_request& request, server::private_module_path_response& response)
{
    response.path_ = directory_config_->get_private_directory(request.module_name_.get());
}

void broker_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::common_item_file_info_request& request, dvblink::messaging::server::common_item_file_info_response& response)
{
    response.result_ = true;

    if (boost::iequals(request.file_item_.get(), common_item_sqlite.get()))
        directory_config_->get_sqlite_file_info(response.file_, response.dir_);
    else if (boost::iequals(request.file_item_.get(), common_item_ffmpeg.get()))
        directory_config_->get_ffmpeg_file_info(response.file_, response.dir_);
    else if (boost::iequals(request.file_item_.get(), common_item_ffprobe.get()))
        directory_config_->get_ffprobe_file_info(response.file_, response.dir_);
    else if (boost::iequals(request.file_item_.get(), common_item_comskip.get()))
        directory_config_->get_comskip_file_info(response.file_, response.dir_);
    else if (boost::iequals(request.file_item_.get(), common_item_xmltv_cat_definitions.get()))
        directory_config_->get_xmltv_cat_defs_file_info(response.file_, response.dir_);
    else if (boost::iequals(request.file_item_.get(), common_item_temp.get()))
    {
        if (!settings_->get_temp_dir().empty())
            response.dir_ = settings_->get_temp_dir();
        else
            response.dir_ = directory_config_->get_temp_path();
    } else
    {
        log_error(L"broker_t::handle common_item_file_info_request. Request for unknown file item %1%") % request.file_item_.to_wstring();
        response.result_ = false;
    }
}

void broker_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::server_info_request& request, dvblink::messaging::server::server_info_response& response)
{
    product_id_t server_id = licensed_products_->get_server_product_id();

    product_info_t product_info;
    if (licensed_products_->find_product(server_id, product_info))
    {
        response.server_info_.install_id_ = permanent_settings::get_server_id().get();
        response.server_info_.product_id_ = product_info.id_;
        response.server_info_.product_name_ = product_info.name_;
        response.server_info_.version_ = product_info.version_; // Version has to be gtoe 7.0.0 as some clients, like Kodi, rely on the version number - sequential from DVBLink.
        response.server_info_.build_ = product_info.revision_;
    } else
    {
        log_error(L"broker_t::handle server_info_request. Cannot find server product information file");
    }
}

void broker_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::ffmpeg_launch_params_request& request, dvblink::messaging::server::ffmpeg_launch_params_response& response)
{
    dvblink::filesystem_path_t ffmpeg_exepath;
    dvblink::filesystem_path_t ffmpeg_dir;
    directory_config_->get_ffmpeg_file_info(ffmpeg_exepath, ffmpeg_dir);

    ffmpeg_profile_reader_t pr(ffmpeg_exepath, ffmpeg_dir, directory_config_->get_ext_ffmpeg_directory());

    ffmpeg_profile_config fpc(directory_config_->get_transcoder_config_filepath());
    std::string profile_id = fpc.get_profile_id_for_usecase(request.usecase_id_);

    response.result_ = pr.get_ffmpeg_launch_params_for_usecase(request.usecase_id_, profile_id, response.launch_params_);
}

void broker_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response)
{
    if (boost::iequals(request.cmd_id_.get(), get_server_params_cmd))
    {
        log_info(L"broker_t::handle xml_command. Get server parameters");
        server_params sp;
        sp.code_page_ = settings_->get_default_codepage();
        sp.log_level_ = settings_->get_log_level();

        std::string res;
        if (write_to_xml(sp, res))
        {
            response.xml_ = res;
            response.result_ = dvblink::xmlcmd_result_success;
        } else
        {
            response.result_ = dvblink::xmlcmd_result_error;
        }
    }
    else
    if (boost::iequals(request.cmd_id_.get(), set_server_params_cmd))
    {
        log_info(L"broker_t::handle xml_command. Set server parameters");
        server_params sp;
        if (read_from_xml(request.xml_.get(), sp))
        {
            settings_->set_default_codepage(sp.code_page_);
            settings_->set_log_level(sp.log_level_);
            settings_->save();
            //apply new settings
            logger_file_writer_->set_log_level(sp.log_level_);
            SetDefaultDVBCodepage(sp.code_page_);

            response.result_ = dvblink::xmlcmd_result_success;
        }
        else
        {
            response.result_ = dvblink::xmlcmd_result_error;
        }
    }
    else
    if (boost::iequals(request.cmd_id_.get(), get_transcoding_params_cmd))
    {
        log_info(L"broker_t::handle xml_command. Get transcoding parameters");

        dvblink::filesystem_path_t ffmpeg_exepath;
        dvblink::filesystem_path_t ffmpeg_dir;
        directory_config_->get_ffmpeg_file_info(ffmpeg_exepath, ffmpeg_dir);

        ffmpeg_profile_reader_t pr(ffmpeg_exepath, ffmpeg_dir, directory_config_->get_ext_ffmpeg_directory());
        pr.read();

        ffmpeg_profile_config::usecase_profiles_map_t usecase_profiles_map;

        transcoder_usecase_list_t tu_list;
        get_transcoder_usecase_list(tu_list);
        for (size_t i=0; i<tu_list.size(); i++)
        {
            ffmpeg_tc_short_profile_desc_list_t profile_list;
            pr.get_profiles_for_usecase(tu_list[i].id_, profile_list);
            usecase_profiles_map[tu_list[i].id_] = profile_list;
        }

        ffmpeg_profile_config fpc(directory_config_->get_transcoder_config_filepath());
        get_transcoding_params_response_t rr;
        fpc.get_profile_config(usecase_profiles_map, rr.params_);

        std::string res;
        if (write_to_xml(rr, res))
        {
            response.xml_ = res;
            response.result_ = dvblink::xmlcmd_result_success;
        } else
        {
            response.result_ = dvblink::xmlcmd_result_error;
        }
    }
    else
    if (boost::iequals(request.cmd_id_.get(), set_transcoding_params_cmd))
    {
        log_info(L"broker_t::handle xml_command. Set transcoding parameters");
        set_transcoding_params_request_t sp;
        if (read_from_xml(request.xml_.get(), sp))
        {
            ffmpeg_profile_config fpc(directory_config_->get_transcoder_config_filepath());
            fpc.set_profile_config(sp.params_);

            response.result_ = dvblink::xmlcmd_result_success;
        }
        else
        {
            response.result_ = dvblink::xmlcmd_result_error;
        }
    }
    else
    if (boost::iequals(request.cmd_id_.get(), do_installation_backup_cmd))
    {
        log_info(L"broker_t::handle xml_command. Do backup");

        //enum all xml files in config directory
        std::vector<boost::filesystem::path> config_files;
        filesystem::find_files(directory_config_->get_config_path().to_boost_filesystem(), config_files, L".xml");

        dvblink::filesystem_path_t out_zip = directory_config_->get_temp_path() / "backup.zip";
        zip_file_writer writer(out_zip);

        for (size_t i=0; i<config_files.size(); i++)
        {
            dvblink::filesystem_path_t f(config_files[i]);
            writer.add_file(f);
        }

        writer.flush();

        std::vector<boost::uint8_t> data;

        {
            FILE* f = filesystem::universal_open_file(out_zip, "r+b");
            if (f != NULL)
            {
                boost::int64_t file_size = filesystem::get_file_size(out_zip.to_string());
                if (file_size > 0)
                {
                    data.resize(file_size);
                    fread(&data[0], file_size, 1, f);
                }
                fclose(f);
            }
        }

        try {
            boost::filesystem::remove(out_zip.to_boost_filesystem());
        } catch(...) {}

        if (data.size() > 0)
        {
            server_binary_data_param slr(data, "application/zip");

            std::string res;
            if (write_to_xml(slr, res)) {
                response.xml_ = res;
                response.result_ = dvblink::xmlcmd_result_success;
            } else
            {
                response.result_ = dvblink::xmlcmd_result_error;
            }
        } else
        {
            response.result_ = dvblink::xmlcmd_result_error;
        }

    }
    else
    if (boost::iequals(request.cmd_id_.get(), do_installation_restore_cmd))
    {
        log_info(L"broker_t::handle xml_command. Do restore");

        bool bres = false;
        server_binary_data_param sbd;
        if (read_from_xml(request.xml_.get(), sbd))
        {
            bres = true;

            std::vector<boost::uint8_t> zipped_data;
            try {
                sbd.get_binary_data(zipped_data);
            } catch(...){bres = false;}

            if (bres)
            {
                bres = false;

                dvblink::filesystem_path_t restore_zip = directory_config_->get_temp_path() / "restore.zip";
                FILE* f = filesystem::universal_open_file(restore_zip, "w+b");
                if (f != NULL)
                {
                    size_t cnt = fwrite(&zipped_data[0], 1, zipped_data.size(), f);
                    fclose(f);
                    bres = (cnt == zipped_data.size());
                }

                if (bres)
                {
                    {
                        //check if zip file is valid
                        zipfile_reader zr(restore_zip);
                        bres = zr.is_valid();
                    }

                    if (bres)
                    {
                        bres = false;

                        //delete all existing xml files from config folder 
                        std::vector<boost::filesystem::path> config_files;
                        filesystem::find_files(directory_config_->get_config_path().to_boost_filesystem(), config_files, L".xml");

                        for (size_t i=0; i<config_files.size(); i++)
                        {
                            try {
                                boost::filesystem::remove(config_files[i]);
                            } catch(...){}
                        }

                        //unzip backup into config folder
                        {
                            zipfile_reader zr(restore_zip);
                            bres = zr.decompress(directory_config_->get_config_path());
                            if (!bres)
                                log_error(L"broker::restore. zip decompress failed");
                        }

                        //remove temp backup file
                        try {
                            boost::filesystem::remove(restore_zip.to_boost_filesystem());
                        } catch(...) {}
                    } else
                    {
                        log_error(L"broker::restore. zip file is corrupt");
                    }
                } else 
                {
                    log_error(L"broker::restore. unable to write zip file to disk");
                }
            } else 
            {
                log_error(L"broker::restore. corrupt data in message");
            }
        } else
        {
            log_error(L"broker::restore. Unable to deserialize binary data");
        }
        response.result_ = bres ? dvblink::xmlcmd_result_success : dvblink::xmlcmd_result_error;
        if (bres)
        {
            log_info(L"broker::restore. REQUESTING SERVER RESTART");
            if (restart_event_ != NULL)
                restart_event_->signal();
        }
    }
    else
    if (boost::iequals(request.cmd_id_.get(), get_server_log_cmd)) {
        log_info(L"broker_t::handle xml_command. Get server log");

        dvblink::filesystem_path_t out_zip = directory_config_->get_temp_path() / "log.zip";
        zip_file_writer writer(out_zip);
        //tvmosaic log file
        writer.add_file(directory_config_->get_log_filepath());

        //dmesg log file
        dvblink::filesystem_path_t dmesg_log = directory_config_->get_temp_path() / "dmesg.txt";

#if !defined(WIN32) && !defined (__ANDROID__)
        std::string dmesg_script = "dmesg > \"" + dmesg_log.to_string() + "\"";
        execute_script(dvblink::filesystem_path_t(dmesg_script));

        if (boost::filesystem::exists(dmesg_log.to_boost_filesystem()))
            writer.add_file(dmesg_log);
#endif

        writer.flush();

        std::vector<boost::uint8_t> data;

        {
            FILE* f = filesystem::universal_open_file(out_zip, "r+b");
            if (f != NULL)
            {
                boost::int64_t file_size = filesystem::get_file_size(out_zip.to_string());
                if (file_size > 0)
                {
                    data.resize(file_size);
                    fread(&data[0], file_size, 1, f);
                }
                fclose(f);
            }
        }

        try {
            boost::filesystem::remove(out_zip.to_boost_filesystem());
        } catch(...) {}

        try {
            boost::filesystem::remove(dmesg_log.to_boost_filesystem());
        } catch(...) {}

        if (data.size() > 0)
        {
            server_binary_data_param slr(data, "application/zip");

            std::string res;
            if (write_to_xml(slr, res))
            {
                response.xml_ = res;
                response.result_ = dvblink::xmlcmd_result_success;
            } else
            {
                response.result_ = dvblink::xmlcmd_result_error;
            }
        } else
        {
            response.result_ = dvblink::xmlcmd_result_error;
        }
    }
    else
    if (boost::iequals(request.cmd_id_.get(), get_directory_tree_cmd))
    {
        log_info(L"broker_t::handle xml_command. Get directory tree");

        directory_tree_request req;
        if (read_from_xml(request.xml_.get(), req))
        {
            filesystem::directory_tree dt(req.path_);
            log_ext_info(L"broker_t::handle xml_command. Get directory tree for >%1%<") % dt.parent_path_.to_wstring();
            bool res = filesystem::get_directory_tree(dt);

            std::string data;
            if (res)
            {
                directory_tree_response resp;
                resp.path_ = dt.parent_path_;
                resp.tree_ = dt.tree_;

                res = write_to_xml(resp, data);
            }

            if (res)
            {
                response.xml_ = data;
                response.result_ = dvblink::xmlcmd_result_success;
            }
            else
            {
                response.result_ = dvblink::xmlcmd_result_error;
            }
        }
    }
    else
    if (boost::iequals(request.cmd_id_.get(), get_server_directory_cmd))
    {
        log_info(L"broker_t::handle xml_command. Get server directory");

        get_server_directory_request req;
        if (read_from_xml(request.xml_.get(), req))
        {
            get_server_directory_response resp;

            switch (req.directory_type_)
            {
            case sdt_shared_root:
                resp.path_ = directory_config_->get_shared_root();
                break;
            case sdt_private_root:
                resp.path_ = directory_config_->get_private_root();
                break;
            case sdt_temp_directory:
                resp.path_ = directory_config_->get_temp_path();
                break;
            }

            std::string data;
            if (write_to_xml(resp, data))
            {
                response.xml_ = data;
                response.result_ = dvblink::xmlcmd_result_success;
            }
            else
            {
                response.result_ = dvblink::xmlcmd_result_error;
            }
        }
    }
    else
    {
        response.result_ = dvblink::xmlcmd_result_error;
    }
}

void broker_t::dump_server_info()
{
    product_id_t server_id = licensed_products_->get_server_product_id();

    product_info_t product_info;
    if (licensed_products_->find_product(server_id, product_info))
    {
        log_forced_info(L"---------------------------");
        log_forced_info(L"Server info:");
        log_forced_info(L"%1%, %2%, %3%") % product_info.name_.to_wstring() % product_info.version_.to_wstring() % product_info.revision_.to_wstring();
        log_forced_info(L"platform id: %1%") % server_id.to_wstring();
        log_forced_info(L"---------------------------");
    }
}

}