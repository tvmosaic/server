/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include "broker.h"
#include <dl_dvblex_core.h>

namespace dvblex {

dvblex_core_t::dvblex_core_t(const char* private_root, const char* shared_root, dvblink::event* restart_event) :
    broker_(boost::shared_ptr<broker_t>(new broker_t(private_root, shared_root, restart_event)))
{
}

dvblex_core_t::~dvblex_core_t()
{
}

bool dvblex_core_t::start()
{
    return broker_->start();
}

void dvblex_core_t::standby()
{
    return broker_->standby();
}

void dvblex_core_t::resume()
{
    return broker_->resume();
}

void dvblex_core_t::stop()
{
    broker_->shutdown();
}

} //dvblex
