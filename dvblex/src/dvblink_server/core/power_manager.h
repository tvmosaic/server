/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>
#include <boost/thread.hpp>
#include <dl_types.h>
#include <dl_message_queue.h>
#include <dl_message_power.h>

namespace dvblink { namespace engine { 
class power_manager;
}}

namespace dvblex { 

class power_manager_t
{

    class message_handler : 
        public dvblink::messaging::power::add_wakeup_time_request::subscriber,
        public dvblink::messaging::power::remove_wakeup_time_request::subscriber,
        public dvblink::messaging::power::get_wakeup_time_request::subscriber,
        public dvblink::messaging::power::enable_standby_request::subscriber,
        public dvblink::messaging::power::disable_standby_request::subscriber
    {
    public:
        message_handler(power_manager_t* power_manager, dvblink::messaging::message_queue_t message_queue) :
            dvblink::messaging::power::add_wakeup_time_request::subscriber(message_queue),
            dvblink::messaging::power::remove_wakeup_time_request::subscriber(message_queue),
            dvblink::messaging::power::get_wakeup_time_request::subscriber(message_queue),
            dvblink::messaging::power::enable_standby_request::subscriber(message_queue),
            dvblink::messaging::power::disable_standby_request::subscriber(message_queue),
            power_manager_(power_manager),
            message_queue_(message_queue)
        {
        }

        ~message_handler()
        {
        }

    protected:

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::add_wakeup_time_request& request, dvblink::messaging::power::add_wakeup_time_response& response)
        {
            power_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::remove_wakeup_time_request& request, dvblink::messaging::power::remove_wakeup_time_response& response)
        {
            power_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::get_wakeup_time_request& request, dvblink::messaging::power::get_wakeup_time_response& response)
        {
            power_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::enable_standby_request& request, dvblink::messaging::power::enable_standby_response& response)
        {
            power_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::disable_standby_request& request, dvblink::messaging::power::disable_standby_response& response)
        {
            power_manager_->handle(sender, request, response);
        }

    private:
        power_manager_t* power_manager_;
        dvblink::messaging::message_queue_t message_queue_;
    };

public:
    power_manager_t();
    ~power_manager_t();

    void init(const dvblink::messaging::message_queue_t& message_queue);
    void term();

    bool enable_standby(const dvblink::client_id_t& client_id);
    bool disable_standby(const dvblink::client_id_t& client_id);

protected:
    dvblink::messaging::message_queue_t message_queue_;
    dvblink::engine::power_manager* power_manager_;
    std::auto_ptr<message_handler> message_handler_;

    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::add_wakeup_time_request& request, dvblink::messaging::power::add_wakeup_time_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::remove_wakeup_time_request& request, dvblink::messaging::power::remove_wakeup_time_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::get_wakeup_time_request& request, dvblink::messaging::power::get_wakeup_time_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::enable_standby_request& request, dvblink::messaging::power::enable_standby_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::disable_standby_request& request, dvblink::messaging::power::disable_standby_response& response);

};

}
