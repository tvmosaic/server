/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <dl_strings.h>
#include <dl_pugixml_helper.h>
#include "settings.h"

using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::pugixml_helpers;
using namespace dvblink;

namespace dvblex { 

static const std::string dvblex_settings_root                = "tvmosaic_settings";
static const std::string dvblex_settings_log_level_node      = "log_level";
static const std::string dvblex_settings_default_codepage_node      = "default_codepage";
static const std::string dvblex_settings_max_log_size_node      = "max_log_size";
static const std::string dvblex_settings_temp_dir_node      = "temp_dir";

static const boost::uint64_t default_max_logfile_size_bytes = 256*1024;

dvblex_settings_t::dvblex_settings_t(const dvblink::filesystem_path_t& config_file) :
    config_file_(config_file)
{
}

dvblex_settings_t::~dvblex_settings_t()
{
}

void dvblex_settings_t::reset()
{
#ifndef _ANDROID_ALL
    log_level_ = log_level_errors_and_warnings;
#else
    log_level_ = log_level_none;
#endif

    default_codepage_ = EDDC_ISO_8859_1;
    max_logfile_size_ = default_max_logfile_size_bytes;
    temp_dir_.clear();
}

void dvblex_settings_t::load()
{
    reset();

    std::string str;
    boost::uint32_t value;

    pugi::xml_document doc;
    if (doc.load_file(config_file_.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL && root_node.name() == dvblex_settings_root)
        {
            if (get_node_value(root_node, dvblex_settings_log_level_node, str))
            {
                string_conv::apply(str.c_str(), value, (boost::uint32_t)log_level_errors_and_warnings);
                log_level_ = (e_log_level)value;
            }

            if (get_node_value(root_node, dvblex_settings_default_codepage_node, str))
            {
                string_conv::apply(str.c_str(), value, (boost::uint32_t)EDDC_ISO_8859_1);
                default_codepage_ = (EDVBDefaultCodepage)value;
            }

            if (get_node_value(root_node, dvblex_settings_max_log_size_node, str))
            {
                string_conv::apply(str.c_str(), max_logfile_size_, default_max_logfile_size_bytes);
            }

            if (get_node_value(root_node, dvblex_settings_temp_dir_node, str))
                temp_dir_ = str;

        }
    }
}

void dvblex_settings_t::save()
{
    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(dvblex_settings_root.c_str());
    if (root_node != NULL)
    {
        std::stringstream buf;

        buf.clear(); buf.str("");
        buf << (boost::uint32_t)log_level_;
        new_child(root_node, dvblex_settings_log_level_node, buf.str());

        buf.clear(); buf.str("");
        buf << (boost::uint32_t)default_codepage_;
        new_child(root_node, dvblex_settings_default_codepage_node, buf.str());

        buf.clear(); buf.str("");
        buf << max_logfile_size_;
        new_child(root_node, dvblex_settings_max_log_size_node, buf.str());

        new_child(root_node, dvblex_settings_temp_dir_node, temp_dir_.to_string());

        pugixml_helpers::xmldoc_dump_to_file(doc, config_file_.to_string());
    }
}

}