/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>
#include <vector>
#include <map>
#include <dl_filesystem_path.h>
#include <dl_transcoder.h>
#include <dl_parameters.h>

namespace dvblex {

const std::string ffmpeg_transcoder_profile_none_id  = "defc9cd6-8b3a-4390-abe0-120594bb654b";
const std::string ffmpeg_transcoder_profile_default_id  = "35437499-ccdd-486c-b366-d2540894ddba";

//ffmpeg profile-usecase instance (cell on profile/usecase crossing)

struct ffmpeg_profile_usecase_instance_t
{
    ffmpeg_profile_usecase_instance_t()
        : is_transport_stream_(true), mime_("video/mpeg")
    {}

    dvblink::tc_usecase_id_t usecase_id_;
    dvblink::transcoder::ffmpeg_param_template_list_t ffmpeg_param_template_list_;
    dvblink::bool_flag_t is_transport_stream_;
    dvblink::tc_usecase_mime_t mime_;
};

//ffmpeg transcoding profile: ffmpeg launch parameters + its usecase instances

enum ffmpeg_installation_type_e
{
    fit_built_in,
    fit_external,
    fit_system
};

struct ffmpeg_transcoder_profile_t
{
    ffmpeg_transcoder_profile_t() :
        type_(fit_built_in),
        default_(false)
        {}

    std::string id_;
    std::string name_;
    ffmpeg_installation_type_e type_;
    bool default_;
    dvblink::transcoder::ffmpeg_env_var_list_t environment_;

    dvblink::transcoder::transcoder_usecase_id_list_t profile_usecases_;
};

typedef std::vector<ffmpeg_transcoder_profile_t> ffmpeg_transcoder_profile_list_t;

struct ffmpeg_tc_short_profile_desc_t
{
    ffmpeg_tc_short_profile_desc_t() :
        default_(false)
        {}

    ffmpeg_tc_short_profile_desc_t(const ffmpeg_transcoder_profile_t& profile) :
        id_(profile.id_),
        name_(profile.name_),
        default_(profile.default_)
        {}

    std::string id_;
    std::string name_;
    bool default_;
};

typedef std::vector<ffmpeg_tc_short_profile_desc_t> ffmpeg_tc_short_profile_desc_list_t;

class ffmpeg_profile_reader_t
{
public:
    ffmpeg_profile_reader_t(const dvblink::filesystem_path_t& int_ffmpeg_exepath, 
                            const dvblink::filesystem_path_t& int_ffmpeg_dir,
                            const dvblink::filesystem_path_t& ext_ffmpeg_root);

    void read();

    void get_profiles_for_usecase(const dvblink::tc_usecase_id_t& usecase_id, ffmpeg_tc_short_profile_desc_list_t& profile_list);

    bool get_ffmpeg_launch_params_for_usecase(const dvblink::tc_usecase_id_t& usecase_id, const std::string& profile,  
        dvblink::transcoder::ffmpeg_launch_params_t& launch_params);

protected:
    dvblink::filesystem_path_t int_ffmpeg_exepath_;
    dvblink::filesystem_path_t int_ffmpeg_dir_;
    dvblink::filesystem_path_t ext_ffmpeg_profiles_dir_;
    dvblink::filesystem_path_t ext_ffmpeg_dir_;
    dvblink::filesystem_path_t ext_ffmpeg_exepath_;

    ffmpeg_transcoder_profile_list_t profile_list_;

    void read_profile_info(const dvblink::filesystem_path_t& parent_dir, const std::string& dirname, ffmpeg_transcoder_profile_t& profile);
    bool read_usecase_instance_file(const dvblink::filesystem_path_t& usecase_file, ffmpeg_profile_usecase_instance_t& usecase_instance);
    bool get_default_profile_for_usecase(const dvblink::tc_usecase_id_t& usecase_id, std::string& profile_id);
};

class ffmpeg_profile_config
{
public:
    typedef std::map<dvblink::tc_usecase_id_t, ffmpeg_tc_short_profile_desc_list_t> usecase_profiles_map_t;

protected:
    typedef std::map<dvblink::tc_usecase_id_t, std::string> tc_usecase_to_profile_map_t;

public:
    ffmpeg_profile_config(const dvblink::filesystem_path_t& config_file);

    const std::string get_profile_id_for_usecase(const dvblink::tc_usecase_id_t& usecase_id);
    void get_profile_config(const usecase_profiles_map_t& usecase_profiles_map, parameters_container_t& config_container);
    void set_profile_config(const concise_param_map_t& config);

protected:
    dvblink::filesystem_path_t config_file_;

    void read_config(tc_usecase_to_profile_map_t& config);
    void write_config(const tc_usecase_to_profile_map_t& config);
};

} //dvblex
