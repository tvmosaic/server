/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <boost/shared_ptr.hpp>
#include <dl_logger_file_writer.h>
#include <dl_message_server.h>
#include <dl_message_queue.h>
#include <dl_message_common.h>
#include "directory_config.h"
#include "component_manager.h"
#include "power_manager.h"
#include "iserver.h"
#include "settings.h"

namespace dvblex { 

class licensed_products_t;
class playback_manager_t;

class broker_t
{
    friend class dvblink::iserver;

    class message_handler : 
        public  dvblink::messaging::server::server_info_request::subscriber,
        public  dvblink::messaging::server::server_state_request::subscriber,
        public  dvblink::messaging::server::common_item_file_info_request::subscriber,
        public  dvblink::messaging::server::shared_module_path_request::subscriber,
        public  dvblink::messaging::server::private_module_path_request::subscriber,
        public  dvblink::messaging::server::ffmpeg_launch_params_request::subscriber,
        public  dvblink::messaging::xml_message_request::subscriber
    {
    public:
        message_handler(broker_t* broker, dvblink::messaging::message_queue_t message_queue) :
          broker_(broker),
          message_queue_(message_queue),
          dvblink::messaging::server::server_info_request::subscriber(message_queue),
          dvblink::messaging::server::server_state_request::subscriber(message_queue),
          dvblink::messaging::server::common_item_file_info_request::subscriber(message_queue),
          dvblink::messaging::server::shared_module_path_request::subscriber(message_queue),
          dvblink::messaging::server::private_module_path_request::subscriber(message_queue),
          dvblink::messaging::server::ffmpeg_launch_params_request::subscriber(message_queue),
          dvblink::messaging::xml_message_request::subscriber(message_queue)
        {
        }

        ~message_handler()
        {
        }

    protected:
        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::server_info_request& request, dvblink::messaging::server::server_info_response& response)
        {
            broker_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::shared_module_path_request& request, dvblink::messaging::server::shared_module_path_response& response)
        {
            broker_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::private_module_path_request& request, dvblink::messaging::server::private_module_path_response& response)
        {
            broker_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::common_item_file_info_request& request, dvblink::messaging::server::common_item_file_info_response& response)
        {
            broker_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::server_state_request& request, dvblink::messaging::server::server_state_response& response)
        {
            broker_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::ffmpeg_launch_params_request& request, dvblink::messaging::server::ffmpeg_launch_params_response& response)
        {
            broker_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response)
        {
            broker_->handle(sender, request, response);
        }

    private:
        broker_t* broker_;
        dvblink::messaging::message_queue_t message_queue_;
    };

public:
    broker_t(const char* private_root, const char* shared_root, dvblink::event* restart_event);
    ~broker_t();

    bool start();
    void shutdown();
    void standby();
    void resume();

protected:
    boost::shared_ptr<directory_config_t> directory_config_;
    boost::shared_ptr<dvblink::logging::logger_file_writer> logger_file_writer_;
    dvblink::iserver_t iserver_instance_;
    dvblink::messaging::message_queue_t message_queue_;
    std::auto_ptr<message_handler> message_handler_;
    boost::shared_ptr<power_manager_t> power_manager_;   
    component_manager_t component_manager_;
    dvblink::dvblink_server_state_e state_;
    boost::shared_ptr<dvblex_settings_t> settings_;
    boost::shared_ptr<licensed_products_t> licensed_products_;
    boost::shared_ptr<playback_manager_t> playback_manager_;
    dvblink::event* restart_event_;

protected:
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::shared_module_path_request& request, dvblink::messaging::server::shared_module_path_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::private_module_path_request& request, dvblink::messaging::server::private_module_path_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::server_info_request& request, dvblink::messaging::server::server_info_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::common_item_file_info_request& request, dvblink::messaging::server::common_item_file_info_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::server_state_request& request, dvblink::messaging::server::server_state_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::xml_message_request& request, dvblink::messaging::xml_message_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::server::ffmpeg_launch_params_request& request, dvblink::messaging::server::ffmpeg_launch_params_response& response);

    void set_and_broadcast_server_state(dvblink::dvblink_server_state_e state);
    void set_server_state(dvblink::dvblink_server_state_e state) {state_ = state;}
    dvblink::dvblink_server_state_e get_server_state() {return state_;}
    void dump_server_info();
};

}
