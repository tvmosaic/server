/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dli_server.h>
#include <dl_messaging_core.h>
#include "base_object_container.h"
#include "ilogger.h"

namespace dvblex { 
    class broker_t;
}

namespace dvblink {

class iserver;
typedef boost::shared_ptr<iserver> iserver_t;

class iserver : public i_server, boost::noncopyable
{
public:
    iserver(dvblex::broker_t* broker);
    ~iserver();

    const boost::uuids::uuid& __stdcall get_uid() {return id_.get();}

    i_result __stdcall query_interface(const base_id_t& requestor_id, const i_guid& iid, i_base_object_t& obj);
    i_result __stdcall query_object_interface(const base_id_t& requestor_id, const base_id_t& parent_object_id, const i_guid& queried_iid, i_base_object_t& queried_obj);

    void __stdcall register_queue(boost::shared_ptr<dvblink::messaging::i_client_message_queue> queue);
    void __stdcall unregister_queue(const dvblink::message_queue_id_t& id);

    void __stdcall register_object(const i_base_object_t& object);
    void __stdcall unregister_object(const base_id_t& object_id);

private:
    base_id_t id_;
    ilogger_t logger_instance_;
    base_object_container_t object_container_instance_;
    messaging::messaging_core_t messaging_core_instance_;
    dvblex::broker_t* broker_;
};

} //dvblink
