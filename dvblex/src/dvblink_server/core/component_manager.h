/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <boost/shared_ptr.hpp>
#include <dli_generic_component.h>
#include <dli_server.h>
#include <dl_message_queue.h>

namespace dvblex { 

class component_manager_t
{
protected:
    typedef std::vector<dvblink::messaging::i_generic_component_t> components_list_t;

public:
    component_manager_t();
    ~component_manager_t();

    bool init(dvblink::messaging::message_queue_t message_queue, dvblink::i_server_t server);
    void term();

    bool start();
    void stop();

    void standby();
    void resume();

protected:
    dvblink::i_server_t server_;
    dvblink::messaging::message_queue_t message_queue_;
    components_list_t components_;
};

}
