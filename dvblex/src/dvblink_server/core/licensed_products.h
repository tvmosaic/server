/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/enable_shared_from_this.hpp>
#include <dl_message_queue.h>
#include <dl_message_license.h>
#include "directory_config.h"

namespace dvblex { 

class licensed_products_t : public boost::enable_shared_from_this<licensed_products_t>
{

    class message_handler : 
        public dvblink::messaging::license::get_installed_products_request::subscriber,
        public dvblink::messaging::license::activate_product_request::subscriber,
        public dvblink::messaging::license::activate_product_trial_request::subscriber
    {
    public:
        message_handler(licensed_products_t* licensed_products, dvblink::messaging::message_queue_t message_queue) :
            dvblink::messaging::license::get_installed_products_request::subscriber(message_queue),
            dvblink::messaging::license::activate_product_request::subscriber(message_queue),
            dvblink::messaging::license::activate_product_trial_request::subscriber(message_queue),
            licensed_products_(licensed_products),
            message_queue_(message_queue)
        {
        }

        ~message_handler()
        {
        }

    protected:

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::license::get_installed_products_request& request, dvblink::messaging::license::get_installed_products_response& response)
        {
            licensed_products_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::license::activate_product_request& request, dvblink::messaging::license::activate_product_response& response)
        {
            licensed_products_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::license::activate_product_trial_request& request, dvblink::messaging::license::activate_product_trial_response& response)
        {
            licensed_products_->handle(sender, request, response);
        }

    private:
        licensed_products_t* licensed_products_;
        dvblink::messaging::message_queue_t message_queue_;
    };

public:
    licensed_products_t(directory_config_t& config);
    ~licensed_products_t();

    bool init(dvblink::messaging::message_queue_t message_queue);
    void term();

    dvblink::product_id_t get_server_product_id();
    bool find_product(const dvblink::product_id_t& product_id, product_info_t& product_info);

protected:
    dvblink::messaging::message_queue_t message_queue_;
    directory_config_t config_;
    std::auto_ptr<message_handler> message_handler_;

    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::license::get_installed_products_request& request, dvblink::messaging::license::get_installed_products_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::license::activate_product_request& request, dvblink::messaging::license::activate_product_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::license::activate_product_trial_request& request, dvblink::messaging::license::activate_product_trial_response& response);

    bool process_xml_product_file(const dvblink::filesystem_path_t& product_info_file_path, product_info_t& product_info);
    void read_installed_products(product_info_list_t& product_list);
    void get_installed_products(product_info_list_t& products_info);

};

}
