/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <dl_filesystem_path.h>

namespace dvblex { 

class directory_config_t
{
public:
    directory_config_t(const char* private_root, const char* shared_root);
    ~directory_config_t();

    dvblink::filesystem_path_t get_private_root(){return private_root_;}
    dvblink::filesystem_path_t get_shared_root(){return shared_root_;}

    dvblink::filesystem_path_t get_private_directory(const std::string& component);
    dvblink::filesystem_path_t get_shared_directory(const std::string& component);
    dvblink::filesystem_path_t get_private_common_directory();
    dvblink::filesystem_path_t get_shared_common_directory();
    dvblink::filesystem_path_t get_config_path();
    dvblink::filesystem_path_t get_temp_path();
    dvblink::filesystem_path_t get_product_info_directory();
    dvblink::filesystem_path_t get_licenses_directory();
    dvblink::filesystem_path_t get_scanners_directory();
    dvblink::filesystem_path_t get_ext_ffmpeg_directory();

    dvblink::filesystem_path_t get_log_filepath();
    dvblink::filesystem_path_t get_settings_filepath();
    dvblink::filesystem_path_t get_transcoder_config_filepath();

    void get_sqlite_file_info(dvblink::filesystem_path_t& file, dvblink::filesystem_path_t& dir);
    void get_ffmpeg_file_info(dvblink::filesystem_path_t& file, dvblink::filesystem_path_t& dir);
    void get_ffprobe_file_info(dvblink::filesystem_path_t& file, dvblink::filesystem_path_t& dir);
    void get_comskip_file_info(dvblink::filesystem_path_t& file, dvblink::filesystem_path_t& dir);
    void get_xmltv_cat_defs_file_info(dvblink::filesystem_path_t& file, dvblink::filesystem_path_t& dir);

protected:
    dvblink::filesystem_path_t private_root_;
    dvblink::filesystem_path_t shared_root_;

    dvblink::filesystem_path_t get_directory_from_root(dvblink::filesystem_path_t& root, const std::string& component);

};

}
