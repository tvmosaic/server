/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/algorithm/string.hpp> 
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_power_man.h>
#include "power_manager.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

namespace dvblex { 

power_manager_t::power_manager_t() :
    power_manager_(NULL)
{
}

power_manager_t::~power_manager_t()
{
}

void power_manager_t::init(const dvblink::messaging::message_queue_t& message_queue)
{
    power_manager_ = new power_manager();
    message_queue_ = message_queue;
    message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));
}

void power_manager_t::term()
{
    message_handler_.reset();
    delete power_manager_;
    power_manager_ = NULL;
}

void power_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::add_wakeup_time_request& request, dvblink::messaging::power::add_wakeup_time_response& response)
{
    wakeup_timer wt(request.wakeup_time_);
    response.result_ = power_manager_->schedule_wakeup_timer(request.client_id_, wt);
}

void power_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::remove_wakeup_time_request& request, dvblink::messaging::power::remove_wakeup_time_response& response)
{
    response.result_ = power_manager_->remove_wakeup_timer(request.client_id_);
}

void power_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::get_wakeup_time_request& request, dvblink::messaging::power::get_wakeup_time_response& response)
{
    wakeup_timer wt;
    response.result_ = power_manager_->get_wakeup_timer(request.client_id_, wt);
    if (response.result_)
        response.wakeup_time_ = wt.wakeup_time;
}

void power_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::enable_standby_request& request, dvblink::messaging::power::enable_standby_response& response)
{
    response.result_ = power_manager_->set_idle_power_state(request.client_id_);
}

void power_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::power::disable_standby_request& request, dvblink::messaging::power::disable_standby_response& response)
{
    response.result_ = power_manager_->set_no_standby_power_state(request.client_id_);
}

bool power_manager_t::enable_standby(const client_id_t& client_id)
{
    return power_manager_->set_idle_power_state(client_id);
}

bool power_manager_t::disable_standby(const client_id_t& client_id)
{
    return power_manager_->set_no_standby_power_state(client_id);
}

}