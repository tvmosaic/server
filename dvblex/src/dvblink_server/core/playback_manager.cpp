/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning (disable : 4100)
#pragma warning (disable : 4244)
#endif

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_types.h>
#include <dl_message_addresses.h>
#include <dl_pugixml_helper.h>
#include "playback_manager.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::messaging;
using namespace dvblink::pugixml_helpers;

static const std::string resume_config_file_name = "resume_config.xml";
static const size_t max_resume_entries = 50;

namespace dvblex {

playback_manager_t::playback_manager_t()
{
}

playback_manager_t::~playback_manager_t()
{
}

bool playback_manager_t::init(dvblink::messaging::message_queue_t message_queue, boost::shared_ptr<directory_config_t>& directory_config)
{
    message_queue_ = message_queue;
    message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));

    resume_config_file_ = directory_config->get_config_path() / resume_config_file_name;

    load();

    return true;
}

void playback_manager_t::term()
{
    message_handler_.reset();
}

void playback_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::register_pb_source_request& request, dvblink::messaging::playback::register_pb_source_response& response)
{
    pb_source_map_[request.pb_source_.id_] = request.pb_source_;
    response.result_ = true;
}

void playback_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::unregister_pb_source_request& request, dvblink::messaging::playback::unregister_pb_source_response& response)
{
    pb_source_map_.erase(request.id_);
    response.result_ = true;
}

static bool container_name_comp_func(const dvblex::playback::pb_container_t& first_item, const dvblex::playback::pb_container_t& second_item)
{
	return boost::to_upper_copy(first_item.name_.get()) < boost::to_upper_copy(second_item.name_.get());
}

void playback_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_objects_request& request, dvblink::messaging::playback::get_objects_response& response)
{
    if (boost::iequals(request.object_id_.get(), dvblex::playback::object_root_id.get()))
    {
        response.result_ = true;

        if (request.is_children_request_)
        {
            logging::log_ext_info(L"playback_manager_t::handle get_objects_request: root object contents request");
            //this is request for the list of playback sources
            //walk through the list of sources and ask each of them for their container object
            pb_source_map_t::iterator pb_src_it = pb_source_map_.begin();
            while (pb_src_it != pb_source_map_.end())
            {
                dvblink::messaging::playback::get_source_container_request req(request.server_address_, request.proto_);
                dvblink::messaging::playback::get_source_container_response resp;
                if (message_queue_->send(pb_src_it->second.id_.get(), req, resp) == success && resp.result_)
                    response.object_.container_list_.push_back(resp.container_);
                else
                    log_error(L"playback_manager_t::handle get_objects_request. Source %1% returned error on get_source_container_request") % pb_src_it->second.id_.to_wstring();

                ++pb_src_it;
            }

            //sort containers on name
            std::sort(response.object_.container_list_.begin(), response.object_.container_list_.end(), container_name_comp_func);

            response.object_.actual_count_ = response.object_.container_list_.size();
            response.object_.total_count_ = response.object_.container_list_.size();
        } else
        {

            dvblex::playback::pb_container_t server_container(dvblex::playback::pbct_container_source, dvblex::playback::pbit_item_unknown);
            server_container.object_id_ = dvblex::playback::object_root_id;
            server_container.parent_id_ = dvblex::playback::object_root_id; //this is just to fill in the field. root does not have a parent
            server_container.name_ = "TV Mosaic";
            server_container.total_count_ = pb_source_map_.size();
            response.object_.container_list_.push_back(server_container);

            response.object_.actual_count_ = response.object_.container_list_.size();
            response.object_.total_count_ = response.object_.container_list_.size();
		}
    } else
    {
        response.result_ = false;
        log_error(L"playback_manager_t::handle get_objects_request. Request for non-root object (should be sent to the source itself)");
    }
}

void playback_manager_t::reset()
{
    pb_resume_map_.clear();
}

static const std::string resume_config_root_node                 = "resume_config";
static const std::string resume_config_object_node               = "object";
static const std::string resume_config_id_attr                   = "id";
static const std::string resume_config_position_attr             = "pos";
static const std::string resume_config_ts_attr                   = "timestamp";

void playback_manager_t::save()
{
    std::stringstream buf;

    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(resume_config_root_node.c_str());
    if (root_node != NULL)
    {
        pb_resume_map_t::iterator it = pb_resume_map_.begin();
        while (it != pb_resume_map_.end())
        {
            pugi::xml_node obj_node = new_child(root_node, resume_config_object_node);
            if (obj_node != NULL)
            {
                add_node_attribute(obj_node, resume_config_id_attr, it->first.get());

                buf.clear(); buf.str("");
                buf << it->second.pos_;
                add_node_attribute(obj_node, resume_config_position_attr, buf.str()); 

                buf.clear(); buf.str("");
                buf << it->second.timestamp_;
                add_node_attribute(obj_node, resume_config_ts_attr, buf.str()); 
            }

            ++it;
        }

        pugixml_helpers::xmldoc_dump_to_file(doc, resume_config_file_.to_string());
    }
}

void playback_manager_t::load()
{
    reset();

    pugi::xml_document doc;
    if (doc.load_file(resume_config_file_.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            pugi::xml_node obj_node = root_node.first_child();
            while (obj_node != NULL)
            {
                if (boost::iequals(obj_node.name(), resume_config_object_node))
                {
                    std::string id;
                    std::string pos;
                    std::string ts;
                    if (get_node_attribute(obj_node, resume_config_id_attr, id) &&
                        get_node_attribute(obj_node, resume_config_position_attr, pos) &&
                        get_node_attribute(obj_node, resume_config_ts_attr, ts))
                    {
                        object_resume_info_t ri;
                        ri.pos_ = atoi(pos.c_str());
                        ri.timestamp_ = atoi(ts.c_str());
                        pb_resume_map_[id] = ri;
                    }
                }

                obj_node = obj_node.next_sibling();
            }
        }
    }
}

void playback_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::set_pb_resume_info_request& request, dvblink::messaging::playback::set_pb_resume_info_response& response)
{
    if (request.pos_ <= 0)
    {
        //delete position
        if (pb_resume_map_.find(request.object_id_) != pb_resume_map_.end())
            pb_resume_map_.erase(request.object_id_);

    } else
    {
        //add/update position
        object_resume_info_t ri;
        time(&ri.timestamp_);
        ri.pos_ = request.pos_;
        pb_resume_map_[request.object_id_] = ri;

        if (pb_resume_map_.size() > max_resume_entries)
        {
            pb_resume_map_t::iterator it = pb_resume_map_.begin();
            dvblink::object_id_t obj_id = it->first;
            time_t ts = it->second.timestamp_;
            while (it != pb_resume_map_.end())
            {
                if (it->second.timestamp_ < ts)
                {
                    ts = it->second.timestamp_;
                    obj_id = it->first;
                }
                ++it;
            }
            pb_resume_map_.erase(obj_id);
        }
    }

    save();

    response.result_ = true;
}

void playback_manager_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_pb_resume_info_request& request, dvblink::messaging::playback::get_pb_resume_info_response& response)
{
    //star tfrom 0 by default
    response.result_ = true;
    response.pos_ = 0;

    if (pb_resume_map_.find(request.object_id_) != pb_resume_map_.end())
        response.pos_ = pb_resume_map_[request.object_id_].pos_;
}

} // dvblink
