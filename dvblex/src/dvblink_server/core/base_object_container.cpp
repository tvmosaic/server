/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/thread.hpp>
#include <dli_base.h>
#include "base_object_container.h"

namespace dvblink {

void base_object_container::shutdown()
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    container_.clear();
}

void base_object_container::add(const i_base_object_t& obj)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    container_[obj->get_uid()] = obj;
}

void base_object_container::remove(const base_id_t& id)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    iter_container_t iter = container_.find(id);
    if (iter != container_.end())
    {
        container_.erase(iter);
    }
}

bool base_object_container::get(const base_id_t& id, i_base_object_t& obj)
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);
    iter_container_t iter = container_.find(id);
    if (iter != container_.end())
    {
        obj = iter->second;
        return true;
    }
    return false;
}

} //dvblink
