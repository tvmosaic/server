/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <map>
#include <boost/noncopyable.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/lexical_cast.hpp>
#include <dli_base.h>

namespace dvblink {

class base_object_container;
typedef boost::shared_ptr<base_object_container> base_object_container_t;

class base_object_container : public boost::noncopyable
{
    typedef std::map<base_id_t, i_base_object_t> container_t;
    typedef container_t::iterator iter_container_t;
    typedef container_t::const_iterator const_iter_container_t;

public:
    base_object_container() {}
    ~base_object_container() {}

    void shutdown();

    void add(const i_base_object_t& obj);
    void remove(const base_id_t& id);
    bool get(const base_id_t& id, i_base_object_t& obj);

private:
    container_t container_;
    boost::shared_mutex lock_;
};

} //dvblink
