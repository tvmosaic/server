/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string/replace.hpp>
#include <dl_logger.h>
#include <dl_file_procedures.h>
#include <dl_pugixml_helper.h>
#include <dl_language_settings.h>
#include <dl_locale_strings.h>
#include "transcoder_config.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::transcoder;

namespace dvblex {

#ifdef WIN32
    static const std::string ffmpeg_exe_name = "ffmpeg.exe";
#else
    static const std::string ffmpeg_exe_name = "ffmpeg";
#endif

const std::string ffmpeg_param_ffmpeg_dir_id  = "%FFMPEGDIR%";

static const std::string ffmpeg_profiles_dir_name = "profiles";
static const std::string ffmpeg_bin_dir_name = "bin";

static const std::string ffmpeg_profile_desc_filename = "info";
static const std::string ffmpeg_info_name_tag = "name";
static const std::string ffmpeg_info_type_tag = "type";
static const std::string ffmpeg_info_default_tag = "default";
static const std::string ffmpeg_info_type_builtin = "builtin";
static const std::string ffmpeg_info_type_external = "external";
static const std::string ffmpeg_info_type_system = "system";
static const std::string ffmpeg_info_env_tag = "env";
static const std::string ffmpeg_info_var_tag = "var";

static const std::string ffmpeg_usecase_params_tag = "params";
static const std::string ffmpeg_usecase_stream_tag = "stream";
static const std::string ffmpeg_usecase_is_ts_tag = "is_transport_stream";
static const std::string ffmpeg_usecase_mime_tag = "mime";

ffmpeg_profile_reader_t::ffmpeg_profile_reader_t(const dvblink::filesystem_path_t& int_ffmpeg_exepath, 
                                                 const dvblink::filesystem_path_t& int_ffmpeg_dir, const dvblink::filesystem_path_t& ext_ffmpeg_root) :
    int_ffmpeg_exepath_(int_ffmpeg_exepath),
    int_ffmpeg_dir_(int_ffmpeg_dir)
{
    ext_ffmpeg_profiles_dir_ = ext_ffmpeg_root / ffmpeg_profiles_dir_name;
    ext_ffmpeg_dir_ = ext_ffmpeg_root / ffmpeg_bin_dir_name;
    ext_ffmpeg_exepath_ = ext_ffmpeg_dir_ / ffmpeg_exe_name;
}

void ffmpeg_profile_reader_t::read()
{
    profile_list_.clear();

    std::vector<dvblink::filesystem_path_t> profile_dir_list;
    //enum all directories inside a profile folder
    boost::filesystem::directory_iterator end_itr;
    for (boost::filesystem::directory_iterator iter(ext_ffmpeg_profiles_dir_.to_boost_filesystem()); iter != end_itr; ++iter)
    {
        if (boost::filesystem::is_directory(iter->status()))
        {
            boost::filesystem::path sub_dir(*iter);
            profile_dir_list.push_back(dvblink::filesystem_path_t(sub_dir.filename()));
        }
    }

    //process each profile
    for (size_t i=0; i<profile_dir_list.size(); i++)
    {
        dvblink::filesystem_path_t profile_dir = ext_ffmpeg_profiles_dir_ / profile_dir_list[i];
        ffmpeg_transcoder_profile_t profile;
        //read info file
        read_profile_info(ext_ffmpeg_profiles_dir_, profile_dir_list[i].to_string(), profile);
        //read all profiles
        std::vector<boost::filesystem::path> profiles;
        if (filesystem::find_files(profile_dir.to_boost_filesystem(), profiles, L".xml"))
        {
            for (size_t j=0; j<profiles.size(); j++)
            {
                dvblink::filesystem_path_t profile_id(profiles[j].stem());
                profile.profile_usecases_.push_back(profile_id.to_string());
            }
        }
        profile_list_.push_back(profile);
    }

}

void ffmpeg_profile_reader_t::read_profile_info(const dvblink::filesystem_path_t& parent_dir, const std::string& dirname, ffmpeg_transcoder_profile_t& profile)
{
    //defaults
    profile.id_ = dirname;
    profile.name_ = dirname;

    //read info file
    dvblink::filesystem_path_t info_file = parent_dir / dirname / ffmpeg_profile_desc_filename;

    std::string str;
    pugi::xml_document doc;
    if (doc.load_file(info_file.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            if (pugixml_helpers::get_node_value(root_node, ffmpeg_info_name_tag, str))
                profile.name_ = str;

            if (pugixml_helpers::get_node_value(root_node, ffmpeg_info_default_tag, str) &&
                boost::iequals(str, pugixml_helpers::xmlnode_value_true))
                    profile.default_ = true;

            if (pugixml_helpers::get_node_value(root_node, ffmpeg_info_type_tag, str))
            {
                if (boost::iequals(str, ffmpeg_info_type_builtin))
                    profile.type_ = fit_built_in;
                else if (boost::iequals(str, ffmpeg_info_type_external))
                    profile.type_ = fit_external;
                else if (boost::iequals(str, ffmpeg_info_type_system))
                    profile.type_ = fit_system;
            }

            pugi::xml_node env_node = root_node.child(ffmpeg_info_env_tag.c_str());
            if (env_node != NULL)
            {
                pugi::xml_node var_node = env_node.first_child();
                while (var_node != NULL)
                {
                    str = var_node.child_value();
                    profile.environment_.push_back(str);

                    var_node = var_node.next_sibling();
                }
            }
        }
    }
}

bool ffmpeg_profile_reader_t::read_usecase_instance_file(const dvblink::filesystem_path_t& usecase_file, ffmpeg_profile_usecase_instance_t& usecase_instance)
{
    bool ret_val = false;

    dvblink::filesystem_path_t usecase_file_name = usecase_file.to_boost_filesystem().stem();

    usecase_instance.usecase_id_ = boost::to_lower_copy(usecase_file_name.to_string());

    pugi::xml_document doc;
    if (doc.load_file(usecase_file.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            ret_val = true;
            std::string str;

            pugi::xml_node stream_node = root_node.child(ffmpeg_usecase_stream_tag.c_str());
            if (stream_node != NULL)
            {
                if (pugixml_helpers::get_node_value(stream_node, ffmpeg_usecase_is_ts_tag, str))
                    usecase_instance.is_transport_stream_ = boost::iequals(str, "true");

                if (pugixml_helpers::get_node_value(stream_node, ffmpeg_usecase_mime_tag, str))
                    usecase_instance.mime_ = str;
            }

            pugi::xml_node params_node = root_node.child(ffmpeg_usecase_params_tag.c_str());
            if (params_node != NULL)
            {
                pugi::xml_node param_node = params_node.first_child();
                while (param_node != NULL)
                {
                    ffmpeg_param_template_t param;
                    //conditions

                    std::string cnd;

                    cnd = ffmpeg_condition_dimensions;
                    if (pugixml_helpers::get_node_attribute(param_node, cnd, str))
                        param.conditions_[cnd] = str;

                    cnd = ffmpeg_condition_bitrate;
                    if (pugixml_helpers::get_node_attribute(param_node, cnd, str))
                        param.conditions_[cnd] = str;

                    cnd = ffmpeg_condition_scale;
                    if (pugixml_helpers::get_node_attribute(param_node, cnd, str))
                        param.conditions_[cnd] = str;

                    //parameter value
                    param.param_ = param_node.child_value();

                    usecase_instance.ffmpeg_param_template_list_.push_back(param);

                    param_node = param_node.next_sibling();
                }
            }
        }
    }

    return ret_val;
}

void ffmpeg_profile_reader_t::get_profiles_for_usecase(const dvblink::tc_usecase_id_t& usecase_id, ffmpeg_tc_short_profile_desc_list_t& profile_list)
{
    for (size_t i=0; i<profile_list_.size(); i++)
    {
        ffmpeg_transcoder_profile_t& profile = profile_list_[i];
        for (size_t j=0; j<profile.profile_usecases_.size(); j++)
        {
            if (boost::iequals(usecase_id.get(), profile.profile_usecases_[j].get()))
            {
                profile_list.push_back(ffmpeg_tc_short_profile_desc_t(profile));
                break;
            }
        }
    }
}

bool ffmpeg_profile_reader_t::get_default_profile_for_usecase(const dvblink::tc_usecase_id_t& usecase_id, std::string& profile_id)
{
    bool ret_val = false;

    ffmpeg_tc_short_profile_desc_list_t profile_list;
    get_profiles_for_usecase(usecase_id, profile_list);

    for (size_t i=0; i<profile_list.size(); i++)
    {
        if (profile_list[i].default_)
        {
            profile_id = profile_list[i].id_;
            ret_val = true;
            break;
        }
    }

    return ret_val;
}

bool ffmpeg_profile_reader_t::get_ffmpeg_launch_params_for_usecase(const dvblink::tc_usecase_id_t& usecase_id, const std::string& prf_id,  
    ffmpeg_launch_params_t& launch_params)
{
    bool ret_val = false;

    //check if no profile is assigned
    if (boost::iequals(prf_id, ffmpeg_transcoder_profile_none_id))
    {
        log_info(L"ffmpeg_profile_reader_t::get_ffmpeg_launch_params_for_usecase. No profile is assigned for usecase %1%") % usecase_id.to_wstring();
        return false;
    }

    std::string profile_id = prf_id;
    //check if it is a default profile
    if (boost::iequals(prf_id, ffmpeg_transcoder_profile_default_id))
    {
        //get actual profile id
        read();

        if (!get_default_profile_for_usecase(usecase_id, profile_id))
        {
            log_info(L"ffmpeg_profile_reader_t::get_ffmpeg_launch_params_for_usecase. No default profile found for usecase %1%") % usecase_id.to_wstring();
            return false;
        }
    }

    ffmpeg_transcoder_profile_t profile;
    read_profile_info(ext_ffmpeg_profiles_dir_, profile_id, profile);

    dvblink::filesystem_path_t usecase_file = ext_ffmpeg_profiles_dir_ / profile_id;
    usecase_file /= usecase_id.to_string() + ".xml";

    ffmpeg_profile_usecase_instance_t usecase_instance;

    if (read_usecase_instance_file(usecase_file, usecase_instance))
    {
        launch_params.arguments_ = usecase_instance.ffmpeg_param_template_list_;
        launch_params.is_transport_stream_ = usecase_instance.is_transport_stream_;
        launch_params.mime_ = usecase_instance.mime_;

        switch (profile.type_)
        {
        case fit_built_in:
            launch_params.ffmpeg_dir_ = int_ffmpeg_dir_;
            launch_params.ffmpeg_exepath_ = int_ffmpeg_exepath_;
            break;
        case fit_external:
            launch_params.ffmpeg_dir_ = ext_ffmpeg_dir_;
            launch_params.ffmpeg_exepath_ = ext_ffmpeg_exepath_;
            break;
        case fit_system:
            launch_params.ffmpeg_dir_ = "";
            launch_params.ffmpeg_exepath_ = ffmpeg_exe_name;
            break;
        default:
            break;
        }

        //environment
        for (size_t i=0; i<profile.environment_.size(); i++)
        {
            std::string str = profile.environment_[i].to_string();
            boost::replace_all(str, ffmpeg_param_ffmpeg_dir_id, launch_params.ffmpeg_dir_.to_string());
            launch_params.ffmpeg_env_.push_back(str);
        }

        ret_val = true;
    }

    return ret_val;
}

ffmpeg_profile_config::ffmpeg_profile_config(const dvblink::filesystem_path_t& config_file) :
    config_file_(config_file)
{
}

const std::string ffmpeg_profile_config::get_profile_id_for_usecase(const dvblink::tc_usecase_id_t& usecase_id)
{
    tc_usecase_to_profile_map_t config;
    read_config(config);

    if (config.find(usecase_id) != config.end())
        return config[usecase_id];

    return ffmpeg_transcoder_profile_default_id;
}

const std::string ffmpeg_params_container_id = "31ed03de-9294-4c57-a41b-38904829cf0f";

void ffmpeg_profile_config::get_profile_config(const usecase_profiles_map_t& usecase_profiles_map, parameters_container_t& config_container)
{
    tc_usecase_to_profile_map_t usecase_profile_config;
    read_config(usecase_profile_config);

    config_container.id_ = ffmpeg_params_container_id;
    config_container.name_ = language_settings::GetInstance()->GetItemName(ffmpeg_params_container_name);
    config_container.desc_ = language_settings::GetInstance()->GetItemName(ffmpeg_params_container_desc);

    transcoder_usecase_list_t tu_list;
    get_transcoder_usecase_list(tu_list);

    for (size_t i=0; i<tu_list.size(); i++)
    {
        std::string selected_profile = ffmpeg_transcoder_profile_default_id;
        if (usecase_profile_config.find(tu_list[i].id_) != usecase_profile_config.end())
            selected_profile = usecase_profile_config[tu_list[i].id_];

        selectable_param_description_t usecase_element;
        usecase_element.key_ = tu_list[i].id_.get();
        usecase_element.name_ = tu_list[i].name_.get();

        usecase_element.parameters_list_.push_back(selectable_param_element_t(ffmpeg_transcoder_profile_default_id, language_settings::GetInstance()->GetItemName(ffmpeg_default_profile_name)));

        usecase_profiles_map_t::const_iterator it = usecase_profiles_map.find(tu_list[i].id_);
        if (it != usecase_profiles_map.end())
        {
            const ffmpeg_tc_short_profile_desc_list_t& profile_list = it->second;
            for (size_t j=0; j<profile_list.size(); j++)
            {
                usecase_element.parameters_list_.push_back(selectable_param_element_t(profile_list[j].id_, profile_list[j].name_));
                if (boost::iequals(selected_profile, profile_list[j].id_))
                    usecase_element.selected_param_ = usecase_element.parameters_list_.size() - 1;
            }
        }
        config_container.selectable_params_.push_back(usecase_element);
    }
}

void ffmpeg_profile_config::set_profile_config(const concise_param_map_t& config)
{
    tc_usecase_to_profile_map_t config_map;

    concise_param_map_t::const_iterator it = config.begin();
    while (it != config.end())
    {
        config_map[it->first.get()] = it->second.get();
        ++it;
    }

    write_config(config_map);
}

static const std::string transcoder_config_root = "transcoder_config";
static const std::string transcoder_config_usecase_tag = "usecase";
static const std::string transcoder_config_id_tag = "id";
static const std::string transcoder_config_profile_tag = "profile";

void ffmpeg_profile_config::read_config(tc_usecase_to_profile_map_t& config)
{
    std::string str;

    pugi::xml_document doc;
    if (doc.load_file(config_file_.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            pugi::xml_node usecase_node = root_node.first_child();
            while(usecase_node != NULL)
            {
                std::string usecase_id;
                if (pugixml_helpers::get_node_attribute(usecase_node, transcoder_config_id_tag, usecase_id))
                {
                    if (pugixml_helpers::get_node_value(usecase_node, transcoder_config_profile_tag, str))
                        config[usecase_id] = str;
                }
                usecase_node = usecase_node.next_sibling();
            }
        }
    }
}

void ffmpeg_profile_config::write_config(const tc_usecase_to_profile_map_t& config)
{
    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(transcoder_config_root.c_str());
    if (root_node != NULL)
    {
        tc_usecase_to_profile_map_t::const_iterator it = config.begin();
        while (it != config.end())
        {
            pugi::xml_node usecase_node = pugixml_helpers::new_child(root_node, transcoder_config_usecase_tag);
            if (usecase_node != NULL)
            {
                pugixml_helpers::add_node_attribute(usecase_node, transcoder_config_id_tag, it->first.get());
                pugixml_helpers::new_child(usecase_node, transcoder_config_profile_tag, it->second);
            }

            ++it;
        }

        pugixml_helpers::xmldoc_dump_to_file(doc, config_file_.to_string());
    }
}

} //dvblex
