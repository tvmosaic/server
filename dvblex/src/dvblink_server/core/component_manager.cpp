/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_message_common.h>
#include <components/network_server.h>

#include <components/xmltv.h>
#include <components/recorder.h>
#include <components/epg_manager.h>
#include <components/logo_manager.h>
#include <components/recorded_tv.h>
#ifndef _ANDROID_ALL
    #include <components/social.h>
#endif
#include <components/source_manager.h>

#include "component_manager.h"

using namespace dvblink::logging;
using namespace dvblink::messaging;

namespace dvblex { 

component_manager_t::component_manager_t()
{
}

component_manager_t::~component_manager_t()
{
}

bool component_manager_t::init(dvblink::messaging::message_queue_t message_queue, dvblink::i_server_t server)
{
    server_ = server;
    message_queue_ = message_queue;

    //initialize all components
    source_manager_obj_t source_man = boost::shared_ptr<source_manager_t>(new source_manager_t());
    source_man ->init(server_);
    components_.push_back(source_man);

    recorder_obj_t recorder = boost::shared_ptr<recorder_t>(new recorder_t());
    recorder->init(server_);
    components_.push_back(recorder);

    epg_manager_obj_t epg_man = boost::shared_ptr<epg_manager_t>(new epg_manager_t());
    epg_man->init(server_);
    components_.push_back(epg_man);

    recorded_tv_obj_t rtv = boost::shared_ptr<recorded_tv_t>(new recorded_tv_t());
    rtv->init(server_);
    components_.push_back(rtv);

#ifndef _ANDROID_ALL
    social_obj_t social = boost::shared_ptr<social_t>(new social_t());
    social->init(server_);
    components_.push_back(social);
#endif
    
    xmltv_obj_t xmltv = boost::shared_ptr<xmltv_t>(new xmltv_t());
    xmltv->init(server_);
    components_.push_back(xmltv);

    network_server_obj_t network_server = boost::shared_ptr<network_server_t>(new network_server_t());
    network_server->init(server_);
    components_.push_back(network_server);

    logo_manager_obj_t logo_manager = boost::shared_ptr<logo_manager_t>(new logo_manager_t());
    logo_manager->init(server_);
    components_.push_back(logo_manager);

    return true;
}

void component_manager_t::term()
{
    components_.clear();
}

bool component_manager_t::start()
{
    //send start to all components
    for (size_t i=0; i<components_.size(); i++)
    {
        start_request req;
        start_response resp;
        message_error error = message_queue_->send(components_[i]->get_uid(), req, resp);
        if (error != success)
            log_error(L"component_manager_t::start. Start for %1% has returned error %2%") % components_[i]->get_uid() % error;
    }
    return true;
}

void component_manager_t::standby()
{
    for (int i=components_.size() - 1; i>=0; i--)
    {
        standby_request req;
        standby_response resp;
        message_error error = message_queue_->send(components_[i]->get_uid(), req, resp);
        if (error != success)
            log_info(L"component_manager_t::standby. standby for %1% has returned error %2%") % components_[i]->get_uid() % error;
    }
}

void component_manager_t::resume()
{
    for (size_t i=0; i<components_.size(); i++)
    {
        resume_request req;
        resume_response resp;
        message_error error = message_queue_->send(components_[i]->get_uid(), req, resp);
        if (error != success)
            log_info(L"component_manager_t::resume. resume for %1% has returned error %2%") % components_[i]->get_uid() % error;
    }
}

void component_manager_t::stop()
{
    //send stop to all components
    for (int i=components_.size() - 1; i>=0; i--)
    {
        shutdown_request req;
        shutdown_response resp;
        message_error error = message_queue_->send(components_[i]->get_uid(), req, resp);
        if (error != success)
            log_info(L"component_manager_t::stop. Shutdown for %1% has returned error %2%") % components_[i]->get_uid() % error;
    }
}

}