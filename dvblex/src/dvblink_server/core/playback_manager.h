/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/uuid/uuid.hpp>
#include <boost/shared_ptr.hpp>

#include <dl_message_queue.h>
#include <dl_message_playback.h>
#include "directory_config.h"

namespace dvblex {

class playback_manager_t
{
    struct object_resume_info_t
    {
        object_resume_info_t()
            : pos_(0), timestamp_(0)
        {}

        boost::int32_t pos_;
        time_t timestamp_;
    };

    typedef std::map<dvblink::object_id_t, object_resume_info_t> pb_resume_map_t;

    class message_handler : 
        public dvblink::messaging::playback::get_objects_request::subscriber,
        public dvblink::messaging::playback::register_pb_source_request::subscriber,
        public dvblink::messaging::playback::unregister_pb_source_request::subscriber,
        public dvblink::messaging::playback::set_pb_resume_info_request::subscriber,
        public dvblink::messaging::playback::get_pb_resume_info_request::subscriber
    {
    public:
        message_handler(playback_manager_t* playback_manager, dvblink::messaging::message_queue_t message_queue) :
            dvblink::messaging::playback::get_objects_request::subscriber(message_queue),
            dvblink::messaging::playback::register_pb_source_request::subscriber(message_queue),
            dvblink::messaging::playback::unregister_pb_source_request::subscriber(message_queue),
            dvblink::messaging::playback::set_pb_resume_info_request::subscriber(message_queue),
            dvblink::messaging::playback::get_pb_resume_info_request::subscriber(message_queue),
            playback_manager_(playback_manager),
            message_queue_(message_queue)
        {
        }

        ~message_handler()
        {
        }

    protected:

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::register_pb_source_request& request, dvblink::messaging::playback::register_pb_source_response& response)
        {
            playback_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::unregister_pb_source_request& request, dvblink::messaging::playback::unregister_pb_source_response& response)
        {
            playback_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_objects_request& request, dvblink::messaging::playback::get_objects_response& response)
        {
            playback_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::set_pb_resume_info_request& request, dvblink::messaging::playback::set_pb_resume_info_response& response)
        {
            playback_manager_->handle(sender, request, response);
        }

        void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_pb_resume_info_request& request, dvblink::messaging::playback::get_pb_resume_info_response& response)
        {
            playback_manager_->handle(sender, request, response);
        }

    private:
        playback_manager_t* playback_manager_;
        dvblink::messaging::message_queue_t message_queue_;
    };

public:
    playback_manager_t();
    ~playback_manager_t();

    bool init(dvblink::messaging::message_queue_t message_queue, boost::shared_ptr<directory_config_t>& directory_config);
    void term();

protected:
    typedef std::map<dvblink::pb_source_id_t, dvblex::playback::pb_source_t> pb_source_map_t;

    dvblink::messaging::message_queue_t message_queue_;
    std::auto_ptr<message_handler> message_handler_;
    pb_source_map_t pb_source_map_;
    dvblink::filesystem_path_t resume_config_file_;
    pb_resume_map_t pb_resume_map_;

private:
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::register_pb_source_request& request, dvblink::messaging::playback::register_pb_source_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::unregister_pb_source_request& request, dvblink::messaging::playback::unregister_pb_source_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_objects_request& request, dvblink::messaging::playback::get_objects_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::set_pb_resume_info_request& request, dvblink::messaging::playback::set_pb_resume_info_response& response);
    void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_pb_resume_info_request& request, dvblink::messaging::playback::get_pb_resume_info_response& response);

    void load();
    void save();
    void reset();
};

} // dvblex
