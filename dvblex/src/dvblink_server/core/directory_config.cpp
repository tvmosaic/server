/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_installation_settings.h>
#include "directory_config.h"
#include "constants.h"

using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblex::settings;
using namespace dvblink;

namespace dvblex { 

const std::string log_file_name			    = "tvmosaic_server.log";
const std::string settings_file_name	        = "tvmosaic_settings.xml";
const std::string transcoder_config_file_name = "transcoder_config.xml";

const std::string product_info_directory	    = "product_info";
const std::string licenses_directory	        = "licenses";
const std::string scanners_directory	        = "scanners";
const std::string temp_directory	            = "temp";
const std::string ext_ffmpeg_directory	        = "ffmpeg";

const std::string ffmpeg_directory		= "ffmpeg";
const std::string sqlite_directory		= "sqlite";
const std::string comskip_directory		= "comskip";

directory_config_t::directory_config_t(const char* private_root, const char* shared_root) :
    private_root_(private_root), shared_root_(shared_root)
{
}

directory_config_t::~directory_config_t()
{
}

dvblink::filesystem_path_t directory_config_t::get_private_directory(const std::string& component)
{
    return get_directory_from_root(private_root_, component);
}

dvblink::filesystem_path_t directory_config_t::get_shared_directory(const std::string& component)
{
    return get_directory_from_root(shared_root_, component);
}

dvblink::filesystem_path_t directory_config_t::get_directory_from_root(dvblink::filesystem_path_t& root, const std::string& component)
{
    return root / component;
}

dvblink::filesystem_path_t directory_config_t::get_private_common_directory()
{
    return get_private_directory(common_component_name);
}

dvblink::filesystem_path_t directory_config_t::get_shared_common_directory()
{
    return get_shared_directory(common_component_name);
}

dvblink::filesystem_path_t directory_config_t::get_config_path()
{
    //private/config
    return get_private_directory(config_component_name);
}

dvblink::filesystem_path_t directory_config_t::get_temp_path()
{
    //shared/temp
    return get_shared_directory(temp_directory);
}

dvblink::filesystem_path_t directory_config_t::get_product_info_directory()
{
    return get_private_common_directory() / product_info_directory;
}

dvblink::filesystem_path_t directory_config_t::get_licenses_directory()
{
    return get_shared_directory(licenses_directory);
}

dvblink::filesystem_path_t directory_config_t::get_ext_ffmpeg_directory()
{
    return get_shared_directory(ext_ffmpeg_directory);
}

dvblink::filesystem_path_t directory_config_t::get_scanners_directory()
{
    return get_shared_directory(scanners_directory);
}

dvblink::filesystem_path_t directory_config_t::get_log_filepath()
{
    return shared_root_ / log_file_name;
}

dvblink::filesystem_path_t directory_config_t::get_settings_filepath()
{
    return get_config_path() / settings_file_name;
}

dvblink::filesystem_path_t directory_config_t::get_transcoder_config_filepath()
{
    return get_config_path() / transcoder_config_file_name;
}

#ifdef WIN32
	#define COMSKIP_EXE_NAME L"comskip.exe"
	#define SQLITE_EXE_NAME L"sqlite.exe"
	#define FFMPEG_EXE_NAME L"ffmpeg.exe"
	#define FFPROBE_EXE_NAME L"ffprobe.exe"
#else
	#define COMSKIP_EXE_NAME L"comskip"
	#define SQLITE_EXE_NAME L"sqlite"
	#define FFMPEG_EXE_NAME L"ffmpeg"
	#define FFPROBE_EXE_NAME L"ffprobe"
#endif

static const std::string xmltv_category_defs_file_name = "xmltv_categorymap.xml";

void directory_config_t::get_sqlite_file_info(dvblink::filesystem_path_t& file, dvblink::filesystem_path_t& dir)
{
    dir = get_private_common_directory() / sqlite_directory;
    file = dir / SQLITE_EXE_NAME;
}

void directory_config_t::get_ffmpeg_file_info(dvblink::filesystem_path_t& file, dvblink::filesystem_path_t& dir)
{
    dir = get_private_common_directory() / ffmpeg_directory;
    file = dir / FFMPEG_EXE_NAME;
}

void directory_config_t::get_ffprobe_file_info(dvblink::filesystem_path_t& file, dvblink::filesystem_path_t& dir)
{
    dir = get_private_common_directory() / ffmpeg_directory;
    file = dir / FFPROBE_EXE_NAME;
}

void directory_config_t::get_comskip_file_info(dvblink::filesystem_path_t& file, dvblink::filesystem_path_t& dir)
{
    dir = get_private_common_directory() / comskip_directory;
    file = dir / COMSKIP_EXE_NAME;
}

void directory_config_t::get_xmltv_cat_defs_file_info(dvblink::filesystem_path_t& file, dvblink::filesystem_path_t& dir)
{
    dir = get_shared_common_directory();
    file = dir / xmltv_category_defs_file_name;
}

}