/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/uuid/uuid.hpp>
#include <dli_base.h>
#include <dli_logger.h>

namespace dvblink {

class ilogger;
typedef boost::shared_ptr<ilogger> ilogger_t;

class ilogger : public logging::i_logger
{
public:
    ilogger() : uid_(engine::uuid::gen_uuid()) {};
    ~ilogger() {};

    const boost::uuids::uuid& __stdcall get_uid() {return uid_.get();}

    i_result __stdcall query_interface(const base_id_t& requestor_id, const i_guid& iid, i_base_object_t& obj);
    void __stdcall log_message(logging::e_log_level log_level, const wchar_t* log_str) const;

private:
    base_id_t uid_;
};

} //dvblink
