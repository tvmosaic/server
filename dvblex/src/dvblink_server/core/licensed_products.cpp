/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_file_procedures.h>
#include <dl_pugixml_helper.h>
#include <dl_language_settings.h>
#include "licensed_products.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink::messaging;
using namespace dvblink::pugixml_helpers;

namespace dvblex { 

licensed_products_t::licensed_products_t(directory_config_t& config) :
    config_(config)
{
}

licensed_products_t::~licensed_products_t()
{
}

bool licensed_products_t::init(dvblink::messaging::message_queue_t message_queue)
{
    message_queue_ = message_queue;
    message_handler_ = std::auto_ptr<message_handler>(new message_handler(this, message_queue_));

    return true;
}

void licensed_products_t::term()
{
}

void licensed_products_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::license::get_installed_products_request& request, dvblink::messaging::license::get_installed_products_response& response)
{
    get_installed_products(response.products_);
    response.result_ = true;
}

bool licensed_products_t::find_product(const dvblink::product_id_t& product_id, product_info_t& product_info)
{
    bool ret_val = false;

    product_info_list_t products_info;
    read_installed_products(products_info);
    for (size_t i=0; i<products_info.size(); i++)
    {
        if (products_info[i].id_ == product_id)
        {
            product_info = products_info[i];
            ret_val = true;
            break;
        }
    }

    return ret_val;
}

void licensed_products_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::license::activate_product_request& request, dvblink::messaging::license::activate_product_response& response)
{
    response.result_ = eapr_error;
    log_error(L"licensed_products_t::handle activate_product_request. Product activation is not implemented in the CE version");
}

void licensed_products_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::license::activate_product_trial_request& request, dvblink::messaging::license::activate_product_trial_response& response)
{
    response.result_ = eapr_error;
    log_error(L"licensed_products_t::handle activate_product_trial_request. Product trial is not implemented in the CE version");
}

static bool product_sorter_function(const product_info_t& first_item, const product_info_t& second_item)
{
	return first_item.display_priority_ < second_item.display_priority_;
}

void licensed_products_t::get_installed_products(product_info_list_t& products_info)
{
    read_installed_products(products_info);

    for (size_t i = 0; i < products_info.size(); i++)
    {
        //CE version only supports FREE products
        products_info[i].license_state_ = lse_free;
    }

    //sort product vector on display priorities
    std::sort(products_info.begin(), products_info.end(), product_sorter_function);
}

void licensed_products_t::read_installed_products(product_info_list_t& product_list)
{
    product_list.clear();

    dvblink::filesystem_path_t product_info_dir = config_.get_product_info_directory();
    //enum all xml files in product info directory
	std::vector<boost::filesystem::path> xml_files;
    dvblink::engine::filesystem::find_files(product_info_dir.to_boost_filesystem(), xml_files, L".xml");

	for (size_t i = 0; i < xml_files.size(); i++)
	{
        dvblink::filesystem_path_t product_info_file_path(xml_files[i]);
        product_info_t product_info;
        if (process_xml_product_file(product_info_file_path, product_info))
        {
            product_info.hw_fingerprint_ = "0x123456789";
            product_list.push_back(product_info);
        }
	}
}

static const std::string product_info_root_node                 = "product_info";
static const std::string product_info_id_node                 = "id";
static const std::string product_info_version_node                 = "version";
static const std::string product_info_revision_node                 = "revision";
static const std::string product_info_name_node                 = "name";
static const std::string product_info_url_node                 = "url";
static const std::string product_info_requires_registartion_node                 = "requires_registration";
static const std::string product_info_requires_subscription_node                 = "requires_subscription";
static const std::string product_info_requires_coupon_node                 = "requires_coupon";
static const std::string product_info_product_type_node                 = "product_type";
static const std::string product_info_trial_available_node                 = "trial_available";
static const std::string product_info_display_priority_node                = "priority";

bool licensed_products_t::process_xml_product_file(const dvblink::filesystem_path_t& product_info_file_path, product_info_t& product_info)
{
    bool res = false;

    pugi::xml_document doc;
    if (doc.load_file(product_info_file_path.to_string().c_str()).status == pugi::status_ok)
	{
		//Get root node
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
		{
            if (boost::iequals(root_node.name(), product_info_root_node))
            {
                std::string str;
                if (get_node_value(root_node, product_info_id_node, str))
                    product_info.id_ = str;
                if (get_node_value(root_node, product_info_version_node, str))
                    product_info.version_ = str;
                if (get_node_value(root_node, product_info_revision_node, str))
                    product_info.revision_ = str;
                if (get_node_value(root_node, product_info_name_node, str))
                    product_info.name_ = str;
                if (get_node_value(root_node, product_info_url_node, str))
                    product_info.url_ = str;
                if (get_node_value(root_node, product_info_product_type_node, str))
                    product_info.product_type_ = str;
                if (get_node_value(root_node, product_info_display_priority_node, str))
                    string_conv::apply(str.c_str(), product_info.display_priority_, product_info.display_priority_);
                if (root_node.child(product_info_trial_available_node.c_str()) != NULL)
                    product_info.trial_available_ = true;
                if (root_node.child(product_info_requires_registartion_node.c_str()) != NULL)
                    product_info.requires_registration_ = true;
                if (root_node.child(product_info_requires_subscription_node.c_str()) != NULL)
                    product_info.requires_subscription_ = true;
                if (root_node.child(product_info_requires_coupon_node.c_str()) != NULL)
                    product_info.requires_coupon_ = true;
                
                res = true;
            }
		}
	}
	return res;
}

dvblink::product_id_t licensed_products_t::get_server_product_id()
{
    dvblink::product_id_t id;

    //CE version only has one product being the server
    product_info_list_t products_info;
    read_installed_products(products_info);

    if (products_info.size() > 0)
        id = products_info[0].id_;
    else
        log_error(L"licensed_products_t::handle get_server_product_id. No products installed?");

    return id;
}


}