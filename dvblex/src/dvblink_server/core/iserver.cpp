/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include "iserver.h"
#include "ilogger.h"
#include "broker.h"

namespace dvblink {

using namespace logging;

iserver::iserver(dvblex::broker_t* broker) :
    id_(engine::uuid::gen_uuid()), broker_(broker)
{
    logger_instance_ = share_object_safely(new ilogger());
    object_container_instance_ = base_object_container_t(new base_object_container);
    messaging_core_instance_ = share_object_safely(new messaging::messaging_core());
    messaging_core_instance_->start();
}

iserver::~iserver()
{
    object_container_instance_->shutdown();
    messaging_core_instance_->shutdown();
}

i_result iserver::query_interface(const base_id_t& requestor_id, const i_guid& iid, i_base_object_t& obj)
{
    i_result res = i_success;
    if (iid == logger_interface)
    {
        obj = logger_instance_;
    }
    else
    {
        res = i_error;
    }
    return res;
}

i_result iserver::query_object_interface(const base_id_t& requestor_id, const base_id_t& parent_object_id, const i_guid& queried_iid, i_base_object_t& queried_obj)
{
    i_result res = i_not_found;
    i_base_object_t parent;
    if (object_container_instance_->get(parent_object_id, parent))
    {
        res = parent->query_interface(requestor_id, queried_iid, queried_obj);
    }
    return res;
}

void iserver::register_queue(boost::shared_ptr<messaging::i_client_message_queue> queue)
{
    queue->init(messaging_core_instance_);
    messaging_core_instance_->register_queue(queue);
}

void iserver::unregister_queue(const message_queue_id_t& id)
{
    messaging_core_instance_->unregister_queue(id);
}

void iserver::register_object(const i_base_object_t& object)
{
    object_container_instance_->add(object);
}

void iserver::unregister_object(const base_id_t& object_id)
{
    object_container_instance_->remove(object_id);
}

} //dvblink
