
import { expect } from 'chai'

function incNumber (number) {
    let unit = number % 1 ?
        String(number).split('.').map(v => parseInt(v))
        : [number]
    ++unit[unit.length-1]
    return unit.join('.')
}


function incNumbers (numbers, value) {
    let last = String(value);
    return numbers.map((current) => {
        return {
            num: last === current.num ? (last = incNumber(last)) : current.num,
        }
    })
}

let getNums = num => num.split('.')

function sortByBumber (a, b) {
    const [an, asn] = getNums(a.num)
    const [bn, bsn] = getNums(b.num)
    return an - bn || asn - bsn
}

let makeNumObject = (array) => array.map(n => ({num: String(n)}))

describe('inc channels number', () => {
    it('integer', () => {
        expect(incNumber('1')).to.equal('2')
    })
    it('float', () => {
        expect(incNumber('1.1')).to.equal('1.2')
    })
    it('great', () => {
        expect(incNumber('1.98')).to.equal('1.99')
    })
    it('not float', () => {
        expect(incNumber('10.9')).to.equal('10.10')
    })
    it('not float 2', () => {
        expect(incNumber('1.10')).to.equal('1.11')
    })
    it('not float great', () => {
        expect(incNumber('1.99')).to.equal('1.100')
    })
})

describe('sort channel numbers', () => {

    it('simple', () => {
        expect(incNumbers(
            makeNumObject(['10.1', '2', '10.10', '11', '10.9', '3']).sort(sortByBumber)
        )).to.deep.equal(
            makeNumObject(['2', '3', '10.1', '10.9', '10.10', '11'])
        )
    })

})

describe('inc channel numbers', () => {
    it('simple', () => {
        expect(incNumbers(
            makeNumObject([1, 2, 10, 11, 12, 20, 21]), 10
        )).to.deep.equal(
            makeNumObject([1, 2, 11, 12, 13, 20, 21])
        )
    })
    it('with continuation', () => {
        expect(incNumbers(
            makeNumObject([1, 2, 10, 11, 12, 13, 15]), 10
        )).to.deep.equal(
            makeNumObject([1, 2, 11, 12, 13, 14, 15])
        )
    })
    it('float', () => {
        expect(incNumbers(
            makeNumObject([1, 2, 10.1, 10.2, 10.3, 10.7, 11]), 10.2
        )).to.deep.equal(
            makeNumObject([1, 2, 10.1, 10.3, 10.4, 10.7, 11])
        )
    })
    it('not float', () => {
        expect(incNumbers(
            makeNumObject([1, 2, 10.8, 10.9, '10.10', 10.11, 11]), 10.9
        )).to.deep.equal(
            makeNumObject([1, 2, 10.8, '10.10', 10.11, 10.12, 11])
        )
    })
})
