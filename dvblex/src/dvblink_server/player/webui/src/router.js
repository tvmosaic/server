
import Vue from 'vue'
import Router from 'vue-router'

import Startup from './views/Startup.vue'
import Products from './views/Products.vue'
import About from './views/About.vue'
import Monitoring from './views/Monitoring.vue'

import Channels from './views/Channels/Channels.vue'
import ChannelsScan from './views/Channels/ChannelsScan.vue'
import ChannelsVisibility from './views/Channels/ChannelsVisibility.vue'
import ChannelsFavorites from './views/Channels/ChannelsFavorites.vue'
import ChannelsProperties from './views/Channels/ChannelsProperties.vue'
import ChannelsLogos from './views/Channels/ChannelsLogos.vue'

import EPG from './views/EPG/EPG.vue'
import EPGSources from './views/EPG/EPGSources.vue'
import EPGChannelMapping from './views/EPG/EPGChannelMapping.vue'

import Settings from './views/Settings/Settings.vue'
import SettingsBackup from './views/Settings/SettingsBackup.vue'
import SettingsGeneral from './views/Settings/SettingsGeneral.vue'
import SettingsNetwork from './views/Settings/SettingsNetwork.vue'
import SettingsRecorder from './views/Settings/SettingsRecorder.vue'
import SettingsTranscoding from './views/Settings/SettingsTranscoding.vue'
import SettingsSendTo from './views/Settings/SettingsSendTo.vue'

import Library from './views/Connect/Library/Library.vue'
import TVRecordings from './views/Connect/TVRecordings/TVRecordings.vue'
import Schedules from './views/Connect/Schedules/Schedules.vue'

import Connect from './views/Connect/Connect.vue'
import TVGuide from './views/Connect/TVGuide/TVGuide.vue'
import Search from './views/Connect/Search/Search.vue'


Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'startup',
            component: Startup,
        }, {
            path: '/products',
            name: 'products',
            component: Products,
        }, {
            path: '/channels',
            name: 'channels',
            component: Channels,
            beforeEnter: (to, from, next) => {
                next(to.name === 'channels' ? {
                    name: 'channels-scan',
                } : {})
            },
            children: [{
                path: 'scan',
                name: 'channels-scan',
                component: ChannelsScan,
            }, {
                path: 'visibility',
                name: 'channels-visibility',
                component: ChannelsVisibility,
            }, {
                path: 'favorites',
                name: 'channels-favorites',
                component: ChannelsFavorites,
            }, {
                path: 'properties',
                name: 'channels-properties',
                component: ChannelsProperties,
            }, {
                path: 'logos',
                name: 'channels-logos',
                component: ChannelsLogos,
            }]
        }, {
            path: '/settings',
            name: 'settings',
            component: Settings,
            beforeEnter: (to, from, next) => {
                next(to.name === 'settings' ? {
                    name: 'settings-general',
                } : {})
            },
            children: [{
                path: 'general',
                name: 'settings-general',
                component: SettingsGeneral,
            }, {
                path: 'network',
                name: 'settings-network',
                component: SettingsNetwork
            }, {
                path: 'transcoding',
                name: 'settings-transcoding',
                component: SettingsTranscoding
            }, {
                path: 'backup',
                name: 'settings-backup',
                component: SettingsBackup
            }, {
                path: 'recorder',
                name: 'settings-recorder',
                component: SettingsRecorder
            }, {
                path: 'sendto',
                name: 'settings-sendto',
                component: SettingsSendTo
            }]
        }, {
            path: '/epg',
            name: 'epg',
            component: EPG,
            beforeEnter: (to, from, next) => {
                next(to.name === 'epg' ? {
                    name: 'epg-mapping',
                } : {})
            },
            children: [{
                path: 'mapping',
                name: 'epg-mapping',
                component: EPGChannelMapping,
            }, {
                path: 'sources',
                name: 'epg-sources',
                component: EPGSources,
            }]
        }, {
            path: '/about',
            name: 'about',
            component: About,
        }, {
            path: '/monitoring',
            name: 'monitoring',
            component: Monitoring,
        }, {
            path: '/connect',
            name: 'connect',
            component: Connect,
            beforeEnter: (to, from, next) => {
                next(to.name === 'connect' ? {
                    name: 'connect-tvguide',
                } : {})
            },
            children: [{
                path: '/tvrecordings',
                name: 'connect-tvrecordings',
                component: TVRecordings,
            }, {
                path: '/library',
                name: 'library',
                component: Library,
            }, {
                path: '/schedules',
                name: 'connect-schedules',
                component: Schedules,
            }, {
                path: '/tvguide',
                name: 'connect-tvguide',
                component: TVGuide,
            }, {
                path: '/search',
                name: 'connect-search',
                component: Search,
                props: (route) => ({
                    query: route.query.query,
                    channel: route.query.channel,
                    genres: route.query.genres,
                }),
            }],
        },
    ]
})
