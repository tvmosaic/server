
(function (Backbone) {

    var App = window.App || (window.App = {});
    var Connect = App.Connect || (App.Connect = {});
    var Views = Connect.Views || (Connect.Views = {});

    // xprogramsline
    Views.ProgramsLine = Backbone.View.extend({
        tagName: 'ul',
        className: 'programs-line nav nav-list clearfix',

        PIXELS_INTO_MINUTE: 5,
        BORDER_WIDTH: 0,
        PADDING: 2,

        events: {
            'change .program-selector input': '_changeProgramSelect'
        },
        _changeProgramSelect: function (event) {
            var $checkbox = $(event.currentTarget);
            var programId = $checkbox.closest('li').attr('id');
            var currentProgram = this.model.findWhere({program_id: programId});
            currentProgram.set('selected', $checkbox.prop('checked'));
            this.trigger('program-select', $checkbox.prop('checked'), currentProgram);
        },
        changeStartTime: function (time, isSelectedLine) {
            var selectedProgram;
            if (isSelectedLine) {
                selectedProgram = this.getSelectedProgram();
            }
            this.lineStartTime = time;

            var needRebuild = time.isAfter(this.lineTimeBounds.start);
            if (needRebuild) {
                this.currentStartTime = time;
            }
            var newFirstProgram = this.model.getByTime(time.unix());
            var index = this.model.indexOf(newFirstProgram);
            if (this.selectedProgram > 0) {
                this.selectedProgram -= index;
            }

            if (needRebuild) {
                this.render();
                if (isSelectedLine) {
                    var programStartTime = moment.unix(selectedProgram.get('start_time'));
                    var programEndTime = moment(programStartTime).add({
                        minutes: selectedProgram.get('duration') / 60
                    });
                    if (!programEndTime.isAfter(this.lineTimeBounds.start)) {
                        this.selectByTime(this.lineTimeBounds.start.unix());
                    } else {
                        this.select(selectedProgram);
                    }
                }
            }

            return needRebuild;
        },
        initialize: function (options) {
            this.parent = options.parent;
            this.lineStartTime = options.lineStartTime;
            this.lineEndTime = options.lineEndTime;
            this.currentStartTime = options.currentStartTime;
            this.lineTimeBounds = {};
            this.selectedProgram = 0;
            this.lineDuration = 0;
            this.addProgramCallback = options.addProgramCallback || _.noop;

            // We cut off the past programs.
            var firstProgram = this.model.getByTime(this.lineStartTime.unix());
            var firstProgramIndex = this.model.indexOf(firstProgram);
            if (firstProgramIndex > 0) {
                this.model.reset(this.model.drop(firstProgramIndex));
            }

            this.model.bind('change', function () {
                this.currentStartTime = this.lineTimeBounds.start;
                this.render();
            }, this);
        },
        render: function () {
            return this.build(moment(this.currentStartTime));
        },
        highlightSelectedProgram: function() {
            var id = this.model.models[this.selectedProgram].get('program_id');
            var self = this;
            _.defer(function () {
                $('li.current').removeClass('current');
                self.$('li#'+id).addClass('current');
            });
            return this;
        },
        move: function (startTime) {
            if (!startTime) {
                startTime = this.lineTimeBounds.start;
            }
            return this.build(startTime);
        },
        build: function (startTime) {
            this.$el.empty();

            var addBaseProgram = _.bind(function (program, classes, width) {
                var name = program.get('name');

                var $li = $('<li>')
                    .addClass('dbl-program-item')
                    .attr('id', program.get('program_id'))
                    .css({width: width+'px'})
                    .attr('title', name);

                // A very short program. We remove the indentation, because with it, the final width will be more than necessary.
                if (width-this.PIXELS_INTO_MINUTE < this.PIXELS_INTO_MINUTE) {
                    $li.css({'padding-left': '0'});
                }

                var genres = program.get('genres');
                if (genres.length) {
                    $li.addClass('dbl-genres');
                    _.each(genres, function (genre) {
                        $li.addClass('dbl-genre-'+genre.toLocaleLowerCase());
                    });
                }

                var $statesSecond = $('<span>')
                    .addClass('dbl-states-second')
                    .appendTo($li);

                $('<div>')
                    .text(name)
                    .addClass('dbl-name')
                    .appendTo($li);

                var programStates = [];
                if (program.get('is_record_conflict') || program.get('is_conflicting')) {
                    programStates.push('dbl-conflict');
                }
                if (program.get('is_record')) {
                    programStates.push('dbl-record');
                }
                if (program.get('hdtv')) {
                    programStates.push('dbl-hdtv');
                }
                var $states = $('<span>')
                    .addClass('dbl-states')
                    .appendTo($li);
                _.each(programStates, function (state) {
                    $states.append(
                        $('<span>')
                            .addClass('dbl-state')
                            .addClass(state)
                    );
                });

                var start = program.get('start');
                var end = program.get('end');
                if (start && end) {
                    var text = start.format(Connect.DateFormatters.time) + ' - ' + end.format(Connect.DateFormatters.time);
                    $('<div>')
                        .text(text)
                        .attr('title', text)
                        .addClass('dbl-time')
                        .appendTo($li);
                }

                var programSelected = program.get('selected');
                $statesSecond.append($('<span class="program-selector" />').append(
                    $('<input type="checkbox" />')
                        .attr('checked', programSelected)
                ));
                if (programSelected) {
                    $statesSecond.append($('<span class="selected"/>'));
                    $li.addClass('selected');
                }

                this.addProgramCallback('tvguide', 'addProgram', this, [
                    program, $li, $states, $statesSecond
                ]);

                this.$el.append($li);
            }, this);

            var alignedTime = moment(startTime).startOf('hours');
            if (startTime.minutes() > 29) {
                alignedTime.minutes(30);
            }

            var firstProgram = this.model.getByTime(startTime.unix());
            var firstProgramIndex = this.model.indexOf(firstProgram);
            if (!firstProgram) {
                firstProgramIndex = 0;
            }
            var programs = this.model.models;
            programs = _.drop(programs, firstProgramIndex);
            firstProgram = _.first(programs);

            var firstProgramStartTime = moment.unix(firstProgram.get('start_time'));
            var firstProgramOffset = alignedTime.diff(firstProgramStartTime, 'minutes');

            this.lineTimeBounds.start = alignedTime;

            var parentWidth = this.parent.width();
            var remainderTime = Math.floor(parentWidth / this.PIXELS_INTO_MINUTE);
            var program, duration, screenDuration, width, classes = [], i, l;

            this.lineDuration = remainderTime;

            for (i=0,l=programs.length; i<l && remainderTime > 0; ++i) {
                program = programs[i];

                duration = Math.ceil(program.get('duration') / 60);
                screenDuration = duration;
                classes = [];

                if (i === 0) {
                    // Adjustment of the duration of the first program, taking into account the beginning.
                    screenDuration -= firstProgramOffset;
                    if (firstProgramOffset > 0) {
                        classes.push('lt');
                    }
                }

                // Taking into account the current line width of the programs.
                if (remainderTime-screenDuration >= 0) {
                    remainderTime -= screenDuration;
                } else {
                    screenDuration = remainderTime;
                    remainderTime = 0;
                    classes.push('gt');
                }

                width = screenDuration * this.PIXELS_INTO_MINUTE;
                // Taking into account the padding of the element.
                width = width - this.BORDER_WIDTH - this.PADDING;
                width = Math.max(width, 0);

                addBaseProgram(program, classes, width);
            }
            if (program) {
                this.lineTimeBounds.end = moment.unix(program.get('start_time')).add({
                    minutes: screenDuration
                });
            }

            return this;
        },


        /*
         * Returns the selected program on the current line.
         */
        getSelectedProgram: function () {
            return this.model.models[this.selectedProgram];
        },

        select: function (program) {
            this.selectByIndex(this.model.indexOf(program));
            return program;
        },

        selectByTime: function (time) {
            time = moment.unix(time);
            // The time of the requested transfer is earlier than the current line boundaries.
            if (time.isBefore(this.lineTimeBounds.start)) {
                time = this.lineTimeBounds.start;
            }
            var program = this.model.getByTime(time.unix()+1);
            if (!program) {
                // Programs may not yet be available at this time.
                // Select the first one.
                this.selectByIndex(0);
                return false;
            }
            this.select(program);
            return program;
        },

        selectByIndex: function (index) {
            if (index < 0) {
                return false;
            }
            this.selectedProgram = index;
            var program = this.model.models[this.selectedProgram];
            if (!program) {
                return false;
            }
            var self = this;
            _.defer(function () {
                self.$('li#'+program.get('program_id')).addClass('current');
            });
            return program;
        },

        /*
         * The program started within the current bounds.
         */
        _isProgramStartInTimeBounds: function (program) {
            var programStartTime = moment.unix(program.get('start_time'));
            return (programStartTime.isSame(this.lineTimeBounds.start) ||
                    programStartTime.isAfter(this.lineTimeBounds.start)) &&
                programStartTime.isBefore(this.lineTimeBounds.end);
        },

        /*
         * The program ended within the current time bounds.
         */
        _isProgramEndInTimeBounds: function (program) {
            var programStartTime = moment.unix(program.get('start_time'));
            var programEndTime = moment(programStartTime).add({
                minutes: program.get('duration') / 60
            });
            return programEndTime.isBefore(this.lineTimeBounds.end) ||
                programEndTime.isSame(this.lineTimeBounds.end);
        },

        /*
         * The program started right on the bounds.
         */
        _isProgramStartOnStartTimeBounds: function (program) {
            var programStartTime = moment.unix(program.get('start_time'));
            return programStartTime.isSame(this.lineTimeBounds.start);
        },

        moveCursorRight: function () {
            this.$('li.current').removeClass('current');

            var programs = this.model.models;
            var program = programs[this.selectedProgram];
            var newLineTimeBoundsStart = null;

            if (this._isProgramEndInTimeBounds(program)) {
                // Checking the next gear.
                if (!this._isProgramStartInTimeBounds(programs[this.selectedProgram+1])) {
                    // The next program started overseas. Move the screen and move the cursor.
                    this.lineTimeBounds.start.add({minutes: 30});
                    this.lineTimeBounds.end.add({minutes: 30});
                    newLineTimeBoundsStart = this.lineTimeBounds.start;
                } else {
                    // The next transmission began within the boundaries. We just move the cursor to the next gear.
                }
                ++this.selectedProgram;
            } else {
                // This program goes abroad. Move the screen and leave the cursor on the current gear.
                this.lineTimeBounds.start.add({minutes: 30});
                this.lineTimeBounds.end.add({minutes: 30});
                newLineTimeBoundsStart = this.lineTimeBounds.start;
            }

            return newLineTimeBoundsStart;
        },

        moveCursorLeft: function () {
            this.$('li.current').removeClass('current');

            var programs = this.model.models;
            var program = programs[this.selectedProgram];
            var newLineTimeBoundsStart = null;

            // The first transmission started before the border.
            var lineStartedBeforeBound = this.lineStartTime.isBefore(this.lineTimeBounds.start);

            if (this._isProgramStartInTimeBounds(program)) {
                if (this._isProgramStartOnStartTimeBounds(program)) {
                    if (!lineStartedBeforeBound) {
                        return null;
                    }
                    // This transfer began exactly at the border. Move the screen and move the cursor.
                    this.lineTimeBounds.start.add({minutes: -30});
                    this.lineTimeBounds.end.add({minutes: -30});
                    newLineTimeBoundsStart = this.lineTimeBounds.start;
                } else {
                    // This transmission is within borders. We just translate the cursor.
                }

                if (this.selectedProgram > 0) {
                    --this.selectedProgram;
                }
            } else {
                if (!lineStartedBeforeBound) {
                    return null;
                }
                // This program goes abroad. Move the screen and leave the cursor on the current gear.
                this.lineTimeBounds.start.add({minutes: -30});
                this.lineTimeBounds.end.add({minutes: -30});
                newLineTimeBoundsStart = this.lineTimeBounds.start;
            }

            return newLineTimeBoundsStart;
        },
        movePageRight: function (duration) {
            this.$('li.current').removeClass('current');
            var calcEnd = moment(this.lineTimeBounds.start);
            // Find the end of the scale from the beginning, rather than using this.lineTimeBounds.end, since this.lineTimeBounds.end is not set for Unknown.
            calcEnd.add({minutes: this.lineDuration});
            calcEnd.add({minutes: duration});
            this.lineTimeBounds.start.add({minutes: duration});
            this.lineTimeBounds.end.add({minutes: duration});
            return this.lineTimeBounds.start;
        },
        movePageLeft: function (duration) {
            this.$('li.current').removeClass('current');
            var calcStart = moment(this.lineTimeBounds.start);
            calcStart.add({minutes: -1 * duration}).add({minutes: 30});
            if(calcStart.isBefore(this.lineStartTime)) {
                duration = (this.lineTimeBounds.start.unix() - this.lineStartTime.unix()) / 60;
            }
            this.lineTimeBounds.start.add({minutes: -1 * duration});
            this.lineTimeBounds.end.add({minutes: -1 * duration});
            return this.lineTimeBounds.start;
        },

        _selectProgram: function (pid) {
            var currentProgram = _.find(this.model.models, function (program) {
                return program.get('program_id') === pid;
            }, this);
            if (!currentProgram) {
                return;
            }
            this.selectedProgram = _.indexOf(this.model.models, currentProgram);
            this.highlightSelectedProgram();
        }
    });
    // / xprogramsline

}).call(this, Backbone);
