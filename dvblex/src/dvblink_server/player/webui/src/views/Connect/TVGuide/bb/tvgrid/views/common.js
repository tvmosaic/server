
(function (Backbone) {

    var App = window.App || (window.App = {});
    var Connect = App.Connect || (App.Connect = {});
    var Views = Connect.Views || (Connect.Views = {});

    // xscrollbar
    Views.SimpleScrollbar = Backbone.View.extend({
        className: 'scrollbar',
        initialize: function () {
            this.$bit = $('<div class="bit"></div>').appendTo(this.$el);
            this._hideTimer = null;
        },
        render: function () {
            this.$el.append();
            return this;
        },

        reset: function () {
            this.$el.addClass('hidden');
            this.$bit.css({
                height: 0,
                top: 0
            });
        },
        update: function (currentElementIndex, elementOnPage, totalElements) {
            this.$el.removeClass('slow');
            if (this._hideTimer) {
                window.clearTimeout(this._hideTimer);
                this._hideTimer = null;
            }
            var multiplier = elementOnPage / totalElements;
            if (multiplier < 1) {
                var height = this.$el.height();
                var bitHeight = height * multiplier;
                var bitOffset = (height - bitHeight) / (totalElements - 1) * currentElementIndex;
                this.$bit.css({
                    height: bitHeight,
                    top: bitOffset
                });
                this.$el.removeClass('hidden');
            } else {
                this.$el.addClass('hidden');
            }
            this._hideTimer = window.setTimeout(_.bind(function () {
                this.$el.addClass('hidden slow');
                this._hideTimer = null;
            }, this), 1000);
        }
    });
    // / xscrollbar


    // xtimeline
    Views.Timeline = Backbone.View.extend({
        className: 'dbl-timeline',
        template: '<span class="dbl-date"></span><ul class="dbl-time list-inline"></ul>',
        initialize: function (options) {
            this.$el.html(this.template);
            this.$time = this.$('.dbl-time');
            this.$date = this.$('.dbl-date');
            this.startTime = options.startTime;
            this.lastStartTime = 0;
            this._programLineWidth = 0;
        },
        resize: function (width) {
            this._programLineWidth = width;
            this.render();
        },
        render: function () {
            var width = this._programLineWidth;
            if (width < 1) {
                return this;
            }

            var MIN_WIDTH_FOR_TIME_LABEL = 70;
            var ONE_TICK_WIDTH = 148;
            var tickNum = Math.ceil(width / (ONE_TICK_WIDTH + 2));
            var lastLabelWidth = width;

            if (tickNum > 1) {
                lastLabelWidth = width % (ONE_TICK_WIDTH + 2);
                if (lastLabelWidth != 0 && lastLabelWidth < MIN_WIDTH_FOR_TIME_LABEL) {
                    lastLabelWidth += ONE_TICK_WIDTH;
                    tickNum--;
                }
            }

            var next = moment(this.startTime).startOf('hours').add({minutes: 30});
            this.$time.empty();
            var lastLabel = this.addTimestamp(
                (this.lastStartTime ? this.lastStartTime : this.startTime).format(Connect.DateFormatters.time)
            );
            for (var i=0; i<tickNum-1; ++i) {
                lastLabel = this.addTimestamp(next.add({minutes: 30}).format(Connect.DateFormatters.time));
            }
            if (lastLabelWidth) {
                lastLabel.css({ width: lastLabelWidth + 'px' });
            }

            return this;
        },
        addTimestamp: function (label) {
            return $('<li class="dbl-timestamp">' + label + '</li>')
                .appendTo(this.$time);
        },
        moveTo: function (startTime) {
            var now = moment();
            if (startTime.isBefore(now)) {
                startTime = now;
            }
            var timestampElements = this.$time.find('.dbl-timestamp');
            var alignedTime = moment(startTime).startOf('hours');
            if (startTime.minutes() > 29) {
                alignedTime.minutes(30);
            }
            $(_.first(timestampElements)).text(startTime.format(Connect.DateFormatters.time));
            _.each(_.drop(timestampElements, 1), function (el) {
                $(el).text(alignedTime.add({minutes: 30}).format(Connect.DateFormatters.time));
            });
            this.updateDateField(startTime);
            this.lastStartTime = startTime;
        },
        updateDateField: function (startTime) {
            this.$date.text(startTime.format(Connect.DateFormatters.date));
        }
    });
    // / xtimeline

}).call(this, Backbone);
