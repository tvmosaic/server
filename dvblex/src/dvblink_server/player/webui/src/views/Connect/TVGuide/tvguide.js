
let App = window.App || (window.App = {});
let Connect = App.Connect || (App.Connect = {});

import ConnectAPI from '@/api/connect'

import Hammer from 'hammerjs'

export default class TVGuide {

    constructor ($el) {
        this.events = []
        this.$el = $el

        this.init()
    }

    on (event, callback) {
        if (!this.events[event]) {
            this.events[event] = []
        }
        this.events[event].push(callback)
        return this
    }
    emit (event, data) {
        const callbacks = this.events[event]
        if (callbacks) {
            callbacks.forEach(callback => callback(data))
        }
    }

    init () {
        this.tvguide = new Connect.Views.TVGuide({
            channelsSync: async function (options, success) {
                const channels = await ConnectAPI.TVGuide.getChannels()
                success(channels)
            },
            programsSync: async function (data, success) {
                try {
                    const programs = await ConnectAPI.TVGuide.getPrograms(data)
                    success(programs)
                } catch (err) {
                    // eslint-disable-next-line
                    console.error('fetch programs', err);
                    alert('Internal error. Please check log files for details.');
                }
            },
            spinnerShow: function () {
                App.Views.Spinner.show();
            },
            spinnerHide: function () {
                App.Views.Spinner.hide();
            },
            errorShow: function (message) {
                App.Views.Spinner.hide();
                alert(message);
            },
            addProgramCallback () {
            },
            channelClickCallback: (cid) => {
                this.emit('channel-click', cid)
            }
        })
            .on('ready', () => {
                this.createHandlers(App, this.tvguide);
                $(window).trigger('resize.app');
                this.emit('ready')
            })
            .on('show-goto-modal', () => {
                this.emit('show-goto-modal')
            });
        this.$el.html(this.tvguide.render().el);
    }

    createHandlers (App, tvguide) {
        var tvguideElement = tvguide.$el;
        var programsListElement = $('.programs-list', tvguideElement);
        var programDialog = this.addProgramDialog(tvguide, programsListElement);

        this.addMousewhell(tvguide, tvguideElement);
        this.addKeyboardHandler(App, tvguide, programDialog);
        this.addControls(tvguide, tvguideElement);
        this.addSwipe(tvguide, tvguideElement);
    }


    addProgramDialog (tvguide, programsListElement) {
        var programDialog = new App.Connect.Views.ProgramDialog({
            model: tvguide.currentProgram,
            channel: tvguide.tvgrid.table.currentChannel,
            channels: tvguide.channels
        });
        tvguide.currentProgram.bind('change', model => {
            this.emit('change-program', model.get('program').toJSON())
        });
        $('.nav-stacked', programsListElement)
            .on('click', '.dbl-program-item', () => {
                setTimeout(() => {
                    this.emit('open-program-dialog', tvguide.currentProgram)
                });
            });

        return programDialog;
    }

    addSwipe (tvguide, tvguideElement) {
        var tvguideHammer = new Hammer(tvguideElement.get(0));
        var swipes = {
            swiperight: function () { tvguide.tvgrid.table.moveProgramPage(-1); },
            swipeleft: function () { tvguide.tvgrid.table.moveProgramPage(1); },
            swipeup: function () { tvguide.tvgrid.table.pageDown(); },
            swipedown: function () { tvguide.tvgrid.table.pageUp(); }
        };
        var tvguideHammerEvents = _.keys(swipes).join(' ');

        function setSwipe (enabled) {
            if (!enabled) {
                tvguideHammer.off(tvguideHammerEvents);
                return false;
            }

            tvguideHammer.on(tvguideHammerEvents, function (event) {
                swipes[event.type].call(this);
                event.preventDefault();
            });
            tvguideHammer.get('swipe').set({
                direction: Hammer.DIRECTION_ALL,
                velocity: 0.01
            });
        }
        setSwipe(true);

        // Disabling pull-to-refresh in android chrome.
        tvguideElement.on('touchmove', function (event) {
            event.preventDefault();
        });

        return true;
    }

    addControls (tvguide) {
        var controls = [
            { className: 'dbl-left dbl-horizontal', handler: function () { tvguide.tvgrid.table.moveProgramPage(-1); } },
            { className: 'dbl-right dbl-horizontal', handler: function () { tvguide.tvgrid.table.moveProgramPage(1); } },
            { className: 'dbl-top dbl-vertical', handler: function () { tvguide.tvgrid.table.pageUp(); } },
            { className: 'dbl-bottom dbl-vertical', handler: function () { tvguide.tvgrid.table.pageDown(); } },
        ];
        controls.forEach(prop => {
            $('<div>')
                .addClass('dbl-tvguide-control ' + prop.className)
                .on('click', function () {
                    prop.handler.call(this);
                    return false;
                })
                .appendTo($('.dbl-app-tvguide'));
        });
    }

    addKeyboardHandler (App, tvguide) {
        const ACTIONS = {
            'tvguide.showProgramDialog': 'Enter',
            'tvguide.gotoPreviousChannel': 'Up',
            'tvguide.gotoNextChannel': 'Down',
            'tvguide.gotoPreviousProgram': 'Left',
            'tvguide.gotoNextProgram': 'Right',
            'tvguide.pageUp': 'PageUp',
            'tvguide.pageDown': 'PageDown',

            'programDialog.watchIfProgramNow': 'Enter',
            'programDialog.hide': 'Escape'
        };

        App.Connect.common.Keyboard.register(tvguide, 'tvguide', 10, _.extend(
            tvguide.actions, {
                'showProgramDialog': () => {
                    this.emit('open-program-dialog')
                }
            }
        ));
        App.Connect.common.Keyboard.map(ACTIONS);
        App.Connect.common.Keyboard.focus(tvguide);
    }

    addMousewhell (tvguide, tvguideElement) {
        tvguideElement[0].addEventListener('wheel', function (event) {
            window.setTimeout(() => {
                tvguide.tvgrid.table.moveChannelCursor(event.deltaY > 0 ? 1 : -1);
            });
            event.stopPropagation();
            event.preventDefault();
        });
    }

}
