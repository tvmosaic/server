
(function (Backbone) {

    var App = window.App || (window.App = {});
    var Connect = App.Connect || (App.Connect = {});
    var Views = Connect.Views || (Connect.Views = {});

    Views.GotoTime = Backbone.View.extend({
        className: 'dbl-goto',
        template: _.template($('#goto-tmpl').html()),
        events: {
            'click .dbl-goto-btn': 'showDialog'
        },
        initialize: function (options) {
            this.table = options.table;
        },
        render: function () {
            this.$el.html(this.template());
            return this;
        },
        showDialog: function () {
            this.trigger('show-modal');
        }
    });

    // xtvgrid xgrid
    Views.TVGrid = Backbone.View.extend({
        className: 'dbl-grid',
        UPDATE_INTERVAL: 1000,
        initialize: function (options) {
            this.startTime = options.startTime;
            this.timeline = new Views.Timeline({
                startTime: this.startTime
            });
            this.table = new Views.GuideTable({
                model: this.model,
                startTime: this.startTime,
                addProgramCallback: options.addProgramCallback,
                channelClickCallback: options.channelClickCallback
            })
                .bind('change:starttime', this.changeStartTime, this)
                .bind('change:program', this.changeProgram, this)
                .bind('resize', this.onTableResize, this);

            this.gotoTime = new Views.GotoTime({
                table: this.table
            })
                .bind('show-modal', function () {
                    this.trigger('show-goto-modal')
                }, this);

            this.modelReady = false;
            this.model.bind('ready', function () {
                this.modelReady = true;
            }, this);

            this._lastUpdate = 0;
        },
        remove: function () {
            // It is necessary, because there is a mandatory signature from the event.
            this.table.remove();
            Backbone.View.prototype.remove.call(this);
        },
        render: function () {
            this.$el.append(this.timeline.render().el);
            this.$el.append(this.table.render().el);
            this.$el.append(this.gotoTime.render().el);
            return this;
        },
        changeStartTime: function (startTime) {
            this.timeline.moveTo(startTime);
        },
        changeProgram: function (program) {
            this.trigger('change:program', program);
        },
        updateTimerStart: function () {
            if (!this._updateTimer) {
                this._updateTimer = window.setInterval(_.bind(function () {
                    if (!this.modelReady) {
                        return;
                    }
                    var now = moment().seconds(0);
                    if (this._lastUpdate === now.unix()) {
                        return;
                    }
                    this.table.changeStartTime(now);
                    this._lastUpdate = now.unix();
                }, this), this.UPDATE_INTERVAL);
            }
        },
        updateTimerStop: function () {
            if (this._updateTimer) {
                window.clearInterval(this._updateTimer);
                this._updateTimer = false;
            }
        },
        onTableResize: function (width) {
            this.timeline.resize(width);
        }
    });
    // / xtvgrid xgrid

}).call(this, Backbone);
