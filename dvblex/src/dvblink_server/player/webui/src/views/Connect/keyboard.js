
export default Object.assign({
    _specialKeys: {
        8: 'backspace',
        9: 'tab',
        10: 'enter',
        13: 'enter',
        16: 'shift',
        17: 'ctrl',
        18: 'alt',
        19: 'pause',
        20: 'capslock',
        27: 'escape',
        32: 'space',
        33: 'pageup',
        34: 'pagedown',
        35: 'end',
        36: 'home',
        37: 'left',
        38: 'up',
        39: 'right',
        40: 'down',
        45: 'insert',
        46: 'delete',
        96: 'np_0',
        97: 'np_1',
        98: 'np_2',
        99: 'np_3',
        100: 'np_4',
        101: 'np_5',
        102: 'np_6',
        103: 'np_7',
        104: 'np_8',
        105: 'np_9',
        106: 'np_*',
        107: 'np_+',
        109: 'np_-',
        110: 'np_.',
        111: 'np_/',
        112: 'f1',
        113: 'f2',
        114: 'f3',
        115: 'f4',
        116: 'f5',
        117: 'f6',
        118: 'f7',
        119: 'f8',
        120: 'f9',
        121: 'f10',
        122: 'f11',
        123: 'f12',
        144: 'numlock',
        145: 'scroll'
    },
    _actions: {},
    _components: {},
    _focused: null,
    _focusStack: [],
    init: function () {
        $('body').on('keydown', _.bind(function (event) {
            this._handle(event);
        }, this));
    },
    _eventToString: function (event) {
        var special = this._specialKeys[event.which];
        var char = special || String.fromCharCode(event.which).toLowerCase();
        var modificators = _.filter(_.map(['alt', 'ctrl', 'shift'], function (modif) {
            return event[modif + 'Key'] ? modif : '';
        })).concat('').join('+');
        return modificators + char;
    },
    _handle: function (event) {
        if (_.isEmpty(this._focused)) {
            return;
        }
        var keyShotcut = this._eventToString(event);
        var action = _.find(this._actions[this._focused.name], {
            key: keyShotcut
        });
        if (!action) {
            return;
        }
        var func = this._focused.actions[action.name];
        if (_.isFunction(func)) {
            func.call(this._focused.component);
        }
    },
    register: function (component, componentName, priority, actions) {
        this._components[priority] = {
            name: componentName,
            component: component,
            actions: actions
        }
    },
    focus: function (component) {
        this._focused = _.find(this._components, function (object) {
            return object.component === component;
        });
        this._focusStack.push(this._focused);
    },
    blur: function (component) {
        this._focusStack = _.reject(this._focusStack, function (focus) {
            return focus.component === component;
        });
        this._focused = _.last(this._focusStack);
    },
    getFocusedName: function () {
        return this._focused.name;
    },
    map: function (actions) {
        this._actions = _.extend(
            this._actions, _.groupBy(_.map(actions, function (key, action) {
                var name = action.split('.');
                return {
                    subsistem: name[0],
                    name: name[1],
                    key: key.toLocaleLowerCase()
                }
            }), function (item) {
                return item.subsistem;
            })
        );
    },
    clearMap: function () {
        this._actions = {};
        return this;
    },
})
