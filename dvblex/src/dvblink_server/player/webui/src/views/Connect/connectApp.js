
let App = window.App || (window.App = {});
let Connect = App.Connect || (App.Connect = {});

Connect.Models || (Connect.Models = {});
Connect.Views || (Connect.Views = {});
Connect.Plugins || (Connect.Plugins = {});


Connect.defaults = {
    dateFormatters: {
        date: 'L',
        time: 'LT'
    },

    Keys: {
        down: 40,
        enter: 13,
        escape: 27,
        left: 37,
        pageDown: 34,
        pageUp: 33,
        right: 39,
        up: 38
    },
};


Connect.DateFormatters = Connect.defaults.dateFormatters;

Connect.Strings = {}

Connect.common = {}

import Keyboard from './keyboard.js'
Connect.common.Keyboard = Keyboard
Connect.common.Keyboard.init()

import moment from 'moment'
window.moment = moment
