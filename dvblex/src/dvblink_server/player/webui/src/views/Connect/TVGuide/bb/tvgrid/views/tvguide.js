
(function (Backbone) {

    var App = window.App || (window.App = {});
    var Connect = App.Connect || (App.Connect = {});
    var Models = Connect.Models || (Connect.Models = {});
    var Views = Connect.Views || (Connect.Views = {});

    // xtvguide xguide
    Views.TVGuide = Backbone.View.extend({
        className: 'dbl-tvguide-content',
        defaultOptions: {
            spinnerShow: _.noop,
            spinnerHide: _.noop,
            errorShow: _.noop,
            channelsUrl: '/channels/',
            programsUrl: '/programs/',
            programModel: Models.Program
        },
        actions: {
            gotoPreviousChannel: function () {
                this.tvgrid.table.moveChannelCursor(-1);
            },
            gotoNextChannel: function () {
                this.tvgrid.table.moveChannelCursor(1);
            },
            gotoPreviousProgram: function () {
                this.tvgrid.table.moveProgramCursor(-1);
            },
            gotoNextProgram: function () {
                this.tvgrid.table.moveProgramCursor(1);
            },
            pageUp: function () {
                this.tvgrid.table.pageUp();
            },
            pageDown: function () {
                this.tvgrid.table.pageDown();
            },
            watchCurrentChannel: function () {
                Connect.common.watchChannel(
                    this.tvgrid.table.currentChannel.get('id'),
                    this.tvgrid.table.currentChannel.get('locked'),
                    this.tvgrid.table.currentChannel
                );
            }
        },
        initialize: function (options) {
            this.options = _.defaults(options, this.defaultOptions);
            this.startTime = moment();
            this.busy = false;
            var tvguide = this;
            this.channels = new Models.Channels([], {
                channelsUrl: this.options.channelsUrl,
                programsUrl: this.options.programsUrl,
                channelsSync: this.options.channelsSync,
                programsSync: this.options.programsSync,
                programModel: this.options.programModel,
                startTime: this.startTime,
                request: {
                    started: _.bind(function () {
                        tvguide.busy = true;
                        this.options.spinnerShow();
                    }, this),
                    success: _.bind(function (response) {
                        var intervals = _.map(response, function (item) {
                            return {
                                start: item.interval[0],
                                end: item.interval[1]
                            };
                        });
                        tvguide.trigger('programs:updated', intervals);
                        tvguide.busy = false;
                        this.options.spinnerHide();
                    }, this),
                    error: _.bind(function (xhr) {
                        this.options.spinnerHide();
                        this.options.errorShow(xhr && xhr.responseText || '');
                    }, this)
                }
            });
            this.tvgrid = new Views.TVGrid({
                model: this.channels,
                startTime: this.startTime,
                addProgramCallback: options.addProgramCallback,
                channelClickCallback: options.channelClickCallback
            })
                .bind('change:program', this.changeProgram, this)
                .bind('show-goto-modal', function () {
                    this.trigger('show-goto-modal')
                }, this);
            this.currentProgram = new Models.CurrentProgram();
            this.selectedPrograms = new Models.SelectedPrograms();
            this.tvgrid.$el.hide();

            this.channels.bind('ready', function () {
                var hasChannels = this.channels.length > 0;
                this.tvgrid.$el.toggle(hasChannels);
                this.$noChannelsMessage.toggle(!hasChannels);
                this.trigger('ready');

                this.channels.each(function (channel) {
                    channel.get('programs').bind('change:selected', function (program, checked) {
                        var selectedProgramInfo = {
                            program: program,
                            channel: channel
                        };
                        if (checked) {
                            this.selectedPrograms.add(selectedProgramInfo);
                        } else {
                            this.selectedPrograms.remove(
                                this.selectedPrograms.findWhere(selectedProgramInfo)
                            );
                        }
                    }, this);
                }, this);
            }, this);

            this.options.spinnerShow();
            this.channels.fetch({
                reset: true,
                success: _.bind(function () {
                    this.options.spinnerHide();
                }, this),
                error: _.bind(function (model, xhr) {
                    this.options.spinnerHide();
                    this.options.errorShow(xhr && xhr.responseText || '');
                }, this)
            });
            this.tvgrid.updateTimerStart();

            this.tvgrid.table.on('beforechange:starttime', _.bind(function (time) {
                this.selectedPrograms.deletePastSelection(time);
            }, this));
        },
        remove: function () {
            // It is necessary, because there is a mandatory signature from the event.
            this.tvgrid.remove();
            Backbone.View.prototype.remove.call(this);
        },
        render: function () {
            this.$el.append(this.tvgrid.render().el);
            this.$noChannelsMessage = (
                $($('#no-channels-tmpl').html())
                    .appendTo(this.$el)
            );
            this.$noChannelsMessage.hide();
            return this;
        },
        changeProgram: function (program) {
            program.set('channel_name', this.tvgrid.table.currentChannel.get('name'));
            program.set('locked', this.tvgrid.table.currentChannel.get('locked'));
            program.set('channel_dvblink_id', this.tvgrid.table.currentChannel.get('extid'));
            this.currentProgram.set('program', program);
        },
        changeComparator: function(sortField) {
            this.channels.comparator = function(one, two) {
                var a = one.get(sortField), b = two.get(sortField);
                if (typeof a === 'number' && typeof b === 'number') {
                    if (a < 0) {
                        if (b < 0) {
                            a = one.get('name');
                            b = two.get('name');
                        } else {
                            return Infinity;
                        }
                    } else {
                        if (b < 0) {
                            return -Infinity;
                        } else {
                            var t = (a + 0.1 * one.get('subNumber')) -
                                (b + 0.1 * two.get('subNumber'));
                            return t
                        }
                    }
                }
                return a.toLowerCase() < b.toLowerCase() ? -1 : 1;
            };
        },
        sortChannels: function (sortField) {
            this.changeComparator(sortField);
            this.channels.sort();
        }
    });
    // / xtvguide xguide

}).call(this, Backbone);
