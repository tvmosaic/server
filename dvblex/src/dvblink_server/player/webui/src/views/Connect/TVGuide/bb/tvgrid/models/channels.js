
(function () {

    var App = window.App || (window.App = {});
    var Connect = App.Connect || (App.Connect = {});
    var Models = Connect.Models || (Connect.Models = {});

    Models.Channels = Backbone.Collection.extend({
        initialize: function (models, options) {
            options = options || {};
            this.programModel = options.programModel;
            this.model = Backbone.Model.extend({
                initialize: function () {
                    this.set('programs', new Models.Programs([], {
                        model: options.programModel
                    }));
                }
            });
            this.channelsSync = options.channelsSync;
            this.programsSync = options.programsSync;
            this.url = options.channelsUrl;
            this.programsUrl = options.programsUrl;
            this.request = options.request || {};
            this.startTime = options.startTime;
            this._emptyResponseChannels = {};
            this._intervals = new Models.Intervals();

            this.ready = _.once(_.bind(function () {
                this.trigger('ready');
            }, this));

            this.bind('reset', function () {
                var start = this.startTime.unix();
                this._emptyResponseChannels = _.fromPairs(this.map(function (ch) {
                    return [ch.get('id'), []];
                }));
                this.requiredInterval(start, 1, 'right');
            }, this);
        },
        sync: function (method, model, options) {
            if (method != 'read') {
                options.error('invalid method');
                return;
            }

            this.channelsSync.call(
                this,
                options,
                _.bind(options.success, this),
                _.bind(options.error, this)
            );
        },
        requiredInterval: function (start, lineDuration, direction, success) {
            this.trigger('onRequireInterval', start, lineDuration, direction);
            success = success || function () {};
            var end = start + lineDuration * 60;
            if (this._intervals.isInclude(new Models.Interval(start, end))) {
                success();
                return this;
            }
            var now = moment().unix();
            var requirededIntervals = this._intervals.sub(new Models.Interval(start, end)).valueOf();
            var requirededFirstInterval = _.first(requirededIntervals);
            var requirededLastInterval = _.last(requirededIntervals);

            if (direction === 'neutral') {
                requirededFirstInterval[0] = Math.max(start - 60 * 60 * 6, now);
                requirededLastInterval[1] = requirededFirstInterval[0] + (lineDuration * 60) + (60 * 60 * 12);
            } else if (direction === 'right') {
                requirededLastInterval[1] = requirededFirstInterval[0] + (60 * 60 * 12);
            } else if (direction === 'left') {
                requirededFirstInterval[0] = Math.max(start - 60 * 60 * 12, now);
            } else {
                throw new Error('Invalid param');
            }

            this._requestEPG(requirededIntervals.valueOf(), _.bind(function (response) {
                _.each(response, _.bind(function (item) {
                    var start = item.interval[0];
                    var end = item.interval[1];
                    var channels = _.extend(
                        _.clone(this._emptyResponseChannels),
                        _.fromPairs(_.map(item.channels, function (channel) {
                            return [channel.cid, channel.programs];
                        }))
                    );
                    channels = _.map(channels, function (value, key) {
                        return { cid: key, programs: value };
                    });
                    this._updatePrograms(channels, start, end);
                    this._intervals.add(new Models.Interval(start, end));
                }, this));
                this.ready();
                this.request.success.apply(this, arguments);
                success();
            }, this));

            return this;
        },

        _requestEPG: function (intervals, success) {
            this.request.started();

            this.programsSync.call(
                this,
                { intervals: intervals },
                _.bind(function (response) {
                    if (!_.isArray(response)) {
                        this.request.error.apply(this);
                        return;
                    }
                    success(response);
                }, this),
                _.bind(function () {
                    this.request.error.apply(this, arguments);
                }, this)
            )
        },

        _updatePrograms: function (responseChannels, start, end) {
            _.each(responseChannels, _.bind(function (responseChannel) {
                var channel = this.findWhere({id: responseChannel.cid});
                if (!channel) {
                    return;
                }
                channel.get('programs')
                    .merge(new Models.Programs(responseChannel.programs, {
                        parse: true,
                        model: this.programModel
                    }), start, end)
            }, this));
        }
    });

}).call(this);
