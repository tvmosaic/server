
$(function() {

    var App = window.App || (window.App = {});
    var Connect = App.Connect || (App.Connect = {});
    var Models = Connect.Models || (Connect.Models = {});
    var Views = Connect.Views || (Connect.Views = {});

    // xprogamdialog
    Views.ProgramDialog = Backbone.View.extend({
        actions: {
            watchChannel: function () {
                this.playCurrentChannel();
            },
            watchIfProgramNow: function () {
                if (!this.programIsNow()) {
                    return;
                }
                this.playCurrentChannel();
            },
            hide: function () {
                this.hide();
            }
        },
        show: function () {
            App.common.Keyboard.focus(this);
        },
        hide: function () { },
        initialize: function (options) {
            this._dialog = {};

            this.channel = options.channel;
            this.channels = options.channels;

            this.oldProgram = null;

            this.model.bind('change', function (model) {
                var program = model.clone().get('program');
                this._changeProgram(program);
                if (this.oldProgram) {
                    this.oldProgram.off('change', this._changeProgram);
                }
                this.oldProgram = model.get('program').bind('change', this._changeProgram, this);
            }, this);
        },

        _changeProgram: function (program) {
            if (program.isUnknownProgram()) {
                program = new Models.TVGuideProgram({
                    isUnknown: true,
                    name: Connect.Strings.unknownName,
                    start: moment(),
                    end: moment()
                });
            }

            program = program.toJSON();
            program.duration = Math.floor(program.duration / 60);
            program.channel_id = this.channel.get('id');

            program.is_now = moment().isBetween(program.start, program.end);
        },

        appendEvent: function (events) {
            var chain = this._dialog.events;
            this._dialog.events = function () { return _.defaults(events, chain()); };
        },

        programIsNow: function () {
            return this._dialog.model.get('is_now');
        },
        playCurrentChannel: function () {
            this._dialog._play();
        }
    });
    // / xprogamdialog
});
