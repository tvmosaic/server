
(function () {

    var App = window.App || (window.App = {});
    var Connect = App.Connect || (App.Connect = {});
    var Models = Connect.Models || (Connect.Models = {});

    Models.TVGuideProgram = Models.Program.extend({
        _request: function (url, data) {
            data = _.extend({}, data, {
                full: true
            });
            return Connect.common.request(url, data)
                .done(_.bind(function (response) {

                    var recordParams = {
                        is_record: false,
                        is_active: false,
                        is_record_conflict: false,
                        is_repeat_record: false,
                        recording_id: '',
                        schedule_id: ''
                    };

                    // Clears the recording status of all programs.
                    _.each(this.collection.models, function (channel) {
                        var programs = channel.get('programs');
                        var recorded_programs = programs.where({is_record: true});
                        if (recorded_programs && recorded_programs.length) {
                            _.each(recorded_programs, function (program) {
                                // Resetting the grid model.
                                program.set(recordParams);
                            }, this);
                        }
                    }, this);

                    // Resetting the dialogue model.
                    this.set(recordParams);

                    // Setting the recording state of programs from the server response.
                    _.each(response, function (record) {
                        var channel = this.collection.findWhere({
                            id: record.channel_id
                        });
                        var program = channel.get('programs').findWhere({
                            program_id: record.program_id
                        });
                        if (!program) {
                            return;
                        }
                        var recordParams = {
                            is_active: record.is_active,
                            is_record_conflict: record.is_record_conflict,
                            is_record: record.is_record,
                            is_repeat_record: record.is_repeat_record,
                            recording_id: record.recording_id,
                            schedule_id: record.schedule_id
                        };
                        program.set(recordParams);
                        // Сброс модели диалога.
                        if (channel.get('id') === this.get('channel_id') &&
                            program.get('program_id') === this.get('program_id')) {
                            this.set(recordParams);
                        }

                    }, this);
                }, this))
                .fail(_.bind(function (xhr) {
                    this.trigger('error', this, xhr);
                }, this));
        }
    });
    _.extend(Models.TVGuideProgram.prototype, this.ProgramMixin);

    Models.CurrentProgram = Backbone.Model.extend({
        defaults: {
            program: new Models.TVGuideProgram()
        }
    });

}).call(this);
