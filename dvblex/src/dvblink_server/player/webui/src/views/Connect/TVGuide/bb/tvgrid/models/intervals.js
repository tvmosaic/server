
(function () {

    var App = window.App || (window.App = {});
    var Connect = App.Connect || (App.Connect = {});
    var Models = Connect.Models || (Connect.Models = {});

    function removeArrayItem (array, index) {
        return array.slice(0, index).concat(array.slice(index + 1));
    }

    function Interval (start, end) {
        // TODO: check overflow this._end > this._start
        this._start = start;
        this._end = end;
    }
    Interval.prototype.valueOf = function () {
        return [this._start, this._end];
    };
    Interval.prototype.start = function () {
        return this._start;
    };
    Interval.prototype.end = function () {
        return this._end;
    };

    function Intervals (initial) {
        this._intervals = initial || [];
    }
    Intervals.prototype.valueOf = function () {
        var values = [];
        for (var i=0, l=this._intervals.length; i<l; ++i) {
            values.push(this._intervals[i].valueOf());
        }
        return values;
    };
    Intervals.prototype._mergeOverlappingIntervals = function (list) {
        // Combines intersecting intervals.
        list = list.slice(0).sort(function (a, b) {
            return a.start() - b.start();
        });
        var i, l;
        var founded = false;
        var exit = list.length;
        while (--exit) {
            for (i=0, l=list.length-1; i<l; ++i) {
                founded = list[i].end() >= list[i+1].start();
                if (founded) { break; }
            }
            if (!founded) { break; }
            founded = false;
            list[i] = new Interval(list[i].start(), list[i+1].end());
            list = removeArrayItem(list, i+1);
        }
        return list;
    }
    Intervals.prototype.add = function (interval) {
        this._intervals.push(new Interval(interval.start(), interval.end()));
        this._intervals = this._mergeOverlappingIntervals(this._intervals);
    };
    Intervals.prototype.isInclude = function (interval) {
        for (var i=0, l=this._intervals.length; i<l; ++i) {
            if (interval.start() >= this._intervals[i].start() &&
                interval.end() <= this._intervals[i].end()) {
                return true;
            }
        }
        return false;
    };
    Intervals.prototype.sub = function (interval) {
        if (this.isInclude(interval)) {
            return new Intervals();
        }
        if (!this._intervals.length) {
            return new Intervals([new Interval(interval.start(), interval.end())]);
        }

        var start = interval.start();
        var end = interval.end();

        // Если искомый интервал кончается раньше начала первого или начинается позже конца последнего.
        if (end <= this._intervals[0].start() ||
            start >= this._intervals[this._intervals.length-1].end()) {
            return new Intervals([new Interval(start, end)]);
        }

        var i, l;
        var last;
        var firstIntervalIndex = 0;
        var lastIntervalIndex = this._intervals.length-1;
        var result = [];

        // Starting from the first, we look for the interval closest to the given beginning (to the right).
        last = this._intervals[0].start();
        for (i=0, l=this._intervals.length; i<l; ++i) {
            if (start >= last && start <= this._intervals[i].end()) {
                firstIntervalIndex = i;
                break;
            }
            last = this._intervals[i].end();
        }

        // Starting from the last one, we look for the interval closest to the given end (to the left).
        last = this._intervals[this._intervals.length-1].end();
        for (i=this._intervals.length-1, l=0-1; i>l; --i) {
            if (end >= this._intervals[i].start() && end <= last) {
                lastIntervalIndex = i;
                break;
            }
            last = this._intervals[i].start();
        }

        if (lastIntervalIndex < firstIntervalIndex) {
            return new Intervals([new Interval(start, end)]);
        }

        // If the given start is not in the interval, add from the beginning to the interval.
        if (this._intervals[firstIntervalIndex].start() > start) {
            result.push(new Interval(start, this._intervals[firstIntervalIndex].start()));
        }
        // Add areas between the intervals.
        for (i=firstIntervalIndex, l=lastIntervalIndex; i<l; ++i) {
            result.push(new Interval(
                this._intervals[i].end(), this._intervals[i+1].start()
            ));
        }
        // If the given end is not included in the interval, add from the interval to the end.
        if (this._intervals[lastIntervalIndex].end() < end) {
            result.push(new Interval(this._intervals[lastIntervalIndex].end(), end));
        }
        return new Intervals(result);
    };
    Intervals.prototype.toArray = function () {
        return this._intervals.slice(0);
    };

    Models.Interval = Interval;
    Models.Intervals = Intervals;

}).call(this);
