
(function (Backbone) {

    var App = window.App || (window.App = {});
    var Connect = App.Connect || (App.Connect = {});
    var Views = Connect.Views || (Connect.Views = {});

    function makeChannelNumber (number, subnumber) {
        return (
            (number > -1) ? (
                (subnumber > 0) ?
                    (number + '.' + subnumber) :
                    ('' + number)
            ) : '');
    }

    // xguidetable
    Views.GuideTable = Backbone.View.extend({
        className: 'dbl-table',
        template: $('#tvguide-table-tmpl').html(),
        events: {
            'click .programs-line > .dbl-program-item': 'click',
            'click .dbl-channel-list li': '_watchChannel'
        },

        click: function (event) {
            this.$('li.current').removeClass('current');
            var programElement = $(event.currentTarget);
            var pid = programElement.attr('id');
            var cid = programElement.parent().closest('li').attr('id');
            this._selectChannel(cid);
            var currentProgramsLine = this._getCurrentProgramsLine();
            if (!currentProgramsLine) {
                return;
            }
            currentProgramsLine._selectProgram(pid);

            this._changedSelectedProgram();
            this._lastMoveProgram = currentProgramsLine.getSelectedProgram();

            return false;
        },

        initialize: function (options) {
            this.$el.html(this.template);
            this.channelListElement = this.$('.dbl-channel-list ul');
            this.programsListElement = this.$('.programs-list ul');

            $('body').removeClass('with-logo');
            this.resetOnceResize();

            this.startTime = options.startTime;
            this.currentStartTime = this.startTime;
            this._lastWindowOffset = 0;

            var VERTICAL_MARGIN = 2;
            this.calcItemHeight = null;
            this.numIncreasedElements = 0;

            this.addProgramCallback = options.addProgramCallback;
            this.channelClickCallback = options.channelClickCallback || _.noop;

            var mockItem = $('<div/>').addClass('dbl-tvguide-content-mock-item')
                .append($('<div/>').addClass('dbl-tvguide-content-mock-item-content'))
                .appendTo('body');
            this.itemHeightOuter = mockItem.outerHeight(true) + VERTICAL_MARGIN;
            mockItem.remove();
            this.numChannelOnScreen = 1;
            this.model.bind('updated sort', this._updateTable, this);
            function resizeTable() {
                var HEADER_FOOTER_HEIGHT = 105;
                var MULT = 5;
                var PADDINGS = 0;
                var MIN_CHANNELS_NUM = 2;

                this.tableHeight = $(window).height() - HEADER_FOOTER_HEIGHT;
                this.numChannelOnScreen = Math.max(
                    Math.floor(this.tableHeight / this.itemHeightOuter),
                    MIN_CHANNELS_NUM
                );

                var heightRemainder = this.tableHeight - this.numChannelOnScreen * this.itemHeightOuter;
                var additionalHeight = Math.floor(heightRemainder / this.numChannelOnScreen);
                this.calcItemHeight = this.itemHeightOuter + additionalHeight;
                this.numIncreasedElements = this.tableHeight - this.numChannelOnScreen * this.calcItemHeight;
                this.calcItemHeight -= VERTICAL_MARGIN;

                var $programList = $('.programs-list');
                $programList.css({width: 'auto'});
                var width = Math.floor($programList.width() / MULT) * MULT;
                // When tvguide is hidden, it will return 0.
                $programList.width(width - PADDINGS);
                this.trigger('resize', width - PADDINGS);

                (this.numChannelOnScreen > 2 ? $.fn.addClass : $.fn.removeClass)
                    .call($('body'), 'no-vert-scroll')

                this._updateTable();
            }

            $(window).on('resize.app', _.bind(function () {
                resizeTable.call(this);
            }, this));

            this.programsList = [];
            this._selectedChannelIndex = 0;
            this._lastMoveProgram = null; // Selected gear at last move between programs.

            this.currentChannel = new Backbone.Model({});

            this.$scrollbar = new Views.SimpleScrollbar();
            this.$scrollbar.reset();
        },
        remove: function () {
            $(window).off('resize.app');
            Backbone.View.prototype.remove.call(this);
        },
        render: function () {
            this.channelListElement.empty();
            this.programsListElement.empty();
            this.programsList = [];

            var name;
            $('body').removeClass('with-logo');
            _.each(this.window, _.bind(function (channel) {
                name = channel.get('name');
                var $a = $('<a href="#">')
                    .attr('tabindex', '-1')
                    .attr('title', name);
                $a.append(
                    $('<span>')
                        .addClass('name')
                        .text(name)
                );
                var logo = channel.get('logo');
                if (logo) {
                    $a.append(
                        $('<span>')
                            .addClass('logo')
                            .append('<span class="align-helper"></span>')
                            .append('<img class="logo-img" src="' + logo + '"/>')
                    );
                }
                $a.toggleClass('with-logo', Boolean(logo));
                var channelNumber = makeChannelNumber(
                    channel.get('number'), channel.get('subNumber')
                );
                if (channelNumber !== '') {
                    $a.append(
                        $('<span>')
                            .addClass('number')
                            .text(channelNumber)
                    );
                }
                this.channelListElement.append(
                    $('<li>')
                        .attr('id', channel.get('id'))
                        .attr('extid', channel.get('extid'))
                        .addClass(channel.get('locked') ? 'dbl-locked' : '')
                        .append($a)
                );

                var li = $('<li>').attr('id', channel.get('id'));
                this.programsListElement.append(li);
                var programsLine = new Views.ProgramsLine({
                    model: channel.get('programs'),
                    parent: li,
                    lineStartTime: this.startTime,
                    lineEndTime: this.endTime,
                    currentStartTime: this.currentStartTime,
                    addProgramCallback: this.addProgramCallback
                });
                this.programsList.push(programsLine);
                li.append(programsLine.render().el);
            }, this));
            this.$el.append(this.$scrollbar.render().el);

            if (_.some(this.channels, function (channel) { return channel.get('logo'); })) {
                $('body').addClass('with-logo');
            }

            if (this._lastSelectedProgram) {
                var pid = this._lastSelectedProgram.get('program_id');
                var currentProgramsLine = this._getCurrentProgramsLine();
                if (currentProgramsLine) {
                    currentProgramsLine._selectProgram(pid);
                }
            }

            if (this.calcItemHeight) {
                var $programs = $('.dbl-tvguide-content .programs-line');
                var $channels = $('.dbl-channel-list li');
                if (this.numIncreasedElements) {
                    $programs.slice(0, this.numIncreasedElements).css({ height: this.calcItemHeight + 1 });
                    $channels.slice(0, this.numIncreasedElements).css({ height: this.calcItemHeight + 1 });
                }
                $programs.slice(this.numIncreasedElements).css({ height: this.calcItemHeight });
                $channels.slice(this.numIncreasedElements).css({ height: this.calcItemHeight });
                this.onceResize();
            }

            return this;
        },
        resetOnceResize: function () {
            this.onceResize = _.once(function () {
                _.defer(function () {
                    $(window).trigger('resize');
                });
            });
        },
        moveChannelCursor: function (num) {
            $('li.current', this.$el).removeClass('current');

            var currentChannelId = this._seekSelectedChannel(num).get('id');
            var windowOffset = this._selectedChannelIndex < this.channels.indexOf(_.first(this.window)) ?
                this.numChannelOnScreen : this._selectedChannelIndex > this.channels.indexOf(_.last(this.window)) ?
                1 : 0;
            if (windowOffset) {
                this._lastWindowOffset = this._selectedChannelIndex + windowOffset;
                this.window = _.takeRight(_.take(
                    this.channels, this._lastWindowOffset
                ), this.numChannelOnScreen);
                this.render();
            }

            var currentProgramsLine = this._getCurrentProgramsLine();
            if (!currentProgramsLine) {
                return;
            }
            if (this._lastMoveProgram) {
                var lastSelectedProgramStartTime = this._lastMoveProgram.get('start_time');
                currentProgramsLine.selectByTime(lastSelectedProgramStartTime);
            } else {
                currentProgramsLine.selectByIndex(0);
            }

            $('li#'+currentChannelId, this.channelListElement).addClass('current');

            this._changedSelectedProgram();

            var currentElementIndex = this._selectedChannelIndex;
            var elementOnPage = this.numChannelOnScreen;
            var totalElements = this.channels.length;
            this.$scrollbar.update(currentElementIndex, elementOnPage, totalElements);
        },
        pageDown: function () {
            this.moveChannelCursor(this.numChannelOnScreen-1);
        },
        pageUp: function () {
            this.moveChannelCursor(-this.numChannelOnScreen+1);
        },
        _getCurrentProgramsLine: function () {
            var currentChannel = this._getSelectedChannel();
            if (!currentChannel) {
                return null;
            }
            var currentChannelPrograms = currentChannel.get('programs');

            return _.find(this.programsList, function (programsLine) {
                return programsLine.model === currentChannelPrograms;
            }, this);
        },
        moveProgramCursor: function (num) {
            this.moveProgram({right: 'moveCursorRight', left: 'moveCursorLeft'}, num);
        },
        moveProgramPage: function (num) {
            this.moveProgram({right: 'movePageRight', left: 'movePageLeft'}, num);
        },
        moveProgram: function (funcs, num) {
            var currentProgramsLine = this._getCurrentProgramsLine();
            if (!currentProgramsLine) {
                return;
            }
            var params = num > 0 ? [funcs.right, 1] : [funcs.left, -1];
            var time = currentProgramsLine[params[0]].call(
                currentProgramsLine, params[1] * num * 60
            );

            var dataReady = $.Deferred();
            if (time) { // The current line has shifted.
                this.model.requiredInterval(
                    time.unix(),
                    currentProgramsLine.lineDuration,
                    num > 0 ? 'right' : 'left',
                    function () {
                        dataReady.resolve();
                    }
                );
            } else {
                dataReady.resolve();
            }

            dataReady.done(_.bind(function () {
                currentProgramsLine.move();
                currentProgramsLine.highlightSelectedProgram();

                if (time) { // The current line has shifted.
                    // Move the rest of the lines.
                    _.each(this.programsList, _.bind(function (programsLine) {
                        // We skip the current line.
                        if (currentProgramsLine != programsLine) {
                            programsLine.move(time);
                        }
                    }, this));
                    this.trigger('change:starttime', time, currentProgramsLine.lineDuration);
                    this.currentStartTime = time;
                }

                this._changedSelectedProgram();
                this._lastMoveProgram = currentProgramsLine.getSelectedProgram();
            }, this));
        },

        moveToCurrentTime: function () {
            var currentProgramsLine = this._getCurrentProgramsLine();
            if (!currentProgramsLine) {
                return;
            }
            var now = moment();
            _.each(this.programsList, _.bind(function (programsLine) {
                programsLine.move(now);
            }, this));
            this.trigger('change:starttime', now, currentProgramsLine.lineDuration);
            this.currentStartTime = now;
            this._changedSelectedProgram();
            this._lastMoveProgram = currentProgramsLine.getSelectedProgram();
        },

        gotoTime: function (time) {
            var currentProgramsLine = this._getCurrentProgramsLine();
            if (!currentProgramsLine) {
                return;
            }
            this.model.requiredInterval(time.unix(), currentProgramsLine.lineDuration, 'neutral', _.bind(function () {
                _.each(this.programsList, _.bind(function (programsLine) {
                    programsLine.move(time);
                }, this));
                this.trigger('change:starttime', time, currentProgramsLine.lineDuration);
                this.currentStartTime = time;
                this._changedSelectedProgram();
                this._lastMoveProgram = currentProgramsLine.getSelectedProgram();
            }, this));
        },

        _getSelectedChannel: function () {
            return this.channels[this._selectedChannelIndex] || null;
        },
        _seekSelectedChannel: function (num) {
            this._selectedChannelIndex += num;
            this._selectedChannelIndex = Math.max(this._selectedChannelIndex, 0);
            this._selectedChannelIndex = Math.min(
                this._selectedChannelIndex, this.channels.length-1);
            return this.channels[this._selectedChannelIndex];
        },
        _changedSelectedProgram: function () {
            this.currentChannel.set(this._getSelectedChannel().toJSON());
            var currentProgramsLine = this._getCurrentProgramsLine();
            if (!currentProgramsLine) {
                return;
            }
            var selectedProgram = currentProgramsLine.getSelectedProgram();
            if (selectedProgram === this._lastSelectedProgram) {
                return;
            }
            this._lastSelectedProgram = selectedProgram;
            this.trigger('change:program', this._lastSelectedProgram);
        },
        changeStartTime: function (time) {
            this.trigger('beforechange:starttime', time);
            var currentProgramsLine = this._getCurrentProgramsLine();
            if (!currentProgramsLine) {
                return;
            }
            var rebuilded = _.some(_.map(this.programsList, function (program) {
                return program.changeStartTime(time, program === currentProgramsLine);
            }, this));
            this.startTime = time;
            if (rebuilded) {
                this.currentStartTime = time;
                this.trigger('change:starttime', time, currentProgramsLine.lineDuration);
                this._changedSelectedProgram();
            }
        },
        _selectChannel: function (cid) {
            var currentChannel = _.find(this.channels, function (channel) {
                return channel.id === cid;
            }, this);
            this._selectedChannelIndex = _.indexOf(this.channels, currentChannel);
        },

        _watchChannel: function (event) {
            var cid = $(event.currentTarget).attr('extid');
            this.watchChannel(cid);
            return false;
        },

        _updateTable: function () {
            if (!this.channelsFilter) {
                this.channels = this.model.models;
            } else {
                this.channels = this.model.models.filter(function (m) {
                    return _.includes(this.channelsFilter, m.get('id'));
                }, this);
            }
            this.endTime = moment.unix(
                _.max(_.map(this.channels, function (channel) {
                    var lastProgram = _.last(channel.get('programs').models);
                    return lastProgram.get('start_time') + lastProgram.get('duration');
                }, this))
            );
            this.window = _.takeRight(_.take(
                this.channels, this._lastWindowOffset || this.numChannelOnScreen
            ), this.numChannelOnScreen);
            this.render();
        },

        setVisibleChannels: function (channels) {
            this.channelsFilter = channels;
            this._selectedChannelIndex = 0;
            this._lastMoveProgram = null;
            this._lastSelectedProgram = null;
            this.resetOnceResize();
            this._updateTable();
        },

        watchChannel: function (cid) {
            this.channelClickCallback(cid, this.model);
        }
    });
    // / xguidetable

}).call(this, Backbone);
