
(function () {
    var App = window.App || (window.App = {});
    var Connect = App.Connect || (App.Connect = {});
    var Models = Connect.Models || (Connect.Models = {});

    var ProgramMixin = {
        MINIMAL_PROGRAM_DURATION: 60,
        defaults: {
            name: Connect.Strings.unknownName,
            start_time: 0,
            duration: 0,

            subname: '',
            description: '',
            short_desc: '',
            image: '',

            categories: '',
            actors: '',
            directors: '',
            writers: '',
            producers: '',
            guests: '',
            year: '',

            is_hdtv: false,
            is_repeat: false,
            is_premiere: false,

            is_record: false,
            is_series: false,
            is_repeat_record: false,

            is_record_conflict: false,
            is_active: false,
            recording_id: '',
            schedule_id: '',

            recommendation: null,

            season: '',
            episode: '',
            channel_name: '',

            rating: 0,
            genres: [],

            isToday: false,
            isUnknown: false,
            selected: false
        },

        isUnknownProgram: function () {
            return this.get('name') === this.defaults.name;
        },

        parse: function (response) {
            _.extend(response, {
                isToday: moment().startOf('day').isSame(
                    moment.unix(response.start_time).startOf('day')
                ),
                start: moment.unix(response.start_time),
                end: moment.unix(response.start_time + response.duration)
            });
            return response;
        }
    };

    var Program = Models.Program = Backbone.Model.extend({
        addRecording: function () { },
    });
    _.extend(Program.prototype, ProgramMixin);

    Models.Programs = Backbone.Collection.extend({

        initialize: function () {
            this.listenTo(this, 'need-update', _.bind(function (options) {
                var fetchOption = { reset: true };
                if (options) {
                    _.extend(fetchOption, { data: options });
                }
                this.fetch(fetchOption);
            }, this));
        },

        comparator: function (item) {
            return item.get('start_time');
        },

        /*
         * Computes the transmission to be play during time.
         */
        getByTime: function (time) {
            time = +time;
            return this.find(function (program) {
                var startTime = +program.get('start_time');
                var duration = +program.get('duration');
                return time >= startTime && startTime + duration > time;
            });
        },

        merge: function (programs, s, e) {
            var start = s || null;
            var end = e || null;
            var existsFirstProgram = this.first();
            var existsLastProgram = this.last();
            if (existsFirstProgram && existsLastProgram) {
                start = Math.min(existsFirstProgram.get('start_time'), start);
                end = Math.max(existsLastProgram.get('start_time') + existsLastProgram.get('duration'), end);
            }

            // We remove unknown transfers, they will be created in correctHoleInPrograms.
            this.remove(_.filter(this.models, function (program) {
                return program.isUnknownProgram();
            }));

            this.add(programs.models);
            this.correctHoleInPrograms(start, end);
            return this;
        },

        /*
         * Corrects errors in the program guide.
         */
        correctHoleInPrograms: function (startLineTime, endLineTime) {
            var firstProgram = _.first(this.models);
            var lastProgram = _.last(this.models);
            if (!firstProgram && !lastProgram) {
                this.add(new this.model({
                    'start_time': startLineTime,
                    'duration': endLineTime - startLineTime,
                    'program_id': _.uniqueId('unknown_'),
                    'isUnknown': true
                }), {at: 0});
                return;
            }
            var firstProgramStartTime = firstProgram.get('start_time');
            var lastProgramEndTime = lastProgram.get('start_time') + lastProgram.get('duration');

            if (startLineTime && firstProgramStartTime > startLineTime) {
                // The first program starts after startLineTime, adding an unknown transfer to the beginning.
                this.add(new this.model({
                    'start_time': startLineTime,
                    'duration': Math.ceil((firstProgramStartTime - startLineTime) / 60) * 60,
                    'program_id': _.uniqueId('unknown_'),
                    'isUnknown': true
                }), {at: 0});
            }

            if (endLineTime && lastProgramEndTime < endLineTime) {
                // The last program ends before endLineTime, we add an unknown transfer to the end.
                this.add(new this.model({
                    'start_time': lastProgramEndTime,
                    'duration': endLineTime - lastProgramEndTime,
                    'program_id': _.uniqueId('unknown_'),
                    'isUnknown': true
                }), {at: this.length});
            }

            // We delete programs that have a duration less than the threshold value.
            this.remove(_.filter(this.models, function (program) {
                return program.get('duration') < program.MINIMAL_PROGRAM_DURATION;
            }));

            // Find holes / overlays in the program.
            var program, nextProgram, currentProgramStart, currentProgramEnd, nextProgramStart;
            var correctProgramList = [];
            var removeProgramList = [];
            for (var i=0, l=this.models.length-1; i<l; ++i) {
                program = this.models[i];
                nextProgram = this.models[i+1];
                currentProgramStart = Number(program.get('start_time'));
                currentProgramEnd = Number(currentProgramStart) + Number(program.get('duration'));
                nextProgramStart = Number(nextProgram.get('start_time'));

                if (currentProgramEnd !== nextProgramStart) {
                    if (currentProgramEnd > nextProgramStart) {
                        // Overlay.
                        if (program.get('program_id') === nextProgram.get('program_id')) {
                            removeProgramList.push(this.models[i]);
                            continue;
                        }
                        // Correct the duration of the current broadcast.
                        this.models[i].set('duration', nextProgramStart-currentProgramStart);
                        // If the corrected transmission has already ended, we delete it.
                        if (startLineTime && startLineTime >= nextProgramStart) {
                            removeProgramList.push(this.models[i]);
                        }
                    } else {
                        // Hole.
                        correctProgramList.push({
                            'index': i+1,
                            'start_time': currentProgramEnd,
                            'duration': nextProgramStart-currentProgramEnd,
                            'program_id': _.uniqueId('unknown_'),
                            'isUnknown': true
                        });
                    }
                }
            }
            if (removeProgramList.length) {
                _.each(removeProgramList, _.bind(function (program) {
                    this.remove(program);
                }, this));
            }
            var index;
            _.each(correctProgramList, _.bind(function (program) {
                index = program.index;
                delete program.index;
                this.add(new this.model(program), {at: index});
            }, this));
            return this;
        }
    });

    var SelectedProgram = Backbone.Model.extend();
    Models.SelectedPrograms = Backbone.Collection.extend({
        model: SelectedProgram,
        clear: function () {
            var programs = this.map(function (selected) {
                return selected.get('program');
            });
            _.each(programs, function (program) {
                program.set('selected', false);
            });
        },
        deletePastSelection: function (now) {
            // Removes selection from past programs.
            var selectedList = this.filter(function (selected) {
                var program = selected.get('program');
                var start = program.get('start_time');
                var end = start + program.get('duration');
                end = moment.unix(end);
                return end.isBefore(now) || end.isSame(now);
            });
            _.each(selectedList, function (selected) {
                selected.get('program').set('selected', false);
            });
        }
    });

}).call(this);
