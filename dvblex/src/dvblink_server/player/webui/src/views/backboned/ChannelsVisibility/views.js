
(function () {
    var App = window.App || (window.App = {});
    var Models = App.Models || (App.Models = {});
    var Views = App.Views || (App.Views = {});
    Views.ChannelVisibility = {};

    function updateFakeDropdown($item) {
        $item.closest('ul').find('li.active').removeClass('active');
        $item.closest('li').addClass('active');
        var $dropdown = $item.closest('.dropdown')
        $dropdown.find('input[type=hidden]').val($item.data('value'));
        $dropdown.find('button u').html($item.text());
    }

    $(function () {
        $('.dropdown .dropdown-menu ul').on('click', 'li a', function() {
            updateFakeDropdown($(this));
        });
    });

    var Provider = Views.ChannelVisibility.Provider = Backbone.View.extend({
        tagName: 'li',
        template: _.template($('#provider-tmpl').html()),
        events: {
            'click': 'reloadChildren'
        },
        initialize: function (){
            this.model.on('change', this.render, this);
        },
        render: function (){
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },
        reloadChildren: function(){
            var loadTransponders = true;

            self.$('.query').val('').next().addClass('hidden');

            if(!this.model.id) {
                var $dropdown = $('#transponders_dropdown');

                $dropdown.find('u').html(window.i18n.channelsAllTransponders);
                $dropdown.addClass('disabled');
                $dropdown.parent().find('ul').empty();

                loadTransponders = false;
            }
            this.model.reloadChildren(loadTransponders);
        }
    });

    Views.ChannelVisibility.Providers = Backbone.View.extend({
        initialize: function () {
            this.model
                .on('request', function () {
                    this.$el.addClass('loading');
                }, this)
                .on('sync', function () {
                    if (this.model.length > 0) {
                        this.$el.removeClass('loading');
                    }
                }, this);

            this.model.on('sync', this.render, this);
        },
        render: function () {
            this.$el.empty();
            this.model.each(function (provider) {
                this.$el.append(new Provider({
                    model: provider
                }).render().el);
            }, this);

            updateFakeDropdown(this.$el.find('li').first().find('a'));
            return this;
        }
    });

    var Transponder = Views.ChannelVisibility.Transponder = Backbone.View.extend({
        tagName: 'li',
        template: _.template($('#transponder-tmpl').html()),
        events: {
            'click': 'reloadChannels'
        },
        initialize: function (){
            this.model.on('change', this.render, this);
        },
        render: function (){
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },
        reloadChannels: function(){
            self.$('#query').val('').next().addClass('hidden');
            this.model.reloadChannels();
        }
    });

    Views.ChannelVisibility.Transponders = Backbone.View.extend({
        initialize: function () {
            this.model
                .on('request', function () {
                    this.$el.addClass('loading').empty();
                    this.$el.closest('.dropdown').find('button').addClass('disabled');
                }, this)
                .on('sync', function () {
                    if (this.model.length > 0) {
                        this.$el.removeClass('loading');
                        this.$el.closest('.dropdown').find('button').removeClass('disabled');
                    }
                }, this);

            this.model.on('sync', this.render, this);
        },
        render: function () {
            this.$el.empty();
            this.model.each(function (transponder) {
                this.$el.append(new Transponder({
                    model: transponder
                }).render().el);
            }, this);

            var $first = this.$el.find('li').first();

            updateFakeDropdown($first.find('a'));

            return this;
        }
    });


    var Channel = Views.ChannelVisibility.Channel = Backbone.View.extend({
        tagName: 'li',
        template: _.template($('#channel-tmpl').html()),
        initialize: function (){

        },
        render: function (){
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },
    });

    Views.ChannelVisibility.Channels = Backbone.View.extend({
        initialize: function () {
            this.model
                .on('request', function () {
                    this.$el.addClass('loading');
                }, this)
                .on('sync', function () {
                    if (this.model.length > 0) {
                        this.$el.removeClass('loading');
                    }
                }, this);
            this.model.on('sync', this.render, this);
        },
        render: function () {
            this.$el.empty();
            this.model.each(function (channel) {
                this.$el.append(new Channel({
                    model: channel
                }).render().el);
            }, this);
            this.$el.closest('.scrollbar-inner').scrollbar();
            return this;
        }
    });

    Views.ChannelVisibility.App = Backbone.View.extend({
        events: {
            'click #checker_unchecker': 'checkUncheck',
            'click #cancel': 'cancel',
            'click #save': 'save',
            'click #channels_list li': 'liClick',
            'click #channels_list li label': 'labelClick',
            'click #search': 'searchClick',
            'keydown #query': 'searchKeydown',
            'click #empty_query': 'emptyQuery',
            'keydown #transponder_query': 'transponderSearchKeydown',
            'click #transponder_search': 'transponderSearch',
            'click #transponder_empty_query': 'transponderEmptyQuery',
            'keyup .query': 'keyup',
        },
        destroy: function () {
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
        },
        initialize: function () {
            this.providers = new App.Models.ChannelVisibility.Providers();
            this.providers
                .on('request', function () {
                    Views.Spinner.show();
                }, this)
                .on('sync', function () {
                    Views.Spinner.hide();
                }, this)
                .on('sync:transponders', function (provider) {
                    this.transponders = new Models.ChannelVisibility.Transponders({providerId: provider.attributes.id});
                    this.transponders
                        .on('request', function () {
                            Views.Spinner.show();
                        }, this)
                        .on('sync', function () {
                            Views.Spinner.hide();
                        }, this)
                        .on('sync:channels', function (transponder) {
                            this.loadChannels(provider.attributes.id, transponder.attributes.id);
                        }, this);
                    this.transpondersView = new Views.ChannelVisibility.Transponders({
                        model: this.transponders,
                        el: this.$('#transponders_list')
                    });
                    this.transponders.fetch();
                }, this)
                .on('request:channels', function () {
                    Views.Spinner.show();
                }, this)
                .on('sync:channels', function (provider) {
                    this.loadChannels(provider.attributes.id);
                }, this);
            this.providersView = new App.Views.ChannelVisibility.Providers({
                model: this.providers,
                el: this.$('#providers_list')
            });

            this.providers.fetch();
            this.loadChannels();
        },
        loadChannels: function(providerId, transponderId) {
            this.channels = new App.Models.ChannelVisibility.Channels({
                providerId: providerId, transponderId: transponderId, storedChannels: this.storedChannels
            });
            this.channels
                .on('request', function () {
                    Views.Spinner.show();
                }, this)
                .on('sync', function () {
                    if(!this.storedChannels) {
                        this.storedChannels = this.channels.allChannels;
                    }
                    Views.Spinner.hide();
                }, this);

            this.channelsView = new App.Views.ChannelVisibility.Channels({
                model: this.channels,
                el: this.$('#channels_list')
            });

            this.channels.fetch();
        },

        checkUncheck: function(e) {
            var $this = $(e.target).closest('div').find('a');
            var $span = $this.find('span');
            var value = $span.hasClass('glyphicon-check') ? true : false;

            $span.toggleClass('glyphicon-check glyphicon-unchecked');
            this.changeAllCheckboxes(value);
        },
        changeAllCheckboxes: function(value) {
            var here = this;

            self.$('#channels_list li:visible input:checkbox').each(function(){
                var $this = $(this);
                $this.attr('checked', value);
                here.changeChannelData($this.val(), value);
            });
        },
        changeChannelData: function(id, value) {
            this.storedChannels[id] = value;
        },
        liClick: function(event) {
            this.updateChannel($(event.currentTarget));
        },
        labelClick: function(event) {
            this.updateChannel($(event.currentTarget).closest('li'));
            event.stopPropagation();
            return false;
        },
        updateChannel($li) {
            var $checkbox = $li.find('input');
            var checked = ($checkbox.attr('checked')) ? false : true;
            $li.find('input').attr('checked', checked);
            this.changeChannelData($checkbox.data('id'), checked);
        },
        cancel: function () {
            this.providers.fetch();
            this.loadChannels();
        },
        save: function() {
            var invisibleChannels = [];

            $.each(this.storedChannels, function (key, value) {
                if(!value) {
                    invisibleChannels.push({
                        id: key,
                    });
                }
            });

            Views.Spinner.show();

            $.ajax({
                url: App.Settings.API_ROOT_URL + '/web/settings/channels/visibility/channels/',
                dataType : 'json',
                data: JSON.stringify({
                    invisible: invisibleChannels
                }),
                timeout: 60000,
                type: 'POST',
                error: function () {
                    alert('Save error');
                },
                complete: function () {
                    Views.Spinner.hide();
                }
            });
        },
        searchKeydown: function (e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
                this.search();
                return false;
            }
        },
        searchClick: function() {
            this.search();
        },
        search: function () {
            var $query = self.$('#query');
            self.$('#channels_list li').each(function(){
                var $this = $(this);

                if(String($this.find('.title').data('name')).toLowerCase().indexOf(String($query.val()).toLowerCase()) > -1 || $query.val().length == 0) {
                    $this.removeClass('hidden');

                } else {
                    $this.addClass('hidden');
                }
            });
        },
        emptyQuery: function() {
            var $query = self.$('#query');
            $query.val('');
            this.search();
        },
        transponderSearchKeydown: function (e) {
            e.stopPropagation();
            if (e.keyCode == 13) {
                this.transponderSearch(e);
                return false;
            }
        },
        transponderSearch: function(e) {
            e.stopPropagation();
            var $query = self.$('#transponder_query');

            self.$('#transponders_list li').each(function(){
                var $this = $(this);

                if(String($this.find('a').data('name')).toLowerCase().indexOf(String($query.val()).toLowerCase()) > -1 || $query.val().length == 0) {
                    $this.removeClass('hidden');

                } else {
                    $this.addClass('hidden');
                }
            });
            var $info = self.$('.block_search_dropdown_info');

            if(self.$('#transponders_list li').length == self.$('#transponders_list li.hidden').length) {
                $info.removeClass('hidden').next().addClass('hidden');
            } else {
                $info.addClass('hidden').next().removeClass('hidden');
            }
        },
        transponderEmptyQuery: function(e) {
            e.stopPropagation();
            var $query = self.$('#transponder_query');

            $query.val('');
            this.transponderSearch(e);
        },
        keyup: function(e) {
            var $this = $(e.target);
            if($this.val().length) {
                $this.next().removeClass('hidden');
            } else {
                $this.next().addClass('hidden');
            }
        },
    });


}).call(this);
