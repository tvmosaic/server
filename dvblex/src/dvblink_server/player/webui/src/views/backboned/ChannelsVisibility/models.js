(function () {

    var App = window.App || (window.App = {});
    var Models = App.Models || (App.Models = {});

    Models.ChannelVisibility = {};

    /* Providers */

    var Provider = Models.ChannelVisibility.Provider = Backbone.Model.extend({
        parse: function (response) {
            this.reloadChildren();
            return response;
        },
        reloadChildren: function(loadTransponders) {
            if(typeof loadTransponders !== 'undefined' && loadTransponders) {
                this.trigger('sync:transponders', this);
            }
            this.trigger('sync:channels', this);
        }
    });

    Models.ChannelVisibility.Providers = Backbone.Collection.extend({
        model: Provider,
        url: App.Settings.API_ROOT_URL + '/web/settings/channels/visibility/providers/',
        parse: function (response) {
            response.unshift({
                id: '',
                name: window.i18n.all,
            });
            return response;
        },
    });

    /* Transponders */

    var Transponder = Models.ChannelVisibility.Transponder = Backbone.Model.extend({
        parse: function (response) {
            return response;
        },
        reloadChannels: function() {
            this.trigger('sync:channels', this);
        }
    });

    Models.ChannelVisibility.Transponders = Backbone.Collection.extend({
        initialize: function(attributes) {
            this.providerId = (typeof attributes !== 'undefined' && typeof attributes.providerId !== 'undefined') ? attributes.providerId : null;
        },
        model: Transponder,
        url: function() {
            return App.Settings.API_ROOT_URL + '/web/settings/channels/visibility/transponders/'
                + ((this.providerId) ? '?provider=' + encodeURIComponent(this.providerId) : '');
        },
        parse: function (response) {
            response.unshift({
                id: '',
                name: window.i18n.all,
            });
            return response;
        },
    });

    /* Channels */

    Models.ChannelVisibility.Channels = Backbone.Collection.extend({
        initialize: function(attributes) {
            this.providerId = (typeof attributes !== 'undefined' && typeof attributes.providerId !== 'undefined') ? attributes.providerId : null;
            this.transponderId = (typeof attributes !== 'undefined' && typeof attributes.transponderId !== 'undefined') ? attributes.transponderId : null;
            this.storedChannels = (typeof attributes !== 'undefined' && typeof attributes.storedChannels !== 'undefined') ? attributes.storedChannels : null;
        },
        url: function() {
            return App.Settings.API_ROOT_URL + '/web/settings/channels/visibility/channels/?'
                + ((this.providerId) ? 'provider=' + encodeURIComponent(this.providerId) + '&' : '')
                + ((this.transponderId) ? 'transponder=' + encodeURIComponent(this.transponderId) : '');
        },
        parse: function (response) {
            var allChannels = {};
            var here = this;
            response = response.map(channel => ({
                ...{
                    number: '',
                },
                ...channel,
            })).sort((a, b) => a.name.localeCompare(b.name));
            $.each(response, function(key, value) {
                allChannels[value.id] = value.visible;
                if(here.storedChannels) {
                    if(value.id in here.storedChannels) {
                        response[key]['visible'] = here.storedChannels[value.id];
                    }
                }
            });

            this.allChannels = allChannels;

            return response;
        },
        allChannels: null
    });
}).call(this);
