
const TVMSERVER_URL = process.env.NODE_ENV === 'development' ? 'http://localhost:9270' : ''

const MONITORING_URL = '/web/#/monitoring'


const LANGUAGES = [
    {id: 'de', name: 'Deutsch'},
    {id: 'en', name: 'English'},
    {id: 'nl', name: 'Nederlands'},
    {id: 'pl', name: 'Polski'},
    {id: 'ru', name: 'Русский'},
    {id: 'uk', name: 'Українська'},
]

const PRODUCTS_STATE_EXTEND = {
    free: {
        'status': 'status.free',
        'description': 'description.free',
        'alert': false,
        'trial': false,
        'activate': false,
        'disabled': false,
        'info': false,
    },
    trial: {
        'status': 'status.trial',
        'description': 'description.trial',
        'alert': false,
        'trial': true,
        'activate': true,
        'disabled': false,
        'info': true,
    },
    registered: {
        'status': 'status.registered',
        'description': 'description.registration',
        'alert': false,
        'trial': false,
        'activate': true,
        'disabled': true,
        'info': true,
    },
    expired: {
        'status':'status.expired',
        'description':'description.expired',
        'alert':true,
        'trial':true,
        'activate':true,
        'disabled': false,
        'info': true,
    },
    no_license_file: {
        'status':'status.noLicenseFile',
        'description':'description.noLicenseFile',
        'alert':false,
        'trial':true,
        'activate':true,
        'disabled': false,
        'info': true,
    },
    no_subscription: {
        'status': 'status.noSubscription',
        'description': 'description.noSubscription',
        'alert': false,
        'trial': false,
        'activate': true,
        'disabled': false,
        'info': true,
    },
    subscribed: {
        'status': 'status.subscribed',
        'description': 'description.subscribed',
        'alert': false,
        'trial': false,
        'activate': true,
        'disabled': false,
        'info': true,
    },
    subscription_expired: {
        'status': 'status.subscriptionExpired',
        'description': 'description.subscriptionExpired',
        'alert': true,
        'trial': false,
        'activate': true,
        'disabled': false,
        'info': true,
    },
    subscription_wrong_fingerprint: {
        'status': 'status.wrongFingerprint',
        'description': 'description.wrongFingerprint',
        'alert': true,
        'trial': true,
        'activate': true,
        'disabled': false,
        'info': true,
    },
    no_coupon: {
        'status': 'status.couponRequired',
        'description': 'description.couponRequired',
        'alert': true,
        'trial': false,
        'activate': true,
        'disabled': false,
        'info': true,
    },
}

const ACTIVATION_SERVER_SUCCESS = [
    'success',
    'in_progress',
    'need_user_info',
]
const ACTIVATION_SERVER_ERRORS = {
    invalid_login: 'errors.incorrectEmailOrPassword',
    invalid_coupon: 'errors.invalidCoupon',
    already_used_coupon: 'errors.alreadyUsedCoupon',
    other_product_coupon: 'errors.otherProductCoupon',
    email_already_in_use: 'errors.emailAlreadyInUse',
    no_activations_available: 'errors.activationErrorNoActivationsAvailable',
    already_activated: 'errors.activationErrorAlreadyActivated',
    no_server_connection: 'errors.noServerConnection',
    unknown: 'errors.activationServerError',
}

const WRONG_LICENSE_STATE = [
    'wrong_fingerprint', 'expired', 'subscription_expired', 'subscription_wrong_fingerprint', 'coupon_wrong_fingerprint',
]


function currentLanguage () {
    return localStorage.settignsLanguage || 'en'
}

function request (path, data, type) {
    return $.ajax({
        url: TVMSERVER_URL + path,
        type: type || 'GET',
        dataType : 'json',
        data: data,
        headers: {
            'Accept-Language': currentLanguage(),
        },
    })
}

function requestMobile (command, authorization, params) {
    return new Promise((resolve, reject) => {
        var data = {
            command: command
        };

        if (params) {
            data.xml_param = '<' + command + '>';
            data.xml_param += $.map(params, function (value, param) {
                return '<' + param + '>' + value + '</' + param + '>';
            }).join('');
            data.xml_param += '</' + command + '>';
        }

        $.ajax({
            url: `${ TVMSERVER_URL }/mobile?command=${ command }`,
            type: 'GET',
            dataType: 'xml',
            data: data,
            timeout: 60000,
            beforeSend: (xhr) => {
                if (authorization?.password?.length > 0) {
                    xhr.setRequestHeader('Authorization', 'Basic ' + btoa(
                        authorization.username + ':' + authorization.password
                    ))
                }
            },
            success: function (response) {
                var $response = $(response);
                var statusCode = $response.find('status_code').text();
                if (parseInt(statusCode) !== 0) {
                    reject('status code not 0', statusCode);
                }
                response = $response.find('xml_result').text();
                if (response.length < 1) {
                    reject('response length < 1', response);
                }
                resolve(response);
            },
            error: function (xhr) {
                reject('ajax error', xhr.responseText);
            }
        })
    });
}


export default Object.assign({
    API_ROOT_URL: TVMSERVER_URL,
    Directories: {
        get: (path) => request('/web/settings/directories/', {path: path})
    },
    General: {
        language () {
            return currentLanguage()
        },
        get (callback) {
            request('/web/settings/general/')
                .then(response => {
                    response.languages = LANGUAGES
                    response.language = currentLanguage()
                    callback(response)
                })
        },

        save (settings, callback) {
            localStorage.settignsLanguage = settings.language
            delete settings.language
            request('/web/settings/general/', JSON.stringify(settings), 'POST')
                .then(() => callback())
        },

        downloadLogFile (cb) {
            cb(TVMSERVER_URL + '/web/settings/general/log/')
        },

        checkNewSoftware (callback) {
            request('/web/settings/general/checkupdates/')
                .then(response => callback(response))
        }
    },

    Network: {
        get (callback) {
            request('/web/settings/network/')
                .then(response => callback(response))
        },

        save (settings, callback) {
            $.ajax({
                url: TVMSERVER_URL + '/web/settings/network/',
                type: 'POST',
                dataType : 'json',
                data: JSON.stringify(settings),
                success: function () {
                    callback();
                }
            })
        },

        checkports (cb) {
            cb(TVMSERVER_URL + '/web/settings/network/checkports/')
        }
    },

    Transcoding: {
        get (callback) {
            request('/web/settings/transcoding/')
                .then(response => callback(response))
        },

        save (settings, callback) {
            $.ajax({
                url: TVMSERVER_URL + '/web/settings/transcoding/',
                type: 'POST',
                dataType : 'json',
                data: JSON.stringify(settings),
                success: function () {
                    callback();
                }
            })
        }
    },

    Recorder: {
        get (callback) {
            request('/web/settings/recorder/')
                .then(response => callback(response))
        },

        save (settings, callback) {
            $.ajax({
                url: TVMSERVER_URL + '/web/settings/recorder/',
                type: 'POST',
                dataType : 'json',
                data: JSON.stringify(settings),
                success: function () {
                    callback();
                }
            })
        },

        repairDB (callback) {
            request('/web/settings/recorder/repairdb/', null, 'POST')
                .then(() => callback())
        }
    },

    Backup: {
        backup (callback) {
            callback(TVMSERVER_URL + '/web/settings/backup/')
        },

        restore (file) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/backup/',
                type: 'POST',
                data: file,
                processData: false,
                contentType: false,
            })
        }
    },

    EPGSources: {
        get (callback) {
            request('/web/settings/epg/sources/')
                .then(response => callback(response))
        },

        refresh (sourceID, callback) {
            $.ajax({
                url: TVMSERVER_URL + '/web/settings/epg/sources/refresh/',
                type: 'POST',
                data: {
                    id: sourceID,
                },
                dataType : 'json',
                success: () => callback(),
            })
        },

        getSettings (sourceID, callback) {
            $.ajax({
                url: TVMSERVER_URL + '/web/settings/epg/sources/settings/',
                type: 'GET',
                data: {
                    id: sourceID,
                },
                dataType : 'json',
                success: (response) => callback(response),
            })
        },

        setSettings (sourceID, settings, callback) {
            $.ajax({
                url: TVMSERVER_URL + '/web/settings/epg/sources/settings/',
                type: 'POST',
                dataType : 'json',
                data: JSON.stringify({
                    id: sourceID,
                    values: settings,
                }),
                success: () => callback(),
            })
        },
    },

    Products: {
        isWrong () {
            return new Promise(resolve => {
                this.get(products => {
                    resolve(products.some(product => WRONG_LICENSE_STATE.indexOf(product.license_state) > -1))
                })
            })
        },
        get (callback) {
            request('/web/settings/products/')
                .then(products => callback(products.map((product) => ({
                        ...product,
                        ...PRODUCTS_STATE_EXTEND[product.license_state]
                }))))
        },

        trial (productID, callback) {
            $.ajax({
                url: TVMSERVER_URL + '/web/settings/products/trial/',
                type: 'POST',
                dataType : 'json',
                data: {
                    id: productID,
                },
                success: (result) => callback(result),
            })
        },

        activate (productID, params, callback) {
            $.ajax({
                url: TVMSERVER_URL + '/web/settings/products/activate/',
                type: 'POST',
                dataType : 'json',
                data: {
                    id: productID,
                    ...params,
                },
                success: (result) => {
                    if (ACTIVATION_SERVER_SUCCESS.indexOf(result.result) < 0) {
                        result.message = ACTIVATION_SERVER_ERRORS[
                            result.result in ACTIVATION_SERVER_ERRORS ? result.result : 'unknown'
                        ]
                    }
                    callback(result)
                },
            })
        },

        downloadProductFile (product, callback) {
            callback(TVMSERVER_URL + '/web/settings/products/file/?id=' + product)
        }
    },

    // TODO: combine with EPGSources into one area EPG / ChannelMapping and EPG / Sources
    EPGChannelMapping: {
        getChannels () {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/epg/mapping/channels/',
                type: 'GET',
                dataType : 'json',
            })
                .then((result) => {
                    return result.map((channel) => {
                        channel.epg_mapping = {
                                ...{
                                    epg_source_id: '',
                                    'default': true,
                                    epg_channel_id: '',
                                    epg_source_name: '',
                                    epg_channel_name: '',
                                },
                                ...channel.epg_mapping,
                        }
                        return channel
                    })

                })
        },

        setChannels (channels) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/epg/mapping/channels/',
                type: 'POST',
                dataType : 'json',
                data: JSON.stringify(channels),
            })
        },

        getFavorites () {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/epg/mapping/favorites/',
                type: 'GET',
                dataType : 'json',
            })
        },

        getChannelsByFavorite (id) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/epg/mapping/favorites/channels',
                type: 'GET',
                dataType : 'json',
                data: {
                    id: id,
                },
            })
                .then(channels => channels.map(channel => channel.id))
        },

        getSources () {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/epg/mapping/epgsources/',
                type: 'GET',
                dataType : 'json',
            })
                .then(channels => channels.filter((channel) => !channel.is_default))
        },

        getEPGChannels (id) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/epg/mapping/epgsources/channels',
                type: 'GET',
                dataType : 'json',
                data: {
                    id: id,
                },
            })
        },

        searchEPG (epgSource, channelID) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/epg/mapping/epgsearch/',
                type: 'GET',
                dataType : 'json',
                data: {
                    epg_source: epgSource,
                    channel: channelID,
                },
            })
        },
    },

    ChannelsProperties: {
        getChannels () {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/properties/channels/',
                type: 'GET',
                dataType : 'json',
            })
                .then((result) => {
                    return result.map((channel) => {
                        channel.properties = {
                            ...{
                                overwrite_number: '',
                                overwrite_name: '',
                            },
                            ...channel.properties,
                        }
                        return channel
                    })
                })
        },

        setChannels (channels) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/properties/channels/',
                type: 'POST',
                dataType : 'json',
                data: JSON.stringify(channels),
            })
        },

        getFavorites () {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/properties/favorites/',
                type: 'GET',
                dataType : 'json',
            })
        },

        getChannelsByFavorite (id) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/properties/favorites/channels',
                type: 'GET',
                dataType : 'json',
                data: {
                    id: id,
                },
            })
                .then(channels => channels.map(channel => channel.id))
        },
    },

    ChannelsLogos: {
        getChannels () {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/logos/channels/',
                type: 'GET',
                dataType : 'json',
            })
                .then((result) => {
                    return result.map((channel) => {
                        channel.logo = {
                                ...{
                                    default: '',
                                    id: '',
                                    url: '',
                                },
                                ...channel.logo,
                        }
                        return channel
                    })

                })
        },

        setChannels (channels) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/logos/channels/',
                type: 'POST',
                dataType : 'json',
                data: JSON.stringify(channels),
            })
        },

        getFavorites () {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/logos/favorites/',
                type: 'GET',
                dataType : 'json',
            })
        },

        getChannelsByFavorite (id) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/logos/favorites/channels',
                type: 'GET',
                dataType : 'json',
                data: {
                    id: id,
                },
            })
                .then(channels => channels.map(channel => channel.id))
        },

        getLogosPackages () {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/logos/packs/',
                type: 'GET',
                dataType : 'json',
            })
        },

        getLogosPackage (id) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/logos/packs/logos/',
                type: 'GET',
                dataType : 'json',
                data: {
                    id: id,
                },
            })
        },

        searchLogo (logoPackage, channelID) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/logos/logosearch/',
                type: 'GET',
                dataType : 'json',
                data: {
                    pack: logoPackage,
                    channel: channelID,
                },
            })
        },
    },

    SendTo: {
        capabilities: () => request('/web/connect/server/capabilities/'),
        getTargets: () => request('/web/settings/sendto/targets/'),
        setTargets: targets => request('/web/settings/sendto/targets/', JSON.stringify(targets), 'POST'),
        getDefaultComskipParams: () => request('/web/settings/sendto/comskip/default/'),
        getComskip: params => request('/web/settings/sendto/comskip/', JSON.stringify(params), 'POST'),
        activate: (workunit, token) => request('/web/settings/sendto/activateworkunit/', JSON.stringify({
            workunit,
            activation_info: {
                id: 'token',
                value: token,
            }
        }), 'POST'),
    },


    ChannelsScan: {
        getDevices () {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/devices/',
                type: 'GET',
                dataType : 'json',
            })
        },

        getTemplates () {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/templates/',
                type: 'GET',
                dataType : 'json',
            })
        },

        createDevice (params) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/devices/',
                type: 'POST',
                dataType : 'json',
                data: JSON.stringify(params),
            })
        },

        removeDevice (deviceID) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/devices/',
                type: 'DELETE',
                dataType : 'json',
                data: {
                    id: deviceID,
                },
            })
        },

        getSettingsDevice (deviceID) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/devices/settings/',
                type: 'GET',
                dataType : 'json',
                data: {
                    id: deviceID,
                },
            })
        },

        setSettingsDevice (deviceID, params) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/devices/settings/',
                type: 'POST',
                dataType : 'json',
                data: JSON.stringify({
                    id: deviceID,
                    values: {
                        ...params,
                    }
                }),
            })
        },

        getScanners (deviceID) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/scanners/',
                type: 'GET',
                dataType : 'json',
                data: {
                    device: deviceID,
                },
            })
        },

        getDeviceStatus (deviceID) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/devices/status/',
                type: 'GET',
                dataType : 'json',
                data: {
                    id: deviceID,
                },
            })
        },

        startScan (deviceID, params) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/start/',
                type: 'POST',
                dataType : 'json',
                data: JSON.stringify({
                    device: deviceID,
                    values: params,
                }),
            })
        },

        cancelScan (deviceID) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/cancel/',
                type: 'POST',
                dataType : 'json',
                data: {
                    device: deviceID,
                },
            })
        },

        applyScan (deviceID) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/apply/',
                type: 'POST',
                dataType : 'json',
                data: {
                    device: deviceID,
                },
            })
        },

        getNetworks (deviceID) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/networks/',
                type: 'GET',
                dataType : 'json',
                data: {
                    device: deviceID,
                },
            })
        },

        getRescanParams (deviceID, providerID) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/rescan/settings/',
                type: 'GET',
                dataType : 'json',
                data: {
                    device: deviceID,
                    provider: providerID,
                },
            })
        },

        rescan (deviceID, providerID, params) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/rescan/start/',
                type: 'POST',
                dataType : 'json',
                data: JSON.stringify({
                    device: deviceID,
                    provider: providerID,
                    values: params,
                }),
            })
        },


        removeProvider (deviceID, providerID) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/scan/providers/',
                type: 'DELETE',
                dataType : 'json',
                data: {
                    id: providerID,
                    device: deviceID,
                },
            })
        },

        monitoring () {
            return Promise.resolve(MONITORING_URL)
        },

    },

    ChannelsFavorites: {

        getFavorites () {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/favorites/',
                type: 'GET',
                dataType : 'json',
            })
                .then(favorites => {
                    return favorites.map(favorite => ({
                        ...favorite,
                        channels: favorite.channels.map(channel => channel.id),
                    }))
                })
        },

        setFavorites (favorites) {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/favorites/',
                type: 'POST',
                dataType : 'json',
                data: JSON.stringify(favorites.map(favorite => ({
                    ...favorite,
                    channels: favorite.channels.map(channel => ({id: channel})),
                }))),
            })
        },

        getChannels () {
            return $.ajax({
                url: TVMSERVER_URL + '/web/settings/channels/favorites/allchannels/',
                type: 'GET',
                dataType : 'json',
            })
        },

    },

    Monitoring: {
        chackAuth: (authorization) => (
            $.ajax({
                url: TVMSERVER_URL + '/mobile?command=get_server_info',
                timeout: 60000,
                type: 'GET',
                dataType: 'xml',
                data: { command: 'get_server_info' },
                beforeSend: (xhr) => {
                    if (authorization?.password?.length > 0) {
                        xhr.setRequestHeader('Authorization', 'Basic ' + btoa(
                            authorization.username + ':' + authorization.password
                        ))
                    }
                }
            })
                .catch((xhr) => {
                    throw new Error(
                        xhr.status === 401 ? 'unauthorized' : 'unknown'
                    )
                })
        ),
        getDevices: (authorization) => (
            requestMobile('get_devices', authorization)
                .then((response) => {
                    const devices = $(response).find('device')
                    return Array.from(devices)
                        .map((device) => {
                            const $this = $(device)
                            const id = String($this.find('id').text())
                            return {
                                id,
                                name: $this.find('name').text(),
                                status: (
                                    $this.find('status').text() === 'streaming' ?
                                        'busy' : 'idle'
                                ),
                            }
                        })
                })
        ),
        getDeviceStatus: (deviceId, authorization) => (
            requestMobile('get_device_status', authorization, {
                device: deviceId
            })
                .then((response) => {
                    var channels = $(response).find('channel')
                    return Array.from(channels)
                        .map((channel) => {
                            const $this = $(channel)
                            const id = String($this.attr('id'))
                            return {
                                id,
                                name: $this.attr('name'),
                                lock: $this.attr('lock') != '0',
                                consumers: parseInt($this.attr('consumers')),
                                bitrate: parseInt($this.attr('bitrate')),
                                packets: parseInt($this.attr('packets')),
                                errors: parseInt($this.attr('errors')),
                                discontinued: parseInt($this.attr('dscnt')),
                                quality: parseInt($this.attr('quality')),
                                strength: parseInt($this.attr('strength')),
                            }
                        })
                })
        )
    }
})
