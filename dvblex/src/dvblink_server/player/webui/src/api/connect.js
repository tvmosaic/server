
import moment from 'moment'

import Tools from '@/tools.js'
import Settings from './settings.js'

const TVMSERVER_URL = process.env.NODE_ENV === 'development' ? 'http://localhost:9270' : ''

const LIBRARY_PROGRAMS_PER_PAGE = 200
const PROGRAM_SEARCH_MAX_COUNT = LIBRARY_PROGRAMS_PER_PAGE
const SETTINGS = {
    languages: [
        {id: "0", name: "Manual"},
        {id: "bul", name: "bul"},
        {id: "ces", name: "ces"},
        {id: "dan", name: "dan"},
        {id: "deu", name: "deu"},
        {id: "ell", name: "ell"},
        {id: "eng", name: "eng"},
        {id: "fin", name: "fin"},
        {id: "fra", name: "fra"},
        {id: "hrv", name: "hrv"},
        {id: "hun", name: "hun"},
        {id: "ita", name: "ita"},
        {id: "nld", name: "nld"},
        {id: "nor", name: "nor"},
        {id: "pol", name: "pol"},
        {id: "rus", name: "rus"},
        {id: "slk", name: "slk"},
        {id: "slv", name: "slv"},
        {id: "spa", name: "spa"},
        {id: "swe", name: "swe"},
        {id: "tur", name: "tur"},
        {id: "ukr", name: "ukr"},
    ],
    scales: [
        {id: '1', name: '1'},
        {id: '2', name: '½'},
        {id: '4', name: '¼'},
    ],
    bitrates: [
        {id: '384', name: '384'},
        {id: '512', name: '512'},
        {id: '1024', name: '1024'},
        {id: '1536', name: '1536'},
        {id: '2048', name: '2048'},
        {id: '2560', name: '2560'},
        {id: '3072', name: '3072'},
        {id: '3584', name: '3584'},
        {id: '4096', name: '4096'},
    ],
}
const SETTINGS_DEFAULTS = {
    supported: false,
    enabled: true,
    language: 'eng',
    scale: '1',
    bitrate: '2048',
}

const DEFAULT_VOLUME = 1


function getClientID () {
    let clientID = localStorage.clientID
    if (!clientID) {
        clientID = Tools.createGUID()
        localStorage.clientID = clientID
    }
    return clientID
}


export default Object.assign({
    Common: {
        decodeChannel: (channel) => atob(channel),
        encodeChannel: (channel) => btoa(channel).replace(/=/g, ''),
    },

    Library: {
        getSources: () => $.ajax({
            url: TVMSERVER_URL + '/web/connect/library/sources/',
            type: 'GET',
            dataType : 'json',
        }),

        getFilters: (sourceID) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/library/filters/',
            type: 'GET',
            dataType : 'json',
            data: {
                id: sourceID,
            },
        })
            .then(response => response
                  .map(category => ({
                      ...category,
                      items: category.items.sort((a, b) => a.name.localeCompare(b.name))
                  }))),

        get: (objectID, page, search) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/library/programs/',
            type: 'GET',
            dataType : 'json',
            data: {
                id: objectID,
                offset: (page - 1) * LIBRARY_PROGRAMS_PER_PAGE,
                count: LIBRARY_PROGRAMS_PER_PAGE,
                search,
            },
        })
            .then(({meta, programs}) => ({
                meta: {
                    ...meta,
                    per_page: LIBRARY_PROGRAMS_PER_PAGE,
                },
                programs: programs.map(program => ({
                    description: '',
                    ...program
                }))
            })),

        programDetails: (objectID) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/library/details/',
            type: 'GET',
            dataType : 'json',
            data: {
                id: objectID,
            },
        }),
    },

    TVRecordings: {
        get: (objectID) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/tvrecordings/programs/',
            type: 'GET',
            dataType : 'json',
            data: {
                id: objectID,
            },
        })
            .then(programs => programs.map(program => ({
                    description: '',
                    ...program,
                    start: moment.unix(program['start']),
                }))
            ),

        getCategories: () => $.ajax({
            url: TVMSERVER_URL + '/web/connect/tvrecordings/categories/',
            type: 'GET',
            dataType : 'json',
            headers: {
                'Accept-Language': Settings.General.language(),
            },
        }),

        remove: records => $.ajax({
            url: TVMSERVER_URL + '/web/connect/tvrecordings/remove/',
            type: 'POST',
            dataType: 'json',
            data: {
                programs: JSON.stringify(records),
            },
        }),

        stop: record => $.ajax({
            url: TVMSERVER_URL + '/web/connect/tvrecordings/stop/',
            type: 'POST',
            dataType: 'json',
            data: {
                id: record,
            },
        }),
    },

    Schedules: {
        get: (objectID, page) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/schedules/',
            type: 'GET',
            dataType : 'json',
            data: {
                page: page,
            },
        })
            .then((schedules) => schedules.map(schedule => ({
                ...schedule,
                days: schedule.days.filter(day => day !== 0),

                startTime: moment.unix(schedule['startTime']),
                endTime: moment.unix(schedule['endTime']),

                recordings: schedule.recordings
                    .map(record => ({
                        ...record,
                        program: {
                            ...record.program,
                            is_conflicting: record['conflicting'],
                            start: moment.unix(record.program['start']),
                        },
                    }))
                    .filter(record => record.program.isRecord),
            }))),

        getFreeSpace: () => $.ajax({
            url: TVMSERVER_URL + '/web/connect/schedules/diskspace/',
            type: 'GET',
            dataType : 'json',
        })
            .then(response => ({
                total: response.total * 1024,
                free: response.free * 1024,
            })),

        getChannels: () => $.ajax({
            url: TVMSERVER_URL + '/web/connect/schedules/channels/',
            type: 'GET',
            dataType : 'json',
        }),

        appendManual: (params) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/schedules/',
            type: 'POST',
            dataType : 'json',
            data: JSON.stringify({
                ...params,
                type: 'manual',
            }),
        }),

        appendEPG: (params) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/schedules/',
            type: 'POST',
            dataType : 'json',
            data: JSON.stringify({
                ...params,
                channel: atob(params.channel),
                type: 'epg',
            }),
        }),

        appendPattern: (params) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/schedules/',
            type: 'POST',
            dataType : 'json',
            data: JSON.stringify({
                ...params,
                ...(params.channel ? {channel: atob(params.channel)} : {}),
                type: 'pattern',
            }),
        }),

        removeRecording: (recordingID, full=false) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/schedules/',
            type: 'DELETE',
            dataType : 'json',
            data: JSON.stringify({
                recording: recordingID,
                ...(full ? {full: true} : {}),
            }),
        }),
        removeSchedule: (scheduleID, full=false) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/schedules/',
            type: 'DELETE',
            dataType : 'json',
            data: JSON.stringify({
                schedule: scheduleID,
                ...(full ? {full: true} : {}),
            }),
        }),

        editSchedule: (params) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/schedules/edit/',
            type: 'POST',
            dataType : 'json',
            data: JSON.stringify(params),
        }),
    },

    TVGuide: {
        getFavorites: () => $.ajax({
            url: TVMSERVER_URL + '/web/connect/tvguide/favorites/',
            type: 'GET',
            dataType : 'json',
        }),

        getGenres: () => $.ajax({
            url: TVMSERVER_URL + '/web/connect/search/genres/',
            type: 'GET',
            dataType : 'json',
        }),

        getChannels: () => $.ajax({
            url: TVMSERVER_URL + '/web/connect/tvguide/channels/',
            type: 'GET',
            dataType : 'json',
        })
            .then(channels => channels.map(channel => ({
                id: btoa(channel.id).replace(/=/g, ''),
                extid: btoa(channel.id).replace(/=/g, ''),

                locked: channel.lock,
                logo: channel.logo,
                name: channel.name,
                number: parseInt(channel.number && channel.number.split('.')[0]) || -1,
                subNumber: parseInt(channel.number && channel.number.split('.')[1]) || -1,
                type: 0,
            }))),

        getPrograms: ({intervals}) => Promise.all([intervals, $.ajax({
            url: TVMSERVER_URL + '/web/connect/tvguide/programs/',
            type: 'POST',
            dataType : 'json',
            data: JSON.stringify({
                start: intervals[0][0],
                end: intervals[0][1],
            }),
        })])
            .then(([interval, response]) => [{
                interval: interval[0],
                channels: response.channels.map(channel => ({
                    cid: btoa(channel.id).replace(/=/g, ''),
                    programs: channel.programs.map(program => ({
                        ...program,
                        program_id: program.id,
                        start_time: program.start,
                    })),
                }))
            }]),

        search: (params) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/tvguide/programs/',
            type: 'POST',
            dataType : 'json',
            data: JSON.stringify({
                ...params,
                maxCount: PROGRAM_SEARCH_MAX_COUNT,
            }),
        })
            .then(response => ({
                channels: response.channels.map(channel => ({
                    cid: btoa(channel.id).replace(/=/g, ''),
                    programs: channel.programs.map(program => ({
                        ...program,
                        start: moment.unix(program['start']),
                        end: moment.unix(program['start'] + program['duration']),
                    })),
                }))
            })),
    },

    Stream: {
        getUrl: async (query, transcoding) => {
            if (!transcoding.enabled) {
                return
            }
            if (query.channel) {
                query.channel = atob(query.channel)
            }
            return $.ajax({
                url: TVMSERVER_URL + '/web/connect/stream/',
                type: 'POST',
                dataType : 'json',
                data: JSON.stringify({
                    client: getClientID(),
                    ...query,
                    params: {
                        bitrate: parseInt(transcoding.bitrate),
                        scaleFactor: parseInt(transcoding.scale),
                        lang: transcoding.language || transcoding.customLanguage,
                        format: query.format,
                    },
                }),
            })
                .then(response => response.url)
        }
    },

    Settings: {
        Transcoding: {
            get: () => $.ajax({
                url: TVMSERVER_URL + '/web/connect/server/capabilities/',
                type: 'GET',
                dataType : 'json',
            })
                .then(capabilities => {
                    let localSettings = {
                        ...SETTINGS_DEFAULTS,
                        ...JSON.parse(localStorage.transcodingSettings || '{}')
                    }
                    localSettings.supported = capabilities.transcoding
                    localSettings.enabled = localSettings.supported && localSettings.enabled
                    return {
                            ...SETTINGS,
                            ...localSettings,
                    }}),

            set (settings) {
                localStorage.transcodingSettings = JSON.stringify(settings)
            },
        },

        Global: {
            get: () => new Promise(resolve => {
                setTimeout(() => resolve({
                    date: localStorage['settings.global.date'] || '',
                    dates: [
                        {id: '', name: 'Auto'},
                        {id: 'DD MMM', name: 'DD MMM (23 Feb)'},
                        {id: 'DD.MM', name: 'DD.MM (02.23)'},
                        {id: 'DD.MM.YYYY', name: 'DD.MM.YYYY (23.02.2019)'},
                        {id: 'DD/MM/YYYY', name: 'DD/MM/YYYY (23/02/2019)'},
                        {id: 'MM.DD', name: 'MM.DD (02.23)'},
                        {id: 'MM.DD.YYYY', name: 'MM.DD.YYYY (02.23.2019)'},
                        {id: 'MM/DD', name: 'MM/DD (02/23)'},
                        {id: 'DD/MM', name: 'MM/DD/YYYY (02/23/2019)'},
                        {id: 'MM/DD/YYYY', name: 'MM/DD/YYYY (02/23/2019)'},
                    ],
                    time: localStorage['settings.global.time'] || '',
                    times: [
                        {id: '', name: 'Auto'},
                        {id: 'HH:mm', name: 'HH:MM (16:42)'},
                        {id: 'hh:mm A', name: 'hh:MM AM/PM (04:42 PM)'},
                        {id: 'hh:mm a', name: 'hh:MM am/pm (04:42 pm)'},
                    ],
                }), 300)
            }),

            set (settings) {
                localStorage['settings.global.time'] = settings.time
                localStorage['settings.global.date'] = settings.date
            },
        },

        Player: {
            setVolume (volume) {
                localStorage['settings.player.volume'] = volume
            },
            getVolume () {
                const oldVolume = localStorage['settings.player.volume']
                return oldVolume === undefined ? DEFAULT_VOLUME : oldVolume
            },
        },
    },

    SendTo: {
        getItems: () => $.ajax({
            url: TVMSERVER_URL + '/web/connect/sendto/items/',
            type: 'GET',
            dataType : 'json',
        }),

        removeItems: (ids) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/sendto/items/',
            data: JSON.stringify(ids),
            type: 'DELETE',
            dataType : 'json',
        }),

        cancelItems: (ids) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/sendto/items/canceled/',
            data: JSON.stringify(ids),
            type: 'POST',
            dataType : 'json',
        }),

        getTargets: () => $.ajax({
            url: TVMSERVER_URL + '/web/connect/sendto/targets/',
            type: 'GET',
            dataType : 'json',
        }),

        addItems: (target, recordings) => $.ajax({
            url: TVMSERVER_URL + '/web/connect/sendto/items/',
            data: JSON.stringify({
                target,
                recordings,
            }),
            type: 'POST',
            dataType : 'json',
        }),
    },

})
