
/*
 * Subtlebox - modern Bootstrap dropdown menu.
 * andrey@dvblink.com
 */

/* global jQuery */

/* eslint-disable */
(function ($, undefined) {
/* eslint-enable */

    var ITEM_HEIGHT = 30;
    var DEFAULT_MENU_MAX_HEIGHT = 350;

    var Subtlebox = function (element, options) {
        this.$element = $(element);
        this.options = $.extend({}, Subtlebox.DEFAULTS, options);

        this.$icons = this.$element.find('.dropdown-toggle .subtlebox-icon');
        this.$texted = this.$element.find('.dropdown-toggle .subtlebox-texted');
        this.originalIcon = this.getIconName(this.$icons);

        this.originalLabel = '';
        var $label = this.$element.find('.subtlebox-label');
        if ($label.length > 0) {
            this.originalLabel = this.$element.find('.subtlebox-label').first().text();
        }

        var $dropdownMenu = this.$element.find('.dropdown-menu')
        this.$element.find('.dropdown-toggle')
            .on('click', $.proxy(function () {
                var height = (Math.floor(
                        ($(window).height() - $dropdownMenu.offset().top) / ITEM_HEIGHT
                ) - 2) * ITEM_HEIGHT + (ITEM_HEIGHT * 0.5);
                height = Math.max(Math.min(height, DEFAULT_MENU_MAX_HEIGHT), ITEM_HEIGHT);
                $dropdownMenu.css({ 'max-height': height });
            }, this));

        this.$element
            .on('change.subtlebox', '.dropdown-menu input',  $.proxy(function (event) {
                this.applyNewIcon($(event.currentTarget));
            }, this))
            .on('change.subtlebox', '.dropdown-menu input',  $.proxy(this.saveState, this));

        this.refresh();
    };

    Subtlebox.DEFAULTS = {};

    Subtlebox.prototype.getIconName = function ($el) {
        return $.map(($el.prop('class') || '').split(' '), function (cls) {
            return ~cls.indexOf('icon-') ? cls : '';
        }).join('');
    }

    Subtlebox.prototype.applyNewIcon = function ($el) {
        var isChecked = $el.prop('checked');

        var newIconName = (
            isChecked ?
                this.getIconName($el.find('+ label .subtlebox-icon')) :
                this.originalIcon
        );
        var $label = $el.find('+ label');

        if (this.$texted.length > 0) {
            var text = $label.text();
            this.$texted.each(function () {
                $(this).text(text);
            });
            return;
        }

        if (this.originalLabel) {
            var label = isChecked ? $label.attr('title') || $label.text() : this.originalLabel;
            this.$element.find('.dropdown-toggle .subtlebox-label')
                .prop('title', label)
                .text(label);
        }

        var getIconName = this.getIconName;
        this.$icons.each(function () {
            var $this = $(this);
            $this.removeClass(getIconName($this)).addClass(newIconName);
        });
    };

    Subtlebox.prototype.getName = function () {
        // In the course of work, it can change, for example, for different roles, so we calculate.
        return this.$element.attr('id') || this.$element.attr('name');
    };

    Subtlebox.prototype.saveState = function (event) {
        var $el = $(event.currentTarget);
        var isChecked = $el.prop('checked');
        var id = isChecked ? $el.attr('id') : null;
        this.permanentSaveValue(this.getName(), id);
    };

    Subtlebox.prototype.loadState = function () {
        var id = this.permanentLoadValue(this.getName());
        if (!id) {
            // Was not set earlier, set the value from the markup.
            this.$element.find('input:checked').change();
            return;
        }
        var $lastCheckedInput = this.$element.find('input[id="' + id + '"]');
        if ($lastCheckedInput.length < 1) {
            $lastCheckedInput = this.$element.find('input:eq(0)');
        }
        $lastCheckedInput
            .prop('checked', true)
            .change();
    };

    Subtlebox.prototype.makePermanentName = function (name) {
        return 'subtlebox.' + name;
    };

    Subtlebox.prototype.permanentSaveValue = function (name, id) {
        if (!name) {
            return;
        }
        if (id) {
            window.localStorage.setItem(this.makePermanentName(name), id);
            return;
        }
        window.localStorage.removeItem(this.makePermanentName(name));
    };

    Subtlebox.prototype.permanentLoadValue = function (name) {
        return window.localStorage.getItem(this.makePermanentName(name));
    };

    Subtlebox.prototype.refresh = function () {
        var $checked = this.$element.find('.dropdown-menu input:checked');
        if ($checked.length > 0) {
            this.applyNewIcon($checked);
        }
    };

    function Plugin (option) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data('bs.subtlebox');
            var options = typeof option == 'object' && option;

            if (!data) {
                data = new Subtlebox(this, options);
                $this.data('bs.subtlebox', data);
            }
            if (typeof option == 'string') {
                data[option]();
            }
        });
    }

    $.fn.subtlebox = Plugin;
    $.fn.subtlebox.Constructor = Subtlebox;

    $(window).on('load.Subtlebox', function () {
        $('[data-subtlebox="subtlebox"]').each(function () {
            Plugin.call($(this));
        });
    });

})(jQuery);
