
import Vue from 'vue'

import router from './router'
import i18n from './i18n'

import App from './App.vue'

Vue.config.productionTip = false

/* global APPLICATION_VERSION */
/* eslint-disable no-console */
console.log('Version: ' + APPLICATION_VERSION)
/* eslint-enable no-console */

new Vue({
    router,
    i18n,
    render: h => h(App),
}).$mount('#app')
