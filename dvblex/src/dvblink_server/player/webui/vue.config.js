
const webpack = require('webpack')
const ArchivePlugin = require('webpack-archive-plugin')

let plugins = [
    new webpack.DefinePlugin({
        'APPLICATION_VERSION': JSON.stringify(require('./package.json').version),
    }),
]

if (process.env.NODE_ENV === 'production') {
    plugins.push(new ArchivePlugin({
        output: '../web/root',
        format: 'zip',
    }))
}

module.exports = {
    publicPath: '/web/',

    pluginOptions: {
        i18n: {
            locale: 'en',
            fallbackLocale: 'en',
            localeDir: 'locales',
            enableInSFC: false
        }
    },

    configureWebpack: () => {
        return {
            devtool: false,
            plugins,
        }
    },
}
