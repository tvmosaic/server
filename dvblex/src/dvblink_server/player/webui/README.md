
# TVMosaic WebUI

## Setup
```
npm install
```

### Development
```
npm run serve
```

### Build for production
```
npm run build
```
