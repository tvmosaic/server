/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <iostream>
#ifdef _WIN32
#include <objbase.h>
#include <strsafe.h>
#endif
#include <dl_common.h>
#include <dl_logger.h>
#include "reg_service.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

#ifdef _WIN32

const DWORD eventID = 0x00000001L;

void ReportToEventLog(
    DWORD localEventID,     // event identifier
    WORD  errorType,        // event category
    WORD  insertsCount,     // count of insert strings
    const char* message,    // insert strings
    const wchar_t* szServiceName)
{
    HANDLE handle = NULL;
    LPCSTR lpszMessage[1];

    lpszMessage[0] = message;

    // Get a handle to the event log.
    handle = RegisterEventSource(
        NULL,               // use local computer 
        szServiceName);     // event source name 

    if (handle == NULL) 
    {
        log_error(L"tvmosaic_reg::ReportToEventLog. Could not register the event source");
        return;
    }

    if (!ReportEventA(handle, // event log handle 
        errorType,            // event type 
        0,                    // event category  
        localEventID,         // event identifier 
        NULL,                 // no user security identifier 
        insertsCount,         // number of substitution strings 
        0,                    // no data 
        (LPCSTR*)&lpszMessage[0], // pointer to strings 
        NULL))                // no data 
    {
        log_error(L"tvmosaic_reg::ReportToEventLog. Could not report the event (%d)") % GetLastError();
    }

    DeregisterEventSource(handle); 
}

bool AddEventSource(const wchar_t* szServicePathName, const wchar_t* szServiceName)
{
	HKEY    hk; 
	DWORD   dwData, dwDisp; 
	wchar_t buffer[MAX_PATH]; 

	// Create the event source as a subkey of the log. 
	swprintf_s(buffer, L"SYSTEM\\CurrentControlSet\\Services\\EventLog\\Application\\%s", szServiceName); 

	if (RegCreateKeyEx(HKEY_LOCAL_MACHINE, buffer, 
		0, NULL, REG_OPTION_NON_VOLATILE,
		KEY_WRITE, NULL, &hk, &dwDisp)) 
	{
		log_error(L"tvmosaic_reg::AddEventSource. Could not create the registry key (%s)") % buffer;
		return false;
	}

	// Set the name of the message file. 
	if (RegSetValueEx(
        hk,                         // subkey handle 
		L"EventMessageFile",        // value name 
		0,                          // must be zero 
		REG_EXPAND_SZ,              // value type 
		(LPBYTE)szServicePathName,  // pointer to value data 
		(DWORD)(wcslen(szServicePathName) * sizeof(szServicePathName[0])+1)))  // length of value data 
	{
		log_error(L"tvmosaic_reg::AddEventSource. Could not set the event message file");
		return false;
	}

	// Set the supported event types. 
	dwData = EVENTLOG_ERROR_TYPE | EVENTLOG_WARNING_TYPE | EVENTLOG_INFORMATION_TYPE; 

	if (RegSetValueEx(
        hk,                 // subkey handle 
		L"TypesSupported",  // value name 
		0,                  // must be zero 
		REG_DWORD,          // value type 
		(LPBYTE)&dwData,    // pointer to value data 
		sizeof(dwData)))    // length of value data 
	{
		log_error(L"tvmosaic_reg::AddEventSource. Could not set the supported types");
		return false;
	}

	RegCloseKey(hk); 
	return true;
}

void InstallService(const wchar_t* szServiceName, const wchar_t* szServiceDisplayedName, const wchar_t* szServiceExePath)
{
    SC_HANDLE service_control_manager = NULL;
    SC_HANDLE handle = NULL;
    bool status_ok = true;

    AddEventSource(szServiceExePath, szServiceName);

    if (status_ok)
    {
        service_control_manager = OpenSCManager(
            NULL,
            SERVICES_ACTIVE_DATABASE,
            SC_MANAGER_ALL_ACCESS);

        status_ok = (service_control_manager != NULL);
        if (!status_ok)
        {
            log_error(L"tvmosaic_reg::InstallService. Could not open the service manager (%d)") % GetLastError();
            status_ok = false;
        }
    }

    if (status_ok) 
    {
        handle = CreateService(
            service_control_manager,
            szServiceName,
            szServiceDisplayedName,
            SERVICE_ALL_ACCESS,
            SERVICE_WIN32_OWN_PROCESS,
            SERVICE_AUTO_START,
            SERVICE_ERROR_NORMAL,
            szServiceExePath,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL);

        if (handle == NULL)
        {
            log_error(L"tvmosaic_reg::InstallService. Could not create service (%d)") % GetLastError();
            status_ok = false;
        }
        else
        {
            SC_ACTION failureAction[3];
            failureAction[0].Type = failureAction[1].Type = failureAction[2].Type = SC_ACTION_RESTART;
            failureAction[0].Delay = failureAction[1].Delay = failureAction[2].Delay = 60 * 1000; // 60 s

            SERVICE_FAILURE_ACTIONS serviceAction;
            serviceAction.dwResetPeriod = 60 * 60 * 24; // reset period: 24 hours in seconds
            serviceAction.lpRebootMsg = L"";
            serviceAction.lpCommand = NULL;
            serviceAction.cActions = sizeof(failureAction)/sizeof(failureAction[0]);
            serviceAction.lpsaActions = failureAction;

            ChangeServiceConfig2(handle, SERVICE_CONFIG_FAILURE_ACTIONS, &serviceAction);
        }
    }

    if (handle != NULL)
    {
        CloseServiceHandle(handle);   
    }

    if (service_control_manager != NULL)
    {
        CloseServiceHandle(service_control_manager);
    }

    if (status_ok)
    {
        ReportToEventLog(eventID, EVENTLOG_INFORMATION_TYPE, 1,  "Service has been successfully installed", szServiceName);
    }
}

void unInstallService(const wchar_t* szServiceName)
{
    SC_HANDLE service_control_manager = NULL;
    SC_HANDLE handle = NULL;
    BOOLEAN status_ok = true;

    if (status_ok)
    {
        service_control_manager = OpenSCManager(NULL,
            SERVICES_ACTIVE_DATABASE,
            SC_MANAGER_ALL_ACCESS);

        if(service_control_manager == NULL)
        {
            log_error(L"tvmosaic_reg::unInstallService. Could not open the service control manager (%d)") % GetLastError();
            status_ok = false;
        }
    }

    if (status_ok)
    {
        handle = OpenService(service_control_manager, szServiceName, SERVICE_ALL_ACCESS);

        if (handle == NULL)
        {
            log_error(L"tvmosaic_reg::unInstallService. Could not find the service (%d)") % GetLastError();
            status_ok = false;
        }
    }

    if (status_ok)
    {
        if (!DeleteService(handle))
        {
            log_error(L"tvmosaic_reg::unInstallService. Could not delete the service (%d)") % GetLastError();
        }
    }

    if (handle != NULL)
    {
        CloseServiceHandle(handle);
    }

    if (service_control_manager != NULL)
    {
        CloseServiceHandle(service_control_manager);
    }

    if (status_ok)
    {
        ReportToEventLog(eventID, EVENTLOG_INFORMATION_TYPE, 1,  "Service has been successfully uninstalled", szServiceName);
    }
}

void DLStopService(const wchar_t* szServiceName)
{
    SC_HANDLE service_control_manager = NULL;
    SC_HANDLE handle = NULL;
    bool status_ok = true;
    SERVICE_STATUS my_serviceStatus;

    memset(&my_serviceStatus, 0, sizeof(my_serviceStatus));

    if (status_ok)
    {
        service_control_manager = OpenSCManager(
            NULL,
            SERVICES_ACTIVE_DATABASE,
            SC_MANAGER_ALL_ACCESS);

        if (service_control_manager == NULL)
        {
            log_error(L"tvmosaic_reg::StopService. Could not open the service control manager (%d)") % GetLastError();
            status_ok = false;
        }
    }

    if (status_ok)
    {
        handle = OpenService(service_control_manager, szServiceName, SERVICE_ALL_ACCESS);
        if(handle == NULL)
        {
            log_error(L"tvmosaic_reg::StopService. Could not find the service (%d)") % GetLastError();
            status_ok = false;
        }
    }

    if (status_ok)
    {
        if (QueryServiceStatus(handle, &my_serviceStatus))
        {
            if (my_serviceStatus.dwCurrentState != SERVICE_STOPPED)
            {
                if (!ControlService(handle, SERVICE_CONTROL_STOP, &my_serviceStatus))
                {
                    log_error(L"tvmosaic_reg::StopService. Could not stop the service (%d)") % GetLastError();
                } else
				{
					//wait 30 seconds until servide is actually stopped
					time_t starttime;
					time(&starttime);
					time_t curtime = starttime;
					while (curtime < starttime + 30)
					{
						if (QueryServiceStatus(handle, &my_serviceStatus))
						{
							if (my_serviceStatus.dwCurrentState == SERVICE_STOPPED)
								break;
						} else
						{
							//break if there was an error
							break;
						}
						Sleep(100);
						time(&curtime);
					}
				}
            }
        }
        else
        {
            log_error(L"tvmosaic_reg::StopService. Unable to query current service status (%d)") % GetLastError();
        }
    }

    if (handle != NULL)
    {
        CloseServiceHandle(handle);
    }

    if (service_control_manager != NULL)
    {
        CloseServiceHandle(service_control_manager);
    }

    if (status_ok)
    {
        ReportToEventLog(eventID, EVENTLOG_INFORMATION_TYPE, 1,  "Service has been successfully stopped", szServiceName);
    }
}

void DLStartService(const wchar_t* szServiceName)
{
    SC_HANDLE service_control_manager = NULL;
    SC_HANDLE handle = NULL;
    bool status_ok = true;
    SERVICE_STATUS my_serviceStatus;

    memset(&my_serviceStatus, 0, sizeof(my_serviceStatus));

    if (status_ok)
    {
        service_control_manager = OpenSCManager(
            NULL,
            SERVICES_ACTIVE_DATABASE,
            SC_MANAGER_ALL_ACCESS);

        if (service_control_manager == NULL)
        {
            log_error(L"tvmosaic_reg::StartService. Could not open the service control manager (%d)") % GetLastError();
            status_ok = false;
        }
    }

    if (status_ok)
    {
        handle = OpenService(service_control_manager, szServiceName, SERVICE_ALL_ACCESS);
        if(handle == NULL)
        {
            log_error(L"tvmosaic_reg::StartService. Could not find the service (%d)") % GetLastError();
            status_ok = false;
        }
    }

    if (status_ok)
    {
        if (QueryServiceStatus(handle, &my_serviceStatus))
        {
            if (my_serviceStatus.dwCurrentState == SERVICE_STOPPED)
            {
                if (!StartService(handle, 0, NULL))
                {
                    log_error(L"tvmosaic_reg::StartService. Could not start the service (%d)") % GetLastError();
                } else
				{
					//wait 10 seconds until servide is running
					time_t starttime;
					time(&starttime);
					time_t curtime = starttime;
					while (curtime < starttime + 10)
					{
						if (QueryServiceStatus(handle, &my_serviceStatus))
						{
							if (my_serviceStatus.dwCurrentState == SERVICE_RUNNING)
								break;
						} else
						{
							//break if there was an error
							break;
						}
						Sleep(100);
						time(&curtime);
					}
				}
            }
        }
        else
        {
            log_error(L"tvmosaic_reg::StartService. Unable to query current service status (%d)") % GetLastError();
        }
    }

    if (handle != NULL)
    {
        CloseServiceHandle(handle);
    }

    if (service_control_manager != NULL)
    {
        CloseServiceHandle(service_control_manager);
    }

    if (status_ok)
    {
        ReportToEventLog(eventID, EVENTLOG_INFORMATION_TYPE, 1,  "Service has been successfully started", szServiceName);
    }
}

void UninstallService(const wchar_t* szServiceName)
{
    DLStopService(szServiceName);
    unInstallService(szServiceName);
}

void DLRestartService(const wchar_t* szServiceName)
{
	DLStopService(szServiceName);
	Sleep(5000);
	DLStartService(szServiceName);
}

dl_service_status_state_e DLGetServiceStatus(const wchar_t* szServiceName)
{
    dl_service_status_state_e ret_val = dl_sss_unknown;

    SC_HANDLE service_control_manager = NULL;
    SC_HANDLE handle = NULL;
    SERVICE_STATUS my_serviceStatus;

    memset(&my_serviceStatus, 0, sizeof(my_serviceStatus));

    service_control_manager = OpenSCManager(
        NULL,
        SERVICES_ACTIVE_DATABASE,
        SC_MANAGER_ALL_ACCESS);

    if (service_control_manager != NULL)
    {
        handle = OpenService(service_control_manager, szServiceName, SERVICE_ALL_ACCESS);
        if(handle != NULL)
        {
            if (QueryServiceStatus(handle, &my_serviceStatus))
            {
                if (my_serviceStatus.dwCurrentState == SERVICE_RUNNING)
                    ret_val = dl_sss_running;
                else if (my_serviceStatus.dwCurrentState == SERVICE_STOPPED)
                    ret_val = dl_sss_stopped;
                else 
                    ret_val = dl_sss_transient;
            } else
            {
                log_error(L"tvmosaic_reg::GetServiceStatus. Unable to query current service status (%d)") % GetLastError();
            }

            CloseServiceHandle(handle);
        } else
        {
            log_error(L"tvmosaic_reg::GetServiceStatus. Could not find the service (%d)") % GetLastError();
        }

        CloseServiceHandle(service_control_manager);
    } else
    {
        log_error(L"tvmosaic_reg::GetServiceStatus. Could not open the service control manager (%d)") % GetLastError();
    }

    return ret_val;
}

#endif
