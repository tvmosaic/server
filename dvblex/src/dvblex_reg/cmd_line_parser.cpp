/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include "cmd_line_parser.h"


CCmdLineParser::CCmdLineParser(std::wstring& strCmdLine) :
    m_strCmdLine(strCmdLine)
{
    std::wstring cmd(m_strCmdLine);
    Analyze(cmd);
}

CCmdLineParser::CCmdLineParser(const wchar_t* szCmdLine) :
    m_strCmdLine(szCmdLine)
{
    std::wstring cmd(m_strCmdLine);
    Analyze(cmd);
}

void CCmdLineParser::Analyze(std::wstring& strCmdLine)
{
    while (strCmdLine.length())
    {
        while (strCmdLine[0] == L' ')
        {
            strCmdLine.erase(0, 1);
        }

        std::wstring strParam;
        if (strCmdLine.length())
        {
            if (strCmdLine[0] == L'"')
            {
                size_t pos = strCmdLine.find_first_of(L'"', 1);
                if (pos != std::wstring::npos)
                {
                    strParam = strCmdLine.substr(1, pos - 1);
                }

                strCmdLine.erase(0, pos + 1);
            }
            else
            {
                size_t pos = strCmdLine.find_first_of(L" ");
                if (pos != std::wstring::npos)
                {
                    strParam = strCmdLine.substr(0, pos);
                }
                else
                {
                    strParam = strCmdLine;
                }

                strCmdLine.erase(0, pos);
            }

            if (strParam.length())
            {
                // trim left
                while (strParam[0] == L' ')
                {
                    strParam.erase(0, 1);
                }
                // trim right
                while (strParam[strParam.length() - 1] == L' ')
                {
                    strParam.erase(strParam.length() - 1, 1);
                }

                m_vecParams.push_back(strParam);
            }
        }
    }
}


