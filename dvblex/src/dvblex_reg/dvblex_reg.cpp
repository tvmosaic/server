/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <iostream>
#ifdef _WIN32
#include <objbase.h>
#include <strsafe.h>
#else
#include <stdlib.h>
//#include <stdio.h>
//#include <unistd.h>
#endif
#include <dl_common.h>
#include <dl_uuid.h>
#include <dl_logger.h>
#include <dl_logger_file_writer.h>
#include <dl_file_procedures.h>
#include <dl_products.h>
#include <dl_utils.h>
#ifdef _WIN32
#include <dl_windows_firewall.h>
#endif
#include <dl_installation_settings.h>
#include <dl_permanent_settings.h>
#include <dl_pugixml_helper.h>
#include <dl_http_comm.curl.h>
#include <dl_url_encoding.h>
#include <dl_zip.h>
#include <dl_file_procedures.h>
#include "dvblex_reg.h"
#include "reg_service.h"
#include "cmd_line_parser.h"


using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblex::settings;
using namespace dvblink::logging;

void AddAppToFirewall(const wchar_t* szAppPath, const wchar_t* szRegisterName);
void RemoveAppFromFirewall(const wchar_t* szRegisterName);
void PrepareNewInstall(const wchar_t* install_path, const wchar_t* private_root, const wchar_t* shared_root);
void WaitForServer(const wchar_t* time_in_sec);
void UnzipToDir(const wchar_t* zipfile, const wchar_t* dest_dir);

//
// !!!!!    if parameter has spaces it should be in quotes          !!!!!!
// !!!!! it would be right to put all parameters in quotes for sure !!!!!!
//

#define INSTALL_SERVICE             L"-installservice"          // szServiceDisplayedName szServiceExePath
#define UNINSTALL_SERVICE           L"-uninstallservice"        // 
#define STOP_SERVICE                L"-stopservice"             //
#define START_SERVICE               L"-startservice"            // 
#define RESTART_SERVICE             L"-restartservice"          // 
#define ADD_APP_TO_FIREWALL         L"-addapptofirewall"        // szAppPath szRegisterName
#define REMOVE_APP_FROM_FIREWALL    L"-removeappfromfirewall"   // szRegisterName
#define GET_SERVER_INSTALL_PATH	    L"-getserverinstallpath"    // result is in output
#define PREPARE_NEW_INSTALL 	    L"-preparenewinstall"       // install path, private root, shared root
#define IS_SERVER_AVAILABLE         L"-isserveravailable"       // time to wait in seconds
#define UNZIP_TO_DIR                L"-unziptodir"              // path to zip file, directory to anzip files to

const std::wstring status_error = L"error";
const std::wstring status_success = L"success";

#ifdef __APPLE__
static void get_launch_dir_from_argv(char* exe_path, filesystem_path_t& launch_dir)
{
	char pathbuf[1024];
	strcpy(pathbuf, exe_path);

 	char* p = strrchr(pathbuf, L'/');
 	if(p)
 	{
        *p = L'\0';
 	}
 	launch_dir = pathbuf;
}
#endif

#ifdef _WIN32
int APIENTRY _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int /*nCmdShow*/)
#else
int main(int argc, char* argv[])
#endif
{
#ifdef _WIN32
	UNREFERENCED_PARAMETER(hPrevInstance);
#endif

    int ret_val = 0;

    //Initialize logging
	filesystem_path_t cur_dir;
	filesystem_path_t exe_path;
    get_app_locations(exe_path, cur_dir);

	cur_dir /= L"tvmosaic_reg.log";
    boost::shared_ptr<logger_file_writer> flg = boost::shared_ptr<logger_file_writer>(new logger_file_writer(cur_dir, log_level_extended_info, false));
    flg->start();

    logger::instance()->set_interface(flg);

    log_info(L"tvmosaic_reg started");

    std::wstringstream command_line;
#ifdef _WIN32
    command_line << lpCmdLine;
#else
    for (int i = 1; i < argc; ++i)
    {
        std::wstring param = string_cast<EC_UTF8>(argv[i]);
        command_line << L" " << param;
    }
#endif
    log_info(L"command line: %1%") % command_line.str();

    CCmdLineParser parser(command_line.str().c_str());
    std::vector<std::wstring> params = parser.GetParams();

    if (params.size())
    {
        std::wstring strKey = params[0];
        params.erase(params.begin());

        log_info(L"%s:") % strKey.c_str();
        for (size_t i = 0; i < params.size(); i++)
        {
            log_info(L"     %s") % params[i].c_str();
        }

#ifdef _WIN32
        if (params.size() == 2 && strKey == INSTALL_SERVICE)
        {
            InstallService(SERVICE_NAME, params[0].c_str(), params[1].c_str());
        }
        else
        if (strKey == UNINSTALL_SERVICE)
        {
            UninstallService(SERVICE_NAME);
        }
        else
        if (strKey == STOP_SERVICE)
        {
            DLStopService(SERVICE_NAME);
        }
        else
        if (strKey == START_SERVICE)
        {
            DLStartService(SERVICE_NAME);
        }
        else
        if (strKey == RESTART_SERVICE)
        {
            DLRestartService(SERVICE_NAME);
        }
        else
        if (params.size() == 2 && strKey == ADD_APP_TO_FIREWALL)
        {
            AddAppToFirewall(params[0].c_str(), params[1].c_str());
        }
        else
        if (params.size() == 1 && strKey == REMOVE_APP_FROM_FIREWALL)
        {
            RemoveAppFromFirewall(params[0].c_str());
        }
        else
#endif
        if (strKey == GET_SERVER_INSTALL_PATH)
        {
            filesystem_path_t p = permanent_settings::get_server_install_path();

            std::wstring res = status_error;
            if (!p.empty())
            {
                const std::wstring tail = L"tail";
                p /= tail;
                res = p.to_wstring();
                size_t pos = res.find_last_of(tail);
                res.erase(pos - tail.length() + 1);
            }
            log_info(L"result: %1%") % res;
            //std::wcout << "\"" << res << "\"" << std::endl;
            std::wcout << res << std::endl;
        } 
        else
        if (params.size() == 3 && strKey == PREPARE_NEW_INSTALL)
        {
            PrepareNewInstall(params[0].c_str(), params[1].c_str(), params[2].c_str());
        } 
        else
        if (params.size() == 2 && strKey == UNZIP_TO_DIR)
        {
			UnzipToDir(params[0].c_str(), params[1].c_str());
        }
        else
        if (params.size() == 1 && strKey == IS_SERVER_AVAILABLE)
        {
			WaitForServer(params[0].c_str());
        }
        else
        {
            log_error(L"Wrong parameter %s, %d") % strKey.c_str() % params.size();
        }
	}
    else
    {
        log_error(L"No parameters");
    }

    log_info(L"tvmosaic_reg stopped");

    logger::instance()->shutdown();
    
	flg->stop();
	flg.reset();

    return ret_val;
}

#ifdef _WIN32
void AddAppToFirewall(const wchar_t* szAppPath, const wchar_t* szRegisterName)
{
    HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
    if (hr == S_OK)
    {
        CWinFirewall fw;
        FW_ERROR_CODE fe = fw.Initialize();
        if (fe == FW_NOERROR)
        {
            int res = 0;
            if ((res = fw.AddRule(szAppPath, szRegisterName, L"TVMosaic Rule Group")) != FW_NOERROR)
            {
                std::wstring errorMes;
                GetFirewallErrorMessage((FW_ERROR_CODE)res, errorMes);        
                log_error(L"AddAppToFirewall. %s") % errorMes.c_str();
            }
            else
            {
                log_info(L"AddAppToFirewall. Added application: %s") % szAppPath;
            }

            fw.Uninitialize();
        }

        CoUninitialize();
    }
}

void RemoveAppFromFirewall(const wchar_t* szRegisterName)
{
    HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
    if (hr == S_OK)
    {
        CWinFirewall fw;
        FW_ERROR_CODE fe = fw.Initialize();
        if (fe == FW_NOERROR)
        {
            int res = 0;
            if ((res = fw.RemoveRule(szRegisterName)) != FW_NOERROR)
            {
                std::wstring errorMes;
                GetFirewallErrorMessage((FW_ERROR_CODE)res, errorMes);        
                log_error(L"RemoveAppFromFirewall. %s") % errorMes.c_str();
            }
            else
            {
                log_info(L"RemoveAppFromFirewall. Removed application: %s") % szRegisterName;
            }

            fw.Uninitialize();
        }

        CoUninitialize();
    }
}
#endif

void PrepareNewInstall(const wchar_t* install_path, const wchar_t* private_root, const wchar_t* shared_root)
{
	//set install path
	log_info(L"set install path %1%") % install_path;
	filesystem_path_t ip(install_path);
    permanent_settings::set_server_install_path(ip);

	//set private root
	log_info(L"set private root path %1%") % private_root;
	ip = private_root;
    permanent_settings::set_private_root(ip);

	//set shared root
	log_info(L"set shared root path %1%") % shared_root;
	ip = shared_root;
    permanent_settings::set_shared_root(ip);

	//generate server uuid
	std::wstring uuid_str;
    dvblink::engine::uuid::gen_uuid(uuid_str);
    boost::to_upper(uuid_str);
	log_info(L"server uuid: %1%") % uuid_str;
    permanent_settings::set_server_id(uuid_str);
}

void WaitForServer(const wchar_t* time_in_sec)
{
    std::string str = string_cast<EC_UTF8>(time_in_sec);
    int time_sec = atoi(str.c_str());
#ifdef WIN32
    time_t t;
    time(&t);

    time_t end_time = t + time_sec;
    while (t < end_time)
    {
        dl_service_status_state_e service_status = DLGetServiceStatus(SERVICE_NAME);
        log_info(L"WaitForServer. Status %1%") % service_status;
        if (service_status == dl_sss_running)
        {
            log_info(L"WaitForServer. Service is running");
            break;
        }

        Sleep(500);
        time(&t);
    }

#else
    //not implemented
#endif

}

void UnzipToDir(const wchar_t* zipfile_w, const wchar_t* dest_dir_w)
{
    dvblink::filesystem_path_t zipfile = zipfile_w;
    dvblink::filesystem_path_t dest_dir = dest_dir_w;
    //check if zip file exists
    if (boost::filesystem::exists(zipfile.to_boost_filesystem()))
    {
        //remove dest directory if it exists
        if (boost::filesystem::exists(dest_dir.to_boost_filesystem()))
        {
            //empty directory first
            engine::filesystem::delete_directory_contents(dest_dir.to_boost_filesystem());

            //delete dir via move
            time_t now;
            time(&now);
            std::stringstream buf;
            buf << now;
            dvblink::filesystem_path_t mv_dest_dir(dest_dir.to_string() + "." + buf.str());
            try {
                boost::filesystem::rename(dest_dir.to_boost_filesystem(), mv_dest_dir.to_boost_filesystem());
            } catch(...){}
            //delete moved directory
            try {
                boost::filesystem::remove_all(mv_dest_dir.to_boost_filesystem());
            } catch(...){}
        }

        if (boost::filesystem::exists(dest_dir.to_boost_filesystem()))
            log_warning(L"UnzipToDir. Destination dir %1% was not deleted properly") % dest_dir.to_wstring();

        //create destination directory
        try {
            boost::filesystem::create_directories(dest_dir.to_boost_filesystem());
        } catch(...){}

        if (boost::filesystem::exists(dest_dir.to_boost_filesystem()))
        {
            dvblink::zip::zipfile_reader zr(zipfile);
            if (!zr.decompress(dest_dir))
                log_error(L"UnzipToDir. Error, while decompressing %1% to %2%") % zipfile.to_wstring() % dest_dir.to_wstring();
        } else
        {
            log_error(L"UnzipToDir. Destination dir %1% cannot be created") % dest_dir.to_wstring();
        }
    } else
    {
        log_error(L"UnzipToDir. Source zip %1% does not exist") % zipfile.to_wstring();
    }
}


