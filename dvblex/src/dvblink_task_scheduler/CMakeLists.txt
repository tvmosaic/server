cmake_minimum_required(VERSION 3.0.0)

project(dvblink_task_scheduler_lib)

set(SOURCES power_man.cpp stdafx.cpp wakeup_timer_man.cpp)

add_library(${PROJECT_NAME} STATIC ${SOURCES})

