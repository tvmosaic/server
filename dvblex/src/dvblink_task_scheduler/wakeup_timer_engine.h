/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_power_man.h>

namespace dvblink { namespace engine {

class wakeup_timer_engine_base
{
public:
    wakeup_timer_engine_base() {}
    virtual ~wakeup_timer_engine_base() {}

    virtual bool schedule_wakeup_timer(const client_id_t& client_id, const wakeup_timer& timer, const filesystem_path_t& app_path, const std::wstring& app_parameters) = 0;
    virtual bool get_wakeup_timer(const client_id_t& client_id, wakeup_timer& timer) = 0;
    virtual bool remove_wakeup_timer(const client_id_t& client_id) = 0;
};

class wakeup_timer_engine_default : public wakeup_timer_engine_base 
{
public:
    bool schedule_wakeup_timer(const client_id_t& /*client_id*/, const wakeup_timer& /*timer*/, const filesystem_path_t& /*app_path*/, const std::wstring& /*app_parameters*/) {return true;}
    bool get_wakeup_timer(const client_id_t& /*client_id*/, wakeup_timer& /*timer*/) {return false;}
    bool remove_wakeup_timer(const client_id_t& /*client_id*/) {return true;}
};

} //engine
} //dvblink
