/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include "wakeup_timer_man.h"
#if defined(_WIN32)
#include "wakeup_timer_engine_win.h"
#include "win_task_scheduler_1_0.h"
#include "win_task_scheduler_2_0.h"
#elif defined(__APPLE__)
#include "wakeup_timer_engine.h"
#include "wakeup_timer_engine_macos.h"
#else
#include "wakeup_timer_engine.h"
#include "wakeup_timer_engine_linux.h"
#endif

namespace dvblink { namespace engine {

wakeup_timer_man::wakeup_timer_man()
{
#if defined(_WIN32)
    if (version_.is_xp())
    {
        man_ = std::auto_ptr<wakeup_timer_engine_base>(new wakeup_timer_engine_win<win_task_scheduler_1_0>());
    }
    else
    {
        man_ = std::auto_ptr<wakeup_timer_engine_base>(new wakeup_timer_engine_win<win_task_scheduler_2_0>());
    }
#elif defined(_UBUNTU_X86) || defined(_UBUNTU_X86_64)
    man_ = std::auto_ptr<wakeup_timer_engine_base>(new wakeup_timer_engine_linux());
#elif defined(__APPLE__)
    man_ = std::auto_ptr<wakeup_timer_engine_base>(new wakeup_timer_engine_macos());
#else
    man_ = std::auto_ptr<wakeup_timer_engine_base>(new wakeup_timer_engine_default());
#endif
}

bool wakeup_timer_man::schedule_wakeup_timer(const client_id_t& client_id, wakeup_timer& timer, const filesystem_path_t& app_path, const std::wstring& app_parameters)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    return man_->schedule_wakeup_timer(client_id, timer, app_path, app_parameters);
}

bool wakeup_timer_man::get_wakeup_timer(const client_id_t& client_id, wakeup_timer& timer)
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);
    return man_->get_wakeup_timer(client_id, timer);
}

bool wakeup_timer_man::remove_wakeup_timer(const client_id_t& client_id)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    return man_->remove_wakeup_timer(client_id);
}

} //engine
} //dvblink
