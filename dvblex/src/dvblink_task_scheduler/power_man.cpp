/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_event.h>
#include <dl_power_man.h>
#include <dl_logger.h>
#include "wakeup_timer_man.h"

#if defined(__APPLE__)
#undef __OSX_AVAILABLE_BUT_DEPRECATED_MSG
#define __OSX_AVAILABLE_BUT_DEPRECATED_MSG(A, B, C, D, E)
#include <IOKit/pwr_mgt/IOPMLib.h>
#include <CoreServices/CoreServices.h>
extern "C" OSErr UpdateSystemActivity(UInt8);
#endif

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4355)
#endif

namespace dvblink { namespace engine {

using namespace logging;

class no_standby_thread
{
public:
    no_standby_thread() : exit_(false),
        power_thread_(std::auto_ptr<boost::thread>(new boost::thread(boost::bind(&no_standby_thread::power_thread_func, this))))
    {
    }

    virtual ~no_standby_thread()
    {
        exit_ = true;
        quit_.signal();
        power_thread_->join();
    }

protected:
    void power_thread_func()
    {
        //indicate that system cannot go to standby now
    #if defined(_WIN32)
        SetThreadExecutionState(ES_CONTINUOUS | ES_SYSTEM_REQUIRED | ES_AWAYMODE_REQUIRED);
        quit_.wait();
        //can go to standby
        SetThreadExecutionState(ES_CONTINUOUS);
    #elif defined(__APPLE__)
        IOPMAssertionID id = 0;
        CFStringRef reason = CFSTR("TVMosaic Server");

        // kIOPMAssertionTypeNoDisplaySleep
        IOReturn ret = IOPMAssertionCreateWithName(
        		kIOPMAssertionTypePreventSystemSleep, kIOPMAssertionLevelOn, reason, &id);
        
        if (ret != kIOReturnSuccess)
        	log_warning(L"power_thread_func: Failed to create kIOPMAssertionTypePreventSystemSleep assertion");

        while (!exit_)
        {
            UpdateSystemActivity(OverallAct);
            quit_.wait(20000); // 20 sec
        }

        if ((ret == kIOReturnSuccess) && (id != 0))
        {
            IOPMAssertionRelease(id);
        }
    #else
        quit_.wait(); // not implemented - just wait
    #endif
    }

private:
    event quit_;
    bool exit_;
    std::auto_ptr<boost::thread> power_thread_;
};

//********************************************************************************************

power_manager::power_manager() :
    man_(NULL)
{
    man_ = new wakeup_timer_man();
}

power_manager::~power_manager()
{
    delete man_;
}

bool power_manager::set_idle_power_state(const client_id_t& client_id)
{
    bool ret_val = true;

    boost::unique_lock<boost::mutex> lock(lock_);

    if (standby_client_map_.find(client_id) != standby_client_map_.end())
    {
        standby_client_map_.erase(client_id);
        if (standby_client_map_.size() == 0)
        {
            ret_val = set_power_state(eps_idle);
        }
    }
    return ret_val;
}

bool power_manager::set_no_standby_power_state(const client_id_t& client_id)
{
    bool ret_val = true;

    boost::unique_lock<boost::mutex> lock(lock_);

    if (standby_client_map_.find(client_id) == standby_client_map_.end())
    {
        standby_client_map_[client_id] = client_id;
        if (standby_client_map_.size() == 1)
            ret_val = set_power_state(eps_no_standby);
    }

    return ret_val;
}

bool power_manager::set_power_state(e_power_state state)
{
    bool res = false;
    switch (state)
    {
    case eps_idle:
        if (no_standby_thread_)
        {
            no_standby_thread_.reset();
            log_info(L"power_manager::set_power_state. Standby mode is allowed");
            res = true;
        }
        break;
    case eps_no_standby:
        if (!no_standby_thread_)
        {
            no_standby_thread_ = no_standby_thread_t(new no_standby_thread());
            log_info(L"power_manager::set_power_state. Standby mode is forbiden");
            res = true;
        }
        break;
    }
    return res;
}

void power_manager::get_power_state(e_power_state& state)
{
    boost::unique_lock<boost::mutex> lock(lock_);
    state = (no_standby_thread_ ? eps_no_standby : eps_idle);
}

///////////////////////////////////////////////////////////////////////////////

bool power_manager::schedule_wakeup_timer(const client_id_t& client_id, wakeup_timer& timer, const filesystem_path_t& app_path, const std::wstring& app_parameters)
{
    return man_->schedule_wakeup_timer(client_id, timer, app_path, app_parameters);
}

bool power_manager::get_wakeup_timer(const client_id_t& client_id, wakeup_timer& timer)
{
    return man_->get_wakeup_timer(client_id, timer);
}

bool power_manager::remove_wakeup_timer(const client_id_t& client_id)
{
    return man_->remove_wakeup_timer(client_id);
}

} // namespace engine
} // namespace dvblink

#ifdef _MSC_VER
#pragma warning(pop)
#endif

// $Id: power_man.cpp 12093 2015-10-23 15:25:21Z pashab $
