/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <comdef.h>
#include <wincred.h>
#include <taskschd.h>
#include <dl_utils.h>
#include <dl_strptime.h>
#include "win_task_scheduler_2_0.h"


namespace dvblink { namespace engine {

void release(IUnknown* obj)
{
    if (obj)
        obj->Release();
}

void win_task_scheduler_2_0::add_timer_thread_func(const client_id_t& client_id,
    const wakeup_timer& timer, const filesystem_path_t& app_path, const std::wstring& app_parameters, bool& res)
{
    res = false;

    std::wstring client_id_str = client_id.to_wstring();
    wchar_t* task_name = (wchar_t*)client_id_str.c_str();

    CoInitialize(NULL);

    ITaskService* task_service = NULL;
    HRESULT hr = CoCreateInstance(CLSID_TaskScheduler, NULL, CLSCTX_INPROC_SERVER, IID_ITaskService, (void**)&task_service);
    if (!FAILED(hr))
    {
        ITaskFolder* task_folder = NULL;
        ITaskDefinition* task = NULL;
        IRegistrationInfo* reg_info = NULL;
        IPrincipal* principal = NULL;
        ITaskSettings* settings = NULL;
        ITriggerCollection* trigger_collection = NULL;
        ITrigger* trigger = NULL;
        IActionCollection* action_collection = NULL;
        IAction* action = NULL;
        IExecAction* exec_action = NULL;
        IRegisteredTask* registered_task = NULL;
        ITimeTrigger* time_trigger = NULL;

        do
        {
            //  Connect to the task service.
            hr = task_service->Connect(_variant_t(), _variant_t(), _variant_t(), _variant_t()); if (FAILED(hr)) break;

            //  Get the pointer to the root task folder.  This folder will hold the new task that is registered.
            hr = task_service->GetFolder(L"\\", &task_folder);  if (FAILED(hr)) break;

            //  If the same task exists, remove it.
            task_folder->DeleteTask(task_name, 0);

            //  Create the task definition object to create the task.
            hr = task_service->NewTask(0, &task); if (FAILED(hr)) break;

            //  Get the registration info for setting the identification.
            hr = task->get_RegistrationInfo(&reg_info); if (FAILED(hr)) break;

            hr = reg_info->put_Author(L"DVBLogic"); if (FAILED(hr)) break;

            //  Create the principal for the task - these credentials
            //  are overwritten with the credentials passed to RegisterTaskDefinition
            //hr = task->get_Principal(&principal);
            //if (FAILED(hr))
            //    break;

            //// Set up principal logon type to interactive logon\
            ////  TODO - check how it works in the system account
            //hr = principal->put_LogonType(TASK_LOGON_SERVICE_ACCOUNT/*TASK_LOGON_INTERACTIVE_TOKEN*/);
            //if (FAILED(hr))
            //    break;

            //  Create the settings for the task
            hr = task->get_Settings(&settings); if (FAILED(hr)) break;

            //  Set setting values for the task.  
            hr = settings->put_StartWhenAvailable(VARIANT_TRUE); if (FAILED(hr)) break;
            hr = settings->put_WakeToRun(VARIANT_TRUE); if (FAILED(hr)) break;
            hr = settings->put_DisallowStartIfOnBatteries(VARIANT_FALSE); if (FAILED(hr)) break;
            hr = settings->put_StopIfGoingOnBatteries(VARIANT_FALSE); if (FAILED(hr)) break;

            //  Get the trigger collection to insert the time trigger.
            hr = task->get_Triggers(&trigger_collection); if (FAILED(hr)) break;

            //  Set the task to start at a certain time. The time format should be YYYY-MM-DDTHH:MM:SS(+-)(timezone).
            struct tm* time_structure = gmtime(&timer.wakeup_time);
            if (time_structure == NULL)
            {
                hr = E_FAIL;
                break;
            }
            char time_buf[1024];
            strftime(time_buf, sizeof(time_buf), "%Y-%m-%dT%H:%M:%SZ", time_structure);
            std::wstring time_iso = string_cast<EC_UTF8>(time_buf);

            if (timer.daily)
            {
                //  Add the time trigger to the task.
                hr = trigger_collection->Create(TASK_TRIGGER_DAILY, &trigger); if (FAILED(hr)) break;
                IDailyTrigger* daily_trigger = NULL;
                hr = trigger->QueryInterface(IID_IDailyTrigger, (void**)&daily_trigger); if (FAILED(hr)) break;

                time_trigger = reinterpret_cast<ITimeTrigger*>(daily_trigger);
                hr = daily_trigger->put_DaysInterval((short)1);  if (FAILED(hr)) break;
            }
            else
            {
                //  Add the time trigger to the task.
                hr = trigger_collection->Create(TASK_TRIGGER_TIME, &trigger); if (FAILED(hr)) break;
                hr = trigger->QueryInterface(IID_ITimeTrigger, (void**)&time_trigger);  if (FAILED(hr)) break;
            }

            hr = time_trigger->put_Id(L"tvmosaic_trigger");  if (FAILED(hr)) break;
            hr = time_trigger->put_StartBoundary(const_cast<wchar_t*>(time_iso.c_str())); if (FAILED(hr)) break;

            //  Add an action to the task. This task will execute notepad.exe.     
            //  Get the task action collection pointer.
            hr = task->get_Actions(&action_collection); if (FAILED(hr)) break;

            //  Create the action, specifying that it is an executable action.
            hr = action_collection->Create(TASK_ACTION_EXEC, &action); if (FAILED(hr)) break;

            //  QI for the executable task pointer.
            hr = action->QueryInterface(IID_IExecAction, (void**)&exec_action); if (FAILED(hr)) break;

            //  Set the path of the executable to notepad.exe.
            hr = exec_action->put_Path(const_cast<wchar_t*>(app_path.get().c_str())); if (FAILED(hr)) break;

            if (!app_parameters.empty())
            {
                hr = exec_action->put_Arguments(const_cast<wchar_t*>(app_parameters.c_str()));  if (FAILED(hr)) break;
            }

            //  Save the task in the root folder.
            VARIANT varPassword;
            varPassword.vt = VT_EMPTY;

            hr = task_folder->RegisterTaskDefinition(task_name, task, TASK_CREATE_OR_UPDATE,
                _variant_t(L"Local Service"), varPassword, TASK_LOGON_SERVICE_ACCOUNT, _variant_t(L""), &registered_task);
            //if there is a security error - retry it with System user
            if (hr != S_OK)
            {
                hr = task_folder->RegisterTaskDefinition(task_name, task, TASK_CREATE_OR_UPDATE,
                    _variant_t(L"SYSTEM"), varPassword, TASK_LOGON_SERVICE_ACCOUNT, _variant_t(L""), &registered_task);
            }
        } while (0);

        release(time_trigger);
        release(task_service);
        release(task_folder);
        release(task);
        release(reg_info);
        release(principal);
        release(settings);
        release(trigger_collection);
        release(trigger);
        release(action_collection);
        release(action);
        release(exec_action);
        release(registered_task);
    }
    
    res = SUCCEEDED(hr);

    CoUninitialize();
}

void win_task_scheduler_2_0::get_timer_thread_func(const client_id_t& client_id, wakeup_timer& timer, bool& res)
{
    res = false;

    CoInitialize(NULL);

    ITaskService* task_service = NULL;
    HRESULT hr = CoCreateInstance(CLSID_TaskScheduler, NULL, CLSCTX_INPROC_SERVER, IID_ITaskService, (void**)&task_service);
    if (!FAILED(hr))
    {
        //  Connect to the task service.
        hr = task_service->Connect(_variant_t(), _variant_t(), _variant_t(), _variant_t());
        if (!FAILED(hr))
        {
            do 
            {
                //  Get the pointer to the root task folder. This folder will hold the new task that is registered.
                ITaskFolder* task_folder = NULL;
                hr = task_service->GetFolder(L"\\", &task_folder);
                if (!FAILED(hr))
                {
                    std::wstring client_id_str = client_id.to_wstring();
                    wchar_t* task_name = (wchar_t*)client_id_str.c_str();

                    IRegisteredTask* task = NULL;
                    ITaskDefinition* task_definition = NULL;
                    ITriggerCollection* trigger_collection = NULL;
                    ITrigger* trigger = NULL;

                    hr = task_folder->GetTask(task_name, &task); if (FAILED(hr)) break;
                    hr = task->get_Definition(&task_definition); if (FAILED(hr)) break;
                    hr = task_definition->get_Triggers(&trigger_collection); if (FAILED(hr)) break;
                    hr = trigger_collection->get_Item(1, &trigger); if (FAILED(hr)) break;

                    TASK_TRIGGER_TYPE2 type;
                    hr = trigger->get_Type(&type); if (FAILED(hr)) break;

                    timer.daily = false;
                    //if (type == TASK_TRIGGER_TIME)
                    if (type == TASK_TRIGGER_DAILY)
                    {
                        IDailyTrigger* daily_trigger = reinterpret_cast<IDailyTrigger*>(trigger);
                        short interval = 0;
                        hr = daily_trigger->get_DaysInterval(&interval); if (FAILED(hr)) break;
                        timer.daily = interval == 1 ? true : false;
                    }
                    
                    LPWSTR start_time = NULL;
                    hr = trigger->get_StartBoundary(&start_time); if (FAILED(hr)) break;
                    
                    std::string time_iso = string_cast<EC_UTF8>(start_time);

                    tm dt_tm;
                    char* conv_res = dl_strptime(time_iso.c_str(), "%Y-%m-%dT%H:%M:%SZ", &dt_tm);
                    timer.wakeup_time = _mkgmtime(&dt_tm);

                    SysFreeString(start_time);

                    release(task_folder);
                    release(task);
                    release(task_definition);
                    release(trigger_collection);
                    release(trigger);

                    res = SUCCEEDED(hr);
                }
            }  while (0);
        }
        release(task_service);
    }
    CoUninitialize();
}

void win_task_scheduler_2_0::delete_timer_thread_func(const client_id_t& client_id, bool& res)
{
    res = false;

    std::wstring client = client_id.to_wstring();
    wchar_t* task_name = const_cast<wchar_t*>(client.c_str());

    CoInitialize(NULL);

    ITaskService* task_service = NULL;
    HRESULT hr = CoCreateInstance(CLSID_TaskScheduler, NULL, CLSCTX_INPROC_SERVER, IID_ITaskService, (void**)&task_service);
    if (!FAILED(hr))
    {
        //  Connect to the task service.
        hr = task_service->Connect(_variant_t(), _variant_t(), _variant_t(), _variant_t());
        if (!FAILED(hr))
        {
            //  Get the pointer to the root task folder.  This folder will hold the new task that is registered.
            ITaskFolder* task_folder = NULL;
            hr = task_service->GetFolder(L"\\", &task_folder );
            if (!FAILED(hr))
            {
                //  If the same task exists, remove it.
                hr = task_folder->DeleteTask(task_name, 0);
                release(task_folder);
            }
        }
    
        release(task_service);
    }

    res = SUCCEEDED(hr);
    CoUninitialize();
}

} //engine
} //dvblink

