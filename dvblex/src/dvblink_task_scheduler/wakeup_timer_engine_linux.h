/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

// https://www.linux.com/learn/docs/672849-wake-up-linux-with-an-rtc-alarm-clock
// http://ragsagar.wordpress.com/2011/08/15/how-to-automatically-wake-up-your-computer-at-a-particular-time-resume-by-rtc-alarm-in-arch-linux/

#pragma once

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <boost/filesystem.hpp>
#include <dl_types.h>
#include "wakeup_timer_engine.h"

namespace dvblink {
namespace engine {

const char* wakeup_file = "/sys/class/rtc/rtc0/wakealarm";

class wakeup_timer_engine_linux : public wakeup_timer_engine_base
{
public:
    bool schedule_wakeup_timer(const client_id_t& client_id,
        const wakeup_timer& timer, const filesystem_path_t& app_path,
        const std::wstring& app_parameters)
    {
        // Set wakeup time
        //
        if (boost::filesystem::exists(wakeup_file))
        {
            time_t now = time(0);
            time_t wakeup = timer.wakeup_time;

            if (wakeup > now)
            {
                FILE* f = fopen(wakeup_file, "w+");

                if (f)
                {
                    bool ok = (fprintf(f, "%lu", wakeup) > 0);
                    fclose(f);
                    return ok;
                }
            }
        }

        return false;
    }

    bool get_wakeup_timer(const client_id_t& client_id, wakeup_timer& timer)
    {
        // Read wakeup time
        //
        if (boost::filesystem::exists(wakeup_file))
        {
            FILE* f = fopen(wakeup_file, "r");

            if (f)
            {
                char buf[64] = { 0 };
                bool ok = (fread(buf, sizeof(buf), 1, f) > 0);

                if (ok)
                {
                    timer.wakeup_time = strtoul(buf, 0, 10);
                }

                fclose(f);
                return ok;
            }
        }

        return false;
    }

    bool remove_wakeup_timer(const client_id_t& client_id)
    {
        if (boost::filesystem::exists(wakeup_file))
        {
            FILE* f = fopen(wakeup_file, "w+");
            
            if (f)
            {
                fclose(f);
                return true;
            }
        }

        return false;
    }
};

} //engine
} //dvblink
