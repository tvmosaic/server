/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_filesystem_path.h>
#include <dl_utils.h>
#include <mstask.h>
#include <wincred.h>
#include "win_task_scheduler_1_0.h"


namespace dvblink { namespace engine {

void release(IUnknown* obj);

void win_task_scheduler_1_0::add_timer_thread_func(const client_id_t& client_id,
    const wakeup_timer& timer, const filesystem_path_t& app_path, const std::wstring& app_parameters, bool& res)
{
    res = false;

    CoInitialize(NULL);

    ITaskScheduler* task_scheduler = NULL;

    HRESULT hr = CoCreateInstance(CLSID_CTaskScheduler, NULL, CLSCTX_INPROC_SERVER, IID_ITaskScheduler, (void**)&task_scheduler);
    if (hr == S_OK)
    {
        ITask* task = NULL;
        ITaskTrigger* task_trigger = NULL;
        IPersistFile* persist_file = NULL;

        //check whether this task exists already
        std::wstring task_name = client_id.to_wstring();
        hr = task_scheduler->Activate(task_name.c_str(), IID_ITask, (IUnknown**)&task);
        if (hr != S_OK)
        {
            //task does not exist yet - create it
            hr = task_scheduler->NewWorkItem(task_name.c_str(), CLSID_CTask, IID_ITask, (IUnknown**)&task);
        }
        if (hr == S_OK)
        {
            //set task properties
            //trigger
            //delete all existing triggers
             
            do 
            {
                unsigned short c;
                hr = task->GetTriggerCount(&c); if (FAILED(hr)) break;

                while (c)
                {
                    task->DeleteTrigger(--c);
                }
                //create a new trigger
                hr = task->CreateTrigger(&c, &task_trigger);
                if (FAILED(hr)) break;

                TASK_TRIGGER trigger = {0};
                trigger.cbTriggerSize = sizeof(trigger);

                boost::posix_time::ptime time = from_time_t(timer.wakeup_time);
                tm td = boost::posix_time::to_tm(time);

                trigger.wBeginDay = static_cast<unsigned short>(td.tm_mday);
                trigger.wBeginMonth = static_cast<unsigned short>(td.tm_mon + 1);
                trigger.wBeginYear = static_cast<unsigned short>(td.tm_year + 1900);
                trigger.wStartHour = static_cast<unsigned short>(td.tm_hour);
                trigger.wStartMinute = static_cast<unsigned short>(td.tm_min);

                if (timer.daily)
                {
                    trigger.TriggerType = TASK_TIME_TRIGGER_DAILY;
                    trigger.Type.Daily.DaysInterval = 1;
                }

                hr = task_trigger->SetTrigger(&trigger); if (FAILED(hr)) break;
                hr = task->SetApplicationName(app_path.get().c_str()); if (FAILED(hr)) break;
                if (!app_parameters.empty())
                {
                    hr = task->SetParameters(app_parameters.c_str()); if (FAILED(hr)) break;
                }

                hr = task->SetFlags(TASK_FLAG_SYSTEM_REQUIRED | TASK_FLAG_DELETE_WHEN_DONE); if (FAILED(hr)) break;
                hr = task->SetCreator(L"DVBLogic"); if (FAILED(hr)) break;

                // to run the task unser system account
                hr = task->SetAccountInformation(L"", NULL);  if (FAILED(hr)) break;
                hr = task->SetMaxRunTime(INFINITE);  if (FAILED(hr)) break;

                //save changes
                hr = task->QueryInterface(IID_IPersistFile, (void**)&persist_file);  if (FAILED(hr)) break;
                hr = persist_file->Save(NULL, TRUE);  if (FAILED(hr)) break;

                res = (hr == S_OK);

            } while (0);

            release(task_trigger);
            release(task);
            release(persist_file);
        }
        release(task_scheduler);
    }

    CoUninitialize();
}

void win_task_scheduler_1_0::get_timer_thread_func(const client_id_t& client_id, wakeup_timer& timer, bool& res)
{
    res = false;

    CoInitialize(NULL);

    HRESULT hr;
    ITaskScheduler* task_scheduler;
    hr = CoCreateInstance(CLSID_CTaskScheduler, NULL, CLSCTX_INPROC_SERVER, IID_ITaskScheduler, (void**)&task_scheduler);
    if (hr == S_OK)
    {
        ITask* task;
        //check whether this task exists already
        hr = task_scheduler->Activate(client_id.to_wstring().c_str(), IID_ITask, (IUnknown**)&task);
        if (hr == S_OK)
        {
            LPWSTR ppwszApplicationName;
            task->GetApplicationName(&ppwszApplicationName);
            wprintf(L"application name: %s\n", ppwszApplicationName);

            WORD trigger_count;
            hr = task->GetTriggerCount(&trigger_count);
            if (hr == S_OK)
            {
                for (WORD CurrentTrigger = 0; CurrentTrigger < trigger_count; CurrentTrigger++)
                {  
                    LPWSTR ppwszTrigger;
                    task->GetTriggerString(CurrentTrigger, &ppwszTrigger); 
                    if (!FAILED(hr))
                    {
                        wprintf(L"%i) %s\n",CurrentTrigger + 1, ppwszTrigger);
                    }
                }
            }

            LPWSTR ppwszAccount;
            if (task->GetAccountInformation(&ppwszAccount) == S_OK)
            {
                wprintf(L"account: %s\n", ppwszAccount);
            }

            //get task properties
            ITaskTrigger* task_trigger;
            hr = task->GetTrigger(0, &task_trigger);
            if (hr == S_OK)
            {
                TASK_TRIGGER trigger;
                hr = task_trigger->GetTrigger(&trigger);
                if (hr == S_OK)
                {
                    struct tm td_info;
                    td_info.tm_mday = trigger.wBeginDay;
                    td_info.tm_mon = trigger.wBeginMonth - 1;
                    td_info.tm_year = trigger.wBeginYear - 1900;
                    td_info.tm_hour = trigger.wStartHour;
                    td_info.tm_min = trigger.wStartMinute;

                    timer.wakeup_time = mktime(&td_info);
                    timer.daily = (trigger.TriggerType == TASK_TIME_TRIGGER_DAILY);
                }

                task_trigger->Release();
                res = SUCCEEDED(hr);
            }

            HRESULT status;
            hr = task->GetStatus(&status);

            task->Release();

        }
        task_scheduler->Release();
    }

    CoUninitialize();
}

void win_task_scheduler_1_0::delete_timer_thread_func(const client_id_t& client_id, bool& res)
{
    res = false;

    CoInitialize(NULL);

    HRESULT hr;
    ITaskScheduler* task_scheduler;
    hr = CoCreateInstance(CLSID_CTaskScheduler, NULL, CLSCTX_INPROC_SERVER, IID_ITaskScheduler, (void**)&task_scheduler);
    if (hr == S_OK)
    {
        hr = task_scheduler->Delete(client_id.to_wstring().c_str());
        res = SUCCEEDED(hr);
        task_scheduler->Release();
    }

    CoUninitialize();
}

} //engine
} //dvblink
