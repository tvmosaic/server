/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

// https://developer.apple.com/library/prerelease/mac/documentation/IOKit/Reference/IOPMLib_header_reference/#//apple_ref/c/func/IOPMSchedulePowerEvent

#pragma once

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <dl_types.h>
#include <dl_logger.h>
#include "wakeup_timer_engine.h"
#include <IOKit/IOKitLib.h>
#include <IOKit/IOMessage.h>
#include <IOKit/pwr_mgt/IOPMLib.h>

namespace dvblink {
namespace engine {

class wakeup_timer_engine_macos : public wakeup_timer_engine_base
{
public:
    bool schedule_wakeup_timer(const client_id_t& client_id,
        const wakeup_timer& timer, const filesystem_path_t& /* app_path */,
        const std::wstring& /* app_parameters */)
    {
        if (timer.wakeup_time < time(NULL))
        {
            //return false;
        }

        timers_t::iterator it = timers_.find(client_id);

        if (it != timers_.end())
        {
            if (it->second.wakeup_time == timer.wakeup_time)
            {
                // timer already exists
                return true;
            }

            // remove existing timer
            remove_wakeup_timer(client_id);
        }

        CFDateRef date_ref = CFDateCreate(kCFAllocatorDefault,
            (CFAbsoluteTime)(timer.wakeup_time - time_diff));

        IOReturn ret = IOPMSchedulePowerEvent(
            date_ref, NULL, CFSTR(kIOPMAutoWakeOrPowerOn)); // kIOPMAutoWake?

        CFRelease(date_ref);

        if (ret == kIOReturnSuccess)
        {
            timers_.insert(std::make_pair(client_id, timer));
            return true;
        } else
        {
        	logging::log_warning(L"schedule_wakeup_timer: Failed to schedule power event for %1%") % timer.wakeup_time;
        }

        return false;
    }

    bool get_wakeup_timer(const client_id_t& client_id, wakeup_timer& timer)
    {
        timers_t::iterator it = timers_.find(client_id);

        if (it == timers_.end())
        {
            return false; // timer not found
        }

        timer = it->second;
        return true;
    }

    bool remove_wakeup_timer(const client_id_t& client_id)
    {
        timers_t::iterator it = timers_.find(client_id);
        
        if (it == timers_.end())
        {
            return true; // timer not found
        }

        const wakeup_timer wt = it->second;
        timers_.erase(it);

        if ((wt.wakeup_time > time_diff) && (wt.wakeup_time > time(NULL)))
        {
            bool found = false;
            timers_t::const_iterator it = timers_.begin();

            while (it != timers_.end())
            {
                if (it->second.wakeup_time == wt.wakeup_time)
                {
                    found = true;
                    break;
                }

                ++it;
            }

            if (!found)
            {
                CFDateRef date_ref = CFDateCreate(kCFAllocatorDefault,
                    (CFAbsoluteTime)(wt.wakeup_time - time_diff));

                IOReturn ret = IOPMCancelScheduledPowerEvent(
                    date_ref, NULL, CFSTR(kIOPMAutoWakeOrPowerOn));  // kIOPMAutoWake?

                CFRelease(date_ref);
                return (ret == kIOReturnSuccess);
            }
        }

        return true;
    }

private:
    static const time_t time_diff = 978307200;
    typedef std::map<client_id_t, wakeup_timer> timers_t;
    timers_t timers_;
};

} //engine
} //dvblink
