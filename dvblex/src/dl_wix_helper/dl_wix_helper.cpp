/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <boost/filesystem.hpp>
#include <dl_filesystem_path.h>
#include <dl_permanent_settings.h>

using namespace dvblink;
using namespace dvblex;
using namespace dvblex::settings;

// DllMain - Initialize and cleanup WiX custom action utils.
extern "C" BOOL WINAPI DllMain(__in HINSTANCE hInst, __in ULONG ulReason, __in LPVOID)
{
	switch(ulReason)
	{
	case DLL_PROCESS_ATTACH:
		WcaGlobalInitialize(hInst);
		break;

	case DLL_PROCESS_DETACH:
		WcaGlobalFinalize();
		break;
	}

	return TRUE;
}

UINT __stdcall DLGetInstallPath(MSIHANDLE hInstall)
{
	HRESULT hr = S_OK;
	UINT er = ERROR_SUCCESS;

	filesystem_path_t install_dir;
	try
	{
		install_dir = permanent_settings::get_server_install_path();
	} catch(...)
	{
		ExitOnFailure(hr, "tvmosaic not installed");
	}

	hr = WcaInitialize(hInstall, "DLGetInstallPath");
	ExitOnFailure(hr, "Failed to initialize");

	if (install_dir.get().size() > 0)
	{
		OutputDebugString(L"Setting INSTALLDIR to ");
		OutputDebugString(install_dir.get().c_str());

		hr = WcaSetProperty(L"INSTALLDIR", install_dir.get().c_str());
		ExitOnFailure(hr, "Failed to set INSTALLDIR property value");
	}

LExit:
	er = SUCCEEDED(hr) ? ERROR_SUCCESS : ERROR_INSTALL_FAILURE;
	return WcaFinalize(er);
}

UINT __stdcall DLCleanupOnUninstall(MSIHANDLE hInstall)
{
	HRESULT hr = S_OK;
	UINT er = ERROR_SUCCESS;
/*
	filesystem_path_t root_dir;
	filesystem_path_t all_user_dir;

	hr = WcaInitialize(hInstall, "DLCleanupOnUninstall");
	ExitOnFailure(hr, "Failed to initialize");

	//dvblink root directory 

	try
	{
		root_dir = installation_settings::get_root_directory();
	} catch(...)
	{
		OutputDebugString(L"Failed to get dvblink root directory");
		ExitOnFailure(hr, "dvblink server not installed");
	}

	OutputDebugString(L"Recursively deleting the contents of DVBLink install folder:");
	OutputDebugString(root_dir.get().c_str());

	try
	{
		boost::filesystem::remove_all(root_dir.get().c_str());
	} catch(...){}

	//all users directory

	try
	{
	    if (!installation_settings::get_dvblink_all_users_directory(all_user_dir))
			all_user_dir = L"";
	} catch(...)
	{
		OutputDebugString(L"Failed to get dvblink all users directory");
		ExitOnFailure(hr, "dvblink server not installed");
	}

	OutputDebugString(L"Recursively deleting the contents of DVBLink all users folder:");
	OutputDebugString(all_user_dir.get().c_str());

	try
	{
		boost::filesystem::remove_all(all_user_dir.get().c_str());
	} catch(...){}
*/
LExit:
	er = SUCCEEDED(hr) ? ERROR_SUCCESS : ERROR_INSTALL_FAILURE;
	return WcaFinalize(er);
}

