/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_locale_strings.h>
#include <dl_language_settings.h>
#include "device_reactor.h"
#include "dvb/dvb_device_factory.h"
#include "file/file_device_factory.h"
#include "iptv/iptv_device_factory.h"

using namespace dvblex;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink;

#if defined(_DEBUG)
    static const char* device_factory_names[] = {dvb_device_factory_t::get_name(), file_device_factory_t::get_name(), iptv_device_factory_t::get_name()};
#else
    static const char* device_factory_names[] = {dvb_device_factory_t::get_name(), iptv_device_factory_t::get_name()};
#endif

static const size_t device_factory_names_num = sizeof(device_factory_names) / sizeof(char*);

device_reactor_t::device_reactor_t(directory_settings_obj_t& directory_settings) :
    directory_settings_(directory_settings)
{
    for (size_t j=0; j<device_factory_names_num; j++)
    {
        device_factory_t* df = create_device_factory_by_name(device_factory_names[j]);
        if (df != NULL)
            device_factory_map_[device_factory_names[j]] = df;
    }
}

device_reactor_t::~device_reactor_t()
{
    device_factory_map_t::iterator it = device_factory_map_.begin();
    while (it != device_factory_map_.end())
    {
        delete it->second;
        ++it;
    }
}

bool device_reactor_t::get_device_list(const dvblink::device_uuid_t& u, device_descriptor_list_t& device_list)
{
    log_info(L"device_reactor_t::get_device_list (%1%)") % string_cast<EC_UTF8>(u.get());

    std::string device_factory_name;
    if (!u.empty())
        device_factory_name = device_factory_t::get_device_factory_from_id(u.get());

    device_list.clear();
    for (size_t j=0; j<device_factory_names_num; j++)
    {
        if (device_factory_name.empty() || boost::iequals(device_factory_name, device_factory_names[j]))
        {
            device_factory_t* df = get_device_factory_by_name(device_factory_names[j]);
            if (df != NULL)
            {
                device_descriptor_list_t dl;
                if (df->get_device_list(u, dl))
                    device_list.insert(device_list.end(), dl.begin(), dl.end());
            }
        }
    }

    return true;
}

device_t* device_reactor_t::create_device(const dvblink::device_id_t& device_id)
{
    device_t* ret_val = NULL;

    log_info(L"device_reactor_t::create_device %1%") % string_cast<EC_UTF8>(device_id.get());

    std::string device_factory_name = device_factory_t::get_device_factory_from_id(device_id);

    device_factory_t* df = get_device_factory_by_name(device_factory_name);

    if (df != NULL)
    {
        ret_val = df->create_device(device_id, directory_settings_);
    } else
    {
        log_error(L"device_reactor_t::create_device. Unable to create device factory for device id %1%") % string_cast<EC_UTF8>(device_id.get());
    }

    return ret_val;
}

device_playback_src_obj_t device_reactor_t::get_playback_src_for_device(const dvblink::device_id_t& device_id)
{
    device_playback_src_obj_t ret_val;

    log_info(L"device_reactor_t::get_playback_src_for_device %1%") % string_cast<EC_UTF8>(device_id.get());

    std::string device_factory_name = device_factory_t::get_device_factory_from_id(device_id);

    device_factory_t* df = get_device_factory_by_name(device_factory_name);

    if (df != NULL)
    {
        ret_val = df->get_playback_src(device_id);
    } else
    {
        log_error(L"device_reactor_t::get_playback_src_for_device. Unable to create device factory for device id %1%") % string_cast<EC_UTF8>(device_id.get());
    }

    return ret_val;
}

bool device_reactor_t::get_device_info(const dvblink::device_id_t& device_id, device_descriptor_t& device_info)
{
    bool ret_val = false;

    std::string device_factory_name = device_factory_t::get_device_factory_from_id(device_id);

    device_factory_t* df = get_device_factory_by_name(device_factory_name);

    if (df != NULL)
    {
        ret_val = df->get_device_info(device_id, device_info);
    }

    return ret_val;
}

device_factory_t* device_reactor_t::create_device_factory_by_name(const std::string& name)
{
    device_factory_t* ret_val = NULL;

    const dvblink::filesystem_path_t config_path = directory_settings_->get_devices_path() / string_cast<EC_UTF8>(name);

    if (boost::iequals(name, dvb_device_factory_t::get_name()))
    {
        ret_val = new dvb_device_factory_t(config_path);
    } else
    if (boost::iequals(name, file_device_factory_t::get_name()))
    {
        ret_val = new file_device_factory_t(config_path);
    } else
    if (boost::iequals(name, iptv_device_factory_t::get_name()))
    {
        ret_val = new iptv_device_factory_t(config_path);
    }
    return ret_val;
}

device_factory_t* device_reactor_t::get_device_factory_by_name(const std::string& name)
{
    device_factory_t* ret_val = NULL;

    if (device_factory_map_.find(name) != device_factory_map_.end())
        ret_val = device_factory_map_[name];

    return ret_val;
}

void device_reactor_t::get_device_aux_list(aux_module_list_t& aux_list)
{
    for (size_t j=0; j<device_factory_names_num; j++)
    {
        device_factory_t* df = get_device_factory_by_name(device_factory_names[j]);
        if (df != NULL)
        {
            aux_module_list_t al;
            df->get_aux_list(al);
            aux_list.insert(aux_list.end(), al.begin(), al.end());
        }
    }
}

source_type_e device_reactor_t::get_source_type_from_tuning_params(const dvblink::device_uuid_t& u, const dvblink::channel_tuning_params_t& tune_params)
{
    source_type_e ret_val = st_unknown;

    std::string device_factory_name = device_factory_t::get_device_factory_from_id(u.get());

    device_factory_t* df = get_device_factory_by_name(device_factory_name);

    if (df != NULL)
    {
        ret_val = df->get_source_type_from_tuning_params(tune_params);
    }

    return ret_val;
}

bool device_reactor_t::get_device_configurable_props(const dvblink::device_id_t& device_id, configurable_device_props_t& props)
{
    bool ret_val = false;

    std::string device_factory_name = device_factory_t::get_device_factory_from_id(device_id);

    device_factory_t* df = get_device_factory_by_name(device_factory_name);

    if (df != NULL)
    {
        ret_val = df->get_device_configurable_props(device_id, props);
    }

    return ret_val;
}

set_device_props_result_e device_reactor_t::set_device_configurable_props(const dvblink::device_id_t& device_id, const concise_param_map_t& props)
{
    set_device_props_result_e ret_val = sdpr_error;

    std::string device_factory_name = device_factory_t::get_device_factory_from_id(device_id);

    device_factory_t* df = get_device_factory_by_name(device_factory_name);

    if (df != NULL)
    {
        ret_val = df->set_device_configurable_props(device_id, props);
    }

    return ret_val;
}

const static std::string add_device_container_id = "fb8e2312-b8a1-41a2-aef4-e35d801f6fcd";
const static std::string device_type_selectable_id = "1c25696c-002f-4bdd-9f5a-8e41878625e8";

void device_reactor_t::get_device_template_list(parameters_container_t& templates)
{
    templates.id_ = add_device_container_id;
    templates.selectable_params_.push_back(selectable_param_description_t());
    selectable_param_description_t& device_templates = templates.selectable_params_[0];

    device_templates.key_ = manual_device_template_id_key;
    device_templates.name_ = language_settings::GetInstance()->GetItemName(manual_device_type_name);

    for (size_t j=0; j<device_factory_names_num; j++)
    {
        device_factory_t* df = get_device_factory_by_name(device_factory_names[j]);
        if (df != NULL)
        {
            selectable_param_list_t tl;
            df->get_device_template_list(tl);
            device_templates.parameters_list_.insert(device_templates.parameters_list_.end(), tl.begin(), tl.end());
        }
    }
}

bool device_reactor_t::create_manual_device(const concise_param_map_t& device_params)
{
    bool ret_val = false;
    if (is_key_present(manual_device_template_id_key, device_params))
    {
        dvblink::parameter_value_t type = get_value_for_param(manual_device_template_id_key, device_params);

        std::string device_factory_name = device_factory_t::get_device_factory_from_id(type.get());

        device_factory_t* df = get_device_factory_by_name(device_factory_name);

        if (df != NULL)
        {
            ret_val = df->create_manual_device(device_params);
        }

    } else
    {
        log_error(L"device_reactor_t::create_device. No device type key found in params map");
    }
    return ret_val;
}

bool device_reactor_t::delete_manual_device(const dvblink::device_id_t& device_id)
{
    bool ret_val = false;

    std::string device_factory_name = device_factory_t::get_device_factory_from_id(device_id);

    device_factory_t* df = get_device_factory_by_name(device_factory_name);

    if (df != NULL)
    {
        ret_val = df->delete_manual_device(device_id);
    }

    return ret_val;
}

void device_reactor_t::get_manual_devices(manual_device_list_t& devices)
{
    for (size_t j=0; j<device_factory_names_num; j++)
    {
        device_factory_t* df = get_device_factory_by_name(device_factory_names[j]);
        if (df != NULL)
        {
            manual_device_list_t mdl;
            df->get_manual_devices(mdl);
            devices.insert(devices.end(), mdl.begin(), mdl.end());
        }
    }
}

void device_reactor_t::add_manual_devices(const manual_device_list_t& devices)
{
    for (size_t i=0; i<devices.size(); i++)
    {
        std::string device_factory_name = device_factory_t::get_device_factory_from_id(devices[i].id_);

        device_factory_t* df = get_device_factory_by_name(device_factory_name);

        if (df != NULL)
        {
            df->add_manual_device(devices[i]);
        }
    }
}

