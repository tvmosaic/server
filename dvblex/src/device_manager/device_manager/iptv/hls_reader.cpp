/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include "hls_reader.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

hls_reader::hls_reader(ts_circle_buffer* input_stream, const char* user_agent) :
    output_buffer_(input_stream)
{
	m_PlaylistLoader = new m3u8_loader(user_agent);
	m_SegmentStreamer = new hls_segment_streamer(user_agent);
}

hls_reader::~hls_reader()
{
    delete m_SegmentStreamer;
    delete m_PlaylistLoader;
}

bool hls_reader::create(const char* url, const char* localAddress)
{
    url_ = url;
    return true;
}

bool hls_reader::start(int restart_count)
{
    m_PlaylistLoader->Start(url_.c_str(), &m_SegmentList);
    m_SegmentStreamer->StartReading(&m_SegmentList, StreamingCallback, this);

	m_SegmentStreamer->StartStreaming();
    return true;
}

void hls_reader::stop()
{
    m_SegmentStreamer->Stop();
    m_PlaylistLoader->Stop();
    m_SegmentList.Clear();
}

void hls_reader::StreamingCallback(unsigned char* buf, int len, void* param)
{
	hls_reader* parent = (hls_reader*)param;
    parent->output_buffer_->write_stream(buf, len);
}
