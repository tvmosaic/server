/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_language_settings.h>
#include <dl_locale_strings.h>
#include "xcodes_vod_provider.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

static const std::string IPTV_XTREAM_CODES_VOD_CONTAINER_ID = "170daf53-10fd-43fd-b47a-ab06b5f93f79";

xcodes_vod_provider_t::xcodes_vod_provider_t(const xtream_codes_info_t& xci)
    : xci_(xci)
{
}

xcodes_vod_provider_t::~xcodes_vod_provider_t()
{
}

void xcodes_vod_provider_t::read_containers()
{
    categories_.clear();
    groups_.clear();
    container_update_time_map_.clear();
    items_update_time_map_.clear();
    xtream_codes_series_map_.clear();

    time_t now;
    time(&now);

    xtream_codes_scanner_t xcs;
    bool exit_flag = false;
    
    //read vod categories
    xtream_codes_scanner_t::xtream_codes_cat_list_t vod_categories;
    xcs.read_vod_categories(xci_, true, &exit_flag, vod_categories);

    std::string vod_cat_id = IPTV_XTREAM_CODES_VOD_CONTAINER_ID;
    categories_[vod_cat_id] = vod_category_t(language_settings::GetInstance()->GetItemName(item_id_t(IDS_IPTV_PB_SRC_VOD)));
    container_update_time_map_[vod_cat_id] = now;
    for (size_t i=0; i<vod_categories.size(); i++)
    {
        categories_[vod_cat_id].groups_.push_back(vod_categories[i].id_);
        groups_[vod_categories[i].id_] = vod_group_t(vod_categories[i].name_);
        container_update_time_map_[vod_categories[i].id_] = now;
    }
    
    //read series 
    xtream_codes_scanner_t::xtream_codes_cat_list_t series_categories;
    xcs.read_vod_categories(xci_, false, &exit_flag, series_categories);
    for (size_t i=0; i<series_categories.size(); i++)
    {
        std::string series_cat_id = series_categories[i].id_;
        categories_[series_cat_id] = vod_category_t(series_categories[i].name_);
        container_update_time_map_[series_cat_id] = now;

        xtream_codes_scanner_t::xtream_codes_series_map_t cat_series;
        xcs.read_category_series(xci_, &exit_flag, series_cat_id, cat_series);
        xtream_codes_scanner_t::xtream_codes_series_map_t::iterator series_it = cat_series.begin();
        while (series_it != cat_series.end())
        {
            xtream_codes_series_map_[series_it->first] = series_it->second;

            categories_[series_cat_id].groups_.push_back(series_it->first);
            groups_[series_it->first] = vod_group_t(series_it->second.name_);
            container_update_time_map_[series_it->first] = now;

            ++series_it;
        }
    }    
}

void xcodes_vod_provider_t::read_items(const std::string& group_id)
{
    time_t now;
    time(&now);

    vod_group_map_t::iterator group_it = groups_.find(group_id);
    if (group_it != groups_.end())
    {
        xtream_codes_scanner_t xcs;
        bool exit_flag = false;

        if (xtream_codes_series_map_.find(group_id) == xtream_codes_series_map_.end())
            xcs.read_vod_category_items(xci_, &exit_flag, group_id, group_it->second.items_);
        else
            xcs.read_series_items(xci_, &exit_flag, xtream_codes_series_map_[group_id], group_it->second.items_);

        vod_item_obj_map_t::iterator items_it = group_it->second.items_.begin();
        while (items_it != group_it->second.items_.end())
        {
            group_it->second.items_sort_list_.push_back(vod_item_desc_t(items_it->second->get_id(), items_it->second->video_info_.m_Name));
            ++items_it;
        }

        try {
            std::sort(group_it->second.items_sort_list_.begin(), group_it->second.items_sort_list_.end(), vod_item_name_sorter(&exit_flag));
        } catch (...)
        {
            //exit was signalled
        }
        items_update_time_map_[group_id] = now;
    }
}

void xcodes_vod_provider_t::add_item_metadata(const std::string& group_id, const std::string& item_id, vod_item_obj_t& item)
{
    if (xtream_codes_series_map_.find(group_id) == xtream_codes_series_map_.end())
    {
        xtream_codes_scanner_t xcs;
        bool exit_flag = false;

        xcs.read_vod_item_info(xci_, &exit_flag, item_id, item);
    }
}
