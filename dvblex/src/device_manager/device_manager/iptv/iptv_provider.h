/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <dl_parameters.h>
#include "playlist_reader.h"
#include "iptv_vod_provider.h"

namespace dvblex { 
class iptv_provider_t
{
public:
    iptv_provider_t();
    virtual ~iptv_provider_t();

    bool scan_channels(const concise_param_map_t& scanner_settings, playlist_channel_map_t& channel_map, std::string& log_str);
    bool get_default_xmltv_url(const concise_param_map_t& scanner_settings, dvblink::url_address_t& url);
    iptv_vod_provider_t* get_iptv_vod_provider(const concise_param_map_t& scanner_settings);

protected:
};

}
