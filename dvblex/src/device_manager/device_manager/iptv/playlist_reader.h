/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>
#include <vector>
#include <map>
#include <sstream>

#include <dl_filesystem_path.h>
#include <dl_network_helper.h>
#include <dl_hash.h>
#include <dl_channel_info.h>
#include <dl_m3u.h>

namespace dvblex {

enum playlist_reader_result_e
{
    prr_ok,
    prr_not_found,
    prr_no_channels
};

typedef std::map<std::string, std::string>  m3u_plus_tag_map_t;

const int UNKNOWN_CHANNEL_NUMBER               = -1;

struct playlist_channel
{
    std::string url;
    std::string name;
    int number;
    int subnumber;
    bool is_vod;

    //extra tags
    m3u_plus_tag_map_t tags;

    playlist_channel() :
        number(-1), subnumber(-1), is_vod(false)
    {}
    
    const std::string get_channel_id() const
    {
        boost::uint64_t hash = dvblink::engine::calculate_64bit_string_hash(url);
        std::stringstream strbuf;
        strbuf << st_iptv << ":" << hash;

        return strbuf.str();
    }

    std::string get_channel_name()
    {
        std::string ret_val = name;

        //if there is a tag tvg-name, use it as a channel name
        if (tags.find(m3u_plus_tvg_name) != tags.end())
            ret_val = boost::algorithm::trim_copy_if(tags[m3u_plus_tvg_name], boost::algorithm::is_any_of(" "));

        return ret_val;
    }

    bool get_channel_epg_offset(time_t& offset, std::string& tvgid_id)
    {
        bool ret_val = false;

        if (tags.find(m3u_plus_tvg_shift) != tags.end() && tags.find(m3u_plus_tvg_id) != tags.end())
        {
            tvgid_id = boost::to_lower_copy(tags[m3u_plus_tvg_id]);
            offset = atoi(tags[m3u_plus_tvg_shift].c_str()) * 3600;
            ret_val = true;
        }

        return ret_val;
    }

    void reset()
    {
        url.clear();
        name.clear();
        number = -1;
        subnumber = -1;
        tags.clear();
        is_vod = false;
    }

    void set_channel_number_from_str(const std::string& str)
    {
        if (sscanf(str.c_str(), "%d.%d", &number, &subnumber) != 2)
            number = atoi(str.c_str());

        if (number <= 0)
            number = UNKNOWN_CHANNEL_NUMBER;
    }

    channel_type_e get_channel_type()
    {
        channel_type_e ret_val = ct_tv;

        if (tags.find(m3u_plus_tvg_type) != tags.end() && 
            boost::iequals(tags[m3u_plus_tvg_type], m3u_plus_tvg_type_radio))
        ret_val = ct_radio;

        return ret_val;
    }

};

typedef std::map<std::string, playlist_channel> playlist_channel_map_t;

class playlist_reader
{
public:
    playlist_reader(const std::string& playlist_path = "") :
      playlist_path_(playlist_path) { }

    void set_playlist(const std::string& playlist_path) { playlist_path_ = playlist_path; }
    const std::string get_playlist() const { return playlist_path_; }
    playlist_reader_result_e get_channels(playlist_channel_map_t& channel_map) const;

private:
    bool get_playlist_content(playlist_channel_map_t& channel_map) const;
    bool get_local_playlist(playlist_channel_map_t& channel_map) const;
    bool get_remote_playlist(playlist_channel_map_t& channel_map) const;
    bool parse_channel_info(const std::string& param_str, playlist_channel& channel) const;
    bool parse_channel_params(const std::string& param_str, playlist_channel& channel_param) const;
    void convert_content_to_channel_map(const std::vector<std::string>& content, playlist_channel_map_t& channel_map) const;
    bool parse_hdhomerun_channels(const std::string& response, playlist_channel_map_t& channel_map) const;
    void process_exttv_line(const std::string& line, std::string& group, std::string& xmltv_id, std::string& logo_url) const;

private:
    std::string playlist_path_;
};

} //dvblex
