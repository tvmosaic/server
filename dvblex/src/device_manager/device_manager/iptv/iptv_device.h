/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include "../device.h"
#include "../directory_settings.h"

#include "iptv_props.h"

namespace dvblex { 

class iptv_epg_scanner_t;

class iptv_device_t : public device_t
{
public:
    iptv_device_t(iptv_device_props_t* iptv_device_props, directory_settings_obj_t& dir_settings);
    virtual ~iptv_device_t();

    virtual channel_scanner_t* get_channel_scanner();
    virtual void release_channel_scanner(channel_scanner_t* scanner);

    virtual program_streamer_t* get_program_streamer(const dvblink::channel_id_t& channel_id, const concise_channel_tune_info_t& tune_params);
    virtual void release_program_streamer(program_streamer_t* streamer);

    virtual epg_scanner_t* get_background_epg_scanner();

    virtual epg_scanner_t* get_epg_scanner();
    virtual void release_epg_scanner(epg_scanner_t* epg_scanner);

    virtual bool get_channel_url_for_format(const concise_channel_tune_info_t& tune_params, device_channel_stream_type_e format, 
        dvblink::url_address_t& url, dvblink::mime_type_t& mime);

protected:
    iptv_device_props_t* iptv_device_props_;
    directory_settings_obj_t dir_settings_;
    iptv_epg_scanner_t* background_epg_scanner_;
};

}
