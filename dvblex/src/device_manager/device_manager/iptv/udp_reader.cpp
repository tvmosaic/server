/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_network_helper.h>
#include "udp_reader.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

udp_reader::udp_reader(dvblink::engine::ts_circle_buffer* input_buffer) :
    receive_started_(false), streaming_thread_(NULL), input_buffer_(input_buffer)
{
}

bool udp_reader::create(const char* url, const char* local_address)
{
    std::string remote_address;
    std::string user;
    std::string pswd;
    std::string url_suffix;
    boost::uint16_t port;

    if (network_helper::parse_net_url(url, remote_address, user, pswd, port, url_suffix) == DL_NET_PROTO_UNKNOWN)
    {
        log_error(L"udp_reader::Create. Error parse network URL: %s") % string_cast<EC_UTF8>(url);
        return false;
    }

    //when source-specific multicast is used (e.g. rtp://87.141.215.251@232.0.20.35:10000), user gets the source address
    return open_connection(local_address, remote_address.c_str(), user.c_str(), port);
}

bool udp_reader::open_connection(const char* local_address, const char* remote_address, const char* mcast_source_address, boost::uint16_t port)
{
    bool ret = true;

    sock_addr local_addr;
    sock_addr remote_addr;
    sock_addr mcast_src_addr;

    do // dummy loop
    {
        std::string str_local_addr = local_address;
        std::string str_remote_addr = remote_address;
        std::string str_mcast_src_addr = mcast_source_address;

        log_info(L"udp_reader::create. LocalAddr=%1%, RemoteAddr=%2%, mcast_src_addr=%3%, Port=%4%")
            % string_cast<EC_UTF8>(str_local_addr) % string_cast<EC_UTF8>(str_remote_addr) % string_cast<EC_UTF8>(str_mcast_src_addr) % port;

        errcode_t err = err_none;

        // create the multicast socket
        //
        sock_ptr_ = udp_socket::sock_ptr(new udp_socket);
        sock_ptr_->set_blocking_mode(false);
        sock_ptr_->set_reuse_addr(true);
        sock_ptr_->set_recv_bufsize(RTPUDP_RECVBUFFER);

        if (!str_remote_addr.empty())
        {
            err = remote_addr.set_address(str_remote_addr);

            if (err != err_none)
            {
                log_error(L"udp_reader::create. Cannot resolve group address");
                ret = false;
                break;
            }
        }

        if (!str_mcast_src_addr.empty())
        {
            err = mcast_src_addr.set_address(str_mcast_src_addr);

            if (err != err_none)
            {
                log_error(L"udp_reader::create. Cannot resolve multicast source address");
                ret = false;
                break;
            }
        }

        if (!str_local_addr.empty())
        {
            err = local_addr.set_address(str_local_addr);

            if (err != err_none)
            {
                log_error(L"udp_reader::create. Cannot resolve local address");
                ret = false;
                break;
            }
        }
        local_addr.set_port(port);
    
        //bind socket
        sock_addr bind_addr;
        if (remote_addr.is_multicast()) // multicast address?
        {
#ifdef _WIN32
            bind_addr = local_addr;
#else
            bind_addr = remote_addr;    //this will filter udp messages, not destined for a particular multicast group (T-Home Entertain 2x streams bug!)
                                        //alternatively IP_MULTICAST_ALL can be used (since kernel 2.6.31).
                                        //Windows does filtering using multicast group membership by default
#endif
        } else
        {
#ifdef _WIN32
            bind_addr = local_addr;
#else
            //bind to INADDR_ANY by default
#endif
        }
        bind_addr.set_port(port);
        
        err = sock_ptr_->bind(bind_addr);

        if (err != err_none)
        {
            log_error(L"udp_reader::create. bind() failed");
            ret = false;
            break;
        }

        if (remote_addr.is_multicast()) // multicast address?
        {
            sock_ptr_->set_multicast_if(local_addr);

            if (mcast_src_addr.is_inaddr_any())
            {
                // join the multicast group
                //
                err = sock_ptr_->add_membership(remote_addr, local_addr);

                if (err != err_none)
                {
                    log_error(L"udp_reader::create. add_membership() failed");
                    ret = false;
                    break;
                }
                else
                {
                    log_info(L"udp_reader::create. Joined multicast group");
                }
            } else 
            {
                // join the source-specific multicast group
                //
                err = sock_ptr_->add_ssm_membership(remote_addr, mcast_src_addr, local_addr);

                if (err != err_none)
                {
                    log_error(L"udp_reader::create. add_ssm_membership() failed");
                    ret = false;
                    break;
                }
                else
                {
                    log_info(L"udp_reader::create. Joined source-specific multicast group");
                }
            }
        }
    }
    while (0);

    if (!ret)
    {
        sock_ptr_.reset(); // close socket
    }
    else
    {
        local_addr_ = local_addr;
        remote_addr_ = remote_addr;
        mcast_src_addr_ = mcast_src_addr;
    }

    return ret;
}

bool udp_reader::start(int restart_count)
{
    // Start receive streaming thread	 
    receive_started_ = true;
    streaming_thread_ = new boost::thread(boost::bind(&udp_reader::thread_function, this));

    return true;
}

void udp_reader::stop()
{
    if (streaming_thread_)
    {
        receive_started_ = false;
        streaming_thread_->join();
        delete streaming_thread_;
        streaming_thread_ = NULL;
    }

    sock_ptr_.reset(); // close socket
}

void udp_reader::thread_function()
{
    stream_receive_function();
}

void udp_reader::stream_receive_function()
{
    log_info(L"udp_reader::stream_receive_function. Thread started");
    
    std::string recv_buf(RECV_BUFSIZE, '\0');
    unsigned char* buf_ptr = (unsigned char*)&recv_buf[0];
    size_t buf_size = recv_buf.size();

    // reset circular buffers
    input_buffer_->reset();
        
    namespace pt = boost::posix_time;
    const timeout_t wait_timeout = pt::milliseconds(100);
    bool data_received = false;

    while (receive_started_ && sock_ptr_)
    {
        errcode_t err = sock_ptr_->wait_for_readable(wait_timeout);

        if (err != err_none)
        {
            continue;
        }

        size_t bytes_received = 0;
        dvblink::sock_addr from;

        err = sock_ptr_->receive_datagram(buf_ptr, buf_size,
            bytes_received, from, false);

        if (err != err_none)
        {
            log_info(L"udp_reader::stream_receive_function. Thread finished");
            receive_started_ = false;
            break;
        }

        if (!data_received && (bytes_received > 0))
        {
            data_received = true;
            log_info(L"udp_reader::stream_receive_function. First packet received (%1% bytes)") % bytes_received;
        }

        input_buffer_->write_stream(buf_ptr, bytes_received);
    }

    sock_ptr_.reset(); // close socket
}
