/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include "../device.h"
#include "../device_factory.h"
#include <dl_channel_info.h>

namespace dvblex { 

class iptv_device_props_t;

class iptv_device_factory_t : public device_factory_t
{
public:
    static const char* get_name(){return "iptv";}

    iptv_device_factory_t(const dvblink::filesystem_path_t& device_config_path);
    virtual ~iptv_device_factory_t();

    virtual device_t* create_device(const dvblink::device_id_t& device_id, directory_settings_obj_t& dir_settings);

    virtual source_type_e get_source_type_from_tuning_params(const dvblink::channel_tuning_params_t& tune_params)
    {
        return st_iptv;
    }

    virtual std::string get_device_factory_name(){return get_name();}

    virtual void get_device_template_list(selectable_param_list_t& templates);
    virtual bool get_device_configurable_props(const dvblink::device_id_t& device_id, configurable_device_props_t& props);

    virtual set_device_props_result_e set_device_configurable_props(const dvblink::device_id_t& device_id, const concise_param_map_t& props);

    virtual device_playback_src_obj_t get_playback_src(const dvblink::device_id_t& device_id);

protected:
    virtual void get_device_desc_for_manual_device(const dvblink::device_id_t& id, 
        const concise_param_map_t& params, device_descriptor_t& dd);

    iptv_device_props_t* get_iptv_device_props(const dvblink::device_id_t& device_id);
};

}
