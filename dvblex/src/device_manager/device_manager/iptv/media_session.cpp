/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <locale.h>
#include <string>
#include <boost/algorithm/string.hpp>
#include "media_session.h"
#include "utility.h"

media_session::media_session() :
    subsession_head_(NULL),
    subsession_tail_(NULL),
    max_play_start_time_(0.0f),
    max_play_end_time_(0.0f),
    scale_(1.0f),
    media_session_type_(NULL),
    session_name_(NULL),
    session_descr_(NULL),
    control_path_(NULL) 
{
    source_filter_addr_.s_addr = 0;
}

media_session::~media_session() 
{
    delete subsession_head_;
    delete[] media_session_type_;
    delete[] session_name_;
    delete[] session_descr_;
    delete[] control_path_;
}

bool media_session::initializeWithSDP(char const* sdpDescription) 
{
    if (sdpDescription == NULL)
    {
        return false;
    }

    // Begin by processing all SDP lines until we see the first "m="
    char const* sdpLine = sdpDescription;
    char const* nextSDPLine;
    while (1) 
    {
        if (!parseSDPLine(sdpLine, nextSDPLine)) 
        {
            return false;
        }
        //##### We should really check for:
        // - "a=control:" attributes (to set the URL for aggregate control)
        // - the correct SDP version (v=0)
        if (sdpLine[0] == 'm')
        {
            break;
        }
        sdpLine = nextSDPLine;
        if (sdpLine == NULL)
        {
            break; // there are no m= lines at all
        }

        // Check for various special SDP lines that we understand:
        if (parseSDPLine_s(sdpLine))
        {
            continue;
        }

        if (parseSDPLine_i(sdpLine))
        {
            continue;
        }

        if (parseSDPLine_c(sdpLine)) 
        {
            continue;
        }

        if (parseSDPAttribute_control(sdpLine)) 
        {
            continue;
        }

        if (parseSDPAttribute_range(sdpLine))
        {
            continue;
        }

        if (parseSDPAttribute_type(sdpLine))
        {
            continue;
        }

        if (parseSDPAttribute_source_filter(sdpLine))
        {
            continue;
        }
    }

    while (sdpLine != NULL)
    {
        // We have a "m=" line, representing a new sub-session:
        media_subsession* subsession = new media_subsession(*this);
        if (subsession == NULL) 
        {
            return false;
        }

        // Parse the line as "m=<medium_name> <client_portNum> RTP/AVP <fmt>"
        // or "m=<medium_name> <client_portNum>/<num_ports> RTP/AVP <fmt>"
        // (Should we be checking for >1 payload format number here?)#####
        char* mediumName = utility::StrDupSize(sdpLine); // ensures we have enough space
        std::string protocolName;
        unsigned payloadFormat;
        if ((sscanf(sdpLine, "m=%s %hu RTP/AVP %u", mediumName, &subsession->client_port_num_, &payloadFormat) == 3 ||
            sscanf(sdpLine, "m=%s %hu/%*u RTP/AVP %u", mediumName, &subsession->client_port_num_, &payloadFormat) == 3) && 
            payloadFormat <= 127) 
        {
            protocolName = "RTP";
        } 
        else if ((sscanf(sdpLine, "m=%s %hu UDP %u", mediumName, &subsession->client_port_num_, &payloadFormat) == 3 ||
            sscanf(sdpLine, "m=%s %hu udp %u", mediumName, &subsession->client_port_num_, &payloadFormat) == 3 ||
            sscanf(sdpLine, "m=%s %hu RAW/RAW/UDP %u", mediumName, &subsession->client_port_num_, &payloadFormat) == 3) &&
            payloadFormat <= 127) 
        {
            // This is a RAW UDP source
            protocolName = "UDP";
        } 
        else 
        {
            // This "m=" line is bad; output an error message saying so:
            char* sdpLineStr;
            if (nextSDPLine == NULL) 
            {
                sdpLineStr = (char*)sdpLine;
            } 
            else 
            {
                sdpLineStr = utility::StrDup(sdpLine);
                sdpLineStr[nextSDPLine-sdpLine] = '\0';
            }
            if (sdpLineStr != (char*)sdpLine)
            { 
                delete[] sdpLineStr;
            }

            delete[] mediumName;
            delete subsession;

            // Skip the following SDP lines, up until the next "m=":
            while (1) 
            {
                sdpLine = nextSDPLine;
                if (sdpLine == NULL)
                {
                    break; // we've reached the end
                }
                if (!parseSDPLine(sdpLine, nextSDPLine))
                { 
                    return false;
                }

                if (sdpLine[0] == 'm') 
                {
                    break; // we've reached the next subsession
                }
            }
            continue;
        }

        // Insert this subsession at the end of the list:
        if (subsession_tail_ == NULL) 
        {
            subsession_head_ = subsession_tail_ = subsession;
        } 
        else 
        {
            subsession_tail_->setNext(subsession);
            subsession_tail_ = subsession;
        }

        subsession->server_port_num_ = subsession->client_port_num_; // by default

        char const* mStart = sdpLine;
        subsession->saved_spd_lines_ = utility::StrDup(mStart);

        subsession->medium_name_ = utility::StrDup(mediumName);
        delete[] mediumName;
        subsession->protocol_name_ = utility::StrDup(protocolName.c_str());
        subsession->rtp_payload_format_ = payloadFormat;

        // Process the following SDP lines, up until the next "m=":
        while (1) 
        {
            sdpLine = nextSDPLine;
            if (sdpLine == NULL) break; // we've reached the end
            if (!parseSDPLine(sdpLine, nextSDPLine)) return false;

            if (sdpLine[0] == 'm') break; // we've reached the next subsession

            // Check for various special SDP lines that we understand:
            if (subsession->parseSDPLine_c(sdpLine))
            {
                continue;
            }
            if (subsession->parseSDPAttribute_rtpmap(sdpLine))
            {
                continue;
            }
            if (subsession->parseSDPAttribute_control(sdpLine))
            {
                continue;
            }
            if (subsession->parseSDPAttribute_range(sdpLine))
            {
                continue;
            }
            if (subsession->parseSDPAttribute_fmtp(sdpLine)) 
            {
                continue;
            }
            if (subsession->parseSDPAttribute_source_filter(sdpLine)) 
            {
                continue;
            }
            if (subsession->parseSDPAttribute_x_dimensions(sdpLine))
            {
                continue;
            }
            if (subsession->parseSDPAttribute_framerate(sdpLine))
            {
                continue;
            }
            // (Later, check for malformed lines, and other valid SDP lines#####)
        }
        if (sdpLine != NULL)
        {
            subsession->saved_spd_lines_[sdpLine-mStart] = '\0';
        }

        // If we don't yet know the codec name, try looking it up from the
        // list of static payload types:
        if (subsession->codec_name_.empty())
        {
            subsession->codec_name_ = lookupPayloadFormat(subsession->rtp_payload_format_, subsession->rtp_timestamp_frequency_, subsession->num_channels_);
            if (subsession->codec_name_.empty())
            {
                return false;
            }
        }

        // If we don't yet know this subsession's RTP timestamp frequency
        // (because it uses a dynamic payload type and the corresponding
        // SDP "rtpmap" attribute erroneously didn't specify it),
        // then guess it now:
        if (subsession->rtp_timestamp_frequency_ == 0) 
        {
            subsession->rtp_timestamp_frequency_ = guessRTPTimestampFrequency(subsession->medium_name_, subsession->codec_name_);
        }
    }

    return true;
}

bool media_session::parseSDPLine(char const* inputLine, char const*& nextLine)
{
    // Begin by finding the start of the next line (if any):
    nextLine = NULL;
    for (char const* ptr = inputLine; *ptr != '\0'; ++ptr) 
    {
        if (*ptr == '\r' || *ptr == '\n') 
        {
            // We found the end of the line
            ++ptr;
            while (*ptr == '\r' || *ptr == '\n')
            {
                ++ptr;
            }
            nextLine = ptr;
            if (nextLine[0] == '\0')
            {
                nextLine = NULL; // special case for end
            }
            break;
        }
    }

    // Then, check that this line is a SDP line of the form <char>=<etc>
    // (However, we also accept blank lines in the input.)
    if (inputLine[0] == '\r' || inputLine[0] == '\n') 
    {
        return true;
    }
    if (strlen(inputLine) < 2 || inputLine[1] != '='
        || inputLine[0] < 'a' || inputLine[0] > 'z') 
    {
        return false;
    }
    return true;
}

std::string media_session::parseCLine(const std::string& sdpLine)
{
    std::string resultStr;
    char* buffer = utility::StrDupSize(sdpLine.c_str()); // ensures we have enough space
    if (sscanf(sdpLine.c_str(), "c=IN IP4 %[^/ ]", buffer) == 1) 
    {
        // Later, handle the optional /<ttl> and /<numAddresses> #####
        resultStr = buffer;
    }

    delete[] buffer;
    return resultStr;
}

bool media_session::parseSDPLine_s(char const* sdpLine) 
{
    // Check for "s=<session name>" line
    char* buffer = utility::StrDupSize(sdpLine);
    bool parseSuccess = false;

    if (sscanf(sdpLine, "s=%[^\r\n]", buffer) == 1) 
    {
        delete[] session_name_; 
        session_name_ = utility::StrDup(buffer);
        parseSuccess = true;
    }
    delete[] buffer;

    return parseSuccess;
}

bool media_session::parseSDPLine_i(char const* sdpLine) 
{
    // Check for "i=<session description>" line
    char* buffer = utility::StrDupSize(sdpLine);
    bool parseSuccess = false;

    if (sscanf(sdpLine, "i=%[^\r\n]", buffer) == 1) 
    {
        delete[] session_descr_;
        session_descr_ = utility::StrDup(buffer);
        parseSuccess = true;
    }
    delete[] buffer;

    return parseSuccess;
}

bool media_session::parseSDPLine_c(char const* sdpLine) 
{
    // Check for "c=IN IP4 <connection-endpoint>"
    // or "c=IN IP4 <connection-endpoint>/<ttl+numAddresses>"
    // (Later, do something with <ttl+numAddresses> also #####)
    std::string connectionEndpointName = parseCLine(sdpLine);
    if (!connectionEndpointName.empty()) 
    {
        connection_end_point_name_ = connectionEndpointName;
        return true;
    }

    return false;
}

bool media_session::parseSDPAttribute_type(char const* sdpLine)
{
    // Check for a "a=type:broadcast|meeting|moderated|test|H.332|recvonly" line:
    bool parseSuccess = false;

    char* buffer = utility::StrDupSize(sdpLine);
    if (sscanf(sdpLine, "a=type: %[^ ]", buffer) == 1) 
    {
        delete[] media_session_type_;
        media_session_type_ = utility::StrDup(buffer);
        parseSuccess = true;
    }
    delete[] buffer;

    return parseSuccess;
}

bool media_session::parseRangeAttribute(const std::string& sdpLine, double& startTime, double& endTime)
{
    return sscanf(sdpLine.c_str(), "a=range: npt = %lg - %lg", &startTime, &endTime) == 2;
}

bool media_session::parseSDPAttribute_control(char const* sdpLine)
{
    // Check for a "a=control:<control-path>" line:
    bool parseSuccess = false;

    char* controlPath = utility::StrDupSize(sdpLine); // ensures we have enough space
    if (sscanf(sdpLine, "a=control: %s", controlPath) == 1)
    {
        parseSuccess = true;
        delete[] control_path_; 
        control_path_ = utility::StrDup(controlPath);
    }
    delete[] controlPath;

    return parseSuccess;
}

bool media_session::parseSDPAttribute_range(char const* sdpLine) 
{
    // Check for a "a=range:npt=<startTime>-<endTime>" line:
    // (Later handle other kinds of "a=range" attributes also???#####)
    bool parseSuccess = false;

    double playStartTime;
    double playEndTime;
    if (parseRangeAttribute(sdpLine, playStartTime, playEndTime)) 
    {
        parseSuccess = true;
        if (playStartTime > max_play_start_time_) 
        {
            max_play_start_time_ = playStartTime;
        }
        if (playEndTime > max_play_end_time_) 
        {
            max_play_end_time_ = playEndTime;
        }
    }

    return parseSuccess;
}

bool media_session::parseSourceFilterAttribute(const std::string& sdpLine, struct in_addr& sourceAddr) 
{
    // Check for a "a=source-filter:incl IN IP4 <something> <source>" line.
    // Note: At present, we don't check that <something> really matches
    // one of our multicast addresses.  We also don't support more than
    // one <source> #####
    char* sourceName = utility::StrDupSize(sdpLine.c_str()); // ensures we have enough space

    if (sscanf(sdpLine.c_str(), "a=source-filter: incl IN IP4 %*s %s", sourceName) != 1)
    {
        delete[] sourceName;
        return false;
    }

    unsigned int sourceAddress = inet_addr(sourceName);
    // If it wasn't one of those, do gethostbyname
    if (sourceAddress == INADDR_NONE)
    {
        LPHOSTENT lphost;
        lphost = gethostbyname(sourceName);

        if (lphost == NULL) 
        { 
            delete[] sourceName;
            return false;
        }

        sourceAddress = ((LPIN_ADDR)lphost->h_addr)->s_addr;
    }

    sourceAddr.s_addr = sourceAddress;

    return true;
}

bool media_session::parseSDPAttribute_source_filter(char const* sdpLine) 
{
    return parseSourceFilterAttribute(sdpLine, source_filter_addr_);
}

std::string media_session::lookupPayloadFormat(unsigned char rtpPayloadType, unsigned& freq, unsigned& nCh) 
{
    // Look up the codec name and timestamp frequency for known (static)
    // RTP payload formats.
    std::string temp;
    switch (rtpPayloadType) 
    {
    case 0: 
        {
            temp = "PCMU"; 
            freq = 8000; 
            nCh = 1; 
            break;
        }
    case 2: 
        {
            temp = "G726-32"; 
            freq = 8000; 
            nCh = 1; 
            break;
        }
    case 3: 
        {
            temp = "GSM"; 
            freq = 8000; 
            nCh = 1; 
            break;
        }
    case 4: 
        {
            temp = "G723"; 
            freq = 8000; 
            nCh = 1; 
            break;
        }
    case 5: 
        {
            temp = "DVI4"; 
            freq = 8000; 
            nCh = 1; 
            break;
        }
    case 6: 
        {
            temp = "DVI4"; 
            freq = 16000; 
            nCh = 1; 
            break;
        }
    case 7: 
        {
            temp = "LPC"; 
            freq = 8000; 
            nCh = 1; 
            break;
        }
    case 8: 
        {
            temp = "PCMA"; 
            freq = 8000; 
            nCh = 1; 
            break;
        }
    case 9: 
        {
            temp = "G722"; 
            freq = 8000; 
            nCh = 1; 
            break;
        }
    case 10: 
        {
            temp = "L16"; 
            freq = 44100; 
            nCh = 2; 
            break;
        }
    case 11: 
        {
            temp = "L16"; 
            freq = 44100; 
            nCh = 1; 
            break;
        }
    case 12: 
        {
            temp = "QCELP"; 
            freq = 8000; 
            nCh = 1; 
            break;
        }
    case 14:
        {
            temp = "MPA"; 
            freq = 90000; 
            nCh = 1; 
            break;
        }
        // 'number of channels' is actually encoded in the media stream
    case 15: 
        {
            temp = "G728"; 
            freq = 8000; 
            nCh = 1; 
            break;
        }
    case 16: 
        {
            temp = "DVI4"; 
            freq = 11025; 
            nCh = 1; 
            break;
        }
    case 17:
        {
            temp = "DVI4"; 
            freq = 22050; 
            nCh = 1; 
            break;
        }
    case 18: 
        {
            temp = "G729"; 
            freq = 8000; 
            nCh = 1; 
            break;
        }
    case 25:
        {
            temp = "CELB";
            freq = 90000;
            nCh = 1;
            break;
        }
    case 26:
        {
            temp = "JPEG";
            freq = 90000;
            nCh = 1;
            break;
        }
    case 28:
        {
            temp = "NV";
            freq = 90000;
            nCh = 1;
            break;
        }
    case 31:
        {
            temp = "H261";
            freq = 90000;
            nCh = 1;
            break;
        }
    case 32:
        {
            temp = "MPV";
            freq = 90000;
            nCh = 1;
            break;
        }
    case 33:
        {
            temp = "MP2T";
            freq = 90000;
            nCh = 1;
            break;
        }
    case 34:
        {
            temp = "H263";
            freq = 90000;
            nCh = 1;
            break;
        }
    };

    //return utility::StrDup(temp.c_str());
    return temp;
}

unsigned media_session::guessRTPTimestampFrequency(char const* mediumName, const std::string& codecName) 
{
    // By default, we assume that audio sessions use a frequency of 8000,
    // and that video sessions use a frequency of 90000.
    // Begin by checking for known exceptions to this rule
    // (where the frequency is known unambiguously (e.g., not like "DVI4"))
    if (boost::algorithm::iequals(codecName.c_str(), "L16"))
    {
        return 44100;
    }

    if (boost::algorithm::iequals(codecName.c_str(), "MPA") ||
        boost::algorithm::iequals(codecName.c_str(), "MPA-ROBUST") ||
        boost::algorithm::iequals(codecName.c_str(), "X-MP3-DRAFT-00"))
    {
        return 90000;
    }

    // Now, guess default values:
    if (strcmp(mediumName, "video") == 0)
    {
        return 90000;
    }

    return 8000; // for "audio", and any other medium
}

// MediaSubsessionIterator
media_subsession_iterator::media_subsession_iterator(media_session& session) 
: our_session_(session) 
{
    reset();
}

media_subsession_iterator::~media_subsession_iterator() 
{
}

media_subsession* media_subsession_iterator::next() 
{
    media_subsession* result = next_;

    if (next_ != NULL) 
    {
        next_ = next_->next_;
    }

    return result;
}

void media_subsession_iterator::reset() 
{
    next_ = our_session_.subsession_head_;
}

// MediaSubsession
media_subsession::media_subsession(media_session& parent) :
session_id_(NULL), server_port_num_(0),
parent_(parent), next_(NULL),
client_port_num_(0), rtp_payload_format_(0xFF),
saved_spd_lines_(NULL), medium_name_(NULL), protocol_name_(NULL),
rtp_timestamp_frequency_(0), control_path_(NULL),
source_filter_addr_(parent.sourceFilterAddr()),
auxiliary_data_size_length_(0), constant_duration_(0), constant_size_(0),
crc_(0), cts_delta_length_(0), deinterleave_buffer_size_(0), dts_delta_length_(0),
index_delta_length_(0), index_length_(0), interleaving_(0), max_displacement_(0),
object_type_(0), octet_align_(0), profile_level_id_(0), robust_sorting_(0),
size_length_(0), stream_state_indication_(0), stream_type_(0),
cpresent_(false), random_access_indication_(false),
config_(NULL), mode_(NULL), sprop_parameter_sets_(NULL),
play_start_time_(0.0), play_end_time_(0.0),
video_width_(0), video_height_(0), video_fps_(0), num_channels_(1), scale_(1.0f)
{
    rtp_info_.seq_num_ = 0; rtp_info_.time_stamp_ = 0; rtp_info_.info_is_new_ = false;
}

media_subsession::~media_subsession() 
{
    delete[] saved_spd_lines_;
    delete[] medium_name_; 
    delete[] protocol_name_;
    delete[] control_path_; 
    delete[] config_;
    delete[] mode_; 
    delete[] sprop_parameter_sets_;

    delete next_;
}

bool media_subsession::setClientPortNum(unsigned short portNum) 
{
    if (client_port_num_ != 0) 
    {
        return false;
    }

    client_port_num_ = portNum;
    return true;
}

unsigned int media_subsession::connectionEndpointAddress() const 
{
    // Get the endpoint name from with us, or our parent session:
    std::string endpointString = connectionEndpointName();
    if (endpointString.empty()) 
    {
        endpointString = parentSession().connectionEndpointName();
    }
    if (endpointString.empty()) 
    {
        return 0;
    }

    unsigned int sourceAddress = inet_addr(endpointString.c_str());
    // If it wasn't one of those, do gethostbyname
    if (sourceAddress == INADDR_NONE)
    {
        LPHOSTENT lphost;
        lphost = gethostbyname(endpointString.c_str());

        if (lphost == NULL) 
        { 
            return 0;
        }

        sourceAddress = ((LPIN_ADDR)lphost->h_addr)->s_addr;
    }

    return sourceAddress;
}

bool media_subsession::parseSDPLine_c(const std::string& sdpLine) 
{
    // Check for "c=IN IP4 <connection-endpoint>"
    // or "c=IN IP4 <connection-endpoint>/<ttl+numAddresses>"
    // (Later, do something with <ttl+numAddresses> also #####)
    std::string connectionEndpointName = parent_.parseCLine(sdpLine);
    if (!connectionEndpointName.empty())
    {
        connection_endpoint_name_ = connectionEndpointName;
        return true;
    }

    return false;
}

bool media_subsession::parseSDPAttribute_rtpmap(const std::string& sdpLine) 
{
    // Check for a "a=rtpmap:<fmt> <codec>/<freq>" line:
    // (Also check without the "/<freq>"; RealNetworks omits this)
    // Also check for a trailing "/<numChannels>".
    bool parseSuccess = false;
    unsigned rtpmapPayloadFormat;
    char* codecName = utility::StrDupSize(sdpLine.c_str()); // ensures we have enough space
    unsigned rtpTimestampFrequency = 0;
    unsigned numChannels = 1;

    if (sscanf(sdpLine.c_str(), "a=rtpmap: %u %[^/]/%u/%u", &rtpmapPayloadFormat, codecName, &rtpTimestampFrequency, &numChannels) == 4 ||
        sscanf(sdpLine.c_str(), "a=rtpmap: %u %[^/]/%u", &rtpmapPayloadFormat, codecName, &rtpTimestampFrequency) == 3 ||
        sscanf(sdpLine.c_str(), "a=rtpmap: %u %s", &rtpmapPayloadFormat, codecName) == 2)
    {
        parseSuccess = true;
        if (rtpmapPayloadFormat == rtp_payload_format_) 
        {
            //// This "rtpmap" matches our payload format, so set our
            //// codec name and timestamp frequency:
            //// (First, make sure the codec name is upper case)
            //{
            //	setlocale(LC_ALL, "POSIX");
            //	for (char* p = codecName; *p != '\0'; ++p)
            //	{
            //		*p = toupper(*p);
            //	}
            //}
            codec_name_ = codecName;
            boost::algorithm::to_upper(codec_name_);
            rtp_timestamp_frequency_ = rtpTimestampFrequency;
            num_channels_ = numChannels;
        }
    }
    delete [] codecName;

    return parseSuccess;
}

bool media_subsession::parseSDPAttribute_control(const std::string& sdpLine) 
{
    // Check for a "a=control:<control-path>" line:
    bool parseSuccess = false;

    char* controlPath = utility::StrDupSize(sdpLine.c_str()); // ensures we have enough space
    if (sscanf(sdpLine.c_str(), "a=control: %s", controlPath) == 1) 
    {
        parseSuccess = true;
        delete[] control_path_;
        control_path_ = utility::StrDup(controlPath);
    }
    delete[] controlPath;

    return parseSuccess;
}

bool media_subsession::parseSDPAttribute_range(const std::string& sdpLine)
{
    // Check for a "a=range:npt=<startTime>-<endTime>" line:
    // (Later handle other kinds of "a=range" attributes also???#####)
    bool parseSuccess = false;

    double playStartTime;
    double playEndTime;
    if (parent_.parseRangeAttribute(sdpLine, playStartTime, playEndTime)) 
    {
        parseSuccess = true;
        if (playStartTime > play_start_time_) 
        {
            play_start_time_ = playStartTime;
            if (playStartTime > parent_.playStartTime()) 
            {
                parent_.playStartTime() = playStartTime;
            }
        }
        if (playEndTime > play_end_time_) 
        {
            play_end_time_ = playEndTime;
            if (playEndTime > parent_.playEndTime()) 
            {
                parent_.playEndTime() = playEndTime;
            }
        }
    }

    return parseSuccess;
}

bool media_subsession::parseSDPAttribute_fmtp(const std::string& sdpLine_str)
{
    // Check for a "a=fmtp:" line:
    // TEMP: We check only for a handful of expected parameter names #####
    // Later: (i) check that payload format number matches; #####
    // (ii) look for other parameters also (generalize?) #####
    const char* sdpLine = sdpLine_str.c_str();
    do 
    {
        if (strncmp(sdpLine, "a=fmtp:", 7) != 0)
        {
            break;
        }
        sdpLine += 7;
        while (isdigit(*sdpLine))
        {
            ++sdpLine;
        }

        // The remaining "sdpLine" should be a sequence of <name>=<value>;
        // parameter assignments.  Look at each of these.
        // First, convert the line to lower-case, to ease comparison:
        char* const lineCopy = utility::StrDup(sdpLine); 
        char* line = lineCopy;
        {
            setlocale(LC_ALL, "POSIX");
            for (char* c = line; *c != '\0'; ++c)
            {
                *c = tolower(*c);
            }
        }
        while (*line != '\0' && *line != '\r' && *line != '\n') 
        {
            unsigned u;
            char* valueStr = utility::StrDupSize(line);
            if (sscanf(line, " auxiliarydatasizelength = %u", &u) == 1) 
            {
                auxiliary_data_size_length_ = u;
            } 
            else if (sscanf(line, " constantduration = %u", &u) == 1) 
            {
                constant_duration_ = u;
            }
            else if (sscanf(line, " constantsize; = %u", &u) == 1) 
            {
                constant_size_ = u;
            }
            else if (sscanf(line, " crc = %u", &u) == 1) 
            {
                crc_ = u;
            }
            else if (sscanf(line, " ctsdeltalength = %u", &u) == 1) 
            {
                cts_delta_length_ = u;
            }
            else if (sscanf(line, " de-interleavebuffersize = %u", &u) == 1) 
            {
                deinterleave_buffer_size_ = u;
            }
            else if (sscanf(line, " dtsdeltalength = %u", &u) == 1) 
            {
                dts_delta_length_ = u;
            }
            else if (sscanf(line, " indexdeltalength = %u", &u) == 1) 
            {
                index_delta_length_ = u;
            }
            else if (sscanf(line, " indexlength = %u", &u) == 1) 
            {
                index_length_ = u;
            }
            else if (sscanf(line, " interleaving = %u", &u) == 1) 
            {
                interleaving_ = u;
            }
            else if (sscanf(line, " maxdisplacement = %u", &u) == 1) 
            {
                max_displacement_ = u;
            }
            else if (sscanf(line, " objecttype = %u", &u) == 1) 
            {
                object_type_ = u;
            }
            else if (sscanf(line, " octet-align = %u", &u) == 1) 
            {
                octet_align_ = u;
            }
            else if (sscanf(line, " profile-level-id = %x", &u) == 1) 
            {
                // Note that the "profile-level-id" parameter is assumed to be hexadecimal
                profile_level_id_ = u;
            }
            else
                if (sscanf(line, " robust-sorting = %u", &u) == 1) 
                {
                    robust_sorting_ = u;
                }
                else
                    if (sscanf(line, " sizelength = %u", &u) == 1) 
                    {
                        size_length_ = u;
                    }
                    else
                        if (sscanf(line, " streamstateindication = %u", &u) == 1) 
                        {
                            stream_state_indication_ = u;
                        }
                        else
                            if (sscanf(line, " streamtype = %u", &u) == 1) 
                            {
                                stream_type_ = u;
                            }
                            else
                                if (sscanf(line, " cpresent = %u", &u) == 1) 
                                {
                                    cpresent_ = u != 0;
                                }
                                else
                                    if (sscanf(line, " randomaccessindication = %u", &u) == 1) 
                                    {
                                        random_access_indication_ = u != 0;
                                    }
                                    else
                                        if (sscanf(line, " config = %[^; \t\r\n]", valueStr) == 1) 
                                        {
                                            delete[] config_; 
                                            config_ = utility::StrDup(valueStr);
                                        }
                                        else
                                            if (sscanf(line, " mode = %[^; \t\r\n]", valueStr) == 1)
                                            {
                                                delete[] mode_; 
                                                mode_ = utility::StrDup(valueStr);
                                            }
                                            else
                                                if (sscanf(sdpLine, " sprop-parameter-sets = %[^; \t\r\n]", valueStr) == 1) 
                                                {
                                                    // Note: We used "sdpLine" here, because the value is case-sensitive.
                                                    delete[] sprop_parameter_sets_;
                                                    sprop_parameter_sets_ = utility::StrDup(valueStr);
                                                } 
                                                else
                                                {
                                                    // Some of the above parameters are Boolean.  Check whether the parameter
                                                    // names appear alone, without a "= 1" at the end:
                                                    if (sscanf(line, " %[^; \t\r\n]", valueStr) == 1) 
                                                    {
                                                        if (strcmp(valueStr, "octet-align") == 0) 
                                                        {
                                                            octet_align_ = 1;
                                                        } 
                                                        else if (strcmp(valueStr, "cpresent") == 0) 
                                                        {
                                                            cpresent_ = true;
                                                        } 
                                                        else if (strcmp(valueStr, "crc") == 0) 
                                                        {
                                                            crc_ = 1;
                                                        } 
                                                        else if (strcmp(valueStr, "robust-sorting") == 0) 
                                                        {
                                                            robust_sorting_ = 1;
                                                        } 
                                                        else if (strcmp(valueStr, "randomaccessindication") == 0) 
                                                        {
                                                            random_access_indication_ = true;
                                                        }
                                                    }
                                                }
                                                delete[] valueStr;

                                                // Move to the next parameter assignment string:
                                                while (*line != '\0' && *line != '\r' && *line != '\n' && *line != ';') 
                                                {
                                                    ++line;
                                                }

                                                while (*line == ';')
                                                {
                                                    ++line;
                                                }

                                                // Do the same with sdpLine; needed for finding case sensitive values:
                                                while (*sdpLine != '\0' && *sdpLine != '\r' && *sdpLine != '\n' && *sdpLine != ';')
                                                {
                                                    ++sdpLine;
                                                }

                                                while (*sdpLine == ';')
                                                {
                                                    ++sdpLine;
                                                }
        }
        delete[] lineCopy;
        return true;
    } while (0);

    return false;
}

bool media_subsession::parseSDPAttribute_source_filter(const std::string& sdpLine) 
{
    return parent_.parseSourceFilterAttribute(sdpLine, source_filter_addr_);
}

bool media_subsession::parseSDPAttribute_x_dimensions(const std::string& sdpLine)
{
    // Check for a "a=x-dimensions:<width>,<height>" line:
    bool parseSuccess = false;

    int width, height;
    if (sscanf(sdpLine.c_str(), "a=x-dimensions:%d,%d", &width, &height) == 2)
    {
        parseSuccess = true;
        video_width_ = (unsigned short)width;
        video_height_ = (unsigned short)height;
    }

    return parseSuccess;
}

bool media_subsession::parseSDPAttribute_framerate(const std::string& sdpLine) 
{
    // Check for a "a=framerate: <fps>" or "a=x-framerate: <fps>" line:
    bool parseSuccess = false;

    float frate;
    int rate;
    if (sscanf(sdpLine.c_str(), "a=framerate: %f", &frate) == 1 || sscanf(sdpLine.c_str(), "a=framerate:%f", &frate) == 1) 
    {
        parseSuccess = true;
        video_fps_ = (unsigned)frate;
    } 
    else
    if (sscanf(sdpLine.c_str(), "a=x-framerate: %d", &rate) == 1) 
    {
        parseSuccess = true;
        video_fps_ = (unsigned)rate;
    }

    return parseSuccess;
}
