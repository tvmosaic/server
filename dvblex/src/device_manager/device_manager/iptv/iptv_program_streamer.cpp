/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/filesystem.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_ts_aligner.h>
#include <dl_network_helper.h>
#include "iptv_program_streamer.h"
#include "http_reader.h"
#include "udp_reader.h"
#include "hls_reader.h"
#include "rtp_reader.h"
#include "rtsp_reader.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

#define IPTV_CIRCLE_BUFFER_TIMEOUT_MS       50
#define CIRCLE_BUFFER_NUM                   512

iptv_program_streamer_t::iptv_program_streamer_t(iptv_device_props_t* iptv_device_props, 
    const dvblink::channel_id_t& channel_id, const concise_channel_tune_info_t& tune_params) :
        program_streamer_t(channel_id, tune_params.tuning_params_.get()), 
        iptv_device_props_(iptv_device_props),
        url_(tune_params.tuning_params_.get()),
        streaming_started_(false),
        streaming_thread_(NULL),
        circle_buffer_(CIRCLE_BUFFER_NUM, RECV_BUFSIZE, L"iptv_program_streamer_t"),
        aligner_(aligner_callback, this)
{
}

iptv_program_streamer_t::~iptv_program_streamer_t()
{
}

bool iptv_program_streamer_t::start()
{
    program_streamer_t::start();

    log_info(L"iptv_program_streamer_t::start. Starting program streamer for channel %1%") % string_cast<EC_UTF8>(channel_id_.get());

    //Start streaming thread
    streaming_started_ = true;
    streaming_thread_ = new boost::thread(boost::bind(&iptv_program_streamer_t::streaming_thread, this));

    return true;
}

void iptv_program_streamer_t::stop()
{
    log_info(L"iptv_program_streamer_t::stop. Stoping program streamer for channel %1%") % string_cast<EC_UTF8>(channel_id_.get());

    if (streaming_started_)
    {	
        // Stop streaming thread if started
        streaming_started_ = false;
        streaming_thread_->join();
        delete streaming_thread_;
        streaming_thread_ = NULL;
    }

    program_streamer_t::stop();
}

iptv_reader_type_e iptv_program_streamer_t::get_reader_type_from_url(const std::string& url)
{
    iptv_reader_type_e retval = iptv_rte_unknown;

    std::string address;
	std::string user;
	std::string pswd;
    unsigned short port;
	std::string url_suffix;
    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(url.c_str(), address, user, pswd, port, url_suffix);

    switch (proto)
    {
    case DL_NET_PROTO_UDP:
        retval = iptv_rte_udp;
        break;
    case DL_NET_PROTO_RTP:
        retval = iptv_rte_rtp;
        break;
    case DL_NET_PROTO_HTTP:
    case DL_NET_PROTO_HTTPS:
        {
            if (boost::icontains(url, ".m3u8"))
            {
                retval = iptv_rte_hls;
            } else
            {
                retval = iptv_rte_http;
            }
        }
        break;
    case DL_NET_PROTO_RTSP:
            retval = iptv_rte_rtsp;
        break;
    case DL_NET_PROTO_RTMP:
            retval = iptv_rte_rtmp;
        break;
    default:
        break;
    }
    return retval;
}

net_reader* iptv_program_streamer_t::get_stream_reader(const std::string& url, iptv_reader_type_e reader_type)
{
    net_reader* retval = NULL;

    iptv_reader_type_e type = reader_type;
    if (type == iptv_rte_unknown)
        type = get_reader_type_from_url(url);

    switch (type)
    {
    case iptv_rte_udp:
        log_info(L"iptv_program_streamer_t::get_stream_reader: iptv_rte_udp");
        retval = new udp_reader(&circle_buffer_);
        break;
    case iptv_rte_rtp:
        log_info(L"iptv_program_streamer_t::get_stream_reader: iptv_rte_rtp");
        retval = new rtp_reader(NULL, &circle_buffer_);
        break;
    case iptv_rte_http:
        log_info(L"iptv_program_streamer_t::get_stream_reader: iptv_rte_http");
        retval = new http_reader(&circle_buffer_, "");
        break;
    case iptv_rte_hls:
        log_info(L"iptv_program_streamer_t::get_stream_reader: iptv_rte_hls");
        retval = new hls_reader(&circle_buffer_, "");
        break;
    case iptv_rte_rtsp:
            log_info(L"iptv_program_streamer_t::get_stream_reader: iptv_rte_rtsp");
            retval = new rtsp_reader(&circle_buffer_, 0);
        break;
    case iptv_rte_rtmp:
            log_info(L"iptv_program_streamer_t::get_stream_reader: iptv_rte_rtmp");
            log_error(L"iptv_program_streamer_t::get_stream_reader: RTMP protocol is not supported");
        break;
    default:
            log_error(L"iptv_program_streamer_t::get_stream_reader: Unknown reader for url %1%") % string_cast<EC_UTF8>(url);
        break;
    }
    return retval;
}

void iptv_program_streamer_t::streaming_thread()
{
    log_info(L"iptv_program_streamer_t::streaming_thread: Streaming thread started. Channel id %1%") % channel_id_.to_wstring();

    int restart_count = 0;
    iptv_reader_type_e reader_type = iptv_rte_unknown;
    while (streaming_started_)
    {
        log_info(L"iptv_program_streamer_t::streaming_thread. Starting stream. Type: %1%, restart count: %2%") % reader_type % restart_count;

        long ms_timeout = 50;
        iptv_reader_type_e prev_reader_type = reader_type;
        if (!iptv_streamer_function(reader_type, ms_timeout, restart_count))
            break;

        log_info(L"iptv_program_streamer_t::streaming_thread. Stream restart is requested after %1% ms") % ms_timeout;

        //wait before resuming reception
        boost::posix_time::ptime start_wait = boost::posix_time::microsec_clock::universal_time();
        const boost::posix_time::time_duration to = boost::posix_time::milliseconds(ms_timeout);

        while (streaming_started_)
        {
            boost::posix_time::ptime now = boost::posix_time::microsec_clock::universal_time();

            if (((now - start_wait) >= to))
                break;

            boost::this_thread::sleep(boost::posix_time::milliseconds(20));
        }

        //break if exit was signalled while waiting
        if (!streaming_started_)
            break;

        //clear circle buffer
        circle_buffer_.reset();
    }

    log_info(L"iptv_program_streamer_t::streaming_thread: Streaming thread finished (channel %1%)") % channel_id_.to_wstring();
}

bool iptv_program_streamer_t::iptv_streamer_function(iptv_reader_type_e& reader_type, long& ms_timeout, int& restart_count)
{
    bool ret_val = false;

    // Create reader
    boost::shared_ptr<net_reader> stream_reader = boost::shared_ptr<net_reader>(get_stream_reader(url_, reader_type));
    
    if (stream_reader == NULL)
    {
        log_error(L"iptv_program_streamer_t::iptv_streamer_function: stream_reader is NULL");
        return false;
    }

    std::string local_ip = iptv_device_props_->get_local_ip();
    log_info(L"iptv_program_streamer_t::iptv_streamer_function: using local IP address %1%") % string_cast<EC_UTF8>(local_ip);

    if (!stream_reader->create(url_.c_str(), local_ip.c_str()))
    {
        log_error(L"iptv_program_streamer_t::iptv_streamer_function. Cannot create stream_reader");
        return false;
    }

    if (!stream_reader->start(restart_count))
    {
        log_error(L"iptv_program_streamer_t::iptv_streamer_function: stream_reader start failed");
        return false;
    }

    namespace pt = boost::posix_time;
    log_info(L"iptv_program_streamer_t::iptv_streamer_function: Pre-buffering started for channel %1%") % channel_id_.to_wstring();

    std::vector<std::vector<unsigned char> > prebuffer_list;
    bool bPrebuffering = true;
    pt::ptime start_tick = pt::microsec_clock::universal_time();
    pt::ptime last_log_write = start_tick;
    pt::ptime last_node_read = boost::date_time::not_a_date_time;

    // Read buffer by buffer now
    while (streaming_started_)
    {
        //check if reader requests connection restart
        iptv_reader_type_e new_reader_type;
        if (stream_reader->restart_required(new_reader_type, ms_timeout, restart_count))
        {
            //change reader type
            if (new_reader_type != iptv_rte_unknown)
                reader_type = new_reader_type;

            ret_val = true;

            log_info(L"iptv_program_streamer_t::iptv_streamer_function. Reader requested connection restart (new type %1%, delay %2%)") % reader_type % ms_timeout;

            break;
        }

        // Read data in a loop
        ts_circle_buffer::node_t node = circle_buffer_.tear_node(IPTV_CIRCLE_BUFFER_TIMEOUT_MS);
        
        if (!node)
        {
            pt::ptime now = pt::microsec_clock::universal_time();

            if (last_node_read.is_special())
                last_node_read = now;

            const pt::time_duration to = pt::milliseconds(2000);

            if (((now - last_node_read) >= to) &&
                ((now - last_log_write) >= to))
            {
                log_info(L"iptv_program_streamer_t::iptv_streamer_function: Waiting for data (channel %1%)") % channel_id_.to_wstring();
                last_log_write = now;
            }
        } else
        {
            last_node_read = boost::date_time::not_a_date_time;
            int buf_len = node->size_to_read();
            const unsigned char* buf_ptr = node->data();

            if (bPrebuffering)
            {
                pt::ptime cur_tick = pt::microsec_clock::universal_time();

                boost::uint32_t pre_buffering_time = iptv_device_props_->get_prebuffering_time_ms();

                if (static_cast<unsigned long>((cur_tick - start_tick).total_milliseconds()) > pre_buffering_time)
                {
                    log_info(L"iptv_program_streamer_t::iptv_streamer_function: Pre-buffering finished (channel %1%)") % channel_id_.to_wstring();

                    // Release pre-buffered content
                    for (unsigned int i = 0; i < prebuffer_list.size(); i++)
                    {
                        process_stream(prebuffer_list.at(i).size(),
                            &prebuffer_list.at(i)[0]);
                    }

                    prebuffer_list.clear();

                    // Send data to parent
                    process_stream(buf_len, (unsigned char*)buf_ptr);

                    // Turn pre-buffering off
                    bPrebuffering = false;
                } 
                else
                {
                    // Buffer this content 
                    prebuffer_list.push_back(std::vector<unsigned char>());
                    prebuffer_list[prebuffer_list.size() - 1].assign(
                        (unsigned char*)buf_ptr, (unsigned char*)buf_ptr + buf_len);
                }
            } 
            else
            {
                // Send data to parent
                process_stream(buf_len, buf_ptr);
            }

            circle_buffer_.put_node(node);
        }
    }

    // Release pre-buffered content (if any)
    prebuffer_list.clear();

    stream_reader->stop();

    return ret_val;
}


void iptv_program_streamer_t::process_stream(size_t len, const unsigned char *data)
{
    aligner_.write_stream((unsigned char*)data, len);
}

void __stdcall iptv_program_streamer_t::aligner_callback(const unsigned char* buf, unsigned long len, void* user_param)
{
    iptv_program_streamer_t* parent = (iptv_program_streamer_t*)user_param;

    parent->write_stream(buf, len);;
}

bool iptv_program_streamer_t::get_signal_info(signal_info_t& si)
{
    bool ret_val = false;

    si.reset();

    //always report good signal?..
    si.lock_ = true;
    si.signal_quality_ = 100;
    si.signal_strength_ = 100;

    ret_val = true;

    return ret_val;
}

