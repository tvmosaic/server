/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <cstdio>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
#include <dl_strings.h>
#include <dl_common.h>
#include <dl_logger.h>
#include <dl_http_comm.curl.h>
#include <dl_pugixml_helper.h>
#include "playlist_reader.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

namespace dvblex {

const std::string UNKNOWN_CHANNEL_NAME        = "Unknown";
const int INVALID_CHANNEL_NUMBER               = 0;
const char CHANNEL_NAME_DELIMITER           = ',';
const char CHANNEL_NUMBER_DELIMITER         = ':';

playlist_reader_result_e playlist_reader::get_channels(playlist_channel_map_t& channel_map) const
{
    playlist_reader_result_e result = prr_not_found;    
    channel_map.clear();

    if (get_playlist_content(channel_map))
        result = channel_map.empty() ? prr_no_channels : prr_ok;

	return result;
}

void playlist_reader::convert_content_to_channel_map(const std::vector<std::string>& content, playlist_channel_map_t& channel_map) const
{
    playlist_channel channel;
    bool bvalid = false;

    std::vector<std::string>::const_iterator it = content.begin();

    while (it != content.end())
    {
        const std::string& s = *it;

        //skip header and vlc options
        if (strstr(s.c_str(), m3u_playlist_header.c_str()) == NULL &&
            strstr(s.c_str(), m3u_playlist_extvlc_option.c_str()) == NULL)
        {
            if (strstr(s.c_str(), m3u_playlist_inf_section.c_str()) != NULL)
            {
                bvalid = parse_channel_info(s, channel);
            } else
            {
                if (parse_channel_params(s, channel))
                {
                    if (bvalid)
                    {
                        //channel_map[channel_param.get_channel_id_string()] = channel_param;
                        std::string key = channel.get_channel_id();

                        if (!key.empty())
                        {
            	            if (channel_map.find(key) == channel_map.end())
            		            channel_map.insert(std::make_pair(key, channel));
                            else 
                                log_warning(L"playlist_reader::convert_content_to_channel_map. Duplicate channel IDs are produced for %1% and %2%") % string_cast<EC_UTF8>(channel_map[key].name) % string_cast<EC_UTF8>(channel.name);
                        }
                        bvalid = false;
                    }
                }
            }
        }

        ++it;
    }
}

bool playlist_reader::get_playlist_content(playlist_channel_map_t& channel_map) const
{
    bool success = false;

    std::string address;
	std::string user;
	std::string pswd;
    unsigned short port;
	std::string url_suffix;

    if (DL_NET_PROTO_UNKNOWN != network_helper::parse_net_url(playlist_path_.c_str(),
        address, user, pswd, port, url_suffix))
    {
        success = get_remote_playlist(channel_map);
    }
    else
    {
        success = get_local_playlist(channel_map);
    }

    return success;
}

bool playlist_reader::get_local_playlist(playlist_channel_map_t& channel_map) const
{
    static const int max_line_size = 2048;

    bool success = false;
    std::vector<std::string> content;
    FILE* playlist_file = NULL;

    dvblink::filesystem_path_t playlist(playlist_path_);

#ifdef _WIN32
    if (NULL == _wfopen_s(&playlist_file, playlist.to_wstring().c_str(), L"r+t, ccs=UTF-8"))
    {
        wchar_t file_line[max_line_size];
        while (fgetws(file_line, max_line_size-1, playlist_file) != NULL)
        {
            std::wstring wstr = boost::algorithm::trim_copy_if(std::wstring(file_line),
                boost::algorithm::is_any_of(L"\r\n"));

            content.push_back(string_cast<EC_UTF8>(wstr));
        }
        fclose(playlist_file);

        success = true;
    }
#else
    playlist_file = fopen(playlist.to_string().c_str(), "r+");

    if (NULL != playlist_file)
    {
        char file_line[max_line_size];
        while (fgets(file_line, max_line_size-1, playlist_file) != NULL)
        {
            content.push_back(boost::algorithm::trim_copy_if(std::string(file_line), boost::algorithm::is_any_of("\r\n")));
        }       
        fclose(playlist_file);
        
        success = true;
    }
#endif
    
    if (success)
        convert_content_to_channel_map(content, channel_map);

    return success;
}

bool playlist_reader::get_remote_playlist(playlist_channel_map_t& channel_map) const
{
    bool success = false;

    std::string address;
	std::string user;
	std::string pswd;
    unsigned short port;
	std::string url_suffix;
    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(playlist_path_.c_str(),
        address, user, pswd, port, url_suffix);

    if (DL_NET_PROTO_HTTP == proto || DL_NET_PROTO_HTTPS == proto)
    {
        http_comm_handler http_comm(PRODUCT_NAME_UTF8, address.c_str(), user.c_str(), pswd.c_str(), port);
        
        if (http_comm.Init())
        {
            std::string response;

            if (http_comm.ExecuteGetWithResponseGZ(url_suffix.c_str(),
                proto == DL_NET_PROTO_HTTPS, response))
            {
        	    success = true;

                if (!parse_hdhomerun_channels(response, channel_map))
                {
            	    try
            	    {
                        std::string content_str;

                        //check whether this is a unicode string
				        if (response.size() > 2 &&
                            (unsigned char)response[0] == 0xFF &&
                            (unsigned char)response[1] == 0xFE)
				        {
                            std::wstring wstr;
					        wstr.assign((wchar_t*)(&response[2]), (response.size() - 2) / 2);
                            content_str = string_cast<EC_UTF8>(wstr);
				        }
				        else
				        {
					        content_str = response;
				        }

                        std::vector<std::string> content;
				        boost::erase_all(content_str, "\r");
                        boost::split(content, content_str, boost::is_any_of("\n"));

                        convert_content_to_channel_map(content, channel_map);
            	    }
            	    catch (...)
            	    {
            	    }
                }
            }

            http_comm.Term();
        }
    }

    return success;
}

static void split_string_in_args(std::vector<std::string>& qargs, const std::string& str, char sep)
{
    char quote_symbol = '"';
    char escape_symbol = '\\';
    if (str.size() > 0)
    {
        bool in_quotes = false;
        size_t start_idx = 0;
        for (size_t i=0; i<str.size(); i++)
        {
            if (str[i] == sep)
            {
                if (!in_quotes)
                {
                    //add token
                    std::string tok = str.substr(start_idx, i - start_idx);
                    if (tok.size() > 0)
                        qargs.push_back(tok);

                    start_idx = i;
                }
            } else
            if (str[i] == quote_symbol)
                in_quotes = !in_quotes;
            else
            if (str[i] == escape_symbol)
            {
                //check if we are in quotes and this is escaped quote
                if (in_quotes)
                {
                    if (i < str.size() - 1 && str[i+1] == quote_symbol)
                    {
                        //skip both symbols
                        i++;
                    }
                }
            }
        }
        std::string tok = str.substr(start_idx + 1, str.size() - 1 - start_idx);
        if (tok.size() > 0)
            qargs.push_back(tok);
    }
}

bool playlist_reader::parse_channel_info(const std::string& param_str, playlist_channel& channel) const
{
	bool success = true;
    
    channel.reset();

    //first split string on comma: the first one is duration plus params, the second - channel name
    std::vector<std::string> params_name_list;
    split_string_in_args(params_name_list, param_str, ',');
    if (params_name_list.size() > 1)
    {
        //both
        channel.name = boost::algorithm::trim_copy_if(params_name_list[1], boost::algorithm::is_any_of(" "));
        channel.name = boost::algorithm::trim_copy_if(channel.name, boost::algorithm::is_any_of(","));

        std::string params_line = params_name_list[0];

        std::vector<std::string> params_list;
        split_string_in_args(params_list, params_line, ' ');
        for (size_t i=0; i<params_list.size(); i++)
        {
            //find =
            size_t pos = params_list[i].find('=');
            if (pos != std::string::npos)
            {
                //tag
                std::string tag = params_list[i].substr(0, pos);
                //remove spaces
                boost::algorithm::trim_if(tag, boost::algorithm::is_any_of(" "));
                //make it lower case
                tag = boost::to_lower_copy(tag);
                //value
                std::string value = params_list[i].substr(pos + 1, params_list[i].size() - pos - 1);
                //remove spaces
                boost::algorithm::trim_if(value, boost::algorithm::is_any_of(" "));
                //remove start/end quotes
                boost::algorithm::trim_if(value, boost::algorithm::is_any_of("\""));
                //unescape quotes
                boost::algorithm::replace_all(value, "\\\"", "\"");

                if (tag.size() > 0 && value.size() > 0)
                    channel.tags[tag] = value;
            }
        }

        //set channel number(s) from tag if it is present
        if (channel.tags.find(m3u_plus_tvg_chno) != channel.tags.end())
            channel.set_channel_number_from_str(channel.tags[m3u_plus_tvg_chno]);

    } else
    if (params_name_list.size() == 1)
    {
        //name only (?)
        channel.name = boost::algorithm::trim_copy_if(params_name_list[0], boost::algorithm::is_any_of(" "));
    } else
    {
        //error (empty string?)
        success = false;
    }

	return success;
}

void playlist_reader::process_exttv_line(const std::string& line, std::string& group, std::string& xmltv_id, std::string& logo_url) const
{
    std::string info_line = line.substr(m3u_playlist_exttv_header.size() + 1);

    typedef boost::tokenizer<boost::char_separator<char>, std::string::const_iterator, std::string> tokenizer_t;
    boost::char_separator<char> sep(";", 0, boost::keep_empty_tokens);
    tokenizer_t tokens(info_line, sep);

    int count = 0;
    for (tokenizer_t::iterator tok_iter = tokens.begin(); tok_iter != tokens.end(); ++tok_iter)
    {
        switch (count)
        {
        case 0:
            group = *tok_iter;
            boost::algorithm::replace_all(group, ",", " / ");
            break;
        case 2:
            xmltv_id = *tok_iter;
            break;
        case 3:
            logo_url = *tok_iter;
            break;
        default:
            break;
        }

        ++count;
    }

}

bool playlist_reader::parse_channel_params(const std::string& param_str, playlist_channel& channel_param) const
{   
    bool ret_val = false;
    std::string str_url = boost::algorithm::trim_copy_if(param_str, boost::algorithm::is_any_of(" "));

    //check if this is a channel group line
    if (boost::istarts_with(str_url, m3u_playlist_extgrp_header))
    {
        std::string group_name = str_url.substr(m3u_playlist_extgrp_header.size() + 1);
        boost::algorithm::trim(group_name);
        if (group_name.size() > 0)
            channel_param.tags[m3u_plus_group_title] = group_name;
        ret_val = false;
    } else
    //else if this is #exttv info line
    if (boost::istarts_with(str_url, m3u_playlist_exttv_header))
    {
        std::string group;
        std::string xmltv_id;
        std::string logo_url;
        process_exttv_line(str_url, group, xmltv_id, logo_url);

        if (!group.empty())
            channel_param.tags[m3u_plus_group_title] = group;
        if (!xmltv_id.empty())
            channel_param.tags[m3u_plus_tvg_id] = xmltv_id;
        if (!logo_url.empty())
            channel_param.tags[m3u_plus_tvg_logo] = logo_url;

        ret_val = false;
    } else
    {
        channel_param.url = str_url;

        //check if this is VOD url
       if (boost::iends_with(str_url, ".mp4") || boost::iends_with(str_url, ".mkv")  || boost::iends_with(str_url, ".avi") || boost::iends_with(str_url, ".divx"))
            channel_param.is_vod = true;

        ret_val = !channel_param.url.empty();
    }

    return ret_val;
}

bool playlist_reader::parse_hdhomerun_channels(const std::string& response, playlist_channel_map_t& channel_map) const
{
    bool ret_val = false;

    std::string str;

    pugi::xml_document doc;
    if (doc.load_buffer(response.c_str(), response.length()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL && boost::iequals(root_node.name(), "Lineup"))
        {
            pugi::xml_node channel_node = root_node.first_child();
            while (channel_node != NULL)
            {
                if (boost::iequals(channel_node.name(), "Program"))
                {
                    int drm_flag = 0;
		            if (pugixml_helpers::get_node_value(channel_node, "drm", str))
                        string_conv::apply(str.c_str(), drm_flag, 0);
                    
                    //import only unprotected channels
                    if (drm_flag == 0)
                    {
                        playlist_channel channel;

                        pugixml_helpers::get_node_value(channel_node, "GuideName", channel.name);
                        
		                if (pugixml_helpers::get_node_value(channel_node, "URL", str))
                            parse_channel_params(str, channel);

		                if (pugixml_helpers::get_node_value(channel_node, "GuideNumber", str))
                            channel.set_channel_number_from_str(str);

                        std::string key = channel.get_channel_id();

                        if (!key.empty())
    		                channel_map.insert(std::make_pair(key, channel));
                    }
                }

                channel_node = channel_node.next_sibling();
            }
        }
    }

    return ret_val;
}

} //dvblex
