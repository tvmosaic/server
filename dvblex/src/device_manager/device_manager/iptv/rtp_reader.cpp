/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <dl_logger.h>
#include <dl_base64.h>
#include <dl_ts_info.h>
#include "rtp_reader.h"
#include "rtp.h"
#include "utility.h"
#include "media_session.h"
#include "iptvh264.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

static bool initialize_rng() // called once
{
    srand((unsigned int)utility::GetTimeNow());
    return true;
}

static bool rng_initialized = initialize_rng();

static bool bin_to_hex(const void* bin, size_t bin_size, std::string& hex, 
    size_t block_octets = 0, char separator = ' ')
{
    typedef unsigned char byte_t;
    const char hex_digits[] = "0123456789ABCDEF";
    bool result = false;

    if (bin && (bin_size > 0))
    {
        size_t hex_length = bin_size * 2;

        if ((block_octets > 0) && (bin_size > block_octets))
        {
            hex_length += ((bin_size / block_octets) - 1);

            if ((bin_size % block_octets) != 0)
            {
                hex_length++;
            }
        }

        hex.resize(hex_length);

        const byte_t* pb = static_cast<const byte_t*>(bin);
        char* ph = &hex[0];
        size_t oct_count = 0;

        for (size_t i = 0; i < bin_size; i++)
        {
            if ((block_octets > 0) && (oct_count == block_octets))
            {
                *ph++ = separator;
                oct_count = 0;
            }

            byte_t b = *pb++;
            *ph++ = hex_digits[(b >> 4) & 0x0F];
            *ph++ = hex_digits[b & 0x0F];

            oct_count++;
        }

        result = true;
    }

    return result;
}

rtp_reader::rtp_reader(media_subsession* subsession, ts_circle_buffer* input_stream) :
    udp_reader(input_stream),
    video_cont_counter_(0),
    pat_cont_counter_(0),
    pmt_cont_counter_(0),
    sub_session_(subsession),
    aligner_(aligner_callback, this),
    iptv_spts_converter_(iptv_spts_converter_callback, this)
{
    //srand((unsigned int)utility::GetTimeNow());
    ssrc_ = utility::Random32();
    log_info(L"rtp_reader::rtp_reader. SSRC=%1%") % ssrc_;

    reception_stats_db_ = new rtp_reception_stats_db();
    packet_gen_ = new ts_packet_generator();

    //process subsession parameters
    ProcessSessionParams();

    iptv_spts_converter_.init();
}

rtp_reader::~rtp_reader()
{
    iptv_spts_converter_.term();

    delete reception_stats_db_;
    delete packet_gen_;
}

void rtp_reader::stream_receive_function()
{
    log_info(L"rtp_reader::stream_receive_function() started");

    std::string recv_buf(RECV_BUFSIZE, '\0');
    unsigned char* buf_ptr = (unsigned char*)&recv_buf[0];
    size_t buf_size = recv_buf.size();

    // reset circular buffers
    input_buffer_->reset();

    namespace pt = boost::posix_time;
    const timeout_t wait_timeout = pt::milliseconds(100);

    while (receive_started_ && sock_ptr_)
    {
        errcode_t err = sock_ptr_->wait_for_readable(wait_timeout);

        if (err != err_none)
        {
            continue;
        }

        size_t bytes_received = 0;
        dvblink::sock_addr from;

        err = sock_ptr_->receive_datagram(buf_ptr, buf_size,
            bytes_received, from, false);

        if (err != err_none)
        {
            log_info(L"rtp_reader::stream_receive_function() finished");
            receive_started_ = false;
            break;
        }

        unsigned char* payload = NULL;
        int payload_len = GetRTPBuffer(buf_ptr, bytes_received, &payload);

        if (payload_len == 0)
        {
            log_warning(L"rtp_reader::stream_receive_function. RTP protocol not recognized. Reset data buffer");

            try
            {
                std::string dump;
                bin_to_hex(buf_ptr, sizeof(RTPHEADER), dump);
                std::wstring wdump = string_cast<EC_UTF8>(dump);
                log_info(L"rtp_reader::stream_receive_function: hex=%1%") % wdump;
            }
            catch (...)
            {
            }
        }
        else if (payload != NULL)
        {
            ProcessPayload(payload, payload_len);
            //log_info(L"====== payload_len: %1%") % payload_len;
        }
    }

    sock_ptr_.reset(); // close socket
}

#if !defined(_SYNOLOGY_PPC)
int rtp_reader::GetRTPBuffer(unsigned char* buffer, int bufLen, unsigned char** payload)
{
    int rtpHeaderSize = sizeof(RTPHEADER);
    int numPadBytes = 0;
    RTPHEADER* rtpHeader = (RTPHEADER*)buffer;

    if (rtpHeader->m_Version == RTP_VERSION)
    {
        rtp_timestamp_ = ntohl(rtpHeader->m_Timestamp);
        unsigned int SSRC = ntohl(rtpHeader->m_SSRC);
        unsigned short rtpSeqNo = ntohs(rtpHeader->m_SeqNumber);

        // Get CSRC length
        if (rtpHeader->m_CSRCCount > 0)
        {
            rtpHeaderSize += (rtpHeader->m_CSRCCount - 1) * sizeof(boost::uint32_t);
        }
        else
        {
            rtpHeaderSize -= sizeof(boost::uint32_t);
        }

        // Adjust payload length to take padding into account
        if (rtpHeader->m_Padding == 1)  
        {
            // Last byte contains number of padding bytes
            numPadBytes = (int)buffer[bufLen - 1];
            if (numPadBytes <= 0)
            {
                numPadBytes = 0;
            }
        }

        // Get header extension length
        if (rtpHeader->m_Extension == 1)
        {
            RTPEXT* rtpExt;
            rtpExt = (RTPEXT*)(buffer + rtpHeaderSize);
            unsigned short length = ntohs(rtpExt->length);
            rtpHeaderSize += sizeof(RTPEXT) + length * sizeof(boost::uint32_t);
        }

        // Get payload data and payload length
        *payload = buffer + rtpHeaderSize;
        bufLen = bufLen - rtpHeaderSize - numPadBytes;

        timeval presentationTime; // computed by
        bool hasBeenSyncedUsingRTCP; // computed by
        reception_stats_db_->NoteIncomingPacket(SSRC, rtpSeqNo, rtp_timestamp_,
            rtp_timestamp_freq_, true, presentationTime, hasBeenSyncedUsingRTCP, bufLen);
    }
    else
    {
        bufLen = 0;
    }

    return (bufLen < 0) ? 0 : bufLen;
}
#else
// PPC
//
int rtp_reader::GetRTPBuffer(unsigned char* buffer, int bufLen, unsigned char** payload)
{
    int rtpHeaderSize = sizeof(RTPHEADER);
    int numPadBytes = 0;

    boost::uint32_t h0 = *((boost::uint32_t*)buffer);
    unsigned int rtp_version = (h0 & 0xC0000000) >> 30;

    if (rtp_version == RTP_VERSION)
    {
        bool has_padding = ((h0 & 0x40000000) != 0);
        bool has_extension = ((h0 & 0x20000000) != 0);
        unsigned int csrc_count = (h0 & 0x0F000000) >> 24;
        bool has_marker = ((h0 & 0x00800000) != 0);
        unsigned int payload_type = (h0 & 0x007F0000) >> 16;
        unsigned int sequence_number = (h0 & 0x0000FFFF);

        rtp_timestamp_ = *((boost::uint32_t*)buffer + 1);
        unsigned int SSRC = *((boost::uint32_t*)buffer + 2);
        unsigned short rtpSeqNo = (unsigned short)sequence_number;

        // Get CSRC length
        if (csrc_count > 0)
        {
            rtpHeaderSize += (csrc_count - 1) * sizeof(boost::uint32_t);
        }
        else
        {
            rtpHeaderSize -= sizeof(boost::uint32_t);
        }

        // Adjust payload length to take padding into account
        if (has_padding)  
        {
            // Last byte contains number of padding bytes
            numPadBytes = (int)buffer[bufLen - 1];
            
            if (numPadBytes <= 0)
            {
                numPadBytes = 0;
            }
        }

        // Get header extension length
        if (has_extension)
        {
            boost::uint32_t eh = *((boost::uint32_t*)(buffer + rtpHeaderSize));
            unsigned short length = (unsigned short)(eh & 0x0000FFFF);
            rtpHeaderSize += sizeof(RTPEXT) + length * sizeof(boost::uint32_t);
        }

        // Get payload data and payload length
        *payload = buffer + rtpHeaderSize;
        bufLen = bufLen - rtpHeaderSize - numPadBytes;

        timeval presentationTime; // computed by
        bool hasBeenSyncedUsingRTCP; // computed by

        reception_stats_db_->NoteIncomingPacket(SSRC, rtpSeqNo, rtp_timestamp_,
            rtp_timestamp_freq_, true, presentationTime, hasBeenSyncedUsingRTCP, bufLen);
    }
    else
    {
        bufLen = 0;
    }

    return (bufLen < 0) ? 0 : bufLen;
}
#endif

void rtp_reader::ProcessSessionParams()
{
    payload_type_ = IRPT_MPEG2_TS; //default
    if (sub_session_ != NULL)
    {
        pid_ = 100; //some arbitrary number

        rtp_timestamp_freq_ = sub_session_->rtpTimestampFrequency();
        if (boost::algorithm::iequals(sub_session_->codecName().c_str(), "h264"))
        {
            payload_type_ = IRPT_H264;
        }

        //parse property-sets
        if (sub_session_->fmtp_spropparametersets() != NULL)
        {
            char* param_set = strdup(sub_session_->fmtp_spropparametersets());

            char* token = strtok(param_set, ",");
            while (token != NULL)
            {
                std::string res;
                base64_decode<std::string, char>(token, res);
                prop_sets_.push_back(res);
                token = strtok(NULL, ",");
            }

            free(param_set);
        }
    }
    else
    {
        rtp_timestamp_freq_ = 0;
    }
}

void rtp_reader::ProcessPayload(unsigned char* payload, int len)
{
    switch (payload_type_)
    {
    case IRPT_H264:
        {
            int nal_len;
            unsigned char* nal_ptr = ProcessH264Payload(payload, len, nal_len);
            if (nal_ptr != NULL)
                ProcessNAL(nal_ptr, nal_len);
        }
        break;
    default:
        ProcessNAL(payload, len);
        break;    
    }
}

void rtp_reader::ProcessNAL(unsigned char* payload, int len)
{
    switch (payload_type_)
    {
    case IRPT_H264:
        ProcessH264NAL(payload, len);
        break;
    default:
        ProcessStream(payload, len);
        break;    
    }
}

#if !defined(_SYNOLOGY_PPC)
void rtp_reader::ProcessH264NAL(unsigned char* data, int len)
{
    H264_PAYLOAD_HEADER* header = (H264_PAYLOAD_HEADER*)data;
    std::vector<unsigned char> pes;

    unsigned char buffer[4]; //00 00 00 01: NALU header
    memset(buffer, 0, sizeof(buffer));
    buffer[3] = 1;

    switch (header->nal_unit_type)
    {
    case 1: //Coded slice of a non-IDR picture
        pes.insert(pes.end(), buffer, buffer + sizeof(buffer));
        break;
    case 5: //Coded slice of an IDR picture
        {
            for (unsigned int i=0; i<prop_sets_.size(); i++)
            {
                pes.insert(pes.end(), buffer, buffer + sizeof(buffer));
                pes.insert(pes.end(), prop_sets_[i].begin(), prop_sets_[i].end());
            }
            pes.insert(pes.end(), buffer, buffer + sizeof(buffer));
        }
        break;
    default:
        break;
    }

    if (header->nal_unit_type == 5) //create stream description packets with the same rate as key-frames
    {
        CreateStreamDescriptionPackets();
    }

    if (pes.size() > 0)
    {
        pes.insert(pes.end(), data, data + len);
        //form pes header: 00 00 01 E0 xx xx 85 80 00 pts data
        std::vector<unsigned char> pes_header;
        pes_header.push_back(00);
        pes_header.push_back(00);
        pes_header.push_back(01);
        pes_header.push_back(0xE0); //stream type
        pes_header.push_back(00); //length placeholder
        pes_header.push_back(00); //length placeholder
        pes_header.push_back(0x85);
        pes_header.push_back(0x80);
        pes_header.push_back(05); //PTS size
        //PTS
        unsigned char ch;
        ch = ((rtp_timestamp_ >> 29) & 0x06) | 0x21;
        pes_header.push_back(ch);
        ch = ((rtp_timestamp_ >> 22) & 0xFF);
        pes_header.push_back(ch);
        ch = ((rtp_timestamp_ >> 14) & 0xFE) | 0x01;
        pes_header.push_back(ch);
        ch = ((rtp_timestamp_ >> 7) & 0xFF);
        pes_header.push_back(ch);
        ch = ((rtp_timestamp_ << 1) & 0xFE) | 0x01;
        pes_header.push_back(ch);

        pes.insert(pes.begin(), pes_header.begin(), pes_header.end());
        unsigned short pes_len = pes.size() - 6; //6 is 00 00 01 + stream type + pes length itself
        pes[4] = pes_len >> 8;
        pes[5] = pes_len & 0xFF;

        //calculate PCR from PTS. 
        __int64 pcr = rtp_timestamp_;
        pcr *= 300;
        pcr -= 10000; // substract a bit to be on the safe side

        packet_gen_->SplitAndSendPESBuffer(&pes[0], pes.size(), &video_cont_counter_, GetVideoPid(pid_), pcr, true, StreamSendFunc, this);
    }        
}
#else
void rtp_reader::ProcessH264NAL(unsigned char* data, int len)
{
    unsigned char h = data[0];

    unsigned int forbidden_zero_bit = (h & 0x80) >> 7;
    unsigned int nal_ref_idc = (h & 0x60) >> 5;
    unsigned int nal_unit_type = (h & 0x1F);

    std::vector<unsigned char> pes;

    unsigned char buffer[4]; //00 00 00 01: NALU header
    memset(buffer, 0, sizeof(buffer));
    buffer[3] = 1;

    switch (nal_unit_type)
    {
    case 1: //Coded slice of a non-IDR picture
        pes.insert(pes.end(), buffer, buffer + sizeof(buffer));
        break;
    case 5: //Coded slice of an IDR picture
        {
            for (unsigned int i=0; i<prop_sets_.size(); i++)
            {
                pes.insert(pes.end(), buffer, buffer + sizeof(buffer));
                pes.insert(pes.end(), prop_sets_[i].begin(), prop_sets_[i].end());
            }
            pes.insert(pes.end(), buffer, buffer + sizeof(buffer));
        }
        break;
    default:
        break;
    }

    if (nal_unit_type == 5) //create stream description packets with the same rate as key-frames
    {
        CreateStreamDescriptionPackets();
    }

    if (pes.size() > 0)
    {
        pes.insert(pes.end(), data, data + len);
        //form pes header: 00 00 01 E0 xx xx 85 80 00 pts data
        std::vector<unsigned char> pes_header;
        pes_header.push_back(00);
        pes_header.push_back(00);
        pes_header.push_back(01);
        pes_header.push_back(0xE0); //stream type
        pes_header.push_back(00); //length placeholder
        pes_header.push_back(00); //length placeholder
        pes_header.push_back(0x85);
        pes_header.push_back(0x80);
        pes_header.push_back(05); //PTS size
        //PTS
        unsigned char ch;
        ch = ((rtp_timestamp_ >> 29) & 0x06) | 0x21;
        pes_header.push_back(ch);
        ch = ((rtp_timestamp_ >> 22) & 0xFF);
        pes_header.push_back(ch);
        ch = ((rtp_timestamp_ >> 14) & 0xFE) | 0x01;
        pes_header.push_back(ch);
        ch = ((rtp_timestamp_ >> 7) & 0xFF);
        pes_header.push_back(ch);
        ch = ((rtp_timestamp_ << 1) & 0xFE) | 0x01;
        pes_header.push_back(ch);

        pes.insert(pes.begin(), pes_header.begin(), pes_header.end());
        unsigned short pes_len = pes.size() - 6; //6 is 00 00 01 + stream type + pes length itself
        pes[4] = pes_len >> 8;
        pes[5] = pes_len & 0xFF;

        //calculate PCR from PTS. 
        __int64 pcr = rtp_timestamp_;
        pcr *= 300;
        pcr -= 10000; // substract a bit to be on the safe side

        packet_gen_->SplitAndSendPESBuffer(&pes[0], pes.size(), &video_cont_counter_, GetVideoPid(pid_), pcr, true, StreamSendFunc, this);
    }        
}
#endif 

void rtp_reader::CreateStreamDescriptionPackets()
{
    //PAT
    int section_length;
    STSStreamInfo stream_info;
    stream_info.TSID = pid_;
    stream_info.currentProgram = pid_;
    stream_info.PMTPid = GetPMTPid(pid_);
    unsigned char* pat = packet_gen_->CreatePATPacket(section_length, 0, &pat_cont_counter_, 
        stream_info.TSID, stream_info.currentProgram, stream_info.PMTPid);
    ProcessStream(pat, 188);

    STSPMTSectionInfo pmt_stream_info;
    //no descriptors
    pmt_stream_info.audio_stream_desc = NULL;
    pmt_stream_info.audio_stream_desc_len = 0;
    pmt_stream_info.video_stream_desc = NULL;
    pmt_stream_info.video_stream_desc_len = 0;
    pmt_stream_info.teletext_stream_desc = NULL;
    pmt_stream_info.teletext_stream_desc_len = 0;

    pmt_stream_info.ts_info = stream_info;

    //video
    pmt_stream_info.ts_info.VideoStreamType = 0x1B;
    pmt_stream_info.ts_info.VideoPID = GetVideoPid(pid_);
    pmt_stream_info.ts_info.PCRPid = GetVideoPid(pid_);
    //no teletext
    pmt_stream_info.ts_info.TeletextStreamType = 0;
    //no audio
    pmt_stream_info.ts_info.AudioStreamType = 0;

    unsigned char* pmt = packet_gen_->CreatePMTSection(section_length, 0, &pmt_stream_info);
    packet_gen_->SplitAndSendSectionBuffer(pmt, section_length, &pmt_cont_counter_, GetPMTPid(pid_), StreamSendFunc, this);
}

void rtp_reader::StreamSendFunc(const unsigned char* buf, int len, void* param)
{
    rtp_reader* parent = (rtp_reader*)param;
    parent->ProcessStream((unsigned char*)buf, len);
}

void rtp_reader::ProcessStream(unsigned char* data, int data_len)
{
    aligner_.write_stream(data, data_len);
}

void __stdcall rtp_reader::aligner_callback(const unsigned char* buf, unsigned long len, void* user_param)
{
    rtp_reader* parent = (rtp_reader*)user_param;
    parent->iptv_spts_converter_.process_stream(buf, len);
}

void rtp_reader::iptv_spts_converter_callback(const unsigned char* buf, size_t len, void* user_param)
{
    rtp_reader* parent = (rtp_reader*)user_param;
    parent->input_buffer_->write_stream(buf, len);
}


#if !defined(_SYNOLOGY_PPC)
unsigned char* rtp_reader::ProcessH264Payload(unsigned char* payload, int len, int& nal_len)
{
    unsigned char* ret_val = NULL;

    H264_PAYLOAD_HEADER* header = (H264_PAYLOAD_HEADER*)payload;
    if (header->nal_unit_type == H264_UT_FU_A)
    {
        //check second payload byte
        H264_FU_HEADER* fu_header = (H264_FU_HEADER*)(payload+1);
        if (fu_header->start_bit)
        {
            nal_accu_buffer_.clear();
            //write nal unit header first
            unsigned char nalu_header = (payload[0] & 0xE0) | (payload[1] & 0x1F);
            nal_accu_buffer_.push_back(nalu_header);
        }
        if (nal_accu_buffer_.size() > 0)
        {
            //write the body itself
            int hdr_len = sizeof(H264_PAYLOAD_HEADER) + sizeof(H264_FU_HEADER);
            nal_accu_buffer_.insert(nal_accu_buffer_.end(), payload + hdr_len, payload + len);
            if (fu_header->end_bit)
            {
                ret_val = &nal_accu_buffer_[0];
                nal_len = nal_accu_buffer_.size();
            }
        }
    }
    else
    {
        nal_len = len;
        ret_val = payload;
    }

    return ret_val;
}
#else
// PPC
//
unsigned char* rtp_reader::ProcessH264Payload(unsigned char* payload, int len, int& nal_len)
{
    unsigned char* ret_val = NULL;
    unsigned char h = payload[0];
    
    unsigned int forbidden_zero_bit = (h & 0x80) >> 7;
    unsigned int nal_ref_idc = (h & 0x60) >> 5;
    unsigned int nal_unit_type = (h & 0x1F);

    if (nal_unit_type == H264_UT_FU_A)
    {
        //check second payload byte
        unsigned char fu_header = payload[1];
        bool start_bit = ((fu_header & 0x80) != 0);
        bool end_bit = ((fu_header & 0x40) != 0);

        if (start_bit)
        {
            nal_accu_buffer_.clear();
            //write nal unit header first
            unsigned char nalu_header = (payload[0] & 0xE0) | (payload[1] & 0x1F);
            nal_accu_buffer_.push_back(nalu_header);
        }

        if (nal_accu_buffer_.size() > 0)
        {
            //write the body itself
            int hdr_len = sizeof(H264_PAYLOAD_HEADER) + sizeof(H264_FU_HEADER);
            nal_accu_buffer_.insert(nal_accu_buffer_.end(), payload + hdr_len, payload + len);

            if (end_bit)
            {
                ret_val = &nal_accu_buffer_[0];
                nal_len = nal_accu_buffer_.size();
            }
        }
    }
    else
    {
        nal_len = len;
        ret_val = payload;
    }

    return ret_val;
}

#endif
