/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_network_helper.h>
#include <dl_common.h>
#include <dl_m3u.h>
#include "http_reader.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

const long http_restart_timeout_format_changed_ms = 50;

http_reader::http_reader(ts_circle_buffer* inputStream, const char* user_agent) :
    input_buffer_(inputStream),
    http_comm_(NULL),
    request_handle_(NULL),
    receive_thread_(NULL),
    new_reader_type_(iptv_rte_unknown),
    restart_required_(false),
    ms_restart_timeout_(http_restart_timeout_format_changed_ms),
    restart_count_(0),
    aligner_(aligner_callback, this),
    smooth_streamer_(smooth_streamer_callback, this, &exit_flag_),
    iptv_spts_converter_(iptv_spts_converter_callback, this)
{
    user_agent_ = strlen(user_agent) == 0 ? PRODUCT_NAME_UTF8 : user_agent;
    stream_start_time_ = boost::posix_time::microsec_clock::universal_time();
}

bool http_reader::create(const char* url, const char* local_address)
{
    if (!url)
        return false;

    url_.assign(url);

    return open_connection();
}

bool http_reader::open_connection()
{
    bool ret_val = false;

    std::string address;
	std::string user;
	std::string pswd;
    unsigned short port;
	std::string url_suffix;
    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(url_.c_str(), address, 
        user, pswd, port, url_suffix);

    if (address.empty() || url_suffix.empty())
    {
        log_error(L"http_reader::open_connection. Invalid url %1%") % string_cast<EC_UTF8>(url_);
        return false;
    }

    http_comm_ = new http_comm_handler(user_agent_.c_str(), address.c_str(), user.c_str(), pswd.c_str(), port);

    if (http_comm_->Init())
    {
        log_info(L"*** IPTV: OpenConnection() - %s:%u %s") % string_cast<EC_UTF8>(address) % port % string_cast<EC_UTF8>(url_suffix);

        request_handle_ = http_comm_->SendGetRequest(url_suffix.c_str(), proto == DL_NET_PROTO_HTTPS);

        if (request_handle_)
        {
            ret_val = true; // success
        }
    }

    if (!ret_val)
    {
        delete http_comm_;
        http_comm_ = NULL;
    }

    return ret_val;
}

bool http_reader::start(int restart_count)
{
    stream_start_time_ = boost::posix_time::microsec_clock::universal_time();
    restart_count_ = restart_count;

    iptv_spts_converter_.init();
	smooth_streamer_.init();

    // Start receive streaming thread	 
    exit_flag_ = false;
    receive_thread_ = new boost::thread(boost::bind(&http_reader::thread_function, this));

    return true;
}

void http_reader::stop()
{
    log_info(L"*** IPTV: StopReceiving() - url=%1%") % string_cast<EC_UTF8>(url_);

    if (request_handle_)
    {
        http_comm_->CancelRequest(request_handle_);
    }

    if (receive_thread_)
    {
    	exit_flag_ = true;
        receive_thread_->join();
        delete receive_thread_;
        receive_thread_ = NULL;
    }

    if (request_handle_)
    {
        http_comm_->CloseRequest(request_handle_);
        request_handle_ = NULL;
    }

    if (http_comm_)
    {
        http_comm_->Term();
        delete http_comm_;
        http_comm_ = NULL;
    }
	smooth_streamer_.term();
    iptv_spts_converter_.term();
}

void http_reader::thread_function()
{
    receive_stream();
}

void http_reader::request_restart_on_http_disconnect()
{
    bool restart = true;

    //reset restart counter if stream was active for more than 1 minute
    const boost::posix_time::time_duration count_reset_timeout = boost::posix_time::milliseconds(60000);
    boost::posix_time::ptime now = boost::posix_time::microsec_clock::universal_time();
    if (now - stream_start_time_ >= count_reset_timeout)
        restart_count_ = 0;

    switch (restart_count_)
    {
    case 0:
        ms_restart_timeout_ = 50;
        break;
    case 1:
        ms_restart_timeout_ = 1000;
        break;
    case 2:
        ms_restart_timeout_ = 5000;
        break;
    case 3:
        ms_restart_timeout_ = 20000;
        break;
    case 4:
        ms_restart_timeout_ = 40000;
        break;
    default:
        //do not restart anymore
        log_warning(L"http_reader::request_restart_on_http_disconnect. Maximum restart count reached");
        restart = false;
        break;
    }
    if (restart)
    {
        ++restart_count_;

        new_reader_type_ = iptv_rte_http;
        restart_required_ = true;
    }
}

void http_reader::receive_stream()
{
    log_info(L"*** IPTV: HTTP thread started - url=%1%") % string_cast<EC_UTF8>(url_);
    unsigned char rcv_buf[4*RECV_BUFSIZE] = { 0 };

    // Reset circular buffers
    input_buffer_->reset();
    bool data_received = false;

    while (!exit_flag_)
    {
        unsigned long bytes_read = sizeof(rcv_buf);
        bool read = http_comm_->ReadRequestResponse(request_handle_, rcv_buf, bytes_read, &exit_flag_);
        
        if (!read || (bytes_read == 0))
        {
        #ifdef _WIN32
            DWORD dw = GetLastError();
            if (!read && dw > 0)
            {
                log_error(L"*** IPTV: ReadRequestResponse() returned %1% - url=%2%")
                    % GetLastError() % string_cast<EC_UTF8>(url_);
            }
        #endif

            //check if restart has to be requested
            if (!exit_flag_)
                request_restart_on_http_disconnect();

            exit_flag_ = true;
            break;
        }
        else
        {
            if (!data_received && (bytes_read > 0))
            {
                data_received = true;
                log_info(L"*** IPTV: first packet received (%1% bytes)") % bytes_read;
                //analyze the contents to check if this is HLS
                http_comm_handler::http_headers_t headers;
                if (http_comm_->ReadResponseHeaders(request_handle_, &headers))
                {
                    //look for content-type application/vnd.apple.mpegurl
                    for (size_t i=0; i<headers.size(); i++)
                    {
                        if (boost::icontains(headers[i], m3u_hls_content_type))
                        {
                            //this is HLS
                            log_info(L"http_reader::receive_stream. HLS is detected. Restart reception");

                            new_reader_type_ = iptv_rte_hls;
                            ms_restart_timeout_ = http_restart_timeout_format_changed_ms;
                            restart_count_ = 0; //reset restart count when switching format
                            restart_required_ = true;

                            exit_flag_ = true;
                            break;
                        }
                    }
                }
            }

            aligner_.write_stream(rcv_buf, bytes_read);
        }
    }

    log_info(L"*** IPTV: HTTP thread terminated - url=%1%") % string_cast<EC_UTF8>(url_);
}

bool http_reader::restart_required(iptv_reader_type_e& reader_type, long& ms_timeout, int& restart_count)
{
    if (restart_required_)
    {
        reader_type = new_reader_type_;
        ms_timeout = ms_restart_timeout_;
        restart_count = restart_count_;
    }

    return restart_required_;
}

void __stdcall http_reader::aligner_callback(const unsigned char* buf, unsigned long len, void* user_param)
{
    http_reader* parent = (http_reader*)user_param;
    parent->iptv_spts_converter_.process_stream(buf, len);
}

void http_reader::iptv_spts_converter_callback(const unsigned char* buf, size_t len, void* user_param)
{
    http_reader* parent = (http_reader*)user_param;
    parent->smooth_streamer_.process_stream(buf, len);
}

void http_reader::smooth_streamer_callback(const unsigned char* buf, size_t len, void* user_param)
{
    http_reader* parent = (http_reader*)user_param;
    parent->input_buffer_->write_stream(buf, len);
}

