/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/filesystem.hpp>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_language_settings.h>
#include <dl_locale_strings.h>
#include <dl_file_procedures.h>
#include "iptv_scan_params.h"
#include "xtream_codes.h"
#include "iptv_scan_provider.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

static std::string iptv_parameters_container_id = "b5747afe-20d3-4dd7-84ca-180c20215b96";

iptv_scan_provider_t::iptv_scan_provider_t(const dvblink::filesystem_path_t& provider_storage_path) :
    provider_storage_path_(provider_storage_path)
{
}

static bool get_provider_info_from_file(const boost::filesystem::path& fname, std::string& id, 
    provider_info_t& provider, provider_scan_list_t* scan_list)
{
    if (!boost::filesystem::exists(fname))
        return false;

    //id is the file name
    //name is the file name
    //scan element is the full file pathname
    scan_element_t se;
#ifdef WIN32
    id = string_cast<EC_UTF8>(fname.filename().wstring());
    provider.name_ = id;
    se = string_cast<EC_UTF8>(fname.wstring());
#else
    id = fname.filename().string();
    provider.name_ = id;
    se = fname.string();
#endif

    provider.scan_params_[provider_auto_origin_favorites_key] = PARAM_VALUE_YES;

    if (scan_list != NULL)
        scan_list->push_back(se);

	return true;
}

void iptv_scan_provider_t::fill_url_playlist_provider_details(provider_info_t& url_playlist_provider)
{
    url_playlist_provider.name_ = language_settings::GetInstance()->GetItemName(iptv_url_entry_provider_name);
    url_playlist_provider.desc_ = language_settings::GetInstance()->GetItemName(iptv_url_entry_provider_desc);
    url_playlist_provider.standard_ = st_iptv;
    url_playlist_provider.id_ = internal_id_to_id(iptv_manual_url_provider);

    url_playlist_provider.scan_params_[provider_auto_origin_favorites_key] = PARAM_VALUE_YES;

    url_playlist_provider.param_container_.id_ = url_playlist_provider.id_.get();
    url_playlist_provider.param_container_.name_ = url_playlist_provider.name_.get();
    url_playlist_provider.param_container_.desc_ = url_playlist_provider.desc_.get();

    editable_param_element_t url_element;
    url_element.key_ = iptv_url_entry_field_key;
    url_element.name_ = language_settings::GetInstance()->GetItemName(iptv_url_entry_field_name);
    url_element.format_ = epf_string;
    url_playlist_provider.param_container_.editable_params_.push_back(url_element);
}

void iptv_scan_provider_t::fill_hdhomerun_provider_details(provider_info_t& hdhomerun_provider)
{
    hdhomerun_provider.name_ = language_settings::GetInstance()->GetItemName(iptv_hdhomerun_provider_name);
    hdhomerun_provider.desc_ = language_settings::GetInstance()->GetItemName(iptv_hdhomerun_provider_desc);
    hdhomerun_provider.standard_ = st_iptv;
    hdhomerun_provider.id_ = internal_id_to_id(iptv_hdhomerun_provider);

    hdhomerun_provider.param_container_.id_ = hdhomerun_provider.id_.get();
    hdhomerun_provider.param_container_.name_ = hdhomerun_provider.name_.get();
    hdhomerun_provider.param_container_.desc_ = hdhomerun_provider.desc_.get();

    editable_param_element_t hdhomerun_element;
    hdhomerun_element.key_ = iptv_hdhomerun_addr_field_key;
    hdhomerun_element.name_ = language_settings::GetInstance()->GetItemName(iptv_hdhomerun_addr_field_name);
    hdhomerun_element.format_ = epf_string;
    hdhomerun_provider.param_container_.editable_params_.push_back(hdhomerun_element);
}

void iptv_scan_provider_t::fill_xtream_codes_provider_details(provider_info_t& xtream_codes_provider)
{
    xtream_codes_provider.name_ = language_settings::GetInstance()->GetItemName(iptv_xtream_codes_provider_name);
    xtream_codes_provider.desc_ = language_settings::GetInstance()->GetItemName(iptv_xtream_codes_provider_desc);
    xtream_codes_provider.standard_ = st_iptv;
    xtream_codes_provider.id_ = internal_id_to_id(iptv_xtream_codes_provider);

    xtream_codes_provider.scan_params_[provider_auto_origin_favorites_key] = PARAM_VALUE_YES;

    xtream_codes_provider.param_container_.id_ = xtream_codes_provider.id_.get();
    xtream_codes_provider.param_container_.name_ = xtream_codes_provider.name_.get();
    xtream_codes_provider.param_container_.desc_ = xtream_codes_provider.desc_.get();

    editable_param_element_t url_element;
    url_element.key_ = iptv_xtream_codes_url_field_key;
    url_element.name_ = language_settings::GetInstance()->GetItemName(iptv_xtream_codes_url_field_name);
    url_element.format_ = epf_string;
    xtream_codes_provider.param_container_.editable_params_.push_back(url_element);

    editable_param_element_t user_element;
    user_element.key_ = iptv_xtream_codes_user_field_key;
    user_element.name_ = language_settings::GetInstance()->GetItemName(iptv_xtream_codes_user_field_name);
    user_element.format_ = epf_string;
    xtream_codes_provider.param_container_.editable_params_.push_back(user_element);

    editable_param_element_t pswd_element;
    pswd_element.key_ = iptv_xtream_codes_pswd_field_key;
    pswd_element.name_ = language_settings::GetInstance()->GetItemName(iptv_xtream_codes_pswd_field_name);
    pswd_element.format_ = epf_string;
    xtream_codes_provider.param_container_.editable_params_.push_back(pswd_element);
}

bool iptv_scan_provider_t::get_rescan_settings(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, parameters_container_t& settings)
{
    //container parameters
    settings.id_ = "55641080-6293-4372-a28d-750792213e4d";
    settings.name_ = dvblink::engine::language_settings::GetInstance()->GetItemName(rescan_settings_container_name);

    //provider tag
    editable_param_element_t tag_element;
    tag_element.key_ = provider_tag_key;
    tag_element.name_ = language_settings::GetInstance()->GetItemName(provider_tag_param_name);

    parameter_value_t current_value = get_value_for_param(provider_tag_key, scanner_settings);

    tag_element.value_ = current_value;
    tag_element.format_ = epf_string;
    settings.editable_params_.push_back(tag_element);

    return true;
}

void iptv_scan_provider_t::get_providers(source_type_e standard, provider_info_map_t& providers)
{
    //only process iptv
    if (standard != st_iptv)
        return;

    providers[standard] = provider_description_t();
    providers[standard].standard_ = standard;
    providers[standard].standard_name_ = language_settings::GetInstance()->GetItemName(iptv_source_standard_name);

    //add iptv parameters
    parameters_container_t& cointainer = providers[standard].parameters_;
    cointainer.id_ = iptv_parameters_container_id;
    cointainer.name_ = language_settings::GetInstance()->GetItemName(iptv_parameters_container_name);
    cointainer.desc_ = language_settings::GetInstance()->GetItemName(iptv_parameters_container_desc);

    //provider tag
    editable_param_element_t tag_element;
    tag_element.key_ = provider_tag_key;
    tag_element.name_ = language_settings::GetInstance()->GetItemName(provider_tag_param_name);
    tag_element.value_ = provider_tag_default_value;
    tag_element.format_ = epf_string;
    cointainer.editable_params_.push_back(tag_element);

    // manual iptv playlist url entry
    provider_info_t url_playlist_provider;
    fill_url_playlist_provider_details(url_playlist_provider);
    providers[standard].providers_.push_back(url_playlist_provider);

    // hdhomerun prime/extend ip address
    provider_info_t hdhomerun_provider;
    fill_hdhomerun_provider_details(hdhomerun_provider);
    providers[standard].providers_.push_back(hdhomerun_provider);

    // xtream-codes provider
    provider_info_t xtream_codes_provider;
    fill_xtream_codes_provider_details(xtream_codes_provider);
    providers[standard].providers_.push_back(xtream_codes_provider);

    dvblink::filesystem_path_t p = provider_storage_path_ / get_source_type_string_from_value(standard);
    //enumerate all m3u files inside this directory (if it exists)
    std::vector<boost::filesystem::path> files;
    filesystem::find_files(p.to_boost_filesystem(), files, L".m3u");
    //add m3u8 files
    std::vector<boost::filesystem::path> m3u8_files;
    filesystem::find_files(p.to_boost_filesystem(), m3u8_files, L".m3u8");
    files.insert(files.end(), m3u8_files.begin(), m3u8_files.end());

    for (size_t idx=0; idx < files.size(); idx++)
    {
        provider_info_t pi;
        pi.standard_ = standard;
        pi.desc_ = language_settings::GetInstance()->GetItemName(iptv_source_standard_desc);

        std::string internal_id;

        if (get_provider_info_from_file(files[idx], internal_id, pi, NULL))
        {
            pi.id_ = internal_id_to_id(internal_id);
            providers[standard].providers_.push_back(pi);
        }
    }
}

void iptv_scan_provider_t::id_to_internal_id(const std::string& id_str, std::string& internal_id)
{
    //scan provider
    std::string sp = get_scan_provider_from_id(id_str);
    if (!sp.empty())
        internal_id = id_str.substr(sp.size() + 1);
}

std::string iptv_scan_provider_t::internal_id_to_id(const std::string& internal_id)
{
    std::stringstream strbuf;
    strbuf << get_scan_provider_name() << get_id_sep_symbol() << internal_id;

    return strbuf.str();
}

bool iptv_scan_provider_t::get_provider_details(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, provider_info_t& provider_info, provider_scan_list_t& scan_list)
{
    bool ret_val = false;
    //convert id to internal id
    std::string internal_id;
    id_to_internal_id(id.get(), internal_id);

    if (boost::iequals(internal_id, iptv_manual_url_provider))
    {
        if (is_key_present(iptv_url_entry_field_key, scanner_settings))
        {
            fill_url_playlist_provider_details(provider_info);

            scan_element_t se = get_iptv_scan_element_from_scan_params(scanner_settings);
            scan_list.push_back(se);
            ret_val = true;
        }
    } else
    if (boost::iequals(internal_id, iptv_hdhomerun_provider))
    {
        if (is_key_present(iptv_hdhomerun_addr_field_key, scanner_settings))
        {
            fill_hdhomerun_provider_details(provider_info);

            scan_element_t se = get_iptv_scan_element_from_scan_params(scanner_settings);
            scan_list.push_back(se);
            ret_val = true;
        }
    } else
    if (boost::iequals(internal_id, iptv_xtream_codes_provider))
    {
        fill_xtream_codes_provider_details(provider_info);

        scan_element_t se = get_iptv_scan_element_from_scan_params(scanner_settings);
        scan_list.push_back(se);
        ret_val = true;
    } else
    {
        //form path to the file
        dvblink::filesystem_path_t p = provider_storage_path_ / get_source_type_string_from_value(st_iptv) / internal_id;

        std::string idstr;
        if (get_provider_info_from_file(p.to_boost_filesystem(), idstr, provider_info, &scan_list))
        {
            provider_info.id_ = internal_id_to_id(idstr);
            provider_info.standard_ = st_iptv;
            provider_info.desc_ = language_settings::GetInstance()->GetItemName(iptv_source_standard_desc);
            //save playlist path into scan parameters
            provider_info.scan_params_[iptv_file_playlist_key] = p.to_string();

            ret_val = true;
        }
    }
    return ret_val;
}

void iptv_scan_provider_t::fill_headend_info(const provider_info_t& provider_info, const concise_param_map_t& scanner_settings, headend_info_t& headend_info)
{
    //string to hash
    std::string hash_str = provider_info.id_.get();
    //convert id to internal id
    std::string internal_id;
    id_to_internal_id(provider_info.id_.get(), internal_id);

    if (boost::iequals(internal_id, iptv_manual_url_provider))
    {
        //remote playlist
        headend_info.name_ = language_settings::GetInstance()->GetItemName(iptv_url_entry_provider_name);

        std::string url = get_value_for_param(iptv_url_entry_field_key, scanner_settings).get();
        headend_info.desc_ = url;

        hash_str += url;
    } else
    if (boost::iequals(internal_id, iptv_hdhomerun_provider))
    {
        //hdhomerun
        headend_info.name_ = language_settings::GetInstance()->GetItemName(iptv_hdhomerun_provider_name);

        std::string addr = get_value_for_param(iptv_hdhomerun_addr_field_key, scanner_settings).get();
        headend_info.desc_ = addr;

        hash_str += addr;
    } else
    if (boost::iequals(internal_id, iptv_xtream_codes_provider))
    {
        //xtream codes
        headend_info.name_ = language_settings::GetInstance()->GetItemName(iptv_xtream_codes_provider_name);

        xtream_codes_info_t sci;
        get_xtream_codes_info_from_scanner(scanner_settings, sci);

        headend_info.desc_ = sci.get_description();

        hash_str += (sci.url_ + sci.user_);
    } else
    {
        //local playlist
        headend_info.name_ = language_settings::GetInstance()->GetItemName(iptv_local_playlist_provider_name);
        headend_info.desc_ = provider_info.name_.get();
    }

    //overwrite headend name with a tag
    parameter_value_t provider_tag = get_value_for_param(provider_tag_key, scanner_settings);
    if (!provider_tag.empty())
        headend_info.name_ = provider_tag.get();

    //hash - headend_id
    boost::to_lower(hash_str);
    headend_info.id_ = make_headend_id(provider_info.id_, hash_str);
}

