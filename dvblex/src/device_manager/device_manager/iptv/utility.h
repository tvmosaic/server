/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#ifdef _WIN32
#include <winsock2.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#endif

#include <stdlib.h>
#include <time.h>
#include <string>
#include <cctype>
#include <cstring>
#include <algorithm>

class utility
{
    struct isequal
    {
        bool operator() (char l, char r) const
        {
            return std::tolower(l) == std::tolower(r);
        }
    };

public:
    static char* StrDup(char const* str);
    static char* StrDupSize(char const* str);
    static int GetTimeOfDay(timeval *tv);
    static double GetTimeNow();
    static unsigned int Random32();
    static bool istrncmp(const char* s1, const char* s2, size_t n)
    {
        return std::equal(s1, s1 + n, s2, isequal());
    }

private:
    utility();
};
