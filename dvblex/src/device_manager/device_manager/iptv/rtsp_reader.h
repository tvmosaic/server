/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <dl_sockets.h>
#endif
#include <string>
#include <iostream>
#include <dl_circle_buffer.h>
#include "net_reader.h"
#include "rtcp_control.h"
#include "media_session.h"

namespace dvblex { 

class rtsp_reader : public net_reader
{
public:
    rtsp_reader(dvblink::engine::ts_circle_buffer* inputStream, unsigned short client_port);
    ~rtsp_reader();

    bool create(const char* url, const char* local_address);
    bool start(int restart_count);
    void stop();

private:
    bool OpenConnection(const char* url);
    void CloseConnection();
    bool OpenTCPConnection(char const* url, int timeout);
    unsigned short GetSourcePortNum(unsigned short& portNum);
    char* Options(char* url);
    char* Describe(const char* url, bool allowKasennaProtocol);
    bool SetupMediaSubsession(media_subsession& subsession);
    bool Play(media_session& session);
    bool Teardown(media_session& session);
    bool SendRequest(char* request);
    bool GetResponse(unsigned int& bytesRead, unsigned int& responseCode, char*& firstLine, char*& nextLineStart);
    unsigned int ReadBuffer(char*& responseBuffer, unsigned responseBufferSize);
    unsigned int ReadBufferExact(char* buffer, int bufferSize);

    char* GetLine(char* startOfLine);
    bool ParseResponseCode(char const* line, unsigned int& responseCode);
    bool IsAbsoluteURL(char const* url);
    void ConstructSubsessionURL(media_subsession const& subsession, char const*& prefix, char const*& separator, char const*& suffix);
    char const* SessionURL(media_session const& session);
    bool ParseTransportResponse(char const* line, char*& serverAddressStr, boost::uint16_t& serverPortNum, unsigned char& rtpChannelId, unsigned char& rtcpChannelId);
    bool ParseRTPInfoHeader(char*& line, boost::uint16_t& seqNum, unsigned int& timestamp);
    bool ParseRangeHeader(char const* buf, double& rangeStart, double& rangeEnd);
    bool ParseScaleHeader(char const* line, float& scale);

private:
    dvblink::engine::ts_circle_buffer* input_stream_;
    udp_reader*	rtsp_stream_reader_;
    rtcp_control* rtcp_reader_;
    media_session* session_;
    SOCKET m_Socket;
    std::string local_addr_;
    unsigned short client_port_;

    char* user_agent_header_;
    std::string authentication_string_;
    std::string user_;
    std::string password_;
    char* base_url_;
    char* last_session_id_;
    char* kasenna_content_type_;
    unsigned int server_addr_;
    std::string server_host_name_;
    char cname_[128];
    bool stream_using_tcp_;

    bool server_is_kasenna_;
    bool server_is_microsoft_;
    int response_buffer_size_;
    char* response_buffer_;
    std::string response_;
    unsigned int cseq_;
    unsigned int session_timeout_; // optionally set in response "Session"
    unsigned char tcp_stream_id_count_; // used for (optional) RTP/TCP
};

}

