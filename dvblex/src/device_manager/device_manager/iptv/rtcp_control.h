/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_socket_api.h>
#ifdef _WIN32
#include <ws2tcpip.h>
#else
#include <netdb.h>
#include <arpa/inet.h>
#endif
#include <dl_timer_procedure.h>
#include "udp_reader.h"
#include "rtp_reader.h"

namespace dvblex { 

class SDESItem 
{
public:
    SDESItem(unsigned char tag, char* value);

    unsigned char const* Data() const { return m_Data; }
    unsigned TotalSize() const;

private:
    unsigned char m_Data[2 + 256]; // first 2 bytes are tag and length
};

class rtcp_control : public udp_reader
{
public:
    rtcp_control(rtp_reader* rtpReader, char* cname, unsigned short serverPort);
    ~rtcp_control() {}

private:
    typedef struct _PACKET
    {
        int   m_nLen;
        char* m_pCurrent;
        char* m_pEnd;
    } PACKET;

    virtual void stream_receive_function();
    bool CheckRTCPPacket(unsigned char* buffer);
    void ParseRTCPPacket(unsigned char* buffer, unsigned int bufferLen);
    void ParseSRFields(PACKET* pkt, int count);
    void ParseRRFields(PACKET* pkt, int count);
    void ParseSDESFields(PACKET* pkt, int count);

    void SendReport();
    void SendBYE();
    void AddBYEFields(PACKET* pkt);
    void AddReportFields(PACKET* pkt);
    void AddSDESFields(PACKET* pkt);
    void AddReportBlock(rtp_reception_stats* stats, PACKET* pkt);
    bool SendBuffer(unsigned char* buffer, int bufLen);

    //static void __stdcall OnExpire(boost::uint32_t uTimerID, boost::uint32_t uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2);
    void OnExpire(const boost::system::error_code& e);
    void OnExpire(int members, int senders, double rtcp_bw, int we_sent, double *avg_rtcp_size, int *initial, double tc, double *tp);
    double RTCPInterval(int members, int senders, double rtcp_bw, int we_sent, double avg_rtcp_size, int initial);
    void Schedule(double nextTime);

    bool GetPacketBytes(unsigned char* dst, PACKET* pkt, int n);
    bool SetPacketBytes(PACKET* pkt, unsigned char* src, int n);
    bool LookPacketBytes(unsigned char* dst, PACKET* pkt, int n);
    bool SkipPacketBytes(PACKET* pkt, int n);

private:
    rtp_reader* rtp_reader_;
    SDESItem cname_;

    dvblink::engine::timer_procedure<rtcp_control>* rtcp_timer_;
    boost::mutex lock_;

    unsigned int tot_session_bw_;
    double aver_rtcp_size_;
    int is_initial_;
    double prev_report_time_;
    int last_sent_size_;
    unsigned short server_port_;
};

}
