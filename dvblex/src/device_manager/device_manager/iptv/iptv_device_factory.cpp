/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_language_settings.h>
#include <dl_locale_strings.h>
#include "iptv_device_factory.h"
#include "iptv_playback_src.h"
#include "iptv_props.h"
#include "iptv_device.h"

using namespace dvblex;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink;

iptv_device_factory_t::iptv_device_factory_t(const dvblink::filesystem_path_t& device_config_path) :
    device_factory_t(device_config_path)
{
}

iptv_device_factory_t::~iptv_device_factory_t()
{
}

device_t* iptv_device_factory_t::create_device(const dvblink::device_id_t& device_id, directory_settings_obj_t& dir_settings)
{
    iptv_device_props_t* iptv_device_props = get_iptv_device_props(device_id);

    return new iptv_device_t(iptv_device_props, dir_settings);
}

void iptv_device_factory_t::get_device_desc_for_manual_device(const dvblink::device_id_t& id, 
                                       const concise_param_map_t& params, device_descriptor_t& dd)
{
    dd.id_ = id;
    dd.name_ = language_settings::GetInstance()->GetItemName(iptv_source_device_name);
    dd.supported_standards_ = st_iptv;
    dd.uuid_ = id.get();
    dd.can_be_deleted_ = true;
    dd.has_settings_ = true;
}

void iptv_device_factory_t::get_device_template_list(selectable_param_list_t& templates)
{
    selectable_param_element_t iptv_element;
    iptv_element.name_ = language_settings::GetInstance()->GetItemName(iptv_source_device_name);
    iptv_element.value_ = get_name();

    templates.push_back(iptv_element);
}

bool iptv_device_factory_t::get_device_configurable_props(const dvblink::device_id_t& device_id, configurable_device_props_t& props)
{
    iptv_device_props_t* iptv_device_props = get_iptv_device_props(device_id);

    iptv_device_props->get_props_as_params(props);

    delete iptv_device_props;

    return true;
}

iptv_device_props_t* iptv_device_factory_t::get_iptv_device_props(const dvblink::device_id_t& device_id)
{
    concise_param_map_t device_props;
    {
        boost::unique_lock<boost::mutex> l(manual_devices_lock_);
        if (device_concise_param_map_.find(device_id) != device_concise_param_map_.end())
            device_props = device_concise_param_map_[device_id];
    }

    iptv_device_props_t* iptv_device_props = new iptv_device_props_t();
    iptv_device_props->set_props(device_props);

    return iptv_device_props;
}

set_device_props_result_e iptv_device_factory_t::set_device_configurable_props(const dvblink::device_id_t& device_id, const concise_param_map_t& props)
{
    set_device_props_result_e ret_val = device_factory_t::set_device_configurable_props(device_id, props);

    if (ret_val != sdpr_error)
    {
        //check if epg scan has to be requested on this device
        iptv_device_props_t iptv_device_props;
        iptv_device_props.set_props(props);
        iptv_device_props_t::xmltv_url_list_t url_list = iptv_device_props.get_xmltv_url();

        for (size_t i=0; i<url_list.size(); i++)
        {
            if (!url_list[i].empty())
            {
                ret_val = (set_device_props_result_e)(ret_val | sdpr_epg_rescan_needed);
                break;
            }
        }
    }

    return ret_val;
}

device_playback_src_obj_t iptv_device_factory_t::get_playback_src(const dvblink::device_id_t& device_id)
{
    device_playback_src_obj_t ret_val;

    boost::shared_ptr<iptv_playback_src_t> ips = boost::shared_ptr<iptv_playback_src_t>(new iptv_playback_src_t());
    ret_val = ips;

    return ret_val;
}
