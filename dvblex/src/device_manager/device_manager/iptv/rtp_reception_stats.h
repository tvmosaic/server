/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_socket_api.h>
#ifdef _WIN32
#include <hash_map>
#else
#include <ext/hash_map>
#endif
#include <boost/thread/mutex.hpp>

#ifdef _WIN32
namespace std
{
    using namespace stdext;
}
#else
namespace std
{
    using namespace __gnu_cxx;
}
#endif

class rtp_reception_stats 
{
public:
    rtp_reception_stats(unsigned int SSRC, unsigned short initialSeqNum);
    rtp_reception_stats(unsigned int SSRC);
    ~rtp_reception_stats();

    unsigned int SSRC() const { return ssrc_; }
    unsigned NumPacketsReceivedSinceLastReset() const { return num_packets_received_since_last_reset_; }
    unsigned TotNumPacketsReceived() const { return tot_num_packets_received_; }
    double TotNumKBytesReceived() const;

    unsigned TotNumPacketsExpected() const { return highest_ext_seq_num_received_ - base_ext_seq_num_received_; }

    unsigned BaseExtSeqNumReceived() const { return base_ext_seq_num_received_; }
    unsigned LastResetExtSeqNumReceived() const { return last_reset_ext_seq_num_received_; }
    unsigned HighestExtSeqNumReceived() const { return highest_ext_seq_num_received_; }

    unsigned Jitter() const;

    unsigned LastReceivedSRNTPmsw() const { return last_received_sr_ntp_msw_; }
    unsigned LastReceivedSRNTPlsw() const { return last_received_sr_ntp_lsw_; }
    timeval const& LastReceivedSRTime() const { return last_received_sr_time_; }

    unsigned MinInterPacketGapUS() const { return min_inter_packet_gap_us_; }
    unsigned MaxInterPacketGapUS() const { return max_inter_packet_gap_us_; }
    timeval const& TotalInterPacketGaps() const { return total_inter_packet_gaps_; }

    void NoteIncomingPacket(unsigned short seqNum, unsigned int rtpTimestamp, unsigned timestampFrequency, bool useForJitterCalculation,
    timeval& resultPresentationTime, bool& resultHasBeenSyncedUsingRTCP, unsigned packetSize /* payload only */);

    void NoteIncomingSR(unsigned int ntpTimestampMSW, unsigned int ntpTimestampLSW, unsigned int rtpTimestamp);
    void Init(unsigned int SSRC);
    void InitSeqNum(unsigned short initialSeqNum);
    // resets periodic stats (called each time they're used to
    // generate a reception report)
    void Reset();
    bool SeqNumLT(unsigned short s1, unsigned short s2);

private:
    unsigned int ssrc_;
    unsigned num_packets_received_since_last_reset_;
    unsigned tot_num_packets_received_;
    unsigned int tot_bytes_received_hi_;
    unsigned int tot_bytes_received_lo_;
    bool have_seen_initial_sequence_number_;
    unsigned base_ext_seq_num_received_;
    unsigned last_reset_ext_seq_num_received_;
    unsigned highest_ext_seq_num_received_;
    int last_transit_; // used in the jitter calculation
    unsigned int previous_packet_rtp_timestamp_;
    double jitter_;
    // The following are recorded whenever we receive a RTCP SR for this SSRC:
    unsigned last_received_sr_ntp_msw_; // NTP timestamp (from SR), most-signif
    unsigned last_received_sr_ntp_lsw_; // NTP timestamp (from SR), least-signif
    timeval last_received_sr_time_;
    timeval last_packet_reception_time_;
    unsigned min_inter_packet_gap_us_;
    unsigned max_inter_packet_gap_us_;
    timeval total_inter_packet_gaps_;

    // Used to convert from RTP timestamp to 'wall clock' time
    bool has_been_synchronized_;
    unsigned int sync_timestamp_;
    timeval sync_time_;
};

typedef std::hash_map<unsigned long, rtp_reception_stats*> TRTPReceptionStats;

class rtp_reception_stats_db 
{
public:
    rtp_reception_stats_db();
    ~rtp_reception_stats_db();

    unsigned TotNumPacketsReceived() const { return tot_num_packets_received_; }
    unsigned NumActiveSourcesSinceLastReset() const { return num_active_sources_since_last_reset_; }

    // resets periodic stats (called each time they're used to
    // generate a reception report)
    void Reset();

    // The following is called whenever a RTP packet is received:
    void NoteIncomingPacket(unsigned int SSRC, unsigned short seqNum, unsigned int rtpTimestamp, unsigned timestampFrequency, bool useForJitterCalculation,
    timeval& resultPresentationTime, bool& resultHasBeenSyncedUsingRTCP, unsigned packetSize /* payload only */);

    // The following is called whenever a RTCP SR packet is received:
    void NoteIncomingSR(unsigned int SSRC, unsigned int ntpTimestampMSW, unsigned int ntpTimestampLSW, unsigned int rtpTimestamp);

    void Lock() {lock_.lock();}
    void Unlock() {lock_.unlock();}

public:
    TRTPReceptionStats reception_stats_;

private:
    rtp_reception_stats* Lookup(unsigned int SSRC) const;
    void RemoveRecord(unsigned int SSRC);
    void Add(unsigned int SSRC, rtp_reception_stats* stats);

private:
    unsigned num_active_sources_since_last_reset_;
    unsigned tot_num_packets_received_; // for all SSRCs
    boost::mutex lock_;
};
