/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread.hpp>
#include <dl_circle_buffer.h>
#include <dl_http_comm.curl.h>
#include <dl_smooth_streamer.h>
#include <dl_ts_aligner.h>
#include "spts_converter.h"
#include "net_reader.h"

namespace dvblex { 

class http_reader : public net_reader
{
public:
    http_reader(dvblink::engine::ts_circle_buffer* inputStream, const char* user_agent);

    bool create(const char* url, const char* local_address);
    bool start(int restart_count);
    void stop();
    bool restart_required(iptv_reader_type_e& reader_type, long& ms_timeout, int& restart_count);

private:
    void thread_function();
    bool open_connection();
    void receive_stream();

private:
    dvblink::engine::ts_circle_buffer* input_buffer_;
    dvblink::http_comm_handler* http_comm_;
    void* request_handle_;
    bool exit_flag_;
    boost::thread* receive_thread_;
    std::string url_;
    std::string user_agent_;
    iptv_reader_type_e new_reader_type_;
    bool restart_required_;
    long ms_restart_timeout_;
    int restart_count_;
    boost::posix_time::ptime stream_start_time_;

    dvblink::engine::ts_packet_aligner aligner_;
    dvblink::smooth_streamer_t smooth_streamer_;
    iptv_spts_converter_t iptv_spts_converter_;

    static void __stdcall aligner_callback(const unsigned char* buf, unsigned long len, void* user_param);
    static void smooth_streamer_callback(const unsigned char* buf, size_t len, void* user_param);
    static void iptv_spts_converter_callback(const unsigned char* buf, size_t len, void* user_param);
    void request_restart_on_http_disconnect();
};

}
