/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <stdio.h>
#include <boost/cstdint.hpp>
#ifdef WIN32
#include <winsock2.h>
#else
#include <dl_sockets.h>
#endif

class media_subsession;

class media_session
{
public:
    media_session();
    virtual ~media_session();

    double& playStartTime() { return max_play_start_time_; }
    double& playEndTime() { return max_play_end_time_; }
    std::string connectionEndpointName() const {return connection_end_point_name_;}
    struct in_addr const& sourceFilterAddr() const { return source_filter_addr_; }
    float& scale() { return scale_; }
    char* mediaSessionType() const { return media_session_type_; }
    char* sessionName() const { return session_name_; }
    char* sessionDescription() const { return session_descr_; }
    char const* controlPath() const { return control_path_; }
    bool initializeWithSDP(char const* sdpDescription);

private:
    friend class media_subsession;
    friend class media_subsession_iterator;

    std::string parseCLine(const std::string& sdpLine);
    bool parseRangeAttribute(const std::string& sdpLine, double& startTime, double& endTime);
    bool parseSourceFilterAttribute(const std::string& sdpLine, struct in_addr& sourceAddr);

    bool parseSDPLine(char const* input, char const*& nextLine);
    bool parseSDPLine_s(char const* sdpLine);
    bool parseSDPLine_i(char const* sdpLine);
    bool parseSDPLine_c(char const* sdpLine);
    bool parseSDPAttribute_type(char const* sdpLine);
    bool parseSDPAttribute_control(char const* sdpLine);
    bool parseSDPAttribute_range(char const* sdpLine);
    bool parseSDPAttribute_source_filter(char const* sdpLine);

    std::string lookupPayloadFormat(unsigned char rtpPayloadType, unsigned& rtpTimestampFrequency, unsigned& numChannels);
    unsigned guessRTPTimestampFrequency(char const* mediumName, const std::string& codecName);

private:
    // Linkage fields
    media_subsession* subsession_head_;
    media_subsession* subsession_tail_;

    // Fields set from a SDP description:
    std::string connection_end_point_name_;
    double max_play_start_time_;
    double max_play_end_time_;
    in_addr source_filter_addr_; // used for SSM
    float scale_; // set from a RTSP "Scale:" header
    char* media_session_type_; // holds a=type value
    char* session_name_; // holds s=<session name> value
    char* session_descr_; // holds i=<session description> value
    char* control_path_; // holds optional a=control: string
};

class media_subsession_iterator
{
public:
    media_subsession_iterator(media_session& session);
    virtual ~media_subsession_iterator();

    media_subsession* next(); // NULL if none
    void reset();

private:
    media_session& our_session_;
    media_subsession* next_;
};

class media_subsession
{
public:
    media_session& parentSession() { return parent_; }
    media_session const& parentSession() const { return parent_; }

    unsigned short clientPortNum() const { return client_port_num_; }
    unsigned char rtpPayloadFormat() const { return rtp_payload_format_; }
    char const* savedSDPLines() const { return saved_spd_lines_; }
    char const* mediumName() const { return medium_name_; }
    const std::string& codecName() const {return codec_name_;}
    char const* protocolName() const { return protocol_name_; }
    char const* controlPath() const { return control_path_; }

    unsigned short videoWidth() const { return video_width_; }
    unsigned short videoHeight() const { return video_height_; }
    unsigned videoFPS() const { return video_fps_; }
    unsigned numChannels() const { return num_channels_; }
    float& scale() { return scale_; }

    unsigned rtpTimestampFrequency() const { return rtp_timestamp_frequency_; }

    // Used only to set the local fields
    double& _playStartTime() { return play_start_time_; }
    double& _playEndTime() { return play_end_time_; }

    bool setClientPortNum(unsigned short portNum);
    std::string connectionEndpointName() const {return connection_endpoint_name_;}
    void set_connectionEndpointName(const std::string& name) {connection_endpoint_name_ = name;}
    //    const std::string& connectionEndpointName() const { return connection_endpoint_name_; }

    // Various parameters set in "a=fmtp:" SDP lines:
    unsigned fmtp_auxiliarydatasizelength() const { return auxiliary_data_size_length_; }
    unsigned fmtp_constantduration() const { return constant_duration_; }
    unsigned fmtp_constantsize() const { return constant_size_; }
    unsigned fmtp_crc() const { return crc_; }
    unsigned fmtp_ctsdeltalength() const { return cts_delta_length_; }
    unsigned fmtp_de_interleavebuffersize() const { return deinterleave_buffer_size_; }
    unsigned fmtp_dtsdeltalength() const { return dts_delta_length_; }
    unsigned fmtp_indexdeltalength() const { return index_delta_length_; }
    unsigned fmtp_indexlength() const { return index_length_; }
    unsigned fmtp_interleaving() const { return interleaving_; }
    unsigned fmtp_maxdisplacement() const { return max_displacement_; }
    unsigned fmtp_objecttype() const { return object_type_; }
    unsigned fmtp_octetalign() const { return octet_align_; }
    unsigned fmtp_profile_level_id() const { return profile_level_id_; }
    unsigned fmtp_robustsorting() const { return robust_sorting_; }
    unsigned fmtp_sizelength() const { return size_length_; }
    unsigned fmtp_streamstateindication() const { return stream_state_indication_; }
    unsigned fmtp_streamtype() const { return stream_type_; }
    bool fmtp_cpresent() const { return cpresent_; }
    bool fmtp_randomaccessindication() const { return random_access_indication_; }
    char const* fmtp_config() const { return config_; }
    char const* fmtp_mode() const { return mode_; }
    char const* fmtp_spropparametersets() const { return sprop_parameter_sets_; }

    unsigned int connectionEndpointAddress() const;

public:
    char const* session_id_; // used by RTSP
    unsigned short server_port_num_; // in host byte order (used by RTSP)
    unsigned char rtp_channel_id_, rtcp_channel_id_; // used by RTSP (for RTP/TCP)
    // Parameters set from a RTSP "RTP-Info:" header
    struct 
    {
        boost::uint16_t seq_num_;
        unsigned int time_stamp_;
        bool info_is_new_; // not part of the RTSP header; instead, set whenever this struct is filled in
    } rtp_info_;

private:
    friend class media_session;
    friend class media_subsession_iterator;

    media_subsession(media_session& parent);
    virtual ~media_subsession();

    void setNext(media_subsession* next) { next_ = next; }

    bool parseSDPLine_c(const std::string& sdpLine);
    bool parseSDPAttribute_rtpmap(const std::string& sdpLine);
    bool parseSDPAttribute_control(const std::string& sdpLine);
    bool parseSDPAttribute_range(const std::string& sdpLine);
    bool parseSDPAttribute_fmtp(const std::string& sdpLine);
    bool parseSDPAttribute_source_filter(const std::string& sdpLine);
    bool parseSDPAttribute_x_dimensions(const std::string& sdpLine);
    bool parseSDPAttribute_framerate(const std::string& sdpLine);

private:
    // Linkage fields
    media_session& parent_;
    media_subsession* next_;

    // Fields set from a SDP description
    std::string     connection_endpoint_name_; // may also be set by RTSP SETUP response
    unsigned short  client_port_num_; // in host byte order
    unsigned char   rtp_payload_format_;
    char*           saved_spd_lines_;
    char*           medium_name_;
    std::string     codec_name_;
    char*           protocol_name_;
    unsigned        rtp_timestamp_frequency_;
    char*           control_path_; // holds optional a=control: string
    in_addr         source_filter_addr_; // used for SSM

    // Parameters set by "a=fmtp:" SDP lines
    unsigned auxiliary_data_size_length_, constant_duration_, constant_size_;
    unsigned crc_, cts_delta_length_, deinterleave_buffer_size_, dts_delta_length_;
    unsigned index_delta_length_, index_length_, interleaving_;
    unsigned max_displacement_, object_type_;
    unsigned octet_align_, profile_level_id_, robust_sorting_;
    unsigned size_length_, stream_state_indication_, stream_type_;
    bool cpresent_, random_access_indication_;
    char* config_, *mode_, *sprop_parameter_sets_;

    double play_start_time_;
    double play_end_time_;
    unsigned short video_width_, video_height_; // screen dimensions (set by an optional a=x-dimensions: <w>,<h> line)
    unsigned video_fps_; // frame rate (set by an optional "a=framerate: <fps>" or "a=x-framerate: <fps>" line)
    unsigned num_channels_; // optionally set by "a=rtpmap:" lines for audio sessions.  Default: 1
    float scale_; // set from a RTSP "Scale:" header
};
