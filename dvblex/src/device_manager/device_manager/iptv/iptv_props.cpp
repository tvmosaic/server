/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_locale_strings.h>
#include <dl_network_helper.h>
#include <dl_language_settings.h>
#include "iptv_props.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

const std::string default_ip_address = "0.0.0.0";
const boost::uint32_t default_prebuf_time_ms = 1500;

const std::string ip_addr_param_key = "local_ip_adddress";
const std::string prebuf_time_param_key = "prebuffering_time";
const std::string xmltv_url_key = "xmltv_url";

const size_t xmltv_url_max_num = 3;

const std::string iptv_configurable_params_container_id = "f4a6762f-d0a7-497e-95b2-1c8f530d445b";

iptv_device_props_t::iptv_device_props_t()
{
    reset();
}

void iptv_device_props_t::set_props(const concise_param_map_t& params_map)
{
    params_map_ = params_map;

    reset();

    //get properties from parameters
    parameter_value_t val = get_value_for_param(ip_addr_param_key, params_map_);
    if (!val.empty())
        local_ip_ = val.get();

    val = get_value_for_param(prebuf_time_param_key, params_map_);
    if (!val.empty())
        string_conv::apply(val.c_str(), prebuffering_time_ms_, default_prebuf_time_ms);

    for (size_t i = 0; i<xmltv_url_max_num; i++)
    {
        std::stringstream strbuf;
        strbuf << xmltv_url_key << (i + 1);

        val = get_value_for_param(dvblink::parameter_key_t(strbuf.str()), params_map_);
        if (!val.empty())
            xmltv_url_list_.push_back(val.get());
    }
}

void iptv_device_props_t::get_props_as_params(configurable_device_props_t& props)
{
    //defaults
    props = configurable_device_props_t();
    //overwrite number of simul. fta streams. It should be always 1
    props.fta_concurrent_streams_ = 1;

    props.param_container_.id_ = iptv_configurable_params_container_id;
    props.param_container_.name_ = language_settings::GetInstance()->GetItemName(device_params_container_name);
    
    //ip address
    selectable_param_description_t local_addr_element;
    local_addr_element.key_ = ip_addr_param_key;
    local_addr_element.name_ = language_settings::GetInstance()->GetItemName(ip_addr_param_name);

    local_addr_element.parameters_list_.push_back(selectable_param_element_t(default_ip_address, language_settings::GetInstance()->GetItemName(ip_addr_auto_param_name)));

    TNetworkAdaptersInfo adapters;
    network_helper::get_local_net_adapters(adapters);
    for (size_t i=0; i<adapters.size(); i++)
    {
        std::string str = string_cast<EC_UTF8>(adapters[i].m_strAddress);
        std::string str_name = string_cast<EC_UTF8>(adapters[i].m_strName);
        str_name += "(" + str + ")";
        local_addr_element.parameters_list_.push_back(selectable_param_element_t(str, str_name));
        if (boost::iequals(str, local_ip_))
            local_addr_element.selected_param_ = local_addr_element.parameters_list_.size() - 1;
    }

    props.param_container_.selectable_params_.push_back(local_addr_element);

    //prebuffering time
    selectable_param_description_t prebuf_time_element;
    prebuf_time_element.key_ = prebuf_time_param_key;
    prebuf_time_element.name_ = language_settings::GetInstance()->GetItemName(prebuf_time_ms_param_name);

    for (boost::uint32_t i=0; i<5; i++)
    {
        boost::uint32_t ui = i*500;

        std::stringstream strbuf;
        strbuf << ui;

        prebuf_time_element.parameters_list_.push_back(selectable_param_element_t(strbuf.str(), strbuf.str()));
        if (ui == prebuffering_time_ms_)
            prebuf_time_element.selected_param_ = prebuf_time_element.parameters_list_.size() - 1;
    }

    props.param_container_.selectable_params_.push_back(prebuf_time_element);

    //xmltv url
    for (size_t i = 0; i<xmltv_url_max_num; i++)
    {
        std::stringstream strbuf;
        strbuf << xmltv_url_key << (i + 1);
        dvblink::parameter_key_t key(strbuf.str());

        editable_param_element_t xmltv_url_element;
        xmltv_url_element.key_ = key;

        strbuf.clear(); strbuf.str("");
        strbuf << language_settings::GetInstance()->GetItemName(xmltv_url_param_name) << " (" << (i + 1) << ")";
        xmltv_url_element.name_ = strbuf.str();
        xmltv_url_element.format_ = epf_string;
        xmltv_url_element.value_ = i < xmltv_url_list_.size() ? xmltv_url_list_.at(i).get() : "";

        props.param_container_.editable_params_.push_back(xmltv_url_element);
    }
    
}

void iptv_device_props_t::reset()
{
    local_ip_ = default_ip_address;
    prebuffering_time_ms_ = default_prebuf_time_ms;
    xmltv_url_list_.clear();
}
