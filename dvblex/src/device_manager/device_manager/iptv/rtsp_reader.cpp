/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_base64.h>
#include <dl_network_helper.h>
#include <dl_md5.h>
#include <dl_common.h>
#include "udp_reader.h"
#include "rtp_reader.h"
#include "rtsp_reader.h"
#include "utility.h"

#ifndef _WIN32
#include <errno.h>
#endif

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

#define RESP_BUFSIZE_RTSP           16384

rtsp_reader::rtsp_reader(ts_circle_buffer* input_stream, unsigned short client_port) :
    input_stream_(input_stream),
    rtsp_stream_reader_(NULL),
    rtcp_reader_(NULL),
    session_(NULL),
    m_Socket(INVALID_SOCKET),
    client_port_(client_port),
    base_url_(NULL),
    last_session_id_(NULL), 
    kasenna_content_type_(NULL),
    server_addr_(0),
    stream_using_tcp_(false),
    server_is_kasenna_(false),
    server_is_microsoft_(false), 
    response_buffer_size_(RESP_BUFSIZE_RTSP),
    cseq_(0),
    session_timeout_(0),
    tcp_stream_id_count_(0)
{
    std::stringstream buf;
    buf << "User-Agent: " << PRODUCT_NAME_UTF8 << "\r\n";
    user_agent_header_ = utility::StrDup(buf.str().c_str());
    
    response_buffer_ = new char[response_buffer_size_ + 1];

    // Get our host name, and use this for the RTCP CNAME
    strcpy(cname_, "Unknown");
    char hostName[128];
    if (gethostname(hostName, 127) == 0)
    {
        hostName[127] = '\0';
        strcpy(cname_, hostName);
    }
}

rtsp_reader::~rtsp_reader()
{
    delete [] response_buffer_;
    delete [] user_agent_header_;
}

bool rtsp_reader::create(const char* url, const char* local_address)
{
    //reset authentication string
    authentication_string_.clear();

    // Save local address	
    local_addr_ = local_address;

    return OpenConnection(url);
}

///<summary>
/// Open rtsp connection
///</summary>
bool rtsp_reader::OpenConnection(const char* url)
{
    // Open the URL, to get a SDP description
    char* sdpDescription = Describe(url, false);

    if (!sdpDescription)
    {
        log_error(L"rtsp_reader::OpenConnection() - Describe() failed");
        CloseConnection();
        return false;
    }

    // Create a media session object from this SDP description
    session_ = new media_session();

    if (!session_->initializeWithSDP(sdpDescription))
    {
        log_error(L"rtsp_reader::OpenConnection. Error initialize with SDP");
        delete[] sdpDescription;
        CloseConnection();
        return false;
    }

    delete[] sdpDescription;

    media_subsession_iterator iter(*session_);
    media_subsession *subsession;
    if ((subsession = iter.next()) != NULL) 
    {
        unsigned short clientPort = client_port_;
        if (!clientPort)
        {
            if (!GetSourcePortNum(clientPort))
            {
                log_error(L"rtsp_reader::OpenConnection. Can't get free port number");
                CloseConnection();
                return false;
            }
        }

        subsession->setClientPortNum(clientPort);
        if (!SetupMediaSubsession(*subsession)) 
        {
            CloseConnection();
            return false;
        }
    }

    if (!Play(*session_)) 
    {
        log_error(L"rtsp_reader::OpenConnection() - Play() failed");
        CloseConnection();
        return false;
    }

    return true;
}

bool rtsp_reader::start(int restart_count)
{
    media_subsession_iterator iter(*session_);
    media_subsession *subsession;

    if ((subsession = iter.next()) != NULL) 
    {
        if (strcmp(subsession->protocolName(), "UDP") == 0)
        {
            rtsp_stream_reader_ = new udp_reader(input_stream_);
        }
        else
        {
            rtsp_stream_reader_ = new rtp_reader(subsession, input_stream_);
            rtcp_reader_ = new rtcp_control((rtp_reader*)rtsp_stream_reader_, cname_, subsession->server_port_num_ + 1);
        }

        // Open RTP/UDP connection for receive IPTV stream
        if (!rtsp_stream_reader_->open_connection(local_addr_.c_str(), subsession->connectionEndpointName().c_str(), "", subsession->clientPortNum()))
        {	
            stop();
            return false;
        }

        if (!rtsp_stream_reader_->start(restart_count))
        {
            stop();
            return false;
        }

        // Open RTCP connection for control
        if (rtcp_reader_ != NULL)
        {
            if (!rtcp_reader_->open_connection(local_addr_.c_str(), subsession->connectionEndpointName().c_str(), "", subsession->clientPortNum() + 1))
            {
                stop();
                return false;
            }

            if (!rtcp_reader_->start(restart_count))
            {
                stop();
                return false;
            }
        }
    }

    return true;
}

void rtsp_reader::stop()
{
    if (rtcp_reader_ != NULL)
    {
        rtcp_reader_->stop();
        delete rtcp_reader_;
        rtcp_reader_ = NULL;
    }

    if (rtsp_stream_reader_ != NULL)
    {
        rtsp_stream_reader_->stop();
        delete rtsp_stream_reader_;
        rtsp_stream_reader_ = NULL;
    }

    // Close rtsp connection
    CloseConnection();
}

void rtsp_reader::CloseConnection()
{
    if (session_ != NULL)
    {
        Teardown(*session_);		
        delete session_;
        session_ = NULL;
    }

    if (m_Socket != INVALID_SOCKET)
    {
        closesocket(m_Socket);
        m_Socket = INVALID_SOCKET;
    }

    if (base_url_ != NULL)
    {
        delete[] base_url_; 
        base_url_ = NULL;
    }

    if (kasenna_content_type_ != NULL)
    {
        delete[] kasenna_content_type_; 
        kasenna_content_type_ = NULL;
    }

    if (last_session_id_ != NULL)
    {
        delete[] last_session_id_;
        last_session_id_ = NULL;
    }

    server_addr_ = 0;
}

///<summary>
/// Get the rtp port number (even port number)
///</summary>
unsigned short rtsp_reader::GetSourcePortNum(unsigned short& portNum)
{
    SOCKET s;
    bool retVal = false;
    std::vector<SOCKET> sockList;
    struct sockaddr_in local;
    socklen_t len = sizeof(struct sockaddr_in);

    // Get free even port
    while (1)
    {
        // Create the socket UDP
        if ((s = socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET)
        {
            break;
        }
        sockList.push_back(s);

        local.sin_family = AF_INET;
        local.sin_port = 0;
        local.sin_addr.s_addr = INADDR_ANY;  
        if (bind(s, (struct sockaddr *)&local, len) == SOCKET_ERROR)
        {
            break;
        }

        struct sockaddr_in test;
        test.sin_port = 0;
        if (getsockname(s, (struct sockaddr*)&test, &len) < 0)
        {
            break;
        }
        portNum = ntohs(test.sin_port);

        if ((portNum & 1) == 0)
        {
            retVal = true;
            break;	
        }
    }

    for (unsigned int i = 0; i < sockList.size(); i++)
    {
        closesocket(sockList[i]);
    }

    return retVal;
}

///<summary>
/// Make rtsp command OPTIONS
///</summary>
char* rtsp_reader::Options(char* url)
{
    char* result = NULL;
    char* request = NULL;
    unsigned int bytesRead, responseCode;
    char* firstLine, *nextLineStart;

    if (!OpenTCPConnection(url, 10))
    {
        return NULL;
    }

    // Send the OPTIONS command:
    const std::string cmdFmt =
        "OPTIONS %s RTSP/1.0\r\n"
        "CSeq: %d\r\n"
        "%s"
        "%s"
        "\r\n";
    unsigned cmdSize = (unsigned)strlen(cmdFmt.c_str())
        + strlen(url)
        + 20
        + strlen(user_agent_header_)
        + authentication_string_.length();
    request = new char[cmdSize];

    //AuthenticateStr_temp = utility::StrDup(m_AuthenticateStr.c_str());
    sprintf(request, cmdFmt.c_str(), url, ++cseq_, user_agent_header_, authentication_string_.c_str());
    //m_AuthenticateStr = AuthenticateStr_temp;

    if (!SendRequest(request))
    {
        delete[] request;
        return NULL;
    }
    delete[] request;

    if (!GetResponse(bytesRead, responseCode, firstLine, nextLineStart))
    {
        return NULL;
    }

    if (responseCode != 200) 
    {	
        return NULL;
    }

    // Look for a "Public:" header (which will contain our result str)
    char* lineStart;
    while (1) 
    {
        lineStart = nextLineStart;
        if (lineStart == NULL)
        {
            break;
        }

        nextLineStart = GetLine(lineStart);
        if (utility::istrncmp(lineStart, "Public: ", 8))
        {
            result = utility::StrDup(&lineStart[8]);
        } 
        else
            if (utility::istrncmp(lineStart, "RealChallenge1: ", 16))
            {

            }
    }

    return result;
}

///<summary>
/// Make rtsp command DESCRIBE
///</summary>
char* rtsp_reader::Describe(const char* url, bool allowKasennaProtocol)
{
    log_info(L"rtsp_reader::Describe() - checkpoint 1");
    unsigned int bytesRead = 0, responseCode = 0;
    char* firstLine = NULL, *nextLineStart = NULL;
    char* request = NULL;

    if (!OpenTCPConnection(url, 10))
    {
        log_error(L"rtsp_reader::Describe() - Cannot open tcp connection");
        return NULL;
    }

    char const* acceptStr = allowKasennaProtocol
        ? "Accept: application/x-rtsp-mh, application/sdp\r\n"
        : "Accept: application/sdp\r\n";

    std::string cmdFmt =
        "DESCRIBE %s RTSP/1.0\r\n"
        "CSeq: %d\r\n"
        "%s"
        "%s"
        "%s"
        "\r\n";
    
    unsigned cmdSize = cmdFmt.length()
        + strlen(url)
        + 20
        + strlen(acceptStr)
        + strlen(user_agent_header_)
        + authentication_string_.size();
    
    request = new char[cmdSize];
    
    sprintf(request, cmdFmt.c_str(),
        url,
        ++cseq_,
        acceptStr,
        user_agent_header_,
        authentication_string_.c_str());

    if (!SendRequest(request))
    {
        log_error(L"rtsp_reader::Describe(). Error send request");
        delete[] request;
        return NULL;
    }

    delete[] request;
    log_info(L"rtsp_reader::Describe() - checkpoint 2");

    if (!GetResponse(bytesRead, responseCode, firstLine, nextLineStart))
    {
        log_error(L"rtsp_reader::Describe() - Cannot get response");
        return NULL;
    }

    // Inspect the first line to check whether it's a result code that
    // we can handle.
    bool wantRedirection = false;
    char* redirectionURL = NULL;

    std::string realm;
    std::string nonce;
    bool digest_auth = false;

    if (responseCode == 401)
    {
        // Digest authentication?
        //
        const std::string& r = response_;

        /*
        try
        {
            std::wstring wr = string_cast<EC_UTF8>(r);
            log_info(L"*** IPTV RESPONSE: %1%") % wr;
        }
        catch (...)
        {
        }
        */

        // Parse the URL
        //
        std::string user;
        std::string password;
        std::string suffix;
        std::string addr;
        boost::uint16_t rtsp_port = 0;

        if (network_helper::parse_net_url(url, addr, user, password, rtsp_port, suffix) == DL_NET_PROTO_UNKNOWN)
        {
            log_error(L"rtsp_reader::Describe: invalid URL");
            return NULL;
        }

        std::string uri = "rtsp://";
        uri += addr;
        
        if (rtsp_port != 0)
        {
            uri += ":";
            uri += boost::lexical_cast<std::string>(rtsp_port);
        }

        uri += suffix;

        log_info(L"rtsp_reader::Describe. URI: %1%") % string_cast<EC_UTF8>(uri);

        // RTSP/1.0 401 Unauthorized
        // CSeq: 3
        // Date: Wed, Jul 17 2013 08:41:24 GMT
        // WWW-Authenticate: Digest realm="LIVE555 Streaming Media", nonce="d0890ad13eda61cb8b9c49995c0e2c2c"
        //
        size_t j = response_.find("WWW-Authenticate: Digest ");

        if (j == std::string::npos)
        {
            log_error(L"'WWW-Authenticate' not found");
            return NULL;
        }

        size_t eol = r.find("\r\n", j + 1);

        if (eol == std::string::npos)
        {
            log_error(L"rtsp_reader::Describe() - Invalid response");
            return NULL;
        }

        // Parse authentication string
        //
        std::string auth(r.begin() + j, r.begin() + eol);

        if (auth.find("qop=\"") != std::string::npos)
        {
            log_error(L"rtsp_reader::Describe() - QOP not supported");
            return NULL;
        }

        size_t realm_offs = auth.find("realm=\"");

        if (realm_offs == std::string::npos)
        {
            log_error(L"rtsp_reader::Describe() - 'realm' not found");
            return NULL;
        }

        size_t nonce_offs = auth.find("nonce=\"");

        if (nonce_offs == std::string::npos)
        {
            log_error(L"rtsp_reader::Describe() - 'nonce' not found");
            return NULL;
        }

        j = auth.find('\"', realm_offs + 7);

        if (j == std::string::npos)
        {
            log_error(L"rtsp_reader::Describe() - cannot get 'realm' value");
            return NULL;
        }

        realm.assign(auth.begin() + realm_offs + 7, auth.begin() + j);

        j = auth.find('\"', nonce_offs + 7);

        if (j == std::string::npos)
        {
            log_error(L"rtsp_reader::Describe() - cannot get 'nonce' value");
            return NULL;
        }

        nonce.assign(auth.begin() + nonce_offs + 7, auth.begin() + j);

        std::string s1 = user_ + ":" + realm + ":" + password_;
        std::string s2 = std::string("DESCRIBE:") + uri;

        md5 h1(s1);
        md5 h2(s2);
        
        std::string ha1 = to_string(h1());
        std::string ha2 = to_string(h2());

        std::string s3 = ha1 + ":" + nonce + ":" + ha2;
        md5 h3(s3);

        std::string resp = to_string(h3());

        authentication_string_ = "Authorization: Digest username=\"";
        authentication_string_ += user_;
        authentication_string_ += "\", realm=\"";
        authentication_string_ += realm;
        authentication_string_ += "\", nonce=\"";
        authentication_string_ += nonce;
        authentication_string_ += "\", uri=\"";
        authentication_string_ += uri;
        authentication_string_ += "\", response=\"";
        authentication_string_ += resp;
        authentication_string_ += "\"\r\n";

        std::string req = std::string("DESCRIBE ") + uri + std::string(" RTSP/1.0\r\n");
        req += std::string("CSeq: ") + boost::lexical_cast<std::string>(++cseq_) + std::string("\r\n");
        req += acceptStr;
        req += user_agent_header_;
        req += authentication_string_;
        req += "\r\n";

        try
        {
            std::wstring w1 = string_cast<EC_UTF8>(auth);
            std::wstring w2 = string_cast<EC_UTF8>(realm);
            std::wstring w3 = string_cast<EC_UTF8>(s3);
            //std::wstring w4 = string_cast<EC_UTF8>(authentication_string_);
            std::wstring w5 = string_cast<EC_UTF8>(req);

            log_info(L"auth1=%1%") % w1;
            log_info(L"realm=%1%") % w2;
            log_info(L"HA1:nonce:HA2=%1%") % w3;
            //log_info(L"auth2=%1%") % w4;
            log_info(L"request=%1%") % w5;
        }
        catch (...)
        {
        }

        if (!SendRequest((char*)req.c_str()))
        {
            log_error(L"rtsp_reader::Describe() - SendRequest() failed");
            return NULL;
        }

        if (!GetResponse(bytesRead, responseCode, firstLine, nextLineStart))
        {
            log_error(L"rtsp_reader::Describe() - GetResponse() failed");

            if (!response_.empty())
            {
                std::wstring wr = string_cast<EC_UTF8>(response_);
                log_info(L"*** IPTV RESPONSE: %1%") % wr;
            }

            return NULL;
        }

        if (responseCode != 200)
        {
            log_error(L"rtsp_reader::Describe() - responseCode=%1%") % responseCode;
            return NULL;
        }

        log_info(L"digest authentication succeeded");

        std::wstring wr = string_cast<EC_UTF8>(response_);
        log_info(L"*** IPTV RESPONSE: %1%") % wr;

        digest_auth = true;
    }
    else if (responseCode == 301 || responseCode == 302)
    {
        wantRedirection = true;
        redirectionURL = new char[RECV_BUFSIZE]; // ensures enough space
    } 
    else if (responseCode != 200)
    {
        log_error(L"rtsp_reader::Describe() - Cannot handle response (code=%1%)") % responseCode;

        if (response_buffer_)
        {
            try
            {
                std::wstring wresp = string_cast<EC_UTF8>(std::string(response_buffer_));
                log_info(L"*** IPTV RESPONSE: %1%") % wresp;
            }
            catch (...)
            {
            }
        }

        return NULL;
    }
    
    log_info(L"rtsp_reader::Describe() - checkpoint 3");

    // Skip over subsequent header lines, until we see a blank line.
    // The remaining data is assumed to be the SDP descriptor that we want.
    // While skipping over the header lines, we also check for certain headers
    // that we recognize.
    // (We should also check for "Content-type: application/sdp",
    // "Content-location", "CSeq", etc.) #####
    char* serverType = new char[RECV_BUFSIZE]; // ensures enough space
    int contentLength = -1;
    char* lineStart;
    
    while (1)
    {
        lineStart = nextLineStart;
        if (lineStart == NULL)
        {
            break;
        }

        nextLineStart = GetLine(lineStart);
        if (lineStart[0] == '\0')
        {
            break; // this is a blank line
        }

        if (sscanf(lineStart, "Content-Length: %d", &contentLength) == 1 || 
            sscanf(lineStart, "Content-length: %d", &contentLength) == 1) 
        {
            if (contentLength < 0)
            {
                break;
            }
        } 
        else if (strncmp(lineStart, "Content-Base:", 13) == 0) 
        {
            int cbIndex = 13;

            while (lineStart[cbIndex] == ' ' || lineStart[cbIndex] == '\t')
            {
                ++cbIndex;
            }

            if (lineStart[cbIndex] != '\0')
            {
                delete[] base_url_; 
                base_url_ = utility::StrDup(&lineStart[cbIndex]);
            }
        }
        else if (sscanf(lineStart, "Server: %s", serverType) == 1) 
        {
            if (strncmp(serverType, "Kasenna", 7) == 0)
            {
                server_is_kasenna_ = true;
            }
            if (strncmp(serverType, "WMServer", 8) == 0)
            {
                server_is_microsoft_ = true;
            }
        }/*
         else if (sscanf(lineStart, "ETag: %s", fRealETagStr) == 1) 
         {

         }*/
        else if (wantRedirection) 
        {
            if (sscanf(lineStart, "Location: %s", redirectionURL) == 1) 
            {
                CloseConnection();
                char* result = Describe(redirectionURL, allowKasennaProtocol);
                delete[] redirectionURL;
                delete[] serverType;

                return result;
            }
        }
    }

    log_info(L"rtsp_reader::Describe() - checkpoint 4");
    delete[] serverType;

    // We're now at the end of the response header lines
    if (wantRedirection) 
    {
        log_error(L"rtsp_reader::Describe. Saw redirection response code, but not a \"Location:\" header");
        delete[] redirectionURL;
        return NULL;
    }

    log_info(L"rtsp_reader::Describe() - checkpoint 5");

    if (lineStart == NULL) 
    {
        log_error(L"rtsp_reader::Describe. No content following header lines");
        return NULL;
    }

    if (digest_auth)
    {
        // recalculate authentication_string_ for SETUP request
        //
        if (!base_url_)
        {
            log_error(L"base_url_ is NULL");
            return NULL;
        }
        
        std::string base_url(base_url_);
        std::wstring wurl = string_cast<EC_UTF8>(base_url);
        log_info(L"base_url=%1%") % wurl;

        std::string s1 = user_ + ":" + realm + ":" + password_;
        std::string s2 = std::string("SETUP:") + base_url;

        md5 h1(s1);
        md5 h2(s2);

        std::string ha1 = to_string(h1());
        std::string ha2 = to_string(h2());

        std::string s3 = ha1 + ":" + nonce + ":" + ha2;
        md5 h3(s3);

        std::string resp = to_string(h3());

        authentication_string_ = "Authorization: Digest username=\"";
        authentication_string_ += user_;
        authentication_string_ += "\", realm=\"";
        authentication_string_ += realm;
        authentication_string_ += "\", nonce=\"";
        authentication_string_ += nonce;
        authentication_string_ += "\", uri=\"";
        authentication_string_ += base_url;
        authentication_string_ += "\", response=\"";
        authentication_string_ += resp;
        authentication_string_ += "\"\r\n";

        std::wstring wauth = string_cast<EC_UTF8>(authentication_string_);
        log_info(L"auth3=%1%") % wauth;
    }

    // Use the remaining data as the SDP descr, but first, check
    // the "Content-length:" header (if any) that we saw.  We may need to
    // read more data, or we may have extraneous data in the buffer.
    char* bodyStart = nextLineStart;
    if (contentLength >= 0) 
    {
        // We saw a "Content-length:" header
        unsigned numBodyBytes = (unsigned)(&firstLine[bytesRead] - bodyStart);
        if (contentLength > (int)numBodyBytes) 
        {
            // We need to read more data.  First, make sure we have enough
            // space for it:
            unsigned numExtraBytesNeeded = contentLength - numBodyBytes;
            unsigned remainingBufferSize = response_buffer_size_ - (bytesRead + (firstLine - response_buffer_));
            if (numExtraBytesNeeded > remainingBufferSize) 
            {	
                return NULL;
            }

            // Keep reading more data until we have enough	
            while (numExtraBytesNeeded > 0) 
            {
                char* ptr = &firstLine[bytesRead];
                int bytesRead2 = recv(m_Socket, ptr, numExtraBytesNeeded, 0);
                if (bytesRead2 < 0) 
                {
                    break;
                }
                ptr[bytesRead2] = '\0';

                bytesRead += bytesRead2;
                numExtraBytesNeeded -= bytesRead2;
            }

            if (numExtraBytesNeeded > 0) 
            {
                return NULL; // one of the reads failed
            }
        }

        // Remove any '\0' characters from inside the SDP description.
        // Any such characters would violate the SDP specification, but
        // some RTSP servers have been known to include them:
        int from, to = 0;
        for (from = 0; from < contentLength; ++from) 
        {
            if (bodyStart[from] != '\0') 
            {
                if (to != from)
                {
                    bodyStart[to] = bodyStart[from];
                }
                ++to;
            }
        }

        bodyStart[to] = '\0'; // trims any extra data
    }

    log_info(L"rtsp_reader::Describe() - checkpoint 6");

    // BEGIN Kasenna BS
    // If necessary, handle Kasenna's non-standard BS response:
    if (server_is_kasenna_ && strncmp(bodyStart, "<MediaDescription>", 18) == 0) 
    {
        // Translate from x-rtsp-mh to sdp
        int videoPid, audioPid;
        boost::uint64_t mh_duration;
        char* currentWord = new char[response_buffer_size_]; // ensures enough space
        delete[] kasenna_content_type_;
        kasenna_content_type_ = new char[response_buffer_size_]; // ensures enough space
        char* currentPos = bodyStart;

        while (strcmp(currentWord, "</MediaDescription>") != 0) 
        {
            sscanf(currentPos, "%s", currentWord);

            if (strcmp(currentWord, "VideoPid") == 0) 
            {
                currentPos += strlen(currentWord) + 1;
                sscanf(currentPos, "%s", currentWord);
                currentPos += strlen(currentWord) + 1;
                sscanf(currentPos, "%d", &videoPid);
                currentPos += 3;
            }			

            if (strcmp(currentWord, "AudioPid") == 0) 
            {
                currentPos += strlen(currentWord) + 1;
                sscanf(currentPos, "%s", currentWord);
                currentPos += strlen(currentWord) + 1;
                sscanf(currentPos, "%d", &audioPid);
                currentPos += 3;
            }

            if (strcmp(currentWord, "Duration") == 0) 
            {
                currentPos += strlen(currentWord) + 1;
                sscanf(currentPos, "%s", currentWord);
                currentPos += strlen(currentWord) + 1;
                sscanf(currentPos, "%llu", &mh_duration);
                currentPos += 3;
            }

            if (strcmp(currentWord, "TypeSpecificData") == 0) 
            {
                currentPos += strlen(currentWord) + 1;
                sscanf(currentPos, "%s", currentWord);
                currentPos += strlen(currentWord) + 1;
                sscanf(currentPos, "%s", kasenna_content_type_);
                currentPos += 3;
                printf("Kasenna Content Type: %s\n", kasenna_content_type_);
            }

            currentPos += strlen(currentWord) + 1;
        }

        if (kasenna_content_type_ != NULL && strcmp(kasenna_content_type_, "PARTNER_41_MPEG-4") == 0)
        {
            char* describeSDP = Describe(url, true);

            delete[] currentWord;
            return describeSDP;
        }

        log_info(L"rtsp_reader::Describe() - checkpoint 7");

        unsigned char byte1 =  server_addr_ & 0x000000ff;
        unsigned char byte2 = (server_addr_ & 0x0000ff00) >>  8;
        unsigned char byte3 = (server_addr_ & 0x00ff0000) >> 16;
        unsigned char byte4 = (server_addr_ & 0xff000000) >> 24;

        char const* sdpFmt =
            "v=0\r\n"
            "o=NoSpacesAllowed 1 1 IN IP4 %u.%u.%u.%u\r\n"
            "s=%s\r\n"
            "c=IN IP4 %u.%u.%u.%u\r\n"
            "t=0 0\r\n"
            "a=control:*\r\n"
            "a=range:npt=0-%llu\r\n"
            "m=video 1554 RAW/RAW/UDP 33\r\n"
            "a=control:trackID=%d\r\n";
        unsigned sdpBufSize = (unsigned)strlen(sdpFmt)
            + 4*3 // IP address
            + strlen(url)
            + 20 // max int length
            + 20; // max int length
        char* sdpBuf = new char[sdpBufSize];
        sprintf(sdpBuf, sdpFmt,
            byte1, byte2, byte3, byte4,
            url,
            byte1, byte2, byte3, byte4,
            mh_duration/1000000,
            videoPid);

        char* result = utility::StrDup(sdpBuf);
        delete[] sdpBuf; 
        delete[] currentWord;
        return result;
    }

    log_info(L"rtsp_reader::Describe() - checkpoint 8");
    return utility::StrDup(bodyStart);
}

///<summary>
/// Make rtsp command SETUP for media subsession
///</summary>
bool rtsp_reader::SetupMediaSubsession(media_subsession& subsession) 
{
    char* request = NULL;
    char* setupStr = NULL;

    if (server_is_microsoft_)
    {
        // Microsoft doesn't send the right endTime on live streams.  Correct this:
        char *tmpStr = subsession.parentSession().mediaSessionType();
        if (tmpStr != NULL && strncmp(tmpStr, "broadcast", 9) == 0) 
        {
            subsession.parentSession().playEndTime() = 0.0;
        }
    }

    // Construct the SETUP command	
    // When sending more than one "SETUP" request, include a "Session:"
    // header in the 2nd and later "SETUP"s.
    char* sessionStr;

    if (last_session_id_ != NULL) 
    {
        sessionStr = new char[20 + strlen(last_session_id_)];
        sprintf(sessionStr, "Session: %s\r\n", last_session_id_);
    } 
    else 
    {
        sessionStr = utility::StrDup("");
    }

    char* transportStr = NULL;
    char const *prefix, *separator, *suffix;
    ConstructSubsessionURL(subsession, prefix, separator, suffix);
    std::string transportFmt;

    if (strcmp(subsession.protocolName(), "UDP") == 0) 
    {
        char const* setupFmt = "SETUP %s%s RTSP/1.0\r\n";
        unsigned setupSize = (unsigned)strlen(setupFmt) + strlen(prefix) + strlen (separator);
        setupStr = new char[setupSize];
        sprintf(setupStr, setupFmt, prefix, separator);
        transportFmt = "Transport: RAW/RAW/UDP%s%s%s=%d-%d\r\n";
    } 
    else 
    {
        char const* setupFmt = "SETUP %s%s%s RTSP/1.0\r\n";
        unsigned setupSize = (unsigned)strlen(setupFmt) + strlen(prefix) + strlen (separator) + strlen(suffix);
        setupStr = new char[setupSize];
        sprintf(setupStr, setupFmt, prefix, separator, suffix);

        transportFmt = "Transport: RTP/AVP%s%s%s=%d-%d\r\n";
    }

    if (transportStr == NULL) 
    {
        // Construct a "Transport:" header.
        char const* transportTypeStr;
        char const* modeStr = "";
        // Note: I think the above is nonstandard, but DSS wants it this way
        char const* portTypeStr;
        unsigned short rtpNumber, rtcpNumber;

        if (stream_using_tcp_)
        {
            // streaming over the RTSP connection
            transportTypeStr = "/TCP;unicast";
            portTypeStr = ";interleaved";
            rtpNumber = tcp_stream_id_count_++;
            rtcpNumber = tcp_stream_id_count_++;
        }
        else
        {
            // normal RTP streaming
            unsigned connectionAddress = subsession.connectionEndpointAddress();
            //bool requestMulticastStreaming = IsMulticastAddress(connectionAddress);
            bool requestMulticastStreaming = ((connectionAddress & 0xE0) == 0xE0);
            transportTypeStr = requestMulticastStreaming ? ";multicast" : ";unicast";
            portTypeStr = ";client_port";
            rtpNumber = subsession.clientPortNum();
            if (rtpNumber == 0) 
            {
                delete[] sessionStr; 
                delete[] setupStr;
                return false;
            }
            rtcpNumber = rtpNumber + 1;
        }

        unsigned transportSize = transportFmt.length()
            + strlen(transportTypeStr) + strlen(modeStr) + strlen(portTypeStr) + 2 * 5 /* max port len */;
        transportStr = new char[transportSize];
        sprintf(transportStr, transportFmt.c_str(), transportTypeStr, modeStr, portTypeStr, rtpNumber, rtcpNumber);
    }

    // (Later implement more, as specified in the RTSP spec, sec D.1 #####)
    std::string cmdFmt =
        "%s"
        "CSeq: %d\r\n"
        "%s"
        "%s"
        "%s"
        "%s"
        "\r\n";

    unsigned cmdSize = cmdFmt.length()
        + strlen(setupStr) + 20
        + strlen(transportStr)
        + strlen(sessionStr)
        + strlen(user_agent_header_)
        + authentication_string_.size();

    request = new char[cmdSize];
    sprintf(request, cmdFmt.c_str(),
        setupStr,
        ++cseq_,
        transportStr,
        sessionStr,
        user_agent_header_,
        authentication_string_.c_str());

    delete[] sessionStr; 
    delete[] setupStr; 
    delete[] transportStr;

    std::wstring wr = string_cast<EC_UTF8>(std::string(request));
    log_info(L"*** IPTV SETUP: %1%") % wr;

    if (!SendRequest(request))
    {
        log_error(L"rtsp_reader::SetupMediaSubsession() - SendRequest() failed");
        delete[] request;
        return false;
    }

    unsigned int bytesRead, responseCode;
    char* firstLine, *nextLineStart;
    if (!GetResponse(bytesRead, responseCode, firstLine, nextLineStart))
    {
        log_error(L"rtsp_reader::SetupMediaSubsession() - GetResponse() failed (responseCode=%1%)") % responseCode;
        return false;
    }

    // Look for a "Session:" header (to set our session id), and
    // a "Transport: " header (to set the server address/port)
    // For now, ignore other headers.
    char* lineStart;
    char* sessionId = new char[response_buffer_size_]; // ensures we have enough space
    unsigned cLength = 0;
    while (1) 
    {
        lineStart = nextLineStart;
        if (lineStart == NULL)
        {
            break;
        }
        nextLineStart = GetLine(lineStart);

        if (sscanf(lineStart, "Session: %[^;]", sessionId) == 1) 
        {
            subsession.session_id_ = utility::StrDup(sessionId);
            delete[] last_session_id_; 
            last_session_id_ = utility::StrDup(sessionId);

            // Also look for an optional "; timeout = " parameter following this:
            char* afterSessionId = lineStart + strlen(sessionId) + strlen ("Session: ");

            int timeoutVal;
            if (sscanf(afterSessionId, "; timeout = %d", &timeoutVal) == 1) 
            {
                session_timeout_ = timeoutVal;
            }

            continue;
        }

        char* serverAddressStr;
        boost::uint16_t serverPortNum;
        unsigned char rtpChannelId, rtcpChannelId;
        
        if (ParseTransportResponse(lineStart, serverAddressStr, serverPortNum, rtpChannelId, rtcpChannelId)) 
        {
            if (serverAddressStr != NULL)
            {
                subsession.set_connectionEndpointName(serverAddressStr);
            } else
            {
                //use original url
                subsession.set_connectionEndpointName(server_host_name_);
            }
            subsession.server_port_num_ = serverPortNum;
            subsession.rtp_channel_id_ = rtpChannelId;
            subsession.rtcp_channel_id_ = rtcpChannelId;
            continue;
        }

        // Also check for a "Content-Length:" header.  Some weird servers include this
        // in the RTSP "SETUP" response.
        if (sscanf(lineStart, "Content-Length: %d", &cLength) == 1) 
        {
            continue;
        }
    }
    delete[] sessionId;

    if (subsession.session_id_ == NULL) 
    {
        log_error(L"rtsp_reader::SetupMediaSubsession. Session: header is missing in the response");
        return false;
    }

    // If we saw a "Content-Length:" header in the response, then discard whatever
    // included data it refers to:
    if (cLength > 0) 
    {
        char* dummyBuf = new char[cLength];
        ReadBuffer(dummyBuf, cLength);
        delete[] dummyBuf;
    }

    // Normal case.
    // Set the RTP and RTCP sockets' destination address and port
    // from the information in the SETUP response (if present):
    unsigned int destAddress = subsession.connectionEndpointAddress();
    
    if (destAddress == 0)
    {
        subsession.connectionEndpointName() = utility::StrDup(server_host_name_.c_str());
    }

    return true;
}

///<summary>
/// Make rtsp command PLAY
///</summary>
bool rtsp_reader::Play(media_session& session) 
{
    char* request = NULL;

    // First, make sure that we have a RTSP session in progress
    if (last_session_id_ == NULL) 
    {
        log_error(L"rtsp_reader::Play. Error no session");
        return false;
    }

    // Send the PLAY command
    std::string cmdFmt =
        "PLAY %s RTSP/1.0\r\n"
        "CSeq: %d\r\n"
        "Session: %s\r\n"
        "%s"
        "%s"
        "\r\n";

    char const* sessURL = SessionURL(session);
    unsigned cmdSize = cmdFmt.length()
        + strlen(sessURL)
        + 20
        + strlen(last_session_id_)
        + strlen(user_agent_header_)
        + authentication_string_.size();

    request = new char[cmdSize];
    sprintf(request, cmdFmt.c_str(),
        sessURL,
        ++cseq_,
        last_session_id_,
        user_agent_header_,
        authentication_string_.c_str());

    if (!SendRequest(request))
    {
        log_error(L"rtsp_reader::Play() - SendRequest() failed");
        delete[] request;
        return false;
    }

    delete[] request;

    unsigned int bytesRead, responseCode;
    char* firstLine, *nextLineStart;

    if (!GetResponse(bytesRead, responseCode, firstLine, nextLineStart))
    {
        log_error(L"rtsp_reader::Play() - GetResponse() failed (responseCode=%1%)") % responseCode;
        return false;
    }

    // Look for various headers that we understand:
    char* lineStart;
    while (1) 
    {
        lineStart = nextLineStart;
        if (lineStart == NULL)
        {
            break;
        }

        nextLineStart = GetLine(lineStart);

        if (ParseScaleHeader(lineStart, session.scale()))
        {
            continue;
        }

        if (ParseRangeHeader(lineStart, session.playStartTime(), session.playEndTime()))
        {
            continue;
        }

        boost::uint16_t seqNum;
        unsigned int timestamp;
        if (ParseRTPInfoHeader(lineStart, seqNum, timestamp)) 
        {
            // This is data for our first subsession.  Fill it in, and do the same for our other subsessions:
            media_subsession_iterator iter(session);
            media_subsession* subsession;
            while ((subsession = iter.next()) != NULL) 
            {
                subsession->rtp_info_.seq_num_ = seqNum;
                subsession->rtp_info_.time_stamp_ = timestamp;
                subsession->rtp_info_.info_is_new_ = true;

                if (!ParseRTPInfoHeader(lineStart, seqNum, timestamp)) 
                {
                    break;
                }
            }
            continue;
        }
    }

    return true;
} 

///<summary>
/// Make rtsp command TEARDOWN
///</summary>
bool rtsp_reader::Teardown(media_session& session) 
{
    char* request = NULL;

    // First, make sure that we have a RTSP session in progress
    if (last_session_id_ == NULL) 
    {
        log_error(L"rtsp_reader::Teardown. Error no session");
        return false;
    }

    // Send the TEARDOWN command
    char const* sessURL = SessionURL(session);
    std::string cmdFmt =
        "TEARDOWN %s RTSP/1.0\r\n"
        "CSeq: %d\r\n"
        "Session: %s\r\n"
        "%s"
        "%s"
        "\r\n";

    unsigned cmdSize = cmdFmt.length()
        + strlen(sessURL)
        + 20
        + strlen(last_session_id_)
        + strlen(user_agent_header_)
        + authentication_string_.size();

    request = new char[cmdSize];
    sprintf(request, cmdFmt.c_str(),
        sessURL,
        ++cseq_,
        last_session_id_,
        user_agent_header_,
        authentication_string_.c_str());

    if (!SendRequest(request))
    {
        log_error(L"rtsp_reader::Teardown. Error send request");
        delete[] request;
        return false;
    }
    delete[] request;

    unsigned int bytesRead, responseCode;
    char* firstLine, *nextLineStart;
    if (!GetResponse(bytesRead, responseCode, firstLine, nextLineStart))
    {
        log_error(L"rtsp_reader::Teardown. Error get response");
        return false;
    }

    // Run through each subsession, deleting its "sessionId"
    media_subsession_iterator iter(session);
    media_subsession* subsession;
    while ((subsession = iter.next()) != NULL) 
    {
        delete[] (char*)subsession->session_id_;
        subsession->session_id_ = NULL;
    }

    delete[] last_session_id_; 
    last_session_id_ = NULL;

    return true;
}

bool rtsp_reader::OpenTCPConnection(char const* url, int timeout)
{
    if (!url || (m_Socket != INVALID_SOCKET) || (timeout <= 0))
    {
        log_error(L"OpenTCPConnection(): invalid parameter");
        return false;
    }

    // Set this as our base URL:
    delete[] base_url_; 
    base_url_ = utility::StrDup(url);
    
    if (!base_url_)
    {
        log_error(L"OpenTCPConnection(): base_url_ is NULL");
        return false;
    }

    // Parse multibyte URL
    std::string url_suffix;
    std::string remoteAddress;
    boost::uint16_t rtspPort = 0;

    log_info(L"OpenTCPConnection(): url=%1%") % string_cast<EC_UTF8>(url);

    if (network_helper::parse_net_url(url, remoteAddress,
        user_, password_, rtspPort, url_suffix) == DL_NET_PROTO_UNKNOWN)
    {
        log_error(L"OpenTCPConnection(): parse_net_url failed");
        return false;
    }

    if (rtspPort == 0)
    {
        rtspPort = 554;
    }

    log_info(L"OpenTCPConnection(): addr=%1%:%2%") % string_cast<EC_UTF8>(remoteAddress) % rtspPort;
    server_host_name_ = remoteAddress;
    
    if (user_.size() != 0 && password_.size() != 0)
    {
        std::string auth_buf = user_;
        auth_buf += ":";
        auth_buf += password_;

        base64_encode<std::string>(auth_buf, authentication_string_);
        
        authentication_string_ = "Authorization: Basic " + authentication_string_ + "\r\n";
    }

    // Create the TCP socket
    if ((m_Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET)
    {
        log_error(L"OpenTCPConnection(): cannot open TCP socket");
        return false;
    }

    struct sockaddr_in serverAddr; 
    serverAddr.sin_family = AF_INET; 
    serverAddr.sin_port = htons(rtspPort);
    struct hostent *lphost = gethostbyname(server_host_name_.c_str()); 
    
    if (!lphost)
    {
        log_error(L"OpenTCPConnection(): gethostbyname failed");
        closesocket(m_Socket);
        return false;
    }

    fd_set set;
    FD_ZERO(&set);
    timeval tvout = {0,0};
    
    if (timeout > 0) 
    {
        FD_SET((unsigned)m_Socket, &set);
        tvout.tv_sec = timeout;
        tvout.tv_usec = 0;
        
        // Non blocking mode
    #ifdef _WIN32
        unsigned long arg = 1;
        ioctlsocket(m_Socket, FIONBIO, &arg);
    #else
        int flags = fcntl(m_Socket, F_GETFL); // get flags
        flags |= O_NONBLOCK;        
        fcntl(m_Socket, F_SETFL, flags); // set flags
    #endif
    }

    unsigned long ulLocalAddress = INADDR_ANY;
    
    if (!local_addr_.empty())
    {
        ulLocalAddress = inet_addr(local_addr_.c_str());
        
        // If it wasn't one of those, do gethostbyname
        if (ulLocalAddress == INADDR_NONE)
        {
            LPHOSTENT lphost = NULL;
            lphost = gethostbyname(local_addr_.c_str());

            if (lphost == NULL) 
            { 
                log_error(L"OpenTCPConnection(): gethostbyname failed (2)");
                closesocket(m_Socket);
                return false;
            }

            ulLocalAddress = ((LPIN_ADDR)lphost->h_addr)->s_addr;
        }
    }

    // Bind to the local interface
    struct sockaddr_in local;
    local.sin_family = AF_INET;
    local.sin_port = 0;
    local.sin_addr.s_addr = ulLocalAddress;    
    
    if (bind(m_Socket, (struct sockaddr *)&local, sizeof(local)) == SOCKET_ERROR)
    {
        log_error(L"OpenTCPConnection(): bind failed");
        return false;
    }

    serverAddr.sin_addr.s_addr = server_addr_ = ((LPIN_ADDR)lphost->h_addr)->s_addr;
    
    if ((connect(m_Socket, (struct sockaddr *)&serverAddr, sizeof(struct sockaddr_in))) == INVALID_SOCKET)
    { 
    #ifdef _WIN32
        int last_err = WSAGetLastError();

        if (WSAEWOULDBLOCK != last_err) 
        {
            log_error(L"OpenTCPConnection(): connect failed (%1%)") % last_err;
            closesocket(m_Socket);
            return false;
        }
    #else
        if ((errno != EINPROGRESS) && (errno != EALREADY))
        {
            log_error(L"OpenTCPConnection(): connect failed (%1%)") % errno;
            closesocket(m_Socket);
            return false;
        }
    #endif

        if (select((int)(m_Socket + 1), NULL, &set, NULL, &tvout) <= 0)
        {
            log_error(L"OpenTCPConnection(): timeout expired");
            closesocket(m_Socket);
            return false;
        }
    }

    // Blocking mode
    #ifdef _WIN32
        unsigned long arg = 0;
        ioctlsocket(m_Socket, FIONBIO, &arg);
    #else
        int flags = fcntl(m_Socket, F_GETFL); // get flags
        flags &= ~O_NONBLOCK;        
        fcntl(m_Socket, F_SETFL, flags); // set flags
    #endif

    int to = 10000;
    setsockopt(m_Socket, SOL_SOCKET, SO_RCVTIMEO, (char*)&to, sizeof(int));

    return true;
}

bool rtsp_reader::SendRequest(char* request)
{
    bool retVal = true;

    if (send(m_Socket, request, (int)strlen(request), 0) == SOCKET_ERROR)
    {
        retVal = false;
    }

    return retVal;
}

bool rtsp_reader::GetResponse(unsigned int& bytesRead,
    unsigned int& responseCode, 
    char*& firstLine, char*& nextLineStart)
{
    bool retVal = true;
    bytesRead = ReadBuffer(response_buffer_, RECV_BUFSIZE);

    if (bytesRead != 0 && bytesRead != static_cast<unsigned int>(SOCKET_ERROR))
    {
        response_ = response_buffer_;
        firstLine = response_buffer_;
        nextLineStart = GetLine(firstLine);

        log_info(L"rtsp_reader::GetResponse. Response: %1%") % string_cast<EC_UTF8>(response_);

        if (!ParseResponseCode(firstLine, responseCode))
        {
            retVal = false;
        }
    }
    else
    {
        response_.clear();
        retVal = false;
    }

    return retVal;
}

unsigned rtsp_reader::ReadBuffer(char*& responseBuffer, unsigned responseBufferSize) 
{
    if (responseBufferSize == 0)
    {
        return 0;
    }

    responseBuffer[0] = '\0';

    // Begin by reading and checking the first byte of the response.
    // If it's '$', then there's an interleaved RTP (or RTCP)-over-TCP
    // packet here.  We need to read and discard it first.
    bool success = false;
    while (1) 
    {
        char firstByte;
        if (recv(m_Socket, &firstByte, 1, 0) != 1)
        {
            break;
        }

        if (firstByte != '$') 
        {
            // Normal case: This is the start of a regular response; use it:
            responseBuffer[0] = firstByte;
            success = true;
            break;
        }
        else 
        {
            // This is an interleaved packet; read and discard it:
            char streamChannelId;
            if (recv(m_Socket, &streamChannelId, 1, 0) != 1)
            {
                break;
            }

            unsigned short size;
            if (ReadBufferExact((char*)&size, 2) != 2)
            {
                break;
            }

            size = ntohs(size);
            //cout << "Discarding interleaved RTP or RTCP packet (" << size << " bytes, channel id " 
            //	<< streamChannelId << ")\n";

            char* tmpBuffer = new char[size];
            if (ReadBufferExact((char*)&tmpBuffer, size) != size)
            {
                delete[] tmpBuffer;
                break;
            }
            delete[] tmpBuffer;

            success = true;
        }
    }

    if (!success)
    {
        return 0;
    }

    // Keep reading data from the socket until we see "\r\n\r\n" (except
    // at the start), or until we fill up our buffer.
    // Don't read any more than this.
    char* p = responseBuffer;
    bool haveSeenNonCRLF = false;
    int bytesRead = 1; // because we've already read the first byte
    while (bytesRead < (int)responseBufferSize) 
    {
        int bytesReadNow = recv(m_Socket, (char*)(responseBuffer + bytesRead), 1, 0);
        if (bytesReadNow <= 0) 
        {
            //cout << "RTSP response was truncated\n";
            break;
        }
        bytesRead += bytesReadNow;

        // Check whether we have "\r\n\r\n" (or "\r\r" or "\n\n"):
        char* lastToCheck = responseBuffer + bytesRead - 4;
        if (lastToCheck < responseBuffer)
        {
            continue;
        }

        for (; p <= lastToCheck; ++p) 
        {
            if (haveSeenNonCRLF) 
            {
                if ((*p == '\r' && *(p + 1) == '\n' && *(p + 2) == '\r' && *(p + 3) == '\n')
                    || (*(p + 2) == '\r' && *(p + 3) == '\r')
                    || (*(p + 2) == '\n' && *(p + 3) == '\n')) 
                {
                    responseBuffer[bytesRead] = '\0';

                    // Before returning, trim any \r or \n from the start:
                    while (*responseBuffer == '\r' || *responseBuffer == '\n') 
                    {
                        ++responseBuffer;
                        --bytesRead;
                    }
                    return bytesRead;
                }
            } 
            else 
            {
                if (*p != '\r' && *p != '\n') 
                {
                    haveSeenNonCRLF = true;
                }
            }
        }
    }
    //cout << "We received a response not ending with <CR><LF><CR><LF>\n";

    return 0;
}

unsigned int rtsp_reader::ReadBufferExact(char* buffer, int bufferSize)
{
    int bytesRead = 0;
    int curBytesRead = 0;
    do 
    {
        if ((curBytesRead = recv(m_Socket, buffer + bytesRead, bufferSize, 0)) <= 0)
        {
            break;
        }

        bytesRead += curBytesRead;		
        bufferSize -= curBytesRead;

    } while (bufferSize != 0);

    return bytesRead;
}

char* rtsp_reader::GetLine(char* startOfLine)
{
    // returns the start of the next line, or NULL if none
    for (char* ptr = startOfLine; *ptr != '\0'; ++ptr) 
    {
        // Check for the end of line: \r\n (but also accept \r or \n by itself):
        if (*ptr == '\r' || *ptr == '\n') 
        {
            // We found the end of the line
            if (*ptr == '\r') 
            {
                *ptr++ = '\0';
                if (*ptr == '\n') 
                {
                    ++ptr;
                }
            } 
            else 
            {
                *ptr++ = '\0';
            }

            return ptr;
        }
    }

    return NULL;
}

bool rtsp_reader::ParseResponseCode(char const* line, unsigned int& responseCode)
{	
    bool retVal = true;

    if (sscanf(line, "%*s%u", &responseCode) != 1) 
    {
        retVal = false;
    }

    return retVal;
}

void rtsp_reader::ConstructSubsessionURL(media_subsession const& subsession, 
                                         char const*& prefix, 
                                         char const*& separator, char const*& suffix)
{
    // Figure out what the URL describing "subsession" will look like.
    // The URL is returned in three parts: prefix; separator; suffix
    //##### NOTE: This code doesn't really do the right thing if "sessionURL()"
    // doesn't end with a "/", and "subsession.controlPath()" is relative.
    // The right thing would have been to truncate "sessionURL()" back to the
    // rightmost "/", and then add "subsession.controlPath()".
    // In practice, though, each "DESCRIBE" response typically contains
    // a "Content-Base:" header that consists of "sessionURL()" followed by
    // a "/", in which case this code ends up giving the correct result.
    // However, we should really fix this code to do the right thing, and
    // also check for and use the "Content-Base:" header appropriately. #####
    prefix = SessionURL(subsession.parentSession());
    if (prefix == NULL)
    {
        prefix = "";
    }

    suffix = subsession.controlPath();
    if (suffix == NULL)
    {
        suffix = "";
    }

    if (IsAbsoluteURL(suffix)) 
    {
        prefix = separator = "";
    }
    else 
    {
        unsigned prefixLen = (unsigned)strlen(prefix);
        separator = (prefix[prefixLen-1] == '/' || suffix[0] == '/') ? "" : "/";
    }
}

char const* rtsp_reader::SessionURL(media_session const& session)
{
    char const* url = session.controlPath();
    if (url == NULL || strcmp(url, "*") == 0) 
    {
        url = base_url_;
    }

    return url;
}

bool rtsp_reader::IsAbsoluteURL(char const* url) 
{
    // Assumption: "url" is absolute if it contains a ':', before any
    // occurrence of '/'
    while (*url != '\0' && *url != '/')
    {
        if (*url == ':')
        {
            return true;
        }
        ++url;
    }

    return false;
}

bool rtsp_reader::ParseTransportResponse(char const* line, char*& serverAddressStr,
    boost::uint16_t& serverPortNum, unsigned char& rtpChannelId,
    unsigned char& rtcpChannelId)
{
    // Initialize the return parameters to 'not found' values:
    serverAddressStr = NULL;
    serverPortNum = 0;
    rtpChannelId = rtcpChannelId = 0xFF;

    char* foundServerAddressStr = NULL;
    bool foundServerPortNum = false;
    bool foundChannelIds = false;
    unsigned rtpCid, rtcpCid;
    bool isMulticast = true; // by default
    char* foundDestinationStr = NULL;
    boost::uint16_t multicastPortNumRTP, multicastPortNumRTCP;
    bool foundMulticastPortNum = false;

    // First, check for "Transport:"
    if (!utility::istrncmp(line, "Transport: ", 11))
    {
        return false;
    }
    line += 11;

    // Then, run through each of the fields, looking for ones we handle:
    char const* fields = line;
    char* field = utility::StrDupSize(fields);
    while (sscanf(fields, "%[^;]", field) == 1) 
    {
        if (sscanf(field, "server_port=%hu", &serverPortNum) == 1) 
        {
            foundServerPortNum = true;
        } 
        else if (utility::istrncmp(field, "source=", 7))
        {
            delete[] foundServerAddressStr;
            foundServerAddressStr = utility::StrDup(field + 7);
        } 
        else if (sscanf(field, "interleaved=%u-%u", &rtpCid, &rtcpCid) == 2) 
        {
            rtpChannelId = (unsigned char)rtpCid;
            rtcpChannelId = (unsigned char)rtcpCid;
            foundChannelIds = true;
        }
        else if (strcmp(field, "unicast") == 0) 
        {
            isMulticast = false;
        }
        else if (utility::istrncmp(field, "destination=", 12))
        {
            delete[] foundDestinationStr;
            foundDestinationStr = utility::StrDup(field + 12);
        }
        else if (sscanf(field, "port=%hu-%hu", &multicastPortNumRTP, &multicastPortNumRTCP) == 2) 
        {
            foundMulticastPortNum = true;
        }

        fields += strlen(field);
        while (fields[0] == ';')
        {
            ++fields; // skip over all leading ';' chars
        }
        if (fields[0] == '\0') 
        {
            break;
        }
    }
    delete[] field;

    // If we're multicast, and have a "destination=" (multicast) address, then use this
    // as the 'server' address (because some weird servers don't specify the multicast
    // address earlier, in the "DESCRIBE" response's SDP:
    if (isMulticast && foundDestinationStr != NULL && foundMulticastPortNum) 
    {
        delete[] foundServerAddressStr;
        serverAddressStr = foundDestinationStr;
        serverPortNum = multicastPortNumRTP;
        return true;
    }
    delete[] foundDestinationStr;

    if (foundServerPortNum || foundChannelIds) 
    {
        serverAddressStr = foundServerAddressStr;
        return true;
    }

    delete[] foundServerAddressStr;

    return false;
}

bool rtsp_reader::ParseRTPInfoHeader(char*& line, boost::uint16_t& seqNum, unsigned int& timestamp)
{
    // At this point in the parsing, "line" should begin with either "RTP-Info: " (for the start of the header),
    // or ",", indicating the RTP-Info parameter list for the 2nd-through-nth subsessions:
    if (utility::istrncmp(line, "RTP-Info: ", 10))
    {
        line += 10;
    } 
    else if (line[0] == ',') 
    {
        ++line;
    } 
    else 
    {
        return false;
    }

    // "line" now consists of a ';'-separated list of parameters, ending with ',' or '\0'.
    char* field = utility::StrDupSize(line);

    while (sscanf(line, "%[^;,]", field) == 1) 
    {
        if (sscanf(field, "seq=%hu", &seqNum) == 1 || sscanf(field, "rtptime=%u", &timestamp) == 1)
        {
        }

        line += strlen(field);
        if (line[0] == '\0' || line[0] == ',')
        {
            break;
        }
        // ASSERT: line[0] == ';'
        ++line; // skip over the ';'
    }

    delete[] field;

    return true;
} 

bool rtsp_reader::ParseRangeHeader(char const* buf, double& rangeStart, double& rangeEnd) 
{
    // First, find "Range:"
    while (1) 
    {
        if (*buf == '\0') return false; // not found
        if (utility::istrncmp(buf, "Range: ", 7))
        {
            break;
        }
        ++buf;
    }

    // Then, run through each of the fields, looking for ones we handle:
    char const* fields = buf + 7;
    while (*fields == ' ')
    {
        ++fields;
    }

    double start, end;
    setlocale(LC_NUMERIC, "C"); 
    if (sscanf(fields, "npt = %lf - %lf", &start, &end) == 2) 
    {
        rangeStart = start;
        rangeEnd = end;
    } 
    else if (sscanf(fields, "npt = %lf -", &start) == 1) 
    {
        rangeStart = start;
        rangeEnd = 0.0;
    } 
    else 
    {
        return false; // The header is malformed
    }

    return true;
} 

bool rtsp_reader::ParseScaleHeader(char const* line, float& scale)
{
    if (!utility::istrncmp(line, "Scale: ", 7))
    {
        return false;
    }

    line += 7;
    setlocale(LC_NUMERIC, "C");
    return sscanf(line, "%f", &scale) == 1;
}
