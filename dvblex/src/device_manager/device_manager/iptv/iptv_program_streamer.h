/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <boost/thread.hpp>
#include <dl_circle_buffer.h>
#include <dl_ts_aligner.h>
#include "../program_streamer.h"
#include "iptv_props.h"
#include "net_reader.h"

namespace dvblex { 

class iptv_program_streamer_t : public program_streamer_t
{
public:
    iptv_program_streamer_t(iptv_device_props_t* iptv_device_props, const dvblink::channel_id_t& channel_id, 
        const concise_channel_tune_info_t& tune_params);
    virtual ~iptv_program_streamer_t();

    virtual bool start();
    virtual void stop();

    virtual bool get_signal_info(signal_info_t& si);

protected:
    iptv_device_props_t* iptv_device_props_;
    std::string url_;
    bool streaming_started_;
    boost::thread* streaming_thread_;
    dvblink::engine::ts_circle_buffer circle_buffer_;
    dvblink::engine::ts_packet_aligner aligner_;

    net_reader* get_stream_reader(const std::string& url, iptv_reader_type_e reader_type);
    void process_stream(size_t len, const unsigned char *data);
    static void __stdcall aligner_callback(const unsigned char* buf, unsigned long len, void* user_param);
    void streaming_thread();
    iptv_reader_type_e get_reader_type_from_url(const std::string& url);
    bool iptv_streamer_function(iptv_reader_type_e& reader_type, long& ms_timeout, int& restart_count);
};

}
