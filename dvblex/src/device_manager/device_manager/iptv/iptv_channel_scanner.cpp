/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/filesystem.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_filesystem_path.h>
#include <dl_language_settings.h>
#include <dl_locale_strings.h>
#include "iptv_channel_scanner.h"
#include "iptv_provider.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

static const std::string no_channel_group_transponder_id = "4dc897bc-849e-4fc0-b1ee-3c16a57ba277";

iptv_channel_scanner_t::iptv_channel_scanner_t() :
    channel_scanner_t()
{
}

iptv_channel_scanner_t::~iptv_channel_scanner_t()
{
}

bool iptv_channel_scanner_t::start()
{
    return true;
}

void iptv_channel_scanner_t::stop()
{
}

bool iptv_channel_scanner_t::do_scan(const std::string& tuning_params, const dvblink::scan_network_id_t& network_id, 
                                     transponder_list_t& found_channels, scan_log_entry_t& log_entry)
{
    bool ret_val = false;

    log_entry.reset();

    std::string log_str;
    playlist_channel_map_t channel_map;
    iptv_provider_t iptv_provider;
    if (iptv_provider.scan_channels(scanner_settings_, channel_map, log_str))
    {
        log_entry.scan_data_ = log_str;
        //extract all channels groups from playlist - they will become transponders
        std::map<dvblink::transponder_id_t, transponder_t> transponder_map;

        playlist_channel_map_t::iterator it = channel_map.begin();
        while (it != channel_map.end())
        {
            playlist_channel& ch = it->second;

            //skip VOD channels
            if (!ch.is_vod)
            {
                if (ch.tags.find(m3u_plus_group_title) != ch.tags.end())
                {
                    dvblink::transponder_id_t trid(ch.tags[m3u_plus_group_title]);
                    if (transponder_map.find(trid) == transponder_map.end())
                    {
                        transponder_map[trid] = transponder_t();
                        transponder_map[trid].tr_id_ = trid;
                        transponder_map[trid].tr_name_ = trid.get();
                    }
                }
            }

            ++it;
        }

        //add special transponder for chanenls without group 
        transponder_t no_channel_group_tr;
        no_channel_group_tr.tr_id_ = no_channel_group_transponder_id;
        no_channel_group_tr.tr_name_ = language_settings::GetInstance()->GetItemName(iptv_no_group_transponder_name);

        int vod_counter = 0;

        it = channel_map.begin();
        while (it != channel_map.end())
        {
            playlist_channel& ch = it->second;

            //skip VOD channels
            if (!ch.is_vod)
            {
                dvblex::device_channel_t channel;
                channel.id_ = ch.get_channel_id();
                channel.name_ = ch.get_channel_name();
                channel.num_ = ch.number;
                channel.sub_num_ = ch.subnumber;
                channel.type_ = ch.get_channel_type();

                //check for tag tvg-logo
                if (ch.tags.find(m3u_plus_tvg_logo) != ch.tags.end())
                    channel.logo_ = ch.tags[m3u_plus_tvg_logo];

                //set origin to group-title
                if (ch.tags.find(m3u_plus_group_title) != ch.tags.end())
                    channel.origin_ = ch.tags[m3u_plus_group_title];

                channel.tune_params_ = ch.url;

                if (ch.tags.find(m3u_plus_group_title) != ch.tags.end())
                {
                    dvblink::transponder_id_t trid(ch.tags[m3u_plus_group_title]);
                    transponder_map[trid].channels_.push_back(channel);
                } else
                {
                    no_channel_group_tr.channels_.push_back(channel);
                }
            } else
            {
                vod_counter += 1;
            }

            ++it;
        }

        if (vod_counter > 0)
            log_info(L"iptv_channel_scanner_t::do_scan. Skipped %1% VOD channels") % vod_counter;

        std::map<dvblink::transponder_id_t, transponder_t>::iterator trit = transponder_map.begin();
        while (trit != transponder_map.end())
        {
            found_channels.push_back(trit->second);
            ++trit;
        }
        if (no_channel_group_tr.channels_.size() > 0)
        {
            //rename group if there are no other channel groups
            if (found_channels.size() == 0)
                no_channel_group_tr.tr_name_ = language_settings::GetInstance()->GetItemName(iptv_all_channes_transponder_name);

            found_channels.push_back(no_channel_group_tr);
        }

        log_entry.signal_info_.lock_ = true;
        log_entry.signal_info_.signal_quality_ = 100;
        log_entry.signal_info_.signal_strength_ = 100;
        log_entry.channels_found_ = channel_map.size();
        //do not report channel names for iptv source. Only number of channels
//        log_entry.add_comment(channel.name_.get());

        ret_val = true;

    }

    return ret_val;
}
