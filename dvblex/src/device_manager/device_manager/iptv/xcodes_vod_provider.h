/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <map>

#include "iptv_vod_provider.h"
#include "xtream_codes.h"

namespace dvblex { 

class xcodes_vod_provider_t : public iptv_vod_provider_t
{
public:
    xcodes_vod_provider_t(const xtream_codes_info_t& xci);
    virtual ~xcodes_vod_provider_t();

protected:
    virtual void read_containers();
    virtual void read_items(const std::string& group_id);
    virtual void add_item_metadata(const std::string& group_id, const std::string& item_id, vod_item_obj_t& item);

    xtream_codes_info_t xci_;
    xtream_codes_scanner_t::xtream_codes_series_map_t xtream_codes_series_map_;
};

}
