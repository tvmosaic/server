/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <vector>
#include <string>

#include <dl_parameters.h>
#include <dl_device_info.h>

namespace dvblex { 

class iptv_device_props_t
{
public:
    typedef std::vector<dvblink::url_address_t> xmltv_url_list_t;

public:
    iptv_device_props_t();

    void set_props(const concise_param_map_t& params_map);
    void get_props_as_params(configurable_device_props_t& props);

    boost::uint32_t get_prebuffering_time_ms(){return prebuffering_time_ms_;}
    std::string get_local_ip(){return local_ip_;}
    xmltv_url_list_t get_xmltv_url() {return xmltv_url_list_;}

protected:
    concise_param_map_t params_map_;
    boost::uint32_t prebuffering_time_ms_;
    std::string local_ip_;
    xmltv_url_list_t xmltv_url_list_;

    void reset();
};

}
