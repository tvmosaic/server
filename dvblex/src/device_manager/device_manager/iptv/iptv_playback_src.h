/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>
#include <sstream>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <dl_pb_video.h>
#include <device_manager/device_playback_src.h>
#include "iptv_vod_provider.h"

namespace dvblex { 

class iptv_vod_provider_t;

class iptv_playback_src_t : public device_playback_src_t
{
    typedef boost::shared_ptr<playback::pb_video_t> pb_video_obj_t;

public:
    iptv_playback_src_t();
    virtual ~iptv_playback_src_t();

protected:
    iptv_vod_provider_t* iptv_vod_provider_;

    virtual void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_objects_request& request, dvblink::messaging::playback::get_objects_response& response);
    virtual void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_source_container_request& request, dvblink::messaging::playback::get_source_container_response& response);
    virtual void handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::search_objects_request& request, dvblink::messaging::playback::search_objects_response& response);
    virtual std::string get_source_name();
    virtual void source_init();
    virtual void source_term();

    void add_source_to_container_list(dvblex::playback::pb_container_list_t& container_list);

    pb_video_obj_t video_obj_from_vod_item(const dvblink::object_id_t& parent_id, const dvblink::object_id_t& object_id, const vod_item_obj_t& vod_item);
    playback::pb_container_t pb_container_from_vod_container(const dvblink::object_id_t& parent_id, const dvblink::object_id_t& object_id, const vod_container_t& vod_cnt);
};

}
