/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

enum EIPTV_H264_UNIT_TYPE
{
    H264_UT_FU_A = 28
};

struct H264_PAYLOAD_HEADER
{
    // 1 byte
    unsigned char nal_unit_type         :5; // 5 bit nal unit type
    unsigned char nal_ref_idc           :2; // 2 bit nal_ref_idc
    unsigned char forbidden_zero_bit    :1; // 0 - forbidden zero bit
};

struct H264_FU_HEADER
{
    // 1 byte
    unsigned char nal_unit_type         :5; // 5 bit nal unit type
    unsigned char reserved              :1; // 
    unsigned char end_bit               :1; // 
    unsigned char start_bit             :1; // 
};
