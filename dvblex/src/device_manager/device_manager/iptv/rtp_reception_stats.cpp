/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include "rtp_reception_stats.h"
#include "utility.h"

#define MILLION                     1000000

rtp_reception_stats_db::rtp_reception_stats_db() :
    tot_num_packets_received_(0) 
{
    Reset();
}

rtp_reception_stats_db::~rtp_reception_stats_db() 
{
    rtp_reception_stats* stats;

    TRTPReceptionStats::iterator it = reception_stats_.begin();
    while (it != reception_stats_.end()) 
    {
        stats = it->second;
        delete stats;

        it++;
    }
}

void rtp_reception_stats_db::Reset() 
{
    boost::unique_lock<boost::mutex> lock(lock_);

    rtp_reception_stats* stats;

    num_active_sources_since_last_reset_ = 0;

    TRTPReceptionStats::iterator it = reception_stats_.begin();
    while (it != reception_stats_.end()) 
    {
        stats = it->second;
        stats->Reset();

        it++;
    }
}

void rtp_reception_stats_db::NoteIncomingPacket(unsigned int SSRC, unsigned short seqNum, unsigned int rtpTimestamp,
                                                unsigned timestampFrequency, bool useForJitterCalculation, timeval& resultPresentationTime, bool& resultHasBeenSyncedUsingRTCP, unsigned packetSize) 
{
    boost::unique_lock<boost::mutex> lock(lock_);

    ++tot_num_packets_received_;
    rtp_reception_stats* stats = Lookup(SSRC);
    if (stats == NULL) 
    {
        // This is the first time we've heard from this SSRC.
        // Create a new record for it:
        stats = new rtp_reception_stats(SSRC, seqNum);
        if (stats == NULL)
        {
            return;
        }
        Add(SSRC, stats);
    }

    if (stats->NumPacketsReceivedSinceLastReset() == 0) 
    {
        ++num_active_sources_since_last_reset_;
    }

    stats->NoteIncomingPacket(seqNum, rtpTimestamp, timestampFrequency,
        useForJitterCalculation,
        resultPresentationTime,
        resultHasBeenSyncedUsingRTCP, packetSize);
}

void rtp_reception_stats_db::NoteIncomingSR(unsigned int SSRC,
                                            unsigned int ntpTimestampMSW, unsigned int ntpTimestampLSW,
                                            unsigned int rtpTimestamp) 
{
    boost::unique_lock<boost::mutex> lock(lock_);

    rtp_reception_stats* stats = Lookup(SSRC);
    if (stats == NULL) 
    {
        // This is the first time we've heard of this SSRC.
        // Create a new record for it:
        stats = new rtp_reception_stats(SSRC);
        if (stats == NULL)
        {
            return;
        }
        Add(SSRC, stats);
    }

    stats->NoteIncomingSR(ntpTimestampMSW, ntpTimestampLSW, rtpTimestamp);
}

void rtp_reception_stats_db::RemoveRecord(unsigned int SSRC) 
{
    rtp_reception_stats* stats = Lookup(SSRC);
    if (stats != NULL) 
    {
        reception_stats_.erase((unsigned long)SSRC);
        delete stats;
    }
}

rtp_reception_stats* rtp_reception_stats_db::Lookup(unsigned int SSRC) const 
{
    rtp_reception_stats* stats = NULL;
    TRTPReceptionStats::const_iterator it = reception_stats_.find((unsigned long)SSRC);
    if (it != reception_stats_.end())
    {
        stats = it->second;
    }

    return stats;
}

void rtp_reception_stats_db::Add(unsigned int SSRC, rtp_reception_stats* stats) 
{
    reception_stats_.insert(std::pair<unsigned long, rtp_reception_stats*>(SSRC, stats));
}

////////////////////////////////////////////////////////////////////////////////

rtp_reception_stats::rtp_reception_stats(unsigned int SSRC, unsigned short initialSeqNum) 
{
    InitSeqNum(initialSeqNum);
    Init(SSRC);
}

rtp_reception_stats::rtp_reception_stats(unsigned int SSRC) 
{
    Init(SSRC);
}

rtp_reception_stats::~rtp_reception_stats() 
{

}

///<summary>
/// Set local variable
///</summary>
void rtp_reception_stats::Init(unsigned int SSRC) 
{
    ssrc_ = SSRC;
    tot_num_packets_received_ = 0;
    tot_bytes_received_hi_ = tot_bytes_received_lo_ = 0;
    have_seen_initial_sequence_number_ = false;
    last_transit_ = ~0;
    previous_packet_rtp_timestamp_ = 0;
    jitter_ = 0.0;
    last_received_sr_ntp_msw_ = last_received_sr_ntp_lsw_ = 0;
    last_received_sr_time_.tv_sec = last_received_sr_time_.tv_usec = 0;
    last_packet_reception_time_.tv_sec = last_packet_reception_time_.tv_usec = 0;
    min_inter_packet_gap_us_ = 0x7FFFFFFF;
    max_inter_packet_gap_us_ = 0;
    total_inter_packet_gaps_.tv_sec = total_inter_packet_gaps_.tv_usec = 0;
    has_been_synchronized_ = false;
    sync_time_.tv_sec = sync_time_.tv_usec = 0;
    Reset();
}

///<summary>
/// Set sequency number
///</summary>
void rtp_reception_stats::InitSeqNum(unsigned short initialSeqNum) 
{
    base_ext_seq_num_received_ = initialSeqNum - 1;
    highest_ext_seq_num_received_ = initialSeqNum;
    have_seen_initial_sequence_number_ = true;
}

void rtp_reception_stats::NoteIncomingPacket(unsigned short seqNum, unsigned int rtpTimestamp,
                                             unsigned timestampFrequency,
                                             bool useForJitterCalculation,
timeval& resultPresentationTime,
    bool& resultHasBeenSyncedUsingRTCP,
    unsigned packetSize) 
{
    if (!have_seen_initial_sequence_number_)
    {
        InitSeqNum(seqNum);
    }

    ++num_packets_received_since_last_reset_;
    ++tot_num_packets_received_;
    unsigned int prevTotBytesReceived_lo = tot_bytes_received_lo_;
    tot_bytes_received_lo_ += packetSize;
    if (tot_bytes_received_lo_ < prevTotBytesReceived_lo)
    { 
        // wrap-around
        ++tot_bytes_received_hi_;
    }

    // Check whether the new sequence number is the highest yet seen:
    unsigned oldSeqNum = (highest_ext_seq_num_received_&0xFFFF);
    if (SeqNumLT((unsigned short)oldSeqNum, seqNum)) 
    {
        // This packet was not an old packet received out of order, so check it:
        unsigned seqNumCycle = (highest_ext_seq_num_received_&0xFFFF0000);
        unsigned seqNumDifference = (unsigned)((int)seqNum-(int)oldSeqNum);
        if (seqNumDifference >= 0x8000) 
        {
            // The sequence number wrapped around, so start a new cycle:
            seqNumCycle += 0x10000;
        }

        unsigned newSeqNum = seqNumCycle|seqNum;
        if (newSeqNum > highest_ext_seq_num_received_) 
        {
            highest_ext_seq_num_received_ = newSeqNum;
        }
    }

    // Record the inter-packet delay
    timeval timeNow;
    utility::GetTimeOfDay(&timeNow);
    if (last_packet_reception_time_.tv_sec != 0 || last_packet_reception_time_.tv_usec != 0) 
    {
        unsigned gap = (timeNow.tv_sec - last_packet_reception_time_.tv_sec) * MILLION 
            + timeNow.tv_usec - last_packet_reception_time_.tv_usec; 
        if (gap > max_inter_packet_gap_us_) 
        {
            max_inter_packet_gap_us_ = gap;
        }
        if (gap < min_inter_packet_gap_us_) 
        {
            min_inter_packet_gap_us_ = gap;
        }
        total_inter_packet_gaps_.tv_usec += gap;

        if (total_inter_packet_gaps_.tv_usec >= MILLION) 
        {
            ++total_inter_packet_gaps_.tv_sec;
            total_inter_packet_gaps_.tv_usec -= MILLION;
        }
    }
    last_packet_reception_time_ = timeNow;

    // Compute the current 'jitter' using the received packet's RTP timestamp,
    // and the RTP timestamp that would correspond to the current time.
    // (Use the code from appendix A.8 in the RTP spec.)
    // Note, however, that we don't use this packet if its timestamp is
    // the same as that of the previous packet (this indicates a multi-packet
    // fragment), or if we've been explicitly told not to use this packet.
    if (useForJitterCalculation && rtpTimestamp != previous_packet_rtp_timestamp_) 
    {
        unsigned arrival = (timestampFrequency * timeNow.tv_sec);
        arrival += (unsigned)((2.0 * timestampFrequency * timeNow.tv_usec + 1000000.0) / 2000000);
        // note: rounding
        int transit = arrival - rtpTimestamp;
        if (last_transit_ == (~0))
        {
            last_transit_ = transit; // hack for first time
        }

        int d = transit - last_transit_;
        last_transit_ = transit;
        if (d < 0)
        {
            d = -d;
        }
        jitter_ += (1.0/16.0) * ((double)d - jitter_);
    }

    // Return the 'presentation time' that corresponds to "rtpTimestamp":
    if (sync_time_.tv_sec == 0 && sync_time_.tv_usec == 0) 
    {
        // This is the first timestamp that we've seen, so use the current
        // 'wall clock' time as the synchronization time.  (This will be
        // corrected later when we receive RTCP SRs.)
        sync_timestamp_ = rtpTimestamp;
        sync_time_ = timeNow;
    }

    int timestampDiff = rtpTimestamp - sync_timestamp_;
    // Note: This works even if the timestamp wraps around
    // (as long as "int" is 32 bits)

    // Divide this by the timestamp frequency to get real time:
    double timeDiff = timestampDiff / (double)timestampFrequency;

    // Add this to the 'sync time' to get our result:
    unsigned const million = 1000000;
    unsigned seconds, uSeconds;
    if (timeDiff >= 0.0) 
    {
        seconds = sync_time_.tv_sec + (unsigned)(timeDiff);
        uSeconds = sync_time_.tv_usec + (unsigned)((timeDiff - (unsigned)timeDiff) * million);
        if (uSeconds >= million) 
        {
            uSeconds -= million;
            ++seconds;
        }
    }
    else
    {
        timeDiff = -timeDiff;
        seconds = sync_time_.tv_sec - (unsigned)(timeDiff);
        uSeconds = sync_time_.tv_usec - (unsigned)((timeDiff - (unsigned)timeDiff) * million);
        if ((int)uSeconds < 0) 
        {
            uSeconds += million;
            --seconds;
        }
    }
    resultPresentationTime.tv_sec = seconds;
    resultPresentationTime.tv_usec = uSeconds;
    resultHasBeenSyncedUsingRTCP = has_been_synchronized_;

    // Save these as the new synchronization timestamp & time:
    sync_timestamp_ = rtpTimestamp;
    sync_time_ = resultPresentationTime;

    previous_packet_rtp_timestamp_ = rtpTimestamp;
}

void rtp_reception_stats::NoteIncomingSR(unsigned int ntpTimestampMSW,
                                         unsigned int ntpTimestampLSW,
                                         unsigned int rtpTimestamp) 
{
    last_received_sr_ntp_msw_ = ntpTimestampMSW;
    last_received_sr_ntp_lsw_ = ntpTimestampLSW;

    utility::GetTimeOfDay(&last_received_sr_time_);

    // Use this SR to update time synchronization information:
    sync_timestamp_ = rtpTimestamp;
    sync_time_.tv_sec = ntpTimestampMSW - 0x83AA7E80; // 1/1/1900 -> 1/1/1970
    double microseconds = (ntpTimestampLSW * 15625.0) / 0x04000000; // 10^6/2^32
    sync_time_.tv_usec = (unsigned)(microseconds+0.5);
    has_been_synchronized_ = true;
}

double rtp_reception_stats::TotNumKBytesReceived() const 
{
    double const hiMultiplier = 0x20000000/125.0; // == (2^32)/(10^3)
    return tot_bytes_received_hi_*hiMultiplier + tot_bytes_received_lo_/1000.0;
}

unsigned rtp_reception_stats::Jitter() const 
{
    return (unsigned)jitter_;
}

void rtp_reception_stats::Reset() 
{
    num_packets_received_since_last_reset_ = 0;
    last_reset_ext_seq_num_received_ = highest_ext_seq_num_received_;
}

bool rtp_reception_stats::SeqNumLT(unsigned short s1, unsigned short s2)
{
    // a 'less-than' on 16-bit sequence numbers
    int diff = s2-s1;
    if (diff > 0) 
    {
        return (diff < 0x8000);
    }
    else if (diff < 0) 
    {
        return (diff < -0x8000);
    }
    else
    { // diff == 0
        return false;
    }
}
