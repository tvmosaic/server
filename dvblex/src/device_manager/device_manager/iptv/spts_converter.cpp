/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include "spts_converter.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

namespace dvblex {

iptv_spts_converter_t::iptv_spts_converter_t(iptv_spts_converter_callback_p callback, void* param) :
    pmt_pid_(INVALID_PACKET_PID), tid_(0), sid_(0), pat_cont_counter_(0), pat_version_(0),
        callback_(callback), param_(param), state_(iscs_pat)
{
}

iptv_spts_converter_t::~iptv_spts_converter_t()
{
}

void iptv_spts_converter_t::init()
{
    pmt_map_.clear();
    pmt_pid_ = INVALID_PACKET_PID;
    pat_section_parser_.Init(PID_PAT);
    state_ = iscs_pat;
    log_info(L"iptv_spts_converter_t::init. Started. Looking for PAT");
}

void iptv_spts_converter_t::term()
{
}

void iptv_spts_converter_t::process_stream(const unsigned char* buffer, size_t len)
{
    if (state_ == iscs_streaming)
    {
        send_stream(buffer, len);
    } else
    {
		int c = len / TS_PACKET_SIZE;
		for (int i=0; i<c; i++)
		{
			const unsigned char* buf = buffer + i*TS_PACKET_SIZE;
			unsigned short pid = dvblink::engine::ts_process_routines::GetPacketPID(buf);
			switch (state_)
			{
			case iscs_pat:
				if (pid == PID_PAT)
				{
					ts_payload_parser::ts_section_list found_sections;
					if (pat_section_parser_.AddPacket(buf, TS_PACKET_SIZE, found_sections) > 0)
					{
						//get PAT section
						for (unsigned int ii=0; ii<found_sections.size(); ii++)
						{
							process_pat_section(found_sections[ii].section, found_sections[ii].length);
						}
						pat_section_parser_.ResetFoundSections(found_sections);
					}
				}
				break;
			case iscs_pmt:
				if (pmt_map_.find(pid) != pmt_map_.end())
				{
					ts_payload_parser::ts_section_list found_sections;
					if (pmt_map_[pid].AddPacket(buf, TS_PACKET_SIZE, found_sections) > 0)
					{
						//get PMT section
						for (unsigned int ii=0; ii<found_sections.size() && state_ == iscs_pmt; ii++)
						{
							process_pmt_section(pid, found_sections[ii].section, found_sections[ii].length);
						}
						pmt_map_[pid].ResetFoundSections(found_sections);
					}
				}
				break;
			default:
				break;
			}
		}
    }
}

void iptv_spts_converter_t::process_pat_section(unsigned char* pat_buffer, size_t pat_len)
{
    CTSPatInfo pat_info;
	pat_info.Init(pat_buffer, pat_len);

	std::vector<SPatProg> services;
	pat_info.GetServices(services);
	for (unsigned int i=0; i<services.size(); i++)
	{
		unsigned short service_num = HILO_GET(services[i].program_number);
		if (service_num != 0)
		{
			unsigned short pid = HILO_GET(services[i].network_pid);
            pmt_map_[pid] = ts_section_payload_parser();
			pmt_map_[pid].Init(pid);
		}
	}

    if (pmt_map_.size() > 0)
    {
        pat_info.GetTSID(tid_);
        state_ = iscs_pmt;
        log_info(L"iptv_spts_converter_t::process_pat_section. PAT found an processed. number of PMTs %1%") % pmt_map_.size();
    }
}

void iptv_spts_converter_t::process_pmt_section(unsigned short pmt_pid, unsigned char* pmt_buffer, size_t pmt_len)
{
    //if section was assembled for this pid - switch to streaming
    CTSPmtInfo pmt_info_;
    pmt_info_.Init(pmt_buffer, pmt_len);
    pmt_info_.GetSectionServiceID(sid_);
    pmt_pid_ = pmt_pid;
    pat_cont_counter_ = 0;
    pat_version_ = 1;

    log_info(L"iptv_spts_converter_t::process_pmt_section. Found PMT (%1%, %2%). Start streaming") % pmt_pid_ % sid_;

    pat_section_parser_.Reset();

    state_ = iscs_streaming;
}

void iptv_spts_converter_t::send_stream(const unsigned char* buf, size_t len)
{
    const unsigned char* start = buf;
    const unsigned char* cur = buf;
    const unsigned char* end = buf + len;
	size_t to_send = 0;

	while (cur < end)
	{
		unsigned short pid = ts_process_routines::GetPacketPID(cur);
		if (pid == PID_PAT)
		{
			if (to_send)
			{
				callback_(start, to_send, param_);
				to_send = 0;
			}

			ts_payload_parser::ts_section_list found_sections;
			if (pat_section_parser_.AddPacket(cur, TS_PACKET_SIZE, found_sections) > 0)
			{
				//get PAT section
				for (unsigned int ii=0; ii<found_sections.size(); ii++)
				{
					stream_pat_section(found_sections[ii].section, found_sections[ii].length);
				}
				pat_section_parser_.ResetFoundSections(found_sections);
			}

			cur += TS_PACKET_SIZE;
			start = cur;
		} else
		{
			cur += TS_PACKET_SIZE;
			to_send += TS_PACKET_SIZE;
		}
	}

	if (to_send)
	{
		callback_(start, to_send, param_);
	}
}

void iptv_spts_converter_t::stream_pat_section(unsigned char* pat_buffer, size_t pat_len)
{
    int buf_len;
    unsigned char* buf = pat_generator_.CreatePATPacket(buf_len, pat_version_, &pat_cont_counter_, tid_, sid_, pmt_pid_);

    callback_(buf, buf_len, param_);
}

}

