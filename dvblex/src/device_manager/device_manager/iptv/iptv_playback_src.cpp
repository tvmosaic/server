/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_pb_object.h>
#include <dl_locale_strings.h>
#include <dl_language_settings.h>
#include "iptv_provider.h"
#include "iptv_playback_src.h"

using namespace dvblex;
using namespace dvblex::playback;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

static const std::string IPTV_PB_ID_SEPARATOR = "/";
static const std::string IPTV_PB_SEARCH_CONTAINER_ID = "94c45155-de46-4863-87d0-f5002c87a49f";

iptv_playback_src_t::iptv_playback_src_t()
    : device_playback_src_t(), iptv_vod_provider_(NULL)
{
}

iptv_playback_src_t::~iptv_playback_src_t()
{
}


void iptv_playback_src_t::source_init()
{
    if (device_playback_src_init_list_.size() > 0)
    {
        concise_param_map_t scanner_settings = device_playback_src_init_list_.at(0).headend_info_.device_config_params_;

        iptv_provider_t ip;
        iptv_vod_provider_ = ip.get_iptv_vod_provider(scanner_settings);
    } else
    {
        log_error(L"iptv_playback_src_t::source_init. No device settings found. Unable to create VOD provider");
    }
}

void iptv_playback_src_t::source_term()
{
    delete iptv_vod_provider_;
}

void iptv_playback_src_t::add_source_to_container_list(dvblex::playback::pb_container_list_t& container_list)
{
    pb_container_t cnt(pbct_container_source, pbit_item_unknown);
    cnt.object_id_ = make_object_id(id_.to_string(), object_root_id.get());
    cnt.parent_id_ = object_root_id.get();
    cnt.source_id_ = id_.to_string();
    cnt.name_ = get_source_name();
    container_list.push_back(cnt);
}

static bool parse_playback_object_id(const object_id_t& object_id, std::string& src_id, std::string& container_id, std::string& id_level_1, std::string& id_level_2)
{
    bool ret_val = false;

    src_id.clear();
    container_id.clear();
    id_level_2.clear();
    id_level_1.clear();

    std::string pb_object_id;
    if (dvblex::playback::parse_object_id(object_id, src_id, pb_object_id))
    {
        //parse pb_object_id into container-item_id
        if (boost::iequals(pb_object_id, object_root_id.get()))
        {
            //source root object (container and id_level_x are empty)
            ret_val = true;
        } else
        {
            //search for a separator
            std::string::size_type pos = pb_object_id.find(IPTV_PB_ID_SEPARATOR);
            if (pos == std::string::npos)
            {
                //container
                container_id = pb_object_id;
                ret_val = true;
            } else
            {
                //container and id_level_x
                container_id = pb_object_id.substr(0, pos);
                std::string::size_type level_2_pos = pb_object_id.find(IPTV_PB_ID_SEPARATOR, pos+1);
                if (level_2_pos == std::string::npos)
                {
                    id_level_1 = pb_object_id.substr(pos+1, pb_object_id.size() - pos - 1);
                } else
                {
                    id_level_1 = pb_object_id.substr(pos+1, level_2_pos - pos - 1);
                    id_level_2 = pb_object_id.substr(level_2_pos+1, pb_object_id.size() - level_2_pos - 1);
                }
                ret_val = true;
            }
        }
    }

    return ret_val;
}

iptv_playback_src_t::pb_video_obj_t iptv_playback_src_t::video_obj_from_vod_item(const object_id_t& parent_id, const object_id_t& object_id, const vod_item_obj_t& vod_item)
{
    pb_video_obj_t iptv_pb_item(new pb_video_t());
    //set object id and url (relative) to object id
    iptv_pb_item->object_id_ = object_id;
    iptv_pb_item->parent_id_ = parent_id;
    iptv_pb_item->video_info_ = vod_item->video_info_;
    iptv_pb_item->thumbnail_ = vod_item->video_info_.m_ImageURL;
    iptv_pb_item->url_ = vod_item->url_;
    iptv_pb_item->size_ = vod_item->size_;

    return iptv_pb_item;
}

pb_container_t iptv_playback_src_t::pb_container_from_vod_container(const object_id_t& parent_id, const object_id_t& object_id, const vod_container_t& vod_cnt)
{
    pb_container_t cnt(pbct_container_group, pbit_item_video);
    cnt.object_id_ = object_id;
    cnt.parent_id_ = parent_id;
    cnt.name_ = vod_cnt.name_;
    cnt.total_count_ = vod_cnt.children_count_;
    return cnt;
}

void iptv_playback_src_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_objects_request& request, dvblink::messaging::playback::get_objects_response& response)
{
    response.result_ = true;

    if (iptv_vod_provider_ != NULL)
    {
        dvblex::playback::pb_object_t& object_info = response.object_;

	    std::string source_id;
	    std::string container_id;
        std::string id_level_1;
        std::string id_level_2;
        if (parse_playback_object_id(request.object_id_, source_id, container_id, id_level_1, id_level_2))
	    {
            if (boost::iequals(source_id, id_.to_string()))
            {
                if (request.is_children_request_)
                {
                    //children request
                    if (container_id.size() == 0)
		            {
		                //this is source root request
			            if (request.object_type_ == pot_object_unknown || request.object_type_ == pot_object_container)
			            {
                            vod_container_list_t categories;
                            iptv_vod_provider_->get_categories(categories);
                            for (size_t i=0; i<categories.size() ; i++)
                            {
                                object_id_t object_id = make_object_id(source_id, categories[i].id_);
                                object_id_t parent_id = make_object_id(source_id, object_root_id.get());
                                pb_container_t cnt = pb_container_from_vod_container(parent_id, object_id, categories[i]);
                                object_info.container_list_.push_back(cnt);
                            }

                            object_info.actual_count_ = object_info.container_list_.size();
                            object_info.total_count_ = object_info.container_list_.size();
                        }

                        response.result_ = true;
                    } else
                    if (!container_id.empty() && id_level_1.empty())
                    {
                        //groups of the given container (aka 2nd level objects)
			            if (request.object_type_ == pot_object_unknown || request.object_type_ == pot_object_container)
			            {
                            vod_container_list_t groups;
                            iptv_vod_provider_->get_groups(container_id, groups);
                            for (size_t i=0; i<groups.size() ; i++)
                            {
                                object_id_t parent_id = make_object_id(source_id, container_id);
                                object_id_t object_id(parent_id.get() + IPTV_PB_ID_SEPARATOR + groups[i].id_);
                                pb_container_t cnt = pb_container_from_vod_container(parent_id, object_id, groups[i]);
                                object_info.container_list_.push_back(cnt);
                            }

                            object_info.actual_count_ = object_info.container_list_.size();
                            object_info.total_count_ = object_info.container_list_.size();
                        }

                        response.result_ = true;
                    } else
                    if (!container_id.empty() && !id_level_1.empty())
                    {
                        //group items
			            if (request.object_type_ == pot_object_unknown || request.object_type_ == pot_object_item)
                        {
                            object_id_t parent_id = make_object_id(source_id, container_id + IPTV_PB_ID_SEPARATOR + id_level_1);

                            vod_item_obj_list_t items;
                            int total = 0;
                            if (iptv_vod_provider_->get_items(id_level_1, request.start_position_, request.requested_count_, items, total))
                            {
                                for (size_t i=0; i<items.size(); i++)
                                {
                                    std::string item_id = items[i]->get_id();

                                    object_id_t object_id(parent_id.get() + IPTV_PB_ID_SEPARATOR + item_id);
                                    pb_video_obj_t iptv_pb_item = video_obj_from_vod_item(parent_id, object_id, items[i]);
                                    object_info.item_list_.push_back(iptv_pb_item);
                                }
                                object_info.actual_count_ = object_info.item_list_.size();
                                object_info.total_count_ = total;
                                response.result_ = true;
                            }
                        }
                    } else
                    {
                        log_error(L"iptv_playback_src_t::handle get_objects_request. Children request for unknown object %1%") % request.object_id_.to_wstring();
                    }
                } else
                {
                    //object info request
                    if (container_id.size() == 0)
		            {
		                //request for source container
                        add_source_to_container_list(object_info.container_list_);
                        object_info.actual_count_ = object_info.container_list_.size();
                        object_info.total_count_ = object_info.container_list_.size();

                        response.result_ = true;
                    } else
                    if (!container_id.empty() && id_level_1.empty())
                    {
                        //request for the first level (aka category)
                        vod_container_t category;
                        if (iptv_vod_provider_->get_category(container_id, category))
                        {
                            object_id_t object_id = make_object_id(source_id, category.id_);
                            object_id_t parent_id = make_object_id(source_id, object_root_id.get());
                            pb_container_t cnt = pb_container_from_vod_container(parent_id, object_id, category);
                            object_info.container_list_.push_back(cnt);

                            object_info.actual_count_ = object_info.container_list_.size();
                            object_info.total_count_ = object_info.container_list_.size();

                            response.result_ = true;
                        }
                    } else
                    if (!container_id.empty() && !id_level_1.empty() && id_level_2.empty())
                    {
                        if (boost::iequals(container_id, IPTV_PB_SEARCH_CONTAINER_ID))
                        {
                            //info about item from the search container
                            object_id_t parent_id = make_object_id(source_id, IPTV_PB_SEARCH_CONTAINER_ID);

                            vod_item_obj_t item;
                            if (iptv_vod_provider_->get_item(id_level_1, item))
                            {
                                std::string item_id = item->get_id();

                                object_id_t object_id(parent_id.get() + IPTV_PB_ID_SEPARATOR + item_id);
                                pb_video_obj_t iptv_pb_item = video_obj_from_vod_item(parent_id, object_id, item);
                                object_info.item_list_.push_back(iptv_pb_item);

                                object_info.actual_count_ = object_info.item_list_.size();
                                object_info.total_count_ = object_info.item_list_.size();
                                response.result_ = true;
                            }
                        } else
                        {
                            //request for the second level (aka group)
                            vod_container_t group;
                            if (iptv_vod_provider_->get_group(id_level_1, group))
                            {
                                object_id_t parent_id = make_object_id(source_id, container_id);
                                object_id_t object_id(parent_id.get() + IPTV_PB_ID_SEPARATOR + id_level_1);
                                pb_container_t cnt = pb_container_from_vod_container(parent_id, object_id, group);
                                object_info.container_list_.push_back(cnt);

                                object_info.actual_count_ = object_info.container_list_.size();
                                object_info.total_count_ = object_info.container_list_.size();

                                response.result_ = true;
                            }
                        }
                    } else
                    if (!container_id.empty() && !id_level_1.empty() && !id_level_2.empty())
                    {
                        //request for item
                        object_id_t parent_id = make_object_id(source_id, container_id + IPTV_PB_ID_SEPARATOR + id_level_1);

                        vod_item_obj_t item;
                        if (iptv_vod_provider_->get_item(id_level_1, id_level_2, item))
                        {
                            std::string item_id = item->get_id();

                            object_id_t object_id(parent_id.get() + IPTV_PB_ID_SEPARATOR + item_id);
                            pb_video_obj_t iptv_pb_item = video_obj_from_vod_item(parent_id, object_id, item);
                            object_info.item_list_.push_back(iptv_pb_item);

                            object_info.actual_count_ = object_info.item_list_.size();
                            object_info.total_count_ = object_info.item_list_.size();
                            response.result_ = true;
                        }

                    } else
                    {
                        log_error(L"iptv_playback_src_t::handle get_objects_request. Info request for unknown object %1%") % request.object_id_.to_wstring();
                    }
                }
            } else
            {
                log_error(L"iptv_playback_src_t::handle get_objects_request. Request for another source %1%") % string_cast<EC_UTF8>(source_id);
            }
        } else
        {
            log_error(L"iptv_playback_src_t::handle get_objects_request. Unable to parse the object ID from %1%") % request.object_id_.to_wstring();
        }
    }
}

void iptv_playback_src_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::search_objects_request& request, dvblink::messaging::playback::search_objects_response& response)
{
    response.result_ = false;

    if (iptv_vod_provider_ == NULL)
        return;

    std::wstring wsearch_string = request.search_string_.to_wstring();

    dvblex::playback::pb_object_t& object_info = response.object_;

	std::string source_id;
	std::string container_id;
    std::string id_level_1;
    std::string id_level_2;
    if (parse_playback_object_id(request.object_id_, source_id, container_id, id_level_1, id_level_2))
	{
        if (boost::iequals(source_id, id_.to_string()))
        {
            //search is supported only inside inside a group
            if (!container_id.empty() && !id_level_1.empty())
            {
		        if (request.object_type_ == pot_object_unknown || request.object_type_ == pot_object_item)
                {
                    object_id_t parent_id = make_object_id(source_id, IPTV_PB_SEARCH_CONTAINER_ID);

                    vod_item_obj_list_t items;
                    if (iptv_vod_provider_->search_items(wsearch_string, id_level_1, items))
                    {
                        for (size_t i=0; i<items.size(); i++)
                        {
                            object_id_t object_id(parent_id.get() + IPTV_PB_ID_SEPARATOR + items[i]->get_id());
                            pb_video_obj_t iptv_pb_item = video_obj_from_vod_item(parent_id, object_id, items[i]);
                            object_info.item_list_.push_back(iptv_pb_item);
                        }
                        object_info.actual_count_ = object_info.item_list_.size();
                        object_info.total_count_ = object_info.item_list_.size();

                        response.result_ = true;
                    } else
                    {
                        log_error(L"iptv_playback_src_t::handle search_objects_request. Group %1% is not found") % string_cast<EC_UTF8>(id_level_1);
                    }
                }
            } else
            {
                log_error(L"iptv_playback_src_t::handle search_objects_request. Can only search inside item groups");
            }

        } else
        {
            log_error(L"iptv_playback_src_t::handle search_objects_request. Request for another source %1%") % string_cast<EC_UTF8>(source_id);
        }
    } else
    {
        log_error(L"iptv_playback_src_t::handle search_objects_request. Unable to parse the object ID from %1%") % request.object_id_.to_wstring();
    }
}

void iptv_playback_src_t::handle(const dvblink::message_sender_t& sender, const dvblink::messaging::playback::get_source_container_request& request, dvblink::messaging::playback::get_source_container_response& response)
{
    dvblex::playback::pb_container_list_t cl;
    add_source_to_container_list(cl);
    if (cl.size() > 0)
    {
        response.container_ = cl[0];
        response.result_ = true;
    } else
    {
        response.result_ = false;
    }
}

std::string iptv_playback_src_t::get_source_name()
{
    std::string name = "IPTV";
    for (size_t i=0; i<device_playback_src_init_list_.size(); i++)
        name = device_playback_src_init_list_.at(i).headend_info_.name_.to_string();

    return name;
}
