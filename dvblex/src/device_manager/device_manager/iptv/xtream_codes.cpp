/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/filesystem.hpp>
#include <boost/tokenizer.hpp>
#include <rapidjson/document.h>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_common.h>
#include <dl_locale_strings.h>
#include <dl_language_settings.h>
#include <dl_http_comm.curl.h>
#include <dl_strptime.h>
#include <dl_rapidjson_laparser.h>
#include "xtream_codes.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

xtream_codes_scanner_t::xtream_codes_scanner_t()
{
}

xtream_codes_scanner_t::~xtream_codes_scanner_t()
{
}

dvblink::scan_element_t xtream_codes_scanner_t::get_scan_element_from_info(const xtream_codes_info_t& info)
{
    std::ostringstream archive_stream;
    boost::archive::text_oarchive archive(archive_stream);

    archive << info;

    return archive_stream.str();
}

bool xtream_codes_scanner_t::get_info_from_scan_element(const dvblink::scan_element_t& se, xtream_codes_info_t& info)
{
    bool ret_val = false;
    try {
        std::istringstream archive_stream(se.to_string());
        boost::archive::text_iarchive archive(archive_stream);

        archive >> info;

        ret_val = info.is_valid();
    } catch(...) 
    {
        ret_val = false;
    } 

    return ret_val;
}

bool xtream_codes_scanner_t::get_info_from_playlist_url(const dvblink::url_address_t& url, xtream_codes_info_t& info)
{
    bool ret_val = false;

    std::string address;
	std::string user;
	std::string pswd;
    unsigned short port;
	std::string url_suffix;
    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(url.to_string().c_str(), address, user, pswd, port, url_suffix);

    if (DL_NET_PROTO_HTTP == proto || DL_NET_PROTO_HTTPS == proto)
    {
        std::stringstream strbuf;
        strbuf << network_helper::get_proto_string(proto) << address << ":" << port;
        info.url_ = strbuf.str();

        std::map<std::string, std::string> http_params;
        network_helper::parse_http_suffix_params(url_suffix, http_params);
        std::map<std::string, std::string>::iterator params_it = http_params.find("username");
        if (params_it != http_params.end())
            info.user_ = params_it->second;

        params_it = http_params.find("password");
        if (params_it != http_params.end())
            info.password_ = params_it->second;

        if (!info.password_.empty() && !info.user_.empty())
        {
            //call xtream codes information url
            std::string xc_info_url = info.get_command_url_stem();
            proto = network_helper::parse_net_url(xc_info_url.c_str(), address, user, pswd, port, url_suffix);

            http_comm_handler http_comm(PRODUCT_NAME_UTF8, address.c_str(), user.c_str(), pswd.c_str(), port);
            
            if (http_comm.Init())
            {
                std::string response;

                if (http_comm.ExecuteGetWithResponseGZ(url_suffix.c_str(), proto == DL_NET_PROTO_HTTPS, response))
                {
                    rapidjson::LookaheadParser r(&response[0]);
                    if (r.IsValid())
                    {
                        r.EnterObject();
                        while (const char* key = r.NextObjectKey())
                        {
                            if (strcmp(key, "user_info") == 0 && r.PeekType() != rapidjson::kNullType)
                            {
                                r.EnterObject();
                                while (const char* ui_key = r.NextObjectKey())
                                {
                                    if (strcmp(ui_key, "status") == 0 && r.PeekType() != rapidjson::kNullType)
                                    {
                                        ret_val = boost::iequals(r.GetString(), "Active");
                                    }
                                    else
                                        r.SkipValue();
                                }
                            } else
                            {
                                r.SkipValue();
                            }
                        }
                    } else
                    {
                        log_info(L"xtream_codes_scanner_t::get_info_from_playlist_url. Failed to deserialize json response");
                    }
                } else 
                {
                    log_info(L"xtream_codes_scanner_t::get_info_from_playlist_url. ExecuteGetWithResponseGZ failed");
                }
            }

        }

    }
    return ret_val;
}

void xtream_codes_scanner_t::read_live_categories(const xtream_codes_info_t& info, xtream_codes_cat_map_t& categories)
{
    std::string address;
	std::string user;
	std::string pswd;
    unsigned short port;
	std::string url_suffix;
    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(info.get_live_categories_url().to_string().c_str(), address, user, pswd, port, url_suffix);

    if (DL_NET_PROTO_HTTP == proto || DL_NET_PROTO_HTTPS == proto)
    {
        http_comm_handler http_comm(PRODUCT_NAME_UTF8, address.c_str(), user.c_str(), pswd.c_str(), port);
        
        if (http_comm.Init())
        {
            std::string response;

            if (http_comm.ExecuteGetWithResponseGZ(url_suffix.c_str(), proto == DL_NET_PROTO_HTTPS, response))
            {
                rapidjson::LookaheadParser r(&response[0]);

                if (r.IsValid())
                {
                    r.EnterArray();
                    while (r.NextArrayValue()) 
                    {
                        xtream_codes_cat_t category;

                        r.EnterObject();
                        while (const char* cat_key = r.NextObjectKey())
                        {
                            if (strcmp(cat_key, "category_id") == 0 && r.PeekType() != rapidjson::kNullType)
                                category.id_ = r.GetString();
                            else if (strcmp(cat_key, "category_name") == 0 && r.PeekType() != rapidjson::kNullType)
                                category.name_ = r.GetString();
                            else
                                r.SkipValue();
                        }
                        categories[category.id_] = category;
                    }
                    log_info(L"xtream_codes_scanner_t::read_live_categories. Successfully read %1% live categories") % categories.size();
                } else
                {
                    log_error(L"xtream_codes_scanner_t::read_live_categories. Failed to deserialize json response");
                }
            } else 
            {
                log_error(L"xtream_codes_scanner_t::read_live_categories. ExecuteGetWithResponseGZ failed");
            }
        }
    } else 
    {
        log_error(L"xtream_codes_scanner_t::read_live_categories. Unknown proto %1%") % string_cast<EC_UTF8>(info.url_);
    }
}

bool xtream_codes_scanner_t::scan_live_channels(const xtream_codes_info_t& info, playlist_channel_map_t& channel_map)
{
    bool ret_val = false;

    //read live categories first
    xtream_codes_cat_map_t categories;
    read_live_categories(info, categories);

    std::string address;
	std::string user;
	std::string pswd;
    unsigned short port;
	std::string url_suffix;
    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(info.get_live_streams_url().to_string().c_str(), address, user, pswd, port, url_suffix);

    if (DL_NET_PROTO_HTTP == proto || DL_NET_PROTO_HTTPS == proto)
    {
        http_comm_handler http_comm(PRODUCT_NAME_UTF8, address.c_str(), user.c_str(), pswd.c_str(), port);
        
        if (http_comm.Init())
        {
            std::string response;

            if (http_comm.ExecuteGetWithResponseGZ(url_suffix.c_str(), proto == DL_NET_PROTO_HTTPS, response))
            {
                rapidjson::LookaheadParser r(&response[0]);
                
                if (r.IsValid())
                {
                    r.EnterArray();
                    while (r.NextArrayValue()) 
                    {
                        playlist_channel channel;

                        r.EnterObject();
                        while (const char* prop_key = r.NextObjectKey())
                        {
                            if (strcmp(prop_key, "stream_id") == 0 && r.PeekType() != rapidjson::kNullType)
                            {
                                int stream_id = r.GetInt();
                                channel.url = info.get_live_stream_url(stream_id).to_string();
                            }
                            else if (strcmp(prop_key, "name") == 0 && r.PeekType() != rapidjson::kNullType)
                                channel.name = r.GetString();
                            else if (strcmp(prop_key, "stream_icon") == 0 && r.PeekType() != rapidjson::kNullType)
                                channel.tags[m3u_plus_tvg_logo] = r.GetString();
                            else if (strcmp(prop_key, "epg_channel_id") == 0 && r.PeekType() != rapidjson::kNullType)
                                channel.tags[m3u_plus_tvg_id] = r.GetString();
                            else if (strcmp(prop_key, "category_id") == 0 && r.PeekType() != rapidjson::kNullType)
                            {
                                std::string cat = r.GetString();
                                if (categories.find(cat) != categories.end())
                                    channel.tags[m3u_plus_group_title] = categories[cat].name_;
                            } else
                            {
                                r.SkipValue();
                            }
                        }
                        channel_map[channel.get_channel_id()] = channel;
                    }

                    log_info(L"xtream_codes_scanner_t::scan_live_channels. Successfully read %1% live channels") % channel_map.size();

                    ret_val = true;
                } else
                {
                    log_error(L"xtream_codes_scanner_t::scan_live_channels. Failed to deserialize json response");
                }
            } else 
            {
                log_error(L"xtream_codes_scanner_t::scan_live_channels. ExecuteGetWithResponseGZ failed");
            }
        }
    } else 
    {
        log_error(L"xtream_codes_scanner_t::scan_live_channels. Unknown proto %1%") % string_cast<EC_UTF8>(info.url_);
    }

    return ret_val;
}

void xtream_codes_scanner_t::read_vod_categories(const xtream_codes_info_t& sci, bool vod, bool* exit_flag, xtream_codes_cat_list_t& vod_categories)
{
    dvblink::url_address_t url = vod ? sci.get_vod_categories_url() : sci.get_series_categories_url();
    std::string address;
	std::string user;
	std::string pswd;
    unsigned short port;
	std::string url_suffix;
    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(url.to_string().c_str(), address, user, pswd, port, url_suffix);

    if (DL_NET_PROTO_HTTP == proto || DL_NET_PROTO_HTTPS == proto)
    {
        http_comm_handler http_comm(PRODUCT_NAME_UTF8, address.c_str(), user.c_str(), pswd.c_str(), port);
        
        if (http_comm.Init())
        {
            std::string response;

            if (http_comm.ExecuteGetWithResponseGZ(url_suffix.c_str(), proto == DL_NET_PROTO_HTTPS, response))
            {
                rapidjson::LookaheadParser r(&response[0]);

                if (r.IsValid())
                {
                    r.EnterArray();
                    while (r.NextArrayValue()) 
                    {
                        xtream_codes_cat_t category;

                        r.EnterObject();
                        while (const char* cat_key = r.NextObjectKey())
                        {
                            if (strcmp(cat_key, "category_id") == 0 && r.PeekType() != rapidjson::kNullType)
                                category.id_ = r.GetString();
                            else if (strcmp(cat_key, "category_name") == 0 && r.PeekType() != rapidjson::kNullType)
                                category.name_ = r.GetString();
                            else
                                r.SkipValue();
                        }
                        vod_categories.push_back(category);
                    }
                    log_info(L"xtream_codes_scanner_t::read_vod_categories. Successfully read %1% vod categories") % vod_categories.size();
                } else
                {
                    log_error(L"xtream_codes_scanner_t::read_vod_categories. Failed to deserialize json response");
                }
            } else 
            {
                log_error(L"xtream_codes_scanner_t::read_vod_categories. ExecuteGetWithResponseGZ failed");
            }
        }
    } else 
    {
        log_error(L"xtream_codes_scanner_t::read_vod_categories. Unknown proto %1%") % string_cast<EC_UTF8>(sci.url_);
    }
}

void xtream_codes_scanner_t::read_vod_category_items(const xtream_codes_info_t& info, bool* exit_flag, const std::string& category_id, vod_item_obj_map_t& items)
{
    std::string address;
    std::string user;
    std::string pswd;
    unsigned short port;
    std::string url_suffix;
    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(info.get_vod_category_streams_url(category_id).to_string().c_str(), address, user, pswd, port, url_suffix);

    http_comm_handler http_comm(PRODUCT_NAME_UTF8, address.c_str(), user.c_str(), pswd.c_str(), port);
    
    if (http_comm.Init())
    {
        std::string response;

        if (http_comm.ExecuteGetWithResponseGZ(url_suffix.c_str(), proto == DL_NET_PROTO_HTTPS, response))
        {
            rapidjson::LookaheadParser r(&response[0]);

            if (r.IsValid())
            {
                r.EnterArray();
                while (r.NextArrayValue()) 
                {
                    vod_item_obj_t item = boost::shared_ptr<vod_item_t>(new vod_item_t());
                    int stream_id = 0;
                    std::string extension;

                    r.EnterObject();
                    while (const char* prop_key = r.NextObjectKey())
                    {
                        if (strcmp(prop_key, "stream_id") == 0 && r.PeekType() != rapidjson::kNullType)
                        {
                            stream_id = r.GetInt();
                            std::string id = boost::lexical_cast<std::string>(stream_id);
                            item->video_info_.id_ = id;
                        }
                        else if (strcmp(prop_key, "container_extension") == 0 && r.PeekType() != rapidjson::kNullType)
                            extension = r.GetString();
                        else if (strcmp(prop_key, "name") == 0 && r.PeekType() != rapidjson::kNullType)
                            item->video_info_.m_Name = r.GetString();
                        else if (strcmp(prop_key, "stream_icon") == 0 && r.PeekType() != rapidjson::kNullType)
                            item->video_info_.m_ImageURL = r.GetString();
                        else if (strcmp(prop_key, "rating_5based") == 0 && r.PeekType() != rapidjson::kNullType)
                        {
                            item->video_info_.m_StarNum = r.GetDouble();
                            if (item->video_info_.m_StarNum > 0)
                                item->video_info_.m_StarNumMax = 5;
                        }
                        else
                            r.SkipValue();
                    }
                    item->url_ = info.get_vod_stream_url(stream_id, extension);
                    items[item->video_info_.id_] = item;
                }
            } else
            {
                log_error(L"xtream_codes_scanner_t::read_vod_category_items. Failed to deserialize json response");
            }
        } else 
        {
            log_error(L"xtream_codes_scanner_t::read_vod_category_items. ExecuteGetWithResponseGZ failed");
        }
    }
    log_info(L"xtream_codes_scanner_t::read_vod_category_items. Read %1% vod items in category %2%") %  items.size() % string_cast<EC_UTF8>(category_id);
}


void xtream_codes_scanner_t::read_category_series(const xtream_codes_info_t& info, bool* exit_flag, const std::string& category_id, xtream_codes_scanner_t::xtream_codes_series_map_t& series)
{
    std::string address;
    std::string user;
    std::string pswd;
    unsigned short port;
    std::string url_suffix;
    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(info.get_category_series_url(category_id).to_string().c_str(), address, user, pswd, port, url_suffix);

    http_comm_handler http_comm(PRODUCT_NAME_UTF8, address.c_str(), user.c_str(), pswd.c_str(), port);
    
    if (http_comm.Init())
    {
        std::string response;

        if (http_comm.ExecuteGetWithResponseGZ(url_suffix.c_str(), proto == DL_NET_PROTO_HTTPS, response))
        {
            rapidjson::LookaheadParser r(&response[0]);
            
            if (r.IsValid())
            {
                r.EnterArray();
                while (r.NextArrayValue()) 
                {
                    xtream_codes_series_t series_info;

                    r.EnterObject();
                    while (const char* prop_key = r.NextObjectKey())
                    {
                        if (strcmp(prop_key, "series_id") == 0 && r.PeekType() != rapidjson::kNullType)
                            series_info.id_ = r.GetInt();
                        else if (strcmp(prop_key, "name") == 0 && r.PeekType() != rapidjson::kNullType)
                            series_info.name_ = r.GetString();
                        else if (strcmp(prop_key, "cover") == 0 && r.PeekType() != rapidjson::kNullType)
                            series_info.cover_ = r.GetString();
                        else if (strcmp(prop_key, "plot") == 0 && r.PeekType() != rapidjson::kNullType)
                            series_info.plot_ = r.GetString();
                        else if (strcmp(prop_key, "cast") == 0 && r.PeekType() != rapidjson::kNullType)
                        {
                            series_info.cast_ = r.GetString();
                            boost::replace_all(series_info.cast_, ",", " /");
                        }
                        else if (strcmp(prop_key, "director") == 0 && r.PeekType() != rapidjson::kNullType)
                        {
                            series_info.director_ = r.GetString();
                            boost::replace_all(series_info.director_, ",", " /");
                        }
                        else if (strcmp(prop_key, "genre") == 0 && r.PeekType() != rapidjson::kNullType)
                            series_info.genre_ = r.GetString();
                        else if (strcmp(prop_key, "rating_5based") == 0 && r.PeekType() != rapidjson::kNullType)
                            series_info.stars_ = r.GetDouble();
                        else if (strcmp(prop_key, "releaseDate") == 0 && r.PeekType() != rapidjson::kNullType)
                        {
                            std::string release_date = r.GetString();
                            struct tm dt_tm;
                            if (dl_strptime(release_date.c_str(), "%Y-%m-%d", &dt_tm) != NULL)
                                series_info.year_ = dt_tm.tm_year + 1900;
                        } else
                        {
                            r.SkipValue();
                        }
                    }
                    series[series_info.get_id()] = series_info;
                }
            } else
            {
                log_error(L"xtream_codes_scanner_t::read_category_series. Failed to deserialize json response");
            }
        } else 
        {
            log_error(L"xtream_codes_scanner_t::read_category_series. ExecuteGetWithResponseGZ failed");
        }
    }
    log_info(L"xtream_codes_scanner_t::read_category_series. Read %1% series for category %2%") %  series.size() % string_cast<EC_UTF8>(category_id);
}

void xtream_codes_scanner_t::add_info_series_to_item(const xtream_codes_scanner_t::xtream_codes_series_t& series_item, vod_item_obj_t& item)
{
    if (item->video_info_.m_Name.empty())
        item->video_info_.m_Name = series_item.name_;
    if (item->video_info_.m_ImageURL.empty())
        item->video_info_.m_ImageURL = series_item.cover_;
    if (item->video_info_.m_ShortDesc.empty())
        item->video_info_.m_ShortDesc = series_item.plot_;
    if (item->video_info_.m_Actors.empty())
        item->video_info_.m_Actors = series_item.cast_;
    if (item->video_info_.m_Directors.empty())
        item->video_info_.m_Directors = series_item.director_;
    if (item->video_info_.m_Categories.empty())
        item->video_info_.m_Categories = series_item.genre_;
    if (item->video_info_.m_Year == 0)
        item->video_info_.m_Year = series_item.year_;
    if (item->video_info_.m_StarNum == 0)
    {
        item->video_info_.m_StarNum = series_item.stars_;
        if (item->video_info_.m_StarNum > 0)
            item->video_info_.m_StarNumMax = 5;
    }
    
}

double get_rating_as_double(const char* value)
{
    std::istringstream iss (value);
    iss.imbue(std::locale("C"));
    double d = 0;
    try {
        iss >> d;
    } catch(...){d = 0;}

    return d;
}

void xtream_codes_scanner_t::read_series_items(const xtream_codes_info_t& info, bool* exit_flag, const xtream_codes_series_t& series_item, vod_item_obj_map_t& items)
{
    std::string address;
    std::string user;
    std::string pswd;
    unsigned short port;
    std::string url_suffix;
    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(info.get_series_info_url(series_item.get_id()).to_string().c_str(), address, user, pswd, port, url_suffix);

    http_comm_handler http_comm(PRODUCT_NAME_UTF8, address.c_str(), user.c_str(), pswd.c_str(), port);
    
    if (http_comm.Init())
    {
        std::string response;

        if (http_comm.ExecuteGetWithResponseGZ(url_suffix.c_str(), proto == DL_NET_PROTO_HTTPS, response))
        {
            rapidjson::LookaheadParser r(&response[0]);
            
            if (r.IsValid())
            {
                r.EnterObject();
                while (const char* key = r.NextObjectKey()) 
                {        
                    if (strcmp(key, "episodes") == 0) 
                    {
                        r.EnterObject();
                        while (const char* episode_key = r.NextObjectKey())
                        {
                            if (r.PeekType() == rapidjson::kArrayType)
                            {
                                r.EnterArray();
                                while (r.NextArrayValue()) 
                                {
                                    vod_item_obj_t item = boost::shared_ptr<vod_item_t>(new vod_item_t());
                                    std::string extension;

                                    r.EnterObject();
                                    while (const char* prop_key = r.NextObjectKey())
                                    {

                                        if (strcmp(prop_key, "id") == 0 && r.PeekType() != rapidjson::kNullType)
                                            item->video_info_.id_ = r.GetString();
                                        else if (strcmp(prop_key, "container_extension") == 0 && r.PeekType() != rapidjson::kNullType)
                                            extension = r.GetString();
                                        else if (strcmp(prop_key, "title") == 0 && r.PeekType() != rapidjson::kNullType)
                                            item->video_info_.m_Name = r.GetString();
                                        else if (strcmp(prop_key, "episode_num") == 0 && r.PeekType() != rapidjson::kNullType)
                                            item->video_info_.m_EpisodeNum = r.GetInt();
                                        else if (strcmp(prop_key, "season") == 0 && r.PeekType() != rapidjson::kNullType)
                                            item->video_info_.m_SeasonNum = r.GetInt();
                                        else if (strcmp(prop_key, "info") == 0 && r.PeekType() != rapidjson::kNullType)
                                        {
                                            r.EnterObject();
                                            while (const char* info_key = r.NextObjectKey())
                                            {
                                                if (strcmp(info_key, "movie_image") == 0 && r.PeekType() != rapidjson::kNullType)
                                                    item->video_info_.m_ImageURL = r.GetString();
                                                else if (strcmp(info_key, "plot") == 0 && r.PeekType() != rapidjson::kNullType)
                                                    item->video_info_.m_ShortDesc = r.GetString();
                                                else if (strcmp(info_key, "name") == 0 && r.PeekType() != rapidjson::kNullType)
                                                    item->video_info_.m_SecondName = r.GetString();
                                                else if (strcmp(info_key, "duration_secs") == 0 && r.PeekType() != rapidjson::kNullType)
                                                    item->video_info_.m_Duration = r.GetInt();
                                                else if (strcmp(info_key, "releasedate") == 0 && r.PeekType() != rapidjson::kNullType)
                                                {
                                                    std::string release_date = r.GetString();
                                                    struct tm dt_tm;
                                                    if (dl_strptime(release_date.c_str(), "%Y-%m-%d", &dt_tm) != NULL)
                                                        item->video_info_.m_Year = dt_tm.tm_year + 1900;
                                                }
                                                else if (strcmp(info_key, "rating") == 0 && r.PeekType() != rapidjson::kNullType)
                                                {
                                                    double d = 0;
                                                    if (r.PeekType() == rapidjson::kStringType)
                                                        d = get_rating_as_double(r.GetString());
                                                    else if (r.PeekType() == rapidjson::kNumberType)
                                                        d = r.GetDouble();

                                                    item->video_info_.m_StarNum = d / 2;
                                                    if (item->video_info_.m_StarNum > 0)
                                                        item->video_info_.m_StarNumMax = 5;
                                                } else
                                                {
                                                    r.SkipValue();
                                                }
                                            }
                                        } else
                                        {
                                            r.SkipValue();
                                        }


                                    }

                                    add_info_series_to_item(series_item, item);

                                    item->url_ = info.get_series_stream_url(item->video_info_.id_, extension);
                                    items[item->video_info_.id_] = item;
                                }

                            } else
                            {
                                r.SkipValue();
                            }
                        }
                    } else
                    {
                        r.SkipValue();
                    }
                }
            } else
            {
                log_error(L"xtream_codes_scanner_t::read_series_items. Failed to deserialize json response");
            }
        } else 
        {
            log_error(L"xtream_codes_scanner_t::read_series_items. ExecuteGetWithResponseGZ failed");
        }
    }
    log_info(L"xtream_codes_scanner_t::read_series_items. Read %1% items for series %2%") %  items.size() % string_cast<EC_UTF8>(series_item.get_id());
}

void xtream_codes_scanner_t::read_vod_item_info(const xtream_codes_info_t& info, bool* exit_flag, const std::string& item_id, vod_item_obj_t& item)
{
    std::string address;
    std::string user;
    std::string pswd;
    unsigned short port;
    std::string url_suffix;
    EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(info.get_vod_info_url(item_id).to_string().c_str(), address, user, pswd, port, url_suffix);

    http_comm_handler http_comm(PRODUCT_NAME_UTF8, address.c_str(), user.c_str(), pswd.c_str(), port);
    
    if (http_comm.Init())
    {
        std::string response;

        if (http_comm.ExecuteGetWithResponseGZ(url_suffix.c_str(), proto == DL_NET_PROTO_HTTPS, response))
        {
            rapidjson::LookaheadParser r(&response[0]);
            
            if (r.IsValid())
            {
                r.EnterObject();
                while (const char* key = r.NextObjectKey()) 
                {        
                    if (strcmp(key, "info") == 0) 
                    {
                        r.EnterObject();
                        while (const char* info_key = r.NextObjectKey())
                        {
                            if (strcmp(info_key, "movie_image") == 0 && r.PeekType() != rapidjson::kNullType)
                                item->video_info_.m_ImageURL = r.GetString();
                            else if (strcmp(info_key, "plot") == 0 && r.PeekType() != rapidjson::kNullType)
                                item->video_info_.m_ShortDesc = r.GetString();
                            else if (strcmp(info_key, "name") == 0 && r.PeekType() != rapidjson::kNullType)
                                item->video_info_.m_SecondName = r.GetString();
                            else if (strcmp(info_key, "duration_secs") == 0 && r.PeekType() != rapidjson::kNullType)
                                item->video_info_.m_Duration = r.GetInt();
                            else if (strcmp(info_key, "cast") == 0 && r.PeekType() != rapidjson::kNullType)
                            {
                                item->video_info_.m_Actors = r.GetString();
                                boost::replace_all(item->video_info_.m_Actors, ",", " /");
                            }
                            else if (strcmp(info_key, "director") == 0 && r.PeekType() != rapidjson::kNullType)
                            {
                                item->video_info_.m_Directors = r.GetString();
                                boost::replace_all(item->video_info_.m_Directors, ",", " /");
                            }
                            else if (strcmp(info_key, "genre") == 0 && r.PeekType() != rapidjson::kNullType)
                                item->video_info_.m_Categories = r.GetString();

                            else if (strcmp(info_key, "releasedate") == 0 && r.PeekType() != rapidjson::kNullType)
                            {
                                std::string release_date = r.GetString();
                                struct tm dt_tm;
                                if (dl_strptime(release_date.c_str(), "%Y-%m-%d", &dt_tm) != NULL)
                                    item->video_info_.m_Year = dt_tm.tm_year + 1900;
                            }
                            else if (strcmp(info_key, "rating") == 0 && r.PeekType() != rapidjson::kNullType)
                            {
                                double d = 0;
                                if (r.PeekType() == rapidjson::kStringType)
                                    d = get_rating_as_double(r.GetString());
                                else if (r.PeekType() == rapidjson::kNumberType)
                                    d = r.GetDouble();

                                item->video_info_.m_StarNum = d / 2;
                                if (item->video_info_.m_StarNum > 0)
                                    item->video_info_.m_StarNumMax = 5;
                            } else
                            {
                                r.SkipValue();
                            }
                        }
                    } else
                    {
                        r.SkipValue();
                    }
                }
            } else
            {
                log_error(L"xtream_codes_scanner_t::read_vod_item_info. Failed to deserialize json response");
            }
        } else 
        {
            log_error(L"xtream_codes_scanner_t::read_vod_item_info. ExecuteGetWithResponseGZ failed");
        }
    }
}
