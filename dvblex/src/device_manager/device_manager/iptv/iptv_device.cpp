/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include <drivers/tuner_factory.h>
#include "../epg_scanner.h"
#include "iptv_device.h"
#include "iptv_channel_scanner.h"
#include "iptv_program_streamer.h"
#include "iptv_epg_scanner.h"
#include "iptv_scan_params.h"
#include "xtream_codes.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

iptv_device_t::iptv_device_t(iptv_device_props_t* iptv_device_props, directory_settings_obj_t& dir_settings) :
    device_t(), iptv_device_props_(iptv_device_props), dir_settings_(dir_settings)
{
    background_epg_scanner_ = new iptv_epg_scanner_t(dir_settings_, iptv_device_props_);
}

iptv_device_t::~iptv_device_t()
{
    delete background_epg_scanner_;
    delete iptv_device_props_;
}

channel_scanner_t* iptv_device_t::get_channel_scanner()
{
    return new iptv_channel_scanner_t();
}

void iptv_device_t::release_channel_scanner(channel_scanner_t* scanner)
{
    delete scanner;
}

program_streamer_t* iptv_device_t::get_program_streamer(const dvblink::channel_id_t& channel_id, const concise_channel_tune_info_t& tune_params)
{
    return new iptv_program_streamer_t(iptv_device_props_, channel_id, tune_params);
}

void iptv_device_t::release_program_streamer(program_streamer_t* streamer)
{
    delete streamer;
}

epg_scanner_t* iptv_device_t::get_background_epg_scanner()
{
    return background_epg_scanner_;
}

epg_scanner_t* iptv_device_t::get_epg_scanner()
{
    //dummy scanner. IPTV source uses background epg scanner
    return new epg_scanner_t();
}

void iptv_device_t::release_epg_scanner(epg_scanner_t* epg_scanner)
{
    delete epg_scanner;
}

bool iptv_device_t::get_channel_url_for_format(const concise_channel_tune_info_t& tune_params, device_channel_stream_type_e format, 
                                               dvblink::url_address_t& url, dvblink::mime_type_t& mime)
{
    bool ret_val = false;

    std::string tuning_info = get_iptv_scan_element_from_scan_params(tune_params.scanner_settings_).to_string();

    //check if url is for xtream-codes server
    xtream_codes_scanner_t xcs;
    xtream_codes_info_t sci;
    if (xcs.get_info_from_scan_element(tuning_info, sci) || xcs.get_info_from_playlist_url(tuning_info, sci))
    {
        //xtream codes server
        if (format == dcst_chromecast)
            ret_val = sci.get_chromecast_channel_url(tune_params.tuning_params_.get(), url, mime);
        else
            ret_val = sci.get_raw_channel_url(tune_params.tuning_params_.get(), url, mime);
    } else
    {
        //return original url in hope that it is chromecast compatible
        url = tune_params.tuning_params_.to_string();
        mime = "video/mpeg";
        ret_val = true;
    }

    return ret_val;
}


