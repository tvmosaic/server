/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <dl_types.h>
#include <dl_parameters.h>
#include "xtream_codes.h"

namespace dvblex { 

static const std::string iptv_manual_url_provider = "a1ffb184a8b0";
static const std::string iptv_url_entry_field_key = "a1ffb184a8b1";
static const std::string iptv_hdhomerun_provider = "a1ffb184a8b2";
static const std::string iptv_hdhomerun_addr_field_key = "a1ffb184a8b3";
static const std::string iptv_file_playlist_key = "a1ffb184a8b4";
static const std::string iptv_xtream_codes_provider = "a1ffb184a8b5";
static const std::string iptv_xtream_codes_url_field_key = "a1ffb184a8b6";
static const std::string iptv_xtream_codes_user_field_key = "a1ffb184a8b7";
static const std::string iptv_xtream_codes_pswd_field_key = "a1ffb184a8b8";

inline void get_xtream_codes_info_from_scanner(const concise_param_map_t& scanner_settings, xtream_codes_info_t& xtream_codes_info)
{
    xtream_codes_info.url_ = get_value_for_param(iptv_xtream_codes_url_field_key, scanner_settings).to_string();
    xtream_codes_info.user_ = get_value_for_param(iptv_xtream_codes_user_field_key, scanner_settings).to_string();
    xtream_codes_info.password_ = get_value_for_param(iptv_xtream_codes_pswd_field_key, scanner_settings).to_string();
}

inline dvblink::scan_element_t get_iptv_scan_element_from_scan_params(const concise_param_map_t& scan_params)
{
    dvblink::scan_element_t ret_val;

    if (is_key_present(iptv_url_entry_field_key, scan_params))
    {
        ret_val = get_value_for_param(iptv_url_entry_field_key, scan_params).get();
    } else
    if (is_key_present(iptv_hdhomerun_addr_field_key, scan_params))
    {
        dvblink::parameter_value_t hdhr_ip = get_value_for_param(iptv_hdhomerun_addr_field_key, scan_params).get();
        std::stringstream strbuf;
        strbuf << "http://" << hdhr_ip.to_string() << "/lineup.xml";
        ret_val = strbuf.str();
    } else
    if (is_key_present(iptv_xtream_codes_url_field_key, scan_params))
    {
        xtream_codes_info_t xci;
        get_xtream_codes_info_from_scanner(scan_params, xci);

        xtream_codes_scanner_t xcs;
        ret_val = xcs.get_scan_element_from_info(xci);
    } else
    if (is_key_present(iptv_file_playlist_key, scan_params))
    {
        ret_val = get_value_for_param(iptv_file_playlist_key, scan_params).get();
    }

    return ret_val;
}

}
