/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/filesystem.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_hash.h>
#include <dl_filesystem_path.h>
#include <dl_xmltv_file_processor.h>
#include <dl_installation_settings.h>
#include "iptv_provider.h"
#include "iptv_epg_scanner.h"

using namespace dvblex;
using namespace dvblex::settings;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

iptv_epg_scanner_t::iptv_epg_scanner_t(directory_settings_obj_t& dir_settings, iptv_device_props_t* props) :
    epg_scanner_t(), props_(props), dir_settings_(dir_settings)
{
}

iptv_epg_scanner_t::~iptv_epg_scanner_t()
{
}

bool iptv_epg_scanner_t::start()
{
    return true;
}

void iptv_epg_scanner_t::stop()
{
}

bool iptv_epg_scanner_t::do_scan(const concise_channel_tune_info_t& tuning_params)
{
    iptv_device_props_t::xmltv_url_list_t url_list = props_->get_xmltv_url();

    iptv_provider_t iptv_provider;

    dvblink::url_address_t xmltv_url;
    if (url_list.size() == 0 && iptv_provider.get_default_xmltv_url(tuning_params.scanner_settings_, xmltv_url))
        url_list.insert(url_list.begin(), xmltv_url);

    if (url_list.size() > 0)
    {
        std::string log_str;
        playlist_channel_map_t channel_map;
        if (iptv_provider.scan_channels(tuning_params.scanner_settings_, channel_map, log_str))
        {
            //fill in xmltv_channel_info_map_t* chinfo_map for time offset
            xmltv_channel_info_map_t ch_info_map;
            playlist_channel_map_t::iterator ch_it = channel_map.begin();
            while (ch_it != channel_map.end())
            {
                std::string tvgid_id;
                time_t offset = 0;
                if (ch_it->second.get_channel_epg_offset(offset, tvgid_id))
                {
                    xmltv_channel_info ci;
                    ci.offset = offset / 60;
                    ci.id = tvgid_id;
                    ch_info_map[tvgid_id] = ci;
                }
                ++ch_it;
            }

            //read keywords_map
            xmltv_category_keywords_t keywords_map;
            const common_file_item_desc_t& cat_defs = dir_settings_->get_install_settings()->get_xmltv_cat_definitions();
            ReadCategoryKeywords(cat_defs.file_, &keywords_map);

            //download and parse xmltv file
            boost::uuids::uuid new_guid = boost::uuids::random_generator()();
            dvblink::filesystem_path_t temp_dir = dir_settings_->get_install_settings()->get_temp_path() / boost::lexical_cast<std::string>(new_guid);

            try {
                boost::filesystem::create_directories(temp_dir.to_boost_filesystem());
            } catch(...) {}

            epg_source_channel_map_t epg_channels_map;
            map_epg_channel_to_epg_t epg_events_map;
            xmltv_file_processor xmltv_file_proc(keywords_map, epg_channels_map, epg_events_map);

            for (size_t i=0; i<url_list.size(); i++)
            {
                if (!url_list[i].empty())
                {
                    //check if this is a local file
                    if (!xmltv_file_proc.read_xmltv_file(dvblink::filesystem_path_t(url_list[i].get()), &ch_info_map, exit_flag_))
                        xmltv_file_proc.process_internet_file(url_list[i], temp_dir, &ch_info_map, exit_flag_);
                }
            }

            playlist_channel_map_t::iterator it = channel_map.begin();
            while (it != channel_map.end() && !exit_flag_)
            {
                if (it->second.tags.find(m3u_plus_tvg_id) != it->second.tags.end())
                {
                    std::string tvgid_id = boost::to_lower_copy(it->second.tags[m3u_plus_tvg_id]);

                    if (epg_events_map.find(tvgid_id) != epg_events_map.end())
                    {
                        //report epg
                        report_epg(it->second.get_channel_id(), epg_events_map[tvgid_id]);
                    }
                }

                ++it;
            }

            //remove temp dir when we are finished
            try {
                boost::filesystem::remove_all(temp_dir.to_boost_filesystem());
            } catch(...) {}

        }
    }

    return true;
}

void iptv_epg_scanner_t::get_scan_transponders(const dvblink::epg_channel_tune_info_list_t& channels, channel_tuning_params_list_t& transponders)
{
    //return tuning info of the first channel in a list - they are all the same for epg scan
    if (channels.size() > 0)
        transponders.push_back(channels[0].tuning_params_);
}

