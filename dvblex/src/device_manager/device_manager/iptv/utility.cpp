/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <string.h>
#ifndef _WIN32
#include <sys/time.h>
#endif
#include "utility.h"

///<summary>
/// Returns a pointer to a new string, which is a duplicate of the string pointed to by str
///</summary>
char* utility::StrDup(char const* str)
{
    char* copy = NULL;
    if (str)
    {
        size_t len = strlen(str) + 1;
        copy = new char[len];
        if (copy)
        {
            strcpy(copy, str);
        }
    }

    return copy;
}

///<summary>
/// Returns a new string with the size of the original string
///</summary>
char* utility::StrDupSize(char const* str)
{
    if (str == NULL)
    {
        return NULL;
    }
    size_t len = strlen(str) + 1;
    char* copy = new char[len];

    return copy;
}

int utility::GetTimeOfDay(timeval *tv)
{
#ifdef _WIN32
    FILETIME ft;
    unsigned __int64 tmpres = 0;
    static int tzflag;

    if (NULL != tv)
    {
        GetSystemTimeAsFileTime(&ft);

        tmpres |= ft.dwHighDateTime;
        tmpres <<= 32;
        tmpres |= ft.dwLowDateTime;

        /*converting file time to unix epoch*/
        tmpres /= 10;  /*convert into microseconds*/
        tmpres -= 11644473600000000Ui64;
        tv->tv_sec = (long)(tmpres / 1000000UL);
        tv->tv_usec = (long)(tmpres % 1000000UL);
    }
    return 0;
#else
    return gettimeofday(tv, NULL);
#endif
}

double utility::GetTimeNow()
{
    timeval timeNow;
    utility::GetTimeOfDay(&timeNow);
    return (double) (timeNow.tv_sec + timeNow.tv_usec/1000000.0);
}

unsigned int utility::Random32()
{
    return (rand() << 16) | rand();
}
