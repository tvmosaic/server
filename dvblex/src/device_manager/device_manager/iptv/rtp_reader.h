/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <dl_circle_buffer.h>
#include <dl_ts_info.h>
#include <dl_ts_aligner.h>
#include "spts_converter.h"
#include "udp_reader.h"
#include "rtp_reception_stats.h"

class media_subsession;

namespace dvblex { 

enum EIPTV_RTP_PAYLOAD_TYPE
{
    IRPT_MPEG2_TS,
    IRPT_H264
};

class rtp_reader : public udp_reader
{
public:
    rtp_reader(media_subsession* subsession, dvblink::engine::ts_circle_buffer* input_stream);
    ~rtp_reader();

    rtp_reception_stats_db& ReceptionStatsDB() const { return *reception_stats_db_; }
    unsigned int SSRC() const { return ssrc_; }

private:
    int GetRTPBuffer(unsigned char* buffer, int bufLen, unsigned char** payload);
    virtual void stream_receive_function();

    void ProcessSessionParams();
    void ProcessPayload(unsigned char* payload, int len);
    void ProcessNAL(unsigned char* payload, int len);
    unsigned char* ProcessH264Payload(unsigned char* payload, int len, int& nal_len);
    void ProcessStream(unsigned char* data, int data_len);
    void ProcessH264NAL(unsigned char* data, int len);
    void CreateStreamDescriptionPackets();
    unsigned short GetVideoPid(unsigned short base_pid){return base_pid;}
    unsigned short GetPMTPid(unsigned short base_pid){return base_pid + 1;}

    static void StreamSendFunc(const unsigned char* buf, int len, void* param);

private:
    unsigned short video_cont_counter_;
    unsigned short pat_cont_counter_;
    unsigned short pmt_cont_counter_;
    unsigned short pid_;
    unsigned long rtp_timestamp_;
    rtp_reception_stats_db* reception_stats_db_;
    unsigned int rtp_timestamp_freq_;
    unsigned int ssrc_;
    media_subsession* sub_session_;
    EIPTV_RTP_PAYLOAD_TYPE  payload_type_;
    std::vector<std::string> prop_sets_;
    std::vector<unsigned char> nal_accu_buffer_;
    dvblink::engine::ts_packet_generator* packet_gen_;

    dvblink::engine::ts_packet_aligner aligner_;
    iptv_spts_converter_t iptv_spts_converter_;

    static void __stdcall aligner_callback(const unsigned char* buf, unsigned long len, void* user_param);
    static void iptv_spts_converter_callback(const unsigned char* buf, size_t len, void* user_param);
};

}
