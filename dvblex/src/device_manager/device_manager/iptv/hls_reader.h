/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread.hpp>
#include <dl_circle_buffer.h>
#include <m3u8segmentlist.h>
#include <m3u8loader.h>
#include <iphone_segment_streamer.h>
#include "net_reader.h"

namespace dvblex { 

class hls_reader : public net_reader
{
public:
    hls_reader(dvblink::engine::ts_circle_buffer* inut_stream, const char* user_agent);
    ~hls_reader();

    bool create(const char* url, const char* local_address);
    bool start(int restart_count);
    void stop();

protected:
    dvblink::engine::ts_circle_buffer* output_buffer_;
    std::string url_;

    m3u8_segment_list m_SegmentList;
	m3u8_loader* m_PlaylistLoader;
	hls_segment_streamer* m_SegmentStreamer;

	static void StreamingCallback(unsigned char* buf, int len, void* param);
};

}
