/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <winsock2.h>
#include <hash_map>
#include <boost/thread/mutex.hpp>

class CRTPReceptionStats 
{
public:
	CRTPReceptionStats(unsigned int SSRC, unsigned short initialSeqNum);
	CRTPReceptionStats(unsigned int SSRC);
	~CRTPReceptionStats();

	unsigned int SSRC() const { return m_SSRC; }
	unsigned NumPacketsReceivedSinceLastReset() const { return m_NumPacketsReceivedSinceLastReset; }
	unsigned TotNumPacketsReceived() const { return m_TotNumPacketsReceived; }
	double TotNumKBytesReceived() const;

	unsigned TotNumPacketsExpected() const { return m_HighestExtSeqNumReceived - m_BaseExtSeqNumReceived; }

	unsigned BaseExtSeqNumReceived() const { return m_BaseExtSeqNumReceived; }
	unsigned LastResetExtSeqNumReceived() const { return m_LastResetExtSeqNumReceived; }
	unsigned HighestExtSeqNumReceived() const { return m_HighestExtSeqNumReceived; }

	unsigned Jitter() const;
	
	unsigned LastReceivedSRNTPmsw() const { return m_LastReceivedSR_NTPmsw; }
	unsigned LastReceivedSRNTPlsw() const { return m_LastReceivedSR_NTPlsw; }
	struct timeval const& LastReceivedSRTime() const { return m_LastReceivedSR_time; }

	unsigned MinInterPacketGapUS() const { return m_MinInterPacketGapUS; }
	unsigned MaxInterPacketGapUS() const { return m_MaxInterPacketGapUS; }
	struct timeval const& TotalInterPacketGaps() const { return m_TotalInterPacketGaps; }

	void NoteIncomingPacket(unsigned short seqNum, unsigned int rtpTimestamp,
		unsigned timestampFrequency,
		bool useForJitterCalculation,
		struct timeval& resultPresentationTime,
		bool& resultHasBeenSyncedUsingRTCP,
		unsigned packetSize /* payload only */);
		
	void NoteIncomingSR(unsigned int ntpTimestampMSW, unsigned int ntpTimestampLSW,
		unsigned int rtpTimestamp);
	void Init(unsigned int SSRC);
	void InitSeqNum(unsigned short initialSeqNum);
	// resets periodic stats (called each time they're used to
	// generate a reception report)
	void Reset();
	bool SeqNumLT(unsigned short s1, unsigned short s2);

private:
	unsigned int   m_SSRC;
	unsigned       m_NumPacketsReceivedSinceLastReset;
	unsigned       m_TotNumPacketsReceived;
	unsigned int   m_TotBytesReceived_hi;
	unsigned int   m_TotBytesReceived_lo;
	bool           m_HaveSeenInitialSequenceNumber;
	unsigned       m_BaseExtSeqNumReceived;
	unsigned       m_LastResetExtSeqNumReceived;
	unsigned       m_HighestExtSeqNumReceived;
	int            m_LastTransit; // used in the jitter calculation
	unsigned int   m_PreviousPacketRTPTimestamp;
	double         m_Jitter;
	// The following are recorded whenever we receive a RTCP SR for this SSRC:
	unsigned       m_LastReceivedSR_NTPmsw; // NTP timestamp (from SR), most-signif
	unsigned       m_LastReceivedSR_NTPlsw; // NTP timestamp (from SR), least-signif
	struct timeval m_LastReceivedSR_time;
	struct timeval m_LastPacketReceptionTime;
	unsigned       m_MinInterPacketGapUS;
	unsigned       m_MaxInterPacketGapUS;
	struct timeval m_TotalInterPacketGaps;

	// Used to convert from RTP timestamp to 'wall clock' time
	bool           m_HasBeenSynchronized;
	unsigned int   m_SyncTimestamp;
	struct timeval m_SyncTime;
};

typedef stdext::hash_map<unsigned long, CRTPReceptionStats*> TRTPReceptionStats;

class CRTPReceptionStatsDB 
{
public:
	CRTPReceptionStatsDB();
	~CRTPReceptionStatsDB();

	unsigned TotNumPacketsReceived() const { return m_TotNumPacketsReceived; }
	unsigned NumActiveSourcesSinceLastReset() const { return m_NumActiveSourcesSinceLastReset; }

	// resets periodic stats (called each time they're used to
	// generate a reception report)
	void Reset();
	
	// The following is called whenever a RTP packet is received:
	void NoteIncomingPacket(unsigned int SSRC, unsigned short seqNum,
		unsigned int rtpTimestamp,
		unsigned timestampFrequency,
		bool useForJitterCalculation,
		struct timeval& resultPresentationTime,
		bool& resultHasBeenSyncedUsingRTCP,
		unsigned packetSize /* payload only */);

	// The following is called whenever a RTCP SR packet is received:
	void NoteIncomingSR(unsigned int SSRC,
		unsigned int ntpTimestampMSW, 
		unsigned int ntpTimestampLSW,
		unsigned int rtpTimestamp);
		
	
	void Lock(){m_lock.lock();};
	void Unlock(){m_lock.unlock();};
	
	TRTPReceptionStats m_ReceptionStats;

private:
	CRTPReceptionStats* Lookup(unsigned int SSRC) const;
	void RemoveRecord(unsigned int SSRC);
	void Add(unsigned int SSRC, CRTPReceptionStats* stats);

	unsigned m_NumActiveSourcesSinceLastReset;
	unsigned m_TotNumPacketsReceived; // for all SSRCs
	boost::mutex m_lock;
};