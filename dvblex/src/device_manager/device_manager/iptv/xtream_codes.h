/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <map>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>

#include <dl_types.h>
#include "playlist_reader.h"
#include "iptv_vod_items.h"

namespace dvblex { 

static const std::string xtream_codes_info_magic = "XTREAM_CODES";

struct xtream_codes_info_t
{
    xtream_codes_info_t() :
        magic_(xtream_codes_info_magic)
    {}

    std::string get_description() {return user_ + "@" + url_;}
    bool is_valid() {return boost::equals(magic_, xtream_codes_info_magic);}

    std::string get_command_url_stem() const
    {
        const char* sym = boost::ends_with(url_, "/") ? "" : "/";
        std::stringstream strbuf;
        strbuf << url_ << sym << "player_api.php?username=" << user_ << "&password=" << password_;

        return strbuf.str();
    }

    dvblink::url_address_t get_live_streams_url() const
    {
        std::stringstream strbuf;
        strbuf << get_command_url_stem() << "&action=get_live_streams";

        return strbuf.str();
    }

    dvblink::url_address_t get_live_categories_url() const
    {
        std::stringstream strbuf;
        strbuf << get_command_url_stem() << "&action=get_live_categories";

        return strbuf.str();
    }

    dvblink::url_address_t get_live_stream_url(int stream_id) const
    {
        const char* sym = boost::ends_with(url_, "/") ? "" : "/";
        std::stringstream strbuf;
        strbuf << url_ << sym << "live/" << user_ << "/" << password_ << "/" << stream_id << ".ts";

        return strbuf.str();
    }

    dvblink::url_address_t get_vod_categories_url() const
    {
        std::stringstream strbuf;
        strbuf << get_command_url_stem() << "&action=get_vod_categories";

        return strbuf.str();
    }

    dvblink::url_address_t get_vod_category_streams_url(const std::string& category_id) const
    {
        std::stringstream strbuf;
        strbuf << get_command_url_stem() << "&action=get_vod_streams&category_id=" << category_id;

        return strbuf.str();
    }

    dvblink::url_address_t get_vod_info_url(const std::string& stream_id) const
    {
        std::stringstream strbuf;
        strbuf << get_command_url_stem() << "&action=get_vod_info&vod_id=" << stream_id;

        return strbuf.str();
    }

    dvblink::url_address_t get_category_series_url(const std::string& category_id) const
    {
        std::stringstream strbuf;
        strbuf << get_command_url_stem() << "&action=get_series&category_id=" << category_id;

        return strbuf.str();
    }

    dvblink::url_address_t get_series_info_url(const std::string& series_id) const
    {
        std::stringstream strbuf;
        strbuf << get_command_url_stem() << "&action=get_series_info&series_id=" << series_id;

        return strbuf.str();
    }

    dvblink::url_address_t get_vod_stream_url(int stream_id, const std::string& ext) const
    {
        const char* sym = boost::ends_with(url_, "/") ? "" : "/";
        std::stringstream strbuf;
        strbuf << url_ << sym << "movie/" << user_ << "/" << password_ << "/" << stream_id << "." << ext;

        return strbuf.str();
    }

    dvblink::url_address_t get_series_categories_url() const
    {
        std::stringstream strbuf;
        strbuf << get_command_url_stem() << "&action=get_series_categories";

        return strbuf.str();
    }

    dvblink::url_address_t get_series_stream_url(const std::string& stream_id, const std::string& ext) const
    {
        const char* sym = boost::ends_with(url_, "/") ? "" : "/";
        std::stringstream strbuf;
        strbuf << url_ << sym << "series/" << user_ << "/" << password_ << "/" << stream_id << "." << ext;

        return strbuf.str();
    }

    dvblink::url_address_t get_xmltv_url() const
    {
        const char* sym = boost::ends_with(url_, "/") ? "" : "/";
        std::stringstream strbuf;
        strbuf << url_ << sym << "xmltv.php?username=" << user_ << "&password=" << password_;

        return strbuf.str();
    }

    bool get_chromecast_channel_url(const std::string& ts_url, dvblink::url_address_t& chromecast_url, dvblink::mime_type_t& mime)
    {
        bool ret_val = false;
        if (boost::ends_with(ts_url, ".ts"))
        {
            chromecast_url = boost::replace_all_copy(ts_url, ".ts", ".m3u8");
            mime = "application/x-mpegURL";
            ret_val = true;
        }
        
        return ret_val;
    }

    bool get_raw_channel_url(const std::string& ts_url, dvblink::url_address_t& chromecast_url, dvblink::mime_type_t& mime)
    {
        chromecast_url = ts_url;
        mime = "video/mpeg";
        
        return true;
    }

    std::string magic_;
    std::string url_;
    std::string user_;
    std::string password_;
};

class xtream_codes_scanner_t
{
public:
    //categories
    struct xtream_codes_cat_t
    {
        xtream_codes_cat_t()
        {}

        xtream_codes_cat_t(const std::string& id, const std::string& name)
            : id_(id), name_(name)
        {}

        std::string id_;
        std::string name_;
    };

    typedef std::map<std::string, xtream_codes_cat_t> xtream_codes_cat_map_t;
    typedef std::vector<xtream_codes_cat_t> xtream_codes_cat_list_t;

    //series
    struct xtream_codes_series_t
    {
        xtream_codes_series_t()
            : id_(0), year_(0), stars_(0)
        {}

        std::string get_id() const {return boost::lexical_cast<std::string>(id_);}

        int id_;
        std::string name_;
        std::string cover_;
        std::string plot_;
        std::string cast_;
        std::string director_;
        std::string genre_;
        long year_;
        long stars_;
    };

    typedef std::vector<xtream_codes_series_t> xtream_codes_series_list_t;
    typedef std::map<std::string, xtream_codes_series_t> xtream_codes_series_map_t;

public:
    xtream_codes_scanner_t();
	virtual ~xtream_codes_scanner_t();

    dvblink::scan_element_t get_scan_element_from_info(const xtream_codes_info_t& info);
    bool get_info_from_scan_element(const dvblink::scan_element_t& se, xtream_codes_info_t& info);
    
    bool get_info_from_playlist_url(const dvblink::url_address_t& url, xtream_codes_info_t& info);

    bool scan_live_channels(const xtream_codes_info_t& info, playlist_channel_map_t& channel_map);

    void read_vod_categories(const xtream_codes_info_t& sci, bool vod, bool* exit_flag, xtream_codes_cat_list_t& vod_categories);
    void read_vod_category_items(const xtream_codes_info_t& info, bool* exit_flag, const std::string& category_id, vod_item_obj_map_t& items);
    void read_vod_item_info(const xtream_codes_info_t& info, bool* exit_flag, const std::string& item_id, vod_item_obj_t& item);
    void read_category_series(const xtream_codes_info_t& info, bool* exit_flag, const std::string& category_id, xtream_codes_series_map_t& series);
    void read_series_items(const xtream_codes_info_t& info, bool* exit_flag, const xtream_codes_series_t& series_item, vod_item_obj_map_t& items);

protected:
    void read_live_categories(const xtream_codes_info_t& info, xtream_codes_cat_map_t& categories);
    void add_info_series_to_item(const xtream_codes_series_t& series_item, vod_item_obj_t& item);
};

}

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dvblex::xtream_codes_info_t& xci, const unsigned int /*version*/)
{
    ar & xci.magic_;
    ar & xci.url_;
    ar & xci.user_;
    ar & xci.password_;
}

} // namespace serialization
} // namespace boost
