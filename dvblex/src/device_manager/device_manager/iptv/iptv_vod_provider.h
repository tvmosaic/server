/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <map>

#include "iptv_vod_items.h"

namespace dvblex { 

struct vod_container_t
{
    vod_container_t()
        : children_count_(-1)
    {}

    vod_container_t(const std::string& id, const std::string& name)
        : id_(id), name_(name), children_count_(-1)
    {}

    vod_container_t(const std::string& id, const std::string& name, int count)
        : id_(id), name_(name), children_count_(count)
    {}

    std::string id_;
    std::string name_;
    int children_count_;
};

typedef std::vector<vod_container_t> vod_container_list_t;

class iptv_vod_provider_t
{
protected:
    typedef std::map<std::string, time_t> update_time_map_t;

    class vod_item_name_sorter
    {
          bool* exit_;

    public:
        vod_item_name_sorter(bool* exit) 
          : exit_(exit)
        {}

        bool operator()(const vod_item_desc_t& item1, const vod_item_desc_t& item2) const 
        {
            if (*exit_)
            {
                throw -1;
            } else
            {
                return item1.name_ < item2.name_;
            }

            return 0;
        }
    };

public:
    iptv_vod_provider_t();
    virtual ~iptv_vod_provider_t();

    bool get_categories(vod_container_list_t& categories);
    bool get_category(const std::string& id, vod_container_t& category);

    bool get_groups(const std::string& container_id, vod_container_list_t& groups);
    bool get_group(const std::string& id, vod_container_t& group);

    bool get_items(const std::string& group_id, int start, int count, vod_item_obj_list_t& items, int& total);
    bool get_item(const std::string& group_id, const std::string& item_id, vod_item_obj_t& item);
    bool get_item(const std::string& item_id, vod_item_obj_t& item);

    bool search_items(const std::wstring& wsearch_string, const std::string& group_id, vod_item_obj_list_t& items);

protected:
    vod_category_map_t categories_;
    vod_group_map_t groups_;
    //cache update times
    time_t update_interval_;
    update_time_map_t container_update_time_map_; //category/group id -> time_t
    update_time_map_t items_update_time_map_; //group id -> time_t

    bool container_need_refresh(const std::string& obj_id);
    bool items_need_refresh(const std::string& group_id);

    virtual void read_containers(){}
    virtual void read_items(const std::string& group_id){}
    virtual void add_item_metadata(const std::string& group_id, const std::string& item_id, vod_item_obj_t& item){}
};

}
