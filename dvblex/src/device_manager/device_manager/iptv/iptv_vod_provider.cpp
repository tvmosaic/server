/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include "iptv_vod_provider.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

static const boost::uint32_t max_search_results_num = 200;

iptv_vod_provider_t::iptv_vod_provider_t()
    : update_interval_(6*3600)
{
}

iptv_vod_provider_t::~iptv_vod_provider_t()
{
}

bool iptv_vod_provider_t::container_need_refresh(const std::string& obj_id)
{
    time_t now;
    time(&now);
    return container_update_time_map_.find(obj_id) == container_update_time_map_.end() || container_update_time_map_[obj_id]  + update_interval_ < now;
}

bool iptv_vod_provider_t::items_need_refresh(const std::string& group_id)
{
    time_t now;
    time(&now);
    return items_update_time_map_.find(group_id) == items_update_time_map_.end() || items_update_time_map_[group_id]  + update_interval_ < now;
}

bool iptv_vod_provider_t::get_categories(vod_container_list_t& categories)
{
    if (categories_.empty() || container_need_refresh(categories_.begin()->first))
        read_containers();

    vod_category_map_t::iterator it = categories_.begin();
    while (it != categories_.end())
    {
        categories.push_back(vod_container_t(it->first, it->second.name_, it->second.groups_.size()));
        ++it;
    }

    return true;
}

bool iptv_vod_provider_t::get_category(const std::string& id, vod_container_t& category)
{
    bool ret_val = false;

    if (container_need_refresh(id))
        read_containers();

    vod_category_map_t::iterator it = categories_.find(id);
    if (it != categories_.end())
    {
        category.id_ = it->first;
        category.name_ = it->second.name_;
        category.children_count_ = it->second.groups_.size();
        ret_val = true;
    }

    return ret_val;
}

bool iptv_vod_provider_t::get_groups(const std::string& container_id, vod_container_list_t& groups)
{
    if (groups_.empty() || container_need_refresh(groups_.begin()->first))
        read_containers();

    if (categories_.find(container_id) != categories_.end())
    {
        const vod_category_t& category = categories_[container_id];
        for (size_t i=0; i<category.groups_.size(); i++)
        {
            vod_group_map_t::iterator it = groups_.find(category.groups_[i]);
            if (it != groups_.end())
                groups.push_back(vod_container_t(it->first, it->second.name_, it->second.items_.size() == 0 ? -1 : it->second.items_.size()));
        }
    }

    return true;
}

bool iptv_vod_provider_t::get_group(const std::string& id, vod_container_t& group)
{
    bool ret_val = false;

    if (container_need_refresh(id))
        read_containers();

    vod_group_map_t::iterator it = groups_.find(id);
    if (it != groups_.end())
    {
        group.id_ = it->first;
        group.name_ = it->second.name_;
        group.children_count_ = it->second.items_.size() == 0 ? -1 : it->second.items_.size();
        ret_val = true;
    }

    return ret_val;
}

bool iptv_vod_provider_t::get_items(const std::string& group_id, int start, int count, vod_item_obj_list_t& items, int& total)
{
#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

    bool ret_val = false;
    total = 0;

    if (container_need_refresh(group_id))
        read_containers();

    vod_group_map_t::iterator it = groups_.find(group_id);
    if (it != groups_.end())
    {
        if (it->second.items_.size() == 0 || items_need_refresh(group_id))
            read_items(group_id);

        boost::int32_t request_item_count = count < 0 ? it->second.items_sort_list_.size() : count;
        size_t start_idx = std::max(start, 0);
        size_t end_idx = std::min(start_idx + request_item_count, it->second.items_sort_list_.size());

        for (size_t idx=start_idx; idx<end_idx; idx++)
        {
            std::string& item_id = it->second.items_sort_list_[idx].id_;
            if (it->second.items_.find(item_id) != it->second.items_.end())
            {
//                add_item_metadata(group_id, item_id, it->second.items_[item_id]);
                items.push_back(it->second.items_[item_id]);
            }
        }

        total = it->second.items_sort_list_.size();

        ret_val = true;
    }

    return ret_val;
}

bool iptv_vod_provider_t::get_item(const std::string& item_id, vod_item_obj_t& item)
{
    bool ret_val = false;

    vod_group_map_t::iterator it = groups_.begin();
    while (it != groups_.end())
    {
        std::string group_id = it->first;

        if (it->second.items_.find(item_id) != it->second.items_.end())
        {
            item = it->second.items_[item_id];

            //try to get add item metadat directly from the provider
            //functionality depends on the provider
            add_item_metadata(group_id, item_id, item);

            ret_val = true;
            break;
        }
        ++it;
    }

    return ret_val;
}

bool iptv_vod_provider_t::get_item(const std::string& group_id, const std::string& item_id, vod_item_obj_t& item)
{
    bool ret_val = false;

    if (container_need_refresh(group_id))
        read_containers();

    vod_group_map_t::iterator it = groups_.find(group_id);
    if (it != groups_.end())
    {
        if (it->second.items_.size() == 0 || items_need_refresh(group_id))
            read_items(group_id);

        if (it->second.items_.find(item_id) != it->second.items_.end())
        {
            item = it->second.items_[item_id];

            //try to get add item metadat directly from the provider
            //functionality depends on the provider
            add_item_metadata(group_id, item_id, item);

            ret_val = true;
        }
    }

    return ret_val;
}

static bool search_contains(const std::wstring& search_text, const vod_item_desc_t& vi)
{
    return boost::icontains(string_cast<EC_UTF8>(vi.name_), search_text);
}

bool iptv_vod_provider_t::search_items(const std::wstring& wsearch_string, const std::string& group_id, vod_item_obj_list_t& items)
{
    bool ret_val = false;

    vod_group_map_t::iterator it = groups_.find(group_id);
    if (it != groups_.end())
    {
        for (size_t idx = 0; idx < it->second.items_sort_list_.size() && items.size() < max_search_results_num; idx++)
        {
            if (search_contains(wsearch_string, it->second.items_sort_list_[idx]))
            {
                std::string& item_id = it->second.items_sort_list_[idx].id_;
                if (it->second.items_.find(item_id) != it->second.items_.end())
                    items.push_back(it->second.items_[item_id]);
            }
        }

        ret_val = true;
    }

    return ret_val;
}



