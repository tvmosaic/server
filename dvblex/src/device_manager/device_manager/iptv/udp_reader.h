/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/cstdint.hpp>
#include <boost/thread.hpp>
#include <dl_socket_api.h>
#include <dl_circle_buffer.h>
#include "net_reader.h"

namespace dvblex { 

class udp_reader : public net_reader
{
public:
    udp_reader(dvblink::engine::ts_circle_buffer* input_buffer);
    virtual ~udp_reader() {}

    virtual bool create(const char* url, const char* local_address);
    virtual bool start(int restart_count);
    virtual void stop();

    bool open_connection(const char* local_address, const char* remote_address, const char* mcast_source_address, boost::uint16_t port);

protected:
    dvblink::udp_socket::sock_ptr sock_ptr_;
    dvblink::sock_addr local_addr_;
    dvblink::sock_addr remote_addr_;
    dvblink::sock_addr mcast_src_addr_;
    bool receive_started_;
    boost::thread* streaming_thread_;
    dvblink::engine::ts_circle_buffer* input_buffer_;

    void thread_function();
    virtual void stream_receive_function();
};

}
