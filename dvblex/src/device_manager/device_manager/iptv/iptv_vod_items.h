/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <map>

#include <boost/shared_ptr.hpp>

#include <dl_pb_video_info.h>
#include <dl_hash.h>

namespace dvblex { 

struct vod_item_t
{
    vod_item_t()
        : size_(0)
    {}

    std::string get_id() {return video_info_.id_; }

    dvblex::playback::pb_video_info_t video_info_;
    dvblink::url_address_t url_;
    boost::uint64_t size_;
};

struct vod_item_desc_t
{
    vod_item_desc_t()
    {}

    vod_item_desc_t(const std::string& id, const std::string& name) :
        id_(id), name_(name)
    {}

    std::string id_;
    std::string name_;
};

typedef std::vector<vod_item_desc_t> vod_item_desc_list_t;
typedef boost::shared_ptr<vod_item_t> vod_item_obj_t;
typedef std::vector<vod_item_obj_t> vod_item_obj_list_t;
typedef std::map<std::string, vod_item_obj_t> vod_item_obj_map_t;

struct vod_group_t
{
    vod_group_t()
    {}

    vod_group_t(const std::string& name)
        : name_(name)
    {}

    static std::string generate_id_from_name(const std::string& name)
    {
        boost::uint64_t hash = dvblink::engine::calculate_64bit_string_hash(name);
        std::stringstream strbuf;
        strbuf << hash;

        return strbuf.str();
    }

    std::string name_;
    vod_item_obj_map_t items_;
    vod_item_desc_list_t items_sort_list_; //items_list_ is used for sorting purposes
};

typedef std::vector<std::string> vod_group_id_list_t;

struct vod_category_t
{
    vod_category_t()
    {}

    vod_category_t(const std::string& name)
        : name_(name)
    {}

    std::string name_;
    vod_group_id_list_t groups_;
};

typedef std::map<std::string, vod_group_t> vod_group_map_t;
typedef std::map<std::string, vod_category_t> vod_category_map_t;

}
