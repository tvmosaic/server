/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include "xtream_codes.h"
#include "playlist_reader.h"
#include "iptv_scan_params.h"
#include "playlist_vod_provider.h"
#include "xcodes_vod_provider.h"
#include "iptv_provider.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

iptv_provider_t::iptv_provider_t()
{
}

iptv_provider_t::~iptv_provider_t()
{
}

bool iptv_provider_t::scan_channels(const concise_param_map_t& scanner_settings, playlist_channel_map_t& channel_map, std::string& log_str)
{
    bool scan_success = false;

    std::string tuning_info = get_iptv_scan_element_from_scan_params(scanner_settings).to_string();

    //check if url is for xtream-codes server
    xtream_codes_scanner_t xcs;
    xtream_codes_info_t sci;
    if (xcs.get_info_from_scan_element(tuning_info, sci) || xcs.get_info_from_playlist_url(tuning_info, sci))
    {
        //xtream codes server
        log_str = sci.get_description();
        log_info(L"iptv_provider_t::scan_channels. Scanning channels with Xtream Codes info %1%") % string_cast<EC_UTF8>(log_str);

        scan_success = xcs.scan_live_channels(sci, channel_map);
    } else
    {
        //playlist url
        log_str = tuning_info;
        log_info(L"iptv_provider_t::scan_channels. Scanning channels from playlist %1%") %  string_cast<EC_UTF8>(log_str);

        std::string playlist_path = tuning_info;
        playlist_reader reader(playlist_path);
        playlist_reader_result_e res = reader.get_channels(channel_map);

        if (res == prr_ok)
            scan_success = true;
        else
            log_error(L"iptv_provider_t::scan_channels. playlist_reader.get_channels() return error %1%") % res;
    }

    return scan_success;
}

bool iptv_provider_t::get_default_xmltv_url(const concise_param_map_t& scanner_settings, dvblink::url_address_t& url)
{
    bool ret_val = false;

    std::string tuning_info = get_iptv_scan_element_from_scan_params(scanner_settings).to_string();

    //check if url is for xtream-codes server
    xtream_codes_scanner_t xcs;
    xtream_codes_info_t sci;
    if (xcs.get_info_from_scan_element(tuning_info, sci) || xcs.get_info_from_playlist_url(tuning_info, sci))
    {
        url = sci.get_xmltv_url();
        ret_val = true;
    }

    return ret_val;
}

iptv_vod_provider_t* iptv_provider_t::get_iptv_vod_provider(const concise_param_map_t& scanner_settings)
{
    iptv_vod_provider_t* ret_val = NULL;

    std::string tuning_info = get_iptv_scan_element_from_scan_params(scanner_settings).to_string();

    //check if url is for xtream-codes server
    xtream_codes_scanner_t xcs;
    xtream_codes_info_t sci;
    if (xcs.get_info_from_scan_element(tuning_info, sci) || xcs.get_info_from_playlist_url(tuning_info, sci))
    {
        //xtream codes server
        log_info(L"iptv_provider_t::get_iptv_vod_provider. Initializing Xtream Codes VOD provider (%1%)") % string_cast<EC_UTF8>(sci.get_description());
        ret_val = new xcodes_vod_provider_t(sci);
    } else
    {
        log_info(L"iptv_provider_t::get_iptv_vod_provider. Initializing playlist VOD provider");
        ret_val = new playlist_vod_provider_t(tuning_info);
    }

    return ret_val;
}
