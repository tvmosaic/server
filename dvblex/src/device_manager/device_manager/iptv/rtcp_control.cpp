/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include "rtcp_control.h"
#include "rtp.h"
#include "utility.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

#define IP_UDP_HDR_SIZE             28

rtcp_control::rtcp_control(rtp_reader* rtpReader, char* cname, unsigned short serverPort) :
    udp_reader(NULL),
    rtp_reader_(rtpReader),
    cname_(RTCP_SDES_CNAME, cname),
    rtcp_timer_(NULL),
    tot_session_bw_(500),
    aver_rtcp_size_(0), 
    is_initial_(1),
    prev_report_time_(utility::GetTimeNow()),
    last_sent_size_(0),
    server_port_(serverPort)
{
}

///<summary>
/// Receive RTCP packet
///</summary>
void rtcp_control::stream_receive_function()
{
    log_info(L"CIPTVBridgeRTCPReader::stream_receive_function() started");

    std::string recv_buf(RECV_BUFSIZE, '\0');
    unsigned char* buf_ptr = (unsigned char*)&recv_buf[0];
    size_t buf_size = recv_buf.size();
    const timeout_t wait_timeout = boost::posix_time::milliseconds(100);

    // Send first report
    rtcp_timer_ = new dvblink::engine::timer_procedure<rtcp_control>(&rtcp_control::OnExpire, *this, 10, false);

    while (receive_started_ && sock_ptr_)
    {
        errcode_t err = sock_ptr_->wait_for_readable(wait_timeout);

        if (err != err_none)
        {
            continue;
        }

        dvblink::sock_addr from;
        size_t bytes_received = 0;

        err = sock_ptr_->receive_datagram(buf_ptr, buf_size,
            bytes_received, from, false);

        if (err != err_none)
        {
            log_info(L"CIPTVBridgeRTCPReader::stream_receive_function() finished");
            receive_started_ = false;
            break;
        }

        log_info(L"CIPTVBridgeRTCPReader::stream_receive_function. RTCP packet");

        if (CheckRTCPPacket(buf_ptr))
        {
            // Parse packet
            boost::mutex::scoped_lock lock(lock_);
            ParseRTCPPacket(buf_ptr, bytes_received);
        }
        else
        {
            log_warning(L"CIPTVBridgeRTCPReader::stream_receive_function() - unexpected data");
        }
    }

    // Stop timer
    boost::mutex::scoped_lock lock(lock_);

    //timeKillEvent(m_RTCPTimer);
    if (rtcp_timer_)
    {
        delete rtcp_timer_;
        rtcp_timer_ = NULL;
    }

    sock_ptr_.reset(); // close socket
}

///<summary>
/// Check RTCP packet
///</summary>
#if !defined(_SYNOLOGY_PPC)
bool rtcp_control::CheckRTCPPacket(unsigned char* buffer)
{	
    bool retVal = false;

    RTCPHEADER rtcpHeader;
    memset(&rtcpHeader, 0, sizeof(RTCPHEADER));
    memcpy(&rtcpHeader, buffer, sizeof(RTCPHEADER));

    if (rtcpHeader.m_Version == RTP_VERSION && rtcpHeader.m_Padding == 0 &&
        (rtcpHeader.m_PayloadType == RTCP_SR || rtcpHeader.m_PayloadType == RTCP_RR))
    {
        retVal = true;
    }

    return retVal;
}
#else
bool rtcp_control::CheckRTCPPacket(unsigned char* buffer)
{	
    bool retVal = false;

    boost::uint32_t h = *((boost::uint32_t*)buffer);
    unsigned int rtp_version = (h & 0xC0000000) >> 30;
    bool has_padding = ((h & 0x40000000) != 0);
    unsigned int payload_type = (h & 0x00FF0000) >> 16;

    if ((rtp_version == RTP_VERSION) && (has_padding == false) &&
        ((payload_type == RTCP_SR) || (payload_type == RTCP_RR)))
    {
        retVal = true;
    }

    return retVal;
}
#endif

///<summary>
/// Parse RTCP packet
///</summary>
void rtcp_control::ParseRTCPPacket(unsigned char* buffer, unsigned int bufferLen)
{
    RTCPHEADER rtcpHeader;
    unsigned char packetType;
    unsigned char count;

    if (bufferLen >= sizeof(rtcpHeader))
    {
        PACKET packet;
        packet.m_nLen = bufferLen;
        packet.m_pCurrent = (char*)buffer;
        packet.m_pEnd = (char*)buffer + bufferLen;

        while (1)
        {
            // Get the fixed RTCP header
            if (!GetPacketBytes((unsigned char*)&rtcpHeader, &packet, sizeof(RTCPHEADER)))
            {
                break;
            }

            // Conversions
        #if !defined(_SYNOLOGY_PPC)
            rtcpHeader.m_Length = ntohs(rtcpHeader.m_Length);
            packetType = rtcpHeader.m_PayloadType;
            count = rtcpHeader.m_Count;
        #else
            boost::uint32_t h = *((boost::uint32_t*)&rtcpHeader);
            unsigned int payload_type = (h & 0x00FF0000) >> 16;
            unsigned int rc = (h & 0x1F000000) >> 24;
            packetType = (unsigned char)payload_type;
            count = (unsigned char)rc;
        #endif

            switch (packetType)
            {
            case RTCP_SR:
                log_info(L"CIPTVBridgeRTCPReader::ParseRTCPPacket. RTCP SR is received");
                ParseSRFields(&packet, count);
                break;			
            case RTCP_RR:
                log_info(L"CIPTVBridgeRTCPReader::ParseRTCPPacket. RTCP RR is received");
                ParseRRFields(&packet, count);
                break;			
            case RTCP_SDES:
                log_info(L"CIPTVBridgeRTCPReader::ParseRTCPPacket. RTCP SDES is received");
                ParseSDESFields(&packet, count);
                break;
            case RTCP_BYE:
                log_info(L"CIPTVBridgeRTCPReader::ParseRTCPPacket. RTCP BYE is received");
                break;
            case RTCP_APP:
                log_info(L"CIPTVBridgeRTCPReader::ParseRTCPPacket. RTCP APP is received");
                break;
            default:
                break;
            }
        }
    }
}

///<summary>
/// Parse RTCP sender report fields
///</summary>
void rtcp_control::ParseSRFields(PACKET* pkt, int count)
{
    RTCP_SR_SENDERINFO senderInfo;
    RTCP_SR_REPORTBLOCK reportBlock;
    int reportsSeen;

    // Get the sender info
    if (!GetPacketBytes((unsigned char*)&senderInfo, pkt, sizeof(RTCP_SR_SENDERINFO)))
    {
        return;
    }

    // Conversions
    senderInfo.m_SenderSSRC = ntohl(senderInfo.m_SenderSSRC);
    senderInfo.m_TimestampMSW = ntohl(senderInfo.m_TimestampMSW);
    senderInfo.m_TimestampLSW = ntohl(senderInfo.m_TimestampLSW);
    senderInfo.m_TimestampRTP = ntohl(senderInfo.m_TimestampRTP);
    senderInfo.m_SenderPktCnt = ntohl(senderInfo.m_SenderPktCnt);
    senderInfo.m_SenderOctetCnt = ntohl(senderInfo.m_SenderOctetCnt);

    rtp_reception_stats_db& receptionStats = rtp_reader_->ReceptionStatsDB();
    receptionStats.NoteIncomingSR(senderInfo.m_SenderSSRC, senderInfo.m_TimestampMSW,
        senderInfo.m_TimestampLSW, senderInfo.m_TimestampRTP);

    // Loop over report blocks
    reportsSeen = 0;
    while(reportsSeen < count)
    {
        // Get the report block
        if (!GetPacketBytes((unsigned char*)&reportBlock, pkt, sizeof(RTCP_SR_REPORTBLOCK)))
        {
            break;
        }

        // Conversions
        reportBlock.m_SSRC = ntohl(reportBlock.m_SSRC);
        reportBlock.m_Lost = ntohl(reportBlock.m_Lost);
        reportBlock.m_ExtSeqnoRecvd = ntohl(reportBlock.m_ExtSeqnoRecvd);
        reportBlock.m_Jitter = ntohl(reportBlock.m_Jitter);
        reportBlock.m_LSR = ntohl(reportBlock.m_LSR);
        reportBlock.m_DLSR = ntohl(reportBlock.m_DLSR);

        reportsSeen++;
    }
}

///<summary>
/// Parse RTCP receiver report fields
///</summary>
void rtcp_control::ParseRRFields(PACKET* pkt, int count)
{
    RTCP_SR_REPORTBLOCK reportBlock;
    int reportsSeen;
    unsigned int ssrc;

    // Get the SSRC
    if (!GetPacketBytes((unsigned char*)&ssrc, pkt, 4))
    {
        return;
    }

    // Conversions 
    ssrc = ntohl(ssrc);

    // Loop over report blocks
    reportsSeen = 0;
    while(reportsSeen < count)
    {
        // Get the report block
        if (!GetPacketBytes((unsigned char*)&reportBlock, pkt, sizeof(RTCP_SR_REPORTBLOCK)))
        {
            break;
        }

        // Conversions
        reportBlock.m_SSRC = ntohl(reportBlock.m_SSRC);
        reportBlock.m_Lost = ntohl(reportBlock.m_Lost);
        reportBlock.m_ExtSeqnoRecvd = ntohl(reportBlock.m_ExtSeqnoRecvd);
        reportBlock.m_Jitter = ntohl(reportBlock.m_Jitter);
        reportBlock.m_LSR = ntohl(reportBlock.m_LSR);
        reportBlock.m_DLSR = ntohl(reportBlock.m_DLSR);

        reportsSeen++;      
    } 
}

///<summary>
/// Parse RTCP source description fields
///</summary>
void rtcp_control::ParseSDESFields(PACKET* pkt, int count)
{
    unsigned int   ssrc;
    unsigned char  type;
    unsigned char  length = 0;
    unsigned char* string;
    int            chunksRead;
    int            padLen;

    chunksRead = 0;
    while(chunksRead < count)
    {
        // Get the SSRC
        if (!GetPacketBytes((unsigned char*)&ssrc, pkt, 4))
        {
            break;
        }

        // Conversions 
        ssrc = ntohl(ssrc);

        // Loop through items
        while (1)
        {
            unsigned char byte;

            if (!GetPacketBytes((unsigned char*)&type, pkt, 1))
            {
                break;
            }

            if (!GetPacketBytes((unsigned char*)&length, pkt, 1))
            {
                break;
            }

            // Allocate memory for the string then get it
            string = new unsigned char[length + 1];
            if (!GetPacketBytes(string, pkt, length))
            {
                break;
            }
            string[length] = '\0';

            // Free string memory
            delete string;

            // Look for a null terminator
            if (LookPacketBytes(&byte, pkt, 1) != true)
            {
                break;
            }
            if (byte == 0)
            {
                break;
            }
        }

        // Figure out the pad and skip by it
        padLen = 4 - (length + 2) % 4;
        if (SkipPacketBytes(pkt, padLen) != true)
        {
            break;
        }

        chunksRead++;
    }
}

///<summary>
/// Copy specified number of bytes from a packet and deposit them in an array
///</summary>
bool rtcp_control::GetPacketBytes(unsigned char* dst, PACKET* pkt, int n)
{
    bool retVal = false;

    if (pkt->m_pCurrent + n <= pkt->m_pEnd)
    {
        memcpy(dst, pkt->m_pCurrent, n);
        pkt->m_pCurrent += n;
        retVal = true;
    }

    return retVal;
}

///<summary>
/// Copy specified number of bytes from a packet and deposit them in an array.
/// Not increment the pointer
///</summary>
bool rtcp_control::LookPacketBytes(unsigned char* dst, PACKET* pkt, int n)
{
    bool retVal = false;

    if (pkt->m_pCurrent + n <= pkt->m_pEnd)
    {
        memcpy(dst, pkt->m_pCurrent, n);
        retVal = true;
    }

    return retVal;
}

///<summary>
/// Jump the pointer ahead the specified number of bytes
///</summary>
bool rtcp_control::SkipPacketBytes(PACKET* pkt, int n)
{
    bool retVal = false;

    if (pkt->m_pCurrent + n <= pkt->m_pEnd)
    {
        pkt->m_pCurrent += n;
        retVal = true;
    }

    return retVal;
}

///<summary>
/// Copy specified number of bytes in packet from array
///</summary>
bool rtcp_control::SetPacketBytes(PACKET* pkt, unsigned char* src, int n)
{
    bool retVal = false;

    if (pkt->m_pCurrent + n <= pkt->m_pEnd)
    {
        memcpy(pkt->m_pCurrent, src, n);
        pkt->m_pCurrent += n;
        pkt->m_nLen += n;

        retVal = true;
    }

    return retVal;
}

///<summary>
/// Send RR packet
///</summary>
void rtcp_control::SendReport()
{
    unsigned char* sendBuf = new unsigned char[RECV_BUFSIZE];
    PACKET pkt;

    pkt.m_nLen = 0;
    pkt.m_pCurrent = (char*)sendBuf;
    pkt.m_pEnd = (char*)sendBuf + RECV_BUFSIZE;

    AddReportFields(&pkt);
    AddSDESFields(&pkt);

    if (SendBuffer(sendBuf, pkt.m_nLen))
    {
        log_info(L"CIPTVBridgeRTCPReader::SendReport. RTCP RR is sent");
    }
    else
    {
        log_warning(L"CIPTVBridgeRTCPReader::SendReport. Error sending RTCP RR");
    }

    delete[] sendBuf;
}

///<summary>
/// Send BYE packet (on future)
///</summary>
void rtcp_control::SendBYE()
{
    unsigned char* sendBuf = new unsigned char[RECV_BUFSIZE];
    PACKET pkt;

    pkt.m_nLen = 0;
    pkt.m_pCurrent = (char*)sendBuf;
    pkt.m_pEnd = (char*)sendBuf + RECV_BUFSIZE;

    AddReportFields(&pkt);
    AddBYEFields(&pkt);	

    if (SendBuffer(sendBuf, pkt.m_nLen))
    {
        log_info(L"CIPTVBridgeRTCPReader::SendBYE. RTCP RR is sent");
    }
    else
    {
        log_warning(L"CIPTVBridgeRTCPReader::SendBYE. Error sending RTCP RR");
    }

    delete[] sendBuf;
}

///<summary>
/// Add report fields in packet
///</summary>
void rtcp_control::AddReportFields(PACKET* pkt)
{
    RTCPHEADER rtcpHeader;
    memset(&rtcpHeader, 0, sizeof(RTCPHEADER));

    unsigned numReportingSources;
    rtp_reception_stats_db& allReceptionStats = rtp_reader_->ReceptionStatsDB();
    numReportingSources = allReceptionStats.NumActiveSourcesSinceLastReset();
    
    if (numReportingSources >= 32)
    { 
        numReportingSources = 32; 
    }

#if !defined(_SYNOLOGY_PPC)
    rtcpHeader.m_Version = RTP_VERSION;
    rtcpHeader.m_Padding = 0;
    rtcpHeader.m_Count = numReportingSources;
    rtcpHeader.m_PayloadType = RTCP_RR;
    rtcpHeader.m_Length = 1 + (unsigned short)(sizeof(RTCP_SR_REPORTBLOCK) / 4) * numReportingSources;
    rtcpHeader.m_Length = htons(rtcpHeader.m_Length);
    SetPacketBytes(pkt, (unsigned char*)&rtcpHeader, sizeof(RTCPHEADER));
#else
    boost::uint32_t h = 0x80000000;
    h |= (numReportingSources << 24);
    h |= (RTCP_RR << 16);
    h |= (1 + (unsigned short)(sizeof(RTCP_SR_REPORTBLOCK) / 4) * numReportingSources);
    SetPacketBytes(pkt, (unsigned char*)&h, sizeof(h));
#endif

    unsigned int SSRC = htonl(rtp_reader_->SSRC());
    SetPacketBytes(pkt, (unsigned char*)&SSRC, 4);

    allReceptionStats.Lock();

    TRTPReceptionStats::iterator it = allReceptionStats.reception_stats_.begin();
    while (it != allReceptionStats.reception_stats_.end()) 
    {
        rtp_reception_stats* receptionStats = it->second;		
        AddReportBlock(receptionStats, pkt);
        it++;
    }

    allReceptionStats.Unlock();

    allReceptionStats.Reset(); // because we have just generated a report
}

///<summary>
/// Add report block for SSRC in packet
///</summary>
void rtcp_control::AddReportBlock(rtp_reception_stats* stats, PACKET* pkt)
{
    RTCP_SR_REPORTBLOCK reportBlock;
    memset(&reportBlock, 0, sizeof(RTCP_SR_REPORTBLOCK));

    reportBlock.m_SSRC = htonl(stats->SSRC());

    unsigned highestExtSeqNumReceived = stats->HighestExtSeqNumReceived();
    unsigned totNumExpected = highestExtSeqNumReceived - stats->BaseExtSeqNumReceived();
    int totNumLost = totNumExpected - stats->TotNumPacketsReceived();
    // 'Clamp' this loss number to a 24-bit signed value:
    if (totNumLost > 0x007FFFFF) 
    {
        totNumLost = 0x007FFFFF;
    } 
    else if (totNumLost < 0) 
    {
        if (totNumLost < -0x00800000)
        {
            totNumLost = 0x00800000;
        }
        totNumLost &= 0x00FFFFFF;
    }

    unsigned numExpectedSinceLastReset = highestExtSeqNumReceived - stats->LastResetExtSeqNumReceived();
    int numLostSinceLastReset = numExpectedSinceLastReset - stats->NumPacketsReceivedSinceLastReset();
    unsigned char lossFraction;
    if (numExpectedSinceLastReset == 0 || numLostSinceLastReset < 0) 
    {
        lossFraction = 0;
    }
    else
    {
        lossFraction = (unsigned char)((numLostSinceLastReset << 8) / numExpectedSinceLastReset);
    }

    reportBlock.m_Lost = htonl((lossFraction << 24) | totNumLost);
    reportBlock.m_ExtSeqnoRecvd = htonl(highestExtSeqNumReceived);
    reportBlock.m_Jitter = htonl(stats->Jitter());

    unsigned NTPmsw = stats->LastReceivedSRNTPmsw();
    unsigned NTPlsw = stats->LastReceivedSRNTPlsw();
    unsigned LSR = ((NTPmsw&0xFFFF) << 16) | (NTPlsw >> 16); // middle 32 bits
    reportBlock.m_LSR = htonl(LSR);

    // Figure out how long has elapsed since the last SR rcvd from this src:
    timeval const& LSRtime = stats->LastReceivedSRTime(); // "last SR"
    timeval timeNow, timeSinceLSR;
    utility::GetTimeOfDay(&timeNow);
    if (timeNow.tv_usec < LSRtime.tv_usec) 
    {
        timeNow.tv_usec += 1000000;
        timeNow.tv_sec -= 1;
    }
    timeSinceLSR.tv_sec = timeNow.tv_sec - LSRtime.tv_sec;
    timeSinceLSR.tv_usec = timeNow.tv_usec - LSRtime.tv_usec;
    // The enqueued time is in units of 1/65536 seconds.
    // (Note that 65536/1000000 == 1024/15625)
    unsigned DLSR;
    if (LSR == 0) 
    {
        DLSR = 0;
    }
    else 
    {
        DLSR = (timeSinceLSR.tv_sec << 16) | ((((timeSinceLSR.tv_usec << 11) + 15625) / 31250) & 0xFFFF);
    }

    reportBlock.m_DLSR = htonl(DLSR);

    SetPacketBytes(pkt, (unsigned char*)&reportBlock, sizeof(RTCP_SR_REPORTBLOCK));
}

///<summary>
/// Add SDES fields in packet
///</summary>
void rtcp_control::AddSDESFields(PACKET* pkt)
{
    // Begin by figuring out the size of the entire SDES report:
    unsigned numBytes = 4;
    // counts the SSRC, but not the header; it'll get subtracted out
    numBytes += cname_.TotalSize(); // includes id and length
    numBytes += 1; // the special END item

    unsigned num4ByteWords = (numBytes + 3) / 4;

    RTCPHEADER rtcpHeader;
    memset(&rtcpHeader, 0, sizeof(RTCPHEADER));

#if !defined(_SYNOLOGY_PPC)
    rtcpHeader.m_Version = RTP_VERSION;
    rtcpHeader.m_Padding = 0;
    rtcpHeader.m_Count = 1;  // 1 SSRC chunk
    rtcpHeader.m_PayloadType = RTCP_SDES;
    rtcpHeader.m_Length = htons(num4ByteWords);
    SetPacketBytes(pkt, (unsigned char*)&rtcpHeader, sizeof(RTCPHEADER));
#else
    boost::uint32_t h = 0x80000000;
    h |= (1 << 24);
    h |= (RTCP_SDES << 16);
    h |= num4ByteWords;
    SetPacketBytes(pkt, (unsigned char*)&h, sizeof(h));
#endif

    unsigned int SSRC = htonl(rtp_reader_->SSRC());
    SetPacketBytes(pkt, (unsigned char*)&SSRC, 4);

    // Add the CNAME
    SetPacketBytes(pkt, (unsigned char*)cname_.Data(), cname_.TotalSize());

    // Add the 'END' item (i.e., a zero byte), plus any more needed to pad
    unsigned numPaddingBytesNeeded = 4 - (pkt->m_nLen % 4);
    unsigned char const zero = '\0';
    while (numPaddingBytesNeeded-- > 0)
    {
        SetPacketBytes(pkt, (unsigned char*)&zero, 1);
    }
}

///<summary>
/// Add BYE fields in packet
///</summary>
void rtcp_control::AddBYEFields(PACKET* pkt)
{
    RTCPHEADER rtcpHeader;
    memset(&rtcpHeader, 0, sizeof(RTCPHEADER));

#if !defined(_SYNOLOGY_PPC)
    rtcpHeader.m_Version = RTP_VERSION;
    rtcpHeader.m_Padding = 0;
    rtcpHeader.m_Count = 1;              // 1 SSRC
    rtcpHeader.m_PayloadType = RTCP_BYE;
    rtcpHeader.m_Length = 1;             // 2 32-bit words total (i.e., with 1 SSRC)
    SetPacketBytes(pkt, (unsigned char*)&rtcpHeader, sizeof(RTCPHEADER));
#else
    boost::uint32_t h = 0x80000000;
    h |= (1 << 24);
    h |= (RTCP_BYE << 16);
    h |= 1;
    SetPacketBytes(pkt, (unsigned char*)&h, sizeof(h));
#endif

    unsigned int SSRC = ntohl(rtp_reader_->SSRC());
    SetPacketBytes(pkt, (unsigned char*)&SSRC, 4);
}

///<summary>
/// Send buffer on server
///</summary>
bool rtcp_control::SendBuffer(unsigned char* buffer, int bufLen)
{
    if (!sock_ptr_ || !buffer || (bufLen == 0))
    {
        return false;
    }

    remote_addr_.set_port(server_port_);
    errcode_t err = sock_ptr_->send_datagram(buffer, bufLen, remote_addr_);

    if (err != err_none)
    {
        return false;
    }

    last_sent_size_ = IP_UDP_HDR_SIZE + bufLen;
    return true;
}

///<summary>
/// Sets a timer to send RTCP packet
///</summary>
void rtcp_control::Schedule(double nextTime) 
{
    int milli_seconds_to_delay = (int)((nextTime - utility::GetTimeNow()) * 1000);
    if (milli_seconds_to_delay < 0 || milli_seconds_to_delay == 0)
    {
        milli_seconds_to_delay = 10;
    }

    if (rtcp_timer_ != NULL)
        rtcp_timer_->restart(milli_seconds_to_delay);
}

///<summary>
/// Timer function sends RTCP packet
///</summary>
//void CALLBACK rtcp_control::OnExpire(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2)
//{
//    rtcp_control* parent = (rtcp_control*)dwUser;
//
//    boost::mutex::scoped_lock lock(parent->lock_);
//    if (parent->m_Socket != INVALID_SOCKET)
//    {
//        double rtcpBW = 0.05 * parent->m_TotSessionBW * 1024 / 8; // bytes per second
//
//        parent->OnExpire(1, 0, rtcpBW, 0, &parent->m_AverRTCPSize,
//            &parent->m_IsInitial, utility::GetTimeNow(), &parent->m_PrevReportTime);
//    }
//}

void rtcp_control::OnExpire(const boost::system::error_code& e)
{
    if (e != boost::asio::error::operation_aborted)
    {
        boost::mutex::scoped_lock lock(lock_);
        
        if (sock_ptr_)
        {
            double rtcpBW = 0.05 * tot_session_bw_ * 1024 / 8; // bytes per second
            OnExpire(1, 0, rtcpBW, 0, &aver_rtcp_size_, &is_initial_, utility::GetTimeNow(), &prev_report_time_);
        }
    }
}

///<summary>
/// Expire timer to send RTCP packet
///</summary>
void rtcp_control::OnExpire(int members, int senders, double rtcp_bw, int we_sent, double *avg_rtcp_size, int *initial, double tc, double *tp)
{
    double t;     // Interval
    double tn;    // Next transmit time

    t = RTCPInterval(members, senders, rtcp_bw, we_sent, *avg_rtcp_size, *initial);

    tn = *tp + t;
    if (tn <= tc) 
    {
        SendReport();
        *avg_rtcp_size = (1./16.) * last_sent_size_ + (15./16.) * (*avg_rtcp_size);
        *tp = tc;

        t = RTCPInterval(members, senders, rtcp_bw, we_sent, *avg_rtcp_size, *initial);

        Schedule(t + tc);
        *initial = 0;
    }
    else
    {
        Schedule(tn);
    }
}

///<summary>
/// Calculates the time of sending the next RTCP packet
///</summary>
double rtcp_control::RTCPInterval(int members, int senders, double rtcp_bw, int we_sent, double avg_rtcp_size, int initial)
{
    // Minimum average time between RTCP packets from this site (in
    // seconds).  This time prevents the reports from `clumping' when
    // sessions are small and the law of large numbers isn't helping
    // to smooth out the traffic.  It also keeps the report interval
    // from becoming ridiculously small during transient outages like
    // a network partition.
    double const RTCP_MIN_TIME = 5.;

    // Fraction of the RTCP bandwidth to be shared among active
    // senders.  (This fraction was chosen so that in a typical
    // session with one or two active senders, the computed report
    // time would be roughly equal to the minimum report time so that
    // we don't unnecessarily slow down receiver reports.) The
    // receiver fraction must be 1 - the sender fraction.
    double const RTCP_SENDER_BW_FRACTION = 0.25;
    double const RTCP_RCVR_BW_FRACTION = (1-RTCP_SENDER_BW_FRACTION);

    // To compensate for "unconditional reconsideration" converging to a
    // value below the intended average.
    double const COMPENSATION = 2.71828 - 1.5;

    double t;                   // interval
    double rtcp_min_time = RTCP_MIN_TIME;
    int n;                      // no. of members for computation

    // Very first call at application start-up uses half the min
    // delay for quicker notification while still allowing some time
    // before reporting for randomization and to learn about other
    // sources so the report interval will converge to the correct
    // interval more quickly.
    if (initial)
    {
        rtcp_min_time /= 2;
    }

    // If there were active senders, give them at least a minimum
    // share of the RTCP bandwidth.  Otherwise all participants share
    // the RTCP bandwidth equally.
    n = members;
    if (senders > 0 && senders < members * RTCP_SENDER_BW_FRACTION)
    {
        if (we_sent) 
        {
            rtcp_bw *= RTCP_SENDER_BW_FRACTION;
            n = senders;
        }
        else 
        {
            rtcp_bw *= RTCP_RCVR_BW_FRACTION;
            n -= senders;
        }
    }

    // The effective number of sites times the average packet size is
    // the total number of octets sent when each site sends a report.
    // Dividing this by the effective bandwidth gives the time
    // interval over which those packets must be sent in order to
    // meet the bandwidth target, with a minimum enforced.  In that
    // time interval we send one report so this time is also our
    // average time between reports.
    t = avg_rtcp_size * n / rtcp_bw;
    if (t < rtcp_min_time)
    {
        t = rtcp_min_time;
    }

    // To avoid traffic bursts from unintended synchronization with
    // other sites, we then pick our actual next report interval as a
    // random number uniformly distributed between 0.5*t and 1.5*t.
    unsigned tmp = rand() & 0x3FFFFFFF; // a random 30-bit integer
    t = t * ((double)tmp / (1024 * 1024 * 1024) + 0.5);
    t = t / COMPENSATION;

    return t;
}

///<summary>
/// SDES description field
///</summary>
SDESItem::SDESItem(unsigned char tag, char* value)
{
    int length = (int)strlen((char const*)value);
    if (length > 255)
    {
        length = 255;
    }

    m_Data[0] = tag;
    m_Data[1] = (unsigned char)length;
    memmove(&m_Data[2], value, length);

    // Pad the trailing bytes to a 4-byte boundary:
    while ((length) % 4 > 0)
    {
        m_Data[2 + length++] = '\0';
    }
}

///<summary>
/// Size SDES description field
///</summary>
unsigned SDESItem::TotalSize() const 
{
    return 2 + (unsigned)m_Data[1];
}

// $Id: rtcp_control.cpp 8622 2013-06-26 12:53:08Z mike $
