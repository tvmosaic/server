/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#define RECV_BUFSIZE                4096
#define RTPUDP_RECVBUFFER			65535

namespace dvblex { 

enum iptv_reader_type_e
{
    iptv_rte_unknown,
    iptv_rte_udp,
    iptv_rte_rtp,
    iptv_rte_http,
    iptv_rte_rtsp,
    iptv_rte_hls,
    iptv_rte_rtmp
};

class net_reader
{
public:
    net_reader() {}
    virtual ~net_reader() {}

    virtual bool create(const char* url, const char* local_address) = 0;
    virtual bool start(int restart_count) = 0;
    virtual void stop() = 0;
    virtual bool restart_required(iptv_reader_type_e& reader_type, long& ms_timeout, int& restart_count){return false;}
};

}
