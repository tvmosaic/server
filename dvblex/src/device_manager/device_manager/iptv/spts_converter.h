/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_ts_info.h>
#include <dl_ts_proc.h>
#include <vector>
#include <map>

namespace dvblex {

class iptv_spts_converter_t
{
    enum iptv_spts_converter_state_e
    {
	    iscs_pat,
	    iscs_pmt,
	    iscs_streaming
    };

    typedef std::map<unsigned short, dvblink::engine::ts_section_payload_parser> pmt_map_t;

public:
    typedef void (*iptv_spts_converter_callback_p)(const unsigned char* buf, size_t len, void* user_param);

	iptv_spts_converter_t(iptv_spts_converter_callback_p callback, void* param);
	~iptv_spts_converter_t();

	void init();
	void term();

	void process_stream(const unsigned char* buf, size_t len);

private:
	unsigned short pmt_pid_;
    unsigned short tid_;
    unsigned short sid_;
    unsigned short pat_cont_counter_;
    unsigned char pat_version_;
    pmt_map_t pmt_map_;
    dvblink::engine::ts_section_payload_parser pat_section_parser_;
    dvblink::engine::ts_packet_generator pat_generator_;

    iptv_spts_converter_callback_p callback_;
    void* param_;
    iptv_spts_converter_state_e state_;

	void process_pmt_section(unsigned short pmt_pid, unsigned char* pmt_buffer, size_t pmt_len);
	void process_pat_section(unsigned char* pat_buffer, size_t pat_len);
    void send_stream(const unsigned char* buffer, size_t len);
	void stream_pat_section(unsigned char* pat_buffer, size_t pat_len);
};

} //dvblex

