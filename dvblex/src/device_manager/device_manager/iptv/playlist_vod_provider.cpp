/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_language_settings.h>
#include <dl_locale_strings.h>
#include "playlist_vod_provider.h"

static const std::string IPTV_PB_ALL_ITEMS_CONTAINER_ID = "a141d3fd-1da2-43c6-baf8-0f4112fe5113";
static const std::string IPTV_PB_BY_NAME_CONTAINER_ID = "aac5dd7d-855b-46b4-87bf-bddb01c1c6ed";
static const std::string IPTV_PB_BY_GROUP_CONTAINER_ID = "629f7bf6-5192-4eef-8a65-8d3b93fd9393";

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

playlist_vod_provider_t::playlist_vod_provider_t(const std::string& playlist_url)
    : playlist_url_(playlist_url)
{
}

playlist_vod_provider_t::~playlist_vod_provider_t()
{
}

void playlist_vod_provider_t::read_containers()
{
    read_playlist_vod();
}

void playlist_vod_provider_t::read_items(const std::string& group_id)
{
    read_playlist_vod();
}

bool playlist_vod_provider_t::is_vod_channel(const playlist_channel& channel)
{
    return channel.is_vod;
}

std::string playlist_vod_provider_t::get_channel_name(playlist_channel& channel)
{
    return channel.get_channel_name();
}

void playlist_vod_provider_t::read_playlist_vod()
{
    categories_.clear();
    groups_.clear();
    container_update_time_map_.clear();
    items_update_time_map_.clear();
    bool exit_flag = false;

    time_t now;
    time(&now);

    playlist_reader reader(playlist_url_);
    playlist_channel_map_t channel_map;
    playlist_reader_result_e res = reader.get_channels(channel_map);

    if (res == prr_ok)
    {
        if (create_all_items_container())
        {
            //all items container
            categories_[IPTV_PB_ALL_ITEMS_CONTAINER_ID] = vod_category_t(language_settings::GetInstance()->GetItemName(item_id_t(iptv_pb_source_all_items)));
            container_update_time_map_[IPTV_PB_ALL_ITEMS_CONTAINER_ID] = now;
            //all by name group
            categories_[IPTV_PB_ALL_ITEMS_CONTAINER_ID].groups_.push_back(IPTV_PB_BY_NAME_CONTAINER_ID);
            groups_[IPTV_PB_BY_NAME_CONTAINER_ID] = vod_group_t(language_settings::GetInstance()->GetItemName(item_id_t(iptv_pb_source_item_by_name)));
            container_update_time_map_[IPTV_PB_BY_NAME_CONTAINER_ID] = now;
        }
        //groups container
        categories_[IPTV_PB_BY_GROUP_CONTAINER_ID] = vod_category_t(language_settings::GetInstance()->GetItemName(item_id_t(iptv_pb_source_item_by_group)));
        container_update_time_map_[IPTV_PB_BY_GROUP_CONTAINER_ID] = now;

        playlist_channel_map_t::iterator it = channel_map.begin();
        while (it != channel_map.end())
        {
            if (is_vod_channel(it->second))
            {
                vod_item_obj_t item = boost::shared_ptr<vod_item_t>(new vod_item_t());
                item->url_ = it->second.url;
                item->video_info_.m_Name = get_channel_name(it->second);
                std::string id = it->second.get_channel_id();
                item->video_info_.id_ = id;

                //thumbnail
                if (it->second.tags.find(m3u_plus_tvg_logo) != it->second.tags.end())
                    item->video_info_.m_ImageURL = it->second.tags[m3u_plus_tvg_logo];

                if (create_all_items_container())
                {
                    groups_[IPTV_PB_BY_NAME_CONTAINER_ID].items_[id] = item;
                    groups_[IPTV_PB_BY_NAME_CONTAINER_ID].items_sort_list_.push_back(vod_item_desc_t(id, item->video_info_.m_Name));
                    items_update_time_map_[IPTV_PB_BY_NAME_CONTAINER_ID] = now;
                }

                //group
                if (it->second.tags.find(m3u_plus_group_title) != it->second.tags.end())
                {
                    std::string group_title = it->second.tags[m3u_plus_group_title];
                    std::string group_id = vod_group_t::generate_id_from_name(group_title);

                    if (groups_.find(group_id) == groups_.end())
                    {
                        categories_[IPTV_PB_BY_GROUP_CONTAINER_ID].groups_.push_back(group_id);
                        groups_[group_id] = vod_group_t(group_title);
                        container_update_time_map_[group_id] = now;
                        items_update_time_map_[group_id] = now;
                    }
                    groups_[group_id].items_[id] = item;
                    groups_[group_id].items_sort_list_.push_back(vod_item_desc_t(id, item->video_info_.m_Name));
                }
            }
            ++it;
        }

        //sort items in all groups by name
        vod_group_map_t::iterator group_it = groups_.begin();
        while (group_it != groups_.end())
        {
            try {
                std::sort(group_it->second.items_sort_list_.begin(), group_it->second.items_sort_list_.end(), vod_item_name_sorter(&exit_flag));
            } catch (...)
            {
                //exit was signalled
            }
            ++group_it;
        }
        log_info(L"playlist_vod_provider_t::read_playlist_vod. Successfully read %1% items and %2% item groups") % groups_[IPTV_PB_BY_NAME_CONTAINER_ID].items_.size() % groups_.size();
    }

}

