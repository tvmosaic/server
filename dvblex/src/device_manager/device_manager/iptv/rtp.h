/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

// Current protocol version
#define RTP_VERSION    2

// RTCP packet type
enum RTCP_TYPE
{
    RTCP_SR   = 200,
    RTCP_RR   = 201,
    RTCP_SDES = 202,
    RTCP_BYE  = 203,
    RTCP_APP  = 204
};

enum SDES_TAGS
{
    RTCP_SDES_END = 0,
    RTCP_SDES_CNAME,
    RTCP_SDES_NAME,
    RTCP_SDES_EMAIL,
    RTCP_SDES_PHONE,
    RTCP_SDES_LOC,
    RTCP_SDES_TOOL,
    RTCP_SDES_NOTE,
    RTCP_SDES_PRIV
};

typedef struct _RTPHEADER
{
    // 1 byte
    unsigned char m_CSRCCount:   4; // 0 - 3 bit CSRC count
    unsigned char m_Extension:   1; // 4 bit header extension flag
    unsigned char m_Padding:     1; // 5 bit padding flag 
    unsigned char m_Version:     2; // 6 - 7 bit protocol version 
    // 2 byte
    unsigned char m_PayloadType: 7; // payload type
    unsigned char m_Marker:      1; // marker bit
    // 3 - 4 bytes
    unsigned short m_SeqNumber;     // sequence number
    // 5 - 8 bytes
    unsigned int m_Timestamp;       // timestamp
    // 9 - 12 bytes
    unsigned int m_SSRC;            // synchronization source
    // 13 - 16 bytes
    unsigned int m_CSRC;            // optional CSRC list				
} RTPHEADER;

typedef struct _RTPEXT
{
    // 1 - 2 bytes 
    unsigned short profile;         // defined by profile
    // 3 - 4 bytes 
    unsigned short length;          // length header extension
} RTPEXT;

typedef struct _RTCPHEADER
{
    // 1 byte
    unsigned char  m_Count:   5;     // 0 - 4 bit varies by packet type
    unsigned char  m_Padding: 1;     // 5 bit padding flag
    unsigned char  m_Version: 2;     // 6 - 7 bit protocol version	
    // 2 byte
    unsigned char  m_PayloadType;    // RTCP packet type	
    // 3-4 bytes
    unsigned short m_Length;         // pkt len in words*/
} RTCPHEADER;

// RTCP SR packet type sender info portion
typedef struct _RTCP_SR_SENDERINFO
{
    unsigned int m_SenderSSRC;       // SSRC of sender
    unsigned int m_TimestampMSW;     // NTP timestamp, most significant word
    unsigned int m_TimestampLSW;     // NTP timestamp, least significant word
    unsigned int m_TimestampRTP;     // RTP timestamp
    unsigned int m_SenderPktCnt;     // sender's packet count
    unsigned int m_SenderOctetCnt;   // sender's octet count
} RTCP_SR_SENDERINFO;

// RTCP SR report block
typedef struct _RTCP_SR_REPORTBLOCK
{
    unsigned int  m_SSRC;            // SSRC of first source
    unsigned int  m_Lost;            // fraction lost (1 byte) and cumulative number of packets lost (3 bytes)
    unsigned int  m_ExtSeqnoRecvd;   // extended highest sequence number received
    unsigned int  m_Jitter;          // interarrival jitter
    unsigned int  m_LSR;             // last SR (LSR)
    unsigned int  m_DLSR;            // delay since last SR (DLSR)
} RTCP_SR_REPORTBLOCK;
