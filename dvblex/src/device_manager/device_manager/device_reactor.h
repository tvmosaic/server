/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <dl_filesystem_path.h>

#include <drivers/aux_module.h>
#include <device_manager/device_playback_src.h>
#include "device.h"
#include "device_factory.h"
#include "directory_settings.h"

namespace dvblex { 

class device_reactor_t
{
protected:
    typedef std::map<std::string, device_factory_t*> device_factory_map_t;

public:
    device_reactor_t(directory_settings_obj_t& directory_settings);
    ~device_reactor_t();

    bool get_device_list(const dvblink::device_uuid_t& u, device_descriptor_list_t& device_list);
    device_t* create_device(const dvblink::device_id_t& device_id);
    bool get_device_info(const dvblink::device_id_t& device_id, device_descriptor_t& device_info);
    source_type_e get_source_type_from_tuning_params(const dvblink::device_uuid_t& u, const dvblink::channel_tuning_params_t& tune_params);

    void get_device_aux_list(dvblink::aux_module_list_t& aux_list);

    bool get_device_configurable_props(const dvblink::device_id_t& device_id, configurable_device_props_t& props);
    set_device_props_result_e set_device_configurable_props(const dvblink::device_id_t& device_id, const concise_param_map_t& props);

    void get_device_template_list(parameters_container_t& templates);
    bool create_manual_device(const concise_param_map_t& device_params);
    bool delete_manual_device(const dvblink::device_id_t& device_id);
    void get_manual_devices(manual_device_list_t& devices);
    void add_manual_devices(const manual_device_list_t& devices);

    device_playback_src_obj_t get_playback_src_for_device(const dvblink::device_id_t& device_id);

protected:
    device_factory_t* get_device_factory_by_name(const std::string& name);
    device_factory_t* create_device_factory_by_name(const std::string& name);

    device_factory_map_t device_factory_map_;
    directory_settings_obj_t directory_settings_;
};

}
