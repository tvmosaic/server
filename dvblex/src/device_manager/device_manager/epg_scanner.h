/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>

#include <boost/thread.hpp>
#include <dl_types.h>
#include <dl_epg_box.h>
#include <dl_channel_info.h>

namespace dvblex { 

class epg_scanner_t
{
protected:
    typedef std::vector<concise_channel_tune_info_t> channel_tuning_params_list_t;

public:
    epg_scanner_t();
	virtual ~epg_scanner_t();

    bool scan(dvblink::epg_box_obj_t& epg_box);
    void cancel();
    bool is_idle();

protected:
    dvblink::epg_box_obj_t epg_box_;
    boost::thread* scanning_thread_;
    bool exit_flag_;
    bool is_idle_;
    std::map<dvblink::channel_id_t, dvblink::channel_id_t> scan_channel_id_map_;

	void scan_thread_function();
    void report_epg(const dvblink::channel_id_t& channel_id, const dvblink::engine::DLEPGEventList& epg_data);

    //overrides for child classes
    virtual bool start(){return true;}
    virtual void stop(){}
    virtual bool do_scan(const concise_channel_tune_info_t& tuning_params){return false;}
    virtual void get_scan_transponders(const dvblink::epg_channel_tune_info_list_t& channels, channel_tuning_params_list_t& transponders){}
};

}
