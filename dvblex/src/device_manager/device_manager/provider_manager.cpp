/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <dl_strings.h>
#include <dl_logger.h>
#include "provider_manager.h"
#include "dvb/dvb_scan_provider.h"
#include "file/file_scan_provider.h"
#include "iptv/iptv_scan_provider.h"

using namespace dvblex;
using namespace dvblink::engine;
using namespace dvblink::logging;

static const char* scan_provider_names[] = {dvb_scan_provider_t::get_name(), file_scan_provider_t::get_name(), iptv_scan_provider_t::get_name()};
static const size_t scan_provider_names_num = sizeof(scan_provider_names) / sizeof(char*);

provider_manager_t::provider_manager_t()
{
}

bool provider_manager_t::init(const dvblink::filesystem_path_t& provider_storage_path)
{
    provider_storage_path_ = provider_storage_path;
    return true;
}

void provider_manager_t::get_providers(dvblink::standard_set_t standard, provider_info_map_t& providers)
{
    //enumerate through source types
    for (size_t i=0; i<source_type_num; i++)
    {
        if ((standard.get() & source_type_values[i]) != 0)
        {
            for (size_t j=0; j<scan_provider_names_num; j++)
            {
                scan_provider_t* scan_provider = get_provider_by_name(scan_provider_names[j]);
                if (scan_provider != NULL)
                {
                    provider_info_map_t new_providers;
                    scan_provider->get_providers(source_type_values[i], providers);
                    providers.insert(new_providers.begin(), new_providers.end());

                    delete scan_provider;
                }
            }
        }
    }
}

bool provider_manager_t::get_provider_details(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, provider_info_t& provider_info, provider_scan_list_t& scan_list)
{
    bool ret_val = false;

    std::string sp_name = scan_provider_t::get_scan_provider_from_id(id.get());
    scan_provider_t* sp = get_provider_by_name(sp_name);
    if (sp != NULL)
    {
        ret_val = sp->get_provider_details(id.get(), scanner_settings, provider_info, scan_list);

        delete sp;
    } else
    {
        log_error(L"provider_manager_t::get_provider_details. Unable to get scan provider from its id (%1%)") % string_cast<EC_UTF8>(id.get());
    }

    return ret_val;
}

bool provider_manager_t::get_rescan_settings(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, parameters_container_t& settings)
{
    bool ret_val = false;

    std::string sp_name = scan_provider_t::get_scan_provider_from_id(id.get());
    scan_provider_t* sp = get_provider_by_name(sp_name);
    if (sp != NULL)
    {
        ret_val = sp->get_rescan_settings(id.get(), scanner_settings, settings);

        delete sp;
    } else
    {
        log_error(L"provider_manager_t::get_rescan_settings. Unable to get scan provider from its id (%1%)") % string_cast<EC_UTF8>(id.get());
    }

    return ret_val;
}

bool provider_manager_t::get_headend_info(const headend_description_for_device_t& headend, headend_info_t& headend_info)
{
    bool ret_val = false;

    std::string sp_name = scan_provider_t::get_scan_provider_from_id(headend.headend_id_.get());
    scan_provider_t* sp = get_provider_by_name(sp_name);
    if (sp != NULL)
    {
        ret_val = sp->get_headend_info(headend, headend_info);

        delete sp;
    } else
    {
        log_error(L"provider_manager_t::get_headend_info. Unable to get headend info from its id (%1%)") % headend.headend_id_.to_wstring();
    }

    return ret_val;
}

bool provider_manager_t::get_headend_from_provider(const provider_info_t& provider_info, const concise_param_map_t& scanner_settings, headend_info_t& headend_info)
{
    bool ret_val = false;

    std::string sp_name = scan_provider_t::get_scan_provider_from_id(provider_info.id_.get());
    scan_provider_t* sp = get_provider_by_name(sp_name);
    if (sp != NULL)
    {
        sp->get_headend_from_provider(provider_info, scanner_settings, headend_info);
        delete sp;

        ret_val = true;
    } else
    {
        log_error(L"provider_manager_t::get_headend_from_provider. Unable to get headend info from provider id (%1%)") % provider_info.id_.to_wstring();
    }

    return ret_val;
}

scan_provider_t* provider_manager_t::get_provider_by_name(const std::string& name)
{
    scan_provider_t* ret_val = NULL;

    if (boost::iequals(name, dvb_scan_provider_t::get_name()))
    {
        ret_val = new dvb_scan_provider_t(provider_storage_path_);
    } else
    if (boost::iequals(name, file_scan_provider_t::get_name()))
    {
        ret_val = new file_scan_provider_t(provider_storage_path_);
    } else
    if (boost::iequals(name, iptv_scan_provider_t::get_name()))
    {
        ret_val = new iptv_scan_provider_t(provider_storage_path_);
    }
    return ret_val;
}
