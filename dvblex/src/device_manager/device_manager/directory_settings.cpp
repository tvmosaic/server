/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/filesystem.hpp>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_hash.h>
#include <dl_utils.h>
#include "directory_settings.h"
#include "constants.h"

using namespace dvblex;
using namespace dvblex::settings;
using namespace dvblink;
using namespace dvblink::engine;

directory_settings_t::directory_settings_t(installation_settings_obj_t& install_settings) :
    install_settings_(install_settings)
{
    private_devices_path_ = install_settings_->get_private_component_path(devices_component_name);
    scanners_path_ = install_settings_->get_shared_component_path(scanners_component_name);
    shared_devices_path_ = install_settings_->get_shared_component_path(devices_component_name);
}

directory_settings_t::~directory_settings_t()
{
}


dvblink::filesystem_path_t directory_settings_t::create_shared_dir_for_device(const device_descriptor_t& dd)
{
    dvblink::filesystem_path_t ret_val;

    //check if directory for this device exists already
    ret_val = get_shared_dir_for_device(dd.id_);

    if (ret_val.to_wstring().empty())
    {
        //create dir

        //name
        std::wstring wstr = dd.name_.to_wstring();
        engine::substitute_invalid_file_chars(wstr);

        //postfix
        std::string postfix = get_device_id_postfix(dd.id_);

        wstr += string_cast<EC_UTF8>(postfix);

        ret_val = get_shared_devices_path();
        ret_val /= wstr;

        try {
            boost::filesystem::create_directories(ret_val.to_boost_filesystem());
        } catch(...) {}

    }

    return ret_val;
}

dvblink::filesystem_path_t directory_settings_t::get_shared_dir_for_device(const dvblink::device_id_t& device_id)
{
    namespace fs = boost::filesystem;

    dvblink::filesystem_path_t ret_val;

    std::string postfix = get_device_id_postfix(device_id);

    //find a directory with this postfix
    try {
        const fs::directory_iterator end;
        for (fs::directory_iterator it(get_shared_devices_path().to_boost_filesystem()); it != end; ++it)
        {
            if (!fs::is_directory(it->status()))
            {
                continue; // skip
            }

            const fs::path& fpath = it->path();

#ifdef WIN32
            std::string fpath_str =  string_cast<EC_UTF8>(fpath.wstring());
#else
            std::string fpath_str = fpath.string();
#endif
            if (boost::ends_with(fpath_str, postfix))
            {
                ret_val = fpath;
                break;
            }
        }
    } catch(...) {}

    return ret_val;
}

std::string directory_settings_t::get_device_id_postfix(const dvblink::device_id_t& device_id)
{
    boost::uint64_t hash = calculate_64bit_string_hash(device_id.to_string());

    std::stringstream buf;
    buf << "____" << hash;

    return buf.str();
}
