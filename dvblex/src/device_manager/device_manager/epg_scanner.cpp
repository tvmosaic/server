/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include "epg_scanner.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

namespace dvblex { 

epg_scanner_t::epg_scanner_t() :
	scanning_thread_(NULL), exit_flag_(false), is_idle_(true)
{
}

epg_scanner_t::~epg_scanner_t()
{
	//stop and delete scanning thread if it is running
	cancel();
}

bool epg_scanner_t::scan(epg_box_obj_t& epg_box)
{
	//stop and delete scanning thread if it is running
	cancel();

    epg_box_ = epg_box;

    is_idle_ = false;
    exit_flag_ = false;
    scanning_thread_ = new boost::thread(boost::bind(&epg_scanner_t::scan_thread_function, this));

    return true;
}

void epg_scanner_t::cancel()
{
	if (scanning_thread_ != NULL)
	{
		exit_flag_ = true;
		scanning_thread_->join();
		delete scanning_thread_;
		scanning_thread_ = NULL;
	}
}

bool epg_scanner_t::is_idle()
{
    return is_idle_;
}

void epg_scanner_t::scan_thread_function()
{
    log_info(L"channel_scanner_t::scan_thread_function started");

    if (start())
    {

        epg_channel_tune_info_list_t channels;
        epg_box_->get_channels_to_scan(channels);

        for (size_t i=0; i<channels.size(); i++)
            scan_channel_id_map_[channels[i].channel_id_] = channels[i].channel_id_;

        channel_tuning_params_list_t transponders;
        get_scan_transponders(channels, transponders);

        for (size_t i=0; i<transponders.size() && !exit_flag_ && !epg_box_->is_cancelled() && scan_channel_id_map_.size() > 0; i++)
        {
            log_info(L"channel_scanner_t::scan_thread_function. Scanning transponder %1% out of %2%") % (i+1) % transponders.size();
            do_scan(transponders[i]);
        }

        stop();

        if (!exit_flag_ && !epg_box_->is_cancelled())
        {
            log_info(L"channel_scanner_t::scan_thread_function. EPG scan is completed");

            epg_box_->scan_finished();
        }else
        {
            log_info(L"channel_scanner_t::scan_thread_function. EPG scan was aborted");
            epg_box_->scan_aborted();
        }
    }

    //delete epg box
    epg_box_.reset();

    is_idle_ = true;
    log_info(L"channel_scanner_t::scan_thread_function finished");
}

void epg_scanner_t::report_epg(const dvblink::channel_id_t& channel_id, const dvblink::engine::DLEPGEventList& epg_data)
{
    if (scan_channel_id_map_.find(channel_id) != scan_channel_id_map_.end())
    {
        log_info(L"epg_scanner_t::report_epg. Reporting epg for channel %1%. Number of events %2%") % string_cast<EC_UTF8>(channel_id.get()) % epg_data.size();

        epg_box_->process_epg_data(channel_id, epg_data);
        scan_channel_id_map_.erase(channel_id);
    }
}

}
