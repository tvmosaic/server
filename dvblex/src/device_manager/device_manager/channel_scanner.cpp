/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include "channel_scanner.h"

using namespace dvblink::logging;
using namespace dvblink::engine;

namespace dvblex { 

channel_scanner_t::channel_scanner_t() :
	scanning_thread_(NULL), exit_flag_(false)
{
}

channel_scanner_t::~channel_scanner_t()
{
	//stop and delete scanning thread if it is running
	cancel();
}

bool channel_scanner_t::scan(const provider_info_t& provider_info, const provider_scan_list_t& scan_data, const concise_param_map_t& scanner_settings)
{
    //make sure that previous run has finished
    cancel();

    //copy/clear parameters
    provider_info_ = provider_info;
	scan_data_ = scan_data;
    scanner_settings_ = scanner_settings;
    //add internal provider settings to scanner settings
    scanner_settings_.insert(provider_info.scan_params_.begin(), provider_info.scan_params_.end());

	progress_info_.reset();
	scanned_channels_.clear();
    scan_log_.clear();
    network_scan_results_.clear();
    network_id_.clear();

	//start scanning thread
	exit_flag_ = false;
	scanning_thread_ = new boost::thread(boost::bind(&channel_scanner_t::scan_thread_function, this));

	return true;
}

bool channel_scanner_t::scan(const dvblink::scan_network_id_t& network_id)
{
    //make sure that previous run has finished
    cancel();

    //copy parameters
    network_id_ = network_id.get();

	//start scanning thread
	exit_flag_ = false;
	scanning_thread_ = new boost::thread(boost::bind(&channel_scanner_t::scan_thread_function, this));

    return true;
}

void channel_scanner_t::cancel()
{
	if (scanning_thread_ != NULL)
	{
		exit_flag_ = true;
		scanning_thread_->join();
		delete scanning_thread_;
		scanning_thread_ = NULL;
	}
}

void channel_scanner_t::get_progress_info(channel_scanner_progress_t& progress_info)
{
	boost::unique_lock<boost::mutex> lock(scan_info_lock_);
	progress_info = progress_info_;
}

void channel_scanner_t::get_scanned_channels(provider_info_t& provider_info, transponder_list_t& scanned_channels, concise_param_map_t& scanner_settings)
{
	boost::unique_lock<boost::mutex> lock(scan_info_lock_);
	scanned_channels = scanned_channels_;
    provider_info = provider_info_;
    scanner_settings = scanner_settings_;
}

void channel_scanner_t::get_scan_log(scan_log_t& scan_log)
{
	boost::unique_lock<boost::mutex> lock(scan_info_lock_);
    scan_log = scan_log_;
}

void channel_scanner_t::get_scanned_networks(scanned_network_list_t& networks)
{
	boost::unique_lock<boost::mutex> lock(scan_info_lock_);
    for (size_t i=0; i<network_scan_results_.size(); i++)
    {
        scanned_network_t sn;
        sn.network_id_ = network_scan_results_[i].network_id_;
        sn.name_ = network_scan_results_[i].name_;
        networks.push_back(sn);
    }
}

channel_scanner_state_e channel_scanner_t::get_state()
{
	boost::unique_lock<boost::mutex> lock(scan_info_lock_);
    return progress_info_.state_;
}

void channel_scanner_t::set_state(channel_scanner_state_e state)
{
	boost::unique_lock<boost::mutex> lock(scan_info_lock_);
    progress_info_.state_ = state;
}

void channel_scanner_t::scan_thread_function()
{
    log_info(L"channel_scanner_t::scan_thread_function started");

    if (start())
    {
        if (get_state() == css_idle)
        {
            //first scan - can either be full channel scan or network scan
            if (is_network_scan(provider_info_))
            {
                log_info(L"channel_scanner_t::scan_thread_function. Scanning networks");
                set_state(css_scanning_networks);

                scan_log_entry_t log_entry;
                if (!scan_networks(scan_data_, network_scan_results_, log_entry))
                    log_error(L"channel_scanner_t::scan_thread_function. Network scan failed");

                {
                    boost::unique_lock<boost::mutex> lock(scan_info_lock_);
                    scan_log_.push_back(log_entry);
                }

            } else
            {
                log_info(L"channel_scanner_t::scan_thread_function. Scanning channels");
                set_state(css_scanning_channels);
                scan_channels(scan_data_, dvblink::scan_network_id_t());
            }
        } else
        if (get_state() == css_networks_ready)
        {
            log_info(L"channel_scanner_t::scan_thread_function. Scanning channels for network %1%") % string_cast<EC_UTF8>(network_id_);
            //scan channels of one of the pre-scanned networks
            set_state(css_scanning_channels);
            //find scan data for this network
            provider_scan_list_t* scan_data = NULL;
            for (size_t idx=0; idx<network_scan_results_.size(); idx++)
            {
                if (boost::iequals(network_scan_results_[idx].network_id_, network_id_))
                {
                    scan_data = &(network_scan_results_[idx].scan_data_);
                    break;
                }
            }
            if (scan_data != NULL)
                scan_channels(*scan_data, network_id_);
            else
                log_error(L"channel_scanner_t::scan_thread_function. No scan data has been found for network id %1%") % string_cast<EC_UTF8>(network_id_);
        } else
        {
            log_error(L"channel_scanner_t::scan_thread_function. Scan cannot be started in this state %1%") % progress_info_.state_;
        }
        stop();
    }

    log_info(L"channel_scanner_t::scan_thread_function finished");

    {
        boost::unique_lock<boost::mutex> lock(scan_info_lock_);

        if (progress_info_.state_ == css_scanning_networks)
            progress_info_.state_ = css_networks_ready;
        else if (progress_info_.state_ == css_scanning_channels)
            progress_info_.state_ = css_ready;
        else
            progress_info_.state_ = css_ready;
    }
}

void channel_scanner_t::scan_channels(const provider_scan_list_t& scan_data, const dvblink::scan_network_id_t& network_id)
{
    int channels_num = 0;

    for (size_t i=0; i<scan_data.size() && !exit_flag_; i++)
    {
        scan_log_entry_t log_entry;
        transponder_list_t found_channels;
        if (do_scan(scan_data[i].get(), network_id, found_channels, log_entry))
        {
            //update channels
            boost::unique_lock<boost::mutex> lock(scan_info_lock_);
            scanned_channels_.insert(scanned_channels_.end(), found_channels.begin(), found_channels.end());
            for (size_t j=0; j<found_channels.size(); j++)
                channels_num += found_channels[j].channels_.size();
        }

        log_info(L"channel_scanner_t::scan_channels. Scanned %1% transponder out of %2%. Channels found %3%") % (i+1) % scan_data.size() % channels_num;

        //update progress and scan log
        {
            boost::unique_lock<boost::mutex> lock(scan_info_lock_);
            progress_info_.channels_found_ = channels_num;
            progress_info_.progress_ = i*100/scan_data.size();

            scan_log_.push_back(log_entry);
        }

    }
}

}
