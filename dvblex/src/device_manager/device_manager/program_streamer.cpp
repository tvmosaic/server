/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include "program_streamer.h"

using namespace dvblex;

program_streamer_t::program_streamer_t(const dvblink::channel_id_t& channel_id, const dvblink::channel_tuning_params_t& tune_params) :
    callback_(NULL), user_param_(NULL), channel_id_(channel_id), tune_params_(tune_params), stream_waiter_(stream_waiter_callback, this)
{
}

program_streamer_t::~program_streamer_t()
{
}

void program_streamer_t::write_stream(const unsigned char* buf, unsigned long len)
{
    if (len > 0)
    {
        if (pmt_parser_.GetPmtInfo() == NULL)
        {
	        pmt_parser_.ProcessStream((unsigned char*)buf, len);
	        //if pmt information is available now - initialize stream waiter
	        if (pmt_parser_.GetPmtInfo() != NULL)
		        stream_waiter_.Start(&pmt_parser_);
        }

        if (pmt_parser_.GetPmtInfo() != NULL)
	        stream_waiter_.ProcessStream((unsigned char*)buf, len);
    }
}

void program_streamer_t::stream_waiter_callback(const unsigned char* buf, int len, void* param)
{
    program_streamer_t* parent = (program_streamer_t*)param;

    if (parent->callback_ != NULL)
        parent->callback_(buf, len, parent->user_param_);
}

bool program_streamer_t::start()
{
	pmt_parser_.Init();

    return true;
}

void program_streamer_t::stop()
{
    stream_waiter_.Stop();
    pmt_parser_.Term();
}

