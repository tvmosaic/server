/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>

#include <boost/shared_ptr.hpp>
#include <dl_installation_settings.h>
#include <dl_filesystem_path.h>
#include <dl_device_info.h>

namespace dvblex { 

class directory_settings_t
{
public:
    directory_settings_t(dvblex::settings::installation_settings_obj_t& install_settings);
    ~directory_settings_t();

    dvblink::filesystem_path_t get_scanners_path(){return scanners_path_;}
    dvblink::filesystem_path_t get_devices_path(){return private_devices_path_;}

    dvblink::filesystem_path_t create_shared_dir_for_device(const device_descriptor_t& dd);
    dvblink::filesystem_path_t get_shared_dir_for_device(const dvblink::device_id_t& device_id);

    const dvblex::settings::installation_settings_obj_t& get_install_settings() {return install_settings_;}

protected:
    dvblex::settings::installation_settings_obj_t install_settings_;
    dvblink::filesystem_path_t scanners_path_;
    dvblink::filesystem_path_t private_devices_path_;
    dvblink::filesystem_path_t shared_devices_path_;

    dvblink::filesystem_path_t get_shared_devices_path(){return shared_devices_path_;}
    std::string get_device_id_postfix(const dvblink::device_id_t& device_id);
};

typedef boost::shared_ptr<directory_settings_t> directory_settings_obj_t;

}
