/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_strings.h>
#include <dl_logger.h>
#include "streamer_container.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

static const size_t default_stats_window_sec = 10;

streamer_container_t::streamer_container_t(program_streamer_t* program_streamer) :
    program_streamer_(program_streamer), exit_flag_(false), max_priority_(STREAMER_PRIORITY_MIN)
{
    stat_analyzer_.reset(default_stats_window_sec);

    program_streamer_->set_callback(&stream_callback, this);
    control_thread_ = new boost::thread(boost::bind(&streamer_container_t::control_thread, this));
}

streamer_container_t::~streamer_container_t()
{
    remove_all_streamers();

    exit_flag_ = true;
    control_thread_->join();
    delete control_thread_;
}

bool streamer_container_t::add_streamer(streamer_object_t& streamer)
{
    add_streamer_params params;
    params.streamer = streamer;

    queue_.ExecuteCommand(ec_add_streamer, &params);

    return params.result;
}

bool streamer_container_t::remove_streamer(const dvblink::streamer_id_t& streamer_id)
{
    remove_streamer_params params;
    params.id = streamer_id;

    queue_.ExecuteCommand(ec_remove_streamer, &params);

    return params.result;
}

bool streamer_container_t::is_idle()
{
    is_idle_params params;

    queue_.ExecuteCommand(ec_is_idle, &params);

    return params.result;
}

void streamer_container_t::streamer_disconnected(const dvblink::streamer_id_t& id)
{
    streamer_disconnected_params* params = new streamer_disconnected_params();
    params->id = id;
    queue_.PostCommand(ec_streamer_disconnected, params);
}

void streamer_container_t::remove_all_streamers()
{
    remove_all_params params;

    queue_.ExecuteCommand(ec_remove_all, &params);
}

boost::uint32_t streamer_container_t::calculate_max_priority()
{
    boost::uint32_t ret_val = STREAMER_PRIORITY_MIN;

    streamer_map_t::iterator it = streamer_map_.begin();
    while (it != streamer_map_.end())
    {
        if (it->second->get_priority() > ret_val)
            ret_val = it->second->get_priority();

        ++it;
    }

    return ret_val;
}

bool streamer_container_t::compare_streamer_disconnected_params(const SDLCommandItem* item, void* user_param)
{
    bool ret_val = false;

    compare_streamer_disconnected_params_t* p = (compare_streamer_disconnected_params_t*)user_param;
    if (item->id == p->cmd_id)
    {
        streamer_disconnected_params* sp = (streamer_disconnected_params*)item->param;
        ret_val = boost::iequals(sp->id.get(), p->streamer_id.get());
    }

    return ret_val;
}

bool streamer_container_t::delete_streamer(const dvblink::streamer_id_t& id)
{
    bool ret_val = false;
    streamer_object_t streamer;

    bool b_stop = false;
    {
        boost::unique_lock<boost::shared_mutex> lock(lock_);
        if (streamer_map_.find(id) != streamer_map_.end())
        {
            streamer = streamer_map_[id];
            streamer_map_.erase(id);

            //update max streaming priority
            max_priority_ = calculate_max_priority();

            //check if program streamer has to be stopped
            b_stop = (streamer_map_.size() == 0);

            ret_val = true;
        }
    }

    if (streamer != NULL)
    {
        streamer->notify_streamer_deleted();
        streamer.reset();
    }

    //remove all pending notifications from this streamer
    compare_streamer_disconnected_params_t params;
    params.cmd_id = ec_streamer_disconnected;
    params.streamer_id = id;
    queue_.remove_commands(&compare_streamer_disconnected_params, &params);

    //if this is last streamer - stop program streamer
    if (b_stop)
    {
        log_info(L"streamer_container_t::delete_streamer. Last streamer was deleted. Stop program streamer");
        program_streamer_->stop();
    }

    return ret_val;
}

void streamer_container_t::control_thread()
{
    while (!exit_flag_)
    {
        //check command queue first
        SDLCommandItem* item;
        while (queue_.PeekCommand(&item))
        {
            switch (item->id)
            {
            case ec_add_streamer:
                {
                    add_streamer_params* params = (add_streamer_params*)item->param;
                    dvblink::streamer_id_t id = params->streamer->get_id();
                    log_info(L"streamer_container_t::control_thread. Added streamer %1%") % string_cast<EC_UTF8>(id.get());

                    bool b_start = false;
                    {
                        boost::unique_lock<boost::shared_mutex> lock(lock_);
                        streamer_map_[id] = params->streamer;

                        params->result = params->streamer->start(this);

                        if (!params->result)
                        {
                            log_error(L"streamer_container_t::control_thread. Streamer start failed for streamer %1%") % string_cast<EC_UTF8>(id.get());
                            streamer_map_.erase(id);
                            params->streamer.reset();
                        } else
                        {
                            //check if program streamer has to be started
                            b_start = (streamer_map_.size() == 1);
                        }

                        //update max streaming priority
                        max_priority_ = calculate_max_priority();

                    }
                    //if this is first streamer - start program streamer
                    if (b_start && params->result)
                    {
                        log_info(L"streamer_container_t::control_thread. First streamer was added. Start program streamer");
                        params->result = program_streamer_->start();

                        if (!params->result)
                        {
                            log_error(L"streamer_container_t::control_thread. program_streamer_->start() failed for streamer %1%") % string_cast<EC_UTF8>(id.get());
                            streamer_map_.erase(id);
                            params->streamer.reset();
                        }
                    }

                }
                break;
            case ec_remove_streamer:
                {
                    remove_streamer_params* params = (remove_streamer_params*)item->param;
                    log_info(L"streamer_container_t::control_thread. Removed streamer %1%") % string_cast<EC_UTF8>(params->id.get());
                    params->result = delete_streamer(params->id);
                }
                break;
            case ec_is_idle:
                {
                    is_idle_params* params = (is_idle_params*)item->param;
                    boost::unique_lock<boost::shared_mutex> lock(lock_);
                    params->result = streamer_map_.size() == 0;
                }
                break;
            case ec_streamer_disconnected:
                {
                    streamer_disconnected_params* params = (streamer_disconnected_params*)item->param;
                    log_info(L"streamer_container_t::control_thread. Removed disconnected streamer %1%") % string_cast<EC_UTF8>(params->id.get());
                    delete_streamer(params->id);
                }
                break;
            case ec_remove_all:
                {
                    std::vector<dvblink::streamer_id_t> ids;
                    {
                        boost::unique_lock<boost::shared_mutex> lock(lock_);

                        streamer_map_t::iterator it = streamer_map_.begin();
                        while (it != streamer_map_.end())
                        {
                            ids.push_back(it->first);
                            ++it;
                        }
                    }

                    for (size_t i=0; i<ids.size(); i++)
                    {
                        log_info(L"streamer_container_t::control_thread. Remove all (streamer %1%)") % string_cast<EC_UTF8>(ids[i].get());
                        delete_streamer(ids[i]);
                    }
                }
                break;
            default:
                break;
            }
            queue_.FinishCommand(&item);
        }
    	boost::this_thread::sleep(boost::posix_time::milliseconds(10));
    }
}

void streamer_container_t::stream_callback(const unsigned char* buf, unsigned long len, void* user_param)
{
    streamer_container_t* parent = (streamer_container_t*)user_param;

    parent->stat_analyzer_.process_buffer(buf, len);

    boost::shared_lock<boost::shared_mutex> lock(parent->lock_);
    streamer_map_t::iterator it = parent->streamer_map_.begin();
    while (it != parent->streamer_map_.end())
    {
        it->second->write_stream(buf, len);
        ++it;
    }
}

boost::uint32_t streamer_container_t::get_max_priority()
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);
    return max_priority_;
}

boost::uint32_t streamer_container_t::get_streamer_num()
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);
    return streamer_map_.size();
}

