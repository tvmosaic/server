/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <dl_channel_info.h>
#include <dl_ts_stream_waiter.h>

namespace dvblex { 

class program_streamer_t
{
public:
    typedef void (*streaming_callback_f)(const unsigned char* buf, unsigned long len, void* user_param);

public:
    program_streamer_t(const dvblink::channel_id_t& channel_id, const dvblink::channel_tuning_params_t& tune_params);
    virtual ~program_streamer_t();

    void set_callback(streaming_callback_f cb, void* user_param){callback_ = cb; user_param_ = user_param;}

    virtual bool start();
    virtual void stop();

    virtual bool get_signal_info(signal_info_t& si){return false;}

protected:
    streaming_callback_f callback_;
    void* user_param_;
    dvblink::channel_id_t channel_id_;
    dvblink::channel_tuning_params_t tune_params_;
	dvblink::engine::CTSPmtParser pmt_parser_;
	dvblink::engine::CTSStreamWaiter stream_waiter_;

    static void stream_waiter_callback(const unsigned char* buf, int len, void* param);
    void write_stream(const unsigned char* buf, unsigned long len);
};

}
