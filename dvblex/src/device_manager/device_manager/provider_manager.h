/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <dl_filesystem_path.h>

#include <dl_channel_info.h>
#include "scan_provider.h"

namespace dvblex { 

class provider_manager_t
{
public:
    provider_manager_t();

    bool init(const dvblink::filesystem_path_t& provider_storage_path);

    void get_providers(dvblink::standard_set_t standard, provider_info_map_t& providers);
    bool get_provider_details(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, provider_info_t& provider_info, provider_scan_list_t& scan_list);
    bool get_rescan_settings(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, parameters_container_t& settings);
    bool get_headend_info(const headend_description_for_device_t& headend, headend_info_t& headend_info);
    bool get_headend_from_provider(const provider_info_t& provider_info, const concise_param_map_t& scanner_settings, headend_info_t& headend_info);

protected:
    dvblink::filesystem_path_t provider_storage_path_;

    scan_provider_t* get_provider_by_name(const std::string& name);
};


}
