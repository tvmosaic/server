/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <sstream>

#include <boost/thread/mutex.hpp>
#include <dl_filesystem_path.h>
#include <device_manager/device_playback_src.h>
#include <drivers/aux_module.h>
#include <dl_channel_info.h>
#include <dl_device_info.h>
#include "device.h"
#include "directory_settings.h"

namespace dvblex { 

class device_factory_t
{
protected:

    typedef std::map<dvblink::device_id_t, concise_param_map_t> device_concise_param_map_t;
    typedef std::map<dvblink::device_id_t, manual_device_t> manual_device_map_t;

public:

    static std::string get_device_factory_from_id(const dvblink::device_id_t& id)
    {
        std::string ret_val = id.get();

        size_t n = id.get().find(get_id_sep_symbol());
        if (n != std::string::npos && n > 0)
            ret_val = id.get().substr(0, n);

        return ret_val;
    }

    device_factory_t(const dvblink::filesystem_path_t& device_config_path) :
        device_config_path_(device_config_path)
    {}

    virtual ~device_factory_t(){}

    virtual bool get_device_list(const dvblink::device_uuid_t& u, device_descriptor_list_t& device_list)
    {
        //append manual devices
        boost::unique_lock<boost::mutex> l(manual_devices_lock_);

        manual_device_map_t::iterator it = manual_devices_.begin();
        while (it != manual_devices_.end())
        {
            device_descriptor_t dd;
            get_device_desc_for_manual_device(it->first, it->second.params_, dd);
            device_list.push_back(dd);

            ++it;
        }

        return true;
    }

    virtual device_t* create_device(const dvblink::device_id_t& device_id, directory_settings_obj_t& dir_settings){return NULL;}

    virtual bool get_device_info(const dvblink::device_id_t& device_id, device_descriptor_t& device_info)
    {
        bool ret_val = false;

        boost::unique_lock<boost::mutex> l(manual_devices_lock_);

        if (manual_devices_.find(device_id) != manual_devices_.end())
        {
            get_device_desc_for_manual_device(device_id, manual_devices_[device_id].params_, device_info);
            ret_val = true;
        }

        return ret_val;
    }

    virtual source_type_e get_source_type_from_tuning_params(const dvblink::channel_tuning_params_t& tune_params){return st_unknown;}

    virtual std::string get_device_factory_name(){return "";}

    virtual void get_aux_list(dvblink::aux_module_list_t& aux_list){}

    virtual bool get_device_configurable_props(const dvblink::device_id_t& device_id, configurable_device_props_t& props)
    {
        props = configurable_device_props_t();
        return true;
    }

    virtual set_device_props_result_e set_device_configurable_props(const dvblink::device_id_t& device_id, const concise_param_map_t& props)
    {
        boost::unique_lock<boost::mutex> l(manual_devices_lock_);

        device_concise_param_map_[device_id] = props;
        return sdpr_success;
    }

    virtual void get_device_template_list(selectable_param_list_t& templates){}

    virtual bool create_manual_device(const concise_param_map_t& device_params)
    {
        boost::unique_lock<boost::mutex> l(manual_devices_lock_);

        //generate new device id
        std::string id;
        dvblink::engine::uuid::gen_uuid(id);

        std::stringstream strbuf;
        strbuf << get_device_factory_name() << get_id_sep_symbol() << id;

        manual_device_t md;
        md.id_ = strbuf.str();
        md.params_ = device_params;
        manual_devices_[md.id_] = md;

        return true;
    }

    virtual bool delete_manual_device(const dvblink::device_id_t& device_id)
    {
        bool ret_val = false;

        boost::unique_lock<boost::mutex> l(manual_devices_lock_);

        if (manual_devices_.find(device_id) != manual_devices_.end())
        {
            manual_devices_.erase(device_id);
            //delete also config params (if any)
            if (device_concise_param_map_.find(device_id) != device_concise_param_map_.end())
                device_concise_param_map_.erase(device_id);

            ret_val = true;
        }

        return ret_val;
    }

    virtual void add_manual_device(const manual_device_t& manual_device)
    {
        boost::unique_lock<boost::mutex> l(manual_devices_lock_);
        manual_devices_[manual_device.id_] = manual_device;
    }

    virtual void get_manual_devices(manual_device_list_t& manual_devices) 
    {
        boost::unique_lock<boost::mutex> l(manual_devices_lock_);
        manual_device_map_t::iterator it = manual_devices_.begin();
        while (it != manual_devices_.end())
        {
            manual_device_t md;
            md.id_ = it->first;
            md.params_ = it->second.params_;

            manual_devices.push_back(md);

            ++it;
        }
    }

    virtual device_playback_src_obj_t get_playback_src(const dvblink::device_id_t& device_id){return device_playback_src_obj_t();}

protected:
    dvblink::filesystem_path_t device_config_path_;
    device_concise_param_map_t device_concise_param_map_;
    manual_device_map_t manual_devices_;
    boost::mutex manual_devices_lock_;

    static std::string get_id_sep_symbol() {return "#";}

    virtual void get_device_desc_for_manual_device(const dvblink::device_id_t& id, 
        const concise_param_map_t& params, device_descriptor_t& dd){}
};

}
