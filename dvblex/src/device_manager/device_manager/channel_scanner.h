/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>

#include <boost/thread.hpp>

#include <dl_parameters.h>
#include <dl_channel_info.h>
#include <dl_scan_info.h>
#include "scan_provider.h"

namespace dvblex { 

class channel_scanner_t
{
public:
    struct network_scan_entry_t
    {
        std::string network_id_;
        std::string name_;
        provider_scan_list_t scan_data_;
    };

    typedef std::vector<network_scan_entry_t> network_scan_results_t;

public:
    channel_scanner_t();
	virtual ~channel_scanner_t();

    bool scan(const provider_info_t& provider_info, const provider_scan_list_t& scan_data, const concise_param_map_t& scanner_settings);
    bool scan(const dvblink::scan_network_id_t& network_id);
    void cancel();

    void get_progress_info(channel_scanner_progress_t& progress_info);
    void get_scanned_channels(provider_info_t& provider_info, transponder_list_t& scanned_channels, concise_param_map_t& scanner_settings);
    void get_scan_log(scan_log_t& scan_log);
    void get_scanned_networks(scanned_network_list_t& networks);

protected:
    boost::thread* scanning_thread_;
    boost::mutex scan_info_lock_;
    bool exit_flag_;
    transponder_list_t scanned_channels_;
    channel_scanner_progress_t progress_info_;
    scan_log_t scan_log_;
	provider_info_t provider_info_;
	provider_scan_list_t scan_data_;
	concise_param_map_t scanner_settings_;
    network_scan_results_t network_scan_results_;
    std::string network_id_;

	void scan_thread_function();
    void scan_channels(const provider_scan_list_t& scan_data, const dvblink::scan_network_id_t& network_id);
    channel_scanner_state_e get_state();
    void set_state(channel_scanner_state_e state);

    //overrides for child classes
    virtual bool start(){return false;}
    virtual void stop(){}
    virtual bool do_scan(const std::string& tune_params, const dvblink::scan_network_id_t& network_id, transponder_list_t& found_channels, scan_log_entry_t& log_entry){return false;}
    virtual bool scan_networks(const provider_scan_list_t& scan_data, network_scan_results_t& network_scan_results, scan_log_entry_t& log_entry){return false;}
    virtual bool is_network_scan(const provider_info_t& provider_info){return false;}
};

}
