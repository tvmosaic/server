/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_platforms.h>
#include <dl_utils.h>
#include <dl_ts_info.h>
#include "eit_epg_converter.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

CDLTSEPGEITConverter::CDLTSEPGEITConverter(const wchar_t* language, bool ignoreShortDesc, bool use_event_id)
{
	if (language != NULL)
	{
	    //add language itself to the list
        std::wstring wstr = language;
        //add all its ISO 639 synonims to the list
	    std::string lang;
        ConvertUCToMultibyte(EC_UTF8, language, lang);
        std::vector<std::string> synonyms;
        GetISO_639_3_Synonyms(lang.c_str(), synonyms);
        for (size_t i=0; i<synonyms.size(); i++)
        {
            ConvertMultibyteToUC(EC_UTF8, synonyms[i].c_str(), wstr);
            boost::to_upper(wstr);
            eit_language_.push_back(wstr);
        }
	}
	
    use_event_id_ = use_event_id;
    m_bIgnoreShortDesc = ignoreShortDesc;
}

CDLTSEPGEITConverter::~CDLTSEPGEITConverter()
{
}

void CDLTSEPGEITConverter::Reset()
{
    m_EventList.clear();
}

static bool CheckKeyword(const std::wstring& src, const wchar_t* keyword)
{
	std::wstring keyword_cpy = keyword;
	std::wstring src_cpy = src;
	boost::to_lower(src_cpy);
	boost::to_lower(keyword_cpy);
	return wcsstr(src_cpy.c_str(), keyword_cpy.c_str()) != NULL;
}

static bool MakeStringFromKeywords(CDLTSEPGKeywordList& keywords,
    const wchar_t* sep, std::wstring& res)
{
	res.clear();
	for (unsigned int i=0; i<keywords.size(); i++)
	{
		if (!IsStringEmpty(keywords[i].c_str()))
		{
			if (i > 0)
				res += sep;
			res += keywords[i];
		}
	}
	return res.size() > 0;
}

void CDLTSEPGEITConverter::AddEITSection(unsigned char* section_ptr)
{
    try
    {
	    //maximum length of all program decsriptions inside the program loop
	    int max_prog_len = ts_process_routines::GetSectionLength(section_ptr) - 18;
    	
        unsigned short onid = ts_process_routines::read_u32bit_from_memory(section_ptr + 10, 2, 0xFF);
        unsigned short sid = ts_process_routines::read_u32bit_from_memory(section_ptr + 3, 2, 0xFF);

        //Check if this section contains actual program data
	    if (max_prog_len > 0)
	    {
		    //beginning of the program loop
		    unsigned char* cur_ptr = section_ptr + 14;
		    int section_prog_offs = 0;
		    //go through all available programs in the loop
		    while (section_prog_offs < max_prog_len)
		    {
                DLEPGEvent epgevent;
			    m_LangEventMap.clear();

			    //Total length of descriptor loop of the currently processed program
			    int desc_total_len = (((WORD)(cur_ptr[10] & 0x0F)) << 8) | cur_ptr[11];

			    //get event id
			    WORD eid = (((WORD)cur_ptr[0]) << 8) | cur_ptr[1];
                std::stringstream strbuf;
	            strbuf.clear(); strbuf.str("");
                strbuf << eid;
                epgevent.id_ = strbuf.str();

			    //get start date
			    WORD mjd = (((WORD)cur_ptr[2]) << 8) | cur_ptr[3];
			    //get start time
			    int total_sec;
			    GetHHMMSS(cur_ptr+4, NULL, NULL, NULL, &total_sec);
			    time_t start_time = MJD2time_t(mjd, total_sec);
                epgevent.m_StartTime = start_time;
			    //Get program duration
			    GetHHMMSS(cur_ptr+7, NULL, NULL, NULL, &total_sec);
                epgevent.m_Duration = total_sec;

			    //now go through all descriptors to extract the rest of program info
			    unsigned char* desc_ptr = cur_ptr + 12;
			    int dtl = desc_total_len;
			    while (dtl > 0)
			    {
				    unsigned char desc_tag = desc_ptr[0];
				    int desc_len = (int)(desc_ptr[1]) + 2;
				    switch (desc_tag)
				    {
				    case 0x4D:
					    //short event descriptor
					    ProcessShortEventDescr(onid, m_LangEventMap, desc_ptr);
					    break;
				    case 0x4E:
					    //extended event descriptor
                        ProcessExtendedDescr(onid, m_LangEventMap, desc_ptr);
					    break;
				    case 0x50:
					    //component descriptor
                        ProcessComponentDescr(epgevent, desc_ptr);
					    break;
				    case 0x54:
					    //content descriptor
					    ProcessContentDescr(epgevent, desc_ptr);
					    break;
				    default:
					    break;
				    }
				    dtl -= desc_len;
				    desc_ptr += desc_len;
				    //Log error if the rest of descriptor length is negative (parsing error!)
				    if (dtl < 0)
				    {
					    log_error(L"CDLTSEPGEITConverter::AddEITSection. The rest of descriptor length is negative. Parser error?!");
				    }
			    }

			    //choose event with the right language
			    SLangEventDesc lang_epgevent;
			    bool bfound = false;
			    for (size_t lang_idx = 0; lang_idx<eit_language_.size(); lang_idx++)
			    {
			        if (m_LangEventMap.find(eit_language_[lang_idx]) != m_LangEventMap.end())
			        {
				        lang_epgevent = m_LangEventMap[eit_language_[lang_idx]];
				        bfound = true;
				        break;
			        }
			    }
			    if (!bfound)
			    {
				    if (m_LangEventMap.find(L"ENG") != m_LangEventMap.end())
				    {
					    lang_epgevent = m_LangEventMap[L"ENG"];
				    } else
				    {
					    if (m_LangEventMap.size() > 0)
					    {
						    lang_epgevent = m_LangEventMap.begin()->second;
					    }
				    }
			    }

                //concatenate the extended text parts
			    if (lang_epgevent.extTextMap.size() > 0 || lang_epgevent.keywordMap.size() > 0)
                {
                    using namespace std;
                    std::wstring newDescription;
                    //Add a description
				    for (int i=0; i<= max(lang_epgevent.maxDescNum, (int)lang_epgevent.extTextMap.size()); i++)
                    {
					    if (lang_epgevent.extTextMap.find(i) != lang_epgevent.extTextMap.end())
						    newDescription += lang_epgevent.extTextMap[i];
                    }

                    //keywords
				    CDLTSEPGKeywordMap::iterator it = lang_epgevent.keywordMap.begin();
				    while (it != lang_epgevent.keywordMap.end())
                    {
					    //process only non-empty values
					    if (it->second.size() > 0)
					    {
						    //check for known tags first
						    if (CheckKeyword(it->first, L"actor"))
						    {
							    std::wstring kwd_str;
							    if (MakeStringFromKeywords(it->second, L"/", kwd_str))
							        ConvertUCToMultibyte(EC_UTF8, kwd_str.c_str(), epgevent.m_Actors);
						    } else
						    {
							    if (CheckKeyword(it->first, L"director"))
							    {
								    std::wstring kwd_str;
								    if (MakeStringFromKeywords(it->second, L"/", kwd_str))
							            ConvertUCToMultibyte(EC_UTF8, kwd_str.c_str(), epgevent.m_Directors);
							    } else
							    {
								    if (CheckKeyword(it->first, L"year") && it->second.size() > 0)
								    {
									    epgevent.m_Year = boost::lexical_cast<unsigned>(it->second[0].c_str());
								    } else
								    {
									    //by default - append the keyword and its description to description string
									    std::wstring kwd_str;
									    if (MakeStringFromKeywords(it->second, L", ", kwd_str))
									    {
										    if (newDescription.size() > 0)
											    newDescription += L"\n";
										    newDescription += it->first;

										    newDescription += L": " + kwd_str;
									    }
								    }
							    }
						    }
					    }

                        it++;
                    }

                    if (newDescription.size() > 0)
				    {
					    if (m_bIgnoreShortDesc)
						    lang_epgevent.shortdesc = newDescription;
					    else
					    {
						    if (lang_epgevent.shortdesc.size() > 0)
							    lang_epgevent.shortdesc += L"\n";
						    lang_epgevent.shortdesc += newDescription;
					    }
				    }
                }
			    //merge both language-specific and non specific parts
	            ConvertUCToMultibyte(EC_UTF8, lang_epgevent.shortdesc.c_str(), epgevent.m_ShortDesc);
	            ConvertUCToMultibyte(EC_UTF8, lang_epgevent.name.c_str(), epgevent.m_Name);
	            ConvertUCToMultibyte(EC_UTF8, lang_epgevent.language.c_str(), epgevent.m_Language);

                if (!use_event_id_)
                {
                    //use start time instead of event id
                    std::stringstream strbuf;
                    strbuf << epgevent.m_StartTime;
                    epgevent.id_ = strbuf.str();
                }

			    m_EventList.push_back(epgevent);

                //Update program loop offset
			    section_prog_offs += desc_total_len + 12;
			    cur_ptr += desc_total_len + 12;
    			
                //Check our parser on correctness
			    if (section_prog_offs > max_prog_len)
			    {
				    log_error(L"CDLTSEPGEITConverter::AddEITSection. The rest of program descriptor loop length is negative. Parser error?!");
			    }
		    }
	    }
    }
    catch (std::exception& x)
    {
        log_error(L"EXCEPTION in CDLTSEPGEITConverter::AddEITSection(): %1%") % x.what();
    }
    catch (...)
    {
        log_error(L"EXCEPTION in CDLTSEPGEITConverter::AddEITSection()");
    }
}

time_t CDLTSEPGEITConverter::MJD2time_t(int mjd, int secs)
{
    return (mjd - 40587)* 86400 + secs;     /* 40587 is the 1.1.1970 */
}

void CDLTSEPGEITConverter::GetHHMMSS(unsigned char* bcd_time, int* hh, int* mm, int* ss, int* total_sec)
{
	int h = ((bcd_time[0]&0xF0) >> 4)*10+(bcd_time[0]&0x0F);
	if (hh != NULL)
		*hh = h;
	int m = ((bcd_time[1]&0xF0) >> 4)*10+(bcd_time[1]&0x0F);
	if (mm != NULL)
		*mm = m;
	int s = ((bcd_time[2]&0xF0) >> 4)*10+(bcd_time[2]&0x0F);
	if (ss != NULL)
		*ss = s;
	if (total_sec != NULL)
		*total_sec = h*3600 + m*60 + s;
}

void CDLTSEPGEITConverter::ProcessContentDescr(DLEPGEvent& epgevent, unsigned char* descr_ptr)
{
	unsigned char nibble1 = (descr_ptr[2] >> 4);
	unsigned char nibble2 = (descr_ptr[2] & 0x0F);
	
    switch (nibble1)
	{
	case 1:
//        epgevent.m_IsMovie = true;
        switch (nibble2)
        {
        case 0:
        case 7:
            epgevent.m_IsDrama = true;
            break;
        case 1:
            epgevent.m_IsThriller = true;
            break;
        case 2:
            epgevent.m_IsAction = true;
            break;
        case 3:
            epgevent.m_IsScienceFiction = true;
            break;
        case 4:
            epgevent.m_IsComedy = true;
            break;
        case 5:
            epgevent.m_IsSoap = true;
            break;
        case 6:
            epgevent.m_IsRomance = true;
            break;
        case 8:
            epgevent.m_IsAdult = true;
            break;
        }
		break;
	case 2:
        switch (nibble2)
        {
        case 0:
        case 1:
        case 2:
            epgevent.m_IsNews = true;
            break;
        case 3:
            epgevent.m_IsDocumentary = true;
            break;
        }
		break;
	case 3:
	case 10:
        epgevent.m_IsSpecial = true;
		break;
	case 4:
        epgevent.m_IsSports = true;
		break;
	case 5:
        epgevent.m_IsKids = true;
		break;
	case 6:
        epgevent.m_IsMusic = true;
		break;
	case 7:
		break;
	case 8:
		break;
	case 9:
        epgevent.m_IsEducational = true;
		break;
	}
}

void CDLTSEPGEITConverter::ProcessShortEventDescr(unsigned short onid, TLangEventMap& eventmap, unsigned char* descr_ptr)
{
    try
    {
	    //Read language identifier (iso-8859-1 encoded)
	    std::wstring lang;
	    ConvertMultibyteToUC(EC_ISO_8859_1, (const char*)(descr_ptr + 2), 3, lang);
	    boost::to_upper(lang);
	    if (eventmap.find(lang) == eventmap.end())
	    {
		    SLangEventDesc sd;
		    eventmap[lang] = sd;
	    }
	    eventmap[lang].language = lang;
	    //Retreive and process event name
	    std::wstring str;
	    int text_len = descr_ptr[5];
	    convert_annexa_to_unicode_for_onid(onid, descr_ptr + 6, text_len, str);
	    eventmap[lang].name = str;

	    //Retreive event description
	    int desc_offs = 6+text_len;
	    text_len = descr_ptr[desc_offs];
	    if (text_len > 0)
	    {
		    convert_annexa_to_unicode_for_onid(onid, descr_ptr + desc_offs + 1, text_len, str);
		    eventmap[lang].shortdesc = str;
	    }
    }
    catch (std::exception& x)
    {
        log_error(L"EXCEPTION in CDLTSEPGEITConverter::ProcessShortEventDescr(): %1%") % x.what();
    }
    catch (...)
    {
        log_error(L"EXCEPTION in CDLTSEPGEITConverter::ProcessShortEventDescr()");
    }
}

void CDLTSEPGEITConverter::ProcessComponentDescr(DLEPGEvent& epgevent, unsigned char* descr_ptr)
{
	unsigned char stream_content = (descr_ptr[2] & 0x0F);
	unsigned char component_type = descr_ptr[3];
    if (stream_content == 0x01 && component_type >= 0x09 && component_type <= 0x10)
        epgevent.m_IsHDTV = true;
}

void CDLTSEPGEITConverter::ProcessExtendedDescr(unsigned short onid, TLangEventMap& eventmap, unsigned char* descr_ptr)
{
    try
    {
	    //Read language identifier (iso-8859-1 encoded)
	    std::wstring lang;
	    ConvertMultibyteToUC(EC_ISO_8859_1, (const char*)(descr_ptr + 3), 3, lang);
	    boost::to_upper(lang);

	    if (eventmap.find(lang) == eventmap.end())
	    {
		    SLangEventDesc sd;
		    eventmap[lang] = sd;
	    }

	    unsigned char desc_num = (descr_ptr[2] >> 4);
	    eventmap[lang].maxDescNum = (descr_ptr[2] & 0x0F);
        //items
        int items_len = descr_ptr[6];
        int text_offs = items_len + 7;
        if (items_len > 0)
        {
            unsigned char* item_ptr = descr_ptr + 7;
            while (items_len > 0)
            {
                //item description
                std::wstring keystr;
	            int item_desc_len = item_ptr[0];
	            convert_annexa_to_unicode_for_onid(onid, item_ptr + 1, item_desc_len, keystr);

                //item
                std::wstring valuestr;
                int item_len = item_ptr[item_desc_len+1];
	            convert_annexa_to_unicode_for_onid(onid, item_ptr + item_desc_len + 2, item_len, valuestr);

			    if (eventmap[lang].keywordMap.find(keystr) == eventmap[lang].keywordMap.end())
                {
                    CDLTSEPGKeywordList kwl;
				    eventmap[lang].keywordMap[keystr] = kwl;
                }
			    eventmap[lang].keywordMap[keystr].push_back(valuestr);

                item_ptr += item_len + item_desc_len + 2;
                items_len -= item_len + item_desc_len + 2;
            }
        }
        //text
        int text_len = descr_ptr[text_offs];
        
        if (text_len > 0)
        {
            std::wstring str;
            convert_annexa_to_unicode_for_onid(onid, descr_ptr + text_offs + 1, text_len, str);
		    if (eventmap[lang].extTextMap.find(desc_num) != eventmap[lang].extTextMap.end())
			    eventmap[lang].extTextMap[desc_num] += str;
		    else
			    eventmap[lang].extTextMap[desc_num] = str;
        }
    }
    catch (std::exception& x)
    {
        log_error(L"EXCEPTION in CDLTSEPGEITConverter::ProcessExtendedDescr(): %1%") % x.what();
    }
    catch (...)
    {
        log_error(L"EXCEPTION in CDLTSEPGEITConverter::ProcessExtendedDescr()");
    }
}

static bool EPGItemStartTimeComp(DLEPGEvent first_item, DLEPGEvent second_item)
{
	return first_item.m_StartTime < second_item.m_StartTime;
}

void CDLTSEPGEITConverter::GetEvents(DLEPGEventList& event_list)
{
    try
    {
        //sort events on time
        std::sort(m_EventList.begin(), m_EventList.end(), EPGItemStartTimeComp);
        event_list = m_EventList;
    }
    catch (std::exception& x)
    {
        log_error(L"EXCEPTION in CDLTSEPGEITConverter::GetEvents(): %1%") % x.what();
    }
    catch (...)
    {
        log_error(L"EXCEPTION in CDLTSEPGEITConverter::GetEvents()");
    }
}

void CDLTSEPGEITConverter::convert_annexa_to_unicode_for_onid(unsigned short onid, unsigned char* annexa_text, int text_len, std::wstring& out_str)
{
    out_str = L"";

    if (text_len > 0)
    {
        unsigned char enc = annexa_text[0];
        if (enc == 0x01 && onid == 318)
        {
            //force ISO-6937 encoding: this is CYFRA+
            std::vector<std::string> resText;
            if (ProcessControlChInAnnexAText(annexa_text, text_len, enc, resText))
            {
                for (unsigned int i=0; i<resText.size(); i++)
                {
                    if (out_str.size() > 0)
                        out_str += L"\n";

                    if (resText[i].size() > 0)
                    {
                        std::wstring cur_str;

			            ConvertMultibyteToUC(EC_ISO_6937, resText[i].c_str(), resText[i].size(), cur_str);
			            //fix for WinXP that cannot process sometimes CP 20269 correctly)
			            if (cur_str.size() == 0)
				            ConvertMultibyteToUC(EC_ISO_8859_1, resText[i].c_str(), resText[i].size(), cur_str); //ISO 8859-1

                        out_str += cur_str;
                    }
                }
            }
        }
        else
        {
            ConvertAnnexATextToUnicode(annexa_text, text_len, out_str);
        }
   }
}
