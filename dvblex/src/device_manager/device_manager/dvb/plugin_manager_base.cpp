/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_utils.h>
#include <dl_ts_info.h>
#include "plugin_manager_base.h"

using namespace dvblink::engine;
using namespace dvblink::logging;

tvs_plugin_manager_base::tvs_plugin_manager_base() :
    csa_handler_(std::auto_ptr<CTVSCSAHandler>(new CTVSCSAHandler())),
    circle_buffer_(std::auto_ptr<dvblink::engine::ts_circle_buffer>(new ts_circle_buffer(16, 16 * TS_PACKET_SIZE, L"tvs_plugin_manager_base"))),
    streaming_thread_(NULL),
    stream_exit_flag_(false),
	add_pid_func_(NULL),
    user_param_(NULL)
{
}

tvs_plugin_manager_base::~tvs_plugin_manager_base()
{
}

bool tvs_plugin_manager_base::init()
{
	pid_set_.reset();
	pids_to_decrypt_.clear();

	//Initialize CSA handler
	csa_handler_->Init();
	
    //Create streaming thread
	stream_exit_flag_ = false;
	streaming_thread_ = new boost::thread(boost::bind(&tvs_plugin_manager_base::streaming_thread, this));

	return true;
}

void tvs_plugin_manager_base::term()
{
	//Stop streaming thread
	if (streaming_thread_ != NULL)
	{
		//signal streaming thread break
		stream_exit_flag_ = true;
		streaming_thread_->join();
		delete streaming_thread_;

        streaming_thread_ = NULL;
	}
	
    //Destroy CSA handler
	csa_handler_->Term();
}

void tvs_plugin_manager_base::process_stream(const unsigned char* buf, size_t len)
{
    //send stream to CSA manager
	csa_handler_->DecryptStream(const_cast<unsigned char*>(buf), len, pids_to_decrypt_);

    //write data to the circle buffer
    pid_filter::pid_filter_map_t filters;
    pid_set_.snapshot(filters);
    
    const unsigned char* curpack = buf;
    for (size_t offset = 0; offset < len; offset += TS_PACKET_SIZE)
    {
        if (filters.find(ts_process_routines::GetPacketPID(curpack)) != filters.end())
        {
            circle_buffer_->write_stream(curpack, TS_PACKET_SIZE);
        }
        curpack += TS_PACKET_SIZE;
    }
}

bool tvs_plugin_manager_base::channel_changed(TProgramm* tuner_info, plugin_program_extra_info& extra_info)
{
	//!! important!! The derived classes should take the scoped lock!!
//    boost::mutex::scoped_lock lock(lock_);

	//Clear pid map
	pid_set_.reset();
	pids_to_decrypt_.clear();

	//Clear circle buffer
	circle_buffer_->reset();

	//Notify CSA handler
	csa_handler_->ChannelChanged(tuner_info);

	return true;
}

void tvs_plugin_manager_base::streaming_thread()
{
	//check in the loop whether new streaming data is available
	while (!stream_exit_flag_)
	{
        ts_circle_buffer::node_t node = circle_buffer_->tear_node(100);
        if (node != NULL)
		{
            boost::mutex::scoped_try_lock lock(lock_, boost::try_to_lock);
			if (lock.owns_lock())
			{
                pid_filter::pid_filter_map_t filters;
                pid_set_.snapshot(filters);
			
				size_t num = node->size_to_read() / TS_PACKET_SIZE;

                const unsigned char* curpack;
				for (size_t i = 0; i < num; i++)
				{
					curpack = node->data() + i * TS_PACKET_SIZE;
					unsigned short pid = ts_process_routines::GetPacketPID(curpack);
					if (filters.find(pid) != filters.end())
                    {
						process_requested_packet(curpack, TS_PACKET_SIZE);
                    }
				}
			}
		
            circle_buffer_->put_node(node);
        }
	}
}

void tvs_plugin_manager_base::add_pid(unsigned short pid)
{
	pid_set_.add_filter(pid);

    boost::mutex::scoped_lock lock(add_pid_lock_);
    if (add_pid_func_ != NULL)
	    add_pid_func_(pid, user_param_);
}

void tvs_plugin_manager_base::add_pids_to_decrypt(std::vector<unsigned short>& pids)
{
    for (size_t i=0; i<pids.size(); i++)
        pids_to_decrypt_[pids[i]] = pids[i];
}

void tvs_plugin_manager_base::set_addpid_func(LPCB_TVS_PLUGMUN_ADDPID addpid_func, void* user_param)
{
    boost::mutex::scoped_lock lock(add_pid_lock_);
	add_pid_func_ = addpid_func;
    user_param_ = user_param;
}

void tvs_plugin_manager_base::reset_addpid_func()
{
    boost::mutex::scoped_lock lock(add_pid_lock_);
	add_pid_func_ = NULL;
    user_param_ = NULL;
}
