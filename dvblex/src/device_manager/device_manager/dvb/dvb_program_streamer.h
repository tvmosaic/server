/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include "../program_streamer.h"

class transponder_streamer;
class tvs_program_streamer;

namespace dvblex { 

class dvb_program_streamer_t : public program_streamer_t
{
public:
    dvb_program_streamer_t(transponder_streamer* transponder_streamer, const std::string& channel_id, const concise_channel_tune_info_t& tune_params);
    virtual ~dvb_program_streamer_t();

    virtual bool start();
    virtual void stop();

    virtual bool get_signal_info(signal_info_t& si);

protected:
    transponder_streamer* transponder_streamer_;
    tvs_program_streamer* program_streamer_;

	static void program_stream_callback(std::wstring& channel_id, const unsigned char* buf, unsigned long len, void* user_param);
};

}
