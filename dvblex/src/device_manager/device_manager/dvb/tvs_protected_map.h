/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef _TVSOURCEPROTECTEDMAP_H_
#define _TVSOURCEPROTECTEDMAP_H_

#include <boost/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/locks.hpp>
#include <map>
#include <set>
#include <vector>
#include <list>

class pid_filter
{
public:
    typedef std::map<unsigned short, unsigned short> pid_filter_map_t;

public:
    pid_filter()
    {
    }
    
    void reset()
    {
        boost::mutex::scoped_lock lock(lock_);
        filters_.clear();
    }
    
    void add_filter(unsigned short pid)
    {
        boost::mutex::scoped_lock lock(lock_);
        filters_[pid] = pid;
    }

    void delete_filter(unsigned short pid)
    {
        boost::mutex::scoped_lock lock(lock_);
        filters_.erase(pid);
    }

    void snapshot(pid_filter_map_t& filters)
    {
        boost::mutex::scoped_lock lock(lock_);
        filters = filters_;
    }
    
protected:
    boost::mutex lock_;
    pid_filter_map_t filters_;    
};


template <class T> class protected_set
{
public:
    typedef std::set<T> container_t;

public:
    void add(T val)
    {
        boost::mutex::scoped_lock lock(lock_);
        set_.insert(val);
    }

    bool find(T val)
    {
        boost::mutex::scoped_lock lock(lock_);
        return (set_.find(val) != set_.end());
    }

    void copy(container_t& new_set)
    {
        boost::mutex::scoped_lock lock(lock_);
        new_set = set_;
    }

    void clear()
    {
        boost::mutex::scoped_lock lock(lock_);
        set_.clear();
    }

protected:
    container_t set_;
    boost::mutex lock_;
};

template <class T1, class T2> class protected_map
{
public:
    typedef std::map<T1, T2> container_t;

public:
	void add(T1 t1, T2 t2)
	{
        boost::mutex::scoped_lock lock(lock_);
		map_[t1] = t2;
	}

	bool find(T1 t1, T2* t2 = NULL)
	{
	    boost::mutex::scoped_lock lock(lock_);

        bool ret_val = false;
		if (map_.find(t1) != map_.end())
		{
		    if (t2 != NULL)
			    *t2 = (map_.find(t1))->second;
			ret_val = true;
		}
		return ret_val;
	}

	void copy(container_t& new_map)
	{
        boost::mutex::scoped_lock lock(lock_);
		new_map = map_;
	}

	void clear()
	{
	    boost::mutex::scoped_lock lock(lock_);
		map_.clear();
	}

    void lock()
    {
        lock_.lock();
    }

    void unlock()
    {
        lock_.unlock();
    }

    container_t* get_container() {return &map_;}
    
protected:
	container_t map_;
	boost::mutex lock_;
};

template <class T> class protected_vector
{
public:
    typedef std::vector<T> container_t;

public:
	void add(T t)
	{
        boost::mutex::scoped_lock lock(lock_);
		vector_.push_back(t);
	}

	size_t count()
	{
        boost::mutex::scoped_lock lock(lock_);
		return vector_.size();
	}

	bool get(int i, T& t)
	{
        boost::mutex::scoped_lock lock(lock_);

        bool ret_val = false;
		if (i < vector_.size())
		{
			t = vector_.at(i);
			ret_val = true;
		}
		return ret_val;
	}

	void copy(container_t& new_vector)
	{
        boost::mutex::scoped_lock lock(lock_);
		new_vector = vector_;
	}

	void clear()
	{
        boost::mutex::scoped_lock lock(lock_);
		vector_.clear();
	}

protected:
	container_t vector_;
	boost::mutex lock_;
};

template <class T> class protected_list
{
public:
    typedef std::list<T> container_t;

public:
	void push_back(T t)
	{
        boost::mutex::scoped_lock lock(lock_);
		list_.push_back(t);
	}

	bool pop_front(T& t)
	{
        boost::mutex::scoped_lock lock(lock_);

        bool ret_val = false;
		if (list_.size() > 0)
		{
			t = list_.front();
			list_.pop_front();
			ret_val = true;
		}
		return ret_val;
	}

	void copy(container_t& new_list)
	{
        boost::mutex::scoped_lock lock(lock_);
		new_list = list_;
	}

	void clear()
	{
        boost::mutex::scoped_lock lock(lock_);
		list_.clear();
	}

protected:
	container_t list_;
    boost::mutex lock_;
};

///////////////////////////////////////////////////////////////////////////////
#endif //_TVSOURCEPROTECTEDMAP_H_
