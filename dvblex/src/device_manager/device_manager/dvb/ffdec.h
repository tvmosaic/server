/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef _TVSOURCEFFDEC_H_
#define _TVSOURCEFFDEC_H_

#include <string>
#include "csa_base.h"


class CTVSFFDecCSA : public CTVSCSABase
{
public:
    CTVSFFDecCSA();
    ~CTVSFFDecCSA();

    virtual bool Init();
    virtual bool Term();

    virtual void SetNewKey(unsigned char* key);
    virtual void Decrypt(unsigned char* buffer, int len);

private:
    unsigned char* m_KeyStruct;
};

///////////////////////////////////////////////////////////////////////////////
#endif //_TVSOURCEFFDEC_H_
