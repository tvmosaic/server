/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <list>
#include "plugin_manager_base.h"

typedef std::list<tvs_plugin_manager_base*> tvs_plugins_vector_t;

class tvs_plugin_collection
{
public:
	tvs_plugin_collection();
	~tvs_plugin_collection();

	bool init(const dvblink::filesystem_path_t& plugins_dir);
	void term();

	tvs_plugin_manager_base* get_free_plugin_manager();
	void return_plugin_manager(tvs_plugin_manager_base* plugin_manager);
	int get_plugins_num(){return plugins_num_;}

protected:
	tvs_plugins_vector_t plugins_;
	int plugins_num_;
};

