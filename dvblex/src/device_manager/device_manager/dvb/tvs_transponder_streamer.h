/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/locks.hpp>
#include <vector>
#include <dl_ts_info.h>
#include "tvs_core_def.h"
#include "tvs_protected_map.h"
#include "tvs_stream_src.h"
#include "tvs_program_streamer.h"

#if defined TVS_PLUGINS_SUPPORTED
	class tvs_plugin_collection;
	class plugin_stream_thread;
#endif

class stream_checker;

namespace dvblink {
	class tuner_t;
}

typedef void (*transponder_streamer_cb_f)(std::wstring& channel_id, const unsigned char* buf, unsigned long len, void* user_param);

struct transponder_channel_desc
{
	std::wstring channel_id;
	tvs_program_streamer* program_src_;
#if defined TVS_PLUGINS_SUPPORTED
	plugin_stream_thread* stream_thread_;
#endif
};

typedef std::map<std::wstring, transponder_channel_desc> transponder_channels_map_t;

class transponder_streamer
{
public:
	transponder_streamer(dvblink::tuner_t* tuner, const dvblink::filesystem_path_t& shared_device_dir);
	~transponder_streamer();

    bool init(dvblink::DL_E_TUNER_TYPES tuner_type, transponder_streamer_cb_f raw_stream_cb = NULL, void* user_param = NULL);
	void term();

	bool start_channel(tvs_program_streamer* program_streamer);
	bool stop_channel(std::wstring& channel_id);

	int get_num_active_programs(){return active_channels_.size();}
	bool get_signal_stats(unsigned char* level, unsigned char* quality, unsigned char* locked);
    dvblink::DL_E_TUNER_TYPES get_tuner_type(){return options_.tuner_type_;}

	bool channel_can_join(transponder_tuning_info_t& tuning_info);
    bool is_idle();

protected:
    dvblink::tuner_t* tuner_;
    tvs_stream_source_options_t options_;
	void* user_param_;
    transponder_streamer_cb_f raw_stream_cb_;
	CTVSStreamSource* stream_src_;
	transponder_channels_map_t active_channels_;
	std::wstring current_transponder_hash_;
	TVSC_DeviceInfo active_device_;
	stream_checker* stream_checker_;
	boost::shared_mutex lock_;
	std::wstring raw_cb_channel_id_;
    int idle_timeout_;
	unsigned long idle_timer_timeout_;

	boost::thread* idle_thread_;
    void idle_thread_func();
    boost::mutex idle_timeout_lock_;
    bool idle_thread_quit_;
    dvblink::filesystem_path_t shared_device_dir_;
	
    static void __stdcall stream_src_callback(const unsigned char* buf, unsigned long len, void* user_param);
	bool tune_transponder(transponder_tuning_info_t& tuning_info);
	bool tune_transponder_impl(transponder_tuning_info_t& tuning_info);
	void stop_all_streams();

#if defined TVS_PLUGINS_SUPPORTED
	std::auto_ptr<tvs_plugin_collection> plugin_collection_;
#endif

	void start_idle_timer();
	void stop_idle_timer();
	void idle_timer_function(const boost::system::error_code& e);
};
