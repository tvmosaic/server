/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>
#include <dl_types.h>
#include <dl_parameters.h>
#include <dl_epgevent.h>

namespace dvblex { 

enum epg_module_status_e
{
    ems_unknown,
    ems_in_progress,
    ems_finished_error,
    ems_finished_success,
    ems_finished_aborted
};

class epg_host_control_t
{
public:
    virtual void add_pid(unsigned short pid){}
};

struct dvb_channel_epg_desc_t
{
    dvb_channel_epg_desc_t() : 
        nid_(0), tid_(0), sid_(0)
        {}

    bool is_valid_channel_id() const {return !channel_id_.empty();}

    dvblink::channel_id_t channel_id_;
    boost::uint32_t nid_;
    boost::uint32_t tid_;
    boost::uint32_t sid_;
    dvblink::engine::DLEPGEventList events_;
};

typedef std::vector<dvb_channel_epg_desc_t> dvb_channel_epg_desc_list_t;

class dvb_epg_module_t
{
public:
    dvb_epg_module_t(epg_host_control_t* host_control) :
      host_control_(host_control)
    {}

    virtual ~dvb_epg_module_t(){}

    virtual bool start_scan(const concise_param_map_t& scan_params){return false;}
    virtual void stop_scan(){}
    virtual epg_module_status_e get_scan_status(){return ems_unknown;}
    virtual const dvb_channel_epg_desc_list_t* get_epg(){return NULL;}
    virtual void write_stream(const unsigned char* buffer, size_t length){}
protected:
    epg_host_control_t* host_control_;
};

}
