/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>

#include <boost/thread/thread.hpp>

#include "../epg_scanner.h"
#include "dvb_epg_module.h"
#include "tvs_stream_src.h"

namespace dvblink {
	class tuner_t;
}

namespace dvblex { 

class dvb_epg_module_t;

class dvb_epg_scanner_t : public epg_scanner_t, epg_host_control_t
{
public:
    dvb_epg_scanner_t(dvblink::tuner_t* tuner, const concise_param_map_t& params);
	virtual ~dvb_epg_scanner_t();

protected:
    dvblink::tuner_t* tuner_;
    CTVSStreamSource stream_src_;
    dvblink::DL_E_TUNER_TYPES tuner_type_;
    dvb_epg_module_t* epg_scanner_module_;
    boost::mutex stream_lock_;
    concise_param_map_t params_;

    std::map<std::string, dvblink::channel_id_t> dvb_id_to_channel_map_;

    virtual void add_pid(unsigned short pid);
    static void __stdcall stream_callback(const unsigned char* Buf, unsigned long Len, void* user_param);

    virtual bool start();
    virtual void stop();
    virtual bool do_scan(const concise_channel_tune_info_t& tuning_params);
    virtual void get_scan_transponders(const dvblink::epg_channel_tune_info_list_t& channels, channel_tuning_params_list_t& transponders);
};

}
