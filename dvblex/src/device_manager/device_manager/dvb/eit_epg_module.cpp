/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <stdexcept>
#include <dl_utils.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_ts_info.h>
#include <boost/lexical_cast.hpp>
#include "dvb_tvs_helpers.h"
#include "dvb_parameters.h"
#include "eit_epg_module.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

namespace dvblex { 

eit_epg_module_t::eit_epg_module_t(epg_host_control_t* host_control)
    : dvb_epg_module_t(host_control), m_ScanThread(NULL), m_Status(ems_unknown)
{
    nosdt_timeout_ = 10000; //10 seconds

    set_config_defaults();
}

eit_epg_module_t::~eit_epg_module_t()
{
    stop_scan();

    m_eit_parser.Reset();
    reset_services_map(&m_CurServiceListMap);
    reset_services_map(&m_OtherServiceListMap);
}

void eit_epg_module_t::set_config_defaults()
{
	//defaults
	m_EITScanTimeout = 120000; //1.5 minutes
	m_IgnoreShortDesc = false; //do not ignore by default
	m_EitPid = PID_EIT; //default DVB value
}


bool eit_epg_module_t::start_scan(const concise_param_map_t& scan_params)
{
    m_Language = L"";
    use_event_id_ = dvb_use_inband_epg_id_default;
    use_epg_only_from_current_tr_ = dvb_use_epg_only_from_current_tr_default;

    //inband epg event id
    parameter_value_t val = get_value_for_param(dvb_use_inband_epg_id_key, scan_params);
    if (!val.empty())
        use_event_id_ = boost::iequals(val.to_string(), PARAM_VALUE_YES);

    //use own transponder for epg
    val = get_value_for_param(dvb_use_own_epg_tr_key, scan_params);
    if (!val.empty())
        use_epg_only_from_current_tr_ = boost::iequals(val.to_string(), PARAM_VALUE_YES);

    log_info(L"eit_epg_module_t::start_scan. Parameters: %1%, %2%") % use_event_id_ % use_epg_only_from_current_tr_;

    if (is_key_present(epg_lang_params_key, scan_params))
    {
        dvblink::parameter_value_t param = get_value_for_param(epg_lang_params_key, scan_params);
        m_Language = string_cast<EC_UTF8>(param.get());
    }

    epg_data_.clear();
    m_Status = ems_in_progress;

    //stop previous scan thread (if it is still there)
    stop_scan_thread();
    set_scan_state_to_idle();

    //Start scanning thread
    m_ScanExit = false;
    m_ScanThread = new boost::thread(boost::bind(&eit_epg_module_t::ScanThread, this));

    return true;
}

void eit_epg_module_t::stop_scan()
{
    stop_scan_thread();
}

void eit_epg_module_t::set_scan_state_to_idle()
{
    boost::unique_lock<boost::mutex> lock(m_eitparser_cs);
    scan_state_ = esse_none;
}

void eit_epg_module_t::set_scan_state_to_eit_sdt()
{
    boost::unique_lock<boost::mutex> lock(m_eitparser_cs);
    scan_state_ = esse_eit_sdt;
}

void eit_epg_module_t::stop_scan_thread()
{
    //Exit scan thread
    if (m_ScanThread != NULL)
    {
        m_ScanExit = true;
	    m_ScanThread->join();
        delete m_ScanThread;
        m_ScanThread = NULL;
    }
}

void eit_epg_module_t::write_stream(const unsigned char* buffer, size_t length)
{
    try
    {
        boost::unique_lock<boost::mutex> lock(m_eitparser_cs, boost::try_to_lock);
        if (lock.owns_lock())
        {
            int c = length / 188;
            
            for (int i=0; i < c; i++)
            {
                unsigned char* packet = (unsigned char*)buffer + 188*i;
                
                switch (scan_state_)
                {
                case esse_eit_sdt:
                    {
                        if (ts_process_routines::GetPacketPID(packet) == m_EitPid)
                        {
		                    ts_payload_parser::ts_section_list found_sections;
                            
                            if (m_payload_parser.AddPacket(packet, 188, found_sections))
                            {
			                    for (unsigned int ii=0; ii<found_sections.size() && !m_bIsCompleted; ii++)
                                {
                                    EMBTS_EPGPARSER_ADDSECTION_RES res =
                                        m_eit_parser.AddSection(found_sections[ii].section,
                                            found_sections[ii].length);

                                    if (res != EMBTS_EPAR_DISCARDED)
                                    {
                                        //signal that we have received at least one meaningful EIT section
                                        if (!m_bEITReceived)
                                            m_bEITReceived = true;
                                    }
                                    //check whether scanning is completed
                                    if (m_bEITReceived)
                                        m_bIsCompleted = m_eit_parser.IsCompleted();
                                }

			                    m_payload_parser.ResetFoundSections(found_sections);
                            }
                        }
                        
                        if (ts_process_routines::GetPacketPID(packet) == PID_SDT)
                        {
				            //process SDT to find the channel names
				            ts_payload_parser::ts_section_list found_sections;
    					    
                            if (m_SDT_payload_parser.AddPacket(packet, 188, found_sections))
				            {
					            for (unsigned int ii=0; ii<found_sections.size() && !m_bIsSDTCompleted; ii++)
						            ProcessSDTSection(found_sections[ii].section, found_sections[ii].length);

					            m_SDT_payload_parser.ResetFoundSections(found_sections);
				            }
			            }
                    }
                    break;
                default:
                    break;
                }
            }
        }
    }
    catch (std::exception& x)
    {
        log_error(L"EXCEPTION in eit_epg_module_t::WriteStream(): %1%") % x.what();
    }
    catch (...)
    {
        log_error(L"EXCEPTION in eit_epg_module_t::WriteStream()");
    }
}

void eit_epg_module_t::switch_to_eit_sdt_scan()
{
    //ask for EIT
    host_control_->add_pid(m_EitPid);
    //ask for SDT
    host_control_->add_pid(PID_SDT);

    //EIT parser
    m_bEITReceived = false;
    m_bIsCompleted = false;
    m_eit_parser.Reset();
    m_payload_parser.Init(m_EitPid);

    //SDT parser
    m_bIsSDTCompleted = false;
    reset_services_map(&m_CurServiceListMap);
    reset_services_map(&m_OtherServiceListMap);
    m_SDT_payload_parser.Init(PID_SDT);

    set_scan_state_to_eit_sdt();
}

void eit_epg_module_t::ScanThread()
{
    set_scan_state_to_idle();

    try
    {
        scan_start_time_ = boost::posix_time::microsec_clock::universal_time();
        switch_to_eit_sdt_scan();

        //Wait until EPG data collection finishes
        unsigned long sleep_value = 500;
        unsigned long tick_count = 0;
        unsigned long noeit_timeout = 6000;
        unsigned long scan_timeout = m_EITScanTimeout;
        
        while (true)
        {
            boost::this_thread::sleep(boost::posix_time::milliseconds(sleep_value));

            tick_count += sleep_value;
            
            //check if completed
            if (m_bIsCompleted && m_bIsSDTCompleted)
            {
                log_info(L"eit_epg_module_t::ScanThread. EPG data collection is finished");
                //exit wait
                break;
            }

            //check for timeouts
            if ((tick_count >= noeit_timeout && !m_bEITReceived) || tick_count >= scan_timeout || m_ScanExit)
            {
                if (!m_ScanExit)
                    log_info(L"eit_epg_module_t::ScanThread. EPG data collection is timed out");

                //exit wait
                break;
            }
        }

        //set scan state to idle
        set_scan_state_to_idle();

        if (!m_ScanExit && m_bEITReceived)
        {
		    std::vector<TVSC_ChannelType> scanned_services_cur;
		    std::vector<TVSC_ChannelType> scanned_services_other;
		    GetScannedServices(&m_CurServiceListMap, scanned_services_cur);
		    GetScannedServices(&m_OtherServiceListMap, scanned_services_other);

            process_seen_services(m_eit_parser.GetCurrentServices());

            if (!use_epg_only_from_current_tr_)
                process_seen_services(m_eit_parser.GetOtherServices());
            else
                log_info(L"eit_epg_module_t::ScanThread. EIT info from <other> transponders is ignored");
        }

        //reset all previously created parsers
        m_eit_parser.Reset();
        reset_services_map(&m_CurServiceListMap);
        reset_services_map(&m_OtherServiceListMap);

        if (m_ScanExit)
        {
            m_Status = ems_finished_aborted;
        }
        else
        {
            if (epg_data_.size() > 0)
                m_Status = ems_finished_success;
            else
                m_Status = ems_finished_error;
        }
    }
    catch (std::exception& x)
    {
        log_error(L"EXCEPTION in eit_epg_module_t::ScanThread(): %1%") % x.what();
    }
    catch (...)
    {
        log_error(L"EXCEPTION in eit_epg_module_t::ScanThread()");
    }
}

void eit_epg_module_t::process_seen_services(CDLTSEPGServiceList* seen_services)
{
    try
    {
        TMBTSEPGSeenServicesList* service_list = seen_services->GetSeenServices();
        
        for (unsigned int i=0; i<service_list->size() && !m_ScanExit; i++)
        {
            dvb_channel_epg_desc_t epg_desc;
            epg_desc.nid_ = service_list->at(i)->GetStreamId()->m_NID;
            epg_desc.tid_ = service_list->at(i)->GetStreamId()->m_TID;
            epg_desc.sid_ = service_list->at(i)->GetStreamId()->m_SID;

            service_list->at(i)->GetEPGEvents(m_Language.c_str(),
                m_IgnoreShortDesc, use_event_id_, epg_desc.events_, m_ScanExit);
            
            epg_data_.push_back(epg_desc);
        }
    }
    catch (std::exception& x)
    {
        log_error(L"EXCEPTION in CEITXMLWriter::AddSeenServices(): %1%") % x.what();
    }
    catch (...)
    {
        log_error(L"EXCEPTION in CEITXMLWriter::AddSeenServices()");
    }
}

void eit_epg_module_t::ProcessSDTSection(unsigned char* section, int section_len)
{
    unsigned char cur_next;
    unsigned char table_id;
    WORD tid, nid;

    table_id = ts_process_routines::GetSDTTableId(section, section_len);
    cur_next = ts_process_routines::GetSDTCurNextIndicator(section, section_len);
    ts_process_routines::GetSDTSectionIds(section, section_len, nid, tid);

    //check table ID and current-next flag
    TTVSServiceListMap* service_map = NULL;
    switch (table_id)
    {
	    case 0x42:
		    service_map = &m_CurServiceListMap;
		    break;
	    case 0x46:
		    service_map = &m_OtherServiceListMap;
		    break;
    }
    if (service_map != NULL && cur_next != 0)
    {
	    DWORD key = (((DWORD)nid) << 16) | tid;
	    if (service_map->find(key) == service_map->end())
		    (*service_map)[key] = new CTVSServiceDVBListDescriptor();
	    //add section to already existing one
	    (*service_map)[key]->AddSection(section, section_len);

        //check timeout
        boost::posix_time::ptime cur_time = boost::posix_time::microsec_clock::universal_time();
        boost::posix_time::time_duration duration(cur_time - scan_start_time_);

        m_bIsSDTCompleted = duration.total_milliseconds() > nosdt_timeout_;
    }
}

bool eit_epg_module_t::IsServiceMapCompleted(TTVSServiceListMap* service_map)
{
	bool bCompleted = true;
	TTVSServiceListMap::iterator it = service_map->begin();
	
    while (it != service_map->end())
	{
		if (!it->second->IsCompleted())
		{
			bCompleted = false;
			break;
		}
		it++;
	}
	
    return bCompleted;
}

void eit_epg_module_t::GetScannedServices(TTVSServiceListMap* service_map,
    std::vector<TVSC_ChannelType>& services)
{
    TTVSServiceListMap::iterator it = service_map->begin();

    while (it != service_map->end())
    {
	    std::vector<TVSC_ChannelType> scanned_services;
	    it->second->GetServices(scanned_services);
	    services.insert(services.begin(), scanned_services.begin(),
            scanned_services.end());
	    it++;
    }
}

void eit_epg_module_t::reset_services_map(TTVSServiceListMap* service_map)
{
	TTVSServiceListMap::iterator it = service_map->begin();
	
    while (it != service_map->end())
	{
        delete it->second;
		it++;
	}

    service_map->clear();
}

}
