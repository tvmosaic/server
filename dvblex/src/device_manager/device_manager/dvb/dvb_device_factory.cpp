/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/


#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_language_settings.h>
#include <dl_locale_strings.h>
#include <drivers/sat2ip.h>
#include <drivers/hdhr.h>
#include <drivers/drv_file.h>
#ifdef WIN32
    #include <drivers/bda.h>
#else
    #include <drivers/v4l.h>
#endif
#include "dvb_device_factory.h"
#include "dvb_device.h"
#include "dvb_parameters.h"
#include "dvb_strings.h"
#include "tuner_ex_properties.h"
#include "dvb_tvs_helpers.h"

using namespace dvblex;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink;

#if defined WIN32
    static const char* factory_names[] = {sat2ip_tuner_factory_t::get_name(), hdhr_tuner_factory_t::get_name(), bda_tuner_factory_t::get_name(), file_tuner_factory_t::get_name()};
#elif defined __APPLE__
    static const char* factory_names[] = {sat2ip_tuner_factory_t::get_name(), hdhr_tuner_factory_t::get_name()};
#elif defined _ANDROID_ALL
    static const char* factory_names[] = {sat2ip_tuner_factory_t::get_name(), hdhr_tuner_factory_t::get_name(), file_tuner_factory_t::get_name()};
#else
    static const char* factory_names[] = {sat2ip_tuner_factory_t::get_name(), hdhr_tuner_factory_t::get_name(), v4l_tuner_factory_t::get_name()};
#endif

static const size_t factory_names_num = sizeof(factory_names) / sizeof(char*);

dvb_device_factory_t::dvb_device_factory_t(const dvblink::filesystem_path_t& device_config_path) :
    device_factory_t(device_config_path)
{
    for (size_t i=0; i<factory_names_num; i++)
    {
        dvblink::tuner_factory_t* tf = create_factory_from_name(factory_names[i]);
        if (tf != NULL)
            tuner_factory_map_[factory_names[i]] = tf;
    }
}

dvb_device_factory_t::~dvb_device_factory_t()
{
    tuner_factory_map_t::iterator it = tuner_factory_map_.begin();
    while (it != tuner_factory_map_.end())
    {
        delete it->second;
        ++it;
    }
}

static standard_set_t get_standards_from_tvs(unsigned long tvs_types)
{
    unsigned long ret_val = st_unknown;

    if (tvs_types == TUNERTYPE_UNKNOWN)
        ret_val = SOURCE_TYPE_DVB_ALL;
    if ((tvs_types & TUNERTYPE_DVBS) != 0)
        ret_val |= st_dvb_s_s2;
    if ((tvs_types & TUNERTYPE_DVBC) != 0)
        ret_val |= st_dvb_c;
    if ((tvs_types & TUNERTYPE_DVBT) != 0)
        ret_val |= st_dvb_t_t2;
    if ((tvs_types & TUNERTYPE_ATSC) != 0)
        ret_val |= st_atsc;
    if ((tvs_types & TUNERTYPE_CLEARQAM) != 0)
        ret_val |= st_cqam;

    return ret_val;
}

static device_connection_type_e get_connection_type_from_tvs(dvblink::DL_E_TUNER_CONNECTION_TYPE tct)
{
    device_connection_type_e ret_val = edct_unknown;

    switch (tct)
    {
    case TCT_NETWORK:
        ret_val = edct_network;
        break;
    case TCT_LOCAL:
        ret_val = edct_local;
        break;
    case TCT_USB:
        ret_val = edct_usb;
        break;
    case TCT_PCIE:
        ret_val = edct_pcie;
        break;
    case TCT_UNKNOWN:
    default:
        ret_val = edct_unknown;
        break;
    }

    return ret_val;
}

static void device_desc_from_tvs(const std::string& id, const TDevAPIDevInfoEx& td, device_descriptor_t& dd)
{
    dd.id_ = id;
    dd.name_ = td.szName;
    dd.supported_standards_ = get_standards_from_tvs(td.TunerType);
    dd.tuners_num_ = 1;
    dd.uri_ = td.uri;
    dd.make_ = td.make;
    dd.model_ = td.model;
    dd.model_num_ = td.modelNum;
    dd.uuid_ = strlen(td.uuid) == 0 ? id : td.uuid;
    dd.uuid_make_ = td.uuid_make;
    dd.connection_type_ = get_connection_type_from_tvs(td.connection_type);
    dd.vid_ = td.vid;
    dd.pid_ = td.pid;
    dd.has_settings_ = true;

    apply_tuner_ex_properties(td, dd);
}

bool dvb_device_factory_t::get_device_list(const dvblink::device_uuid_t& u, device_descriptor_list_t& device_list)
{
    log_info(L"dvb_device_factory_t::get_device_list (%1%)") % string_cast<EC_UTF8>(u.get());

    std::string factory_name;
    std::string device_uuid;
    if (!u.empty())
        path_to_device_uuid(u, factory_name, device_uuid);

    for (size_t i=0; i<factory_names_num; i++)
    {
        if (factory_name.empty() || boost::iequals(factory_name, factory_names[i]))
        {
            tuner_factory_t* tf = get_factory_from_name(factory_names[i]);
            if (tf != NULL)
            {
                TDevAPIDevListEx devices;
                if (tf->DeviceGetList(&devices) == dvblink::SUCCESS)
                {
                    for (int c=0; c<devices.Count; c++)
                    {
                        if (device_uuid.empty() || boost::iequals(devices.Devices[c].uuid, device_uuid))
                        {
                            internal_id_t internal_id;
                            internal_id.device_path_ = string_cast<EC_UTF8>(devices.Devices[c].szDevicePathW);
                            internal_id.frontend_idx_ = devices.Devices[c].FrontendIdx;
                            internal_id.tuner_factory_name_ = tf->get_factory_name();

                            device_descriptor_t dd;
                            device_desc_from_tvs(internal_id_to_path(internal_id), devices.Devices[c], dd);
                            dd.uuid_ = device_uuid_to_path(internal_id.tuner_factory_name_, devices.Devices[c].uuid);

                            device_list.push_back(dd);

                            log_info(L"dvb_device_factory_t::get_device_list. Id %1%, Name %2%, Standards %3%") % string_cast<EC_UTF8>(dd.id_.get()) % string_cast<EC_UTF8>(dd.name_.get()) % dd.supported_standards_;
                        }
                    }
                }
            }
        }
    }

    return true;
}

bool dvb_device_factory_t::get_device_info(const dvblink::device_id_t& device_id, device_descriptor_t& device_info)
{
    bool ret_val = false;

    internal_id_t internal_id;
    path_to_internal_id(device_id.get(), internal_id);

    tuner_factory_t* tf = get_factory_from_name(internal_id.tuner_factory_name_);
    if (tf != NULL)
    {
        TDevAPIDevInfoEx tuner_info;
        ret_val = tf->get_tuner_info(internal_id.device_path_, internal_id.frontend_idx_, tuner_info);

        device_desc_from_tvs(device_id.to_string(), tuner_info, device_info);
        device_info.uuid_ = device_uuid_to_path(internal_id.tuner_factory_name_, tuner_info.uuid);
    }
    return ret_val;
}

device_t* dvb_device_factory_t::create_device(const dvblink::device_id_t& device_id, directory_settings_obj_t& dir_settings)
{
    device_t* ret_val = NULL;

    internal_id_t internal_id;
    path_to_internal_id(device_id.get(), internal_id);

    tuner_factory_t* tf = get_factory_from_name(internal_id.tuner_factory_name_);
    if (tf != NULL)
    {
        dvblink::tuner_t* tuner = tf->get_tuner(internal_id.device_path_, internal_id.frontend_idx_);
        if (tuner != NULL)
        {
            //device properties and external ci
            std::wstring ext_ci_id = external_ci_device_id_none;
            concise_param_map_t device_params;

            {
                boost::unique_lock<boost::mutex> l(manual_devices_lock_);
                if (device_concise_param_map_.find(device_id) != device_concise_param_map_.end())
                {
                    device_params = device_concise_param_map_[device_id];

                    parameter_value_t val = get_value_for_param(dvb_ext_ci_device_key, device_params);
                    if (!val.empty())
                        ext_ci_id = val.to_wstring();
                }
            }
            tuner->SetExtCIDevice(ext_ci_id);

            ret_val = new dvb_device_t(device_id, tuner, dir_settings, device_params);
        } else
        {
            log_error(L"dvb_device_factory_t::create_device. Cannot create tuner for device id %1%") % string_cast<EC_UTF8>(device_id.get());
        }
    } else
    {
        log_error(L"dvb_device_factory_t::create_device. Cannot create tuner factory for device id %1%") % string_cast<EC_UTF8>(device_id.get());
    }

    return ret_val;
}

dvblink::tuner_factory_t* dvb_device_factory_t::create_factory_from_name(const std::string& name)
{
    tuner_factory_t* ret_val = NULL;

    const dvblink::filesystem_path_t config_path = device_config_path_ / string_cast<EC_UTF8>(name);

#if defined(_ANDROID_ALL)
    if (boost::iequals(name, hdhr_tuner_factory_t::get_name()))
    {
        ret_val = new hdhr_tuner_factory_t(config_path);
    }
    if (boost::iequals(name, sat2ip_tuner_factory_t::get_name()))
    {
        ret_val = new sat2ip_tuner_factory_t(config_path);
    }
    if (boost::iequals(name, file_tuner_factory_t::get_name()))
    {
        ret_val = new file_tuner_factory_t(config_path);
    }
#else
    if (boost::iequals(name, sat2ip_tuner_factory_t::get_name()))
    {
        ret_val = new sat2ip_tuner_factory_t(config_path);
    }
    if (boost::iequals(name, hdhr_tuner_factory_t::get_name()))
    {
        ret_val = new hdhr_tuner_factory_t(config_path);
    }
#ifdef WIN32
    if (boost::iequals(name, file_tuner_factory_t::get_name()))
    {
        ret_val = new file_tuner_factory_t(config_path);
    }
    if (boost::iequals(name, bda_tuner_factory_t::get_name()))
    {
        ret_val = new bda_tuner_factory_t(config_path);
    }
#else
#ifndef __APPLE__
    if (boost::iequals(name, v4l_tuner_factory_t::get_name()))
    {
        ret_val = new v4l_tuner_factory_t(config_path);
    }
#endif
#endif
#endif
    return ret_val;
}

tuner_factory_t* dvb_device_factory_t::get_factory_from_name(const std::string& name)
{
    tuner_factory_t* tf = NULL;

    if (tuner_factory_map_.find(name) != tuner_factory_map_.end())
        tf = tuner_factory_map_[name];

    return tf;
}

void dvb_device_factory_t::path_to_device_uuid(const dvblink::device_uuid_t& p, std::string& factory, std::string& device_uuid)
{
    //device factory
    std::string df = get_device_factory_from_id(p.get());
    if (!df.empty())
    {
        std::string path = p.get().substr(df.size() + 1);
        //factory name and uuid
        size_t n = path.find(get_id_sep_symbol());
        if (n != std::string::npos && n > 0)
        {
            factory = path.substr(0, n);
            device_uuid = path.substr(n+1);
        }
    }
}

dvblink::device_uuid_t dvb_device_factory_t::device_uuid_to_path(const std::string& factory, const std::string& device_uuid)
{
    std::stringstream strbuf;
    strbuf << get_device_factory_name() << get_id_sep_symbol() << factory << get_id_sep_symbol() << device_uuid;

    return strbuf.str();
}

void dvb_device_factory_t::path_to_internal_id(const std::string& id, internal_id_t& internal_id)
{
    //device factory
    std::string df = get_device_factory_from_id(id);
    if (!df.empty() && id.size() > df.size())
    {
        std::string path = id.substr(df.size() + 1);
        //factory name
        size_t n = path.find(get_id_sep_symbol());
        if (n != std::string::npos && n > 0)
        {
            internal_id.tuner_factory_name_ = path.substr(0, n);

            //frontend idx
            size_t m = path.find(get_id_sep_symbol(), n+1);
            if (n != std::string::npos)
            {
                std::string str = path.substr(n+1, m - n - 1);
                int i;
                string_conv::apply(str.c_str(), i, 0);
                internal_id.frontend_idx_ = i;

                //the rest is internal device path
                internal_id.device_path_ = path.substr(m+1);
            }
        }
    }
}

std::string dvb_device_factory_t::internal_id_to_path(const internal_id_t& internal_id)
{
    std::stringstream strbuf;
    strbuf << get_device_factory_name() << get_id_sep_symbol() << internal_id.tuner_factory_name_ << get_id_sep_symbol() << internal_id.frontend_idx_ << get_id_sep_symbol() << internal_id.device_path_;

    return strbuf.str();
}

void dvb_device_factory_t::get_aux_list(aux_module_list_t& aux_list)
{
    log_info(L"dvb_device_factory_t::get_aux_list");

    for (size_t i=0; i<factory_names_num; i++)
    {
        tuner_factory_t* tf = get_factory_from_name(factory_names[i]);
        if (tf != NULL)
        {
            aux_module_list_t al;
            tf->get_aux_list(al);
            aux_list.insert(aux_list.end(), al.begin(), al.end());
        }
    }
}

source_type_e dvb_device_factory_t::get_source_type_from_tuning_params(const dvblink::channel_tuning_params_t& tune_params)
{
    return dvblex_tuner_type_from_tvs(get_tuner_type_from_tuning_string(tune_params.get()));
}

bool dvb_device_factory_t::get_device_configurable_props(const dvblink::device_id_t& device_id, configurable_device_props_t& props)
{
    //defaults
    props = configurable_device_props_t();

    std::string ext_ci_id = string_cast<EC_UTF8>(external_ci_device_id_none);

    std::string use_inband_event_id_cur = get_yes_no_param_from_bool(dvb_use_inband_epg_id_default);
    std::string use_own_epg_tr_cur = get_yes_no_param_from_bool(dvb_use_epg_only_from_current_tr_default);

    {
        boost::unique_lock<boost::mutex> l(manual_devices_lock_);
        if (device_concise_param_map_.find(device_id) != device_concise_param_map_.end())
        {
            concise_param_map_t& cpm = device_concise_param_map_[device_id];
            //fta concurrency
            parameter_value_t val = get_value_for_param(dvb_concurrent_fta_num_key, cpm);
            if (!val.empty())
                string_conv::apply(val.c_str(), props.fta_concurrent_streams_, props.fta_concurrent_streams_);

            //enc concurrency
            val = get_value_for_param(dvb_concurrent_enc_num_key, cpm);
            if (!val.empty())
                string_conv::apply(val.c_str(), props.enc_concurrent_streams_, props.enc_concurrent_streams_);

            //ext ci
            val = get_value_for_param(dvb_ext_ci_device_key, cpm);
            if (!val.empty())
                ext_ci_id = val.to_string();

            //inband epg event id
            val = get_value_for_param(dvb_use_inband_epg_id_key, cpm);
            if (!val.empty())
                use_inband_event_id_cur = val.to_string();            

            //use own transponder for epg
            val = get_value_for_param(dvb_use_own_epg_tr_key, cpm);
            if (!val.empty())
                use_own_epg_tr_cur = val.to_string();            
        }
    }

    props.param_container_.id_ = dvb_configurable_params_container_id;
    props.param_container_.name_ = language_settings::GetInstance()->GetItemName(device_params_container_name);
    
    selectable_param_description_t fta_concurrency_element;
    fta_concurrency_element.key_ = dvb_concurrent_fta_num_key;
    fta_concurrency_element.name_ = language_settings::GetInstance()->GetItemName(fta_concurrency_num_name);

    fta_concurrency_element.parameters_list_.push_back(selectable_param_element_t("-1", language_settings::GetInstance()->GetItemName(concurrency_num_no_limit_name)));
    for (int i=1; i<=5; i++)
    {
        std::stringstream strbuf;
        strbuf << i;
        fta_concurrency_element.parameters_list_.push_back(selectable_param_element_t(strbuf.str(), strbuf.str()));
        if (i == props.fta_concurrent_streams_)
            fta_concurrency_element.selected_param_ = fta_concurrency_element.parameters_list_.size() - 1;
    }

    props.param_container_.selectable_params_.push_back(fta_concurrency_element);

    selectable_param_description_t enc_concurrency_element;
    enc_concurrency_element.key_ = dvb_concurrent_enc_num_key;
    enc_concurrency_element.name_ = language_settings::GetInstance()->GetItemName(enc_concurrency_num_name);

    enc_concurrency_element.parameters_list_.push_back(selectable_param_element_t("-1", language_settings::GetInstance()->GetItemName(concurrency_num_no_limit_name)));
    for (int i=1; i<=5; i++)
    {
        std::stringstream strbuf;
        strbuf << i;
        enc_concurrency_element.parameters_list_.push_back(selectable_param_element_t(strbuf.str(), strbuf.str()));
        if (i == props.enc_concurrent_streams_)
            enc_concurrency_element.selected_param_ = enc_concurrency_element.parameters_list_.size() - 1;
    }

    props.param_container_.selectable_params_.push_back(enc_concurrency_element);

    //external ci devices
    selectable_param_description_t extci_element;
    extci_element.key_ = dvb_ext_ci_device_key;
    extci_element.name_ = language_settings::GetInstance()->GetItemName(ext_ci_device_choice_name);

    //None
    extci_element.parameters_list_.push_back(selectable_param_element_t(string_cast<EC_UTF8>(external_ci_device_id_none), 
        language_settings::GetInstance()->GetItemName(ext_ci_device_none_name)));
    extci_element.selected_param_ = 0;

    //get list of ext ci devices
    internal_id_t internal_id;
    path_to_internal_id(device_id.get(), internal_id);

    tuner_factory_t* tf = get_factory_from_name(internal_id.tuner_factory_name_);
    if (tf != NULL)
    {
        TExtCIDeviceList ext_ci_devices;
        if (tf->ExtCIDeviceGetList(internal_id.device_path_, &ext_ci_devices) == dvblink::SUCCESS)
        {
            for (int i=0; i<ext_ci_devices.Count; i++)
            {
                std::string hwid = string_cast<EC_UTF8>(ext_ci_devices.Devices[i].hwid);
                extci_element.parameters_list_.push_back(selectable_param_element_t(hwid, string_cast<EC_UTF8>(ext_ci_devices.Devices[i].name)));
                if (boost::iequals(hwid, ext_ci_id))
                    extci_element.selected_param_ = extci_element.parameters_list_.size() - 1;
            }
        }

        props.param_container_.selectable_params_.push_back(extci_element);

        //use inband epg event ids
        selectable_param_description_t inband_event_id_param;
        inband_event_id_param.key_ = dvb_use_inband_epg_id_key;
        inband_event_id_param.name_ = language_settings::GetInstance()->GetItemName(use_inband_epg_id_choice_name);
        inband_event_id_param.selected_param_ = boost::iequals(use_inband_event_id_cur, PARAM_VALUE_YES) ? 0 : 1;

        selectable_param_element_t inband_event_id_element;

        inband_event_id_element.value_ = PARAM_VALUE_YES;
        inband_event_id_element.name_ = language_settings::GetInstance()->GetItemName(parameter_value_yes);
        inband_event_id_param.parameters_list_.push_back(inband_event_id_element);

        inband_event_id_element.value_ = PARAM_VALUE_NO;
        inband_event_id_element.name_ = language_settings::GetInstance()->GetItemName(parameter_value_no);
        inband_event_id_param.parameters_list_.push_back(inband_event_id_element);

        props.param_container_.selectable_params_.push_back(inband_event_id_param);

        //use own transponder for epg events
        selectable_param_description_t own_tr_for_epg_param;
        own_tr_for_epg_param.key_ = dvb_use_own_epg_tr_key;
        own_tr_for_epg_param.name_ = language_settings::GetInstance()->GetItemName(own_tr_for_epg_choice_name);
        own_tr_for_epg_param.selected_param_ = boost::iequals(use_own_epg_tr_cur, PARAM_VALUE_YES) ? 0 : 1;

        selectable_param_element_t own_tr_for_epg_element;

        own_tr_for_epg_element.value_ = PARAM_VALUE_YES;
        own_tr_for_epg_element.name_ = language_settings::GetInstance()->GetItemName(parameter_value_yes);
        own_tr_for_epg_param.parameters_list_.push_back(own_tr_for_epg_element);

        own_tr_for_epg_element.value_ = PARAM_VALUE_NO;
        own_tr_for_epg_element.name_ = language_settings::GetInstance()->GetItemName(parameter_value_no);
        own_tr_for_epg_param.parameters_list_.push_back(own_tr_for_epg_element);

        props.param_container_.selectable_params_.push_back(own_tr_for_epg_param);

        return true;
    }
    return false;
}

