/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <algorithm>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <dl_logger.h>
#include <dl_utils.h>
#include <dl_ts_info.h>
#include <dl_file_procedures.h>
#include <dl_filesystem_path.h>
#include <dl_strings.h>

#define JSON_SPIRIT_MVALUE_ENABLED
#include <json_spirit.h>

#include "dvbjapi_plugin_manager.h"

using namespace dvblink::engine;
using namespace dvblink::logging;

#ifdef WIN32
    #define plugin_file_mask L".dll"
#elif __APPLE__
    #define plugin_file_mask L".dylib"
#else
    #define plugin_file_mask L".so"
#endif

dvbjapi_plugin_manager::dvbjapi_plugin_manager(const dvblink::filesystem_path_t& source_path, const std::wstring& plugin_dir) :
	tvs_plugin_manager_base(),
	source_path_(source_path),
	plugin_dir_(plugin_dir),
    command_thread_(NULL)
{
}

static bool check_dvbjapi_plugin(const std::wstring& fname)
{
    bool ret_val = false;

	log_ext_info(L"check_dvbjapi_plugin: %1%") % fname;

    dvblink::engine::dyn_lib plugin_file(fname);
    if (plugin_file.Load())
    {
        if (plugin_file.GetFuncAddr(DVBJAPI_RCV_FROM_PARENT_FN_NAME) != NULL)
            ret_val = true;
        plugin_file.Unload();
    } else
    {
    	std::wstring err;
    	plugin_file.GetError(err);
    	log_ext_info(L"plugin load error: %1%, %2") % fname % err;
    }

    return ret_val;
}

bool dvbjapi_plugin_manager::is_valid_dvbjapi_plugin_dir(const dvblink::filesystem_path_t& parent_path, const std::wstring& dir_name)
{
	bool ret_val = false;
	if (boost::istarts_with(dir_name, L"plugins"))
	{
		//check if the directory has at least one .dll file
		dvblink::filesystem_path_t path = parent_path;
		path /= dir_name;

		std::vector<boost::filesystem::path> plugin_files;
        dvblink::engine::filesystem::find_files(path.get(), plugin_files, plugin_file_mask);

	    for (size_t i = 0; i < plugin_files.size() && !ret_val; i++)
	    {
            if (check_dvbjapi_plugin(plugin_files[i].wstring()))
            {
                ret_val = true;
                break;
            }
        }

		if (ret_val)
			log_info(L"Found valid dvbjapi plugin directory %1%") % path.get();
		else
			log_info(L"Directory %1% does not have any dvbjapi plugins inside") % path.get();
	}
	return ret_val;
}

bool dvbjapi_plugin_manager::init()
{
    dvblink::filesystem_path_t plugins_folder = source_path_ / plugin_dir_;

	log_info(L"dvbjapi_plugin_manager::init. Plugin dir %1%") % plugins_folder;

	tvs_plugin_manager_base::init();

    //enumerate all plugins in this directory
	std::vector<boost::filesystem::path> plugin_files;
    dvblink::engine::filesystem::find_files(plugins_folder.to_wstring(), plugin_files, plugin_file_mask);

	for (size_t i = 0; i < plugin_files.size(); i++)
	{
        if (check_dvbjapi_plugin(plugin_files[i].wstring()))
        {
            //only start sommand processing thread if there are plugins present
            exit_command_thread_ = false;
            command_thread_ = new boost::thread(boost::bind(&dvbjapi_plugin_manager::command_thread_func, this));

            dvbjapi_plugin_desc_ptr_t plugin_desc = boost::shared_ptr<dvbjapi_plugin_desc>(new dvbjapi_plugin_desc());
            plugin_desc->plugin_man = this;
            plugin_desc->id = plugin_files[i].filename().wstring();
            dvblink::filesystem_path_t plugin_path(plugin_files[i]);
            plugin_desc->plugin = boost::shared_ptr<dvbjapi_plugin>(new dvbjapi_plugin(plugin_path));

            dvbjapi_plugin_map_[plugin_desc->id] = plugin_desc;
            if (plugin_desc->plugin->init(dvbjapi_plugin_manager::dvbjapi_plugin_callback, (void*)plugin_desc.get()))
            {
	            log_info(L"dvbjapi_plugin_manager::init. Successfully initialized plugin %1%") % plugin_desc->id;
            } else
            {
                log_error(L"dvbjapi_plugin_manager::init. Error loading plugin %1%") % plugin_desc->id;
                dvbjapi_plugin_map_.erase(plugin_desc->id);
                plugin_desc.reset();
            }

            break;
        }
    }

	return true;
}

void dvbjapi_plugin_manager::term()
{
    dvbjapi_plugin_map_t::iterator it = dvbjapi_plugin_map_.begin();
    while (it != dvbjapi_plugin_map_.end())
    {
        log_info(L"dvbjapi_plugin_manager::term. Unloading plugin %1%") % it->second->id;

        it->second->plugin->term();
        ++it;
    }

    dvbjapi_plugin_map_.clear();

    if (command_thread_ != NULL)
    {
        exit_command_thread_ = true;
        command_thread_->join();
        delete command_thread_;
        command_thread_ = NULL;
    }

	tvs_plugin_manager_base::term();
}

bool dvbjapi_plugin_manager::channel_changed(TProgramm* tuner_info, plugin_program_extra_info& extra_info)
{
    boost::mutex::scoped_lock lock(lock_);

	tvs_plugin_manager_base::channel_changed(tuner_info, extra_info);

    change_channel_params params;
    params.nid = extra_info.nid;
    params.channel_name = tuner_info->Name;
    params.tid= tuner_info->Link_TP;
    params.sid = tuner_info->Link_SID;
    params.pmt_pid = tuner_info->PMT_pid;
    params.extra_info = extra_info;

    command_queue_.ExecuteCommand(EC_CHANGE_CHANNEL, &params);

	return true;
}

void dvbjapi_plugin_manager::process_requested_packet(const unsigned char* buf, size_t len)
{
    dvbjapi_plugin_map_t::iterator it = dvbjapi_plugin_map_.begin();
    while (it != dvbjapi_plugin_map_.end())
    {
        it->second->plugin->write_stream(buf, len);
        ++it;
    }
}

int dvbjapi_plugin_manager::dvbjapi_plugin_callback(char *value, void* user_param)
{
    dvbjapi_plugin_desc* desc = (dvbjapi_plugin_desc*)user_param;

    plugin_cmd_params* params = new plugin_cmd_params();
    params->plugin_id = desc->id;
    params->cmd = value;

    desc->plugin_man->command_queue_.PostCommand(EC_PLUGIN_CMD, params);

    return 0;
}

void dvbjapi_plugin_manager::command_thread_func()
{

    while (!exit_command_thread_)
    {
        //check command queue first
        SDLCommandItem* item;
        while (command_queue_.PeekCommand(&item))
        {
            switch (item->id)
            {
            case EC_CHANGE_CHANNEL:
                {
                    change_channel_params* ch_change_params = (change_channel_params*)item->param;
                    process_channel_change(ch_change_params);
                }
                break;
            case EC_PLUGIN_CMD:
                {
                    plugin_cmd_params* cmd_params = (plugin_cmd_params*)item->param;
                    process_plugin_cmd(cmd_params);
                }
                break;
            default:
                break;
            }
            command_queue_.FinishCommand(&item);
        }
    	boost::this_thread::sleep(boost::posix_time::milliseconds(10));
    }
}

static std::string us_to_hex_str(unsigned short us)
{
    char char_buf[16];
    sprintf(char_buf, "%04X", us);
    return std::string(char_buf);
}

static std::string vector_to_hex_string(psi_binary_buffer_t& v)
{
    std::string result;
    for (size_t i=0; i<v.size(); i++)
    {
        char char_buf[16];
        sprintf(char_buf, "%02X", v[i]);
        result += char_buf;
    }
    return result;
}

void dvbjapi_plugin_manager::convert_to_json_cmd(change_channel_params* params, std::string& json_cmd)
{
    json_spirit::Array array_obj;

    json_spirit::Object obj;
    obj.push_back(json_spirit::Pair( "CMD", "CHANNEL_CHANGE"));
    obj.push_back(json_spirit::Pair( "NAME", params->channel_name));
    obj.push_back(json_spirit::Pair( "SID", us_to_hex_str(params->sid)));
    obj.push_back(json_spirit::Pair( "NIT", us_to_hex_str(params->nid)));
    obj.push_back(json_spirit::Pair( "TID", us_to_hex_str(params->tid)));
    obj.push_back(json_spirit::Pair( "PMT", us_to_hex_str(params->pmt_pid)));
    if (params->extra_info.pmt_section.size() > 0)
        obj.push_back(json_spirit::Pair( "PMT_DATA", vector_to_hex_string(params->extra_info.pmt_section)));
/*
    if (params->extra_info.cat_section.size() > 0)
        obj.push_back(json_spirit::Pair( "CAT_DATA", vector_to_hex_string(params->extra_info.cat_section)));
*/
    array_obj.push_back(obj);

    json_cmd = json_spirit::write(array_obj, 0);
}

void dvbjapi_plugin_manager::process_channel_change(change_channel_params* params)
{
    current_channel_ = *params;

    std::string json_cmd;
    convert_to_json_cmd(params, json_cmd);

    std::wstring plugin_id; //empty: send to all
    send_cmd_to_plugin(json_cmd, plugin_id);
}

void dvbjapi_plugin_manager::send_cmd_to_plugin(const std::string& json_cmd, const std::wstring& plugin_id)
{
    dvbjapi_plugin_map_t::iterator it = dvbjapi_plugin_map_.begin();
    while (it != dvbjapi_plugin_map_.end())
    {
        if (plugin_id.size() == 0 || (boost::iequals(plugin_id, it->first)) )
            it->second->plugin->execute_command(json_cmd);

        ++it;
    }
}

void dvbjapi_plugin_manager::process_plugin_cmd(plugin_cmd_params* params)
{
    try 
    {
        json_spirit::mValue value;
        if (json_spirit::read(params->cmd, value))
        {
            json_spirit::mArray cmd_array_obj = value.get_array();

            for (size_t i=0; i<cmd_array_obj.size(); i++)
            {
                const json_spirit::mObject& obj = cmd_array_obj[i].get_obj();

                json_spirit::mObject::const_iterator iter = obj.find("CMD");
                if (iter != obj.end())
                {
                    std::string cmd = iter->second.get_value<std::string>();
                    if (boost::iequals(cmd, "START_PIDS"))
                    {
                        iter = obj.find("PIDS");
                        if (iter != obj.end())
                        {
                            std::string pids_to_add = iter->second.get_value<std::string>();
                            add_pids(pids_to_add);
                        }
                    } else
                    if (boost::iequals(cmd, "KEY_CHANGE"))
                    {
                        iter = obj.find("KEY_EVEN");
                        if (iter != obj.end())
                        {
                            std::string key = iter->second.get_value<std::string>();
                            set_key(true, key);
                        }
                        iter = obj.find("KEY_ODD");
                        if (iter != obj.end())
                        {
                            std::string key = iter->second.get_value<std::string>();
                            set_key(false, key);
                        }
                    } else
                    if (boost::iequals(cmd, "VERSION"))
                    {
                        std::string version_str = json_spirit::write(obj, 0);
                        std::wstring wstr = string_cast<EC_UTF8>(version_str);
                        log_info(L"dvbjapi_plugin_manager::process_plugin_cmd. version info %1%") % wstr;
                    } else
                    if (boost::iequals(cmd, "GET_VERSION"))
                    {
                        std::string version_json_str = "[ { \"CMD\":\"VERSION\" , \"JSON\":\"0.3\", \"VERSION\":\"6.x\", \"APPLICATION_NAME\":\"TVMosaic\" } ]";
                        send_cmd_to_plugin(version_json_str, params->plugin_id);
                    } else
                    if (boost::iequals(cmd, "ERROR"))
                    {
                        std::string error_str = json_spirit::write(obj, 0);
                        std::wstring wstr = string_cast<EC_UTF8>(error_str);
                        log_error(L"dvbjapi_plugin_manager::process_plugin_cmd. Error: %1%") % wstr;
                    } else
                    if (boost::iequals(cmd, "LOG"))
                    {
                        iter = obj.find("TEXT");
                        if (iter != obj.end())
                        {
                            std::string message = iter->second.get_value<std::string>();
                            std::wstring message_wstr = string_cast<EC_UTF8>(message);
                            log_ext_info(L"dvbjapi_plugin_manager::process_plugin_cmd. Plugin %1%: %2%") % params->plugin_id % message_wstr;
                        }
                    } else
                    if (boost::iequals(cmd, "STOP_PIDS") || boost::iequals(cmd, "STOP_ALL_PIDS"))
                    {
                        //stop pids is ignored
                    } else
                    if (boost::iequals(cmd, "START_RAW") || boost::iequals(cmd, "STOP_RAW"))
                    {
                        //start/stop raw is ignored
                    } else
                    if (boost::iequals(cmd, "NOP"))
                    {
                        //nothing to do
                    } else
                    {
                        std::wstring wstr = string_cast<EC_UTF8>(cmd);
                        log_warning(L"dvbjapi_plugin_manager::process_plugin_cmd. Unknown command %1%") % wstr;
                    }
                } else
                {
                    log_warning(L"dvbjapi_plugin_manager::process_plugin_cmd. No command value");
                }
            }
        } else
        {
            std::wstring wstr = string_cast<EC_UTF8>(params->cmd);
            log_warning(L"dvbjapi_plugin_manager::process_plugin_cmd. Received unparseable command %1%") % wstr;
        }
    } catch(...)
    {
        log_warning(L"dvbjapi_plugin_manager::process_plugin_cmd. exception!");
    }
}

void dvbjapi_plugin_manager::add_pids(const std::string& pids_to_add)
{
    std::wstring wstr = string_cast<EC_UTF8>(pids_to_add);
    log_info(L"dvbjapi_plugin_manager::add_pids: %1%") % wstr;

    //parse pids string on space
    typedef boost::tokenizer<boost::char_separator<char>, std::string::const_iterator, std::string> tokenizer_t;
    boost::char_separator<char> sep(" ", 0, boost::keep_empty_tokens);
    tokenizer_t tokens(pids_to_add, sep);

    for (tokenizer_t::iterator tok_iter = tokens.begin(); tok_iter != tokens.end(); ++tok_iter)
    {
        unsigned short pid = (unsigned short)strtoul((*tok_iter).c_str(), NULL, 16);
        if (pid != 0)
	        add_pid(pid);
    }
}

static psi_binary_buffer_t string_to_hex(const std::string& hex_str)
{
    psi_binary_buffer_t result;

    static const char* const lut = "0123456789ABCDEF";
    std::string input = hex_str;
    boost::to_upper(input);
    size_t len = input.length();
    if ( (len & 1) == 0)
    {
        for (size_t i = 0; i < len; i += 2)
        {
            char a = input[i];
            const char* p = std::lower_bound(lut, lut + 16, a);
            if (*p != a)
            {
                //throw std::invalid_argument("not a hex digit");
                continue;
            }

            char b = input[i + 1];
            const char* q = std::lower_bound(lut, lut + 16, b);
            if (*q != b)
            {
                //throw std::invalid_argument("not a hex digit");
                continue;
            }

            unsigned char ch = ((p - lut) << 4) | (q - lut);
            result.push_back(ch);
        }
    }
    return result;
}

static void swap_vector_bytes(psi_binary_buffer_t& buffer)
{
    psi_binary_buffer_t new_buffer;
    for (size_t i=0; i<buffer.size() / 2; i++)
    {
        new_buffer.push_back(buffer[i*2 + 1]);
        new_buffer.push_back(buffer[i*2]);
    }
    buffer = new_buffer;
}

void dvbjapi_plugin_manager::set_key(bool even_key, const std::string& key)
{
    log_ext_info(L"Info. dvbjapi_plugin_manager::set_key. (%1%)") % even_key;
    psi_binary_buffer_t data = string_to_hex(key);
    swap_vector_bytes(data); //this is needed because of the legacy MDAPI support. Bytes there come as unsigned short and are swapped
    csa_handler_->SetDW(even_key, &data[0]);
}
