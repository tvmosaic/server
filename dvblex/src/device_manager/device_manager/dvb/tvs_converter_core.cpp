/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <dl_logger.h>
#include <dl_ts.h>
#include "tvs_converter_core.h"

using namespace dvblink::engine;
using namespace dvblink::logging;

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4355)
#endif

CTVSConverterCore::CTVSConverterCore() :
    m_StreamCallback(NULL),
    m_PATVersionNumber((unsigned char)(rand() * 32.0 / RAND_MAX))
{
}

#ifdef _MSC_VER
#pragma warning(pop)
#endif

CTVSConverterCore::~CTVSConverterCore()
{
}

int CTVSConverterCore::Init(unsigned short cur_nid, unsigned short cur_tid,
    unsigned short cur_sid, unsigned short pmt_pid, std::vector<unsigned short>& program_pids, LPCB_TVS_CCSTREAMFUNC cb, void* user_param)
{
    m_StreamCallback = cb;
	m_UserParam = user_param;
    m_NID = cur_nid;
    m_TID = cur_tid;
    m_SID = cur_sid;
    m_PMTPid = pmt_pid;

    //initialize PID map
    m_PIDs.clear();
    for (unsigned int i=0; i<program_pids.size(); i++)
    {
        m_PIDs[program_pids[i]] = program_pids[i];
    }

    //add PMT and EIT to the pid map
    m_PIDs[m_PMTPid] = m_PMTPid;
    m_PIDs[PID_EIT] = PID_EIT;

	m_NewPATContinuityCounter = 0;
	ts_process_routines::IncPacketVersion(m_PATVersionNumber);
	m_NewPMTContinuityCounter = 0;

	//PMT
	m_PMTParser.Init(m_PMTPid);

    prev_time_ = boost::posix_time::microsec_clock::universal_time();

	return 1;
}

void CTVSConverterCore::Term()
{
    m_StreamCallback = NULL;
}

void CTVSConverterCore::update_program_pids(std::vector<unsigned short>& program_pids)
{
    for (unsigned int i=0; i<program_pids.size(); i++)
    {
        m_PIDs[program_pids[i]] = program_pids[i];
    }
}

void CTVSConverterCore::ProcessPMT(const unsigned char* packet)
{
    //gather original PMT section
    ts_payload_parser::ts_section_list found_sections;
    if (m_PMTParser.AddPacket(packet, TS_PACKET_SIZE, found_sections) > 0)
    {
        //get PMT section
        for (size_t i = 0; i < found_sections.size(); i++)
        {
            unsigned short sid = 0;
            if (ts_process_routines::GetPMTSectionServiceID(found_sections[i].section, found_sections[i].length, sid) && sid == m_SID)
            {
                //send newly created PMT
                m_PacketGen.SplitAndSendSectionBuffer(found_sections[i].section,
                    found_sections[i].length, &m_NewPMTContinuityCounter, m_PMTPid, SendStreamingData, this);
            }
        }
        m_PMTParser.ResetFoundSections(found_sections);
    }
}

int CTVSConverterCore::ProcessStream(const unsigned char* buf, unsigned long len)
{
	CheckTimer();

	const unsigned char* start = buf;
	const unsigned char* cur = buf;
	const unsigned char* end = buf + len;
	size_t to_send = 0;

	while (cur < end)
	{
		unsigned short pid = ts_process_routines::GetPacketPID(cur);
		if (pid == m_PMTPid || m_PIDs.find(pid) == m_PIDs.end())
		{
			if (to_send)
			{
				SendStreamingData(start, to_send, this);
				to_send = 0;
			}

			if (pid == m_PMTPid)
			{
				ProcessPMT(cur);
			}

			cur += TS_PACKET_SIZE;
			start = cur;
		} else
		{
			cur += TS_PACKET_SIZE;
			to_send += TS_PACKET_SIZE;
		}
	}

	if (to_send)
	{
		SendStreamingData(start, to_send, this);
	}

	return 1;
}

void CTVSConverterCore::CreatePATPacket()
{
    int buf_len;
    unsigned char* buf = m_PacketGen.CreatePATPacket(buf_len, m_PATVersionNumber, 
        &m_NewPATContinuityCounter, m_TID, m_SID, m_PMTPid);

    SendStreamingData(buf, buf_len, this);
}

void CTVSConverterCore::SendStreamingData(const unsigned char* buf, int len, void* param)
{
	CTVSConverterCore* parent = (CTVSConverterCore*)param;
	parent->m_StreamCallback(buf, len, parent->m_UserParam);
}

void CTVSConverterCore::CheckTimer()
{
    boost::posix_time::ptime cur_time = boost::posix_time::microsec_clock::universal_time();
    
    boost::posix_time::time_duration time_span(cur_time - prev_time_);

    if (time_span.total_milliseconds() > 100)
    {
        prev_time_ = cur_time;
        //Time to generate and send description packets
		CreatePATPacket();
    }
}
