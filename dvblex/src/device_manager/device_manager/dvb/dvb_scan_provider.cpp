/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/filesystem.hpp>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_ini_file.h>
#include <dl_language_settings.h>
#include "dvb_scan_provider.h"
#include "dvb_strings.h"
#include "dvb_parameters.h"
#include "tvs_core_def.h"
#include "tvs_tr_parser.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

static const provider_scan_type_e provider_scan_type_values[] = {pst_transponder_list, pst_fast_search};
static const char* provider_scan_type_strings[] = {"transponders", "fast_scan"};
static const size_t provider_scan_type_num = sizeof(provider_scan_type_values) / sizeof(provider_scan_type_e);

static std::string dvb_s_parameters_container_id = "00da6e19-0e51-44f5-ad7c-06e8f58a55b1";

inline std::string get_provider_scan_string_from_value(provider_scan_type_e v)
{
    std::string ret_val;

    for (size_t i=0; i<provider_scan_type_num; i++)
    {
        if (provider_scan_type_values[i] == v)
        {
            ret_val = provider_scan_type_strings[i];
            break;
        }
    }

    return ret_val;
}

dvb_scan_provider_t::dvb_scan_provider_t(const dvblink::filesystem_path_t& provider_storage_path) :
    provider_storage_path_(provider_storage_path)
{
}

static void enum_files(const boost::filesystem::path& p, std::vector<boost::filesystem::path>& files)
{
    namespace fs = boost::filesystem;

    if (fs::exists(p) && fs::is_directory(p))
    {
        try
        {
            const fs::directory_iterator end;
            for (fs::directory_iterator it(p); it != end; ++it)
            {
                if (!fs::is_regular_file(it->status()))
                {
                    continue; // skip
                }

                files.push_back(it->path());
            }
        }
        catch (...)
        {
        }
    }
}

static bool get_provider_info_from_file(const boost::filesystem::path& fname, std::string& id, dvblink::provider_name_t& name, dvblink::provider_country_t& country, provider_scan_list_t* scan_list)
{
	bool ret_val = false;

    //id is the file name
#ifdef WIN32
    id = string_cast<EC_UTF8>(fname.filename().wstring());
#else
    id = fname.filename().string();
#endif

    //get human readable name from the file
    ini_file_reader iniFileReader;
    TDLIniFileTree contents;
    filesystem_path_t p(fname);
	iniFileReader.read_ini_file(p.to_string().c_str(), contents);
    
    TDLIniFileTree::iterator it = contents.begin();
	while (it != contents.end())
	{
		//sattype section
		if (boost::iequals("SATTYPE", it->first))
		{
		    for (unsigned int i=0; i<it->second.size(); i++)
		    {
				int id_idx = 0;
				string_conv::apply(it->second[i].key.c_str(), id_idx, 0);
				switch (id_idx)
				{
				case 1:
                    //do nothing
					break;
				case 2:
					name = it->second[i].value;
					ret_val = true;
					break;
				case 3:
					country = it->second[i].value;
					break;
				default:
					break;
				}
		    }
		}
        //transponders section
        if (boost::iequals("DVB", it->first) && scan_list != NULL)
        {
            for (unsigned int i = 0; i<it->second.size(); i++)
            {
                int id_idx = 0;
                string_conv::apply(it->second[i].key.c_str(), id_idx, 0);
                //ignore line with a number of transponders
                if (id_idx != 0)
                {
                    //recreate the original line and push string to the list of transponders
                    std::string tr_line = it->second[i].key + "=" + it->second[i].value;
                    scan_list->push_back(tr_line);
                }
            }
        }
		++it;
	}
	//check whether all parameters where correctly read
	if (name.empty() || id.empty())
	{
		log_error(L"get_provider_info_from_file. Name or ID were not correctly parsed from SATTYPE section? (%1%)") % p.to_wstring();
		ret_val = false;
	}
	return ret_val;
}

void dvb_scan_provider_t::get_manual_fast_scan_provider(provider_info_t& pi)
{
    pi.standard_ = st_dvb_c;

    internal_id_t internal_id;
    internal_id.scan_type_ = pst_fast_search;
    internal_id.standard_ = st_dvb_c;
    internal_id.internal_id_ = dvb_man_fast_scan_key;
    pi.name_ = language_settings::GetInstance()->GetItemName(manual_fast_scan_provider_name);
    pi.desc_ = language_settings::GetInstance()->GetItemName(manual_fast_scan_provider_desc);
    pi.scan_params_[dvb_scan_type_key] = dvb_scan_type_network;

    pi.id_ = internal_id_to_id(internal_id);

    pi.param_container_.id_ = pi.id_.get();
    pi.param_container_.name_ = pi.name_.get();
    pi.param_container_.desc_ = pi.desc_.get();

    //add frequency, modulation and symbol rate
    editable_param_element_t freq_element;
    freq_element.key_ = dvb_frequency_khz_key;
    freq_element.name_ = language_settings::GetInstance()->GetItemName(freq_khz_parameter_name);
    freq_element.value_ = dvb_default_frequency_khz;
    freq_element.format_ = epf_number;
    pi.param_container_.editable_params_.push_back(freq_element);

    editable_param_element_t sr_element;
    sr_element.key_ = dvb_symbol_rate_key;
    sr_element.name_ = language_settings::GetInstance()->GetItemName(sr_parameter_name);
    sr_element.value_ = dvb_default_symbol_rate;
    sr_element.format_ = epf_number;
    pi.param_container_.editable_params_.push_back(sr_element);

    selectable_param_description_t modulation_element;
    modulation_element.key_ = dvb_modulation_key;
    modulation_element.name_ = language_settings::GetInstance()->GetItemName(modulation_parameter_name);
    modulation_element.parameters_list_.push_back(selectable_param_element_t(dvb_qam64_key, language_settings::GetInstance()->GetItemName(qam64_parameter_name)));
    modulation_element.parameters_list_.push_back(selectable_param_element_t(dvb_qam128_key, language_settings::GetInstance()->GetItemName(qam128_parameter_name)));
    modulation_element.parameters_list_.push_back(selectable_param_element_t(dvb_qam256_key, language_settings::GetInstance()->GetItemName(qam256_parameter_name)));
    pi.param_container_.selectable_params_.push_back(modulation_element);
}

standard_name_t dvb_scan_provider_t::get_name_from_standard(source_type_e standard, bool short_name)
{
    standard_name_t name = language_settings::GetInstance()->GetItemName(standard_name_unknown);
    switch(standard)
    {
    case st_dvb_t_t2:
        if (short_name)
            name = language_settings::GetInstance()->GetItemName(standard_name_short_dvbt);
        else
            name = language_settings::GetInstance()->GetItemName(standard_name_dvbt);
        break;
    case st_dvb_c:
        if (short_name)
            name = language_settings::GetInstance()->GetItemName(standard_name_short_dvbc);
        else
            name = language_settings::GetInstance()->GetItemName(standard_name_dvbc);
        break;
    case st_dvb_s_s2:
        if (short_name)
            name = language_settings::GetInstance()->GetItemName(standard_name_short_dvbs);
        else
            name = language_settings::GetInstance()->GetItemName(standard_name_dvbs);
        break;
    case st_atsc:
        if (short_name)
            name = language_settings::GetInstance()->GetItemName(standard_name_short_atsc);
        else
            name = language_settings::GetInstance()->GetItemName(standard_name_atsc);
        break;
    case st_cqam:
        if (short_name)
            name = language_settings::GetInstance()->GetItemName(standard_name_short_cqam);
        else
            name = language_settings::GetInstance()->GetItemName(standard_name_cqam);
        break;
    }
    return name;
}

void dvb_scan_provider_t::get_providers(source_type_e standard, provider_info_map_t& providers)
{
    //only process dvb-x standards
    if (standard != st_dvb_t_t2 && standard != st_dvb_c && standard != st_dvb_s_s2 &&
        standard != st_atsc && standard != st_cqam)
        return;

    providers[standard] = provider_description_t();
    providers[standard].standard_ = standard;
    providers[standard].standard_name_ = get_name_from_standard(standard, false);

    //add scan setting for all / not-encrypted channels
    parameters_container_t& cointainer = providers[standard].parameters_;
    cointainer.id_ = dvb_s_parameters_container_id;
    cointainer.name_ = language_settings::GetInstance()->GetItemName(scan_parameters_container_name);
    cointainer.desc_ = language_settings::GetInstance()->GetItemName(scan_parameters_container_desc);

    selectable_param_description_t enc_free_ch_param;
    enc_free_ch_param.key_ = dvb_scan_fta_only_key;
    enc_free_ch_param.name_ = language_settings::GetInstance()->GetItemName(scan_parameters_fta_only_name);

    selectable_param_element_t enc_free_ch_element;

    enc_free_ch_element.value_ = PARAM_VALUE_YES;
    enc_free_ch_element.name_ = language_settings::GetInstance()->GetItemName(parameter_value_yes);
    enc_free_ch_param.parameters_list_.push_back(enc_free_ch_element);

    enc_free_ch_element.value_ = PARAM_VALUE_NO;
    enc_free_ch_element.name_ = language_settings::GetInstance()->GetItemName(parameter_value_no);
    enc_free_ch_param.parameters_list_.push_back(enc_free_ch_element);

    cointainer.selectable_params_.push_back(enc_free_ch_param);

    if (standard == st_dvb_s_s2)
    {
        //add extra selectable parameters for dvb-s (diseqc and lnb)
        selectable_param_description_t diseqc_param;
        diseqc_param.key_ = dvb_diseqc_type_key;
        diseqc_param.name_ = language_settings::GetInstance()->GetItemName(diseqc_parameter_name);
        int c = 0;
        while (!cDiseqcTypeDefaults[c].typeID.empty())
        {
            selectable_param_element_t element;
            element.value_ = string_cast<EC_UTF8>(cDiseqcTypeDefaults[c].typeID);
            element.name_ = string_cast<EC_UTF8>(cDiseqcTypeDefaults[c].typeName);
            diseqc_param.parameters_list_.push_back(element);
            ++c;
        }
        cointainer.selectable_params_.push_back(diseqc_param);

        selectable_param_description_t lnb_param;
        lnb_param.key_ = dvb_lnb_type_key;
        lnb_param.name_ = language_settings::GetInstance()->GetItemName(lnb_parameter_name);
        c = 0;
        while (!cLNBTypeDefaults[c].typeID.empty())
        {
            selectable_param_element_t element;
            element.value_ = string_cast<EC_UTF8>(cLNBTypeDefaults[c].typeID);
            element.name_ = string_cast<EC_UTF8>(cLNBTypeDefaults[c].typeName);
            lnb_param.parameters_list_.push_back(element);
            ++c;
        }
        cointainer.selectable_params_.push_back(lnb_param);

        editable_param_element_t lof1_element;
        lof1_element.key_ = dvb_lof1_mhz_key;
        lof1_element.name_ = language_settings::GetInstance()->GetItemName(lof1_mhz_param_name);
        lof1_element.value_ = dvb_default_lof_mhz;
        lof1_element.format_ = epf_number;
        cointainer.editable_params_.push_back(lof1_element);

        editable_param_element_t lof2_element;
        lof2_element.key_ = dvb_lof2_mhz_key;
        lof2_element.name_ = language_settings::GetInstance()->GetItemName(lof2_mhz_param_name);
        lof2_element.value_ = dvb_default_lof_mhz;
        lof2_element.format_ = epf_number;
        cointainer.editable_params_.push_back(lof2_element);

        editable_param_element_t lofsw_element;
        lofsw_element.key_ = dvb_lofsw_mhz_key;
        lofsw_element.name_ = language_settings::GetInstance()->GetItemName(lofsw_mhz_param_name);
        lofsw_element.value_ = dvb_default_lof_mhz;
        lofsw_element.format_ = epf_number;
        cointainer.editable_params_.push_back(lofsw_element);

    }

    if (standard == st_dvb_c)
    {
        //add manual fast scan parameters container
        provider_info_t pi;
        get_manual_fast_scan_provider(pi);
        providers[standard].providers_.push_back(pi);
    }

    dvblink::filesystem_path_t base_path = provider_storage_path_ / string_cast<EC_UTF8>(get_source_type_string_from_value(standard));
    //enumerate through provider scan types
    for (size_t j=0; j<provider_scan_type_num; j++)
    {
        dvblink::filesystem_path_t p = base_path / string_cast<EC_UTF8>(provider_scan_type_strings[j]);
        //enumerate all files inside this directory (if it exists)
        std::vector<boost::filesystem::path> files;
        enum_files(p.to_boost_filesystem(), files);
        for (size_t idx=0; idx < files.size(); idx++)
        {
            provider_info_t pi;
            pi.standard_ = standard;
            switch (provider_scan_type_values[j])
            {
            case pst_transponder_list:
                pi.desc_ = language_settings::GetInstance()->GetItemName(normal_scan_desc);
                break;
            case pst_fast_search:
                pi.desc_ = language_settings::GetInstance()->GetItemName(network_scan_desc);
                break;
            default:
                break;
            }

            internal_id_t internal_id;
            internal_id.scan_type_ = provider_scan_type_values[j];
            internal_id.standard_ = standard;

            if (get_provider_info_from_file(files[idx], internal_id.internal_id_, pi.name_, pi.country_, NULL))
            {
                pi.id_ = internal_id_to_id(internal_id);
                providers[standard].providers_.push_back(pi);
            }
        }
    }
}

void dvb_scan_provider_t::id_to_internal_id(const std::string& id_str, internal_id_t& internal_id)
{
    //scan provider
    std::string sp = get_scan_provider_from_id(id_str);
    if (!sp.empty())
    {
        std::string id = id_str.substr(sp.size() + 1);
        //standard
        size_t n = id.find(get_id_sep_symbol());
        if (n != std::string::npos && n > 0)
        {
            std::string str = id.substr(0, n);
            unsigned long ul;
            string_conv::apply(str.c_str(), ul, (unsigned long)st_unknown);
            internal_id.standard_ = (source_type_e)ul;

            //scan type
            size_t m = id.find(get_id_sep_symbol(), n+1);
            if (n != std::string::npos)
            {
                str = id.substr(n+1, m - n - 1);
                string_conv::apply(str.c_str(), ul, (unsigned long)pst_transponder_list);
                internal_id.scan_type_ = (provider_scan_type_e)ul;

                //the rest is internal id
                internal_id.internal_id_ = id.substr(m+1);
            }
        }
    }
}

std::string dvb_scan_provider_t::internal_id_to_id(const internal_id_t& internal_id)
{
    std::stringstream strbuf;
    strbuf << get_scan_provider_name() << get_id_sep_symbol() << internal_id.standard_ << get_id_sep_symbol() << internal_id.scan_type_ << get_id_sep_symbol() << internal_id.internal_id_;

    return strbuf.str();
}

bool dvb_scan_provider_t::get_provider_details(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, provider_info_t& provider_info, provider_scan_list_t& scan_list)
{
    bool ret_val = false;

    //convert id to internal id
    internal_id_t internal_id;
    id_to_internal_id(id.get(), internal_id);

    if (boost::iequals(internal_id.internal_id_, dvb_man_fast_scan_key))
    {
        dvb_scan_provider_t::get_manual_fast_scan_provider(provider_info);
        //add scan string from scanner_settings map
        unsigned long freq = 0;
        unsigned long sr = 0;
        DL_E_MOD_DVBC qam_val = MOD_DVBC_QAM_64;
        concise_param_map_t::const_iterator it;
        if ((it = scanner_settings.find(dvb_frequency_khz_key)) != scanner_settings.end())
            freq = atol(it->second.c_str());

        if ((it = scanner_settings.find(dvb_symbol_rate_key)) != scanner_settings.end())
            sr = atol(it->second.c_str());

        if ((it = scanner_settings.find(dvb_modulation_key)) != scanner_settings.end())
        {
            if (boost::iequals(it->second.get(), dvb_qam64_key))
                qam_val = MOD_DVBC_QAM_64;
            if (boost::iequals(it->second.get(), dvb_qam128_key))
                qam_val = MOD_DVBC_QAM_128;
            if (boost::iequals(it->second.get(), dvb_qam256_key))
                qam_val = MOD_DVBC_QAM_256;
        }

        ret_val = (freq != 0) && (sr != 0);

        std::string tune_string = CTVSTransponderParser::make_dvbc_tuning_string(freq, sr, qam_val);
        scan_list.push_back(tune_string);
    } else
    {
        //form path to the file
        dvblink::filesystem_path_t p = provider_storage_path_ / string_cast<EC_UTF8>(get_source_type_string_from_value(internal_id.standard_)) / string_cast<EC_UTF8>(get_provider_scan_string_from_value(internal_id.scan_type_)) / internal_id.internal_id_;

        std::string idstr;
        ret_val = get_provider_info_from_file(p.to_boost_filesystem(), idstr, provider_info.name_, provider_info.country_, &scan_list);
        provider_info.id_ = internal_id_to_id(internal_id);

        std::string scan_type;
        if (internal_id.scan_type_ == pst_transponder_list)
            scan_type = dvb_scan_type_normal;
        else if (internal_id.scan_type_ == pst_fast_search)
            scan_type = dvb_scan_type_network;
        provider_info.scan_params_[dvb_scan_type_key] = scan_type;
        provider_info.standard_ = internal_id.standard_;
        switch (provider_info.standard_)
        {
        case pst_transponder_list:
            provider_info.desc_ = language_settings::GetInstance()->GetItemName(normal_scan_desc);
            break;
        case pst_fast_search:
            provider_info.desc_ = language_settings::GetInstance()->GetItemName(network_scan_desc);
            break;
        default:
            break;
        }

    }

    return ret_val;
}

void dvb_scan_provider_t::fill_headend_info(const provider_info_t& provider_info, const concise_param_map_t& scanner_settings, headend_info_t& headend_info)
{
    //string to hash
    std::string hash_str = provider_info.id_.get();
    //name
    headend_info.name_ = provider_info.name_.get();

    //description starts with standard
    headend_info.desc_ = get_name_from_standard(provider_info.standard_, true).get();

    //diseqc for dvb-s
    if (provider_info.standard_ == st_dvb_s_s2)
    {
        parameter_value_t diseqc_type = get_value_for_param(dvb_diseqc_type_key, scanner_settings);
        if (!diseqc_type.empty())
        {
            std::string str = language_settings::GetInstance()->GetItemName(diseqc_parameter_name) + ": ";

            int c = 0;
            while (!cDiseqcTypeDefaults[c].typeID.empty())
            {
                if (boost::iequals(diseqc_type.get(), cDiseqcTypeDefaults[c].typeID))
                {
                    str += string_cast<EC_UTF8>(cDiseqcTypeDefaults[c].typeName);
                    break;
                }
                ++c;
            }
            headend_info.desc_ += ", " + str;
        }
    }

    //convert id to internal id
    internal_id_t internal_id;
    id_to_internal_id(provider_info.id_.get(), internal_id);

    //network scan
    if (boost::iequals(internal_id.internal_id_, dvb_man_fast_scan_key))
    {
        //add frequency to description
        std::string str = language_settings::GetInstance()->GetItemName(freq_khz_parameter_name) + ": ";
        parameter_value_t freq = get_value_for_param(dvb_frequency_khz_key, scanner_settings);
        if (!freq.empty())
        {
            str += freq.get();
            //add frequency to hash
            hash_str += freq.get();
        }

        headend_info.desc_ += ", " + str;
    }

    //hash - headend_id
    boost::to_lower(hash_str);
    headend_info.id_ = make_headend_id(provider_info.id_, hash_str);

}

bool dvb_scan_provider_t::get_rescan_settings(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, parameters_container_t& settings)
{
    //call default function implementation
    scan_provider_t::get_rescan_settings(id, scanner_settings, settings);

    //add scan fta channels only settings
    selectable_param_description_t enc_free_ch_param;
    enc_free_ch_param.key_ = dvb_scan_fta_only_key;
    enc_free_ch_param.name_ = language_settings::GetInstance()->GetItemName(scan_parameters_fta_only_name);

    selectable_param_element_t enc_free_ch_element;

    enc_free_ch_element.value_ = PARAM_VALUE_YES;
    enc_free_ch_element.name_ = language_settings::GetInstance()->GetItemName(parameter_value_yes);
    enc_free_ch_param.parameters_list_.push_back(enc_free_ch_element);

    enc_free_ch_element.value_ = PARAM_VALUE_NO;
    enc_free_ch_element.name_ = language_settings::GetInstance()->GetItemName(parameter_value_no);
    enc_free_ch_param.parameters_list_.push_back(enc_free_ch_element);

    settings.selectable_params_.push_back(enc_free_ch_param);

    return true;
}
