/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <drivers/tuner_factory.h>
#include "dvb_tvs_helpers.h"
#include "tvs_core_def.h"
#include "dvb_epg_scanner.h"
#include "dvb_parameters.h"
#include "eit_epg_module.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

static const std::wstring epg_scanner_client_id = L"dvb_epg_scanner";

dvb_epg_scanner_t::dvb_epg_scanner_t(dvblink::tuner_t* tuner, const concise_param_map_t& params) :
    tuner_(tuner), tuner_type_(dvblink::TUNERTYPE_UNKNOWN), epg_scanner_module_(NULL), params_(params)
{
}

dvb_epg_scanner_t::~dvb_epg_scanner_t()
{
}

static const std::string make_channel_id_hash(boost::uint32_t nid, boost::uint32_t tid, boost::uint32_t sid)
{
    std::stringstream buf;
    buf << nid << ":" << tid << ":" << sid;

    return buf.str();
}

bool dvb_epg_scanner_t::start()
{
    epg_channel_tune_info_list_t channels;
    epg_box_->get_channels_to_scan(channels);

    for (size_t i=0; i<channels.size(); i++)
    {
        DL_E_TUNER_TYPES tuner_type;
        concise_program_info_t program_info;
        if (get_channel_tuning_params_from_string(channels[i].tuning_params_, tuner_type, program_info))
        {
            const std::string hash = make_channel_id_hash(program_info.nid_, program_info.tid_, program_info.sid_);
            dvb_id_to_channel_map_[hash] = channels[i].channel_id_;
        }
    }

    return true;
}

void dvb_epg_scanner_t::stop()
{
    if (stream_src_.is_tuner_started())
        stream_src_.Term();
}

bool dvb_epg_scanner_t::do_scan(const concise_channel_tune_info_t& tuning_params)
{
    bool ret_val = false;

    dvblink::DL_E_TUNER_TYPES tuner_type = dvblink::TUNERTYPE_UNKNOWN;
    concise_program_info_t program_info;
    if (get_channel_tuning_params_from_string(tuning_params, tuner_type, program_info))
    {
        if (tuner_type_ == dvblink::TUNERTYPE_UNKNOWN ||
            tuner_type_ != tuner_type)
        {
            //(re)init stream source
            if (stream_src_.is_tuner_started())
                stream_src_.Term();

            tvs_stream_source_options_t options;
            options.tuner_type_ = tuner_type;
            stream_src_.Init(tuner_, options, stream_callback, this);

            tuner_type_ = tuner_type;
        }
        //tune transponder
        if (stream_src_.TuneTransponder(&program_info.tuning_info_.transponder_info, program_info.tuning_info_.diseqc,
            program_info.tuning_info_.diseqc_rawdata.c_str()) > 0)
        {

            {
                boost::unique_lock<boost::mutex> lock(stream_lock_);
                epg_scanner_module_ = new eit_epg_module_t(this);
            }

            if (epg_scanner_module_->start_scan(params_))
            {
                while (true)
                {
                    //if exit is signalled - break and exit
                    if (exit_flag_)
                    {
                        epg_scanner_module_->stop_scan();
                        ret_val = false;
                        break;
                    }
                    //otherwise, check scan status
                    if (epg_scanner_module_->get_scan_status() != ems_in_progress)
                    {
                        //process whatever was collected from epg data and exit
                        const dvb_channel_epg_desc_list_t* epg = epg_scanner_module_->get_epg();
                        if (epg != NULL)
                        {
                            dvb_channel_epg_desc_list_t::const_iterator it = epg->begin();
                            while (it != epg->end() && !exit_flag_)
                            {
                                channel_id_t ch_id;
                                if (it->is_valid_channel_id())
                                {
                                    ch_id = it->channel_id_;
                                } else
                                {
                                    const std::string hash = make_channel_id_hash(it->nid_, it->tid_, it->sid_);
                                    if (dvb_id_to_channel_map_.find(hash) != dvb_id_to_channel_map_.end())
                                        ch_id = dvb_id_to_channel_map_[hash];
                                }

                                if (!ch_id.empty())
                                    report_epg(ch_id, it->events_);

                                ++it;
                            }
                        }
                        break;
                    }

                    boost::this_thread::sleep(boost::posix_time::milliseconds(10));
                }
                ret_val = true;
            }

            {
                boost::unique_lock<boost::mutex> lock(stream_lock_);
                delete epg_scanner_module_;
                epg_scanner_module_ = NULL;
            }

        } else
        {
            log_error(L"dvb_epg_scanner_t::do_scan. Unable to tune transponder");
        }
    } else
    {
        log_error(L"dvb_epg_scanner_t::do_scan. get_channel_tuning_params_from_string returned error for %1%") % string_cast<EC_UTF8>(tuning_params.tuning_params_.get());
    }
    return ret_val;
}

void dvb_epg_scanner_t::add_pid(unsigned short pid)
{
    stream_src_.AddPID(epg_scanner_client_id.c_str(), pid);
}

void __stdcall dvb_epg_scanner_t::stream_callback(const unsigned char* Buf, unsigned long Len, void* user_param)
{
    dvb_epg_scanner_t* parent = (dvb_epg_scanner_t*)user_param;

    boost::unique_lock<boost::mutex> lock(parent->stream_lock_);

    if (parent->epg_scanner_module_ != NULL)
        parent->epg_scanner_module_->write_stream(Buf, Len);
}

void dvb_epg_scanner_t::get_scan_transponders(const epg_channel_tune_info_list_t& channels, channel_tuning_params_list_t& transponders)
{
    std::map<std::wstring, concise_channel_tune_info_t> res_map;
    for (size_t i=0; i<channels.size(); i++)
    {
        DL_E_TUNER_TYPES tuner_type;
        concise_program_info_t program_info;
        if (get_channel_tuning_params_from_string(channels[i].tuning_params_, tuner_type, program_info))
        {
            std::wstring hash;
            GetTransponderHash(tuner_type, program_info.tuning_info_.transponder_info, program_info.tuning_info_.diseqc_rawdata.c_str(), program_info.tuning_info_.diseqc, hash);
            std::wstringstream buf;
            buf << tuner_type << L":" << hash;
            if (res_map.find(buf.str()) == res_map.end())
                res_map[buf.str()] = channels[i].tuning_params_;
        }
    }
    std::map<std::wstring, concise_channel_tune_info_t>::iterator it = res_map.begin();
    while (it != res_map.end())
    {
        transponders.push_back(it->second);
        ++it;
    }
}
