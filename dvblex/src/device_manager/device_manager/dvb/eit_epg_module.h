/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>
#include <vector>
#include <boost/thread.hpp>
#include <dl_parameters.h>
#include "tvs_service_list.h"
#include "dvb_epg_module.h"
#include "eit_epg_parser.h"

namespace dvblex { 

enum eit_scanner_state_e
{
    esse_none,
    esse_eit_sdt,
    esse_completed
};

class eit_epg_module_t : public dvb_epg_module_t
{
public:
    eit_epg_module_t(epg_host_control_t* host_control);
    ~eit_epg_module_t();

    virtual bool start_scan(const concise_param_map_t& scan_params);
    virtual void stop_scan();

    virtual epg_module_status_e get_scan_status(){return m_Status;}

    virtual const dvb_channel_epg_desc_list_t* get_epg(){return &epg_data_;}

    virtual void write_stream(const unsigned char* buffer, size_t length);

protected:
    eit_scanner_state_e scan_state_;
    volatile epg_module_status_e m_Status;
    boost::thread* m_ScanThread;
    bool m_ScanExit;
    boost::mutex m_eitparser_cs;
    CDLTSEPGParser m_eit_parser;
    dvblink::engine::ts_section_payload_parser m_payload_parser;
    bool m_bEITReceived;
    bool m_bIsCompleted;
    dvb_channel_epg_desc_list_t epg_data_;
    std::wstring m_Language;
	unsigned long m_EITScanTimeout;
	bool m_IgnoreShortDesc;
	unsigned short m_EitPid;
    bool use_event_id_;
    boost::posix_time::ptime scan_start_time_;
    unsigned long nosdt_timeout_;
    bool use_epg_only_from_current_tr_;

    void ScanThread();
	void set_config_defaults();
    void stop_scan_thread();
    void set_scan_state_to_idle();
    void set_scan_state_to_eit_sdt();
    void switch_to_eit_sdt_scan();
    void reset_services_map(TTVSServiceListMap* service_map);

	//channel names variables
    bool m_bIsSDTCompleted;
	TTVSServiceListMap m_CurServiceListMap;
	TTVSServiceListMap m_OtherServiceListMap;
    dvblink::engine::ts_section_payload_parser m_SDT_payload_parser;
	void ProcessSDTSection(unsigned char* section, int section_len);
	bool IsServiceMapCompleted(TTVSServiceListMap* service_map);
	void GetScannedServices(TTVSServiceListMap* service_map, std::vector<TVSC_ChannelType>& services);
    void process_seen_services(CDLTSEPGServiceList* seen_services);
};

}