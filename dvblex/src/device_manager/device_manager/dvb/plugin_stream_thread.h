/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/


#pragma once

#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <map>
#include <dl_ts_info.h>
#include <dl_event.h>
#include "tvs_program_streamer.h"

class plugin_stream_thread
{
public:
	plugin_stream_thread(tvs_program_streamer* program_src);
	~plugin_stream_thread();

	void write_stream(const unsigned char* buf, size_t len);
	void wait_for_write_to_finish();

protected:
	void streaming_thread_func();

	boost::thread* streaming_thread_;
	bool exit_;
	dvblink::event wait_event_;
	dvblink::event write_finished_event_;
	const unsigned char* buf_;
	size_t len_;
	tvs_program_streamer* program_src_;
};

