/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef _TVSOURCECHANNELSCANNER_H_
#define _TVSOURCECHANNELSCANNER_H_

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/locks.hpp>
#include <map>
#include <dl_ts_info.h>
#include <drivers/tuner_factory.h>
#include "tvs_core_def.h"
#include "tvs_service_list.h"
#include "tvs_stream_src.h"

//**************************************************************************
//Channel scanner states

enum ETVS_SCAN_STATES
{
	ETVS_SS_IDLE,
	ETVS_SS_SDT,
	ETVS_SS_SDT_COMPLETED,
	ETVS_SS_CHANNELSCAN_NIT,
    ETVS_SS_CHANNELSCAN_NIT_COMPLETED,
	ETVS_SS_PAT,
	ETVS_SS_PAT_COMPLETED,
	ETVS_SS_PMT,
	ETVS_SS_PMT_COMPLETED,
	ETVS_SS_NIT,
	ETVS_SS_NIT_COMPLETED,
	ETVS_SS_SDT_ALL_SERVICES,
	ETVS_SS_SDT_ALL_SERVICES_COMPLETED,
};

//**************************************************************************

enum scan_info_state_e
{
    sis_pending,
    sis_scanning,
    sis_completed
};

//types for CA descriptors scan (f@#$ng AnySee!)

struct STVSChannelScanInfo
{
    STVSChannelScanInfo(){pmt_pid = 0; state = sis_pending;}

    std::wstring ch_id;
    unsigned short sid;
    unsigned short pmt_pid;
    std::vector<dvblink::engine::STSCADescriptorInfo> ca_desc_list;
    dvblink::engine::ts_section_payload_parser payload_parser;
    scan_info_state_e state;
};

typedef std::vector<STVSChannelScanInfo> TTVSChannelScanInfoExList;

//**************************************************************************

const unsigned short tvs_channel_scanner_invalid_network_id = 0xFFFF;

//**************************************************************************

class CTVSChannelScanner
{
public:
    CTVSChannelScanner(dvblink::tuner_t* tuner);
	~CTVSChannelScanner();

	int Init(dvblink::DL_E_TUNER_TYPES device_type, const TVSC_HeadendType& headend);
	int Term();

    int Tune(const std::wstring& tr_data, bool bwait_for_lock, int timeout_msec);
    int ScanTransponderList(int timeout_msec, TVSC_NetworkScanList& network_list);
	int Scan(int timeout_msec, bool fta_channels_only, std::vector<TVSC_ServiceInfo>* expectedServices, TVSC_ChannelMap& scanned_services);
	bool GetSignalStats(unsigned char* level, unsigned char* quality, unsigned char* locked);

    void get_transponder_id(const std::wstring& tr_data, std::wstring& id);

protected:
	static void __stdcall StreamFunction(const unsigned char* Buf, unsigned long Len, void* user_param);
	void SwitchToSDTScan();
	void SwitchToIdle();
	void SwitchToCompleted();
    void SwitchToNITScan();
    void SwitchToChNumCompleted();
    void SwitchToChNumScan(TVSC_ChannelMap& scanned_services);
    void SwitchToPATScan(TVSC_ChannelMap& scanned_services);
    void SwitchToPATScanCompleted();
    void SwitchToPMTScan();
    void SwitchToPMTScanCompleted();
    void SwitchToSDTAllServiceScan();
    void SwitchToSDTAllServiceScanCompleted();
    void ProcessSDTPayload(unsigned char* payload, int payload_length);
	void ProcessCompletedSections(std::vector<TVSC_ServiceInfo>* expectedServices, TVSC_ChannelMap& scanned_services);
	void ResetDescriptors();
    void ProcessCollectedDeliveryDescriptors(TVSC_NetworkScanList& network_list);
    void ProcessNITPayload(unsigned char* payload, int payload_length);
    bool IsServiceExpected(std::vector<TVSC_ServiceInfo>* expectedServices, TVSC_ChannelType& service);
	bool IsDVBDevice();
    void ProcessScannedChannelNumbers(TVSC_ChannelMap& scanned_services);
    void ProcessNITPayloadForChNum(unsigned char* payload, int payload_length);
    void RemoveFoundLCNServices(std::vector<dvblink::engine::SDVBTLCNDesc>& lcn_services);
    bool FindEncryptedChannel(TVSC_ChannelMap& services);
    void ProcessScannedCA(TVSC_ChannelMap& scanned_services);
    void convert_qam_pat_to_services(std::vector<TVSC_ChannelType>& service_list);
    void convert_na_dvbs_pat_to_services(std::vector<TVSC_ChannelType>& service_list);
    void remove_non_fta_channels_from_scan(TVSC_ChannelMap& scanned_services);
    void add_next_pmt_pid(unsigned short pid_to_remove);

private:
    dvblink::tuner_t* tuner_;
    tvs_stream_source_options_t options_;
    TVSC_HeadendType headend_;
    TVSC_TransponderTypeList m_TransponderList;
    CTVSStreamSource* m_StreamSource;
    unsigned short m_ServicesTablePID;
    unsigned short m_ServicesTableId;
    std::wstring scanner_id_;

    ETVS_SCAN_STATES m_ScanningState;
	boost::recursive_mutex m_cs;
	dvblink::engine::ts_section_payload_parser m_SectionPayloadParser;
	dvblink::engine::ts_section_payload_parser pat_parser_;
    std::vector<dvblink::engine::STSPATServiceInfo> pat_services_;
	TTVSServiceListMap m_ServiceListMap;
    std::vector<dvblink::engine::STSDeliverySystemInfo> m_DeliverySystems;
    TTVSNITListMap m_NITSectionsMap;
    std::map<unsigned short, unsigned short> m_ExpectedNIDMap;
    TTVSChannelScanInfoExList m_ChannelCAScanInfo;
    std::map<unsigned short, unsigned short> m_PMTMap;
    std::map<unsigned short, dvblink::engine::STSServiceInfoEx> sid_to_service_map_;

	//currently set transponder
	dvblink::TTransponderInfo m_Transponder;
	unsigned char m_DiseqcValue;
    std::wstring m_RawDiseqcData;
};

///////////////////////////////////////////////////////////////////////////////
#endif //_TVSOURCECHANNELSCANNER_H_
