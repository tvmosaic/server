/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <vector>
#include <dl_ts_info.h>
#include <dl_packet_buffer.h>
#include "tvs_core_def.h"
#include "tvs_converter_core.h"
#include "tvs_protected_map.h"

class CTVSStreamSource;
class tvs_plugin_manager_base;
class CTVSPluginCallbacks;
struct TProgramm;

class tvs_program_streamer
{
public:
	typedef void (*program_streamer_cb_f)(std::wstring& channel_id, const unsigned char* buf, unsigned long len, void* user_param);

	enum ETVS_PROGRAM_STREAMING_STATE
	{
		ETVS_PST_IDLE,
		ETVS_PST_PAT,
		ETVS_PST_PAT_COMPLETED,
		ETVS_PST_PMT,
		ETVS_PST_PMT_COMPLETED,
		ETVS_PST_CA,
		ETVS_PST_CA_COMPLETED,
		ETVS_PST_STREAMING
	};

public:
	tvs_program_streamer(std::wstring& channel_id, concise_program_info_t& program_info, program_streamer_cb_f cb, void* user_param);
	~tvs_program_streamer();

	void start(CTVSStreamSource* stream_source);
	void stop();
	void write_stream(const unsigned char* buf, unsigned long len);

    std::wstring get_channel_id(){return channel_id_;}
    void get_tuning_info(transponder_tuning_info_t& tuning_info){tuning_info = program_info_.tuning_info_;}
    bool is_channel_encrypted() {return program_info_.encrypted_; }

#ifdef TVS_PLUGINS_SUPPORTED
	void set_plugin_manager(tvs_plugin_manager_base* plugin_man);
	tvs_plugin_manager_base* release_plugin_manager();
    static void __stdcall cb_add_pid(unsigned short pid, void* user_param);
#endif

protected:
    void SetIdleScanState();
    void SetPATScanState();
    void SetPATCompletedScanState();
    void SetPMTScanState();
    void SetPMTCompletedScanState();
    void SetCAScanState();
    void SetCACompletedScanState();
    void SetStreamingState();
    void AddPID(unsigned short pid);
    void AddPIDs(std::vector<unsigned short>& pids);
    static void  __stdcall StreamCallback(const unsigned char* Buf, unsigned long Len, void* user_param);
    void FillTProgrammStructure(TProgramm* new_program);
    int GetEMMPidForCASystemID(unsigned short ca_system_id);
    bool ProcessCATPacket(const unsigned char* packet);
	void SendStreamWithCATFilter(const unsigned char* buf, unsigned long len);
    void get_elementary_pids(std::vector<unsigned short>& pids);
    void check_pmt_version(const unsigned char* buf, unsigned long len);
	void write_stream_impl(const unsigned char* buf, unsigned long len);
    void process_streaming_data(const unsigned char* buf, unsigned long len);

	static void converter_callback(const unsigned char* buf, unsigned long len, void* user_param);

private:
    program_streamer_cb_f user_cb_;
	void* user_param_;
	std::wstring channel_id_;
	CTVSStreamSource* stream_source_;
	concise_program_info_t program_info_;
#ifdef TVS_PLUGINS_SUPPORTED
    tvs_plugin_manager_base* plugin_manager_;
#endif

	void tuning_thread();

	//Program selector staff
	boost::thread* tuning_thread_;
	bool exit_;
	dvblink::engine::ts_section_payload_parser m_SectionPayloadParser;
	dvblink::engine::ts_section_payload_parser m_CATParser;
	std::vector<dvblink::engine::STSCADescriptorInfo> m_cat_ca_desc_list;
	ETVS_PROGRAM_STREAMING_STATE program_change_state_;
	unsigned short m_pmt_pid;
	unsigned short m_pcr_pid;
	std::vector<dvblink::engine::STSESInfo> m_ProgramStreamInfo;
	std::vector<dvblink::engine::STSCADescriptorInfo> m_pmt_ca_desc_list;
	std::vector<unsigned char> m_PMTSectionBuffer;
	std::vector<unsigned char> m_CATSectionBuffer;
	CTVSConverterCore spts_stream_converter_;
	bool abort_ca_scan_;
	unsigned char pmt_version_;
	pid_filter pid_filter_;
	dvblink::engine::ts_packet_buffer stream_buffer_;	
};
