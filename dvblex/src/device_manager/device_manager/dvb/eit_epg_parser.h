/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_platforms.h>
#include <dl_epgevent.h>
#include <vector>
#include <string>
#define TSEPG_MAX_SECTIONS_PER_SEGMENT 8
#define TSEPG_MAX_SEGMENTS_PER_TABLE 32
#define TSEPG_MAX_TABLES_CUR_STREAM 16
#define TSEPG_CUR_STREAM_TABLEID_BASE 0x50
#define TSEPG_OTHER_STREAM_TABLEID_BASE 0x60

enum EMBTS_EPGPARSER_ADDSECTION_RES
{
	EMBTS_EPAR_DISCARDED = 0,
	EMBTS_EPAR_ACCEPTED,
	EMBTS_EPAR_EPG_SERVICE_COMPLETE,
	EMBTS_EPAR_EPG_INFO_COMPLETE
};

int GetBaseSectionNumPerSegment(int segment_num);
int GetSegmentNumForSection(int section_num);

class CDLTSEPGStreamId
{
public:
	CDLTSEPGStreamId(){m_NID = 0; m_TID = 0; m_SID = 0;}
	CDLTSEPGStreamId(unsigned short NID, unsigned short TID, unsigned short SID){m_NID = NID; m_TID = TID; m_SID = SID;}

	bool IsEqual(CDLTSEPGStreamId& id);

	unsigned short m_NID;
	unsigned short m_TID;
	unsigned short m_SID;
};

//segment-wide section Id to walk through the sections
struct SEPGSegmentSection
{
	int m_Section;
	unsigned char* m_SectionData;
};

//collection of services in a segment (Segment description)
class CDLTSEPGSegment
{
public:
	CDLTSEPGSegment(int segment_num);
	~CDLTSEPGSegment();

	EMBTS_EPGPARSER_ADDSECTION_RES AddSection(unsigned char* eit_section, int eit_section_len);

	bool GetFirstSection(SEPGSegmentSection& section);
	bool GetNextSection(SEPGSegmentSection& section);

	bool IsCompleted();

protected:
	int m_SegmentNum;
	int m_MaxSectionNum;
	unsigned char* m_Sections[TSEPG_MAX_SECTIONS_PER_SEGMENT];
};

//table-wide section Id to walk through the sections
struct SEPGTableSection
{
	int m_Segment;
	int m_Section;
	unsigned char* m_SectionData;
};

//Table - collection of segments
class CDLTSEPGTable
{
public:
	CDLTSEPGTable();
	~CDLTSEPGTable();

	void Reset();
	bool IsCompleted();
	EMBTS_EPGPARSER_ADDSECTION_RES AddSection(unsigned char* eit_section, int eit_section_len);

	bool GetFirstSection(SEPGTableSection& section);
	bool GetNextSection(SEPGTableSection& section);

protected:
	CDLTSEPGSegment* m_Segments[TSEPG_MAX_SEGMENTS_PER_TABLE];
	int m_Version;
	int m_LastSectionNum;
};

//*******************************************
class CDLTSEPGSeenService
{
public:
	CDLTSEPGSeenService(int table_base, CDLTSEPGStreamId& stream_id, int last_tableid);
	~CDLTSEPGSeenService();

	EMBTS_EPGPARSER_ADDSECTION_RES AddSection(unsigned char* eit_section, int eit_section_len);
	bool IsCompleted();
    void GetEPGEvents(const wchar_t* language, bool bIgnoreShortDesc, bool use_event_id, dvblink::engine::DLEPGEventList& event_list, bool& bCancelFlag);

	CDLTSEPGStreamId* GetStreamId(){return &m_StreamId;}
	bool IsStreamIdEqual(CDLTSEPGStreamId& stream_id);

protected:
	int m_TableBaseId;
	int m_LastTableId;
	bool m_bCompleted;
	CDLTSEPGStreamId m_StreamId;

	CDLTSEPGTable* m_EPGTables[TSEPG_MAX_TABLES_CUR_STREAM];
};

typedef std::vector<CDLTSEPGSeenService*> TMBTSEPGSeenServicesList;

class CDLTSEPGServiceList
{
public:
	CDLTSEPGServiceList(int table_base);
	~CDLTSEPGServiceList();

	void Reset();
	EMBTS_EPGPARSER_ADDSECTION_RES AddSection(unsigned char* eit_section, int eit_section_len);
	bool IsCompleted();
	bool AcceptSection(unsigned char* eit_section, int eit_section_len);

    TMBTSEPGSeenServicesList* GetSeenServices(){return &m_SeenServices;};

protected:
	bool IsServiceCompleted(CDLTSEPGStreamId* stream_id);
	int FindEPGStream(CDLTSEPGStreamId& stream_id);

	TMBTSEPGSeenServicesList m_SeenServices;
	int m_TableBaseId;
};

//It is assumed that all operations on CDLTSEPGParser class are thread-safe. If it is not
//the case, then the container class should take care of synchronization.
class CDLTSEPGParser
{
public:
	CDLTSEPGParser();
	~CDLTSEPGParser();

	void Reset();
	EMBTS_EPGPARSER_ADDSECTION_RES AddSection(unsigned char* eit_section, int eit_section_len);
	bool IsCompleted();

    CDLTSEPGServiceList* GetCurrentServices(){return m_CurTrServices;};
    CDLTSEPGServiceList* GetOtherServices(){return m_OtherTrServices;};

protected:
	CDLTSEPGServiceList* m_CurTrServices;
	CDLTSEPGServiceList* m_OtherTrServices;
};


