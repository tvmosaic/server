/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/thread.hpp>
#include <dl_circle_buffer.h>
#include "tvs_protected_map.h"
#include "mdapi.h"
#include "csa.h"

typedef std::vector<unsigned char> psi_binary_buffer_t;

struct plugin_program_extra_info
{
    unsigned short nid;
    psi_binary_buffer_t pmt_section;
    psi_binary_buffer_t cat_section;
};

class tvs_plugin_manager_base
{
public:
    typedef void (__stdcall *LPCB_TVS_PLUGMUN_ADDPID)(unsigned short pid, void* user_param);

public:
	tvs_plugin_manager_base();
	virtual ~tvs_plugin_manager_base();

	virtual bool init();
	virtual void term();
	
    virtual void process_stream(const unsigned char* buf, size_t len);

    virtual bool channel_changed(TProgramm* tuner_info, plugin_program_extra_info& extra_info);
    
    virtual void add_pids_to_decrypt(std::vector<unsigned short>& pids);

    void set_addpid_func(LPCB_TVS_PLUGMUN_ADDPID addpid_func, void* user_param);
    void reset_addpid_func();

protected:
    void streaming_thread();
	virtual void process_requested_packet(const unsigned char* buf, size_t len){};
	void add_pid(unsigned short pid);

    boost::mutex lock_;
    std::auto_ptr<CTVSCSAHandler> csa_handler_;

	//decoupling circle buffer
    std::auto_ptr<dvblink::engine::ts_circle_buffer> circle_buffer_;
	boost::thread* streaming_thread_;
	volatile bool stream_exit_flag_;

private:
	pid_filter pid_set_;
	LPCB_TVS_PLUGMUN_ADDPID add_pid_func_; 
	void* user_param_;
	std::map<unsigned short, unsigned short> pids_to_decrypt_;
	
	boost::mutex add_pid_lock_;
};


