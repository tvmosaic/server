/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_utils.h>
#include <dl_ts_info.h>
#include "dvbjapi.h"
#include "dvbjapi_plugin.h"

using namespace dvblink::engine;
using namespace dvblink::logging;

dvbjapi_plugin::dvbjapi_plugin(dvblink::filesystem_path_t& plugin_path) :
	plugin_path_(plugin_path),
    dvbjapi_plugin_callback_(NULL)
{
}

bool dvbjapi_plugin::init(dvbjapi_plugin_callback_f cb, void* user_param)
{
    bool ret_val = false;
    log_info(L"dvbjapi_plugin::init. Plugin %1%") % plugin_path_.to_wstring();

    dvbjapi_plugin_callback_ = cb;
    user_param_ = user_param;

    plugin_.SetLibName(plugin_path_.to_string());
    if (plugin_.Load())
    {
		dvbjapi_load_ = (dvbjapi_load_f)plugin_.GetFuncAddr(DVBJAPI_LOAD_FN_NAME);
		dvbjapi_unload_ = (dvbjapi_unload_f)plugin_.GetFuncAddr(DVBJAPI_UNLOAD_FN_NAME);
		dvbjapi_receive_from_parent_ = (dvbjapi_receive_from_parent_f)plugin_.GetFuncAddr(DVBJAPI_RCV_FROM_PARENT_FN_NAME);
		dvbjapi_pid_data_ = (dvbjapi_pid_data_f)plugin_.GetFuncAddr(DVBJAPI_PID_DATA_FN_NAME);

        if (dvbjapi_load_ != NULL && dvbjapi_unload_ != NULL &&
            dvbjapi_receive_from_parent_ != NULL && dvbjapi_pid_data_ != NULL)
        {
            int ret = dvbjapi_load_(plugin_path_.to_string().c_str(), &dvbjapi_plugin::send_to_parent_callback, (void*)this);
            if (ret == DVBJAPI_SUCCESS)
            {
                log_info(L"dvbjapi_plugin::init. Successfully loaded plugin %1%") % plugin_path_.to_wstring();
                send_get_version_cmd();
                ret_val = true;
            } else
            {
                plugin_.Unload();
                dvbjapi_plugin_callback_ = NULL;
    	        log_error(L"dvbjapi_plugin::init. Load returned error %1% in plugin %2%") % ret % plugin_path_.to_wstring();
            }
        } else
        {
            plugin_.Unload();
            dvbjapi_plugin_callback_ = NULL;
    	    log_error(L"dvbjapi_plugin::init. Missing required exports in plugin %1%") % plugin_path_.to_wstring();
        }
    } else
    {
        dvbjapi_plugin_callback_ = NULL;
    	log_error(L"dvbjapi_plugin::init. unable to load plugin %1%") % plugin_path_.to_wstring();
    }

	return ret_val;
}

void dvbjapi_plugin::term()
{
    if (plugin_.is_loaded())
    {
        dvbjapi_unload_();

        plugin_.Unload();

        log_info(L"dvbjapi_plugin::term. Successfully unloaded plugin %1%") % plugin_path_.to_wstring();
    }
    dvbjapi_plugin_callback_ = NULL;
}

void dvbjapi_plugin::execute_command(const std::string& json_cmd)
{
    dvbjapi_receive_from_parent_((char*) json_cmd.c_str());
}

void dvbjapi_plugin::write_stream(const unsigned char* buf, size_t len)
{
    dvbjapi_pid_data_((unsigned char*)buf, len);
}

int STDCALL dvbjapi_plugin::send_to_parent_callback(char *value, void* user_param)
{
    dvbjapi_plugin* plugin = (dvbjapi_plugin*)user_param;
    plugin->dvbjapi_plugin_callback_(value, plugin->user_param_);

    return DVBJAPI_SUCCESS;
}

void dvbjapi_plugin::send_get_version_cmd()
{
    std::string get_version_cmd = "[ { \"CMD\":\"GET_VERSION\" } ]";
    execute_command(get_version_cmd);
}

