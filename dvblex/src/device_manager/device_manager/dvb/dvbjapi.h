/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

/*
typedef int _stdcall SendToParentCallback(char *value, void* user_param);  //typedef for SendToParent callback function. It takes json array string, zerro terminated as a parameter

int _stdcall Load(SendToParentCallback* callback); This function is called at the start. It tales parent callback as a parameter. The callback is valid until Unload function exits.
void _stdcall Unload();
int _stdcall ReceiveFromParent(char *value); // json array string, zerro terminated
void _stdcall PidData(unsigned char *data, int datalen); // unsigned char, [188 * x]
void _stdcall RawData(unsigned char *data, int datalen); // unsigned char, [188 * x]
*/

#if defined(_WIN32)
#define STDCALL __stdcall
#else
#define STDCALL __attribute__ ((stdcall))
#endif

#define DVBJAPI_SUCCESS     0

typedef int (STDCALL *dvbjapi_send_to_parent_callback_f)(char *value, void* user_param);  //typedef for SendToParent callback function. It takes json array string, zerro terminated as a parameter

typedef int (STDCALL *dvbjapi_load_f)(const char* module_path, dvbjapi_send_to_parent_callback_f callback, void* user_param); //This function is called at the start. It tales parent callback as a parameter. The callback is valid until Unload function exits.
typedef void (STDCALL *dvbjapi_unload_f)();
typedef int (STDCALL *dvbjapi_receive_from_parent_f)(char *value); // json array string, zerro terminated
typedef void (STDCALL *dvbjapi_pid_data_f)(unsigned char *data, int datalen); // unsigned char, [188 * x]
typedef void (STDCALL *dvbjapi_raw_data_f)(unsigned char *data, int datalen); // unsigned char, [188 * x]

#define DVBJAPI_LOAD_FN_NAME                "Load"
#define DVBJAPI_UNLOAD_FN_NAME              "Unload"
#define DVBJAPI_RCV_FROM_PARENT_FN_NAME     "ReceiveFromParent"
#define DVBJAPI_PID_DATA_FN_NAME            "PidData"
