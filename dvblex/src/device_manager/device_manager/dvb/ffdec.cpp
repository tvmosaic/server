/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <string>
#include "ffdec.h"
#include "ffdecsa/ffdecsa.h"


CTVSFFDecCSA::CTVSFFDecCSA()
{
}

CTVSFFDecCSA::~CTVSFFDecCSA()
{
}

bool CTVSFFDecCSA::Init()
{
	m_KeyStruct = (unsigned char*)dvblink_ffdcsa::get_key_struct();
    return true;
}

bool CTVSFFDecCSA::Term()
{
	dvblink_ffdcsa::free_key_struct(m_KeyStruct);
	return true;
}

void CTVSFFDecCSA::SetNewKey(unsigned char* key)
{
	dvblink_ffdcsa::set_control_words(m_KeyStruct, key, key + 8);
}

void CTVSFFDecCSA::Decrypt(unsigned char* buffer, int len)
{
    unsigned char* cluster[3];
    unsigned char* p;
    unsigned char* end = buffer+len;
    for(p=buffer; p<end; )
    {
        cluster[0]=p; cluster[1]=end;
        cluster[2]=NULL;
        p += 188*dvblink_ffdcsa::decrypt_packets((unsigned char*)m_KeyStruct, cluster);
    }
}
