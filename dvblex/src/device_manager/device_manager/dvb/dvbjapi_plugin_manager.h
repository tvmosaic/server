/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <map>
#include <boost/shared_ptr.hpp>
#include <dl_command_queue.h>
#include "plugin_manager_base.h"
#include "dvbjapi_plugin.h"

class dvbjapi_plugin_manager;

struct dvbjapi_plugin_desc
{
    std::wstring id;
    boost::shared_ptr<dvbjapi_plugin> plugin;
    dvbjapi_plugin_manager* plugin_man;
};

typedef boost::shared_ptr<dvbjapi_plugin_desc> dvbjapi_plugin_desc_ptr_t;

typedef std::map<std::wstring, dvbjapi_plugin_desc_ptr_t> dvbjapi_plugin_map_t;

class dvbjapi_plugin_manager : public tvs_plugin_manager_base
{
protected:
    enum commands_e {EC_PLUGIN_CMD = dvblink::engine::command_queue::post_command_offset + 1, EC_CHANGE_CHANNEL = 2};

    struct plugin_cmd_params
    {
        std::wstring plugin_id;
        std::string cmd;
    };

    struct change_channel_params
    {
        std::string channel_name;
        int nid;
        int tid;
        int sid;
        unsigned short pmt_pid;
        plugin_program_extra_info extra_info;
    };

public:
	dvbjapi_plugin_manager(const dvblink::filesystem_path_t& source_path, const std::wstring& plugin_dir);

	virtual bool init();
	virtual void term();
	
    virtual bool channel_changed(TProgramm* tuner_info, plugin_program_extra_info& extra_info);

    static bool is_valid_dvbjapi_plugin_dir(const dvblink::filesystem_path_t& parent_path, const std::wstring& dir_name);

protected:
	virtual void process_requested_packet(const unsigned char* buf, size_t len);
    static int dvbjapi_plugin_callback(char *value, void* user_param);
    void command_thread_func();
    void send_cmd_to_plugin(const std::string& json_cmd, const std::wstring& plugin_id);

    void convert_to_json_cmd(change_channel_params* params, std::string& json_cmd);
    void add_pids(const std::string& pids_to_add);
    void set_key(bool even_key, const std::string& key);

    void process_channel_change(change_channel_params* params);
    void process_plugin_cmd(plugin_cmd_params* params);

	dvblink::filesystem_path_t source_path_;
	std::wstring plugin_dir_;
    dvbjapi_plugin_map_t dvbjapi_plugin_map_;
    dvblink::engine::command_queue command_queue_;
	boost::thread* command_thread_;
	bool exit_command_thread_;
    change_channel_params current_channel_;
};
