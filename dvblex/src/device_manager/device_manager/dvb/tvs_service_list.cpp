/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <string.h>
#include <map>
#include <dl_logger.h>
#include "tvs_service_list.h"

using namespace dvblink::engine;
using namespace dvblink::logging;

//***************************************************************************************

CTVSSectionDescriptor::CTVSSectionDescriptor(unsigned char* section, int length) :
    m_Length(length)
{
	m_Section = new unsigned char[m_Length];
	memcpy(m_Section, section, m_Length);
}

//***************************************************************************************

CTVSServiceListDescriptorBase::CTVSServiceListDescriptorBase()
{
	m_MaxSectionNum = -1;
}

CTVSServiceListDescriptorBase::~CTVSServiceListDescriptorBase()
{
	ResetSectionList();
}

void CTVSServiceListDescriptorBase::ResetSectionList()
{
	TTVSSectionDescMap::iterator it = m_SectionDescMap.begin();
	while (it != m_SectionDescMap.end())
	{
		delete it->second.m_Section;
		++it;
	}
	m_SectionDescMap.clear();
}

bool CTVSServiceListDescriptorBase::AddSection(unsigned char* section, int length)
{
	//get data from the section
	unsigned short nid, tid;
	unsigned char version, cur_next;
	unsigned char cur_section_num, max_section_num;

	GetSectionIDs(section, length, nid, tid);
	GetSectionStats(section, length, version, cur_next);
	GetSectionNumbers(section, length, cur_section_num, max_section_num);

	if (m_MaxSectionNum == -1)
	{
		//Initialize version and maximum section number variable
		m_MaxSectionNum = max_section_num;
		m_Version = version;
		//Get TSID and NID
		m_NID = nid;
		m_TID = tid;
		//Add first section to the list
		m_SectionDescMap[cur_section_num] = CTVSSectionDescriptor(section, length);
	}
    else
	{
		 if (version != m_Version)
		 {
			//Version has been changed. Reset the contents
			m_Version = version;
			m_MaxSectionNum = max_section_num;
			m_NID = nid;
			m_TID = tid;
			ResetSectionList();
		 }
		if (m_SectionDescMap.find(cur_section_num) == m_SectionDescMap.end())
		{
			//Add section to the list if it does not exist yet
			m_SectionDescMap[cur_section_num] = CTVSSectionDescriptor(section, length);
		}
	}
	return true;
}

bool CTVSServiceListDescriptorBase::IsCompleted()
{
	if (m_MaxSectionNum == -1)
		return false;

	bool ret_val = true;

	for (int i=0; i<=m_MaxSectionNum; i++)
	{
		if (m_SectionDescMap.find(i) == m_SectionDescMap.end())
		{
			ret_val = false;
			break;
		}
	}

	return ret_val;
}

TSVC_CHANNEL_MEDIA_TYPE CTVSServiceListDescriptorBase::GetServiceTypeFromDVBType(unsigned char type)
{
	TSVC_CHANNEL_MEDIA_TYPE ret_val = TSVC_CMT_OTHER;

	switch (type)
	{
	case 0x02:
	case 0x0A:
		ret_val = TSVC_CMT_RADIO;
		break;
	case 0x01:
	case 0x11:
	case 0x16:
	case 0x19:
		ret_val = TSVC_CMT_TV;
		break;
	}

	return ret_val;
}

void CTVSServiceListDescriptorBase::GetServices(std::vector<TVSC_ChannelType>& services)
{
	services.clear();
	for (int i=0; i<=m_MaxSectionNum; i++)
	{
		if (m_SectionDescMap.find(i) != m_SectionDescMap.end())
		{
			GetServicesFromSection(m_SectionDescMap.find(i)->second.m_Section,
				m_SectionDescMap.find(i)->second.m_Length, services);

		}
        else
		{
			log_warning(L"CTVSChannelScanner::GetServices. Section %1% is missing from sections list") % i;
		}
	}
}

//*****************************************************************

CTVSServiceDVBListDescriptor::CTVSServiceDVBListDescriptor() :
	CTVSServiceListDescriptorBase()
{
}

void CTVSServiceDVBListDescriptor::GetSectionIDs(unsigned char* section, int length, unsigned short& nid, unsigned short& tid)
{
	ts_process_routines::GetSDTSectionIds(section, length, nid, tid);
}

void CTVSServiceDVBListDescriptor::GetSectionStats(unsigned char* section, int length, unsigned char& version, unsigned char& cur_next)
{
	version = ts_process_routines::GetSDTVersion(section, length);
	cur_next = ts_process_routines::GetSDTCurNextIndicator(section, length);
}

void CTVSServiceDVBListDescriptor::GetSectionNumbers(unsigned char* section, int length, unsigned char& cur_section_num, unsigned char& max_section_num)
{
	cur_section_num = ts_process_routines::GetSDTCurSectionNum(section, length);
	max_section_num = ts_process_routines::GetSDTMaxSectionNum(section, length);
}

void CTVSServiceDVBListDescriptor::GetServicesFromSection(unsigned char* section, int length, std::vector<TVSC_ChannelType>& services)
{
	std::vector<STSServiceInfoEx> sdt_services;
	if (ts_process_routines::GetSDTServicesEx(section, length, sdt_services))
	{
		for (unsigned int srv_idx = 0; srv_idx < sdt_services.size(); srv_idx++)
		{
			TVSC_ChannelType ch;
			SetChannelDefaults(ch);
			ch.encrypted = sdt_services[srv_idx].encrypted;
			ch.name = sdt_services[srv_idx].name;
			ch.nid = sdt_services[srv_idx].nid;
			ch.provider = sdt_services[srv_idx].provider;
			ch.sid = sdt_services[srv_idx].sid;
			ch.tid = sdt_services[srv_idx].tid;
			ch.type = GetServiceTypeFromDVBType(sdt_services[srv_idx].type);

			services.push_back(ch);
		}
	}
}

//*****************************************************************

CTVSServiceATSCListDescriptor::CTVSServiceATSCListDescriptor() :
	CTVSServiceListDescriptorBase()
{
}

void CTVSServiceATSCListDescriptor::GetSectionIDs(unsigned char* section, int length, unsigned short& nid, unsigned short& tid)
{
	nid = 0;
	ts_process_routines::GetTVCTSectionTSID(section, length, tid);
}

void CTVSServiceATSCListDescriptor::GetSectionStats(unsigned char* section, int length, unsigned char& version, unsigned char& cur_next)
{
	ts_process_routines::GetTVCTSectionStats(section, length, version, cur_next);
}

void CTVSServiceATSCListDescriptor::GetSectionNumbers(unsigned char* section, int length, unsigned char& cur_section_num, unsigned char& max_section_num)
{
	ts_process_routines::GetTVCTSectionNumbers(section, length, cur_section_num, max_section_num);
}

void CTVSServiceATSCListDescriptor::GetServicesFromSection(unsigned char* section, int length, std::vector<TVSC_ChannelType>& services)
{
	std::vector<STSTVCTServiceInfoEx> vct_services;
	if (ts_process_routines::GetTVCTServicesEx(section, length, vct_services))
	{
		for (unsigned int srv_idx = 0; srv_idx < vct_services.size(); srv_idx++)
		{
			TVSC_ChannelType ch;
			SetChannelDefaults(ch);
			ch.name = vct_services[srv_idx].name;
			ch.sid = vct_services[srv_idx].sid;
			ch.tid = vct_services[srv_idx].tid;
			ch.type = GetServiceTypeFromDVBType(vct_services[srv_idx].type);
			ch.mj_num = vct_services[srv_idx].mj_num;
			ch.mn_num = vct_services[srv_idx].mn_num;

			services.push_back(ch);
		}
	}
}

//********************************************************************************

CTVSNITScanner::CTVSNITScanner() :
	CTVSServiceListDescriptorBase()
{
}

void CTVSNITScanner::GetSectionIDs(unsigned char* section, int length, unsigned short& nid, unsigned short& tid)
{
	tid = 0;
    ts_process_routines::GetNetworkIDFromNIT(section, length, nid);
}

void CTVSNITScanner::GetSectionStats(unsigned char* section, int length, unsigned char& version, unsigned char& cur_next)
{
	ts_process_routines::GetNITSectionStats(section, length, version, cur_next);
}

void CTVSNITScanner::GetSectionNumbers(unsigned char* section, int length, unsigned char& cur_section_num, unsigned char& max_section_num)
{
	ts_process_routines::GetNITSectionNumbers(section, length, cur_section_num, max_section_num);
}

void CTVSNITScanner::GetServicesFromSection(unsigned char* section, int length, std::vector<TVSC_ChannelType>& services)
{
    services.clear();
}

void CTVSNITScanner::GetChannelNumbers(std::vector<SDVBTLCNDesc>& services)
{
	for (int i=0; i<=m_MaxSectionNum; i++)
	{
		if (m_SectionDescMap.find(i) != m_SectionDescMap.end())
		{
            std::vector<SDVBTLCNDesc> lcn_services;
			ts_process_routines::GetLCNFromNIT(m_SectionDescMap.find(i)->second.m_Section,
				m_SectionDescMap.find(i)->second.m_Length, lcn_services);

            for (unsigned int j=0; j<lcn_services.size(); j++)
                services.push_back(lcn_services[j]);

		}
        else
		{
			log_warning(L"CTVSChannelScanner::GetChannelNumbers. Section %1% is missing from sections list") % i;
		}
	}
}
