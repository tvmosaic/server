/*

Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.

*/

#pragma once

#define GET_INTERNAL_PARALLELISM	1
#define GET_SUGGESTED_CLUSTER_SIZE	2
#define SET_CONTROL_WORDS		3	
#define SET_EVEN_CONTROL_WORD		4
#define SET_ODD_CONTROL_WORD		5
#define DECRYPT_PACKETS			6

typedef struct _init_block 
{
	unsigned int cmd;			// 4 bytes
	unsigned int rc;			// 4 bytes
	unsigned int internal_parallelism;	// 4 bytes
	unsigned int suggested_cluster_size;	// 4 bytes
	unsigned int ts_pkts_per_cluster;	// 4 bytes number of ts packets to be decrypted in an ffdecsa cluster
	unsigned char even[8]; 			// 8 bytes
	unsigned char odd[8];			// 8 bytes
	unsigned int buffer;			// 4 bytes pointer to buffer holding crypted ts packets
	unsigned int buffer_size;		// 4 bytes size of buffer
	unsigned char pad[84];			// pad to a full cache line (128 bytes)
} init_block_t;
