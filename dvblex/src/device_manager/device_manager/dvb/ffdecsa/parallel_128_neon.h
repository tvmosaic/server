/* FFdecsa -- fast decsa algorithm
 *
 * Copyright (C) 2007 Dark Avenger
 *               2003-2004  fatih89r
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#pragma once

#include <arm_neon.h>

#define MEMALIGN __attribute__((aligned(16)))

union __u128 {
    unsigned int u[4];
    uint32x4_t v;
};

static const union __u128 ff0 = {{0x00000000U, 0x00000000U, 0x00000000U, 0x00000000U}};
static const union __u128 ff1 = {{0xffffffffU, 0xffffffffU, 0xffffffffU, 0xffffffffU}};

typedef uint32x4_t group;
#define GROUP_PARALLELISM 128
#define FF0()      ff0.v
#define FF1()      ff1.v
#define FFAND(a,b) vandq_u32((a),(b))
#define FFOR(a,b)  vorrq_u32((a),(b))
#define FFXOR(a,b) veorq_u32((a),(b))
#define FFNOT(a)   vmvnq_u32((a))

/* 64 rows of 64 bits */

static const union __u128 ff29 = {{0x29292929U, 0x29292929U, 0x29292929U, 0x29292929U}};
static const union __u128 ff02 = {{0x02020202U, 0x02020202U, 0x02020202U, 0x02020202U}};
static const union __u128 ff04 = {{0x04040404U, 0x04040404U, 0x04040404U, 0x04040404U}};
static const union __u128 ff10 = {{0x10101010U, 0x10101010U, 0x10101010U, 0x10101010U}};
static const union __u128 ff40 = {{0x40404040U, 0x40404040U, 0x40404040U, 0x40404040U}};
static const union __u128 ff80 = {{0x80808080U, 0x80808080U, 0x80808080U, 0x80808080U}};

typedef uint32x4_t batch;
#define BYTES_PER_BATCH 16
#define B_FFAND(a,b) FFAND((a),(b))
#define B_FFOR(a,b)  FFOR((a),(b))
#define B_FFXOR(a,b) FFXOR((a),(b))
#define B_FFN_ALL_29() ff29.v
#define B_FFN_ALL_02() ff02.v
#define B_FFN_ALL_04() ff04.v
#define B_FFN_ALL_10() ff10.v
#define B_FFN_ALL_40() ff40.v
#define B_FFN_ALL_80() ff80.v


#define B_FFSH8L(a, n) \
({ \
	uint32x4_t ret; \
	if ((n) <= 0) {\
		ret = a; \
	} \
	else if ((n) > 31) { \
		ret = vdupq_n_u32(0); \
	} \
	else { \
		ret = vshlq_n_u32((a), (n)); \
	} \
	ret; \
})

#define  B_FFSH8R(a, n) \
({ \
	uint32x4_t ret; \
	if ((n) <= 0) { \
		ret = a; \
	} \
	else if ((n)> 31) { \
		ret = vdupq_n_u32(0); \
	} \
	else { \
		ret = vshrq_n_u32((a), (n)); \
	} \
	ret; \
})

#define M_EMPTY() 

#undef BEST_SPAN
#define BEST_SPAN            16

#undef XOR_8_BY
static inline void XOR_8_BY(unsigned char *d, unsigned char *s1, unsigned char *s2)
{
	uint32x2_t vs1 = vld1_u32((uint32_t*)s1);
	uint32x2_t vs2 = vld1_u32((uint32_t*)s2);
	vs1 = veor_u32(vs1, vs2);
	vst1_u32((uint32_t*)d, vs1);
}

#undef XOREQ_8_BY
#define XOREQ_8_BY(d,s)      XOR_8_BY(d, d, s)

#undef COPY_8_BY
static inline void COPY_8_BY(unsigned char *d, unsigned char *s1)
{
	uint32x2_t vs1 = vld1_u32((uint32_t*)s1);
	vst1_u32((uint32_t*)d, vs1);
}

#undef XOR_BEST_BY
static inline void XOR_BEST_BY(unsigned char *d, unsigned char *s1, unsigned char *s2)
{
	uint32x4_t vs1 = vld1q_u32((uint32_t*)s1);
	uint32x4_t vs2 = vld1q_u32((uint32_t*)s2);
	vs1 = veorq_u32(vs1, vs2);
	vst1q_u32((uint32_t*)d, vs1);
}

#include "fftable.h"
