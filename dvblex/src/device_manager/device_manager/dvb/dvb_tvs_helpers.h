/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <drivers/tuner_factory.h>
#include <drivers/deviceapi.h>
#include <dl_channel_info.h>
#include "tvs_core_def.h"

namespace dvblex { 

dvblink::DL_E_TUNER_TYPES tvs_tuner_type_from_dvblex(dvblex::source_type_e type);
dvblex::source_type_e dvblex_tuner_type_from_tvs(dvblink::DL_E_TUNER_TYPES type);

bool fill_lnb_values_for_type(const std::string& type, TVSC_LNBType& lnb, const std::string& lof1, const std::string& lof2, const std::string& lofsw);
bool fill_diseqc_values_for_type(const std::string& type, TVSC_DiseqcType& diseqc);
void tvs_channel_to_dvblex(const TVSC_ChannelType& tvs_channel, dvblex::device_channel_t& channel);

std::string get_channel_tuning_string_from_params(dvblink::DL_E_TUNER_TYPES tuner_type, const concise_program_info_t& program_info);

dvblink::DL_E_TUNER_TYPES get_tuner_type_from_tuning_string(const std::string& tuning_string);

bool get_channel_tuning_params_from_string(const concise_channel_tune_info_t& tune_info, dvblink::DL_E_TUNER_TYPES& tuner_type, concise_program_info_t& program_info);

}
