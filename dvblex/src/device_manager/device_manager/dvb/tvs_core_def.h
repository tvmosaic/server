/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef _TVSOURCECOREDEF_H_
#define _TVSOURCECOREDEF_H_

#include <wchar.h>
#include <string>
#include <vector>
#include <map>
#include <dl_ts_info.h>
#include <drivers/deviceapi.h>

#if defined(__ANDROID__)

#undef TVS_PLUGINS_SUPPORTED

#else

#define TVS_PLUGINS_SUPPORTED

#endif

//Device type

struct TVSC_DeviceType
{
	int TypeID;
	std::wstring TypeName;
};

extern const TVSC_DeviceType cDeviceTypeDefaults[];

typedef std::vector<TVSC_DeviceType> TVSC_DeviceTypeList;

//Device options
struct TVSC_DeviceOptions
{
    bool StopStreamOnIdle;
    bool retuneOnFailure;
    unsigned long customDiseqcDelay;
    bool repeatDiseqc;
    bool wait_for_lock_on_tune;
	int num_streams_to_decrypt;
	int num_simul_fta_streams;
	bool enable_multichannel;
};

//Device info
struct TVSC_DeviceInfo
{
	std::wstring driverName;    //name of the device driver file
	int deviceIndex;
	int frontend_idx;
	std::wstring deviceName;	//human readable device name
	std::wstring devicePath;	//system specific device path
	int autodetected;			//indicates whether this device was autodetected vs. added by user
	int deviceType;				//device type - TUNERTYPE_xxx

    TVSC_DeviceOptions options;

    TVSC_DeviceInfo() {Reset();}
	void Reset()
    {
        driverName = L""; deviceIndex = 0; frontend_idx = 0; deviceName = L""; devicePath = L"";
        autodetected = 0; deviceType = 0; options.retuneOnFailure = false; options.StopStreamOnIdle = true;
        options.customDiseqcDelay = 15; options.repeatDiseqc = false; options.wait_for_lock_on_tune = false; options.num_streams_to_decrypt = 1;
        options.num_simul_fta_streams = -1;
        options.enable_multichannel = true;
    };
	bool IsValid(){return (driverName.size() > 0);}
    bool IsSameTunerDevice(TVSC_DeviceInfo& other_device);
};

typedef std::vector<TVSC_DeviceInfo> TVSC_DeviceList;

//Overrides
struct TVSC_TunerTypeOverride
{
	std::wstring driverName;	//name of the device driver file
	int deviceIndex;			//device index
	int frontend_idx;           //frontend index
	int deviceType;				//device type - TUNERTYPE_xxx
    std::wstring devicePath;    //device HW path (may be empty! only works for BDA devices)

    bool IsTheSameDevice(TVSC_DeviceInfo& device);
};

typedef std::vector<TVSC_TunerTypeOverride> TVSC_TunerTypeOverrideList;

enum ETVS_CHANNEL_SOURCE_TYPE
{
	ETVS_CCT_TRANSPONDER_FILE,
	ETVS_CCT_ICHDB,
	ETVS_CCT_CHANNEL_FILE,
	ETVS_CCT_OTHER,
};

//Channel source: satellite, cable or terrestrial provider
//either local or taken from channelscope
struct TVSC_ChannelSource
{
	std::wstring sourceID; //ini file name in case of local or ID in channelscope database 
	std::wstring sourceName; //name of satellite or cable or terrestrial provider
	ETVS_CHANNEL_SOURCE_TYPE type_;
};

typedef std::vector<TVSC_ChannelSource> TVSC_ChannelSourceList;

//LNB types
struct TVSC_LNBType
{
	std::wstring typeID;	//LNB type ID string
	std::wstring typeName;	//LNB type human readable name
	unsigned long LOF1;		//LOF 1
	unsigned long LOF2;		//LOF 2
	unsigned long LOFSW;	//LOF sw
};

extern const TVSC_LNBType cLNBTypeDefaults[];

typedef std::vector<TVSC_LNBType> TVSC_LNBTypeList;

bool IsBandstackedLNB(const wchar_t* lnbtype);
bool IsBandstackedFSSLNB(const wchar_t* lnbtype);

//Diseqc type
struct TVSC_DiseqcType
{
	std::wstring typeID;	//diseqc type ID string
	std::wstring typeName;	//diseqc type human readable name
};

extern const TVSC_DiseqcType cDiseqcTypeDefaults[];

typedef std::vector<TVSC_DiseqcType> TVSC_DiseqcTypeList;

//custom diseqc string
struct TVSC_DiseqcCustomString
{
	std::wstring rawdata_string;
	std::wstring name;
};

//unicable (Single Cable Distribution) structures

enum ETVS_UNICABLE_SLOT_POS
{
    ETVS_USP_POS_A,
    ETVS_USP_POS_B
};

struct TVSC_UnicableSlotInfo
{
    std::wstring id;
    int slot_number;
    ETVS_UNICABLE_SLOT_POS position;
    unsigned long frequency;
};

typedef std::vector<TVSC_UnicableSlotInfo> TVSC_UnicableSlotList;

//Headend
struct TVSC_HeadendType
{
	std::wstring headendID;	//GUID of the headend instance
	TVSC_ChannelSource channelSource; //channel source
	TVSC_LNBType LNB;	//LNB (only aplicable for satellite headend)
	TVSC_DiseqcType diseqc;	//Diseqc type (only applicable for satellite headend)
};

typedef std::vector<TVSC_HeadendType> TVSC_HeadendList;

//Transponder
struct TVSC_TransponderType
{
	long transponderID;	//number of transponder in the ini file
	std::wstring trName; //transponder name
	std::wstring data; //actual transponder data. Depends on the transponder type (sat, cable, terrestrial etc.)
};

typedef std::vector<TVSC_TransponderType> TVSC_TransponderTypeList;

enum TSVC_CHANNEL_MEDIA_TYPE
{
	TSVC_CMT_TV = 1,
	TSVC_CMT_RADIO = 2,
	TSVC_CMT_OTHER = 3
};

//Channel
class TVSC_ChannelType
{
public:
    TVSC_ChannelType()
    {
        auto_sync = false;
        mj_num = -1;
        mn_num = 0;
    }

	std::wstring id;
	//tuning params
	unsigned char diseqc;
    std::wstring diseqc_rawdata;
	dvblink::TTransponderInfo transponder_info;
	//logical params
	unsigned short nid;
	unsigned short tid;
	unsigned short sid;
	unsigned char type;
	unsigned char encrypted;
	std::wstring name;
	std::wstring provider;
	int mj_num;
	int mn_num;
    //CA descriptors
    std::vector<dvblink::engine::STSCADescriptorInfo> ca_descriptors;
    unsigned short ecm_pid;
    bool auto_sync;
};

void SetChannelDefaults(TVSC_ChannelType& chinfo);
void SetTransponderDefaults(dvblink::TTransponderInfo& trinfo);

struct TVSC_ServiceInfo
{
	unsigned short nid;	/*!< Network ID */
	unsigned short tid;	/*!< Transport stream ID */
	unsigned short sid;	/*!< Service ID */
    int ch_num;
};

struct TVSC_TransponderScanData
{
    TVSC_TransponderType transponder;
    bool bFilterServices;
    std::vector<TVSC_ServiceInfo> expectedServices;
};

typedef std::vector<TVSC_TransponderScanData> TVSC_TransponderScanList;

//network scan results
struct TVSC_NetworkScanData
{
    unsigned short nid;
    std::wstring name;
    TVSC_TransponderScanList transponders;
};

//already installed TVSources
struct TVSC_InstalledTVSource
{
    std::wstring id;
    std::wstring name;
};

//inband EPG scan options
enum ETVSC_EPGScanOptionsEnum
{
    ETVSC_ESO_NONE,
    ETVSC_ESO_MAPPED,
    ETVSC_ESO_KNOWN,
    ETVSC_ESO_ALL
};

struct STVSC_EPGScanTransponder
{
    std::wstring headendId;
    long transponderId;
};

struct STVSC_EPGScanOptions
{
    STVSC_EPGScanOptions(){scanMode = ETVSC_ESO_MAPPED; IsEnabled = 1; language = L"eng"; showNoNameChannels = 0; use_event_id_ = 1;}
    ETVSC_EPGScanOptionsEnum scanMode;
    int IsEnabled;
    std::vector<STVSC_EPGScanTransponder> extraTransponders;
	std::wstring language;
	int showNoNameChannels;
	int use_event_id_;
};

struct STVSC_ExclusionOptions
{
	void reset(){nid = tid = sid = 0; bNoTeletext = false;}
	unsigned short nid;
	unsigned short tid;
	unsigned short sid;
	bool bNoTeletext;
};

struct fast_scan_params
{
    std::wstring name;
    std::wstring frequency;
    std::wstring symbol_rate;
    std::wstring modulation;
    std::wstring polarisation;
    std::wstring fec;
    std::wstring dvbs_type;
};

struct active_transponder_data
{
	unsigned char diseqc;
    std::wstring diseqc_rawdata;
	dvblink::TTransponderInfo transponder_info;
    std::wstring headend_id;
};

struct transponder_tuning_info_t
{
    transponder_tuning_info_t() :
        diseqc(dvblink::DISEQC_NONE)
    {
        memset(&transponder_info, 0, sizeof(transponder_info));
        transponder_info.dwSize = sizeof(transponder_info);
    }

	unsigned char diseqc;
    std::wstring diseqc_rawdata;
	dvblink::TTransponderInfo transponder_info;
};

struct concise_program_info_t
{
    concise_program_info_t() :
        encrypted_(false), tid_(0), nid_(0), sid_(0)
    {
    }

    transponder_tuning_info_t tuning_info_;
    bool encrypted_;
    unsigned short tid_;
    unsigned short nid_;
    unsigned short sid_;
};

typedef std::vector<fast_scan_params> fast_scan_params_list_t;

typedef std::map<std::wstring, STVSC_EPGScanOptions> TTVSC_EPGScanOptionsMap;

//list of channels with exclusions
typedef std::vector<STVSC_ExclusionOptions> STVSC_ExclusionOptionsList;

//list of installed tvsources
typedef std::vector<TVSC_InstalledTVSource> TVSC_InstalledTVSourceList;

//network scan information
typedef std::vector<TVSC_NetworkScanData> TVSC_NetworkScanList;

//list of channels by their ID
typedef std::vector<std::wstring> TVSC_ChannelIDList;

//Map of channel ID string to the channel info structure
typedef std::map<std::wstring, TVSC_ChannelType> TVSC_ChannelMap;

//Map of mapped frequency to channel ID string
typedef std::map<unsigned long, std::wstring> TVSC_ChannelFreqMap;

//List of mapping frequencies
typedef std::vector<unsigned long> TVSC_FreqList;

//Channel map of headend to its channels
typedef std::map<std::wstring, std::vector<std::wstring> > TVSC_HeadendChannelMap;

//channel id
const std::wstring create_channel_id(unsigned long freq, unsigned short nid, unsigned short tid, unsigned short sid, dvblink::DL_E_TUNER_TYPES device_type);

//this function returns human-readable transponder description string
void MakeTransponderString(int device_type, dvblink::TTransponderInfo& trinfo, std::wstring& resstr);
void GetCASystemName(unsigned short CASysId, std::wstring& sys_name);
void GetDeviceTypeName(int type, std::wstring& type_name);
void GetTransponderHash(int device_type, dvblink::TTransponderInfo& transponder_info, const wchar_t* diseqc_rawdata, unsigned char diseqc, std::wstring& tr_hash);

///////////////////////////////////////////////////////////////////////////////
#endif //_TVSOURCECOREDEF_H_
