/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <dvblex_net_lib/dl_network_helper.h>
#include <dl_utils.h>
#include <dl_logger.h>
#include "tvs_tr_parser.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

long CTVSTransponderParser::m_TransponderIDFeed = 1;

CTVSTransponderParser::CTVSTransponderParser()
{
}

CTVSTransponderParser::~CTVSTransponderParser()
{
}

std::string CTVSTransponderParser::make_dvbc_tuning_string(unsigned long freq, unsigned long sr, dvblink::DL_E_MOD_DVBC qam)
{
    std::string qam_value;
    switch (qam)
    {
    case MOD_DVBC_QAM_4:
        qam_value = "QAM4";
        break;
    case MOD_DVBC_QAM_16:
        qam_value = "QAM16";
        break;
    case MOD_DVBC_QAM_32:
        qam_value = "QAM32";
        break;
    case MOD_DVBC_QAM_64:
        qam_value = "QAM64";
        break;
    case MOD_DVBC_QAM_128:
        qam_value = "QAM128";
        break;
    case MOD_DVBC_QAM_256:
        qam_value = "QAM256";
        break;
    }
    std::stringstream buf;
    buf << freq << ",," << sr << ",," << qam_value;

    return buf.str();
}

int CTVSTransponderParser::ParseTransponderData(const TVSC_UnicableSlotList& unicable_slots, dvblink::DL_E_TUNER_TYPES device_type, TVSC_HeadendType& headend, const std::wstring& tr_data, 
                                                dvblink::TTransponderInfo* transponder, unsigned char* diseqc_value, std::wstring& rawdata_string)
{
    SetTransponderDefaults(*transponder);

    bool bParsedOk = false;
    *diseqc_value = dvblink::DISEQC_NONE;

    //parse the contents of transponder string
    std::vector<std::wstring> paramStrArray;
    if (ParseTransponderString(tr_data.c_str(), paramStrArray) > 0)
    {
        bParsedOk = true;
        //parameter order: frequency, polarization, symbol rate, fec, modulation
        for (unsigned int i=0; i<6 && bParsedOk; i++)
        {
            switch (i)
            {
                //frequency
            case 0:
                {
                    string_conv::apply(paramStrArray[i].c_str(), transponder->dwFreq, 0);  //already in KHz except for satellite, for ATSC it is a physical channel number
                    if (device_type == dvblink::TUNERTYPE_DVBS)
                        transponder->dwFreq *= 1000;//should be in KHz
                    if (transponder->dwFreq <= 0)
                    {
                        bParsedOk = false;
                        log_error(L"CTVSStreamSource::ParseTransponderData. Invalid frequency value %1%") % paramStrArray[i];
                    }
                    //to fill in correct LOF value we have to know polarization in some cases. We will do it in the next step
                }
                break;
                //polarization
            case 1:
                //ignore this value for everything except DVB-S
                if (device_type == dvblink::TUNERTYPE_DVBS &&
                    paramStrArray.size() > 1)
                {
                    if (boost::iequals(paramStrArray[i].c_str(), L"V") ||
                        boost::iequals(paramStrArray[i].c_str(), L"R"))
                        transponder->Pol = dvblink::POL_VERTICAL;
                    else
                        if (boost::iequals(paramStrArray[i].c_str(), L"H") ||
                            boost::iequals(paramStrArray[i].c_str(), L"L"))
                            transponder->Pol = dvblink::POL_HORIZONTAL;
                        else
                        {
                            bParsedOk = false;
                            log_error(L"CTVSStreamSource::ParseTransponderData. Invalid polarization value %1%") % paramStrArray[i];
                            break;
                        }
                        //Calculate LOF values
                        //linear universal LNB
                        if (boost::iequals(headend.LNB.typeID.c_str(), L"KuLinearUniversal"))
                        {
                            transponder->dwLOF = (((unsigned long)transponder->dwFreq) > headend.LNB.LOFSW) ? headend.LNB.LOF2 : headend.LNB.LOF1;
                            transponder->dwLnbKHz = (((unsigned long)transponder->dwFreq) > headend.LNB.LOFSW) ? dvblink::LNB_SELECTION_22 : dvblink::LNB_SELECTION_0;
                            transponder->LOF1 = headend.LNB.LOF1;
                            transponder->LOF2 = headend.LNB.LOF2;
                            transponder->LOFSW = headend.LNB.LOFSW;
                        }
                        //Ku-circular, C-Band and NA Legacy
                        if (boost::iequals(headend.LNB.typeID.c_str(), L"KuCircular") ||
                            boost::iequals(headend.LNB.typeID.c_str(), L"CBand") ||
                            boost::iequals(headend.LNB.typeID.c_str(), L"NALegacy"))
                        {
                            transponder->dwLOF = headend.LNB.LOF1;
                            transponder->dwLnbKHz = dvblink::LNB_SELECTION_0;
                            transponder->LOF1 = transponder->LOF2 = transponder->LOFSW = transponder->dwLOF;
                        }
                        //bandstacked dishes
                        if (IsBandstackedLNB(headend.LNB.typeID.c_str()))
                        {
                            transponder->dwLnbKHz = dvblink::LNB_SELECTION_0;
                            if (transponder->Pol == dvblink::POL_HORIZONTAL)
                            {
                                //update frequency value and polarization for bandstacked dishes
                                if (IsBandstackedFSSLNB(headend.LNB.typeID.c_str()))
                                {
                                    //Linear (FSS)
                                    transponder->dwFreq = 24600000 - transponder->dwFreq;
                                }
                                else
                                {
                                    //Circular (DSS) and C-band
                                    transponder->dwFreq = 25600000 - transponder->dwFreq;
                                }
                                transponder->Pol = dvblink::POL_VERTICAL;
                                transponder->dwLOF = headend.LNB.LOF1;
                                transponder->LOF1 = transponder->LOF2 = transponder->LOFSW = transponder->dwLOF;
                            }
                            else
                            {
                                transponder->dwLOF = headend.LNB.LOF1;
                                transponder->LOF1 = transponder->LOF2 = transponder->LOFSW = transponder->dwLOF;
                            }
                        }
                }
                break;
                //symbol rate
            case 2:
                //only applicable to DVB-S, DVB-C
                if (device_type == dvblink::TUNERTYPE_DVBS ||
                    device_type == dvblink::TUNERTYPE_DVBC)
                {
                    if (paramStrArray.size() > 2)
                    {
                        string_conv::apply(paramStrArray[2].c_str(), transponder->dwSr, 0);
                        transponder->dwSr *= 1000;
                        if (transponder->dwSr <= 0)
                        {
                            bParsedOk = false;
                            log_error(L"CTVSStreamSource::ParseTransponderData. Invalid symbol rate value %1%") % paramStrArray[2];
                        }
                    }
                    else
                    {
                        bParsedOk = false;
                        log_error(L"CTVSStreamSource::ParseTransponderData. Symbol rate is not in the partameter list");
                    }
                }
                break;
                //fec
            case 3:
                //only directly applicable to DVB-S
                if (device_type == dvblink::TUNERTYPE_DVBS)
                {
                    transponder->dwFec = dvblink::FEC_AUTO; //default
                    if (paramStrArray.size() > 3)
                    {
                        if (paramStrArray[3].length() > 0)
                        {
                            transponder->dwFec = 0xFF;
                            const wchar_t* fec = paramStrArray[3].c_str();
                            if (boost::iequals(fec, L"12"))
                                transponder->dwFec = dvblink::FEC_1_2;
                            if (boost::iequals(fec, L"13"))
                                transponder->dwFec = dvblink::FEC_1_3;
                            if (boost::iequals(fec, L"14"))
                                transponder->dwFec = dvblink::FEC_1_4;
                            if (boost::iequals(fec, L"23"))
                                transponder->dwFec = dvblink::FEC_2_3;
                            if (boost::iequals(fec, L"25"))
                                transponder->dwFec = dvblink::FEC_2_5;
                            if (boost::iequals(fec, L"34"))
                                transponder->dwFec = dvblink::FEC_3_4;
                            if (boost::iequals(fec, L"35"))
                                transponder->dwFec = dvblink::FEC_3_5;
                            if (boost::iequals(fec, L"45"))
                                transponder->dwFec = dvblink::FEC_4_5;
                            if (boost::iequals(fec, L"56"))
                                transponder->dwFec = dvblink::FEC_5_6;
                            if (boost::iequals(fec, L"78"))
                                transponder->dwFec = dvblink::FEC_7_8;
                            if (boost::iequals(fec, L"67"))
                                transponder->dwFec = dvblink::FEC_6_7;
                            if (boost::iequals(fec, L"511"))
                                transponder->dwFec = dvblink::FEC_5_11;
                            if (boost::iequals(fec, L"89"))
                                transponder->dwFec = dvblink::FEC_8_9;
                            if (boost::iequals(fec, L"9a") ||
                                boost::iequals(fec, L"910") ||
                                boost::iequals(fec, L"91"))
                                transponder->dwFec = dvblink::FEC_9_10;
                            if (boost::iequals(fec, L"Auto"))
                                transponder->dwFec = dvblink::FEC_AUTO;
                            if (transponder->dwFec == 0xFF)
                            {
                                bParsedOk = false;
                                log_error(L"CTVSStreamSource::ParseTransponderData. Invalid FEC value %1%") % fec;
                            }
                        }
                    }
                }
                //also used to override automatic 7 vs. 8 MHz bandwidth for DVB-T
                if (device_type == dvblink::TUNERTYPE_DVBT)
                {
                    if (transponder->dwFreq < 474000)
                        transponder->dwLOF = 7;
                    else
                        transponder->dwLOF = 8;
                    if (paramStrArray.size() > 3 && paramStrArray[3].length() > 0)
                    {
                        const wchar_t* bnd = paramStrArray[3].c_str();
                        if (boost::iequals(bnd, L"6"))
                            transponder->dwLOF = 6;
                        if (boost::iequals(bnd, L"7"))
                            transponder->dwLOF = 7;
                        if (boost::iequals(bnd, L"8"))
                            transponder->dwLOF = 8;
                        if (boost::iequals(bnd, L"10"))
                            transponder->dwLOF = 10;
                    }
                }
                break;
                //modulation
            case 4:
                {
                    const wchar_t* mod_str = L"";
                    if (paramStrArray.size() > 4)
                        mod_str = paramStrArray[4].c_str();
                    switch (device_type)
                    {
                    case dvblink::TUNERTYPE_DVBC:
                    case dvblink::TUNERTYPE_CLEARQAM:
                        {
                            transponder->dwLOF = dvblink::MOD_DVBC_QAM_64;
                            if (wcslen(mod_str) > 0)
                            {
                                if (boost::iequals(mod_str, L"QAM4"))
                                    transponder->dwLOF = dvblink::MOD_DVBC_QAM_4;
                                if (boost::iequals(mod_str, L"QAM16"))
                                    transponder->dwLOF = dvblink::MOD_DVBC_QAM_16;
                                if (boost::iequals(mod_str, L"QAM32"))
                                    transponder->dwLOF = dvblink::MOD_DVBC_QAM_32;
                                if (boost::iequals(mod_str, L"QAM64"))
                                    transponder->dwLOF = dvblink::MOD_DVBC_QAM_64;
                                if (boost::iequals(mod_str, L"QAM128"))
                                    transponder->dwLOF = dvblink::MOD_DVBC_QAM_128;
                                if (boost::iequals(mod_str, L"QAM256"))
                                    transponder->dwLOF = dvblink::MOD_DVBC_QAM_256;
                            }
                        }
                        break;
                    case dvblink::TUNERTYPE_DVBS:
                        {
                            transponder->dwModulation = dvblink::MOD_DVBS_QPSK;
                            if (wcslen(mod_str) > 0)
                            {
                                if (boost::iequals(mod_str, L"DVB-S"))
                                    transponder->dwModulation = dvblink::MOD_DVBS_QPSK;
                                if (boost::iequals(mod_str, L"S2") || boost::iequals(mod_str, L"DVB-S2"))
                                    transponder->dwModulation = dvblink::MOD_DVBS_8PSK;
                                if (boost::iequals(mod_str, L"NBC-QPSK"))
                                    transponder->dwModulation = dvblink::MOD_DVBS_NBC_QPSK;
                                if (boost::iequals(mod_str, L"Turbo-8PSK"))
                                    transponder->dwModulation = dvblink::MOD_TURBO_8PSK;
                                if (boost::iequals(mod_str, L"Turbo-QPSK"))
                                    transponder->dwModulation = dvblink::MOD_TURBO_QPSK;
                            }
                        }
                        break;
                    default:
                    	break;
                    }
                }
                break;
                //extra (satellite / terrestrial) modulation
            case 5:
                {
                    const wchar_t* mod_str = L"";
                    if (paramStrArray.size() > 5)
                        mod_str = paramStrArray[5].c_str();
                    if (device_type == dvblink::TUNERTYPE_DVBT)
                    {
                        if (wcslen(mod_str) > 0)
                        {
                            string_conv::apply(mod_str, transponder->plp_id, 0);
                            //transponder->plp_id is (value + 1), 0 is reserved to indicate dvb-t mode
                            transponder->plp_id += 1;
                        }
                    }
                    if (device_type == dvblink::TUNERTYPE_DVBS)
                    {
                        if (wcslen(mod_str) > 0)
                        {
                            if (boost::iequals(mod_str, L"8PSK"))
							{
                                //if previous parameter was "dvb-s" - do not change modulation
                                if (transponder->dwModulation != dvblink::MOD_DVBS_QPSK)
                                    transponder->dwModulation = dvblink::MOD_DVBS_8PSK; //dvb-s2 transponder: 8psk modulation
							}
                            if (boost::iequals(mod_str, L"QPSK"))
							{
								if (transponder->dwModulation == dvblink::MOD_DVBS_8PSK)
									transponder->dwModulation = dvblink::MOD_DVBS_NBC_QPSK; //dvb-s2 transponder: nbc-qpsk modulation
							}
                            if (boost::iequals(mod_str, L"16APSK"))
							{
								if (transponder->dwModulation == dvblink::MOD_DVBS_8PSK)
									transponder->dwModulation = dvblink::MOD_DVBS_16APSK; //dvb-s2 transponder: 16qpsk modulation
							}
                            if (boost::iequals(mod_str, L"32APSK"))
							{
								if (transponder->dwModulation == dvblink::MOD_DVBS_8PSK)
									transponder->dwModulation = dvblink::MOD_DVBS_32APSK; //dvb-s2 transponder: 32qpsk modulation
							}
                        }
                    }
                }
                break;
            default:
                break;
            }
        }
    
        //Set diseqc value
        if (device_type == dvblink::TUNERTYPE_DVBS && !GetDiseqcValueFromType(unicable_slots, headend, transponder, diseqc_value, rawdata_string))
            bParsedOk = false;

    } else
    {
        log_error(L"CTVSStreamSource::ParseTransponderData. Transponder string contains no parameters: %1%") % paramStrArray[0];
    }
    return bParsedOk;
}

void Trim(std::wstring& str)
{
	// Whitespace characters
	wchar_t whspc[] = L" \t\r\n\v\f";

	// Whack off first part
	unsigned int pos = str.find_first_not_of( whspc );
	if( pos != std::wstring::npos )
	str.replace( 0, pos, L"" );

	// Whack off trailing stuff
	pos = str.find_last_not_of( whspc );
	if( pos != std::wstring::npos )
	str.replace( pos + 1, str.length() - pos, L"" );
}

int CTVSTransponderParser::ParseTransponderString(const wchar_t* tr_str, std::vector<std::wstring>& paramStrArray)
{
	paramStrArray.clear();

	wchar_t* trstr_copy = wcsdup(tr_str);

	size_t length = wcslen(trstr_copy);
	wchar_t* cur_str = trstr_copy;
	for (unsigned int i=0; i<length; i++)
	{
		if (trstr_copy[i] == L',')
		{
			trstr_copy[i] = L'\0';
			std::wstring s = cur_str;
			Trim(s);
			paramStrArray.push_back(s);
			cur_str = trstr_copy + i + 1;
		}
	}
	//process last token (if any)
	if (wcslen(cur_str) > 0)
	{
		std::wstring s = cur_str;
		Trim(s);
		paramStrArray.push_back(s);
	}

	free(trstr_copy);

	return paramStrArray.size();
}


bool CTVSTransponderParser::GetDiseqcValueFromType(const TVSC_UnicableSlotList& unicable_slots, TVSC_HeadendType& headend, dvblink::TTransponderInfo* transponder, unsigned char* diseqc_value, std::wstring& rawdata_string)
{
	bool ret_val = true;
	*diseqc_value = 0xFF;
    rawdata_string = L"";

    const wchar_t* diseqc_type = headend.diseqc.typeID.c_str();

	if (boost::iequals(diseqc_type, L"None"))
		*diseqc_value = dvblink::DISEQC_NONE;
	if (boost::iequals(diseqc_type, L"SimpleA"))
		*diseqc_value = dvblink::DISEQC_SIMPLE_A;
	if (boost::iequals(diseqc_type, L"SimpleB"))
		*diseqc_value = dvblink::DISEQC_SIMPLE_B;
	if (boost::iequals(diseqc_type, L"Level1AA"))
		*diseqc_value = dvblink::DISEQC_LEVEL_1_A_A;
	if (boost::iequals(diseqc_type, L"Level1BA"))
		*diseqc_value = dvblink::DISEQC_LEVEL_1_B_A;
	if (boost::iequals(diseqc_type, L"Level1AB"))
		*diseqc_value = dvblink::DISEQC_LEVEL_1_A_B;
	if (boost::iequals(diseqc_type, L"Level1BB"))
		*diseqc_value = dvblink::DISEQC_LEVEL_1_B_B;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB1"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_1;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB2"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_2;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB3"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_3;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB4"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_4;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB5"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_5;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB6"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_6;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB7"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_7;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB8"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_8;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB9"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_9;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB10"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_10;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB11"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_11;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB12"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_12;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB13"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_13;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB14"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_14;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB15"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_15;
	if (boost::iequals(diseqc_type, L"Diseqc11LNB16"))
		*diseqc_value = dvblink::DISEQC_LEVEL_11_16;

	if (*diseqc_value == 0xFF)
    {
        if (wcslen(diseqc_type) != 0)
        {
            //this is either unicable or a custom diseqc value
            if (!GetUnicableDiseqcString(unicable_slots, headend, transponder, diseqc_value, rawdata_string))
            {
                *diseqc_value = dvblink::DISEQC_12;
                //diseqc type contains raw data string
                rawdata_string = diseqc_type;
            }
        }
        else
        {
		    ret_val = false;
        }
    }

	return ret_val;
}

bool CTVSTransponderParser::GetUnicableDiseqcString(const TVSC_UnicableSlotList& unicable_slots, TVSC_HeadendType& headend, dvblink::TTransponderInfo* transponder, 
                                                    unsigned char* diseqc_value, std::wstring& rawdata_string)
{
    bool ret_val = false;

    //find a slot for this channels
    int idx = -1;
    for (unsigned int i=0; i<unicable_slots.size(); i++)
    {
        if (boost::iequals(unicable_slots[i].id, headend.diseqc.typeID))
        {
            //found!
            idx = i;
            break;
        }
    }
    if (idx >= 0)
    {
        *diseqc_value = dvblink::DISEQC_UNICABLE;
        rawdata_string = headend.diseqc.typeID;

        ret_val = true;
    }
/*
    if (idx >= 0)
    {
        //calculate diseqc sequence
        //E0 : command, 10: broadcast address, 5A: Channel change command
        //Data byte 0: b7,b6,b5: Slot Number (0-7), b4:PosA=0/PosB=1, b3: Vert=0/Horz=1,b2:LoBand=0/HiBand=1
        //Data byte 0:b1,b0; Data byte 1:b7-b0: 10 bits of the tuning word
        //tunning word: round((Transponder Freq  - LNB Freq + Slot Freq)/4 - 350), all freqs in MHz
        unsigned short tuning_word = static_cast<unsigned short>(round_double((transponder->dwFreq/1000 - transponder->dwLOF/1000 + unicable_slots[idx].frequency/1000)/4.0 - 350));
        unsigned char b0 = 0;
        //slot
        b0 = b0 | ((unsigned char)unicable_slots[idx].slot_number << 5);
        //PosA/B
        if (unicable_slots[idx].position == ETVS_USP_POS_B)
            b0 = b0 | 0x10;
        //vert/horz
        if (transponder->Pol == dvblink::POL_HORIZONTAL)
            b0 = b0 | 0x08;
        //lo/high band
        if (((unsigned long)transponder->dwFreq) > headend.LNB.LOFSW)
            b0 = b0 | 0x04;

        //tuning word
        b0 = b0 | ((tuning_word >> 8) & 0x03);
        unsigned char b1 = (tuning_word & 0x00FF);

        *diseqc_value = dvblink::DISEQC_12;
        rawdata_string = L"E0105A";
        rawdata_string += string_cast<EC_UTF8>(network_helper::decode_char_to_hex(b0));
        rawdata_string += string_cast<EC_UTF8>(network_helper::decode_char_to_hex(b1));

        //adjust frequency
        transponder->dwFreq = unicable_slots[idx].frequency + transponder->dwLOF;
        ret_val = true;
    }
*/
    return ret_val;
}

int CTVSTransponderParser::GetTransponderFromDeliveryInfo(STSDeliverySystemInfo& delivery_info,
        TVSC_TransponderType& transponder)
{
    std::stringstream sbuf;
    //create a transponder string
    std::string tr_string;
    for (unsigned int i=0; i<6; i++)
    {
        switch (i)
        {
        case 0:
            {
                //frequency (divide it by 100 for sat and by 10 for cable/terr)
                unsigned long freq = 0;
                switch (delivery_info.systemType)
                {
                case ETS_DS_SAT:
                case ETS_DS_SAT_S2:
                    freq = delivery_info.frequency/100;
                    break;
                case ETS_DS_TERRESTRIAL:
                    freq = delivery_info.frequency/100;
                    break;
                case ETS_DS_CABLE:
                    freq = delivery_info.frequency/10;
                    break;
                default:
                    freq = delivery_info.frequency;
                    break;
                }
                sbuf.clear(); sbuf.str("");
                sbuf << freq;
                tr_string += sbuf.str();
            }
            break;
        case 1:
            //polarization - only for dvb-s
            if (delivery_info.systemType == ETS_DS_SAT || 
				delivery_info.systemType == ETS_DS_SAT_S2)
            {
                switch (delivery_info.polarization)
                {
                case ETS_PT_HOR:
                    tr_string += ",H";
                    break;
                case ETS_PT_VER:
                    tr_string += ",V";
                    break;
                case ETS_PT_LEFT:
                    tr_string += ",L";
                    break;
                case ETS_PT_RIGHT:
                    tr_string += ",R";
                    break;
                }
            }
            else
            {
                tr_string += ",H";
            }
            break;
        case 2:
            //symbol rate
            if (delivery_info.systemType == ETS_DS_SAT ||
				delivery_info.systemType == ETS_DS_SAT_S2 ||
                delivery_info.systemType == ETS_DS_CABLE)
            {
                sbuf.clear(); sbuf.str("");
                sbuf << delivery_info.symbol_rate/10;
                tr_string += ",";
                tr_string += sbuf.str();
            }
            else
            {
                tr_string += ",6900";
            }
            break;
        case 3:
            //fec for sat
            //bandwidth for terrestrial
            switch (delivery_info.systemType)
            {
            case ETS_DS_SAT:
            case ETS_DS_SAT_S2:
                {
                    switch (delivery_info.fec)
                    {
                    case ETS_FT_12:
                        tr_string += ",12";
                        break;
                    case ETS_FT_23:
                        tr_string += ",23";
                        break;
                    case ETS_FT_34:
                        tr_string += ",34";
                        break;
                    case ETS_FT_56:
                        tr_string += ",56";
                        break;
                    case ETS_FT_78:
                        tr_string += ",78";
                        break;
                    case ETS_FT_89:
                        tr_string += ",89";
                        break;
                    case ETS_FT_910:
                        tr_string += ",9a";
                        break;
                    default:
                        tr_string += ",";
                        break;
                    }
                }
                break;
            case ETS_DS_TERRESTRIAL:
                sbuf.clear(); sbuf.str("");
                sbuf << delivery_info.bandwidth;
                tr_string += ",";
                tr_string += sbuf.str();
                break;
            case ETS_DS_CABLE:
                tr_string += ",";
                break;
            default:
                tr_string += ",";
                break;
            }
            break;
        case 4:
            //modulation
			if (delivery_info.systemType == ETS_DS_SAT)
			{
				tr_string += ",DVB-S";
			} else
			if (delivery_info.systemType == ETS_DS_SAT_S2)
			{
				tr_string += ",DVB-S2";
			} else
			{
				switch (delivery_info.modulation)
				{
				case ETS_MT_QAM16:
					tr_string += ",QAM16";
					break;
				case ETS_MT_QAM32:
					tr_string += ",QAM32";
					break;
				case ETS_MT_QAM64:
					tr_string += ",QAM64";
					break;
				case ETS_MT_QAM128:
					tr_string += ",QAM128";
					break;
				case ETS_MT_QAM256:
					tr_string += ",QAM256";
					break;
				default:
					break;
				}
			}
            break;
		case 5:
			//modulation for dvb-s/s2 transponders
            if (delivery_info.systemType == ETS_DS_SAT ||
				delivery_info.systemType == ETS_DS_SAT_S2)
			{
				switch (delivery_info.modulation)
				{
				case ETS_MT_QPSK:
					tr_string += ",QPSK";
					break;
				case ETS_MT_8PSK:
					tr_string += ",8PSK";
					break;
				default:
					break;
				}
			}
			break;
        }
    }
    transponder.transponderID = m_TransponderIDFeed++;
    transponder.trName = string_cast<EC_UTF8>(tr_string);
    transponder.data = string_cast<EC_UTF8>(tr_string);
    return 1;
}
