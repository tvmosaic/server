/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include <drivers/tuner_factory.h>
#include <dl_parameters.h>
#include "dvb_parameters.h"
#include "dvb_device.h"
#include "dvb_channel_scanner.h"
#include "tvs_transponder_streamer.h"
#include "dvb_program_streamer.h"
#include "dvb_epg_scanner.h"
#include "dvb_tvs_helpers.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

dvb_device_t::dvb_device_t(const dvblink::device_id_t& device_id, tuner_t* tuner, directory_settings_obj_t& dir_settings, const concise_param_map_t& device_params) :
    device_t(), tuner_(tuner), transponder_streamer_(NULL), dir_settings_(dir_settings), device_id_(device_id), device_params_(device_params)
{
}

dvb_device_t::~dvb_device_t()
{
    delete_transponder_streamer();
    delete tuner_;
}

channel_scanner_t* dvb_device_t::get_channel_scanner()
{
    delete_transponder_streamer();

    return new dvb_channel_scanner_t(tuner_);
}

void dvb_device_t::delete_transponder_streamer()
{
    if (transponder_streamer_ != NULL)
    {
        transponder_streamer_->term();
        delete transponder_streamer_;
        transponder_streamer_ = NULL;
    }
}

void dvb_device_t::release_channel_scanner(channel_scanner_t* scanner)
{
    delete scanner;
}

epg_scanner_t* dvb_device_t::get_epg_scanner()
{
    delete_transponder_streamer();

    return new dvb_epg_scanner_t(tuner_, device_params_);
}

void dvb_device_t::release_epg_scanner(epg_scanner_t* epg_scanner)
{
    delete epg_scanner;
}

program_streamer_t* dvb_device_t::get_program_streamer(const dvblink::channel_id_t& channel_id, const concise_channel_tune_info_t& tune_params)
{
    DL_E_TUNER_TYPES tuner_type = get_tuner_type_from_tuning_string(tune_params.tuning_params_.get());

    if (transponder_streamer_ == NULL)
    {
        dvblink::filesystem_path_t shared_dir;
        shared_dir = dir_settings_->get_shared_dir_for_device(device_id_);

        transponder_streamer_ = new transponder_streamer(tuner_, shared_dir);
        transponder_streamer_->init(tuner_type);
    } else
    {
        //transponder streamer already exists and may be streaming channels
        DL_E_TUNER_TYPES current_tuner_type = transponder_streamer_->get_tuner_type();
        if (current_tuner_type != tuner_type)
        {
            //only allow tuner type change if there are no active streams (otherwie higher levels should have stopped them)
            if (transponder_streamer_->get_num_active_programs() > 0)
            {
                log_error(L"dvb_device_t::get_program_streamer. Cannot change tuner type while streaming is active");
                return NULL;
            } else
            {
                log_info(L" dvb_device_t::get_program_streamer. Channel request for %1%. Switching devivce type from %2% to %3%") % string_cast<EC_UTF8>(channel_id.get()) % (int)current_tuner_type % (int)tuner_type;
                transponder_streamer_->term();
                transponder_streamer_->init(tuner_type);
            }
        }
    }

    return new dvb_program_streamer_t(transponder_streamer_, channel_id.get(), tune_params);
}

void dvb_device_t::release_program_streamer(program_streamer_t* streamer)
{
    delete streamer;
}

bool dvb_device_t::can_join_streaming(const concise_channel_tune_info_t& channel_tune_params)
{
    bool ret_val = false;

    if (transponder_streamer_ != NULL)
    {
        DL_E_TUNER_TYPES tuner_type;
        concise_program_info_t pi;
        if (get_channel_tuning_params_from_string(channel_tune_params, tuner_type, pi))
        {
            if (transponder_streamer_->get_tuner_type() == tuner_type)
                ret_val = transponder_streamer_->channel_can_join(pi.tuning_info_);
        }
    }

    return ret_val;
}

bool dvb_device_t::is_stream_idle()
{
    bool ret_val = true;

    if (transponder_streamer_ != NULL)
        ret_val = transponder_streamer_->is_idle();

    return ret_val;
}

void dvb_device_t::find_conflicting_headends(const headend_description_for_device_list_t& existing_headends, const headend_info_map_t& headend_info,
        const headend_description_for_device_t& new_headend, headend_id_list_t& conflicting_headends)
{
    for (size_t i=0; i<existing_headends.size(); i++)
    {
        //if provider id is the same, there is no conflict
        if (!boost::iequals(existing_headends[i].headend_id_.get(), new_headend.headend_id_.get()))
        {
            headend_info_map_t::const_iterator ep = headend_info.find(existing_headends[i].headend_id_);
            headend_info_map_t::const_iterator np = headend_info.find(new_headend.headend_id_);
            if (ep != headend_info.end() && np != headend_info.end())
            {
                //providers with same broadcast type are conflicting, unless they are dvb-s on different disec
                if (ep->second.standard_ == np->second.standard_)
                {
                    bool bconflict = true;
                    parameter_value_t dt1, dt2;
                    if (ep->second.standard_ == st_dvb_s_s2)
                    {
                        dt1 = get_value_for_param(dvb_diseqc_type_key, existing_headends[i].device_config_params_);
                        dt2 = get_value_for_param(dvb_diseqc_type_key, new_headend.device_config_params_);
                        bconflict = boost::iequals(dt1.get(), dt2.get());
                    }
                    if (bconflict)
                    {
                        conflicting_headends.push_back(existing_headends[i].headend_id_);
                        log_info(L"dvb_device_t::find_conflicting_headends. Detected conflict between headends %1% (%2%) and %3% (%4%)") % existing_headends[i].headend_id_.to_wstring() % dt1.to_wstring() % new_headend.headend_id_.to_wstring() % dt2.to_wstring();
                    }
                }
            }
        }
    }
}
