/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <assert.h>
#include <string.h>
#include <dl_ts_info.h>
#include <dl_logger.h>
#include "eit_epg_parser.h"
#include "eit_epg_converter.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

////////////////////////////////////////////

int GetBaseSectionNumPerSegment(int segment_num)
{
	return TSEPG_MAX_SECTIONS_PER_SEGMENT * segment_num;
}

int GetSegmentNumForSection(int section_num)
{
	return section_num / TSEPG_MAX_SECTIONS_PER_SEGMENT;
}

////////////////////////////////////////////

bool CDLTSEPGStreamId::IsEqual(CDLTSEPGStreamId& id)
{
	if (m_NID == id.m_NID &&
		m_TID == id.m_TID &&
		m_SID == id.m_SID)
		return true;
	return false;
}

////////////////////////////////////////////////

CDLTSEPGSegment::CDLTSEPGSegment(int segment_num)
{
	for (int i=0; i<TSEPG_MAX_SECTIONS_PER_SEGMENT; i++)
		m_Sections[i] = NULL;
	m_SegmentNum = segment_num;
	m_MaxSectionNum = 0;
}

CDLTSEPGSegment::~CDLTSEPGSegment()
{
	for (int i=0; i<TSEPG_MAX_SECTIONS_PER_SEGMENT; i++)
	{
		if (m_Sections[i] != NULL)
			free(m_Sections[i]);
	}
}

EMBTS_EPGPARSER_ADDSECTION_RES CDLTSEPGSegment::AddSection(unsigned char* eit_section, int eit_section_len)
{
	EMBTS_EPGPARSER_ADDSECTION_RES ret_val = EMBTS_EPAR_DISCARDED;

	//If it is the first section - find out how many are still expected to come
	if (m_MaxSectionNum == 0)
	{
		//fill all parameters of this EPG data item
		int n = ts_process_routines::GetEITMaxSegmentSectionNum(eit_section, eit_section_len);
		m_MaxSectionNum = n - GetBaseSectionNumPerSegment(m_SegmentNum) + 1;
		if (m_MaxSectionNum <= 0 || m_MaxSectionNum > TSEPG_MAX_SECTIONS_PER_SEGMENT)
			m_MaxSectionNum = TSEPG_MAX_SECTIONS_PER_SEGMENT; //set expectation to the maximum number of segments
	}

	if (m_MaxSectionNum != 0)
	{
		int sec_num = ts_process_routines::GetEITCurSectionNum(eit_section, eit_section_len);
		//adjust section number to the segment base
		sec_num -= GetBaseSectionNumPerSegment(m_SegmentNum);
		//check if it is not greater then maximum expected number of sections
		if (sec_num <0 || sec_num >= m_MaxSectionNum)
		{
			//log error
			log_info(L"CDLTSEPGServiceInfoChunck::AddSection. EPG section does not belong to this segment");
		}
        else
		{
			if (m_Sections[sec_num] == NULL)
			{
				//Add section
				void* newptr = malloc(eit_section_len);
				memcpy(newptr, eit_section, eit_section_len);
				m_Sections[sec_num] = (unsigned char*)newptr;
				ret_val = EMBTS_EPAR_ACCEPTED;
			}
		}
	}
	return ret_val;
}

bool CDLTSEPGSegment::IsCompleted()
{
	bool ret_val = true;

	for (int i=0; i<m_MaxSectionNum; i++)
	{
		if (m_Sections[i] == NULL)
		{
			ret_val = false;
			break;
		}
	}

	return ret_val;
}

bool CDLTSEPGSegment::GetFirstSection(SEPGSegmentSection& section)
{
	bool ret_val = false;

	for (int i=0; i<TSEPG_MAX_SECTIONS_PER_SEGMENT; i++)
	{
		if (m_Sections[i] != NULL)
		{
			section.m_Section = i;
			section.m_SectionData = m_Sections[i];
			ret_val = true;
			break;
		}
	}
	
    return ret_val;
}

bool CDLTSEPGSegment::GetNextSection(SEPGSegmentSection& section)
{
	bool ret_val = false;

	for (int i=section.m_Section + 1; i<TSEPG_MAX_SECTIONS_PER_SEGMENT; i++)
	{
		if (m_Sections[i] != NULL)
		{
			section.m_Section = i;
			section.m_SectionData = m_Sections[i];
			ret_val = true;
			break;
		}
	}
	
    return ret_val;
}

////////////////////////////////////////////////

CDLTSEPGTable::CDLTSEPGTable()
{
	for (int i=0; i<TSEPG_MAX_SEGMENTS_PER_TABLE; i++)
		m_Segments[i] = NULL;
	m_Version = -1;
}

CDLTSEPGTable::~CDLTSEPGTable()
{
	Reset();
}

bool CDLTSEPGTable::IsCompleted()
{
	bool ret_val = false;
	if (m_Version != -1)
	{
		ret_val = true;
		int high_seg_num = m_LastSectionNum / TSEPG_MAX_SECTIONS_PER_SEGMENT;
		for (int i = 0; i <= high_seg_num; i++)
		{
			if (m_Segments[i] == NULL)
			{
				ret_val = false;
				break;
			}
            else
			{
				ret_val &= m_Segments[i]->IsCompleted();
			}
		}
	}
	return ret_val;
}

void CDLTSEPGTable::Reset()
{
	for (int i = 0; i < TSEPG_MAX_SEGMENTS_PER_TABLE; i++)
	{
		if (m_Segments[i] != NULL)
		{
			delete m_Segments[i];
			m_Segments[i] = NULL;
		}
	}

	m_Version = -1;
}

EMBTS_EPGPARSER_ADDSECTION_RES CDLTSEPGTable::AddSection(unsigned char* eit_section, int eit_section_len)
{
	EMBTS_EPGPARSER_ADDSECTION_RES ret_val = EMBTS_EPAR_DISCARDED;

    try
    {
	    //check version first
	    if (m_Version == -1)
	    {
		    //This is the first section in this table
		    m_Version = ts_process_routines::GetEITVersion(eit_section, eit_section_len);
		    m_LastSectionNum = ts_process_routines::GetEITMaxSectionNum(eit_section, eit_section_len);
	    }
        else
	    {
		    int ver = ts_process_routines::GetEITVersion(eit_section, eit_section_len);
		    if (ver != m_Version)
		    {
			    //Restart parsing
			    Reset();
			    m_Version = ver;
			    m_LastSectionNum = ts_process_routines::GetEITMaxSectionNum(eit_section, eit_section_len);
			    log_info(L"CDLTSEPGTable::AddSection. EPG table with new version number has arrived. Restarting parsing");
		    }
	    }

	    //find the segment where this section belongs
	    int sec_num = ts_process_routines::GetEITCurSectionNum(eit_section, eit_section_len);
	    if (sec_num <= m_LastSectionNum)
	    {
		    int seg_num = GetSegmentNumForSection(sec_num);
		    if (m_Segments[seg_num] == NULL)
		    {
			    m_Segments[seg_num] = new CDLTSEPGSegment(seg_num);
		    }
		    ret_val = m_Segments[seg_num]->AddSection(eit_section, eit_section_len);
	    }
        else
	    {
		    log_info(L"CDLTSEPGTable::AddSection. Current section number (%d) is more then maximum section number (%d)") % sec_num % m_LastSectionNum;
	    }
    }
    catch (std::exception& x)
    {
        log_error(L"EXCEPTION in CDLTSEPGTable::AddSection(): %1%") % x.what();
    }
    catch (...)
    {
        log_error(L"EXCEPTION in CDLTSEPGTable::AddSection()");
    }

	return ret_val;
}

bool CDLTSEPGTable::GetFirstSection(SEPGTableSection& section)
{
	bool ret_val = false;
	int seg_num = -1;
	
    //Find first non-NULL segment
	for (int i=0; i<TSEPG_MAX_SEGMENTS_PER_TABLE; i++)
	{
		if (m_Segments[i] != NULL)
		{
			seg_num = i;
			break;
		}
	}
	
    if (seg_num != -1)
	{
		SEPGSegmentSection seg_section;
		
        if (m_Segments[seg_num]->GetFirstSection(seg_section))
		{
			section.m_Segment = seg_num;
			section.m_Section = seg_section.m_Section;
			section.m_SectionData = seg_section.m_SectionData;
			ret_val = true;
		}
	}
	
    return ret_val;
}

bool CDLTSEPGTable::GetNextSection(SEPGTableSection& section)
{
	bool ret_val = false;
	//Try to get the next valid section in the current segment first
	SEPGSegmentSection seg_section;
	seg_section.m_Section = section.m_Section;
	
    if (m_Segments[section.m_Segment]->GetNextSection(seg_section))
	{
		section.m_Section = seg_section.m_Section;
		section.m_SectionData = seg_section.m_SectionData;
		ret_val = true;
	}
    else
	{
		//get next segment
		int seg_num = -1;
		for (int i=section.m_Segment+1; i<TSEPG_MAX_SEGMENTS_PER_TABLE; i++)
		{
			if (m_Segments[i] != NULL)
			{
				seg_num = i;
				break;
			}
		}
		
        if (seg_num != -1)
		{
			if (m_Segments[seg_num]->GetFirstSection(seg_section))
			{
				section.m_Segment = seg_num;
				section.m_Section = seg_section.m_Section;
				section.m_SectionData = seg_section.m_SectionData;
				ret_val = true;
			}
		}
	}
	
    return ret_val;
}

////////////////////////////////////////////////

CDLTSEPGSeenService::CDLTSEPGSeenService(int table_base,
    CDLTSEPGStreamId& stream_id, int last_tableid)
{
	m_TableBaseId = table_base;
	m_StreamId = stream_id;
	m_bCompleted = false;
	m_LastTableId = last_tableid;

	for (int i = 0; i < TSEPG_MAX_TABLES_CUR_STREAM; i++)
	{
		m_EPGTables[i] = NULL;
	}
}

CDLTSEPGSeenService::~CDLTSEPGSeenService()
{
	for (int i=0; i<TSEPG_MAX_TABLES_CUR_STREAM; i++)
	{
		if (m_EPGTables[i] != NULL)
		{
			delete m_EPGTables[i];
		}
	}
}

bool CDLTSEPGSeenService::IsStreamIdEqual(CDLTSEPGStreamId& stream_id)
{
	return m_StreamId.IsEqual(stream_id);
}

bool CDLTSEPGSeenService::IsCompleted()
{
	bool ret_val = false;

	if (m_bCompleted)
	{
		ret_val = true;
	}
    else
	{
		ret_val = true;
		int max_table_idx = m_LastTableId - m_TableBaseId;
		
        for (int i=0; i<=max_table_idx; i++)
		{
			if (m_EPGTables[i] == NULL)
			{
				ret_val = false;
				break;
			}
            else
			{
				ret_val &= m_EPGTables[i]->IsCompleted();
			}
		}
		if (ret_val)
			m_bCompleted = true;
	}
	
    return ret_val;
}

EMBTS_EPGPARSER_ADDSECTION_RES CDLTSEPGSeenService::AddSection(unsigned char* eit_section, int eit_section_len)
{
	EMBTS_EPGPARSER_ADDSECTION_RES ret_val = EMBTS_EPAR_DISCARDED;
	
    try
    {
        if (!m_bCompleted)
        {
            int table_id = ts_process_routines::GetEITTableId(eit_section, eit_section_len);
            int table_idx = table_id - m_TableBaseId;
            if (table_idx >= 0 && table_idx < TSEPG_MAX_TABLES_CUR_STREAM)
            {
                if (m_EPGTables[table_idx] == NULL)
                {
                    m_EPGTables[table_idx] = new CDLTSEPGTable();
                }
                ret_val = m_EPGTables[table_idx]->AddSection(eit_section, eit_section_len);
                if (ret_val == EMBTS_EPAR_ACCEPTED)
                {
                    //check whether the EPG info for this service is complete
                    if (IsCompleted())
                    {
                        ret_val = EMBTS_EPAR_EPG_SERVICE_COMPLETE;
                        //Log event
                        log_info(L"CDLTSEPGSeenService::AddSection. EPG parsing is completed for stream with NID %d, TID %d, SID %d") % m_StreamId.m_NID % m_StreamId.m_TID % m_StreamId.m_SID;
                    }
                }
            }
            else
            {
                //Log error
            	log_info(L"CDLTSEPGSeenService::AddSection. Table id is invalid (%d)") % table_id;
            }
        }
    }
    catch (std::exception& x)
    {
        log_error(L"EXCEPTION in CDLTSEPGSeenService::AddSection(): %1%") % x.what();
    }
    catch (...)
    {
        log_error(L"EXCEPTION in CDLTSEPGSeenService::AddSection()");
    }
    
    return ret_val;
}

void CDLTSEPGSeenService::GetEPGEvents(const wchar_t* language, bool bIgnoreShortDesc, bool use_event_id, DLEPGEventList& event_list, bool& bCancelFlag)
{
    event_list.clear();

    CDLTSEPGEITConverter eit_converter(language, bIgnoreShortDesc, use_event_id);
	
    //Walk through all tables
	for (int i = 0; i <= m_LastTableId - m_TableBaseId; i++)
	{
		if (m_EPGTables[i] != NULL)
		{
			SEPGTableSection section;
			if (m_EPGTables[i]->GetFirstSection(section))
			{
				do
				{
                    eit_converter.AddEITSection(section.m_SectionData);
					if (bCancelFlag)
						break;
				}
                while(m_EPGTables[i]->GetNextSection(section));
			}
		}
        else
		{
			//Log error
        	log_info(L"CDLTSEPGSeenService::GetEPGEvents. EPG table is expected but does not exist (yet?)");
		}
	}
    
    eit_converter.GetEvents(event_list);
}

//////////////////////////////////////////////////////////////////////////////////////
CDLTSEPGServiceList::CDLTSEPGServiceList(int table_base)
{
	m_TableBaseId = table_base;
}

CDLTSEPGServiceList::~CDLTSEPGServiceList()
{
	Reset();
}

void CDLTSEPGServiceList::Reset()
{
	for (size_t i = 0; i < m_SeenServices.size(); i++)
    {
		delete m_SeenServices.at(i);
    }

	m_SeenServices.clear();
}

EMBTS_EPGPARSER_ADDSECTION_RES CDLTSEPGServiceList::AddSection(unsigned char* eit_section, int eit_section_len)
{
	EMBTS_EPGPARSER_ADDSECTION_RES ret_val = EMBTS_EPAR_DISCARDED;

    try
    {
        if (AcceptSection(eit_section, eit_section_len))
        {
            unsigned short NetworkId, TSId, ServiceId;
            if (ts_process_routines::GetEITSectionIds(eit_section, eit_section_len, NetworkId, TSId, ServiceId))
            {
                CDLTSEPGStreamId stream_id(NetworkId, TSId, ServiceId);
                int idx = FindEPGStream(stream_id);
                if (idx == -1)
                {
                    log_info(L"CDLTSEPGServiceList::AddSection. EPG parsing has started for stream with NID %d, TID %d, SID %d") % NetworkId % TSId % ServiceId;
                    //Add this new stream to the list of seen streams
                    int last_table_id = ts_process_routines::GetEITLastTableId(eit_section, eit_section_len);
                    if (last_table_id < m_TableBaseId)
                        last_table_id = m_TableBaseId + TSEPG_MAX_TABLES_CUR_STREAM - 1; //this is the corner case (bad data?). Set expectation to all tables
                    if ((last_table_id >= m_TableBaseId) && (last_table_id < (m_TableBaseId + TSEPG_MAX_TABLES_CUR_STREAM)))
                    {
                        CDLTSEPGSeenService* new_stream = new CDLTSEPGSeenService(m_TableBaseId, stream_id, last_table_id);
                        m_SeenServices.push_back(new_stream);
                        idx = (int)m_SeenServices.size() - 1;
                    }
                    else
                    {
                        //Log error
                    	log_info(L"CDLTSEPGServiceList::AddSection. Invalid last table id (%d)") % last_table_id;
                    }
                }
                else
                {
                    //check if this stream is already completed
                    if (m_SeenServices.at(idx)->IsCompleted())
                    {
                        idx = -1;
                    }
                }
                if (idx != -1)
                {
                    ret_val = m_SeenServices.at(idx)->AddSection(eit_section, eit_section_len);
                }
            }
        }
    }
    catch (std::exception& x)
    {
        log_error(L"EXCEPTION in CDLTSEPGServiceList::AddSection(): %1%") % x.what();
    }
    catch (...)
    {
        log_error(L"EXCEPTION in CDLTSEPGServiceList::AddSection()");
    }

	return ret_val;
}

bool CDLTSEPGServiceList::AcceptSection(unsigned char* eit_section, int eit_section_len)
{
	bool ret_val = false;
	
    //check whether section is complete
	if (ts_process_routines::GetSectionLength(eit_section) == eit_section_len)
	{
		//Get table id
		unsigned char table_id = ts_process_routines::GetEITTableId(eit_section, eit_section_len);
		//only accept "schedule" sections for current transponder
		if (table_id >= m_TableBaseId && table_id < m_TableBaseId + TSEPG_MAX_TABLES_CUR_STREAM)
		{
			//check current-next indicator: only current sections are accepted
			if (ts_process_routines::GetEITCurNextIndicator(eit_section, eit_section_len) == 0x01)
			{
				ret_val = true;
			}
		}
	}

	return ret_val;
}

bool CDLTSEPGServiceList::IsCompleted()
{
	bool ret_val = true;
	
    for (size_t i = 0; i < m_SeenServices.size(); i++)
	{
		ret_val &= IsServiceCompleted(m_SeenServices.at(i)->GetStreamId());
	}
	
    return ret_val;
}

bool CDLTSEPGServiceList::IsServiceCompleted(CDLTSEPGStreamId* stream_id)
{
	bool ret_val = false;
	int srv_idx = FindEPGStream(*stream_id);

    if (srv_idx != -1)
	{
		ret_val = m_SeenServices.at(srv_idx)->IsCompleted();
	}

    return ret_val;
}

int CDLTSEPGServiceList::FindEPGStream(CDLTSEPGStreamId& stream_id)
{
	int ret_val = -1;
	
    for (size_t i = 0; i < m_SeenServices.size(); i++)
	{
		if (m_SeenServices.at(i)->IsStreamIdEqual(stream_id))
		{
			ret_val = static_cast<int>(i);
			break;
		}
	}
	
    return ret_val;
}

////////////////////////////////////////////////

CDLTSEPGParser::CDLTSEPGParser()
{
    try
    {
	    m_CurTrServices = new CDLTSEPGServiceList(TSEPG_CUR_STREAM_TABLEID_BASE);
	    m_OtherTrServices = new CDLTSEPGServiceList(TSEPG_OTHER_STREAM_TABLEID_BASE);
    }
    catch (std::exception& x)
    {
        log_error(L"EXCEPTION in CDLTSEPGParser::CDLTSEPGParser(): %1%") % x.what();
        throw;
    }
    catch (...)
    {
        log_error(L"CDLTSEPGParser::CDLTSEPGParser()");
        throw;
    }
}

CDLTSEPGParser::~CDLTSEPGParser()
{
	Reset();
	delete m_OtherTrServices;
	delete m_CurTrServices;
}

void CDLTSEPGParser::Reset()
{
	m_CurTrServices->Reset();
	m_OtherTrServices->Reset();
}

EMBTS_EPGPARSER_ADDSECTION_RES CDLTSEPGParser::AddSection(unsigned char* eit_section, int eit_section_len)
{
	EMBTS_EPGPARSER_ADDSECTION_RES ret_val = EMBTS_EPAR_DISCARDED;

	if (m_CurTrServices->AcceptSection(eit_section, eit_section_len))
	{
		ret_val = m_CurTrServices->AddSection(eit_section, eit_section_len);
	}
    else
	{
		if (m_OtherTrServices->AcceptSection(eit_section, eit_section_len))
		{
			ret_val = m_OtherTrServices->AddSection(eit_section, eit_section_len);
		}
	}
	
    return ret_val;
}

bool CDLTSEPGParser::IsCompleted()
{
	return m_CurTrServices->IsCompleted() && m_OtherTrServices->IsCompleted();
}
