/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>
#include <map>
#include <dl_epgevent.h>

typedef std::vector<std::wstring> CDLTSEPGKeywordList;
typedef std::map<std::wstring, CDLTSEPGKeywordList> CDLTSEPGKeywordMap;

class CDLTSEPGEITConverter
{
public:
	struct SLangEventDesc
	{
		SLangEventDesc(){maxDescNum = -1;};

		std::map<int, std::wstring> extTextMap;
		int maxDescNum;
		CDLTSEPGKeywordMap keywordMap;
		std::wstring language;
		std::wstring name;
		std::wstring shortdesc;
	};
	typedef std::map<std::wstring, SLangEventDesc> TLangEventMap;

	CDLTSEPGEITConverter(const wchar_t* language, bool ignoreShortDesc, bool use_event_id);
	~CDLTSEPGEITConverter();

    void Reset();
	void AddEITSection(unsigned char* section_ptr);
    void GetEvents(dvblink::engine::DLEPGEventList& event_list);

protected:
	time_t MJD2time_t(int mjd, int secs);
	void GetHHMMSS(unsigned char* bcd_time, int* hh, int* mm, int* ss, int* total_sec);
	void ProcessShortEventDescr(unsigned short onid, TLangEventMap& eventmap, unsigned char* descr_ptr);
	void ProcessExtendedDescr(unsigned short onid, TLangEventMap& eventmap, unsigned char* descr_ptr);

	void ProcessContentDescr(dvblink::engine::DLEPGEvent& epgevent, unsigned char* descr_ptr);
	void ProcessComponentDescr(dvblink::engine::DLEPGEvent& epgevent, unsigned char* descr_ptr);

    void convert_annexa_to_unicode_for_onid(unsigned short onid, unsigned char* annexa_text, int text_len, std::wstring& out_str);

    dvblink::engine::DLEPGEventList m_EventList;
	TLangEventMap m_LangEventMap;
	bool m_bIgnoreShortDesc;
	std::vector<std::wstring> eit_language_;
    bool use_event_id_;
};


