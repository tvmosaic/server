/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/


#ifndef _TVSOURCETRPARSER_H_
#define _TVSOURCETRPARSER_H_

#include <dl_ts_info.h>
#include <drivers/deviceapi.h>
#include "tvs_core_def.h"

class CTVSTransponderParser
{
public:
	CTVSTransponderParser();
	~CTVSTransponderParser();

    static int ParseTransponderData(const TVSC_UnicableSlotList& unicable_slots, dvblink::DL_E_TUNER_TYPES device_type, TVSC_HeadendType& headend,
        const std::wstring& tr_data, dvblink::TTransponderInfo* transponder, unsigned char* diseqc_value, std::wstring& rawdata_string);

    static int GetTransponderFromDeliveryInfo(dvblink::engine::STSDeliverySystemInfo& delivery_info, TVSC_TransponderType& transponder);

    static std::string make_dvbc_tuning_string(unsigned long freq, unsigned long sr, dvblink::DL_E_MOD_DVBC qam_value);

	static bool GetDiseqcValueFromType(const TVSC_UnicableSlotList& unicable_slots, TVSC_HeadendType& headend, dvblink::TTransponderInfo* transponder, unsigned char* diseqc_value, std::wstring& rawdata_string);
protected:
    static long m_TransponderIDFeed;
	static int ParseTransponderString(const wchar_t* tr_str, std::vector<std::wstring>& paramStrArray);
    static bool GetUnicableDiseqcString(const TVSC_UnicableSlotList& unicable_slots, TVSC_HeadendType& headend, dvblink::TTransponderInfo* transponder, unsigned char* diseqc_value, std::wstring& rawdata_string);
};

///////////////////////////////////////////////////////////////////////////////
#endif //_TVSOURCETRPARSER_H_
