/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <map>
#include <dl_ts_info.h>
#include "tvs_core_def.h"

//**************************************************************************
//Holder class for DVB sections

class CTVSSectionDescriptor
{
public:
	CTVSSectionDescriptor(unsigned char* section, int length);
	CTVSSectionDescriptor(){};

	unsigned char* m_Section;
	int m_Length;
};

typedef std::map<unsigned char, CTVSSectionDescriptor> TTVSSectionDescMap;

//**************************************************************************
//Base class for services scan (SDT sections)

class CTVSServiceListDescriptorBase
{
public:
	CTVSServiceListDescriptorBase();
	virtual ~CTVSServiceListDescriptorBase();

	bool AddSection(unsigned char* section, int length);
	bool IsCompleted();
	void GetServices(std::vector<TVSC_ChannelType>& services);
	static TSVC_CHANNEL_MEDIA_TYPE GetServiceTypeFromDVBType(unsigned char type);

protected:
	virtual void GetSectionIDs(unsigned char* section, int length, unsigned short& nid, unsigned short& tid){};
	virtual void GetSectionStats(unsigned char* section, int length, unsigned char& version, unsigned char& cur_next){};
	virtual void GetSectionNumbers(unsigned char* section, int length, unsigned char& cur_section_num, unsigned char& max_section_num){};
	virtual void GetServicesFromSection(unsigned char* section, int length, std::vector<TVSC_ChannelType>& services){};

	void ResetSectionList();

public:
    int m_MaxSectionNum;
    TTVSSectionDescMap m_SectionDescMap;

private:
	unsigned char m_Version;
	unsigned short m_TID;
	unsigned short m_NID;
};

typedef std::map<unsigned long, CTVSServiceListDescriptorBase*> TTVSServiceListMap;

//**************************************************************************
//Standard specific service descriptors

class CTVSServiceDVBListDescriptor : public CTVSServiceListDescriptorBase
{
public:
	CTVSServiceDVBListDescriptor();

protected:
	virtual void GetSectionIDs(unsigned char* section, int length, unsigned short& nid, unsigned short& tid);
	virtual void GetSectionStats(unsigned char* section, int length, unsigned char& version, unsigned char& cur_next);
	virtual void GetSectionNumbers(unsigned char* section, int length, unsigned char& cur_section_num, unsigned char& max_section_num);
	virtual void GetServicesFromSection(unsigned char* section, int length, std::vector<TVSC_ChannelType>& services);
};

class CTVSServiceATSCListDescriptor : public CTVSServiceListDescriptorBase
{
public:
	CTVSServiceATSCListDescriptor();

protected:
	virtual void GetSectionIDs(unsigned char* section, int length, unsigned short& nid, unsigned short& tid);
	virtual void GetSectionStats(unsigned char* section, int length, unsigned char& version, unsigned char& cur_next);
	virtual void GetSectionNumbers(unsigned char* section, int length, unsigned char& cur_section_num, unsigned char& max_section_num);
	virtual void GetServicesFromSection(unsigned char* section, int length, std::vector<TVSC_ChannelType>& services);
};

//**************************************************************************
//NIT scanner class for channel nnumbers retrieval

class CTVSNITScanner : public CTVSServiceListDescriptorBase
{
public:
    CTVSNITScanner();

	void GetChannelNumbers(std::vector<dvblink::engine::SDVBTLCNDesc>& services);

protected:
	virtual void GetSectionIDs(unsigned char* section, int length, unsigned short& nid, unsigned short& tid);
	virtual void GetSectionStats(unsigned char* section, int length, unsigned char& version, unsigned char& cur_next);
	virtual void GetSectionNumbers(unsigned char* section, int length, unsigned char& cur_section_num, unsigned char& max_section_num);
	virtual void GetServicesFromSection(unsigned char* section, int length, std::vector<TVSC_ChannelType>& services);
};

typedef std::map<unsigned short, CTVSNITScanner*> TTVSNITListMap;
