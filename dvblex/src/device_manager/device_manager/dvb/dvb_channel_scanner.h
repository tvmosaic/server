/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/


#pragma once

#include <vector>
#include <string>
#include <map>

#include "../channel_scanner.h"

class CTVSChannelScanner;
struct TVSC_NetworkScanData;

namespace dvblink {
	class tuner_t;
}

namespace dvblex { 

class dvb_channel_scanner_t : public channel_scanner_t
{
public:
    dvb_channel_scanner_t(dvblink::tuner_t* tuner);
	virtual ~dvb_channel_scanner_t();

protected:
    dvblink::tuner_t* tuner_;
    CTVSChannelScanner* channel_scanner_;
    std::vector<TVSC_NetworkScanData>* network_list_;

    virtual bool start();
    virtual void stop();
    virtual bool do_scan(const std::string& tune_params, const dvblink::scan_network_id_t& network_id, transponder_list_t& found_channels, scan_log_entry_t& log_entry);
    virtual bool scan_networks(const provider_scan_list_t& scan_data, network_scan_results_t& network_scan_results, scan_log_entry_t& log_entry);
    virtual bool is_network_scan(const provider_info_t& provider_info);
};

}
