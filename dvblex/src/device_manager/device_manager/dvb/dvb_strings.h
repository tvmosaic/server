/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <string>

namespace dvblex {

const std::string manual_fast_scan_provider_name		= "IDS_SERVER_MANUAL_FASTSCAN_NAME";
const std::string manual_fast_scan_provider_desc		= "IDS_SERVER_MANUAL_FASTSCAN_DESC";
const std::string lnb_parameter_name		            = "IDS_SERVER_LNB_PARAM_NAME";
const std::string diseqc_parameter_name		            = "IDS_SERVER_DISEQC_PARAM_NAME";
const std::string freq_khz_parameter_name		            = "IDS_SERVER_FREQ_KHZ_PARAM_NAME";
const std::string sr_parameter_name		                    = "IDS_SERVER_SR_PARAM_NAME";
const std::string modulation_parameter_name		            = "IDS_SERVER_MODULATION_PARAM_NAME";
const std::string qam64_parameter_name		                = "IDS_SERVER_MOD_QAM64_PARAM_NAME";
const std::string qam128_parameter_name		                = "IDS_SERVER_MOD_QAM128_PARAM_NAME";
const std::string qam256_parameter_name		                = "IDS_SERVER_MOD_QAM256_PARAM_NAME";
const std::string lof1_mhz_param_name		            = "IDS_SERVER_LOF1_PARAM_NAME";
const std::string lof2_mhz_param_name		            = "IDS_SERVER_LOF2_PARAM_NAME";
const std::string lofsw_mhz_param_name		            = "IDS_SERVER_LOFSW_PARAM_NAME";

const std::string fta_concurrency_num_name		                = "IDS_SERVER_FTA_CONCURRENCY_NUM_NAME";
const std::string enc_concurrency_num_name		                = "IDS_SERVER_ENC_CONCURRENCY_NUM_NAME";
const std::string concurrency_num_no_limit_name		            = "IDS_SERVER_CONCURRENCY_NUM_NO_LIMIT_NAME";

const std::string normal_scan_desc		= "IDS_SERVER_REGULAR_SCAN_NAME";
const std::string network_scan_desc		= "IDS_SERVER_NETWORK_SCAN_NAME";

const std::string standard_name_unknown		= "IDS_SERVER_STANDARD_UNKNOWN_NAME";
const std::string standard_name_dvbt		= "IDS_SERVER_STANDARD_DVBT_NAME";
const std::string standard_name_dvbc		= "IDS_SERVER_STANDARD_DVBC_NAME";
const std::string standard_name_dvbs		= "IDS_SERVER_STANDARD_DVBS_NAME";
const std::string standard_name_atsc		= "IDS_SERVER_STANDARD_ATSC_NAME";
const std::string standard_name_cqam        = "IDS_SERVER_STANDARD_CQAM_NAME";
const std::string standard_name_short_dvbt		= "IDS_SERVER_STANDARD_DVBT_SHORT_NAME";
const std::string standard_name_short_dvbc		= "IDS_SERVER_STANDARD_DVBC_SHORT_NAME";
const std::string standard_name_short_dvbs		= "IDS_SERVER_STANDARD_DVBS_SHORT_NAME";
const std::string standard_name_short_atsc		= "IDS_SERVER_STANDARD_ATSC_SHORT_NAME";
const std::string standard_name_short_cqam        = "IDS_SERVER_STANDARD_CQAM_SHORT_NAME";

const std::string scan_parameters_container_name		= "IDS_SERVER_SCAN_PARAMS_CONTAINER_NAME";
const std::string scan_parameters_container_desc		= "IDS_SERVER_SCAN_PARAMS_CONTAINER_DESC";

const std::string scan_parameters_fta_only_name        = "IDS_SERVER_SCAN_PARAMS_FTA_ONLY_NAME";

const std::string ext_ci_device_choice_name      = "IDS_SERVER_EXTERNAL_CI_CHOICE";
const std::string ext_ci_device_none_name        = "IDS_SERVER_EXTERNAL_CI_CHOICE_NONE";

const std::string use_inband_epg_id_choice_name  = "IDS_SERVER_USE_INBAND_EPG_CHOICE";
const std::string own_tr_for_epg_choice_name  = "IDS_SERVER_USE_OWN_EPG_TRANSPONDER";

}
