/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef _TVSOURCESTREAMSRC_H_
#define _TVSOURCESTREAMSRC_H_

#include <map>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/locks.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"
#include <boost/thread.hpp>
#include <dl_packet_buffer.h>
#include <dl_circle_buffer.h>
#include <dl_command_queue.h>
#include <dl_ts_info.h>
#include <drivers/deviceapi.h>
#include "tvs_protected_map.h"
#include "tvs_core_def.h"
#include <drivers/tuner_factory.h>

struct tvs_stream_source_options_t
{
    tvs_stream_source_options_t() :
        tuner_type_(dvblink::TUNERTYPE_UNKNOWN), custom_diseqc_delay_sec_(15), repeat_diseqc_(false), stop_stream_when_idle_(true)
    {
    }

    dvblink::DL_E_TUNER_TYPES tuner_type_;
    unsigned long custom_diseqc_delay_sec_;
    bool repeat_diseqc_;
    bool stop_stream_when_idle_;
    TVSC_UnicableSlotList unicable_slots_;
    dvblink::filesystem_path_t stream_file_path_;
};

enum ETVSDiseqcChunkType {EDCT_COMMAND, EDCT_DELAY};

struct TTVSDiseqcChunk
{
    ETVSDiseqcChunkType type;
    unsigned long delay;
    dvblink::TDiseqcCmd command;
};

typedef std::vector<TTVSDiseqcChunk> TTVSDiseqcCommandSeq;

typedef std::map<std::wstring, std::wstring> stream_wstring_map_t;
typedef protected_map<unsigned short, stream_wstring_map_t> stream_pid_client_map_t;

class CTVSStreamSource
{
    typedef void (__stdcall *LPCB_TVS_STREAMFUNC)(const unsigned char* Buf, unsigned long Len, void* user_param);

    enum ECommands {EC_TUNE, EC_GET_SIGNAL_STATS, EC_ADD_PID, EC_SEND_PMT, EC_UNLOAD, EC_ADD_PIDS, EC_REMOVE_ALL_PIDS, EC_SEND_PMT_PID, EC_EXTRA_STREAM_CB};

    struct STuneParams
    {
        dvblink::TTransponderInfo* transponder;
        unsigned char diseqc_value;
        const wchar_t* diseqc_rawdata;
        int result;
    };

    struct SGetSignalStatsParams
    {
        dvblink::TSignalInfo* signalInfo;
        bool result;
    };

    struct SAddPidParams
    {
        const wchar_t* client;
        unsigned short pid;
        bool result;
    };

    struct SAddPidsParams
    {
        const wchar_t* client;
        std::vector<unsigned short>* pids;
        bool result;
    };

    struct SRemoveAllPidsParams
    {
        const wchar_t* client;
        bool result;
    };

    struct SSendPMTParams
    {
        unsigned char* pmt;
        int pmt_len;
		dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd;
        bool result;
    };

    struct SSendPMTPidParams
    {
        unsigned short pmt_pid;
		unsigned short sid;
		dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd;
        bool result;
    };

    struct SUnloadParams
    {
        bool result;
    };

    struct SExtraStreamCallbackParams
    {
        LPCB_TVS_STREAMFUNC stream_cb;
        void* user_param;
        bool result;
    };

public:
    CTVSStreamSource();
	~CTVSStreamSource();

    void Init(dvblink::tuner_t* tuner, const tvs_stream_source_options_t& options, LPCB_TVS_STREAMFUNC stream_callback, void* user_param);
	void Term();

	bool ShowDeviceDialog();
	int TuneTransponder(dvblink::TTransponderInfo* transponder, unsigned char diseqc_value, const wchar_t* diseqc_rawdata);
	bool GetSignalStats(dvblink::TSignalInfo* signalInfo);
	int AddPID(const wchar_t* client, unsigned short pid);
    int AddPIDs(const wchar_t* client, std::vector<unsigned short>& pids);
    bool RemoveAllPids(const wchar_t* client);
	int SendPMTToCI(unsigned char* pmt, int pmt_len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd);
	int SendPMTPidToCI(unsigned short pmt_pid, unsigned short sid, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd);
    void set_extra_stream_callback(LPCB_TVS_STREAMFUNC stream_cb, void* user_param);
    int Stop();

	unsigned long GetAdditionalTuneDelay(unsigned char diseqc_value);

    //to be used only from the stream callback!
	bool cmdAddPID(const wchar_t* client, unsigned short pid);
    bool cmdAddPIDs(const wchar_t* client, std::vector<unsigned short>* pids);
	bool cmdSendPMTToCI(unsigned char* pmt, int pmt_len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd);
	bool cmdSendPMTPidToCI(unsigned short pmt_pid, unsigned short sid, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd);
    bool cmdRemovePid(const wchar_t* client, unsigned short pid);

    bool is_tuner_started() {return is_tuner_started_;}

protected:
    bool IntTuneTransponder(dvblink::TTransponderInfo* transponder, unsigned char diseqc_value, const wchar_t* diseqc_rawdata);

	static void StreamCallback(unsigned char *Data, int len, void* param);
	void process_incoming_stream(unsigned char *Data, int len);

	void StreamingThread();
	int StopStream();
    bool LoadAndInitDriver();
    void UnloadAndDeinitDriver();
	int DeleteAllPIDs();
	bool SelectLNB(dvblink::TTransponderInfo* transponder, unsigned char diseqc_value, const wchar_t* diseqc_rawdata);
    bool SendDiseqcCommandSequence(TTVSDiseqcCommandSeq& commands);
    bool IsSameLNB(dvblink::TTransponderInfo* transponder, unsigned char diseqc_value, const wchar_t* diseqc_rawdata);
    bool GetUnicableDiseqcString(dvblink::TTransponderInfo* transponder, const wchar_t* slot_id, std::wstring& rawdata_string);
    bool check_timer();

    //command processing functions
	bool cmdTuneTransponder(dvblink::TTransponderInfo* transponder, unsigned char diseqc_value, const wchar_t* diseqc_rawdata);
	bool cmdGetSignalStats(dvblink::TSignalInfo* signalInfo);
    bool cmdStop();
    bool cmdRemoveAllPids(const wchar_t* client);
    void cmd_set_extra_stream_callback(LPCB_TVS_STREAMFUNC stream_cb, void* user_param);

    //fill ts file for debugging purposes
    void create_full_ts_file(long freq);

private:
    dvblink::tuner_t* tuner_;
    dvblink::engine::ts_circle_buffer m_CircleBuffer;
	boost::thread* m_StreamingThread;
	volatile bool m_StreamExitFlag;
	LPCB_TVS_STREAMFUNC m_StreamCallback;
	void* m_UserParam;
	stream_pid_client_map_t m_StreamPIDs;
	pid_filter pid_filter_;
	boost::mutex m_InputStreamMutex;
    dvblink::engine::command_queue m_CommandQueue;
	dvblink::engine::ts_packet_buffer stream_buffer_;
    tvs_stream_source_options_t options_;
    bool is_tuner_started_;

    //parameters of currently tuned LNB
    unsigned char m_prev_diseqc_value;
    std::wstring m_prev_diseqc_rawdata;
    unsigned char m_prev_command_byte;

    boost::posix_time::ptime prev_time_;

    //full ts stream for debugging purposes
    FILE* full_ts_f_;

    //extra stream callback
    LPCB_TVS_STREAMFUNC extra_stream_callback_;
	void* extra_user_param_;
};

///////////////////////////////////////////////////////////////////////////////
#endif //_TVSOURCESTREAMSRC_H_
