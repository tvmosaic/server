/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <dl_dynlib.h>
#include "dvbjapi.h"

typedef int (*dvbjapi_plugin_callback_f)(char *value, void* user_param);

class dvbjapi_plugin
{
public:
	dvbjapi_plugin(dvblink::filesystem_path_t& plugin_path);

	bool init(dvbjapi_plugin_callback_f cb, void* user_param);
	void term();
	
    void execute_command(const std::string& json_cmd);
	void write_stream(const unsigned char* buf, size_t len);

protected:
	dvblink::filesystem_path_t plugin_path_;
	dvblink::engine::dyn_lib plugin_;
    dvbjapi_plugin_callback_f dvbjapi_plugin_callback_;
    void* user_param_;

    //plugin interface
    dvbjapi_load_f dvbjapi_load_;
    dvbjapi_unload_f dvbjapi_unload_;
    dvbjapi_receive_from_parent_f dvbjapi_receive_from_parent_;
    dvbjapi_pid_data_f dvbjapi_pid_data_;

    static int STDCALL send_to_parent_callback(char *value, void* user_param);
    void send_get_version_cmd();
};
