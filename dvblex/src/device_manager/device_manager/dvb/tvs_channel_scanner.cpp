/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <map>
#include <sstream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>
#include <dl_logger.h>
#include <dl_ts.h>
#include <dl_atsc_qam_helper.h>
#include "tvs_channel_scanner.h"
#include "tvs_stream_src.h"
#include "tvs_tr_parser.h"

#define MAX_PMT_PIDS_TO_SCAN    4

using namespace std;
using namespace dvblink::engine;
using namespace dvblink::logging;

CTVSChannelScanner::CTVSChannelScanner(dvblink::tuner_t* tuner) :
    tuner_(tuner),
    m_StreamSource(NULL),
    scanner_id_(L"channel_scanner")
{
}

CTVSChannelScanner::~CTVSChannelScanner()
{
}

int CTVSChannelScanner::Init(dvblink::DL_E_TUNER_TYPES device_type, const TVSC_HeadendType& headend)
{
	int ret_val = 0;

	m_ScanningState = ETVS_SS_IDLE;
    options_.tuner_type_ = device_type;
    headend_ = headend;

	//load driver and start device
	if (m_StreamSource == NULL)
	{
		m_StreamSource = new CTVSStreamSource();
		m_StreamSource->Init(tuner_, options_, StreamFunction, this);
		ret_val = 1;
	}
	return ret_val;
}

int CTVSChannelScanner::Term()
{
	int ret_val = 0;
	//stop device and unload driver
	if (m_StreamSource != NULL)
	{
		m_StreamSource->Term();
		delete m_StreamSource;
        m_StreamSource = NULL;
		ret_val = 1;
	}
	return ret_val;
}

void CTVSChannelScanner::get_transponder_id(const std::wstring& tr_data, std::wstring& id)
{
    id.clear();

	dvblink::TTransponderInfo transponder;
	unsigned char diseqc_value;
    std::wstring raw_diseqc_data;

    if (CTVSTransponderParser::ParseTransponderData(options_.unicable_slots_, options_.tuner_type_,
	    headend_, tr_data, &transponder, &diseqc_value, raw_diseqc_data) > 0)
    {
        GetTransponderHash(options_.tuner_type_, transponder, raw_diseqc_data.c_str(), diseqc_value, id);
    }
}

int CTVSChannelScanner::Tune(const std::wstring& tr_data, bool bwait_for_lock, int timeout)
{
	int ret_val = 0;

	//Reset scanning state
	SwitchToIdle();

    //parse the transponder parameters from transponder string
    if (CTVSTransponderParser::ParseTransponderData(options_.unicable_slots_, options_.tuner_type_,
	    headend_, tr_data, &m_Transponder, &m_DiseqcValue, m_RawDiseqcData) > 0)
    {
	    if (m_StreamSource->TuneTransponder(&m_Transponder, m_DiseqcValue, m_RawDiseqcData.c_str()) > 0)
	    {
		    ret_val = 1;

		    if (bwait_for_lock)
		    {
			    ret_val = 0;
			    //adjust timeout for motorized dish (if needed)
			    timeout += m_StreamSource->GetAdditionalTuneDelay(m_DiseqcValue);

		        boost::posix_time::ptime start_time = boost::posix_time::microsec_clock::universal_time();

			    unsigned long sleep_ms = 100; //once in 100 ms
			    while (true)
			    {
				    dvblink::TSignalInfo signalInfo;
				    if (m_StreamSource->GetSignalStats(&signalInfo))
				    {
					    if (signalInfo.Locked)
					    {
                            log_info(L"CTVSChannelScanner::Tune. Signal stats: %1%, %2%, %3%") % signalInfo.Level % signalInfo.Locked % signalInfo.Quality;
						    ret_val = 1;
						    break;
					    }
				    }
			        boost::posix_time::ptime cur_time = boost::posix_time::microsec_clock::universal_time();
			        boost::posix_time::time_duration duration(cur_time - start_time);
			        if (duration.total_milliseconds() > timeout)
                    {
                        log_info(L"CTVSChannelScanner::Tune. Wait for lock timeout");
			        	break;
                    }

			        boost::this_thread::sleep(boost::posix_time::milliseconds(sleep_ms));
			    }
		    }
	    }
        else
	    {
		    log_error(L"CTVSChannelScanner::Tune. Failed to tune to a transponder");
	    }
    }
    else
    {
	    log_error(L"CTVSChannelScanner::Tune. Failed to parse transponder data %1%") % tr_data;
    }
	return ret_val;
}

bool CTVSChannelScanner::GetSignalStats(unsigned char* level, unsigned char* quality, unsigned char* locked)
{
	bool ret_val = false;

    dvblink::TSignalInfo signalInfo;	
    if (m_StreamSource->GetSignalStats(&signalInfo))
    {
	    *level = signalInfo.Level;
	    *quality = signalInfo.Quality;
	    *locked = signalInfo.Locked;
	    ret_val = true;
    }

	return ret_val;
}

int CTVSChannelScanner::Scan(int timeout, bool fta_channels_only, std::vector<TVSC_ServiceInfo>* expectedServices, TVSC_ChannelMap& scanned_services)
{
    int ret_val = 0;

    log_info(L"CTVSChannelScanner::Scan. Received channel scan request (%1%)") % fta_channels_only;

    if (m_ScanningState == ETVS_SS_IDLE)
    {
        //switch to SDT scan state
        SwitchToSDTScan();
        //Wait until wither timeout expires or ETVS_SS_SDT_COMPLETED state is reached
        unsigned long sleep_ticks = 100;
        int total_wait_ticks = 0;
        while (total_wait_ticks < timeout && m_ScanningState != ETVS_SS_SDT_COMPLETED)
        {
            boost::this_thread::sleep(boost::posix_time::milliseconds(sleep_ticks));
            total_wait_ticks += sleep_ticks;
        }

        if (options_.tuner_type_ == dvblink::TUNERTYPE_CLEARQAM && pat_services_.size() > 0 && 
            m_ScanningState != ETVS_SS_SDT_COMPLETED)
        {
            //if signal is clearqam, and no services were detected, but services were found in PAT - set state to ETVS_SS_SDT_COMPLETED
            log_info(L"CTVSChannelScanner::Scan. CQAM services were not found in VCT, but in PAT. Forcing state to successful completion");
            m_ScanningState = ETVS_SS_SDT_COMPLETED;
        }

        if (options_.tuner_type_ == dvblink::TUNERTYPE_DVBS && pat_services_.size() > 0 && 
            m_ScanningState != ETVS_SS_SDT_COMPLETED)
        {
            //if signal is dvb-s, and no services were detected via sdt, but they are present in PAT then it is probably NA satellite
            //scan SDT for all (current and other mux) services and then assign channel names by SID
            log_info(L"CTVSChannelScanner::Scan. Services were not found in SDT, but in PAT. Starting additional SDT scan for channel names");
            SwitchToSDTAllServiceScan();

            int sdt_timeout = 30000; //30 seconds in total should be enough
            int sdt_total_wait_ticks = 0;
            while (sdt_total_wait_ticks < sdt_timeout && m_ScanningState != ETVS_SS_SDT_ALL_SERVICES_COMPLETED)
            {
                boost::this_thread::sleep(boost::posix_time::milliseconds(sleep_ticks));
                sdt_total_wait_ticks += sleep_ticks;
            }
            //proceed further as usual
            m_ScanningState = ETVS_SS_SDT_COMPLETED;
        }

        if (m_ScanningState == ETVS_SS_SDT_COMPLETED)
        {
            //Parse information about identified channels
            ProcessCompletedSections(expectedServices, scanned_services);

            if (scanned_services.size() > 0)
            {
                //double timeout to search for a) encrypted channels and b) channel numbers
                total_wait_ticks = 0;

                //do channel number search only for dvb-c/t
                //this operation is put at the end because some providers (ziggo?!) do not report number of NIT sections to expect correctly
                //but, also, we  do it only for regular scans. For network scans channel numbers are already in expectedServices
                if (expectedServices == NULL)
                {
                    if (options_.tuner_type_ == dvblink::TUNERTYPE_DVBT ||
				        options_.tuner_type_ == dvblink::TUNERTYPE_DVBC)
                    {
                        //now do the channel number scan
                        SwitchToChNumScan(scanned_services);
                        while (total_wait_ticks < timeout && m_ScanningState != ETVS_SS_CHANNELSCAN_NIT_COMPLETED)
                        {
                            boost::this_thread::sleep(boost::posix_time::milliseconds(sleep_ticks));
                            total_wait_ticks += sleep_ticks;
                        }

                        if (m_ScanningState != ETVS_SS_CHANNELSCAN_NIT_COMPLETED)
                        {
                            //give a warning
                            log_warning(L"CTVSChannelScanner::Scan. Timeout is reached waiting for channel numbers to be scanned from NIT");
                        }

                        //Force NIT completed state
                        SwitchToChNumCompleted();

                        //process collected sections independently of scanning result
                        ProcessScannedChannelNumbers(scanned_services);

                    }
                }

                //scan CAT if any of the services on this transponder are encrypted
                if (total_wait_ticks >= timeout)
                    total_wait_ticks = 0;

                //switch to PAT scanning first
                SwitchToPATScan(scanned_services);
                while (total_wait_ticks < timeout && m_ScanningState != ETVS_SS_PAT_COMPLETED)
                {
                    boost::this_thread::sleep(boost::posix_time::milliseconds(sleep_ticks));
                    total_wait_ticks += sleep_ticks;
                }

                if (m_ScanningState == ETVS_SS_PAT_COMPLETED)
                {
                    //switch to PMT scanning
                    SwitchToPMTScan();
                    while (total_wait_ticks < timeout && m_ScanningState != ETVS_SS_PMT_COMPLETED)
                    {
                        boost::this_thread::sleep(boost::posix_time::milliseconds(sleep_ticks));
                        total_wait_ticks += sleep_ticks;
                    }
                    //process scanned CA descriptors
                    ProcessScannedCA(scanned_services);
                    if (m_ScanningState != ETVS_SS_PMT_COMPLETED)
                    {
                        //give a warning
                        log_warning(L"CTVSChannelScanner::Scan. Timeout is reached waiting for PMTs");
                    }
                }
                else
                {
                    //give a warning
                    log_warning(L"CTVSChannelScanner::Scan. Timeout is reached waiting for PAT");
                }
            }

            if (fta_channels_only)
                remove_non_fta_channels_from_scan(scanned_services);

            //Good return value independently of channel number scan status
            ret_val = 1;
        }
        else
        {
            //error
            log_error(L"CTVSChannelScanner::Scan. Timeout is reached waiting for services to be scanned");
        }

        //Switch to idle state to be ready for the next scanning request
        SwitchToIdle();
    }
    else
    {
        log_error(L"CTVSChannelScanner::Scan. Unable to start scanning in non-Idle state %1%") % m_ScanningState;
    }

    return ret_val;
}

bool CTVSChannelScanner::FindEncryptedChannel(TVSC_ChannelMap& services)
{
    bool ret_val = false;

    TVSC_ChannelMap::iterator it = services.begin();
    while (it != services.end())
    {
        if (it->second.encrypted != 0)
        {
            ret_val = true;
            break;
        }
        ++it;
    }

    return ret_val;
}

void CTVSChannelScanner::SwitchToSDTScan()
{
    log_info(L"CTVSChannelScanner::SwitchToSDTScan. Switching to SDT scan state");

    if (IsDVBDevice())
	{
		m_ServicesTablePID = PID_SDT; //SDT PID
		m_ServicesTableId = TID_SDT_ACT; //SDT section
	    //Set PID filter
	    m_StreamSource->AddPID(scanner_id_.c_str(), m_ServicesTablePID);
	}
    else
	{
		if (options_.tuner_type_ == dvblink::TUNERTYPE_ATSC)
		{
			m_ServicesTablePID = PID_ATSC_BASE; //Base PID
			m_ServicesTableId = TID_ATSC_VCT; //VCT section
	        //Set PID filter
	        m_StreamSource->AddPID(scanner_id_.c_str(), m_ServicesTablePID);
		} else
		{
			if (options_.tuner_type_ == dvblink::TUNERTYPE_CLEARQAM)
			{
				m_ServicesTablePID = INVALID_PID; //assign pid to invalid - we will detect on the fly which one is present (PID_ATSC_BASE or PID_CLEARQAM_BASE)
				m_ServicesTableId = TID_CLEARQAM_LVCT; //LVCT section
	            //Set PID filter for both PID_ATSC_BASE and PID_CLEARQAM_BASE
	            m_StreamSource->AddPID(scanner_id_.c_str(), PID_ATSC_BASE);
	            m_StreamSource->AddPID(scanner_id_.c_str(), PID_CLEARQAM_BASE);
			} else
			{
				log_error(L"CTVSChannelScanner::SwitchToSDTScan. Unsupported device type!");
			}
		}
	}

	{
		boost::unique_lock<boost::recursive_mutex> lock(m_cs);

        pat_services_.clear();

        //add PAT pid for fallback when no service tables are present in the stream
        m_StreamSource->AddPID(scanner_id_.c_str(), PID_PAT);
        //init PAT paser
        pat_parser_.Init(PID_PAT);

		//Initialize section payload parser (except for those cases when pid will be decided on the fly)
        if (m_ServicesTablePID != INVALID_PID)
		    m_SectionPayloadParser.Init(m_ServicesTablePID);
		//Change state to "scan for SDT"
		m_ScanningState = ETVS_SS_SDT;
	}
}

void CTVSChannelScanner::SwitchToPATScan(TVSC_ChannelMap& scanned_services)
{
    log_info(L"CTVSChannelScanner::SwitchToPATScan. Switching to PAT scan state");

    m_ChannelCAScanInfo.clear();
    TVSC_ChannelMap::iterator it = scanned_services.begin();
    while (it != scanned_services.end())
    {
        STVSChannelScanInfo si;
        si.ch_id = it->first;
        si.sid = it->second.sid;
        m_ChannelCAScanInfo.push_back(si);
        ++it;
    }

    //Set PID filter for PAT packets
	m_StreamSource->AddPID(scanner_id_.c_str(), 0x00);

	{
		boost::unique_lock<boost::recursive_mutex> lock(m_cs);

		//Initialize PAT payload parser
		m_SectionPayloadParser.Init(0x00);
		//Change state to "scan for PAT"
		m_ScanningState = ETVS_SS_PAT;
	}

}

void CTVSChannelScanner::SwitchToPATScanCompleted()
{
    log_info(L"CTVSChannelScanner::SwitchToPATScanCompleted. PAT scan is completed");

    boost::unique_lock<boost::recursive_mutex> lock(m_cs);

	m_ScanningState = ETVS_SS_PAT_COMPLETED;
}

void CTVSChannelScanner::SwitchToSDTAllServiceScan()
{
    log_info(L"CTVSChannelScanner::SwitchToSDTAllServiceScan. Switching to SDT full services scan state");

    sid_to_service_map_.clear();

    //Set PID filter for SDT packets
	m_StreamSource->AddPID(scanner_id_.c_str(), PID_SDT);

	{
		boost::unique_lock<boost::recursive_mutex> lock(m_cs);

		//Initialize payload parser
		m_SectionPayloadParser.Init(PID_SDT);
		//Change state to "scan for all SDT services"
		m_ScanningState = ETVS_SS_SDT_ALL_SERVICES;
	}
}

void CTVSChannelScanner::SwitchToSDTAllServiceScanCompleted()
{
    log_info(L"CTVSChannelScanner::SwitchToSDTAllServiceScanCompleted. SDT all services scan is completed");

    boost::unique_lock<boost::recursive_mutex> lock(m_cs);

	m_ScanningState = ETVS_SS_SDT_ALL_SERVICES_COMPLETED;
}

void CTVSChannelScanner::SwitchToPMTScan()
{
    log_info(L"CTVSChannelScanner::SwitchToPMTScan. Switching to PMT scan state");

    m_PMTMap.clear();

    int pid_count = 0;
    TTVSChannelScanInfoExList::iterator it = m_ChannelCAScanInfo.begin();
    while (it != m_ChannelCAScanInfo.end())
    {
        if (it->pmt_pid != 0)
        {
            it->payload_parser.Init(it->pmt_pid);
            m_PMTMap[it->pmt_pid] = it->pmt_pid;
            //add limited number of PMT pids initially
            if (pid_count < MAX_PMT_PIDS_TO_SCAN)
            {
	            m_StreamSource->AddPID(scanner_id_.c_str(), it->pmt_pid);
                it->state = sis_scanning;
            }
            pid_count++;
        }
        else
        {
            it->state = sis_completed;
        }
        ++it;
    }

    {
		boost::unique_lock<boost::recursive_mutex> lock(m_cs);

		//Change state to "scan for PMT"
		m_ScanningState = ETVS_SS_PMT;
    }
}

void CTVSChannelScanner::add_next_pmt_pid(unsigned short pid_to_remove)
{
    //check if all programs from that PMT are processed
    bool b_finished = true;
    TTVSChannelScanInfoExList::iterator it = m_ChannelCAScanInfo.begin();
    while (it != m_ChannelCAScanInfo.end())
    {
        if (it->pmt_pid == pid_to_remove)
        {
            if (it->state != sis_completed)
            {
                b_finished = false;
                break;
            }
        }
        ++it;
    }
    if (b_finished)
    {
        unsigned short pid_to_add = INVALID_PID;
        it = m_ChannelCAScanInfo.begin();
        while (it != m_ChannelCAScanInfo.end())
        {
            if (it->state == sis_pending)
            {
                pid_to_add = it->pmt_pid;
                it->state = sis_scanning;
                break;
            }
            ++it;
        }
        //remove prev pid and add new pid
        if (pid_to_add != INVALID_PID)
        {
            //this function is executed from the stream callback - use special functions for that
            m_StreamSource->cmdRemovePid(scanner_id_.c_str(), pid_to_remove);
            m_StreamSource->cmdAddPID(scanner_id_.c_str(), pid_to_add);
        }
    }
    
}

void CTVSChannelScanner::SwitchToPMTScanCompleted()
{
    log_info(L"CTVSChannelScanner::SwitchToPMTScanCompleted. PMT scan is completed");

	boost::unique_lock<boost::recursive_mutex> lock(m_cs);

	m_ScanningState = ETVS_SS_PMT_COMPLETED;
}

void CTVSChannelScanner::SwitchToChNumScan(TVSC_ChannelMap& scanned_services)
{
    log_info(L"CTVSChannelScanner::SwitchToChNumScan. Switching to NIT scan state");

    //Initialize network ID map
    m_ExpectedNIDMap.clear();
    //New services
    TVSC_ChannelMap::iterator it = scanned_services.begin();
    while (it != scanned_services.end())
    {
        if (m_ExpectedNIDMap.find(it->second.nid) == m_ExpectedNIDMap.end())
            m_ExpectedNIDMap[it->second.nid] = it->second.nid;

        ++it;
    }

	//Set PID filter for NIT packets
	m_StreamSource->AddPID(scanner_id_.c_str(), 0x10);

	{
		boost::unique_lock<boost::recursive_mutex> lock(m_cs);

		//Initialize NIT payload parser
		m_SectionPayloadParser.Init(0x10); //NIT PID
		//Change state
		m_ScanningState = ETVS_SS_CHANNELSCAN_NIT;
	}
}

void CTVSChannelScanner::SwitchToIdle()
{
	boost::unique_lock<boost::recursive_mutex> lock(m_cs);

	ResetDescriptors();
	m_ScanningState = ETVS_SS_IDLE;
}

void CTVSChannelScanner::SwitchToCompleted()
{
	boost::unique_lock<boost::recursive_mutex> lock(m_cs);
	m_ScanningState = ETVS_SS_SDT_COMPLETED;
}

void CTVSChannelScanner::SwitchToChNumCompleted()
{
	boost::unique_lock<boost::recursive_mutex> lock(m_cs);
	m_ScanningState = ETVS_SS_CHANNELSCAN_NIT_COMPLETED;
}

void __stdcall CTVSChannelScanner::StreamFunction(const unsigned char* Buf, unsigned long Len, void* user_param)
{
	CTVSChannelScanner* parent = (CTVSChannelScanner*)user_param;
	if ((Len % 188) == 0)
	{
	    boost::unique_lock<boost::recursive_mutex> lock(parent->m_cs, boost::try_to_lock);
	    if (lock.owns_lock())
	    {
			//add new packet(s) to accumulation buffer
			int c = Len / 188;
			for (int i=0; i<c; i++)
			{
				const unsigned char* curpack = Buf + i*188;
				switch (parent->m_ScanningState)
				{
					case ETVS_SS_IDLE:
					case ETVS_SS_SDT_COMPLETED:
					case ETVS_SS_NIT_COMPLETED:
					case ETVS_SS_CHANNELSCAN_NIT_COMPLETED:
					case ETVS_SS_PAT_COMPLETED:
					case ETVS_SS_PMT_COMPLETED:
						break;
                    case ETVS_SS_SDT_ALL_SERVICES:
                        {
                            unsigned short packet_pid = ts_process_routines::GetPacketPID(curpack);
							if (packet_pid == PID_SDT)
							{
								ts_payload_parser::ts_section_list found_sections;
								if (parent->m_SectionPayloadParser.AddPacket(curpack, 188, found_sections) > 0)
								{
									for (unsigned int ii=0; ii<found_sections.size(); ii++)
                                    {
                                        std::vector<STSServiceInfoEx> services;
                                        ts_process_routines::GetSDTServicesEx(found_sections[ii].section, found_sections[ii].length, services);
                                        for (unsigned int i=0; i<services.size(); i++)
                                            parent->sid_to_service_map_[services[i].sid] = services[i];
                                    }
									parent->m_SectionPayloadParser.ResetFoundSections(found_sections);
								}
							}
                        }
                        break;
					case ETVS_SS_SDT:
						{
                            unsigned short packet_pid = ts_process_routines::GetPacketPID(curpack);
                            if (parent->m_ServicesTablePID == INVALID_PID)
                            {
                                if (parent->options_.tuner_type_ == dvblink::TUNERTYPE_CLEARQAM && 
                                    (packet_pid == PID_ATSC_BASE || packet_pid == PID_CLEARQAM_BASE))
                                {
                                    //found psip pid for clearqam signal
                                    log_info(L"CTVSChannelScanner::StreamFunction. Found PID for packets with PSIP (%1%)") % packet_pid;
                                    parent->m_ServicesTablePID = packet_pid;
		                            parent->m_SectionPayloadParser.Init(parent->m_ServicesTablePID);
                                }
                            }

							if (packet_pid == parent->m_ServicesTablePID)
							{
								ts_payload_parser::ts_section_list found_sections;
								if (parent->m_SectionPayloadParser.AddPacket(curpack, 188, found_sections) > 0)
								{
									for (unsigned int ii=0; ii<found_sections.size(); ii++)
										parent->ProcessSDTPayload(found_sections[ii].section, found_sections[ii].length);
									parent->m_SectionPayloadParser.ResetFoundSections(found_sections);
								}
							}

                            if (packet_pid == PID_PAT && parent->pat_services_.size() == 0)
                            {
                                //process PAT sections
								ts_payload_parser::ts_section_list found_sections;
								if (parent->pat_parser_.AddPacket(curpack, 188, found_sections) > 0)
                                {
									for (unsigned int ii=0; ii<found_sections.size(); ii++)
									{
										std::vector<STSPATServiceInfo> services;
										ts_process_routines::GetPATStreamPIDs(found_sections[ii].section, found_sections[ii].length, services);
                                        parent->pat_services_.insert(parent->pat_services_.begin(), services.begin(), services.end());
                                    }
									parent->m_SectionPayloadParser.ResetFoundSections(found_sections);
                                }
                            }
						}
						break;
					case ETVS_SS_CHANNELSCAN_NIT:
						{
							if (ts_process_routines::GetPacketPID(curpack) == 0x10)
							{
								ts_payload_parser::ts_section_list found_sections;
								if (parent->m_SectionPayloadParser.AddPacket(curpack, 188, found_sections) > 0)
								{
									for (unsigned int ii=0; ii<found_sections.size(); ii++)
										parent->ProcessNITPayloadForChNum(found_sections[ii].section, found_sections[ii].length);
									parent->m_SectionPayloadParser.ResetFoundSections(found_sections);
								}
							}
						}
						break;
					case ETVS_SS_NIT:
						{
							if (ts_process_routines::GetPacketPID(curpack) == 0x10)
							{
								ts_payload_parser::ts_section_list found_sections;
								if (parent->m_SectionPayloadParser.AddPacket(curpack, 188, found_sections) > 0)
								{
									for (unsigned int ii=0; ii<found_sections.size(); ii++)
										parent->ProcessNITPayload(found_sections[ii].section, found_sections[ii].length);
									parent->m_SectionPayloadParser.ResetFoundSections(found_sections);
								}
							}
						}
						break;
					case ETVS_SS_PAT:
						{
							if (ts_process_routines::GetPacketPID(curpack) == 0x00)	//PAT
							{
								ts_payload_parser::ts_section_list found_sections;
								if (parent->m_SectionPayloadParser.AddPacket(curpack, 188, found_sections) > 0)
								{
									for (unsigned int ii=0; ii<found_sections.size(); ii++)
									{
										std::vector<STSPATServiceInfo> services;
										ts_process_routines::GetPATStreamPIDs(found_sections[ii].section, found_sections[ii].length, services);
										for (unsigned int srv_idx = 0; srv_idx < services.size(); srv_idx++)
										{
											for (unsigned int csi_idx = 0; csi_idx < parent->m_ChannelCAScanInfo.size(); csi_idx++)
											{
												if (parent->m_ChannelCAScanInfo[csi_idx].sid == services[srv_idx].sid)
												{
													parent->m_ChannelCAScanInfo[csi_idx].pmt_pid = services[srv_idx].pmt_pid;
													break;
												}
											}
										}
									}

									parent->m_SectionPayloadParser.ResetFoundSections(found_sections);

									parent->SwitchToPATScanCompleted();
								}
							}
						}
						break;
					case ETVS_SS_PMT:
						{
							unsigned short pid = ts_process_routines::GetPacketPID(curpack);
							if (parent->m_PMTMap.find(pid) != parent->m_PMTMap.end())
							{
								for (unsigned int csi_idx = 0; csi_idx < parent->m_ChannelCAScanInfo.size(); csi_idx++)
								{
									if (parent->m_ChannelCAScanInfo[csi_idx].pmt_pid == pid)
									{
										ts_payload_parser::ts_section_list found_sections;
										if (parent->m_ChannelCAScanInfo[csi_idx].payload_parser.AddPacket(curpack, 188, found_sections) > 0)
										{
											for (unsigned int ii=0; ii<found_sections.size(); ii++)
											{
												unsigned short sid;
												ts_process_routines::GetPMTSectionServiceID(found_sections[ii].section, found_sections[ii].length, sid);
												if (parent->m_ChannelCAScanInfo[csi_idx].state != sis_completed && sid == parent->m_ChannelCAScanInfo[csi_idx].sid)
												{
													std::vector<STSCADescriptorInfo> ca_desc_list;
													ts_process_routines::GetCADescriptorsFromPMT(found_sections[ii].section, found_sections[ii].length, ca_desc_list);
													parent->m_ChannelCAScanInfo[csi_idx].ca_desc_list.insert(parent->m_ChannelCAScanInfo[csi_idx].ca_desc_list.begin(),
														ca_desc_list.begin(), ca_desc_list.end());
													ca_desc_list.clear();
													std::vector<STSESInfo> stream_info;
													ts_process_routines::GetPMTStreamsInfo(found_sections[ii].section, found_sections[ii].length, stream_info);
													for (unsigned int str_idx = 0; str_idx < stream_info.size(); str_idx++)
													{
														if (stream_info[str_idx].ca_descriptors.size() > 0)
														{
															parent->m_ChannelCAScanInfo[csi_idx].ca_desc_list.insert(parent->m_ChannelCAScanInfo[csi_idx].ca_desc_list.begin(),
																stream_info[str_idx].ca_descriptors.begin(), stream_info[str_idx].ca_descriptors.end());
														}
													}
													//filter duplicate CA descriptors
													ca_desc_list.clear();
													std::map<unsigned long, unsigned long> ca_map;
													for (unsigned int ca_idx=0; ca_idx<parent->m_ChannelCAScanInfo[csi_idx].ca_desc_list.size(); ca_idx++)
													{
														unsigned long key = parent->m_ChannelCAScanInfo[csi_idx].ca_desc_list[ca_idx].CI_system_ID;
														key = key << 16;
														key |= parent->m_ChannelCAScanInfo[csi_idx].ca_desc_list[ca_idx].PID;
														if (ca_map.find(key) == ca_map.end())
														{
															ca_map[key] = key;
															ca_desc_list.push_back(parent->m_ChannelCAScanInfo[csi_idx].ca_desc_list[ca_idx]);
														}
													}
													parent->m_ChannelCAScanInfo[csi_idx].ca_desc_list = ca_desc_list;

													parent->m_ChannelCAScanInfo[csi_idx].state = sis_completed;
													
													parent->add_next_pmt_pid(pid);
												}
											}
											parent->m_ChannelCAScanInfo[csi_idx].payload_parser.ResetFoundSections(found_sections);
										}
									}
								}
								//check whether pmt scan is complete
								bool bFinished = true;
								for (unsigned int csi_idx = 0; csi_idx < parent->m_ChannelCAScanInfo.size(); csi_idx++)
								{
									if (parent->m_ChannelCAScanInfo[csi_idx].state != sis_completed)
									{
										bFinished = false;
										break;
									}
								}
								if (bFinished)
									parent->SwitchToPMTScanCompleted();
							}
						}
						break;
					default:
						break;
				}
			}
		}
	}
}

void CTVSChannelScanner::ProcessSDTPayload(unsigned char* payload, int payload_length)
{
	unsigned char cur_next;
	unsigned char table_id;
	unsigned short tid, nid;
	if (IsDVBDevice())
	{
		table_id = ts_process_routines::GetSDTTableId(payload, payload_length);
		cur_next = ts_process_routines::GetSDTCurNextIndicator(payload, payload_length);
		ts_process_routines::GetSDTSectionIds(payload, payload_length, nid, tid);
	}
    else
	{
		table_id = ts_process_routines::GetTVCTTableId(payload, payload_length);
		unsigned char version;
		ts_process_routines::GetTVCTSectionStats(payload, payload_length, version, cur_next);
		ts_process_routines::GetTVCTSectionTSID(payload, payload_length, tid);
		nid = 0;
	}
	//check table ID and current-next flag
	
	if (table_id == m_ServicesTableId && cur_next != 0)
	{
		unsigned long key = (((unsigned long)nid) << 16) | tid;
		if (m_ServiceListMap.find(key) == m_ServiceListMap.end())
		{
			//create a new list
			if (IsDVBDevice())
				m_ServiceListMap[key] = new CTVSServiceDVBListDescriptor();
			else
				m_ServiceListMap[key] = new CTVSServiceATSCListDescriptor();
		}
		//add section to already existing one
		m_ServiceListMap[key]->AddSection(payload, payload_length);
		//check whether parsing is complete
		bool bCompleted = true;
		TTVSServiceListMap::iterator it  = m_ServiceListMap.begin();
		while (it != m_ServiceListMap.end())
		{
			if (!it->second->IsCompleted())
			{
				bCompleted = false;
				break;
			}
			++it;
		}
		//change parser's state if completed
		if (bCompleted)
			SwitchToCompleted();
	}
}

void CTVSChannelScanner::ProcessCompletedSections(std::vector<TVSC_ServiceInfo>* expectedServices, TVSC_ChannelMap& scanned_services)
{
	std::vector<TVSC_ChannelType> service_list;
	//walk through all sections and extract identified services
	TTVSServiceListMap::iterator it = m_ServiceListMap.begin();
	while (it != m_ServiceListMap.end())
	{
		CTVSServiceListDescriptorBase* sd = it->second;
	    std::vector<TVSC_ChannelType> tmp_service_list;
		sd->GetServices(tmp_service_list);
        service_list.insert(service_list.begin(), tmp_service_list.begin(), tmp_service_list.end());
		++it;
	}
    if (options_.tuner_type_ == dvblink::TUNERTYPE_CLEARQAM && service_list.size() == 0)
    {
        log_info(L"CTVSChannelScanner::ProcessCompletedSections. No services were detected in VCT. Using PAT for service detection");
        //no services were detected for CQAM from VCT
        //use PAT services for this purpose
        convert_qam_pat_to_services(service_list);
    }

    if (options_.tuner_type_ == dvblink::TUNERTYPE_DVBS && service_list.size() == 0)
    {
        log_info(L"CTVSChannelScanner::ProcessCompletedSections. No services were detected in SDT. Using PAT for service detection");
        //no services were detected using SDT (NA satellites?)
        //use PAT services for this purpose
        convert_na_dvbs_pat_to_services(service_list);
    }

	for (unsigned int srv_idx = 0; srv_idx < service_list.size(); srv_idx++)
	{
		TVSC_ChannelType ch = service_list[srv_idx];
		ch.diseqc = m_DiseqcValue;
        ch.diseqc_rawdata = m_RawDiseqcData;
		ch.transponder_info = m_Transponder;
		//fill in service id based on its dvb scan parameters
        ch.id = create_channel_id(ch.transponder_info.dwFreq, ch.tid, ch.nid, ch.sid, options_.tuner_type_);

        if (IsServiceExpected(expectedServices, ch))
        {
            scanned_services[ch.id] = ch;
        }
	}

	log_info(L"CTVSChannelScanner::ProcessCompletedSections. Channel scan is completed. Found %1% channels (filtered from %2%)") % scanned_services.size() % service_list.size();
}

void CTVSChannelScanner::convert_qam_pat_to_services(std::vector<TVSC_ChannelType>& service_list)
{
    for (size_t i=0; i<pat_services_.size(); i++)
    {
        TVSC_ChannelType channel;
        channel.nid = 0;
        channel.tid = pat_services_[i].tid;
        channel.sid = pat_services_[i].sid;
        channel.type = TSVC_CMT_TV;
        channel.encrypted = 0;
        int ph_ch = qam_center_frequency_to_channel(m_Transponder.dwFreq * 1000);
        channel.mj_num = ph_ch;
        channel.mn_num = pat_services_[i].sid;
        std::wstringstream str;
        str << L"Service " << ph_ch << L"-" << pat_services_[i].sid;
        channel.name = str.str();
        service_list.push_back(channel);
    }
}

void CTVSChannelScanner::convert_na_dvbs_pat_to_services(std::vector<TVSC_ChannelType>& service_list)
{
    for (size_t i=0; i<pat_services_.size(); i++)
    {
        TVSC_ChannelType channel;
        channel.nid = 0;
        channel.tid = pat_services_[i].tid;
        channel.sid = pat_services_[i].sid;
        channel.type = TSVC_CMT_TV;
        channel.encrypted = 0;
        channel.mj_num = pat_services_[i].sid;
        channel.mn_num = 0;
        if (sid_to_service_map_.find(pat_services_[i].sid) != sid_to_service_map_.end())
        {
            channel.name = sid_to_service_map_[pat_services_[i].sid].name;
            channel.type = CTVSServiceListDescriptorBase::GetServiceTypeFromDVBType(sid_to_service_map_[pat_services_[i].sid].type);
        } else
        {
            std::wstringstream str;
            str << L"Service " << pat_services_[i].sid;
            channel.name = str.str();
        }
        service_list.push_back(channel);
    }
}

bool CTVSChannelScanner::IsServiceExpected(std::vector<TVSC_ServiceInfo>* expectedServices, TVSC_ChannelType& service)
{
    bool ret_val = false;

    if (expectedServices == NULL)
        return true;

    for (unsigned int i=0; i<expectedServices->size(); i++)
    {
        if (expectedServices->at(i).tid == service.tid &&
            expectedServices->at(i).sid == service.sid)
        {
            //copy channel number from expected services as well
            service.mj_num = expectedServices->at(i).ch_num;

            ret_val = true;
            break;
        }
    }

    return ret_val;
}

void CTVSChannelScanner::ProcessNITPayloadForChNum(unsigned char* payload, int payload_length)
{
	unsigned char table_id = ts_process_routines::GetNITTableId(payload, payload_length);
    unsigned char cur_next, version;
    ts_process_routines::GetNITSectionStats(payload, payload_length, version, cur_next);
    unsigned short nid;
    ts_process_routines::GetNetworkIDFromNIT(payload, payload_length, nid);

	//check table ID for current transponder and current-next flag
	if ((table_id == 0x40 || table_id == 0x41) && cur_next != 0)
	{
        //we only finish processing if carousel is completed
        bool bCarouselCompleted = false;
        //get all transport streams from this section
        std::vector<STSNITTSInfo> streams_info;
        ts_process_routines::GetTSInfoFromNIT(payload, payload_length, streams_info);
        //check whether this network contains the networks that we need
        bool bNeeded = false;
        for (unsigned int i=0; i<streams_info.size(); i++)
        {
            if (m_ExpectedNIDMap.find(streams_info[i].onid) != m_ExpectedNIDMap.end())
            {
                bNeeded = true;
                break;
            }
        }
        if (bNeeded)
        {
            //check the services that this NIT section has
            std::vector<SDVBTLCNDesc> lcn_services;
            ts_process_routines::GetLCNFromNIT(payload, payload_length, lcn_services);
//            RemoveFoundLCNServices(lcn_services);

		    if (m_NITSectionsMap.find(nid) == m_NITSectionsMap.end())
		    {
                m_NITSectionsMap[nid] = new CTVSNITScanner();
            }
            else
            {
                bCarouselCompleted = true;
            }
	        //add section
	        m_NITSectionsMap[nid]->AddSection(payload, payload_length);
        }
	    //check whether parsing is complete
	    bool bCompleted = true;
	    TTVSNITListMap::iterator it  = m_NITSectionsMap.begin();
	    while (it != m_NITSectionsMap.end())
	    {
		    if (!it->second->IsCompleted())
		    {
			    bCompleted = false;
			    break;
		    }
		    ++it;
	    }
	    //change parser's state if completed
        //carousel finished and all sections were collected OR all services are found
	    if ((bCompleted && bCarouselCompleted))
		    SwitchToChNumCompleted();
	}
}

void CTVSChannelScanner::ProcessScannedChannelNumbers(TVSC_ChannelMap& scanned_services)
{
    std::map<unsigned short, TDVBTLCNList> networks_lcn;
    //Retrieve list of all found lcns
    TTVSNITListMap::iterator it  = m_NITSectionsMap.begin();
    while (it != m_NITSectionsMap.end())
    {
        TDVBTLCNList lcn_list;
        it->second->GetChannelNumbers(lcn_list);
        networks_lcn[it->first] = lcn_list;
        ++it;
    }

    //find required network
    TDVBTLCNList* lcn_services = NULL;
    //try channels' own network first
    if (scanned_services.size() > 0 && networks_lcn.find(scanned_services.begin()->second.nid) != networks_lcn.end())
    {
        lcn_services = &(networks_lcn[scanned_services.begin()->second.nid]);
        log_info(L"CTVSChannelScanner::ProcessScannedChannelNumbers. Using LCN map for network %1%") % scanned_services.begin()->second.nid;
    }

    //if not found - take the first available
    if (lcn_services == NULL)
    {
        if (networks_lcn.size() > 0)
        {
            lcn_services = &(networks_lcn.begin()->second);
            log_info(L"CTVSChannelScanner::ProcessScannedChannelNumbers. Using LCN map for network %1%") % networks_lcn.begin()->first;
        }
    }

    int match_count = 0;

    if (lcn_services != NULL && lcn_services->size() > 0)
    {
        //match services from SDT and NIT
        TVSC_ChannelMap::iterator srv_it = scanned_services.begin();
        while (srv_it != scanned_services.end())
        {
            for (unsigned int i=0; i<lcn_services->size(); i++)
            {
                if (srv_it->second.nid == lcn_services->at(i).onid && 
                    srv_it->second.tid == lcn_services->at(i).ts_id && 
                    srv_it->second.sid == lcn_services->at(i).service_id)
                {
                    srv_it->second.mj_num = lcn_services->at(i).lcn;
                    log_info(L"CTVSChannelScanner::ProcessScannedChannelNumbers. Service %1%: LCN %2%") % srv_it->second.sid % srv_it->second.mj_num;
                    match_count += 1;
                }
            }
            ++srv_it;
        }
        log_info(L"CTVSChannelScanner::ProcessScannedChannelNumbers. %1% LCN services were found out of which %2% were matched") % lcn_services->size() % match_count;
    }
    
}

void CTVSChannelScanner::ProcessScannedCA(TVSC_ChannelMap& scanned_services)
{
    if (m_ChannelCAScanInfo.size() > 0)
    {
        for (unsigned int i=0; i<m_ChannelCAScanInfo.size(); i++)
        {
            if (m_ChannelCAScanInfo[i].pmt_pid == 0 || m_ChannelCAScanInfo[i].state != sis_completed)
                continue;
            //find this channel in all channels
            if (scanned_services.find(m_ChannelCAScanInfo[i].ch_id.c_str()) != scanned_services.end())
            {
                TVSC_ChannelType* ch = &(scanned_services[m_ChannelCAScanInfo[i].ch_id.c_str()]);
                if (m_ChannelCAScanInfo[i].ca_desc_list.size() > 0)
                {
                    ch->encrypted = 1;
                    ch->ca_descriptors = m_ChannelCAScanInfo[i].ca_desc_list;
                    //check if ecm_pid is still valid
                    if (ch->ecm_pid != 0)
                    {
                        bool bFound = false;
                        for (unsigned int ii=0; ii<ch->ca_descriptors.size(); ii++)
                        {
                            if (ch->ecm_pid == ch->ca_descriptors[ii].PID)
                            {
                                bFound = true;
                                break;
                            }
                        }
                        if (!bFound)
                            ch->ecm_pid = 0;
                    }
                }
                else
                {
                    ch->ca_descriptors.clear();
                    ch->ecm_pid = 0;
                }
            }
        }
    }
}

void CTVSChannelScanner::ResetDescriptors()
{
	TTVSServiceListMap::iterator it = m_ServiceListMap.begin();
	while (it != m_ServiceListMap.end())
	{
		delete it->second;
		++it;
	}
	m_ServiceListMap.clear();

    TTVSNITListMap::iterator nit_it = m_NITSectionsMap.begin();
    while (nit_it != m_NITSectionsMap.end())
    {
		delete nit_it->second;
        ++nit_it;
    }
	m_NITSectionsMap.clear();
}

bool CTVSChannelScanner::IsDVBDevice()
{
	if (options_.tuner_type_ == dvblink::TUNERTYPE_DVBS ||
		options_.tuner_type_ == dvblink::TUNERTYPE_DVBC ||
		options_.tuner_type_ == dvblink::TUNERTYPE_DVBT)
		return true;

	return false;
}

int CTVSChannelScanner::ScanTransponderList(int timeout, TVSC_NetworkScanList& network_list)
{
	int ret_val = 0;
    timeout = 10000;

	if (!IsDVBDevice())
	{
		log_error(L"CTVSChannelScanner::ScanTransponderList. Network scan is not supported for non-DVB networks");
		return 0;
	}

	log_info(L"CTVSChannelScanner::ScanTransponderList. Received network scan request");

	if (m_ScanningState == ETVS_SS_IDLE)
	{
		network_list.clear();
		//switch to NIT scan state
		SwitchToNITScan();
		//Wait until wither timeout expires or ETVS_SS_NIT_COMPLETED state is reached
		unsigned long sleep_ticks = 100;
		int total_wait_ticks = 0;
		while (total_wait_ticks < timeout && m_ScanningState != ETVS_SS_NIT_COMPLETED)
		{
			boost::this_thread::sleep(boost::posix_time::milliseconds(sleep_ticks));
			total_wait_ticks += sleep_ticks;
		}

        //Switch to idle state to be ready for the next scanning request
		SwitchToIdle();

        //currently ETVS_SS_SDT_COMPLETED is not triggered - we just 
        //collect as many transponders as we can within timeout

        //Parse information about transponders from system delivery descriptors
		ProcessCollectedDeliveryDescriptors(network_list);
		ret_val = 1;

	}
    else
	{
		log_error(L"CTVSChannelScanner::ScanTransponderList. Unable to start network scan in non-Idle state %1%") % m_ScanningState;
	}
	return ret_val;
}

void CTVSChannelScanner::SwitchToNITScan()
{
	//Set PID filter for NIT
	m_StreamSource->AddPID(scanner_id_.c_str(), 0x10);

	{
		boost::unique_lock<boost::recursive_mutex> lock(m_cs);

		m_DeliverySystems.clear();
		//Initialize SDT payload parser
		m_SectionPayloadParser.Init(0x10);
		//Change state to "scan for NIT"
		m_ScanningState = ETVS_SS_NIT;
	}
}

void CTVSChannelScanner::ProcessNITPayload(unsigned char* payload, int payload_length)
{
    //get delivery descriptors from this NIT
    std::vector<STSDeliverySystemInfo> delivery_systems;
    ts_process_routines::GetDeliverySystemsFromNIT(payload, payload_length, delivery_systems);
    for (unsigned int i=0; i<delivery_systems.size(); i++)
        m_DeliverySystems.push_back(delivery_systems[i]);
}

void CTVSChannelScanner::ProcessCollectedDeliveryDescriptors(TVSC_NetworkScanList& network_list)
{
    network_list.clear();

    std::map<std::wstring, STSDeliverySystemInfo> ds_map; //this map is needed to remove duplicates in delivery systems

    for (unsigned int i=0; i<m_DeliverySystems.size(); i++)
    {
        TVSC_TransponderType transponder;
        CTVSTransponderParser::GetTransponderFromDeliveryInfo(m_DeliverySystems[i], transponder);
        std::wstringstream strbuf;
        strbuf << transponder.data << L":" << m_DeliverySystems[i].nid;
        if (ds_map.find(strbuf.str()) == ds_map.end())
        {
            //this transponder was not added yet to the map
            ds_map[strbuf.str()] = m_DeliverySystems[i];
        }
    }
    //walk through the refined delivery system map and create transponder list for each network
    std::map<unsigned short, TVSC_NetworkScanData> tr_map;
    std::map<std::wstring, STSDeliverySystemInfo>::iterator it = ds_map.begin();
    while (it != ds_map.end())
    {
        TVSC_TransponderType transponder;
        CTVSTransponderParser::GetTransponderFromDeliveryInfo(it->second, transponder);
        if (tr_map.find(it->second.nid) == tr_map.end())
        {
            tr_map[it->second.nid] = TVSC_NetworkScanData();
            tr_map[it->second.nid].nid = it->second.nid;
            tr_map[it->second.nid].name = it->second.networkName;
        }
        TVSC_TransponderScanData scanData;
        scanData.transponder = transponder;
        scanData.bFilterServices = it->second.bFilterServices;
        for (unsigned int i=0; i<it->second.expectedServices.size(); i++)
        {
            TVSC_ServiceInfo si;
            si.nid = it->second.expectedServices[i].onid;
            si.tid = it->second.expectedServices[i].tid;
            si.sid = it->second.expectedServices[i].sid;
            si.ch_num = it->second.expectedServices[i].ch_num;
            scanData.expectedServices.push_back(si);
        }
        tr_map[it->second.nid].transponders.push_back(scanData);
        ++it;
    }
    //walk through resulting map and create a list out of it
    std::map<unsigned short, TVSC_NetworkScanData>::iterator it_map = tr_map.begin();
    while (it_map != tr_map.end())
    {
        network_list.push_back(it_map->second);
        ++it_map;
    }
}

void CTVSChannelScanner::remove_non_fta_channels_from_scan(TVSC_ChannelMap& scanned_services)
{
    std::vector<std::wstring> services_to_remove;
    TVSC_ChannelMap::iterator it = scanned_services.begin();
    while (it != scanned_services.end())
    {
        if (scanned_services.find(it->first) != scanned_services.end())
        {
            if (scanned_services[it->first].encrypted != 0)
                services_to_remove.push_back(it->first);
        }
        ++it;
    }

    if (services_to_remove.size() > 0)
    {
        for (size_t i=0; i<services_to_remove.size(); i++)
        {
            scanned_services.erase(services_to_remove[i]);
        }
    }
}

