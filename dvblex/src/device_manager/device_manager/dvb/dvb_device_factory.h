/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <drivers/tuner_factory.h>

#include "../device.h"
#include "../device_factory.h"
#include <dl_channel_info.h>

namespace dvblex { 

class dvb_device_factory_t : public device_factory_t
{
protected:
    struct internal_id_t
    {
        std::string tuner_factory_name_;
        std::string device_path_;
        int frontend_idx_;
    };

    typedef std::map<std::string, dvblink::tuner_factory_t*> tuner_factory_map_t;

public:
    static const char* get_name(){return "dvb";}

    dvb_device_factory_t(const dvblink::filesystem_path_t& device_config_path);
    virtual ~dvb_device_factory_t();

    virtual bool get_device_list(const dvblink::device_uuid_t& u, device_descriptor_list_t& device_list);
    virtual device_t* create_device(const dvblink::device_id_t& device_id, directory_settings_obj_t& dir_settings);
    virtual bool get_device_info(const dvblink::device_id_t& device_id, device_descriptor_t& device_info);
    virtual source_type_e get_source_type_from_tuning_params(const dvblink::channel_tuning_params_t& tune_params);

    virtual std::string get_device_factory_name(){return get_name();}

    virtual void get_aux_list(dvblink::aux_module_list_t& aux_list);
    virtual bool get_device_configurable_props(const dvblink::device_id_t& device_id, configurable_device_props_t& props);

protected:
    tuner_factory_map_t tuner_factory_map_;

    dvblink::tuner_factory_t* get_factory_from_name(const std::string& name);
    dvblink::tuner_factory_t* create_factory_from_name(const std::string& name);

    void path_to_internal_id(const std::string& path, internal_id_t& internal_id);
    std::string internal_id_to_path(const internal_id_t& internal_id);
    dvblink::device_uuid_t device_uuid_to_path(const std::string& factory, const std::string& device_uuid);
    void path_to_device_uuid(const dvblink::device_uuid_t& p, std::string& factory, std::string& device_uuid);
};

}
