/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <dl_logger.h>
#include "tvs_stream_checker.h"

using namespace std;
using namespace dvblink::engine;
using namespace dvblink::logging;

stream_checker::stream_checker(unsigned short pid_to_check) :
	pid_to_check_(pid_to_check)
{
	is_streaming_ = false;
}

stream_checker::~stream_checker()
{
}

void stream_checker::reset()
{
	boost::unique_lock<boost::mutex> lock(lock_);
	is_streaming_ = false;
}

bool stream_checker::is_streaming()
{
	boost::unique_lock<boost::mutex> lock(lock_);
	return is_streaming_;
}

void stream_checker::write_stream(const unsigned char* buf, unsigned long len)
{
	//if we already found a packetand streaming - do not check again
	if (is_streaming_)
		return;

	boost::unique_lock<boost::mutex> lock(lock_);
    unsigned long packet_num = len / TS_PACKET_SIZE;
	for (unsigned long i=0; i<packet_num; i++)
	{
		const unsigned char* cur = buf + i*TS_PACKET_SIZE;

        if (ts_process_routines::GetPacketPID(cur) == pid_to_check_)
			is_streaming_ = true;

		break;
	}
}
