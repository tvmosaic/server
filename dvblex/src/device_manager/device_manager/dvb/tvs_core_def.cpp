/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/algorithm/string.hpp>
#include "tvs_core_def.h"

using namespace dvblink::engine;

bool TVSC_DeviceInfo::IsSameTunerDevice(TVSC_DeviceInfo& other_device)
{
    bool retval = false;
    //check devicePath first
    if (devicePath.size() > 0 && other_device.devicePath.size() > 0)
    {
        retval = boost::iequals(devicePath.c_str(), other_device.devicePath.c_str()) && other_device.frontend_idx == frontend_idx;
    }
    else
    {
        //check device index, forntend index and name
        retval = boost::iequals(driverName.c_str(), other_device.driverName.c_str()) && deviceIndex == other_device.deviceIndex && 
            other_device.frontend_idx == frontend_idx;
    }
    return retval;
}

bool TVSC_TunerTypeOverride::IsTheSameDevice(TVSC_DeviceInfo& device)
{
    bool retval = false;
    //check devicePath first
    if (devicePath.size() > 0 && device.devicePath.size() > 0)
    {
        retval = boost::iequals(devicePath.c_str(), device.devicePath.c_str()) && device.frontend_idx == frontend_idx;
    }
    else
    {
        //check device index and name
        retval = boost::iequals(driverName.c_str(), device.driverName.c_str()) && deviceIndex == device.deviceIndex && 
            device.frontend_idx == frontend_idx;
    }
    return retval;
}


extern const TVSC_DeviceType cDeviceTypeDefaults[] =
{
	{1, L"Satellite (DVB-S/S2)"},
	{2, L"Cable (DVB-C)"},
	{3, L"Terrestrial (DVB-T/T2)"},
	{4, L"ATSC"},
	{5, L"Digital Cable (US)"},
	{0xFF, L""}
};

const TVSC_LNBType cLNBTypeDefaults[] = 
	{
		{L"KuLinearUniversal", L"Ku-Linear (Universal)", 9750000, 10600000, 11700000},
		{L"KuCircular", L"Ku-Circular", 10750000, 0, 0},
		{L"CBand", L"C-Band", 5150000, 0, 0},
		{L"NABandstackedDPKuHi", L"NA Bandstacked DP Ku-Hi (DSS)", 11250000, 14350000, 0},
		{L"NABandstackedDPKuLo", L"NA Bandstacked DP Ku-Lo (FSS)", 10750000, 13850000, 0},
		{L"NABandstackedKuHi", L"NA Bandstacked Ku-Hi (DSS)", 11250000, 10675000, 0},
		{L"NABandstackedKuLo", L"NA Bandstacked Ku-Lo (FSS)", 10750000, 10175000, 0},
		{L"NABandstackedC", L"NA Bandstacked C", 5150000, 5750000, 0},
		{L"NALegacy", L"NA Legacy", 11250000, 0, 0},
		{L"", L"", 0, 0, 0}
	};

const TVSC_DiseqcType cDiseqcTypeDefaults[] = 
{
	{L"None", L"None"},
	{L"SimpleA", L"Simple A"},
	{L"SimpleB", L"Simple B"},
	{L"Level1AA", L"Pos A/A (LNB1 Diseqc1.0)"},
	{L"Level1BA", L"Pos B/A (LNB2 Diseqc1.0)"},
	{L"Level1AB", L"Pos A/B (LNB3 Diseqc1.0)"},
	{L"Level1BB", L"Pos B/B (LNB4 Diseqc1.0)"},
	{L"Diseqc11LNB1", L"LNB1 Diseqc1.1"},
	{L"Diseqc11LNB2", L"LNB2 Diseqc1.1"},
	{L"Diseqc11LNB3", L"LNB3 Diseqc1.1"},
	{L"Diseqc11LNB4", L"LNB4 Diseqc1.1"},
	{L"Diseqc11LNB5", L"LNB5 Diseqc1.1"},
	{L"Diseqc11LNB6", L"LNB6 Diseqc1.1"},
	{L"Diseqc11LNB7", L"LNB7 Diseqc1.1"},
	{L"Diseqc11LNB8", L"LNB8 Diseqc1.1"},
	{L"Diseqc11LNB9", L"LNB9 Diseqc1.1"},
	{L"Diseqc11LNB10", L"LNB10 Diseqc1.1"},
	{L"Diseqc11LNB11", L"LNB11 Diseqc1.1"},
	{L"Diseqc11LNB12", L"LNB12 Diseqc1.1"},
	{L"Diseqc11LNB13", L"LNB13 Diseqc1.1"},
	{L"Diseqc11LNB14", L"LNB14 Diseqc1.1"},
	{L"Diseqc11LNB15", L"LNB15 Diseqc1.1"},
	{L"Diseqc11LNB16", L"LNB16 Diseqc1.1"},
	{L"", L""}
};

bool IsBandstackedLNB(const wchar_t* lnbtype)
{
	if (boost::iequals(lnbtype, L"NABandstackedDPKuHi") ||
			boost::iequals(lnbtype, L"NABandstackedDPKuLo") ||
			boost::iequals(lnbtype, L"NABandstackedKuHi") ||
			boost::iequals(lnbtype, L"NABandstackedKuLo") ||
			boost::iequals(lnbtype, L"NABandstackedC"))
		return true;
	return false;
}

bool IsBandstackedFSSLNB(const wchar_t* lnbtype)
{
	if (boost::iequals(lnbtype, L"NABandstackedDPKuLo") ||
			boost::iequals(lnbtype, L"NABandstackedKuLo"))
		return true;
	return false;
}

const std::wstring create_channel_id(unsigned long freq, unsigned short nid, unsigned short tid, unsigned short sid, dvblink::DL_E_TUNER_TYPES device_type)
{
    std::wstringstream strbuf;
    strbuf << (unsigned long)device_type << ":" << freq << ":" << tid << ":" << nid << ":" << sid;
    return strbuf.str();
}

void MakeTransponderString(int device_type, dvblink::TTransponderInfo& trinfo, std::wstring& resstr)
{
    std::wstringstream strbuf;
    strbuf << trinfo.dwFreq;
    resstr = strbuf.str();
}

void SetChannelDefaults(TVSC_ChannelType& chinfo)
{
	chinfo.diseqc = dvblink::DISEQC_NONE;
	chinfo.nid = 0;
	chinfo.tid = 0;
	chinfo.sid = 0;
	chinfo.type = TSVC_CMT_TV;
	chinfo.encrypted = 0;
	chinfo.name = L"";
	chinfo.provider = L"";
	chinfo.mj_num = -1;
	chinfo.mn_num = 0;
	SetTransponderDefaults(chinfo.transponder_info);
    chinfo.ecm_pid = 0;
}

void SetTransponderDefaults(dvblink::TTransponderInfo& trinfo)
{
	memset(&trinfo, 0, sizeof(trinfo));
	trinfo.dwSize = sizeof(trinfo);
}

void GetCASystemName(unsigned short CASysId, std::wstring& sys_name)
{
    switch (CASysId)
    {
    case 0x4800:
        sys_name = L"Accessgate";
        break;
    case 0x4A20:
        sys_name = L"AlphaCrypt";
        break;
    case 0x1702:
    case 0x1722:
    case 0x1762:
        sys_name = L"BetaCrypt";
        break;
    case 0x2600:
        sys_name = L"BISS";
        break;
    case 0x4900:
        sys_name = L"China Crypt";
        break;
    case 0x22F0:
        sys_name = L"Codicrypt";
        break;
    case 0x0B00:
        sys_name = L"Conax";
        break;
    case 0x0D00:
    case 0x0D02:
    case 0x0D03:
    case 0x0D05:
    case 0x0D07:
    case 0x0D20:
        sys_name = L"Cryptoworks";
        break;
    case 0x4ABF:
        sys_name = L"CTI-CAS";
        break;
    case 0x0700:
        sys_name = L"DigiCipher";
        break;
    case 0x4A70:
        sys_name = L"DreamCrypt";
        break;
    case 0x4A10:
        sys_name = L"EasyCas";
        break;
    case 0x0602:
    case 0x0604:
    case 0x0606:
    case 0x0608:
    case 0x0622:
    case 0x0626:
        sys_name = L"Irdeto";
        break;
    case 0x4AA1:
        sys_name = L"KeyFly";
        break;
    case 0x0100:
        sys_name = L"Seca Mediaguard";
        break;
    case 0x1800:
    case 0x1801:
    case 0x1810:
    case 0x1830:
        sys_name = L"Nagravision";
        break;
    case 0x4A02:
        sys_name = L"Novel-SuperTV";
        break;
    case 0x4AD4:
        sys_name = L"OmniCrypt";
        break;
    case 0x0E00:
        sys_name = L"PowerVu";
        break;
    case 0x1000:
        sys_name = L"RAS";
        break;
    case 0x4AE0:
        sys_name = L"RossCrypt";
        break;
    case 0x0101:
        sys_name = L"RusCrypto";
        break;
    case 0x4A60:
    case 0x4A61:
    case 0x4A63:
        sys_name = L"SkyCrypt";
        break;
    case 0x4A80:
        sys_name = L"ThalesCrypt";
        break;
    case 0x0500:
        sys_name = L"Viaccess";
        break;
    case 0x0911:
    case 0x0919: 
    case 0x0960:
    case 0x0961:
        sys_name = L"NDS Videoguard";
        break;
    case 0x4AD0:
    case 0x4AD1:
        sys_name = L"X-Crypt";
        break;
    case 0x5500:
        sys_name = L"Z-Crypt";
        break;
    default:
        sys_name = L"Unknown";
        break;
    }
}

void GetDeviceTypeName(int type, std::wstring& type_name)
{
    type_name = L"Unknown";
    unsigned int i=0;
    while (cDeviceTypeDefaults[i].TypeID != 0xFF)
    {
        if (cDeviceTypeDefaults[i].TypeID == type)
        {
            type_name = cDeviceTypeDefaults[i].TypeName;
            break;
        }
        ++i;
    }
}

void GetTransponderHash(int device_type, dvblink::TTransponderInfo& transponder_info, const wchar_t* diseqc_rawdata, unsigned char diseqc, std::wstring& tr_hash)
{
    tr_hash = L"";

    switch (device_type)
    {
    case dvblink::TUNERTYPE_DVBS:
        {
            std::wstringstream strbuf;
            strbuf << transponder_info.dwFreq << L":" << transponder_info.dwModulation  << L":" << transponder_info.Pol << L":" << transponder_info.dwSr  << L":";
            strbuf << transponder_info.dwLOF << L":" << transponder_info.dwLnbKHz  << L":" << transponder_info.dwFec;
            strbuf << L":" << diseqc;

            std::wstring rawdata_str = diseqc_rawdata;
            boost::to_upper(rawdata_str);
            strbuf << L":" << rawdata_str;
            tr_hash = strbuf.str();
        }
        break;
    case dvblink::TUNERTYPE_DVBC:
        {
            std::wstringstream strbuf;
            strbuf << transponder_info.dwFreq << L":" << transponder_info.dwSr << L":" << transponder_info.dwLOF;
            tr_hash = strbuf.str();
        }
        break;
    case dvblink::TUNERTYPE_DVBT:
        {
            std::wstringstream strbuf;
            strbuf << transponder_info.dwFreq << L":" << transponder_info.dwLOF;
            tr_hash = strbuf.str();
        }
        break;
    case dvblink::TUNERTYPE_ATSC:
        {
            std::wstringstream strbuf;
            strbuf << transponder_info.dwFreq;
            tr_hash = strbuf.str();
        }
        break;
    case dvblink::TUNERTYPE_CLEARQAM:
        {
            std::wstringstream strbuf;
            strbuf << transponder_info.dwFreq;
            tr_hash = strbuf.str();
        }
        break;
    }
}

