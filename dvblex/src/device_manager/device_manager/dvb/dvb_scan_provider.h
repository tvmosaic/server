/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <dl_filesystem_path.h>

#include "../scan_provider.h"

namespace dvblex { 

enum provider_scan_type_e
{
    pst_transponder_list = 0x00000001,
    pst_fast_search = 0x00000002
};

class dvb_scan_provider_t : public scan_provider_t
{
protected:
    struct internal_id_t
    {
        std::string internal_id_;
        provider_scan_type_e scan_type_;
        source_type_e standard_;
    };

public:
    dvb_scan_provider_t(const dvblink::filesystem_path_t& provider_storage_path);

    static const char* get_name(){return "dvb";}

    virtual void get_providers(source_type_e standard, provider_info_map_t& providers);
    virtual bool get_provider_details(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, provider_info_t& provider_info, provider_scan_list_t& scan_list);
    virtual std::string get_scan_provider_name(){return get_name();}
    virtual void fill_headend_info(const provider_info_t& provider_info, const concise_param_map_t& scanner_settings, headend_info_t& headend_info);
    virtual bool get_rescan_settings(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, parameters_container_t& settings);

protected:
    dvblink::filesystem_path_t provider_storage_path_;

    void id_to_internal_id(const std::string& id, internal_id_t& internal_id);
    std::string internal_id_to_id(const internal_id_t& internal_id);
    void get_manual_fast_scan_provider(provider_info_t& pi);
    dvblink::standard_name_t get_name_from_standard(source_type_e standard, bool short_name);
};

}
