/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <dl_device_info.h>
#include <drivers/deviceapi.h>

namespace dvblex { 

struct tuner_ex_properties_t
{
    const char* vid;
    const char* pid;
    const char* make;
    const char* model;
    const char* model_number;
    device_connection_type_e connection_type;
    bool should_be_merged;
};

const tuner_ex_properties_t tuner_ex_properties[] = 
    {
        {"07ca", "0837", "AVerMedia", "AVerTV Volar Hybrid Q", "H837", edct_usb, false},
#ifdef WIN32
        {"1d19", "0100", "DVBLogic", "TVButler", "100TC", edct_usb, true},
        {"2040", "0264", "Hauppauge", "WinTV-soloHD", "1589", edct_usb, true},
#else
        {"2040", "0264", "Hauppauge", "WinTV-soloHD", "1589", edct_usb, false},
#endif
        {"2040", "026d", "Hauppauge", "WinTV-dualHD", "1595", edct_usb, true},
        {"2040", "0265", "Hauppauge", "WinTV-dualHD", "1590", edct_usb, true},
        {"2040", "b123", "Hauppauge", "WinTV-HVR-955Q", "1191/1578", edct_usb, false},
        {"14f1", "8852", "Hauppauge", "WinTV-quadHD", "1607", edct_pcie, true},
        {NULL, NULL, "", "", "", edct_unknown, false}
    };

inline void apply_tuner_ex_properties(const dvblink::TDevAPIDevInfoEx& td, device_descriptor_t& dd)
{
    if (strlen(td.vid) > 0 && strlen(td.pid) > 0)
    {
        int c = 0;
        while (tuner_ex_properties[c].vid != NULL && tuner_ex_properties[c].pid != NULL)
        {
            if (boost::iequals(tuner_ex_properties[c].vid, td.vid) && boost::iequals(tuner_ex_properties[c].pid, td.pid))
            {
                if (tuner_ex_properties[c].make != NULL)
                    dd.make_ = tuner_ex_properties[c].make;

                if (tuner_ex_properties[c].model != NULL)
                    dd.model_ = tuner_ex_properties[c].model;

                if (tuner_ex_properties[c].model_number != NULL)
                    dd.model_num_ = tuner_ex_properties[c].model_number;

                if (tuner_ex_properties[c].connection_type != edct_unknown)
                    dd.connection_type_ = tuner_ex_properties[c].connection_type;

                dd.should_be_merged_ = tuner_ex_properties[c].should_be_merged;

                break;
            }
            ++c;
        }
    }
}


}
