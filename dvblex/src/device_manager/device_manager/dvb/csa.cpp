/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_packet_buffer.h>
#include <dl_logger.h>
#include <dl_ts_info.h>
#include "mdapi.h"
#include "csa_impl.h"
#include "ffdec.h"
#include "csa.h"

using namespace dvblink::engine;
using namespace dvblink::logging;


CTVSCSAHandler::CTVSCSAHandler()
{
}

CTVSCSAHandler::~CTVSCSAHandler()
{
}

void CTVSCSAHandler::Init()
{
    m_ResetBuffer = false;
    m_CSAHandler = NULL;

    //try to initialize ffdecsa first
    m_CSAHandler = new CTVSFFDecCSA();
    if (!m_CSAHandler->Init())
    {
        delete m_CSAHandler;
        m_CSAHandler = NULL;
    }
    //if ffdecsa is not found fall back to internal softcsa
    if (m_CSAHandler == NULL)
    {
        m_CSAHandler = new CTVSSoftCSA();
        m_CSAHandler->Init();
    }
}

void CTVSCSAHandler::Term()
{
}

void CTVSCSAHandler::ChannelChanged(TProgramm* tuner_info)
{
	SetCSAStateToIdle();
}

void CTVSCSAHandler::DecryptStream(unsigned char* buf, int len, std::map<unsigned short, unsigned short>& pids_to_decrypt)
{
    if (m_CSAState == ECW_SS_ACTIVE)
    {
        boost::unique_lock<boost::recursive_mutex> lock(m_cs);

        m_CSAHandler->Decrypt(buf, len);
    }
}

void CTVSCSAHandler::SetCSAStateToIdle()
{
    boost::unique_lock<boost::recursive_mutex> lock(m_cs);
	m_CSAState = ECW_SS_IDLE;
}

void CTVSCSAHandler::SetDW(bool b_dw_even, unsigned char* pdw)
{
	switch (m_CSAState)
	{
	case ECW_SS_IDLE:
		//this is the first part of control word
		if (b_dw_even)
		{
			CopyDWEven(pdw);
			m_CSAState = ECW_SS_WAIT_DW2;
		}
        else
		{
			CopyDWOdd(pdw);
			m_CSAState = ECW_SS_WAIT_DW1;
		}
		break;
	case ECW_SS_WAIT_DW1:
		if (b_dw_even)
		{
			CopyDWEven(pdw);
			//calculate a key
			CalculateNewKey();
			//start using it
			m_CSAState = ECW_SS_ACTIVE;
		}
        else
		{
			CopyDWOdd(pdw);
			//this is again dw2. Wait longer
		}
		break;
	case ECW_SS_WAIT_DW2:
		if (b_dw_even)
		{
			CopyDWEven(pdw);
			//this is again dw1. Wait longer
		}
        else
		{
			CopyDWOdd(pdw);
			//calculate a key
			CalculateNewKey();
			//start using it
			m_CSAState = ECW_SS_ACTIVE;
		}
		break;
	case ECW_SS_ACTIVE:
		if (b_dw_even)
			CopyDWEven(pdw);
		else
			CopyDWOdd(pdw);
		//calculate a key
		CalculateNewKey();
		break;
	default:
		break;
	}
}

void CTVSCSAHandler::CopyDWEven(unsigned char* pdw)
{
    log_ext_info(L"Info. CTVSCSAHandler: even CW");
	m_cw[0]=pdw[1];
	m_cw[1]=pdw[0];
	m_cw[2]=pdw[3];
	m_cw[3]=pdw[2];
	m_cw[4]=pdw[5];
	m_cw[5]=pdw[4];
	m_cw[6]=pdw[7];
	m_cw[7]=pdw[6];
}

void CTVSCSAHandler::CopyDWOdd(unsigned char* pdw)
{
    log_ext_info(L"Info. CTVSCSAHandler: odd CW");
	m_cw[8]=pdw[1];
	m_cw[9]=pdw[0];
	m_cw[10]=pdw[3];
	m_cw[11]=pdw[2];
	m_cw[12]=pdw[5];
	m_cw[13]=pdw[4];
	m_cw[14]=pdw[7];
	m_cw[15]=pdw[6];
}

void CTVSCSAHandler::CalculateNewKey()
{
    boost::unique_lock<boost::recursive_mutex> lock(m_cs);
    m_CSAHandler->SetNewKey(m_cw);
}

/*
SAA_COMMAND[6]:=0; // DW1 == odd
SendMessage(MultiDecWindow,WM_USER,MDAPI_DVB_COMMAND,(LongWord(@SAA_COMMAND)));
for i:=0 to 7 do SAA_COMMAND[i+8]:=CWSAAG2[i];
SAA_COMMAND[6]:=1; // DW2 = even
SendMessage(MultiDecWindow,WM_USER,MDAPI_DVB_COMMAND,(LongWord(@SAA_COMMAND)));


void set_cws(unsigned char *cws, struct key *key) {   
    memcpy(key->odd_ck,cws+8,8);   
    memcpy(key->even_ck,cws,8);   
    key_schedule(key->odd_ck,key->odd_kk);   
    key_schedule(key->even_ck,key->even_kk);   
}   

void decrypt( struct key *key, unsigned char *data )   
{   
  const int *kk;   
}

struct key { 
	int odd_kk[57], even_kk[57]; 
	unsigned char odd_ck[8], even_ck[8]; 
};

*/



