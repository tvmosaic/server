/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include "../device.h"

namespace dvblex { 

//keys
static const std::string dvb_lnb_type_key = "dvb_lnb_type";
static const std::string dvb_diseqc_type_key = "dvb_diseqc_type";
static const std::string dvb_scan_type_key = "dvb_scan_type";
static const std::string dvb_scan_fta_only_key = "dvb_fta_only";
static const std::string dvb_man_fast_scan_key = "aab1ca4ad62f";
static const std::string dvb_frequency_khz_key = "a12ad5d15b8d";
static const std::string dvb_symbol_rate_key = "dd4eb0253553";
static const std::string dvb_modulation_key = "a1063f138400";
static const std::string dvb_qam64_key = "a1063f138401";
static const std::string dvb_qam128_key = "a1063f138402";
static const std::string dvb_qam256_key = "a1063f138403";
static const std::string dvb_lof1_mhz_key = "f8ae50741c01";
static const std::string dvb_lof2_mhz_key = "f8ae50741c02";
static const std::string dvb_lofsw_mhz_key = "f8ae50741c03";

static const std::string dvb_configurable_params_container_id = "9e2164c5-a97b-4ab1-b904-67a537f8d4e3";
static const std::string dvb_concurrent_fta_num_key = "dvb_concurrent_fta_num";
static const std::string dvb_concurrent_enc_num_key = "dvb_concurrent_enc_num";

static const std::string epg_lang_params_key = "lang";

static const std::string dvb_ext_ci_device_key = "9880a250f80d";

static const std::string dvb_use_inband_epg_id_key = "dvb_use_inband_epg_id";
static const std::string dvb_use_own_epg_tr_key = "dvb_use_own_epg_tr";

//values
static const std::string dvb_scan_type_normal = "dvb_scan_type_normal";
static const std::string dvb_scan_type_network = "dvb_scan_type_network";

//constants
static const int dvb_default_tune_timeout_msec = 5000;
static const int dvbt_default_tune_timeout_msec = 4000;
static const int dvb_default_scan_timeout_msec = 10000;
static const int dvb_default_network_scan_timeout_msec = 10000;
const std::string dvb_default_frequency_khz = "164000";
const std::string dvb_default_symbol_rate = "6900";
static std::string dvb_default_lof_mhz = "-1";

const bool dvb_use_inband_epg_id_default = true;
const bool dvb_use_epg_only_from_current_tr_default = false;

}

