/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/locks.hpp>

class CTVSCSABase;
struct TProgramm;

class CTVSCSAHandler
{
public:
	CTVSCSAHandler();
	~CTVSCSAHandler();

	void Init();
	void Term();
	
    void DecryptStream(unsigned char* buf, int len, std::map<unsigned short, unsigned short>& pids_to_decrypt);
	void ChannelChanged(TProgramm* tuner_info);
	void SetDW(bool b_dw_even, unsigned char* pdw);

    const unsigned char* ProcessStream(const unsigned char* buf, int len, int& ret_len);

protected:
    void SetCSAStateToIdle();
    void CalculateNewKey();
    void CopyDWEven(unsigned char* pdw);
    void CopyDWOdd(unsigned char* pdw);

private:
    enum ECW_SCAN_STATE
	{
		ECW_SS_IDLE,
		ECW_SS_WAIT_DW1,
		ECW_SS_WAIT_DW2,
		ECW_SS_ACTIVE
	};

	unsigned char m_cw[16];
	ECW_SCAN_STATE m_CSAState;
	boost::recursive_mutex m_cs;
    bool m_ResetBuffer;
    CTVSCSABase* m_CSAHandler;
};
