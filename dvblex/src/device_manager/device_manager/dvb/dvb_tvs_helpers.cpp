/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <dl_strings.h>
#include <dl_parameters.h>
#include <drivers/tuner_factory.h>
#include <dl_pugixml_helper.h>
#include "dvb_parameters.h"
#include "dvb_tvs_helpers.h"
#include "tvs_tr_parser.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::engine;

DL_E_TUNER_TYPES dvblex::tvs_tuner_type_from_dvblex(source_type_e type)
{
    DL_E_TUNER_TYPES ret_val = TUNERTYPE_UNKNOWN;

    switch (type)
    {
    case st_dvb_t_t2:
        ret_val = TUNERTYPE_DVBT;
        break;
    case st_dvb_c:
        ret_val = TUNERTYPE_DVBC;
        break;
    case st_dvb_s_s2:
        ret_val = TUNERTYPE_DVBS;
        break;
    case st_atsc:
        ret_val = TUNERTYPE_ATSC;
        break;
    case st_cqam:
        ret_val = TUNERTYPE_CLEARQAM;
        break;
    case st_unknown:
    default:
        ret_val = TUNERTYPE_UNKNOWN;
        break;
    }

    return ret_val;
}

source_type_e dvblex::dvblex_tuner_type_from_tvs(DL_E_TUNER_TYPES type)
{
    source_type_e ret_val = st_unknown;

    switch (type)
    {
    case TUNERTYPE_DVBT:
        ret_val = st_dvb_t_t2;
        break;
    case TUNERTYPE_DVBC:
        ret_val = st_dvb_c;
        break;
    case TUNERTYPE_DVBS:
        ret_val = st_dvb_s_s2;
        break;
    case TUNERTYPE_ATSC:
        ret_val = st_atsc;
        break;
    case TUNERTYPE_CLEARQAM:
        ret_val = st_cqam;
        break;
    case TUNERTYPE_UNKNOWN:
    default:
        ret_val = st_unknown;
        break;
    }

    return ret_val;
}

bool dvblex::fill_lnb_values_for_type(const std::string& type, TVSC_LNBType& lnb, const std::string& lof1, const std::string& lof2, const std::string& lofsw)
{
    bool ret_val = false;

    std::wstring wtype = string_cast<EC_UTF8>(type);
    int c = 0;
    while (!cLNBTypeDefaults[c].typeID.empty())
    {
        if (boost::iequals(type, cLNBTypeDefaults[c].typeID))
        {
            lnb = cLNBTypeDefaults[c];

            //set lof values
            int l1, l2, lsw;

            string_conv::apply(lof1.c_str(), l1, -1);
            if (l1 >=0)
                lnb.LOF1 = l1*1000;

            string_conv::apply(lof2.c_str(), l2, -1);
            if (l2 >=0)
                lnb.LOF2 = l2*1000;

            string_conv::apply(lofsw.c_str(), lsw, -1);
            if (lsw >=0)
                lnb.LOFSW = lsw*1000;

            ret_val = true;
            break;
        }
        ++c;
    }

    return ret_val;
}

bool dvblex::fill_diseqc_values_for_type(const std::string& type, TVSC_DiseqcType& diseqc)
{
    bool ret_val = false;

    std::wstring wtype = string_cast<EC_UTF8>(type);
    int c = 0;
    while (!cDiseqcTypeDefaults[c].typeID.empty())
    {
        if (boost::iequals(type, cDiseqcTypeDefaults[c].typeID))
        {
            diseqc = cDiseqcTypeDefaults[c];
            ret_val = true;
            break;
        }
        ++c;
    }

    return ret_val;
}

void dvblex::tvs_channel_to_dvblex(const TVSC_ChannelType& tvs_channel, dvblex::device_channel_t& channel)
{
    channel.id_ = string_cast<EC_UTF8>(tvs_channel.id);
    channel.name_ = string_cast<EC_UTF8>(tvs_channel.name);
    channel.origin_ = string_cast<EC_UTF8>(tvs_channel.provider);
    channel.tid_ = tvs_channel.tid;
    channel.nid_ = tvs_channel.nid;
    channel.sid_ = tvs_channel.sid;
    channel.encrypted_ = tvs_channel.encrypted > 0;
    channel.num_ = tvs_channel.mj_num > 0 ? tvs_channel.mj_num : invalid_channel_number_;
    channel.sub_num_ = tvs_channel.mn_num > 0 ? tvs_channel.mn_num : invalid_channel_number_;
    channel.type_ = tvs_channel.type == TSVC_CMT_RADIO ? ct_radio : ct_tv;

    std::stringstream strbuf;
    strbuf << tvs_channel.transponder_info.dwFreq << ":" << tvs_channel.nid << ":" << tvs_channel.tid << ":" << tvs_channel.sid;
    channel.comment_ = strbuf.str();
}

static const std::string tuning_string_root = "tunedata";
static const std::string tuner_type_key = "tt";
static const std::string encrypted_key = "en";
static const std::string diseqc_key = "dq";
static const std::string diseqc_raw_key = "dr";
static const std::string freq_key = "fr";
static const std::string modulation_key = "md";
static const std::string symbol_rate_key = "sr";
static const std::string polarization_key = "pl";
static const std::string lof_key = "lf";
static const std::string lof1_key = "l1";
static const std::string lof2_key = "l2";
static const std::string lofsw_key = "ls";
static const std::string plpid_key = "pp";
static const std::string lnbkhz_key = "hz";
static const std::string inversion_key = "in";
static const std::string fec_key = "fc";
static const std::string tid_key = "t";
static const std::string nid_key = "n";
static const std::string sid_key = "s";

static void add_int_to_xml(pugi::xml_node node, const std::string& key, int value)
{
    std::stringstream strbuf;

    strbuf << value;
    dvblink::pugixml_helpers::new_child(node, key, strbuf.str());
}

std::string dvblex::get_channel_tuning_string_from_params(DL_E_TUNER_TYPES tuner_type, const concise_program_info_t& program_info)
{
    std::string ret_val;

    pugi::xml_document doc;
    pugi::xml_node root_node = doc.append_child(tuning_string_root.c_str());

    if (root_node != NULL)
    {
        add_int_to_xml(root_node, tuner_type_key, (int)tuner_type);
        add_int_to_xml(root_node, diseqc_key, program_info.tuning_info_.diseqc);

        dvblink::pugixml_helpers::new_child(root_node, diseqc_raw_key, program_info.tuning_info_.diseqc_rawdata);

        add_int_to_xml(root_node, freq_key, program_info.tuning_info_.transponder_info.dwFreq);
        add_int_to_xml(root_node, modulation_key, program_info.tuning_info_.transponder_info.dwModulation);
        add_int_to_xml(root_node, symbol_rate_key, program_info.tuning_info_.transponder_info.dwSr);
        add_int_to_xml(root_node, polarization_key, program_info.tuning_info_.transponder_info.Pol);
        add_int_to_xml(root_node, lof_key, program_info.tuning_info_.transponder_info.dwLOF);
        add_int_to_xml(root_node, lof1_key, program_info.tuning_info_.transponder_info.LOF1);
        add_int_to_xml(root_node, lof2_key, program_info.tuning_info_.transponder_info.LOF2);
        add_int_to_xml(root_node, lofsw_key, program_info.tuning_info_.transponder_info.LOFSW);
        add_int_to_xml(root_node, plpid_key, program_info.tuning_info_.transponder_info.plp_id);
        add_int_to_xml(root_node, lnbkhz_key, program_info.tuning_info_.transponder_info.dwLnbKHz);
        add_int_to_xml(root_node, inversion_key, program_info.tuning_info_.transponder_info.dwInversion);
        add_int_to_xml(root_node, fec_key, program_info.tuning_info_.transponder_info.dwFec);
        add_int_to_xml(root_node, tid_key, program_info.tid_);
        add_int_to_xml(root_node, nid_key, program_info.nid_);
        add_int_to_xml(root_node, sid_key, program_info.sid_);
        int enc = program_info.encrypted_ ? 1 : 0;
        add_int_to_xml(root_node, encrypted_key, enc);
    }

    dvblink::pugixml_helpers::xmldoc_dump_to_string(doc, ret_val);

    return ret_val;
}

static int get_int_from_xml(pugi::xml_node node, const std::string& key, int default_value)
{
    int ret_val = default_value;

    std::string str;
    if (dvblink::pugixml_helpers::get_node_value(node, key, str))
        string_conv::apply(str.c_str(), ret_val, default_value);

    return ret_val;
}

DL_E_TUNER_TYPES dvblex::get_tuner_type_from_tuning_string(const std::string& tuning_string)
{
    DL_E_TUNER_TYPES ret_val = TUNERTYPE_UNKNOWN;

    pugi::xml_document doc;
    if (doc.load_buffer(tuning_string.c_str(), tuning_string.length()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            ret_val = (DL_E_TUNER_TYPES)get_int_from_xml(root_node, tuner_type_key, (int)TUNERTYPE_UNKNOWN);
        }
    }

    return ret_val;
}

bool dvblex::get_channel_tuning_params_from_string(const concise_channel_tune_info_t& tune_info, DL_E_TUNER_TYPES& tuner_type, concise_program_info_t& program_info)
{
    bool ret_val = false;

    TTransponderInfo trinfo;
    SetTransponderDefaults(trinfo);

    pugi::xml_document doc;
    if (doc.load_buffer(tune_info.tuning_params_.to_string().c_str(), tune_info.tuning_params_.to_string().length()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            tuner_type = (DL_E_TUNER_TYPES)get_int_from_xml(root_node, tuner_type_key, (int)TUNERTYPE_UNKNOWN);
            program_info.tuning_info_.diseqc = (unsigned char)get_int_from_xml(root_node, diseqc_key, DISEQC_NONE);

            dvblink::pugixml_helpers::get_node_value(root_node, diseqc_raw_key, program_info.tuning_info_.diseqc_rawdata);

            program_info.tuning_info_.transponder_info.dwFreq = get_int_from_xml(root_node, freq_key, trinfo.dwFreq);
            program_info.tuning_info_.transponder_info.dwModulation = get_int_from_xml(root_node, modulation_key, trinfo.dwModulation);
            program_info.tuning_info_.transponder_info.dwSr = get_int_from_xml(root_node, symbol_rate_key, trinfo.dwSr);
            program_info.tuning_info_.transponder_info.Pol = (unsigned char)get_int_from_xml(root_node, polarization_key, trinfo.Pol);
            program_info.tuning_info_.transponder_info.dwLOF = get_int_from_xml(root_node, lof_key, trinfo.dwLOF);
            program_info.tuning_info_.transponder_info.LOF1 = get_int_from_xml(root_node, lof1_key, trinfo.LOF1);
            program_info.tuning_info_.transponder_info.LOF2 = get_int_from_xml(root_node, lof2_key, trinfo.LOF2);
            program_info.tuning_info_.transponder_info.LOFSW = get_int_from_xml(root_node, lofsw_key, trinfo.LOFSW);
            program_info.tuning_info_.transponder_info.plp_id = get_int_from_xml(root_node, plpid_key, trinfo.plp_id);
            program_info.tuning_info_.transponder_info.dwLnbKHz = get_int_from_xml(root_node, lnbkhz_key, trinfo.dwLnbKHz);
            program_info.tuning_info_.transponder_info.dwInversion = get_int_from_xml(root_node, inversion_key, trinfo.dwInversion);
            program_info.tuning_info_.transponder_info.dwFec = get_int_from_xml(root_node, fec_key, trinfo.dwFec);
            program_info.tid_ = (unsigned short)get_int_from_xml(root_node, tid_key, 0);
            program_info.nid_ = (unsigned short)get_int_from_xml(root_node, nid_key, 0);
            program_info.sid_ = (unsigned short)get_int_from_xml(root_node, sid_key, 0);
            int enc = get_int_from_xml(root_node, encrypted_key, 0);
            program_info.encrypted_ = enc > 0 ? true : false;

            //if scanner settings parameters are present - adjust lnb and diseqc
            if (tuner_type == dvblink::TUNERTYPE_DVBS && tune_info.scanner_settings_.size() > 0)
            {
                TVSC_HeadendType headend;
                //LNB parameter is ignored - we assume that all LNBs, using the same provider, have the same settings

                //diseqc - only pre-defined diseqc types are applied (e.g. no unicable and no raw data)
                parameter_value_t diseqc_type = get_value_for_param(dvb_diseqc_type_key, tune_info.scanner_settings_);

                if (!diseqc_type.empty() && fill_diseqc_values_for_type(diseqc_type.get(), headend.diseqc))
                {
                    dvblink::TTransponderInfo transponder;
                    CTVSTransponderParser::GetDiseqcValueFromType(TVSC_UnicableSlotList(), headend, &transponder, &program_info.tuning_info_.diseqc, program_info.tuning_info_.diseqc_rawdata);
                }
            }

            ret_val = true;

        }
    }

    return ret_val;
}

