/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/filesystem.hpp>
#include <dl_logger.h>
#include <dl_file_procedures.h>
#include <dl_filesystem_path.h>
#include "dvbjapi_plugin_manager.h"
#include "plugin_collection.h"

using namespace dvblink::logging;

tvs_plugin_collection::tvs_plugin_collection() :
	plugins_num_(0)
{
}

tvs_plugin_collection::~tvs_plugin_collection()
{
}

bool tvs_plugin_collection::init(const dvblink::filesystem_path_t& plugins_dir)
{
    if (!plugins_dir.to_wstring().empty() && boost::filesystem::exists(plugins_dir.to_boost_filesystem()))
    {
        boost::filesystem::directory_iterator end_itr;
        for (boost::filesystem::directory_iterator itr(plugins_dir.to_boost_filesystem()); itr != end_itr; ++itr)
        {
		    if (boost::filesystem::is_directory(itr->status()))
		    {
			    dvblink::filesystem_path_t plugin_dir = itr->path().filename().wstring();

                if (dvbjapi_plugin_manager::is_valid_dvbjapi_plugin_dir(plugins_dir, plugin_dir.to_wstring()))
                {
			        dvbjapi_plugin_manager* new_manager = new dvbjapi_plugin_manager(plugins_dir, plugin_dir.to_wstring());
			        new_manager->init();
			        plugins_.push_back(new_manager);
			        ++plugins_num_;
                }
            }
        }
    }

	log_info(L"tvs_plugin_collection::init. Initialized %1% plugins") % plugins_num_;
	return true;
}

void tvs_plugin_collection::term()
{
	while (plugins_.size() > 0)
	{
		plugins_.front()->term();
		delete plugins_.front();
		plugins_.pop_front();
	}
}

tvs_plugin_manager_base* tvs_plugin_collection::get_free_plugin_manager()
{
	tvs_plugin_manager_base* plugin = NULL;

	if (plugins_.size() > 0)
	{
		plugin = plugins_.front();
		plugins_.pop_front();
	}
	return plugin;
}

void tvs_plugin_collection::return_plugin_manager(tvs_plugin_manager_base* plugin_manager)
{
	plugins_.push_back(plugin_manager);
}

