/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_stream_types.h>
#include <dl_ts.h>
#include <drivers/tuner_factory.h>
#include "tvs_transponder_streamer.h"
#include "tvs_stream_src.h"
#include "tvs_stream_checker.h"

#if defined TVS_PLUGINS_SUPPORTED
	#include "plugin_manager_base.h"
	#include "plugin_collection.h"
	#include "plugin_stream_thread.h"
#endif

using namespace dvblink::engine;
using namespace dvblink::logging;

#define DEFAULT_STREAM_WAIT_TIME_MS 5000

transponder_streamer::transponder_streamer(dvblink::tuner_t* tuner, const dvblink::filesystem_path_t& shared_device_dir) :
    tuner_(tuner),
	stream_src_(NULL),
	idle_timeout_(-1),
	idle_timer_timeout_(10),
    shared_device_dir_(shared_device_dir)
{
	stream_checker_ = new stream_checker(PID_PAT);

#if defined TVS_PLUGINS_SUPPORTED
	plugin_collection_ = std::auto_ptr<tvs_plugin_collection>(new tvs_plugin_collection());
#endif
    
    idle_thread_quit_ = false;
    idle_thread_ = new boost::thread(boost::bind(&transponder_streamer::idle_thread_func, this));
}

transponder_streamer::~transponder_streamer()
{
	delete stream_checker_;
	
    idle_thread_quit_ = true;
    idle_thread_->join();
    delete idle_thread_;	
}

void transponder_streamer::idle_thread_func()
{
    while (!idle_thread_quit_)
    {
        int counter_value = -1;
        {
    	    boost::unique_lock<boost::mutex> lock(idle_timeout_lock_);
    	    if (idle_timeout_ >= 0)
    	        --idle_timeout_;
    	        
    	    counter_value = idle_timeout_;
        }
        
        if (counter_value == 0)
        {
		    log_info(L"Stopping graph");

			if (stream_src_ != NULL)
				stream_src_->Stop();
			
		}
        
        boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
    }
    log_info(L"transponder_streamer::idle_thread_func finished");
}

bool transponder_streamer::init(dvblink::DL_E_TUNER_TYPES tuner_type, transponder_streamer_cb_f raw_stream_cb, void* user_param)
{
	user_param_ = user_param;
    raw_stream_cb_ = raw_stream_cb;
	current_transponder_hash_.clear();

    options_.tuner_type_ = tuner_type;

    stream_src_ = new CTVSStreamSource();
	stream_src_->Init(tuner_, options_, stream_src_callback, this);

#if defined TVS_PLUGINS_SUPPORTED
	plugin_collection_->init(shared_device_dir_);
#endif

	start_idle_timer();

	return true;
}

void transponder_streamer::term()
{
    if (stream_src_ != NULL)
    {
	    stop_all_streams();
	    stop_idle_timer();

#if defined TVS_PLUGINS_SUPPORTED
		plugin_collection_->term();
#endif

	    stream_src_->Term();
        delete stream_src_;
	    stream_src_ = NULL;

        options_.tuner_type_ = dvblink::TUNERTYPE_UNKNOWN;
    }
}

bool transponder_streamer::is_idle()
{
    bool ret_val = true;

    if (stream_src_ != NULL)
        ret_val = !(stream_src_->is_tuner_started());

    return ret_val;
}

bool transponder_streamer::start_channel(tvs_program_streamer* program_streamer)
{
	bool res = false;

    transponder_tuning_info_t tuning_info;
    program_streamer->get_tuning_info(tuning_info);

    std::wstring channel_id = program_streamer->get_channel_id();

	log_info(L"transponder_streamer::start_channel. Start request for channel %1%") % channel_id;

	stop_idle_timer();
	//if this is the first one - tune transponder
	if (tune_transponder(tuning_info))
	{
		//initialize program stream and add channel to the map of active channels
		transponder_channel_desc channel_desc;
		channel_desc.channel_id = channel_id;
        channel_desc.program_src_ = program_streamer;

#if defined TVS_PLUGINS_SUPPORTED
		channel_desc.stream_thread_ = new plugin_stream_thread(channel_desc.program_src_);

		//if channel is not fta - get plugin manager from the pool
		if (program_streamer->is_channel_encrypted())
		{
			tvs_plugin_manager_base* plugin = plugin_collection_->get_free_plugin_manager();
			if (plugin != NULL)
				channel_desc.program_src_->set_plugin_manager(plugin);
		}
#endif

        channel_desc.program_src_->start(stream_src_);

		{
			boost::unique_lock<boost::shared_mutex> lock(lock_);
			active_channels_[channel_id] = channel_desc;
		}
		res = true;
	} else
	{
		log_error(L"transponder_streamer::start_channel. Tuning stransponder for channel %1% failed") % channel_id;
		//If not successful - start idle timer again
		if (active_channels_.size() == 0)
			start_idle_timer();
	}
	return res;
}

bool transponder_streamer::stop_channel(std::wstring& channel_id)
{
	log_info(L"transponder_streamer::stop_channel. Stop request for channel %1%") % channel_id;
	if (active_channels_.find(channel_id) != active_channels_.end())
	{
		tvs_program_streamer* program_src;

#if defined TVS_PLUGINS_SUPPORTED
		plugin_stream_thread* stream_thread;
#endif

        {
			boost::unique_lock<boost::shared_mutex> lock(lock_);
			program_src = active_channels_[channel_id].program_src_;
#if defined TVS_PLUGINS_SUPPORTED
			stream_thread = active_channels_[channel_id].stream_thread_;
#endif
			active_channels_.erase(channel_id);
		}

		program_src->stop();

#if defined TVS_PLUGINS_SUPPORTED
		tvs_plugin_manager_base* plugin = program_src->release_plugin_manager();
		if (plugin != NULL)
			plugin_collection_->return_plugin_manager(plugin);

		delete stream_thread;
#endif
    
    }
	if (active_channels_.size() == 0)
	{
		//clear transponder hash
		current_transponder_hash_.clear();
		//last channel. Start Idle timer
		start_idle_timer();
	}
	return true;
}

void transponder_streamer::stop_all_streams()
{
	std::vector<std::wstring> channel_ids;
	transponder_channels_map_t::iterator it = active_channels_.begin();
	while (it != active_channels_.end())
	{
		channel_ids.push_back(it->first);
		++it;
	}
	for (size_t i=0; i<channel_ids.size(); i++)
		stop_channel(channel_ids[i]);
}

bool transponder_streamer::get_signal_stats(unsigned char* level, unsigned char* quality, unsigned char* locked)
{
	bool ret_val = false;
	if (stream_src_ != NULL)
	{
		dvblink::TSignalInfo si;
		ret_val = stream_src_->GetSignalStats(&si);
		if (ret_val)
		{
			*level = si.Level;
			*quality = si.Quality;
			*locked = si.Locked;
		}
	}
	return ret_val;
}

bool transponder_streamer::channel_can_join(transponder_tuning_info_t& tuning_info)
{
	//we only check if transponder is the same
	std::wstring tr_hash;
	GetTransponderHash(options_.tuner_type_, tuning_info.transponder_info, tuning_info.diseqc_rawdata.c_str(), tuning_info.diseqc, tr_hash);

	return current_transponder_hash_.size() == 0 || tr_hash == current_transponder_hash_;
}

void __stdcall transponder_streamer::stream_src_callback(const unsigned char* buf, unsigned long len, void* user_param)
{
	transponder_streamer* parent = (transponder_streamer*)user_param;
	parent->stream_checker_->write_stream(buf, len);

	if (parent->raw_stream_cb_ != NULL)
		parent->raw_stream_cb_(parent->raw_cb_channel_id_, buf, len, parent->user_param_);

	boost::shared_lock<boost::shared_mutex> lock(parent->lock_);

	transponder_channels_map_t::iterator it = parent->active_channels_.begin();
	while (it != parent->active_channels_.end())
	{
#if defined TVS_PLUGINS_SUPPORTED
		it->second.stream_thread_->write_stream(buf, len);
#else
		it->second.program_src_->write_stream(buf, len);
#endif
		++it;
	}

#if defined TVS_PLUGINS_SUPPORTED
	//wait until stream is processed
	it = parent->active_channels_.begin();
	while (it != parent->active_channels_.end())
	{
		it->second.stream_thread_->wait_for_write_to_finish();
		++it;
	}
#endif

}

bool transponder_streamer::tune_transponder(transponder_tuning_info_t& tuning_info)
{
	bool res = false;

	if (current_transponder_hash_.size() == 0)
	{
		//first channel on this transponder - tune it
		if (tune_transponder_impl(tuning_info))
		{
			//save transponder hash
			GetTransponderHash(options_.tuner_type_, tuning_info.transponder_info, tuning_info.diseqc_rawdata.c_str(), tuning_info.diseqc, current_transponder_hash_);
            res = true;
		} else
		{
			log_error(L"tune_transponder. Unable to tune transponder");
		}
	} else
	{
		//transponder is already tuned
		res = true;
	}
	return res;
}

bool transponder_streamer::tune_transponder_impl(transponder_tuning_info_t& tuning_info)
{
	bool ret_val = stream_src_->TuneTransponder(&tuning_info.transponder_info, tuning_info.diseqc, tuning_info.diseqc_rawdata.c_str()) > 0;
    if (ret_val)
    {
        //ask for signal stats (some tuners do not start stream if getsignalstats is not called at least once)
        unsigned char level, quality, locked;
        if (get_signal_stats(&level, &quality, &locked))
        {
            log_info(L"transponder_streamer::tune_transponder_impl. Signal stats: %1%, %2%, %3%") % level % quality % locked;
        }
    }
    return ret_val;
}

void transponder_streamer::start_idle_timer()
{
	log_info(L"transponder_streamer::start_idle_timer");

    boost::unique_lock<boost::mutex> lock(idle_timeout_lock_);
    idle_timeout_ = idle_timer_timeout_;
}

void transponder_streamer::stop_idle_timer()
{
	log_info(L"transponder_streamer::stop_idle_timer");

    boost::unique_lock<boost::mutex> lock(idle_timeout_lock_);
    idle_timeout_ = -1;
}
