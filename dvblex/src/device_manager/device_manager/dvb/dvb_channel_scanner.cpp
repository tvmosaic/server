/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include <drivers/tuner_factory.h>
#include "dvb_channel_scanner.h"
#include "tvs_channel_scanner.h"
#include "dvb_tvs_helpers.h"
#include <dl_parameters.h>
#include "dvb_parameters.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

dvb_channel_scanner_t::dvb_channel_scanner_t(dvblink::tuner_t* tuner) :
    channel_scanner_t(), tuner_(tuner)
{
    channel_scanner_ = new CTVSChannelScanner(tuner_);
    network_list_ = new TVSC_NetworkScanList();
}

dvb_channel_scanner_t::~dvb_channel_scanner_t()
{
    delete channel_scanner_;
    delete network_list_;
}

bool dvb_channel_scanner_t::start()
{
    //tuner type
    DL_E_TUNER_TYPES tuner_type = tvs_tuner_type_from_dvblex(provider_info_.standard_);
    //headend. All fields do not matter, except for LNB and diseqc in case of sat rception
    TVSC_HeadendType headend;
    //LNB
    parameter_value_t lnb_type = get_value_for_param(dvb_lnb_type_key, scanner_settings_);
    parameter_value_t lof1 = get_value_for_param(dvb_lof1_mhz_key, scanner_settings_);
    parameter_value_t lof2 = get_value_for_param(dvb_lof2_mhz_key, scanner_settings_);
    parameter_value_t lofsw = get_value_for_param(dvb_lofsw_mhz_key, scanner_settings_);

    if (!fill_lnb_values_for_type(lnb_type.get(), headend.LNB, lof1.to_string(), lof2.to_string(), lofsw.to_string()))
        fill_lnb_values_for_type(string_cast<EC_UTF8>(cLNBTypeDefaults[0].typeID), headend.LNB, lof1.to_string(), lof2.to_string(), lofsw.to_string()); //use some default value
    //diseqc
    parameter_value_t diseqc_type = get_value_for_param(dvb_diseqc_type_key, scanner_settings_);

    if (!fill_diseqc_values_for_type(diseqc_type.get(), headend.diseqc))
        fill_diseqc_values_for_type(string_cast<EC_UTF8>(cDiseqcTypeDefaults[0].typeID), headend.diseqc); //use some default value

    return channel_scanner_->Init(tuner_type, headend) > 0 ? true : false;
}

void dvb_channel_scanner_t::stop()
{
    channel_scanner_->Term();
}

static std::string get_values_from_key_value_pair(const std::string& str)
{
    std::string ret_val = str;

    size_t n = str.find("=");
    if (n != std::string::npos)
        ret_val = str.substr(n+1);

    return ret_val;
}

static std::vector<TVSC_ServiceInfo>* get_expected_services_for_scan_data(const std::wstring& tuning_params,
                                                                   CTVSChannelScanner* channel_scanner,
                                                                   unsigned short network_id,
                                                                   std::vector<TVSC_NetworkScanData>* network_list)
{
    std::vector<TVSC_ServiceInfo>* ret_val = NULL;
    if (network_id != tvs_channel_scanner_invalid_network_id && network_list != NULL)
    {
        std::wstring wid;
        channel_scanner->get_transponder_id(tuning_params, wid);
        for (size_t i=0; i<network_list->size() && ret_val == NULL; i++)
        {
            if (network_list->at(i).nid == network_id)
            {
                for (size_t j=0; j<network_list->at(i).transponders.size(); j++)
                {
                    std::wstring wid1;
                    channel_scanner->get_transponder_id(network_list->at(i).transponders[j].transponder.data, wid1);
                    if (boost::iequals(wid, wid1) && network_list->at(i).transponders[j].bFilterServices)
                    {
                        ret_val = &(network_list->at(i).transponders[j].expectedServices);
                        log_info(L"get_expected_services_for_scan_data. Using expected services for network %1%") % network_id;
                        break;
                    }
                }
            }
        }
    }
    return ret_val;
}

static bool make_default_dvbt2_transponder(const std::wstring& in_trans, std::wstring& out_trans)
{
    out_trans = in_trans;
    //count , in the in transponder
    int comma_count = 0;
    for (size_t i=0; i<in_trans.size(); i++)
    {
        if (in_trans[i] == L',')
            comma_count += 1;
    }
    //check if it is already dvb-t2 transponder
    if (comma_count == 5)
        return false;
    //dvb-t2 is the 6th parameter. Insert commas if needed.
    for (int i=0; i<4 - comma_count; i++)
        out_trans += L',';
    //add dvb-t2 plp id
    out_trans += L",0";
    return true;
}

bool dvb_channel_scanner_t::do_scan(const std::string& tuning_params, const dvblink::scan_network_id_t& network_id, transponder_list_t& found_channels, scan_log_entry_t& log_entry)
{
    bool ret_val = false;

    std::string tune_params = get_values_from_key_value_pair(tuning_params);

    log_entry.reset();
    log_entry.scan_data_ = tune_params;

    std::wstring tr_data = string_cast<EC_UTF8>(tune_params);

    DL_E_TUNER_TYPES tuner_type = tvs_tuner_type_from_dvblex(provider_info_.standard_);
    int tune_timeout = dvb_default_tune_timeout_msec;
    if (tuner_type == TUNERTYPE_DVBT) //we might be doing two scans for dvb-t. Adjust timeout
        tune_timeout = dvbt_default_tune_timeout_msec;

    bool b_tune_success = channel_scanner_->Tune(tr_data, true, tune_timeout) > 0;

    if (!b_tune_success && tuner_type == TUNERTYPE_DVBT)
    {
        //retry tuning using dvb-t2 (with default plp id)
        std::wstring dvbt2_trans;
        if (make_default_dvbt2_transponder(tr_data, dvbt2_trans))
        {
            tr_data = dvbt2_trans;
            b_tune_success = channel_scanner_->Tune(tr_data, true, tune_timeout) > 0;
        }
    }

    //tune transponder
    if (b_tune_success)
    {
        transponder_t transponder_info;
        std::wstring wid;
        channel_scanner_->get_transponder_id(tr_data, wid);
        transponder_info.tr_id_ = string_cast<EC_UTF8>(wid);
        transponder_info.tr_name_ = string_cast<EC_UTF8>(tr_data);

        unsigned short tvs_network_id = tvs_channel_scanner_invalid_network_id;
        if (!network_id.empty())
            tvs_network_id = (unsigned short)atoi(network_id.to_string().c_str());

        bool fta_only = is_key_present(dvb_scan_fta_only_key, scanner_settings_) && boost::iequals(get_value_for_param(dvb_scan_fta_only_key, scanner_settings_).get(), PARAM_VALUE_YES);
        std::vector<TVSC_ServiceInfo>* expectedServices = get_expected_services_for_scan_data(tr_data, 
            channel_scanner_, tvs_network_id, network_list_);
        TVSC_ChannelMap scanned_services;
	    if (channel_scanner_->Scan(dvb_default_scan_timeout_msec, fta_only, expectedServices, scanned_services) > 0)
        {
            TVSC_ChannelMap::iterator it = scanned_services.begin();
            while (it != scanned_services.end())
            {
                dvblex::device_channel_t channel;
                tvs_channel_to_dvblex(it->second, channel);

                concise_program_info_t pi;
                pi.tuning_info_.transponder_info = it->second.transponder_info;
                pi.tuning_info_.diseqc_rawdata = it->second.diseqc_rawdata;
                pi.tuning_info_.diseqc = it->second.diseqc;
                pi.tid_ =  channel.tid_.get();
                pi.nid_ =  channel.nid_.get();
                pi.sid_ =  channel.sid_.get();
                pi.encrypted_ = channel.encrypted_.get();
                channel.tune_params_ = get_channel_tuning_string_from_params(tvs_tuner_type_from_dvblex(provider_info_.standard_), pi);

                transponder_info.channels_.push_back(channel);
                log_entry.channels_found_ += 1;
                log_entry.add_comment(channel.name_.get());

                ++it;
            }

            found_channels.push_back(transponder_info);

            ret_val = true;
        } else
        {
            log_info(L"dvb_channel_scanner_t::do_scan. Scan failed for %1%") % tr_data;
        }

        unsigned char level;
        unsigned char quality;
        unsigned char locked;
        if (channel_scanner_->GetSignalStats(&level, &quality, &locked))
        {
            log_entry.signal_info_.lock_ = locked > 0;
            log_entry.signal_info_.signal_quality_ = quality;
            log_entry.signal_info_.signal_strength_ = level;
        }
    } else
    {
        log_warning(L"dvb_channel_scanner_t::do_scan. Tune failed for %1%") % tr_data;
    }

    return ret_val;
}

static void tvs_networks_to_dvblex(const TVSC_NetworkScanList& network_list, channel_scanner_t::network_scan_results_t& networks)
{
    networks.clear();

    for (size_t i=0; i<network_list.size(); i++)
    {
        channel_scanner_t::network_scan_entry_t entry;
        try
        {
            entry.network_id_ = boost::lexical_cast<std::string>(network_list[i].nid);
        } catch (...){}
        entry.name_ = string_cast<EC_UTF8>(network_list[i].name);
        for (size_t j=0; j<network_list[i].transponders.size(); j++)
            entry.scan_data_.push_back(string_cast<EC_UTF8>(network_list[i].transponders[j].transponder.data));

        networks.push_back(entry);
    }
}

bool dvb_channel_scanner_t::scan_networks(const provider_scan_list_t& scan_data, network_scan_results_t& network_scan_results, scan_log_entry_t& log_entry)
{
    bool ret_val = false;
    log_entry.reset();
    network_list_->clear();

    // scan_data in this case should have only one entry - tune parameters for one transponder
    if (scan_data.size() > 0)
    {
        std::string tune_params = get_values_from_key_value_pair(scan_data[0].get());

        log_entry.scan_data_ = tune_params;

        //tune transponder
        std::wstring tr_data = string_cast<EC_UTF8>(tune_params);
        if (channel_scanner_->Tune(tr_data, true, dvb_default_tune_timeout_msec) > 0)
        {
            if (channel_scanner_->ScanTransponderList(dvb_default_network_scan_timeout_msec, *network_list_))
            {
                tvs_networks_to_dvblex(*network_list_, network_scan_results_);

                ret_val = true;
            } else
            {
                log_error(L"dvb_channel_scanner_t::scan_networks. Scan networks failed for %1%") % tr_data;
            }

            unsigned char level;
            unsigned char quality;
            unsigned char locked;
            if (channel_scanner_->GetSignalStats(&level, &quality, &locked))
            {
                log_entry.signal_info_.lock_ = locked > 0;
                log_entry.signal_info_.signal_quality_ = quality;
                log_entry.signal_info_.signal_strength_ = level;
            }
        } else
        {
            log_error(L"dvb_channel_scanner_t::scan_networks. Tune failed for %1%") % tr_data;
        }
    } else
    {
        log_error(L"dvb_channel_scanner_t::scan_networks. Scan data has no transponders");
    }

    return ret_val;
}

bool dvb_channel_scanner_t::is_network_scan(const provider_info_t& provider_info)
{
    return is_key_present(dvb_scan_type_key, scanner_settings_) && boost::iequals(get_value_for_param(dvb_scan_type_key, scanner_settings_).get(), dvb_scan_type_network);
}
