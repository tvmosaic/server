/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include "dvb_program_streamer.h"
#include "tvs_program_streamer.h"
#include "tvs_transponder_streamer.h"
#include "dvb_tvs_helpers.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

dvb_program_streamer_t::dvb_program_streamer_t(transponder_streamer* transponder_streamer, const std::string& channel_id, const concise_channel_tune_info_t& tune_params) :
    program_streamer_t(channel_id, tune_params.tuning_params_.get()), transponder_streamer_(transponder_streamer), program_streamer_(NULL)
{
    std::wstring wchannel_id = string_cast<EC_UTF8>(channel_id);
    DL_E_TUNER_TYPES tuner_type;
    concise_program_info_t pi;
    if (!get_channel_tuning_params_from_string(tune_params, tuner_type, pi))
        log_warning(L"dvb_program_streamer_t::dvb_program_streamer_t. Could not parse tuning parameters fro tuning string %1%") % string_cast<EC_UTF8>(tune_params.tuning_params_.get());

    program_streamer_ = new tvs_program_streamer(wchannel_id, pi, program_stream_callback, this);
}

dvb_program_streamer_t::~dvb_program_streamer_t()
{
    delete program_streamer_;
}

bool dvb_program_streamer_t::start()
{
    program_streamer_t::start();

    log_info(L"dvb_program_streamer_t::start. Starting program streamer for channel %1%") % string_cast<EC_UTF8>(channel_id_.get());
    return transponder_streamer_->start_channel(program_streamer_);
}

void dvb_program_streamer_t::stop()
{
    log_info(L"dvb_program_streamer_t::stop. Stoping program streamer for channel %1%") % string_cast<EC_UTF8>(channel_id_.get());

    std::wstring wchannel_id = program_streamer_->get_channel_id();
    transponder_streamer_->stop_channel(wchannel_id);

    program_streamer_t::stop();
}

void dvb_program_streamer_t::program_stream_callback(std::wstring& channel_id, const unsigned char* buf, unsigned long len, void* user_param)
{
    dvb_program_streamer_t* parent = (dvb_program_streamer_t*)user_param;
    parent->write_stream(buf, len);
}

bool dvb_program_streamer_t::get_signal_info(signal_info_t& si)
{
    bool ret_val = false;

    si.reset();

    if (transponder_streamer_ != NULL)
    {
        unsigned char level;
        unsigned char quality;
        unsigned char locked;
	    if (transponder_streamer_->get_signal_stats(&level, &quality, &locked))
        {
            si.lock_ = locked > 0;
            si.signal_quality_ = quality;
            si.signal_strength_ = level;
        }
    }
    return ret_val;
}

