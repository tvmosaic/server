/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_stream_types.h>
#include <dl_ts.h>
#include "tvs_program_streamer.h"
#include "tvs_protected_map.h"
#include "tvs_stream_src.h"
#include "plugin_manager_base.h"

using namespace dvblink::engine;
using namespace dvblink::logging;

tvs_program_streamer::tvs_program_streamer(std::wstring& channel_id, concise_program_info_t& program_info, program_streamer_cb_f cb, void* user_param) :
    user_cb_(cb),
    user_param_(user_param),
    channel_id_(channel_id),
    stream_source_(NULL),
    program_info_(program_info),
#ifdef TVS_PLUGINS_SUPPORTED
    plugin_manager_(NULL),
#endif
	tuning_thread_(NULL),
	exit_(false),
    program_change_state_(ETVS_PST_IDLE),
    spts_stream_converter_(),
	stream_buffer_(72),
    pmt_version_(0xFF)
{
	log_info(L"tvs_program_streamer::tvs_program_streamer. Select program for nid %1%, tid %2%, sid %3%") % program_info.nid_ % program_info.tid_ % program_info.sid_;
}

void tvs_program_streamer::start(CTVSStreamSource* stream_source)
{
    stream_source_ = stream_source;
	//start tuning thread
    exit_ = false;
	tuning_thread_ = new boost::thread(boost::bind(&tvs_program_streamer::tuning_thread, this));
}

void tvs_program_streamer::stop()
{
	//send stop decryption command to the CAM
	if (m_PMTSectionBuffer.size() > 0 && program_info_.encrypted_)
	{
		stream_source_->SendPMTToCI(&m_PMTSectionBuffer[0], m_PMTSectionBuffer.size(), dvblink::SERVICE_DECRYPT_REMOVE);
		//also try sending the pmt pid (only for Octopus.net sat2ip tuner. Other tuners do not implement this function
		stream_source_->SendPMTPidToCI(m_pmt_pid, program_info_.sid_, dvblink::SERVICE_DECRYPT_REMOVE);
	}

	spts_stream_converter_.Term();
	//stop tuning thread (if not stopped already)
	if (tuning_thread_ != NULL)
	{
		exit_ = true;
		tuning_thread_->join();
		delete tuning_thread_;
	}
	stream_source_->RemoveAllPids(channel_id_.c_str());
	pid_filter_.reset();
	stream_buffer_.Reset();
}

tvs_program_streamer::~tvs_program_streamer()
{
}

void tvs_program_streamer::get_elementary_pids(std::vector<unsigned short>& pids)
{
    pids.clear();
    bool bAddPCR = true;
	for (unsigned int i=0; i<m_ProgramStreamInfo.size(); i++)
	{
		pids.push_back(m_ProgramStreamInfo[i].PID);
        if (m_ProgramStreamInfo[i].PID == m_pcr_pid)
            bAddPCR = false;
	}
	//PCR PID
    if (bAddPCR)
	    pids.push_back(m_pcr_pid);
    //Add NULL packet PID
    pids.push_back(NULL_PACKET_PID);
}

void tvs_program_streamer::tuning_thread()
{
	log_info(L"tvs_program_streamer::tuning_thread. Starting tuning thread for program %1%") % channel_id_;

	int sleep_ticks = 50;
	int wait_timeout = 0;

	SetPATScanState();

	while (!exit_ && program_change_state_ != ETVS_PST_STREAMING)
	{
		switch (program_change_state_)
		{
		case ETVS_PST_PAT_COMPLETED:
			log_info(L"tvs_program_streamer::tuning_thread. Waiting for PMT...");
			SetPMTScanState();
			break;
		case ETVS_PST_PMT_COMPLETED:
			log_info(L"tvs_program_streamer::tuning_thread. PAT and PMT found. Checking CA descriptors");
			//Check whether CAT descriptors have been parsed already
			if (m_pmt_ca_desc_list.size() > 0 && m_cat_ca_desc_list.size() == 0)
			{
				//wait additional 2 seconds 
				wait_timeout = 2000;
				SetCAScanState();
			} else
			{
				SetCACompletedScanState();
			}
			break;
		case ETVS_PST_CA:
			if (!abort_ca_scan_)
			{
				wait_timeout -= sleep_ticks;
				if (wait_timeout <=0 )
					abort_ca_scan_ = true;
			}
			break;
		case ETVS_PST_CA_COMPLETED:
			//start actual streaming
			{
				log_info(L"tvs_program_streamer::tuning_thread. Start streaming");
				std::vector<unsigned short> pids_to_add;
				//Walk through array of found PIDs and set the filters
				for (unsigned int i=0; i<m_ProgramStreamInfo.size(); i++)
					pids_to_add.push_back(m_ProgramStreamInfo[i].PID);
				//PCR PID
				pids_to_add.push_back(m_pcr_pid);

				//Add general purpose PIDs: PAT, SDT, NIT, EIT, CAT etc.
				pids_to_add.push_back(PID_PAT); //PAT
				pids_to_add.push_back(PID_CAT); //CAT
				pids_to_add.push_back(PID_NIT); //NIT
				pids_to_add.push_back(PID_SDT); //SDT
				pids_to_add.push_back(PID_EIT); //EIT
				pids_to_add.push_back(NULL_PACKET_PID); //NULL packet
				AddPIDs(pids_to_add);
				//Send PMT section to hardware CI module
				if (program_info_.encrypted_)
				{
					stream_source_->SendPMTToCI(&m_PMTSectionBuffer[0], m_PMTSectionBuffer.size(), dvblink::SERVICE_DECRYPT_ADD);
					//also try sending the pmt pid (only for Octopus.net sat2ip tuner. Other tuners do not implement this function
					stream_source_->cmdSendPMTPidToCI(m_pmt_pid, program_info_.sid_, dvblink::SERVICE_DECRYPT_ADD);

#ifdef TVS_PLUGINS_SUPPORTED
				    if (plugin_manager_ != NULL)
				    {
                        //notify plugin manager that program has changed
				        TProgramm new_program;
				        FillTProgrammStructure(&new_program);

                        plugin_program_extra_info extra_info;
                        extra_info.nid = program_info_.nid_;
                        extra_info.pmt_section = m_PMTSectionBuffer;
                        extra_info.cat_section = m_CATSectionBuffer;
					    plugin_manager_->channel_changed(&new_program, extra_info);
    					
				        std::vector<unsigned short> pids_to_decrypt;
	                    get_elementary_pids(pids_to_decrypt);
					    plugin_manager_->add_pids_to_decrypt(pids_to_decrypt);
                    }
#endif
                }
                //Start client streaming
				SetStreamingState();
			}
			break;
		default:
			break;
		}
		boost::this_thread::sleep(boost::posix_time::milliseconds(sleep_ticks));
	}
	log_info(L"tvs_program_streamer::tuning_thread. Finished tuning thread for program %1%") % channel_id_;
}

void tvs_program_streamer::FillTProgrammStructure(TProgramm* new_program)
{
	memset(new_program, 0, sizeof(*new_program));
	//name. Use combination of nid:tid:sid
    std::stringstream buf;
    buf << program_info_.nid_ << ":" << program_info_.tid_ << ":" << program_info_.sid_;
    std::string name = buf.str();
	strncpy(new_program->Name, name.c_str(), 29);
	new_program->Name[29] = '\0';
	//provider
/*
	ConvertUCToMultibyte(dvblink::engine::EC_ISO_8859_1, program_info_.provider.c_str(), str);
	strncpy(new_program->Anbieter, str.c_str(), 29);
	new_program->Anbieter[29] = '\0';
*/
	//Land
	new_program->Land[0] = '\0';
	new_program->freq = program_info_.tuning_info_.transponder_info.dwFreq;
	new_program->tp_id = program_info_.tid_;
	new_program->SID_pid = program_info_.sid_;
	new_program->PMT_pid = m_pmt_pid;
	new_program->PCR_pid = m_pcr_pid;
//    new_program->ECM_PID = program_info_.ecm_pid;
	new_program->Link_TP = program_info_.tid_;
	new_program->Link_SID = program_info_.sid_;
    new_program->CA_Anzahl = m_pmt_ca_desc_list.size();
	if (new_program->CA_Anzahl > 0 && m_cat_ca_desc_list.size() == 0)
	{
		//give a warning that CAT has not been parsed yet
		log_warning(L"Although PMT contains security descriptors, CAT has not been parsed yet");
	}
	for (unsigned int i=0; i<m_pmt_ca_desc_list.size() && i < MAX_CA_SYSTEMS; i++)
	{
		new_program->CA_System[i].CA_Typ = m_pmt_ca_desc_list[i].CI_system_ID;
		new_program->CA_System[i].ECM = m_pmt_ca_desc_list[i].PID;
		int emmpid = GetEMMPidForCASystemID(m_pmt_ca_desc_list[i].CI_system_ID);
		if (emmpid == -1)
			emmpid = m_pmt_ca_desc_list[i].PID;
		new_program->CA_System[i].EMM = emmpid;
	}
	for (unsigned int i=0; i<m_ProgramStreamInfo.size(); i++)
	{
		if (m_ProgramStreamInfo[i].type == MBTS_ST_VIDEO_MPEG1 ||
			m_ProgramStreamInfo[i].type == MBTS_ST_VIDEO_MPEG2 ||
			m_ProgramStreamInfo[i].type == MBTS_ST_VIDEO_MPEG4 ||
			m_ProgramStreamInfo[i].type == MBTS_ST_VIDEO_H264)
			new_program->Video_pid = m_ProgramStreamInfo[i].PID;

		if (m_ProgramStreamInfo[i].type == MBTS_ST_AUDIO_MPEG1 ||
			m_ProgramStreamInfo[i].type == MBTS_ST_AUDIO_MPEG2 ||
			m_ProgramStreamInfo[i].type == MBTS_ST_AUDIO_AAC)
			new_program->Audio_pid = m_ProgramStreamInfo[i].PID;

		if (m_ProgramStreamInfo[i].type == MBTS_ST_AUDIO_AC3)
			new_program->AC3_pid = m_ProgramStreamInfo[i].PID;

		if (m_ProgramStreamInfo[i].type == MBTS_ST_PRIVATE_DATA)
			new_program->TeleText_pid = m_ProgramStreamInfo[i].PID;
	}
}

int tvs_program_streamer::GetEMMPidForCASystemID(unsigned short ca_system_id)
{
	int ret_val = -1;
	for (unsigned i=0; i<m_cat_ca_desc_list.size(); i++)
	{
		if (m_cat_ca_desc_list[i].CI_system_ID == ca_system_id)
		{
			ret_val = m_cat_ca_desc_list[i].PID;
			break;
		}
	}
	return ret_val;
}

bool tvs_program_streamer::ProcessCATPacket(const unsigned char* packet)
{
    bool ret_val = false;
	//If it was not processed before
	if (m_cat_ca_desc_list.size() == 0)
	{
	    if (ts_process_routines::GetPacketPID(packet) == PID_CAT)	//CAT
	    {
			ts_payload_parser::ts_section_list found_sections;
			if (m_CATParser.AddPacket(packet, TS_PACKET_SIZE, found_sections))
			{
				for (unsigned int ii=0; ii<found_sections.size() && m_cat_ca_desc_list.size() == 0; ii++)
                {
					ts_process_routines::GetCADescriptorsFromCAT(found_sections[ii].section, found_sections[ii].length, m_cat_ca_desc_list);
                    if (m_cat_ca_desc_list.size() > 0)
                    {
                        m_CATSectionBuffer.clear();
						m_CATSectionBuffer.assign(found_sections[ii].section, found_sections[ii].section + found_sections[ii].length);
                    }
                }
				m_CATParser.ResetFoundSections(found_sections);

		        log_info(L"Successfully parsed CAT CA descriptors");

                ret_val = true;
			}
    	}
	}
    else
    {
        ret_val = true;
    }
    return ret_val;
}

void tvs_program_streamer::SendStreamWithCATFilter(const unsigned char* buf, unsigned long len)
{
    const unsigned char* start = buf;
    const unsigned char* cur = buf;
    const unsigned char* end = buf + len;
	size_t to_send = 0;

	while (cur < end)
	{
		unsigned short pid = ts_process_routines::GetPacketPID(cur);
		if (pid == PID_CAT)
		{
			if (to_send)
			{
				spts_stream_converter_.ProcessStream(start, to_send);
				to_send = 0;
			}

			cur += TS_PACKET_SIZE;
			start = cur;
		} else
		{
			cur += TS_PACKET_SIZE;
			to_send += TS_PACKET_SIZE;
		}
	}

	if (to_send)
	{
		spts_stream_converter_.ProcessStream(start, to_send);
	}
}

void tvs_program_streamer::converter_callback(const unsigned char* buf, unsigned long len, void* user_param)
{
	tvs_program_streamer* parent = (tvs_program_streamer*)user_param;
	parent->user_cb_(parent->channel_id_, buf, len, parent->user_param_);
}

void tvs_program_streamer::write_stream(const unsigned char* buf, unsigned long len)
{
    pid_filter::pid_filter_map_t filters;
    pid_filter_.snapshot(filters);

    unsigned char* curptr = (unsigned char*)buf;
    unsigned char* start = (unsigned char*)buf;
    size_t to_send = 0;

    //add new packet(s) to accumulation buffer
    size_t num = len / TS_PACKET_SIZE;
    for (size_t i = 0; i < num; i++)
    {
        if (filters.find(ts_process_routines::GetPacketPID(curptr)) != filters.end())
        {
            to_send += TS_PACKET_SIZE;
            curptr += TS_PACKET_SIZE;
        }
        else
        {
            if (to_send)
            {
                write_stream_impl(start, to_send);
                to_send = 0;
            }
            curptr += TS_PACKET_SIZE;
            start = curptr;
        }
    }

    if (to_send)
    {
        write_stream_impl(start, to_send);
    }
}

void tvs_program_streamer::process_streaming_data(const unsigned char* buf, unsigned long len)
{
    check_pmt_version(buf, len);

#ifdef TVS_PLUGINS_SUPPORTED
	if (plugin_manager_ != NULL)
		plugin_manager_->process_stream(buf, len);
#endif

	SendStreamWithCATFilter(buf, len);
}

void tvs_program_streamer::write_stream_impl(const unsigned char* buf, unsigned long len)
{
    if (program_change_state_ == ETVS_PST_STREAMING)
    {
        //during streaming - assemble packets into bigger batches
		int remaining_length = len;
		while (remaining_length > 0)
		{
			int added_length;
			bool b_send = stream_buffer_.AddPackets(buf + (len - remaining_length), remaining_length, added_length);
			if (b_send)
			{
				int packet_num;
				unsigned char* sendbuf = stream_buffer_.GetPacketBuffer(packet_num);
				process_streaming_data(sendbuf, packet_num*TS_PACKET_SIZE);
				stream_buffer_.Reset();
			}
			remaining_length -= added_length;
		}
    } else
    {
	    int c = len / TS_PACKET_SIZE;
	    for (int i=0; i<c; i++)
	    {
		    const unsigned char* curpack = buf + i*TS_PACKET_SIZE;
		    switch (program_change_state_)
		    {
		    case ETVS_PST_CA:
			    if (ProcessCATPacket(curpack) || abort_ca_scan_)
                {
                    SetCACompletedScanState();
                }
                break;
		    case ETVS_PST_PAT_COMPLETED:
			    ProcessCATPacket(curpack);
				    break;
            case ETVS_PST_CA_COMPLETED:
		    case ETVS_PST_IDLE:
		    case ETVS_PST_PMT_COMPLETED:
			    break;
		    case ETVS_PST_PAT:
			    {
				    ProcessCATPacket(curpack);

				    if (ts_process_routines::GetPacketPID(curpack) == 0x00)	//PAT
				    {
					    ts_payload_parser::ts_section_list found_sections;
					    if (m_SectionPayloadParser.AddPacket(curpack, TS_PACKET_SIZE, found_sections) > 0)
					    {
						    m_pmt_pid = 0;
                            log_ext_info(L"required sid %1%") % program_info_.sid_;
						    for (unsigned int ii=0; (ii<found_sections.size()) && (m_pmt_pid == 0); ii++)
						    {
                                std::wstringstream buf;
							    //get PMT pid for our program from PAT
							    std::vector<STSPATServiceInfo> services;
							    ts_process_routines::GetPATStreamPIDs(found_sections[ii].section, found_sections[ii].length, services);
                                buf << L"services (" << services.size() << L"): ";
							    for (unsigned int srv_idx=0; srv_idx<services.size(); srv_idx++)
							    {
                                    buf << services[srv_idx].sid << L", ";
								    if (services[srv_idx].sid == program_info_.sid_)
								    {
									    //Found!
									    m_pmt_pid = services[srv_idx].pmt_pid;
									    break;
								    }
							    }
                                log_ext_info(buf.str().c_str());
							    if (m_pmt_pid != 0)
                                {
								    SetPATCompletedScanState();
                                    log_ext_info(L"pmt pid found: %1%") % m_pmt_pid;
                                }
						    }
						    m_SectionPayloadParser.ResetFoundSections(found_sections);
					    }
				    }
			    }
			    break;
		    case ETVS_PST_PMT:
			    {
				    ProcessCATPacket(curpack);

				    if (ts_process_routines::GetPacketPID(curpack) == m_pmt_pid)
				    {
					    ts_payload_parser::ts_section_list found_sections;
					    if (m_SectionPayloadParser.AddPacket(curpack, TS_PACKET_SIZE, found_sections) > 0)
					    {
						    for (unsigned int ii=0; ii<found_sections.size(); ii++)
						    {
							    //check whether this is PMT for our service
							    unsigned short sid = 0;
							    if (ts_process_routines::GetPMTSectionServiceID(found_sections[ii].section, found_sections[ii].length, sid) &&
								    sid == program_info_.sid_)
							    {
							        //Get PMT version
							        pmt_version_ = ts_process_routines::GetPMTVersion(found_sections[ii].section, found_sections[ii].length);
								    //Get PIDs of all elementary streams from PMT
								    ts_process_routines::GetPMTStreamsInfo(found_sections[ii].section, found_sections[ii].length, m_ProgramStreamInfo);
								    //Get PCR PID
								    ts_process_routines::GetPMTSectionPCRPID(found_sections[ii].section, found_sections[ii].length, m_pcr_pid);
								    //Get CA descriptors from PMT
                                    m_pmt_ca_desc_list.clear();
                                    std::vector<STSCADescriptorInfo> ca_desc_list;
                        	        ts_process_routines::GetCADescriptorsFromPMT(found_sections[ii].section, found_sections[ii].length, ca_desc_list);
                                    m_pmt_ca_desc_list.insert(m_pmt_ca_desc_list.begin(),
                                        ca_desc_list.begin(), ca_desc_list.end());
								    //Get CA descriptors from PMT for each stream
                                    ca_desc_list.clear();
                                    std::vector<STSESInfo> stream_info;
                                    ts_process_routines::GetPMTStreamsInfo(found_sections[ii].section, found_sections[ii].length, stream_info);
                                    for (unsigned int str_idx = 0; str_idx < stream_info.size(); str_idx++)
                                    {
                                        if (stream_info[str_idx].ca_descriptors.size() > 0)
                                        {
                                            m_pmt_ca_desc_list.insert(m_pmt_ca_desc_list.begin(),
                                                stream_info[str_idx].ca_descriptors.begin(), stream_info[str_idx].ca_descriptors.end());
                                        }
                                    }
                                    //filter duplicate CA descriptors
                                    ca_desc_list.clear();
                                    std::map<unsigned long, unsigned long> ca_map;
                                    for (unsigned int ca_idx=0; ca_idx<m_pmt_ca_desc_list.size(); ca_idx++)
                                    {
                                        unsigned long key = m_pmt_ca_desc_list[ca_idx].CI_system_ID;
                                        key = key << 16;
                                        key |= m_pmt_ca_desc_list[ca_idx].PID;
                                        if (ca_map.find(key) == ca_map.end())
                                        {
                                            ca_map[key] = key;
                                            ca_desc_list.push_back(m_pmt_ca_desc_list[ca_idx]);
                                        }
                                    }
                                    m_pmt_ca_desc_list = ca_desc_list;
                                    
                                    //Save PMT section for use with CI module
									m_PMTSectionBuffer.assign(found_sections[ii].section, found_sections[ii].section + found_sections[ii].length);
								    //change state
								    SetPMTCompletedScanState();
							    }
						    }
						    m_SectionPayloadParser.ResetFoundSections(found_sections);
					    }
				    }
			    }
			    break;
		    default:
			    break;
		    }
	    }
    }
}

void tvs_program_streamer::SetIdleScanState()
{
	program_change_state_ = ETVS_PST_IDLE;
}

void tvs_program_streamer::SetPATScanState()
{
    log_info(L"tvs_program_streamer::SetPATScanState. Waiting for PAT...");

	abort_ca_scan_ = false;

	AddPID(PID_PAT);
	m_SectionPayloadParser.Init(PID_PAT);
	AddPID(PID_CAT);
	m_CATParser.Init(PID_CAT);
    m_CATSectionBuffer.clear();
    m_cat_ca_desc_list.clear();
	program_change_state_ = ETVS_PST_PAT;
}

void tvs_program_streamer::SetPATCompletedScanState()
{
	program_change_state_ = ETVS_PST_PAT_COMPLETED;
}

void tvs_program_streamer::SetPMTScanState()
{
	AddPID(m_pmt_pid);
	m_SectionPayloadParser.Init(m_pmt_pid);
	program_change_state_ = ETVS_PST_PMT;
}

void tvs_program_streamer::SetPMTCompletedScanState()
{
	program_change_state_ = ETVS_PST_PMT_COMPLETED;
}

void tvs_program_streamer::SetCAScanState()
{
	program_change_state_ = ETVS_PST_CA;
}

void tvs_program_streamer::SetCACompletedScanState()
{
	program_change_state_ = ETVS_PST_CA_COMPLETED;
}

void tvs_program_streamer::SetStreamingState()
{
	std::vector<unsigned short> pids;
	get_elementary_pids(pids);
	spts_stream_converter_.Init(program_info_.nid_, program_info_.tid_, program_info_.sid_, m_pmt_pid, pids, converter_callback, this);

	//init section parser to continue gather and process PMT packets
	m_SectionPayloadParser.Init(m_pmt_pid);

	program_change_state_ = ETVS_PST_STREAMING;
}

#ifdef TVS_PLUGINS_SUPPORTED

void tvs_program_streamer::set_plugin_manager(tvs_plugin_manager_base* plugin_man)
{
    plugin_manager_ = plugin_man;
    if (plugin_manager_ != NULL)
        plugin_manager_->set_addpid_func(cb_add_pid, this);
}

tvs_plugin_manager_base* tvs_program_streamer::release_plugin_manager()
{
    if (plugin_manager_ != NULL)
        plugin_manager_->reset_addpid_func(); 
    return plugin_manager_;
}

void __stdcall tvs_program_streamer::cb_add_pid(unsigned short pid, void* user_param)
{
    tvs_program_streamer* parent = (tvs_program_streamer*)user_param;
    parent->AddPID(pid);
}

#endif

void tvs_program_streamer::AddPID(unsigned short pid)
{
    pid_filter_.add_filter(pid);
	stream_source_->AddPID(channel_id_.c_str(), pid);
}

void tvs_program_streamer::AddPIDs(std::vector<unsigned short>& pids)
{
    for (size_t i=0; i<pids.size(); i++)
        pid_filter_.add_filter(pids[i]);
        
	stream_source_->AddPIDs(channel_id_.c_str(), pids);
}

void tvs_program_streamer::check_pmt_version(const unsigned char* buf, unsigned long len)
{
    int c = len / TS_PACKET_SIZE;
    for (int i=0; i<c; i++)
    {
	    const unsigned char* curpack = buf + i*TS_PACKET_SIZE;
	    if (ts_process_routines::GetPacketPID(curpack) == m_pmt_pid)
	    {
            ts_payload_parser::ts_section_list found_sections;
            if (m_SectionPayloadParser.AddPacket(curpack, TS_PACKET_SIZE, found_sections) > 0)
            {
                for (unsigned int ii=0; ii<found_sections.size(); ii++)
                {
	                //check whether this is PMT for our service
	                unsigned short sid = 0;
	                if (ts_process_routines::GetPMTSectionServiceID(found_sections[ii].section, found_sections[ii].length, sid) &&
		                sid == program_info_.sid_)
	                {
	                    //Get PMT version
	                    unsigned char version = ts_process_routines::GetPMTVersion(found_sections[ii].section, found_sections[ii].length);
	                    if (pmt_version_ != version)
	                    {
                            log_info(L"tvs_program_streamer::check_pmt_version. New PMT version is detected: %1%, %2%") % version % pmt_version_;
	                        pmt_version_ = version;
		                    //Get PIDs of all elementary streams from PMT
		                    ts_process_routines::GetPMTStreamsInfo(found_sections[ii].section, found_sections[ii].length, m_ProgramStreamInfo);
		                    //Get PCR PID
		                    ts_process_routines::GetPMTSectionPCRPID(found_sections[ii].section, found_sections[ii].length, m_pcr_pid);
		                    //add pids
		                    std::vector<unsigned short> pids;
		                    get_elementary_pids(pids);
		                    
		                    for (size_t p=0; p<pids.size(); p++)
                                pid_filter_.add_filter(pids[p]);
                                
	                        stream_source_->cmdAddPIDs(channel_id_.c_str(), &pids);
	                        //update pid list within converter core
                            spts_stream_converter_.update_program_pids(pids);
				            //Send PMT section to hardware CI module
                            m_PMTSectionBuffer.clear();
							m_PMTSectionBuffer.assign(found_sections[ii].section, found_sections[ii].section + found_sections[ii].length);
				            if (program_info_.encrypted_)
				            {
					            stream_source_->cmdSendPMTToCI(&m_PMTSectionBuffer[0], m_PMTSectionBuffer.size(), dvblink::SERVICE_DECRYPT_UPDATE);
								//also try sending the pmt pid (only for Octopus.net sat2ip tuner. Other tuners do not implement this function
								stream_source_->cmdSendPMTPidToCI(m_pmt_pid, program_info_.sid_, dvblink::SERVICE_DECRYPT_UPDATE);

	                #ifdef TVS_PLUGINS_SUPPORTED
	                            //update pids to decrypt at plugins
				                if (plugin_manager_ != NULL)
				                {
				                    std::vector<unsigned short> pids_to_decrypt;
	                                get_elementary_pids(pids_to_decrypt);
					                plugin_manager_->add_pids_to_decrypt(pids_to_decrypt);
                                }
	                #endif					            
                            }
                        }
                    }
                }
			    m_SectionPayloadParser.ResetFoundSections(found_sections);
            }
        }
    }
}
