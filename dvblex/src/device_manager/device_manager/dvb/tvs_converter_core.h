/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef _TVSCONVERTERCORE_H_
#define _TVSCONVERTERCORE_H_

#include <boost/thread/locks.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"

#include <map>
#include <vector>
#include <dl_ts_info.h>

typedef void (*LPCB_TVS_CCSTREAMFUNC)(const unsigned char* buf, unsigned long len, void* user_param);

class CTVSConverterCore
{
public:
	CTVSConverterCore();
	~CTVSConverterCore();

    int Init(unsigned short cur_nid, unsigned short cur_tid, unsigned short cur_sid, unsigned short pmt_pid, std::vector<unsigned short>& program_pids, LPCB_TVS_CCSTREAMFUNC cb, void* user_param);
	void Term();

	int ProcessStream(const unsigned char* Buf, unsigned long Len);
	//update_program_pids can only be called in the same thread as ProcessStream! (there is no concurrency protection)
	void update_program_pids(std::vector<unsigned short>& program_pids);

protected:
    static void SendStreamingData(const unsigned char* buf, int len, void* param);
    void CreatePATPacket();
    void CheckTimer();

    void ProcessPMT(const unsigned char* packet);

    void StreamDescEventFunc(const boost::system::error_code& e);

private:
    LPCB_TVS_CCSTREAMFUNC m_StreamCallback;
	void* m_UserParam;
    unsigned short m_NID;
    unsigned short m_TID;
    unsigned short m_SID;
    unsigned short m_PMTPid;
	//map of pids of elementary streams for fast lookup
	std::map<unsigned short, unsigned short> m_PIDs;

	//Continuity counter for PAT packets
	unsigned short m_NewPATContinuityCounter;	
	//PAT packet version number
	unsigned char m_PATVersionNumber;
	//PMT packet continuity counter
	unsigned short m_NewPMTContinuityCounter;

	//PMT parser
	dvblink::engine::ts_section_payload_parser m_PMTParser;

	//TS packet generator
	dvblink::engine::ts_packet_generator m_PacketGen;
    //Watchdog to generate description packets
    boost::posix_time::ptime prev_time_;
};

///////////////////////////////////////////////////////////////////////////////
#endif //_TVSCONVERTERCORE_H_

