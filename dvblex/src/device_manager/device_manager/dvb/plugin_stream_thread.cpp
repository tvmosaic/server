/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <dl_logger.h>
#include <dl_utils.h>
#include <dl_strings.h>
#include <dl_common.h>
#include "plugin_stream_thread.h"

using namespace std;
using namespace dvblink::engine;
using namespace dvblink::logging;

plugin_stream_thread::plugin_stream_thread(tvs_program_streamer* program_src)
{
	program_src_ = program_src;
	buf_ = NULL;
	len_ = 0;
	exit_ = false;
	streaming_thread_ = new boost::thread(boost::bind(&plugin_stream_thread::streaming_thread_func, this));
}

plugin_stream_thread::~plugin_stream_thread()
{
	exit_ = true;
	wait_event_.signal();
	streaming_thread_->join();
	delete streaming_thread_;
}

void plugin_stream_thread::write_stream(const unsigned char* buf, size_t len)
{
	write_finished_event_.reset();
	buf_ = buf;
	len_ = len;
	wait_event_.signal();
}

void plugin_stream_thread::wait_for_write_to_finish()
{
	write_finished_event_.wait();
}

void plugin_stream_thread::streaming_thread_func()
{
	while (true)
	{
		wait_event_.wait();
		//check if exit was signaled
		if (exit_)
			break;
		//else process buffer
		program_src_->write_stream(buf_, len_);
		wait_event_.reset();
		write_finished_event_.signal();
	}
}
