/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <dl_logger.h>
#include <dl_utils.h>
#include <dvblex_net_lib/dl_network_helper.h>
#include <dl_file_procedures.h>
#include "tvs_stream_src.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

CTVSStreamSource::CTVSStreamSource() :
    tuner_(NULL),
    m_CircleBuffer(256, 40 * 188, L"CTVSStreamSource"),
    m_StreamingThread(NULL),
	stream_buffer_(40),
    is_tuner_started_(false),
    full_ts_f_(NULL),
    extra_stream_callback_(NULL)
{
}

CTVSStreamSource::~CTVSStreamSource()
{
    Term();
}

void CTVSStreamSource::Init(dvblink::tuner_t* tuner, const tvs_stream_source_options_t& options, LPCB_TVS_STREAMFUNC stream_callback, void* user_param)
{
    tuner_ = tuner;
	m_StreamCallback = stream_callback;
	m_UserParam = user_param;
    extra_stream_callback_ = NULL;
	m_StreamingThread = NULL;
    m_prev_diseqc_value = dvblink::DISEQC_NONE;
    options_ = options;
    is_tuner_started_ = false;

    //start driver handling thread
	m_StreamExitFlag = false;
	m_StreamingThread = new boost::thread(boost::bind(&CTVSStreamSource::StreamingThread, this));
//	SetThreadPriority(m_StreamingThread, THREAD_PRIORITY_HIGHEST);

    prev_time_ = boost::posix_time::microsec_clock::universal_time();
}

void CTVSStreamSource::Term()
{
	//Stop streaming thread
	if (m_StreamingThread != NULL)
	{
		//signal streaming thread break
		m_StreamExitFlag = true;

		//wait for it to be stopped
		log_info(L"CTVSStreamSource::Term. Waiting for streaming thread to stop");
		m_StreamingThread->join();
		delete m_StreamingThread;
		m_StreamingThread = NULL;

		log_info(L"CTVSStreamSource::Term. Streaming is stopped");
	}

	log_info(L"CTVSStreamSource::Term. Stream source is stopped");
}

unsigned long CTVSStreamSource::GetAdditionalTuneDelay(unsigned char diseqc_value)
{
	unsigned long ret_val = 0;

	if (options_.tuner_type_ == dvblink::TUNERTYPE_DVBS && diseqc_value == dvblink::DISEQC_12)
		ret_val = options_.custom_diseqc_delay_sec_*1000;

	return ret_val;
}

bool CTVSStreamSource::IntTuneTransponder(dvblink::TTransponderInfo* transponder_info, unsigned char diseqc_value, const wchar_t* diseqc_rawdata)
{
	bool ret_val = false;

    if (tuner_ == NULL)
        return false;

    //make a copy as transponder data can be adjusted (in case of Unicable for example)
    dvblink::TTransponderInfo tr_info = *transponder_info;

	//Select LNB for satellite device
	if (options_.tuner_type_ == dvblink::TUNERTYPE_DVBS)
		ret_val = SelectLNB(&tr_info, diseqc_value, diseqc_rawdata);
	else
		ret_val = true; //to continue processing
	//if Ok
	if (ret_val)
	{
		//Tune to a new transponder
		int res = tuner_->SetTuner(&tr_info);
		if (res == dvblink::SUCCESS)
		{
			ret_val = true;
		}
        else
		{
			log_error(L"CTVSStreamSource::IntTuneTransponder. tuner_->SetTuner returned an error");
			ret_val = false;
		}
	}
    else
	{
		log_error(L"CTVSStreamSource::IntTuneTransponder. Failed to select LNB for diseqc value %1%") % diseqc_value;
	}

    return ret_val;
}

bool CTVSStreamSource::cmdTuneTransponder(
    dvblink::TTransponderInfo* transponder, unsigned char diseqc_value, const wchar_t* diseqc_rawdata)
{
	bool ret_val = false;

	log_info(L"CTVSStreamSource::TuneTransponder. Transponder tuning request: diseqc %1%, "
        L"freq %2%, modulation %3%, polarization %4%, symbol rate %5%, LOF %6%, LNB selection signal %7%, FEC %8%") %
		diseqc_value % transponder->dwFreq % transponder->dwModulation % transponder->Pol %
        transponder->dwSr % transponder->dwLOF % transponder->dwLnbKHz % transponder->dwFec;

    //close input stream pipeline
    boost::unique_lock<boost::mutex> lock(m_InputStreamMutex);

    //clear circle buffer
    m_CircleBuffer.reset();
	//reset packet buffer
	stream_buffer_.Reset();
    //Stop stream on the current channel
    StopStream();

    ret_val = IntTuneTransponder(transponder, diseqc_value, diseqc_rawdata);

    //create full ts file for debugging purposes
    create_full_ts_file(transponder->dwFreq);

	return ret_val;
}

int CTVSStreamSource::StopStream()
{
	DeleteAllPIDs();

	return 1;
}

void CTVSStreamSource::StreamCallback(unsigned char *data, int len, void* param)
{
	CTVSStreamSource* parent = (CTVSStreamSource*)param;

	//check whether input stream pipeline is open
    boost::unique_lock<boost::mutex> lock(parent->m_InputStreamMutex, boost::try_to_lock);
    if (lock.owns_lock())
    {
        //if debug file is open - write data there first
        if (parent->full_ts_f_ != NULL)
            fwrite(data, len, 1, parent->full_ts_f_);

        pid_filter::pid_filter_map_t filters;
        parent->pid_filter_.snapshot(filters);

        unsigned char* curptr = data;
        unsigned char* start = data;
        size_t to_send = 0;

		//add new packet(s) to accumulation buffer
		size_t num = len / TS_PACKET_SIZE;
		for (size_t i = 0; i < num; i++)
		{
            if (filters.find(ts_process_routines::GetPacketPID(curptr)) != filters.end())
            {
                to_send += TS_PACKET_SIZE;
                curptr += TS_PACKET_SIZE;
            }
            else
            {
                if (to_send)
                {
                    parent->process_incoming_stream(start, to_send);
                    to_send = 0;
                }
                curptr += TS_PACKET_SIZE;
                start = curptr;
            }
        }

        if (to_send)
        {
            parent->process_incoming_stream(start, to_send);
        }
    }
}

bool CTVSStreamSource::check_timer()
{
    bool ret_val = false;
    
    boost::posix_time::ptime cur_time = boost::posix_time::microsec_clock::universal_time();
    
    boost::posix_time::time_duration time_span(cur_time - prev_time_);

    if (time_span.total_milliseconds() > 100)
    {
        prev_time_ = cur_time;
        ret_val = true;
    }
    return ret_val;
}

void CTVSStreamSource::process_incoming_stream(unsigned char *Data, int len)
{
	if (len > 0)
	{
		int remaining_length = len;
		bool b_send = false;
		while (remaining_length > 0)
		{
			int added_length;
			bool b = stream_buffer_.AddPackets(Data + (len - remaining_length), remaining_length, added_length);
			if (b)
			{
				int packet_num;
				unsigned char* buf = stream_buffer_.GetPacketBuffer(packet_num);
                m_CircleBuffer.write_stream(buf, packet_num*TS_PACKET_SIZE);
				stream_buffer_.Reset();
			}
			b_send |= b;
			remaining_length -= added_length;
		}
		if (!b_send)
		{
			if (check_timer())
			{
				int packet_num;
				unsigned char* buf = stream_buffer_.GetPacketBuffer(packet_num);
				if (packet_num > 0)
                {
					m_CircleBuffer.write_stream(buf, packet_num*TS_PACKET_SIZE);
                }

				stream_buffer_.Reset();
			}
		}
	}
}

bool CTVSStreamSource::cmdAddPID(const wchar_t* client, unsigned short pid)
{
	log_info(L"CTVSStreamSource::AddPID. Requesting PID %1%, client %2%") % pid % client;

    if (tuner_ == NULL)
        return false;

    std::wstring str_client = client;

    m_StreamPIDs.lock();
    
    std::map<unsigned short, stream_wstring_map_t>* container = m_StreamPIDs.get_container();
    bool pid_inthemap = container->find(pid) != container->end();
    if (!pid_inthemap)
        container->insert(make_pair(pid, stream_wstring_map_t()));
        
    (container->find(pid))->second.insert(make_pair(str_client, str_client));

    m_StreamPIDs.unlock();

	//check whether this pid was already added
	if (!pid_inthemap)
	{
        pid_filter_.add_filter(pid);
        tuner_->AddFilter(pid);
    }
		
	return true;
}

bool CTVSStreamSource::cmdAddPIDs(const wchar_t* client, std::vector<unsigned short>* pids)
{
    for (size_t i=0; i<pids->size(); i++)
        cmdAddPID(client, pids->at(i));
	return 1;
}

bool CTVSStreamSource::cmdRemovePid(const wchar_t* client, unsigned short pid)
{
	log_info(L"CTVSStreamSource::cmdRemovePid. Removing pid %1% for client %2%") % pid % client;

    if (tuner_ == NULL)
        return false;

    bool b_delete = false;

    m_StreamPIDs.lock();
    
    std::map<unsigned short, stream_wstring_map_t>* container = m_StreamPIDs.get_container();
    std::map<unsigned short, stream_wstring_map_t>::iterator it = container->find(pid);
    if (it != container->end())
    {
        if (it->second.find(client) != it->second.end())
            it->second.erase(client);
            
        if (it->second.size() == 0)
        {
            container->erase(it->first);
            b_delete = true;
        }
    }

    m_StreamPIDs.unlock();
	
	if (b_delete)
	{
	    pid_filter_.delete_filter(pid);
        tuner_->DelFilter(pid);
	}
	
	return true;
}

bool CTVSStreamSource::cmdRemoveAllPids(const wchar_t* client)
{
	log_info(L"CTVSStreamSource::RemoveAllPids. Removing PIDs for client %1%") % client;

    if (tuner_ == NULL)
        return false;

    std::vector<unsigned short> pids_to_delete;

    m_StreamPIDs.lock();
    
    std::map<unsigned short, stream_wstring_map_t>* container = m_StreamPIDs.get_container();
    std::map<unsigned short, stream_wstring_map_t>::iterator it = container->begin();
    while (it != container->end())
    {
        if (it->second.find(client) != it->second.end())
        {
            it->second.erase(client);
        }
        if (it->second.size() == 0)
        {
            pids_to_delete.push_back(it->first);
            container->erase(it->first);
            it = container->begin();
        } else
        {
            ++it;
        }
    }

    m_StreamPIDs.unlock();
	
	for (size_t i=0; i < pids_to_delete.size(); i++)
	{
        pid_filter_.delete_filter(pids_to_delete[i]);
        tuner_->DelFilter(pids_to_delete[i]);
	}
	
	return true;
}

int CTVSStreamSource::DeleteAllPIDs()
{
    std::vector<unsigned short> pids_to_delete;

    if (tuner_ == NULL)
        return 0;

    m_StreamPIDs.lock();
    
    std::map<unsigned short, stream_wstring_map_t>* container = m_StreamPIDs.get_container();
    std::map<unsigned short, stream_wstring_map_t>::iterator it = container->begin();
    while (it != container->end())
    {
        pids_to_delete.push_back(it->first);
        ++it;
    }

    m_StreamPIDs.unlock();
	
	for (size_t i=0; i < pids_to_delete.size(); i++)
        tuner_->DelFilter(pids_to_delete[i]);
	
	m_StreamPIDs.clear();
	
	pid_filter_.reset();
	
	return 1;
}

unsigned char GetDiseqcCommandByte(dvblink::TTransponderInfo* transponder, unsigned char lnb_num)
{
	unsigned char cmd = 0xF0;
	//hi/lo band - 22KHz selection flag
	if (transponder->dwLnbKHz == dvblink::LNB_SELECTION_22)
		cmd |= 0x01;
	if (transponder->Pol == dvblink::POL_HORIZONTAL)
		cmd |= 0x02;
	cmd |= ((lnb_num << 2) & 0x0C);
	return cmd;
}

bool ParseDiseqcString(const wchar_t* diseqc_rawdata, unsigned char* buffer, unsigned char& buffer_size)
{
    bool ret_val = false;
    std::wstring normalized_rawdata;
    //strip all spaces from the string
    for (unsigned int i=0; i<wcslen(diseqc_rawdata); i++)
    {
        if (diseqc_rawdata[i] != L' ')
            normalized_rawdata += diseqc_rawdata[i];
    }
    if (normalized_rawdata.size() > 0 && //string is not empty
        (normalized_rawdata.size() % 2) == 0 && //each byte has to be represented by two characters
        normalized_rawdata.size()/2 <= buffer_size) //string is not bigger than diseqc buffer size
    {
        ret_val = true;

        boost::to_lower(normalized_rawdata); 
        buffer_size = 0;
        while (buffer_size < normalized_rawdata.size()/2)
        {
            unsigned int data;
            const wchar_t* curstr = normalized_rawdata.c_str() + buffer_size*2;
            wchar_t buf[3];
            buf[0] = curstr[0]; buf[1] = curstr[1]; buf[2] = L'\0';

            int res = swscanf(buf, L"%2x", &data);
            if (res != 1)
            {
                log_error(L"ParseDiseqcRawData. Cannot convert %1% to HEX number") % buf;
                ret_val = false;
                break;
            }
            buffer[buffer_size] = data;
            buffer_size += 1;
        }
    }
    else
    {
        log_error(L"ParseDiseqcRawData. Raw data string size is incorrect: %1%") % normalized_rawdata.size();
    }
    return ret_val;
}

#define MAX_DELAY_STRING_SIZE   4

bool ParseDiseqcRawData(const wchar_t* diseqc_rawdata, TTVSDiseqcCommandSeq& commands)
{
    bool ret_val = false;
    std::wstring normalized_rawdata;
    //strip all spaces from the string
    for (unsigned int i=0; i<wcslen(diseqc_rawdata); i++)
    {
        if (diseqc_rawdata[i] != L' ')
            normalized_rawdata += diseqc_rawdata[i];
    }
    boost::to_lower(normalized_rawdata); 
    if (normalized_rawdata.size() > 0)
    {
        //make a duplicate
        wchar_t* temp_str = wcsdup(normalized_rawdata.c_str());
        //go through the string with wcstok, searching for W symbol
        wchar_t* cur_str = temp_str;
        while (true)
        {
            wchar_t* token_ptr = wcschr(cur_str, L'w');
            if (token_ptr != NULL)
            {
                //set \0 into this position
                *token_ptr = L'\0';
            }
            //everything before the token is the diseqc command
            if (wcslen(cur_str) > 0)
            {
                TTVSDiseqcChunk chunck;
                chunck.type = EDCT_COMMAND;
                memset(&chunck.command, 0, sizeof(chunck.command));
                chunck.command.dwSize = sizeof(chunck.command);
                chunck.command.iLength = 16;
                if (ParseDiseqcString(cur_str, chunck.command.Data, chunck.command.iLength))
                {
                    commands.push_back(chunck);
                    //advance cur_str pointer
                    cur_str += wcslen(cur_str);
                }
                else
                {
                    //error - exit parsing
                    ret_val = false;
                    break;
                }
            }
            //4 digits after the token - delay
            if (token_ptr != NULL)
            { 
                wchar_t* delay_str = token_ptr + 1;
                if (wcslen(delay_str) >= MAX_DELAY_STRING_SIZE)
                {
                    wchar_t numbuf[MAX_DELAY_STRING_SIZE+1];
                    wcsncpy(numbuf, delay_str, MAX_DELAY_STRING_SIZE);
                    TTVSDiseqcChunk chunck;
                    chunck.type = EDCT_DELAY;
                    string_conv::apply(numbuf, chunck.delay, static_cast<unsigned long>(0));
                    commands.push_back(chunck);

                    //advance cur_str pointer
                    cur_str += MAX_DELAY_STRING_SIZE + 1;
                }
                else
                {
                    //error
                    ret_val = false;
                    break;
                }
            }
            //check whether we need to break here
            if (wcslen(cur_str) == 0)
            {
                //success
                ret_val = true;
                break;
            }
        }
        free(temp_str);
    }
    return ret_val;
}

bool CTVSStreamSource::IsSameLNB(dvblink::TTransponderInfo* transponder, unsigned char diseqc_value, const wchar_t* diseqc_rawdata)
{
#ifndef WIN32
	//always send diseqc on linux platform
	return false;
#endif

	bool ret_val = false;

    //check diseqc value first
    if (m_prev_diseqc_value == diseqc_value)
    {
        unsigned char cbyte;
        //if this is diseqc 1.0 - check command byte
        //if this is diseqc 1.2 - check raw_data
        switch (diseqc_value)
        {
        	case dvblink::DISEQC_LEVEL_1_A_A:
                cbyte = GetDiseqcCommandByte(transponder, 0);
                if (cbyte == m_prev_command_byte)
                    ret_val = true;
                break;
	        case dvblink::DISEQC_LEVEL_1_B_A:
                cbyte = GetDiseqcCommandByte(transponder, 1);
                if (cbyte == m_prev_command_byte)
                    ret_val = true;
                break;
	        case dvblink::DISEQC_LEVEL_1_A_B:
                cbyte = GetDiseqcCommandByte(transponder, 2);
                if (cbyte == m_prev_command_byte)
                    ret_val = true;
                break;
	        case dvblink::DISEQC_LEVEL_1_B_B:
                cbyte = GetDiseqcCommandByte(transponder, 3);
                if (cbyte == m_prev_command_byte)
                    ret_val = true;
                break;
	        case dvblink::DISEQC_12:
                ret_val = boost::iequals(diseqc_rawdata, m_prev_diseqc_rawdata.c_str());
                break;
        }
    }

    return ret_val;
}

bool CTVSStreamSource::SelectLNB(dvblink::TTransponderInfo* transponder, unsigned char diseqc_value, const wchar_t* diseqc_rawdata)
{
	bool ret_val = true;

    if (tuner_ == NULL)
        return false;

    bool bSameLNB = IsSameLNB(transponder, diseqc_value, diseqc_rawdata);
    if (bSameLNB)
    {
        log_info(L"CTVSStreamSource::SelectLNB. Request for the currently selected LNB. No diseqc command will be sent");
        return true;
    }

	log_info(L"CTVSStreamSource::SelectLNB. Selecting LNB for diseqc value %1%") % diseqc_value;

    m_prev_diseqc_value = diseqc_value;

	int tone_burst = dvblink::TONEBURST_NONE;
	bool bSendDiseqc = false;
	dvblink::TDiseqcCmd diseqc_cmd;
	memset(&diseqc_cmd, 0, sizeof(dvblink::TDiseqcCmd));
	diseqc_cmd.dwSize = sizeof(diseqc_cmd);

	switch (diseqc_value)
	{
	case dvblink::DISEQC_NONE:
		break;
	case dvblink::DISEQC_SIMPLE_A:
		tone_burst = dvblink::TONEBURST_1;
		break;
	case dvblink::DISEQC_SIMPLE_B:
		tone_burst = dvblink::TONEBURST_2;
		break;
	case dvblink::DISEQC_LEVEL_1_A_A:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[3] = {0xE0, 0x10, 0x38};
            memcpy(diseqc_cmd.Data, data, 3);
            diseqc_cmd.Data[3] = GetDiseqcCommandByte(transponder, 0);
            m_prev_command_byte = diseqc_cmd.Data[3];
        }
		break;
	case dvblink::DISEQC_LEVEL_1_B_A:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[3] = {0xE0, 0x10, 0x38};
            memcpy(diseqc_cmd.Data, data, 3);
            diseqc_cmd.Data[3] = GetDiseqcCommandByte(transponder, 1);
            m_prev_command_byte = diseqc_cmd.Data[3];
        }
		break;
	case dvblink::DISEQC_LEVEL_1_A_B:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[3] = {0xE0, 0x10, 0x38};
            memcpy(diseqc_cmd.Data, data, 3);
            diseqc_cmd.Data[3] = GetDiseqcCommandByte(transponder, 2);
            m_prev_command_byte = diseqc_cmd.Data[3];
        }
		break;
	case dvblink::DISEQC_LEVEL_1_B_B:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[3] = {0xE0, 0x10, 0x38};
            memcpy(diseqc_cmd.Data, data, 3);
            diseqc_cmd.Data[3] = GetDiseqcCommandByte(transponder, 3);
            m_prev_command_byte = diseqc_cmd.Data[3];
        }
		break;
	case dvblink::DISEQC_LEVEL_11_1:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xF0};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_2:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xF1};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_3:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xF2};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_4:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xF3};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_5:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xF4};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_6:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xF5};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_7:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xF6};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_8:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xF7};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_9:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xF8};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_10:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xF9};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_11:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xFA};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_12:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xFB};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_13:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xFC};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_14:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xFD};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_15:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xFE};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_LEVEL_11_16:
        {
		    bSendDiseqc = true;
		    diseqc_cmd.iLength = 4;
            unsigned char data[4] = {0xE0, 0x10, 0x39, 0xFF};
            memcpy(diseqc_cmd.Data, data, 4);
        }
		break;
	case dvblink::DISEQC_12:
        {
            m_prev_diseqc_rawdata = diseqc_rawdata;
            TTVSDiseqcCommandSeq commands;
            if (ParseDiseqcRawData(diseqc_rawdata, commands))
            {
                ret_val = SendDiseqcCommandSequence(commands);
                if (ret_val && options_.repeat_diseqc_)
                    SendDiseqcCommandSequence(commands);
            }
            else
            {
		        log_error(L"CTVSStreamSource::SelectLNB. Unable to parse custom diseqc string %1%") % diseqc_rawdata;
		        ret_val = false;
            }
        }
        break;
	case dvblink::DISEQC_UNICABLE:
        {
            std::wstring rawdata_string;
            if (GetUnicableDiseqcString(transponder, diseqc_rawdata, rawdata_string))
            {
                TTVSDiseqcCommandSeq commands;
                if (ParseDiseqcRawData(rawdata_string.c_str(), commands))
                {
#ifdef WIN32
                    //up voltage to 18V
                    tuner_->LNBPower(dvblink::POW_18V);
#endif
                    ret_val = SendDiseqcCommandSequence(commands);
                    if (ret_val && options_.repeat_diseqc_)
                        SendDiseqcCommandSequence(commands);
#ifdef WIN32
                    //reduce voltage to 13V
                    tuner_->LNBPower(dvblink::POW_13V);
#endif
                }
                else
                {
		            log_error(L"CTVSStreamSource::SelectLNB. Unable to parse unicable diseqc string %1%") % rawdata_string;
		            ret_val = false;
                }
            }
            else
            {
	            log_error(L"CTVSStreamSource::SelectLNB. GetUnicableDiseqcString failed for slot id %1%") % diseqc_rawdata;
	            ret_val = false;
            }
        }
        break;
	default:
		ret_val = false;
		log_error(L"CTVSStreamSource::SelectLNB. Unsupported diseqc value %1%") % diseqc_value;
		break;
	}

	if (ret_val && diseqc_value != dvblink::DISEQC_12 && diseqc_value != dvblink::DISEQC_UNICABLE)
	{
		if (tone_burst != dvblink::TONEBURST_NONE || bSendDiseqc)
		{
			dvblink::PDiseqcCmd pcmd = (bSendDiseqc) ? &diseqc_cmd : NULL;
			int res = tuner_->SendDiseqc(pcmd, tone_burst);
			if (res != dvblink::SUCCESS)
			{
				ret_val = false;
				log_error(L"CTVSStreamSource::SelectLNB. SendDiseqc command has failed %1%") % res;
			}
            else
            {
                if (options_.repeat_diseqc_)
                {
                    //Wait 50 ms
                	boost::this_thread::sleep(boost::posix_time::milliseconds(50));
                    //repeat diseqc command
                    tuner_->SendDiseqc(pcmd, tone_burst);
                }
            }
		}
	}

    if (!ret_val)
    {
        //clear previous LNB data in case of error
        m_prev_diseqc_value = dvblink::DISEQC_NONE;
    }

	return ret_val;
}

static double round_double(double r) 
{
    return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5);
}

bool CTVSStreamSource::GetUnicableDiseqcString(dvblink::TTransponderInfo* transponder, const wchar_t* slot_id,
                                               std::wstring& rawdata_string)
{
    bool ret_val = false;
    rawdata_string.clear();

    //find a slot for this channels
    int idx = -1;
    for (unsigned int i=0; i<options_.unicable_slots_.size(); i++)
    {
        if (boost::iequals(options_.unicable_slots_[i].id, slot_id))
        {
            //found!
            idx = i;
            break;
        }
    }
    if (idx >= 0)
    {
        //calculate diseqc sequence
        //E0 : command, 10: broadcast address, 5A: Channel change command
        //Data byte 0: b7,b6,b5: Slot Number (0-7), b4:PosA=0/PosB=1, b3: Vert=0/Horz=1,b2:LoBand=0/HiBand=1
        //Data byte 0:b1,b0; Data byte 1:b7-b0: 10 bits of the tuning word
        //tunning word: round((Transponder Freq  - LNB Freq + Slot Freq)/4 - 350), all freqs in MHz
        unsigned short tuning_word = static_cast<unsigned short>(round_double((transponder->dwFreq/1000 - transponder->dwLOF/1000 + options_.unicable_slots_[idx].frequency/1000)/4.0 - 350));
        unsigned char b0 = 0;
        //slot
        b0 = b0 | ((unsigned char)options_.unicable_slots_[idx].slot_number << 5);
        //PosA/B
        if (options_.unicable_slots_[idx].position == ETVS_USP_POS_B)
            b0 = b0 | 0x10;
        //vert/horz
        if (transponder->Pol == dvblink::POL_HORIZONTAL)
            b0 = b0 | 0x08;
        //lo/high band
        if (((unsigned long)transponder->dwFreq) > transponder->LOFSW)
            b0 = b0 | 0x04;

        //tuning word
        b0 = b0 | ((tuning_word >> 8) & 0x03);
        unsigned char b1 = (tuning_word & 0x00FF);

        rawdata_string = L"E0105A";
        rawdata_string += string_cast<EC_UTF8>(network_helper::decode_char_to_hex(b0));
        rawdata_string += string_cast<EC_UTF8>(network_helper::decode_char_to_hex(b1));

        //adjust frequency
        transponder->dwFreq = options_.unicable_slots_[idx].frequency + transponder->dwLOF;
        //adjust polarity (13V)
        transponder->Pol = dvblink::POL_VERTICAL;
        ret_val = true;
    }
    return ret_val;
}

bool CTVSStreamSource::SendDiseqcCommandSequence(TTVSDiseqcCommandSeq& commands)
{
    bool ret_val = true;

    if (tuner_ == NULL)
        return false;

    for (unsigned int i=0; i<commands.size() && ret_val; i++)
    {
        switch (commands[i].type)
        {
        case EDCT_DELAY:
            {
                log_info(L"CTVSStreamSource::SendDiseqcCommandSequence. Delay %1% milliseconds") % commands[i].delay;
                boost::this_thread::sleep(boost::posix_time::milliseconds(commands[i].delay));
            }
            break;
        case EDCT_COMMAND:
            {
                int res = tuner_->SendDiseqc(&commands[i].command, dvblink::TONEBURST_NONE);
                if (res != dvblink::SUCCESS)
                {
                    log_info(L"CTVSStreamSource::SendDiseqcCommandSequence. SendDiseqc failed");
                    ret_val = false;
                }
            }
            break;
        default:
            break;
        }
    }
    return ret_val;
}

bool CTVSStreamSource::cmdSendPMTToCI(unsigned char* pmt, int pmt_len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd)
{
    if (tuner_ == NULL)
        return false;

	tuner_->CISendPMT(pmt, pmt_len, cmd);
	return true;
}

bool CTVSStreamSource::cmdSendPMTPidToCI(unsigned short pmt_pid, unsigned short sid, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd)
{
    if (tuner_ == NULL)
    return false;

	tuner_->CISendPMTPid(pmt_pid, sid, cmd);
	return true;
}

bool CTVSStreamSource::cmdGetSignalStats(dvblink::TSignalInfo* signalInfo)
{
    if (tuner_ == NULL)
        return false;

	dvblink::TTransponderInfo tr;
    return (tuner_->GetTunerState(signalInfo, &tr) == dvblink::SUCCESS) ? true : false;
}

bool CTVSStreamSource::ShowDeviceDialog()
{
	return false;
}

bool CTVSStreamSource::cmdStop()
{
    UnloadAndDeinitDriver();

    return 1;
}

bool CTVSStreamSource::LoadAndInitDriver()
{
    bool ret_val = false;

    if (tuner_ == NULL)
        return false;

    //reset previous diseqc command
    m_prev_diseqc_value = dvblink::DISEQC_NONE;

	//Set transport stream callback
	tuner_->SetCallback(&StreamCallback, this);
    if (tuner_->StartDevice(options_.tuner_type_) == dvblink::SUCCESS)
	{
        //wait until graph becomes operational (obsolete?)
//            unsigned long wd = 50;
//            boost::this_thread::sleep(boost::posix_time::milliseconds(wd));
        is_tuner_started_ = true;
		ret_val = true;
	}
    else
	{
        log_error(L"CTVSStreamSource::LoadAndInitDriver. Failed to start device path %1%, type %2%, frontend %3%") % string_cast<EC_UTF8>(tuner_->get_device_path()) % (int)options_.tuner_type_ % tuner_->get_frontend_idx();
	}
    return ret_val;
}

void CTVSStreamSource::UnloadAndDeinitDriver()
{
    if (tuner_ == NULL)
        return;

    //close input stream pipeline
    boost::unique_lock<boost::mutex> lock(m_InputStreamMutex);

    //stop stream on current channel
	StopStream();
	//stop device 
    tuner_->StopDevice();
    //close debug file
    if (full_ts_f_ != NULL)
    {
        fclose(full_ts_f_);
        full_ts_f_ = NULL;
    }

    is_tuner_started_ = false;
}

//This thread assumes full control over the device itself
//No other thread can access device controls except through this one
void CTVSStreamSource::StreamingThread()
{
#ifdef WIN32
    //Initialize COM for this thread
    HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
    bool bCOMInitialized = (hr != RPC_E_CHANGED_MODE);
#endif

    try 
    {
	    //check in the loop whether new streaming data is available
	    while (!m_StreamExitFlag)
	    {
            ts_circle_buffer::node_t node = m_CircleBuffer.tear_node(50);

            //check command queue first
            SDLCommandItem* item;
            while (m_CommandQueue.PeekCommand(&item))
            {
                switch (item->id)
                {
                case EC_TUNE:
                    {
                        int res = 1;
                        //check whether device has been unloaded
                        if (!is_tuner_started_)
                        {
                            res = LoadAndInitDriver();
                        }

                        if (res > 0)
                        {
                            STuneParams* tune_params = (STuneParams*)item->param;
                            
                            tune_params->result = cmdTuneTransponder(
                                tune_params->transponder,
                                tune_params->diseqc_value,
                                tune_params->diseqc_rawdata);

                            //prevent from sending current buffer to a new stream
                            if (node != NULL)
                            {
                                m_CircleBuffer.put_node(node);
                                node = NULL;
                            }
                        }
                    }
                    break;
                case EC_GET_SIGNAL_STATS:
                    {
                        SGetSignalStatsParams* sstats_params = (SGetSignalStatsParams*)item->param;
                        sstats_params->result = cmdGetSignalStats(sstats_params->signalInfo);
                    }
                    break;
                case EC_ADD_PID:
                    {
                        SAddPidParams* addpid_params = (SAddPidParams*)item->param;
                        addpid_params->result = cmdAddPID(addpid_params->client, addpid_params->pid);
                    }
                    break;
                case EC_ADD_PIDS:
                    {
                        SAddPidsParams* addpid_params = (SAddPidsParams*)item->param;
                        addpid_params->result = cmdAddPIDs(addpid_params->client, addpid_params->pids);
                    }
                    break;
                case EC_REMOVE_ALL_PIDS:
                    {
                        SRemoveAllPidsParams* rempid_params = (SRemoveAllPidsParams*)item->param;
                        rempid_params->result = cmdRemoveAllPids(rempid_params->client);
                    }
                    break;
                case EC_SEND_PMT:
                    {
                        SSendPMTParams* sendpmt_params = (SSendPMTParams*)item->param;
                        sendpmt_params->result = cmdSendPMTToCI(sendpmt_params->pmt, sendpmt_params->pmt_len, sendpmt_params->cmd);
                    }
                    break;
                case EC_SEND_PMT_PID:
                    {
                        SSendPMTPidParams* sendpmt_params = (SSendPMTPidParams*)item->param;
                        sendpmt_params->result = cmdSendPMTPidToCI(sendpmt_params->pmt_pid, sendpmt_params->sid, sendpmt_params->cmd);
                    }
                    break;
                case EC_UNLOAD:
                    {
                        SUnloadParams* unload_params = (SUnloadParams*)item->param;
                        unload_params->result = cmdStop();
                    }
                    break;
                case EC_EXTRA_STREAM_CB:
                    {
                        SExtraStreamCallbackParams* cb_params = (SExtraStreamCallbackParams*)item->param;
                        cmd_set_extra_stream_callback(cb_params->stream_cb, cb_params->user_param);
                        cb_params->result = true;
                    }
                    break;
                default:
                    break;
                }
                m_CommandQueue.FinishCommand(&item);
            }

            //Process stream
            if (node != NULL)
		    {
			    if (m_StreamCallback)
                {
				    m_StreamCallback(node->data(), node->size_to_read(), m_UserParam);
                }

                if (extra_stream_callback_)
                {
                    extra_stream_callback_(node->data(), node->size_to_read(), extra_user_param_);
                }

                m_CircleBuffer.put_node(node);
		    }
	    }

        UnloadAndDeinitDriver();
    }
    catch (std::exception&)
    {
        log_error(L"EXCEPTION in CTVSStreamSource::StreamingThread()");
    }
    catch (...)
    {
        log_error(L"*EXCEPTION in CTVSStreamSource::StreamingThread()");
    }

#ifdef WIN32
    if (bCOMInitialized)
    {
        CoUninitialize();
    }
#endif
}

int CTVSStreamSource::TuneTransponder(dvblink::TTransponderInfo* transponder,
    unsigned char diseqc_value, const wchar_t* diseqc_rawdata)
{
    STuneParams tune_params;
    tune_params.transponder = transponder;
    tune_params.diseqc_value = diseqc_value;
    tune_params.diseqc_rawdata = diseqc_rawdata;
    tune_params.result = 0;

    m_CommandQueue.ExecuteCommand(EC_TUNE, &tune_params);

    return tune_params.result;
}

bool CTVSStreamSource::GetSignalStats(dvblink::TSignalInfo* signalInfo)
{
    SGetSignalStatsParams sstats_param;
    sstats_param.signalInfo = signalInfo;
    sstats_param.result = false;

    m_CommandQueue.ExecuteCommand(EC_GET_SIGNAL_STATS, &sstats_param);

    return sstats_param.result;
}

void CTVSStreamSource::set_extra_stream_callback(LPCB_TVS_STREAMFUNC stream_cb, void* user_param)
{
    SExtraStreamCallbackParams cb_params;
    cb_params.stream_cb = stream_cb;
    cb_params.user_param = user_param;
    cb_params.result = false;

    m_CommandQueue.ExecuteCommand(EC_EXTRA_STREAM_CB, &cb_params);
}


int CTVSStreamSource::AddPID(const wchar_t* client, unsigned short pid)
{
    SAddPidParams addpid_params;
    addpid_params.client = client;
    addpid_params.pid = pid;
    addpid_params.result = false;

    m_CommandQueue.ExecuteCommand(EC_ADD_PID, &addpid_params);

    return addpid_params.result;
}

int CTVSStreamSource::AddPIDs(const wchar_t* client, std::vector<unsigned short>& pids)
{
    SAddPidsParams addpid_params;
    addpid_params.client = client;
    addpid_params.pids = &pids;
    addpid_params.result = false;

    m_CommandQueue.ExecuteCommand(EC_ADD_PIDS, &addpid_params);

    return addpid_params.result;
}

bool CTVSStreamSource::RemoveAllPids(const wchar_t* client)
{
    SRemoveAllPidsParams rempid_params;
    rempid_params.client = client;
    rempid_params.result = false;

    m_CommandQueue.ExecuteCommand(EC_REMOVE_ALL_PIDS, &rempid_params);

    return rempid_params.result;
}

int CTVSStreamSource::SendPMTToCI(unsigned char* pmt, int pmt_len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd)
{
    SSendPMTParams sendpmt_params;
    sendpmt_params.pmt = pmt;
    sendpmt_params.pmt_len = pmt_len;
    sendpmt_params.cmd = cmd;
    sendpmt_params.result = false;

    m_CommandQueue.ExecuteCommand(EC_SEND_PMT, &sendpmt_params);

    return sendpmt_params.result;
}

int CTVSStreamSource::SendPMTPidToCI(unsigned short pmt_pid, unsigned short sid, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd)
{
    SSendPMTPidParams sendpmt_params;
    sendpmt_params.pmt_pid = pmt_pid;
    sendpmt_params.sid = sid;
    sendpmt_params.cmd = cmd;
    sendpmt_params.result = false;

    m_CommandQueue.ExecuteCommand(EC_SEND_PMT_PID, &sendpmt_params);

    return sendpmt_params.result;
}

int CTVSStreamSource::Stop()
{
    int ret_val = 0;

    if (options_.stop_stream_when_idle_)
    {
        SUnloadParams unload_params;
        unload_params.result = false;

        m_CommandQueue.ExecuteCommand(EC_UNLOAD, &unload_params);

        ret_val = unload_params.result;
    }

    return ret_val;
}

void CTVSStreamSource::cmd_set_extra_stream_callback(LPCB_TVS_STREAMFUNC stream_cb, void* user_param)
{
    extra_stream_callback_ = stream_cb;
    extra_user_param_ = user_param;
}

void CTVSStreamSource::create_full_ts_file(long freq)
{
    //close previous file
    if (full_ts_f_ != NULL)
    {
        fclose(full_ts_f_);
        full_ts_f_ = NULL;
    }


    //options_.stream_file_path_ = L"C:\\work\\test\\dvblex";
    if (!options_.stream_file_path_.get().empty())
    {
        time_t now;
        time(&now);
	    std::stringstream buf;
        buf << freq << "_" << now << ".ts";
        dvblink::filesystem_path_t ts_path = options_.stream_file_path_ / buf.str();
        full_ts_f_ = filesystem::universal_open_file(ts_path, "w+b");
    }
}
