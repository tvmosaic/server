/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_strings.h>
#include <dl_logger.h>
#include "constants.h"
#include <device_manager/device_manager.h>
#include "device.h"
#include "channel_scanner.h"
#include "epg_scanner.h"
#include "device_reactor.h"
#include "provider_manager.h"
#include "streamer_container.h"
#include "directory_settings.h"

using namespace dvblex;
using namespace dvblex::settings;
using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

device_manager_t::device_manager_t(installation_settings_obj_t& install_settings) :
    idle_timer_period_msec_(5000)
{
    directory_settings_ = boost::shared_ptr<directory_settings_t>(new directory_settings_t(install_settings));
    device_reactor_ = boost::shared_ptr<device_reactor_t>(new device_reactor_t(directory_settings_));
    provider_manager_ = boost::shared_ptr<provider_manager_t>(new provider_manager_t());

	idle_timer_ = new timer_procedure<device_manager_t>(&device_manager_t::idle_timer_func, *this, idle_timer_period_msec_);
}

device_manager_t::~device_manager_t()
{
    term();
    delete idle_timer_;
}

bool device_manager_t::init(const manual_device_list_t& manual_devices)
{
    log_info(L"device_manager_t::init.");

    log_info(L"device_manager_t::init. Initialize provider manager");

    provider_manager_->init(directory_settings_->get_scanners_path());

    log_info(L"device_manager_t::init. Initialize all device auxes");

    device_reactor_->get_device_aux_list(aux_module_list_);
    //walk through all auxes and start them
    for (size_t i=0; i<aux_module_list_.size(); i++)
        aux_module_list_[i]->init();

    //set manual devices
    device_reactor_->add_manual_devices(manual_devices);

    return true;
}

void device_manager_t::term()
{
    log_info(L"device_manager_t::term");

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    log_info(L"device_manager_t::term. Stop all devices");
    device_map_t::iterator it = device_map_.begin();
    while (it != device_map_.end())
    {
        device_id_t device_id = it->first;
        device_state_e state = get_device_state_int(device_id);
        //switch device to idle
        switch_to_idle(state, device_id);
        //destroy device
        if (it->second.device_ != NULL)
        {
            delete it->second.device_;
            it->second.device_ = NULL;
        }

        ++it;
    }

    device_map_.clear();

    log_info(L"device_manager_t::term. Shut down all device auxes");
    //shut down all auxes
    for (size_t i=0; i<aux_module_list_.size(); i++)
    {
        aux_module_list_[i]->term();
        delete aux_module_list_[i];
    }

    aux_module_list_.clear();
}

void device_manager_t::get_providers(standard_set_t standard, provider_info_map_t& providers)
{
    provider_manager_->get_providers(standard, providers);
}

bool device_manager_t::get_provider_details(const provider_id_t& id, const concise_param_map_t& scanner_settings, provider_info_t& provider_info, provider_scan_list_t& scan_list)
{
    return provider_manager_->get_provider_details(id, scanner_settings, provider_info, scan_list);
}

bool device_manager_t::get_rescan_settings(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, parameters_container_t& settings)
{
    return provider_manager_->get_rescan_settings(id, scanner_settings, settings);
}

bool device_manager_t::get_device_list(const dvblink::device_uuid_t& u, device_descriptor_list_t& device_list)
{
    bool ret_val = device_reactor_->get_device_list(u, device_list);

    if (ret_val)
    {
        boost::unique_lock<boost::shared_mutex> lock(lock_);

        //update device states
        for (size_t i=0; i<device_list.size(); i++)
        {
            device_list[i].state_ = get_device_state_int(device_list[i].id_);
            log_info(L"device_manager_t::get_device_list. Device %1%, id %2%, tuners %3%, state %4%, standards %5%") % string_cast<EC_UTF8>(device_list[i].name_.get()) % string_cast<EC_UTF8>(device_list[i].id_.get()) % device_list[i].tuners_num_ % device_list[i].state_ % device_list[i].supported_standards_.get();
        }
    }

    return ret_val;
}

device_state_e device_manager_t::get_device_state_int(const dvblink::device_id_t& device_id)
{
    device_state_e state = ds_none;

    if (device_map_.find(device_id) != device_map_.end())
    {
        state = ds_idle;

        if (device_map_[device_id].channel_scanner_ != NULL)
        {
            state = ds_channel_scan_in_progress;

            //check if scan has been finished already
            channel_scanner_progress_t progress_info;
            device_map_[device_id].channel_scanner_->get_progress_info(progress_info);
            if (progress_info.state_ == css_networks_ready)
                state = ds_networks_scanned;
            else if (progress_info.state_ == css_ready)
                state = ds_channel_scan_finished;
            
        } else
        if (device_map_[device_id].epg_scanner_ != NULL)
        {
            state = ds_epg_scan_in_progress;

            //check if epg scan has been finished already
            if (device_map_[device_id].epg_scanner_->is_idle())
                state = ds_epg_scan_finished;            
        } else
        if (device_map_[device_id].streamer_container_map_.size() > 0)
        {
            streamer_container_map_t::iterator it = device_map_[device_id].streamer_container_map_.begin();
            while (it != device_map_[device_id].streamer_container_map_.end())
            {
                if (!it->second->is_idle())
                {
                    state = ds_streaming;
                    break;
                }
                ++it;
            }
        }
    }

    return state;
}

void device_manager_t::remove_all_streams_int(const dvblink::device_id_t& device_id)
{
    if (device_map_.find(device_id) != device_map_.end())
    {
        //stop all streams
        streamer_container_map_t::iterator it = device_map_[device_id].streamer_container_map_.begin();
        while (it != device_map_[device_id].streamer_container_map_.end())
        {
            it->second->remove_all_streamers();
            device_map_[device_id].device_->release_program_streamer(it->second->get_program_streamer());
            delete it->second;
            ++it;
        }
        device_map_[device_id].streamer_container_map_.clear();
    }
}

bool device_manager_t::switch_to_idle(device_state_e state, const dvblink::device_id_t& device_id, bool create_device)
{
    bool ret_val = true;

    //stop all streams (if any) and remove all containers
    remove_all_streams_int(device_id);

    switch (state)
    {
    case ds_idle:
        break;
    case ds_none:
        {
            if (create_device)
            {
                //create device and add it to the map
                device_info_t di;
                di.id_ = device_id;
                di.device_= device_reactor_->create_device(device_id);
                if (di.device_ != NULL)
                {
                    device_map_[di.id_] = di;
                } else
                {
                    log_error(L"device_manager_t::switch_to_idle. Failed to create device with id %1%") % string_cast<EC_UTF8>(device_id.get());
                    ret_val = false;
                }
            }
        }
        break;
    case ds_streaming:
        //streams have been removed already
        break;
    case ds_channel_scan_in_progress:
        {
            //cancel channel scan
            device_map_[device_id].channel_scanner_->cancel();
        }
        //and then..
    case ds_networks_scanned:
    case ds_channel_scan_finished:
        {
            //destroy scanner
            device_map_[device_id].device_->release_channel_scanner(device_map_[device_id].channel_scanner_);
            device_map_[device_id].channel_scanner_ = NULL;
        }
        break;

    case ds_epg_scan_in_progress:
        {
            //cancel epg scan
            device_map_[device_id].epg_scanner_->cancel();
        }
        //and then..
    case ds_epg_scan_finished:
        {
            //destroy epgscanner
            device_map_[device_id].device_->release_epg_scanner(device_map_[device_id].epg_scanner_);
            device_map_[device_id].epg_scanner_ = NULL;
        }
        break;

    default:
        ret_val = false;
        break;
    }
    return ret_val;
}

bool device_manager_t::scan_start(const dvblink::device_id_t& device_id, const provider_info_t& provider_info, const provider_scan_list_t& scan_data, const concise_param_map_t& scanner_settings)
{
    bool ret_val = false;
    bool device_ready = false;

    log_info(L"device_manager_t::scan_start. Device %1%") % string_cast<EC_UTF8>(device_id.get());

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    device_state_e state = get_device_state_int(device_id);

    switch (state)
    {
    case ds_streaming:
    case ds_channel_scan_in_progress:
        //we are not allowed to scan in this state
        log_error(L"device_manager_t::scan_start. Device %1% is in state %2%. Scan is not possible.") % string_cast<EC_UTF8>(device_id.get()) % state;
        break;
    default:
        device_ready = switch_to_idle(state, device_id, true);
        break;
    }

    if (device_ready)
    {
        device_map_[device_id].channel_scanner_ = device_map_[device_id].device_->get_channel_scanner();
        if (device_map_[device_id].channel_scanner_ != NULL)
        {
            device_map_[device_id].channel_scanner_->scan(provider_info, scan_data, scanner_settings);
            ret_val = true;
        } else
        {
            log_error(L"device_manager_t::scan_start. Failed to create scanner for device %1%") % string_cast<EC_UTF8>(device_id.get());
        }
    }
    return ret_val;
}

bool device_manager_t::scan_network(const dvblink::device_id_t& device_id, const dvblink::scan_network_id_t& network_id)
{
    bool ret_val = false;

    log_info(L"device_manager_t::scan_network. Device %1%, network %2%") % string_cast<EC_UTF8>(device_id.get()) % string_cast<EC_UTF8>(network_id.get());

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    device_state_e state = get_device_state_int(device_id);

    if (state == ds_networks_scanned)
    {
        if (device_map_[device_id].channel_scanner_ != NULL)
        {
            if (device_map_[device_id].channel_scanner_->scan(network_id))
            {
                ret_val = true;
            } else
            {
                log_error(L"device_manager_t::scan_network. Start scan has returned an error on device %1%") % string_cast<EC_UTF8>(device_id.get());
            }
        } else
        {
            log_error(L"device_manager_t::scan_network. Channels canner is NULL on device %1%???") % string_cast<EC_UTF8>(device_id.get());
        }
    } else
    {
        log_error(L"device_manager_t::scan_network. Operation is not possible in this state (%1%)") % state;
    }

    return ret_val;
}

bool device_manager_t::scan_cancel(const dvblink::device_id_t& device_id)
{
    bool ret_val = false;

    log_info(L"device_manager_t::scan_cancel. Device %1%") % string_cast<EC_UTF8>(device_id.get());

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    device_state_e state = get_device_state_int(device_id);

    if (state == ds_channel_scan_in_progress || state == ds_channel_scan_finished || state == ds_networks_scanned)
    {
        ret_val = switch_to_idle(state, device_id);
    } else
    {
        log_error(L"device_manager_t::scan_cancel. Operation is not possible in this state (%1%)") % state;
    }

    return ret_val;
}

bool device_manager_t::scan_get_progress(const dvblink::device_id_t& device_id, channel_scanner_progress_t& progress_info)
{
    bool ret_val = false;

    boost::shared_lock<boost::shared_mutex> lock(lock_);

    device_state_e state = get_device_state_int(device_id);

    if (state == ds_channel_scan_in_progress || state == ds_channel_scan_finished || state == ds_networks_scanned)
    {
        device_map_[device_id].channel_scanner_->get_progress_info(progress_info);

        log_info(L"device_manager_t::scan_get_progress. Device %1%, progress %2%, channels_found %3%") % string_cast<EC_UTF8>(device_id.get()) % progress_info.progress_ % progress_info.channels_found_;

        ret_val = true;
    } else
    {
        log_error(L"device_manager_t::scan_get_progress. Operation is not possible in this state (%1%)") % state;
    }

    return ret_val;
}

bool device_manager_t::scan_get_channels(const dvblink::device_id_t& device_id, headend_info_t& headend, concise_param_map_t& scanner_settings, transponder_list_t& channels)
{
    bool ret_val = false;

    log_info(L"device_manager_t::scan_get_channels. Device %1%") % string_cast<EC_UTF8>(device_id.get());

    boost::shared_lock<boost::shared_mutex> lock(lock_);

    device_state_e state = get_device_state_int(device_id);

    if (state == ds_channel_scan_in_progress || state == ds_channel_scan_finished || state == ds_networks_scanned)
    {
        provider_info_t provider_info;
        device_map_[device_id].channel_scanner_->get_scanned_channels(provider_info, channels, scanner_settings);
        ret_val = provider_manager_->get_headend_from_provider(provider_info, scanner_settings, headend);
    } else
    {
        log_error(L"device_manager_t::scan_get_channels. Operation is not possible in this state (%1%)") % state;
    }

    return ret_val;
}

bool device_manager_t::scan_get_networks(const dvblink::device_id_t& device_id, scanned_network_list_t& networks)
{
    bool ret_val = false;

    log_info(L"device_manager_t::scan_get_networks. Device %1%") % string_cast<EC_UTF8>(device_id.get());

    boost::shared_lock<boost::shared_mutex> lock(lock_);

    device_state_e state = get_device_state_int(device_id);

    if (state == ds_networks_scanned)
    {
        device_map_[device_id].channel_scanner_->get_scanned_networks(networks);
        ret_val = true;
    } else
    {
        log_error(L"device_manager_t::scan_get_networks. Operation is not possible in this state (%1%)") % state;
    }

    return ret_val;
}

bool device_manager_t::scan_get_log(const dvblink::device_id_t& device_id, scan_log_t& scan_log)
{
    bool ret_val = false;

    log_info(L"device_manager_t::scan_get_log. Device %1%") % string_cast<EC_UTF8>(device_id.get());

    boost::shared_lock<boost::shared_mutex> lock(lock_);

    device_state_e state = get_device_state_int(device_id);

    if (state == ds_channel_scan_in_progress || state == ds_channel_scan_finished || state == ds_networks_scanned)
    {
        device_map_[device_id].channel_scanner_->get_scan_log(scan_log);
        ret_val = true;
    } else
    {
        log_error(L"device_manager_t::scan_get_log. Operation is not possible in this state (%1%)") % state;
    }

    return ret_val;
}

bool device_manager_t::get_channel_url_for_format(const dvblink::device_id_t& device_id, const concise_channel_tune_info_t& tune_params, device_channel_stream_type_e format, 
                                                  dvblink::url_address_t& url, dvblink::mime_type_t& mime)
{
    bool ret_val = false;

    //this information can be asked in any device state
    boost::unique_lock<boost::shared_mutex> lock(lock_);

    device_state_e state = get_device_state_int(device_id);

    //create device if it does not exist yet
    if (state == ds_none)
        switch_to_idle(state, device_id, true);

    if (device_map_.find(device_id) != device_map_.end())
        ret_val = device_map_[device_id].device_->get_channel_url_for_format(tune_params, format, url, mime);

    return ret_val;
}

bool device_manager_t::add_streamer(const dvblink::device_id_t& device_id, const dvblink::channel_id_t& channel_id, const concise_channel_tune_info_t& tune_params, streamer_object_t& streamer)
{
    bool ret_val = false;
    bool device_ready = false;

    log_info(L"device_manager_t::add_streamer. Channel %1%, device %2%, streamer id %3% ") % string_cast<EC_UTF8>(channel_id.get()) % string_cast<EC_UTF8>(device_id.get()) % string_cast<EC_UTF8>(streamer->get_id().get());

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    device_state_e state = get_device_state_int(device_id);

    switch (state)
    {
    case ds_channel_scan_in_progress:
    case ds_networks_scanned:
        //we are not allowed to add streamer in this state
        log_error(L"device_manager_t::add_streamer. Device %1% is in state %2%. Cannot add streamer in this state.") % string_cast<EC_UTF8>(device_id.get()) % state;
        break;
    case ds_streaming:
        //do not reset device if there is ongoing streaming already
        device_ready = true;
        break;
    default:
        device_ready = switch_to_idle(state, device_id, true);

        //re-read device state
        state = get_device_state_int(device_id);
        break;
    }

    if (device_ready)
    {
        bool b_already_streaming = device_map_[device_id].streamer_container_map_.find(channel_id) != device_map_[device_id].streamer_container_map_.end();
        if ((state == ds_streaming && (device_map_[device_id].device_->can_join_streaming(tune_params) || b_already_streaming)) || state == ds_idle)
        {
            //if device is streaming we need to check if a) one of the currently streamed channels is requested or b) if new channel can join others
            //otherwise - just create a new streamer container / program streamer
            if (b_already_streaming)
            {
                //add new streamer to existing stream container
                log_info(L"device_manager_t::add_streamer. Adding new streamer for device %1% to existing container for channel %2%.") % string_cast<EC_UTF8>(device_id.get()) % string_cast<EC_UTF8>(channel_id.get());

                ret_val = device_map_[device_id].streamer_container_map_[channel_id]->add_streamer(streamer);
            } else
            {
                log_info(L"device_manager_t::add_streamer. Adding new streamer container for channel %1% on device device %2%.") % string_cast<EC_UTF8>(channel_id.get()) % string_cast<EC_UTF8>(device_id.get());
                //add new streamer container for this channel
                program_streamer_t* ps = device_map_[device_id].device_->get_program_streamer(channel_id, tune_params);
                if (ps != NULL)
                {
                    streamer_container_t* streamer_container = new streamer_container_t(ps);
                    device_map_[device_id].streamer_container_map_[channel_id] = streamer_container;
                    ret_val = device_map_[device_id].streamer_container_map_[channel_id]->add_streamer(streamer);
                } else
                {
                    log_error(L"device_manager_t::add_streamer. Device %1% returned NULL on program streamer request for channel %2%.") % string_cast<EC_UTF8>(device_id.get()) % string_cast<EC_UTF8>(channel_id.get());
                }
            }
        } else
        {
            log_info(L"device_manager_t::add_streamer. Unable to add new streamer for channel %1% on device %2% in state %3%.") % string_cast<EC_UTF8>(channel_id.get()) % string_cast<EC_UTF8>(device_id.get()) % state;
        }
    }
    return ret_val;
}

bool device_manager_t::remove_streamer(const dvblink::streamer_id_t& streamer_id)
{
    bool ret_val = false;
    log_info(L"device_manager_t::remove_streamer. Streamer %1%") % string_cast<EC_UTF8>(streamer_id.get());

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    device_map_t::iterator device_it = device_map_.begin();
    while (device_it != device_map_.end() && !ret_val)
    {
        device_state_e state = get_device_state_int(device_it->first);

        if (state == ds_idle || state == ds_streaming)
        {
            streamer_container_map_t::iterator it = device_it->second.streamer_container_map_.begin();
            while (it != device_it->second.streamer_container_map_.end())
            {
                if (it->second->remove_streamer(streamer_id))
                {
                    log_info(L"device_manager_t::remove_streamer. Streamer %1% was removed successfully from device %2%") % string_cast<EC_UTF8>(streamer_id.get()) % string_cast<EC_UTF8>(device_it->first.get());

                    if (it->second->is_idle())
                    {
                        log_info(L"device_manager_t::remove_streamer. Device %1% has no streams for channel %2%. Removing stream container.") % string_cast<EC_UTF8>(device_it->first.get()) % string_cast<EC_UTF8>(it->first.get());

                        //no clients anymore. Delete the stream container as well
                        device_it->second.device_->release_program_streamer(it->second->get_program_streamer());
                        delete it->second;
                        device_it->second.streamer_container_map_.erase(it->first);
                    }
                    ret_val = true;
                    break;
                }

                ++it;
            }
        }
        ++device_it;
    }

    return ret_val;
}

void device_manager_t::reset_device(const dvblink::device_id_t& device_id)
{
    log_info(L"device_manager_t::reset_device. Device %1%") % string_cast<EC_UTF8>(device_id.get());

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    device_state_e state = get_device_state_int(device_id);
    switch_to_idle(state, device_id);
}

device_state_e device_manager_t::get_device_state(const dvblink::device_id_t& device_id)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    return get_device_state_int(device_id);
}

bool device_manager_t::get_device_info(const dvblink::device_id_t& device_id, device_descriptor_t& device_info)
{
    bool ret_val = false;

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    if (device_reactor_->get_device_info(device_id, device_info))
    {
        device_info.state_ = get_device_state_int(device_id);
        ret_val = true;
    }

    return ret_val;
}

bool device_manager_t::is_channel_present_on_device(const dvblink::device_id_t& device_id, const dvblink::device_id_t& channel_id)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);

    bool ret_val = false;

    if (get_device_state_int(device_id) == ds_streaming)
    {
        if (device_map_.find(device_id) != device_map_.end())
        {
            streamer_container_map_t::iterator it = device_map_[device_id].streamer_container_map_.begin();
            while (it != device_map_[device_id].streamer_container_map_.end())
            {
                if (boost::iequals(it->first.get(), channel_id.get()))
                {
                    ret_val = true;
                    break;
                }
                ++it;
            }
        }
    }

    return ret_val;
}

bool device_manager_t::get_device_status(const dvblink::device_id_t& device_id, device_status_t& status)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);

    status.id_ = device_id;
    status.state_ = get_device_state_int(device_id);
    if (status.state_ == ds_streaming)
    {
        if (device_map_.find(device_id) != device_map_.end())
        {
            program_streamer_t* program_streamer = NULL;
            streamer_container_map_t::iterator it = device_map_[device_id].streamer_container_map_.begin();
            while (it != device_map_[device_id].streamer_container_map_.end())
            {
                if (!it->second->is_idle())
                {
                    if (program_streamer == NULL)
                        program_streamer = it->second->get_program_streamer();

                    stream_channel_stats_t stream_channel_stats;
                    stream_channel_stats.channel_id_ = it->first;
                    stream_channel_stats.consumer_num_ = it->second->get_streamer_num();
                    it->second->calc_stat(stream_channel_stats.stream_stats_);

                    status.stream_channels_.push_back(stream_channel_stats);

                    if (it->second->get_max_priority() > status.max_priority_)
                        status.max_priority_ = it->second->get_max_priority();
                }
                ++it;
            }
            if (program_streamer != NULL)
                program_streamer->get_signal_info(status.signal_info_);

        }
    } else
    if (status.state_ == ds_channel_scan_in_progress || status.state_ == ds_channel_scan_finished || status.state_ == ds_networks_scanned)
    {
        //progress info
        if (device_map_.find(device_id) != device_map_.end() && device_map_[device_id].channel_scanner_ != NULL)
        {
            device_map_[device_id].channel_scanner_->get_progress_info(status.scan_progress_);
        }
    }

    return true;
}

bool device_manager_t::start_epg_scan(const dvblink::device_id_t& device_id, epg_box_obj_t& epg_box)
{
    bool ret_val = false;
    bool device_ready = false;

    log_info(L"device_manager_t::start_epg_scan. device %1%") % device_id.to_wstring();

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    device_state_e state = get_device_state_int(device_id);

    switch (state)
    {
    case ds_streaming:
    case ds_channel_scan_in_progress:
    case ds_networks_scanned:
    case ds_channel_scan_finished:
    case ds_epg_scan_in_progress:
        //we are not allowed to start epg scan in this state
        //log_ext_info(L"device_manager_t::start_epg_scan. Device %1% is in state %2%. Cannot start epg scan in this state.") % string_cast<EC_UTF8>(device_id.get()) % state;
        break;
    default:
        device_ready = switch_to_idle(state, device_id, true);

        //re-read device state
        state = get_device_state_int(device_id);
        break;
    }

    //it is safe to use device_map_[device_id] at this point as it definitely exists
    if (device_map_[device_id].device_->get_background_epg_scanner() != NULL)
    {
        log_info(L"device_manager_t::start_epg_scan. Background epg scan was started on device %1%") % device_id.to_wstring();
        //device supports background epg scan
        ret_val = device_map_[device_id].device_->get_background_epg_scanner()->scan(epg_box);
    } else 
    {
        if (device_ready)
        {
            device_map_[device_id].epg_scanner_ = device_map_[device_id].device_->get_epg_scanner();
            if (device_map_[device_id].epg_scanner_ != NULL)
            {
                if (device_map_[device_id].epg_scanner_->scan(epg_box))
                {
                    ret_val = true;
                } else
                {
                    log_error(L"device_manager_t::start_epg_scan. Scan() returned an error on device %1%") % string_cast<EC_UTF8>(device_id.get());
                    switch_to_idle(state, device_id, false);
                }
            } else
            {
                log_error(L"device_manager_t::start_epg_scan. Failed to create epg scanner for device %1%") % string_cast<EC_UTF8>(device_id.get());
            }
        }
    }
    return ret_val;
}

void device_manager_t::idle_timer_func(const boost::system::error_code& e)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);

    std::vector<device_id_t> devices_to_delete;

    device_map_t::iterator it = device_map_.begin();
    while (it != device_map_.end())
    {
        device_id_t device_id = it->first;
        device_state_e state = get_device_state_int(device_id);

        //check if device can be unloaded
        bool bunload = false;
        if (state == ds_none || state == ds_idle || state == ds_epg_scan_finished)
        {
            bunload = true;
            if (it->second.device_ != NULL)
            {
                bool b_bkg_epg_active = it->second.device_->get_background_epg_scanner() != NULL && !it->second.device_->get_background_epg_scanner()->is_idle();
                bunload = it->second.device_->is_stream_idle() && !b_bkg_epg_active;
            }
        }
        if (bunload)
        {
            log_info(L"device_manager_t::idle_timer_func. Device %1% is idle. Unloading it.") % string_cast<EC_UTF8>(device_id.get());
            //switch device to idle
            switch_to_idle(state, device_id);
            //destroy device
            if (it->second.device_ != NULL)
                delete it->second.device_;

            devices_to_delete.push_back(device_id);
        } else
        {
            std::vector<channel_id_t> stream_contianers_to_delete;
            //check if there are any unused stream containers and delete them
            streamer_container_map_t::iterator itc = device_map_[device_id].streamer_container_map_.begin();
            while (itc != device_map_[device_id].streamer_container_map_.end())
            {
                if (itc->second->is_idle())
                {
                    log_info(L"device_manager_t::idle_timer_func. Stream container for channel %1% on device %2% is idle. Deleting it.") % string_cast<EC_UTF8>(itc->first.get()) % string_cast<EC_UTF8>(device_id.get());
                    device_map_[device_id].device_->release_program_streamer(itc->second->get_program_streamer());
                    delete itc->second;
                    stream_contianers_to_delete.push_back(itc->first);
                }
                ++itc;
            }
            for (size_t i=0; i<stream_contianers_to_delete.size(); i++)
                device_map_[device_id].streamer_container_map_.erase(stream_contianers_to_delete[i]);
        }

        ++it;
    }

    for (size_t i=0; i<devices_to_delete.size(); i++)
        device_map_.erase(devices_to_delete[i]);
}


void device_manager_t::find_conflicting_headends(const dvblink::device_id_t& device_id, const headend_description_for_device_list_t& existing_headends, 
        const headend_description_for_device_t& new_headend, headend_id_list_t& conflicting_headends)
{
    headend_info_t pi;

    //get detailed info on each provider
    headend_info_map_t headend_info_map;
    for (size_t i=0; i<existing_headends.size(); i++)
    {
        if (get_headend_info(existing_headends[i], pi))
            headend_info_map[existing_headends[i].headend_id_] = pi;
    }

    if (get_headend_info(new_headend, pi))
        headend_info_map[new_headend.headend_id_] = pi;

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    device_state_e state = get_device_state_int(device_id);
    //create device if it does not exist yet
    if (state == ds_none)
        switch_to_idle(state, device_id);

    if (device_map_.find(device_id) != device_map_.end())
    {
        device_map_[device_id].device_->find_conflicting_headends(existing_headends, headend_info_map, new_headend, conflicting_headends);
    }
}

bool device_manager_t::get_headend_from_provider(const provider_info_t& provider_info, const concise_param_map_t& scanner_settings, headend_info_t& headend_info)
{
    return provider_manager_->get_headend_from_provider(provider_info, scanner_settings, headend_info);
}

bool device_manager_t::get_headend_info(const headend_description_for_device_t& headend, headend_info_t& headend_info)
{
    return provider_manager_->get_headend_info(headend, headend_info);
}

source_type_e device_manager_t::get_source_type_from_tuning_params(const dvblink::device_uuid_t& u, const dvblink::channel_tuning_params_t& tune_params)
{
    return device_reactor_->get_source_type_from_tuning_params(u, tune_params);
}

bool device_manager_t::can_join_streaming(const dvblink::device_id_t& device_id, const concise_channel_tune_info_t& channel_tune_params)
{
    bool ret_val = false;

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    device_state_e state = get_device_state_int(device_id);

    ret_val = (state == ds_streaming && device_map_[device_id].device_->can_join_streaming(channel_tune_params));

    return ret_val;
}

bool device_manager_t::is_idle()
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);

    bool ret_val = true;

    device_map_t::iterator device_it = device_map_.begin();
    while (device_it != device_map_.end())
    {
        device_state_e state = get_device_state_int(device_it->first);

        if (state != ds_idle && state != ds_none && state != ds_epg_scan_finished)
        {
            ret_val = false;
            break;
        }

        ++device_it;
    }

    return ret_val;
}

bool device_manager_t::get_device_configurable_props(const dvblink::device_id_t& device_id, configurable_device_props_t& props)
{
    return device_reactor_->get_device_configurable_props(device_id, props);
}

set_device_props_result_e device_manager_t::set_device_configurable_props(const dvblink::device_id_t& device_id, const concise_param_map_t& props)
{
    return device_reactor_->set_device_configurable_props(device_id, props);
}

dvblink::filesystem_path_t device_manager_t::create_shared_dir_for_device(const device_descriptor_t& dd)
{
    return directory_settings_->create_shared_dir_for_device(dd);
}

dvblink::filesystem_path_t device_manager_t::get_scanners_directory()
{
    return directory_settings_->get_scanners_path();
}

void device_manager_t::get_device_template_list(parameters_container_t& templates)
{
    device_reactor_->get_device_template_list(templates);
}

bool device_manager_t::create_manual_device(const concise_param_map_t& device_params)
{
    return device_reactor_->create_manual_device(device_params);
}

bool device_manager_t::delete_manual_device(const dvblink::device_id_t& device_id)
{
    //switch device to idle
    boost::unique_lock<boost::shared_mutex> lock(lock_);

    device_state_e state = get_device_state_int(device_id);
    switch_to_idle(state, device_id, false);
    //re-read device state
    state = get_device_state_int(device_id);
    //unload device
    device_map_t::iterator it = device_map_.find(device_id);
    if (it != device_map_.end())
    {
        //destroy device
        if (it->second.device_ != NULL)
        {
            delete it->second.device_;
            it->second.device_ = NULL;
        }

        device_map_.erase(it);
    }

    return device_reactor_->delete_manual_device(device_id);
}

void device_manager_t::get_manual_devices(manual_device_list_t& devices)
{
    return device_reactor_->get_manual_devices(devices);
}

device_playback_src_obj_t device_manager_t::get_playback_src_for_device(const dvblink::device_id_t& device_id)
{
    return device_reactor_->get_playback_src_for_device(device_id);
}
