/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <list>
#include <boost/thread/shared_mutex.hpp>
#include <boost/date_time.hpp>
#include <dl_device_info.h>

namespace dvblex {

class stream_stat_analyzer
{
    class stat_block
    {
    public:
        stat_block();

        const boost::posix_time::ptime& start_time() const {return start_time_;}
        const boost::posix_time::ptime last_time() const {return start_time_ + boost::posix_time::milliseconds(duration_);}
        boost::uint64_t duration() const {return duration_;}

        bool inited() const {return start_time_ != boost::posix_time::not_a_date_time;}
        void init(const boost::posix_time::ptime& start_time);

        void process_buffer(const unsigned char* buf, size_t packet_num, boost::uint64_t delta);

        boost::uint64_t packet_count() const {return packet_count_;}
        boost::uint64_t error_packet_count() const {return error_packet_count_;}
        boost::uint64_t discontinuity_count() const {return discontinuity_count_;}

    private:
        boost::uint64_t packet_count_;
        boost::uint64_t error_packet_count_;
        boost::uint64_t discontinuity_count_;

        typedef dvblink::base_type_t<unsigned short, 0> pid_t;
        typedef std::map<pid_t, boost::uint8_t> pid_to_cont_counter_map_t;
        pid_to_cont_counter_map_t packet_to_cont_counter_map_;

        boost::posix_time::ptime start_time_;
        boost::uint64_t duration_;
    };


public:
    stream_stat_analyzer();

    void reset(size_t window_sec);
    void process_buffer(const unsigned char* buf, unsigned long len);

    void calc_stat(stream_stats_t& stat) const;

private:
    mutable boost::shared_mutex lock_;
    size_t stat_num_;

    static boost::uint64_t block_dur_c;

    typedef std::list<stat_block> stat_t;
    stat_t stat_;
};

} // dvblink
