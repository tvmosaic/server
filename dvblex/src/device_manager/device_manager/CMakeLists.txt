cmake_minimum_required(VERSION 3.0.0)

project(device_manager)

include_directories(${TVMOSAIC_ANKER_DIR}/../include)
include_directories(${TVMOSAIC_ANKER_DIR}/../include/dvblex_streaming_lib)
include_directories(${TVMOSAIC_ANKER_DIR}/../include/dvblex_net_lib)
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

file(GLOB SOURCES "*.cpp" "file/*.cpp" "iptv/*.cpp" "dvb/*.cpp" "dvb/ffdecsa/*.cpp")

add_library(${PROJECT_NAME} STATIC ${SOURCES})

