/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>

#include <boost/thread.hpp>
#include <dl_command_queue.h>

#include "program_streamer.h"
#include "stream_stat_analyzer.h"
#include <dl_streamer.h>

namespace dvblex { 

class streamer_container_t : public dvblink::streamer_callbacks_t
{
protected:
    typedef std::map<dvblink::streamer_id_t, dvblink::streamer_object_t> streamer_map_t;

    enum commands_e {ec_add_streamer = 1, ec_remove_streamer = 2, ec_is_idle = 3, ec_streamer_disconnected = 4 + dvblink::engine::command_queue::post_command_offset, ec_remove_all = 5};

    struct add_streamer_params
    {
        dvblink::streamer_object_t streamer;
        bool result;
    };

    struct remove_streamer_params
    {
        dvblink::streamer_id_t id;
        bool result;
    };

    struct is_idle_params
    {
        bool result;
    };

    struct streamer_disconnected_params
    {
        dvblink::streamer_id_t id;
    };

    struct remove_all_params
    {
        bool result;
    };

    struct compare_streamer_disconnected_params_t
    {
        unsigned long cmd_id;
        dvblink::streamer_id_t streamer_id;
    };

public:
    streamer_container_t(program_streamer_t* program_streamer);
    ~streamer_container_t();

    bool add_streamer(dvblink::streamer_object_t& streamer);
    bool remove_streamer(const dvblink::streamer_id_t& streamer_id);
    void remove_all_streamers();

    bool is_idle();

    program_streamer_t* get_program_streamer(){return program_streamer_;}
    boost::uint32_t get_max_priority();
    boost::uint32_t get_streamer_num();

    void calc_stat(stream_stats_t& stat) const {return stat_analyzer_.calc_stat(stat);}

protected:
    program_streamer_t* program_streamer_;
    streamer_map_t streamer_map_;
    dvblink::engine::command_queue queue_;
    boost::thread* control_thread_;
    bool exit_flag_;
    boost::shared_mutex lock_;
    boost::uint32_t max_priority_;
    stream_stat_analyzer stat_analyzer_;

    virtual void streamer_disconnected(const dvblink::streamer_id_t& id);
    static void stream_callback(const unsigned char* buf, unsigned long len, void* user_param);
    void control_thread();
    bool delete_streamer(const dvblink::streamer_id_t& id);
    boost::uint32_t calculate_max_priority();
    static bool compare_streamer_disconnected_params(const dvblink::engine::SDLCommandItem* item, void* user_param);
};

}
