/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/thread/locks.hpp>
#include <dl_logger.h>
#include <dl_ts_info.h>
#include "stream_stat_analyzer.h"

using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

namespace dvblex {

const boost::uint64_t milli1000 = 1000;
boost::uint64_t stream_stat_analyzer::block_dur_c = milli1000; //msec

stream_stat_analyzer::stat_block::stat_block() :
    packet_count_(0),
    error_packet_count_(0),
    discontinuity_count_(0),
    start_time_(boost::posix_time::not_a_date_time),
    duration_(0)
{
}

void stream_stat_analyzer::stat_block::init(const boost::posix_time::ptime& start_time)
{
    start_time_ = start_time;
    packet_count_ = error_packet_count_ = discontinuity_count_ = 0;
}

void stream_stat_analyzer::stat_block::process_buffer(const unsigned char* buf, size_t cnt, boost::uint64_t delta)
{
    duration_ += delta;
    packet_count_ += cnt;

    const unsigned char* buf_end = buf + cnt * TS_PACKET_SIZE;
    for (const unsigned char* cur_ptr = buf; cur_ptr < buf_end; cur_ptr += TS_PACKET_SIZE)
    {
        pid_t pid = ts_process_routines::GetPacketPID(cur_ptr);
        boost::uint8_t cont_counter = ts_process_routines::GetContinuityCounter(cur_ptr);

        pid_to_cont_counter_map_t::iterator it = packet_to_cont_counter_map_.find(pid);
        if (it == packet_to_cont_counter_map_.end())
        {
            packet_to_cont_counter_map_[pid] = cont_counter;
        }
        else
        {
            //check continuity counter
            boost::uint8_t& cc = it->second;
            if (cc != cont_counter && ts_process_routines::GetNextContinuityCounter(cc) != cont_counter)
            {
                ++discontinuity_count_;
            }
            cc = cont_counter;
        }
        //in any case check for error indicator
        if (ts_process_routines::GetTSErrorIndicator(cur_ptr) != 0)
        {
            ++error_packet_count_;
        }
    }
}

//////////////////////////////////////////////////////////////////////////


stream_stat_analyzer::stream_stat_analyzer() :
    stat_num_(1)
{
    reset(block_dur_c);
}

void stream_stat_analyzer::reset(const size_t window_sec)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);

    stat_num_ = static_cast<size_t>(window_sec * milli1000 / block_dur_c);
    while (!stat_.empty())
    {
        stat_.pop_front();
    }
    stat_.push_back(stat_block());
}

void stream_stat_analyzer::calc_stat(stream_stats_t& stat) const
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);
    
    stat.bitrate_ = stat.packet_count_ = stat.error_packet_count_ = stat.discontinuity_count_ = 0;
    stat_t::const_iterator it = stat_.begin();
    stat_t::const_iterator it_end = stat_.end();

    boost::uint64_t dur_cnt = 0;
    while (it != it_end)
    {
        const stat_block& block = *it;
        stat.packet_count_ += block.packet_count();
        dur_cnt += block.duration();
        stat.error_packet_count_ += block.error_packet_count();
        stat.discontinuity_count_ += block.discontinuity_count();
        ++it;
    }

    if (dur_cnt)
    {
        stat.bitrate_ = stat.packet_count_ * TS_PACKET_SIZE * 8 / dur_cnt; //kilobits per seconds
    }
}

//size_t g_total_cnt = 0;

void stream_stat_analyzer::process_buffer(const unsigned char* buf, unsigned long len)
{
    if (!len)
        return;

    boost::unique_lock<boost::shared_mutex> lock(lock_);

    boost::posix_time::ptime cur_time = boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time();
    stat_block* block = &stat_.back();
    if (!block->inited())
    {
//        g_total_cnt = 0;
        block->init(cur_time);
        return;
    }

    size_t total_cnt = len / TS_PACKET_SIZE;
//    g_total_cnt += total_cnt;

    boost::posix_time::time_duration time_span(cur_time - block->last_time());
    if (time_span.total_milliseconds() <= 0)
    {
        block->process_buffer(buf, total_cnt, 0);
    }
    else
    {
        boost::uint64_t total_delta = time_span.total_milliseconds();
        
        size_t cnt = 0, offset_cnt = 0;
        boost::uint64_t block_remaining_dur = 0;
        while (total_delta)
        {
            boost::uint64_t delta = block_dur_c - block->duration();

            block_remaining_dur = total_delta <= delta ? total_delta : delta;
            cnt = block_remaining_dur != total_delta ? size_t(total_cnt * block_remaining_dur / total_delta) : total_cnt;

            if (offset_cnt*TS_PACKET_SIZE < len && (offset_cnt + cnt)*TS_PACKET_SIZE <= len)
            {
                block->process_buffer(buf + offset_cnt * TS_PACKET_SIZE, cnt, block_remaining_dur);
                
                boost::uint64_t block_duration = block->duration();
                if (block_duration >= block_dur_c)
                {
                    boost::posix_time::ptime last_time = block->last_time();
                    if (stat_.size() >= stat_num_)
                    {
                        //stat_t::const_iterator it_stat = stat_.begin();
                        //stat_t::const_iterator it_stat_end = stat_.end();
                        //while (it_stat != it_stat_end)
                        //{
                        //    std::cout << it_stat->packet_count() << " ";
                        //    ++it_stat;
                        //}
                        //std::cout << " -> " << g_total_cnt << std::endl;

                        stat_.pop_front();
                    }
                    stat_.push_back(stat_block());
                    block = &stat_.back();
                    block->init(last_time);
                }
            } else
            {
                log_warning(L"stream_stat_analyzer::process_buffer: buffer overflow");
                break;
            }

            //check if we are still within buffers!
            if (total_cnt < cnt || total_delta < block_remaining_dur)
            {
                log_warning(L"stream_stat_analyzer::process_buffer: counter overflow");
                break;
            }
            
            offset_cnt += cnt;
            total_cnt -= cnt;
            total_delta -= block_remaining_dur;
        }
    }
}

} // dvblink
