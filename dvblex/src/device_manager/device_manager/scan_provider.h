/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>
#include <sstream>

#include <dl_parameters.h>
#include <dl_channel_info.h>
#include <dl_provider_info.h>
#include <dl_headend.h>
#include <dl_hash.h>
#include <dl_locale_strings.h>
#include <dl_language_settings.h>

namespace dvblex { 

static const std::string provider_tag_key = "fef124ceab69";
static const std::string provider_tag_default_value = "";

class scan_provider_t
{
public:
    scan_provider_t(){};

    static std::string get_scan_provider_from_id(const std::string& id)
    {
        std::string ret_val;

        size_t n = id.find(get_id_sep_symbol());
        if (n != std::string::npos && n > 0)
            ret_val = id.substr(0, n);

        return ret_val;
    }

    dvblink::headend_id_t make_headend_id(const dvblink::provider_id_t& provider_id, const std::string& string_to_hash)
    {
        std::stringstream strbuf;
        strbuf << get_scan_provider_name() << get_id_sep_symbol() << dvblink::engine::calculate_64bit_string_hash(string_to_hash) << get_id_sep_symbol() << provider_id;

        return strbuf.str();
    }

    void headend_id_to_provider_id(const dvblink::headend_id_t& headend_id, dvblink::provider_id_t& provider_id)
    {
        const std::string& id_str = headend_id.get();
        //scan provider
        std::string sp = get_scan_provider_from_id(id_str);
        if (!sp.empty())
        {
            std::string str1 = id_str.substr(sp.size() + 1);
            //headend hash
            size_t n = str1.find(get_id_sep_symbol());
            if (n != std::string::npos && n > 0)
            {
                std::string hash = str1.substr(0, n);

                provider_id = str1.substr(n+1);
            }
        }
    }

    bool get_headend_info(const headend_description_for_device_t& headend, headend_info_t& headend_info)
    {
        dvblink::provider_id_t provider_id;
        headend_id_to_provider_id(headend.headend_id_, provider_id);

        provider_info_t provider_info;
        provider_scan_list_t scan_list;
        bool ret_val = get_provider_details(provider_id, headend.device_config_params_, provider_info, scan_list);

        if (ret_val)
            get_headend_from_provider(provider_info, headend.device_config_params_, headend_info);

        return ret_val;
    }

    void get_headend_from_provider(const provider_info_t& provider_info, const concise_param_map_t& scanner_settings, headend_info_t& headend_info)
    {
        fill_headend_info(provider_info, scanner_settings, headend_info); //id_, name_ and desc_
        headend_info.provider_id_ = provider_info.id_;
        headend_info.standard_ = provider_info.standard_;
        headend_info.device_config_params_ = scanner_settings;
    }

    virtual void get_providers(source_type_e standard, provider_info_map_t& providers){}
    virtual bool get_provider_details(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, provider_info_t& provider_info, provider_scan_list_t& scan_list){return false;};
    virtual std::string get_scan_provider_name(){return "";}
    virtual void fill_headend_info(const provider_info_t& provider_info, const concise_param_map_t& scanner_settings, headend_info_t& headend_info)
    {
        //name
        headend_info.name_ = provider_info.name_.get();

        //hash - headend_id
        std::string hash_str = provider_info.id_.get();
        headend_info.id_ = make_headend_id(provider_info.id_, hash_str);
    }

    virtual bool get_rescan_settings(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, parameters_container_t& settings)
    {
        //container parameters
        settings.id_ = "55641080-6293-4372-a28d-750792213e4c";
        settings.name_ = dvblink::engine::language_settings::GetInstance()->GetItemName(rescan_settings_container_name);

        //fill in "hide new channels" setting, which is valid for all providers
        selectable_param_description_t hide_new_ch_param;
        hide_new_ch_param.key_ = provider_hide_new_channels_on_rescan_key;
        hide_new_ch_param.name_ = dvblink::engine::language_settings::GetInstance()->GetItemName(hide_new_channels_on_rescan_name);

        selectable_param_element_t hide_new_ch_element;

        hide_new_ch_element.value_ = PARAM_VALUE_YES;
        hide_new_ch_element.name_ = dvblink::engine::language_settings::GetInstance()->GetItemName(parameter_value_yes);
        hide_new_ch_param.parameters_list_.push_back(hide_new_ch_element);

        hide_new_ch_element.value_ = PARAM_VALUE_NO;
        hide_new_ch_element.name_ = dvblink::engine::language_settings::GetInstance()->GetItemName(parameter_value_no);
        hide_new_ch_param.parameters_list_.push_back(hide_new_ch_element);

        settings.selectable_params_.push_back(hide_new_ch_param);

        return true;
    }

protected:
    static std::string get_id_sep_symbol() {return "#";}
};

}
