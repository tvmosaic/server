/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/filesystem.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_hash.h>
#include <dl_filesystem_path.h>
#include "file_channel_scanner.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

file_channel_scanner_t::file_channel_scanner_t() :
    channel_scanner_t()
{
}

file_channel_scanner_t::~file_channel_scanner_t()
{
}

bool file_channel_scanner_t::start()
{
    return true;
}

void file_channel_scanner_t::stop()
{
}

bool file_channel_scanner_t::do_scan(const std::string& tuning_params, const dvblink::scan_network_id_t& network_id, 
                                     transponder_list_t& found_channels, scan_log_entry_t& log_entry)
{
    bool ret_val = false;

    log_entry.reset();
    log_entry.scan_data_ = tuning_params;

    dvblink::filesystem_path_t fpath(tuning_params);
    if (boost::filesystem::exists(fpath.to_boost_filesystem()))
    {
        boost::uint64_t hash = calculate_64bit_string_hash(tuning_params);
        std::stringstream strbuf;
        strbuf << st_file << ":" << hash;

        transponder_t transponder_info;
        transponder_info.tr_id_ = strbuf.str();
        transponder_info.tr_name_ = strbuf.str();

        dvblex::device_channel_t channel;
        channel.id_ = strbuf.str();

#ifdef WIN32
    channel.name_ = string_cast<EC_UTF8>(fpath.to_boost_filesystem().stem().wstring());
#else
    channel.name_ = fpath.to_boost_filesystem().stem().string();
#endif

        channel.tune_params_ = tuning_params;

        transponder_info.channels_.push_back(channel);

        found_channels.push_back(transponder_info);

        log_entry.signal_info_.lock_ = true;
        log_entry.signal_info_.signal_quality_ = 100;
        log_entry.signal_info_.signal_strength_ = 100;
        log_entry.channels_found_ = 1;
        log_entry.add_comment(channel.name_.get());

        ret_val = true;
    } else
    {
        log_error(L"file_channel_scanner_t::do_scan. File not found %1%") % fpath.to_wstring();
    }

    return ret_val;
}
