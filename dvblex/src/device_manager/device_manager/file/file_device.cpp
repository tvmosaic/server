/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_strings.h>
#include <drivers/tuner_factory.h>
#include "../epg_scanner.h"
#include "file_device.h"
#include "file_channel_scanner.h"
#include "file_program_streamer.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

file_device_t::file_device_t(const dvblink::filesystem_path_t& shared_device_dir) :
    device_t(), shared_device_dir_(shared_device_dir)
{
}

file_device_t::~file_device_t()
{
}

channel_scanner_t* file_device_t::get_channel_scanner()
{
    return new file_channel_scanner_t();
}

void file_device_t::release_channel_scanner(channel_scanner_t* scanner)
{
    delete scanner;
}

program_streamer_t* file_device_t::get_program_streamer(const dvblink::channel_id_t& channel_id, const concise_channel_tune_info_t& tune_params)
{
    return new file_program_streamer_t(channel_id, tune_params);
}

void file_device_t::release_program_streamer(program_streamer_t* streamer)
{
    delete streamer;
}

epg_scanner_t* file_device_t::get_epg_scanner()
{
    //dummy epg scanner. EPG scan is not supported on file sources
    return new epg_scanner_t();
}

void file_device_t::release_epg_scanner(epg_scanner_t* epg_scanner)
{
    delete epg_scanner;
}
