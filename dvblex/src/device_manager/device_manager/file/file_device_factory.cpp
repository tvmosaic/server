/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_language_settings.h>
#include <dl_locale_strings.h>
#include "file_device_factory.h"
#include "file_device.h"

using namespace dvblex;
using namespace dvblink::engine;
using namespace dvblink::logging;
using namespace dvblink;

file_device_factory_t::file_device_factory_t(const dvblink::filesystem_path_t& device_config_path) :
    device_factory_t(device_config_path)
{
}

file_device_factory_t::~file_device_factory_t()
{
}

device_t* file_device_factory_t::create_device(const dvblink::device_id_t& device_id, directory_settings_obj_t& dir_settings)
{
    return new file_device_t(dir_settings->get_shared_dir_for_device(device_id));
}

void file_device_factory_t::get_device_desc_for_manual_device(const dvblink::device_id_t& id, 
                                       const concise_param_map_t& params, device_descriptor_t& dd)
{
    dd.id_ = id;
    dd.name_ = language_settings::GetInstance()->GetItemName(file_source_device_name);
    dd.supported_standards_ = st_file;
    dd.uuid_ = id.get();
    dd.can_be_deleted_ = true;
}

void file_device_factory_t::get_device_template_list(selectable_param_list_t& templates)
{
    selectable_param_element_t file_element;
    file_element.name_ = language_settings::GetInstance()->GetItemName(file_source_device_name);
    file_element.value_ = get_name();

    templates.push_back(file_element);
}

