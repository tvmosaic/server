/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <sstream>
#include <boost/filesystem.hpp>
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_language_settings.h>
#include <dl_locale_strings.h>
#include <dl_file_procedures.h>
#include "file_scan_provider.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::engine;
using namespace dvblink::logging;

file_scan_provider_t::file_scan_provider_t(const dvblink::filesystem_path_t& provider_storage_path) :
    provider_storage_path_(provider_storage_path)
{
}

static bool get_provider_info_from_file(const boost::filesystem::path& fname, std::string& id, 
    dvblink::provider_name_t& name, dvblink::provider_country_t& country, provider_scan_list_t* scan_list)
{
    if (!boost::filesystem::exists(fname))
        return false;

    //id is the file name
    //name is the file name
    //scan element is the full file pathname
    scan_element_t se;
#ifdef WIN32
    id = string_cast<EC_UTF8>(fname.filename().wstring());
    name = id;
    se = string_cast<EC_UTF8>(fname.wstring());
#else
    id = fname.filename().string();
    name = id;
    se = fname.string();
#endif

    if (scan_list != NULL)
        scan_list->push_back(se);

	return true;
}

void file_scan_provider_t::get_providers(source_type_e standard, provider_info_map_t& providers)
{
    //only process files
    if (standard != st_file)
        return;

    providers[standard] = provider_description_t();
    providers[standard].standard_ = standard;
    providers[standard].standard_name_ = language_settings::GetInstance()->GetItemName(file_source_standard_name);

    dvblink::filesystem_path_t p = provider_storage_path_ / get_source_type_string_from_value(standard);
    //enumerate all files inside this directory (if it exists)
    std::vector<boost::filesystem::path> files;
    filesystem::find_files(p.to_boost_filesystem(), files, L".ts");
    for (size_t idx=0; idx < files.size(); idx++)
    {
        provider_info_t pi;
        pi.standard_ = standard;
        pi.desc_ = language_settings::GetInstance()->GetItemName(file_source_standard_desc);

        std::string internal_id;

        if (get_provider_info_from_file(files[idx], internal_id, pi.name_, pi.country_, NULL))
        {
            pi.id_ = internal_id_to_id(internal_id);
            providers[standard].providers_.push_back(pi);
        }
    }
}

void file_scan_provider_t::id_to_internal_id(const std::string& id_str, std::string& internal_id)
{
    //scan provider
    std::string sp = get_scan_provider_from_id(id_str);
    if (!sp.empty())
        internal_id = id_str.substr(sp.size() + 1);
}

std::string file_scan_provider_t::internal_id_to_id(const std::string& internal_id)
{
    std::stringstream strbuf;
    strbuf << get_scan_provider_name() << get_id_sep_symbol() << internal_id;

    return strbuf.str();
}

bool file_scan_provider_t::get_provider_details(const dvblink::provider_id_t& id, const concise_param_map_t& scanner_settings, provider_info_t& provider_info, provider_scan_list_t& scan_list)
{
    bool ret_val = false;
    //convert id to internal id
    std::string internal_id;
    id_to_internal_id(id.get(), internal_id);

    //form path to the file
    dvblink::filesystem_path_t p = provider_storage_path_ / get_source_type_string_from_value(st_file) / internal_id;

    std::string idstr;
    if (get_provider_info_from_file(p.to_boost_filesystem(), idstr, provider_info.name_, provider_info.country_, &scan_list))
    {
        provider_info.id_ = internal_id_to_id(idstr);
        provider_info.standard_ = st_file;
        provider_info.desc_ = language_settings::GetInstance()->GetItemName(file_source_standard_desc);
        ret_val = true;
    }
    return true;
}
