/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <boost/thread.hpp>
#include <dl_filesystem_path.h>
#include <dl_smooth_streamer.h>
#include <dl_ts_aligner.h>

#include "../program_streamer.h"

namespace dvblex { 

class file_program_streamer_t : public program_streamer_t
{
public:
    file_program_streamer_t(const dvblink::channel_id_t& channel_id, const concise_channel_tune_info_t& tune_params);
    virtual ~file_program_streamer_t();

    virtual bool start();
    virtual void stop();

    virtual bool get_signal_info(signal_info_t& si);

protected:
    dvblink::filesystem_path_t file_;
    dvblink::smooth_streamer_t smooth_streamer_;
    boost::thread* streaming_thread_;
    volatile bool exit_flag_;
    dvblink::engine::ts_packet_aligner packet_aligner_;

    void streaming_thread();
    FILE* open_file(time_t& last_mod_time);
    void read_file(FILE* f, time_t last_mod_time);
    static void __stdcall aligner_callback(const unsigned char* buf, unsigned long len, void* user_param);
    static void process(const unsigned char* buf, size_t len, void* user_param);
};

}
