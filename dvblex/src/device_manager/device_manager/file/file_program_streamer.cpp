/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/filesystem.hpp>
#include <sys/types.h>
#include <sys/stat.h>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_file_procedures.h>
#include "file_program_streamer.h"

using namespace dvblex;
using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

file_program_streamer_t::file_program_streamer_t(const dvblink::channel_id_t& channel_id, const concise_channel_tune_info_t& tune_params) :
    program_streamer_t(channel_id, tune_params.tuning_params_.get()), 
    file_(tune_params.tuning_params_.get()),
    smooth_streamer_(&process, this, &exit_flag_),
    streaming_thread_(NULL),
    exit_flag_(false),
    packet_aligner_(aligner_callback, this)
{
}

file_program_streamer_t::~file_program_streamer_t()
{
}

bool file_program_streamer_t::start()
{
    program_streamer_t::start();

    log_info(L"file_program_streamer_t::start. Starting program streamer for channel %1%") % string_cast<EC_UTF8>(channel_id_.get());

    if (streaming_thread_ == NULL)
    {
        //Start streaming thread
        exit_flag_ = false;
        streaming_thread_ = new boost::thread(boost::bind(&file_program_streamer_t::streaming_thread, this));
    }

    return true;
}

void file_program_streamer_t::stop()
{
    log_info(L"file_program_streamer_t::stop. Stoping program streamer for channel %1%") % string_cast<EC_UTF8>(channel_id_.get());

    if (streaming_thread_)
    {
        // Stop streaming thread if started
        exit_flag_ = true;
        streaming_thread_->join();
        delete streaming_thread_;
        streaming_thread_ = NULL;
    }

    program_streamer_t::stop();
}

static time_t get_modification_time(const dvblink::filesystem_path_t& file_path)
{
    time_t ret_val = -1;

    #ifdef _WIN32
        struct _stat stat_buf;
        if (_wstat(file_path.to_wstring().c_str(), &stat_buf) == 0)
        {
            ret_val = stat_buf.st_mtime;
        }
    #else
        struct stat stat_buf;
        if (stat(file_path.to_string().c_str(), &stat_buf) == 0)
        {
            ret_val = stat_buf.st_mtime;
        }
    #endif

    return ret_val;
}

FILE* file_program_streamer_t::open_file(time_t& last_mod_time)
{
    FILE* f = NULL;

    while (!exit_flag_)
    {
        f = filesystem::universal_open_file(file_, "rb");
        if (f == NULL)
        {
            boost::this_thread::sleep(boost::posix_time::milliseconds(200));
        } else
        {
            last_mod_time = get_modification_time(file_);

            break;
        }
    }
    return f;
}

void file_program_streamer_t::read_file(FILE* f, time_t last_mod_time)
{
    if (f != NULL)
    {
        size_t buffer_size = 188*32;
        unsigned char* buffer = new unsigned char[buffer_size];

        smooth_streamer_.init();

        while (!exit_flag_)
        {
            size_t bytes_read = fread(buffer, 1, buffer_size, f);

            time_t new_last_mod_time = get_modification_time(file_);

            if (ferror(f) != 0 || new_last_mod_time != last_mod_time)
            {
                //error. exit reading loop
                log_info(L"file_program_streamer_t::read_file. File %1% has changed. Restarting...") % file_.to_wstring();
                break;
            }

            if (bytes_read > 0)
                packet_aligner_.write_stream(buffer, bytes_read);

            if (feof(f) != 0)
            {
                //end of file is reached. re-position to the beginning
                fseek(f, 0, SEEK_SET);
            }
        }

        smooth_streamer_.term();

        fclose(f);
        delete buffer;
    }
}

void file_program_streamer_t::streaming_thread()
{
    while (!exit_flag_)
    {
        time_t last_mod_time;
        FILE* f = open_file(last_mod_time);
        read_file(f, last_mod_time);
        if (!exit_flag_)
        {
            //file was modified. Wait a bit before re-opening it again
            int wait_counter = 30;
            while (wait_counter > 0 && !exit_flag_)
            {
                boost::this_thread::sleep(boost::posix_time::milliseconds(100));
                wait_counter -= 1;
            }
        }
    }
}

void __stdcall file_program_streamer_t::aligner_callback(const unsigned char* buf, unsigned long len, void* user_param)
{
    file_program_streamer_t* parent = (file_program_streamer_t*)user_param;

    parent->smooth_streamer_.process_stream(buf, len);
}

void file_program_streamer_t::process(const unsigned char* buf, size_t len, void* user_param)
{
    file_program_streamer_t* parent = (file_program_streamer_t*)user_param;

    parent->write_stream(buf, len);;
}

bool file_program_streamer_t::get_signal_info(signal_info_t& si)
{
    bool ret_val = false;

    si.reset();

    if (boost::filesystem::exists(file_.to_boost_filesystem()))
    {
        si.lock_ = true;
        si.signal_quality_ = 100;
        si.signal_strength_ = 100;

        ret_val = true;
    }

    return ret_val;
}

