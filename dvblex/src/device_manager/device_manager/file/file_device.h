/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include "../device.h"

namespace dvblex { 

class file_device_t : public device_t
{
public:
    file_device_t(const dvblink::filesystem_path_t& shared_device_dir);
    virtual ~file_device_t();

    virtual channel_scanner_t* get_channel_scanner();
    virtual void release_channel_scanner(channel_scanner_t* scanner);

    virtual program_streamer_t* get_program_streamer(const dvblink::channel_id_t& channel_id, const concise_channel_tune_info_t& tune_params);
    virtual void release_program_streamer(program_streamer_t* streamer);

    virtual epg_scanner_t* get_epg_scanner();
    virtual void release_epg_scanner(epg_scanner_t* epg_scanner);

protected:
    dvblink::filesystem_path_t shared_device_dir_;
};

}
