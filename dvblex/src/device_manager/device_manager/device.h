/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>

#include <boost/algorithm/string.hpp>

#include <dl_headend.h>
#include "program_streamer.h"
#include "channel_scanner.h"
#include "epg_scanner.h"

namespace dvblex { 

class device_t
{
public:
    device_t(){}
    virtual ~device_t(){}

    virtual program_streamer_t* get_program_streamer(const dvblink::channel_id_t& channel_id, const concise_channel_tune_info_t& tune_params){return NULL;}
    virtual void release_program_streamer(program_streamer_t* streamer){}

    virtual bool can_join_streaming(const concise_channel_tune_info_t& channel_tune_params){return false;}

    virtual void find_conflicting_headends(const headend_description_for_device_list_t& existing_headends, const headend_info_map_t& headend_info,
        const headend_description_for_device_t& new_headend, headend_id_list_t& conflicting_headends) 
    {
        //by default all existing providers are conflicting, except when it is the same provider
        for (size_t i=0; i<existing_headends.size(); i++)
        {
            if (!boost::iequals(existing_headends[i].headend_id_.to_string(), new_headend.headend_id_.to_string()))
                conflicting_headends.push_back(existing_headends[i].headend_id_);
        }
    }

    virtual channel_scanner_t* get_channel_scanner(){return NULL;}
    virtual void release_channel_scanner(channel_scanner_t* scanner){}

    virtual epg_scanner_t* get_background_epg_scanner(){return NULL;}

    virtual epg_scanner_t* get_epg_scanner(){return NULL;}
    virtual void release_epg_scanner(epg_scanner_t* epg_scanner){}

    virtual bool is_stream_idle(){return true;}

    virtual bool get_channel_url_for_format(const concise_channel_tune_info_t& tune_params, device_channel_stream_type_e format, 
        dvblink::url_address_t& url, dvblink::mime_type_t& mime){return false;}

};

}
