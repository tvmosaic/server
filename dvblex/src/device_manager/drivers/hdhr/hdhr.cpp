/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <sstream>
#include <boost/thread.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_atsc_qam_helper.h>
#include <dl_ts.h>
#include <drivers/deviceapi.h>
#include <drivers/hdhr.h>
#include <hdhomerun_os.h>
#include <hdhomerun_types.h>
#include <hdhomerun_pkt.h>
#include <hdhomerun_debug.h>
#include <hdhomerun_discover.h>
#include <hdhomerun_device.h>

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

class receiver_wrapper
{
public:
	receiver_wrapper(hdhomerun_device_t* device)
	{
		receiving_thread_ = NULL;
		stop_flag_ = false;
		stream_callback_ = NULL;
		hdhr_device_ = device;
	}

    bool start_receiving(tuner_t::TTSCallback* scb, void* param)
	{
		bool res = false;
		stream_callback_ = scb;
        param_ = param;

		//set-up hdhr device for receiving
		int ret  = hdhomerun_device_stream_start(hdhr_device_);
		if (ret == 1)
		{
			stop_flag_ = false;
			receiving_thread_ = new boost::thread(boost::bind(&receiver_wrapper::receiving_function, this));
			res = true;
		} else
		{
			log_error(L"HDHR receiver_wrapper. hdhomerun_device_stream_start returned an error %1%") % ret;
		}
		return res;
	}

	void stop_receiving()
	{
		if (receiving_thread_ != NULL)
		{
			stop_flag_ = true;
			receiving_thread_->join();
			delete receiving_thread_;
			receiving_thread_ = NULL;
			//stop hdhr receiving
			hdhomerun_device_stream_flush(hdhr_device_);
			hdhomerun_device_stream_stop(hdhr_device_);
			//reset pid filter
			pid_map_.clear();
			apply_pid_filter();
		}
	}

	void add_pid(unsigned short pid)
	{
		if (pid == NULL_PACKET_PID)
			return;

		if (pid_map_.find(pid) == pid_map_.end())
		{
			log_info(L"HDHR add_pid. Adding pid %1%") % pid;
			pid_map_[pid] = pid;
			apply_pid_filter();
		}
	}

	void delete_pid(unsigned short pid)
	{
		if (pid_map_.find(pid) != pid_map_.end())
		{
			log_info(L"HDHR delete_pid. Deleting pid %1%") % pid;
			pid_map_.erase(pid);
			apply_pid_filter();
		}
	}

protected:
	boost::thread* receiving_thread_;
	bool stop_flag_;
    tuner_t::TTSCallback* stream_callback_;
    void* param_;
	hdhomerun_device_t* hdhr_device_;
	std::map<unsigned short, unsigned short> pid_map_;

	void receiving_function()
	{
		const int VIDEO_FOR_1_SEC = 20000000 / 8;  // Same number is used on hdhomerun_config.
		while(!stop_flag_) 
		{
			size_t data_size = 0;
			uint8_t* data = hdhomerun_device_stream_recv(hdhr_device_, VIDEO_FOR_1_SEC, &data_size);
			if(data_size > 0 && stream_callback_ != NULL)
			{
				(*stream_callback_)((unsigned char*)data, data_size, param_);
			}
			boost::this_thread::sleep(boost::posix_time::milliseconds(10));
		}
	}

	void apply_pid_filter()
	{
		std::ostringstream str;

		std::map<unsigned short, unsigned short>::iterator it = pid_map_.begin();
		while (it != pid_map_.end())
		{
			str << "0x" << std::hex << std::uppercase << it->second << " ";
			++it;
		}
	   int ret = hdhomerun_device_set_tuner_filter(hdhr_device_, str.str().c_str());
	   if (ret != 1)
		   log_error(L"HDHR apply_pid_filter. Error adding pids to filter %1%") % ret;
	}

};

//hdhr_tuner_factory_t *******************

static bool device_sort_function(hdhomerun_discover_device_t first_item, hdhomerun_discover_device_t second_item)
{
    return first_item.device_id < second_item.device_id;
}

static void sort_devices_on_id(hdhomerun_discover_device_t* devices, int device_num)
{
    //make a vector and sort it
    std::vector<hdhomerun_discover_device_t> device_list;
    for (int i=0; i<device_num; i++)
        device_list.push_back(devices[i]);
	std::sort(device_list.begin(), device_list.end(), device_sort_function);
    for (size_t i=0; i<device_list.size(); i++)
        devices[i] = device_list[i];
}

static const std::string decode_char_to_hex(unsigned char ch)
{
	int ich = ch;
    std::ostringstream ostr;
	ostr << std::setw( 2 ) << std::setfill('0') << std::hex << std::uppercase << ich;
	return ostr.str();
}

int hdhr_tuner_factory_t::DeviceGetList(PDevAPIDevListEx pDL)
{
    bool res = false;
	pDL->Count = 0;

	const int max_devices = 16;
	hdhomerun_discover_device_t devices[max_devices];
	int devices_num = hdhomerun_discover_find_devices_custom_v2(0, HDHOMERUN_DEVICE_TYPE_TUNER, 
		HDHOMERUN_DEVICE_ID_WILDCARD, devices, max_devices);
	
	if (devices_num >=0 )
	{
        //sort devices array on device id
        sort_devices_on_id(devices, devices_num);
        //copy devices into our list
		for (int i=0; i<devices_num; i++)
		{
            bool badd = true;

			//device id
			std::wstringstream deviceid;
			deviceid << devices[i].device_id;
			//device type and name
			std::string device_name = "HDHomeRun";
			unsigned long device_type = TUNERTYPE_UNKNOWN;
            std::string hdhr_device_model_str;
            std::string hdhr_device_hw_model_str;
			hdhomerun_device_t* hdhr_device = hdhomerun_device_create(devices[i].device_id, 0, 1, NULL);
			if (hdhr_device != NULL)
			{
				const char* device_name_str = hdhomerun_device_get_name(hdhr_device);
				if (device_name_str != NULL)
				{
					device_name = device_name_str;
					//reverse find "-" - this is a tuner number after it
					std::string::size_type pos = device_name.rfind('-');
					if (pos != std::string::npos)
						device_name.resize(pos);
                    device_name = "HDHomeRun " + device_name;
				}
        
				const char* device_model_str = hdhomerun_device_get_model_str(hdhr_device);
				if (device_model_str != NULL)
				{
                    hdhr_device_model_str = device_model_str;
        		    log_info(L"hdhr_tuner_factory_t::DeviceGetList. Found model %1%") % string_cast<EC_UTF8>(hdhr_device_model_str);

                    
                    if (boost::iequals(device_model_str, "hdhomerun_dvbt"))
						device_type = TUNERTYPE_DVBC | TUNERTYPE_DVBT; //old eu model. Can be either dvb-t or dvb-c
					else if (strstr(device_model_str, "atsc") != NULL)
						device_type = TUNERTYPE_ATSC | TUNERTYPE_CLEARQAM;
					else if (strstr(device_model_str, "dvbtc") != NULL)
						device_type = TUNERTYPE_DVBC | TUNERTYPE_DVBT;
					else if (strstr(device_model_str, "dvbc") != NULL)
						device_type = TUNERTYPE_DVBC;
					else if (strstr(device_model_str, "dvbt") != NULL)
						device_type = TUNERTYPE_DVBT;
                    else
                        device_type = TUNERTYPE_DVBT;

                    //do not add cable card tuners to the list
                    //they do not work properly with TVSource, use IPTV instead
					if (strstr(device_model_str, "cablecard") != NULL)
						badd = false;
                
                }
                if (hdhomerun_device_get_hw_model_str(hdhr_device) != NULL)
                    hdhr_device_hw_model_str = hdhomerun_device_get_hw_model_str(hdhr_device);
				hdhomerun_device_destroy(hdhr_device);
			}
            if (badd)
            {
			    //device ip
			    char ipbuf[256];
			    sprintf(ipbuf, "%d.%d.%d.%d", 
				    (devices[i].ip_addr >> 24) & 0xFF, 
				    (devices[i].ip_addr >> 16) & 0xFF, 
				    (devices[i].ip_addr >> 8) & 0xFF, 
				    (devices[i].ip_addr >> 0) & 0xFF);

			    for (int fidx = 0; fidx < devices[i].tuner_count; fidx++)
			    {
				    pDL->Devices[pDL->Count].TunerIdx = i;
				    pDL->Devices[pDL->Count].FrontendIdx = fidx;

                    std::ostringstream ostr;
                    ostr << device_name << " (" << ipbuf << ") - " << fidx;
                    strcpy(pDL->Devices[pDL->Count].szName, ostr.str().c_str()); //ip address as a name
				    wcscpy(pDL->Devices[pDL->Count].szDevicePathW, deviceid.str().c_str());
				    pDL->Devices[pDL->Count].TunerType = device_type;
                    //model
                    strcpy(pDL->Devices[pDL->Count].model, hdhr_device_model_str.c_str());
                    //make
                    strcpy(pDL->Devices[pDL->Count].make, "Silicondust");
                    //uri
                    strcpy(pDL->Devices[pDL->Count].uri, devices[i].base_url);
                    //modelNum
                    strcpy(pDL->Devices[pDL->Count].modelNum, hdhr_device_hw_model_str.c_str());
                    //uuid
                    std::string hdhr_id_str;
                    for (int j = 3; j >= 0; j--)
                    {
                        unsigned char b = (devices[i].device_id >> j*8) & 0xFF;
                        hdhr_id_str += decode_char_to_hex(b);
				    }
                    strcpy(pDL->Devices[pDL->Count].uuid, hdhr_id_str.c_str());
                    //uuid make
                    strcpy(pDL->Devices[pDL->Count].uuid_make, "hdhomerun");
                    pDL->Devices[pDL->Count].connection_type = TCT_NETWORK;

				    pDL->Count += 1;
			    }
            }
		}
		res = true;
	} else
	{
		res = false; // network error?
	}

	return res ? dvblink::SUCCESS : dvblink::FAIL;
}

tuner_t* hdhr_tuner_factory_t::get_tuner(const std::string& device_path, int fdx)
{
    return new hdhr_tuner_t(device_path, fdx);
}

//hdhr_tuner_t ************************
hdhr_tuner_t::hdhr_tuner_t(const std::string& device_path, int fdx) :
    tuner_t(device_path, fdx), hdhr_device_(NULL), receiver_(NULL), tuner_type_(TUNERTYPE_UNKNOWN)
{
}

hdhr_tuner_t::~hdhr_tuner_t()
{
}

int hdhr_tuner_t::StartDevice(DL_E_TUNER_TYPES tuner_type)
{	
	int res = dvblink::FAIL;

    std::wstring wdevice_id = string_cast<EC_UTF8>(device_path_);

	log_info(L"hdhr_tuner_t::StartDevice. Starting device %1%, frontend %2%") % wdevice_id % fdx_;

	//get device id from the start parameters
	int device_id;
	string_conv::apply<int, int, wchar_t>(wdevice_id.c_str(), device_id, -1);
	if (device_id != -1)
	{
		//create device
		hdhr_device_ = hdhomerun_device_create(device_id, 0, fdx_, NULL);
		if (hdhr_device_ != NULL)
		{
            //try to lock a tuner
            char* error = NULL;
            if (hdhomerun_device_tuner_lockkey_request(hdhr_device_, &error) == 1)
            {
			    //create stream receiver wrapper
			    receiver_ = new receiver_wrapper(hdhr_device_);
			    tuner_type_ = tuner_type;

			    res = dvblink::SUCCESS;
            } else 
            {
		        hdhomerun_device_destroy(hdhr_device_);
		        hdhr_device_ = NULL;

			    log_error(L"hdhr_tuner_t::StartDevice. hdhomerun_device_tuner_lockkey_request failed");
            }
		} else
		{
			log_error(L"hdhr_tuner_t::StartDevice. hdhomerun_device_create failed");
		}
	} else
	{
		log_error(L"hdhr_tuner_t::StartDevice. Cannot convert %1% to the device id") % wdevice_id;
	}
	return res;
}

int hdhr_tuner_t::StopDevice()
{
	if (hdhr_device_ != NULL)
	{
		receiver_->stop_receiving();

		delete receiver_;
		receiver_ = NULL;

        hdhomerun_device_tuner_lockkey_release(hdhr_device_);

		hdhomerun_device_destroy(hdhr_device_);
		hdhr_device_ = NULL;
	}
	return dvblink::SUCCESS;
}


static std::string get_modulation_from_transponder_info(DL_E_MOD_DVBC mod)
{
	switch (mod)
	{
	case MOD_DVBC_QAM_4:
		return "qam4";
	case MOD_DVBC_QAM_16:
		return "qam16";
	case MOD_DVBC_QAM_32:
		return "qam32";
	case MOD_DVBC_QAM_64:
		return "qam64";
	case MOD_DVBC_QAM_128:
		return "qam128";
	case MOD_DVBC_QAM_256:
		return "qam256";
	}
	return "";
}

static std::string get_tuning_string(dvblink::PTransponderInfo Tp, DL_E_TUNER_TYPES tuner_type)
{
	std::string ret_val;
	switch (tuner_type)
	{
	case TUNERTYPE_DVBT:
		{
			std::ostringstream is;
			is << "auto" << Tp->dwLOF << "t:" << (Tp->dwFreq * 1000);
			ret_val = is.str();
		}
		break;
	case TUNERTYPE_DVBC:
		{
			std::ostringstream is;
			is << "a8" << get_modulation_from_transponder_info((DL_E_MOD_DVBC)Tp->dwLOF) << "-" << (Tp->dwSr / 1000) << ":" << (Tp->dwFreq * 1000);
			ret_val = is.str();
		}
		break;
	case TUNERTYPE_ATSC:
		{
			std::ostringstream is;
			is << "8vsb:" << atsc_channel_to_center_frequency(Tp->dwFreq);
			ret_val = is.str();
		}
		break;
	case TUNERTYPE_CLEARQAM:
		{
			std::ostringstream is;
			is << get_modulation_from_transponder_info((DL_E_MOD_DVBC)Tp->dwLOF) << ":" << (Tp->dwFreq * 1000);
			ret_val = is.str();
		}
		break;
	case TUNERTYPE_DVBS:
	default:
		break;
	}
	std::wstring wstr = string_cast<EC_UTF8>(ret_val);
	log_info(L"HDHR get_tuning_string: %1%") % wstr;
	return ret_val;
}

int hdhr_tuner_t::SetTuner(dvblink::PTransponderInfo Tp)
{	
	int res = dvblink::FAIL;

	std::string tuning_string = get_tuning_string(Tp, tuner_type_);

	//stop previous receiving
	receiver_->stop_receiving();
	//tune new channel
	int ret = hdhomerun_device_set_tuner_channel(hdhr_device_, tuning_string.c_str());
	if (ret == 1)
	{
		//wait for signal  lock (informative only)
		hdhomerun_tuner_status_t hdhomerun_status;
		ret = hdhomerun_device_wait_for_lock(hdhr_device_, &hdhomerun_status);
		log_info(L" hdhr_tuner_t::SetTuner. wait_for_lock status: %1%, %2%") % hdhomerun_status.lock_supported % hdhomerun_status.signal_to_noise_quality;
		//start receiving
		receiver_->start_receiving(callback_, callback_param_);
		res = dvblink::SUCCESS;
	} else
	{
		log_error(L" hdhr_tuner_t::SetTuner. hdhomerun_device_set_tuner_channel returned an error %1%") % ret;
	}

    return res;
}

int hdhr_tuner_t::GetTunerState(dvblink::PSignalInfo TunerState, dvblink::PTransponderInfo Tp)
{
	int res = dvblink::FAIL;
	hdhomerun_tuner_status_t hdhomerun_status;
	int ret = hdhomerun_device_get_tuner_status(hdhr_device_, NULL, &hdhomerun_status);
	if (ret  == 1)
	{
		TunerState->dwSize = sizeof(dvblink::TSignalInfo);
		TunerState->Level = hdhomerun_status.signal_strength;
		TunerState->Locked = hdhomerun_status.lock_supported;
		TunerState->Quality = hdhomerun_status.signal_to_noise_quality;
		res = dvblink::SUCCESS;
	} else
	{
		log_error(L"hdhr_tuner_t::GetTunerState. hdhomerun_device_get_tuner_status failed %1%") % ret;
	}

	return res;
}

int hdhr_tuner_t::SendDiseqc(dvblink::PDiseqcCmd RawDiseqc, int ToneBurst)
{
	return dvblink::FAIL;
}

int hdhr_tuner_t::AddFilter(int pid)
{	
	receiver_->add_pid(pid);
	return dvblink::SUCCESS;
}

int hdhr_tuner_t::DelFilter(int pid)
{
	receiver_->delete_pid(pid);
	return dvblink::SUCCESS;
}

