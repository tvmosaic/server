cmake_minimum_required(VERSION 3.0.0)

project(sat2ip)

include_directories(${TVMOSAIC_ANKER_DIR}/../include)
include_directories(${TVMOSAIC_ANKER_DIR}/../include/dvblex_net_lib)
include_directories(${TVMOSAIC_ANKER_DIR}/../include/dvblex_streaming_lib)

file(GLOB SOURCES "*.cpp" "jrtplib/*.cpp")

add_library(${PROJECT_NAME} STATIC ${SOURCES})

