//${RTP_WINSOCK_HEADERS}
//${RTP_INTTYPE_HEADERS}

#ifdef WIN32

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include <Winsock2.h>

#include "rtptypes_win.h"

#else

#include <inttypes.h>
#include <stddef.h>

#endif
