/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <dl_logger.h>
#include <dl_strings.h>
#include "sat2ip_params.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

static const std::string get_fec(dvblink::TTransponderInfo* tr_info)
{
    switch (tr_info->dwFec)
    {
    case FEC_1_2:
        return "12";
    case FEC_2_3:
        return "23";
    case FEC_3_4:
        return "34";
    case FEC_5_6:
        return "56";
    case FEC_7_8:
        return "78";
    case FEC_8_9:
        return "89";
    case FEC_3_5:
        return "35";
    case FEC_4_5:
        return "45";
    case FEC_9_10:
        return "910";
    case FEC_AUTO:
        log_warning(L"auto fec not supported by sat2ip. Defaulting to 3/4");
        return "34";
    }
    log_warning(L"fec not supported by sat2ip %1%. Defaulting to 3/4") % tr_info->dwFec;
    return "34";
}

static const std::string get_frequency(dvblink::TTransponderInfo* tr_info)
{
    std::ostringstream str;
    str << (tr_info->dwFreq / 1000);
    return str.str();
}

static const std::string get_dvbc_frequency(dvblink::TTransponderInfo* tr_info)
{
    std::ostringstream str;
    double d = double(tr_info->dwFreq) / 1000;
    str << std::setprecision(2) << std::setiosflags(std::ios_base::fixed) << d;
    return str.str();
}

static const std::string get_dvbt_frequency(dvblink::TTransponderInfo* tr_info)
{
    std::ostringstream str;
    double d = double(tr_info->dwFreq) / 1000;
    str << std::setprecision(2) << std::setiosflags(std::ios_base::fixed) << d;
    return str.str();
}

static const std::string get_sr(dvblink::TTransponderInfo* tr_info)
{
    std::ostringstream str;
    str << (tr_info->dwSr / 1000);
    return str.str();
}

static const std::string get_dvbc_sr(dvblink::TTransponderInfo* tr_info)
{
    std::ostringstream str;
    str << (tr_info->dwSr / 1000);
    return str.str();
}

static const std::string get_polarity(dvblink::TTransponderInfo* tr_info)
{
    switch (tr_info->Pol)
    {
    case POL_HORIZONTAL:
        return "h";
    case POL_VERTICAL:
        return "v";
    }
    return "";
}

static const std::string get_dvbt_tuning_string(dvblink::TTransponderInfo* tr_info)
{
    if (tr_info->plp_id != 0)
    {
        std::ostringstream str;
        str << "msys=dvbt2&plp=" << (tr_info->plp_id - 1);
        return str.str();
    }

    return "msys=dvbt";
}

static bool is_dvbs(dvblink::TTransponderInfo* tr_info)
{
    return tr_info->dwModulation == MOD_DVBS_QPSK;
}

static const std::string get_dvbs_system(dvblink::TTransponderInfo* tr_info)
{
    if (is_dvbs(tr_info))
        return "dvbs";

    return "dvbs2";
}

static const std::string get_modulation(dvblink::TTransponderInfo* tr_info)
{
    switch (tr_info->dwModulation)
    {
    case MOD_DVBS_QPSK:
    case MOD_DVBS_NBC_QPSK:
        return "qpsk";
    case MOD_DVBS_8PSK:
        return "8psk";
    }
    log_warning(L"modulation not supported by sat2ip %1%. Defaulting to qpsk") % tr_info->dwModulation;
    return "qpsk";
}

static const std::string get_dvbc_modulation(dvblink::TTransponderInfo* tr_info)
{
    switch (tr_info->dwLOF)
    {
    case MOD_DVBC_QAM_4:
        return "4qam";
    case MOD_DVBC_QAM_16:
        return "16qam";
    case MOD_DVBC_QAM_32:
        return "32qam";
    case MOD_DVBC_QAM_64:
        return "64qam";
    case MOD_DVBC_QAM_128:
        return "128qam";
    case MOD_DVBC_QAM_256:
        return "256qam";
    }
    log_warning(L"get_dvbc_modulation. Unsupported modulation %1%. Defaulting to 64qam") % tr_info->dwLOF;
    return "64qam";
}

static const std::string get_dvbt_bandwidth(dvblink::TTransponderInfo* tr_info)
{
    std::ostringstream str;
    str << tr_info->dwLOF;
    return str.str();
}

const std::string get_sat2ip_tuning_string(dvblink::TTransponderInfo* tr_info, DL_E_TUNER_TYPES tuner_type)
{
    std::ostringstream str;
    if (tuner_type == TUNERTYPE_DVBS)
    {
        str << "freq=" << get_frequency(tr_info);
        str << "&";
        str << "pol=" << get_polarity(tr_info);
        str << "&";
        str << "msys=" << get_dvbs_system(tr_info);
        str << "&";
        str << "mtype=" << get_modulation(tr_info);
        str << "&";
        str << "sr=" << get_sr(tr_info);
        str << "&";
        str << "fec=" << get_fec(tr_info);
        if (!is_dvbs(tr_info))
            str << "&ro=0.35&plts=off";
    } else
    if (tuner_type == TUNERTYPE_DVBC)
    {
        str << "freq=" << get_dvbc_frequency(tr_info);
        str << "&";
        str << "msys=dvbc";
        str << "&";
        str << "mtype=" << get_dvbc_modulation(tr_info);
        str << "&";
        str << "sr=" << get_dvbc_sr(tr_info);
    } else
    if (tuner_type == TUNERTYPE_DVBT)
    {
        str << "freq=" << get_dvbt_frequency(tr_info);
        str << "&";
        str << get_dvbt_tuning_string(tr_info);
        str << "&";
        str << "bw=" << get_dvbt_bandwidth(tr_info);
    } else
        log_error(L"get_sat2ip_tuning_string. Unsupported tuner type %1%") % tuner_type;

    std::wstring command = string_cast<EC_UTF8>(str.str());
    log_info(L"get_sat2ip_tuning_string. Resulting string %1%") % command;

    return str.str();
}

int get_sat2ip_src_from_diseqc(dvblink::DL_E_DISEQC_TYPE diseqc)
{
    switch (diseqc)
    {
    case DISEQC_NONE:
    case DISEQC_LEVEL_1_A_A:
        return 1;
        break;
    case DISEQC_LEVEL_1_B_A:
        return 2;
        break;
    case DISEQC_LEVEL_1_A_B:
        return 3;
        break;
    case DISEQC_LEVEL_1_B_B:
        return 4;
        break;
    default:
    	break;
    }
    //src is 1 by default
    return 1;
}
