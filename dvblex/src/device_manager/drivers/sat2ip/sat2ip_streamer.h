/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <drivers/deviceapi.h>
#include <dl_socket_api.h>
#include <http_utils.h>
#include <rtsp_client.h>
#include <dl_command_queue.h>
#include "jrtplib/rtpsession.h"

namespace dvblink
{

class sat2ip_streamer
{
public:
    enum ECommands {EC_DESCRIBE};

    struct sat2ip_describe_cmd
    {
        dvblink::TSignalInfo signalInfo;
        int frontend_num;
        bool result;
    };

	struct pmt_pid_desc
	{
		unsigned short pmt_pid;
		unsigned short sid;

		unsigned long get_key()
		{
			unsigned long res = pmt_pid;
			res = (res << 16) | sid;
			return res;
		}
	};

	typedef std::map<unsigned long, pmt_pid_desc> pmt_pid_desc_map_t;
	typedef std::vector<pmt_pid_desc> pmt_pid_desc_list_t;

public:
    typedef void (*stream_cb_func)(const unsigned char* buffer, size_t length, void* user_param);

    sat2ip_streamer(const std::string& server_ip, int frontend_idx, dvblink::DL_E_TUNER_TYPES tuner_type);
    ~sat2ip_streamer();

    void set_extci_device(const std::wstring& ext_ci) {ext_ci_ = ext_ci;};
    bool tune(dvblink::TTransponderInfo* tr_info, dvblink::DL_E_DISEQC_TYPE diseqc);
    bool open_rtsp_session();
    bool start_reception(stream_cb_func cb_func, void* user_param);
    bool add_pid(unsigned short pid);
    bool delete_pid(unsigned short pid);
    bool set_pmt_pid(pmt_pid_desc pmt, int listmng);
    int get_frontend_number();
    bool get_signal_stats(dvblink::TSignalInfo* signal_stats);
    void stop();

protected:
    std::string server_ip_;
    std::string stream_id_;
    dvblink::streaming::rtsp_client* rtsp_client_;
    int frontend_idx_;
    stream_cb_func cb_func_;
    void* user_param_;
    std::map<unsigned short, unsigned short> pids_map_;
    boost::thread* reception_thread_;
    boost::thread* control_thread_;
    bool stop_read_flag_;
    std::vector<unsigned short> pids_to_add_;
    std::vector<unsigned short> pids_to_delete_;
    boost::mutex pids_lock_;
    jrtplib::RTPSession* session_;
    dvblink::engine::command_queue command_queue_;
    unsigned short base_port_;
    dvblink::DL_E_TUNER_TYPES tuner_type_;
	pmt_pid_desc_list_t pmt_to_add_;
	pmt_pid_desc_list_t pmt_to_delete_;
	pmt_pid_desc_map_t pmt_pid_map_;
    std::wstring ext_ci_;

    void reception_thread_func();
    void control_thread_func();
    bool sat2ip_send_command(const std::string& command, std::string& status_line, dvblink::streaming::http_headers& headers, std::string& body);
    bool prepare_rtp_session();
    bool send_prepare_command(dvblink::TTransponderInfo* tr_info, dvblink::DL_E_DISEQC_TYPE diseqc, unsigned short actual_port);
    bool send_describe_command(dvblink::TSignalInfo* signal_stats, int* frontend_num);
    bool send_teardown_command();
    bool send_options_command();
//    bool send_add_pid_command(std::vector<unsigned short>& pids);
//    bool send_delete_pid_command(std::vector<unsigned short>& pids);
	bool send_set_pmt_pid_command();
	void stop_reception();
    bool send_play_command();
    unsigned short get_base_port();
    void send_stream(const unsigned char* buf, int len);
	void clear_reception_buffers();
    std::string get_current_pids_string();
};

} //namespace dvblink
