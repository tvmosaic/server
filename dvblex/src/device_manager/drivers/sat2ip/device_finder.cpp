/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <sstream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dvblex_net_lib/dl_network_helper.h>
#include <sat-ip.h>
#include "device_finder.h"
#include "sat2ip_streamer.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::streaming;

static int get_frontend_num_dvbs(std::string& device_ip)
{
    int frontend_num = 0;
    
	log_info(L"SAT2IP - get_frontend_num_dvbs");
    
    sat2ip_streamer streamer(device_ip, 0, TUNERTYPE_DVBS);

    //use some bogus dvb-s tuning data to initiate sat2ip session
    dvblink::TTransponderInfo tr_info;
    memset(&tr_info, 0, sizeof(tr_info));
    
    tr_info.dwSize = sizeof(dvblink::TTransponderInfo);
    tr_info.dwFreq = 10743000;
    tr_info.Pol = dvblink::POL_VERTICAL;
    tr_info.dwModulation = dvblink::MOD_DVBS_QPSK;
    tr_info.dwFec = dvblink::FEC_2_3;
    tr_info.dwSr = 22000000;
    
    //try tuning first
    bool success = streamer.tune(&tr_info, dvblink::DISEQC_NONE);
    //if not successful (frontend is probably busy streaming for someone else) - try joining that session
    if (!success)
        success = streamer.open_rtsp_session();
    if (success)
    {
        frontend_num = streamer.get_frontend_number();
        streamer.stop();
    }
    else
    {
    	log_info(L"SAT2IP - get_frontend_num: tune failed");
    }

    return frontend_num;
}

static int get_frontend_num_dvbt(std::string& device_ip)
{
    int frontend_num = 0;
    
	log_info(L"SAT2IP - get_frontend_num_dvbt");
	
    sat2ip_streamer streamer(device_ip, 0, TUNERTYPE_DVBT);

    //use some bogus dvb-s tuning data to initiate sat2ip session
    dvblink::TTransponderInfo tr_info;
    memset(&tr_info, 0, sizeof(tr_info));
    
    tr_info.dwSize = sizeof(dvblink::TTransponderInfo);
    tr_info.dwFreq = 546000;
    tr_info.dwLOF = 7; //bandwidth
    
    //try tuning first
    bool success = streamer.tune(&tr_info, dvblink::DISEQC_NONE);
    //if not successful (frontend is probably busy streaming for someone else) - try joining that session
    if (!success)
        success = streamer.open_rtsp_session();
    if (success)
    {
        frontend_num = streamer.get_frontend_number();
        streamer.stop();
    }
    else
    {
    	log_info(L"SAT2IP - get_frontend_num: tune failed");
    }

    return frontend_num;
}

static int get_frontend_num(std::string& device_ip)
{
    int frontend_num = 0;
    //try dvb-s first
    frontend_num = get_frontend_num_dvbs(device_ip);
    if (frontend_num <= 0)
    {
        //try dvb-t
        frontend_num = get_frontend_num_dvbt(device_ip);
    }
    
    return frontend_num;
}

unsigned long get_type_from_frontends(const sat2ip_fe_num_map_t& fe_map)
{
    unsigned long ret_val = TUNERTYPE_UNKNOWN;

    sat2ip_fe_num_map_t::const_iterator it;
    if ((it = fe_map.find(dvbs_fe_key)) != fe_map.end() && it->second > 0)
        ret_val |= TUNERTYPE_DVBS;

    if ((it = fe_map.find(dvbc_fe_key)) != fe_map.end() && it->second > 0)
        ret_val |= TUNERTYPE_DVBC;

    if ((it = fe_map.find(dvbt_fe_key)) != fe_map.end() && it->second > 0)
        ret_val |= TUNERTYPE_DVBT;

    return ret_val;
}

std::string get_name_from_sat2ip_desc(const sat2ip_server_desc& desc, const std::string& device_ip, int fdx)
{
    std::ostringstream ostr;
    ostr << desc.friendly_name << " (" << device_ip << ") - " << fdx;

    return ostr.str();
}

void tuner_info_from_sat2ip_desc(const sat2ip_server_desc& desc, const std::string& device_ip, int fidx, TDevAPIDevInfoEx& tuner_info)
{
    tuner_info.TunerType = get_type_from_frontends(desc.fe_num_map);
    strcpy(tuner_info.szName, get_name_from_sat2ip_desc(desc, device_ip, fidx).c_str());
    std::wstring device_id = string_cast<EC_UTF8>(desc.udn);
    wcscpy(tuner_info.szDevicePathW, device_id.c_str());
    tuner_info.FrontendIdx = fidx;
    tuner_info.TunerIdx = 0;

    strcpy(tuner_info.uri, desc.uri.c_str());
    strcpy(tuner_info.make, desc.manufacturer.c_str());
    strcpy(tuner_info.model, desc.model_name.c_str());
    strcpy(tuner_info.modelNum, desc.model_number.c_str());
    strcpy(tuner_info.uuid, desc.udn.c_str());
    tuner_info.connection_type = TCT_NETWORK;
}

bool get_device_list(dvblink::PDevAPIDevListEx pDL, int max_wait_time_ms)
{
    //log_info(L"SAT2IP - get_device_list()");
    bool res = false;

    timeout_t to = (max_wait_time_ms < 0) ? infinite_timeout :
        boost::posix_time::milliseconds(max_wait_time_ms);

    satip_servers_t server_list;
    errcode_t err = discover_satip_servers(server_list, to);

    if (err != err_none)
    {
        log_error(L"SAT2IP - discover_satip_servers() failed (%1%)") % int(err);
    }
    else if (server_list.empty())
    {
        log_info(L"SAT2IP - discover_satip_servers(): no servers found");
    }
    else
    {
        try
        {
            std::string addr;
            std::wstring waddr;
            size_t n = server_list.size();

            if (n > 1)
            {
                log_info(L"SAT2IP - %1% servers have been discovered:") % n;

                for (size_t k = 0; k < server_list.size(); k++)
                {
                    satip_server_info& t = server_list[k];
                    t.device_addr.get_address(addr);
                    waddr = string_cast<EC_UTF8>(addr);
                    log_info(L"  %1%") % waddr;
                }
            }
            else
            {
                server_list[0].device_addr.get_address(addr);
                waddr = string_cast<EC_UTF8>(addr);
                log_info(L"SAT2IP - server discovered: %1%") % waddr;
            }
        }
        catch (...)
        {
        }
    }
    
    if (server_list.size() > 0)
    {
        for (size_t i = 0; i < server_list.size(); i++)
        {
            std::string device_ip;
            server_list[i].device_addr.get_address(device_ip);
            
            std::wstring wurl = string_cast<EC_UTF8>(server_list[i].descr_url);
            log_info(L"SAT2IP - description XML: %1%") % wurl;
            
            sat2ip_server_desc desc;
            err = get_server_props_from_description_xml(server_list[i].descr_url, desc);
            if (err == err_none)
            {
                std::vector<std::string> fe_to_combine;

                if (boost::icontains(desc.model_name, "octopus"))
                {
                    fe_to_combine.push_back(dvbc_fe_key);
                    fe_to_combine.push_back(dvbt_fe_key);
                }

                //retrieve number of frontends
                int frontend_num = desc.get_fe_num(fe_to_combine);

                //fall back to the old algorithm for sat2ip receivers, which do not support satip:X_SATIPCAP tag
                if (frontend_num == 0)
                    frontend_num = get_frontend_num(device_ip);

                //adjust number of frontends for Fritzbox 6490 tuner
                if (boost::icontains(desc.model_name, "fritz") && (boost::icontains(desc.model_name, "6490") || boost::icontains(desc.model_name, "6590")))
                {
                    log_info(L"SAT2IP - get_device_list: adjusting tuner number for Fritzbox 6490/6590 to 4");
                    frontend_num = 4;
                }

                for (int fidx = 0; fidx < frontend_num; fidx++)
                {
                    tuner_info_from_sat2ip_desc(desc, device_ip, fidx, pDL->Devices[pDL->Count]);
                    pDL->Count++;
                }
            }
            else
            {
                std::wstring wurl = string_cast<EC_UTF8>(server_list[i].descr_url);
                log_error(L"SAT2IP - get_server_props_from_description_xml failed with error %1%") % (int)err;
            }
        }
        
        res = true;
    }

    return res;
}

struct find_sat2ip_server_data
{
    const char* udn;
    std::string* device_ip;
    std::string* description_url;
};

static bool new_server_callback(satip_servers_t* list, void* user_param)
{
    bool res = true;

    find_sat2ip_server_data* data = (find_sat2ip_server_data*)user_param;
    if (list->size() > 0)
    {
        if (boost::icontains(list->at(list->size() - 1).usn, data->udn))
        {
            //found!
            list->at(list->size() - 1).device_addr.get_address(*(data->device_ip));
            *data->description_url = list->at(list->size() - 1).descr_url;

            std::wstring wip = string_cast<EC_UTF8>(*(data->device_ip));
            std::wstring wudn = string_cast<EC_UTF8>(data->udn);
            log_info(L"SAT2IP - new_server_callback: found sat2ip server %1% at %2%") % wudn % wip;
            //break search cycle - we have found the device
            res = false;
        }
    }
    return res;
}

bool get_device_ip_by_device_path(const wchar_t* device_path, std::string& device_ip, std::string& desc_url, int max_wait_time_ms)
{
    log_info(L"SAT2IP - get_device_ip_by_device_path()");
    device_ip.clear();

    find_sat2ip_server_data find_data;
    find_data.device_ip = &device_ip;
    find_data.description_url = &desc_url;
    std::string udn = string_cast<EC_UTF8>(device_path);
    find_data.udn = udn.c_str();

    satip_servers_t server_list;

    errcode_t err = discover_satip_servers(server_list,
        boost::posix_time::milliseconds(max_wait_time_ms),
        &new_server_callback, &find_data);

    if (err != err_none)
    {
        log_error(L"SAT2IP - discover_satip_servers() failed (%1%)") % int(err);
        return false;
    }

    if (server_list.empty())
    {
        log_info(L"SAT2IP - discover_satip_servers(): no servers found");
        return false;
    }
    
    return device_ip.size() > 0;
}
