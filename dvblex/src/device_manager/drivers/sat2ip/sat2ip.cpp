/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <dl_logger.h>
#include <drivers/sat2ip.h>
#include <sat-ip.h>
#include "device_finder.h"
#include "sat2ip_params.h"
#include "sat2ip_streamer.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::streaming;

sat2ip_tuner_factory_t::sat2ip_tuner_factory_t(const dvblink::filesystem_path_t& device_config_path) :
    tuner_factory_t(device_config_path)
{
}

int sat2ip_tuner_factory_t::DeviceGetList(dvblink::PDevAPIDevListEx pDL)
{

    bool res = get_device_list(pDL, SAT2IP_DISCOVERY_TIMEOUT);

    return res ? dvblink::SUCCESS : dvblink::FAIL;
}

tuner_t* sat2ip_tuner_factory_t::get_tuner(const std::string& device_path, int fdx)
{
    return new sat2ip_tuner_t(device_path, fdx);
}

bool sat2ip_tuner_factory_t::get_tuner_info(const std::string& device_path, int fdx, TDevAPIDevInfoEx& tuner_info)
{
    bool ret_val = false;

    std::wstring spath = string_cast<EC_UTF8>(device_path);

    log_info(L"sat2ip_tuner_factory_t::get_tuner_info. device %1%, frontend index %2%") % spath % fdx;
    //get ip and description url of the server
    std::string device_ip;
    std::string desc_url;

    if (get_device_ip_by_device_path(spath.c_str(), device_ip, desc_url, SAT2IP_FIND_SERVER_TIMEOUT))
    {
        sat2ip_server_desc desc;
        if (get_server_props_from_description_xml(desc_url, desc) == err_none)
        {
            tuner_info_from_sat2ip_desc(desc, device_ip, fdx, tuner_info);
            ret_val = true;
        }
    }

    return ret_val;
}

int sat2ip_tuner_factory_t::ExtCIDeviceGetList(const std::string& device_path, PExtCIDeviceList pDL)
{
    pDL->Count = 0;

    //find a sat2ip server and check if it is Octopus.NET
    std::wstring spath = string_cast<EC_UTF8>(device_path);
    std::string device_ip;
    std::string desc_url;
    if (get_device_ip_by_device_path(spath.c_str(), device_ip, desc_url, SAT2IP_FIND_SERVER_TIMEOUT))
    {
        sat2ip_server_desc desc;
        if (get_server_props_from_description_xml(desc_url, desc) == err_none)
        {
		    if (boost::icontains(desc.model_name, L"octopus"))
		    {
			    //add two CI slots for octopus.net
			    pDL->Count = 2;
			    //#1
			    wcscpy(pDL->Devices[0].name, L"Octopus.Net CI Slot 1");
			    wcscpy(pDL->Devices[0].hwid, L"1");
			    //#2
			    wcscpy(pDL->Devices[1].name, L"Octopus.Net CI Slot 2");
			    wcscpy(pDL->Devices[1].hwid, L"2");
		    }
        }
    }

	return dvblink::SUCCESS;
}

/////////////////////////////////////////////////////

sat2ip_tuner_t::sat2ip_tuner_t(const std::string& device_path, int fdx) :
    tuner_t(device_path, fdx), streamer_(NULL), last_diseqc_(DISEQC_NONE), ext_ci_device_path_(external_ci_device_id_none)
{
}

sat2ip_tuner_t::~sat2ip_tuner_t()
{
    if (streamer_ != NULL)
        delete streamer_;
}

int sat2ip_tuner_t::StartDevice(DL_E_TUNER_TYPES tuner_type)
{	
    int res = dvblink::FAIL;
    
    std::wstring spath = string_cast<EC_UTF8>(device_path_);

    log_info(L"SAT2IP. Start device %1%, frontend index %2%") % spath % fdx_;
    //get ip of the selected server
    int max_discovery_attempts = 3;
    int count = 0;
    while (count < max_discovery_attempts)
    {
        std::string device_ip;
        std::string desc_url;

        if (get_device_ip_by_device_path(spath.c_str(), device_ip, desc_url, SAT2IP_FIND_SERVER_TIMEOUT))
        {
            streamer_ = new sat2ip_streamer(device_ip, fdx_, (DL_E_TUNER_TYPES)tuner_type);
			streamer_->set_extci_device(ext_ci_device_path_);

            res = dvblink::SUCCESS;
            break;
        }
        else
        {
            log_warning(L"SAT2IP. Unable to find a server %1% on the network. Attempt %2%")
                % spath % (count + 1);
        }
        ++count;
    }

    return res;
}

int sat2ip_tuner_t::StopDevice()
{
    if (streamer_ != NULL)
    {
        streamer_->stop();

        delete streamer_;
        streamer_ = NULL;
    }
	return dvblink::SUCCESS;
}

void sat2ip_tuner_t::stream_function(const unsigned char* buffer, size_t length, void* user_param)
{
    sat2ip_tuner_t* parent = (sat2ip_tuner_t*)user_param;
    parent->callback_((unsigned char*)buffer, length, parent->callback_param_);
}

int sat2ip_tuner_t::SetTuner(dvblink::PTransponderInfo Tp)
{
    int res = dvblink::FAIL;
    if (streamer_ != NULL)
    {
        //start new one
        if (streamer_->tune(Tp, last_diseqc_))
        {
            //start streaming
            if (streamer_->start_reception(&stream_function, (void*)this))
            {
                res = dvblink::SUCCESS;
            } 
            else
            {
                //stop otherwise
                streamer_->stop();
            }
        }
    }
    return res;
}

int sat2ip_tuner_t::GetTunerState(dvblink::PSignalInfo TunerState, dvblink::PTransponderInfo Tp)
{
    int res = dvblink::FAIL;
    if (streamer_ != NULL)
    {
        if (streamer_->get_signal_stats(TunerState))
            res = dvblink::SUCCESS;
    }

	return res;
}

static dvblink::DL_E_DISEQC_TYPE get_10_diseqc_from_raw_cmd(unsigned char *cmd, int len)
{
    dvblink::DL_E_DISEQC_TYPE ret_val = DISEQC_NONE;
    if (len == 4) 
    {
	    if(cmd[2]==0x38) 
        {
		    switch(cmd[3]) 
            {
			    case 0xf0:
			    case 0xf1:
			    case 0xf2:
			    case 0xf3: 
				    ret_val = DISEQC_LEVEL_1_A_A;
			    break;
			    case 0xf4:
			    case 0xf5:
			    case 0xf6:
			    case 0xf7:
				    ret_val = DISEQC_LEVEL_1_B_A;
			    break;
			    case 0xf8:
			    case 0xf9:
			    case 0xfa:
			    case 0xfb: 
				    ret_val = DISEQC_LEVEL_1_A_B;
			    break;
			    case 0xfc:
			    case 0xfd:
			    case 0xfe:
			    case 0xff: 
				    ret_val = DISEQC_LEVEL_1_B_B;
			    break;
		    }
	    }
    }
    return ret_val;
}

int sat2ip_tuner_t::SendDiseqc(dvblink::PDiseqcCmd RawDiseqc, int ToneBurst)
{
    if (RawDiseqc != NULL)
        last_diseqc_ = get_10_diseqc_from_raw_cmd(RawDiseqc->Data, RawDiseqc->iLength);

	return dvblink::SUCCESS;
}

int sat2ip_tuner_t::AddFilter(int pid)
{	
    int res = dvblink::FAIL;
    if (streamer_ != NULL)
    {
        if (streamer_->add_pid(pid))
            res = dvblink::SUCCESS;
    }

	return res;
}

int sat2ip_tuner_t::DelFilter(int pid)
{
    int res = dvblink::FAIL;
    if (streamer_ != NULL)
    {
        if (streamer_->delete_pid(pid))
            res = dvblink::SUCCESS;
    }

	return res;
}

int sat2ip_tuner_t::CISendPMTPid(unsigned short pid, unsigned short sid, int listmng)
{
    int res = dvblink::FAIL;
    if (streamer_ != NULL)
    {
		sat2ip_streamer::pmt_pid_desc desc;
		desc.pmt_pid = pid;
		desc.sid = sid;
        if (streamer_->set_pmt_pid(desc, listmng))
            res = dvblink::SUCCESS;
    }

	return res;
}

void sat2ip_tuner_t::SetExtCIDevice(const std::wstring& ext_ci_device_id)
{
    ext_ci_device_path_ = ext_ci_device_id;
}

