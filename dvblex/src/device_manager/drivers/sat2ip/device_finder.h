/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <drivers/deviceapi.h>
#include <sat-ip.h>

bool get_device_list(dvblink::PDevAPIDevListEx pDL, int max_wait_time_ms);
bool get_device_ip_by_device_path(const wchar_t* device_path, std::string& device_ip, std::string& desc_url, int max_wait_time_ms);
std::string get_name_from_sat2ip_desc(const dvblink::streaming::sat2ip_server_desc& desc, const std::string& device_ip, int fdx);
unsigned long get_type_from_frontends(const dvblink::streaming::sat2ip_fe_num_map_t& fe_map);
void tuner_info_from_sat2ip_desc(const dvblink::streaming::sat2ip_server_desc& desc, const std::string& device_ip, int fidx, dvblink::TDevAPIDevInfoEx& tuner_info);

