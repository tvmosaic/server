/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <sstream>

#ifndef WIN32
    #include <unistd.h>
#endif

#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_ts.h>
#include <dl_ts_info.h>
#include <sdp.h>
#include <sat-ip.h>
#include <http_utils.h>
#include <rtsp_client.h>
#include "sat2ip_params.h"
#include "sat2ip_streamer.h"
#include "jrtplib/rtpsessionparams.h"
#include "jrtplib/rtpudpv4transmitter.h"
#include "jrtplib/rtppacket.h"
#include "jrtplib/rtpsourcedata.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink::streaming;
using namespace jrtplib;

const unsigned short PID_MIP                                 = 0x15;	    /* Mega-frame Initialization Packets */

#define MAX_BIND_ATTEMPTS   100
#define RTP_HEADER_SIZE     12

sat2ip_streamer::sat2ip_streamer(const std::string& server_ip, int frontend_idx, DL_E_TUNER_TYPES tuner_type) :
    rtsp_client_(NULL),
    cb_func_(NULL),
	reception_thread_(NULL),
    control_thread_(NULL),
    session_(NULL),
    tuner_type_(tuner_type),
    ext_ci_(external_ci_device_id_none)
{
    server_ip_ = server_ip;
    frontend_idx_ = frontend_idx + 1; //our frontends are from 0, sat2ip - from 1
}

sat2ip_streamer::~sat2ip_streamer()
{
    stop();
}

unsigned short sat2ip_streamer::get_base_port()
{
    //get random port in the range 20000 - 30000
    srand(time(NULL));
    return ((rand() % 10000) + 20000) & 0xFFFE;
}

bool sat2ip_streamer::prepare_rtp_session()
{
    bool res = false;

	RTPUDPv4TransmissionParams transparams;
    transparams.SetRTPReceiveBuffer(128*1024);
	RTPSessionParams sessparams;
	sessparams.SetOwnTimestampUnit(1.0/8000.0);		
	sessparams.SetAcceptOwnPackets(true);

    base_port_ = get_base_port();
    log_info(L"sat2ip_streamer::prepare_rtp_session. Searching for free ports starting from base %1%") % base_port_;
    int attempts = 0;
    while (attempts < MAX_BIND_ATTEMPTS)
    {
        transparams.SetPortbase(base_port_);

        session_ = new RTPSession();
	    int status = session_->Create(sessparams, &transparams);
        if (status == 0)
        {
            log_info(L"sat2ip_streamer::prepare_rtp_session. Found two free ports %1% and %2%") % base_port_ % (base_port_ + 1);
            res = true;
            break;
        } else
        {
        	log_ext_info(L"sat2ip_streamer::prepare_rtp_session: session create error %1%") % status;
            delete session_;
            session_ = NULL;
        }

        base_port_ += 2;
        attempts += 1;
    }
    if (!res)
        log_error(L"sat2ip_streamer::prepare_rtp_session. Unable to find the free ports for rtsp streaming");

    return res;
}

bool sat2ip_streamer::sat2ip_send_command(const std::string& command, std::string& status_line, 
    dvblink::streaming::http_headers& headers, std::string& body)
{
    std::string req;
    bool ok = rtsp_client_->prepare_request(command, req);

    if (!ok)
    {
        log_error(L"sat2ip_streamer::sat2ip_send_command. prepare_request failed");
        return false;
    }

    log_ext_info(L"sat2ip_streamer::sat2ip_send_command. Request: %1%") % string_cast<EC_UTF8>(req);
    ok = rtsp_client_->send_request(req);

    if (!ok)
    {
        log_error(L"sat2ip_streamer::sat2ip_send_command. send_request failed");
        return false;
    }

    const timeout_t timeout = boost::posix_time::milliseconds(10000);
    ok = rtsp_client_->receive_response(status_line, headers, body, timeout);
    
    if (!ok)
    {
        log_error(L"sat2ip_streamer::sat2ip_send_command. receive_response failed");
        std::string rec_buffer;
        rtsp_client_->get_last_received_buffer(rec_buffer);
        log_ext_info(L"sat2ip_streamer::sat2ip_send_command. Receive buffer: %1%") % string_cast<EC_UTF8>(rec_buffer);
        return false;
    }

    return true;
}

bool sat2ip_streamer::send_prepare_command(dvblink::TTransponderInfo* tr_info,
    dvblink::DL_E_DISEQC_TYPE diseqc, unsigned short actual_port)
{
	//if stream is already established, send PLAY command or SETUP otherwise
	if (stream_id_.size() > 0)
	{
		std::ostringstream str;
		str << "PLAY rtsp://" << server_ip_ << "/stream=" << stream_id_;
		str << "?src=" << get_sat2ip_src_from_diseqc(diseqc) << "&fe=" << frontend_idx_ << "&" << get_sat2ip_tuning_string(tr_info, tuner_type_) << "&pids=" << get_current_pids_string();
		str << " RTSP/1.0\r\n";
		str << "\r\n";

		std::string status_line;
		http_headers headers;
		std::string body;
		if (!sat2ip_send_command(str.str(), status_line, headers, body))
			return false;
	} else
	{
		std::ostringstream str;
		str << "SETUP rtsp://" << server_ip_ << "/?src=" << get_sat2ip_src_from_diseqc(diseqc) << "&fe=" << frontend_idx_ << "&" << get_sat2ip_tuning_string(tr_info, tuner_type_) << "&pids=" << get_current_pids_string();

        if (!boost::iequals(ext_ci_, external_ci_device_id_none))
		{
			std::string shwid = string_cast<EC_UTF8>(ext_ci_);
			str << "&x_ci=" << shwid;
		}

		str << " RTSP/1.0\r\n";
		str << "Transport: RTP/AVP;unicast;client_port="<< actual_port << "-" << (actual_port + 1) <<"\r\n";
		str << "\r\n";

		std::string status_line;
		http_headers headers;
		std::string body;
		if (!sat2ip_send_command(str.str(), status_line, headers, body))
			return false;

		bool found = find_http_header(headers, "com.ses.streamID", stream_id_);

		if (!found)
		{
			log_error(L"sat2ip_streamer::send_prepare_command. Server response does not cotain com.ses.streamID header");
			std::wstring werr;
			werr = string_cast<EC_UTF8>(status_line);
			log_ext_info(L"status line: %1%") % werr;
			werr = string_cast<EC_UTF8>(body);
			log_ext_info(L"body: %1%") % werr;
			for (size_t i=0; i<headers.size(); i++)
			{
				std::wstring wname, wvalue;
				wname = string_cast<EC_UTF8>(headers[i].name);
				wvalue = string_cast<EC_UTF8>(headers[i].value);
				log_ext_info(L"header %1%: %2%=%3%") % i % wname % wvalue;
			}
			return false;
		}
		std::wstring wstr = string_cast<EC_UTF8>(stream_id_);
		log_info(L"sat2ip_streamer::send_prepare_command. Succeeded. New stream ID is %1%") % wstr;
	}
    return true;
}

bool sat2ip_streamer::send_describe_command(dvblink::TSignalInfo* signal_stats, int* frontend_num)
{
    *frontend_num = -1;
    signal_stats->dwSize = sizeof(signal_stats);
    signal_stats->Level = signal_stats->Quality = signal_stats->Locked = 0;

    std::ostringstream str;
    if (stream_id_.size() == 0)
        str << "DESCRIBE rtsp://" << server_ip_ << "/ RTSP/1.0\r\n";
    else
        str << "DESCRIBE rtsp://" << server_ip_ << "/stream=" << stream_id_ << " RTSP/1.0\r\n";
    str << "Accept: application/sdp\r\n";
    str << "\r\n";

    std::string status_line;
    http_headers headers;
    std::string body;

    if (!sat2ip_send_command(str.str(), status_line, headers, body))
        return false;

    sdp_pairs_t pairs;
    bool ok = parse_sdp(body, pairs);

    if (ok)
    {
        unsigned int n = 0;
        get_frontends_number(pairs, n);

        if (n == 0)
        {
            log_error(L"sat2ip_streamer::send_describe_command. Zero frontends are detected");
            return false;
        }
        else
        {
            *frontend_num = n;
        }

        //also parse signal parameters
        bool lock;
        int level;
        int quality;

        if (get_signal_params(pairs, lock, quality, level))
        {
            signal_stats->Locked = lock ? 1 : 0;
            signal_stats->Quality = quality;
            signal_stats->Level = level;
        }
    }

    return true;
}

bool sat2ip_streamer::send_teardown_command()
{
    log_info(L"sat2ip_streamer::send_teardown_command. Sending TEARDOWN command");

    std::ostringstream str;
    str << "TEARDOWN rtsp://" << server_ip_ << "/stream=" << stream_id_ << " RTSP/1.0\r\n";
    str << "\r\n";

    std::string status_line;
    http_headers headers;
    std::string body;

    return sat2ip_send_command(str.str(), status_line, headers, body);
}

bool sat2ip_streamer::send_options_command()
{
    std::ostringstream str;
    str << "OPTIONS rtsp://" << server_ip_ << "/ RTSP/1.0\r\n";
    str << "\r\n";

    std::string status_line;
    http_headers headers;
    std::string body;

    return sat2ip_send_command(str.str(), status_line, headers, body);
}

bool sat2ip_streamer::open_rtsp_session()
{
    bool res = false;

	if (rtsp_client_ != NULL)
		return true;

//    if (stream_id_.size() > 0 || rtsp_client_ != NULL)
//        return false; //stop previous stream first

    rtsp_client_ = new rtsp_client();

    if (rtsp_client_->connect(server_ip_))
    {
        res = prepare_rtp_session();
    }
    else
    {
        std::wstring wip = string_cast<EC_UTF8>(server_ip_);
        log_error(L"sat2ip_streamer::open_rtsp_session. rtsp_client_.connect failed %1%") % wip;
    }
    //clean up if not successful
    if (!res)
        stop();

    return res;
}

void sat2ip_streamer::stop_reception()
{
    if (reception_thread_ != NULL)
    {
        stop_read_flag_ = true;

        reception_thread_->join();
        delete reception_thread_;
        reception_thread_ = NULL;

        control_thread_->join();
        delete control_thread_;
        control_thread_ = NULL;
    }

    cb_func_ = NULL;
    user_param_ = NULL;

    {
        boost::unique_lock<boost::mutex> lock(pids_lock_);

        pids_map_.clear();

        pids_to_add_.clear();
        pids_to_delete_.clear();

		pmt_to_add_.clear();
		pmt_to_delete_.clear();
		pmt_pid_map_.clear();

    }
}

bool sat2ip_streamer::tune(dvblink::TTransponderInfo* tr_info, dvblink::DL_E_DISEQC_TYPE diseqc)
{
    bool res = false;

    if (open_rtsp_session())
    {
		stop_reception();

        {
            boost::unique_lock<boost::mutex> lock(pids_lock_);

            //add PAT and MIP pids by default (MIP is required by FritzBox??)
            pids_map_.clear();
            pids_map_[PID_PAT] = PID_PAT;
            pids_map_[PID_MIP] = PID_MIP;
        }

		if(send_prepare_command(tr_info, diseqc, base_port_))
        {
			//clear all currently available data in the buffers
			clear_reception_buffers();

            res = true;
        }
    }
    else
    {
        std::wstring wip = string_cast<EC_UTF8>(server_ip_);
        log_error(L"sat2ip_streamer::tune. open_rtsp_session failed");
    }
    //clean up if not successful
    if (!res)
        stop();

    return res;
}

int sat2ip_streamer::get_frontend_number()
{
    dvblink::TSignalInfo signal_stats;
    int frontend_num;
    if (send_describe_command(&signal_stats, &frontend_num))
        return frontend_num;

    return -1;
}

std::string sat2ip_streamer::get_current_pids_string()
{
    boost::unique_lock<boost::mutex> lock(pids_lock_);

    std::ostringstream str;

    std::map<unsigned short, unsigned short>::iterator it = pids_map_.begin();
    while (it != pids_map_.end())
    {
        if (it != pids_map_.begin())
            str << ",";

        str << it->first;

        ++it;
    }
    return str.str();
}


bool sat2ip_streamer::send_play_command()
{
    std::ostringstream str;
    str << "PLAY rtsp://" << server_ip_ << "/stream=" << stream_id_ << "?pids=" << get_current_pids_string() << " RTSP/1.0\r\n";
    str << "\r\n";

    std::string status_line;
    http_headers headers;
    std::string body;

    return sat2ip_send_command(str.str(), status_line, headers, body);
}

bool sat2ip_streamer::start_reception(stream_cb_func cb_func, void* user_param)
{
    if (!send_play_command())
        return false;

    cb_func_ = cb_func;
    user_param_ = user_param;
    //start reception thread
    stop_read_flag_ = false;
    reception_thread_ = new boost::thread(boost::bind(&sat2ip_streamer::reception_thread_func, this));
    control_thread_ = new boost::thread(boost::bind(&sat2ip_streamer::control_thread_func, this));

#ifdef WIN32
	HANDLE thread_handle = reception_thread_->native_handle();
	SetThreadPriority(thread_handle, THREAD_PRIORITY_TIME_CRITICAL);
#else
    //linux
    pthread_t thread_id = (pthread_t)reception_thread_->native_handle();
    int policy = SCHED_RR;
    sched_param param;
    param.sched_priority =  sched_get_priority_min(policy) + (sched_get_priority_max(policy) - sched_get_priority_min(policy))*3/4;
    log_ext_info(L"sat2ip_streamer::start_reception. Trying to raise reception thread priority to %1%") % param.sched_priority;
    int retcode;
    if ((retcode = pthread_setschedparam(thread_id, policy, &param)) != 0)
    {
        log_warning(L"sat2ip_streamer::start_reception. pthread_setschedparam failed %1%") % retcode;
    }

#endif

    return true;
}

void sat2ip_streamer::stop()
{
	stop_reception();

    if (session_ != NULL)
    {
        log_info(L"sat2ip_streamer::stop. Deleting rtp session");
	    session_->BYEDestroy(RTPTime(10,0),0,0);
        delete session_;
        session_ = NULL;
    }

    if (stream_id_.size() > 0)
    {
        send_teardown_command();
        stream_id_.clear();
    }

    if (rtsp_client_ != NULL)
    {
        if (rtsp_client_->is_connected())
            rtsp_client_->disconnect();
        delete rtsp_client_;
        rtsp_client_ = NULL;
    }
}

bool sat2ip_streamer::get_signal_stats(dvblink::TSignalInfo* signal_stats)
{
    sat2ip_describe_cmd describe_param;
    describe_param.result = false;

    command_queue_.ExecuteCommand(EC_DESCRIBE, &describe_param);

    if (describe_param.result)
        *signal_stats = describe_param.signalInfo;

    return describe_param.result;
}

bool sat2ip_streamer::send_set_pmt_pid_command()
{
	//do not send the actual command if there are no pmt pids in the list
	//the next set_pmt command or tune will reset the last pmt pid
	if (pmt_pid_map_.size() == 0)
		return true;

    std::ostringstream str;
    str << "PLAY rtsp://" << server_ip_ << "/stream=" << stream_id_ << "?pids=" << get_current_pids_string() << "&x_pmt=";

	pmt_pid_desc_map_t::iterator it = pmt_pid_map_.begin();
	while (it != pmt_pid_map_.end())
	{
        if (it != pmt_pid_map_.begin())
            str << ",";
//		str << it->second.sid << "." << it->second.pmt_pid;
		str << it->second.pmt_pid;

		++it;
    }
	
	str << " RTSP/1.0\r\n";
    str << "\r\n";

    std::string status_line;
    http_headers headers;
    std::string body;

    bool ok = sat2ip_send_command(str.str(), status_line, headers, body);
    return ok;
}
/*
bool sat2ip_streamer::send_add_pid_command(std::vector<unsigned short>& pids)
{
    std::ostringstream str;
    str << "PLAY rtsp://" << server_ip_ << "/stream=" << stream_id_ << "?addpids=";
    for (size_t i=0; i<pids.size(); i++)
    {
        if (i != 0)
            str << ",";
        str << pids[i];
    }
    str << " RTSP/1.0\r\n";
    str << "\r\n";

    std::string status_line;
    http_headers headers;
    std::string body;

    bool ok = sat2ip_send_command(str.str(), status_line, headers, body);
    return ok;
}

bool sat2ip_streamer::send_delete_pid_command(std::vector<unsigned short>& pids)
{
    std::ostringstream str;
    str << "PLAY rtsp://" << server_ip_ << "/stream=" << stream_id_ << "?delpids=";
    for (size_t i=0; i<pids.size(); i++)
    {
        if (i != 0)
            str << ",";
        str << pids[i];
    }
    str << " RTSP/1.0\r\n";
    str << "\r\n";

    std::string status_line;
    http_headers headers;
    std::string body;

    bool ok = sat2ip_send_command(str.str(), status_line, headers, body);
    return ok;
}
*/
bool sat2ip_streamer::set_pmt_pid(pmt_pid_desc pmt, int listmng)
{
    bool res = false;

    if (stream_id_.size() == 0)
        return false;

	log_info(L"sat2ip_streamer::set_pmt_pid. pid %1%, sid %2%, listmng %3%") % pmt.pmt_pid % pmt.sid % listmng;

	switch (listmng)
	{
	case dvblink::SERVICE_DECRYPT_ADD:
		{
			boost::unique_lock<boost::mutex> lock(pids_lock_);
			pmt_to_add_.push_back(pmt);
			res = true;
		}
		break;
	case dvblink::SERVICE_DECRYPT_REMOVE:
		{
			boost::unique_lock<boost::mutex> lock(pids_lock_);
			pmt_to_delete_.push_back(pmt);
			res = true;
		}
		break;
	}

    return res;
}

bool sat2ip_streamer::add_pid(unsigned short pid)
{
    if (stream_id_.size() == 0)
        return false;

    //we do not add/handle NULL packet pid
    if (pid == NULL_PACKET_PID)
        return true;

    log_info(L"sat2ip_streamer::add_pid. Adding pid %1%") % pid;

    boost::unique_lock<boost::mutex> lock(pids_lock_);
    pids_to_add_.push_back(pid);

    return true;
}

bool sat2ip_streamer::delete_pid(unsigned short pid)
{
    if (stream_id_.size() == 0)
        return false;

    log_info(L"sat2ip_streamer::delete_pid. Deleting pid %1%") % pid;

    boost::unique_lock<boost::mutex> lock(pids_lock_);
    pids_to_delete_.push_back(pid);

    return true;
}

void sat2ip_streamer::send_stream(const unsigned char* buf, int len)
{
	const unsigned char* start = buf;
	const unsigned char* cur = buf;
	const unsigned char* end = buf + len;
	size_t to_send = 0;

	while (cur < end)
	{
		unsigned short pid = ts_process_routines::GetPacketPID(cur);

        if (pid == NULL_PACKET_PID)
		{
			if (to_send)
			{
                cb_func_(start, to_send, user_param_);
				to_send = 0;
			}

			start = cur;
		}
		else
		{
			to_send += TS_PACKET_SIZE;
		}
		cur += TS_PACKET_SIZE;
	}

	if (to_send)
	{
        cb_func_(start, to_send, user_param_);
	}
}

void sat2ip_streamer::clear_reception_buffers()
{
	session_->Poll();

	session_->BeginDataAccess();
        	
    // check incoming packets
    if (session_->GotoFirstSourceWithData())
    {
        do
        {
	        RTPSourceData* srcdat = session_->GetCurrentSourceInfo();
			srcdat->FlushPackets();
        } while (session_->GotoNextSourceWithData());
    }
    	
    session_->EndDataAccess();
}

void sat2ip_streamer::reception_thread_func()
{
    log_info(L"sat2ip_streamer::reception_thread_func. Reception thread is started");

    unsigned short prev_seq_num = 0;
    while(!stop_read_flag_)
    {
		session_->WaitForIncomingData(RTPTime(0,50000));

		int status = session_->Poll();

		session_->BeginDataAccess();
        	
        // check incoming packets
        if (session_->GotoFirstSourceWithData())
        {
	        do
	        {
		        RTPPacket *pack;
		        RTPSourceData *srcdat;
        		
		        srcdat = session_->GetCurrentSourceInfo();
        		
		        while ((pack = session_->GetNextPacket()) != NULL)
		        {
                    unsigned short seq = pack->GetSequenceNumber();
                    if (prev_seq_num != 0)
                    {
                        if (seq - prev_seq_num > 1)
                            log_warning(L"sat2ip_streamer::reception_thread_func. missed packets: %1%, %2%") % seq % prev_seq_num;
                    }
                    prev_seq_num = seq;

                    if (pack->GetPayloadLength() > 0)
                    {
                    	send_stream((const unsigned char*)pack->GetPayloadData(), pack->GetPayloadLength());
                    }
			        session_->DeletePacket(pack);
		        }
	        }
            while (session_->GotoNextSourceWithData());
        }
        	
        session_->EndDataAccess();
    }

    log_info(L"sat2ip_streamer::reception_thread_func. Reception thread is finished");
}

void sat2ip_streamer::control_thread_func()
{
    log_info(L"sat2ip_streamer::control_thread_func. Control thread is started");

    namespace pt = boost::posix_time;
    pt::ptime start_options = pt::microsec_clock::local_time();

    std::string buf(4096, '\0');
    while(!stop_read_flag_)
    {

        bool b_pid_changes = false;

        {
            boost::unique_lock<boost::mutex> lock(pids_lock_);

            //check for pids to add
            for (size_t i=0; i<pids_to_add_.size(); i++)
            {
                unsigned short pid = pids_to_add_[i];
                if (pids_map_.find(pid) == pids_map_.end())
                {
                    pids_map_[pid] = pid;
                    b_pid_changes |= true;
                }
            }
            pids_to_add_.clear();

    		//check for pids to delete
            for (size_t i=0; i<pids_to_delete_.size(); i++)
            {
                unsigned short pid = pids_to_delete_[i];
                if (pid != PID_MIP && pid != PID_PAT && pids_map_.find(pid) != pids_map_.end())
                {
                    pids_map_.erase(pid);
                    b_pid_changes |= true;
                }
            }
        }

        if (b_pid_changes)
            send_play_command();

		//check for pmt pids changes
		bool b_send_pmt_pid_cmd = false;
        {
            boost::unique_lock<boost::mutex> lock(pids_lock_);
			b_send_pmt_pid_cmd = pmt_to_add_.size() > 0 || pmt_to_delete_.size() > 0;

			for (size_t i=0; i<pmt_to_add_.size(); i++)
				pmt_pid_map_[pmt_to_add_.at(i).get_key()] = pmt_to_add_.at(i);

			for (size_t i=0; i<pmt_to_delete_.size(); i++)
				pmt_pid_map_.erase(pmt_to_delete_.at(i).get_key());

			pmt_to_add_.clear();
			pmt_to_delete_.clear();
        }

		if (b_send_pmt_pid_cmd)
            send_set_pmt_pid_command();

        pt::ptime now = pt::microsec_clock::local_time();
        
        //send keep-alive message
        if ((now - start_options) >= pt::seconds(10))
        {
            send_options_command();
            start_options = now;
        }

        SDLCommandItem* item;
        if (command_queue_.PeekCommand(&item))
        {
            switch (item->id)
            {
            case EC_DESCRIBE:
                {
                    sat2ip_describe_cmd* describe_params = (sat2ip_describe_cmd*)item->param;
                    describe_params->result = send_describe_command(&(describe_params->signalInfo), &(describe_params->frontend_num));
                }; break;
            }
            command_queue_.FinishCommand(&item);
        }

        boost::this_thread::sleep(boost::posix_time::milliseconds(10));
    }
    
    log_info(L"sat2ip_streamer::control_thread_func. Control thread is finished");
}
