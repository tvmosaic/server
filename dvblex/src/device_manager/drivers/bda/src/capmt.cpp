/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include "capmt.h"
#include <vector>
#include <string>
#include <dl_ts_info.h>

using namespace dvblink::engine;

bool CreateCAPMTFromPMT(unsigned char* pmt, int pmt_len, E_CAPMT_LIST_MGMT listmgt, 
						E_CAPMT_CMD_ID cmd, unsigned char* ca_pmt, int& ca_pmt_len)
{
    unsigned char version = 1; //hardcode it for now
    //get information about ES from pmt
    std::vector<TCA_PMT_ES_DESC> stream_info;
    unsigned short ServiceId;
	ts_process_routines::GetPMTStreamsInfoWithDesc(pmt, pmt_len, ServiceId, stream_info);
    //get all ca descriptors
    std::vector<TCA_PMT_CA_DESC> ca_desc_list;
	ts_process_routines::GetCADescriptorsFromPMTWithDesc(pmt, pmt_len, ca_desc_list);
    //Start forming the CA_PMT
    ca_pmt_len = 0;
    //list management
    ca_pmt[ca_pmt_len] = listmgt;
    ca_pmt_len += 1;
    //program number
	ca_pmt[ca_pmt_len] = (ServiceId >> 8) & 0xFF;
    ca_pmt[ca_pmt_len+1] = ServiceId & 0xFF;
    ca_pmt_len += 2;
	//Reserved, Version number, Current-Next indicator (always set to 1)
	ca_pmt[ca_pmt_len] = (BYTE)(0xC0|(((version&0x1F)<<1) & 0xFE) | 0x01);
    ca_pmt_len += 1;
    //remember program info length offset and reserve place for it
    int prg_info_len_offset = ca_pmt_len;
    ca_pmt_len += 2;
    if (ca_desc_list.size() > 0)
    {
        //command
        ca_pmt[ca_pmt_len] = cmd;
        ca_pmt_len += 1;
        //CA descriptors
        for (unsigned int i=0; i<ca_desc_list.size(); i++)
        {
            memcpy(ca_pmt + ca_pmt_len, &(ca_desc_list[i].at(0)), ca_desc_list[i].size());
            ca_pmt_len += ca_desc_list[i].size();
        }
    }
    //update length field
    WORD l = ca_pmt_len - prg_info_len_offset - 2;
	ca_pmt[prg_info_len_offset] = (l >> 8) & 0x0F;
	ca_pmt[prg_info_len_offset+1] = (l & 0x00FF);
    //elementary streams
    for (unsigned int i=0; i<stream_info.size(); i++)
    {
		if (ts_process_routines::IsAudioStream(stream_info[i].type, stream_info[i].other_desc_list) ||
			ts_process_routines::IsVideoStream(stream_info[i].type, stream_info[i].other_desc_list) || 
            stream_info[i].ca_desc_list.size() != 0)
        {
            //stream type
            ca_pmt[ca_pmt_len] = stream_info[i].type;
            ca_pmt_len += 1;
            //ES pid
            ca_pmt[ca_pmt_len] = (BYTE)(0xE0 | ((stream_info[i].pid >> 8) & 0x1F));
	        ca_pmt[ca_pmt_len + 1] = stream_info[i].pid & 0xFF;
            ca_pmt_len += 2;
            //remember ES info length offset and reserve place for it
            int es_info_len_offset = ca_pmt_len;
            ca_pmt_len += 2;
            if (stream_info[i].ca_desc_list.size() > 0)
            {
                //command
                ca_pmt[ca_pmt_len] = cmd;
                ca_pmt_len += 1;
                //CA descriptors
                for (unsigned int es_ca_idx=0; es_ca_idx<stream_info[i].ca_desc_list.size(); es_ca_idx++)
                {
                    memcpy(ca_pmt + ca_pmt_len, &(stream_info[i].ca_desc_list[es_ca_idx].at(0)), stream_info[i].ca_desc_list[es_ca_idx].size());
                    ca_pmt_len += stream_info[i].ca_desc_list[es_ca_idx].size();
                }
            }
            //update es info length field
            WORD l = ca_pmt_len - es_info_len_offset - 2;
	        ca_pmt[es_info_len_offset] = ((l >> 8) & 0x0F);
	        ca_pmt[es_info_len_offset+1] = (l & 0x00FF);
        }
    }
    return true;
}
