/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <errno.h>
#include <conio.h>
#include <string.h>
#include <boost/algorithm/string.hpp>
#include <boost/thread.hpp>

#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <tuner.h>
#include <commctrl.h>
//#include <atlbase.h>
#include <stdarg.h>

#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_utils.h>
#include <drivers/bda.h>
#include <dl_ts_info.h>
#include <dl_dshow_enum.h>
#include <dl_dshow_helpers.h>
#include "ds_graph.h"
#include "common.h"
#include "ds_samplegrabber.h"
#include "ds_msdemuxer.h"
#include "ds_signalstats.h"
#include "ds_tunerequest.h"
#include "ds_propertyset.h"
#include "ds_topology.h"
#include "bda_aux.h"
#include "extciman.h"
#include "ddci.h"

using namespace dvblink::logging;
using namespace dvblink::engine;

/*
	GRAPH OUTLINE

	Network Provider---->Tuner---->Capture---->SampleGrabber---->MPEG2 Demuxer---->TIF	

*/

// *** bda_tuner_factory_t

namespace dvblink {

bda_tuner_factory_t::bda_tuner_factory_t(const dvblink::filesystem_path_t& device_config_path) :
    tuner_factory_t(device_config_path)
{
}

int bda_tuner_factory_t::DeviceGetList(dvblink::PDevAPIDevListEx pDL)
{
    boost::thread* t = new boost::thread(boost::bind(&bda_tuner_factory_t::device_enum_thread, pDL));
    t->join();
    delete t;

    //postprocess list for device types
    for (int i=0; i<pDL->Count; i++)
    {
        std::string device_path = string_cast<EC_UTF8>(pDL->Devices[i].szDevicePathW);
        bda_device_descriptor_t descriptor;
        if (bda_type_storage_t::get_device_descriptor(device_path, descriptor))
            pDL->Devices[i].TunerType = descriptor.types_;
    }

    return dvblink::SUCCESS;
}

tuner_t* bda_tuner_factory_t::get_tuner(const std::string& device_path, int fdx)
{
    //update device types before returning new tuner to make sure it is not in use when type is asked
    bda_type_storage_t::update_descriptors();

    return new bda_tuner_t(device_config_path_, device_path, fdx);
}

void bda_tuner_factory_t::device_enum_thread(dvblink::PDevAPIDevListEx pDL)
{
	CDLCoInitHolder coinit(COINIT_MULTITHREADED);

    DeviceGetListEx_impl(pDL);
}

void bda_tuner_factory_t::get_aux_list(aux_module_list_t& aux_list)
{
    bda_aux_t* aux = new bda_aux_t();
    aux_list.push_back(aux);
}

int bda_tuner_factory_t::ExtCIDeviceGetList(const std::string& device_path, PExtCIDeviceList pDL)
{
    boost::thread* t = new boost::thread(boost::bind(&bda_tuner_factory_t::ext_ci_device_enum_thread, pDL));
    t->join();
    delete t;

    return dvblink::SUCCESS;
}

void bda_tuner_factory_t::ext_ci_device_enum_thread(dvblink::PExtCIDeviceList pDL)
{
	CDLCoInitHolder coinit(COINIT_MULTITHREADED);

    pDL->Count = 0;

    ext_ci_manager ext_ci_man;

    ext_device_list_t devices;
    if (ext_ci_man.get_device_list(devices))
    {
        pDL->Count = devices.size();
        for (unsigned int i=0; i<devices.size(); i++)
            pDL->Devices[i] = devices[i].device_info_;
    }
}

//bda_tuner_t *************************

bda_tuner_t::bda_tuner_t(const dvblink::filesystem_path_t& dll_path, const std::string& device_path, int fdx) :
    tuner_t(device_path, fdx), tuner_type_(TUNERTYPE_UNKNOWN), 
    pGraph_(NULL),
    pfltnetworkprovider_(NULL),
    sample_grabber_man_(NULL),
    pfltdemuxer_(NULL),
    pflttif_(NULL),
    pmediactrl_(NULL),
    ptuner_(NULL),
    pTuningSpace_(NULL),
    is_device_started_(false),
    Tone_(0),
    Diseqc_(0),
    decryption_state_(dvblink::SERVICE_DECRYPT_FIRST),
    pBDAFrequencyFilter_(NULL),
    pBDASignalStatistics_(NULL),
    pBDALNBInfo_(NULL),
    pBDADigitalDemodulator_(NULL),
    pDiseqcCommandInterface_(NULL),
    pfltTuner_(NULL),
    pfltCapture_(NULL),
    pfltCaptureEx_(NULL),
    pfltCaptureEx1_(NULL),
    packet_aligner_(aligner_callback, this),
    dll_path_(dll_path),
    ext_ci_id_(external_ci_device_id_none)
{
    memset(wsTunerName_, 0, sizeof(wsTunerName_));
    memset(wsDevicePath_, 0, sizeof(wsDevicePath_));

    ps_man_ = new property_set_manager_t();
    ext_ci_man_ = new ext_ci_manager();
}

bda_tuner_t::~bda_tuner_t()
{
    delete ps_man_;
    delete ext_ci_man_;
}

void bda_tuner_t::update_property_set_graph_info()
{
    property_set_graph_info_t gi;
    gi.pGraph_ = pGraph_;
    gi.pfltnetworkprovider_ = pfltnetworkprovider_;
    gi.pfltsamplegrabber_ = sample_grabber_man_ == NULL ? NULL : sample_grabber_man_->get_filter();
    gi.pfltdemuxer_ = pfltdemuxer_;
    gi.pflttif_ = pflttif_;
    gi.pmediactrl_ = pmediactrl_;
    gi.ptuner_ = ptuner_;
    gi.pTuningSpace_ = pTuningSpace_;
    gi.pBDAFrequencyFilter_ = pBDAFrequencyFilter_;
    gi.pBDASignalStatistics_ = pBDASignalStatistics_;
    gi.pBDALNBInfo_ = pBDALNBInfo_;
    gi.pBDADigitalDemodulator_ = pBDADigitalDemodulator_;
    gi.pDiseqcCommandInterface_ = pDiseqcCommandInterface_;
    gi.pfltTuner_ = pfltTuner_;
    gi.pfltCapture_ = pfltCapture_;
    gi.pfltCaptureEx_ = pfltCaptureEx_;
    gi.pfltCaptureEx1_ = pfltCaptureEx1_;

    ps_man_->get_property_set()->set_graph_info(gi);
}

int bda_tuner_t::BDACleanUp()
{
    if (ps_man_ != NULL && ps_man_->get_property_set() != NULL)
	    ps_man_->get_property_set()->property_set_deinit();

	if(pflttif_) {
		pflttif_->Release();
		pflttif_ = NULL;
	}

	if(pfltdemuxer_) {
		pfltdemuxer_->Release();
		pfltdemuxer_ = NULL;
	}

	if(sample_grabber_man_) {
		delete sample_grabber_man_;
		sample_grabber_man_ = NULL;
	}

	if(ptuner_) {
		ptuner_->Release();
		ptuner_ = NULL;
	}

	if(pBDASignalStatistics_) {
		pBDASignalStatistics_->Release();
		pBDASignalStatistics_ = NULL;
	}

	if(pBDAFrequencyFilter_) {
		pBDAFrequencyFilter_->Release();
		pBDAFrequencyFilter_ = NULL;
	}

	if(pBDALNBInfo_) {
		pBDALNBInfo_->Release();
		pBDALNBInfo_ = NULL;
	}

	if(pBDADigitalDemodulator_) {
		pBDADigitalDemodulator_->Release();
		pBDADigitalDemodulator_ = NULL;
	}

	if(pDiseqcCommandInterface_) {
		pDiseqcCommandInterface_->Release();
		pDiseqcCommandInterface_ = NULL;
	}
    
	if(pfltCapture_) {
		pfltCapture_->Release();
		pfltCapture_ = NULL;
	}

	if(pfltCaptureEx_) {
		pfltCaptureEx_->Release();
		pfltCaptureEx_ = NULL;
	}

	if(pfltCaptureEx1_) {
		pfltCaptureEx1_->Release();
		pfltCaptureEx1_ = NULL;
	}

	if(pfltTuner_) {
		pfltTuner_->Release();
		pfltTuner_ = NULL;
	}

	if(pTuningSpace_) {
		pTuningSpace_->Release();
		pTuningSpace_ = NULL;
	}

	if(pfltnetworkprovider_) {
		pfltnetworkprovider_->Release();
		pfltnetworkprovider_ = NULL;
	}

    ext_ci_man_->destroy_device();

	if(pGraph_) 
		free_graph(&pGraph_, &pmediactrl_, NULL);

    if (ps_man_ != NULL)
        ps_man_->term();

	return 0;
}

HRESULT bda_tuner_t::ConnectExtCIFilter(IBaseFilter* upstream_filter, IBaseFilter* sample_grabber_filter)
{
    HRESULT ret_val = S_OK;
    if (ext_ci_man_->get_device() != NULL && ext_ci_man_->get_device()->type_ == ecdt_dd_ci)
    {
        if (FAILED(add_filter(pGraph_, DDCI_GetDeviceFilter(ext_ci_man_->get_device()->device_handle_), ext_ci_man_->get_device()->device_info_.name)) ||
            !ConnectFilters(pGraph_, upstream_filter, DDCI_GetDeviceFilter(ext_ci_man_->get_device()->device_handle_)) ||
            FAILED(add_filter(pGraph_, sample_grabber_filter, L"Sample grabber")) ||
            !ConnectFilters(pGraph_, DDCI_GetDeviceFilter(ext_ci_man_->get_device()->device_handle_), sample_grabber_filter))
        {
	        log_error(L"cannot connect ddci filter to upstream filter/sample grabber");
            ret_val = E_FAIL;
        }
    }
    else
    {
	    if (FAILED(add_filter(pGraph_, sample_grabber_filter, L"Sample grabber")))
	    {
            log_error(L"failed to add sample grabber filter");
            ret_val = E_FAIL;
	    }
        else
	    {
		    if (!ConnectFilters(pGraph_, upstream_filter, sample_grabber_filter))
		    {
                if (ps_man_->get_type() == BDACT_DD)
                {
			        //The following code is written for Digital Devices CI support
			        //If CI module support is enabled there is one more capture filter 
			        //that has to be connected to regular DD capture filter
			        if (create_capture(pGraph_, upstream_filter, &pfltCaptureEx_) == S_OK)
			        {
				        if (!ConnectFilters(pGraph_, pfltCaptureEx_, sample_grabber_filter))
				        {
					        //do it one more time - as DD tuner can support now additional capture :)
					        if (create_capture(pGraph_, pfltCaptureEx_, &pfltCaptureEx1_) == S_OK)
					        {
						        if (!ConnectFilters(pGraph_, pfltCaptureEx1_, sample_grabber_filter))
						        {
							        //do it one more time - as DD tuner can support now additional capture :)
							        log_error(L"cannot connect extra upstream filter to sample grabber filter");
							        ret_val = E_FAIL;
						        }
						        else
						        {
							        //success! This is definitely DD tuner with two CI modules!
						        }
					        }
					        else
					        {
						        log_error(L"cannot connect upstream filter to sample grabber filter");
						        ret_val = E_FAIL;
					        }
				        }
                        else
				        {
					        //success! This is DD tuner with one CI module!
				        }
			        }
                    else
			        {
				        log_error(L"cannot connect upstream filter to sample grabber filter");
				        ret_val = E_FAIL;
			        }
                } else
                {
                    log_error(L"cannot connect upstream filter to sample grabber filter");
                    ret_val = E_FAIL;
                }
		    }
		}
	}

    return ret_val;
}

int bda_tuner_t::StartDevice(DL_E_TUNER_TYPES tuner_type)
{	
	dvblink::TDevAPIDevListEx device_list;
	if(DeviceGetListEx_impl(&device_list) == dvblink::FAIL) 
	{
        log_error(L"bda_tuner_t::StartDevice: DeviceGetListEx_impl failed");
		
		BDACleanUp();
		return dvblink::FAIL;
	}	

    tuner_type_ = tuner_type;
    std::wstring wdevice_path = string_cast<EC_UTF8>(device_path_);

    int dwDeviceNo = find_device_by_path(&device_list, wdevice_path.c_str());
    if (dwDeviceNo == -1)
    {
        log_warning(L"bda_tuner_t::StartDevice. Could not find the match for %1%") % wdevice_path;
		BDACleanUp();
		return dvblink::FAIL;
    }

	log_info(L"StartDevice()  dwDeviceNo:%d") % dwDeviceNo;
	Diseqc_ = 0;
	Tone_ = dvblink::TONEBURST_NONE;

	HRESULT hr;

    if (!boost::iequals(ext_ci_id_, external_ci_device_id_none))
        ext_ci_man_->create_device(ext_ci_id_);

	if (FAILED(create_graph(&pGraph_, &pmediactrl_, NULL))) 
    {
		log_error(L"cannot create the graph");
		BDACleanUp();
		return 1;
	}

	if (FAILED(create_tuner(pGraph_, wdevice_path, &pfltTuner_, wsTunerName_, 255, wsDevicePath_, 511))) 
    {
		log_error(L"cannot create tuner filter");		
		BDACleanUp();		
		return 5;
	}

    ps_man_->init(tuner_type_, wsTunerName_, wsDevicePath_, dll_path_.get().c_str());
	bool bUniversalProvider = ps_man_->use_universal_provider();

	if (FAILED(create_network_provider(pGraph_, tuner_type_, &pfltnetworkprovider_, &ptuner_, bUniversalProvider)))
    {
		log_error(L"cannot create network provider filter");		
		BDACleanUp();		
		return 2;
	}
	
	if (FAILED(hr = load_tuning_space(tuner_type_, &pTuningSpace_))) 
    {
		hr = create_tuning_space(tuner_type_, &pTuningSpace_);
	}
	if (FAILED(hr)) 
    {
		log_error(L"cannot create tuning space");
		BDACleanUp();		
		return 3;
	}

	//for legacy network provider this should happen before connecting network provider and tuner filters
	//for the Universal Microsoft network provider - after
	if (!bUniversalProvider)
	{
		if (FAILED(init_tuning_space(ptuner_, tuner_type_, pTuningSpace_))) 
		{
			log_error(L"cannot initialize the tuning space");
			BDACleanUp();		
			return 4;
		}
	}

	//get_tuner_type(pfltTuner_, &bda_tuner_type);

    update_property_set_graph_info();

	ps_man_->get_property_set()->property_set_init();
	
	if (!ConnectFilters(pGraph_, pfltnetworkprovider_, pfltTuner_)) 
    {
		log_error(L"cannot connect network provider to tuner filter");
		BDACleanUp();		
		return 6;
	}

	// Interfaces should be queried after connecting the NP & tuner 
	get_bda_filter_interfaces(pfltTuner_, &pBDAFrequencyFilter_, &pBDASignalStatistics_, &pBDALNBInfo_, &pBDADigitalDemodulator_, &pDiseqcCommandInterface_);

    pfltCapture_ = NULL;
	if (!ps_man_->get_bda_options()->no_capture_filter)
	{
	    //try to create "preferred" capture - e.g. the one that belongs to the same device as tuner
		if (FAILED(create_preferred_capture(wsDevicePath_, pGraph_, pfltTuner_, &pfltCapture_)))
		{
		    //try the old way then - enumerating all present captures
		    if (FAILED(create_capture(pGraph_, pfltTuner_, &pfltCapture_)))
            {
			    log_warning(L"cannot create capture filter");		
			    pfltCapture_ = NULL;
		    }
        }
    }
	
    sample_grabber_man_ = new sample_grabber_man_t(&bda_tuner_t::grabber_callback, this);
    if (sample_grabber_man_->get_filter() == NULL)
    {
		log_error(L"cannot create sample grabber filter");		
		BDACleanUp();		
		return 9;
	}

	if (pfltCapture_) 
    {
        if (FAILED(ConnectExtCIFilter(pfltCapture_, sample_grabber_man_->get_filter())))
        {
	        BDACleanUp();		
	        return 10;
        }
	}
    else 
    {  // No capture filter,  device has only tuner filter
        if (FAILED(ConnectExtCIFilter(pfltTuner_, sample_grabber_man_->get_filter())))
        {
	        BDACleanUp();		
	        return 11;
        }
	}
	
	if (FAILED(create_ms_demuxer(pGraph_, &pfltdemuxer_)))
    {
		log_error(L"cannot create ms mpeg-2 demultiplexer filter");		
		BDACleanUp();		
		return 11;
	}

    if (!ConnectFilters(pGraph_, sample_grabber_man_->get_filter(), pfltdemuxer_))
    {
		log_error(L"cannot connect sample grabber filter to ms mpeg-2 demultiplexer filter");
		BDACleanUp();
		return 12;
	} 

	if (FAILED(create_tif(pGraph_, &pflttif_)))
    {
		log_error(L"cannot create transport information filter");		
		BDACleanUp();		
		return 13;
	}

	if (!ConnectFilters(pGraph_, pfltdemuxer_, pflttif_))
    {
		log_error(L"cannot connect ms mpeg-2 demultiplexer filter to transport information filter");
		BDACleanUp();		
		return 14;
	} 
	
	start_graph(pmediactrl_, 3000);

	if (bUniversalProvider)
	{
		if (FAILED(init_tuning_space(ptuner_, tuner_type_, pTuningSpace_))) 
		{
			log_error(L"cannot initialize the tuning space");
			BDACleanUp();		
			return 4;
		}
	}

    update_property_set_graph_info();
	ps_man_->get_property_set()->property_set_after_graph_start();

    //turn lnb power on
    if (tuner_type_ == dvblink::TUNERTYPE_DVBS)
        ps_man_->get_property_set()->property_set_set_lnb_power(dvblink::POW_13V);

	is_device_started_ = true;
	
	log_info(L"StartDevice()    OK");
	log_info(L"----------------------------------------------------------------------------");
	log_info(L"");

	
	return dvblink::SUCCESS;
}


//----------------------------------------------------------------------------

int bda_tuner_t::StopDevice()
{
	if(!is_device_started_) 
		return dvblink::SUCCESS;

	log_info(L"StopDevice()");	

	is_device_started_ = false;	

    //turn lnb power off
    if (tuner_type_ == dvblink::TUNERTYPE_DVBS)
        ps_man_->get_property_set()->property_set_set_lnb_power(dvblink::POW_OFF);

	stop_graph(pmediactrl_, 3000);

	BDACleanUp();

	return dvblink::SUCCESS;
}


//----------------------------------------------------------------------------
HRESULT bda_tuner_t::BDASetTuner(int tuner_type, dvblink::PTransponderInfo Tp, int diseqc_port, unsigned long timeout)
{
    HRESULT ret_val = E_FAIL;

	if (tuner_type == dvblink::TUNERTYPE_DVBS)
	{
		ps_man_->get_property_set()->property_set_set_22khz(Tp->dwLnbKHz != dvblink::LNB_SELECTION_0);		
		ps_man_->get_property_set()->property_set_set_modulation_params(Tp);
	}

	ret_val = tune_request(ptuner_, pTuningSpace_, tuner_type, Tp, diseqc_port, pfltTuner_, pBDALNBInfo_, pBDAFrequencyFilter_, pBDADigitalDemodulator_, ps_man_->get_property_set());

	if (FAILED(ret_val))
    {
        log_error(L"BDASetTuner::tune_request failed");
    } else
	{
		//sleep 200 ms to give tuner a chance to tune (still needed?? But does not hurt.)
		Sleep(200);
    }

	return ret_val;
}

//----------------------------------------------------------------------------
int bda_tuner_t::SetTuner(dvblink::PTransponderInfo Tp)
{	
	int ret_val = dvblink::FAIL;

	if(!is_device_started_) 
    {
		log_error(L"Device is not started!  SetTuner() failed.");
		return ret_val;
	}

	if (Tp->dwFreq == 0)
	{
        ret_val = dvblink::SUCCESS;
	}
    else
	{
		log_info(L"SetTuner() %d, %s, %d  Mod:%d LOF:%d  22Khz:%d") % Tp->dwFreq % (Tp->Pol==dvblink::POL_VERTICAL?L"V":L"H") % Tp->dwSr % Tp->dwModulation % Tp->dwLOF % Tp->dwLnbKHz;
		
 		HRESULT hr = ps_man_->get_property_set()->property_set_set_tuner(Tp, Diseqc_, Tone_);
		if (hr == E_NOINTERFACE)
			hr = BDASetTuner(tuner_type_, Tp, Diseqc_, 5000);

		if (FAILED(hr))
        {
			log_error(L"SetTuner() failed");
			ret_val = dvblink::FAIL;
        }
        else
        {
			ret_val = dvblink::SUCCESS;
        }
	}
		
	//reset decryption state
	decryption_state_ = dvblink::SERVICE_DECRYPT_FIRST;

	return ret_val;
}


//----------------------------------------------------------------------------
int bda_tuner_t::GetTunerState(dvblink::PSignalInfo TunerState, dvblink::PTransponderInfo Tp)
{
	if(!is_device_started_) return dvblink::FAIL;

	ZeroMemory(TunerState, sizeof(dvblink::TSignalInfo));

	BOOL locked, present;
	int strength, quality;

	dvblink::TSignalInfo st;
	st.dwSize = sizeof(st);
	HRESULT hr = ps_man_->get_property_set()->property_set_get_tuner_state(&st);
	if (hr == E_NOINTERFACE)
		hr = get_signal_stats(pfltTuner_, &locked, &present, &strength, &quality);
	else
    {
		locked = st.Locked;
		present = st.Locked;
		strength = st.Level;
		quality = st.Quality;
	}


	if FAILED( hr ) 
		return dvblink::FAIL;

	
	TunerState->Quality = (unsigned char) quality;
	TunerState->Level = (unsigned char) strength;
	TunerState->Locked = locked?1:0;	

	if (TunerState->Locked) {
		if(TunerState->Level==0) TunerState->Level = TunerState->Quality;
		if(TunerState->Quality==0) TunerState->Quality = TunerState->Level;
		if (TunerState->Level>100) TunerState->Level = 100;
		if (TunerState->Quality>100) TunerState->Quality = 100;

	}
    else
    {
		TunerState->Quality = 0;
		TunerState->Level = 0;
	}


	return dvblink::SUCCESS;
}

//----------------------------------------------------------------------------
int bda_tuner_t::LNBPower(dvblink::DL_E_LNB_POWER_TYPES lnb_power)
{
	if(!is_device_started_) return dvblink::FAIL;
	
	HRESULT hr = ps_man_->get_property_set()->property_set_set_lnb_power(lnb_power);
    if (FAILED(hr))
        hr = set_lnb_voltage_using_polarity(pBDAFrequencyFilter_, lnb_power);

	return dvblink::SUCCESS;
}

//----------------------------------------------------------------------------
int bda_tuner_t::SendDiseqc(dvblink::PDiseqcCmd RawDiseqc, int ToneBurst)
{
	if(!is_device_started_) return dvblink::FAIL;
	
	if(ToneBurst<0||ToneBurst>2) return EINVAL; 

    Diseqc_ = 0;
	Tone_ = dvblink::TONEBURST_NONE;

	if(RawDiseqc)
	{
		int i;
		wchar_t s[255];
		wchar_t s2[10];		

		wcscpy_s(s, 255, L"SendDiseqc()  ");
		for(i = 0;i<RawDiseqc->iLength;i++) {
				swprintf_s(s2, L"%0.2X ", RawDiseqc->Data[i]);			
				wcscat_s(s, 255, s2);
			}
		swprintf_s(s2, L" Len:%d", RawDiseqc->iLength);
		wcscat_s(s, 255, s2);		
		log_info(s);


		HRESULT hr = ps_man_->get_property_set()->property_set_send_diseqc(&RawDiseqc->Data[0], RawDiseqc->iLength);
	
        if (hr == E_NOINTERFACE)
        {
            Diseqc_ = get_10_diseqc_from_raw_cmd(RawDiseqc->Data, RawDiseqc->iLength);
			log_info(L"SendDiseqc without raw diseqc,   Diseqc:%d") % Diseqc_;
		}
		
		Tone_ = dvblink::TONEBURST_NONE;
	}
	else
	{		
		Tone_ = ToneBurst;

		ps_man_->get_property_set()->property_set_send_toneburst(Tone_);
	}

	return dvblink::SUCCESS;
}

//----------------------------------------------------------------------------
int bda_tuner_t::AddFilter(int pid)
{	
	if(!is_device_started_) return dvblink::FAIL;

	if(pid<0) return dvblink::FAIL;
	
	return SUCCEEDED(ps_man_->get_property_set()->property_set_add_pid(pid)) ? dvblink::SUCCESS : dvblink::FAIL;
}

//----------------------------------------------------------------------------
int bda_tuner_t::DelFilter(int pid)
{
	
	if(!is_device_started_) return dvblink::FAIL;
	
	if(pid<0) return dvblink::FAIL;

	return SUCCEEDED(ps_man_->get_property_set()->property_set_del_pid(pid)) ? dvblink::SUCCESS : dvblink::FAIL;
}

//----------------------------------------------------------------------------
int bda_tuner_t::CISendPMT(unsigned char *buf, int len, int listmng)
{
    HRESULT hr = E_FAIL;

	dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd = (dvblink::DL_E_SERVICE_DECRYPTION_CMD)listmng;

	//check if this is a first call after channel change
	if (decryption_state_ == dvblink::SERVICE_DECRYPT_FIRST && cmd == dvblink::SERVICE_DECRYPT_ADD)
		cmd = dvblink::SERVICE_DECRYPT_FIRST;

    if (ext_ci_man_->get_device() != NULL)
    {
        if (ext_ci_man_->send_pmt(buf, len, cmd))
            hr = S_OK;
    }
    else
    {
        hr = ps_man_->get_property_set()->property_set_ci_send_pmt(buf, len, cmd);
    }

	//reset the decryption state value
	if (cmd == dvblink::SERVICE_DECRYPT_FIRST)
		decryption_state_ = dvblink::SERVICE_DECRYPT_ADD;
    
	if (hr == E_NOINTERFACE)
		return dvblink::ENOTIMPLEMENTED;
	if (hr == S_OK)
		return dvblink::SUCCESS;
	return dvblink::FAIL;
}

void bda_tuner_t::grabber_callback(unsigned char* data, int len, void* param)
{
    bda_tuner_t* parent = (bda_tuner_t*)param;
    parent->packet_aligner_.write_stream(data, len);
}

void __stdcall bda_tuner_t::aligner_callback(const unsigned char* buf, unsigned long len, void* user_param)
{
    bda_tuner_t* parent = (bda_tuner_t*)user_param;

    unsigned char* start = (unsigned char*)buf;
    unsigned char* end = start + len;
    unsigned char* cur = start;

    size_t to_send = 0;
    
    while (cur < end)
    {
        if (ts_process_routines::GetPacketPID(cur) == NULL_PACKET_PID) //do not send NULL packets
        {
            // send ready data
            if (to_send)
            {
                parent->callback_(start, to_send, parent->callback_param_);
                to_send = 0;
            }
            
            cur += TS_PACKET_SIZE;
            start = cur;
        }
        else
        {
            cur += TS_PACKET_SIZE;
            to_send += TS_PACKET_SIZE;
        }
    }

    if (to_send)
    {
        parent->callback_(start, to_send, parent->callback_param_);
    }
}

void bda_tuner_t::SetExtCIDevice(const std::wstring& ext_ci_device_id)
{
    ext_ci_id_ = ext_ci_device_id;
}

}	
