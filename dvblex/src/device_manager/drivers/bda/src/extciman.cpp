/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <map>
#include "extciman.h"
#include "ddci.h"

ext_ci_manager::ext_ci_manager()
    : device_(NULL)
{
}

ext_ci_manager::~ext_ci_manager()
{
    if (device_ != NULL)
        delete device_;
}

bool ext_ci_manager::get_device_list(ext_device_list_t& devices)
{
    devices.clear();
    //walk through all available types of external CI modules
    DDCI_EnumDevices(devices);

    return true;
}

bool ext_ci_manager::create_device(const std::wstring& hwid)
{
    bool ret_val = false;

    if (device_ == NULL)
    {
        //get device list and find our device
        ext_device_list_t devices;
        get_device_list(devices);

        for (size_t i=0; i<devices.size(); i++)
        {
            if (boost::iequals(devices[i].device_info_.hwid, hwid))
            {
                //found
                device_ = new ext_device_t();
                *device_ = devices[i];
                break;
            }
        }

        if (device_ != NULL)
        {
            switch (device_->type_)
            {
            case ecdt_dd_ci:
                {
                    device_->device_handle_ = DDCI_CreateDevice(hwid);
                    if (device_->device_handle_ != NULL)
                    {
                        ret_val = true;
                    } else
                    {
                        delete device_;
                        device_ = NULL;
                    }
                }
                break;

            default:
                delete device_;
                device_ = NULL;
                break;
            }
        }
    }

    return ret_val;
}

void ext_ci_manager::destroy_device()
{
    if (device_ != NULL)
    {
        switch (device_->type_)
        {
        case ecdt_dd_ci:
            {
                DDCI_DestroyDevice(device_->device_handle_);
            }
            break;
        default:
            break;
        }

        delete device_;
        device_ = NULL;
    }
}

bool ext_ci_manager::send_pmt(unsigned char* pmt_ptr, int pmt_len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd)
{
    bool ret_val = false;
    if (device_ != NULL)
    {
        switch (device_->type_)
        {
        case ecdt_dd_ci:
            ret_val = DDCI_SendPMT(device_->device_handle_, pmt_ptr, pmt_len, cmd);
            break;
        default:
            break;
        }
    }
    return ret_val;
}

