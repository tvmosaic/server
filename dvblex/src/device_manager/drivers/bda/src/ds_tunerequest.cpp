/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <boost/algorithm/string.hpp>

#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <tuner.h>
#include <commctrl.h>
#include <stdarg.h>
#include <bdatif.h>
#include <dl_dshow_enum.h>
#include <dl_dshow_helpers.h>
#include <dl_atsc_qam_helper.h>
#include "ds_graph.h"
#include "common.h"
#include "propertysets/hauppauge/hauppauge_main.h"
#include <drivers/deviceapi.h>
#include "ds_propertyset.h"
#include "ds_topology.h"

#pragma warning ( disable : 4995)

#include <atlbase.h>
#include <dl_logger.h>

using namespace dvblink::logging;
using namespace dvblink::engine;

int get_10_diseqc_from_raw_cmd(unsigned char *cmd, int len)
{
    int ret_val = 0;
    if (len == 4) 
    {
	    if(cmd[2]==0x38) 
        {
		    switch(cmd[3]) 
            {
			    case 0xf0:
			    case 0xf1:
			    case 0xf2:
			    case 0xf3: 
				    ret_val = 1;
			    break;
			    case 0xf4:
			    case 0xf5:
			    case 0xf6:
			    case 0xf7:
				    ret_val = 2;
			    break;
			    case 0xf8:
			    case 0xf9:
			    case 0xfa:
			    case 0xfb: 
				    ret_val = 3;
			    break;
			    case 0xfc:
			    case 0xfd:
			    case 0xfe:
			    case 0xff: 
				    ret_val = 4;
			    break;
		    }
	    }
    }
    return ret_val;
}

static void GetLOFValues(dvblink::PTransponderInfo Tp, unsigned long& lof1, unsigned long& lof2, unsigned long& lofsw)
{
    try
    {
		lof1 = Tp->LOF1;
		lof2 = Tp->LOF2;
		lofsw = Tp->LOFSW;
    } catch (...)
    {
		lof1 = lof2 = lofsw = Tp->dwLOF;
	}
}


BinaryConvolutionCodeRate bda_standard_get_fec(int fec)
{
	switch(fec) {
		case  dvblink::FEC_1_2:
			return BDA_BCC_RATE_1_2;				
			break;
		case  dvblink::FEC_2_3:
			return BDA_BCC_RATE_2_3;	
			break;
		case  dvblink::FEC_3_4:
			return BDA_BCC_RATE_3_4;				
			break;
		case  dvblink::FEC_3_5:
			return BDA_BCC_RATE_3_5;
			break;
		case  dvblink::FEC_4_5:
			return BDA_BCC_RATE_4_5;
			break;
		case  dvblink::FEC_5_6:
			return BDA_BCC_RATE_5_6;				
			break;
		case  dvblink::FEC_5_11:
			return BDA_BCC_RATE_5_11;				
			break;
		case  dvblink::FEC_6_7:
			return BDA_BCC_RATE_6_7;
			break;			
		case dvblink::FEC_1_4:
			return BDA_BCC_RATE_1_4;
			break;
		case dvblink::FEC_1_3:
			return BDA_BCC_RATE_1_3;
			break;
		case  dvblink::FEC_2_5:
			return BDA_BCC_RATE_2_5;
			break;
		case  dvblink::FEC_7_8:
			return BDA_BCC_RATE_7_8;				
			break;
		case  dvblink::FEC_8_9:
			return BDA_BCC_RATE_8_9; 
			break;
		case  dvblink::FEC_9_10:
			return BDA_BCC_RATE_9_10; 
			break;
		default:
			return BDA_BCC_RATE_NOT_SET;
			break;
	}		
}

ModulationType bda_standard_get_dvbs_modulation(int mod)
{
	switch (mod) {
		case dvblink::MOD_DVBS_QPSK:
			return BDA_MOD_QPSK;
			break;
		case dvblink::MOD_DVBS_8PSK:
			return BDA_MOD_NBC_8PSK;  // some cards might need BDA_MOD_8PSK here ?
			break;
		case dvblink::MOD_DVBS_16APSK:  
			return BDA_MOD_16APSK;
			break;
		case dvblink::MOD_DVBS_32APSK:  
			return BDA_MOD_32APSK;
			break;
		case dvblink::MOD_TURBO_8PSK:  // just for test
			return BDA_MOD_8PSK;
			break;
		case dvblink::MOD_DVBS_NBC_QPSK: 
			return BDA_MOD_NBC_QPSK;  
			break; 
		default:
			return BDA_MOD_QPSK; 
			break;
	}
}


ModulationType get_dvbc_modulation(int mod)
{
	switch (mod) {
		case dvblink::MOD_DVBC_QAM_16:
			return BDA_MOD_16QAM;
			break;
		case dvblink::MOD_DVBC_QAM_32:
			return BDA_MOD_32QAM;
			break;
		case dvblink::MOD_DVBC_QAM_64:
			return BDA_MOD_64QAM;
			break;
		case dvblink::MOD_DVBC_QAM_128:
			return BDA_MOD_128QAM;
			break;
		case dvblink::MOD_DVBC_QAM_256:
			return BDA_MOD_256QAM;
			break;
		default:
			return BDA_MOD_64QAM; 
			break;
	}
}

HRESULT create_dvbc_locator(dvblink::PTransponderInfo Tp, property_set_base_t* prop_set, IDVBCLocator **ppDVBCLocator)
{	
	HRESULT hr;

	if FAILED(hr = CoCreateInstance(__uuidof(DVBCLocator)/*CLSID_DVBCLocator*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(IDVBCLocator), (void**) ppDVBCLocator)) {
		log_error(L"cannot create the dvbc locator");
		return hr;
	}

    (*ppDVBCLocator)->put_InnerFEC(BDA_FEC_METHOD_NOT_SET);
    (*ppDVBCLocator)->put_InnerFECRate(BDA_BCC_RATE_NOT_SET);
    (*ppDVBCLocator)->put_OuterFEC(BDA_FEC_METHOD_NOT_SET);
    (*ppDVBCLocator)->put_OuterFECRate(BDA_BCC_RATE_NOT_SET);

	(*ppDVBCLocator)->put_CarrierFrequency(Tp->dwFreq);

	if (hauppauge_tuner_requires_dvbc_sr_correction(prop_set))
	{
		log_info(L"Applying SR DVB-C correction for Hauppauge tuner");
		(*ppDVBCLocator)->put_SymbolRate((int)(Tp->dwSr));
	} else
		(*ppDVBCLocator)->put_SymbolRate((int)(Tp->dwSr / 1000));

	(*ppDVBCLocator)->put_Modulation(get_dvbc_modulation(Tp->dwLOF));
	return hr;
}


HRESULT create_dvbt_locator(dvblink::PTransponderInfo Tp, IDVBTLocator **ppDVBTLocator)
{	
	HRESULT hr;

	if FAILED(hr = CoCreateInstance(__uuidof(DVBTLocator)/*CLSID_DVBTLocator*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(IDVBTLocator), (void**) ppDVBTLocator)) {
		log_error(L"cannot create the dvbt locator");
		return hr;
	}

	if FAILED((*ppDVBTLocator)->put_CarrierFrequency(Tp->dwFreq)) 
		log_warning(L"cannot put_CarrierFrequency()");
	//(*ppDVBTLocator)->put_Modulation(get_dvbt_modulation(Tp->dwModulation));  ??
	if FAILED((*ppDVBTLocator)->put_Bandwidth(Tp->dwLOF))
		log_warning(L"cannot put_Bandwidth()");
	return hr;
}

HRESULT create_atsc_locator(dvblink::PTransponderInfo Tp, IATSCLocator **ppATSCLocator)
{	
	HRESULT hr;

	if FAILED(hr = CoCreateInstance(__uuidof(ATSCLocator), NULL, CLSCTX_INPROC_SERVER, __uuidof(IATSCLocator), (void**) ppATSCLocator)) {
		log_error(L"cannot create the atsc locator");
		return hr;
	}

	if FAILED((*ppATSCLocator)->put_PhysicalChannel(Tp->dwFreq)) 
		log_warning(L"cannot put_PhysicalChannel()");
	return hr;
}

HRESULT create_clear_qam_locator(dvblink::PTransponderInfo Tp, IDigitalCableLocator **ppDigitalCableLocator)
{	
	HRESULT hr;

	if FAILED(hr = CoCreateInstance(__uuidof(DigitalCableLocator), NULL, CLSCTX_INPROC_SERVER, __uuidof(IDigitalCableLocator), (void**) ppDigitalCableLocator)) {
		log_error(L"cannot create the clear qam locator");
		return hr;
	}

    int channel = qam_center_frequency_to_channel(Tp->dwFreq * 1000);
    if (channel == 0)
    {
        log_error(L"create_clear_qam_locator. Cannot convert %1% into a physical channel") % (Tp->dwFreq * 1000);
        return E_FAIL;
    }

    hr = (*ppDigitalCableLocator)->put_PhysicalChannel(channel);
    if (hr != S_OK)
        log_error(L"create_clear_qam_locator. put_PhysicalChannel failed for channel %1%") % channel;

    return hr;
}

HRESULT tune_dvb_request(ITuner *ptuner, ITuningSpace *pTuningSpace, int tuner_type, dvblink::PTransponderInfo Tp, int diseqc_port, property_set_base_t* prop_set)
{
	HRESULT hr = S_OK;

	// create tune request
	ITuneRequest* pTuneRequest;
	if (FAILED(hr = pTuningSpace->CreateTuneRequest(&pTuneRequest)))	
    {
		log_error(L"cannot create Tune Request");
		return hr; 
	}

	// get interface for the tune request
	IDVBTuneRequest *pDVBTuneRequest = NULL;
	hr=pTuneRequest->QueryInterface(__uuidof(IDVBTuneRequest), (void**)&pDVBTuneRequest);
    pTuneRequest->Release();

    if (hr == S_OK && pDVBTuneRequest != NULL)
    {
		pDVBTuneRequest->put_ONID(-1);
		pDVBTuneRequest->put_TSID(-1);
		pDVBTuneRequest->put_SID(-1);
		
		IDVBSLocator *pDVBSLocator = NULL;
	    IDVBCLocator *pDVBCLocator = NULL;
	    IDVBTLocator *pDVBTLocator = NULL;

	    switch (tuner_type) 
        {
		    case dvblink::TUNERTYPE_DVBS:
				    hr = E_FAIL; //not supported anymore
			    break;
		    case dvblink::TUNERTYPE_DVBC:
			    if (SUCCEEDED(hr= create_dvbc_locator(Tp, prop_set, &pDVBCLocator)))
                {
				    hr = pDVBTuneRequest->put_Locator(pDVBCLocator);
                    pDVBCLocator->Release();
			    }
			    break;
		    case dvblink::TUNERTYPE_DVBT:
			    if (SUCCEEDED(hr= create_dvbt_locator(Tp, &pDVBTLocator)))
                {
				    hr = pDVBTuneRequest->put_Locator(pDVBTLocator);
                    pDVBTLocator->Release();
			    }
			    break;
            default:
                hr = E_FAIL;
                break;
	    }
        if (hr == S_OK)
        {
	        try 
            {
                hr = ptuner->put_TuneRequest(pDVBTuneRequest);
	        } catch(...)
	        {
                hr = E_FAIL;
	        }
            if (FAILED(hr))
		        log_error(L"put_TuneRequest()");
        }
        else
        {
		    log_error(L"cannot put locator");
        }

        pDVBTuneRequest->Release();
    }
    else
    {
		log_error(L"cannot get IDVBTuneRequest interface");
    }
	
	return hr;
}

HRESULT tune_atsc_request(ITuner *ptuner, ITuningSpace *pTuningSpace, int tuner_type, dvblink::PTransponderInfo Tp, int diseqc_port)
{
	HRESULT hr = S_OK;

	// create tune request
	ITuneRequest* pTuneRequest;
	if (FAILED(hr = pTuningSpace->CreateTuneRequest(&pTuneRequest)))	
    {
		log_error(L"cannot create Tune Request");
		return hr; 
	}

	// get interface for the ATSC tune request
	IATSCChannelTuneRequest* pATSCTuneRequest = NULL;
	hr=pTuneRequest->QueryInterface(__uuidof(IATSCChannelTuneRequest), (void**)&pATSCTuneRequest);
    pTuneRequest->Release();

    if (hr == S_OK && pATSCTuneRequest != NULL)
    {
		pATSCTuneRequest->put_Channel(-1);
		pATSCTuneRequest->put_MinorChannel(-1);

	    IATSCLocator *pATSCLocator = NULL;
	    if (SUCCEEDED(hr = create_atsc_locator(Tp, &pATSCLocator)))
        {
		    hr = pATSCTuneRequest->put_Locator(pATSCLocator);
            pATSCLocator->Release();
	    }

		if (hr == S_OK)
        {
	        try 
            {
                hr = ptuner->put_TuneRequest(pATSCTuneRequest);
	        } catch(...)
	        {
                hr = E_FAIL;
	        }
            if (FAILED(hr))
		        log_error(L"put_TuneRequest()");
        }
        else
        {
		    log_error(L"cannot put locator");
        }

        pATSCTuneRequest->Release();
    }
    else
    {
		log_error(L"cannot get IATSCChannelTuneRequest interface");
    }
	
	return hr;
}

HRESULT tune_clear_qam_request(ITuner *ptuner, ITuningSpace *pTuningSpace, int tuner_type, dvblink::PTransponderInfo Tp, int diseqc_port)
{
	HRESULT hr = S_OK;

	// create tune request
	ITuneRequest* pTuneRequest;
	if (FAILED(hr = pTuningSpace->CreateTuneRequest(&pTuneRequest)))	
    {
		log_error(L"cannot create Tune Request");
		return hr; 
	}

	// get interface for the DigitalCable tune request
	IDigitalCableTuneRequest* pDigitalCableTuneRequest = NULL;
	hr=pTuneRequest->QueryInterface(__uuidof(IDigitalCableTuneRequest), (void**)&pDigitalCableTuneRequest);
    pTuneRequest->Release();

    if (hr == S_OK && pDigitalCableTuneRequest != NULL)
    {
		pDigitalCableTuneRequest->put_Channel(-1);
		pDigitalCableTuneRequest->put_MinorChannel(-1);
		pDigitalCableTuneRequest->put_MajorChannel(-1);
		pDigitalCableTuneRequest->put_SourceID(-1);

	    IDigitalCableLocator *pDigitalCableLocator = NULL;
	    if (SUCCEEDED(hr = create_clear_qam_locator(Tp, &pDigitalCableLocator)))
        {
		    hr = pDigitalCableTuneRequest->put_Locator(pDigitalCableLocator);
            pDigitalCableLocator->Release();
	    }

		if (hr == S_OK)
        {
	        try 
            {
                hr = ptuner->put_TuneRequest(pDigitalCableTuneRequest);
	        } catch(...)
	        {
                hr = E_FAIL;
	        }
            if (FAILED(hr))
		        log_error(L"put_TuneRequest()");
        }
        else
        {
		    log_error(L"cannot put locator");
        }

        pDigitalCableTuneRequest->Release();
    } else
    {
		log_error(L"cannot get IDigitalCableTuneRequest interface");
    }
	
	return hr;
}

ULONG get_standard_input_range_from_diseqc(int diseqc_port)
{
	ULONG inputRange=0;

	switch(diseqc_port) 
    {
		case 1:
			inputRange=0x0000;	// Option = 0, Position = 0		LNB1  
			break;
		case 2:
			inputRange=0x0001;	// Option = 0, Position = 1		LNB2
			break;
		case 3:
			inputRange=0x0100;	// Option = 1, Position = 0		LNB3
			break;
		case 4:
			inputRange=0x0101;	// Option = 1, Position = 1		LNB4
			break;
		default:
			inputRange = -1;
			break;
	}
    return inputRange;
}

HRESULT dvbs_tune_request(ITuner *ptuner, ITuningSpace *pTuningSpace, 
                          int tuner_type, dvblink::PTransponderInfo Tp, int diseqc_port,
                          IBaseFilter *pfltTuner, IBDA_LNBInfo* pBDALNBInfo, IBDA_FrequencyFilter *pBDAFrequencyFilter, IBDA_DigitalDemodulator* pBDADigitalDemodulator,
                          property_set_base_t* prop_set)
{
    if (/*pBDALNBInfo == NULL || */ pBDAFrequencyFilter == NULL || pBDADigitalDemodulator == NULL)
    {
        log_error(L"dvbs_tune_request error. Some of the interfaces are missing: %1%, %2%, %3%") % pBDALNBInfo % pBDAFrequencyFilter % pBDADigitalDemodulator;
        return E_FAIL;
    }
	IBDA_DeviceControl *pDeviceControl = NULL;
	HRESULT hr = pfltTuner->QueryInterface(&pDeviceControl);
	if (SUCCEEDED(hr) && pDeviceControl) 
    {							
		hr = pDeviceControl->StartChanges(); 
        if (FAILED(hr))
            log_error(L"dvbs_tune_request: StartChanges error %1%") % hr;

        ULONG sr = Tp->dwSr / 1000;
        BinaryConvolutionCodeRate fec = prop_set->property_set_get_fec(Tp->dwFec);
        ModulationType mt = prop_set->property_set_get_dvbs_modulation(Tp->dwModulation);
        SpectralInversion si = BDA_SPECTRAL_INVERSION_AUTOMATIC;
	    unsigned long lof1, lof2, lofsw;
	    GetLOFValues(Tp, lof1, lof2, lofsw);

        if (pBDALNBInfo != NULL)
        {
	        hr = pBDALNBInfo->put_LocalOscilatorFrequencyLowBand(lof1);
            if (FAILED(hr))
                log_error(L"dvbs_tune_request: put_LocalOscilatorFrequencyLowBand error %1%") % hr;
	        hr = pBDALNBInfo->put_LocalOscilatorFrequencyHighBand(lof2);
            if (FAILED(hr))
                log_error(L"dvbs_tune_request: put_LocalOscilatorFrequencyHighBand error %1%") % hr;
	        hr = pBDALNBInfo->put_HighLowSwitchFrequency(lofsw);
            if (FAILED(hr))
                log_error(L"dvbs_tune_request: put_HighLowSwitchFrequency error %1%") % hr;
        }
	    hr = pBDAFrequencyFilter->put_Range(prop_set->property_set_get_input_range_from_diseqc(diseqc_port));
        if (FAILED(hr) && hr != 0x80070490)
            log_error(L"dvbs_tune_request: put_Range error %1%") % hr;
	    hr = pBDAFrequencyFilter->put_Polarity(Tp->Pol==dvblink::POL_VERTICAL ? BDA_POLARISATION_LINEAR_V : BDA_POLARISATION_LINEAR_H);
        if (FAILED(hr))
            log_error(L"dvbs_tune_request: put_Polarity error %1%") % hr;
	    hr = pBDAFrequencyFilter->put_FrequencyMultiplier(1000L);
        if (FAILED(hr))
            log_error(L"dvbs_tune_request: put_FrequencyMultiplier error %1%") % hr;
	    hr = pBDAFrequencyFilter->put_Frequency(Tp->dwFreq);
        if (FAILED(hr))
            log_error(L"dvbs_tune_request: put_Frequency error %1%") % hr;
	    hr = pBDADigitalDemodulator->put_SpectralInversion(&si);
//        if (FAILED(hr))
//            log_error(L"dvbs_tune_request: put_SpectralInversion error %1%") % hr;
	    hr = pBDADigitalDemodulator->put_ModulationType(&mt);
        if (FAILED(hr) && hr != 0x80070490) //element not found error
            log_error(L"dvbs_tune_request: put_ModulationType error %1%") % hr;
	    hr = pBDADigitalDemodulator->put_SymbolRate((ULONG *)&sr);
        if (FAILED(hr))
            log_error(L"dvbs_tune_request: put_SymbolRate error %1%") % hr;
	    hr = pBDADigitalDemodulator->put_InnerFECRate(&fec);
        if (FAILED(hr))
            log_error(L"dvbs_tune_request: put_InnerFECRate error %1%") % hr;

	    hr = pDeviceControl->CheckChanges();
		hr = pDeviceControl->CommitChanges();		
        if (FAILED(hr))
            log_error(L"dvbs_tune_request: StartChanges error %1%") % hr;
		
		pDeviceControl->Release();
		pDeviceControl = NULL;

	} else
    {
        log_error(L"dvbs_tune_request: no IBDA_DeviceControl interface");
    }
    return hr;
}

HRESULT tune_request(ITuner *ptuner, ITuningSpace *pTuningSpace, int tuner_type, dvblink::PTransponderInfo Tp, int diseqc_port,
                     IBaseFilter *pfltTuner, IBDA_LNBInfo* pBDALNBInfo, IBDA_FrequencyFilter *pBDAFrequencyFilter, IBDA_DigitalDemodulator* pBDADigitalDemodulator,
                     property_set_base_t* prop_set)
{
	HRESULT hr = S_OK;

	switch (tuner_type)
	{
	case dvblink::TUNERTYPE_DVBS:
        return dvbs_tune_request(ptuner, pTuningSpace, tuner_type, Tp, diseqc_port, pfltTuner, pBDALNBInfo, pBDAFrequencyFilter, pBDADigitalDemodulator, prop_set);
        break;
	case dvblink::TUNERTYPE_DVBC:
	case dvblink::TUNERTYPE_DVBT:
		return tune_dvb_request(ptuner, pTuningSpace, tuner_type, Tp, diseqc_port, prop_set);
		break;
	case dvblink::TUNERTYPE_ATSC:
		return tune_atsc_request(ptuner, pTuningSpace, tuner_type, Tp, diseqc_port);
		break;
	case dvblink::TUNERTYPE_CLEARQAM:
		return tune_clear_qam_request(ptuner, pTuningSpace, tuner_type, Tp, diseqc_port);
		break;
	default:
		break;
	}
	return E_FAIL;
}
