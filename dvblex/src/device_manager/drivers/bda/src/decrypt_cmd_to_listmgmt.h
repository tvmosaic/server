/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <dl_logger.h>
#include "capmt.h"
#include <drivers/deviceapi.h>

inline bool decryption_cmd_to_listmgt(dvblink::DL_E_SERVICE_DECRYPTION_CMD decrypt_cmd, E_CAPMT_LIST_MGMT& list_mgmt,
									  E_CAPMT_CMD_ID& cmd)
{
	bool ret_val = true;
	switch (decrypt_cmd)
	{
	case dvblink::SERVICE_DECRYPT_FIRST:
		list_mgmt = CAPMT_LIST_ONLY;
		cmd = CAPMT_CMD_OK_DESCRAMBLING;
		dvblink::logging::log_info(L"decryption_cmd_to_listmgt. Set first program to decrypt");
		break;
	case dvblink::SERVICE_DECRYPT_ADD:
		list_mgmt = CAPMT_LIST_ADD;
		cmd = CAPMT_CMD_OK_DESCRAMBLING;
		dvblink::logging::log_info(L"decryption_cmd_to_listmgt. Add program to decrypt");
		break;
	case dvblink::SERVICE_DECRYPT_REMOVE:
		list_mgmt = CAPMT_LIST_UPDATE;
		cmd = CAPMT_CMD_NOT_SELECTED;
		dvblink::logging::log_info(L"decryption_cmd_to_listmgt. Remove program to decrypt");
		break;
	case dvblink::SERVICE_DECRYPT_UPDATE:
		list_mgmt = CAPMT_LIST_UPDATE;
		cmd = CAPMT_CMD_OK_DESCRAMBLING;
		dvblink::logging::log_info(L"decryption_cmd_to_listmgt. Update program to decrypt");
		break;
	default:
		ret_val = false;
		dvblink::logging::log_error(L"decryption_cmd_to_listmgt. Unknown command %1%") % cmd;
		break;
	}
	return ret_val;
}
