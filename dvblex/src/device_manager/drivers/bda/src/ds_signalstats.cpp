/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>

#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <tuner.h>
#include <commctrl.h>
//#include <atlbase.h>
#include <stdarg.h>
#include <bdatif.h>
#include <dl_dshow_enum.h>
#include <dl_dshow_helpers.h>
#include "ds_graph.h"
#include "common.h"

using namespace dvblink::engine;

HRESULT get_signal_stats(IBaseFilter *pflttuner, BOOL *locked, BOOL *present, int *strength, int *quality)
{
	HRESULT hr = E_FAIL;
    HRESULT hres = E_FAIL;
    long longVal = *strength = *quality = 0;
    BYTE byteVal = *locked = *present = FALSE;

	if(!pflttuner) 
        return E_FAIL;

	//Get IID_IBDA_Topology
	IBDA_Topology * pBDATopology = NULL;
	hr = pflttuner->QueryInterface(__uuidof(IBDA_Topology), (void**)&pBDATopology);
	if (hr == S_OK && pBDATopology != NULL)
    {
	    ULONG NodeTypes;
	    ULONG NodeType[32];
	    IUnknown *pNode;

        hr = pBDATopology->GetNodeTypes(&NodeTypes, 32, NodeType);
        if (hr == S_OK)
        {
            hres = S_OK;
	        for ( int i=0 ; i<(int)NodeTypes ; i++ )
	        {
		        hr = pBDATopology->GetControlNode(0, 1, NodeType[i], &pNode);
		        if (hr == S_OK)
		        {
			        IBDA_SignalStatistics *pBDASignalStatistics;

			        hr = pNode->QueryInterface(__uuidof(IBDA_SignalStatistics), (void **)&pBDASignalStatistics);
			        if (hr == S_OK)
			        {
				        longVal = 0;
				        if (SUCCEEDED(hr = pBDASignalStatistics->get_SignalStrength(&longVal)))
					        *strength = max(min(max(longVal, 0), 100), *strength);

				        longVal = 0;
				        if (SUCCEEDED(hr = pBDASignalStatistics->get_SignalQuality(&longVal)))
					        *quality = max(min(max(longVal, 0), 100), *quality);

				        byteVal = 0;
				        if (SUCCEEDED(hr = pBDASignalStatistics->get_SignalLocked(&byteVal)))
					        *locked |= byteVal;

				        byteVal = 0;
				        if (SUCCEEDED(hr = pBDASignalStatistics->get_SignalPresent(&byteVal)))
					        *present |= byteVal;

				        pBDASignalStatistics->Release();
				        pBDASignalStatistics = NULL;
			        }
			        pNode->Release();
			        pNode = NULL;
		        }
	        }
        }
	    pBDATopology->Release();
    }

	return hres;
}
