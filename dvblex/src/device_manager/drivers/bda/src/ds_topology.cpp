/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>

#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <tuner.h>
#include <commctrl.h>
#include <stdarg.h>
#include <bdatif.h>
#include <dl_dshow_enum.h>
#include <dl_dshow_helpers.h>
#include "ds_graph.h"
#include <drivers/deviceapi.h>
#include <dl_logger.h>
#include "ds_topology.h"

using namespace dvblink::logging;
using namespace dvblink::engine;

const wchar_t *BDAInterfaceToName(REFIID iid)
{
    const wchar_t *szInterfaceName = L"";

	if (iid == __uuidof(IBDA_NetworkProvider)        ) { szInterfaceName = _T("IID_IBDA_NetworkProvider   "); }

	else if (iid == __uuidof(IBDA_EthernetFilter)    ) { szInterfaceName = _T("IID_IBDA_EthernetFilter    "); }

	else if (iid == __uuidof(IBDA_IPV4Filter)        ) { szInterfaceName = _T("IID_IBDA_IPV4Filter        "); }

	else if (iid == __uuidof(IBDA_IPV6Filter)        ) { szInterfaceName = _T("IID_IBDA_IPV6Filter        "); }

	else if (iid == __uuidof(IBDA_DeviceControl)     ) { szInterfaceName = _T("IID_IBDA_DeviceControl     "); }

	else if (iid == __uuidof(IBDA_PinControl)        ) { szInterfaceName = _T("IID_IBDA_PinControl        "); }

	else if (iid == __uuidof(IBDA_SignalProperties)  ) { szInterfaceName = _T("IID_IBDA_SignalProperties  "); }

	else if (iid == __uuidof(IBDA_SignalStatistics)  ) { szInterfaceName = _T("IID_IBDA_SignalStatistics  "); }

	else if (iid == __uuidof(IBDA_Topology)          ) { szInterfaceName = _T("IID_IBDA_Topology          "); }

	else if (iid == __uuidof(IBDA_VoidTransform)     ) { szInterfaceName = _T("IID_IBDA_VoidTransform     "); }
	
	else if (iid == __uuidof(IBDA_NullTransform)     ) { szInterfaceName = _T("IID_IBDA_NullTransform     "); }

	else if (iid == __uuidof(IBDA_FrequencyFilter)   ) { szInterfaceName = _T("IID_IBDA_FrequencyFilter   "); }

	else if (iid == __uuidof(IBDA_LNBInfo)           ) { szInterfaceName = _T("IID_IBDA_LNBInfo           "); }

	else if (iid == __uuidof(IBDA_AutoDemodulate)    ) { szInterfaceName = _T("IID_IBDA_AutoDemodulate    "); }

	else if (iid == __uuidof(IBDA_DigitalDemodulator)) { szInterfaceName = _T("IID_IBDA_DigitalDemodulator"); }

	else if (iid == __uuidof(IBDA_IPSinkControl)     ) { szInterfaceName = _T("IID_IBDA_IPSinkControl     "); }

	else if (iid == __uuidof(IBDA_IPSinkInfo)        ) { szInterfaceName = _T("IID_IBDA_IPSinkInfo        "); }

	else if (iid == __uuidof(IEnumPIDMap)            ) { szInterfaceName = _T("IID_IEnumPIDMap             "); }

	else if (iid == __uuidof(IMPEG2PIDMap)           ) { szInterfaceName = _T("IID_IMPEG2PIDMap            "); }

//	else if (iid == IID_IFrequencyMap                ) { szInterfaceName = _T("IID_IFrequencyMap          "); }

	else                                         { szInterfaceName = _T("Unknown Interface          "); }

	return szInterfaceName;
}

void get_pintype_str(int x, char *str)
{
	switch(x){
		case 0: 
			strcpy_s(str, 255, "Input");
			return;
		case 1:
			strcpy_s(str, 255, "Output");
			return;
		default:
			strcpy_s(str, 255, "Unknown");
			return;
	}
}

HRESULT get_bda_filter_interfaces(IBaseFilter *pfilter,
                                  IBDA_FrequencyFilter **pBDAFrequencyFilter,
                                  IBDA_SignalStatistics **pBDASignalStatistics,
                                  IBDA_LNBInfo** pBDALNBInfo,
                                  IBDA_DigitalDemodulator** pBDADigitalDemodulator,
                                  IBDA_DiseqCommand** pDiseqcCommandInterface)
{
	HRESULT hr = S_OK;

    *pBDAFrequencyFilter = NULL;
    *pBDASignalStatistics = NULL;
    *pBDALNBInfo = NULL;
    *pBDADigitalDemodulator = NULL;
    *pDiseqcCommandInterface = NULL;
    
    IBDA_Topology* piTopology = NULL;
	IUnknown* piUnknownNode = NULL;
	IUnknown* piUnknownInterface = NULL;
	ULONG PinTypes = 0;
	ULONG PinType[256];
	ULONG NodeTypes = 0;
	ULONG NodeType[256];
	ULONG Interfaces = 0;
	GUID Interface[256];
	char str[255];

	hr = pfilter->QueryInterface(__uuidof(IBDA_Topology), (void**)&piTopology);
	if FAILED(hr)	
	{
		log_error(L"cannot get the topology");
		return hr;
	}
	

	hr = piTopology->GetPinTypes(&PinTypes, 256, PinType);
	if (hr == S_OK)
	{
		log_info(L"GetTunerInterfaces: Pin types (%d) :") % PinTypes;
		for (ULONG i = 0; i < PinTypes; i++)
		{
			get_pintype_str(PinType[i], str);
			log_info(L"GetTunerInterfaces:		Pin %d Type = %S") % i % str;
		}
	}

	///////////////////////////////

	hr = piTopology->GetNodeTypes(&NodeTypes, 256, NodeType);
	if (SUCCEEDED(hr))
	{
		log_info(L"GetTunerInterfaces: Node types (%d):") % NodeTypes;

		ULONG i;
		for (i = 0; i < NodeTypes; i++)
		{
			log_info(L"GetTunerInterfaces:		Node %d Type = %d") % i % NodeType[i];

		}
		log_info(L"GetTunerInterfaces: Node interfaces:");

		for (i = 0; i < NodeTypes; i++)
		{
			hr = piTopology->GetNodeInterfaces(NodeType[i], &Interfaces, 256, Interface);

            if (hr == S_OK)
			{
				hr = piTopology->GetControlNode(0, 1, NodeType[i], &piUnknownNode);
                if (hr == S_OK)
                {
				    log_info(L"GetTunerInterfaces:    Interfaces on NodeType %d   (%d interfaces):") % i % Interfaces;

				    for (ULONG ii = 0; ii < Interfaces; ii++)
				    {
					    LPOLESTR olestr=NULL;
					    StringFromCLSID(Interface[ii], &olestr);

                        hr = piUnknownNode->QueryInterface(Interface[ii], (void **)&piUnknownInterface);
					    if (hr == S_OK)
					    {
						    log_info(L"GetTunerInterfaces:       %s %s") % olestr % BDAInterfaceToName(Interface[ii]);

						    if (!*pBDASignalStatistics && Interface[ii] == __uuidof(IBDA_SignalStatistics))
							    piUnknownInterface->QueryInterface(__uuidof(IBDA_SignalStatistics), (void**)pBDASignalStatistics);
						    if (!*pBDAFrequencyFilter && Interface[ii] == __uuidof(IBDA_FrequencyFilter))
							    piUnknownInterface->QueryInterface(__uuidof(IBDA_FrequencyFilter), (void**)pBDAFrequencyFilter);
							if (!*pBDALNBInfo && Interface[ii] == IID_IBDA_LNBInfo)										
							    piUnknownInterface->QueryInterface(__uuidof(IBDA_LNBInfo), (void**)pBDALNBInfo);
						    if (!*pBDADigitalDemodulator && Interface[ii] == IID_IBDA_DigitalDemodulator) 
							    piUnknownInterface->QueryInterface(__uuidof(IBDA_DigitalDemodulator), (void**)pBDADigitalDemodulator); 
						    if (*pDiseqcCommandInterface == NULL && Interface[ii] == __uuidof(IBDA_DiseqCommand)) 
							    piUnknownInterface->QueryInterface(__uuidof(IBDA_DiseqCommand), (void**)pDiseqcCommandInterface); 

						    piUnknownInterface->Release();
					    }
					    else
					    {
						    log_info(L"GetTunerInterfaces: %s %s - QueryInterface FAILED ! - hr = 0x%0lx") % olestr % BDAInterfaceToName(Interface[ii]) % hr;
					    }

                        CoTaskMemFree(olestr);

				    } //  next interface

				    piUnknownNode->Release();
                }
                else
                {
				    log_error(L"GetTunerInterfaces: FAILED: piTopology->GetControlNode(..)     i(node type index):%d") % i;
                }
			}
            else
            {
				log_error(L"GetTunerInterfaces: FAILED: piTopology->GetNodeInterfaces(..)     i(node type index):%d") % i;
            }
		} //next node type

	}
	return hr;
}

HRESULT set_lnb_voltage_using_polarity(IBDA_FrequencyFilter *pBDAFrequencyFilter, dvblink::DL_E_LNB_POWER_TYPES lnb_power)
{
    if (pBDAFrequencyFilter == NULL || lnb_power == dvblink::POW_OFF)
        return E_FAIL;

    Polarisation pol = BDA_POLARISATION_NOT_SET;
    switch (lnb_power)
    {
    case dvblink::POW_13V:
        pol = BDA_POLARISATION_LINEAR_V;
        break;
    case dvblink::POW_18V:
        pol = BDA_POLARISATION_LINEAR_H;
        break;
    }

    HRESULT hr = pBDAFrequencyFilter->put_Polarity(pol);
    if (hr != S_OK)
        log_warning(L"set_lnb_voltage_using_polarity. put_Polarity failed (%1%)") % hr;
    return hr;
}

