/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#ifndef _DS_TUNEREQUEST_H
#define _DS_TUNEREQUEST_H

#include "ds_propertyset.h"

ModulationType bda_standard_get_dvbs_modulation(int mod);
BinaryConvolutionCodeRate bda_standard_get_fec(int fec);
ULONG get_standard_input_range_from_diseqc(int diseqc_port);
int get_10_diseqc_from_raw_cmd(unsigned char *cmd, int len);
ModulationType get_dvbc_modulation(int mod);

HRESULT tune_request(ITuner *ptuner, ITuningSpace *pTuningSpace, int tuner_type, dvblink::PTransponderInfo Tp, int diseqc_port,
                     IBaseFilter *pfltTuner, IBDA_LNBInfo* pBDALNBInfo, IBDA_FrequencyFilter *pBDAFrequencyFilter, IBDA_DigitalDemodulator* pBDADigitalDemodulator,
                     property_set_base_t* prop_set);

#endif


