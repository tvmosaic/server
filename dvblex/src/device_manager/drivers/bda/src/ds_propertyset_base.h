/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#ifndef _DS_PROPERTYSET_BASE_H
#define _DS_PROPERTYSET_BASE_H

#include <bdatypes.h>
#include <drivers/deviceapi.h>
#include "bda_diseqc.h"

struct property_set_graph_info_t
{
    IGraphBuilder *pGraph_;
    IBaseFilter *pfltnetworkprovider_;
    IBaseFilter *pfltsamplegrabber_;
    IBaseFilter *pfltdemuxer_;
    IBaseFilter *pflttif_;
    IMediaControl *pmediactrl_;
    ITuner *ptuner_;
    ITuningSpace *pTuningSpace_;
    IBDA_FrequencyFilter *pBDAFrequencyFilter_;
    IBDA_SignalStatistics *pBDASignalStatistics_;
    IBDA_LNBInfo* pBDALNBInfo_;
    IBDA_DigitalDemodulator* pBDADigitalDemodulator_;
    IBDA_DiseqCommand* pDiseqcCommandInterface_;
    IBaseFilter *pfltTuner_;
    IBaseFilter *pfltCapture_;
    IBaseFilter *pfltCaptureEx_;
    IBaseFilter *pfltCaptureEx1_;
};

class property_set_base_t
{
public:
    property_set_base_t() {}
    virtual ~property_set_base_t(){}

    void set_device_info(const wchar_t* tuner_name, const wchar_t* device_path, const wchar_t* dll_path, dvblink::DL_E_TUNER_TYPES tuner_type, const wchar_t* vid, const wchar_t* pid)
    {
        tuner_name_ = tuner_name;
        device_path_ = device_path;
        dll_path_ = dll_path;
        tuner_type_ = tuner_type;
        vid_ = vid;
        pid_ = pid;
    }

    void set_graph_info(property_set_graph_info_t& graph_info)
    {
        graph_info_ = graph_info;
    }

    const std::wstring& get_vid(){return vid_;}
    const std::wstring& get_pid(){return pid_;}

    virtual HRESULT property_set_init();
    virtual HRESULT property_set_deinit();
    virtual HRESULT property_set_after_graph_start();
    virtual HRESULT property_set_send_diseqc(unsigned char *cmd, int len);
    virtual HRESULT property_set_send_toneburst(int type);
    virtual HRESULT property_set_set_22khz(bool active);
    virtual HRESULT property_set_set_modulation_params(dvblink::PTransponderInfo Tp);
    virtual HRESULT property_set_set_lnb_power(dvblink::DL_E_LNB_POWER_TYPES lnb_power);
    virtual HRESULT property_set_set_tuner(dvblink::PTransponderInfo Tp, int diseqc, int toneburst);
    virtual HRESULT property_set_get_tuner_state(dvblink::PSignalInfo stats);
    virtual BinaryConvolutionCodeRate property_set_get_fec(int fec);
    virtual ModulationType property_set_get_dvbs_modulation(int mod);
    virtual ULONG property_set_get_input_range_from_diseqc(int diseqc_port);
    virtual HRESULT property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng);
    virtual HRESULT property_set_add_pid(int pid);
    virtual HRESULT property_set_del_pid(int pid);
protected:
    std::wstring tuner_name_;
    std::wstring device_path_;
    std::wstring dll_path_;
    dvblink::DL_E_TUNER_TYPES tuner_type_;
    property_set_graph_info_t graph_info_;
    std::wstring vid_;
    std::wstring pid_;
};

#endif
