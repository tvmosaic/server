/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

class GrabberCallback;
interface ISampleGrabber;

class sample_grabber_man_t
{
public:
    typedef void (*ts_callback_f)(unsigned char* data, int len, void* param);

public:
    sample_grabber_man_t(ts_callback_f cb, void* param);
    ~sample_grabber_man_t();

    IBaseFilter* get_filter(){return psgfilter_;}

protected:
    IBaseFilter *psgfilter_;
	ISampleGrabber* sample_grabber_;
    ts_callback_f callback_;
    void* param_;
    GrabberCallback* gb_;

    static int callback_SampleGrabber(double Time, BYTE *pBuffer, long BufferLen, LPVOID data);
};

