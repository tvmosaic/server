/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_strings.h>
#include <dl_logger.h>
#include <dl_utils.h>
#include <drivers/deviceapi.h>
#include "bda_aux.h"
#include "common.h"

namespace dvblink {

bda_type_storage_t* bda_type_storage_t::instance_ = NULL;

using namespace engine;
using namespace logging;

bda_aux_t::bda_aux_t() :
	aux_module_t()
{
	
}

bda_aux_t::~bda_aux_t()
{
	
}

bool bda_aux_t::init()
{
	log_info(L"bda_aux_t::init");
    bda_type_storage_t::init();
	
	return true;
}

void bda_aux_t::term()
{
	log_info(L"bda_aux_t::term");
    bda_type_storage_t::term();
}
//////////////////////////////////////////////////

void bda_type_storage_t::init()
{
    if (instance_ == NULL)
        instance_ = new bda_type_storage_t();
}

void bda_type_storage_t::term()
{
    if (instance_ != NULL)
    {
        delete instance_;
        instance_ = NULL;
    }
}

bool bda_type_storage_t::get_device_descriptor(const std::string& device_path, bda_device_descriptor_t& descriptor)
{
    bool ret_val = get_device_descriptor_int(device_path, descriptor);
    if (!ret_val)
    {
        //update descriptors and try again
        update_descriptors();

        ret_val = get_device_descriptor_int(device_path, descriptor);
    }

    return ret_val;
}

bool bda_type_storage_t::get_device_descriptor_int(const std::string& device_path, bda_device_descriptor_t& descriptor)
{
    bool ret_val = false;

    if (instance_ != NULL)
    {
        boost::unique_lock<boost::mutex> lock(instance_->lock_);
        if (instance_->bda_device_descriptor_map_.find(device_path) != instance_->bda_device_descriptor_map_.end())
        {
            descriptor = instance_->bda_device_descriptor_map_[device_path];
            ret_val = true;
        }
    }

    return ret_val;
}

void bda_type_storage_t::update_descriptors()
{
    boost::thread* t = new boost::thread(boost::bind(&bda_type_storage_t::update_descriptors_thread));
    t->join();
    delete t;
}

void bda_type_storage_t::update_descriptors_thread()
{
    if (instance_ == NULL)
        return;

	CDLCoInitHolder coinit(COINIT_MULTITHREADED);

    boost::unique_lock<boost::mutex> lock(instance_->lock_);

    TDevAPIDevListEx tDL;
    memset(&tDL, 0, sizeof(tDL));
    DeviceGetListEx_impl(&tDL);

    std::map<std::wstring, tuner_description_t> devices_to_update;

    for (int i=0; i<tDL.Count; i++)
    {
        std::string device_id = string_cast<EC_UTF8>(tDL.Devices[i].szDevicePathW);
        if (instance_->bda_device_descriptor_map_.find(device_id) == instance_->bda_device_descriptor_map_.end())
            devices_to_update[tDL.Devices[i].szDevicePathW] = tuner_description_t();
    }

    if (devices_to_update.size() > 0)
    {
        get_tuner_type(devices_to_update);

        std::map<std::wstring, tuner_description_t>::iterator it = devices_to_update.begin();
        while (it != devices_to_update.end())
        {
            std::string device_id = string_cast<EC_UTF8>(it->first);
            bda_device_descriptor_t d;
            d.name_ = it->second.name_;
            d.types_ = it->second.types_;
            instance_->bda_device_descriptor_map_[device_id] = d;

            ++it;
        }
    }
}


} //dvblink
