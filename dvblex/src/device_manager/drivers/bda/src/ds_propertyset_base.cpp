/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <boost/algorithm/string.hpp>

#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <tuner.h>
#include <commctrl.h>
#include <stdarg.h>
#include <bdatif.h>

#include <drivers/deviceapi.h>
#include "ds_tunerequest.h"
#include "ds_propertyset_base.h"

#include <dl_logger.h>

using namespace dvblink::logging;

HRESULT property_set_base_t::property_set_init()
{
	return E_NOINTERFACE;
}

HRESULT property_set_base_t::property_set_deinit()
{
	return E_NOINTERFACE;
}

HRESULT property_set_base_t::property_set_after_graph_start()
{
	return E_NOINTERFACE;
}

HRESULT property_set_base_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
	return E_NOINTERFACE;
}

HRESULT property_set_base_t::property_set_send_toneburst(int type)
{
	return E_NOINTERFACE;
}

HRESULT property_set_base_t::property_set_set_22khz(bool active)
{
	return E_NOINTERFACE;
}

HRESULT property_set_base_t::property_set_set_modulation_params(dvblink::PTransponderInfo Tp)
{
	return E_NOINTERFACE;
}

HRESULT property_set_base_t::property_set_set_lnb_power(dvblink::DL_E_LNB_POWER_TYPES lnb_power)
{
	return E_NOINTERFACE;
}

HRESULT property_set_base_t::property_set_set_tuner(dvblink::PTransponderInfo Tp, int diseqc, int toneburst)
{
	return E_NOINTERFACE;
}

HRESULT property_set_base_t::property_set_get_tuner_state(dvblink::PSignalInfo stats)
{
	return E_NOINTERFACE;
}

BinaryConvolutionCodeRate property_set_base_t::property_set_get_fec(int fec) 
{
	return bda_standard_get_fec(fec);
}


ModulationType property_set_base_t::property_set_get_dvbs_modulation(int mod)
{
	return bda_standard_get_dvbs_modulation(mod);
}

ULONG property_set_base_t::property_set_get_input_range_from_diseqc(int diseqc_port)
{
	return get_standard_input_range_from_diseqc(diseqc_port);
}

HRESULT property_set_base_t::property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng) 
{
	return E_NOINTERFACE;
}

HRESULT property_set_base_t::property_set_add_pid(int pid)
{
	return E_NOINTERFACE;
/*
	IKsPropertySet *ppropsetTunerPin = NULL;
	DWORD TypeSupport=0;
	
	if (pfltCapture) 
    {
		// get the property sets on the Capture filter
		hr = get_filter_property_set(pfltCapture, &ppropsetTunerPin);
		if (hr == S_OK)
        {
			hr = ppropsetTunerPin->QuerySupported(KSPROPSETID_BdaPIDFilter,
								KSPROPERTY_BDA_PIDFILTER_MAP_PIDS, 
								&TypeSupport);
            if (FAILED(hr))
            {
                ppropsetTunerPin->Release();
                ppropsetTunerPin = NULL;
            }
		}
	}

	if (FAILED(hr))
    {
		// get the property sets on the tuner filter
		hr = get_filter_property_set(pfltTuner, &ppropsetTunerPin);
		if (hr == S_OK)
        {
		    hr = ppropsetTunerPin->QuerySupported(KSPROPSETID_BdaPIDFilter,
							    KSPROPERTY_BDA_PIDFILTER_MAP_PIDS, 
							    &TypeSupport);	
            if (FAILED(hr))
            {
                ppropsetTunerPin->Release();
                ppropsetTunerPin = NULL;
            }
        }
	}

    if (ppropsetTunerPin != NULL && (TypeSupport & KSPROPERTY_SUPPORT_SET) == KSPROPERTY_SUPPORT_SET)
    {
	    BDA_PID_MAP pidmap;
	    KSP_NODE instance;

	    pidmap.MediaSampleContent = MEDIA_TRANSPORT_PACKET;
	    pidmap.ulcPIDs = 1;
	    pidmap.aulPIDs[0] = pid;
    	
	    hr = ppropsetTunerPin->Set(KSPROPSETID_BdaPIDFilter, 
                        				    KSPROPERTY_BDA_PIDFILTER_MAP_PIDS, 
					      			      &instance,sizeof(instance),
					        			    &pidmap, sizeof(pidmap));
    }
    if (ppropsetTunerPin != NULL)
	    ppropsetTunerPin->Release();
	return hr;
	*/
}


HRESULT property_set_base_t::property_set_del_pid(int pid)
{
    return E_NOINTERFACE;
/*
	IKsPropertySet *ppropsetTunerPin = NULL;
	DWORD TypeSupport=0;

	if (pfltCapture) 
    {
		// get the property sets on the Capture filter
		hr = get_filter_property_set(pfltCapture, &ppropsetTunerPin);
        if (hr == S_OK)
        {
			hr = ppropsetTunerPin->QuerySupported(KSPROPSETID_BdaPIDFilter,
								KSPROPERTY_BDA_PIDFILTER_UNMAP_PIDS, 
								&TypeSupport);	
            if (FAILED(hr))
            {
                ppropsetTunerPin->Release();
                ppropsetTunerPin = NULL;
            }
		}
	}

	if (FAILED(hr))
    {

		// get the property sets on the tuner filter
		hr = get_filter_property_set(pfltTuner, &ppropsetTunerPin);
        if (hr == S_OK)
        {
		    hr = ppropsetTunerPin->QuerySupported(KSPROPSETID_BdaPIDFilter,
							    KSPROPERTY_BDA_PIDFILTER_UNMAP_PIDS, 
							    &TypeSupport);	
            if (FAILED(hr))
            {
                ppropsetTunerPin->Release();
                ppropsetTunerPin = NULL;
            }
        }
	}

    if (ppropsetTunerPin != NULL && (TypeSupport & KSPROPERTY_SUPPORT_SET) == KSPROPERTY_SUPPORT_SET)
    {
	    BDA_PID_UNMAP pidmap;
	    KSP_NODE instance;

	    pidmap.ulcPIDs = 1;
	    pidmap.aulPIDs[0] = pid;
    	
	    hr = ppropsetTunerPin->Set(KSPROPSETID_BdaPIDFilter, 
                        				    KSPROPERTY_BDA_PIDFILTER_UNMAP_PIDS, 
					      			      &instance,sizeof(instance),
					        			    &pidmap, sizeof(pidmap));
    }
    if (ppropsetTunerPin != NULL)
	    ppropsetTunerPin->Release();
	return hr;
	*/
}



