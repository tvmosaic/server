/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <string>
#include <map>

#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <tuner.h>
#include <drivers/deviceapi.h>

int find_device_by_path(dvblink::TDevAPIDevListEx* device_list, const wchar_t* device_path);

int DeviceGetListEx_impl(dvblink::PDevAPIDevListEx pDL);

bool parse_tuner_identity_from_path(const wchar_t* path, std::wstring& vid, std::wstring& pid, std::wstring& subsys, std::wstring& device_instance_path);

void diseqc2str(unsigned char *cmd, int len, wchar_t *str);

struct tuner_description_t
{
    tuner_description_t() :
        types_(dvblink::TUNERTYPE_UNKNOWN)
    {}

    std::string name_;
    unsigned long types_;
};

void get_tuner_type(std::map<std::wstring, tuner_description_t>& tuners);

HRESULT get_filter_property_set(IBaseFilter *pfilter, IKsPropertySet **pkspropertyset);

// -1  = ALL
//  0  = PINDIR_INPUT 
//  1  = PINDIR_OUTPUT
HRESULT get_pin_property_set(IBaseFilter *pfilter, int sdir, IKsPropertySet **pkspropertyset);
