/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>

#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <tuner.h>
#include <commctrl.h>
#include <stdarg.h>
#include <bdatif.h>

#include <boost/algorithm/string.hpp>
#include <drivers/deviceapi.h>
#include <dl_dshow_enum.h>
#include <dl_dshow_helpers.h>
#include "ds_graph.h"
#include "ds_topology.h"
#include "ds_propertyset.h"
#include "common.h"
#include <dl_logger.h>

using namespace dvblink::logging;
using namespace dvblink::engine;


HRESULT create_graph(IGraphBuilder **pfltgraph, IMediaControl **pmediactrl, DWORD *dwrot)
{	
	HRESULT hr;

	// Create the graph builder object
    if (FAILED (hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC,
		IID_IGraphBuilder, (LPVOID *)pfltgraph)))
	{
		log_error(L"create_graph(): Couldn't CoCreate IGraphBuilder");
		return hr;
	}


	// Query IMediaControl interface
	if (FAILED(hr = (*pfltgraph)->QueryInterface(IID_IMediaControl,(LPVOID *)pmediactrl)))
	{
		log_error(L"create_graph(): Couldn't get MediaControl interface");
		*pfltgraph = NULL;
		return hr;
	}

	if (dwrot) 
		AddGraphToRot(*pfltgraph, dwrot);
	
	return S_OK;
}

HRESULT free_graph(IGraphBuilder **pfltgraph, IMediaControl **pmediactrl, DWORD *dwrot)
{
	if (*pfltgraph) {
		(*pfltgraph)->Release();
		*pfltgraph = NULL;
	}

	if (*pmediactrl) {
		(*pmediactrl)->Release();
		*pmediactrl = NULL;
	}

	if (dwrot) 
		RemoveGraphFromRot(*dwrot);

	return S_OK;
}

HRESULT start_graph(IMediaControl* pmediactrl, DWORD timeout)
{
	HRESULT hr = E_FAIL;

	if (pmediactrl != NULL)
	{
		hr = pmediactrl->Run();
		if (hr == S_OK)
		{
			//wait until running state
			DWORD elapsed_time = 0;
			FILTER_STATE fs;
			hr = pmediactrl->GetState(1, (OAFilterState*)&fs);
			while ((hr != S_OK || fs != State_Running) && elapsed_time < timeout)
			{
				Sleep(50);
				elapsed_time += 50;
				hr = pmediactrl->GetState(1, (OAFilterState*)&fs);
			}
			log_info(L"start_graph: hr=%d, state=%d") % hr % fs;
			hr = (hr == S_OK && fs == State_Running) ? S_OK : E_FAIL;
		}
		if (hr != S_OK)
			log_warning(L"Cannot start graph, %d") % hr;
	}
	return hr;
}

HRESULT stop_graph(IMediaControl* pmediactrl, DWORD timeout)
{
	HRESULT hr = E_FAIL;

	if (pmediactrl != NULL)
	{
		hr = pmediactrl->Stop();
		if (hr == S_OK)
		{
			//wait until running state
			DWORD elapsed_time = 0;
			FILTER_STATE fs;
			hr = pmediactrl->GetState(1, (OAFilterState*)&fs);
			while ((hr != S_OK || fs != State_Stopped) && elapsed_time < timeout)
			{
				Sleep(50);
				elapsed_time += 50;
				hr = pmediactrl->GetState(1, (OAFilterState*)&fs);
			}
			log_info(L"stop_graph: hr=%d, state=%d") % hr % fs;
			hr = (hr == S_OK && fs == State_Stopped) ? S_OK : E_FAIL;
		}
		if (hr != S_OK)
			log_warning(L"Cannot stop graph, %d") % hr;
	}
	return hr;
}

HRESULT add_filter(IGraphBuilder *pfltgraph, IBaseFilter *pfilter, WCHAR *filter_name)
{
	HRESULT hr;		
	WCHAR *wname;

	if (!filter_name) {		
		CLSID clsid;		
		pfilter->GetClassID(&clsid);
		WCHAR wsCLSID[256];
		get_filter_name(&clsid, &wsCLSID[0]);
		wname = &wsCLSID[0];
	}	
	else
		wname = filter_name;
		
    hr = pfltgraph->AddFilter(pfilter, wname);
	if (FAILED(hr))
	{
		log_error(L"Cannot add the filter (%s) to the graph") % wname;
	} else
	{
		log_info(L"add_filter (%s)") % wname;
	}
	return hr;
}

HRESULT remove_filter(IGraphBuilder *pfltgraph, IBaseFilter *pfilter)
{
	HRESULT hr;

	if (FAILED(hr=pfltgraph->RemoveFilter(pfilter)))
	{
		log_error(L"Cannot remove the filter from the graph");		
	}
	return hr;
}                                                                        


HRESULT create_tuner(IGraphBuilder *pfltgraph, const std::wstring& device_path, IBaseFilter **pfilter, wchar_t *wstunername, int wstunername_len,
                     wchar_t *wsdevicepath, int wsdevicepath_len)
{	
	int i = 0;
	*pfilter = NULL;

	TDSEnum *penumBDATuners = enum_create(KSCATEGORY_BDA_NETWORK_TUNER);
	if(!penumBDATuners) return E_FAIL;
	
	while (enum_next(penumBDATuners) == S_OK) {
		if (SUCCEEDED(enum_get_name(penumBDATuners)))
        {
            if (boost::iequals(penumBDATuners->szDevicePathW, device_path))
            {
    		    if (enum_get_filter(penumBDATuners, pfilter) == S_OK) 
                {
			        log_info(L"BDA Device Name: [%s]") % penumBDATuners->szNameW;
			        wcscpy_s(wstunername, wstunername_len, penumBDATuners->szNameW);				
                    wcscpy_s(wsdevicepath, wsdevicepath_len, penumBDATuners->szDevicePathW);				
			        HRESULT hr = add_filter(pfltgraph, *pfilter, &penumBDATuners->szNameW[0]);
			        enum_free(penumBDATuners);											
			        return hr;
                }
		    }
        }
		i++;
	}

	enum_free(penumBDATuners);
	return E_FAIL;
}

HRESULT create_preferred_capture(const wchar_t* wsDevicePath, IGraphBuilder *pfltgraph, IBaseFilter *pftuner, IBaseFilter **pfilter)
{		
	HRESULT hr = E_FAIL;
	*pfilter = NULL;	

	log_info(L"Looking for preferred capture for tuner %s") % wsDevicePath;

	TDSEnum *penumBDACapture = enum_create(KSCATEGORY_BDA_RECEIVER_COMPONENT);
	if(!penumBDACapture)
        return E_FAIL;

    const wchar_t* ptr = wcsrchr(wsDevicePath, L'#');
    if (ptr != NULL)
    {
        int cmp_num = ptr - wsDevicePath;

	    while (enum_next(penumBDACapture) == S_OK)
        {
		    enum_get_name(penumBDACapture);
		
            if (_wcsnicmp(wsDevicePath, penumBDACapture->szDevicePathW, cmp_num) == 0)
            {
	            log_info(L"Found preferred capture: %s") % penumBDACapture->szDevicePathW;
                //found preferred filter
		        if (enum_get_filter(penumBDACapture, pfilter) != S_OK)
	                *pfilter = NULL;	//filter creation failed. break with error
		            
	            break;
            }
        }
    }

    if (*pfilter != NULL)
    {
        //try to add this filter to graph and connect it
		hr = add_filter(pfltgraph, *pfilter, penumBDACapture->szNameW);
        if (hr == S_OK)
        {
		    try 
		    {
			    if (ConnectFilters(pfltgraph, pftuner, *pfilter))
                    hr = S_OK;
                else
                    hr = E_FAIL;
		    }catch(...)
		    {
			    hr = E_FAIL;
		    }

		    if (hr != S_OK)
		    {
		        remove_filter(pfltgraph, *pfilter);
		        (*pfilter)->Release();
		        *pfilter = NULL;
		    }
        } else
        {
			(*pfilter)->Release();
			*pfilter = NULL;
        }
    }

	enum_free(penumBDACapture);

    return (*pfilter == NULL) ? E_FAIL : S_OK;
}

HRESULT create_capture(IGraphBuilder *pfltgraph, IBaseFilter *pftuner, IBaseFilter **pfilter)
{		
	HRESULT hr = E_FAIL;
	*pfilter = NULL;	

	TDSEnum *penumBDACapture = enum_create(KSCATEGORY_BDA_RECEIVER_COMPONENT);
	if(!penumBDACapture)
        return E_FAIL;

	while (enum_next(penumBDACapture) == S_OK)
    {
		if (enum_get_filter(penumBDACapture, pfilter) == S_OK) 
		{
			enum_get_name(penumBDACapture);

			if (wcsstr(penumBDACapture->szNameW, L"DVBLink") != NULL ||
				wcsstr(penumBDACapture->szNameW, L"Bridge") != NULL)
			{
				(*pfilter)->Release();
				*pfilter = NULL;
				continue;
			}

			hr = add_filter(pfltgraph, *pfilter, penumBDACapture->szNameW);
            if (FAILED(hr))
            {
				(*pfilter)->Release();
				*pfilter = NULL;
                continue;
            }
			
			__try {
				if (ConnectFilters(pfltgraph, pftuner, *pfilter))
                    hr = S_OK;
                else
                    hr = E_FAIL;
			}__except(EXCEPTION_EXECUTE_HANDLER)
			{
				hr = E_FAIL;
			}
            //if filter was found and connected - break
			if (hr == S_OK)
                break;

			remove_filter(pfltgraph, *pfilter);
			(*pfilter)->Release();
			*pfilter = NULL;
		}
    }
	enum_free(penumBDACapture);
    return (*pfilter == NULL) ? E_FAIL : S_OK;
}

HRESULT create_network_provider(IGraphBuilder *pfltgraph, int tuner_type, IBaseFilter **ppfilter, ITuner **ptuner, bool& bUniversalProvider)
{
	HRESULT hr = E_FAIL;

	switch (tuner_type) {
		case dvblink::TUNERTYPE_DVBS:	
			hr = CoCreateInstance(CLSID_DVBSNetworkProvider, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)ppfilter);
			bUniversalProvider = false;
			break;
		case dvblink::TUNERTYPE_DVBC:
			hr = CoCreateInstance(CLSID_DVBCNetworkProvider, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)ppfilter);
			bUniversalProvider = false;
			break;
		case dvblink::TUNERTYPE_DVBT:
			if (bUniversalProvider)
			{
				hr = CoCreateInstance(CLSID_NetworkProvider, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)ppfilter);
				bUniversalProvider = true;
			}
			if (!bUniversalProvider || hr != S_OK) //if generic network provider is not present - try the legacy one
			{
			    hr = CoCreateInstance(CLSID_DVBTNetworkProvider, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)ppfilter);
				bUniversalProvider = false;
			}
			break;
		case dvblink::TUNERTYPE_ATSC:
			if (bUniversalProvider)
			{
				hr = CoCreateInstance(CLSID_NetworkProvider, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)ppfilter);
				bUniversalProvider = true;
			}
			if (!bUniversalProvider || hr != S_OK) //if generic network provider is not present - try the legacy one
			{
				hr = CoCreateInstance(CLSID_ATSCNetworkProvider, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)ppfilter);
				bUniversalProvider = false;
			}
			break;
		case dvblink::TUNERTYPE_CLEARQAM:
			bUniversalProvider = true;
		    hr = CoCreateInstance(CLSID_NetworkProvider, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)ppfilter);
			break;
	}

	if(FAILED (hr))
	{	
		log_error(L"cannot create network provider");
		return hr;
	}
	
	if(FAILED (add_filter(pfltgraph, *ppfilter, NULL)))
	{	
		log_error(L"cannot add network provider filter to the graph");
		return hr;
	}

	hr = (*ppfilter)->QueryInterface(__uuidof(ITuner), (void**) ptuner);
	if(FAILED (hr))
	{		
		log_error(L"pNetworkProvider->QueryInterface(ITuner)");
		return hr;
	}

	return hr;
}


