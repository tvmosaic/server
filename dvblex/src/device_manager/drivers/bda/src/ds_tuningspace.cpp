/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>

#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <tuner.h>
#include <commctrl.h>
#include <stdarg.h>
#include <dl_dshow_enum.h>
#include <dl_dshow_helpers.h>
#include <drivers/deviceapi.h>
#include <dl_logger.h>

using namespace dvblink::logging;
using namespace dvblink::engine;

HRESULT load_tuning_space(int tuner_type, ITuningSpace **ppTuningSpace)
{
	*ppTuningSpace = NULL;

	ITuningSpaceContainer *pTuningSpaceContainer = NULL;		
	HRESULT hr = CoCreateInstance(__uuidof(SystemTuningSpaces)/*CLSID_SystemTuningSpaces*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(ITuningSpaceContainer), (void**)&pTuningSpaceContainer);
    if (hr == S_OK && pTuningSpaceContainer != NULL)
    {
	    // Get the enumerator for the collection.
	    IEnumTuningSpaces *pTuningSpaceEnum = NULL;
	    hr = pTuningSpaceContainer->get_EnumTuningSpaces(&pTuningSpaceEnum);
        if (hr == S_OK && pTuningSpaceEnum != NULL)
        {
	        // Loop through the collection.
	        int nTuningSpaces = 0;
	        ITuningSpace *pTuningSpace = NULL;
	        while (*ppTuningSpace == NULL && S_OK == pTuningSpaceEnum->Next(1, &pTuningSpace, NULL))
	        {
		        nTuningSpaces++;

		        // get network type
		        BSTR bstrNetworkType = NULL;	
		        hr = pTuningSpace->get_NetworkType(&bstrNetworkType);

		        // get tuning-space name			
		        BSTR bstrTuningSpaceName = NULL;
		        hr = pTuningSpace->get_UniqueName(&bstrTuningSpaceName);
		        if(FAILED(hr)) 
			        log_error(L"pTuningSpace->get_UniqueName()");

                if (bstrTuningSpaceName != NULL && bstrNetworkType != NULL)
                {
		            log_info(L"TuningSpace Name: '%s'  NetworkType: %s") % bstrTuningSpaceName % bstrNetworkType;
            		
					switch (tuner_type)
					{
						case dvblink::TUNERTYPE_DVBS:
						{
							//if(wcscmp(bstrNetworkType, L"{FA4B375A-45B4-4D45-8440-263957B11623}") == 0) {				
							if(wcscmp(bstrTuningSpaceName, L"DD2 DVB-S") == 0) 
							{
								*ppTuningSpace = pTuningSpace;
								(*ppTuningSpace)->AddRef();
							}			
						}
						break;

					case dvblink::TUNERTYPE_DVBC:
						{
							//if(wcscmp(bstrNetworkType, L"{DC0C0FE7-0485-4266-B93F-68FBF80ED834}") == 0) {
							if(wcscmp(bstrTuningSpaceName, L"DD2 DVB-C") == 0) 
							{
								*ppTuningSpace = pTuningSpace;
								(*ppTuningSpace)->AddRef();
							}
						}
						break;
            		
					case dvblink::TUNERTYPE_DVBT:
						{
							//if(wcscmp(bstrNetworkType, L"{216C62DF-6D7F-4E9A-8571-05F14EDB766A}") == 0) {
							if(wcscmp(bstrTuningSpaceName, L"DD2 DVB-T") == 0) 
							{
								*ppTuningSpace = pTuningSpace;
								(*ppTuningSpace)->AddRef();
							}			
						}
						break;
					case dvblink::TUNERTYPE_ATSC:
						{
							if(wcsstr(bstrTuningSpaceName, L"ATSC") != NULL) 
							{
								*ppTuningSpace = pTuningSpace;
								(*ppTuningSpace)->AddRef();
							}			
						}
						break;
					case dvblink::TUNERTYPE_CLEARQAM:
						{
							if(wcscmp(bstrTuningSpaceName, L"Digital Cable") == 0 || 
								wcscmp(bstrTuningSpaceName, L"ClearQAM") == 0) 
							{
								*ppTuningSpace = pTuningSpace;
								(*ppTuningSpace)->AddRef();
							}			
						}
						break;
					}
                }

	            pTuningSpace->Release();
	            pTuningSpace = NULL;

                if (bstrNetworkType != NULL)
	                SysFreeString(bstrNetworkType);
                if (bstrTuningSpaceName != NULL)
	                SysFreeString(bstrTuningSpaceName);
	        }
        	
	        log_info(L"Enumeration found %d tuning spaces") % nTuningSpaces;
            pTuningSpaceEnum -> Release();
        }
        else
        {
		    log_error(L"pTuningSpaceContainer->get_EnumTuningSpaces()");
        }
    		
	    pTuningSpaceContainer->Release();
	    pTuningSpaceContainer = NULL;
    }
    else
    {
		log_error(L"CoCreateInstance(CLSID_SystemTuningSpaces()");
    }

	if(*ppTuningSpace != NULL) 
    {
		log_info(L"Found TuningSpace");
		return S_OK;
	}

	log_error(L"Couldn't find TuningSpace !");
	return E_FAIL;
}

HRESULT init_tuning_space(ITuner *ptuner, int tuner_type, ITuningSpace *pTuningSpace)
{
    HRESULT hr = E_FAIL;
	hr = ptuner->put_TuningSpace(pTuningSpace);
	return hr;
    if (hr == S_OK)
    {
	    // Have to init with a tune request or it will not connect
	    // Some tuner filters need a tuning request to expose their output pin
        ITuneRequest *pTuneRequest = NULL;
        hr = pTuningSpace->CreateTuneRequest(&pTuneRequest);
        if (hr == S_OK)
        {
	        // create a locator, use it as default locator and for the initial tune request
	        IDVBSLocator *pDVBSLocator = NULL;
	        IDVBCLocator *pDVBCLocator = NULL;
	        IDVBTLocator *pDVBTLocator = NULL;
	        IATSCLocator *pATSCLocator = NULL;

	        switch(tuner_type) 
            {
		        case dvblink::TUNERTYPE_DVBS:
			        hr = CoCreateInstance(__uuidof(DVBSLocator)/*CLSID_DVBSLocator*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(IDVBSLocator), (void**)&pDVBSLocator);		
			        if SUCCEEDED(hr) {
				        pDVBSLocator->put_CarrierFrequency(-1);
				        pDVBSLocator->put_SymbolRate(-1);
				        pDVBSLocator->put_InnerFEC(BDA_FEC_METHOD_NOT_SET);            
				        pDVBSLocator->put_InnerFECRate(BDA_BCC_RATE_NOT_SET);      
				        pDVBSLocator->put_OuterFEC(BDA_FEC_METHOD_NOT_SET);        
				        pDVBSLocator->put_OuterFECRate(BDA_BCC_RATE_NOT_SET);      
				        pDVBSLocator->put_Modulation(BDA_MOD_NOT_SET);             				
        				
				        hr = pTuneRequest->put_Locator(pDVBSLocator);
			        }
			        break;
		        case dvblink::TUNERTYPE_DVBC:
			        hr = CoCreateInstance(__uuidof(DVBCLocator)/*CLSID_DVBCLocator*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(IDVBCLocator), (void**)&pDVBCLocator);
			        if SUCCEEDED(hr) {
				        pDVBCLocator->put_CarrierFrequency(-1);
				        pDVBCLocator->put_SymbolRate(-1);
        				
				        hr = pTuneRequest->put_Locator(pDVBCLocator);	
			        }
			        break;	
		        case dvblink::TUNERTYPE_DVBT:
			        hr = CoCreateInstance(__uuidof(DVBTLocator)/*CLSID_DVBTLocator*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(IDVBTLocator), (void**)&pDVBTLocator);
			        if SUCCEEDED(hr) {
				        pDVBTLocator->put_CarrierFrequency(-1);		
        				
				        hr = pTuneRequest->put_Locator(pDVBTLocator);
			        }
			        break;
		        case dvblink::TUNERTYPE_ATSC:
			        hr = CoCreateInstance(__uuidof(ATSCLocator), NULL, CLSCTX_INPROC_SERVER, __uuidof(IATSCLocator), (void**)&pATSCLocator);
			        if SUCCEEDED(hr) 
					{
				        pATSCLocator->put_PhysicalChannel(-1);
				        pATSCLocator->put_TSID(-1);
        				
				        hr = pTuneRequest->put_Locator(pATSCLocator);
			        }
			        break;
	        }

            hr = ptuner->put_TuneRequest(pTuneRequest);
            if (FAILED (hr))
            {
                log_warning(L"Couldn't put_TuneRequest");        
            }

	        if (pDVBSLocator)
		        pDVBSLocator->Release();
	        if (pDVBTLocator)
		        pDVBTLocator->Release();
	        if (pDVBCLocator)
		        pDVBCLocator->Release(); 
	        if (pATSCLocator)
		        pATSCLocator->Release(); 

            if (pTuneRequest)
		        pTuneRequest->Release();
        }
    }

	return hr;
}

HRESULT create_dvbs_tuning_space(ITuningSpace **ppTuningSpace)
{
	HRESULT hr = E_FAIL;
    *ppTuningSpace = NULL;

	ITuningSpaceContainer *pTuningSpaceContainer = NULL;		
	hr = CoCreateInstance(__uuidof(SystemTuningSpaces)/*CLSID_SystemTuningSpaces*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(ITuningSpaceContainer), (void**)&pTuningSpaceContainer);
    if (hr == S_OK)
    {
	    // create DVB-S tuning space
	    IDVBSTuningSpace  *pDVBSTuningSpace = NULL;		
	    hr = CoCreateInstance(__uuidof(DVBSTuningSpace)/*CLSID_DVBSTuningSpace*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(IDVBSTuningSpace), (void**)&pDVBSTuningSpace);
        if (hr == S_OK)
        {
	        // set unique name
	        BSTR bstrTuningSpaceName = SysAllocString(L"DD2 DVB-S");;
	        pDVBSTuningSpace->put_UniqueName(bstrTuningSpaceName);
	        SysFreeString(bstrTuningSpaceName);

	        // set friendly name  (shown as Description in the registry)
	        BSTR bstrFriendlyName = SysAllocString(L"DD2 DVB-S");
	        hr = pDVBSTuningSpace->put_FriendlyName(bstrFriendlyName); 
	        SysFreeString(bstrFriendlyName);

	        // set system type
	        pDVBSTuningSpace->put_SystemType(DVB_Satellite);

	        // set network type
	        hr = pDVBSTuningSpace->put__NetworkType(CLSID_DVBSNetworkProvider);
            if (hr == S_OK)
            {
	            // set the Frequency mapping						
	            BSTR bstrFreqMapping = SysAllocString(L"-1");
	            hr = pDVBSTuningSpace->put_FrequencyMapping(bstrFreqMapping); // not used
	            SysFreeString(bstrFreqMapping);

	            // Set default locator
	            IDVBSLocator *pDVBSLocator = NULL;
	            hr = CoCreateInstance(__uuidof(DVBSLocator)/*CLSID_DVBSLocator*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(IDVBSLocator), (void**)&pDVBSLocator);		
	            if SUCCEEDED(hr) 
                {
		            pDVBSLocator->put_CarrierFrequency(-1);
		            pDVBSLocator->put_SymbolRate(-1);
		            pDVBSLocator->put_InnerFEC(BDA_FEC_METHOD_NOT_SET);            
		            pDVBSLocator->put_InnerFECRate(BDA_BCC_RATE_NOT_SET);      
		            pDVBSLocator->put_OuterFEC(BDA_FEC_METHOD_NOT_SET);        
		            pDVBSLocator->put_OuterFECRate(BDA_BCC_RATE_NOT_SET);      
		            pDVBSLocator->put_Modulation(BDA_MOD_NOT_SET);             				

		            pDVBSTuningSpace->put_DefaultLocator(pDVBSLocator);				

	                // get the number of tuning spaces that have been created
	                LONG lCount = 0;
	                hr = pTuningSpaceContainer->get_Count(&lCount); 			

	                // add the tuning space to the registry
	                VARIANT varID;				
	                VariantInit(&varID);			  
	                lCount++;
	                varID.intVal = lCount;
	                log_info(L"Tuning Space ID:%d") % lCount;
	                //varID.vt = VT_INT;				
	                hr = pTuningSpaceContainer->Add(pDVBSTuningSpace, &varID);
	                if FAILED(hr) 
		                log_warning(L"pTuningSpaceContainer->Add(pDVBTuningSpace...)      Already added !");
	                VariantClear(&varID);  

	                pDVBSLocator->Release();

                    *ppTuningSpace = pDVBSTuningSpace;
                    (*ppTuningSpace)->AddRef();
                }
            }
            else
            {
		        log_error(L"put_NetworkType failed\n");
            }
	        pDVBSTuningSpace->Release();
        }
        else
        {
		    log_error(L"CoCreateInstance(CLSID_DVBSTuningSpace...)");
        }

	    pTuningSpaceContainer->Release();
	    pTuningSpaceContainer = NULL;
    }
    else
    {
		log_error(L"CoCreateInstance(CLSID_SystemTuningSpaces()");
    }
    return *ppTuningSpace == NULL ? E_FAIL : S_OK;
}

HRESULT create_dvbc_tuning_space(ITuningSpace **ppTuningSpace)
{
	HRESULT hr = E_FAIL;
    *ppTuningSpace = NULL;

	ITuningSpaceContainer *pTuningSpaceContainer = NULL;		
	hr = CoCreateInstance(__uuidof(SystemTuningSpaces)/*CLSID_SystemTuningSpaces*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(ITuningSpaceContainer), (void**)&pTuningSpaceContainer);
    if (hr == S_OK)
    {
	    // create DVB-C tuning space			
	    IDVBTuningSpace  *pDVBCTuningSpace = NULL;		
	    hr = CoCreateInstance(__uuidof(DVBTuningSpace)/*CLSID_DVBTuningSpace*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(IDVBTuningSpace), (void**)&pDVBCTuningSpace);
        if (hr == S_OK)
        {
	        // set unique name
	        BSTR bstrTuningSpaceName = SysAllocString(L"DD2 DVB-C");
	        pDVBCTuningSpace->put_UniqueName(bstrTuningSpaceName);
	        SysFreeString(bstrTuningSpaceName);

	        // set friendly name  (shown as Description in the registry)
	        BSTR bstrFriendlyName = SysAllocString(L"DD2 DVB-C");
	        hr = pDVBCTuningSpace->put_FriendlyName(bstrFriendlyName); 
	        SysFreeString(bstrFriendlyName);

	        // set system type
	        pDVBCTuningSpace->put_SystemType(DVB_Cable);

	        // set network type
	        hr = pDVBCTuningSpace->put__NetworkType(CLSID_DVBCNetworkProvider);
            if (hr == S_OK)
            {
	            // set the Frequency mapping						
	            BSTR bstrFreqMapping = SysAllocString(L"-1");
	            hr = pDVBCTuningSpace->put_FrequencyMapping(bstrFreqMapping); // not used
	            SysFreeString(bstrFreqMapping);

	            // Set default locator
	            IDVBCLocator *pDVBCLocator = NULL;
	            hr = CoCreateInstance(__uuidof(DVBCLocator)/*CLSID_DVBCLocator*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(IDVBCLocator), (void**)&pDVBCLocator);		
	            if SUCCEEDED(hr) 
                {
		            pDVBCLocator->put_CarrierFrequency(-1);
		            pDVBCLocator->put_SymbolRate(-1);
		            pDVBCLocator->put_InnerFEC(BDA_FEC_METHOD_NOT_SET);            
		            pDVBCLocator->put_InnerFECRate(BDA_BCC_RATE_NOT_SET);      
		            pDVBCLocator->put_OuterFEC(BDA_FEC_METHOD_NOT_SET);        
		            pDVBCLocator->put_OuterFECRate(BDA_BCC_RATE_NOT_SET);      
		            pDVBCLocator->put_Modulation(BDA_MOD_NOT_SET);             				

		            pDVBCTuningSpace->put_DefaultLocator(pDVBCLocator);				

	                // get the number of tuning spaces that have been created
	                LONG lCount = 0;
	                hr = pTuningSpaceContainer->get_Count(&lCount); 			

	                // add the tuning space to the registry
	                VARIANT varID;				
	                VariantInit(&varID);			  
	                lCount++;
	                varID.intVal = lCount;
	                log_info(L"Tuning Space ID:%d") % lCount;
	                //varID.vt = VT_INT;				
	                hr = pTuningSpaceContainer->Add(pDVBCTuningSpace, &varID);
	                if (FAILED(hr))
		                log_warning(L"pTuningSpaceContainer->Add(pDVBCuningSpace...)      Already added !");

                    VariantClear(&varID);  

	                pDVBCLocator->Release();

                    *ppTuningSpace = pDVBCTuningSpace;
                    (*ppTuningSpace)->AddRef();
                }
            }
            else
            {
                log_error(L"put__NetworkType has failed");
            }

	        pDVBCTuningSpace->Clone(ppTuningSpace);
	        pDVBCTuningSpace->Release();
        }
        else
        {
		    log_error(L"CoCreateInstance(CLSID_DVBCTuningSpace...)");
        }

	    pTuningSpaceContainer->Release();
	    pTuningSpaceContainer = NULL;
    }
    else
    {
		log_error(L"CoCreateInstance(CLSID_SystemTuningSpaces()");
    }
    return *ppTuningSpace == NULL ? E_FAIL : S_OK;
}

HRESULT create_dvbt_tuning_space(ITuningSpace **ppTuningSpace)
{
	HRESULT hr = E_FAIL;
    *ppTuningSpace = NULL;

	ITuningSpaceContainer *pTuningSpaceContainer = NULL;		
	hr = CoCreateInstance(__uuidof(SystemTuningSpaces)/*CLSID_SystemTuningSpaces*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(ITuningSpaceContainer), (void**)&pTuningSpaceContainer);
    if (hr == S_OK)
    {
	    // create DVB-T tuning space			
	    IDVBTuningSpace  *pDVBTTuningSpace = NULL;		
	    hr = CoCreateInstance(__uuidof(DVBTuningSpace)/*CLSID_DVBTuningSpace*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(IDVBTuningSpace), (void**)&pDVBTTuningSpace);
        if (hr == S_OK)
        {
	        // set unique name
	        BSTR bstrTuningSpaceName = SysAllocString(L"DD2 DVB-T");
	        pDVBTTuningSpace->put_UniqueName(bstrTuningSpaceName);
	        SysFreeString(bstrTuningSpaceName);

	        // set friendly name  (shown as Description in the registry)
	        BSTR bstrFriendlyName = SysAllocString(L"DD2 DVB-T");
	        hr = pDVBTTuningSpace->put_FriendlyName(bstrFriendlyName); 
	        SysFreeString(bstrFriendlyName);

	        // set system type
	        pDVBTTuningSpace->put_SystemType(DVB_Terrestrial);

	        // set network type
	        hr = pDVBTTuningSpace->put__NetworkType(CLSID_DVBTNetworkProvider);
            if (hr == S_OK)
            {
	            // set the Frequency mapping						
	            BSTR bstrFreqMapping = SysAllocString(L"-1");
	            hr = pDVBTTuningSpace->put_FrequencyMapping(bstrFreqMapping); // not used
	            SysFreeString(bstrFreqMapping);

	            // Set default locator
	            IDVBTLocator *pDVBTLocator = NULL;
	            hr = CoCreateInstance(__uuidof(DVBTLocator)/*CLSID_DVBTLocator*/, NULL, CLSCTX_INPROC_SERVER, __uuidof(IDVBTLocator), (void**)&pDVBTLocator);		
	            if SUCCEEDED(hr) 
                {
		            /*pDVBTLocator->put_CarrierFrequency(-1);
		            pDVBTLocator->put_SymbolRate(-1);
		            pDVBTLocator->put_InnerFEC(BDA_FEC_METHOD_NOT_SET);            
		            pDVBTLocator->put_InnerFECRate(BDA_BCC_RATE_NOT_SET);      
		            pDVBTLocator->put_OuterFEC(BDA_FEC_METHOD_NOT_SET);        
		            pDVBTLocator->put_OuterFECRate(BDA_BCC_RATE_NOT_SET);      
		            pDVBTLocator->put_Modulation(BDA_MOD_NOT_SET);             				*/

		            pDVBTTuningSpace->put_DefaultLocator(pDVBTLocator);				

	                // get the number of tuning spaces that have been created
	                LONG lCount = 0;
	                hr = pTuningSpaceContainer->get_Count(&lCount); 			

	                // add the tuning space to the registry
	                VARIANT varID;				
	                VariantInit(&varID);			  
	                lCount++;
	                varID.intVal = lCount;
	                log_info(L"Tuning Space ID:%d") % lCount;
	                //varID.vt = VT_INT;				
	                hr = pTuningSpaceContainer->Add(pDVBTTuningSpace, &varID);
	                if (FAILED(hr))
		                log_warning(L"pTuningSpaceContainer->Add(pDVBTuningSpace...)      Already added !");
	                VariantClear(&varID);  

	                pDVBTLocator->Release();

                    *ppTuningSpace = pDVBTTuningSpace;
                    (*ppTuningSpace)->AddRef();
                }
            }
            else
            {
		        log_error(L"put_NetworkType failed\n");
            }

	        pDVBTTuningSpace->Release();
        }
        else
        {
		    log_error(L"CoCreateInstance(CLSID_DVBTTuningSpace...)");
        }

	    pTuningSpaceContainer->Release();
	    pTuningSpaceContainer = NULL;
    }
    else
    {
		log_error(L"CoCreateInstance(CLSID_SystemTuningSpaces()");
    }
    return *ppTuningSpace == NULL ? E_FAIL : S_OK;
}

HRESULT create_tuning_space(int tuner_type, ITuningSpace **ppTuningSpace)
{
    *ppTuningSpace = NULL;

	switch(tuner_type) 
    {
        case dvblink::TUNERTYPE_DVBS:
			return create_dvbs_tuning_space(ppTuningSpace);
	        break;
        case dvblink::TUNERTYPE_DVBC:
			return create_dvbc_tuning_space(ppTuningSpace);
	        break;	
        case dvblink::TUNERTYPE_DVBT:
			return create_dvbt_tuning_space(ppTuningSpace);
	        break;
    }

	return E_FAIL;
}
