/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#ifndef _DS_TOPOLOGY_H

#include "bda_diseqc.h"
#include <drivers/deviceapi.h>

HRESULT get_bda_filter_interfaces(IBaseFilter *pfilter,
                                  IBDA_FrequencyFilter **pBDAFrequencyFilter,
                                  IBDA_SignalStatistics **pBDASignalStatistics,
                                  IBDA_LNBInfo** pBDALNBInfo,
                                  IBDA_DigitalDemodulator** pBDADigitalDemodulator,
                                  IBDA_DiseqCommand** pDiseqcCommandInterface);

HRESULT set_lnb_voltage_using_polarity(IBDA_FrequencyFilter *pBDAFrequencyFilter, dvblink::DL_E_LNB_POWER_TYPES lnb_power);

#define _DS_TOPOLOGY_H
#endif
