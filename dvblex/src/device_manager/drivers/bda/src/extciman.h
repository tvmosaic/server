/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <vector>
#include <string>
#include <drivers/deviceapi.h>

enum ext_ci_device_type_e
{
    ecdt_none,
    ecdt_dd_ci
};

class ext_device_t
{
public:
    ext_device_t() :
      device_handle_(NULL),
      type_(ecdt_none)
    {
        device_info_.name[0] = L'\0';
        device_info_.hwid[0] = L'\0';
    }

    void* device_handle_;
    dvblink::TExtCIDevice device_info_;
    ext_ci_device_type_e type_;
};

typedef std::vector<ext_device_t> ext_device_list_t;

class ext_ci_manager
{
public:
    ext_ci_manager();
    ~ext_ci_manager();

    bool get_device_list(ext_device_list_t& devices);
    
    bool create_device(const std::wstring& hwid);
    void destroy_device();

    ext_device_t* get_device() {return device_;}
    bool send_pmt(unsigned char* pmt_ptr, int pmt_len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd);

protected:
    ext_device_t* device_;
};
