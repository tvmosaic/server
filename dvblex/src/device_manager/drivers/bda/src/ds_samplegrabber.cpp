/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>

#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <tuner.h>
#include <commctrl.h>
#include <stdarg.h>

//***********************************
//This is a "dxtrans.h" workaround for all newer version of DirectShow SDK
#pragma include_alias( "dxtrans.h", "qedit.h" )
#define __IDxtCompositor_INTERFACE_DEFINED__
#define __IDxtAlphaSetter_INTERFACE_DEFINED__
#define __IDxtJpeg_INTERFACE_DEFINED__
#define __IDxtKey_INTERFACE_DEFINED__
#include <qedit.h>
//***********************************

#include <bdatif.h>
#include <dl_dshow_enum.h>
#include <dl_dshow_helpers.h>
#include "ds_graph.h"
#include "common.h"
#include <dl_logger.h>
#include "ds_samplegrabber.h"

using namespace dvblink::logging;
using namespace dvblink::engine;

// Sample grabber class to capture the data
class GrabberCallback : public ISampleGrabberCB
{
public:
    STDMETHODIMP_(ULONG) AddRef() { return 2; }
    STDMETHODIMP_(ULONG) Release() { return 1; }

    STDMETHODIMP QueryInterface(REFIID riid, void **ppvObject)
    {
        if (NULL == ppvObject) return E_POINTER;
        if (riid == __uuidof(IUnknown))
        {
            *ppvObject = static_cast<IUnknown*>(this);
             return S_OK;
        }
        if (riid == __uuidof(ISampleGrabberCB))
        {
            *ppvObject = static_cast<ISampleGrabberCB*>(this);
             return S_OK;
        }
        return E_NOTIMPL;
    }

    STDMETHODIMP SampleCB(double Time, IMediaSample *pSample)
    {
        return E_NOTIMPL;
    }

    STDMETHODIMP BufferCB(double Time, BYTE *pBuffer, long BufferLen)
    {
		(*m_callback)(Time, pBuffer, BufferLen, m_callbackdata);

        return S_OK;
    }

	void SetCallback(int (*callback_SampleGrabber)(double, BYTE*, long, LPVOID), LPVOID data)
	{
		m_callback=callback_SampleGrabber;
		m_callbackdata=data;
	}

private:
	int (*m_callback)(double, BYTE*, long, LPVOID);
	LPVOID m_callbackdata;
};


sample_grabber_man_t::sample_grabber_man_t(ts_callback_f cb, void* param) :
    psgfilter_(NULL), sample_grabber_(NULL), callback_(cb), param_(param)
{
    gb_ = new GrabberCallback();

    //create sample grabber
	HRESULT hr;

	// Create the sample grabber filter
	if SUCCEEDED( hr = CoCreateInstance(CLSID_SampleGrabber, NULL, CLSCTX_INPROC,	IID_IBaseFilter, (LPVOID *) &psgfilter_)) 
	{
	    psgfilter_->QueryInterface(IID_ISampleGrabber, (void **) &sample_grabber_);

        if (sample_grabber_ != NULL)
        {
	        AM_MEDIA_TYPE sgType;
	        ZeroMemory(&sgType, sizeof(sgType));
	        sgType.majortype=GUID_NULL;
	        sgType.subtype=GUID_NULL;
	        sgType.formattype=GUID_NULL;	
	        if FAILED(hr = sample_grabber_->SetMediaType(&sgType)) {
		        log_error(L"Cannot set sample grabber media type");
		        sample_grabber_->Release();
		        sample_grabber_ = NULL;
		        psgfilter_->Release();
		        psgfilter_ = NULL;
	        } else
            {
	            gb_->SetCallback(callback_SampleGrabber, this);

	            // set callback function for sample grabber 
	            if FAILED(hr = sample_grabber_->SetCallback(gb_, 1)){
		            log_error(L"Cannot set sample grabber callback");
		            sample_grabber_->Release();
		            sample_grabber_ = NULL;
		            psgfilter_->Release();
		            psgfilter_ = NULL;
	            }
            }
        }
    }
}

sample_grabber_man_t::~sample_grabber_man_t()
{
	if (sample_grabber_ != NULL)
	{
		sample_grabber_->SetCallback(NULL, 1);
	    sample_grabber_->Release();
	}

	if (psgfilter_ != NULL)
        psgfilter_->Release();

    delete gb_;
}

int sample_grabber_man_t::callback_SampleGrabber(double Time, BYTE *pBuffer, long BufferLen, LPVOID data)
{
    sample_grabber_man_t* parent = (sample_grabber_man_t*)data;

    parent->callback_(pBuffer, BufferLen, parent->param_);

	return 0;
}
