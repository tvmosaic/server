/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <map>

#include <boost/thread.hpp>
#include <drivers/aux_module.h>

namespace dvblink { 

struct bda_device_descriptor_t
{
    std::string name_;
    unsigned long types_;
};

typedef std::map<std::string, bda_device_descriptor_t> bda_device_descriptor_map_t;

class bda_type_storage_t
{
public:
    static void init();
    static void term();

    static bool get_device_descriptor(const std::string& device_path, bda_device_descriptor_t& descriptor);
    static void update_descriptors();

protected:
    static bda_type_storage_t* instance_;
    bda_device_descriptor_map_t bda_device_descriptor_map_;
    boost::mutex lock_;

    static bool get_device_descriptor_int(const std::string& device_path, bda_device_descriptor_t& descriptor);
    static void update_descriptors_thread();

    bda_type_storage_t(){}
    ~bda_type_storage_t(){}
};


class bda_aux_t : public aux_module_t
{
public:
    bda_aux_t();
    virtual ~bda_aux_t();

    virtual bool init();
    virtual void term();
};

}
