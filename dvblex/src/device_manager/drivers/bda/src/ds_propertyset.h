/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#ifndef _DS_PROPERTYSET_H
#define _DS_PROPERTYSET_H

#include <bdatypes.h>
#include "ds_propertyset_base.h"

enum EBDA_CARD_TYPE
{
     BDACT_OTHER,
     BDACT_KNC,
     BDACT_TWINHAN,
     BDACT_AF9015,	
     BDACT_COMPRO,	// Compro Videomate PCI
     BDACT_FIREDTV,   // FireDTV 
     BDACT_TWINHAN_MANTIS,
     BDACT_TBS_QBOX,
     BDACT_HAUPPAUGE,
     BDACT_DVBWORLD,
     BDACT_GENIATECH,
     BDACT_TBS,
     BDACT_TEVII,
     BDACT_CREATIX,
     BDACT_TECHNISAT_SKYSTARHD2,
     BDACT_AVERMEDIA_DVB_T_STICK,	//Avermedia Axxx DVB-T devices
     BDACT_TECHNOTREND,	//all bda technotrend cards (budget, usb and premium)
     BDACT_TECHNISAT_SKYSTAR_USB2HD,
     BDACT_TWINHAN_TERRATEC,
     BDACT_ANYSEE,
     BDACT_OMICOM,
     BDACT_DD,
     BDACT_TBS_NXP,
     BDACT_HDHOMERUN,
	 BDACT_CYNERGY_DUAL_TSTICK,
	 BDACT_PCTV_DVBT2_TUNER,
     BDACT_PCTV,
     BDACT_DVBSKY,
     BDACT_TECHNOTREND_DVBSKY,	//TT-connect and TT-budget CT2-4650 CI
     BDACT_GENPIX
};

struct TBDAOptions {
    TBDAOptions() :
    no_capture_filter(false), input_range_after_tune_request(false), input_range_direct(false)
    {}

	bool no_capture_filter;
	bool input_range_after_tune_request;
	bool input_range_direct;
};

class property_set_manager_t
{
public:
    property_set_manager_t();
    ~property_set_manager_t();

    bool init(dvblink::DL_E_TUNER_TYPES tuner_type, const wchar_t *wstunername, const wchar_t* device_path, const wchar_t* dll_path);
    void term();

    property_set_base_t* get_property_set(){return property_set_;}
    const wchar_t* get_vid(){return tuner_vid_;}
    const wchar_t* get_pid(){return tuner_pid_;}
    EBDA_CARD_TYPE get_type(){return BDACardType_;}
    const TBDAOptions* get_bda_options(){return &bdaoptions_;}
    bool use_universal_provider(){return b_universal_provider_;}

protected:
    dvblink::DL_E_TUNER_TYPES tuner_type_;
    TBDAOptions bdaoptions_;
    wchar_t tuner_vid_[256];
    wchar_t tuner_pid_[256];
    wchar_t tuner_subsys_[256];
    EBDA_CARD_TYPE BDACardType_;
    bool b_universal_provider_;
    property_set_base_t* property_set_;

    bool property_parse_tuner_identity(const wchar_t* device_path);
    bool property_use_universal_provider();
    void fill_bda_options();
    EBDA_CARD_TYPE get_device_type(const wchar_t *wstunername);
    HRESULT set_property_set_functions(EBDA_CARD_TYPE BDACardType);
    void create_property_set();
};

#endif
