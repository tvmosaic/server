/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <boost/algorithm/string.hpp>

#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <tuner.h>
#include <commctrl.h>
#include <stdarg.h>
#include <bdatif.h>

#include <drivers/deviceapi.h>
#include <dl_dshow_enum.h>
#include <dl_dshow_helpers.h>
#include "ds_graph.h"
#include "common.h"
#include "ds_propertyset.h"
#include "ds_tunerequest.h"

#include "propertysets\turbosight\tbs_main.h"
#include "propertysets\compro\compro_main.h"
#include "propertysets\firedtv\fdtv_main.h"
#include "propertysets\twinhan\twinhan_main.h"
#include "propertysets\dvbworld\dvbworld_main.h"
#include "propertysets\geniatech\gtech_main.h"
#include "propertysets\hauppauge\hauppauge_main.h"
#include "propertysets\tevii\tevii_main.h"
#include "propertysets\creatix\creatix_main.h"
#include "propertysets\technisat\technisat_main.h"
#include "propertysets\avermedia\main.h"
#include "propertysets\hdhomerun\hdhr_main.h"
#include "propertysets\technotrend\technotrend_main.h"
#include "propertysets\knc\knc.h"
#include "propertysets\anysee\anysee_main.h"
#include "propertysets\omicom\omicom_main.h"
#include "propertysets\DigitalDevices\dd_main.h"
#include "propertysets\pctv\pctv.h"
#include "propertysets\dvbsky\dvbsky_main.h"
#include "propertysets\genpix\genpix.h"
#include "propertysets\astrometa\main.h"
#include "propertysets\realtek\main.h"

#include <dl_logger.h>

using namespace dvblink::logging;
using namespace dvblink::engine;

#define NOXON_TUNER_VENDOR_ID L"0ccd"
#define NOXON_DAB_TUNER_VENDOR_ID L"00b3"

bool property_set_manager_t::property_parse_tuner_identity(const wchar_t* device_path)
{
	std::wstring vid;
	std::wstring pid;
	std::wstring subsys;
    std::wstring device_instance_path;
	bool ret_val = parse_tuner_identity_from_path(device_path, vid, pid, subsys, device_instance_path);
	if (ret_val)
	{
		wcscpy(tuner_vid_, vid.c_str());
		wcscpy(tuner_pid_, pid.c_str());
		wcscpy(tuner_subsys_, subsys.c_str());
	}
    return wcslen(tuner_pid_) > 0 && wcslen(tuner_vid_) > 0;
}

property_set_manager_t::property_set_manager_t() :
    BDACardType_(BDACT_OTHER), b_universal_provider_(false), property_set_(NULL)
{
    memset(tuner_vid_, 0, sizeof(tuner_vid_));
    memset(tuner_pid_, 0, sizeof(tuner_pid_));
    memset(tuner_subsys_, 0, sizeof(tuner_subsys_));
}

property_set_manager_t::~property_set_manager_t()
{
}

bool property_set_manager_t::init(dvblink::DL_E_TUNER_TYPES tuner_type, const wchar_t *wstunername, const wchar_t* device_path, const wchar_t* dll_path)
{
    tuner_type_ = tuner_type;

    if (!property_parse_tuner_identity(device_path))
        log_warning(L"Could not parse tuner identity for %1%") % device_path;
    else
        log_info(L"Tuner identity: vid (%1%), pid (%2%), subsys (%3%)") % tuner_vid_ % tuner_pid_ % tuner_subsys_;

    b_universal_provider_ = property_use_universal_provider();
    BDACardType_ = get_device_type(wstunername);
    fill_bda_options();
    create_property_set();
    //init property set
    property_set_->set_device_info(wstunername, device_path, dll_path, tuner_type, tuner_vid_, tuner_pid_);
    return true;
}

void property_set_manager_t::term()
{
    if (property_set_ != NULL)
        delete property_set_;
}

EBDA_CARD_TYPE property_set_manager_t::get_device_type(const wchar_t *wstunername)
{
	wchar_t str[256];
	wcscpy_s(str, 255, wstunername);
	_wcslwr_s(str, 255);
//	AddLog(L"LOWER:%s", str);

	EBDA_CARD_TYPE ret_val =  BDACT_OTHER;
	if(wcsstr(str, L"genpix") > 0)
		ret_val =  BDACT_GENPIX;
	if(wcsstr(str, L"anysee") > 0)
		ret_val =  BDACT_ANYSEE;
	if(wcsstr(str, L"philips") > 0)
		ret_val =  BDACT_KNC;
	if(wcsstr(str, L"knc bda") > 0 ||
		wcsstr(str, L"mystique") > 0)
		ret_val =  BDACT_KNC;
	if(wcsstr(str, L"878") > 0)
		ret_val =  BDACT_TWINHAN;
	if(wcsstr(str, L"twinhan") > 0)
			ret_val =  BDACT_TWINHAN;	
     if (wcsstr(str, L"terratec") > 0 ||
		wcsstr(str, L"cinergy") > 0 ||
		wcsstr(str, L"dtv-dvb") > 0)
			ret_val =  BDACT_TWINHAN_TERRATEC;
     if (wcsstr(str, L"digital") > 0 &&
		wcsstr(str, L"devices") > 0)
			ret_val =  BDACT_DD;
	if(wcsstr(str, L"mantis") > 0)
		ret_val =  BDACT_TWINHAN_MANTIS;	// Mantis
	if(wcsstr(str, L"af9015") > 0)
		ret_val =  BDACT_AF9015;	
	if(wcsstr(str, L"videomate") > 0)
		ret_val =  BDACT_COMPRO;	
	if(wcsstr(str, L"omicom") > 0)
		ret_val =  BDACT_OMICOM;
	if(wcsstr(str, L"hdhomerun") > 0)
			ret_val =  BDACT_HDHOMERUN;	
	
	if(wcsstr(str, L"firedtv") > 0)
		ret_val =  BDACT_FIREDTV;
	if(wcsstr(str, L"floppy") > 0)
		ret_val =  BDACT_FIREDTV;
	if(wcsstr(str, L"pctv") > 0)
		ret_val =  BDACT_PCTV;

    const tbs_tuner_desc_t* tbs_desc = find_tbs_tuner_desc(wstunername);
    if (tbs_desc != NULL)
    {
        switch (tbs_desc->type)
        {
            case ttdte_pci:
			    ret_val =  BDACT_TBS;
                break;
            case ttdte_nxp:
			    ret_val =  BDACT_TBS_NXP;
                break;
            case ttdte_usb:
			    ret_val =  BDACT_TBS_QBOX;
                break;
        }
    }

	if(wcsstr(wstunername, TERRATEC_S2_USB_HD_TUNER_NAME) > 0) 
		ret_val =  BDACT_GENIATECH;

	if(wcsstr(wstunername, HAUPPAUGE_NOVAS2_TUNER_FILTER_NAME) > 0) 
		ret_val =  BDACT_HAUPPAUGE;
	if(wcsstr(wstunername, HAUPPAUGE_NOVAS_TUNER_FILTER_NAME) > 0) 
		ret_val =  BDACT_HAUPPAUGE;
	if(wcsstr(wstunername, HAUPPAUGE_5500_TUNER_FILTER_NAME) > 0) 
		ret_val =  BDACT_HAUPPAUGE;

	if(wcsstr(wstunername, HAUPPAUGE_NOVAS_USB_TUNER_FILTER_NAME) > 0)  // test
		ret_val =  BDACT_GENIATECH;

    if(boost::icontains(wstunername, DVBSKY_TUNER_FILTER_NAME)) 
		ret_val =  BDACT_DVBSKY;

	if(wcsstr(wstunername, GENIATECH_TUNER_FILTER_NAME2) > 0) 
		ret_val =  BDACT_GENIATECH;	

	if(wcsstr(wstunername, TWINHAN_1027_TUNER_FILTER_NAME) > 0) 
		ret_val =  BDACT_TWINHAN;	

	if(wcsstr(wstunername, DVBWORLD_USB_2104_TUNER_FILTER_NAME) > 0) 
		ret_val =  BDACT_DVBWORLD;	
	if(wcsstr(wstunername, DVBWORLD_PCI_2004_TUNER_FILTER_NAME) > 0) 
		ret_val =  BDACT_DVBWORLD;
	if(wcsstr(wstunername, DVBWORLD_PCIE_2005_TUNER_FILTER_NAME) > 0) 
		ret_val =  BDACT_DVBWORLD;	
	if(wcsstr(wstunername, DVBWORLD_PCIE_2006_TUNER_FILTER_NAME) > 0) 
		ret_val =  BDACT_DVBWORLD;	

	if(wcsstr(wstunername, TEVII_USB_TUNER_FILTER_NAME) > 0) 
		ret_val =  BDACT_TEVII;	

	if(wcsstr(wstunername, TEVII_NEW_USB_TUNER_FILTER_NAME) > 0) 
		ret_val =  BDACT_TEVII;	

	if(wcsstr(wstunername, CREATIX_TUNER_FILTER_NAME) > 0) 
		ret_val =  BDACT_CREATIX;	

	if(wcsstr(wstunername, TECHNISAT_SKYSTARHD_TUNER_FILTER_NAME) > 0)
		ret_val =  BDACT_TECHNISAT_SKYSTARHD2;

	if(wcsstr(wstunername, TECHNISAT_SKYSTAR_USB2HD_TUNER_FILTER_NAME) > 0)
		ret_val =  BDACT_TECHNISAT_SKYSTAR_USB2HD;

	if(wcsstr(wstunername, CYNERGY_DUAL_TSTICK_TUNER_NAME) > 0) 
		ret_val =  BDACT_CYNERGY_DUAL_TSTICK;	

	if(wcsstr(wstunername, BDG2_NAME_C_TUNER) > 0 ||
		wcsstr(wstunername, BDG2_NAME_S_TUNER) > 0 ||
		wcsstr(wstunername, BDG2_NAME_T_TUNER) > 0 ||
		wcsstr(wstunername, BUDGET3NAME_TUNER) > 0 ||
		wcsstr(wstunername, USB2BDA_DVB_NAME_C_TUNER) > 0 ||
		wcsstr(wstunername, USB2BDA_DVB_NAME_S_TUNER) > 0 ||
		wcsstr(wstunername, USB2BDA_DVB_NAME_T_TUNER) > 0 ||
		wcsstr(wstunername, PREMIUM_NAME_DIGITAL_TUNER) > 0 ||
		wcsstr(wstunername, USB2BDA_DVBS_NAME_PIN_TUNER) > 0)
			ret_val =  BDACT_TECHNOTREND;

    //overwrite generic name-based detection with a more precise vid/pid detection if needed
	if ((boost::iequals(tuner_vid_, TT_USB_4500_TUNER_VENDOR_ID) && boost::iequals(tuner_pid_, TT_USB_4500_TUNER_PRODUCT_ID)) ||
	    (boost::iequals(tuner_vid_, TT_PCIE_4500_TUNER_VENDOR_ID) && boost::iequals(tuner_pid_, TT_PCIE_4500_TUNER_PRODUCT_ID) &&
        boost::iequals(tuner_subsys_, TT_PCIE_4500_TUNER_SUBSYS_ID)) )
		ret_val =  BDACT_TECHNOTREND_DVBSKY;

	if (boost::iequals(tuner_vid_, TERRATEC_TEVII_VENDOR_ID) && boost::iequals(tuner_pid_, TERRATEC_CINERGY_S2_DUAL_PRODUCT_ID))
		ret_val =  BDACT_TEVII;

	log_info(L"ret_val: %d") % ret_val;

	return ret_val;
}

void property_set_manager_t::fill_bda_options() 
{
	// FireDTV uses the PropertySet on the tuner filter
	//bdaoptions_.propertyset_on_the_filter = BDACardType_ == BDACT_FIREDTV || BDACardType_ == BDACT_TBS_QBOX;

	// Turbosight DVB-T USB stick 
	// Turbosight DVB-S USB QBox
	// Avermedia DVB-T Stick
    // All Avermedia sticks except for Gigabyte U8300, Reddo dvb-t/c tuner and Avermedia ATS/3D sat tuner
	bdaoptions_.no_capture_filter = (boost::iequals(tuner_vid_, AVERMEDIA_TUNER_VENDOR_ID) && !(boost::iequals(tuner_pid_, AVERMEDIA_3DSAT_PRODUCT_ID) || 
        boost::iequals(tuner_pid_, AVERMEDIA_H837_PRODUCT_ID))) || 
        (boost::iequals(tuner_vid_, AVERMEDIA_TUNER_VENDOR_ID_1) && !boost::iequals(tuner_pid_, GIGABYTE_U8300_TUNER_PRODUCT_ID) &&
        !boost::iequals(tuner_pid_, REDDO_DVBTC_TUNER_PRODUCT_ID)) || 
        (boost::iequals(tuner_vid_, REALTEK_TUNER_VENDOR_ID) && boost::iequals(tuner_pid_, REALTEK_CLONE_1_PRODUCT_ID)) ||
        (boost::iequals(tuner_vid_, AVERMEDIA_TUNER_VENDOR_ID_3) && boost::iequals(tuner_pid_, AVERMEDIA_CLONE_2_PRODUCT_ID)) ||
        (boost::iequals(tuner_vid_, ASTROMETA_TUNER_VENDOR_ID) && boost::iequals(tuner_pid_, ASTROMETA_DTV_PRODUCT_ID)) ||
        (boost::iequals(tuner_vid_, REALTEK_TUNER_VENDOR_1_ID) && boost::iequals(tuner_pid_, REALTEK_DVBT_1_PRODUCT_ID)) ||
		(boost::iequals(tuner_vid_, REALTEK_TUNER_VENDOR_ID) && boost::iequals(tuner_pid_, REALTEK_DVBLOGIC_SAMPLE_PRODUCT_ID)) ||
		(boost::iequals(tuner_vid_, REALTEK_TUNER_VENDOR_ID) && boost::iequals(tuner_pid_, REALTEK_DVBT_PRODUCT_ID)) ||
		(boost::iequals(tuner_vid_, DVBLOGIC_TUNER_VENDOR_ID) && boost::iequals(tuner_pid_, DVBLOGIC_100TC_PRODUCT_ID)) ||
		(BDACardType_ == BDACT_TEVII) ||
		(boost::iequals(tuner_vid_, NOXON_TUNER_VENDOR_ID) && boost::iequals(tuner_pid_, NOXON_DAB_TUNER_VENDOR_ID)) ||
        ((tuner_type_ == dvblink::TUNERTYPE_DVBC || tuner_type_ == dvblink::TUNERTYPE_DVBT) && BDACardType_ == BDACT_KNC) || (BDACardType_ == BDACT_HDHOMERUN) ||
		(BDACardType_ == BDACT_CYNERGY_DUAL_TSTICK);

	bdaoptions_.input_range_after_tune_request = (BDACardType_ == BDACT_CREATIX);

	bdaoptions_.input_range_direct = (BDACardType_ == BDACT_CREATIX);
}

bool property_set_manager_t::property_use_universal_provider()
{
	//do not create Universal provider by default
	//do it only for atsc, clearQAM and dvb-T2 pctv tuner
	return (tuner_type_ == dvblink::TUNERTYPE_ATSC) || 
	       (tuner_type_ == dvblink::TUNERTYPE_CLEARQAM) || 
    	   (boost::iequals(tuner_vid_, PCTV_TUNER_VENDOR_ID) && 
    	    (boost::iequals(tuner_pid_, PCTV_T2_TUNER_PROD_ID) || boost::iequals(tuner_pid_, PCTV_292E_TUNER_PROD_ID)) ||
    	   (boost::iequals(tuner_vid_, DVBLOGIC_TUNER_VENDOR_ID) && boost::iequals(tuner_pid_, DVBLOGIC_100TC_PRODUCT_ID))
    	   );
}

void property_set_manager_t::create_property_set()
{
	switch(BDACardType_) {
		case BDACT_TBS:
            property_set_ = new turbosight_property_set_t();
			break;
		case BDACT_TBS_QBOX:
            property_set_ = new turbosight_qbox_property_set_t();
			break;
		case BDACT_TBS_NXP:
            property_set_ = new turbosight_nxp_property_set_t();
			break;
		case BDACT_COMPRO:
            property_set_ = new compro_property_set_t();
			break;				
		case BDACT_FIREDTV:
            property_set_ = new fdtv_property_set_t();
			break;							
		case BDACT_TECHNISAT_SKYSTAR_USB2HD:
		case BDACT_TECHNISAT_SKYSTARHD2:
		case BDACT_TWINHAN:
		case BDACT_TWINHAN_MANTIS:			
        case BDACT_TWINHAN_TERRATEC:
            property_set_ = new twinhan_property_set_t();
			break;
        case BDACT_GENPIX:
            property_set_ = new genpix_property_set_t();
			break;
		case BDACT_HAUPPAUGE:
            property_set_ = new hauppauge_property_set_t();
			break;
		case BDACT_ANYSEE:
            property_set_ = new anysee_property_set_t();
			break;
		case BDACT_HDHOMERUN:
            property_set_ = new hdhr_property_set_t();
			break;
        case BDACT_DVBWORLD:
            property_set_ = new dvbworld_property_set_t();
			break;
        case BDACT_DVBSKY:
        case BDACT_TECHNOTREND_DVBSKY:
            property_set_ = new dvbsky_property_set_t();
			break;			
        case BDACT_GENIATECH:
            property_set_ = new geniatech_property_set_t();
			break;			
		case BDACT_TEVII:
            property_set_ = new tevii_property_set_t();
			break;
		case BDACT_OMICOM:
            property_set_ = new omicom_property_set_t();
			break;
		case BDACT_PCTV:
            property_set_ = new pctv_property_set_t();
			break;            
        case BDACT_TECHNOTREND:
            property_set_ = new technotrend_property_set_t();
			break;
		case BDACT_KNC:
            property_set_ = new knc_property_set_t();
			break;
		case BDACT_DD:
            property_set_ = new dd_property_set_t();
			break;
        default:
            property_set_ = new property_set_base_t();
            break;
	}
}



