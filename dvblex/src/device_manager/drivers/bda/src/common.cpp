/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <boost/algorithm/string.hpp>

#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <tuner.h>
#include <commctrl.h>
#include <stdarg.h>

#include <drivers/deviceapi.h>
#include <dl_logger.h>
#include <dl_dshow_enum.h>
#include <dl_dshow_helpers.h>
#include "propertysets\hdhomerun\hdhr_main.h"
#include "propertysets\realtek\main.h"
#include "common.h"

using namespace dvblink::engine;
using namespace dvblink::logging;

int find_device_by_path(dvblink::TDevAPIDevListEx* device_list, const wchar_t* device_path)
{
    int ret_val = -1;

    if (wcslen(device_path) > 0)
    {
        log_info(L"find_device_by_path. searching for matching tuner with device path %1%") % device_path;
        //walk through present devices to match the device path
        for (int i=0; i<device_list->Count; i++)
        {
            if (boost::iequals(device_list->Devices[i].szDevicePathW, device_path))
            {
                //found
                ret_val = i;
                log_info(L"find_device_by_path. Found device index %1% for device path %2%") % ret_val % device_path;
                break;
            }
        }
    }
    return ret_val;
}

int DeviceGetListEx_impl(dvblink::PDevAPIDevListEx pDL)
{
	TDSEnum *penumBDATuners = enum_create(KSCATEGORY_BDA_NETWORK_TUNER);

	while (enum_next(penumBDATuners) == S_OK)
	{	
        if (pDL->Count == DL_DEVAPI_MAX_DEVICES)
        {
		    log_warning(L"DeviceGetListEx_impl: There are more devices present in the system than maximum allowed (%1%)") % DL_DEVAPI_MAX_DEVICES;
            break;
        }

	    if (SUCCEEDED(enum_get_name(penumBDATuners)))
        {
	        log_info(L"DeviceGetListEx_impl: Filter [%s]") % penumBDATuners->szNameW;

            //skip Silicon dust virtual tuners and DVBLink virtual tuners
            if (!boost::icontains(penumBDATuners->szName, SILICONDUST_TUNER_NAME_SUBSTR) && 
                !boost::icontains(penumBDATuners->szName, "DVBLink"))
            {
		        //determine tuner type
		        pDL->Devices[pDL->Count].TunerType = dvblink::TUNERTYPE_UNKNOWN; //Default

		        strcpy_s(pDL->Devices[pDL->Count].szName, 255, penumBDATuners->szName);
                wcscpy_s(pDL->Devices[pDL->Count].szDevicePathW, 512, penumBDATuners->szDevicePathW);

                //parse tuner identity for name/make/model
                std::wstring vid;
                std::wstring pid;
                std::wstring subsys;
                std::wstring device_instance_path;
                if (parse_tuner_identity_from_path(penumBDATuners->szDevicePathW, vid, pid, subsys, device_instance_path))
                {
		            strcpy_s(pDL->Devices[pDL->Count].vid, 32, string_cast<EC_UTF8>(vid).c_str());
		            strcpy_s(pDL->Devices[pDL->Count].pid, 32, string_cast<EC_UTF8>(pid).c_str());

                    if (boost::iequals(vid, DVBLOGIC_TUNER_VENDOR_ID) && boost::iequals(pid, DVBLOGIC_100TC_PRODUCT_ID))
                    {
                        strcpy(pDL->Devices[pDL->Count].make, DVBLOGIC_100TC_MAKE);
                        strcpy(pDL->Devices[pDL->Count].model, DVBLOGIC_100TC_MODEL);
                        strcpy(pDL->Devices[pDL->Count].modelNum, DVBLOGIC_100TC_MODEL_NUM);
					    strcpy(pDL->Devices[pDL->Count].uuid_make, DVBLOGIC_100TC_UUID_MAKE);
                        pDL->Devices[pDL->Count].connection_type = dvblink::TCT_USB;
                    }
                    if (device_instance_path.length() > 0)
                    {
	                    log_info(L"DeviceGetListEx_impl: device instance path %1%") % device_instance_path;
                        std::string uuid_str = string_cast<EC_UTF8>(device_instance_path);
                        strcpy(pDL->Devices[pDL->Count].uuid, uuid_str.c_str());

                        //tuner connection type
                        if (boost::istarts_with(device_instance_path, "usb"))
                            pDL->Devices[pDL->Count].connection_type = dvblink::TCT_USB;
                        else if (boost::istarts_with(device_instance_path, "pci"))
                            pDL->Devices[pDL->Count].connection_type = dvblink::TCT_PCIE;
                    }
                }

                pDL->Devices[pDL->Count].FrontendIdx = 0; //always for bda
                pDL->Devices[pDL->Count].TunerIdx = pDL->Count;
		        pDL->Count++;
            } else
            {
                log_ext_info(L"BDA: DeviceGetListEx_impl. Skipped Silicondust virtual BDA driver");
            }
        } else
        {
            log_ext_info(L"BDA: DeviceGetListEx_impl. Skipped nameless BDA driver");
        }
	}

	enum_free(penumBDATuners);	
	log_info(L"DeviceGetListEx_impl: pDL->Count:%d") % pDL->Count;

	return dvblink::SUCCESS;
}

unsigned long get_tuner_type_from_filter(IBaseFilter *pFilter)
{
    unsigned long ret_val = dvblink::TUNERTYPE_UNKNOWN;
	HRESULT hr;

	IBDA_Topology *pTopology = NULL;
	#define NODE_SIZE 20
	_BDANODE_DESCRIPTOR nodes[NODE_SIZE];
	unsigned long cnt = 0;
	
	if SUCCEEDED(pFilter->QueryInterface(__uuidof(IBDA_Topology), (void**)&pTopology)) {
		hr = pTopology->GetNodeDescriptors(&cnt, NODE_SIZE, nodes);
		if (hr == S_OK && cnt > 0) {
			for (unsigned int ii = 0; ii < cnt; ii++) {
				if (nodes[ii].guidFunction == KSNODE_BDA_QPSK_DEMODULATOR) {
					ret_val |= dvblink::TUNERTYPE_DVBS;
                    log_ext_info(L"BDA: get_tuner_type_from_filter: DVB-S/S2");
				} else
				if (nodes[ii].guidFunction == KSNODE_BDA_COFDM_DEMODULATOR) {
					ret_val |= dvblink::TUNERTYPE_DVBT;
                    log_ext_info(L"BDA: get_tuner_type_from_filter: DVB-T/T2");
				} else
				if (nodes[ii].guidFunction == KSNODE_BDA_QAM_DEMODULATOR) {
					ret_val |= dvblink::TUNERTYPE_DVBC;
                    log_ext_info(L"BDA: get_tuner_type_from_filter: DVB-C/CQAM");
				} else
				if (nodes[ii].guidFunction == KSNODE_BDA_8VSB_DEMODULATOR) {
					ret_val |= dvblink::TUNERTYPE_ATSC;
                    log_ext_info(L"BDA: get_tuner_type_from_filter: ATSC");
				} else
                {
                    OLECHAR* bstrGuid = NULL;
                    StringFromCLSID(nodes[ii].guidFunction, &bstrGuid);
                    if (bstrGuid != NULL)
                    {
                        log_ext_info(L"BDA: get_tuner_type_from_filter. Unknown guid type %1%") % std::wstring(bstrGuid);
                        ::CoTaskMemFree(bstrGuid);
                    }
                }
				
			}

		}
		pTopology->Release();
	}	

	return ret_val;
}

void get_tuner_type(std::map<std::wstring, tuner_description_t>& tuners)
{
    TDSEnum *penumBDATuners = enum_create(KSCATEGORY_BDA_NETWORK_TUNER);

	while (enum_next(penumBDATuners) == S_OK)
	{	
		enum_get_name(penumBDATuners);
        if (tuners.find(penumBDATuners->szDevicePathW) != tuners.end())
        {
            tuners[penumBDATuners->szDevicePathW].name_ = penumBDATuners->szName;

		    IBaseFilter* pfilter = NULL;
		    if (enum_get_filter(penumBDATuners, &pfilter) == S_OK) 
		    {
                unsigned long t = get_tuner_type_from_filter(pfilter);
                dvblink::adjust_qam_for_atsc(t);
			    tuners[penumBDATuners->szDevicePathW].types_ = t;

			    pfilter->Release();
		    }
		    log_info(L"BDA get_tuner_type. Detected type(s) %1% for device %2%") % tuners[penumBDATuners->szDevicePathW].types_ % penumBDATuners->szDevicePathW;
        }
	}

	enum_free(penumBDATuners);	
}

static bool get_tuner_device_instance_from_attr(const wchar_t* path, const wchar_t* attr, std::wstring& device_instance_path)
{
	bool ret_val = false;
    device_instance_path.clear();

    std::wstring path_str = path;
    size_t pos_attr = path_str.find(attr);
    if (pos_attr != std::wstring::npos)
    {
        size_t pos_start = path_str.rfind(L"\\", pos_attr);
        size_t pos_end = path_str.find(L"{", pos_attr);
        if (pos_start != std::wstring::npos && pos_end != std::wstring::npos)
        {
            device_instance_path = path_str.substr(pos_start + 1, pos_end - pos_start - 1);
            ret_val = true;
        }
    }

    return ret_val;
}

static bool get_tuner_identity_attr(const wchar_t* path, const wchar_t* attr, std::wstring& attr_value)
{
	bool ret_val = false;
	attr_value.clear();

    const wchar_t* str_to_find = attr;
    const wchar_t* ptr1 = wcsstr(path, str_to_find);
    if (ptr1 != NULL)
    {
        const wchar_t* ptr2_1 = wcsstr(ptr1, L"&");
		const wchar_t* ptr2_2 = wcsstr(ptr1, L"#");
		const wchar_t* ptr2_res = NULL;
		if (ptr2_1 != NULL && ptr2_2 != NULL)
			ptr2_res = ptr2_2 > ptr2_1 ? ptr2_1 : ptr2_2;
		else
		{
			ptr2_res = ptr2_1;
			if (ptr2_res == NULL)
				ptr2_res = ptr2_2;//can be NULL as well
		}
        if (ptr2_res != NULL)
		{
            attr_value.assign(ptr1+wcslen(str_to_find), ptr2_res - (ptr1 + wcslen(str_to_find)));
			ret_val = true;
		}
    }
	return ret_val;
}

bool parse_tuner_identity_from_path(const wchar_t* path, std::wstring& vid, std::wstring& pid, std::wstring& subsys, std::wstring& device_instance_path)
{
	vid.clear();
	pid.clear();
	subsys.clear();

    std::wstring buffer1;
	std::wstring buffer2;
	bool b1 = get_tuner_identity_attr(path, L"vid_", buffer1);
	bool b2 = get_tuner_identity_attr(path, L"pid_", buffer2);

	if (b1 && b2)
	{
		vid = buffer1;
		pid = buffer2;
        get_tuner_device_instance_from_attr(path, L"vid_", device_instance_path);
	} else
	{
		//try another way
        b1 = get_tuner_identity_attr(path, L"ven_", buffer1);
		if (b1)
			vid = buffer1;

        b2 = get_tuner_identity_attr(path, L"dev_", buffer1);
		if (b2)
			pid = buffer1;

        if (b1 && b2)
            get_tuner_device_instance_from_attr(path, L"dev_", device_instance_path);

		if (get_tuner_identity_attr(path, L"subsys_", buffer1))
			subsys = buffer1;
	}
    return vid.size() > 0 && pid.size() > 0;
}

void diseqc2str(unsigned char *cmd, int len, wchar_t *str)
{
	wchar_t s[255];	
	str[0] = L'\0';
		
	for(int i = 0; i < len; i++)  {
		swprintf_s(s, L"%0.2X ", cmd[i]);
		wcscat_s(str, 255, s);
	}		
}

HRESULT get_filter_property_set(IBaseFilter *pfilter, IKsPropertySet **pkspropertyset)
{
	return pfilter->QueryInterface(IID_IKsPropertySet, (void **) pkspropertyset);
}

// -1  = ALL
//  0  = PINDIR_INPUT 
//  1  = PINDIR_OUTPUT
HRESULT get_pin_property_set(IBaseFilter *pfilter, int sdir, IKsPropertySet **pkspropertyset)
{
	HRESULT hr = E_FAIL;
	*pkspropertyset = NULL;
	IEnumPins* pPinEnum = NULL;
	pfilter->EnumPins(&pPinEnum);
	if (pPinEnum)
	{
		IPin* pPin=NULL;
		while (!(*pkspropertyset) && SUCCEEDED(pPinEnum->Next(1, &pPin, NULL)))
		{
			PIN_DIRECTION dir;
			if (SUCCEEDED(pPin->QueryDirection(&dir)) && ((sdir==0 && dir==PINDIR_INPUT) || (sdir==1 && dir==PINDIR_OUTPUT) || (sdir==-1))  )		
			{						
				hr = pPin->QueryInterface(IID_IKsPropertySet, (void **) pkspropertyset);										
			}
			pPin->Release();
		}
		pPinEnum->Release();
	}

	return hr;
}
