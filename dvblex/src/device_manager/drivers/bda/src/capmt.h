/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <vector>

enum E_CAPMT_LIST_MGMT
{
	CAPMT_LIST_MORE = 0,
	CAPMT_LIST_FIRST = 1,
	CAPMT_LIST_LAST = 2,
	CAPMT_LIST_ONLY = 3,
	CAPMT_LIST_ADD = 4,
	CAPMT_LIST_UPDATE = 5
};

enum E_CAPMT_CMD_ID
{
	CAPMT_CMD_OK_DESCRAMBLING = 1,
	CAPMT_CMD_OK_MMI = 2,
	CAPMT_CMD_QUERY = 3,
	CAPMT_CMD_NOT_SELECTED = 4
};

bool CreateCAPMTFromPMT(unsigned char* pmt, int pmt_len, E_CAPMT_LIST_MGMT listmgt, 
						E_CAPMT_CMD_ID cmd, unsigned char* ca_pmt, int& ca_pmt_len);
