/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#ifndef __IBDA_DiseqCommand_FWD_DEFINED__
#define __IBDA_DiseqCommand_FWD_DEFINED__
typedef interface IBDA_DiseqCommand IBDA_DiseqCommand;
#endif 	/* __IBDA_DiseqCommand_FWD_DEFINED__ */

#ifndef __IBDA_DiseqCommand_INTERFACE_DEFINED__
#define __IBDA_DiseqCommand_INTERFACE_DEFINED__

/* interface IBDA_DiseqCommand */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IBDA_DiseqCommand;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F84E2AB0-3C6B-45e3-A0FC-8669D4B81F11")
    IBDA_DiseqCommand : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE put_EnableDiseqCommands( 
            /* [in] */ BOOLEAN bEnable) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE put_DiseqLNBSource( 
            /* [in] */ ULONG ulLNBSource) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE put_DiseqUseToneBurst( 
            /* [in] */ BOOLEAN bUseToneBurst) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE put_DiseqRepeats( 
            /* [in] */ ULONG ulRepeats) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE put_DiseqSendCommand( 
            /* [in] */ ULONG ulRequestId,
            /* [in] */ ULONG ulcbCommandLen,
            /* [size_is][in] */ __RPC__in_ecount_full(ulcbCommandLen) BYTE *pbCommand) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE get_DiseqResponse( 
            /* [in] */ ULONG ulRequestId,
            /* [out][in] */ __RPC__inout ULONG *pulcbResponseLen,
            /* [size_is][out][in] */ __RPC__inout_ecount_full(*pulcbResponseLen) BYTE pbResponse[  ]) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IBDA_DiseqCommandVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            __RPC__in IBDA_DiseqCommand * This,
            /* [in] */ __RPC__in REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            __RPC__in IBDA_DiseqCommand * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            __RPC__in IBDA_DiseqCommand * This);
        
        HRESULT ( STDMETHODCALLTYPE *put_EnableDiseqCommands )( 
            __RPC__in IBDA_DiseqCommand * This,
            /* [in] */ BOOLEAN bEnable);
        
        HRESULT ( STDMETHODCALLTYPE *put_DiseqLNBSource )( 
            __RPC__in IBDA_DiseqCommand * This,
            /* [in] */ ULONG ulLNBSource);
        
        HRESULT ( STDMETHODCALLTYPE *put_DiseqUseToneBurst )( 
            __RPC__in IBDA_DiseqCommand * This,
            /* [in] */ BOOLEAN bUseToneBurst);
        
        HRESULT ( STDMETHODCALLTYPE *put_DiseqRepeats )( 
            __RPC__in IBDA_DiseqCommand * This,
            /* [in] */ ULONG ulRepeats);
        
        HRESULT ( STDMETHODCALLTYPE *put_DiseqSendCommand )( 
            __RPC__in IBDA_DiseqCommand * This,
            /* [in] */ ULONG ulRequestId,
            /* [in] */ ULONG ulcbCommandLen,
            /* [size_is][in] */ __RPC__in_ecount_full(ulcbCommandLen) BYTE *pbCommand);
        
        HRESULT ( STDMETHODCALLTYPE *get_DiseqResponse )( 
            __RPC__in IBDA_DiseqCommand * This,
            /* [in] */ ULONG ulRequestId,
            /* [out][in] */ __RPC__inout ULONG *pulcbResponseLen,
            /* [size_is][out][in] */ __RPC__inout_ecount_full(*pulcbResponseLen) BYTE pbResponse[  ]);
        
        END_INTERFACE
    } IBDA_DiseqCommandVtbl;

    interface IBDA_DiseqCommand
    {
        CONST_VTBL struct IBDA_DiseqCommandVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IBDA_DiseqCommand_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IBDA_DiseqCommand_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IBDA_DiseqCommand_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IBDA_DiseqCommand_put_EnableDiseqCommands(This,bEnable)	\
    ( (This)->lpVtbl -> put_EnableDiseqCommands(This,bEnable) ) 

#define IBDA_DiseqCommand_put_DiseqLNBSource(This,ulLNBSource)	\
    ( (This)->lpVtbl -> put_DiseqLNBSource(This,ulLNBSource) ) 

#define IBDA_DiseqCommand_put_DiseqUseToneBurst(This,bUseToneBurst)	\
    ( (This)->lpVtbl -> put_DiseqUseToneBurst(This,bUseToneBurst) ) 

#define IBDA_DiseqCommand_put_DiseqRepeats(This,ulRepeats)	\
    ( (This)->lpVtbl -> put_DiseqRepeats(This,ulRepeats) ) 

#define IBDA_DiseqCommand_put_DiseqSendCommand(This,ulRequestId,ulcbCommandLen,pbCommand)	\
    ( (This)->lpVtbl -> put_DiseqSendCommand(This,ulRequestId,ulcbCommandLen,pbCommand) ) 

#define IBDA_DiseqCommand_get_DiseqResponse(This,ulRequestId,pulcbResponseLen,pbResponse)	\
    ( (This)->lpVtbl -> get_DiseqResponse(This,ulRequestId,pulcbResponseLen,pbResponse) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IBDA_DiseqCommand_INTERFACE_DEFINED__ */
