/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include "../../ds_propertyset_base.h"

#define NOVAS2_TUNER_FILTER_NAME  L"Hauppauge WinTV 88x DVB-S/S2 Tuner/Demod"
#define NOVAS_TUNER_FILTER_NAME  L"Hauppauge WinTV 88x DVB-S Tuner/Demod"
#define NOVAS2_CAPTURE_FILTER_NAME  L"Hauppauge WinTV 88x TS Capture"

class novas2_set_base_t : public property_set_base_t
{
public:
    novas2_set_base_t();

    virtual HRESULT property_set_init();
    virtual HRESULT property_set_deinit();
    virtual HRESULT property_set_send_diseqc(unsigned char *cmd, int len);
    virtual HRESULT property_set_set_modulation_params(dvblink::PTransponderInfo Tp);
    virtual ModulationType property_set_get_dvbs_modulation(int mod);
protected:
    IKsPropertySet *ppropsetTunerInput;
    IKsPropertySet *ppropsetTunerOutput;

    HRESULT HAUPPAUGECTRL_SetPilotMode(IKsPropertySet *ppropset, int pilot);
    HRESULT HAUPPAUGECTRL_SetRollOffMode(IKsPropertySet *ppropset, int rolloff);
    HRESULT HAUPPAUGECTRL_SetFEC(IKsPropertySet *ppropset, int fec);
    HRESULT HAUPPAUGECTRL_SetModulation(IKsPropertySet *ppropset, int mod);
    HRESULT HAUPPAUGEDCTRL_SendDiseqc(IKsPropertySet *ppropset, unsigned char *cmd, int len);


};

