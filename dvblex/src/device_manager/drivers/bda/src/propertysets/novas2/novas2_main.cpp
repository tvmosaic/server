/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>

#include <drivers/deviceapi.h>
#include "../../ds_propertyset.h"
#include "../../ds_tunerequest.h"
#include "../../common.h"		
#include "novas2_main.h"
#include <dl_logger.h>

#define AddLog(x) dvblink::logging::log_warning(x)

using namespace dvblink;

// #define HAUPPAUGE_DEBUG

// Hauppauge property set
static const GUID KSPROPSETID_NOVAS2        = { 0xFAA8F3E5, 0x31D4, 0x4E41, {0x88, 0xEF, 0x00, 0xA0, 0xC9, 0xF2, 0x1F, 0xC7} };


// Hauppauge properties
#define KSPROPERTY_DISEQC				0x00  // ?  diseqc_params len = 188
#define KSPROPERTY_PILOT				0x20
#define KSPROPERTY_ROLLOFF				0x21

// Hauppauge roll-off values
#define CL_ROLLOFF_02		1
#define CL_ROLLOFF_025		2
#define CL_ROLLOFF_035		3


// test (?)
static const BYTE data1[32]     = {0x6b, 0x5c, 0xf1, 0x60, 0x0c, 0xd8, 0x86, 0x06, 0x84, 0xea, 0xbd, 0x0f, 0x8c, 0x55, 0x48, 0x06, 0xff, 0xff, 0xff, 0xff, 0x14, 0x99, 0x22, 0x06, 0x40, 0xa5, 0x51, 0x06, 0x02};
static const BYTE data2[32]     = {0x32, 0x07, 0x91, 0x7c, 0x02, 0x00, 0x00, 0x00, 0xe8, 0x06, 0x36, 0x00, 0xbb, 0xc7, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4c, 0xd5, 0x12, 0x00, 0xf8, 0xff, 0xff, 0xff, 0x90};
static const BYTE dinstance[32] = {0x6d, 0x05, 0x91, 0x7c, 0x3f, 0x00, 0x00, 0x00, 0xd0, 0xc4, 0xaf, 0x06, 0x0f, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
static BYTE disq[188] = {0};


// diseqc structure
typedef struct {
	BYTE LNB_Cmd[176];
	DWORD len;
	DWORD unk1;
	DWORD unk2;
} TNovaDisqInst, *PNovaDisqInst;


typedef struct {
	BYTE test[32];
} TNovaModInst, *PNovaModInst;

novas2_set_base_t::novas2_set_base_t() :
    ppropsetTunerInput(NULL), ppropsetTunerOutput(NULL)
{
}

HRESULT novas2_set_base_t::property_set_init()
{
	HRESULT hr;

	// get the property sets on the tuner filter pins
	hr = get_pin_property_set(graph_info_.pfltTuner_, 0, &ppropsetTunerInput);
	if FAILED(hr) 
		AddLog(L"WARNING: No propertyset interface on the input pin of the tuner filter");

	hr = get_pin_property_set(graph_info_.pfltTuner_, 1, &ppropsetTunerOutput);
	if FAILED(hr) 
		AddLog(L"WARNING: No propertyset interface on the output pin of the tuner filter");

	// to get the property sets on the filter (not the pins of it),  use this:
	// get_filter_property_set()


	// Maybe NovaS2 uses the propertyset on the capture filter for some functions:
    // get_filter_property_set(pfltCapture, &ppropsetCapture)

	return hr;
}

HRESULT novas2_set_base_t::property_set_deinit()
{
	if(ppropsetTunerInput) {
		ppropsetTunerInput->Release();
		ppropsetTunerInput = NULL;
	}

	if(ppropsetTunerOutput) {
		ppropsetTunerOutput->Release();
		ppropsetTunerOutput = NULL;
	}
	return S_OK;
}


HRESULT novas2_set_base_t::HAUPPAUGECTRL_SetPilotMode(IKsPropertySet *ppropset, int pilot)
{
	#ifdef HAUPPAUGE_DEBUG
	AddLog(L"HAUPPAUGECTRL_SetPilotMode(%p, %d)", ppropset, pilot);
	#endif

	DWORD TypeSupport=0;
	HRESULT hr = ppropset->QuerySupported(KSPROPSETID_NOVAS2,
									KSPROPERTY_PILOT, 
									&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			AddLog(L"FAILED: NovaS2 KSPROPERTY_PILOT not supported!");			
			return hr;
	}

	TNovaModInst instance;
	ZeroMemory(&instance, sizeof(instance));

	DWORD data;
	data = pilot;

	hr = ppropset->Set(KSPROPSETID_NOVAS2,
									KSPROPERTY_PILOT,
									&instance,
									sizeof(instance),
									&data,
									sizeof(data));


	if FAILED(hr)  {
			AddLog(L"FAILED: NovaS2 KSPROPERTY_PILOT failed!");			
			return hr;
	}

	return hr;
}

HRESULT novas2_set_base_t::HAUPPAUGECTRL_SetRollOffMode(IKsPropertySet *ppropset, int rolloff)
{
	#ifdef HAUPPAUGE_DEBUG
	AddLog(L"HAUPPAUGECTRL_SetRollOffMode(%p, %d)", ppropset, rolloff);
	#endif

	DWORD TypeSupport=0;
	HRESULT hr = ppropset->QuerySupported(KSPROPSETID_NOVAS2,
									KSPROPERTY_ROLLOFF, 
									&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			AddLog(L"FAILED: NovaS2 KSPROPERTY_PILOT not supported!");			
			return hr;
	}

	TNovaModInst instance;
	ZeroMemory(&instance, sizeof(instance));

	DWORD data;
	data = rolloff;

	hr = ppropset->Set(KSPROPSETID_NOVAS2,
									KSPROPERTY_ROLLOFF,
									&instance,
									sizeof(instance),
									&data,
									sizeof(data));


	if FAILED(hr)  {
			AddLog(L"FAILED: NovaS2 KSPROPERTY_PILOT failed!");			
			return hr;
	}

	return hr;
}

HRESULT novas2_set_base_t::HAUPPAUGECTRL_SetFEC(IKsPropertySet *ppropset, int fec)
{
	#ifdef HAUPPAUGE_DEBUG
	AddLog(L"HAUPPAUGECTRL_SetFEC(%p, %d)", ppropset, fec);
	#endif

	DWORD TypeSupport=0;
	HRESULT hr = ppropset->QuerySupported(KSPROPSETID_BdaDigitalDemodulator,
									KSPROPERTY_BDA_INNER_FEC_RATE, 
									&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			AddLog(L"FAILED: NovaS2 KSPROPERTY_BDA_INNER_FEC_RATE not supported!");			
			return hr;
	}

	TNovaModInst instance;
	ZeroMemory(&instance, sizeof(instance));

	DWORD data;
	data = fec;

	hr = ppropset->Set(KSPROPSETID_BdaDigitalDemodulator,
									KSPROPERTY_BDA_INNER_FEC_RATE,
									&instance,
									sizeof(instance),
									&data,
									sizeof(data));


	if FAILED(hr)  {
			AddLog(L"FAILED: NovaS2 KSPROPERTY_BDA_INNER_FEC_RATE failed!");			
			return hr;
	}

	return hr;
}

HRESULT novas2_set_base_t::HAUPPAUGECTRL_SetModulation(IKsPropertySet *ppropset, int mod)
{
	#ifdef HAUPPAUGE_DEBUG
	AddLog(L"HAUPPAUGECTRL_SetModulation(%p, %d)", ppropset, mod);
	#endif

	DWORD TypeSupport=0;
	HRESULT hr = ppropset->QuerySupported(KSPROPSETID_BdaDigitalDemodulator,
									KSPROPERTY_BDA_MODULATION_TYPE, 
									&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			AddLog(L"FAILED: NovaS2 KSPROPERTY_BDA_MODULATION_TYPE not supported!");			
			return hr;
	}

	TNovaModInst instance;
	ZeroMemory(&instance, sizeof(instance));

	DWORD data;
	data = mod;

	hr = ppropset->Set(KSPROPSETID_BdaDigitalDemodulator, 
									KSPROPERTY_BDA_MODULATION_TYPE,
									&instance,
									sizeof(instance),
									&data,
									sizeof(data));


	if FAILED(hr)  {
			AddLog(L"FAILED: NovaS2 KSPROPERTY_BDA_MODULATION_TYPE failed!");			
			return hr;
	}

	return hr;
}


ModulationType novas2_set_base_t::property_set_get_dvbs_modulation(int mod)
{
	switch (mod) {
		case MOD_DVBS_QPSK:
			return BDA_MOD_QPSK;
			break;
		case MOD_DVBS_8PSK:
			return (ModulationType)27;  // old SDK's BDA_MOD_MAX value      specific to nova s2
			break;
		case MOD_DVBS_16APSK:  
			return BDA_MOD_16APSK;
			break;
		case MOD_DVBS_32APSK:  
			return BDA_MOD_32APSK;
			break;
		case MOD_TURBO_8PSK:  // just for test
			return BDA_MOD_8PSK;
			break;
		case MOD_DVBS_NBC_QPSK: 
			return BDA_MOD_BPSK;  // specific to nova s2
			break; 
		default:
			return BDA_MOD_QPSK; 
			break;
	}
}



HRESULT novas2_set_base_t::HAUPPAUGEDCTRL_SendDiseqc(IKsPropertySet *ppropset, unsigned char *cmd, int len)
{
	#ifdef HAUPPAUGE_DEBUG
	AddLog(L"HAUPPAUGECTRL_SetModulation(%p, %X %X %X %X, %d)", ppropset, cmd[0], cmd[1], cmd[2], cmd[3], len);
	#endif
	

	DWORD TypeSupport=0;
	HRESULT hr = ppropset->QuerySupported(KSPROPSETID_NOVAS2,
							KSPROPERTY_DISEQC, 
							&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			AddLog(L"FAILED: NovaS2 KSPROPERTY_DISEQC not supported!");			
			return E_FAIL;
	}
	
	BYTE instance[32];
	ZeroMemory(&instance, sizeof(instance));			
	
	TNovaDisqInst disq;
	ZeroMemory(&disq, sizeof(disq));

	/*	disq.LNB_Cmd[0]=0xE0;
	disq.LNB_Cmd[1]=0x10;
	disq.LNB_Cmd[2]=0x38;
	disq.LNB_Cmd[3]=0xF2;
	disq.len       =4; */
	disq.unk1	   = 3;	
	disq.len       = len;

	CopyMemory(&disq.LNB_Cmd[0], cmd, min(sizeof(disq.LNB_Cmd), len));
	CopyMemory(&instance,&dinstance,32);

	hr = ppropset->Set(KSPROPSETID_NOVAS2,
									KSPROPERTY_DISEQC,
									&instance,
									sizeof(instance),
									&disq,
									sizeof(disq));
	if FAILED(hr)  {
			AddLog(L"FAILED: NovaS2 KSPROPERTY_DISEQC failed!");			
			return hr;
	}
	return hr;
}





HRESULT novas2_set_base_t::property_set_set_modulation_params(PTransponderInfo Tp)
{
	HAUPPAUGECTRL_SetPilotMode(ppropsetTunerInput, -1);	
	HAUPPAUGECTRL_SetRollOffMode(ppropsetTunerInput, -1);	
	HAUPPAUGECTRL_SetModulation(ppropsetTunerOutput, property_set_get_dvbs_modulation(Tp->dwModulation));
	HAUPPAUGECTRL_SetFEC(ppropsetTunerOutput, bda_standard_get_fec(Tp->dwFec));
	return S_OK;
}


HRESULT novas2_set_base_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
	return HAUPPAUGEDCTRL_SendDiseqc(ppropsetTunerInput, cmd, len);
}