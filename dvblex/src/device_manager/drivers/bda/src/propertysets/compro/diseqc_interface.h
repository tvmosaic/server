/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#ifndef		HEADER_DISEQC_INTERFACE_H
#define		HEADER_DISEQC_INTERFACE_H

#pragma once

// GUID and structs for DiSEqC control
#define STORAGESIZE			10
#define DISEQCSWITCHSIZE	4

#define DISEQCSWITCHMODE_NONE		0
#define DISEQCSWITCHMODE_SINGLE		1
#define DISEQCSWITCHMODE_22K		2
#define DISEQCSWITCHMODE_MINI		3
#define DISEQCSWITCHMODE_DISEQC_10	4
#define DISEQCSWITCHMODE_DISEQC_12	5


// GUID of DiSEqC Property
// {0c12bf87-5bc0-4dda-9d07-21e5c2f3b9ae}
static const GUID VAMP_DVBS_DISEQC_ACCESS_PROPERTY = 
{ 0x0c12bf87, 0x5bc0, 0x4dda, { 0x9d, 0x07, 0x21, 0xe5, 0xc2, 0xf3, 0xb9, 0xae } };

// GUID of MAC Address Property
// {A1AA3F96-02EA-4ccb-A714-00BCD398ADB4}
static const GUID VAMP_DVBS_MAC_ADDRESS_PROPERTY = 
{ 0xa1aa3f96, 0x2ea, 0x4ccb, { 0xa7, 0x14, 0x0, 0xbc, 0xd3, 0x98, 0xad, 0xb4 } };


// Property IDs for DiSEqC
#define	VAMP_DVBS_DISEQC_PROP_ID_COMMAND		0
#define	VAMP_DVBS_DISEQC_PROP_ID_RAW_COMMAND	1
#define	VAMP_DVBS_DISEQC_PROP_ID_ENABLE_22K		2

// DiSEqC framing byte, same as spec.
typedef enum
{
	DISEQC_MASTER_NOREPLY_FIRST			= 0xE0,		// Command from Master, No reply required, First transmission
	DISEQC_MASTER_NOREPLY_REPEATED		= 0xE1,		// Command from Master, No reply required, Repeated transmission
	DISEQC_MASTER_REPLY_FIRST			= 0xE2,		// Command from Master, Reply required, First transmission
	DISEQC_MASTER_REPLY_REPEATED		= 0xE3,		// Command from Master, Reply required, Repeated transmission
	DISEQC_SLAVE_REPLAY_OK				= 0xE4,		// Reply from Slave, ��OK��, no errors detected
	DISEQC_SLAVE_REPLAY_UNSUPPORTED		= 0xE5,		// Reply from Slave, Command not supported by slave
	DISEQC_SLAVE_REPLAY_ERROR			= 0xE6,		// Reply from Slave, Parity Error detected - Request repeat
	DISEQC_SLAVE_REPLAY_UNRECOGNISED	= 0xE7,		// Reply from Slave, Command not recognised - Request repeat
} tmDiSEqCFraming_t, *ptmDiSEqCFraming_t;

// DiSEqC address byte, same as spec.
typedef enum
{
	DISEQC_TO_ALL_DEVICE				= 0x00,		// Any Device (Master to all devices)
	DISEQC_TO_LNB_SWITCHER_SMATV		= 0x10,		// Any LNB, Switcher or SMATV (Master to all...)
	DISEQC_TO_LNB						= 0x11,		// LNB
	DISEQC_TO_LNB_WITH_SWITCH			= 0x12,		// LNB with Loop-through switching
	DISEQC_TO_SWITCHER_WITHOUT_DC		= 0x14,		// Switcher (d.c. blocking)
	DISEQC_TO_SWITCHER_WITH_DC			= 0x15,		// Switcher with d.c. Loop-through
	DISEQC_TO_SMATV						= 0x18,		// SMATV
	DISEQC_ANY_POLAR					= 0x20,		// Any Polariser (Master to all Polarisers)
	DISEQC_LINEAR_POLAR					= 0x21,		// Linear Polarisation (Skew) Controller
	DISEQC_ANY_POS						= 0x30,		// Any Positioner (Master to all Positioners)
	DISEQC_POLAR_POS					= 0x31,		// Polar / Azimuth Positioner
	DISEQC_ELEVATION_POS				= 0x32,		// Elevation Positioner
	DISEQC_ANY_AID						= 0x40,		// Any Installer Aid (Master to all Installer Aids)
	DISEQC_SIGNAL_STRENGTH				= 0x41,		// Signal-strength analogue value
//	DISEQC_FAMILY_ADDRESS_REALLOCATION	= 0x6X,		// Family reserved for address re-allocations
	DISEQC_ANY_SLAVE					= 0x70,		// Any Intelligent Slave Interfaces (Master to all)
	DISEQC_SUBSCRIBER_INTERFACE			= 0x71,		// Interface for subscriber controlled headends
//	DISEQC_RESERVED_OEM					= 0xFX,		// Reserved for OEM Extensions
} tmDiSEqCAddress_t, *ptmDiSEqCAddress_t;

// DiSEqC command byte, same as spec.
typedef enum
{
	DISEQC_RESET						= 0x00,		// Reset DiSEqC microcontroller
	DISEQC_STANDBY						= 0x02,		// Switch peripheral power supply off
	DISEQC_POWER_ON						= 0x03,		// Switch peripheral power supply on
	DISEQC_SET_LO						= 0x20,		// Select the Low Local Oscillator frequency
	DISEQC_SET_VR						= 0x21,		// Select Vertical Polarisation (or Right circular)
	DISEQC_SET_POS_A					= 0x22,		// Select Satellite position A (or position C)
	DISEQC_SET_S0A						= 0x23,		// Select Switch Option A (e.g. positions A/B)
	DISEQC_SET_HI						= 0x24,		// Select the High Local Oscillator frequency
	DISEQC_SET_HL						= 0x25,		// Select Horizontal Polarisation (or Left circular)
	DISEQC_SET_POS_B					= 0x26,		// Select Satellite position B (or position D)
	DISEQC_SET_S0B						= 0x27,		// Select Switch Option B (e.g. positions C/D)
	DISEQC_SET_S1A						= 0x28,		// Select switch S1 input A (deselect input B)
	DISEQC_SET_S2A						= 0x29,		// Select switch S2 input A (deselect input B)
	DISEQC_SET_S3A						= 0x2A,		// Select switch S3 input A (deselect input B)
	DISEQC_SET_S4A						= 0x2B,		// Select switch S4 input A (deselect input B)
	DISEQC_SET_S1B						= 0x2C,		// Select switch S1 input B (deselect input A)
	DISEQC_SET_S2B						= 0x2D,		// Select switch S2 input B (deselect input A)
	DISEQC_SET_S3B						= 0x2E,		// Select switch S3 input B (deselect input A)
	DISEQC_SET_S4B						= 0x2F,		// Select switch S4 input B (deselect input A)
	DISEQC_SET_SWITCH_PORT				= 0x38,		// Set DiSEqC switch port
	DISEQC_DRIVE_HALT					= 0x60,		// Stop Positioner movement
	DISEQC_LIMITS_OFF					= 0x63,		// Disable Limits
	DISEQC_LIMIT_EAST					= 0x66,		// Set East Limit (& Enable recommended)
	DISEQC_LIMIT_WEST					= 0x67,		// Set West Limit (& Enable recommended)
	DISEQC_DRIVE_EAST					= 0x68,		// Drive Motor East (with optional timeout/steps), dwData = Timeout in second
	DISEQC_DRIVE_WEST					= 0x69,		// Drive Motor West (with optional timeout/steps), dwData = Timeout in second
//Positive (non-zero) values of the parameter byte (i.e. 01 - 7F hexadecimal) indicate a "timeout" value in seconds
//Negative (non-zero) values of the parameter byte (i.e. 80 - FF hexadecimal) indicate a "steps" value
	DISEQC_STORE_NN						= 0x6A,		// Store Satellite Position & Enable Limits, dwData = Position
	DISEQC_GOTO_NN						= 0x6B,		// Drive Motor to Satellite Position nn, dwData = Position, Position 0 = Reference Position
	DISEQC_GOTO_XX						= 0x6E,		// Drive Motor to Angular Position (�X)
	DISEQC_SET_POSNS					= 0x6F,		// (Re-) Calculate Satellite Positions
} tmDiSEqCCommand_t, *ptmDiSEqCCommand_t;


static struct
{
	UCHAR ucFraming;
	UCHAR ucAddress;
	UCHAR ucCommand;
	UCHAR ucLength; 
}DiSEqCCommand[] =
{
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_RESET,			3,		// Reset DiSEqC microcontroller
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_TO_LNB_SWITCHER_SMATV,	DISEQC_STANDBY,			3,		// Switch peripheral power supply off
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_TO_LNB_SWITCHER_SMATV,	DISEQC_POWER_ON,		3,		// Switch peripheral power supply on
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_SET_LO,			3,		// Select the Low Local Oscillator frequency
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_SET_VR,			3,		// Select Vertical Polarisation (or Right circular)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_TO_LNB_SWITCHER_SMATV,	DISEQC_SET_POS_A,		3,		// Select Satellite position A (or position C)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_TO_LNB_SWITCHER_SMATV,	DISEQC_SET_S0A,			3,		// Select Switch Option A (e.g. positions A/B)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_SET_HI,			3,		// Select the High Local Oscillator frequency
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_SET_HL,			3,		// Select Horizontal Polarisation (or Left circular)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_TO_LNB_SWITCHER_SMATV,	DISEQC_SET_POS_B,		3,		// Select Satellite position B (or position D)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_TO_LNB_SWITCHER_SMATV,	DISEQC_SET_S0B,			3,		// Select Switch Option B (e.g. positions C/D)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_SET_S1A,			3,		// Select switch S1 input A (deselect input B)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_SET_S2A,			3,		// Select switch S2 input A (deselect input B)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_SET_S3A,			3,		// Select switch S3 input A (deselect input B)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_SET_S4A,			3,		// Select switch S4 input A (deselect input B)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_SET_S1B,			3,		// Select switch S1 input B (deselect input A)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_SET_S2B,			3,		// Select switch S2 input B (deselect input A)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_SET_S3B,			3,		// Select switch S3 input B (deselect input A)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_SET_S4B,			3,		// Select switch S4 input B (deselect input A)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_TO_LNB_SWITCHER_SMATV,	DISEQC_SET_SWITCH_PORT,	4,		// Set DiSEqC switch port
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_DRIVE_HALT,		3,		// Stop Positioner movement
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_LIMITS_OFF,		3,		// Disable Limits
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_LIMIT_EAST,		3,		// Set East Limit (& Enable recommended)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_LIMIT_WEST,		3,		// Set West Limit (& Enable recommended)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_DRIVE_EAST,		4,		// Drive Motor East (with optional timeout/steps), dwData = Timeout in second
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_DRIVE_WEST,		4,		// Drive Motor West (with optional timeout/steps), dwData = Timeout in second
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_STORE_NN,		4,		// Store Satellite Position & Enable Limits, dwData = Position
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_GOTO_NN,			4,		// Drive Motor to Satellite Position nn, dwData = Position, Position 0 = Reference Position
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_GOTO_XX,			5,		// Drive Motor to Angular Position (�X)
	DISEQC_MASTER_NOREPLY_FIRST,	DISEQC_POLAR_POS,				DISEQC_SET_POSNS,		6,		// (Re-) Calculate Satellite Positions
};

// Enable / Disable 22kHz Tone
typedef enum
{
	L22KHZ_TONE_OFF			= 0,
	L22KHZ_TONE_ON		= 1
} tm22kHzTone_t, *ptm22kHzTone_t;

// Enable / Disable LNB Power
typedef enum
{
	LNB_POWER_OFF			= 2,
	LNB_POWER_ON		= 3
} tmLNBPower_t, *ptmLNBPower_t;

// for VAMP_DVBS_DISEQC_PROP_ID_COMMAND
typedef struct
{
    UINT32     dwCommand;			// requested command
} tDiSEqCRequest, *ptDiSEqCRequest;

typedef struct
{
    UINT32      dwData;				// command data
} tDiSEqCData, *ptDiSEqCData;

// for VAMP_DVBS_DISEQC_PROP_ID_RAW_COMMAND
typedef struct
{
    UINT32     dwRawCommandLength;	// command length, max = 8
} tDiSEqCRawRequest, *ptDiSEqCRawRequest;

typedef struct
{
	UCHAR		ucData[8];			// command sequence
} tDiSEqCRawData, *ptDiSEqCRawData;

// for VAMP_DVBS_DISEQC_PROP_ID_ENABLE_22K
typedef struct
{
	UCHAR		ucEnable;			// enable / disable
} tDiSEqCEnable22kTone, *ptDiSEqCEnable22kTone;

// for VAMP_DVBS_MAC_ADDRESS_PROPERTY, property ID = 0
typedef struct
{
    UCHAR      ucAddress[6];		// get value
} tMACAddData, *ptMACAddData;

#endif		HEADER_DISEQC_INTERFACE_H
/*
//-----------------------------------------------------------------------------------------------------------------------
Sample Code:
// Set DiSEqC raw command : 
			if (pbfBDA_DVBS_Tuner)// BDA source filter : KSCATEGORY_BDA_NETWORK_TUNER
			{
				tDiSEqCRawRequest DiSEqCRequest;
				tDiSEqCRawData DiSEqCData;

				ZeroMemory(&DiSEqCRequest, sizeof(DiSEqCRequest));
				ZeroMemory(&DiSEqCData, sizeof(DiSEqCData));

				DiSEqCCommand[i].ucCommand = DiSEqC Command;
				DiSEqCRequest.dwRawCommandLength = DiSEqCCommand[i].ucLength;
				DiSEqCData.ucData[0] = DISEQC_MASTER_NOREPLY_FIRST;
				DiSEqCData.ucData[1] = DISEQC_TO_LNB_SWITCHER_SMATV;
				DiSEqCData.ucData[2] = DISEQC_SET_SWITCH_PORT;
				DiSEqCData.ucData[3] = 0xF0; // DiSEqC command set values
				
				// query for interface
				IKsPropertySet *pIKsPropertySet = NULL;
				HRESULT hr = pbfBDA_DVBS_Tuner->QueryInterface(IID_IKsPropertySet,(void **)&pIKsPropertySet);
				if(SUCCEEDED(hr) && pIKsPropertySet)
				{
					hr = pIKsPropertySet->Set(VAMP_DVBS_DISEQC_ACCESS_PROPERTY,
												VAMP_DVBS_DISEQC_PROP_ID_RAW_COMMAND, 
												&DiSEqCRequest,
												sizeof(DiSEqCRequest) + sizeof(KSPROPERTY),
												&DiSEqCData,
												sizeof(DiSEqCData));

					pIKsPropertySet->Release();
					pIKsPropertySet = NULL;

					if(SUCCEEDED(hr))
					{
						// Succeed
					}
					else
					{
						// Fail
					}// end if
				}// end if
			}// end if


// Enable / Disable 22kHz Tone and Enable / Disable LNB Power
			if (m_pbfBDA_DVBS_Tuner)// BDA source filter : KSCATEGORY_BDA_NETWORK_TUNER
			{
				tDiSEqCEnable22kTone Enable22k;
				ZeroMemory(&Enable22k, sizeof(Enable22k));

				Enable22k.ucEnable = 22KHZ_TONE_OFF;// 22KHZ_TONE_OFF, 22KHZ_TONE_ON, LNB_POWER_OFF, LNB_POWER_ON

				// query for interface
				IKsPropertySet *pIKsPropertySet = NULL;
				HRESULT hr = m_pbfBDA_DVBS_Tuner->QueryInterface(IID_IKsPropertySet,(void **)&pIKsPropertySet);
				if(SUCCEEDED(hr) && pIKsPropertySet)
				{
					hr = pIKsPropertySet->Set(VAMP_DVBS_DISEQC_ACCESS_PROPERTY,
												VAMP_DVBS_DISEQC_PROP_ID_ENABLE_22K, 
												NULL,
												NULL,
												&Enable22k,
												sizeof(Enable22k));

					pIKsPropertySet->Release();
					pIKsPropertySet = NULL;

					if(SUCCEEDED(hr))
					{
						// Succeed
					}
					else
					{
						// Fail
					}// end if
				}// end if
			}// end if
//-----------------------------------------------------------------------------------------------------------------------
*/