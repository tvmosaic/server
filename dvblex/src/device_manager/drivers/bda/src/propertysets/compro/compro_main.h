/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include "../../ds_propertyset_base.h"

#define COMPRO_S350_TUNER_FILTER_NAME  L"VideoMate DVB-S DVBS Tuner"
#define COMPRO_S350_CAPTURE_FILTER_NAME  L"VideoMate DVB-S Digital Capture"

#define COMPRO_S500_TUNER_FILTER_NAME  L"VideoMate S500 DTV Tuner"
#define COMPRO_S500_CAPTURE_FILTER_NAME  L"VideoMate S500 Digital Video"

class compro_property_set_t : public property_set_base_t
{
public:
    compro_property_set_t();

    virtual HRESULT property_set_init();
    virtual HRESULT property_set_deinit();
    virtual HRESULT property_set_send_diseqc(unsigned char *cmd, int len);
    virtual HRESULT property_set_set_22khz(bool active);
    virtual HRESULT property_set_set_lnb_power(dvblink::DL_E_LNB_POWER_TYPES lnb_power);
protected:
    IKsPropertySet *ppropsetTunerFilter_Compro;
    bool bLNBPowerOff_compro;

    HRESULT COMPRO_PowerAnd22khz(int x);
};
