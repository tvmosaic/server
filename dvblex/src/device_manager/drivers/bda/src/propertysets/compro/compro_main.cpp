/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>

#include <drivers/deviceapi.h>
#include "DiSEqC_Interface.h"
#include "../../common.h"		
#include "compro_main.h"		

#include <dl_logger.h>

using namespace dvblink::logging;

compro_property_set_t::compro_property_set_t() :
    ppropsetTunerFilter_Compro(NULL), bLNBPowerOff_compro(false)
{
}

HRESULT compro_property_set_t::property_set_init()
{
	HRESULT hr;

	// get the property sets on the tuner filter
	hr = get_filter_property_set(graph_info_.pfltTuner_, &ppropsetTunerFilter_Compro);
	if FAILED(hr) 
		log_warning(L"No propertyset interface on the tuner filter");

	return hr;
}

HRESULT compro_property_set_t::property_set_deinit()
{
	if(ppropsetTunerFilter_Compro) {
		ppropsetTunerFilter_Compro->Release();
		ppropsetTunerFilter_Compro = NULL;
	}
	return S_OK;
}

HRESULT compro_property_set_t::COMPRO_PowerAnd22khz(int x)
{
	log_info(L"COMPRO_PowerAnd22khz(%d)") % x;
	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerFilter_Compro->QuerySupported(VAMP_DVBS_DISEQC_ACCESS_PROPERTY,
							VAMP_DVBS_DISEQC_PROP_ID_ENABLE_22K, 
							&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Compro VAMP_DVBS_DISEQC_PROP_ID_ENABLE_22K not supported!");			
			return E_FAIL;
	}

	tDiSEqCEnable22kTone Enable22k;
	ZeroMemory(&Enable22k, sizeof(Enable22k));
	Enable22k.ucEnable = x;//x; // L22KHZ_TONE_OFF, L22KHZ_TONE_ON, LNB_POWER_OFF, LNB_POWER_ON


	
	hr = ppropsetTunerFilter_Compro->Set(VAMP_DVBS_DISEQC_ACCESS_PROPERTY,
												VAMP_DVBS_DISEQC_PROP_ID_ENABLE_22K, 
												NULL,
												NULL,
												&Enable22k,
												sizeof(Enable22k));

	if FAILED(hr)	
	{
			log_error(L"Compro VAMP_DVBS_DISEQC_PROP_ID_ENABLE_22K failed!");			
			return hr;
	}


	return hr;
}



HRESULT compro_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{	
	log_info(L"compro_send_diseqc()");

	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerFilter_Compro->QuerySupported(VAMP_DVBS_DISEQC_ACCESS_PROPERTY,
							VAMP_DVBS_DISEQC_PROP_ID_RAW_COMMAND, 
							&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Compro VAMP_DVBS_DISEQC_PROP_ID_RAW_COMMAND not supported!");			
			return E_FAIL;
	}
	
	tDiSEqCRawRequest DiSEqCRequest;
	tDiSEqCRawData DiSEqCData;
	ZeroMemory(&DiSEqCRequest, sizeof(DiSEqCRequest));
	ZeroMemory(&DiSEqCData, sizeof(DiSEqCData));

	if (len > 8)
		len = 8;

	DiSEqCRequest.dwRawCommandLength = len;
	CopyMemory(&DiSEqCData.ucData[0], cmd, len);

	for (int i=0;i<5;i++) {

			
	hr = ppropsetTunerFilter_Compro->Set(VAMP_DVBS_DISEQC_ACCESS_PROPERTY, 
                        				VAMP_DVBS_DISEQC_PROP_ID_RAW_COMMAND, 
					      			  &DiSEqCRequest,sizeof(DiSEqCRequest) + sizeof(KSPROPERTY),
					        			&DiSEqCData, sizeof(DiSEqCData));
	Sleep(50);

	}

	if FAILED(hr)	
	{
			log_error(L"Compro VAMP_DVBS_DISEQC_PROP_ID_RAW_COMMAND failed!");			
			return hr;
	}

	return hr;
}


HRESULT compro_property_set_t::property_set_set_22khz(bool active)
{
//	if (bLNBPowerOff_compro) 
		return S_OK;

	//return COMPRO_PowerAnd22khz(active ? L22KHZ_TONE_ON : L22KHZ_TONE_OFF);
}

HRESULT compro_property_set_t::property_set_set_lnb_power(dvblink::DL_E_LNB_POWER_TYPES lnb_power)
{
	log_info(L"compro_set_lnb_power()");
    bLNBPowerOff_compro = (lnb_power == dvblink::POW_13V || lnb_power == dvblink::POW_18V);
	return COMPRO_PowerAnd22khz(bLNBPowerOff_compro ? LNB_POWER_ON : LNB_POWER_OFF);
}