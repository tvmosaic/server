/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#define HAUPPAUGE_NOVAS2_TUNER_FILTER_NAME  L"Hauppauge WinTV 88x DVB-S/S2 Tuner/Demod"
#define HAUPPAUGE_NOVAS_TUNER_FILTER_NAME  L"Hauppauge WinTV 88x DVB-S Tuner/Demod"
#define HAUPPAUGE_5500_TUNER_FILTER_NAME  L"Hauppauge WinTV 885 Alt BDA Tuner/Demod"

#define HAUPPAUGE_NOVAS_USB_TUNER_FILTER_NAME  L"Hauppauge 47xxx WinTV DVBS Tuner" // Hauppauge Nova-S-USB2

#define HAUPPAUGE_NOVAS2_CAPTURE_FILTER_NAME  L"Hauppauge WinTV 88x TS Capture"
#define HAUPPAUGE_NOVAS_CAPTURE_FILTER_NAME  L"Hauppauge WinTV 88x TS Capture"

#define HAUPPAUGE_VID   L"2040"
#define HAUPPAUGE_PCTV_VID   L"2013"

#define HAUPPAUGE_WINTV_SOLOHD_EU_PID   L"0264"
#define HAUPPAUGE_WINTV_DUALHD_EU_PID   L"0265"
#define HAUPPAUGE_WINTV_DUALHD_ISDBT_PID   L"0266"

#define HAUPPAUGE_WINTV_292E_BULK_PID   L"825F"
#define HAUPPAUGE_WINTV_292E_SE_BULK_PID   L"8264"
#define HAUPPAUGE_WINTV_2920E_BULK_PID   L"8265"

#define HAUPPAUGE_WINTV_SOLOHD_RETAIL_BULK_PID   L"8268"
#define HAUPPAUGE_WINTV_SOLOHD_OEM_BULK_PID   L"8264"
#define HAUPPAUGE_WINTV_DUALHD_RETAIL_BULK_PID   L"8265"
#define HAUPPAUGE_WINTV_DUALHD_OEMDN_BULK_PID   L"8269"
#define HAUPPAUGE_WINTV_DUALHD_OEMAZ_BULK_PID   L"8278"
#define HAUPPAUGE_WINTV_SOLOHD_RETAIL_ISDBT_BULK_PID   L"8266"
#define HAUPPAUGE_WINTV_SOLOHD_OEM_ISDBT_BULK_PID   L"826A"

#include "../../ds_propertyset_base.h"

bool hauppauge_tuner_requires_dvbc_sr_correction(property_set_base_t* prop_set);

class hauppauge_property_set_t : public property_set_base_t
{
public:
    hauppauge_property_set_t();

    virtual HRESULT property_set_init();
    virtual HRESULT property_set_deinit();
    virtual HRESULT property_set_send_diseqc(unsigned char *cmd, int len);
    virtual HRESULT property_set_send_toneburst(int type);
    virtual HRESULT property_set_set_modulation_params(dvblink::PTransponderInfo Tp);
    virtual ModulationType property_set_get_dvbs_modulation(int mod);
protected:
    IKsPropertySet *ppropsetTunerInput;
    IKsPropertySet *ppropsetTunerOutput;

    HRESULT HAUPPAUGECTRL_send_diseqc(unsigned char *cmd, int len, int ToneBurst);
};
