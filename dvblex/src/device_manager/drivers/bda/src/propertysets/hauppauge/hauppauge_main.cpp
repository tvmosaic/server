/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include <string.h>
#include <boost/algorithm/string.hpp>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>

#include <drivers/deviceapi.h>
#include "../../ds_propertyset.h"
#include "../../ds_tunerequest.h"
#include "../../common.h"		
#include <dl_dshow_enum.h>
#include <dl_logger.h>
#include "hauppauge_main.h"

using namespace dvblink::logging;
using namespace dvblink::engine;

bool hauppauge_tuner_requires_dvbc_sr_correction(property_set_base_t* prop_set)
{
	bool ret_val = false;

	if (prop_set != NULL)
	{
		if ((boost::iequals(prop_set->get_vid(), HAUPPAUGE_VID) &&
			(boost::iequals(prop_set->get_pid(), HAUPPAUGE_WINTV_SOLOHD_EU_PID) ||
			boost::iequals(prop_set->get_pid(), HAUPPAUGE_WINTV_DUALHD_EU_PID) ||
			boost::iequals(prop_set->get_pid(), HAUPPAUGE_WINTV_DUALHD_ISDBT_PID) ||
			boost::iequals(prop_set->get_pid(), HAUPPAUGE_WINTV_SOLOHD_RETAIL_BULK_PID) ||
			boost::iequals(prop_set->get_pid(), HAUPPAUGE_WINTV_SOLOHD_OEM_BULK_PID) ||
			boost::iequals(prop_set->get_pid(), HAUPPAUGE_WINTV_DUALHD_RETAIL_BULK_PID) ||
			boost::iequals(prop_set->get_pid(), HAUPPAUGE_WINTV_DUALHD_OEMDN_BULK_PID) ||
			boost::iequals(prop_set->get_pid(), HAUPPAUGE_WINTV_DUALHD_OEMAZ_BULK_PID) ||
			boost::iequals(prop_set->get_pid(), HAUPPAUGE_WINTV_SOLOHD_RETAIL_ISDBT_BULK_PID) ||
			boost::iequals(prop_set->get_pid(), HAUPPAUGE_WINTV_SOLOHD_OEM_ISDBT_BULK_PID))) ||
			(boost::iequals(prop_set->get_vid(), HAUPPAUGE_PCTV_VID) &&
			(boost::iequals(prop_set->get_pid(), HAUPPAUGE_WINTV_292E_BULK_PID) ||
			boost::iequals(prop_set->get_pid(), HAUPPAUGE_WINTV_292E_SE_BULK_PID) ||
			boost::iequals(prop_set->get_pid(), HAUPPAUGE_WINTV_2920E_BULK_PID))))
		{
			ret_val = true;
		}
	}
	return ret_val;
}

const GUID KSPROPSETID_BdaTunerExtensionProperties =
{0xfaa8f3e5, 0x31d4, 0x4e41, {0x88, 0xef, 0x00, 0xa0, 0xc9, 0xf2, 0x1f, 0xc7}};


typedef enum
{
 BDA_TUNER_NODE = 0,
 BDA_DEMODULATOR_NODE
}BDA_NODES;
 
typedef enum
{
 KSPROPERTY_HP_BDA_DISEQC = 0,
 KSPROPERTY_HP_BDA_PILOT = 0x20,
 KSPROPERTY_HP_BDA_ROLL_OFF = 0x21
} KSPROPERTY_HP_BDA_TUNER_EXTENSION;
 
typedef enum DiseqcVer
{  
 DISEQC_VER_1X=1,
 DISEQC_VER_2X,
 ECHOSTAR_LEGACY,       // (not supported)
 DISEQC_VER_UNDEF=0     // undefined (results in an error)
} DISEQC_VER;
 
typedef enum RxMode
{
 RXMODE_INTERROGATION=1, // Expecting multiple devices attached
 RXMODE_QUICKREPLY,      // Expecting 1 rx (rx is suspended after 1st rx received)
 RXMODE_NOREPLY,         // Expecting to receive no Rx message(s)
 RXMODE_DEFAULT=0        // use current register setting
} RXMODE;
 
const BYTE DISEQC_TX_BUFFER_SIZE = 150; // 3 bytes per message * 50 messages
const BYTE DISEQC_RX_BUFFER_SIZE = 8;      // reply fifo size, do not increase
 
typedef struct _DISEQC_MESSAGE_PARAMS
{
 UCHAR      uc_diseqc_send_message[DISEQC_TX_BUFFER_SIZE+1];
 UCHAR      uc_diseqc_receive_message[DISEQC_RX_BUFFER_SIZE+1];
 ULONG      ul_diseqc_send_message_length;
 ULONG      ul_diseqc_receive_message_length;
 ULONG      ul_amplitude_attenuation;
 BOOL       b_tone_burst_modulated;
 DISEQC_VER diseqc_version;
 RXMODE     receive_mode;
 BOOL       b_last_message;
} DISEQC_MESSAGE_PARAMS, *PDISEQC_MESSAGE_PARAMS;
 
typedef enum
{
 TONE_BURST_UNMODULATED = 0,
 TONE_BURST_MODULATED
} TONE_BURST_MODULATION_TYPE;

hauppauge_property_set_t::hauppauge_property_set_t() :    
    ppropsetTunerInput(NULL), ppropsetTunerOutput(NULL)
{
}


HRESULT hauppauge_property_set_t::property_set_init()
{
	HRESULT hr;

	// get the property sets on the tuner filter pins
	hr = get_pin_property_set(graph_info_.pfltTuner_, 0, &ppropsetTunerInput);
	if FAILED(hr) 
		log_warning(L"No propertyset interface on the input pin of the tuner filter");

	hr = get_pin_property_set(graph_info_.pfltTuner_, 1, &ppropsetTunerOutput);
	if FAILED(hr) 
		log_warning(L"No propertyset interface on the output pin of the tuner filter");

	// to get the property sets on the filter (not the pins of it),  use this:
	// get_filter_property_set()


	// Maybe hauppauge uses the propertyset on the capture filter for some functions:
    // get_filter_property_set(pfltCapture, &ppropsetCapture)

	return hr;
}

HRESULT hauppauge_property_set_t::property_set_deinit()
{
	if(ppropsetTunerInput) {
		ppropsetTunerInput->Release();
		ppropsetTunerInput = NULL;
	}

	if(ppropsetTunerOutput) {
		ppropsetTunerOutput->Release();
		ppropsetTunerOutput = NULL;
	}

	return S_OK;
}

HRESULT HAUPPAUGECTRL_SetPilotMode(IKsPropertySet *ppropset, int pilot)
{
	#ifdef HAUPPAUGE_DEBUG
	log_info(L"HAUPPAUGECTRL_SetPilotMode(%p, %d)") % ppropset % pilot;
	#endif

	DWORD TypeSupport=0;
	HRESULT hr = ppropset->QuerySupported(KSPROPSETID_BdaTunerExtensionProperties,
									KSPROPERTY_HP_BDA_PILOT, 
									&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"FAILED: hauppauge KSPROPERTY_PILOT not supported!");			
			return hr;
	}

	KSP_NODE instance_data;
	ZeroMemory(&instance_data, sizeof(instance_data));

	Pilot data;
	data = (Pilot)pilot;	

	hr = ppropset->Set(KSPROPSETID_BdaTunerExtensionProperties,
									KSPROPERTY_HP_BDA_PILOT,
									&instance_data,
									sizeof(instance_data),
									&data,
									sizeof(data));


	if FAILED(hr)  {
			log_error(L"hauppauge KSPROPERTY_HP_PILOT failed!");			
			return hr;
	}

	return hr;
}

HRESULT HAUPPAUGECTRL_SetRollOffMode(IKsPropertySet *ppropset, int rolloff)
{
	#ifdef HAUPPAUGE_DEBUG
	log_info(L"HAUPPAUGECTRL_SetRollOffMode(%p, %d)") % ppropset % rolloff;
	#endif

	DWORD TypeSupport=0;
	HRESULT hr = ppropset->QuerySupported(KSPROPSETID_BdaTunerExtensionProperties,
									KSPROPERTY_HP_BDA_ROLL_OFF, 
									&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"hauppauge KSPROPERTY_HP_PILOT not supported!");			
			return hr;
	}

	KSP_NODE instance_data;
	ZeroMemory(&instance_data, sizeof(instance_data));

	DWORD data;
	data = rolloff;

	hr = ppropset->Set(KSPROPSETID_BdaTunerExtensionProperties,
									KSPROPERTY_HP_BDA_ROLL_OFF,
									&instance_data,
									sizeof(instance_data),
									&data,
									sizeof(data));


	if FAILED(hr)  {
			log_error(L"hauppauge KSPROPERTY_HP_PILOT failed!");			
			return hr;
	}

	return hr;
}


HRESULT HAUPPAUGECTRL_SetFEC(IKsPropertySet *ppropset, int fec)
{
	#ifdef HAUPPAUGE_DEBUG
	log_info(L"HAUPPAUGECTRL_SetFEC(%p, %d)") % ppropset % fec;
	#endif

	DWORD TypeSupport=0;
	HRESULT hr = ppropset->QuerySupported(KSPROPSETID_BdaDigitalDemodulator,
									KSPROPERTY_BDA_INNER_FEC_RATE, 
									&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"FAILED: hauppauge KSPROPERTY_BDA_INNER_FEC_RATE not supported!");			
			return hr;
	}

	KSP_NODE instance_data;
	ZeroMemory(&instance_data, sizeof(instance_data));

	DWORD data;
	data = fec;

	hr = ppropset->Set(KSPROPSETID_BdaDigitalDemodulator,
									KSPROPERTY_BDA_INNER_FEC_RATE,
									&instance_data,
									sizeof(instance_data),
									&data,
									sizeof(data));


	if FAILED(hr)  {
			log_error(L"hauppauge KSPROPERTY_BDA_INNER_FEC_RATE failed!");			
			return hr;
	}

	return hr;
}

HRESULT HAUPPAUGECTRL_SetModulation(IKsPropertySet *ppropset, int mod)
{
	#ifdef HAUPPAUGE_DEBUG
	log_info(L"HAUPPAUGECTRL_SetModulation(%p, %d)") % ppropset % mod;
	#endif

	DWORD TypeSupport=0;
	HRESULT hr = ppropset->QuerySupported(KSPROPSETID_BdaDigitalDemodulator,
									KSPROPERTY_BDA_MODULATION_TYPE, 
									&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"hauppauge KSPROPERTY_BDA_MODULATION_TYPE not supported!");			
			return hr;
	}

	KSP_NODE instance_data;
	ZeroMemory(&instance_data, sizeof(instance_data));

	ModulationType data;
	data = (ModulationType) mod;

	hr = ppropset->Set(KSPROPSETID_BdaDigitalDemodulator, 
									KSPROPERTY_BDA_MODULATION_TYPE,
									&instance_data,
									sizeof(instance_data),
									&data,
									sizeof(data));


	if FAILED(hr)  {
			log_error(L"hauppauge KSPROPERTY_BDA_MODULATION_TYPE failed!");			
			return hr;
	}

	return hr;
}


ModulationType hauppauge_property_set_t::property_set_get_dvbs_modulation(int mod)
{
	switch (mod) {
		case dvblink::MOD_DVBS_8PSK:
			return BDA_MOD_8PSK;
			break;
		case dvblink::MOD_DVBS_NBC_QPSK: 
			return BDA_MOD_BPSK;  // specific to nova s2
			break; 
		default:
			bda_standard_get_dvbs_modulation(mod); 
			break;
	}
    return BDA_MOD_QPSK;
}

HRESULT hauppauge_property_set_t::property_set_set_modulation_params(dvblink::PTransponderInfo Tp)
{
	HAUPPAUGECTRL_SetPilotMode(ppropsetTunerInput, -1);	
	HAUPPAUGECTRL_SetRollOffMode(ppropsetTunerInput, -1);	
	HAUPPAUGECTRL_SetModulation(ppropsetTunerOutput, property_set_get_dvbs_modulation(Tp->dwModulation));
	HAUPPAUGECTRL_SetFEC(ppropsetTunerOutput, bda_standard_get_fec(Tp->dwFec));
	return S_OK;
}

HRESULT hauppauge_property_set_t::HAUPPAUGECTRL_send_diseqc(unsigned char *cmd, int len, int ToneBurst)
{
    log_info(L"HAUPPAUGECTRL_send_diseqc. ToneBurst: %d\n") % ToneBurst;
	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerInput->QuerySupported(KSPROPSETID_BdaTunerExtensionProperties,
							KSPROPERTY_HP_BDA_DISEQC, 
							&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Hauppauge KSPROPERTY_HP_DISEQC not supported!");			
			return E_FAIL;
	}
	
		
    KSP_NODE instance_data;
	ZeroMemory(&instance_data, sizeof(instance_data));

    DISEQC_MESSAGE_PARAMS m_pDiSEqCMessageParams;
	ZeroMemory(&m_pDiSEqCMessageParams, sizeof(m_pDiSEqCMessageParams));

    memcpy(&m_pDiSEqCMessageParams.uc_diseqc_send_message[0],
           cmd,
           min(sizeof(m_pDiSEqCMessageParams.uc_diseqc_send_message), len));

    m_pDiSEqCMessageParams.ul_diseqc_send_message_length = min(sizeof(m_pDiSEqCMessageParams.uc_diseqc_send_message), len);
   
    // set amplitude attenuation to 3, maximum Vp-p amplitude of 22KHz tone
    // used for DiSEqC bytes.
    m_pDiSEqCMessageParams.ul_amplitude_attenuation      = 3;
    
    switch (ToneBurst)
    {
    case dvblink::TONEBURST_2:
        // set tune burst to modulated
        m_pDiSEqCMessageParams.b_tone_burst_modulated        = TONE_BURST_MODULATED;
        break;
    case dvblink::TONEBURST_NONE:
    case dvblink::TONEBURST_1:
    default:
        // set tune burst to un-modulated    
        m_pDiSEqCMessageParams.b_tone_burst_modulated        = TONE_BURST_UNMODULATED;
        break;
    }

    // set DiSEqC version to Version 1.X (no receive messages)
    m_pDiSEqCMessageParams.diseqc_version                = DISEQC_VER_1X;

	// set DiSEqC receive message mode to no reply messages
    m_pDiSEqCMessageParams.receive_mode                  = RXMODE_NOREPLY;
    
    // set last message flag to true
    m_pDiSEqCMessageParams.b_last_message = TRUE;    


	hr = ppropsetTunerInput->Set(KSPROPSETID_BdaTunerExtensionProperties,
									KSPROPERTY_HP_BDA_DISEQC,
									&instance_data,
									sizeof(instance_data),
									&m_pDiSEqCMessageParams,
									sizeof(m_pDiSEqCMessageParams));
	if FAILED(hr)  {
			log_error(L"Hauppauge KSPROPERTY_HP_DISEQC failed!");			
			return hr;
	}
	return hr;
}

HRESULT hauppauge_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
    return HAUPPAUGECTRL_send_diseqc(cmd, len, dvblink::TONEBURST_NONE);
}

HRESULT hauppauge_property_set_t::property_set_send_toneburst(int type)
{
    unsigned char cmd[4] = {0xE0, 0x10, 0x38, 0xF0};

    return HAUPPAUGECTRL_send_diseqc(cmd, 4, type);
}
