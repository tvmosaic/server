/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>
#include <vector>
#include <boost/algorithm/string.hpp>

#include <drivers/deviceapi.h>
#include "../../ds_propertyset.h"
#include "../../common.h"		
#include "../../ds_tunerequest.h"
#include "../../ds_topology.h"
#include "pctv.h"
#include <dl_logger.h>

using namespace dvblink;
using namespace dvblink::logging;

const long MODE_DVBT = 0x10;
const long MODE_DVBC = 0x11;

pctv_property_set_t::pctv_property_set_t()
{
}

HRESULT pctv_property_set_t::property_set_init()
{
	return S_OK;
}

HRESULT pctv_property_set_t::property_set_deinit()
{
	return S_OK;
}

ULONG pctv_property_set_t::property_set_get_input_range_from_diseqc(int diseqc_port)
{
    if (diseqc_port > 0)
        return diseqc_port;
    else
        return -1;
}

HRESULT pctv_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
    if (graph_info_.pDiseqcCommandInterface_ != NULL)
    {
        BOOLEAN benable = TRUE;
        HRESULT hr = graph_info_.pDiseqcCommandInterface_->put_EnableDiseqCommands(benable);
        //check if this is 1.0 diseqc command
        int diseqc10 = get_10_diseqc_from_raw_cmd(cmd, len);
        if (diseqc10 == 0)
        {
            //this is not 1.0 command - attempt sending diseqc command
            hr = graph_info_.pDiseqcCommandInterface_->put_DiseqSendCommand(1, len, cmd);
            return hr;
        }
    }
    return E_NOINTERFACE; //do input range for diseqc 1.0
}

HRESULT pctv_property_set_t::pctv_tune_request(dvblink::PTransponderInfo Tp)
{
    if (graph_info_.pBDAFrequencyFilter_ == NULL || graph_info_.pBDADigitalDemodulator_ == NULL)
    {
        log_error(L"pctv_tune_request error. Some of the interfaces are missing: %1%, %2%") % graph_info_.pBDAFrequencyFilter_ % graph_info_.pBDADigitalDemodulator_;
        return E_FAIL;
    }
	IBDA_DeviceControl *pDeviceControl = NULL;
	HRESULT hr = graph_info_.pfltTuner_->QueryInterface(&pDeviceControl);
	if (SUCCEEDED(hr) && pDeviceControl) 
    {							
		hr = pDeviceControl->StartChanges(); 
        if (FAILED(hr))
            log_error(L"pctv_tune_request: StartChanges error %1%") % hr;

	    hr = graph_info_.pBDAFrequencyFilter_->put_Range((ULONG)BDA_RANGE_NOT_SET);
	    hr = graph_info_.pBDAFrequencyFilter_->put_FrequencyMultiplier(1000L);
        if (FAILED(hr))
            log_error(L"pctv_tune_request: put_FrequencyMultiplier error %1%") % hr;
	    hr = graph_info_.pBDAFrequencyFilter_->put_Frequency(Tp->dwFreq);
        if (FAILED(hr))
            log_error(L"pctv_tune_request: put_Frequency error %1%") % hr;

        SpectralInversion si = BDA_SPECTRAL_INVERSION_AUTOMATIC;
	    hr = graph_info_.pBDADigitalDemodulator_->put_SpectralInversion(&si);
//        if (FAILED(hr))
//            log_error(L"pctv_tune_request: put_SpectralInversion error %1%") % hr;

        ULONG sr = Tp->dwSr;
	    hr = graph_info_.pBDADigitalDemodulator_->put_SymbolRate((ULONG *)&sr);
        if (FAILED(hr))
            log_error(L"pctv_tune_request: put_SymbolRate error %1%") % hr;

        ModulationType mt = get_dvbc_modulation(Tp->dwLOF);
	    hr = graph_info_.pBDADigitalDemodulator_->put_ModulationType(&mt);
        if (FAILED(hr) && hr != 0x80070490) //element not found error
            log_error(L"pctv_tune_request: put_ModulationType error %1%") % hr;


	    hr = pDeviceControl->CheckChanges();
		hr = pDeviceControl->CommitChanges();		
        if (FAILED(hr))
            log_error(L"pctv_tune_request: StartChanges error %1%") % hr;
		
		pDeviceControl->Release();
		pDeviceControl = NULL;

	} else
    {
        log_error(L"pctv_tune_request: no IBDA_DeviceControl interface");
    }
    return hr;
}

HRESULT pctv_property_set_t::property_set_set_tuner(dvblink::PTransponderInfo Tp, int diseqc, int toneburst)
{
    if (boost::iequals(vid_, PCTV_TUNER_VENDOR_ID) && boost::iequals(pid_, PCTV_292E_TUNER_PROD_ID))
    {
        log_info(L"pctv_set_set_tuner: PCTV 292e detected. Set tuner mode to %1%.") % tuner_type_;
        IKsPropertySet *ppropsetTunerPin_pctv = NULL;
        // get the property sets on the tuner filter pins
        HRESULT hr = get_pin_property_set(graph_info_.pfltTuner_, PINDIR_OUTPUT, &ppropsetTunerPin_pctv);
        if (SUCCEEDED(hr))
        {
            KSPROPERTY_DVBC_SET_MODE_S DVBCT;
            
            if (tuner_type_ == TUNERTYPE_DVBC)
                DVBCT.Mode = MODE_DVBC;
            else
                DVBCT.Mode = MODE_DVBT;
    	
            KSPROPERTY ks;
            hr = ppropsetTunerPin_pctv->Set(PROPSETID_BdaExtension2800, KSPROPERTY_DVBC_SET_MODE, 
                			                &ks, sizeof(ks),
                			                &DVBCT, sizeof(KSPROPERTY_DVBC_SET_MODE_S));
            if (FAILED(hr))
                log_error(L"pctv_set_set_tuner: setting PCTV 292e tuner mode is failed");
    	
            ppropsetTunerPin_pctv->Release();
        }
        
        //in case of DVB-C mode - do proprietory tuning
        if (tuner_type_ == TUNERTYPE_DVBC)
            return pctv_tune_request(Tp);
    }

    //return no interface to do normal bda tuning
    return E_NOINTERFACE;
}
