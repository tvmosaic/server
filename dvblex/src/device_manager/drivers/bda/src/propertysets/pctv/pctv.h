/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include "../../ds_propertyset_base.h"

#define PCTV_TUNER_VENDOR_ID   L"2013"
#define PCTV_T2_TUNER_PROD_ID   L"024f"
#define PCTV_292E_TUNER_PROD_ID   L"025f"

const GUID PROPSETID_BdaExtension2800 = { 0xB88700A5, 0x3A0D, 0x475e, { 0x92, 0xA1, 0xF2, 0x01, 0x42, 0x12, 0x55, 0xCC }};

typedef enum
{
    // DVB-S/S2
    KSPROPERTY_DISEQC_SEND_MSG = 0,     // Send DiSEqC Messages
    KSPROPERTY_DISEQC_LOCK,             // Lock DiSEqC Bus
    KSPROPERTY_DISEQC_UNLOCK,           // UnLock Bus

    KSPROPERTY_LNB_SET_BAND_SEL,        // Band Selection for Ku-Band LNB
    KSPROPERTY_DVBS2_SET_ROLLOFF,
    KSPROPERTY_DVBS2_SET_PILOT,

    // DVB-H
    KSPROPERTY_DVBH_SET_MODE = 0x100,   // Set DVB-H mode
    KSPROPERTY_DVBH_SET_PSISI,          // Set PSI/SI table ID
    KSPROPERTY_DVBH_GET_PSISI,          // Get PSI/SI table data
    KSPROPERTY_DVBH_CONFIGURE,          // Configure PID & FEC

    // QAM (ATSC/QAM64/QAM256)
    KSPROPERTY_QAM_SET_MODE  = 0x200,   // Set QAM Mode, see emFilter.h
    KSPROPERTY_QAM_SET_FREQ,            // Set Frequency

    // ATSC & DVB-T & others
    KSPROPERTY_ENABLE_PID_FILTER=0x300, // Enable/Disable 28xx PID filter
    KSPROPERTY_DRX_GPIO,                // Set DRX GPIO pin value
    KSPROPERTY_DVBT_SET_MIRROR,         // Set SpectralInversion for DVBT Net Provider in DVBC mode

    KSPROPERTY_GET_FER,                 // Get Frame Erasure Ratio


    // ISDB-T
    KSPROPERTY_ISDB_SET_MODE = 0x400,   // Set ISDB 1/3 seg
    KSPROPERTY_ISDB_SET_ENCRYPTION_KEY, // Set Encryption Key (16 bytes)
    KSPROPERTY_ISDB_GET_CAPABILITY,     // Get ISDB Capability ()
                                        //      1    Segment 0x01000000
                                        //      1/3  Segment 0x02000000
                                        //      FULL Segment 0x04000000
                                        //      Unsupported  0x00000000

    // DVB-C
    KSPROPERTY_DVBC_SET_MODE = 0x500,   // Set DVBT(0x10) or DVBC(0x11)
    KSPROPERTY_DVBC_SET_BW,             // Set BW for DVBC Net Provider in DVBT mode
    KSPROPERTY_DVBC_GET_CHANNEL,        // Get DVBC channel params (SymbolRate, etc)
    KSPROPERTY_DVBC_SET_AUTOSCAN,       // Set QAM, SR lists, etc. for auto-scan
    KSPROPERTY_DVBC_GET_AUTOSCAN,       // Get QAM, SR lists, etc. for auto-scan


    // CMMB
    KSPROPERTY_CMMB_SET_CHANNELS = 0x600, // Set params includeing timeslot start/count and mode


    // DVB-T2
    KSPROPERTY_DVBT2_SET_PLP = 0x700,   // Set params includeing timeslot start/count and mode
    KSPROPERTY_DVBT2_GET_PLP,
} KSPROPERTY_BDA_EXTENSION_2800;

typedef struct
{
    ULONG Mode;         // 0x10=DVBT, 0x11=DVBC
} KSPROPERTY_DVBC_SET_MODE_S, *PKSPROPERTY_DVBC_SET_MODE_S;

class pctv_property_set_t : public property_set_base_t
{
public:
    pctv_property_set_t();

    virtual HRESULT property_set_init();
    virtual HRESULT property_set_deinit();
    virtual HRESULT property_set_set_tuner(dvblink::PTransponderInfo Tp, int diseqc, int toneburst);
    virtual HRESULT property_set_send_diseqc(unsigned char *cmd, int len);
    virtual ULONG property_set_get_input_range_from_diseqc(int diseqc_port);
protected:
    HRESULT pctv_tune_request(dvblink::PTransponderInfo Tp);
};
