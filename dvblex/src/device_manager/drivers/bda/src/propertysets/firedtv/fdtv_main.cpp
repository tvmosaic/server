/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>
#include <vector>

#include <drivers/deviceapi.h>
#include "../../ds_propertyset.h"
#include "../../common.h"		
#include <dl_logger.h>
#include "fdtv_main.h"		

using namespace dvblink::logging;

//forward declaration
HRESULT firedtv_ci_get_status(unsigned long *status);
static void ResetPids();
static int FindPid(unsigned short pid);
static void SetPid(int idx, unsigned short pid);
static void GetPidVector(std::vector<unsigned short>& pids);

typedef enum {
	KSPROPERTY_FIRESAT_SELECT_MULTIPLEX_DVB_S,
	KSPROPERTY_FIRESAT_SELECT_SERVICE_DVB_S,
	KSPROPERTY_FIRESAT_SELECT_PIDS_DVB_S, // USE ALSO FOR DVB-C
	KSPROPERTY_FIRESAT_SIGNAL_STRENGTH_TUNER,
	KSPROPERTY_FIRESAT_DRIVER_VERSION,
	KSPROPERTY_FIRESAT_SELECT_MULTIPLEX_DVB_T,
	KSPROPERTY_FIRESAT_SELECT_PIDS_DVB_T,
	KSPROPERTY_FIRESAT_SELECT_MULTIPLEX_DVB_C,
	KSPROPERTY_FIRESAT_SELECT_PIDS_DVB_C, // DON�T USE
	KSPROPERTY_FIRESAT_GET_FRONTEND_STATUS,
	KSPROPERTY_FIRESAT_GET_SYSTEM_INFO,
	KSPROPERTY_FIRESAT_GET_FIRMWARE_VERSION,
	KSPROPERTY_FIRESAT_LNB_CONTROL,
	KSPROPERTY_FIRESAT_GET_LNB_PARAM,
	KSPROPERTY_FIRESAT_SET_LNB_PARAM,
	KSPROPERTY_FIRESAT_SET_POWER_STATUS,
	KSPROPERTY_FIRESAT_SET_AUTO_TUNE_STATUS,
	KSPROPERTY_FIRESAT_FIRMWARE_UPDATE,
	KSPROPERTY_FIRESAT_FIRMWARE_UPDATE_STATUS,
	KSPROPERTY_FIRESAT_CI_RESET,
	KSPROPERTY_FIRESAT_CI_WRITE_TPDU,
	KSPROPERTY_FIRESAT_CI_READ_TPDU,
	KSPROPERTY_FIRESAT_HOST2CA,
	KSPROPERTY_FIRESAT_CA2HOST,
	KSPROPERTY_FIRESAT_GET_BOARD_TEMP,
	KSPROPERTY_FIRESAT_TUNE_QPSK,
	KSPROPERTY_FIRESAT_REMOTE_CONTROL_REGISTER,
	KSPROPERTY_FIRESAT_REMOTE_CONTROL_CANCEL,
	KSPROPERTY_FIRESAT_GET_CI_STATUS,
	KSPROPERTY_FIRESAT_TEST_INTERFACE,
} KSPROPERTY_FIRESAT;

//// CI PMT List management constants for multiservice decryption ///////////
#define FIRESAT_CI_ONLY		3	
#define FIRESAT_CI_FIRST	1	
#define FIRESAT_CI_MORE		0	
#define FIRESAT_CI_LAST		2	

//// CI PMT Command ID //////////////////////////////////////////////////////
#define FIRESAT_CI_CMD_DESCRAMBLE	1	//  FireDTV + Twinhan
#define FIRESAT_CI_CMD_MMI			2	//  Twinhan 
#define FIRESAT_CI_CMD_QUERY		3	//  Twinhan 
#define FIRESAT_CI_CMD_NSELECTED	4	//  Twinhan 

//CI status enum
enum DE_CI_STATUS
{
  /// CI_ERR_MSG_AVAILABLE
  CI_ERR_MSG_AVAILABLE  = 0x01,
  /// CI_MODULE_INIT_READY
  CI_MODULE_INIT_READY  = 0x02,
  /// CI_MODULE_ERROR
  CI_MODULE_ERROR       = 0x04,
  /// CI_MODULE_IS_DVB
  CI_MODULE_IS_DVB      = 0x08,
  /// CI_MODULE_PRESENT
  CI_MODULE_PRESENT     = 0x10,
  /// CI_APP_INFO_AVAILABLE
  CI_APP_INFO_AVAILABLE = 0x20,
  /// CI_DATE_TIME_REQEST
  CI_DATE_TIME_REQEST   = 0x40,
  /// CI_PMT_REPLY
  CI_PMT_REPLY          = 0x80,
  /// CI_MMI_REQUEST
  CI_MMI_REQUEST        = 0x100
};

// GUID of DiSEqC Property
static const GUID KSPROPSETID_Firesat = { 0xab132414, 0xd060, 0x11d0, { 0x85,
0x83, 0x00, 0xc0, 0x4f, 0xd9, 0xba,0xf3 } };


typedef struct _KSPROPERTY_FIRESAT_LNB_CMD
{
	UCHAR Voltage;	//0xFF...Don�t Care
	UCHAR ContTone;	//0xFF...Don�t Care
	UCHAR Burst;	//0xFF...Don�t Care
	UCHAR NrDiseqcCmds; // 1 - 3
	struct{
		UCHAR Length; //3 - 6
		UCHAR Framing;
		UCHAR Address;
		UCHAR Command;
		UCHAR Data[3];
	}DiseqcCmd[3] ;
}KSPROPERTY_FIRESAT_LNB_CMD, *PKSPROPERTY_FIRESAT_LNB_CMD;

#define FIRESAT_MAX_PMT_SIZE 1024

typedef struct _KSPROPERTY_FIRESAT_CA_DATA{
	UCHAR uSlot;
	UCHAR uTag;
	BOOL bMore; //don�t care; set by driver
	USHORT uLength;
	UCHAR uData[FIRESAT_MAX_PMT_SIZE];
}KSPROPERTY_FIRESAT_CA_DATA, *PKSPROPERTY_FIRESAT_CA_DATA;

typedef struct _KSPROPERTY_FIRESAT_CI_STATUS
{
	USHORT uStatus;
}FIRESAT_CI_STATUS, *PFIRESAT_CI_STATUS;

typedef struct _KSPROPERTY_FIRESAT_SELECT_PIDS_DVBS_DATA //also for DVBC
{
    unsigned int bCurrentTransponder;
    unsigned int bFullTransponder;
    unsigned int uLnb;
    unsigned int uFrequency;
    unsigned int uSymbolRate;
    unsigned short uFecInner;
    unsigned char uPolarization;
    unsigned short uNumberOfValidPids; // 1-16
    unsigned short pids[FIREDTV_MAX_PID_NUM];
}KSPROPERTY_FIRESAT_SELECT_PIDS_DVBS_DATA, *PKSPROPERTY_FIRESAT_SELECT_PIDS_DVBS_DATA;

typedef struct _KSPROPERTY_FIRESAT_SELECT_PIDS_DVBT_DATA //also for DVBC
{
    unsigned int bCurrentTransponder;//Set TRUE
    unsigned int bFullTransponder;   //Set FALSE when selecting PIDs
    unsigned int uFrequency;    // kHz 47.000-860.000
    unsigned char uBandwidth;    // BANDWIDTH_8_MHZ, BANDWIDTH_7_MHZ, BANDWIDTH_6_MHZ
    unsigned char uConstellation;// CONSTELLATION_DVB_T_QPSK,CONSTELLATION_QAM_16,CONSTELLATION_QAM_64,OFDM_AUTO
    unsigned char uCodeRateHP;   // CR_12,CR_23,CR_34,CR_56,CR_78,OFDM_AUTO
    unsigned char uCodeRateLP;   // CR_12,CR_23,CR_34,CR_56,CR_78,OFDM_AUTO
    unsigned char uGuardInterval;// GUARD_INTERVAL_1_32,GUARD_INTERVAL_1_16,GUARD_INTERVAL_1_8,GUARD_INTERVAL_1_4,OFDM_AUTO
    unsigned char uTransmissionMode;// TRANSMISSION_MODE_2K, TRANSMISSION_MODE_8K, OFDM_AUTO
    unsigned char uHierarchyInfo;// HIERARCHY_NONE,HIERARCHY_1,HIERARCHY_2,HIERARCHY_4,OFDM_AUTO
    unsigned short uNumberOfValidPids; // 1-16
    unsigned short pids[FIREDTV_MAX_PID_NUM];
}KSPROPERTY_FIRESAT_SELECT_PIDS_DVBT_DATA, *PKSPROPERTY_FIRESAT_SELECT_PIDS_DVBT_DATA;

#define FIREDTV_INVALID_PID 0xFFFF

fdtv_property_set_t::fdtv_property_set_t() :
    ppropsetTunerFilter_firedtv(NULL)
{
}

HRESULT fdtv_property_set_t::property_set_init()
{
	HRESULT hr;

	//Reset pid array
	ResetPids();

	// get the property sets on the tuner filter
	hr = get_filter_property_set(graph_info_.pfltTuner_, &ppropsetTunerFilter_firedtv);
	if FAILED(hr) 
		log_warning(L"FireDTV. No propertyset interface on the pins of the tuner filter");

	return hr;
}

HRESULT fdtv_property_set_t::property_set_deinit()
{
	if(ppropsetTunerFilter_firedtv) 
	{
		ppropsetTunerFilter_firedtv->Release();
		ppropsetTunerFilter_firedtv = NULL;
	}
	return S_OK;
}

HRESULT fdtv_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
	wchar_t s[255];	
	diseqc2str(cmd, len, &s[0]);
	log_info(L"firedtv_send_diseqc(<%s>, %d)") % s % len;

	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerFilter_firedtv->QuerySupported(KSPROPSETID_Firesat,
							KSPROPERTY_FIRESAT_LNB_CONTROL, 
							&TypeSupport);
	
	if (FAILED(hr))
	{
			log_error(L"firedtv KSPROPERTY_BDA_DISEQC_MESSAGE not supported!");			
			return E_FAIL;
	}

	KSPROPERTY_FIRESAT_LNB_CMD LNB_Cmd;
	ZeroMemory(&LNB_Cmd, sizeof(LNB_Cmd));

	LNB_Cmd.Voltage = 0xFF;
	LNB_Cmd.ContTone= 0xFF;
	LNB_Cmd.Burst = 0xFF;


	LNB_Cmd.NrDiseqcCmds = 1;
	LNB_Cmd.DiseqcCmd[0].Length = min(len , 6);
	CopyMemory(&LNB_Cmd.DiseqcCmd[0].Framing, cmd, min(len , 6));

	hr = ppropsetTunerFilter_firedtv->Set(KSPROPSETID_Firesat, 
                        			KSPROPERTY_FIRESAT_LNB_CONTROL, 
									&LNB_Cmd,
									sizeof(LNB_Cmd),
									&LNB_Cmd,
									sizeof(LNB_Cmd));

	if (FAILED(hr))
		log_error(L"FireDTV. Send Diseqc command has failed with error %d") % hr;			
	return hr;
}


// PMT buf must start with 0x02
// listmng: List management constant  (CI_ONLY for one-channel only descrambling)
HRESULT fdtv_property_set_t::property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng)
{
	if (listmng != dvblink::SERVICE_DECRYPT_FIRST)
	{
		if (listmng == dvblink::SERVICE_DECRYPT_ADD)
			log_error(L"firedtv_ci_send_pmt: multi-channel decryption is not supported");
		return E_FAIL;
	}

	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerFilter_firedtv->QuerySupported(KSPROPSETID_Firesat,
							KSPROPERTY_FIRESAT_HOST2CA, 
							&TypeSupport);
	
	if (FAILED(hr) || (TypeSupport & KSPROPERTY_SUPPORT_SET) == 0)
	{
			log_error(L"firedtv KSPROPERTY_FIRESAT_HOST2CA Set not supported!");			
			return E_FAIL;
	}

	//get status of CI module
	unsigned long ci_status;
	hr = firedtv_ci_get_status(&ci_status);
	if (FAILED(hr) || (ci_status & CI_MODULE_PRESENT) == 0)
	{
		log_error(L"firedtv_ci_send_pmt: ci_get_status returned error %d, %d") % hr % ci_status;
		return E_FAIL;
	}

	KSPROPERTY_FIRESAT_CA_DATA CA_Data_ret;
	KSPROPERTY_FIRESAT_CA_DATA CA_Data;

	ZeroMemory(&CA_Data, sizeof(CA_Data));

	CA_Data.uSlot = 0;
	CA_Data.uTag = 2;
	CA_Data.bMore = FALSE;
	CA_Data.uData[0] = FIRESAT_CI_ONLY; // List Management = ONLY
	CA_Data.uData[1] = FIRESAT_CI_CMD_DESCRAMBLE; // pmt_cmd = OK DESCRAMBLING
	CA_Data.uLength = 2 + len;
	// Copy PMT into CA datagram
	memcpy(&CA_Data.uData[2], buf, len);

	memcpy(&CA_Data_ret, &CA_Data, sizeof(CA_Data));
	hr = ppropsetTunerFilter_firedtv->Set(KSPROPSETID_Firesat,
					KSPROPERTY_FIRESAT_HOST2CA,
					&CA_Data, 
					sizeof(KSPROPERTY_FIRESAT_CA_DATA),
					&CA_Data_ret,
					sizeof(KSPROPERTY_FIRESAT_CA_DATA));
	if (FAILED(hr))
	{
		log_error(L"FireDTV. Send PMT has failed with error %d. Retrying") % hr;
		Sleep(500);
		hr = ppropsetTunerFilter_firedtv->Set(KSPROPSETID_Firesat,
						KSPROPERTY_FIRESAT_HOST2CA,
						&CA_Data, 
						sizeof(KSPROPERTY_FIRESAT_CA_DATA),
						&CA_Data_ret,
						sizeof(KSPROPERTY_FIRESAT_CA_DATA));
	}
	if (FAILED(hr))
	{
		log_error(L"FireDTV. Send PMT has failed with error %d") % hr;	
	}
	return hr;	
}

HRESULT fdtv_property_set_t::firedtv_ci_reset() 
{
	KSPROPERTY_FIRESAT_CA_DATA CA_Data_ret;
	KSPROPERTY_FIRESAT_CA_DATA CA_Data;

	CA_Data.uSlot = 0;
	CA_Data.uTag = 0; // CA Reset
	CA_Data.bMore = FALSE;
	CA_Data.uLength = 1;
	CA_Data.uData[0] = 0; // HW Reset of CI part

	memcpy(&CA_Data_ret, &CA_Data, sizeof(CA_Data));
	HRESULT hr = ppropsetTunerFilter_firedtv->Set(KSPROPSETID_Firesat,
					KSPROPERTY_FIRESAT_HOST2CA,
					&CA_Data,
					sizeof(KSPROPERTY_FIRESAT_CA_DATA),
					&CA_Data_ret,
					sizeof(KSPROPERTY_FIRESAT_CA_DATA));
	if (FAILED(hr))
	{
		log_error(L"FireDTV. CI reset failed with error %d") % hr;
	}
	return hr;
}

HRESULT fdtv_property_set_t::firedtv_ci_get_status(unsigned long *status)
{
	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerFilter_firedtv->QuerySupported(KSPROPSETID_Firesat,
							KSPROPERTY_FIRESAT_GET_CI_STATUS, 
							&TypeSupport);
	
	if (FAILED(hr) || (TypeSupport & KSPROPERTY_SUPPORT_GET) == 0)
	{
			log_error(L"firedtv KSPROPERTY_FIRESAT_GET_CI_STATUS Get is not supported!");
			return E_FAIL;
	}

	FIRESAT_CI_STATUS instance;
	FIRESAT_CI_STATUS uCIStatus;
	DWORD dwBytesReturned;

	*status = 0;

	hr = ppropsetTunerFilter_firedtv->Get(KSPROPSETID_Firesat,
					KSPROPERTY_FIRESAT_GET_CI_STATUS,
					&instance,
					sizeof(FIRESAT_CI_STATUS),
					&uCIStatus,
					sizeof(FIRESAT_CI_STATUS),
					&dwBytesReturned);

	if (hr == 0x8007001F)
	{
		log_error(L"firedtv GetStatus is failed with -Device attached to the system is not functioning- error. Resetting CAM...");
		//Device attached to the system is not functioning
		firedtv_ci_reset();
		//try again
		hr = ppropsetTunerFilter_firedtv->Get(KSPROPSETID_Firesat,
						KSPROPERTY_FIRESAT_GET_CI_STATUS,
						&instance,
						sizeof(FIRESAT_CI_STATUS),
						&uCIStatus,
						sizeof(FIRESAT_CI_STATUS),
						&dwBytesReturned);
	}

	*status = uCIStatus.uStatus;	

	return hr;
}

HRESULT fdtv_property_set_t::firedtv_set_pids()
{
    KSPROPERTY_FIRESAT_SELECT_PIDS_DVBT_DATA dvbt_pid_data;
    KSPROPERTY_FIRESAT_SELECT_PIDS_DVBS_DATA dvbs_pid_data;

    memset(&dvbt_pid_data, 0, sizeof(dvbt_pid_data));
    memset(&dvbs_pid_data, 0, sizeof(dvbs_pid_data));
    dvbs_pid_data.bCurrentTransponder = true;
    dvbs_pid_data.bFullTransponder = false;
    dvbt_pid_data.bCurrentTransponder = true;
    dvbt_pid_data.bFullTransponder = false;

    std::vector<unsigned short> pids;
    GetPidVector(pids);
    log_info(L"firedtv_set_pids: there are %d pids to filter") % pids.size();

    dvbs_pid_data.uNumberOfValidPids = pids.size();
    dvbt_pid_data.uNumberOfValidPids = pids.size();

	if (pids.size() > 0)
	{
		for (unsigned int i=0; i<min(pids.size(), FIREDTV_MAX_PID_NUM); i++)
		{
			dvbs_pid_data.pids[i] = pids[i]; 
			dvbt_pid_data.pids[i] = pids[i];
		}
	}
    else
	{
		dvbs_pid_data.bFullTransponder = true;
		dvbt_pid_data.bFullTransponder = true;
	}

	if (ppropsetTunerFilter_firedtv == NULL)
		return E_NOINTERFACE;

    //prepare data to send
    void* pid_data = &dvbs_pid_data;
    unsigned long pid_data_length = sizeof(dvbs_pid_data);
    unsigned long property_id = KSPROPERTY_FIRESAT_SELECT_PIDS_DVB_S;

	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerFilter_firedtv->QuerySupported(KSPROPSETID_Firesat, property_id, &TypeSupport);

	if (hr != S_OK || (TypeSupport & KSPROPERTY_SUPPORT_SET) == 0)
	{
        //No DVB-S/C property set - try DVB-T

        log_info(L"firedtv: no dvb-s pid filtering. Trying dvb-t...");
        pid_data = &dvbt_pid_data;
        pid_data_length = sizeof(dvbt_pid_data);
        property_id = KSPROPERTY_FIRESAT_SELECT_PIDS_DVB_T;

        hr = ppropsetTunerFilter_firedtv->QuerySupported(KSPROPSETID_Firesat, property_id, &TypeSupport);

	    if (hr != S_OK || (TypeSupport & KSPROPERTY_SUPPORT_SET) == 0)
	    {
            log_error(L"firedtv: interface to enable HW pid filtering is not available. Reverting to default mechanism");			
		    return E_NOINTERFACE;
        }
	}

	hr = ppropsetTunerFilter_firedtv->Set(KSPROPSETID_Firesat, 
                        			property_id, 
									pid_data,
									pid_data_length,
									pid_data,
									pid_data_length);

	if (FAILED(hr))
        log_error(L"ERROR: firedtv_set_pids. Set for property Id %d has failed with error %d") % property_id % hr;

    return hr;
}

HRESULT fdtv_property_set_t::property_set_set_tuner(dvblink::PTransponderInfo Tp, int diseqc, int toneburst)
{
    //reset pid filter list
    ResetPids();
    //return no interface to do default tuning
    return E_NOINTERFACE;
}
/*
HRESULT fdtv_property_set_t::property_set_add_pid(int pid)
{
    //check whether this pid is already on the list
    int idx = FindPid(pid);
    if (idx != -1)
        return S_OK;
    //find empty place
    idx = FindPid(FIREDTV_INVALID_PID);
    if (idx == -1)
    {
        log_error(L"firedtv_set_add_pid. Maximum number of pids to filter is reached");
        return E_FAIL;
    }
    //add pid and apply new selection
    SetPid(idx, pid);
    firedtv_set_pids();
    return E_NOINTERFACE;
}

HRESULT fdtv_property_set_t::property_set_del_pid(int pid)
{
    int idx = FindPid(pid);
    if (idx == -1)
        return S_OK;
    //remove pid
    SetPid(idx, FIREDTV_INVALID_PID);
	firedtv_set_pids();
    return E_NOINTERFACE;
}
*/
void fdtv_property_set_t::ResetPids()
{
    memset(&g_pids_to_filter, 0xFF, sizeof(g_pids_to_filter));
}

int fdtv_property_set_t::FindPid(unsigned short pid)
{
    for (unsigned int i=0; i<FIREDTV_MAX_PID_NUM; i++)
    {
        if (g_pids_to_filter[i] == pid)
            return i;
    }
    return -1;
}

void fdtv_property_set_t::SetPid(int idx, unsigned short pid)
{
    g_pids_to_filter[idx] = pid;
}

void fdtv_property_set_t::GetPidVector(std::vector<unsigned short>& pids)
{
    pids.clear();
    for (unsigned int i=0; i<FIREDTV_MAX_PID_NUM; i++)
    {
        if (g_pids_to_filter[i] != FIREDTV_INVALID_PID)
            pids.push_back(g_pids_to_filter[i]);
    }
}

