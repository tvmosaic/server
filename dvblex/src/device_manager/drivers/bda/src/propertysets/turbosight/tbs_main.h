/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <windows.h>
#include <boost/algorithm/string.hpp>
#include "../../ds_propertyset_base.h"

enum tbs_tuner_driver_type_e
{
    ttdte_pci,
    ttdte_nxp,
    ttdte_usb
};

struct tbs_tuner_desc_t
{
    const wchar_t* filter_name;
    tbs_tuner_driver_type_e type;
    bool has_ci;
};

const tbs_tuner_desc_t tbs_tuner_description[] = 
{
	{L"TBS 8910 BDA Tuner/Demod", ttdte_pci, false},
	{L"TBS 8920 BDA Tuner/Demod", ttdte_pci, false},

	{L"TBS 6920 BDA DVBS/S2 Tuner/Demod", ttdte_pci, false},

	{L"TBS 6980 BDA DVBS/S2 A Tuner/Demod", ttdte_pci, false},
	{L"TBS 6980 BDA DVBS/S2 B Tuner/Demod", ttdte_pci, false},
	{L"QBOX DVBS Tuner", ttdte_usb, false},
	{L"TBS QBOX DVB-S2 Tuner", ttdte_usb, false},

	{L"TBS 8921 BDA Tuner/Demod", ttdte_pci, false}, 	
	{L"TBS 6921 BDA DVBS/S2 Tuner/Demod", ttdte_pci, false}, 
	{L"TBS QBOX3 DVB-S2 Tuner", ttdte_usb, false},	
	{L"TBS 6981 BDA DVBS/S2 A Tuner/Demod", ttdte_pci, false},
	{L"TBS 6981 BDA DVBS/S2 B Tuner/Demod", ttdte_pci, false}, 	

	{L"TBS 6922 DVBS/S2 Tuner", ttdte_nxp, false},
	{L"TBS 8922 DVBS/S2 Tuner", ttdte_pci, false},

	{L"TBS 5922 USB DVB-S2 Tuner", ttdte_usb, false},

	{L"TBS 6925 DVBS/S2 Tuner", ttdte_nxp, false}, 
	{L"TBS 5925 DVBS2 Tuner", ttdte_usb, false},

	{L"TBS 5980 CI Tuner", ttdte_usb, true},  // QBOX 2 CI

	{L"TBS 6984 Quad DVBS/S2 Tuner A", ttdte_nxp, false},
	{L"TBS 6984 Quad DVBS/S2 Tuner B", ttdte_nxp, false}, 
	{L"TBS 6984 Quad DVBS/S2 Tuner C", ttdte_nxp, false},
	{L"TBS 6984 Quad DVBS/S2 Tuner D", ttdte_nxp, false},	

	{L"TBS 6928 DVBS/S2 Tuner", ttdte_nxp, true},	// PCI-E CI
	{L"TBS 6991 DVBS/S2 Tuner A", ttdte_nxp, true},	// PCI-E CI
	{L"TBS 6991 DVBS/S2 Tuner B", ttdte_nxp, true},	// PCI-E CI
	{L"TBS 6992 DVBS/S2 Tuner A", ttdte_nxp, true},	// PCI-E CI
	{L"TBS 6992 DVBS/S2 Tuner B", ttdte_nxp, true},	// PCI-E CI

	{L"TBS 6982 DVBS/S2 Tuner A", ttdte_nxp, false},
	{L"TBS 6982 DVBS/S2 Tuner B", ttdte_nxp, false},

	//DVB-T/T2  C
	{L"TBS 5880 DVB-T/T2 Tuner", ttdte_usb, true},	// USB CI
	{L"TBS 5880 DVBC Tuner", ttdte_usb, true},		// USB CI

	{L"TBS 6220 DVBT/T2 Tuner", ttdte_nxp, false},
	{L"TBS 6280 DVBT/T2 Tuner A", ttdte_nxp, false},
	{L"TBS 6280 DVBT/T2 Tuner B", ttdte_nxp, false},

	{L"TBS 6284 DVBT/T2 Tuner A", ttdte_nxp, false},
	{L"TBS 6284 DVBT/T2 Tuner B", ttdte_nxp, false},
	{L"TBS 6284 DVBT/T2 Tuner C", ttdte_nxp, false},
	{L"TBS 6284 DVBT/T2 Tuner D", ttdte_nxp, false},

	{L"TBS 6618 BDA DVBC Tuner", ttdte_nxp, true},  // PCI-E CI

	//added 2012 03 01 tbs 5
	{L"TBS 5280 DVB-T/T2 Tuner A", ttdte_usb, false},
	{L"TBS 5280 DVB-T/T2 Tuner B", ttdte_usb, false},

	{L"TBS 5280 DVBC Tuner A", ttdte_usb, false},
	{L"TBS 5280 DVBC Tuner B", ttdte_usb, false},

	{L"TBS 5680 DVBC Tuner", ttdte_usb, true},	
	//end

	//added 2012 11 29 liuy  19 cards
	{L"TBS 6680 BDA DVBC Tuner A", ttdte_nxp, true},  //CI
	{L"TBS 6680 BDA DVBC Tuner B", ttdte_nxp, true},  //CI


	{L"TBS 5990 DVBS/S2 Tuner A", ttdte_nxp, true},  //CI
	{L"TBS 5990 DVBS/S2 Tuner B", ttdte_nxp, true},  //CI


	{L"TBS 6985 DVBS/S2 Tuner A", ttdte_nxp, false},
	{L"TBS 6985 DVBS/S2 Tuner B", ttdte_nxp, false}, 
	{L"TBS 6985 DVBS/S2 Tuner C", ttdte_nxp, false},
	{L"TBS 6985 DVBS/S2 Tuner D", ttdte_nxp, false},

	{L"TBS 6905 DVBS/S2 Tuner 0", ttdte_nxp, false},
	{L"TBS 6905 DVBS/S2 Tuner 1", ttdte_nxp, false}, 
	{L"TBS 6905 DVBS/S2 Tuner 2", ttdte_nxp, false},
	{L"TBS 6905 DVBS/S2 Tuner 3", ttdte_nxp, false},

	{L"TBS 6221 DVBT/T2 Tuner", ttdte_nxp, false},
	{L"TBS 6281 DVBT/T2 Tuner A", ttdte_nxp, false},
	{L"TBS 6281 DVBT/T2 Tuner B", ttdte_nxp, false},

	{L"TBS 6285 DVBT/T2 Tuner A", ttdte_nxp, false},
	{L"TBS 6285 DVBT/T2 Tuner B", ttdte_nxp, false},
	{L"TBS 6285 DVBT/T2 Tuner C", ttdte_nxp, false},
	{L"TBS 6285 DVBT/T2 Tuner D", ttdte_nxp, false},

	{L"TBS 5220 DVB-T/T2 BDA Tuner", ttdte_usb, false}, //DVBT Dongle
	{L"TBS 5220 DVBC Tuner", ttdte_usb, false},
 
	{L"TBS 5881 DVB-T/T2 Tuner", ttdte_usb, true},  //CI
	{L"TBS 5881 DVBC Tuner", ttdte_usb, true}, //CI

	// 26.11.2013  (following items added)
	{L"TBS 6812 ISDB-T BDA Tuner A", ttdte_nxp, false},
	{L"TBS 6812 ISDB-T BDA Tuner B", ttdte_nxp, false},
	{L"TBS 6814 ISDB-T BDA Tuner A", ttdte_nxp, false},
	{L"TBS 6814 ISDB-T BDA Tuner B", ttdte_nxp, false},
	{L"TBS 6814 ISDB-T BDA Tuner C", ttdte_nxp, false},
	{L"TBS 6814 ISDB-T BDA Tuner D", ttdte_nxp, false},
	{L"TBS 6988 DVBS/S2 BDA Tuner A", ttdte_nxp, false},
	{L"TBS 6988 DVBS/S2 BDA Tuner B", ttdte_nxp, false},
	{L"TBS 5710 ATSC BDA Tuner", ttdte_usb, false},

	{L"TBS 5281 DVB-T/T2 Tuner A", ttdte_usb, false},
	{L"TBS 5281 DVB-T/T2 Tuner B", ttdte_usb, false},
	{L"TBS 5281 DVBC Tuner A", ttdte_usb, false},
	{L"TBS 5281 DVBC Tuner B", ttdte_usb, false},
	{L"TBS 6290 DVBT/T2 Tuner A", ttdte_nxp, true},  //CI
	{L"TBS 6290 DVBT/T2 Tuner B", ttdte_nxp, true}, //CI
	{L"TBS 6290 DVBC Tuner A", ttdte_nxp, true},   //CI
	{L"TBS 6290 DVBC Tuner B", ttdte_nxp, true},//CI
	//end
		
	//DVBC dongle  added 2013 12 05 
	{L"TBS DVBC Tuner", ttdte_usb, false},
	{L"TBS QBOX DVB-T/T2 BDA Tuner", ttdte_usb, false},
	{L"TBS QBOX DVB-C BDA Tuner", ttdte_usb, false},
	{L"TBS QBOX DVB-S2 BDA Tuner", ttdte_usb, false},



	//added 2013 12 12 
	{L"TBS 6281 DVBC Tuner A", ttdte_nxp, false},
	{L"TBS 6281 DVBC Tuner B", ttdte_nxp, false},

		
	//added 2014 03 18
	{L"TBS 6221 DVBC Tuner", ttdte_nxp, false},
	{L"TBS 6285 DVBC Tuner A", ttdte_nxp, false},
	{L"TBS 6285 DVBC Tuner B", ttdte_nxp, false},
	{L"TBS 6285 DVBC Tuner C", ttdte_nxp, false},
	{L"TBS 6285 DVBC Tuner D", ttdte_nxp, false},

	{L"TBS 5810 ISDB-T BDA Tuner", ttdte_usb, false},
	{L"TBS 5820 ISDB-T Tuner A", ttdte_usb, false},
	{L"TBS 5820 ISDB-T Tuner B", ttdte_usb, false},
	{L"TBS 6983 DVBS/S2 Tuner A", ttdte_nxp, false},
	{L"TBS 6983 DVBS/S2 Tuner B", ttdte_nxp, false},
    
    //added 2014 11 06
    {L"TBS 7220 DVBT/T2 Tuner", ttdte_nxp, false},
    {L"TBS 7220 DVBC Tuner", ttdte_nxp, false},
    {L"TBS 6908 DVBS/S2 Tuner 0", ttdte_nxp, false},
    {L"TBS 6908 DVBS/S2 Tuner 1", ttdte_nxp, false},
    {L"TBS 6908 DVBS/S2 Tuner 2", ttdte_nxp, false},
    {L"TBS 6908 DVBS/S2 Tuner 3", ttdte_nxp, false},
    {L"TBS 6904 DVBS/S2 Tuner 0", ttdte_nxp, false},
    {L"TBS 6904 DVBS/S2 Tuner 1", ttdte_nxp, false},
    {L"TBS 6904 DVBS/S2 Tuner 2", ttdte_nxp, false},
    {L"TBS 6904 DVBS/S2 Tuner 3", ttdte_nxp, false},
    {L"TBS 6814 ISDB-T Tuner 0", ttdte_nxp, false},
    {L"TBS 6814 ISDB-T Tuner 1", ttdte_nxp, false},
    {L"TBS 6814 ISDB-T Tuner 2", ttdte_nxp, false},
    {L"TBS 6814 ISDB-T Tuner 3", ttdte_nxp, false},

    //added 2015 01 06
    {L"TBS 6905 DVBS/S2 Tuner 0", ttdte_nxp, false},
    {L"TBS 6905 DVBS/S2 Tuner 1", ttdte_nxp, false},
    {L"TBS 6905 DVBS/S2 Tuner 2", ttdte_nxp, false},
    {L"TBS 6905 DVBS/S2 Tuner 3", ttdte_nxp, false},

    //added 2015 04 24
    {L"TBS 6205 DVB-T/T2 Tuner 0", ttdte_nxp, false},
    {L"TBS 6205 DVB-T/T2 Tuner 1", ttdte_nxp, false},
    {L"TBS 6205 DVB-T/T2 Tuner 2", ttdte_nxp, false},
    {L"TBS 6205 DVB-T/T2 Tuner 3", ttdte_nxp, false},

    {L"TBS 6205 DVBC Tuner 0", ttdte_nxp, false},
    {L"TBS 6205 DVBC Tuner 1", ttdte_nxp, false},
    {L"TBS 6205 DVBC Tuner 2", ttdte_nxp, false},
    {L"TBS 6205 DVBC Tuner 3", ttdte_nxp, false},
    {L"TBS 6001 DVBS/S2 Tuner A", ttdte_nxp, false},

    //added 2015 07 04
    {L"TBS 6704 ATSC BDA Tuner", ttdte_nxp, false},
    {L"TBS 6704 ATSC Tuner 0", ttdte_nxp, false},
    {L"TBS 6704 ATSC Tuner 1", ttdte_nxp, false},
    {L"TBS 6704 ATSC Tuner 2", ttdte_nxp, false},
    {L"TBS 6704 ATSC Tuner 3", ttdte_nxp, false},

	// new 2015 12 10 ///////////////////////////////////////
	
	{ L"TBS 6910 DVBS/S2 Tuner 0", ttdte_nxp, true },
	{ L"TBS 6910 DVBS/S2 Tuner 1", ttdte_nxp, true },
		//added tbs6909 20150521 
	{ L"TBS 6909 DVBS/S2 Tuner 0", ttdte_nxp, false },
	{ L"TBS 6909 DVBS/S2 Tuner 1", ttdte_nxp, false },
	{ L"TBS 6909 DVBS/S2 Tuner 2", ttdte_nxp, false },
	{ L"TBS 6909 DVBS/S2 Tuner 3", ttdte_nxp, false },
	{ L"TBS 6909 DVBS/S2 Tuner 4", ttdte_nxp, false },
	{ L"TBS 6909 DVBS/S2 Tuner 5", ttdte_nxp, false },
	{ L"TBS 6909 DVBS/S2 Tuner 6", ttdte_nxp, false },
	{ L"TBS 6909 DVBS/S2 Tuner 7", ttdte_nxp, false },
		//added tbs5520 5 29 
	{ L"TBS 5520 BDA Tuner", ttdte_usb, false },
		//added tbs5520 0609 
	{ L"TBS 6204 DVB-T/T2 Tuner 0", ttdte_nxp, false },
	{ L"TBS 6204 DVB-T/T2 Tuner 1", ttdte_nxp, false },
	{ L"TBS 6204 DVB-T/T2 Tuner 2", ttdte_nxp, false },
	{ L"TBS 6204 DVB-T/T2 Tuner 3", ttdte_nxp, false },

	{ L"TBS 6902 DVBS/S2 Tuner 0", ttdte_nxp, false },
	{ L"TBS 6902 DVBS/S2 Tuner 1", ttdte_nxp, false },

	{ L"TBS 6903 DVBS/S2 Tuner 0", ttdte_nxp, false },
	{ L"TBS 6903 DVBS/S2 Tuner 1", ttdte_nxp, false },

		//added 27/07/2015 
	{ L"TBS 6514 DTMB Tuner 0", ttdte_nxp, false },
	{ L"TBS 6514 DTMB Tuner 1", ttdte_nxp, false },
	{ L"TBS 6514 DTMB Tuner 2", ttdte_nxp, false },
	{ L"TBS 6514 DTMB Tuner 3", ttdte_nxp, false },

	{ L"TBS 6522 BDA Tuner 0", ttdte_nxp, false },
	{ L"TBS 6522 BDA Tuner 1", ttdte_nxp, false },

    // latest march 2016
	{L"TBS 6290Se DVBT/T2 Tuner 0", ttdte_nxp, true},
	{L"TBS 6290Se DVBT/T2 Tuner 1", ttdte_nxp, true},

	//20160905
	{L"TBS 6522 Tuner 0", ttdte_nxp, false},
	{L"TBS 6522 Tuner 1", ttdte_nxp, false},

	{L"TBS 6528 DVB CI Tuner", ttdte_nxp, true},
	{L"TBS 6590 DVB CI Tuner 0", ttdte_nxp, true},
	{L"TBS 6590 DVB CI Tuner 1", ttdte_nxp, true},
	
	{L"TBS 6209 Tuner 0", ttdte_nxp, false},
	{L"TBS 6209 Tuner 1", ttdte_nxp, false},
	{L"TBS 6209 Tuner 2", ttdte_nxp, false},
	{L"TBS 6209 Tuner 3", ttdte_nxp, false},
	{L"TBS 6209 Tuner 4", ttdte_nxp, false},
	{L"TBS 6209 Tuner 5", ttdte_nxp, false},
	{L"TBS 6209 Tuner 6", ttdte_nxp, false},
	{L"TBS 6209 Tuner 7", ttdte_nxp, false},
	
	{L"TBS 5927 USB DVB-S2 Tuner", ttdte_usb, false},

	{L"TBS 5580 BDA CI Tuner", ttdte_usb, true},
    
    {L"", ttdte_nxp, false} //last element
};

inline const tbs_tuner_desc_t* find_tbs_tuner_desc(const wchar_t* name)
{
    const tbs_tuner_desc_t* ret_val = NULL;
    int idx = 0;
    while (true)
    {
        if (boost::iequals(tbs_tuner_description[idx].filter_name, L""))
            break; //end of the list
            
        if (boost::iequals(tbs_tuner_description[idx].filter_name, name))
        {
            ret_val = tbs_tuner_description + idx;
            break; //found
        }
        ++idx;
    }
    return ret_val;
}

/*
//OEM tuners
#define TURBOSIGHT_8910_PROF_TUNER_FILTER_NAME  L"Prof 6200 BDA Tuner/Demod"
#define TURBOSIGHT_PROF_7500_TUNER_FILTER_NAME  L"Prof 7500 DVBS Tuner"
#define TURBOSIGHT_7301_TUNER_FILTER_NAME  L"Prof 7301 BDA Tuner/Demod"
#define TURBOSIGHT_8920_OMICOM_TUNER_FILTER_NAME  L"Omicom SkyStar 4 BDA Tuner/Demod"
#define TURBOSIGHT_8920_PROF_TUNER_FILTER_NAME  L"Prof 7300 BDA Tuner/Demod"
#define TURBOSIGHT_8920_SATRADE_TUNER_FILTER_NAME L"ST4200 BDA Tuner/Demod"
#define TURBOSIGHT_6950_TUNER_FILTER_NAME  L"TBS 6950 BDA DVBS/S2 Tuner"
#define TURBOSIGHT_8910_TUNER_FILTER_NAME  L"TBS 8910 BDA Tuner/Demod"
#define TURBOSIGHT_QBOX_PROF_TUNER_FILTER_NAME  L"Prof 1100 DVBS Tuner"
#define TURBOSIGHT_8000_TUNER_FILTER_NAME  L"Prof 8000 BDA DVBS/S2 Tuner/Demod"
*/

class tbs_ci_handler_t;

class turbosight_property_set_t : public property_set_base_t
{
public:
    turbosight_property_set_t();
    ~turbosight_property_set_t();

    virtual HRESULT property_set_init();
    virtual HRESULT property_set_deinit();

    virtual HRESULT property_set_send_diseqc(unsigned char *cmd, int len);
    virtual HRESULT property_set_send_toneburst(int type);
    virtual HRESULT property_set_set_22khz(bool active);
    virtual HRESULT property_set_set_modulation_params(dvblink::PTransponderInfo Tp);
    virtual HRESULT property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng);
protected:
    IKsPropertySet *ppropsetTunerPin;
    tbs_ci_handler_t* tbs_ci_handler_;
    int turbosight_last_22khz_state;
    int turbosight_last_toneburst_state;
};

class turbosight_qbox_property_set_t : public property_set_base_t
{
public:
    turbosight_qbox_property_set_t();
    ~turbosight_qbox_property_set_t();

    virtual HRESULT property_set_init();
    virtual HRESULT property_set_deinit();
    virtual HRESULT property_set_send_diseqc(unsigned char *cmd, int len);
    virtual HRESULT property_set_send_toneburst(int type);
    virtual HRESULT property_set_set_22khz(bool active);
    virtual HRESULT property_set_set_tuner(dvblink::PTransponderInfo Tp, int diseqc, int toneburst);
    virtual HRESULT property_set_get_tuner_state(dvblink::PSignalInfo stats);
    virtual HRESULT property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng);
protected:
    IKsPropertySet *ppropsetTunerFilter_qbox;
    tbs_ci_handler_t* tbs_ci_handler_;
    int turbosight_last_22khz_state;
    int turbosight_last_toneburst_state;
};

class turbosight_nxp_property_set_t : public property_set_base_t
{
public:
    turbosight_nxp_property_set_t();
    ~turbosight_nxp_property_set_t();

    virtual HRESULT property_set_init();
    virtual HRESULT property_set_deinit();
    virtual HRESULT property_set_send_diseqc(unsigned char *cmd, int len);
    virtual HRESULT property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng);
    virtual HRESULT property_set_after_graph_start();

protected:
    IKsPropertySet *ppropsetTunerPin;
    tbs_ci_handler_t* tbs_ci_handler_;
};
