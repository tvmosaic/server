/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

typedef HANDLE (*hStartCI)( IBaseFilter *Filter, WCHAR *tuner_name, int iDeviceIndex ); 
typedef void (*hExitCI)(HANDLE device);
typedef void (*hTBS_ci_SendPmt)(HANDLE device,unsigned char *tempmt, unsigned short uLength );
typedef void (*hTBS_ci_SendTDT)(HANDLE device,unsigned char *tempmt, unsigned short uLength );
typedef void (*hTBS_ci_MMI_Process)(HANDLE device,unsigned char *command,unsigned char *ret_data);
typedef bool (*hTBS_ci_Camavailable)(HANDLE device);

#define TBS_MAX_PMT_SIZE 1024
typedef struct _TBS_CA_DATA
{
	UCHAR uSlot;
	UCHAR uTag;
	BOOL bMore; //don�t care; set by driver
	USHORT uLength;
	UCHAR uData[TBS_MAX_PMT_SIZE];
} TBS_CA_DATA, *PTBS_CA_DATA;

class tbs_ci_handler_t
{
public:
	tbs_ci_handler_t();
	~tbs_ci_handler_t();

	bool init(IBaseFilter* tuner_filter, const wchar_t* tuner_name, const wchar_t* device_path, const wchar_t* dll_path);
	void term();

	bool send_pmt(unsigned char* pmt, int size, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd);
protected:
	HINSTANCE dll_handle_;
	HANDLE ci_lib_h_;

	hStartCI start_ci_f_;
	hExitCI exit_ci_f_;
	hTBS_ci_SendPmt send_pmt_f_;
	hTBS_ci_Camavailable ci_cam_avail_f_;
    std::wstring tuner_name_;
    std::wstring dll_path_;
    std::wstring device_path_;
};

