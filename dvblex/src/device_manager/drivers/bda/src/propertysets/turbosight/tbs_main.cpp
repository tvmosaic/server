/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>

#include <drivers/deviceapi.h>
#include "THBDA_Ioctl.h"
#include "../../common.h"		
#include <dl_dshow_enum.h>
#include "tbsci.h"		
#include "tbs_main.h"		
#include <dl_logger.h>

using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblink;

/*****************************************************************************

 8910 and 8920 Functions

*****************************************************************************/

turbosight_property_set_t::turbosight_property_set_t() :
    ppropsetTunerPin(NULL)
{
    tbs_ci_handler_ = new tbs_ci_handler_t();
}

turbosight_property_set_t::~turbosight_property_set_t()
{
    delete tbs_ci_handler_;
}

HRESULT turbosight_property_set_t::property_set_init()
{
	log_info(L"turbosight_init()");
	HRESULT hr;

    turbosight_last_22khz_state = HZ_22K_OFF;
    turbosight_last_toneburst_state = Value_Burst_OFF;

	// get the property sets on the tuner filter pins
	hr = get_pin_property_set(graph_info_.pfltTuner_, -1, &ppropsetTunerPin);	
	if FAILED(hr) 
		log_warning(L"No propertyset interface on the pins of the tuner filter");

    tbs_ci_handler_->init(graph_info_.pfltTuner_, tuner_name_.c_str(), device_path_.c_str(), dll_path_.c_str());

	return hr;
}


HRESULT turbosight_property_set_t::property_set_deinit()
{
	tbs_ci_handler_->term();

	if(ppropsetTunerPin) {
		ppropsetTunerPin->Release();
		ppropsetTunerPin = NULL;
	}

	return S_OK;
}

static HRESULT ext_send_diseqc(unsigned char *cmd, int len, IKsPropertySet *propset_if)
{

	HRESULT hr = E_FAIL;
	DWORD TypeSupport=0;
	TBS_ACCESS_STRUCT TBSAccessCmd;	

	if (propset_if) 
	{
		hr = propset_if->QuerySupported(KSPROPSETID_BdaTunerExtensionProperties, KSPROPERTY_BDA_TBSACCESS, &TypeSupport);	
		
		if (SUCCEEDED(hr) && (TypeSupport & KSPROPERTY_SUPPORT_SET) ) {		
			log_info(L"turbosight ext_send_diseqc: using the new diseqc interface (KSPROPERTY_BDA_TBSACCESS)");
			
			ZeroMemory(&TBSAccessCmd, sizeof(TBSAccessCmd));	
			TBSAccessCmd.access_mode = TBSACCESS_DISEQC;
			TBSAccessCmd.uc_diseqc_send_message_length = len;		
			CopyMemory(&TBSAccessCmd.uc_diseqc_send_message[0], cmd, len);		
			hr = propset_if->Set(KSPROPSETID_BdaTunerExtensionProperties, KSPROPERTY_BDA_TBSACCESS, 
				NULL, 0, &TBSAccessCmd, sizeof(TBS_ACCESS_STRUCT));
			
			if FAILED(hr)  
			    log_error(L"turbosight ext_send_diseqc: Turbosight new interface PCI/NXP diseqc cmd failed!");			
		};
	}

	if SUCCEEDED(hr) 
	    return hr;

	if (propset_if) 
	{
		hr = propset_if->QuerySupported(KSPROPERTYSET_QBOXControl, KSPROPERTY_CTRL_TBSACCESS, &TypeSupport);	
		
		if (SUCCEEDED(hr) && (TypeSupport & KSPROPERTY_SUPPORT_SET) ) 
		{
			log_info(L"turbosight ext_send_diseqc: using the new diseqc interface (KSPROPERTY_CTRL_TBSACCESS)");	
			
			ZeroMemory(&TBSAccessCmd, sizeof(TBSAccessCmd));	
			TBSAccessCmd.access_mode = TBSACCESS_DISEQC;
			TBSAccessCmd.uc_diseqc_send_message_length = len;		
			CopyMemory(&TBSAccessCmd.uc_diseqc_send_message[0], cmd, len);		
			hr = propset_if->Set(KSPROPERTYSET_QBOXControl, KSPROPERTY_CTRL_TBSACCESS, 
				NULL, 0, &TBSAccessCmd, sizeof(TBS_ACCESS_STRUCT));
				
			if FAILED(hr)  
			    log_info(L"turbosight ext_send_diseqc: Turbosight new interface USB diseqc cmd failed!");			
		}
	}	
	return hr;
}

HRESULT turbosight_property_set_t::property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng)
{
	return tbs_ci_handler_->send_pmt(buf, len, listmng) ? S_OK : E_FAIL;
}

HRESULT turbosight_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
	wchar_t s[255];	
	diseqc2str(cmd, len, &s[0]);
	log_info(L"turbosight_send_diseqc(<%s>, %d)") % s % len;

    //try new interface first
    HRESULT hr = ext_send_diseqc(cmd, len, ppropsetTunerPin);
    if (SUCCEEDED(hr))
        return hr;

	DWORD TypeSupport=0;
	hr = ppropsetTunerPin->QuerySupported(KSPROPSETID_BdaTunerExtensionProperties,
							KSPROPERTY_BDA_DISEQC_MESSAGE, 
							&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Turbosight KSPROPERTY_BDA_DISEQC_MESSAGE not supported!");			
			return E_FAIL;
	}

	DWORD bytesReturned;
	DISEQC_MESSAGE_PARAMS DiSEqCRequest;
	ZeroMemory(&DiSEqCRequest, sizeof(DiSEqCRequest));
    DiSEqCRequest.uc_diseqc_send_message_length = len;
	DiSEqCRequest.tbscmd_mode = TBSDVBSCMD_DISEQC;
	CopyMemory(DiSEqCRequest.uc_diseqc_send_message, cmd, len);

	hr = ppropsetTunerPin->Get(KSPROPSETID_BdaTunerExtensionProperties, 
                        				KSPROPERTY_BDA_DISEQC_MESSAGE, 
					      			  &DiSEqCRequest,sizeof(DISEQC_MESSAGE_PARAMS),
					        			&DiSEqCRequest, sizeof(DISEQC_MESSAGE_PARAMS),
                            				&bytesReturned);
	Sleep(200);
	
	if FAILED(hr)	
	{
			log_error(L"Turbosight KSPROPERTY_BDA_DISEQC_MESSAGE failed!");			
			return hr;
	}


	return hr;
}

HRESULT turbosight_property_set_t::property_set_send_toneburst(int type)
{
	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerPin->QuerySupported(KSPROPSETID_BdaTunerExtensionProperties,
							KSPROPERTY_BDA_DISEQC_MESSAGE, 
							&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Turbosight KSPROPERTY_BDA_DISEQC_MESSAGE not supported!");			
			return E_FAIL;	
	}


	DWORD bytesReturned;
	DISEQC_MESSAGE_PARAMS DiSEqCRequest;
	ZeroMemory(&DiSEqCRequest, sizeof(DiSEqCRequest));
	DiSEqCRequest.tbscmd_mode = TBSDVBSCMD_22KTONEDATA;    
	DiSEqCRequest.HZ_22K = turbosight_last_22khz_state;	
	switch (type) {
		case dvblink::TONEBURST_NONE: 
			turbosight_last_toneburst_state = Value_Burst_OFF;
			break;
		case dvblink::TONEBURST_1: 
			turbosight_last_toneburst_state = Value_Tone_Burst_ON;
			break;
		case dvblink::TONEBURST_2: 
			turbosight_last_toneburst_state = Value_Data_Burst_ON;
			break;
	}
	DiSEqCRequest.Tone_Data_Burst = turbosight_last_toneburst_state;

	hr = ppropsetTunerPin->Get(KSPROPSETID_BdaTunerExtensionProperties, 
                        				KSPROPERTY_BDA_DISEQC_MESSAGE, 
					      			  &DiSEqCRequest,sizeof(DISEQC_MESSAGE_PARAMS),
					        			&DiSEqCRequest, sizeof(DISEQC_MESSAGE_PARAMS),
                            				&bytesReturned);

	if FAILED(hr)	
	{
			log_error(L"Turbosight KSPROPERTY_BDA_DISEQC_MESSAGE failed!");			
			return hr;
	}

	return hr;

}

HRESULT turbosight_property_set_t::property_set_set_22khz(bool active)
{
	turbosight_last_22khz_state	= active?HZ_22K_ON:HZ_22K_OFF;

	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerPin->QuerySupported(KSPROPSETID_BdaTunerExtensionProperties,
							KSPROPERTY_BDA_DISEQC_MESSAGE, 
							&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Turbosight KSPROPERTY_BDA_DISEQC_MESSAGE not supported!");			
			return E_FAIL;
	}

	DWORD bytesReturned;
	DISEQC_MESSAGE_PARAMS DiSEqCRequest;
	ZeroMemory(&DiSEqCRequest, sizeof(DiSEqCRequest));
	DiSEqCRequest.tbscmd_mode = TBSDVBSCMD_22KTONEDATA;    
	DiSEqCRequest.Tone_Data_Burst = turbosight_last_toneburst_state;    
	DiSEqCRequest.HZ_22K = turbosight_last_22khz_state;

	hr = ppropsetTunerPin->Get(KSPROPSETID_BdaTunerExtensionProperties, 
                        				KSPROPERTY_BDA_DISEQC_MESSAGE, 
					      			  &DiSEqCRequest,sizeof(DISEQC_MESSAGE_PARAMS),
					        			&DiSEqCRequest, sizeof(DISEQC_MESSAGE_PARAMS),
                            				&bytesReturned);

	if FAILED(hr)	
	{
			log_error(L"Turbosight KSPROPERTY_BDA_DISEQC_MESSAGE failed!");			
			return hr;
	}

	return hr;
}


static BinaryConvolutionCodeRate turbosight_get_fec(int fec)
{
	switch(fec) {
		case  dvblink::FEC_1_2:
			return BDA_BCC_RATE_1_2;				
			break;
		case  dvblink::FEC_1_3:
			return BDA_BCC_RATE_1_3;				
			break;
		case  dvblink::FEC_1_4:
			return BDA_BCC_RATE_1_4;				
			break;
		case  dvblink::FEC_2_3:
			return BDA_BCC_RATE_2_3;	
			break;
		case  dvblink::FEC_2_5:
			return BDA_BCC_RATE_2_5;	
			break;
		case  dvblink::FEC_3_4:
			return BDA_BCC_RATE_3_4;				
			break;
		case  dvblink::FEC_3_5:
			return BDA_BCC_RATE_3_5;
			break;
		case  dvblink::FEC_4_5:
			return BDA_BCC_RATE_4_5;
			break;
		case  dvblink::FEC_5_6:
			return BDA_BCC_RATE_5_6;				
			break;
		case  dvblink::FEC_5_11:
			return BDA_BCC_RATE_5_11;				
			break;
		case  dvblink::FEC_6_7:
			return BDA_BCC_RATE_6_7;				
			break;
		case  dvblink::FEC_7_8:
			return BDA_BCC_RATE_7_8;				
			break;
		case  dvblink::FEC_8_9:
			return BDA_BCC_RATE_5_11;  // special case for turbosight
			break;
		case  dvblink::FEC_9_10:
			return BDA_BCC_RATE_7_8; // special case for turbosight
			break;
		default:
			return BDA_BCC_RATE_NOT_SET;
			break;
	}		
}

HRESULT turbosight_property_set_t::property_set_set_modulation_params(dvblink::PTransponderInfo Tp)
{
	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerPin->QuerySupported(KSPROPSETID_BdaTunerExtensionProperties,
							KSPROPERTY_BDA_NBC_PARAMS, 
							&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Turbosight KSPROPERTY_BDA_NBC_PARAMS not supported!");			
			return E_FAIL;
	}


	BDA_NBC_PARAMS DVBType;
	ZeroMemory(&DVBType, sizeof(DVBType));
	//*removed DVBType.dvbtype = BDACardType == BDACT_TBS_8920 ? 2 : 1;	
	DVBType.fecrate = turbosight_get_fec(Tp->dwFec);  // meaningful only for 8920
	//DVBType.fecrate = BDA_BCC_RATE_NOT_SET;
	//*need to know: if fecrate=auto search,and Modulation is s2,than DVBType.dvbtype=0;driver will searsh all kinds of fecrate,slow
	//*                     if fecrate=auto,and Modulation is s1,than DVBType.dvbtype=1;driver auto search s1's fecrate
	//*			    if fecrate!=auto,and Modulation is s1,so  DVBType.dvbtype=1;driver auto search s1's fecrate
	//*		 	    if fecrate!=auto,and Modulation is s2,so DVBType.dvbtype=2;driver will searsh desire fecrate,very faster
	//* dvbtype==0  ScanDVBAutoTransponder   it will search both s1 and s2,slow
	//* dvbtype==1  ScanDVBSTransponder   only search s1,auto search fec
	//* dvbtype==2  ScanDVBS2Transponder   only search s2,but not auto search fec,just you want kind
	//* because this conexant cx24116 is auto search s1,but not auto search s2,big problem is it
	//* if user don't know the channle s1 or s2,than use _dvb_type==0
	//* if user know the channle is s1,_dvb_type==1
	//* if user know the channle is s2,and know fec type,than use _dvb_type==2
	//* if user not sure the s2's fec type ,_dvb_type==0
	//* so the chip is not good,we will use new chip ,next hardware version
	//* i forget to tell you ,that 8910 had change the demodulation chip.  new version is not stv0299,is stv0288.  we test stv0288 is good,in my lab
	switch(Tp->dwModulation) {  
		case dvblink::MOD_DVBS_QPSK:  // DVB-S QPSK
			DVBType.modtype = BDA_MOD_QPSK;
			DVBType.dvbtype = 1; //*
			break;
		case dvblink::MOD_DVBS_NBC_QPSK:  // DVB-S2 QPSK (NBC)
			DVBType.modtype = BDA_MOD_OQPSK;
			DVBType.dvbtype = 2; //*
            if(Tp->dwFec == dvblink::FEC_AUTO) //*
				DVBType.dvbtype = 0; //*
			break;
		case dvblink::MOD_DVBS_8PSK:  // DVB-S2 8PSK
			DVBType.modtype = BDA_MOD_BPSK;
			DVBType.dvbtype = 2; //*
			if(Tp->dwFec == dvblink::FEC_AUTO) //*
				DVBType.dvbtype = 0; //*
			break;
		default:
			DVBType.modtype = BDA_MOD_QPSK;
			DVBType.dvbtype = 0; //*
	}
	
	hr = ppropsetTunerPin->Set(KSPROPSETID_BdaTunerExtensionProperties, 
                        				KSPROPERTY_BDA_NBC_PARAMS, 
					      			  &DVBType, sizeof(DVBType),
					        			&DVBType, sizeof(DVBType)
									);

	if FAILED(hr)	
	{
			log_error(L"Turbosight KSPROPERTY_BDA_NBC_PARAMS failed!");			
			return hr;
	}

	return hr;
}

/*
HRESULT turbosight_property_set_t::property_set_set_lnb_power(bool active)
{

	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerPin->QuerySupported(KSPROPSETID_BdaTunerExtensionProperties,
							KSPROPERTY_BDA_DISEQC_MESSAGE, 
							&TypeSupport);	

	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Turbosight KSPROPERTY_BDA_DISEQC_MESSAGE not supported!");			
			return E_FAIL;
	}

	DWORD bytesReturned;
	DISEQC_MESSAGE_PARAMS DiSEqCRequest;
	ZeroMemory(&DiSEqCRequest, sizeof(DiSEqCRequest));
	DiSEqCRequest.tbscmd_mode = TBSDVBSCMD_LNBPOWER;
	DiSEqCRequest.b_LNBPower = !active;

	hr = ppropsetTunerPin->Get(KSPROPSETID_BdaTunerExtensionProperties, 
                        				KSPROPERTY_BDA_DISEQC_MESSAGE, 
					      			  &DiSEqCRequest,sizeof(DISEQC_MESSAGE_PARAMS),
					        			&DiSEqCRequest, sizeof(DISEQC_MESSAGE_PARAMS),
                            				&bytesReturned);

	if FAILED(hr)	
	{
			log_error(L"Turbosight KSPROPERTY_BDA_DISEQC_MESSAGE failed!");			
			return hr;
	}

	return hr;

}
*/
/*
static HRESULT create_dvbs_locator(dvblink::PTransponderInfo Tp, IDVBSLocator **ppDVBSLocator)
{	
	HRESULT hr;

	if FAILED(hr = CoCreateInstance(__uuidof(DVBSLocator), NULL, CLSCTX_INPROC_SERVER, __uuidof(IDVBSLocator), (void**) ppDVBSLocator)) {
		log_error(L"cannot create the locator");
		return hr;
	}

	(*ppDVBSLocator)->put_CarrierFrequency(Tp->dwFreq);
	(*ppDVBSLocator)->put_SymbolRate((int)(Tp->dwSr / 1000));			
	(*ppDVBSLocator)->put_SignalPolarisation(Tp->Pol==dvblink::POL_VERTICAL ? BDA_POLARISATION_LINEAR_V : BDA_POLARISATION_LINEAR_H);		
	(*ppDVBSLocator)->put_InnerFECRate(property_set_get_fec(Tp->dwFec));				
	//(*ppDVBSLocator)->put_InnerFEC(BDA_FEC_METHOD_NOT_SET);		
	(*ppDVBSLocator)->put_Modulation(property_set_get_dvbs_modulation(Tp->dwModulation));
	return hr;
}

static HRESULT put_lnb_parameters(dvblink::PTransponderInfo Tp, int diseqc)
{
	HRESULT hr;
	IDVBSTuningSpace  *pDVBSTuningSpace = NULL;
	if FAILED(hr = g_pTuningSpace->QueryInterface(__uuidof(IDVBSTuningSpace), (void **)&pDVBSTuningSpace)) {
		log_error(L"cannot get IDVBSTuningSpace interface");
		return hr;
	}
	
	// DVB-S LNB parameters		
	pDVBSTuningSpace->put__NetworkType(CLSID_DVBSNetworkProvider);
    pDVBSTuningSpace->put_LNBSwitch(Tp->LOFSW);		
    pDVBSTuningSpace->put_LowOscillator(Tp->LOF1);
    pDVBSTuningSpace->put_HighOscillator(Tp->LOF2);
	pDVBSTuningSpace->put_SpectralInversion(BDA_SPECTRAL_INVERSION_AUTOMATIC);		

	pDVBSTuningSpace->Release();
	return S_OK;
}

static HRESULT int_tune_request(dvblink::PTransponderInfo Tp, int diseqc)
{
	HRESULT hr = S_OK;

	put_lnb_parameters(Tp, diseqc);

	// create tune request
	ITuneRequest* pTuneRequest;
	if (FAILED(hr = g_pTuningSpace->CreateTuneRequest(&pTuneRequest)))	
    {
		log_error(L"cannot create Tune Request");
		return hr; 
	}

	// get interface for the tune request
	IDVBTuneRequest *pDVBTuneRequest = NULL;
	hr=pTuneRequest->QueryInterface(__uuidof(IDVBTuneRequest), (void**)&pDVBTuneRequest);
    pTuneRequest->Release();

    if (hr == S_OK && pDVBTuneRequest != NULL)
    {
		pDVBTuneRequest->put_ONID(-1);
		pDVBTuneRequest->put_TSID(-1);
		pDVBTuneRequest->put_SID(-1);
		
		IDVBSLocator *pDVBSLocator = NULL;
	    if (SUCCEEDED(hr = create_dvbs_locator(Tp, &pDVBSLocator)))
        {
		    hr = pDVBTuneRequest->put_Locator(pDVBSLocator);
            pDVBSLocator->Release();
	    }

        if (hr == S_OK)
        {
            ITuner* tuner = NULL;
	        hr = g_pfltnetworkprovider->QueryInterface(__uuidof(ITuner), (void **)&tuner);
            if (hr == S_OK)
            {
	            try 
                {
                    hr = tuner->put_TuneRequest(pDVBTuneRequest);
	            } catch(...)
	            {
                    hr = E_FAIL;
	            }
                if (FAILED(hr))
		            log_error(L"put_TuneRequest()");

                tuner->Release();
            } else
            {
	            log_error(L"cannot query tuner from IBaseFilter");
            }
        }
        else
        {
		    log_error(L"cannot put locator");
        }

        pDVBTuneRequest->Release();
    }
    else
    {
		log_error(L"cannot get IDVBTuneRequest interface");
    }
	
	return hr;
}

HRESULT turbosight_set_tuner(dvblink::PTransponderInfo Tp, int diseqc, int toneburst)
{
    if (TunerType != dvblink::TUNERTYPE_DVBS)
        return E_NOINTERFACE; //do regular tuning for non-dvb-s tuners

    HRESULT ret_val = E_FAIL;
	turbosight_set_22khz(Tp->dwLnbKHz != dvblink::LNB_SELECTION_0);		
	turbosight_set_modulation_params(Tp);

	ret_val = int_tune_request(Tp, diseqc);

	if (FAILED(ret_val))
    {
        log_error(L"turbosight_set_tuner failed");
    } else
	{
		//sleep 200 ms to give tuner a chance to tune
		Sleep(200);
	}
	return ret_val;
}
*/
/*****************************************************************************

 QBOX Functions

*****************************************************************************/

turbosight_qbox_property_set_t::turbosight_qbox_property_set_t() :
    ppropsetTunerFilter_qbox(NULL)
{
    tbs_ci_handler_ = new tbs_ci_handler_t();
}

turbosight_qbox_property_set_t::~turbosight_qbox_property_set_t()
{
    delete tbs_ci_handler_;
}

HRESULT turbosight_qbox_property_set_t::property_set_init()
{
	log_info(L"turbosight_qbox_init()");
	HRESULT hr;

    turbosight_last_22khz_state = HZ_22K_OFF;
    turbosight_last_toneburst_state = Value_Burst_OFF;

	// get the property sets on the tuner filter pins
	hr = get_filter_property_set(graph_info_.pfltTuner_, &ppropsetTunerFilter_qbox);
	if FAILED(hr) 
		log_warning(L"No propertyset interface on the tuner filter");

    tbs_ci_handler_->init(graph_info_.pfltTuner_, tuner_name_.c_str(), device_path_.c_str(), dll_path_.c_str());

	return hr;
}

HRESULT turbosight_qbox_property_set_t::property_set_deinit()
{
	tbs_ci_handler_->term();

	if(ppropsetTunerFilter_qbox) {
		ppropsetTunerFilter_qbox->Release();
		ppropsetTunerFilter_qbox = NULL;
	}
	return S_OK;
}

HRESULT turbosight_qbox_property_set_t::property_set_set_22khz(bool active)
{
	turbosight_last_22khz_state	= active?HZ_22K_ON:HZ_22K_OFF;

	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerFilter_qbox->QuerySupported(KSPROPERTYSET_QBOXControl, 
                        				KSPROPERTY_CTRL_22K_TONE, 
										&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Turbosight QBox KSPROPERTY_CTRL_22K_TONE not supported!");			
			return E_FAIL;
	}


	QBOXDVBSCMD QBoxCmd;
	ZeroMemory(&QBoxCmd, sizeof(QBoxCmd));
	QBoxCmd.HZ_22K=turbosight_last_22khz_state;
	QBoxCmd.Tone_Data_Burst=turbosight_last_toneburst_state;

	hr = ppropsetTunerFilter_qbox->Set(KSPROPERTYSET_QBOXControl, 
                        				KSPROPERTY_CTRL_22K_TONE, 
					      			    NULL,
									    0,
										&QBoxCmd,
										sizeof( QBOXDVBSCMD ));

	if FAILED(hr)	
	{
			log_error(L"Turbosight QBox KSPROPERTY_CTRL_22K_TONE failed!");			
			return hr;
	}

	return hr;

}

HRESULT turbosight_qbox_property_set_t::property_set_send_toneburst(int type)
{
	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerFilter_qbox->QuerySupported(KSPROPERTYSET_QBOXControl, 
                        				KSPROPERTY_CTRL_22K_TONE, 
										&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Turbosight QBox KSPROPERTY_CTRL_22K_TONE not supported!");			
			return E_FAIL;
	}


	QBOXDVBSCMD QBoxCmd;
	ZeroMemory(&QBoxCmd, sizeof(QBoxCmd));
	QBoxCmd.HZ_22K = turbosight_last_22khz_state;	
	switch (type) {
		case dvblink::TONEBURST_NONE: 
			turbosight_last_toneburst_state = Value_Burst_OFF;
			break;
		case dvblink::TONEBURST_1: 
			turbosight_last_toneburst_state = Value_Tone_Burst_ON;
			break;
		case dvblink::TONEBURST_2: 
			turbosight_last_toneburst_state = Value_Data_Burst_ON;
			break;
	}
	QBoxCmd.Tone_Data_Burst = turbosight_last_toneburst_state;

	hr = ppropsetTunerFilter_qbox->Set(KSPROPERTYSET_QBOXControl, 
                        				KSPROPERTY_CTRL_22K_TONE, 
					      			    NULL,
									    0,
										&QBoxCmd,
										sizeof( QBOXDVBSCMD ));

	if FAILED(hr)	
	{
			log_error(L"Turbosight QBox KSPROPERTY_CTRL_22K_TONE failed!");			
			return hr;
	}


	return hr;

}

HRESULT turbosight_qbox_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
	wchar_t s[255];	
	diseqc2str(cmd, len, &s[0]);
	log_info(L"turbosight_qbox_send_diseqc (<%s>, %d)") % s % len;

    //try new interface first
    HRESULT hr = ext_send_diseqc(cmd, len, ppropsetTunerFilter_qbox);
    if (SUCCEEDED(hr))
        return hr;

	DWORD TypeSupport=0;
	hr = ppropsetTunerFilter_qbox->QuerySupported(KSPROPERTYSET_QBOXControl, 
                        				KSPROPERTY_CTRL_MOTOR, 
										&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Turbosight QBox KSPROPERTY_CTRL_MOTOR not supported!");			
			return E_FAIL;
	}


	QBOXDVBSCMD QBoxCmd;
	ZeroMemory(&QBoxCmd, sizeof(QBoxCmd));	
	CopyMemory(QBoxCmd.motor, cmd, len);	

	hr = ppropsetTunerFilter_qbox->Set(KSPROPERTYSET_QBOXControl, 
                        				KSPROPERTY_CTRL_MOTOR, 
					      			    NULL,
									    0,
										&QBoxCmd,
										sizeof( QBOXDVBSCMD ));

	Sleep(200);

	if FAILED(hr)	
	{
			log_error(L"Turbosight QBox KSPROPERTY_CTRL_MOTOR failed!");			
			return hr;
	}

	return hr;

}
/*
HRESULT turbosight_qbox_property_set_t::property_set_set_lnb_power(bool active)
{
	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerFilter_qbox->QuerySupported(KSPROPERTYSET_QBOXControl, 
                        				KSPROPERTY_CTRL_LNBPW, 
										&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Turbosight QBox KSPROPERTY_CTRL_LNBPW not supported!");			
			return E_FAIL;
	}


	QBOXDVBSCMD QBoxCmd;
	ZeroMemory(&QBoxCmd, sizeof(QBoxCmd));	
	QBoxCmd.LNB_POWER = active;

	hr = ppropsetTunerFilter_qbox->Set(KSPROPERTYSET_QBOXControl, 
                        				KSPROPERTY_CTRL_LNBPW, 
					      			    NULL,
									    0,
										&QBoxCmd,
										sizeof( QBOXDVBSCMD ));

	if FAILED(hr)	
	{
			log_error(L"Turbosight QBox KSPROPERTY_CTRL_LNBPW failed!");			
			return hr;
	}


	return hr;

}
*/

HRESULT turbosight_qbox_property_set_t::property_set_set_tuner(dvblink::PTransponderInfo Tp, int diseqc, int toneburst)
{
    return E_NOINTERFACE;
/*
	DWORD TypeSupport=0;
	HRESULT hr;

	QBOXDVBSCMD QBoxCmd;
	ZeroMemory(&QBoxCmd, sizeof(QBoxCmd))	;
	QBoxCmd.ChannelFrequency = Tp->dwFreq / 1000;
	QBoxCmd.SymbolRate = Tp->dwSr / 1000;
	QBoxCmd.ulLNBLOFHighBand = Tp->LOF2 / 1000;
	QBoxCmd.ulLNBLOFLowBand = Tp->LOF1 / 1000;
	QBoxCmd.Polarity = Tp->Pol;

	
	// ** following lines might be unnecessarry for KSPROPERTY_CTRL_LOCK_TUNER***
	QBoxCmd.LNB_POWER = LNB_POWER_ON;
	QBoxCmd.HZ_22K = Tp->dwLnbKHz == dvblink::LNB_SELECTION_0 ? HZ_22K_OFF:HZ_22K_ON;
	QBoxCmd.Tone_Data_Burst = turbosight_last_toneburst_state;
	// *** / ***

	hr = ppropsetTunerFilter_qbox->Set(KSPROPERTYSET_QBOXControl, 
                        				KSPROPERTY_CTRL_LOCK_TUNER, 
					      			    NULL,
									    0,
										&QBoxCmd,
										sizeof( QBOXDVBSCMD ));


	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Turbosight QBox KSPROPERTY_CTRL_LOCK_TUNER failed!");			
			return E_FAIL;
	}

	return hr;
*/
}

HRESULT turbosight_qbox_property_set_t::property_set_get_tuner_state(dvblink::PSignalInfo stats) 
{
    //return no interface if tuner is not a dvb-s tuner
    if (tuner_type_ != dvblink::TUNERTYPE_DVBS)
        return E_NOINTERFACE;

    DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerFilter_qbox->QuerySupported(KSPROPERTYSET_QBOXControl, 
                        				KSPROPERTY_CTRL_TUNER, 
										&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Turbosight QBox KSPROPERTY_CTRL_TUNER not supported!");			
			return E_FAIL;
	}


	QBOXDVBSCMD QBoxCmd;
	ZeroMemory(&QBoxCmd, sizeof(QBoxCmd));	

	DWORD BytesRead;
	hr = ppropsetTunerFilter_qbox->Get(KSPROPERTYSET_QBOXControl, 
                        				KSPROPERTY_CTRL_TUNER, 
										&QBoxCmd,
										sizeof( QBOXDVBSCMD ),
										&QBoxCmd,
										sizeof( QBOXDVBSCMD ),
										&BytesRead);


	stats->Level = QBoxCmd.strength;
	stats->Quality = QBoxCmd.quality;
	stats->Locked = QBoxCmd.lock;

	if FAILED(hr)	
	{
			log_error(L"Turbosight QBox KSPROPERTY_CTRL_TUNER failed!");			
			return hr;
	}

	return hr;
	
}

HRESULT turbosight_qbox_property_set_t::property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng)
{
	return tbs_ci_handler_->send_pmt(buf, len, listmng) ? S_OK : E_FAIL;
}

//******************************************************************************
turbosight_nxp_property_set_t::turbosight_nxp_property_set_t() :
    ppropsetTunerPin(NULL)
{
    tbs_ci_handler_ = new tbs_ci_handler_t();
}

turbosight_nxp_property_set_t::~turbosight_nxp_property_set_t()
{
    delete tbs_ci_handler_;
}

HRESULT turbosight_nxp_property_set_t::property_set_init()
{
	log_info(L"turbosight_init()");
	HRESULT hr;

	// get the property sets on the tuner filter pins
	hr = get_pin_property_set(graph_info_.pfltTuner_, -1, &ppropsetTunerPin);	
	if FAILED(hr) 
		log_warning(L"No propertyset interface on the pins of the tuner filter");

	return hr;
}

HRESULT turbosight_nxp_property_set_t::property_set_after_graph_start()
{
    tbs_ci_handler_->init(graph_info_.pfltTuner_, tuner_name_.c_str(), device_path_.c_str(), dll_path_.c_str());

    return S_OK;
}

HRESULT turbosight_nxp_property_set_t::property_set_deinit()
{
	tbs_ci_handler_->term();

	if(ppropsetTunerPin) {
		ppropsetTunerPin->Release();
		ppropsetTunerPin = NULL;
	}

	return S_OK;
}

HRESULT turbosight_nxp_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
	wchar_t s[255];	
	diseqc2str(cmd, len, &s[0]);
	log_info(L"turbosight_nxp_send_diseqc(<%s>, %d)") % s % len;

    //try new interface first
    HRESULT hr = ext_send_diseqc(cmd, len, ppropsetTunerPin);
    if (SUCCEEDED(hr))
        return hr;

	DWORD TypeSupport=0;

	if (ppropsetTunerPin != NULL)
	{

		hr = ppropsetTunerPin->QuerySupported(KSPROPSETID_BdaTunerExtensionProperties,
			KSPROPERTY_BDA_DISEQC_MESSAGE, 
			&TypeSupport);

		if FAILED(hr)
		{
			log_error(L"ERROR: Turbosight NXP BDA_DISEQC_MESSAGE not supported!");			
			return E_FAIL;
		}
	}
 	//DWORD bytesReturned;

	//added 2011 02 16
	TBSDISEQC_MESSAGE_PARAMS BadDisqcSend;
	ZeroMemory(&BadDisqcSend, sizeof(BadDisqcSend));	
	BadDisqcSend.uc_diseqc_send_message_length = len;
	CopyMemory(BadDisqcSend.uc_diseqc_send_message, cmd, len);	

	hr = ppropsetTunerPin->Set(KSPROPSETID_BdaTunerExtensionProperties, 
		KSPROPERTY_BDA_DISEQC_MESSAGE, 
		&BadDisqcSend,
		sizeof( TBSDISEQC_MESSAGE_PARAMS ),
		&BadDisqcSend,
		sizeof( TBSDISEQC_MESSAGE_PARAMS ));

	if FAILED(hr)	
	{
		log_error(L"FAILED: Turbosight NXP BDA_DISEQC_MESSAGE failed!");				
		return hr;
	}
	else
	{
		log_info(L"Success: Turbosight NXP BDA_DISEQC_MESSAGE success!");	
		return hr;
	}

	return hr;

}

HRESULT turbosight_nxp_property_set_t::property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng)
{
	return tbs_ci_handler_->send_pmt(buf, len, listmng) ? S_OK : E_FAIL;
}
