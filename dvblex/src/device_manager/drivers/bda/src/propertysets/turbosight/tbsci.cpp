/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>

#include <drivers/deviceapi.h>
#include "THBDA_Ioctl.h"
#include "../../common.h"		
#include <dl_dshow_enum.h>
#include "tbsci.h"
#include "tbs_main.h"
#include "../../capmt.h"
#include "../../decrypt_cmd_to_listmgmt.h"
#include <dl_logger.h>

using namespace dvblink::logging;
using namespace dvblink::engine;

tbs_ci_handler_t::tbs_ci_handler_t() :
	dll_handle_(NULL), start_ci_f_(NULL), exit_ci_f_(NULL), send_pmt_f_(NULL), ci_cam_avail_f_(NULL)
{
}

tbs_ci_handler_t::~tbs_ci_handler_t()
{
}

static int get_filter_index(const wchar_t* device_path)
{
    int ret_val = -1;

	TDSEnum *penumBDATuners = enum_create(KSCATEGORY_BDA_NETWORK_TUNER);
	if(!penumBDATuners) 
        return -1;
	
    int i=0;
	while (enum_next(penumBDATuners) == S_OK) 
    {
		if (SUCCEEDED(enum_get_name(penumBDATuners)))
        {
            if (boost::iequals(penumBDATuners->szDevicePathW, device_path))
            {
                ret_val = i;
                break;
            }
        }
		i++;
	}

	enum_free(penumBDATuners);

	return ret_val;
}

bool tbs_ci_handler_t::init(IBaseFilter* tuner_filter, const wchar_t* tuner_name, const wchar_t* device_path, const wchar_t* dll_path)
{
	bool ret_val = false;

    //until we get this working properly - no support for TBS CI
//    dll_handle_ = NULL;
//    return false;

    tuner_name_ = tuner_name;
    dll_path_ = dll_path;
    device_path_ = device_path;

    const tbs_tuner_desc_t* tbs_desc = find_tbs_tuner_desc(tuner_name_.c_str());
    if (tbs_desc != NULL && tbs_desc->has_ci)
    {
		//get current device index
		int device_idx = get_filter_index(device_path_.c_str());
		if (device_idx == -1)
		{
			log_error(L"tbs_ci_handler_t::init. get_tuner_index returned -1");
			return false;
		}
	    //load ci handling dll
	    std::wstring dll_pathname = dll_path_;
	    dll_pathname += L"\\TbsCIapi.dll";
	    log_info(L"TBS: Loading %s") % dll_pathname.c_str();
	    dll_handle_ = LoadLibrary(dll_pathname.c_str());
	    if (dll_handle_ != NULL)
	    {
		    //Initialize functions
		    start_ci_f_ = (hStartCI)GetProcAddress(dll_handle_, "On_Start_CI2");
		    exit_ci_f_ = (hExitCI)GetProcAddress(dll_handle_, "On_Exit_CI");
		    send_pmt_f_ = (hTBS_ci_SendPmt)GetProcAddress(dll_handle_, "TBS_ci_SendPmt");
		    ci_cam_avail_f_ = (hTBS_ci_Camavailable)GetProcAddress(dll_handle_, "Camavailable");
		    if (start_ci_f_ != NULL &&
			    exit_ci_f_ != NULL &&
			    send_pmt_f_ != NULL &&
			    ci_cam_avail_f_ != NULL )
		    {
			    //try to start CI handling (this may fail if no CI is present/supported)
			    ci_lib_h_ = start_ci_f_(tuner_filter, (wchar_t*)tuner_name_.c_str(), device_idx);
			    if (ci_lib_h_ != NULL)
			    {
                    bool b = ci_cam_avail_f_(ci_lib_h_);
                    log_info(L"TBS CI module control was started and initialized. Cam availability: %1%") % b;
				    ret_val = true;
			    } else
			    {
				    log_warning(L"Starting TBS CI module has failed");
				    FreeLibrary(dll_handle_);
				    dll_handle_ = NULL;
			    }
		    } else
		    {
			    log_error(L"TBS CI control dll does not export one of the required functions");
		    }
	    } else
	    {
		    log_error(L"Unable to load TBS CI control dll");
	    }
    }
	return ret_val;
}

void tbs_ci_handler_t::term()
{
	if (dll_handle_ != NULL)
	{
		exit_ci_f_(ci_lib_h_);

		FreeLibrary(dll_handle_);
		dll_handle_ = NULL;
	}
}

bool tbs_ci_handler_t::send_pmt(unsigned char* pmt, int size, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd)
{
	if (dll_handle_ == NULL)
		return false;

	log_info(L"tbs_ci_handler_t::send_pmt: command %1%") % cmd;

	if (!ci_cam_avail_f_(ci_lib_h_))
	{
		log_info(L"TBS: no CAM available");
		return false;
	}

	E_CAPMT_LIST_MGMT list_mgmt;
	E_CAPMT_CMD_ID list_mgmt_cmd;
	if (!decryption_cmd_to_listmgt(cmd, list_mgmt, list_mgmt_cmd))
		return false;

	TBS_CA_DATA ca_data;

	ca_data.uSlot = 0;
	ca_data.uTag = 2;
	ca_data.uData[0] = list_mgmt;
	ca_data.uData[1] = list_mgmt_cmd;
	ca_data.uLength = 2 + size;
	// Copy PMT into CA datagram
	memcpy(&ca_data.uData[2], pmt, size);

	send_pmt_f_(ci_lib_h_, ca_data.uData, ca_data.uLength);

	return true;
}
