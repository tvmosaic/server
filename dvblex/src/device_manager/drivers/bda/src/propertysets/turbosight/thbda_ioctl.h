#ifndef THBDA_IOCTL_H
#define THBDA_IOCTL_H

/*******************************************************************************************************/
/* PHANTOM_LNB_BURST */
/*******************************************************************************************************/
typedef enum _PHANTOMLnbburst  {
    PHANTOM_LNB_BURST_MODULATED=1,                /* Modulated: Tone B               */
    PHANTOM_LNB_BURST_UNMODULATED,                /* Not modulated: Tone A           */
    PHANTOM_LNB_BURST_UNDEF=0                     /* undefined (results in an error) */
}   PHANTOM_LNB_BURST;
/*******************************************************************************************************/
/* PHANTOM_RXMODE */
/*******************************************************************************************************/
typedef enum _PHANTOMRxMode  {
    PHANTOM_RXMODE_INTERROGATION=0,              /* Demod expects multiple devices attached */
    PHANTOM_RXMODE_QUICKREPLY=1,                 /* demod expects 1 rx (rx is suspended after 1st rx received) */
    PHANTOM_RXMODE_NOREPLY=2                     /* demod expects to receive no Rx message(s) */
}   PHANTOM_RXMODE;

typedef struct {
	unsigned long ChannelFrequency;//
	unsigned long ulLNBLOFLowBand;
	unsigned long ulLNBLOFHighBand;
	unsigned long SymbolRate;
	unsigned char Polarity;

	UCHAR           LNB_POWER;              // LNB_POWER_ON | LNB_POWER_OFF
    UCHAR           HZ_22K;                 // HZ_22K_OFF | HZ_22K_ON
    UCHAR           Tone_Data_Burst;        // Data_Burst_ON | Tone_Burst_ON |Tone_Data_Disable
    UCHAR           DiSEqC_Port;            // DiSEqC_NULL | DiSEqC_A | DiSEqC_B | DiSEqC_C | DiSEqC_D

	unsigned char motor[5];

	unsigned char ir_code;
	unsigned char lock;
	unsigned char strength;
	unsigned char quality;

	unsigned char reserved[256];
	unsigned char FecType;
	unsigned char ModuType;
} QBOXDVBSCMD, *PQBOXDVBSCMD;

// this is defined in bda tuner/demod driver source (splmedia.h)
const GUID KSPROPSETID_BdaTunerExtensionProperties =
{0xfaa8f3e5, 0x31d4, 0x4e41, {0x88, 0xef, 0xd9, 0xeb, 0x71, 0x6f, 0x6e, 0xc9}};

// this is defined in bda tuner/demod driver source (splmedia.h)
// {C62FDC56-DA2A-4808-9DEB-34EE917EAB21}
const GUID KSMETHODSETID_SatelliteControl = 
{0xc62fdc56, 0xda2a, 0x4808, {0x9d, 0xeb, 0x34, 0xee, 0x91, 0x7e, 0xab, 0x21}};
// {BDB891DA-1E7A-46c4-8246-7ABDE36DDB57}
//DEFINE_GUID(<<name>>, 
//0xbdb891da, 0x1e7a, 0x46c4, 0x82, 0x46, 0x7a, 0xbd, 0xe3, 0x6d, 0xdb, 0x57);
// this is defined in bda tuner/demod driver source (splmedia.h)

#define STATIC_KSPROPERTYSET_QBOXControl \
    0xbdb891da, 0x1e7a, 0x46c4, 0x82, 0x46, 0x7a, 0xbd, 0xe3, 0x6d, 0xdb, 0x57
DEFINE_GUIDSTRUCT( "C6EFE5EB-855A-4f1b-B7AA-87B5E1DC4113", KSPROPERTYSET_QBOXControl );
#define KSPROPERTYSET_QBOXControl DEFINE_GUIDNAMED( KSPROPERTYSET_QBOXControl )


// added 2010-08-17
// #define STATIC_KSPROPSETID_BdaDiseqCommand \
// 	0xf84e2ab0, 0x3c6b, 0x45e3, 0xa0, 0xfc, 0x86, 0x69, 0xd4, 0xb8, 0x1f, 0x11
// DEFINE_GUIDSTRUCT("F84E2AB0-3C6B-45e3-A0FC-8669D4B81F11", KSPROPSETID_BdaDiseqCommand);
// #define KSPROPSETID_BdaDiseqCommand DEFINE_GUIDNAMED(KSPROPSETID_BdaDiseqCommand)
// added 2010-08-17
// typedef enum {
// 	KSPROPERTY_BDA_DISEQC_ENABLE = 0,
// 	KSPROPERTY_BDA_DISEQC_LNB_SOURCE,
// 	KSPROPERTY_BDA_DISEQC_USETONEBURST,
// 	KSPROPERTY_BDA_DISEQC_REPEATS,
// 	KSPROPERTY_BDA_DISEQC_SEND,
// 	KSPROPERTY_BDA_DISEQC_RESPONSE
// } KSPROPERTY_BDA_DISEQC_COMMAND;
//////////////////////////////////////////////////////////////////////////

typedef enum
{
    KSPROPERTY_CTRL_TUNER,
	KSPROPERTY_CTRL_IR,
	KSPROPERTY_CTRL_22K_TONE,
	KSPROPERTY_CTRL_MOTOR,
	KSPROPERTY_CTRL_LNBPW,
	KSPROPERTY_CTRL_LOCK_TUNER,
	KSPROPERTY_CTRL_MAC,
	KSPROPERTY_CTRL_DEVICEID,
	KSPROPERTY_CTRL_CI_SEND_PMT,
	KSPROPERTY_CTRL_BLIND_SCAN,  //was KSPROPERTY_CTRL_CI_RESET in a previous version??
	KSPROPERTY_CTRL_CI_GET_STATUS,
	KSPROPERTY_CTRL_TBSACCESS  = 18,
	KSPROPERTY_CTRL_PLPINFO = 19,//added 2012 11 09 liuy DVBT2 PLP interface
	KSPROPERTY_CTRL_GOLDCODE = 20//added 2013 03 18 liuy Gold and Root Code interface
} KSPROPERTY_UTICA;

#define LNB_POWER_OFF                       0
#define LNB_POWER_ON                        1

#define HZ_22K_OFF                          0
#define HZ_22K_ON                           1

#define Value_Data_Burst_ON                       1
#define Value_Tone_Burst_ON                       0
#define Value_Burst_OFF	                        2

#define POLARITY_H                          0
#define POLARITY_V                          1

#define DiSEqC_NULL                         0
#define DiSEqC_A                            1
#define DiSEqC_B                            2
#define DiSEqC_C                            3
#define DiSEqC_D                            4

const BYTE DISEQC_TX_BUFFER_SIZE = 150; // 3 bytes per message * 50 messages
const BYTE DISEQC_RX_BUFFER_SIZE = 8;   // reply fifo size, hardware limitation
/////////////LIUZHENG ,ADDED ///////////
/*******************************************************************************************************/
/* PHANTOM_ROLLOFF */
/*******************************************************************************************************/
typedef enum _PHANTOMRollOff  {   /* matched filter roll-off factors */
    PHANTOM_ROLLOFF_02=0,             /*   roll-off factor is 0.2  */
    PHANTOM_ROLLOFF_025=1,            /*   roll-off factor is 0.25 */
    PHANTOM_ROLLOFF_035=2,            /*   roll-off factor is 0.35 */
    PHANTOM_ROLLOFF_UNDEF=0xFF        /*   roll-off factor is undefined */
}   PHANTOM_ROLLOFF;
/*******************************************************************************************************/
/* Pilot                                                                                               */
/*******************************************************************************************************/
typedef enum _PHANTOMPilot {
    PHANTOM_PILOT_OFF     = 0,
    PHANTOM_PILOT_ON      = 1,
    PHANTOM_PILOT_UNKNOWN = 2 /* not used */
}   PHANTOM_PILOT;

// DVBS2 required channel attributes not covered by BDA
typedef struct _BDA_NBC_PARAMS
{
    PHANTOM_ROLLOFF      rolloff;
    PHANTOM_PILOT        pilot;
	int                           dvbtype;// 1 for dvbs 2 for dvbs2 0 for auto
    BinaryConvolutionCodeRate fecrate;
	ModulationType       modtype;
	
} BDA_NBC_PARAMS, *PBDA_NBC_PARAMS;
typedef enum _TBSDVBSExtensionPropertiesCMDMode {
    TBSDVBSCMD_LNBPOWER=0x00,      
    TBSDVBSCMD_MOTOR=0x01,            
    TBSDVBSCMD_22KTONEDATA=0x02,               
    TBSDVBSCMD_DISEQC=0x03              
}   TBSDVBSExtensionPropertiesCMDMode;
// this is defined in bda tuner/demod driver source (splmedia.h)
typedef enum {
    KSPROPERTY_BDA_DISEQC_MESSAGE = 0,  //Custom property for Diseqc messaging
    KSPROPERTY_BDA_DISEQC_INIT,         //Custom property for Intializing Diseqc.
    KSPROPERTY_BDA_SCAN_FREQ,           //Not supported 
    KSPROPERTY_BDA_CHANNEL_CHANGE,      //Custom property for changing channel
    KSPROPERTY_BDA_DEMOD_INFO,          //Custom property for returning demod FW state and version
    KSPROPERTY_BDA_EFFECTIVE_FREQ,      //Not supported 
    KSPROPERTY_BDA_SIGNAL_STATUS,       //Custom property for returning signal quality, strength, BER and other attributes
    KSPROPERTY_BDA_LOCK_STATUS,         //Custom property for returning demod lock indicators 
    KSPROPERTY_BDA_ERROR_CONTROL,       //Custom property for controlling error correction and BER window
    KSPROPERTY_BDA_CHANNEL_INFO,        //Custom property for exposing the locked values of frequency,symbol rate etc after
                                        //corrections and adjustments
    KSPROPERTY_BDA_NBC_PARAMS,
    
	KSPROPERTY_BDA_BLIND_SCAN = 11,	
	KSPROPERTY_BDA_TBSACCESS  = 21,	
    KSPROPERTY_BDA_PLPINFO = 22,//added 2012 11 09 liuy DVBT2 PLP interface
    KSPROPERTY_BDA_GOLDCODE = 23//added 2013 02 25 liuy	 23

} KSPROPERTY_BDA_TUNER_EXTENSION;
// DVB-S/S2 DiSEqC message parameters
typedef struct _DISEQC_MESSAGE_PARAMS
{
    UCHAR      uc_diseqc_send_message[DISEQC_TX_BUFFER_SIZE+1];
    UCHAR      uc_diseqc_send_message_length;

    UCHAR      uc_diseqc_receive_message[DISEQC_RX_BUFFER_SIZE+1];
    UCHAR      uc_diseqc_receive_message_length;    
    
    PHANTOM_LNB_BURST burst_tone;       //Burst tone at last-message: (modulated = ToneB; Un-modulated = ToneA). 
    PHANTOM_RXMODE    receive_mode;     //Reply mode: interrogation/no reply/quick reply.
    TBSDVBSExtensionPropertiesCMDMode tbscmd_mode;

	   UCHAR           HZ_22K;                 // HZ_22K_OFF | HZ_22K_ON
	    UCHAR           Tone_Data_Burst;        // Data_Burst_ON | Tone_Burst_ON |Tone_Data_Disable
    UCHAR      uc_parity_errors;        //Parity errors:  0 indicates no errors; binary 1 indicates an error.
    UCHAR      uc_reply_errors;         //1 in bit i indicates error in byte i. 
    BOOL       b_last_message;          //Indicates if current message is the last message (TRUE means last message).
    BOOL       b_LNBPower;//liuzheng added for lnb power static
    
} DISEQC_MESSAGE_PARAMS, *PDISEQC_MESSAGE_PARAMS;
////////////////////////////////////////////////////////////////

//added 2011 02 16 liuy
typedef struct TBSDISEQC_MESSAGE_PARAMS
{
	UCHAR	   uc_diseqc_send_message[10];
	UCHAR	   uc_diseqc_send_message_length;

} TBSDISEQC_MESSAGE_PARAMS;

//end 2011 01 27


// {30696A6C-8F60-4f7f-BB1E-A203D7B13C16}
//static const GUID PROPSETID_CX_GOSHAWK2_DIAG_PROP =
//{ 0x30696a6c, 0x8f60, 0x4f7f, { 0xbb, 0x1e, 0xa2, 0x3, 0xd7, 0xb1, 0x3c, 0x16 } };
// {CBBF16AE-2A99-477e-B0D7-9C2274EB209E}
DEFINE_GUIDSTRUCT( "CBBF16AE-2A99-477e-B0D7-9C2274EB209E", PROPSETID_CX_GOSHAWK2_DIAG_PROP);
#define  PROPSETID_CX_GOSHAWK2_DIAG_PROP  DEFINE_GUIDNAMED( PROPSETID_CX_GOSHAWK2_DIAG_PROP )
#define MAX_SUBADDRESS_BYTES    8
#define MAX_I2C_BYTES           16
#define AUDIO_ADC_I2C_ADDRESS   0x34

typedef enum
{
    KSPROPERTY_DISABLE_USERMODE_I2C = 0,
    KSPROPERTY_ENABLE_USERMODE_I2C  = 1
}KSPROPERTY_I2C_STATES;

typedef struct _I2C_ACCESS
{
    KSPROPERTY_I2C_STATES state;    
}I2C_ACCESS, *PI2C_ACCESS;

typedef struct _I2C_STRUCT
{
    BYTE i2c_interface_select;
    BYTE chip_address;
    BYTE num_bytes;
    BYTE bytes[MAX_I2C_BYTES];
}I2C_STRUCT, *PI2C_STRUCT;

typedef struct _I2C_WRITE_THEN_READ_STRUCT
{
    BYTE i2c_interface_select;
    BYTE chip_address;
    BYTE num_write_bytes;
    BYTE write_bytes[MAX_SUBADDRESS_BYTES];
    BYTE num_read_bytes;
    BYTE read_bytes[MAX_I2C_BYTES];
}I2C_WRITE_THEN_READ_STRUCT, *PI2C_WRITE_THEN_READ_STRUCT;

typedef enum {
    CX_GOSHAWK2_DIAG_PROP_I2C_ACCESS             = 0,
    CX_GOSHAWK2_DIAG_PROP_I2C_WRITE              = 1,
    CX_GOSHAWK2_DIAG_PROP_I2C_READ               = 2,
    CX_GOSHAWK2_DIAG_PROP_I2C_WRITE_THEN_READ    = 3    
} CX_GOSHAWK2_DIAGNOSTIC_PROPERTIES;


///-----------------------
// NEW INTERFACE

typedef enum _tbsaccessmode{
	TBSACCESS_LNBPOWER=0x00,
	TBSACCESS_DISEQC=0x01,
	TBSACCESS_22K=0x02
}TBSAccessMode;

typedef enum _tbslnbpowermode{
	TBS_LNBPOWER_OFF=0x00,
	TBS_LNBPOWER_ON=0x03,
	TBS_LNBPOWER_13V=0x02,
	TBS_LNBPOWER_18V=0x01,
}TBSLNBPowerMode;

typedef enum _tbs22kmode{
	TBS22K_OFF=0x00,
	TBS22K_ON=0x01,
	//Added for the simple diseqc control
	TBSBURST_UNMODULATED =0x02, //tone burst unmodulated ,satellite A
	TBSBURST_MODULATED   =0x03 ////tone burst modulated, satellite B
}TBS22KMode;

typedef struct _tbs_access
{

	TBSAccessMode      access_mode;
	
	TBS22KMode         tbs22k_mode;
	
	DWORD                uc_reservedtemp;//changed UCHARE to DWORD because of memory layout

	TBSLNBPowerMode    LNBPower_mode;

	UCHAR              uc_diseqc_send_message[128];
	DWORD              uc_diseqc_send_message_length;//changed UCHARE to DWORD because of memory layout

	UCHAR              uc_diseqc_receive_message[128];
	DWORD              uc_diseqc_receive_message_length;//changed UCHARE to DWORD because of memory layout

	UCHAR              uc_reserved[254];//reserved for future use 

}TBS_ACCESS_STRUCT;

#endif
