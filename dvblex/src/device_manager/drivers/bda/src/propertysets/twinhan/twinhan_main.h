/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include "../../ds_propertyset_base.h"

#define TWINHAN_1020_TUNER_FILTER_NAME  L"xxxx"
#define TWINHAN_1020_CAPTURE_FILTER_NAME  L"xxxx"
#define TWINHAN_1030_TUNER_FILTER_NAME  L"xxxx"
#define TWINHAN_1030_CAPTURE_FILTER_NAME  L"xxxx"

#define CYNERGY_DUAL_TSTICK_TUNER_NAME	L"Cinergy T-Stick"

class twinhan_property_set_t : public property_set_base_t
{
public:
    twinhan_property_set_t();

    virtual HRESULT property_set_init();
    virtual HRESULT property_set_deinit();
    virtual HRESULT property_set_send_diseqc(unsigned char *cmd, int len);
    virtual HRESULT property_set_set_lnb_power(dvblink::DL_E_LNB_POWER_TYPES lnb_power);
    virtual HRESULT property_set_set_tuner(dvblink::PTransponderInfo Tp, int diseqc, int toneburst);
    virtual ModulationType property_set_get_dvbs_modulation(int mod);
    virtual HRESULT property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng);
protected:
    IKsPropertySet *ppropsetTunerPin_twinhan;
    unsigned char g_ca_pmt[32768];

    HRESULT twinhan_ci_get_status(unsigned long *status);
    HRESULT TWINHAN_SetLNB(dvblink::PTransponderInfo ptp, int diseqc_port_no, int tone_data_burst);
    HRESULT twinhan_DeviceIOControl(DWORD  dwIoControlCode, LPVOID lpInBuffer, DWORD  nInBufferSize, LPVOID lpOutBuffer, DWORD  nOutBufferSize, LPDWORD lpBytesReturned);
};
