/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>
#include <vector>

#include <drivers/deviceapi.h>
#include "THIOCtrl.h"
#include "../../common.h"		
#include "../../ds_tunerequest.h"
#include "../../capmt.h"
#include "../../decrypt_cmd_to_listmgmt.h"
#include <dl_logger.h>
#include "twinhan_main.h"

using namespace dvblink::logging;

HRESULT twinhan_property_set_t::twinhan_DeviceIOControl(DWORD  dwIoControlCode,
								LPVOID lpInBuffer,
								DWORD  nInBufferSize,
								LPVOID lpOutBuffer,
								DWORD  nOutBufferSize,
								LPDWORD lpBytesReturned)
{
	KSPROPERTY instance_data;
	THBDACMD THBDACmd;
	THBDACmd.CmdGUID = GUID_THBDA_CMD;
	THBDACmd.dwIoControlCode = dwIoControlCode;
	THBDACmd.lpInBuffer = lpInBuffer;
	THBDACmd.nInBufferSize = nInBufferSize;
	THBDACmd.lpOutBuffer = lpOutBuffer;
	THBDACmd.nOutBufferSize = nOutBufferSize;
	THBDACmd.lpBytesReturned = lpBytesReturned;

	return ppropsetTunerPin_twinhan->Set(GUID_THBDA_TUNER,
				NULL, &instance_data, sizeof(instance_data),
				&THBDACmd, sizeof(THBDACmd));	
}

twinhan_property_set_t::twinhan_property_set_t() :
    ppropsetTunerPin_twinhan(NULL)
{
}

HRESULT twinhan_property_set_t::property_set_init()
{
	HRESULT hr;

	// get the property sets on the tuner filter pins
	hr = get_pin_property_set(graph_info_.pfltTuner_, 0, &ppropsetTunerPin_twinhan);
	if FAILED(hr) 
		log_warning(L"Twinhan. No propertyset interface on the pins of the tuner filter");

	return hr;
}

HRESULT twinhan_property_set_t::property_set_deinit()
{
	if(ppropsetTunerPin_twinhan) {
		ppropsetTunerPin_twinhan->Release();
		ppropsetTunerPin_twinhan = NULL;
	}
	return S_OK;
}

HRESULT twinhan_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
    wchar_t s[255];	
	diseqc2str(cmd, len, &s[0]);
	log_info(L"twinhan_send_diseqc(<%s>, %d)") % s % len;

    DWORD dwReturned=0;
	DiSEqC_DATA data;
	ZeroMemory(&data, sizeof(DiSEqC_DATA));
	CopyMemory(data.command, cmd, min(len, sizeof(data.command)));
	data.command_len = min(len, sizeof(data.command));
	HRESULT hr = twinhan_DeviceIOControl((DWORD)THBDA_IOCTL_SET_DiSEqC, &data, sizeof(DiSEqC_DATA), NULL, 0, &dwReturned);
	if FAILED(hr)
    {
		log_error(L"THBDA_IOCTL_SET_DiSEqC failed");
    	return hr;
    }
		
	return hr;
}

HRESULT twinhan_property_set_t::TWINHAN_SetLNB(dvblink::PTransponderInfo ptp, int diseqc_port_no, int tone_data_burst)
{
	log_info(L"TWINHAN_SetLNB()");
  
	DWORD dwReturned=0;

	LNB_DATA data;
	ZeroMemory(&data, sizeof(LNB_DATA));

	data.DiSEqC_Port = diseqc_port_no;
	data.f22K_Output = ptp->dwLnbKHz == dvblink::LNB_SELECTION_0?1:2;
	data.Tone_Data_Burst = tone_data_burst;
	data.ulLNBLOFHighBand = ptp->LOF2 / 1000;
	data.ulLNBLOFLowBand = ptp->LOF1 / 1000;
	data.ulLNBLOFHiLoSW = ptp->LOFSW / 1000;
	data.LNB_POWER = ptp->Pol == dvblink::POL_LNBPOWEROFF ? 0 : 1;
	
	HRESULT hr = twinhan_DeviceIOControl((DWORD)THBDA_IOCTL_SET_LNB_DATA, &data, sizeof(LNB_DATA), NULL, 0, &dwReturned);
	if FAILED(hr)
		log_error(L"FAILED: THBDA_IOCTL_SET_LNB_DATA failed!");

	return hr;
}	

HRESULT twinhan_property_set_t::property_set_set_tuner(dvblink::PTransponderInfo Tp, int diseqc, int toneburst) 
{
    if (tuner_type_ == dvblink::TUNERTYPE_DVBS && (diseqc > 0 || toneburst > 0))
	    TWINHAN_SetLNB(Tp, diseqc, toneburst); 
	
	return E_NOINTERFACE; // let bda.cpp execute standard tuning procedure
}

HRESULT twinhan_property_set_t::property_set_set_lnb_power(dvblink::DL_E_LNB_POWER_TYPES lnb_power)
{
    DWORD dwReturned = 0;
	LNB_DATA lnb_data;
	ZeroMemory(&lnb_data, sizeof(LNB_DATA));
	HRESULT hr = twinhan_DeviceIOControl((DWORD)THBDA_IOCTL_GET_LNB_DATA, NULL, 0, &lnb_data, sizeof(LNB_DATA), &dwReturned);
	if SUCCEEDED(hr)
	{
        dwReturned = 0;
		lnb_data.LNB_POWER = (lnb_power == dvblink::POW_13V || lnb_power == dvblink::POW_18V) ? LNB_POWER_ON : LNB_POWER_OFF;
	    hr = twinhan_DeviceIOControl((DWORD)THBDA_IOCTL_SET_LNB_DATA, &lnb_data, sizeof(LNB_DATA), NULL, 0, &dwReturned);
		if (FAILED(hr))
		{
            log_error(L"set lnb power: THBDA_IOCTL_SET_LNB_DATA failed");
		}
	} else
    {
        log_error(L"set lnb power: THBDA_IOCTL_GET_LNB_DATA failed");
    }
    return S_OK;
}

HRESULT twinhan_property_set_t::property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd)
{
	log_info(L"twinhan_CISendPMT()...");

	unsigned long ci_status;
	HRESULT hr = twinhan_ci_get_status(&ci_status);
	if (FAILED(hr) || (ci_status >= CI_STATUS_EMPTY && ci_status != CI_STATUS_CAM_OK))
	{
		log_error(L"twinhan_CISendPMT: CI status error %d, %d") % hr % ci_status;
		return E_FAIL;
	}

	E_CAPMT_LIST_MGMT list_mgmt;
	E_CAPMT_CMD_ID list_mgmt_cmd;
	if (!decryption_cmd_to_listmgt(cmd, list_mgmt, list_mgmt_cmd))
		return E_FAIL;

    memset(g_ca_pmt, 0, sizeof(g_ca_pmt));
    //convert pmt to ca_pmt
    int ca_pmt_len;
    CreateCAPMTFromPMT(buf, len, list_mgmt, list_mgmt_cmd, g_ca_pmt, ca_pmt_len);
    if (ca_pmt_len > sizeof(g_ca_pmt))
        log_error(L"twinhan_CISendPMT: Resulting CA_PMT size is more than allocated buffer: %d vs. %d") % ca_pmt_len % sizeof(g_ca_pmt);

    DWORD dwReturned=0;
	hr = twinhan_DeviceIOControl((DWORD)THBDA_IOCTL_CI_SEND_PMT, g_ca_pmt, ca_pmt_len, NULL, 0, &dwReturned);
	/*
	if (FAILED(hr))
	{
		AddLog(L"twinhan_CISendPMT() is failed with error %d. Retrying...", hr);
		Sleep(500);
		dwReturned=0;
		hr = twinhan_DeviceIOControl((DWORD)THBDA_IOCTL_CI_SEND_PMT, buf, len, NULL, 0, &dwReturned);
	}
	*/
	if (FAILED(hr))
		log_error(L"twinhan_CISendPMT() error %d!") % hr;
	return hr;
}

HRESULT twinhan_property_set_t::twinhan_ci_get_status(unsigned long *status)
{
    DWORD dwReturned=0;
	THCIState state_info;
	memset(&state_info, 0, sizeof(state_info));
	HRESULT hr = twinhan_DeviceIOControl((DWORD)THBDA_IOCTL_CI_GET_STATE, NULL, 0, &state_info, sizeof(state_info), &dwReturned);

	*status = state_info.ulCIState;

	log_info(L"twinhan_CIGetStatus(): retcode %d, status %d") % hr % (*status);

	return hr;
}

ModulationType twinhan_property_set_t::property_set_get_dvbs_modulation(int mod)
{
	switch (mod) {
		case dvblink::MOD_DVBS_NBC_QPSK: 
		case dvblink::MOD_DVBS_8PSK:
			return BDA_MOD_8VSB; 
			break;
		default:
			return bda_standard_get_dvbs_modulation(mod); 
			break;
	}
}

