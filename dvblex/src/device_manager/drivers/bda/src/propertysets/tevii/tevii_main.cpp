/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <streams.h>
#include <winioctl.h>
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>

#include <drivers/deviceapi.h>
#include "../../ds_propertyset.h"
#include "../../common.h"		
#include "../../ds_tunerequest.h"		
#include "..\..\..\Include\tevii\tevii.h"
#include <dl_logger.h>
#include "tevii_main.h"

using namespace dvblink::logging;

void __stdcall tevii_property_set_t::TeViiCaptureFunc(void* lParam, BYTE* Buffer, int Length)
{
}

tevii_property_set_t::tevii_property_set_t():
    gTeViiDeviceIndex(-1)
{
}


HRESULT tevii_property_set_t::property_set_init()
{
    HRESULT hr = E_FAIL;
    int apiversion = GetAPIVersion();
	log_info(L"Using TeVii library version %d") % apiversion;

    int devicenum = FindDevices();
    log_info(L"Found %d TeVii device(s)") % devicenum;
    log_info(L"Looking for %s") % device_path_.c_str();

    gTeViiDeviceIndex = -1;
    if (devicenum > 0)
    {
        for (int i=0; i<devicenum; i++)
        {
            wchar_t buffer[512];
            const char* device_path = GetDevicePath(i);
			size_t sz;
            mbstowcs_s(&sz, buffer, 512, device_path, 511);
            log_info(L"Comparing %s...") % buffer;
            if (_wcsicmp(buffer, device_path_.c_str()) == 0)
            {
                gTeViiDeviceIndex = i;
                log_info(L"Found! Index %d") % gTeViiDeviceIndex;
                break;
            }
        }
        if (gTeViiDeviceIndex != -1)
        {
            //open and initialize device handle
            if (OpenDevice(gTeViiDeviceIndex, TeViiCaptureFunc, NULL))
            {
                hr = S_OK;
            }
            else
            {
            	log_error(L"TeVii OpenDevice failed");
            }
        }
        else
        {
        	log_error(L"No matching TeVii device is found");
        }
    }
    else
    {
    	log_error(L"No TeVii devices are found");
    }
    //find matching tevii device using devicepath
    return hr;
}

HRESULT tevii_property_set_t::property_set_deinit()
{
    if (gTeViiDeviceIndex != -1)
    {

        CloseDevice(gTeViiDeviceIndex);
        gTeViiDeviceIndex = -1;
    }
	return S_OK;
}

HRESULT tevii_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
    HRESULT hr = E_FAIL;
    if (gTeViiDeviceIndex != -1 &&
        SendDiSEqC(gTeViiDeviceIndex, cmd, len, 1, false))
    {
		Sleep(100);
    	log_info(L"TeVii SendDiSEqC Ok");
        hr = S_OK;
    }
    else
    {
    	log_error(L"TeVii SendDiSEqC has failed");
    }

	return hr;
}

static TPolarization GetPolarization(unsigned char tvs_pol)
{
	TPolarization ret_val = TPol_None;
	if (tvs_pol == dvblink::POL_HORIZONTAL)
		ret_val = TPol_Horizontal;
	else
		ret_val = TPol_Vertical;

	return ret_val;
}

static TMOD GetModulation(int tvs_mod)
{
	TMOD ret_val = TMOD_AUTO;

	switch (tvs_mod)
	{
	case dvblink::MOD_DVBS_QPSK:
		ret_val = TMOD_QPSK;
		break;
	case dvblink::MOD_DVBS_8PSK:
		ret_val = TMOD_DVBS2_8PSK;
		break;
	case dvblink::MOD_DVBS_16APSK:
		ret_val = TMOD_DVBS2_16PSK;
		break;
	case dvblink::MOD_DVBS_32APSK:
		ret_val = TMOD_DVBS2_32PSK;
		break;
	case dvblink::MOD_TURBO_QPSK:
		ret_val = TMOD_TURBO_QPSK;
		break;
	case dvblink::MOD_TURBO_8PSK:
		ret_val = TMOD_TURBO_8PSK;
		break;
	case dvblink::MOD_DVBS_NBC_QPSK:
		ret_val = TMOD_DVBS2_QPSK;
		break;
	}

	return ret_val;
}

static TFEC GetFec(int tvs_fec)
{
	TFEC ret_val = TFEC_AUTO;

	switch (tvs_fec)
	{
	case dvblink::FEC_AUTO:
		ret_val = TFEC_AUTO;
		break;
	case dvblink::FEC_1_2:
		ret_val = TFEC_1_2;
		break;
	case dvblink::FEC_1_3:
		ret_val = TFEC_1_3;
		break;
	case dvblink::FEC_1_4:
		ret_val = TFEC_1_4;
		break;
	case dvblink::FEC_2_3:
		ret_val = TFEC_2_3;
		break;
	case dvblink::FEC_2_5:
		ret_val = TFEC_2_5;
		break;
	case dvblink::FEC_3_4:
		ret_val = TFEC_3_4;
		break;
	case dvblink::FEC_3_5:
		ret_val = TFEC_3_5;
		break;
	case dvblink::FEC_4_5:
		ret_val = TFEC_4_5;
		break;
	case dvblink::FEC_5_6:
		ret_val = TFEC_5_6;
		break;
	case dvblink::FEC_7_8:
		ret_val = TFEC_7_8;
		break;
	case dvblink::FEC_8_9:
		ret_val = TFEC_8_9;
		break;
	case dvblink::FEC_9_10:
		ret_val = TFEC_9_10;
		break;
	case dvblink::FEC_6_7:
		ret_val = TFEC_6_7;
		break;
	case dvblink::FEC_5_11:
		ret_val = TFEC_5_11;
		break;
	}

	return ret_val;
}

HRESULT tevii_property_set_t::property_set_set_tuner(dvblink::PTransponderInfo Tp, int diseqc, int toneburst)
{
    HRESULT hr = E_FAIL;
    if (gTeViiDeviceIndex != -1)
	{
		BOOL b = TuneTransponder(gTeViiDeviceIndex, Tp->dwFreq, Tp->dwSr, Tp->dwLOF, 
			GetPolarization(Tp->Pol), 
			Tp->dwLnbKHz == dvblink::LNB_SELECTION_22, 
			GetModulation(Tp->dwModulation),
			GetFec(Tp->dwFec));
		if (b)
		{
			log_info(L"TeVii TuneTransponder Ok");
			hr = S_OK;
		}
        else
		{
			log_error(L"TeVii TuneTransponder has failed");
		}
	}

	return hr;
}

HRESULT tevii_property_set_t::property_set_get_tuner_state(dvblink::PSignalInfo stats)
{
    HRESULT hr = E_FAIL;
    if (gTeViiDeviceIndex != -1)
	{
		BOOL IsLocked;
		DWORD Strength;
		DWORD Quality;
		if (GetSignalStatus(gTeViiDeviceIndex, &IsLocked, &Strength, &Quality))
		{
			stats->dwSize = sizeof(dvblink::TSignalInfo);
			stats->Level = Strength;
			stats->Locked = IsLocked;
			stats->Quality = Quality;
			hr = S_OK;
		}
	}
	return hr;
}

