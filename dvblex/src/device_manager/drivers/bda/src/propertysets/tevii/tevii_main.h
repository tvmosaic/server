/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#ifdef _MSC_VER
#pragma warning(disable: 4995)
#pragma warning(disable: 4996)
#pragma warning(disable: 4100)
#endif

#include "../../ds_propertyset_base.h"

#define TEVII_USB_TUNER_FILTER_NAME  L"TeVii DVB-S/S2 Receiver"
#define TEVII_USB_CAPTURE_FILTER_NAME  L"TeVii DVB-S/S2 Receiver"

#define TEVII_NEW_USB_TUNER_FILTER_NAME  L"DVB-HD DVB-S/S2 Receiver"

#define TERRATEC_TEVII_VENDOR_ID   L"153b"
#define TERRATEC_CINERGY_S2_DUAL_PRODUCT_ID   L"1181"

class tevii_property_set_t : public property_set_base_t
{
public:
    tevii_property_set_t();

    virtual HRESULT property_set_init();
    virtual HRESULT property_set_deinit();
    virtual HRESULT property_set_send_diseqc(unsigned char *cmd, int len);
    virtual HRESULT property_set_set_tuner(dvblink::PTransponderInfo Tp, int diseqc, int toneburst);
    virtual HRESULT property_set_get_tuner_state(dvblink::PSignalInfo stats);
protected:
    int gTeViiDeviceIndex;

    static void __stdcall TeViiCaptureFunc(void* lParam, BYTE* Buffer, int Length);
};
