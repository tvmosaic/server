/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#define AVERMEDIA_TUNER_VENDOR_ID   L"07ca"
#define AVERMEDIA_TUNER_VENDOR_ID_1   L"1b80"
#define AVERMEDIA_TUNER_VENDOR_ID_3   L"15a4"

#define GIGABYTE_U8300_TUNER_PRODUCT_ID   L"d416"
#define REDDO_DVBTC_TUNER_PRODUCT_ID   L"e425"
#define AVERMEDIA_3DSAT_PRODUCT_ID   L"0889"
#define AVERMEDIA_H837_PRODUCT_ID   L"0837"

#define AVERMEDIA_CLONE_2_PRODUCT_ID   L"9016"

