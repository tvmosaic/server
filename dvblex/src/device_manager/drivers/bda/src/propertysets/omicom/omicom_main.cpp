/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>
#include <vector>

#include <drivers/deviceapi.h>
#include "../../ds_propertyset.h"
#include "omicom_main.h"
#include "../../common.h"		
#include "../../ds_tunerequest.h"
#include <dl_logger.h>

using namespace dvblink::logging;

typedef enum KSPROPERTY_OMC_DISEQC
{
    KSPROPERTY_OMC_DISEQC_WRITE = 0,
    KSPROPERTY_OMC_DISEQC_READ,
    KSPROPERTY_OMC_DISEQC_SET22K
}KSPROPERTY_OMC_DISEQC;

typedef struct OMC_BDA_DISEQC_DATA
{
    ULONG nLen;
    UCHAR pBuffer[64];
    ULONG nRepeatCount;
} OMC_BDA_DISEQC_DATA, *POMC_BDA_DISEQC_DATA;

// {7DB2DEEA-42B4-423d-A2F7-19C32E51CCC1}
static const GUID KSPROPSETID_OmcDiSEqCProperties = { 0x7DB2DEEA, 0x42B4, 0x423d, { 0xA2, 0xF7, 0x19, 0xC3, 0x2E, 0x51, 0xCC, 0xC1}};

omicom_property_set_t::omicom_property_set_t() :
    ppOMCIKsControl(NULL)
{
}

HRESULT omicom_property_set_t::property_set_init()
{
	HRESULT hr = E_FAIL;

    log_info(L"OMICOM: Initializing.");

    // query for KS property interface
    hr = graph_info_.pfltTuner_->QueryInterface(IID_IKsControl, (void **)&ppOMCIKsControl);
    if( hr != S_OK || !ppOMCIKsControl)
    {
        log_error(L"OMICOM: QueryInterface failed.");
        hr = E_FAIL;
    }
    
    return hr;
}

HRESULT omicom_property_set_t::SetProperty(const GUID Set, ULONG Id, PVOID buf, ULONG len)
{
    KSPROPERTY prop;
    DWORD dwNumBytesReturned;

    prop.Set = Set;
    prop.Id = Id;
    prop.Flags = KSPROPERTY_TYPE_SET;

    HRESULT hr = ppOMCIKsControl->KsProperty(&prop,sizeof(KSPROPERTY),buf,len,&dwNumBytesReturned);

    return hr;
}

HRESULT omicom_property_set_t::DiSEqCWrite(UCHAR *pBuffer, ULONG nLen, ULONG nRepeatCount)
{
    if(pBuffer == NULL || nLen <= 0 || nLen > 64)
    {
        return E_FAIL;
    }

    OMC_BDA_DISEQC_DATA data;
    memset(&data, 0, sizeof(data));

    memcpy(&data.pBuffer[0], pBuffer, nLen);
    data.nLen = nLen;
    data.nRepeatCount = nRepeatCount;

    HRESULT hr = SetProperty(KSPROPSETID_OmcDiSEqCProperties, KSPROPERTY_OMC_DISEQC_WRITE, 
        &data, sizeof(OMC_BDA_DISEQC_DATA));

    return hr;
}

HRESULT omicom_property_set_t::property_set_deinit()
{
    if (ppOMCIKsControl != NULL)
    {
        ppOMCIKsControl->Release();
        ppOMCIKsControl = NULL;
    }
	return S_OK;
}

HRESULT omicom_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
	HRESULT hr = E_FAIL;
    if (ppOMCIKsControl != NULL)
    {
        hr = DiSEqCWrite(cmd, len, 1);
        if (FAILED(hr))
        {
            log_error(L"OMICOM: SendDiseqc function failed %d.") % hr;
        }
    }
	return hr;
}

