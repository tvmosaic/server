/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#define GENIATECH_TUNER_FILTER_NAME  L"Digital TV" //geniatech digitar 3 - tuner filter name for windows vista
#define GENIATECH_CAPTURE_FILTER_NAME  L"Conexant 2388x AVStream TS Capture"  // ?

#define GENIATECH_TUNER_FILTER_NAME2  L"Conexant BDA DVB-S Tuner/Demod"  //geniatech digitar 3 - tuner filter name for windows xp

#define TERRATEC_S2_USB_HD_TUNER_NAME   L"Cinergy S2 USB HD Tuner"

// experimental , just for testing 1027
#define TWINHAN_1027_TUNER_FILTER_NAME  L"DTV-DVB 1027 DVBS BDA Tuner Filter"
#define TWINHAN_1027_CAPTURE_FILTER_NAME  L"xxxx"


#include "../../ds_propertyset_base.h"

class geniatech_property_set_t : public property_set_base_t
{
public:
    geniatech_property_set_t();

    virtual HRESULT property_set_init();
    virtual HRESULT property_set_deinit();
    virtual HRESULT property_set_send_diseqc(unsigned char *cmd, int len);
protected:
    IKsPropertySet *ppropsetTunerOutputPin_geniatech;
    IKsPropertySet *ppropsetTunerInputPin_geniatech;
};

