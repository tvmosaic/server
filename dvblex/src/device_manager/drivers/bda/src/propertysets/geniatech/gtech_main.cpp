/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include <streams.h>
#include <winioctl.h>
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>


#include <drivers/deviceapi.h>
#include "../../ds_propertyset.h"
#include "../../common.h"		
#include <dl_logger.h>
#include "gtech_main.h"

using namespace dvblink::logging;

#define CAMARIC_FEATURES

#include "cobra_enum.h"	
#include "geniatech.h"	

geniatech_property_set_t::geniatech_property_set_t() :
    ppropsetTunerOutputPin_geniatech(NULL), ppropsetTunerInputPin_geniatech(NULL)
{
}

HRESULT geniatech_property_set_t::property_set_init()
{
	HRESULT hr;

	// get the property sets on the tuner filter input pin
	hr = get_pin_property_set(graph_info_.pfltTuner_, 0, &ppropsetTunerInputPin_geniatech);
	if FAILED(hr) 
		log_warning(L"No propertyset interface on the input pin of the tuner filter");

	// get the property sets on the tuner filter output pin
	hr = get_pin_property_set(graph_info_.pfltTuner_, 1, &ppropsetTunerOutputPin_geniatech);
	if FAILED(hr) 
		log_warning(L"No propertyset interface on the output pin of the tuner filter");

	return hr;
}

HRESULT geniatech_property_set_t::property_set_deinit()
{
	if(ppropsetTunerInputPin_geniatech) {
		ppropsetTunerInputPin_geniatech->Release();
		ppropsetTunerInputPin_geniatech = NULL;
	}

	if(ppropsetTunerOutputPin_geniatech) {
		ppropsetTunerOutputPin_geniatech->Release();
		ppropsetTunerOutputPin_geniatech = NULL;
	}
	return S_OK;
}


HRESULT geniatech_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
	log_info(L"geniatech_send_diseqc");
	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerInputPin_geniatech->QuerySupported(KSPROPSETID_BdaTunerExtensionProperties,
							KSPROPERTY_BDA_DISEQC, 
							&TypeSupport);
	
	if FAILED(hr)
	//if (FAILED(hr) || !(TypeSupport & KSPROPERTY_SUPPORT_SET))
	{
			log_error(L"Geniatech KSPROPERTY_DISEQC not supported!");			
			return E_FAIL;
	}

	DISEQC_MESSAGE_PARAMS m_pDiSEqCMessageParams;
	KSP_NODE instance_data;
	
	ZeroMemory(&instance_data, sizeof(instance_data));
	ZeroMemory(&m_pDiSEqCMessageParams, sizeof(m_pDiSEqCMessageParams));

    memcpy(&m_pDiSEqCMessageParams.uc_diseqc_send_message[0],           
		cmd,
		len);
           //min(sizeof(m_pDiSEqCMessageParams.uc_diseqc_send_message), len));

    m_pDiSEqCMessageParams.ul_diseqc_send_message_length = min(sizeof(m_pDiSEqCMessageParams.uc_diseqc_send_message), len);
   

	m_pDiSEqCMessageParams.ul_diseqc_receive_message_length = 0;

    // set amplitude attenuation to 3, maximum Vp-p amplitude of 22KHz tone
    // used for DiSEqC bytes.
    m_pDiSEqCMessageParams.ul_amplitude_attenuation      = 3;
    
    // set tune burst to un-modulated    
    m_pDiSEqCMessageParams.b_tone_burst_modulated        = TONE_BURST_MODULATED;

    // set DiSEqC version to Version 1.X (no receive messages)
    m_pDiSEqCMessageParams.diseqc_version                = DISEQC_VER_1X;

	// set DiSEqC receive message mode to no reply messages
    m_pDiSEqCMessageParams.receive_mode                  = RXMODE_NOREPLY;
    
    // set last message flag to true
    m_pDiSEqCMessageParams.b_last_message = TRUE;    


	hr = ppropsetTunerInputPin_geniatech->Set(KSPROPSETID_BdaTunerExtensionProperties,
									KSPROPERTY_BDA_DISEQC,
									&instance_data,
									sizeof(instance_data),
									//&m_pDiSEqCMessageParams,
							//		sizeof(m_pDiSEqCMessageParams),
									&m_pDiSEqCMessageParams,
									sizeof(m_pDiSEqCMessageParams));
	if FAILED(hr)  {
			log_error(L"Geniatech KSPROPERTY_DISEQC failed!");			
			return hr;
	}
	return hr;
}
