/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>
#include <vector>

#include <drivers/deviceapi.h>
#include "anysee_main.h"
#include "anysee_ci.h"
#include "../../common.h"		
#include "../../ds_tunerequest.h"
#include <dl_logger.h>

using namespace dvblink::logging;

const GUID GUID_ANYSEE_CAPTURE_FILTER_PROPERTY = { 0xb8e78938, 0x899d, 0x41bd, { 0xb5, 0xb4, 0x62, 0x69, 0xf2,  0x80, 0x18, 0x99 } };

#define USERPROPERTY_SEND_DiSEqC_DATA   24
#define AS_DiSEqC_DATA_MAXLEN   16

typedef enum _AS_EnumDiSEqCToneBurst_
{
 No_DiSEqCToneBurst = 0,
 SA_DiSEqCToneBurst,
 SB_DiSEqCToneBurst
}AS_EnumDiSEqCToneBurst;

typedef struct _AS_USERPROPERTY_DiSEqC_S_
{
    KSPROPERTY Property; 
    DWORD dwLength;       // DiSEqC Data(include command) Length
    BYTE Data[AS_DiSEqC_DATA_MAXLEN];       // DiSEqC Data
    BYTE ToneBurst;      // EnumDiSEqCToneBurst
}AS_USERPROPERTY_DiSEqC_S, *PAS_USERPROPERTY_DiSEqC_S;

anysee_property_set_t::anysee_property_set_t() :
    ppropsetCapturePin_anysee(NULL), ci_control(NULL)
{
}

HRESULT anysee_property_set_t::property_set_after_graph_start()
{
	HRESULT hr = E_FAIL;

    if (graph_info_.pfltCapture_ != NULL)
    {
	    // get the property sets on the tuner filter pins
	    hr = get_filter_property_set(graph_info_.pfltCapture_, &ppropsetCapturePin_anysee);
	    if (SUCCEEDED(hr))
	    {
	        ci_control = new CAnySeeCI();
	        if (!ci_control->Init(ppropsetCapturePin_anysee, device_path_.c_str(), dll_path_.c_str()))
	        {
	            delete ci_control;
	            ci_control = NULL;
	        }
	    } else
	    {
		    log_warning(L"AnySee. No propertyset interface on the pins of the capture filter");
        }
    }
    else
    {
	    log_warning(L"AnySee. Capture filter is NULL - cannot get its propertyset");
    }

	return hr;
}


HRESULT anysee_property_set_t::property_set_deinit()
{
    if (ci_control != NULL)
    {
        ci_control->Term();
        delete ci_control;
        ci_control = NULL;
    }
	if(ppropsetCapturePin_anysee) 
    {
		ppropsetCapturePin_anysee->Release();
		ppropsetCapturePin_anysee= NULL;
	}
	return S_OK;
}

HRESULT anysee_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
    if (ppropsetCapturePin_anysee == NULL)
        return E_FAIL;

    wchar_t s[255];	
	diseqc2str(cmd, len, &s[0]);
	log_info(L"anysee_send_diseqc(<%s>, %d)") % s % len;

    AS_USERPROPERTY_DiSEqC_S DiSEqCProperty;

    DiSEqCProperty.dwLength = len;
    memcpy(DiSEqCProperty.Data, cmd, min(len, AS_DiSEqC_DATA_MAXLEN));
    DiSEqCProperty.ToneBurst = No_DiSEqCToneBurst;

    HRESULT hr = ppropsetCapturePin_anysee->Set(GUID_ANYSEE_CAPTURE_FILTER_PROPERTY, USERPROPERTY_SEND_DiSEqC_DATA,
        &DiSEqCProperty,
        sizeof(AS_USERPROPERTY_DiSEqC_S),
        &DiSEqCProperty,
        sizeof(AS_USERPROPERTY_DiSEqC_S));
	if FAILED(hr)
    {
		log_error(L"anysee_send_diseqc failed %d") % hr;
    	return E_NOINTERFACE; //also do input range tuning
    }
		
	return hr;
}

HRESULT anysee_property_set_t::property_set_send_toneburst(int type)
{
    if (ppropsetCapturePin_anysee == NULL)
        return E_FAIL;
	log_info(L"anysee_set_send_toneburst %d") % type;

    AS_USERPROPERTY_DiSEqC_S DiSEqCProperty;
    DiSEqCProperty.dwLength = 0;
    memset(DiSEqCProperty.Data, 0, AS_DiSEqC_DATA_MAXLEN);

    switch (type)
    {
    case dvblink::TONEBURST_1:
        DiSEqCProperty.ToneBurst = SA_DiSEqCToneBurst;
        break;
    case dvblink::TONEBURST_2:
        DiSEqCProperty.ToneBurst = SB_DiSEqCToneBurst;
        break;
    case dvblink::TONEBURST_NONE:
    default:
        DiSEqCProperty.ToneBurst = No_DiSEqCToneBurst;
        break;
    }

    HRESULT hr = ppropsetCapturePin_anysee->Set(GUID_ANYSEE_CAPTURE_FILTER_PROPERTY, USERPROPERTY_SEND_DiSEqC_DATA,
        &DiSEqCProperty,
        sizeof(AS_USERPROPERTY_DiSEqC_S),
        &DiSEqCProperty,
        sizeof(AS_USERPROPERTY_DiSEqC_S));
	if FAILED(hr)
    {
		log_error(L"FAILED: anysee_set_send_toneburst failed %d") % hr;
    	return E_NOINTERFACE; //also do input range tuning
    }
		
	return hr;
}

HRESULT anysee_property_set_t::property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng)
{
    HRESULT hr = E_FAIL;
    
	if (listmng != dvblink::SERVICE_DECRYPT_FIRST)
	{
		if (listmng == dvblink::SERVICE_DECRYPT_ADD)
			log_error(L"anysee_ci_send_pmt: multi-channel decryption is not supported");
		return E_FAIL;
	}

    if (ci_control != NULL)
        hr = ci_control->SetPMT(buf, len) ? S_OK : E_FAIL;
    
    return hr;
}
