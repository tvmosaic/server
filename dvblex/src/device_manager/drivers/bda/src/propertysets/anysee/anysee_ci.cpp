/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "anysee_ci.h"
#include <windows.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>
#include <string>
#include "../../common.h"
#include <dl_ts_info.h>
#include <dl_logger.h>

using namespace dvblink::logging;
using namespace dvblink::engine;

static const GUID GUID_ANYSEE_CAPTURE_FILTER_PROPERTY = { 0xb8e78938, 0x899d, 0x41bd, { 0xb5, 0xb4, 0x62, 0x69, 0xf2,  0x80, 0x18, 0x99 } };
bool CAnySeeCI::m_CIInitialized = false;

#define USERPROPERTY_READ_PLATFORM 6

typedef struct _tagUSERPROPERTY_READ_PLATFORM_S 
{
    KSPROPERTY Property;
    DWORD  dwDeviceInfo;  
} USERPROPERTY_READ_PLATFORM_S, *PUSERPROPERTY_READ_PLATFORM_S;

#define FW_PCB508TC    18   // DVB-T + DVB-C + Smartcard Interface + CI
#define FW_PCB508S2    19   // DVB-S2 + Smartcard Interface + CI
#define FW_PCB508T2C   20   // DVB-T2 + DVB-C + Smartcard Interface + CI
#define FW_PCB508PTC      21   // PCI + PCB508TC
#define FW_PCB508PS2      22   // PCI + PCB508S2
#define FW_PCB508PT2C      23   // PCI + PCB508T2C

CAnySeeCI::CAnySeeCI()
{
    m_dll = NULL;
    m_ci_control = NULL;
    m_CIInitialized = false;
    m_CIThread = NULL;
    InitializeCriticalSection(&m_cs);
}

CAnySeeCI::~CAnySeeCI()
{
    DeleteCriticalSection(&m_cs);
}

bool CAnySeeCI::Init(IKsPropertySet* propset, const wchar_t* device_path, const wchar_t* dll_path)
{
    bool ret_val = false;

    m_CIThread = NULL;
    m_CIInitialized = false;
    //check if hardware supports CI
    if (IsCISupported(propset))
    {
		std::wstring dll_pathname = dll_path;
		dll_pathname += L"\\";
		dll_pathname += ANYSEE_CI_DLL_NAME;
	    log_info(L"AnySee: Loading %s") % dll_pathname.c_str();
    	
	    m_dll = LoadLibraryEx(dll_pathname.c_str(), NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
	    if (m_dll != NULL)
	    {
		    m_create_f = (pCreateDtvCIAPI)GetProcAddress(m_dll, "CreateDtvCIAPI");
		    m_destroy_f = (pDestroyDtvCIAPI)GetProcAddress(m_dll, "DestroyDtvCIAPI");
		    pGetanyseeNumberofDevicesEx enum_f = (pGetanyseeNumberofDevicesEx)GetProcAddress(m_dll, "GetanyseeNumberofDevicesEx");
		    if (m_create_f != NULL && m_destroy_f != NULL && enum_f != NULL)
		    {
                m_create_f(&m_ci_control);
                if (m_ci_control != NULL)
                {
		            //get number of anysee ci 
		            ANYSEECIDEVICESINFO anysee_devinfo;
		            int devnum = enum_f(&anysee_devinfo);
		            if (devnum > 0) 
		            {
		                //find a match to our tuner device
		                unsigned long ci_idx;
		                if (BDAIdxToCIIdx(anysee_devinfo, devnum, device_path, ci_idx))
		                {
		                    HWND wnd = NULL;//::GetDesktopWindow();
	                        if (SUCCEEDED(m_ci_control->OpenCILib(wnd, ci_idx)))
	                        {
                                if (SUCCEEDED(m_ci_control->CI_Control(CI_CONTROL_IS_PLUG_OPEN, (LPARAM*)&CIStateFunc, (LPARAM*)&CIMessageFunc)))
                                {
                                    //start ci handling thread
	                                m_ExitFlag = false;
	                                m_CIThread = CreateThread(NULL, 0, CIThread, (void*) this, 0, NULL);
                                    
	                                ret_val = true;
	                            } else
	                            {
                                    log_error(L"AnySee: CI_CONTROL_IS_OPEN failed");
                                    m_destroy_f(&m_ci_control);
	                                FreeLibrary(m_dll);
	                                m_dll = NULL;
	                            }
	                        } else
	                        {
                                log_error(L"AnySee: OpenCILib failed");
                                m_destroy_f(&m_ci_control);
	                            FreeLibrary(m_dll);
	                            m_dll = NULL;
	                        }
                        } else
                        {
                            log_error(L"AnySee: Cannot find a match for device path %s") % device_path;
                            m_destroy_f(&m_ci_control);
	                        FreeLibrary(m_dll);
	                        m_dll = NULL;
                        }
		            } else
		            {
	                    log_error(L"AnySee: No AnySee devices with CI are present in the system");
                        m_destroy_f(&m_ci_control);
		                FreeLibrary(m_dll);
		                m_dll = NULL;
		            }
                } else
                {
                    log_error(L"AnySee: Cannot create CI control object");
                    FreeLibrary(m_dll);
                    m_dll = NULL;
                }
		    } else
		    {
	            log_error(L"AnySee: Required functions are not exported");
		        FreeLibrary(m_dll);
		        m_dll = NULL;
		    }
	    } else
	    {
	        log_error(L"AnySee: Unable to load %s") % dll_pathname.c_str();
	    }
    } else
    {
        log_info(L"AnySee: This hardware does not have CI support");
    }
    return ret_val;
}

void CAnySeeCI::Term()
{
	if (m_CIThread != NULL)
	{
		m_ExitFlag = true;
		WaitForSingleObject(m_CIThread, INFINITE);
		CloseHandle(m_CIThread);
		m_CIThread = NULL;
	}

    if (m_dll != NULL)
    {
        m_destroy_f(&m_ci_control);
        FreeLibrary(m_dll);
        m_dll = NULL;
    }
}
 
bool CAnySeeCI::IsCISupported(IKsPropertySet* propset)
{
    bool ret_val = false;
    USERPROPERTY_READ_PLATFORM_S DeviceInfo;
 
    HRESULT hr = propset->Get(GUID_ANYSEE_CAPTURE_FILTER_PROPERTY, 
        USERPROPERTY_READ_PLATFORM,
        &DeviceInfo,
        sizeof(USERPROPERTY_READ_PLATFORM_S),
        &DeviceInfo,
        sizeof(USERPROPERTY_READ_PLATFORM_S),
        NULL );
        
    if( SUCCEEDED(hr))
    {
        unsigned long devtype = ((DeviceInfo.dwDeviceInfo >> 16) & 0x0FF);
        log_info(L"AnySee CI : Detected device type %d") % devtype;
        switch( devtype )
        {
        case FW_PCB508TC:
        case FW_PCB508S2:
        case FW_PCB508T2C:
        case FW_PCB508PTC:
        case FW_PCB508PS2:
        case FW_PCB508PT2C:
            ret_val = true;
        }
    } else
    {
        log_error(L"AnySee CI : Could not detect AnySee device type (%d)") % hr;
    }
     
    return ret_val;
} 

bool CAnySeeCI::BDAIdxToCIIdx(ANYSEECIDEVICESINFO& devinfo, int devnum, const wchar_t* device_path, unsigned long& ci_idx)
{
    bool ret_val = false;

    //wcsncmp

    wchar_t buffer[512];
	size_t sz;
    
    for (int i=0; i< devnum; i++)
    {
        mbstowcs_s(&sz, buffer, 512, devinfo.pBuffer[i], 511);
        //find last # symbol in the string
        wchar_t* ptr = wcsrchr(buffer, L'#');
        if (ptr != NULL)
        {
            int n = ptr - buffer;
            if (_wcsnicmp(buffer, device_path, n) == 0)
            {
                ci_idx = devinfo.dwADIndex[i];
                log_info(L"AnySee CI : Found device index match. device path %s, ci_idx %d") % device_path % ci_idx;
                ret_val = true;
                break;
            }
        }
    }
    
    return ret_val;
}

void CAnySeeCI::CIStateFunc(int nPort, int nCmd, char *str)
{
    log_info(L"AnySee CIState callback: port %d, command %d") % nPort % nCmd;
    if (nCmd == CI_MSG_INSERT_CAM)
    {
        CAnySeeCI::m_CIInitialized = true;
    }
}

void CAnySeeCI::CIMessageFunc(int nPort, PVOID pData )
{
    log_info(L"AnySee CIMessage callback: port %d") % nPort;
}

bool CAnySeeCI::SetPMT(unsigned char* pmt, int len)
{
    DTVCIPMT* pmtinfo = new DTVCIPMT();
    memset(pmtinfo, 0, sizeof(*pmtinfo));
    pmtinfo->vcni = pmt[5];
    dvblink::engine::ts_process_routines::GetPMTSectionPCRPID(pmt, len, pmtinfo->wPCRPid);
    
    //get main loop ca descriptors
    std::vector<TCA_PMT_CA_DESC> ca_desc_list;
	ts_process_routines::GetCADescriptorsFromPMTWithDesc(pmt, len, ca_desc_list);
    CopyDescriptorsToBuffer(pmtinfo->Program_CADescriptor, 256,  ca_desc_list);
    
    std::vector<TCA_PMT_ES_DESC> stream_info;
	ts_process_routines::GetPMTStreamsInfoWithDesc(pmt, len, pmtinfo->wProgramNumber, stream_info);
    for (unsigned int i=0; i<stream_info.size() && pmtinfo->dwNumberOfESPMTs < MAX_PIDS; i++)
    {
        bool bAdded = false;
		if (ts_process_routines::IsVideoStream(stream_info[i].type, stream_info[i].ca_desc_list))
        {
            pmtinfo->pDTVCIESPMT[pmtinfo->dwNumberOfESPMTs].MPEG2ESType = Video_MPEG2ESType;
            bAdded = true;
        }
		if (ts_process_routines::IsAudioStream(stream_info[i].type, stream_info[i].ca_desc_list))
        {
            pmtinfo->pDTVCIESPMT[pmtinfo->dwNumberOfESPMTs].MPEG2ESType = Audio_MPEG2ESType;
            bAdded = true;
        }
		if (ts_process_routines::IsSubtitlesStream(stream_info[i].type, stream_info[i].ca_desc_list))
        {
            pmtinfo->pDTVCIESPMT[pmtinfo->dwNumberOfESPMTs].MPEG2ESType = Subtitling_MPEG2ESType;
            bAdded = true;
        }
        if (bAdded)
        {
            pmtinfo->pDTVCIESPMT[pmtinfo->dwNumberOfESPMTs].Type = stream_info[i].type;
            pmtinfo->pDTVCIESPMT[pmtinfo->dwNumberOfESPMTs].wPid = stream_info[i].pid;
            CopyDescriptorsToBuffer(pmtinfo->pDTVCIESPMT[pmtinfo->dwNumberOfESPMTs].CADescriptor, 256,  stream_info[i].ca_desc_list);
            pmtinfo->dwNumberOfESPMTs += 1;
        }
    }
    
    //send pmt to CI module
    EnterCriticalSection(&m_cs);
    m_pmtdata.clear();
    m_pmtdata.assign((unsigned char*)pmtinfo, (unsigned char*)pmtinfo + sizeof(DTVCIPMT));
    LeaveCriticalSection(&m_cs);
    
    delete pmtinfo;
    
    return S_OK;
}

void CAnySeeCI::CopyDescriptorsToBuffer(unsigned char* buffer, int bufsize, std::vector<TCA_PMT_CA_DESC>& ca_desc_list)
{
    unsigned char* copy_start = buffer + 2;
    int buffer_len = bufsize - 2;
    WORD copy_len = 0;
    for (unsigned int i=0; i<ca_desc_list.size(); i++)
    {
        if (ca_desc_list[i].size() <= buffer_len)
        {
            memcpy(copy_start, &(ca_desc_list[i].at(0)), ca_desc_list[i].size());
            copy_start += ca_desc_list[i].size();
            copy_len += ca_desc_list[i].size();
            buffer_len -= ca_desc_list[i].size();
        }
    }
    //copy total length of descriptors
   memcpy(buffer, &copy_len, sizeof(copy_len));
}

DWORD WINAPI CAnySeeCI::CIThread(LPVOID lpParameter)
{
    CAnySeeCI* parent = (CAnySeeCI*)lpParameter;
    while (!parent->m_ExitFlag)
    {
        EnterCriticalSection(&parent->m_cs);
        if (m_CIInitialized && 
            parent->m_pmtdata.size() > 0)
        {
            if (parent->m_ci_control != NULL)
            {
                log_info(L"AnySee CI: Sending PMT to CI module");
                HRESULT hr = parent->m_ci_control->CI_Control(CI_CONTROL_SET_PMT, (LPARAM *)(&parent->m_pmtdata[0]), NULL);
                if (hr != S_OK)
                    log_error(L"AnySee CI: Sending PMT to CI module failed %d") % hr;
                parent->m_pmtdata.clear();
            }
        }
        LeaveCriticalSection(&parent->m_cs);
        Sleep(50);
    }
    
    return 0;
}
