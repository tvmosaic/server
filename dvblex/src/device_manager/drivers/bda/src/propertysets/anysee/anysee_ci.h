/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <objbase.h>
#include <vector>
#include <dl_ts_info.h>
#include "../../capmt.h"

interface IKsPropertySet;

#define ANYSEE_CI_DLL_NAME L"CIAPI.dll"
#define MAX_PIDS 16

enum CI_MESSAGE_COMMAND
{
    CI_MSG_EXTRACTED_CAM = 2002, // CAM Extracting Message.
    CI_MSG_CLEAR = 2100, // Clear message
    CI_MSG_INITIALIZATION_CAM = 2101, // CAM Initializing message
    CI_MSG_INSERT_CAM = 2102, // CAM initialization finishing message.
    CI_SEND_PMT_COMPLET = 2103 // PMT Set up message.
};

typedef enum _EnumMPEG2ESType_
{
    Unknown_MPEG2ESType = 0,
    Audio_MPEG2ESType,
    Video_MPEG2ESType,
    TeleText_MPEG2ESType,
    Subtitling_MPEG2ESType,
    PrivatePESPackets_MPEG2ESType,
}EnumMPEG2ESType;
 
typedef struct _DTVCI_ESPMT_
{
    WORD wPid;                                       //   Elementary PID
    BYTE MPEG2ESType;                        //    EnumMPEG2ESTyep value.
    BYTE Type;                                        //    elementary stream type of program map 
    BYTE CADescriptor[256];                   //   CA Descriptor data.
}DTVCI_ESPMT, *PDTVCI_ESPMT;
 
typedef struct _DTVCIPMT_
{
    BYTE vcni;

    WORD wPCRPid;
    WORD wProgramNumber;
    BYTE Program_CADescriptor[256];

    DWORD dwNumberOfESPMTs;                        // number of Elementary PID
    DTVCI_ESPMT pDTVCIESPMT[MAX_PIDS];
}DTVCIPMT, *PDTVCIPMT;

struct DtvCIApi 
{
    virtual HRESULT WINAPI OpenCILib(HWND hwnd,int iPortIndex) PURE;
    virtual HRESULT WINAPI CI_Control(DWORD command, LPARAM *pParam, LPARAM *pRetParam) PURE;
};

#define ANYSEE_CI_MAX_DEVICES_NUM 32

typedef struct _anyseeCIDevicesInfo_
{
    char pBuffer[ANYSEE_CI_MAX_DEVICES_NUM][256]; // Filter graph device path(=DisplayName)
    DWORD dwLength[ANYSEE_CI_MAX_DEVICES_NUM]; // The length of pBuffer
    DWORD dwFGIndex[ANYSEE_CI_MAX_DEVICES_NUM]; // Filter graph device path(=DisplayName) index(1, 2, 3, ...)
    DWORD dwADIndex[ANYSEE_CI_MAX_DEVICES_NUM]; // anysee device number(1, 2, 3, ...)
}ANYSEECIDEVICESINFO, *PANYSEECIDEVICESINFO;

typedef DWORD (WINAPI* pCreateDtvCIAPI)(DtvCIApi **);
typedef DWORD (WINAPI* pDestroyDtvCIAPI)(DtvCIApi **);
typedef int (WINAPI* pGetanyseeNumberofDevices)();
typedef int (WINAPI* pGetanyseeNumberofDevicesEx)(PANYSEECIDEVICESINFO pList);

enum CI_CONTROL_COMMAND
{
    CI_CONTROL_GET_DEVICE_NUM = 1100, // Connected Set number
    CI_CONTROL_IS_OPEN = 1104, // Check the status of connected set.
    CI_CONTROL_SET_KEY = 1105, // Key setting.
    CI_CONTROL_SET_TDT = 1106, // TDT setting.
    CI_CONTROL_SET_PMT = 1110, // PMT setting
    CI_CONTROL_IS_PLUG_OPEN = 2000 // Check the status of connected set and set callback func.
};

typedef void ( *pCIStateFunc)(int nPort, int nCmd, char *str); // CI Status Callback Function.
typedef void ( *pCIMessageFunc)(int nPort, PVOID pData); // CI Menu Data Callback Function.

class CAnySeeCI
{
public:
    CAnySeeCI();
    ~CAnySeeCI();
    
    bool Init(IKsPropertySet* propset, const wchar_t* device_path, const wchar_t* dll_path);
    void Term();
    
    bool SetPMT(unsigned char* pmt, int len);
protected:
    bool IsCISupported(IKsPropertySet* propset);
    bool BDAIdxToCIIdx(ANYSEECIDEVICESINFO& devinfo, int devnum, const wchar_t* device_path, unsigned long& ci_idx);
	void CopyDescriptorsToBuffer(unsigned char* buffer, int bufsize, std::vector<dvblink::engine::TCA_PMT_CA_DESC>& ca_desc_list);
    static DWORD WINAPI CIThread(LPVOID lpParameter);

    HMODULE m_dll;
    pCreateDtvCIAPI m_create_f;
    pDestroyDtvCIAPI m_destroy_f;
    DtvCIApi* m_ci_control;
    std::vector<unsigned char> m_pmtdata;
    CRITICAL_SECTION m_cs;
    HANDLE m_CIThread;
    bool m_ExitFlag;
    
    static void CIStateFunc(int nPort, int nCmd, char *str);
    static void CIMessageFunc(int nPort, PVOID pData);
    static bool m_CIInitialized;
};


