/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <dshow.h>

enum EKNCCiSlotStatus
{
	/// Initializing
	KNCCI_SS_Initializing      = 0,
	/// Transport
	KNCCI_SS_Transport         = 1,
	/// Resource
	KNCCI_SS_Resource          = 2,
	/// Application
	KNCCI_SS_Application       = 3,
	/// ConditionalAccess
	KNCCI_SS_ConditionalAccess =	4,
	/// Ready
	KNCCI_SS_Ready             = 5,
	/// OpenService
	KNCCI_SS_OpenService       = 6,
	/// Releasing
	KNCCI_SS_Releasing         = 7,
	/// CloseMMI
	KNCCI_SS_CloseMMI          = 8,
	/// Request
	KNCCI_SS_Request           = 9,
	/// Menu
	KNCCI_SS_Menu              = 10,
	/// MenuChoice
	KNCCI_SS_MenuChoice        = 11,
	/// OpenDisplay
	KNCCI_SS_OpenDisplay       = 12,
	/// CloseDisplay
	KNCCI_SS_CloseDisplay      = 13,
	/// None
	KNCCI_SS_None              = 99
};

typedef void (__cdecl *OnKncCiStateFunc)(unsigned char slot, EKNCCiSlotStatus State, char* lpszMessage, void* pParam);
typedef void (__cdecl *OnKncCiOpenDisplayFunc)(unsigned char slot, void* pParam);
typedef void (__cdecl *OnKncCiMenuFunc)(unsigned char slot, char* lpszTitle, char* lpszSubTitle, char* lpszBottom, unsigned int nNumChoices, void* pParam);
typedef void (__cdecl *OnKncCiMenuChoiceFunc)(unsigned char slot, unsigned int nChoice, char* lpszText, void* pParam);
typedef void (__cdecl *OnKncCiRequestFunc)(unsigned char slot, bool bBlind, unsigned int nAnswerLength, char* lpszText, void* pParam);
typedef void (__cdecl *OnKncCiCloseDisplayFunc)(unsigned char slot, unsigned int nDelay, void* pParam);

struct SKNCCiCallbacks
{
	// context 
	void* pParam;
	//CI state callback
	OnKncCiStateFunc onCiState;
	//opening display
	
	OnKncCiOpenDisplayFunc onOpenDisplay;
	//CI menu
	OnKncCiMenuFunc onCiMenu;
	//CI menu choices
	OnKncCiMenuChoiceFunc onCiMenuChoice;
	//CI requests
	OnKncCiRequestFunc onRequest;
	//closing CI
	OnKncCiCloseDisplayFunc onCloseDisplay;	
};


typedef unsigned int (__stdcall *KNCBDA_CI_EnableFunc)(unsigned int m_iDeviceIndex, IBaseFilter* tunerFilter,  SKNCCiCallbacks* callbacks);
typedef unsigned int (__stdcall *KNCBDA_CI_DisableFunc)(unsigned int m_iDeviceIndex);
typedef unsigned int (__stdcall *KNCBDA_CI_IsAvailableFunc)(unsigned int m_iDeviceIndex);
typedef unsigned int (__stdcall *KNCBDA_CI_IsReadyFunc)(unsigned int m_iDeviceIndex);
typedef unsigned int (__stdcall *KNCBDA_CI_HW_EnableFunc)(unsigned int m_iDeviceIndex, bool param);
typedef unsigned int (__stdcall *KNCBDA_CI_GetNameFunc)(unsigned int m_iDeviceIndex, char* Name, unsigned int BufferSize);
typedef unsigned int (__stdcall *KNCBDA_CI_SendPMTCommandFunc)(unsigned int m_iDeviceIndex, void* caPMT, unsigned int caPmtLen);
typedef unsigned int (__stdcall *KNCBDA_CI_EnterMenuFunc)(unsigned int m_iDeviceIndex, unsigned char nSlot);
typedef unsigned int (__stdcall *KNCBDA_CI_SelectMenuFunc)(unsigned int m_iDeviceIndex, unsigned char nSlot, unsigned char nChoice);
typedef unsigned int (__stdcall *KNCBDA_CI_CloseMenuFunc)(unsigned int m_iDeviceIndex, unsigned char nSlot);
typedef unsigned int (__stdcall *KNCBDA_CI_SendMenuAnswerFunc)(unsigned int m_iDeviceIndex, unsigned char nSlot, bool cancel, char* MenuAnswer);
typedef unsigned int (__stdcall *KNCBDA_HW_EnableFunc)(unsigned int m_iDeviceIndex, IBaseFilter* tunerFilter);
