/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include "kncdefs.h"
#include <dl_dshow_enum.h>
#include <vector>
#include <string>
#include <algorithm>
#include <bdatypes.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <tuner.h>
#include <drivers/deviceapi.h>
#include "../../ds_propertyset.h"
#include "knc.h"
#include "../../capmt.h"
#include "../../common.h"
#include "../../decrypt_cmd_to_listmgmt.h"
#include <dl_logger.h>

using namespace dvblink::logging;
using namespace dvblink::engine;

knc_property_set_t::knc_property_set_t() :
    g_KNCLib(NULL), g_KNCDeviceIdx(-1), g_bCIPresent(false),
    KNCBDA_CI_Enable(NULL), KNCBDA_CI_Disable(NULL), KNCBDA_CI_IsAvailable(NULL), KNCBDA_CI_IsReady(NULL), KNCBDA_CI_HW_Enable(NULL),
    KNCBDA_CI_GetName(NULL), KNCBDA_CI_SendPMTCommand(NULL), KNCBDA_HW_Enable(NULL)
{
    g_Callbacks.pParam = this;
    g_Callbacks.onCiState = OnKncCiState;
    g_Callbacks.onOpenDisplay = OnKncCiOpenDisplay;
    g_Callbacks.onCiMenu = OnKncCiMenu;
    g_Callbacks.onCiMenuChoice = OnKncCiMenuChoice;
    g_Callbacks.onRequest = OnKncCiRequest;
    g_Callbacks.onCloseDisplay = OnKncCiCloseDisplay;
}

int knc_property_set_t::GetKNCDeviceIndex()
{
	int ret_val = -1;

	std::vector<std::wstring> tuner_name_list;

	TDSEnum *penumBDATuners = enum_create(KSCATEGORY_BDA_NETWORK_TUNER);
	while (enum_next(penumBDATuners) == S_OK)
	{	
		enum_get_name(penumBDATuners);
		wchar_t* tunername_lwr = _wcsdup(penumBDATuners->szNameW);
		_wcslwr_s(tunername_lwr, wcslen(tunername_lwr)+1);
		if (wcsstr(tunername_lwr, L"knc bda") != NULL ||
			wcsstr(tunername_lwr, L"mystique") != NULL)
			tuner_name_list.push_back(penumBDATuners->szDevicePathW);

		free(tunername_lwr);
	}
	enum_free(penumBDATuners);

	log_info(L"KNC: found %d KNC devices") % tuner_name_list.size();

	if (tuner_name_list.size() > 0)
	{
		std::sort(tuner_name_list.begin(), tuner_name_list.end());
		//walk through and compare device name that we have
		for (unsigned int i=0; i<tuner_name_list.size(); i++)
		{
			if (_wcsicmp(tuner_name_list[i].c_str(), device_path_.c_str()) == 0)
			{
				ret_val = i;
				break;
			}
		}
	}

	log_info(L"KNC: Device index is %d") % ret_val;

	return ret_val;
}

HRESULT knc_property_set_t::property_set_init()
{
	//find knc device index
	g_KNCDeviceIdx = GetKNCDeviceIndex();
	if (g_KNCDeviceIdx != -1)
	{
		//Load KNC dll
		std::wstring dll_pathname = dll_path_;
		dll_pathname += L"\\KNCBDACTRL.dll";
		log_info(L"KNC: Loading %s") % dll_pathname.c_str();
		g_KNCLib = LoadLibrary(dll_pathname.c_str());
		if (g_KNCLib != NULL)
		{
			//Initialize functions
			KNCBDA_CI_Enable = (KNCBDA_CI_EnableFunc)GetProcAddress(g_KNCLib, "KNCBDA_CI_Enable");
			KNCBDA_CI_Disable = (KNCBDA_CI_DisableFunc)GetProcAddress(g_KNCLib, "KNCBDA_CI_Disable");
			KNCBDA_CI_IsAvailable = (KNCBDA_CI_IsAvailableFunc)GetProcAddress(g_KNCLib, "KNCBDA_CI_IsAvailable");
			KNCBDA_CI_IsReady = (KNCBDA_CI_IsReadyFunc)GetProcAddress(g_KNCLib, "KNCBDA_CI_IsReady");
			KNCBDA_CI_HW_Enable = (KNCBDA_CI_HW_EnableFunc)GetProcAddress(g_KNCLib, "KNCBDA_CI_HW_Enable");
			KNCBDA_CI_GetName = (KNCBDA_CI_GetNameFunc)GetProcAddress(g_KNCLib, "KNCBDA_CI_GetName");
			KNCBDA_CI_SendPMTCommand = (KNCBDA_CI_SendPMTCommandFunc)GetProcAddress(g_KNCLib, "KNCBDA_CI_SendPMTCommand");
			KNCBDA_HW_Enable = (KNCBDA_HW_EnableFunc)GetProcAddress(g_KNCLib, "KNCBDA_HW_Enable");
			if (KNCBDA_CI_Enable != NULL &&
				KNCBDA_CI_Disable != NULL &&
				KNCBDA_CI_IsAvailable != NULL &&
				KNCBDA_CI_IsReady != NULL &&
				KNCBDA_CI_HW_Enable != NULL &&
				KNCBDA_CI_GetName != NULL &&
				KNCBDA_CI_SendPMTCommand != NULL &&
				KNCBDA_HW_Enable != NULL)
			{
				//Enable hardware
				if (KNCBDA_HW_Enable(g_KNCDeviceIdx, graph_info_.pfltTuner_) != 0)
				{
					//Initialize KNC CI module
					if (KNCBDA_CI_Enable(g_KNCDeviceIdx, graph_info_.pfltTuner_, &g_Callbacks) != 0)
					{
						//check whether CAM module is present
						if (KNCBDA_CI_IsReady(g_KNCDeviceIdx) != 0)
						{
							log_info(L"KNC: CI module is detected");
							g_bCIPresent = true;
						}
                        else
                        {
							log_warning(L"KNC: CI module is not ready (yet?)");
                        }
					}
                    else
					{
						log_error(L"KNC: KNCBDA_CI_Enable has failed");
					}
				}
                else
				{
					log_error(L"KNC: KNCBDA_HW_Enable has failed");
				}
			}
            else
			{
				log_error(L"KNC: One or more exported functions are not present in the library");
				FreeLibrary(g_KNCLib);
				g_KNCLib = NULL;
			}
		}
        else
		{
			log_error(L"KNC: LoadLibrary(KNCBDACTRL.dll) is failed with %d") % GetLastError();
		}
	}
	return S_OK;
}

HRESULT knc_property_set_t::property_set_deinit()
{
	if (g_KNCLib != NULL)
	{
		KNCBDA_CI_Disable(g_KNCDeviceIdx);
		FreeLibrary(g_KNCLib);
		g_KNCLib = NULL;
	}
	g_KNCDeviceIdx = -1;
	g_bCIPresent = false;

	return S_OK;
}

HRESULT knc_property_set_t::property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd)
{
	HRESULT hr = E_FAIL;
	if (g_bCIPresent)
	{
		log_info(L"knc_ci_send_pmt()...");

		E_CAPMT_LIST_MGMT list_mgmt;
		E_CAPMT_CMD_ID list_mgmt_cmd;
		if (!decryption_cmd_to_listmgt(cmd, list_mgmt, list_mgmt_cmd))
			return E_FAIL;

		memset(g_ca_pmt, 0, sizeof(g_ca_pmt));
		//convert pmt to ca_pmt
		int ca_pmt_len;
		CreateCAPMTFromPMT(buf, len, list_mgmt, list_mgmt_cmd, g_ca_pmt, ca_pmt_len);
		if (ca_pmt_len > sizeof(g_ca_pmt))
			log_error(L"knc_ci_send_pmt: Error! Resulting CA_PMT size is more than allocated buffer: %d vs. %d") % ca_pmt_len % sizeof(g_ca_pmt);

		if (KNCBDA_CI_SendPMTCommand(g_KNCDeviceIdx, g_ca_pmt, ca_pmt_len) != 0)
		{
			hr = S_OK;
		}
        else
		{
			log_error(L"KNCBDA_CI_SendPMTCommand has failed");
		}
	}
	return hr;
}

void knc_property_set_t::OnKncCiState(unsigned char slot, EKNCCiSlotStatus State, char* lpszMessage, void* pParam)
{
    knc_property_set_t* parent = (knc_property_set_t*)pParam;
	log_info(L"OnKncCiState: index %d, State %d") % parent->g_KNCDeviceIdx % State;
    if (State == KNCCI_SS_Ready)
        parent->g_bCIPresent = true;
}

void knc_property_set_t::OnKncCiOpenDisplay(unsigned char slot, void* pParam)
{
}

void knc_property_set_t::OnKncCiMenu(unsigned char slot, char* lpszTitle, char* lpszSubTitle, char* lpszBottom, unsigned int nNumChoices, void* pParam)
{
}

void knc_property_set_t::OnKncCiMenuChoice(unsigned char slot, unsigned int nChoice, char* lpszText, void* pParam)
{
}

void knc_property_set_t::OnKncCiRequest(unsigned char slot, bool bBlind, unsigned int nAnswerLength, char* lpszText, void* pParam)
{
}

void knc_property_set_t::OnKncCiCloseDisplay(unsigned char slot, unsigned int nDelay, void* pParam)
{
}

