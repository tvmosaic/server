/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include "../../ds_propertyset_base.h"
#include "kncdefs.h"

class knc_property_set_t : public property_set_base_t
{
public:
    knc_property_set_t();

    virtual HRESULT property_set_init();
    virtual HRESULT property_set_deinit();
    virtual HRESULT property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng);
protected:
    int g_KNCDeviceIdx;
    HMODULE g_KNCLib;
    SKNCCiCallbacks g_Callbacks;
    bool g_bCIPresent;
    unsigned char g_ca_pmt[32768];

    KNCBDA_CI_EnableFunc KNCBDA_CI_Enable;
    KNCBDA_CI_DisableFunc KNCBDA_CI_Disable;
    KNCBDA_CI_IsAvailableFunc KNCBDA_CI_IsAvailable;
    KNCBDA_CI_IsReadyFunc KNCBDA_CI_IsReady;
    KNCBDA_CI_HW_EnableFunc KNCBDA_CI_HW_Enable;
    KNCBDA_CI_GetNameFunc KNCBDA_CI_GetName;
    KNCBDA_CI_SendPMTCommandFunc KNCBDA_CI_SendPMTCommand;
    KNCBDA_HW_EnableFunc KNCBDA_HW_Enable;

    static void OnKncCiState(unsigned char slot, EKNCCiSlotStatus State, char* lpszMessage, void* pParam);
    static void OnKncCiOpenDisplay(unsigned char slot, void* pParam);
    static void OnKncCiMenu(unsigned char slot, char* lpszTitle, char* lpszSubTitle, char* lpszBottom, unsigned int nNumChoices, void* pParam);
    static void OnKncCiMenuChoice(unsigned char slot, unsigned int nChoice, char* lpszText, void* pParam);
    static void OnKncCiRequest(unsigned char slot, bool bBlind, unsigned int nAnswerLength, char* lpszText, void* pParam);
    static void OnKncCiCloseDisplay(unsigned char slot, unsigned int nDelay, void* pParam);

    int GetKNCDeviceIndex();
};
