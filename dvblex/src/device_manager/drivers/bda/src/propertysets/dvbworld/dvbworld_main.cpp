/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <streams.h>
#include <winioctl.h>
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>

#include <drivers/deviceapi.h>
#include "../../ds_propertyset.h"
#include "../../common.h"		
#include "../../ds_tunerequest.h"		
#include <dl_logger.h>
#include "dvbworld_main.h"

using namespace dvblink::logging;

struct DiseqcCmd
{
    GUID    GUID_ID;
    unsigned char cmd[256];
    unsigned long cmd_len;
};

struct Tuner_S_Param_Old  //obsolete
{
    GUID          GUID_ID;
    unsigned long symbol_rate;//k
    unsigned long frequency;//k
    unsigned long lnb;
    int           hv; 
    bool          b22k;
    int           diseqcPort;    
};

struct Tuner_S_Param2 
{ 
	GUID                    GUID_ID; 
	unsigned long symbol_rate;//ksps 
	unsigned long frequency;//kHz 
	unsigned long lnb;//Khz 
	int                      hv; //1:H.0:V. 
	bool                    b22k; 
	int                      diseqcPort;//1-4. 
	int          FEC;//Add a item into this struct!  
	int          Modulation;//Add a item into this struct! 
	//1:DVB-S1,QPSK 
	//2:DVB-S2,QPSK 
	//3:DVB-S2,8PSK 
	int          Burst;//0:Undefined burst;1:Burst A;2:Burst B. 
}; 


struct Signal_Status
{    
    int          nStrength; //0-100
    int          nQuality; //0-100
    int          nLock;//  0 : unlocked    >0 : locked
    int          reserved;
};


static const GUID GUID_DiseqcCmdSend = //DiseqcCmd interface GUID
{ 0x61ae2cdf, 0x87e8, 0x445c, { 0x8a, 0x7, 0x35, 0x6e, 0xd2, 0x28, 0xfb, 0x4e } };

static const GUID GUID_HID_INTERFACE = //HID interface GUID
{ 0xdc0a8dca, 0x2c9c, 0x45d5, { 0x81, 0xf9, 0xdd, 0xb3, 0xf2, 0xba, 0x7e, 0xa6 } };


static const GUID GUID_READ_MAC =  //READ MAC
{ 0x334d58a, 0xc4f, 0x40aa, { 0xab, 0x21, 0x44, 0x59, 0xc3, 0xf1, 0x65, 0x5a } };

static const GUID GUID_TUNER_S_LOCK = //TUNER_LOCK_PARAM
{ 0x8bed860a, 0xa7b4, 0x4e90, { 0x9d, 0xf4, 0x13, 0x20, 0xc9, 0x49, 0x22, 0x61 } };

static const GUID GUID_SIGNAL_STATUS = //SIGNAL_STATUS
{ 0xb2a693d0, 0x1295, 0x4b8e, { 0x8f, 0x3, 0x6, 0x27, 0xb0, 0x76, 0x98, 0xab } };

static const GUID IID_IKsObject =  //IKsObject interface GUID
{ 0x423c13a2, 0x2070, 0x11d0, { 0x9e, 0xf7, 0x00, 0xaa, 0x00, 0xa2, 0x16, 0xa1 } };


#define LINEAR_V 0
#define LINEAR_H 1

#define DISEQC_PORT_A 1
#define DISEQC_PORT_B 2
#define DISEQC_PORT_C 3
#define DISEQC_PORT_D 4


HRESULT dvbworld_property_set_t::GetDriverHandle(IBaseFilter *pflt)
{
	HRESULT hr;
	hr = pflt->QueryInterface(IID_IKsObject,(void**)&g_piKsObject);
	
	if (!g_piKsObject)
		return E_FAIL;

	g_handle = g_piKsObject->KsGetObjectHandle();

	log_info(L"dvbworld  GetDriverHandle()  g_handle:%d") % g_handle;

	return S_OK;
}

dvbworld_property_set_t::dvbworld_property_set_t() :
    g_pi(NULL), g_piKsObject(NULL), g_handle(NULL)
{
}

HRESULT dvbworld_property_set_t::property_set_init()
{
	log_info(L"dvbworld_init()");

	HRESULT hr;
	g_handle = NULL;	

	// try to  get the property sets on the capture filter
	// if it fails, try the tuner filter
	if(graph_info_.pfltCapture_)	
		hr = GetDriverHandle(graph_info_.pfltCapture_);
	else
		hr = E_FAIL;
	if FAILED(hr) {
		hr = GetDriverHandle(graph_info_.pfltTuner_);
		if FAILED(hr) 
			log_error(L"FAILED: cannot get driver handle on the tuner filter");
	}

	return hr;
}


HRESULT dvbworld_property_set_t::property_set_deinit()
{

	g_piKsObject->Release();
	return S_OK;
}

HRESULT dvbworld_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{	
	log_info(L"dvbworld_send_diseqc()");
	
	if (!g_handle)
			return E_FAIL;

	HRESULT hr;

    unsigned long ret = 0;
    DiseqcCmd tempCmd;
    memset(&tempCmd,0,sizeof(DiseqcCmd));
    tempCmd.GUID_ID = GUID_DiseqcCmdSend; 

	memcpy(&tempCmd.cmd[0], cmd, min(sizeof(tempCmd.cmd), len));
	tempCmd.cmd_len = len;

    KsSynchronousDeviceControl(g_handle,
        IOCTL_KS_PROPERTY,
        &tempCmd,
        sizeof(DiseqcCmd),
        NULL,
        NULL,
        &ret);
	
	hr = S_OK;

	return hr;
}



// note: dvbworld SDK is probably also supporting the standard BDA tuning methods
HRESULT dvbworld_property_set_t::property_set_set_tuner(dvblink::PTransponderInfo Tp, int diseqc, int toneburst)
{
	if (!g_handle)
			return E_FAIL;

    unsigned long ret = 0;

    //Lock Channel
	Tuner_S_Param2 tuner_s_param;
	//memset((void *)&tuner_s_param, 0, sizeof(tuner_s_param));
    tuner_s_param.GUID_ID = GUID_TUNER_S_LOCK;
	tuner_s_param.frequency = Tp->dwFreq;
	tuner_s_param.symbol_rate = Tp->dwSr / 1000;
	tuner_s_param.lnb = Tp->dwLOF;
	tuner_s_param.b22k = Tp->dwLnbKHz == dvblink::LNB_SELECTION_0 ? false : true;
	tuner_s_param.hv = Tp->Pol == dvblink::POL_HORIZONTAL ? LINEAR_H : LINEAR_V;
    tuner_s_param.diseqcPort = diseqc;
	tuner_s_param.Burst = toneburst;
	tuner_s_param.FEC = bda_standard_get_fec(Tp->dwFec);
	
	switch(Tp->dwModulation) {		
		case dvblink::MOD_DVBS_8PSK:
			tuner_s_param.Modulation = 3;
			break;

		case dvblink::MOD_DVBS_NBC_QPSK:
			tuner_s_param.Modulation = 2;
			break;
	
		default:
			tuner_s_param.Modulation = 1;
			break;
	}
	log_info(L"dvbworld_set_tuner()  mod:%d") % tuner_s_param.Modulation;

    void* pInBuf = (Tuner_S_Param2 *)&tuner_s_param;
	KsSynchronousDeviceControl(g_handle,
		IOCTL_KS_PROPERTY,
		pInBuf,
		sizeof(Tuner_S_Param2),
		NULL,
		0,
		&ret);


	return S_OK;
}


// this function doesnt seem to work on dvbworld usb dvb-s2 for a reason !?  so cancelled in ds_propertyset.cpp
/*
HRESULT dvbworld_property_set_t::property_set_get_tuner_state(dvblink::PSignalInfo stats) 
{
	if (!g_handle)
			return E_FAIL;

    unsigned long ret = 0;
	//Tuner_S_Param2 tuner_s_param;
	Signal_Status signal_status;
	memset((void *)&signal_status, 0, sizeof(signal_status));

	KSPROPERTY Property;
	Property.Set = GUID_SIGNAL_STATUS;

	KsSynchronousDeviceControl(g_handle,
		IOCTL_KS_PROPERTY,
        &Property,
        sizeof(KSPROPERTY),
		&signal_status,
		sizeof(Signal_Status),
		&ret);

	stats->Level = signal_status.nStrength;
	stats->Quality = signal_status.nQuality;
	stats->Locked = signal_status.nLock;

	log_info(L"dvbworld_get_tuner_state() Level:%d  Quality:%d  Locked:%d   ret:%d  g_handle:%d") % stats->Level % stats->Quality % stats->Locked % ret % g_handle;

	return S_OK;
}
*/
HRESULT dvbworld_property_set_t::property_set_after_graph_start()
{
	if (!g_handle)
		return E_FAIL;

	return S_OK;
}

