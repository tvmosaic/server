/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#define DVBWORLD_PCI_2004_TUNER_FILTER_NAME  L"DVB WORLD BDA Tuner Filter" //dvb-s
#define DVBWORLD_PCI_2004_CAPTURE_FILTER_NAME  L"DVB WORLD BDA Capture Filter" //dvb-s

#define DVBWORLD_USB_2104_TUNER_FILTER_NAME  L"USB 2104 DVB-S2 Tuner"  //dvb-s2
#define DVBWORLD_PCIE_2005_TUNER_FILTER_NAME  L"PCIE DW2005 DVB-S2 Tuner"  //dvb-s2

#define DVBWORLD_PCIE_2006_TUNER_FILTER_NAME  L"PCIE PCIE_2006 DVB-S2 Tuner"  //dvb-s2

#include "../../ds_propertyset_base.h"

interface IKsObject;

class dvbworld_property_set_t : public property_set_base_t
{
public:
    dvbworld_property_set_t();

    virtual HRESULT property_set_init();
    virtual HRESULT property_set_deinit();
    virtual HRESULT property_set_after_graph_start();
    virtual HRESULT property_set_send_diseqc(unsigned char *cmd, int len);
    virtual HRESULT property_set_set_tuner(dvblink::PTransponderInfo Tp, int diseqc, int toneburst);
    //virtual HRESULT property_set_get_tuner_state(dvblink::PSignalInfo stats);  // NOT working on Vista 64 drivers ,  but working fine on XP 32 drivers
protected:
    IKsObject*  g_pi;
    IKsObject*  g_piKsObject;  //KsObject Interface
    void*   g_handle;  //driver handle

    HRESULT GetDriverHandle(IBaseFilter *pflt);
};
