/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once 

#include "../../ds_propertyset_base.h"

#define TECHNISAT_SKYSTARHD_TUNER_FILTER_NAME  L"TechniSat Mantis DVBS BDA Receiver"  // SkyStar HD
#define TECHNISAT_SKYSTAR_USB2HD_TUNER_FILTER_NAME  L"UDST7000BDA"  // Technisat SkyStar USB 2 HD

//#define TECHNOTREND_3200_TUNER_FILTER_NAME  L"TechnoTrend BDA/DVB-S Tuner" // TT-3200

ModulationType technisat_get_dvbs_modulation(int mod);

/*
TechniSat UDST7000BDA DVBS Tuner0 = Technisat SkyStar USB 2 HD
TechniSat Mantis DVBS BDA Receiver = Technisat SkyStar HD2 PCI/USB
Technotrend S2-3200 = Technisat Skystar HD
*/

class technisat_property_set_t : public property_set_base_t
{
public:
    technisat_property_set_t();

    virtual ModulationType property_set_get_dvbs_modulation(int mod);
};
