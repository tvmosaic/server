/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>

#include <drivers/deviceapi.h>
#include "../../ds_propertyset.h"
#include "../../ds_tunerequest.h"
#include "../../common.h"		
#include "technisat_main.h"

technisat_property_set_t::technisat_property_set_t()
{
}


ModulationType technisat_property_set_t::property_set_get_dvbs_modulation(int mod)
{
	switch (mod) {
		case dvblink::MOD_DVBS_NBC_QPSK: 
		case dvblink::MOD_DVBS_8PSK:
			return BDA_MOD_8VSB; 
			break;
		case dvblink::MOD_TURBO_8PSK:  // just for test
			return BDA_MOD_8PSK;
			break;
		default:
			return bda_standard_get_dvbs_modulation(mod); 
			break;
	}
}



