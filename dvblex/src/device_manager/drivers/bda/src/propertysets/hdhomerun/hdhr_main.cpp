/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>
#include <vector>

#include <drivers/deviceapi.h>
#include "../../ds_propertyset.h"
#include "../../common.h"		
#include "../../ds_tunerequest.h"
#include <dl_logger.h>
#include "hdhr_main.h"

using namespace dvblink::logging;

hdhr_property_set_t::hdhr_property_set_t() :
    g_hdhr_pfif(NULL)
{
}

HRESULT hdhr_property_set_t::property_set_after_graph_start()
{
	HRESULT hr = E_FAIL;

	g_hdhr_pfif = NULL;
    if (graph_info_.pfltTuner_ != NULL)
    {
		//locate KSNODE_BDA_PID_FILTER node on a tuner

		IBDA_Topology* piTopology = NULL;
		hr = graph_info_.pfltTuner_->QueryInterface(__uuidof(IBDA_Topology), (void**)&piTopology);
		if (hr == S_OK && piTopology != NULL)
		{
			const unsigned long max_node_size = 20;
			_BDANODE_DESCRIPTOR nodes[max_node_size];
			unsigned long cnt = 0;
			hr = piTopology->GetNodeDescriptors(&cnt, max_node_size, nodes);
			if (hr == S_OK) 
			{
				for (unsigned int i = 0; i < cnt; i++) 
				{
					if (nodes[i].guidFunction == KSNODE_BDA_PID_FILTER)
					{
						IUnknown* piUnknownNode = NULL;
						hr = piTopology->GetControlNode(0, 1, nodes[i].ulBdaNodeType, &piUnknownNode);
						if (hr == S_OK && piUnknownNode != NULL)
						{
							hr = piUnknownNode->QueryInterface(__uuidof(IMPEG2PIDMap), (void **)&g_hdhr_pfif);
							if (hr == S_OK)
							{
								log_info(L"Found HDHomeRun pid filtering interface");
							}
							piUnknownNode->Release();
						} else
						{
							log_error(L"HDHR: cannot GetControlNode");
						}
					}
				}
			} else
			{
				log_error(L"HDHR: cannot GetNodeDescriptors");
			}
			piTopology->Release();
		} else
		{
			log_error(L"HDHR: cannot get the topology");
		}
	}
	return hr;
}


HRESULT hdhr_property_set_t::property_set_deinit()
{
    if (g_hdhr_pfif != NULL)
    {
        g_hdhr_pfif->Release();
        g_hdhr_pfif = NULL;
    }
	return S_OK;
}

HRESULT hdhr_property_set_t::property_set_add_pid(int pid)
{
	if (g_hdhr_pfif != NULL)
	{
		unsigned long ulpid = pid;
		HRESULT hr = g_hdhr_pfif->MapPID(1, &ulpid, MEDIA_TRANSPORT_PACKET);
		return hr;
	}
	return E_FAIL;
}

HRESULT hdhr_property_set_t::property_set_del_pid(int pid)
{
	if (g_hdhr_pfif != NULL)
	{
		unsigned long ulpid = pid;
		HRESULT hr = g_hdhr_pfif->UnmapPID(1, &ulpid);
		return hr;
	}
	return E_FAIL;
}
