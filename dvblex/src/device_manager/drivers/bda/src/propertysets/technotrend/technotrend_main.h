/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#ifndef _TECHNOTREND_MAIN_H_
#define _TECHNOTREND_MAIN_H_

#define TT_USB_4500_TUNER_VENDOR_ID   L"0b48"
#define TT_USB_4500_TUNER_PRODUCT_ID   L"3012"

#define TT_PCIE_4500_TUNER_VENDOR_ID   L"14f1"
#define TT_PCIE_4500_TUNER_PRODUCT_ID   L"8852"
#define TT_PCIE_4500_TUNER_SUBSYS_ID   L"301313c2"

#include "../../ds_propertyset_base.h"
#include "..\..\..\Include\Technotrend\bdaapi_typedefs.h"

class CTTCIHandler;

enum ETTDeviceType
{
	ETT_DT_UNKNOWN = 0,
	ETT_DT_140x
};

class technotrend_property_set_t : public property_set_base_t
{
public:
    technotrend_property_set_t();
    ~technotrend_property_set_t();

    virtual HRESULT property_set_deinit();
    virtual HRESULT property_set_after_graph_start();
    virtual HRESULT property_set_send_diseqc(unsigned char *cmd, int len);
    virtual ModulationType property_set_get_dvbs_modulation(int mod);
    virtual HRESULT property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng);
    virtual HRESULT property_set_send_toneburst(int type);

protected:
    HANDLE g_TTDeviceHandle;
    ETTDeviceType g_ttDeviceType;
    CTTCIHandler* g_TTCIHandler;

    HRESULT FindTunerPins(IBaseFilter* pTunerFilter, IPin** ppPinInput, IPin** ppPinOutput);
    HRESULT GetPinMedium(IPin* pPin, REGPINMEDIUM* pMedium);
    DEVICE_CAT GetTTDeviceType();
};


#endif
