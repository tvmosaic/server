/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "..\..\..\Include\Technotrend\ttBdaDrvApi.h"
#include "..\..\..\Include\Technotrend\bdaapi_cimsg.h"
#include "streams.h"
#include <windows.h>
#include <string>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <uuids.h>
#include <ksproxy.h>
#include <tuner.h>

#include <dl_logger.h>
#include <drivers/deviceapi.h>
#include "../../ds_propertyset.h"
#include "../../ds_tunerequest.h"
#include "technotrend_main.h"

using namespace dvblink::logging;

static BOOL ConvertMultibyteToUC(UINT codepage, unsigned char* src, std::wstring& dest);

class CTTCIHandler
{
public:
	CTTCIHandler();
	~CTTCIHandler();

	int Init(HANDLE device_handle);
	int Term();

	int SendPMTSection(unsigned char* pmt, int pmt_len);
protected:
	TS_CiCbFcnPointer m_CallBackStruct;
	HANDLE m_DeviceHandle;
	bool m_CIAvailable;
	unsigned char m_SlotStatus;
	int m_CAResult;
    WORD m_LastSID;
    CRITICAL_SECTION m_cs;

    WORD GetPMTSectionServiceID(void* pmt_buffer, int pmt_length);
	void GetSlotStatus();
	static void m_OnSlotStatus(PVOID Context, BYTE nSlot, BYTE nStatus, TYP_SLOT_INFO* csInfo);
	static void m_OnCAStatus(PVOID Context, BYTE nSlot, BYTE nReplyTag, WORD wStatus);
    static void OnDisplayString(PVOID Context, BYTE  nSlot, char* pString, WORD  wLength);
    static void OnDisplayMenu(PVOID Context, BYTE  nSlot, WORD  wItems, char* pStringArray, WORD  wLength);
    static void OnDisplayList(PVOID Context, BYTE  nSlot, WORD  wItems, char* pStringArray, WORD  wLength);
    static void OnSwitchOsdOff(PVOID Context, BYTE  nSlot);
    static void OnInputRequest(PVOID Context, BYTE  nSlot, BOOL  bBlindAnswer, BYTE  nExpectedLength, DWORD dwKeyMask);
    static void OnLscSetDescriptor(PVOID Context, BYTE  nSlot, TYPE_CONNECT_DESCR* pDescriptor);
    static void OnLscConnect(PVOID Context, BYTE  nSlot);
    static void OnLscDisconnect(PVOID Context, BYTE  nSlot);
    static void OnLscSetParams(PVOID Context, BYTE  nSlot, BYTE  BufferSize, BYTE  Timeout10Ms);
    static void OnLscEnquireStatus(PVOID Context, BYTE  nSlot);
    static void OnLscGetNextBuffer(PVOID Context, BYTE  nSlot, BYTE  PhaseID);
    static void OnLscTransmitBuffer(PVOID Context, BYTE  nSlot, BYTE  PhaseID, BYTE* pData, WORD  nLength);
};

technotrend_property_set_t::technotrend_property_set_t():
    g_TTDeviceHandle(NULL), g_ttDeviceType(ETT_DT_UNKNOWN), g_TTCIHandler(NULL)
{
    g_TTCIHandler = new CTTCIHandler();
}

technotrend_property_set_t::~technotrend_property_set_t()
{
    delete g_TTCIHandler;
}

HRESULT technotrend_property_set_t::property_set_after_graph_start()
{
	HRESULT hr = E_FAIL;
	g_ttDeviceType = ETT_DT_UNKNOWN;
	//Identify type of the device itself
	DEVICE_CAT device_type = GetTTDeviceType();
	if (device_type != UNKNOWN)
	{
		//find out index of the device itself
		int device_idx = -1;
		IPin* inpin;
		IPin* outpin;
		hr = FindTunerPins(graph_info_.pfltTuner_, &inpin, &outpin);
		if (SUCCEEDED(hr))
		{
		    REGPINMEDIUM Medium;
		    memset(&Medium, 0, sizeof(Medium));//CLSID clsMedium; DWORD dw1; DWORD dw2;
		    if (GetPinMedium(outpin, &Medium) == S_OK)
			    device_idx = Medium.dw1;
			inpin->Release();
			outpin->Release();
		}
		//Open device interface
		if (device_idx != -1)
		{
			g_TTDeviceHandle = bdaapiOpenHWIdx(device_type, device_idx);
			if (g_TTDeviceHandle != NULL)
			{
				//determine exact type of the device
				//for some types we do not need TT device control library
				TS_FilterNames fnames;
				memset(&fnames, 0, sizeof(fnames));
				bdaapiGetDevNameAndFEType(g_TTDeviceHandle, &fnames);
				std::wstring devname;
				ConvertMultibyteToUC(CP_ACP, (unsigned char*)fnames.szProductName, devname);
				log_info(L"Technotrend device type: %s") % devname.c_str();
				if (strstr(fnames.szProductName, "1400") > 0 ||
					strstr(fnames.szProductName, "1401") > 0)
				{
					g_ttDeviceType = ETT_DT_140x;
					log_info(L"Technotrend 1400 or 1401 is detected");
				}
				//Init CI slot
				g_TTCIHandler->Init(g_TTDeviceHandle);
				hr = S_OK;
			}
            else
			{
				log_error(L"technotrend_after_graph_start - bdaapiOpenHWIdx has failed!");
			}
		}
	}
    else
	{
		log_error(L"technotrend_after_graph_start - Unknown TT device category!");
	}

	return hr;
}

HRESULT technotrend_property_set_t::property_set_deinit()
{
	if (g_TTDeviceHandle != NULL)
	{
		//Term CI slot handling
		g_TTCIHandler->Term();
		//Close TT handle
		bdaapiClose(g_TTDeviceHandle);
		g_TTDeviceHandle = NULL;
	}
	return S_OK;
}

ModulationType technotrend_property_set_t::property_set_get_dvbs_modulation(int mod)
{
	switch (mod) {
		case dvblink::MOD_DVBS_8PSK:
		case dvblink::MOD_DVBS_NBC_QPSK:
			return BDA_MOD_8VSB; 
			break;
		default:
			return bda_standard_get_dvbs_modulation(mod); 
			break;
	}
}

HRESULT technotrend_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
	//for 1400 or 1401 TT library should not be used 
	if (g_ttDeviceType == ETT_DT_140x)
		return E_NOINTERFACE;

	HRESULT hr = E_FAIL;
	if (g_TTDeviceHandle != NULL &&
		len > 0)
	{
		//find out polarization value
		Polarisation pol = BDA_POLARISATION_LINEAR_H;
		if (len == 4 && get_10_diseqc_from_raw_cmd(cmd, len) != 0)
        {
            pol = ((cmd[3] & 0x02) == 0x02) ? BDA_POLARISATION_LINEAR_H : BDA_POLARISATION_LINEAR_V;
        }

		//Set diseqc repeat to 1, no tone burst
		TYPE_RET_VAL res = bdaapiSetDiSEqCMsg(g_TTDeviceHandle, cmd, len, 1, 0, pol);

        if (len == 5) //unicable: repeat diseqc command
		    res = bdaapiSetDiSEqCMsg(g_TTDeviceHandle, cmd, len, 1, 0, pol);

		if (res == RET_ERROR)
		{
			//general error. Wait 500 ms and retry
			log_warning(L"technotrend_send_diseqc - bdaapiSetDiSEqCMsg failed with error %d - retrying after wait") % res;
			Sleep(500);
			res = bdaapiSetDiSEqCMsg(g_TTDeviceHandle, cmd, len, 1, 0, pol);
		}
		if (res != RET_SUCCESS)
		{
			log_error(L"technotrend_send_diseqc - bdaapiSetDiSEqCMsg failed with error %d") % res;
		}
        else
		{
			log_info(L"technotrend_send_diseqc - success");
			hr = S_OK;
		}
	}
	return hr;
}

HRESULT technotrend_property_set_t::property_set_send_toneburst(int type)
{
	//for 1400 or 1401 TT library should not be used 
	if (g_ttDeviceType == ETT_DT_140x)
		return E_NOINTERFACE;

	HRESULT hr = E_FAIL;
	if (g_TTDeviceHandle != NULL)
	{
		//No diseqc data, set diseqc repeat to 1
		TYPE_RET_VAL res = bdaapiSetDiSEqCMsg(g_TTDeviceHandle, NULL, 0, 1, type, BDA_POLARISATION_LINEAR_H);
		if (res == RET_ERROR)
		{
			//general error. Wait 500 ms and retry
			log_warning(L"technotrend_send_diseqc - bdaapiSetDiSEqCMsg failed with error %d - retrying after wait") % res;
			Sleep(500);
			res = bdaapiSetDiSEqCMsg(g_TTDeviceHandle, NULL, 0, 1, type, BDA_POLARISATION_LINEAR_H);
		}
		if (res != RET_SUCCESS)
		{
			log_error(L"technpotrend_send_toneburst - bdaapiSetDiSEqCMsg failed with error %d") % res;
		}
        else
		{
			hr = S_OK;
		}
	}
	return hr;
}

DEVICE_CAT technotrend_property_set_t::GetTTDeviceType()
{
	DEVICE_CAT device_type = UNKNOWN;
	std::wstring capture_name;
	//get name of the capture filter
	if (graph_info_.pfltCapture_ != NULL)
	{
		FILTER_INFO fi;
		if (SUCCEEDED(graph_info_.pfltCapture_->QueryFilterInfo(&fi)))
		{
			capture_name = fi.achName;
            // USB 2.0 products
			if ( _wcsicmp(capture_name.c_str(), USB2BDA_DVB_NAME) == 0 )
                device_type = USB_2;
            // Budget 2
			if ( _wcsicmp(capture_name.c_str(), BDG2_NAME) == 0 )
                device_type = BUDGET_2;
            // Budget 3 aka TT-budget T-3000
			if ( _wcsicmp(capture_name.c_str(), BUDGET3NAME) == 0 )
                device_type = BUDGET_3;
            // USB 2.0 Pinnacle aka PCTV 400e
			if ( _wcsicmp(capture_name.c_str(), USB2BDA_DVBS_NAME_PIN) == 0 )
                device_type = USB_2_PINNACLE;
		}
        else
		{
			log_error(L"Technotrend::GetTTDeviceType - QueryFilterInfo has failed!");
		}
	}
    else
	{
		log_error(L"Technotrend::GetTTDeviceType - Capture filter pointer is NULL!");
	}
	return device_type;
}

HRESULT technotrend_property_set_t::FindTunerPins(IBaseFilter* pTunerFilter, IPin** ppPinInput, IPin** ppPinOutput)
{
	HRESULT hr = E_FAIL;
	IEnumPins* pinEnum = NULL;
	*ppPinInput = NULL;
	*ppPinOutput = NULL;

    if ( pTunerFilter )
	{
		hr = pTunerFilter->EnumPins(&pinEnum);
		if ( FAILED(hr) )
        {
			log_error(L"Technotrend::FindTunerPins - Couldn't enum pins!  hr=0x%x") % hr;
        }
        else
		{
			ULONG fetched = 0;
			IPin* pPin = NULL; 
			while ( pinEnum->Next (1, &pPin, &fetched) == S_OK )
			{
				if ( pPin )
				{
					PIN_INFO PinInfo;
					hr = pPin->QueryPinInfo(&PinInfo);
					if ( PinInfo.dir == PINDIR_INPUT )
						*ppPinInput  = pPin;
					else
						*ppPinOutput = pPin;
				}
                else
				{
					log_error(L"Technotrend::FindTunerPins - Couldn't find pin!  hr=0x%x") % hr;
				}
			}
			hr = pinEnum->Release();
			if ( FAILED(hr) )
				log_error(L"Technotrend::FindTunerPins - Couldn't release enum pin!  hr=0x%x") % hr;
		}
	}
	
	if ( *ppPinInput == NULL )
		log_error(L"Technotrend::FindTunerPins - Couldn't find tuner input pin!");

	if ( *ppPinOutput == NULL )
		log_error(L"Technotrend::FindTunerPins - Couldn't find tuner output pin!");
	
    return hr;
}

HRESULT technotrend_property_set_t::GetPinMedium(IPin* pPin, REGPINMEDIUM* pMedium)
{
	if ( pPin == NULL || pMedium == NULL )
		return E_POINTER;

	IKsPin          *pKsPin = NULL;
    KSMULTIPLE_ITEM *pmi;

    HRESULT hr = pPin->QueryInterface(IID_IKsPin, (void **)&pKsPin);
    if ( FAILED(hr) )
	{
		log_error(L"Technotrend::GetPinMedium - Pin does not support IKsPin!");
        return hr;  // Pin does not support IKsPin.
	}

    hr = pKsPin->KsQueryMediums(&pmi);
    pKsPin->Release();
    if ( FAILED(hr) )
	{
		log_error(L"Technotrend::GetPinMedium - Pin does not support mediums!");
        return hr;  // Pin does not support mediums.
	}

    if ( pmi->Count )
    {
        // Use pointer arithmetic to reference the first medium structure.
        REGPINMEDIUM *pTemp = (REGPINMEDIUM*)(pmi + 1);
        memcpy(pMedium, pTemp, sizeof(REGPINMEDIUM));
    }        
    CoTaskMemFree(pmi);
    return S_OK;
}


HRESULT technotrend_property_set_t::property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng)
{
	if (listmng != dvblink::SERVICE_DECRYPT_FIRST)
	{
		if (listmng == dvblink::SERVICE_DECRYPT_ADD)
			log_error(L"technotrend_ci_send_pmt: multi-channel decryption is not supported");
		return E_FAIL;
	}

	g_TTCIHandler->SendPMTSection(buf, len);
	return S_OK;
}

CTTCIHandler::CTTCIHandler()
{
	m_CIAvailable = false;
    m_LastSID = 0;
    InitializeCriticalSection(&m_cs);

	//assign fields of the static structure
    m_CallBackStruct.p01Context = m_CallBackStruct.p02Context = m_CallBackStruct.p03Context = 
    m_CallBackStruct.p04Context = m_CallBackStruct.p05Context = m_CallBackStruct.p06Context = 
    m_CallBackStruct.p07Context = m_CallBackStruct.p08Context = m_CallBackStruct.p09Context = 
    m_CallBackStruct.p10Context = m_CallBackStruct.p11Context = m_CallBackStruct.p12Context = 
    m_CallBackStruct.p13Context = m_CallBackStruct.p14Context = this;
	m_CallBackStruct.p01 = m_OnSlotStatus;
	m_CallBackStruct.p02 = m_OnCAStatus;
	m_CallBackStruct.p03 = OnDisplayString;
	m_CallBackStruct.p04 = OnDisplayMenu;
	m_CallBackStruct.p05 = OnDisplayList;
	m_CallBackStruct.p06 = OnSwitchOsdOff;
	m_CallBackStruct.p07 = OnInputRequest;
	m_CallBackStruct.p08 = OnLscSetDescriptor;
	m_CallBackStruct.p09 = OnLscConnect;
	m_CallBackStruct.p10 = OnLscDisconnect;
	m_CallBackStruct.p11 = OnLscSetParams;
	m_CallBackStruct.p12 = OnLscEnquireStatus;
	m_CallBackStruct.p13 = OnLscGetNextBuffer;
	m_CallBackStruct.p14 = OnLscTransmitBuffer;
}

CTTCIHandler::~CTTCIHandler()
{
    DeleteCriticalSection(&m_cs);
}

int CTTCIHandler::Init(HANDLE device_handle)
{
	int ret_val = 0;
	m_CIAvailable = false;

	if (device_handle != NULL)
	{
		m_DeviceHandle = device_handle;
		bdaapiOpenCI(m_DeviceHandle, m_CallBackStruct);
		bdaapiCloseCI(m_DeviceHandle);

		//open CI slot handler
		TYPE_RET_VAL res = bdaapiOpenCI(m_DeviceHandle, m_CallBackStruct);
		if (res == RET_SUCCESS)
		{
			m_CIAvailable = true;
			ret_val = 1;
		}
        else
		{
			log_warning(L"Technotrend::bdaapiOpenCI has returned an error %d") % res;
		}
	}

	return ret_val;
}

int CTTCIHandler::Term()
{
	if (m_CIAvailable)
	{
		bdaapiCloseCI(m_DeviceHandle);
		m_CIAvailable = false;
	}
	return 1;
}

WORD CTTCIHandler::GetPMTSectionServiceID(void* pmt_buffer, int pmt_length)
{
	unsigned char* buffer = (unsigned char*)pmt_buffer;
	WORD ServiceId = ((((WORD)buffer[3]) << 8) & 0xFF00) | buffer[4];
	return ServiceId;
}

int CTTCIHandler::SendPMTSection(unsigned char* pmt, int pmt_len)
{
	//check whether CI handle was opened
	if (m_CIAvailable)
	{
        //Reset last remembered SID
        EnterCriticalSection(&m_cs);
	    WORD sid = GetPMTSectionServiceID(pmt, pmt_len);
        m_LastSID = sid;
        LeaveCriticalSection(&m_cs);

        //Refresh CI slot status
		m_SlotStatus = CI_SLOT_UNKNOWN_STATE;
		GetSlotStatus();
		//Wait until slot status is Ok - the response will be sent asynchronously
		int count = 2;
		while (m_SlotStatus != CI_SLOT_MODULE_OK && m_SlotStatus != CI_SLOT_CA_OK && count >= 0)
		{
			count -= 1;
			Sleep(50);
		}
		if (count >=0)
		{
			//slot is ready - send sid to decode
			log_info(L"Technotrend::SendPMTSection. Sending decoding request for sid %d") % sid;
			m_CAResult = -1;

            EnterCriticalSection(&m_cs);
			TYPE_RET_VAL result = bdaapiCIMultiDecode(m_DeviceHandle, &sid, 1);
            LeaveCriticalSection(&m_cs);

			if (result == RET_SUCCESS)
			{
				//Wait until result is ok
				count = 2;
				while (m_CAResult == -1 && count >= 0)
				{
					count -= 1;
					Sleep(50);
				}
				if (count < 0)
					log_error(L"Technotrend::SendPMTSection. Timeout while waiting for success return code");
			}
            else
			{
				log_error(L"Technotrend::SendPMTSection. bdaapiCIMultiDecode has failed with error %d") % result;
			}
		}
        else
		{
			log_error(L"Technotrend::SendPMTSection. CI slot is not ready. Wait timeout. Status %d") % m_SlotStatus;
		}
	}

	return 0;
}

void CTTCIHandler::GetSlotStatus()
{
	//we work only with the first slot
    TYPE_RET_VAL result = bdaapiCIGetSlotStatus(m_DeviceHandle, 0);
    if (result != RET_SUCCESS)
		log_error(L"Technotrend::GetSlotStatus. bdaapiCIGetSlotStatus has returned an error %d ") % result;
}

void CTTCIHandler::m_OnSlotStatus(PVOID Context, BYTE nSlot, BYTE nStatus, TYP_SLOT_INFO* csInfo)
{
	CTTCIHandler* parent = (CTTCIHandler*)Context;
	parent->m_SlotStatus = nStatus;
	log_info(L"Technotrend::m_OnSlotStatus. Slot %d, status %d ") % nSlot % nStatus;
    //Resend last SID on status ok
    EnterCriticalSection(&parent->m_cs);
    if (nStatus == CI_SLOT_MODULE_OK && parent->m_LastSID != 0)
    {
    	log_info(L"Resending bdaapiCIMultiDecode for SID %d ") % parent->m_LastSID;
	    TYPE_RET_VAL result = bdaapiCIMultiDecode(parent->m_DeviceHandle, &parent->m_LastSID, 1);
    }
    LeaveCriticalSection(&parent->m_cs);
}

void CTTCIHandler::m_OnCAStatus(PVOID Context, BYTE nSlot, BYTE nReplyTag, WORD wStatus)
{
	CTTCIHandler* parent = (CTTCIHandler*)Context;
	log_info(L"Technotrend::m_OnCAStatus. Slot %d, ReplyTag %d , status %d") % nSlot % nReplyTag % wStatus;
	switch (nReplyTag)
	{
	case CI_PSI_COMPLETE:
		log_info(L"Technotrend::m_OnCAStatus. CI_PSI_COMPLETE. Number of programs %d") % wStatus;
		break;
	case CI_SWITCH_PRG_REPLY:
        switch (wStatus)
        {
            case ERR_INVALID_DATA:
				log_error(L"Technotrend::m_OnCAStatus. ERR_INVALID_DATA");
                break;
            case ERR_NO_CA_RESOURCE:
				log_error(L"Technotrend::m_OnCAStatus. ERR_NO_CA_RESOURCE");
                break;
            case ERR_NONE:
				log_info(L"Technotrend::m_OnCAStatus. ERR_NONE. Program is set");
                parent->m_CAResult = 1;
                break;
            default:
                break;
        }
		break;
	default:
		break;
	}
}

static BOOL ConvertMultibyteToUC(UINT codepage, unsigned char* src, std::wstring& dest)
{
	BOOL ret_val = FALSE;
	dest = L"";
	int wsize = MultiByteToWideChar(codepage, 0, (const char*)src, -1, NULL, 0);
	if (wsize > 0)
	{
		wchar_t* wbuf = (wchar_t*)malloc((wsize+1)*sizeof(wchar_t));

		wsize = MultiByteToWideChar(codepage, 0, (const char*)src, -1, wbuf, wsize);
		if (wsize > 0)
		{
			dest = wbuf;
			ret_val = TRUE;
		}
		free(wbuf);
	}
	return ret_val;
}
  
void CTTCIHandler::OnDisplayString(PVOID Context, BYTE  nSlot, char* pString, WORD  wLength)
{
}

void CTTCIHandler::OnDisplayMenu(PVOID Context, BYTE  nSlot, WORD  wItems, char* pStringArray, WORD  wLength)
{
}
    
void CTTCIHandler::OnDisplayList(PVOID Context, BYTE  nSlot, WORD  wItems, char* pStringArray, WORD  wLength)
{
}
    
void CTTCIHandler::OnSwitchOsdOff(PVOID Context, BYTE  nSlot)
{
}

void CTTCIHandler::OnInputRequest(PVOID Context, BYTE  nSlot, BOOL  bBlindAnswer, BYTE  nExpectedLength, DWORD dwKeyMask)
{
}

void CTTCIHandler::OnLscSetDescriptor(PVOID Context, BYTE  nSlot, TYPE_CONNECT_DESCR* pDescriptor)
{
}

void CTTCIHandler::OnLscConnect(PVOID Context, BYTE  nSlot)
{
}

void CTTCIHandler::OnLscDisconnect(PVOID Context, BYTE  nSlot)
{
}

void CTTCIHandler::OnLscSetParams(PVOID Context, BYTE  nSlot, BYTE  BufferSize, BYTE  Timeout10Ms)
{
}

void CTTCIHandler::OnLscEnquireStatus(PVOID Context, BYTE  nSlot)
{
}

void CTTCIHandler::OnLscGetNextBuffer(PVOID Context, BYTE  nSlot, BYTE  PhaseID)
{
}

void CTTCIHandler::OnLscTransmitBuffer(PVOID Context, BYTE  nSlot, BYTE  PhaseID, BYTE* pData, WORD  nLength)
{
}
