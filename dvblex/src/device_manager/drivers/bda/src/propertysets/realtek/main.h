/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#define REALTEK_TUNER_VENDOR_ID   L"048d"

#define REALTEK_DVBT_PRODUCT_ID   L"9135"
#define REALTEK_CLONE_1_PRODUCT_ID   L"9006"
#define REALTEK_DVBLOGIC_SAMPLE_PRODUCT_ID   L"9306"

/////////////////////

#define DVBLOGIC_TUNER_VENDOR_ID   L"1d19"
#define DVBLOGIC_100TC_PRODUCT_ID   L"0100"
#define DVBLOGIC_100TC_MAKE   "DVBLogic"
#define DVBLOGIC_100TC_UUID_MAKE   "dvblogic"
#define DVBLOGIC_100TC_MODEL   "TVButler"
#define DVBLOGIC_100TC_MODEL_NUM   "100TC"

/////////////////////

#define REALTEK_TUNER_VENDOR_1_ID   L"1f4d"

#define REALTEK_DVBT_1_PRODUCT_ID   L"c803"

