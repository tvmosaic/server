/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>
#include <vector>

#include <drivers/deviceapi.h>
#include "../../ds_propertyset.h"
#include "BST_BDAExt.h"
#include "bstci_bda_api.h"
#include "../../common.h"		
#include "../../ds_tunerequest.h"
#include "../../capmt.h"
#include "../../decrypt_cmd_to_listmgmt.h"
#include <dl_logger.h>
#include "dvbsky_main.h"

using namespace dvblink::logging;


dvbsky_property_set_t::dvbsky_property_set_t() :
    ppropsetTunerPin_dvbsky(NULL)
{
}

HRESULT dvbsky_property_set_t::property_set_init()
{
	// get the property sets on the tuner filter pins
	HRESULT hr = get_pin_property_set(graph_info_.pfltTuner_, -1, &ppropsetTunerPin_dvbsky);	
	if FAILED(hr) 
        log_warning(L"DVBSky: No propertyset interface on the pins of the tuner filter");

	return hr;
}

HRESULT dvbsky_property_set_t::property_set_after_graph_start()
{
    dvbsky_get_ci_info();
    return S_OK;
}

HRESULT dvbsky_property_set_t::property_set_deinit()
{
	if(ppropsetTunerPin_dvbsky) {
		ppropsetTunerPin_dvbsky->Release();
		ppropsetTunerPin_dvbsky = NULL;
	}
	return S_OK;
}

HRESULT dvbsky_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
    wchar_t s[255];	
	diseqc2str(cmd, len, &s[0]);
	log_info(L"dvbsky_send_diseqc(<%s>, %d)") % s % len;

	DWORD TypeSupport=0;
	HRESULT hr = ppropsetTunerPin_dvbsky->QuerySupported(KSPROPSETID_DD_BdaTunerExtensionProperties,
							KSPROPERTY_DD_BDA_DISEQC, 
							&TypeSupport);
	
	if FAILED(hr)
	{
        log_error(L"dvbsky_send_diseqc: KSPROPERTY_DD_BDA_DISEQC not supported!");			
        return E_FAIL;
	}

	_CAMARIC_DISEQC_MESSAGE_PARAMS DiSEqCRequest;
	ZeroMemory(&DiSEqCRequest, sizeof(DiSEqCRequest));
	DiSEqCRequest.ul_diseqc_send_message_length = len;
	DiSEqCRequest.diseqc_version = DISEQC_VER_1X;
	DiSEqCRequest.receive_mode = RXMODE_NOREPLY;
	DiSEqCRequest.b_last_message = TRUE;
	DiSEqCRequest.b_tone_burst_modulated = TRUE; 
	CopyMemory(DiSEqCRequest.uc_diseqc_send_message, cmd, len);

	KSPROPERTY ks;
	hr = ppropsetTunerPin_dvbsky->Set(KSPROPSETID_DD_BdaTunerExtensionProperties, 
                        			KSPROPERTY_DD_BDA_DISEQC, 
                        			&ks,sizeof(ks),
                        			&DiSEqCRequest, sizeof(_CAMARIC_DISEQC_MESSAGE_PARAMS));

	Sleep(200);
	
	if FAILED(hr)	
	{
        log_error(L"ppropsetTunerPin_dvbsky: KSPROPERTY_DD_BDA_DISEQC failed!");			
	}

	return hr;
}

bool dvbsky_property_set_t::bst_ci_control(PBSTCI_BDA_EXT_CMD pcicmd)
{
    if (ppropsetTunerPin_dvbsky == NULL)
    {
        return false;
    }

    DWORD type_support = 0;
    // See if a property set is supported
    HRESULT hr = ppropsetTunerPin_dvbsky->QuerySupported(KSPROPSETID_BSTCI_BdaTunerExtensionProperties,
                                     KSPROPERTY_BDA_BSTCI_IOCTL, 
                                     &type_support);
                                     
    if (FAILED(hr))
    {
        log_warning(L"bst_ci_control: QuerySupported failed.");
	    return false;
	}

    if (type_support & KSPROPERTY_SUPPORT_SET)
    {  
        KSP_NODE instance_data;
        // make call into driver
	    hr = ppropsetTunerPin_dvbsky->Set(KSPROPSETID_BSTCI_BdaTunerExtensionProperties, 
                   KSPROPERTY_BDA_BSTCI_IOCTL, 
	   		       &instance_data, sizeof(instance_data),
			       pcicmd, sizeof(BSTCI_BDA_EXT_CMD));

	    if (FAILED(hr))
	    {
            log_warning(L"bst_ci_control: KSPROPERTY_BDA_BSTCI_IOCTL, SET failed");
            return false;
	    }
     }
     else
     {
        log_warning(L"bst_ci_control: KSPROPERTY_BDA_BSTCI_IOCTL, SET not supported");
        return false;
     }

    return true;
}

bool dvbsky_property_set_t::dvbsky_get_ci_info()
{
    bool ret_val = false;

	BSTCI_CAM_STATUS ci_status;
	ZeroMemory(&ci_status, sizeof(BSTCI_CAM_STATUS));

	BSTCI_BDA_EXT_CMD ci;
	ZeroMemory(&ci, sizeof(BSTCI_BDA_EXT_CMD));

    ci.dwCmd = BSTCI_IOCTL_CMD(BSTCI_IOCTL_CI_STATUS, 0, 0);
	ci.lpOutputBuffer = &ci_status;
	ci.dwOutputBufferLength = sizeof(ci_status);
	
	if(bst_ci_control(&ci))
    {
        ret_val = (ci_status.dwCamStatus & BSTCI_CAM_PRESENT) != 0;
        if (ret_val)
        {
            log_info(L"dvbsky_get_ci_info: CAM present. Vendor %1%, device %2%") % ci_status.wCamVendor % ci_status.wCamDevice;
        }
    }

    return ret_val;
}

HRESULT dvbsky_property_set_t::property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd)
{
	log_info(L"dvbsky_ci_send_pmt()...");

	E_CAPMT_LIST_MGMT list_mgmt;
	E_CAPMT_CMD_ID list_mgmt_cmd;
	if (!decryption_cmd_to_listmgt(cmd, list_mgmt, list_mgmt_cmd))
		return E_FAIL;

	BSTCI_BDA_EXT_CMD ci;
	ZeroMemory(&ci, sizeof(BSTCI_BDA_EXT_CMD));
	UCHAR uType, uCmd;
	uType = list_mgmt;//PMT_LIST_ONLY;
	uCmd = list_mgmt_cmd;//PMT_OK_DESCRAMBLING;
	ci.dwCmd = BSTCI_IOCTL_CMD(BSTCI_IOCTL_CI_PMT_LIST_CHANGE, uType, uCmd);
	ci.lpInputBuffer = buf;
	ci.dwInputBufferLength = len;

	bool b = bst_ci_control(&ci);
    
    return b ? S_OK : E_FAIL;
}
