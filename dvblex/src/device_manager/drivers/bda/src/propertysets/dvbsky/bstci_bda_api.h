/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#ifndef __BSTCI_BDA_API_H__
#define __BSTCI_BDA_API_H__

/*--------------------------BST Common Interface---------------------------*/
// {4FDC5D3A-1543-479e-9FC3-B7DBA473FB95}
const GUID KSPROPSETID_BSTCI_BdaTunerExtensionProperties = 
{ 0x4fdc5d3a, 0x1543, 0x479e, { 0x9f, 0xc3, 0xb7, 0xdb, 0xa4, 0x73, 0xfb, 0x95 } };

typedef enum {
    KSPROPERTY_BDA_BSTCI_IOCTL = 0,
} KSPROPERTY_BSTCI_BDA_TUNER_EXTENSION;

typedef struct _BSTCI_BDA_EXT_CMD
{
	DWORD dwCmd;                 //BSTCI_IOCTL_CMD: cmd,arg1,arg2
	LPVOID lpInputBuffer;        //input buffer
	DWORD dwInputBufferLength;   //length of input buffer
	LPVOID lpOutputBuffer;       //output buffer
	DWORD dwOutputBufferLength;  //length of output buffer
	LPDWORD lpOutputLength;      //length of the driver fill in output buffer.
	
}BSTCI_BDA_EXT_CMD, *PBSTCI_BDA_EXT_CMD;

#define BSTCI_IOCTL_CMD(cmd, arg1, arg2)	(((cmd & 0xff)) << 16 | ((arg1 & 0xff) << 8) | (arg2 & 0xff))
#define BSTCI_IOCTL_MAX_BUFFER_SIZE			65536

/* Supported IOCTLs */
typedef enum
{
	BSTCI_IOCTL_CI_STATUS = 0x20,
	BSTCI_IOCTL_CI_APPLICATION_INFO = 0x21,
	BSTCI_IOCTL_CI_CONDITIONAL_ACCESS_INFO = 0x22,
	BSTCI_IOCTL_CI_RESET = 0x23,
	BSTCI_IOCTL_CI_MMI_ENTER_MENU = 0x30,
	BSTCI_IOCTL_CI_MMI_GET_MENU = 0x31,
	BSTCI_IOCTL_CI_MMI_ANSWER_MENU = 0x32,
	BSTCI_IOCTL_CI_MMI_CLOSE = 0x33,
	BSTCI_IOCTL_CI_MMI_GET_ENQUIRY = 0x34,
	BSTCI_IOCTL_CI_MMI_PUT_ANSWER = 0x35,
	BSTCI_IOCTL_CI_PMT_LIST_CHANGE = 0x40,
};

/* PMT_LIST_CHANGE first argument */
typedef enum
{
	PMT_LIST_MORE = 0,
	PMT_LIST_FIRST = 1,
	PMT_LIST_LAST = 2,
	PMT_LIST_ONLY = 3
};

/* PMT_LIST_CHANGE second argument */
typedef enum
{
	PMT_OK_DESCRAMBLING = 1,
	PMT_OK_MMI = 2,
	PMT_QUERY = 3
};

/* MMI_PUT_ANSWER first argument */
typedef enum
{
	MMI_ANSWER_CANCEL = 0,
	MMI_ANSWER_OK = 1,
};

/* BSTCI_CAM_STATUS::dwCamStatus bits */
typedef enum
{
	BSTCI_CAM_PRESENT = 1,
	BSTCI_CAM_MMI_DATA_READY = 2,
	BSTCI_CAM_MMI_ENQ_READY = 4,
};

/* MMI menu limitations */
enum
{
	BSTCI_MAX_STRING_LENGTH = 256,
	BSTCI_MAX_CA_ID_COUNT = 256,
	MENU_MAX_ITEM_LENGTH = BSTCI_MAX_STRING_LENGTH,
	MENU_MAX_ITEM_COUNT = 64,
};

/* BSTCI_IOCTL_CI_STATUS reply format */
typedef struct _BSTCI_CAM_STATUS
{
	DWORD dwCamStatus;
	WORD wCamVendor;
	WORD wCamDevice;
	CHAR cCamString[BSTCI_MAX_STRING_LENGTH];
}BSTCI_CAM_STATUS, *PBSTCI_CAM_STATUS;

typedef struct _BSTCI_CAM_APPLICATION_INFO
{
	BYTE bAppType;
	WORD wAppVendor;
	WORD wAppCode;
	CHAR cAppString[BSTCI_MAX_STRING_LENGTH];
}BSTCI_CAM_APPLICATION_INFO, *PBSTCI_CAM_APPLICATION_INFO;

typedef struct _BSTCI_CAM_INFO
{
	DWORD dwSize;
	WORD wCaSystemIdList[BSTCI_MAX_CA_ID_COUNT];
}BSTCI_CAM_INFO, *PBSTCI_CAM_INFO;

/* BSTCI_IOCTL_CI_MMI_GET_MENU reply format */
typedef struct _BSTCI_CAM_MENU
{
	BOOL bIsMenu;
	CHAR cTitle[MENU_MAX_ITEM_LENGTH];
	CHAR cSubTitle[MENU_MAX_ITEM_LENGTH];
	CHAR cBottomText[MENU_MAX_ITEM_LENGTH];
	CHAR cItems[MENU_MAX_ITEM_COUNT][MENU_MAX_ITEM_LENGTH];
	DWORD dwItemCount;
}BSTCI_CAM_MENU, *PBSTCI_CAM_MENU;

typedef struct _BSTCI_CAM_MMI_ENQUIRY
{
	BOOL bBlindAnswer;
	BYTE bAnswerLength;
	CHAR cString[MENU_MAX_ITEM_LENGTH];
}BSTCI_CAM_MMI_ENQUIRY,*PBSTCI_CAM_MMI_ENQUIRY;

typedef struct _BSTCI_CAM_MMI_ANSWER
{
	BYTE bAnswerLength;
	CHAR cAnswer[MENU_MAX_ITEM_LENGTH];
}BSTCI_CAM_MMI_ANSWER, *PBSTCI_CAM_MMI_ANSWER;

#endif//__BSTCI_BDA_API_H__

