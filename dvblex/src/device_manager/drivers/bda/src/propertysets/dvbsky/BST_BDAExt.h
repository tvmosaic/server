/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once 

/*---------------------------------------------------------------------------*/
/*BDA Tuner Filter*/
/*---------------------------------------------------------------------------*/
const BYTE DISEQC_TX_BUFFER_SIZE = 150; 
const BYTE DISEQC_RX_BUFFER_SIZE = 8;   // reply fifo size, hardware limitation

typedef enum DiseqcVer{  
  DISEQC_VER_1X=1,                     /* Employs DiseqC version 1.x */
  DISEQC_VER_2X,                       /* Employs DiseqC version 2.x */
  ECHOSTAR_LEGACY,                     /* Employs Echostar Legacy LNB messaging. */
  DISEQC_VER_UNDEF=0                   /* undefined (results in an error) */
} DISEQC_VER;

typedef enum RxMode{
  RXMODE_INTERROGATION=1,              /* Demod expects multiple devices attached */
  RXMODE_QUICKREPLY,                   /* demod expects 1 rx (rx is suspended after 1st rx received) */
  RXMODE_NOREPLY,                      /* demod expects to receive no Rx message(s) */
  RXMODE_DEFAULT=0                     /* use current register setting  */
} RXMODE;

typedef struct _CAMARIC_DISEQC_MESSAGE_PARAMS
{
    UCHAR      uc_diseqc_send_message[DISEQC_TX_BUFFER_SIZE+1]; //Message to send.
    UCHAR      uc_diseqc_receive_message[DISEQC_RX_BUFFER_SIZE+1]; //Message received.
    ULONG      ul_diseqc_send_message_length; //Length of message to send.
    ULONG      ul_diseqc_receive_message_length; //Length of message received.
    ULONG      ul_amplitude_attenuation; //Don't care.
    BOOL       b_tone_burst_modulated; //TRUE: tone burst modulated, FALSE: tone burst no modulated.

    DISEQC_VER diseqc_version; //DISEQC_VER_1X: no reply message. DISEQC_VER_2X: need reply message.
    RXMODE     receive_mode; //if no reply need, set to RXMODE_NOREPLY. Default set to RXMODE_DEFAULT. 
    BOOL       b_last_message; //TRUE, the last message; FALSE, no the last message.
	
} CAMARIC_DISEQC_MESSAGE_PARAMS, *PCAMARIC_DISEQC_MESSAGE_PARAMS;


// BDA Tuner Extension. 2010-4-3 12:30
// {03CBDCB9-36DC-4072-AC42-2F94F4ECA05E}
#define STATIC_KSPROPSETID_DD_BdaTunerExtensionProperties \
0x3cbdcb9, 0x36dc, 0x4072, 0xac, 0x42, 0x2f, 0x94, 0xf4, 0xec, 0xa0, 0x5e
DEFINE_GUIDSTRUCT("03CBDCB9-36DC-4072-AC42-2F94F4ECA05E", KSPROPSETID_DD_BdaTunerExtensionProperties);
#define KSPROPSETID_DD_BdaTunerExtensionProperties DEFINE_GUIDNAMED(KSPROPSETID_DD_BdaTunerExtensionProperties)


////////////////////////////////////////////////////////////////////
typedef enum {
		KSPROPERTY_DD_BDA_DISEQC = 0,
		KSPROPERTY_DD_BDA_REMOTE_CODE,
		KSPROPERTY_DD_BDA_BLSCAN_CMD,
		KSPROPERTY_DD_BDA_BLSCAN_DATA,
		KSPROPERTY_DD_BDA_DVB_PLP_ID
		
} KSPROPERTY_DD_BDA_TUNER_EXTENSION;

/*---------------------------------------------------------------------------*/
//Ts catpure filter
/*---------------------------------------------------------------------------*/
#define MAX_SUBADDRESS_BYTES    8
#define MAX_I2C_BYTES           16
typedef struct _I2C_STRUCT
{
    /*
    S850/S950: 
               i2c1 - eeprom 
               i2c2 - tuner/demod
    S952:      
               i2c1 - eeprom, channel 1(tuner/demod) 
               i2c2 - channel 2(tuner/demod)
    */	
    BYTE i2c_interface_select; /*1 - i2c1, 2 - i2c2, 3 - internal.*/
    BYTE chip_address;
    BYTE num_bytes;
    BYTE bytes[MAX_I2C_BYTES];
}I2C_STRUCT, *PI2C_STRUCT;


typedef struct _I2C_WRITE_THEN_READ_STRUCT
{
    /*
    S850/S950: 
               i2c1 - eeprom 
               i2c2 - tuner/demod
    S952:      
               i2c1 - eeprom, channel 1(tuner/demod) 
               i2c2 - channel 2(tuner/demod)
    */
    BYTE i2c_interface_select; /*1 - i2c1, 2 - i2c2, 3 - internal.*/
    
    BYTE chip_address;
    BYTE num_write_bytes;
    BYTE write_bytes[MAX_SUBADDRESS_BYTES];
    BYTE num_read_bytes;
    BYTE read_bytes[MAX_I2C_BYTES];
}I2C_WRITE_THEN_READ_STRUCT, *PI2C_WRITE_THEN_READ_STRUCT;


typedef enum {
		CX_CORONA_DIAG_PROP_I2C_WRITE               = 0,
		CX_CORONA_DIAG_PROP_I2C_READ                = 1,
		CX_CORONA_DIAG_PROP_I2C_WRITE_THEN_READ     = 2
} CX_CORONA_DIAGNOSTIC_PROPERTIES;

static const GUID PROPSETID_CX_CORONA_DIAG_PROP =
{ 0x96ea8493, 0x34, 0x4935, { 0x95, 0x31, 0xdf, 0xd5, 0xd6, 0x16, 0x7c, 0x8b } };

/*---------------------------------------------------------------------------*/
//Blind scan
/*---------------------------------------------------------------------------*/
#define FE_TP_MAX	2048

typedef enum _BST_BLSCAN_CMD_TYPE
{
	BST_BLSCAN_CMD_START = 0,
	BST_BLSCAN_CMD_STOP,
	BST_BLSCAN_CMD_SETPARA,
	BST_BLSCAN_CMD_QUERY
	
}BST_BLSCAN_CMD_TYPE;

typedef enum _BST_BLSCAN_STATUS_TYPE
{
//IDLE->READY->PROCESSING->POST_PROCESS->FINISHED
// ^                                       |
// |---------------------------------------|	
	BST_BLSCAN_FINISHED = 0, //Blind scan is finished, data is ready now.
	BST_BLSCAN_PROCESSING, //Blind scan is processing, wait for finished or stop it.
	BST_BLSCAN_POST_PROCESS, //Almost finished, do some post process then finished.
	BST_BLSCAN_READY, //The parameter for Blind scan is set, ready to kick start.
	BST_BLSCAN_IDLE, //Blind scan is idle, need to feed new parameters.
	BST_BLSCAN_ABORT
		
}BST_BLSCAN_STATUS_TYPE;

//Blind scan data structure.
typedef struct _BST_BLSCAN_CMD
{
	//Command
	ULONG bls_command;
	ULONG begin_freqMHz;
	ULONG end_freqMHz;
	UCHAR uc_scanTimes; //1,2,3
	UCHAR uc_scanAlgorithm; //1: (Segment Lock:    algorithm = MT_FE_BS_ALGORITHM_A);
				//2: (Whole Lock Mode: algorithm = MT_FE_BS_ALGORITHM_B);
				//Default: 1
	BOOL b_voltage; //TRUE: 13V, FALSE: 18V
	BOOL b_22K; //TRUE: 22K on, FALSE: 22K off.
	
	//Status
	ULONG bls_status;
	ULONG bls_freqKHz; //Ticks will update by blind scan process.	
	
}BST_BLSCAN_CMD, *PBST_BLSCAN_CMD;

typedef enum _MT_FE_CODE_RATE
{
	MtFeCodeRate_Undef = 0
	,MtFeCodeRate_1_4
	,MtFeCodeRate_1_3
	,MtFeCodeRate_2_5
	,MtFeCodeRate_1_2
	,MtFeCodeRate_3_5
	,MtFeCodeRate_2_3
	,MtFeCodeRate_3_4
	,MtFeCodeRate_4_5
	,MtFeCodeRate_5_6
	,MtFeCodeRate_7_8
	,MtFeCodeRate_8_9
	,MtFeCodeRate_9_10
} MT_FE_CODE_RATE;

typedef enum _MT_FE_TYPE
{
	MtFeType_Undef = 0
	,MtFeType_DvbC
	,MtFeType_DvbT
	,MtFeType_Dtmb
	,MtFeType_DvbS
	,MtFeType_DvbS2
	,MtFeType_DvbS_Unknown
	,MtFeType_DvbS_Checked
} MT_FE_TYPE;

typedef struct _MT_FE_TP_INFO
{
    ULONG  freq_KHz;
    ULONG  sym_rate_KSs;
    MT_FE_TYPE dvb_type;
    MT_FE_CODE_RATE code_rate;
}MT_FE_TP_INFO;

typedef struct _BST_BLSCAN_DATA
{
	ULONG ul_tps;
	MT_FE_TP_INFO fe_tp[FE_TP_MAX];
	
}BST_BLSCAN_DATA, *PBST_BLSCAN_DATA;

