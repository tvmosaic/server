/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#define CREATIX_TUNER_FILTER_NAME  L"7231 BDA DVBS Tuner" 


/*
no, the property "InputRange" is not a private property set from Philips 
 
It is a property supported by Microsoft and is implemented on the  
"Frequency Filter Interface" of our Digital Tuner Filter. 
  
IBDA_FrequencyFilter::put_Range 
 
Below is a code drop how to access the "InputRange" property from any  
DirectShow application. 
 
 
               ..... 
               ......... 
 
        hr = m_pDVBTuneRequest->put_ONID(-1); 
        hr = m_pDVBTuneRequest->put_TSID(-1); 
        hr = m_pDVBTuneRequest->put_SID(servicePID); 
        hr = m_pITuner->put_TuneRequest(m_pDVBTuneRequest); 
 
                //  access the InputRange property directly 
 
               USHORT usRange = 0; 
  
               // set input range (option and position) 
               usRange = pDVBSettings->option; 
               usRange <<= 8; 
               usRange += (UCHAR)pDVBSettings->position; 
 
               PutTunerInputRange(usRange); 
  
                ......... 
                ..... 
 
Make sure you make the "InputRange" access AFTER you send the tune  
request.  
 
The call of the "PutTunerInputRange" function performs a re-tune with all  parameters previously passed to the driver. 
 
Changing the property together with the other DVB related properties does  
not work, because the NetworkProvider always  
overwrites the "InputRange" Property with -1. 
So I make the "InputRange" change after I finished the tuning with the  
Microsoft components. 
 
The source code for the function "PutTunerInputRange" is attached as .cpp  
file to this mail. 
 
If you need more information, please contact me again. 
 
 
That is exactly a DiSEqC Multiswitch for two LNBs and 8 clients. 
 
The meaning of position and option is as follows: 
 
               |   option   position  
=======+============== 
LNB A     |      0               0  
LNB B     |      0               1  
LNB C     |      1               0  
LNB D     |      1               1  
 
 
The "InputRange" property contains only the two bits 'option' and  
'position' coded  
as shown above. 
Where 'position' is at bit position 0 (LSB) and 'option' is at bit  
position 8 (LSB of the second byte) of the DWORD variable. 
 
That means  
 
- for LNB A you must set 'InputRange' to 0x00000000 
- for LNB B you must set 'InputRange' to 0x00000001 
- for LNB C you must set 'InputRange' to 0x00000100 
- for LNB D you must set 'InputRange' to 0x00000101 
 
 
The driver then extracts the two bit out of the 'InputRange' property  
value and uses this information  
to build the DiSEqC command that will be transmitted on the antenna feed. 
 
If you don't have a switch, don't care. The DiSEqC transmission does not  
disturb a LNB that is connected directly to your  
tuner board. 
 
The DiSEqC transmission is repeated 3 times, so that 3 levels (cascades)  
of switches are supported as well

*/