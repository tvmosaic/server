// Definitions for DD KS property sets.
// Author Manfred V�lkel, 2010
// (c) 2011 Digital Devices GmbH Germany.  All rights reserved

// $Id: DD_KSProperties.h 354 2011-03-17 00:46:24Z manfred $

#pragma once
#pragma warning( push )
#pragma warning( disable : 4201 )
#include <ks.h>
#pragma warning( pop )

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////


#define STATIC_KSPROPERTYSET_DD_COMMON_INTERFACE\
    0x0aa8a501, 0xa240, 0x11de, 0xb1, 0x30, 0x00, 0x00, 0x00, 0x00, 0x4d, 0x56
DEFINE_GUIDSTRUCT( "0aa8a501-a240-11de-b130-000000004d56", KSPROPERTYSET_DD_COMMON_INTERFACE );
#define KSPROPERTYSET_DD_COMMON_INTERFACE DEFINE_GUIDNAMED( KSPROPERTYSET_DD_COMMON_INTERFACE )

// The list of properties in this property set.

typedef enum
{
    KSPROPERTY_DD_DECRYPT_PROGRAM = 0,
    KSPROPERTY_DD_CAM_MENU_TITLE = 1,
    KSPROPERTY_DD_CAM_CAIDS = 2,
    KSPROPERTY_DD_CI_BITRATE = 3,
    KSPROPERTY_DD_UNLOCK_CIFILTER = 4,
} KSPROPERTY_DD_COMMON_INTERFACE;

typedef enum {
    DD_DECRYPT_NOFORWARD_CHAINING  = 0x80000000,
    DD_DECRYPT_NOBACKWARD_CHAINING = 0x40000000,
} DD_DECRYPT_PROGRAM_FLAGS;

typedef struct {
    BYTE    MenuTitle[256];
} DD_CAM_MENU_TITLE;

typedef struct {
    USHORT  CASystemID[64];
} DD_CAM_CAIDS;

#define STATIC_KSMETHODSET_DD_CAM_CONTROL\
    0x0aa8a511, 0xa240, 0x11de, 0xb1, 0x30, 0x00, 0x00, 0x00, 0x00, 0x4d, 0x56
DEFINE_GUIDSTRUCT( "0aa8a511-a240-11de-b130-000000004d56", KSMETHODSET_DD_CAM_CONTROL );
#define KSMETHODSET_DD_CAM_CONTROL DEFINE_GUIDNAMED( KSMETHODSET_DD_CAM_CONTROL )

typedef enum
{
    KSMETHOD_DD_CAM_RESET,
    KSMETHOD_DD_CAM_ENTER_MENU,
    KSMETHOD_DD_CAM_CLOSE_MENU,
    KSMETHOD_DD_CAM_GET_MENU,
    KSMETHOD_DD_CAM_MENU_REPLY,
    KSMETHOD_DD_CAM_ANSWER,
    KSMETHOD_DD_CAM_SEND_CA_PMT,
} KSMETHOD_DD_CAM_CONTROL;

typedef struct {
    ULONG   Id;
    ULONG   Type;
    ULONG   NumChoices;
    ULONG   Length;
    ULONG   Data[1];
} DD_CAM_MENU_DATA;

typedef struct {
    ULONG   Id;
    ULONG   Length;
    ULONG   Data[1];
} DD_CAM_TEXT_DATA;

typedef struct {
    ULONG   Id;
    ULONG   Choice;
} DD_CAM_MENU_REPLY;

typedef struct {
    ULONG   Length;
    ULONG   Data[1];    // 1st byte = ca_pmt_list_management
} DD_CAM_CA_PMT;
