/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>
#include <vector>

#include <drivers/deviceapi.h>
#include "../../ds_propertyset.h"
#include "../../common.h"		
#include "../../capmt.h"
#include "../../decrypt_cmd_to_listmgmt.h"
#include "../../ds_tunerequest.h"
#include "../../ds_topology.h"
#include "DD_KSProperties.h"
#include "dd_main.h"
#include <dl_logger.h>

using namespace dvblink::logging;

dd_property_set_t::dd_property_set_t() :
    ppropsetCapturePin_dd(NULL), pcontrolCapturePin_dd(NULL)
{
}

HRESULT dd_property_set_t::property_set_after_graph_start()
{
	HRESULT hr = E_FAIL;

    if (graph_info_.pfltCaptureEx_ != NULL)
    {
	    // get the property set
	    hr = get_filter_property_set(graph_info_.pfltCaptureEx_, &ppropsetCapturePin_dd);
	    if FAILED(hr) 
		    log_warning(L"DigitalDevices. No propertyset interface on the capture filter");
		//query IKsControl interface
		hr = graph_info_.pfltCaptureEx_->QueryInterface(IID_IKsControl, (void **)&pcontrolCapturePin_dd);
    }
    else
    {
	    log_warning(L"DigitalDevices. DD CI filter is NULL - no CI module is present (?)");
    }

	return hr;
}


HRESULT dd_property_set_t::property_set_deinit()
{
	if(ppropsetCapturePin_dd != NULL) 
    {
		ppropsetCapturePin_dd->Release();
		ppropsetCapturePin_dd= NULL;
	}
	
	if(pcontrolCapturePin_dd != NULL) 
    {
		pcontrolCapturePin_dd->Release();
		pcontrolCapturePin_dd= NULL;
	}
	
	return S_OK;
}

static bool GetPMTSectionServiceID(void* pmt_buffer, int pmt_length, unsigned short& ServiceId)
{
    unsigned char* buffer = (unsigned char*)pmt_buffer;
    ServiceId = ((((unsigned short)buffer[3]) << 8) & 0xFF00) | buffer[4];
    return true;
}

HRESULT dd_property_set_t::do_single_program_decryption(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng)
{
	HRESULT ret_val = E_FAIL;
	if (listmng != dvblink::SERVICE_DECRYPT_FIRST)
	{
		if (listmng == dvblink::SERVICE_DECRYPT_ADD)
			log_error(L"dd_ci_send_pmt: multi-channel decryption is not supported");
		return E_FAIL;
	}

	unsigned short ServiceId;
	GetPMTSectionServiceID(buf, len, ServiceId);

    log_info(L"DigitalDevices. Sending decryption request for program %d") % ServiceId;

	ULONG Program = ServiceId;
	ret_val = ppropsetCapturePin_dd->Set(KSPROPERTYSET_DD_COMMON_INTERFACE, 
		KSPROPERTY_DD_DECRYPT_PROGRAM,
		&Program, sizeof( ULONG ),
		&Program, sizeof( ULONG ));

	return ret_val;
}

HRESULT dd_property_set_t::do_multiple_program_decryption(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd)
{
	HRESULT ret_val = E_FAIL;

    if (pcontrolCapturePin_dd != NULL)
    {
	    E_CAPMT_LIST_MGMT list_mgmt;
	    E_CAPMT_CMD_ID list_mgmt_cmd;
	    if (!decryption_cmd_to_listmgt(cmd, list_mgmt, list_mgmt_cmd))
		    return E_FAIL;

        memset(g_ca_pmt, 0, sizeof(g_ca_pmt));
        //convert pmt to ca_pmt
        DD_CAM_CA_PMT* dd_ca_pmt = (DD_CAM_CA_PMT*)g_ca_pmt;
        int ca_pmt_len;
        CreateCAPMTFromPMT(buf, len, list_mgmt, list_mgmt_cmd, (unsigned char*)(dd_ca_pmt->Data), ca_pmt_len);
        if (ca_pmt_len > sizeof(g_ca_pmt) - sizeof(ULONG))
            log_error(L"DD do_multiple_program_decryption: Resulting CA_PMT size is more than allocated buffer: %d vs. %d") % ca_pmt_len % sizeof(g_ca_pmt);


        dd_ca_pmt->Length = ca_pmt_len;

        KSMETHOD KsMethod = {STATIC_KSMETHODSET_DD_CAM_CONTROL,
                                 KSMETHOD_DD_CAM_SEND_CA_PMT,
                                 KSMETHOD_TYPE_SEND};
        ULONG dwReturned = 0;
	    ret_val = pcontrolCapturePin_dd->KsMethod(&KsMethod, sizeof(KsMethod),
		    dd_ca_pmt, ca_pmt_len + sizeof( ULONG ), &dwReturned);
    }
    
    return ret_val;        
}

HRESULT dd_property_set_t::property_set_ci_send_pmt(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD listmng)
{
	HRESULT ret_val = E_FAIL;

	if (ppropsetCapturePin_dd != NULL)
	{
	    ret_val = do_multiple_program_decryption(buf, len, listmng);
	    //if invalid arg is returned (CAM is in MTD mode) - try regular single program decryption
	    if (ret_val == E_INVALIDARG)
            ret_val = do_single_program_decryption(buf, len, listmng);
	}

	return ret_val;
}

HRESULT dd_property_set_t::property_set_send_diseqc(unsigned char *cmd, int len)
{
    if (graph_info_.pDiseqcCommandInterface_ != NULL)
    {
        BOOLEAN benable = TRUE;
        HRESULT hr = graph_info_.pDiseqcCommandInterface_->put_EnableDiseqCommands(benable);
        //check if this is 1.0 diseqc command
        int diseqc10 = get_10_diseqc_from_raw_cmd(cmd, len);
        if (diseqc10 == 0)
        {
            //this is not 1.0 command - attempt sending diseqc command
            hr = graph_info_.pDiseqcCommandInterface_->put_DiseqSendCommand(1, len, cmd);
            return hr;
        }
    }
    return E_NOINTERFACE; //do input range for diseqc 1.0
}

