/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>

#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <tuner.h>
#include <commctrl.h>
#include <stdarg.h>
#include <bdatif.h>
#include <dl_dshow_enum.h>
#include <dl_dshow_helpers.h>
#include "ds_graph.h"
#include "common.h"
#include <dl_logger.h>

using namespace dvblink::logging;
using namespace dvblink::engine;

HRESULT	stop_psi_parser(IBaseFilter *pfltdemux, IBaseFilter *pfltnetworkprovider)
{
    return S_OK;

	IMPEG2PIDMap *pPIDMap = NULL;
	if (pfltdemux) 
    {
		HRESULT hr = pfltdemux->QueryInterface(__uuidof(IMPEG2PIDMap), (void**) &pPIDMap);
		if (SUCCEEDED(hr))
        {
			ULONG pids[4];
			pids[0] = 0x0000;
			pids[1] = 0x0001;
			pids[2] = 0x0011;
			pids[3] = 0x0012;			
			hr = pPIDMap->UnmapPID(4, pids);
			if(pPIDMap)
				pPIDMap->Release();
		}
	}

	IMPEG2_TIF_CONTROL *pTIFControl = NULL;
	if (pfltnetworkprovider) 
    {
		HRESULT hr = pfltnetworkprovider->QueryInterface(__uuidof(IMPEG2_TIF_CONTROL), (void**) &pTIFControl);
		if(pTIFControl != NULL)
        {
            ULONG pids[2];
			pids[0] = 0x0000; 	
			pids[1] = 0x0011; 	
			pTIFControl->AddPIDs(2, pids); 
			pTIFControl->Release();
		}
	}

	return S_OK;
}

HRESULT create_ms_demuxer(IGraphBuilder *pfltgraph, IBaseFilter **pfilter)
{	
	HRESULT hr;
	*pfilter = NULL;

    if (FAILED(hr = CoCreateInstance(CLSID_MPEG2Demultiplexer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)pfilter)))
    {
		log_warning(L"Cannot load the MS MPEG-2 Demultiplexer");
		return hr;
    } 
	
	
	if (FAILED( add_filter(pfltgraph, *pfilter, NULL)))
    {
		(*pfilter)->Release();
		*pfilter = NULL;
		log_error(L"cannot add the MS MPEG-2 Demultiplexer filter to the graph");
		return hr;
	}	
	
	return S_OK;
}

HRESULT create_tif(IGraphBuilder *pfltgraph, IBaseFilter **pfilter)
{		
	*pfilter = NULL;

	TDSEnum *penumTIFfilters = enum_create(KSCATEGORY_BDA_TRANSPORT_INFORMATION);
	if(!penumTIFfilters) 
        return E_FAIL;
	
	while (enum_next(penumTIFfilters) == S_OK) 
    {		
		if (enum_get_filter(penumTIFfilters, pfilter) == S_OK) 
        {
			enum_free(penumTIFfilters);				
			return add_filter(pfltgraph, *pfilter, NULL);			
		}		
	}

	enum_free(penumTIFfilters);
	return E_FAIL;
}