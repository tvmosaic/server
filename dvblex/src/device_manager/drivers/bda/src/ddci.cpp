/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <stdafx.h>

#include <windows.h>
#include <string.h>
#include <dshow.h>
#include <initguid.h>
#include <ks.h>
#include <ksmedia.h>
#include <bdatypes.h>
#include <bdamedia.h>
#include <bdaiface.h>
#include <uuids.h>
#include <tuner.h>
#include <commctrl.h>
#include <ksproxy.h>
#include <map>

#include "ddci.h"
#include "capmt.h"
#include "decrypt_cmd_to_listmgmt.h"
#include "ds_propertyset.h"
#include "ds_tunerequest.h"
#include "common.h"		
#include <dl_dshow_enum.h>
#include "DD_KSProperties.h"
#include <dl_logger.h>
#include <dl_strings.h>

using namespace dvblink::logging;
using namespace dvblink::engine;

bool DDCI_EnumDevices(ext_device_list_t& devices)
{
	TDSEnum *penumCapFilters = enum_create(KSCATEGORY_BDA_RECEIVER_COMPONENT);
	if(penumCapFilters) 
    {
	    while (enum_next(penumCapFilters) == S_OK) 
        {
		    enum_get_name(penumCapFilters);		
		    if (wcsstr(penumCapFilters->szDevicePathW, L"dev_0011") > 0 &&
                wcsstr(penumCapFilters->szDevicePathW, L"ven_dd01") > 0) 
            {
                ext_device_t device_desc;
                wcscpy_s(device_desc.device_info_.name, penumCapFilters->szNameW);
                wcscpy_s(device_desc.device_info_.hwid, penumCapFilters->szDevicePathW);
                device_desc.type_ = ecdt_dd_ci;
                devices.push_back(device_desc);
		    }
	    }
	    enum_free(penumCapFilters);
    }
    else
    {
        log_error(L"WinTVCI_EnumDevices: failed to enum bda receiver filters");				
    }

    return true;
}

bool unlock_dd_filter(IBaseFilter* filter)
{
    bool ret_val = false;
    //get propertyset interface
    IKsControl *ppset;
	HRESULT hr = filter->QueryInterface(IID_IKsControl, (void **) &ppset);
    if (hr == S_OK)
    {
        KSPROPERTY KsProperty = {STATIC_KSPROPERTYSET_DD_COMMON_INTERFACE,
                                 KSPROPERTY_DD_UNLOCK_CIFILTER,
                                 KSPROPERTY_TYPE_SET};
        ULONG Param = 1;
        DWORD dwReturned = 0;
        hr = ppset->KsProperty((PKSPROPERTY)&KsProperty,sizeof(KsProperty),
                                    &Param, sizeof( ULONG ),
                                    &dwReturned );
        if (hr == S_OK)
        {
            ret_val = true;
        } else
        {
	        log_error(L"unlock_dd_filter. Unlock returned an error %d. Filter is already assigned?") % hr;
        }

        ppset->Release();
    } else
    {
	    log_error(L"unlock_dd_filter. No propertyset interface on the capture filter");
    }
    return ret_val;
}

bool dd_get_menu_title(IBaseFilter* filter)
{
    bool ret_val = false;
    //get propertyset interface
    IKsControl *ppset;
	HRESULT hr = filter->QueryInterface(IID_IKsControl, (void **) &ppset);
    if (hr == S_OK)
    {
        KSPROPERTY KsProperty = {STATIC_KSPROPERTYSET_DD_COMMON_INTERFACE,
                                 KSPROPERTY_DD_CAM_MENU_TITLE,
                                 KSPROPERTY_TYPE_GET};

        DD_CAM_MENU_TITLE pTitle;
        DWORD dwReturned = 0;
        hr = ppset->KsProperty((PKSPROPERTY)&KsProperty,sizeof(KsProperty),
                                    &pTitle, sizeof( DD_CAM_MENU_TITLE ),
                                    &dwReturned );
        if (hr == S_OK && dwReturned > 0)
        {
            std::wstring wtitle;
            ConvertMultibyteToUC(EC_UTF8, (const char*)pTitle.MenuTitle, dwReturned, wtitle);
            log_info(L"dd_get_menu_title. cam menu title: %1%") % wtitle;
        } else
        {
            log_info(L"dd_get_menu_title. no cam module inserted");
        }

        ret_val = true;

        ppset->Release();
    } else
    {
	    log_error(L"unlock_dd_filter. No propertyset interface on the capture filter");
    }
    return ret_val;
}

void* DDCI_CreateDevice(const std::wstring& device_hwid)
{
    IBaseFilter* ret_val = NULL;
    TDSEnum *penumCapFilters = enum_create(KSCATEGORY_BDA_RECEIVER_COMPONENT);
	if(penumCapFilters) 
    {
	    while (enum_next(penumCapFilters) == S_OK) 
        {
		    enum_get_name(penumCapFilters);		
		    if (_wcsicmp(penumCapFilters->szDevicePathW, device_hwid.c_str()) == 0) 
            {
			    if (enum_get_filter(penumCapFilters, &ret_val) == S_OK) 
                {
                    if (unlock_dd_filter(ret_val))
                    {
                        //print info about the inserted CAM
                        dd_get_menu_title(ret_val);
                        break;
                    } else
                    {
                        log_error(L"DDCI_CreateDevice: unlock filter failed");
                        ret_val->Release();
                        ret_val = NULL;
                        break;
                    }
                } else
                {
                    log_error(L"DDCI_CreateDevice: failed to enum bda receiver filters");				
                }
		    }
	    }
	    enum_free(penumCapFilters);
    }
    else
    {
        log_error(L"DDCI_CreateDevice: failed to enum bda receiver filters");				
    }
    return ret_val;
}

void DDCI_DestroyDevice(void* device)
{
    if (device != NULL)
    {
        IBaseFilter* filter = (IBaseFilter*)device;
        filter->Release();
    }
}

static BOOL GetPMTSectionServiceID(void* pmt_buffer, int pmt_length, WORD& ServiceId)
{
    unsigned char* buffer = (unsigned char*)pmt_buffer;
    ServiceId = ((((WORD)buffer[3]) << 8) & 0xFF00) | buffer[4];
    return TRUE;
}

static HRESULT do_single_program_decryption(void* device, unsigned char *pmt_ptr, int pmt_len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd)
{
	HRESULT hr = E_FAIL;
	if (cmd != dvblink::SERVICE_DECRYPT_FIRST)
	{
		if (cmd == dvblink::SERVICE_DECRYPT_ADD)
			log_error(L"dd_ci4all_send_pmt: multi-channel decryption is not supported");
		return E_FAIL;
	}

    IBaseFilter* filter = (IBaseFilter*)device;

    IKsControl *ppset;
    hr = filter->QueryInterface(IID_IKsControl, (void **) &ppset);
    if (hr == S_OK)
    {
        KSPROPERTY KsProperty = {STATIC_KSPROPERTYSET_DD_COMMON_INTERFACE,
                                 KSPROPERTY_DD_DECRYPT_PROGRAM,
                                 KSPROPERTY_TYPE_SET};

	    WORD ServiceId;
	    GetPMTSectionServiceID(pmt_ptr, pmt_len, ServiceId);

        log_info(L"DDCI_SendPMT. Sending decryption request for program %d") % ServiceId;

        ULONG Param = ServiceId;
        Param |= (DD_DECRYPT_NOFORWARD_CHAINING|DD_DECRYPT_NOBACKWARD_CHAINING);

        DWORD dwReturned = 0;
        hr = ppset->KsProperty((PKSPROPERTY)&KsProperty,sizeof(KsProperty),
                                    &Param, sizeof( ULONG ),
                                    &dwReturned );
        if (hr != S_OK)
        {
            log_warning(L"DDCI_SendPMT. DD_DECRYPT_PROGRAM call returned an error");
        }

        ppset->Release();
    } else
    {
        log_error(L"DDCI_SendPMT. No propertyset interface on the capture filter");
    }
	
	return hr;
}

static HRESULT do_multiple_program_decryption(void* device, unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd)
{
	HRESULT ret_val = E_FAIL;

    IBaseFilter* filter = (IBaseFilter*)device;

    IKsControl *ppset;
    ret_val = filter->QueryInterface(IID_IKsControl, (void **) &ppset);
    if (ret_val == S_OK)
    {
	    E_CAPMT_LIST_MGMT list_mgmt;
	    E_CAPMT_CMD_ID list_mgmt_cmd;
	    if (!decryption_cmd_to_listmgt(cmd, list_mgmt, list_mgmt_cmd))
		    return E_FAIL;

        unsigned char* ca_pmt = new unsigned char[32768];

        memset(ca_pmt, 0, sizeof(ca_pmt));
        //convert pmt to ca_pmt
        DD_CAM_CA_PMT* dd_ca_pmt = (DD_CAM_CA_PMT*)ca_pmt;
        int ca_pmt_len;
        CreateCAPMTFromPMT(buf, len, list_mgmt, list_mgmt_cmd, (unsigned char*)(dd_ca_pmt->Data), ca_pmt_len);
        if (ca_pmt_len > sizeof(ca_pmt) - sizeof(ULONG))
            log_error(L"dd_ci4all do_multiple_program_decryption: Resulting CA_PMT size is more than allocated buffer: %d vs. %d") % ca_pmt_len % sizeof(ca_pmt);

        dd_ca_pmt->Length = ca_pmt_len;

        KSMETHOD KsMethod = {STATIC_KSMETHODSET_DD_CAM_CONTROL,
                                 KSMETHOD_DD_CAM_SEND_CA_PMT,
                                 KSMETHOD_TYPE_SEND};
        ULONG dwReturned = 0;
	    ret_val = ppset->KsMethod(&KsMethod, sizeof(KsMethod),
		    dd_ca_pmt, ca_pmt_len + sizeof( ULONG ), &dwReturned);
		    
        ppset->Release();

        delete ca_pmt;

    } else
    {
        log_error(L"dd_ci4all do_multiple_program_decryption:. No kscontrol interface on the capture filter");
    }    
    return ret_val;        
}

bool DDCI_SendPMT(void* device, unsigned char* pmt_ptr, int pmt_len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd)
{
    HRESULT hr = E_FAIL;
    if (device != NULL)
    {
	    hr = do_multiple_program_decryption(device, pmt_ptr, pmt_len, cmd);
	    //if invalid arg is returned (CAM is in MTD mode) - try regular single program decryption
	    if (hr == E_INVALIDARG)
            hr = do_single_program_decryption(device, pmt_ptr, pmt_len, cmd);
    }
    return hr == S_OK;
}

IBaseFilter* DDCI_GetDeviceFilter(void* device)
{
    if (device != NULL)
        return (IBaseFilter*)device;

    return NULL;
}
