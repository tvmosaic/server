/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#ifndef _DS_GRAPH_H

HRESULT create_graph(IGraphBuilder **pfltgraph, IMediaControl **pmediactrl, DWORD *dwrot);
HRESULT free_graph(IGraphBuilder **pfltgraph, IMediaControl **pmediactrl, DWORD *dwrot);
HRESULT add_filter(IGraphBuilder *pfltgraph, IBaseFilter *pfilter, WCHAR *filter_name);
HRESULT remove_filter(IGraphBuilder *pfltgraph, IBaseFilter *pfilter);
HRESULT create_tuner(IGraphBuilder *pfltgraph, const std::wstring& device_path, IBaseFilter **pfilter, wchar_t *wstunername, int wstunername_len,
                     wchar_t *wsdevicepath, int wsdevicepath_len);
HRESULT create_preferred_capture(const wchar_t* wsDevicePath, IGraphBuilder *pfltgraph, IBaseFilter *pftuner, IBaseFilter **pfilter);
HRESULT create_capture(IGraphBuilder *pfltgraph, IBaseFilter *pftuner, IBaseFilter **pfilter);
HRESULT create_network_provider(IGraphBuilder *pfltgraph, int tuner_type, IBaseFilter **ppfilter, ITuner **ptuner, bool& bUniversalProvider);
HRESULT load_tuning_space(int tuner_type, ITuningSpace **ppTuningSpace);
HRESULT create_tuning_space(int tuner_type, ITuningSpace **ppTuningSpace);
HRESULT init_tuning_space(ITuner *ptuner, int tuner_type, ITuningSpace *pTuningSpace);
HRESULT get_filter_property_set(IBaseFilter *pfilter, IKsPropertySet **pkspropertyset);
HRESULT get_pin_property_set(IBaseFilter *pfilter, IKsPropertySet **pkspropertyset);
HRESULT start_graph(IMediaControl* pmediactrl, DWORD timeout);
HRESULT stop_graph(IMediaControl* pmediactrl, DWORD timeout);

#define _DS_GRAPH_H
#endif
