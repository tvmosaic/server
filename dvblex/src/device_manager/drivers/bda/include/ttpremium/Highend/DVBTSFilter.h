//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     DVBTSFilter.h
//
//  Project(s):   ttdvbacc
//
//  Author:       GRi
//
//  Purpose:      Filter class
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: DVBTSFilter.h $
//   $Revision: 3 $  
//    $Modtime: 25.07.01 14:32 $
//
//   $Log: /DVB-PC/API/HighEnd/include/DVBTSFilter.h $
//     >>   
//     >>   3     25.07.01 17:03 Guido
//     >>   
//     >>   2     18.07.01 14:41 Guido
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   6     15.05.01 16:30 Guido
//
///////////////////////////////////////////////////////////////////////////

// DVBTSFilter.h: Schnittstelle f�r die Klasse CDVBTSFilter.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DVBTSFILTER_H__61B6C2F8_CA3D_4686_9119_70EC7D695581__INCLUDED_)
#define AFX_DVBTSFILTER_H__61B6C2F8_CA3D_4686_9119_70EC7D695581__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DVBCommon.h"

// timeout in ms
#define STOP_THREAD_TIMEOUT 200

// Gr��e des Piping-Buffers (bLength-Parameter bei Piping-Filter)
#define	PIPE_SIZE_NONE		0x00 // default  (1 KByte)
#define	PIPE_SIZE_1P		0x01 // 1 Paket	 (184 Byte)
#define	PIPE_SIZE_2P		0x02 // 2 Pakete (368 Byte)
#define	PIPE_SIZE_1K		0x03 // 1 KByte
#define	PIPE_SIZE_2K		0x04 // 2 KByte
#define	PIPE_SIZE_4K		0x05 // 4 KByte
#define	PIPE_SIZE_8K		0x06 // 8 KByte
#define PIPE_SIZE_16K		0x07 //16 KByte
#define PIPE_SIZE_32K		0x08 //32 KByte


class AFX_EXT_CLASS CDVBTSFilter// : public CDVBCommon
{
public:

	typedef enum
	{	NO_FILTER_TYPE,
		STREAMING_FILTER,
		PIPING_FILTER,
//		PES_FILTER,
//		ES_FILTER,
		SECTION_FILTER,
		MPE_SECTION_FILTER,
//		PID_FILTER,
//		MULTI_PID_FILTER,
//		TS_FILTER,
		MPE_SECTION_HIGHSPEED
	} FILTERTYPE;

	CDVBTSFilter(); // 256 KByte interner Buffer
	CDVBTSFilter(DWORD BufferSize); // variabler interner Buffer
	virtual ~CDVBTSFilter();

	DVB_ERROR SetFilter(FILTERTYPE FilterType, WORD wPID, BYTE* pbFilterData = NULL, BYTE* pbFilterMask = NULL, BYTE bLength = 0);
	DVB_ERROR ResetFilter(void);
	BYTE  GetFilterID(void); // f�r DVBWinSock und DVBFastSocket (SOpen) (f�r Kompatibilit�t)
	DWORD GetByteCount(void);

	static DVB_ERROR GetFiltersByteCount(DWORD* aCounts, BYTE NrOfCounts);

	// Dummy-Funktionen f�r Kompatibilit�t
	BOOL SOpen(BYTE FilterID, int* err = NULL) {if(err) *err=0; return TRUE;};
	void SClose() {return;};

	// f�r Data-Routing durch Windows-Sockets
	DVB_ERROR SetWinSockFilter(FILTERTYPE FilterType, WORD wPID, BYTE* pbFilterData = NULL, BYTE* pbFilterMask = NULL, BYTE bLength = 0);

protected:
	// CDVBTSFilter Overridable callback
	virtual void OnDataArrival(BYTE* Buff, int len);

private:
	DVB_ERROR AddFilter(BYTE& bFilterID, FILTERTYPE FilterType, WORD wPID, BYTE* pbFilterData, BYTE* pbFilterMask, BYTE bLength);
	DVB_ERROR DelFilter(BYTE bFilterID);
	DWORD GetFilterStatus(BYTE bFilterID);
	BYTE GetFilterMode(FILTERTYPE FilterType);
	BOOL Run();

	void* m_pAccu;
	CEvent m_Event;
	BOOL m_bSendToWinSock;

	BYTE m_bFilterID;
//	WORD m_wFilterHandle;
	void TSCallback(int FilterType, BYTE FilterID, BYTE* pStart, DWORD Length, BYTE Flags);

	BOOL StartThread(int iPriority = THREAD_PRIORITY_NORMAL);
	BOOL StopThread(DWORD dwTimeout = STOP_THREAD_TIMEOUT); // in Millisekunden
	BOOL IsRunning();

	CCriticalSection m_CriticalSection;
	volatile long m_lRunThread;
	CWinThread* m_pThread;

	static UINT WorkerThread(LPVOID pParam);
	// Eintrittsfunktion der Daten
	static void ExtCallback(void* pObj, int FilterType, BYTE FilterID, BYTE* pStart, DWORD Length, BYTE Flags);
};

#endif // !defined(AFX_DVBTSFILTER_H__61B6C2F8_CA3D_4686_9119_70EC7D695581__INCLUDED_)
