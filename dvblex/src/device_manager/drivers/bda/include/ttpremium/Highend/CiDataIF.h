//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     CiDataIF.h
//
//  Project(s):   
//
//  Author:       GRi
//
//  Purpose:      Common Interface defines
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: CiDataIF.h $
//   $Revision: 1 $  
//    $Modtime: 15.05.01 15:52 $
//
//   $Log: /DVB-PC/API/HighEnd/include/CiDataIF.h $
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   4     15.05.01 16:29 Guido
//
///////////////////////////////////////////////////////////////////////////

#ifndef _CI_DATA_IF
#define	_CI_DATA_IF

//#define	CI_SLOTS	2

//SendCIMessage Tags
#define	CI_MSG_NONE				0
#define	CI_MSG_CI_INFO			1
#define	CI_MSG_MENU				2
#define	CI_MSG_LIST				3
#define	CI_MSG_TEXT				4
#define	CI_MSG_REQUEST_INPUT	5
#define	CI_MSG_INPUT_COMPLETE	6
#define	CI_MSG_LIST_MORE		7
#define	CI_MSG_MENU_MORE		8
#define	CI_MSG_CLOSE_MMI_IMM	9
#define	CI_MSG_SECTION_REQUEST	0xA
#define	CI_MSG_CLOSE_FILTER		0xB
#define	CI_PSI_COMPLETE			0xC
#define	CI_MODULE_READY			0xD
#define	CI_SWITCH_PRG_REPLY		0xE
#define	CI_MSG_TEXT_MORE		0xF

#define	CI_HC_TUNE				0x20
#define	CI_HC_REPLACE			0x21
#define	CI_HC_CLEAR_REPLACE		0x22
#define	CI_HC_ASK_RELEASE		0x23

#define	CI_LSC_CMD				0x30
#define	CI_LSC_DESCRIPTOR		0x31
#define	CI_LSC_SEND_LAST		0x32
#define	CI_LSC_SEND_MORE		0x33

#define	CI_MSG_CA_PMT			0xE0
#define	CI_MSG_ERROR			0xF0

//error codes
#define	ERR_NONE				0
#define	ERR_WRONG_FLT_INDEX		1
#define	ERR_SET_FLT				2
#define	ERR_CLOSE_FLT			3
#define	ERR_INVALID_DATA		4
#define	ERR_NO_CA_RESOURCE		5

//slot status
#define	CI_SLOT_EMPTY			0
#define	CI_SLOT_MODULE_INSERTED	1
#define	CI_SLOT_MODULE_OK		2
#define CI_SLOT_CA_OK			3
#define	CI_SLOT_DBG_MSG			4
#define	CI_SLOT_UNKNOWN_STATE	0xFF

//key code masks (see DVB document A017 page 44)
#define	CI_KEYMASK_MENU			0x1
#define	CI_KEYMASK_LIST			0x1
#define	CI_KEYMASK_ALL			0x3FFFF

//control codes contained in strings
#define	CI_CTRL_LF				0x8A


//commands for the low speed communication
#define	CI_LSC_CONNECT_ON_CHANNEL		0x1
#define	CI_LSC_DISCONNECT_ON_CHANNEL	0x2
#define	CI_LSC_SET_PARAMS				0x3
#define	CI_LSC_ENQUIRE_STATUS			0x4
#define	CI_LSC_GET_NEXT_BUFFER			0x5

//commands reply ID
#define	CI_LSC_CONNECT_ACK				0x1
#define	CI_LSC_DISCONNECT_ACK			0x2
#define	CI_LSC_SET_PARAMS_ACK			0x3
#define	CI_LSC_STATUS_REPLY				0x4
#define	CI_LSC_GET_NEXT_BUFFER_ACK		0x5
#define	CI_LSC_SEND_ACK					0x6

//commands reply return values
#define REPLY_NO_ERROR					0x0
#define	REPLY_ERROR						0xFF
#define	STATUS_REPLY_DISCONNECTED		0x0
#define	STATUS_REPLY_CONNECTED			0x1


#define	SI_TELEPHONE_DESCRIPTOR			0x1
#define	CABLE_RETURN_CAHNNEL_DESCRIPTOR	0x2
#define	INTERNET_DESCRIPTOR				0x3

#define	LSC_V42BIS				0x0001
#define	LSC_V42					0x0002
#define	LSC_V21					0x0004
#define LSC_V22					0x0008
#define	LSC_V22BIS				0x0010
#define LSC_V23					0x0020
#define LSC_V32					0x0040
#define LSC_V32BIS				0x0080
#define LSC_V34					0x0100
#define LSC_V27TER				0x0200
#define LSC_V29					0x0400

#endif
