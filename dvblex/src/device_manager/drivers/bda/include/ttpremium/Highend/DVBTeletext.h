//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     DVBTeletext.h
//
//  Project(s):   ttdvbacc
//
//  Author:       GRi
//
//  Purpose:      Teletext/Videotext class
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: DVBTeletext.h $
//   $Revision: 2 $  
//    $Modtime: 17.07.01 16:40 $
//
//   $Log: /DVB-PC/API/HighEnd/include/DVBTeletext.h $
//     >>   
//     >>   2     18.07.01 14:41 Guido
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   4     15.05.01 16:30 Guido
//
///////////////////////////////////////////////////////////////////////////

// DVBTeletext.h: Schnittstelle f�r die Klasse CDVBTeletext.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DVBTELETEXT_H__1C207A91_BFA6_4943_9BC0_BC06E68865FA__INCLUDED_)
#define AFX_DVBTELETEXT_H__1C207A91_BFA6_4943_9BC0_BC06E68865FA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DVBCommon.h"

// timeout in ms
#define STOP_THREAD_TIMEOUT 200

class AFX_EXT_CLASS CDVBTeletext// : public CDVBCommon
{
public:
	
	typedef enum
	{
		DISABLE_PAGE_CAPTURE = 0,
		CAPTURE_PAGE_PARITY_CORRECTED = 1,
		CAPTURE_PAGE_HAMMING_8_4_CORRECTED = 2,
		CAPTURE_PAGE_8BIT_DATA = 3
	} TTX_REQUEST;

	CDVBTeletext();
	virtual ~CDVBTeletext();

	DVB_ERROR TTXOn(WORD wPID);
	DVB_ERROR TTXOff();
	BOOL TTXModifySearchTable(WORD wMag, WORD wTens, WORD wUnits, TTX_REQUEST Request);
	BOOL TTXGetP29_P830(BOOL bPage29);

protected:
	// CDVBTeletext Overridable callback
	virtual void OnTTXPage(BYTE* TTXPage, int len);
	
private:
//	BOOL m_fIsOn;
	void* m_pAccu;
	CEvent m_Event;

	BYTE m_bFilterID;
	void TSCallback(int FilterType, BYTE FilterID, BYTE* pStart, DWORD Length, BYTE Flags);

	BOOL StartThread(int iPriority = THREAD_PRIORITY_NORMAL);
	BOOL StopThread(DWORD dwTimeout = STOP_THREAD_TIMEOUT); // in Millisekunden
	BOOL IsRunning();
	BOOL Run();

	CCriticalSection m_CriticalSection;
	volatile long m_lRunThread;
	CWinThread* m_pThread;

	static UINT WorkerThread(LPVOID pParam);
	// Eintrittsfunktion der Daten
	static void ExtCallback(void* pObj, int FilterType, BYTE FilterID, BYTE* pStart, DWORD Length, BYTE Flags);
};

#endif // !defined(AFX_DVBTELETEXT_H__1C207A91_BFA6_4943_9BC0_BC06E68865FA__INCLUDED_)
