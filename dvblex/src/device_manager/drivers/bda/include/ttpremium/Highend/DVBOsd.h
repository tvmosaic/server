//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     DVBOsd.h
//
//  Project(s):   ttdvbacc
//
//  Author:       GRi
//
//  Purpose:      On-Screen-Display class
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: DVBOsd.h $
//   $Revision: 3 $  
//    $Modtime: 7.09.01 14:45 $
//
//   $Log: /DVB-PC/API/HighEnd/include/DVBOsd.h $
//     >>   
//     >>   3     7.09.01 16:26 Guido
//     >>   
//     >>   2     18.07.01 14:41 Guido
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   3     15.05.01 16:30 Guido
//
///////////////////////////////////////////////////////////////////////////

// Overlay.h: Schnittstelle f�r die Klasse COverlay.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DVBOSD_H__7CB177E0_680B_11D2_8E91_0080AD190E2E__INCLUDED_)
#define AFX_DVBOSD_H__7CB177E0_680B_11D2_8E91_0080AD190E2E__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "DVBCommon.h"

// defines for BlitBitmap-Function (parameter 'trans')
#define	TRANSP_FLAG		0x01
#define	PALETTE_FLAG	0x02

class AFX_EXT_CLASS CDVBOsd// : public CDVBCommon  
{
public:

	typedef enum _OSDPALTYPE
	{
		NoPalet =  0,       // No palette
		Pal1Bit =  2,       // 2 colors for 1 Bit Palette
		Pal2Bit =  4,       // 4 colors for 2 bit palette
		Pal4Bit =  16,      // 16 colors for 4 bit palette
		Pal8Bit =  256      // 256 colors for 16 bit palette
	} OSDPALTYPE, *POSDPALTYPE;

	typedef enum _DISPTYPE
	{
	   BITMAP1,             // 1 bit bitmap
	   BITMAP2,             // 2 bit bitmap
	   BITMAP4,             // 4 bit bitmap
	   BITMAP8,             // 8 bit bitmap
	   BITMAP1HR,           // 1 Bit bitmap half resolution
	   BITMAP2HR,           // 2 bit bitmap half resolution
	   BITMAP4HR,           // 4 bit bitmap half resolution
	   BITMAP8HR,           // 8 bit bitmap half resolution
	   YCRCB422,            // 4:2:2 YCRCB Graphic Display
	   YCRCB444,            // 4:4:4 YCRCB Graphic Display
	   YCRCB444HR,          // 4:4:4 YCRCB graphic half resolution
	   VIDEOTSIZE,          // Full Size MPEG Video Display
	   VIDEOHSIZE,          // MPEG Video Display Half Resolution
	   VIDEOQSIZE,          // MPEG Video Display Quarter Resolution
	   VIDEOESIZE,          // MPEG Video Display Eighth Resolution
	   CURSOR               // Cursor
	} DISPTYPE, *PDISPTYPE; // Window display type

	CDVBOsd();
	virtual ~CDVBOsd();

	DVB_ERROR CreateOSDWindow(BYTE windownr, DISPTYPE DisplayType, WORD width, WORD height);
	DVB_ERROR DestroyOSDWindow(BYTE windownr);

	DVB_ERROR ResetBlend(BYTE windownr);
	DVB_ERROR SetColorBlend(BYTE windownr);
	DVB_ERROR SetWindowBlend(BYTE windownr, BYTE blending);
	DVB_ERROR SetBlend_(BYTE windownr, OSDPALTYPE colordepth, WORD index, BYTE blending);
	DVB_ERROR SetColor_(BYTE windownr, OSDPALTYPE colordepth, WORD index, WORD colorHi, WORD colorLo);
	DVB_ERROR BringToTop(BYTE windownr);
	DVB_ERROR SetFont(BYTE windownr, BYTE fontsize, WORD colorFG, WORD colorBG);
	DVB_ERROR WriteText(BYTE windownr, WORD X, WORD Y, BYTE length, char* cText);
	DVB_ERROR DrawLine(BYTE windownr, WORD X, WORD Y, WORD dx, WORD dy, WORD colornr);
	DVB_ERROR DrawBlock(BYTE windownr, WORD X, WORD Y, WORD dx, WORD dy, WORD colornr);
	DVB_ERROR HideWindow(BYTE windownr);
	DVB_ERROR MoveWindowRel(BYTE windownr, WORD X, WORD Y);
	DVB_ERROR MoveWindowAbs(BYTE windownr, WORD X, WORD Y);
	DVB_ERROR SetWindowTrans(BYTE windownr, BOOL trans);
	// f�r Bitmap-UpLoad
	DVB_ERROR LoadBMP(const CString& BMPFileName);
	DVB_ERROR BlitBitmap(WORD windownr, WORD x, WORD y, WORD trans);
	DVB_ERROR ReleaseBitmap(void);
private:
	DVB_ERROR LoadBitmap(WORD format, WORD dx, WORD dy); //load Bmp to ARM-Memory
};

#endif // !defined(AFX_DVBOSD_H__7CB177E0_680B_11D2_8E91_0080AD190E2E__INCLUDED_)
