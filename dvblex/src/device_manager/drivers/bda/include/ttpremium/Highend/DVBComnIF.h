//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     DVBComnIF.h
//
//  Project(s):   ttdvbacc
//
//  Author:       GRi
//
//  Purpose:      Common-Interface-Control class
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: DVBComnIF.h $
//   $Revision: 3 $  
//    $Modtime: 21.11.02 11:41 $
//
//   $Log: /DVB-PC/API/HighEnd/include/DVBComnIF.h $
//     >>   
//     >>   3     21.11.02 11:50 J�rg
//     >>   
//     >>   2     18.07.01 14:41 Guido
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   6     15.05.01 16:30 Guido
//
///////////////////////////////////////////////////////////////////////////

// DVBComnIF.h: Schnittstelle f�r die Klasse CDVBComnIF.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DVBCOMNIF_H__55655C66_D89B_469E_9889_3EDD740CE678__INCLUDED_)
#define AFX_DVBCOMNIF_H__55655C66_D89B_469E_9889_3EDD740CE678__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DVBCommon.h"
#include "CiDataIF.h"

// timeout in ms
#define STOP_THREAD_TIMEOUT 200

// Do not use this class and the CDVBPsi class at the same time!
class AFX_EXT_CLASS CDVBComnIF
{
public:
	typedef enum _typ_ConnectionType
	{
		LscPhone		=1,
		LscCable		=2,
		LscInternet		=3,
		LscSerial
	} typ_ConnectionType;

	typedef struct _typ_ConnectionDesc
	{
		typ_ConnectionType	ConnectionType;
		CString				DialIn; //phone number / channel ID
		CString				ClientIP;
		CString				ServerIP;
		CString				TcpPort;
		CString				ConnectAutID;
		CString				LogonUsername;
		CString				LogonPassword;
		BYTE				RetryCount;
		BYTE				Timeout10Ms;
	} typ_ConnectionDesc;

	typedef struct _typ_SlotInfo
	{
		BYTE	nStatus;
		char*	pMenuTitleString;
		WORD*	pCaSystemIDs;
		WORD	wNoOfCaSystemIDs;
	} typ_SlotInfo;
	
	CDVBComnIF();
	virtual ~CDVBComnIF();

	DVB_ERROR Open(); // DVB_ERROR Init();
	DVB_ERROR Close();
	DVB_ERROR GetCIVersion(CString& strLabel, CString& strDate);

	DVB_ERROR ReadPSIFast(WORD PNR); // SID
	DVB_ERROR SetProgram(WORD PNR); // SID
	DVB_ERROR SendTime(void); // empty!
	DVB_ERROR EnterModuleMenu(BYTE nSlot);
	DVB_ERROR GetSlotStatus(BYTE nSlot = 0);
	DVB_ERROR Answer(BYTE nSlot, char* pKeyBuffer, BYTE nLength);
	DVB_ERROR MenuAnswer(BYTE nSlot, BYTE Selection);
	DVB_ERROR CloseMMI(BYTE nSlot);
	DVB_ERROR AskRelease(void);
	DVB_ERROR CommsReply(BYTE nSlot, BYTE CommsReplyId, BYTE ReturnValue);
	DVB_ERROR DataReceived(BYTE nSlot, BYTE PhaseID, BYTE* pData, WORD nLength);
	int	ConvertCharBuf(CString &string, int len);

protected:
	virtual	void OnSlotStatus(BYTE nSlot, BYTE nStatus, typ_SlotInfo* csInfo);
	virtual void OnCAStatus(BYTE nSlot, BYTE nReplyTag, WORD wStatus);
	virtual void OnDisplayString(BYTE nSlot, char* pString, WORD wLength);
	virtual void OnDisplayMenu(BYTE nSlot, WORD wItems, char* pStringArray, WORD wLength);
	virtual void OnDisplayList(BYTE nSlot, WORD wItems, char* pStringArray, WORD wLength);
	virtual void OnSwitchOsdOff(BYTE nSlot);
	virtual void OnInputRequest(BYTE nSlot, BOOL bBlindAnswer, BYTE nExpectedLength, DWORD dwKeyMask);

	virtual	void OnTune(BYTE nSlot, WORD NID, WORD OrigNID, WORD TSID, WORD SID);
	virtual void OnReplace(BYTE nSlot, BYTE ReplacementRef, WORD ReplacedPID, WORD ReplacementPID);
	virtual void OnClearReplace(BYTE nSlot, BYTE ReplacementRef);

	virtual void OnLscSetDescriptor(BYTE nSlot, typ_ConnectionDesc* pDescriptor);
	virtual void OnLscConnect(BYTE nSlot);
	virtual void OnLscDisconnect(BYTE nSlot);
	virtual void OnLscSetParams(BYTE nSlot, BYTE BufferSize, BYTE Timeout10Ms);
	virtual void OnLscEnquireStatus(BYTE nSlot);
	virtual void OnLscGetNextBuffer(BYTE nSlot, BYTE PhaseID);
	virtual void OnLscTransmitBuffer(BYTE nSlot, BYTE PhaseID, BYTE* pData, WORD nLength);

private:
	void ProcessCiData(BYTE* Buff, int len);
	void DecodeConnectionDesc(char* pSource, typ_ConnectionDesc* pConnectionDesc);
	short DecodeLengthField(BYTE* papdu, WORD* nLength);
	DVB_ERROR SendCICommand(BYTE SubCommand, BYTE* Params, BYTE ParamLen);

//	SYSTEMTIME sysTime;
//	WORD	m_wActualKeyMask;
//	BYTE	m_nExpectedInputLength;
//	UINT	m_CITimer;

private:
	void* m_pAccu;
	CEvent m_Event;

	BYTE m_bFilterID;
	void TSCallback(int FilterType, BYTE FilterID, BYTE* pStart, DWORD Length, BYTE Flags);

	BOOL StartThread(int iPriority = THREAD_PRIORITY_NORMAL);
	BOOL StopThread(DWORD dwTimeout = STOP_THREAD_TIMEOUT); // in Millisekunden
	BOOL IsRunning();
	BOOL Run();

	CCriticalSection m_CriticalSection;
	volatile long m_lRunThread;
	CWinThread* m_pThread;

	static UINT WorkerThread(LPVOID pParam);
	// Eintrittsfunktion der Daten
	static void ExtCallback(void* pObj, int FilterType, BYTE FilterID, BYTE* pStart, DWORD Length, BYTE Flags);
};

#endif // !defined(AFX_DVBCOMNIF_H__55655C66_D89B_469E_9889_3EDD740CE678__INCLUDED_)
