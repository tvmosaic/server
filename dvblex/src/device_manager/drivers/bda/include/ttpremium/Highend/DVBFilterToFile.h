//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     DVBFilterToFile.h
//
//  Project(s):   ttdvbacc
//
//  Author:       GRi
//
//  Purpose:      writes filtered data to file
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: DVBFilterToFile.h $
//   $Revision: 2 $  
//    $Modtime: 17.07.01 16:40 $
//
//   $Log: /DVB-PC/API/HighEnd/include/DVBFilterToFile.h $
//     >>   
//     >>   2     18.07.01 14:41 Guido
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   5     15.05.01 16:30 Guido
//
///////////////////////////////////////////////////////////////////////////

// DVBFilterToFile.h: interface for the CDVBFilterToFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILTERTOFILE_H__18B567D2_27F9_11D4_B892_00105A22770E__INCLUDED_)
#define AFX_FILTERTOFILE_H__18B567D2_27F9_11D4_B892_00105A22770E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "DVBFastSocket.h"
#include "DVBTSFilter.h"

//class __declspec(dllexport) CDVBFilterToFile : public CDVBFastSocket, public CDVBFilter
class AFX_EXT_CLASS CDVBFilterToFile : public CDVBTSFilter
{
public:
	CDVBFilterToFile();
	virtual ~CDVBFilterToFile();

	void SetFileParams(const CString& Name, UINT nOpenFlags = CFile::modeCreate | CFile::modeWrite);
	DVB_ERROR StartFilter(CDVBTSFilter::FILTERTYPE FilterType, WORD wPID, BYTE* pbFilterData = NULL, BYTE* pbFilterMask = NULL, BYTE bLength = 0);
	DVB_ERROR StopFilter(void);
	DWORD GetFileLen(void);

protected:
	virtual void OnDataArrival(BYTE* Buff, int len);

private:	
	CString m_FileName;
	CFile m_File;
	UINT m_OpenFlags;
};

#endif // !defined(AFX_FILTERTOFILE_H__18B567D2_27F9_11D4_B892_00105A22770E__INCLUDED_)
