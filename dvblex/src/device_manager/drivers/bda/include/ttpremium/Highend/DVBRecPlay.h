//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     DVBRecPlay.h
//
//  Project(s):   ttdvbacc
//
//  Author:       GRi
//
//  Purpose:      Memory based Record/Playback class
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: DVBRecPlay.h $
//   $Revision: 7 $  
//    $Modtime: 30.09.04 10:44 $
//
//   $Log: /DVB-PC/API/HighEnd/include/DVBRecPlay.h $
//     >>   
//     >>   7     30.09.04 12:15 Guido
//     >>   
//     >>   6     30.09.04 10:27 J�rg
//     >>   
//     >>   5     28.09.04 9:33 J�rg
//     >>   
//     >>   4     27.09.04 16:32 J�rg
//     >>   
//     >>   3     10.10.02 16:53 Guido
//     >>   
//     >>   2     18.07.01 14:41 Guido
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   4     15.05.01 16:30 Guido
//
///////////////////////////////////////////////////////////////////////////

// DVBRecPlay.h: Schnittstelle f�r die Klasse CDVBRecPlay.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DVBRECPLAY_H__FB6408C5_FA92_487C_8B26_868E2ED012BE__INCLUDED_)
#define AFX_DVBRECPLAY_H__FB6408C5_FA92_487C_8B26_868E2ED012BE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DVBCommon.h"

// BlockSize 
#define MAX_BLOCK_SIZE 8192

class AFX_EXT_CLASS CDVBRecPlay  
{
public:
	typedef enum _REC_PLAY_FORMAT
	{	RP_None,
		AudioPPES,		// packetized Audio-PES
		AudioMp2,		// MPEG1/2-LayerII Audio-Frames
		AudioPCM,		// PCM-Audio-Samples
		VideoPPES,		// packetized Video-PES
		AudioVideoPPES	// packetized Audio- and Video-PES
	} REC_PLAY_FORMAT, *PREC_PLAY_FORMAT;

	typedef enum _REC_PLAY_MODE
	{	RP_Stop,
		RP_Record,
		RP_Playback,
		RP_PlaybackPause,
		RP_PlaybackSlow,
		RP_PlaybackFast
	} REC_PLAY_MODE, *PREC_PLAY_MODE;

	CDVBRecPlay();
	virtual ~CDVBRecPlay();

	// allowed record/playback state transitions (functions return DVB_ERR_STATE otherwise):
	// from Stop			to	Record and Playback
	// from Record			to	Stop
	// from Playback		to	Stop, PlaybackPause, PlaybackSlow, PlaybackFast
	// from PlaybackPause	to	Stop, Playback, PlaybackSlow, PlaybackFast, PlaybackPause
	// from PlaybackFast	to	Stop, PlaybackPause, PlaybackSlow, Playback
	// from PlaybackSlow	to	Stop, PlaybackPause, Playback, PlaybackFast

	DVB_ERROR Playback(REC_PLAY_FORMAT format, WORD wSampleFreq = 0); // wSampleFreq = PCM-Audio and MP2-Audio sample frequency
	DVB_ERROR Record(REC_PLAY_FORMAT format);
	DVB_ERROR Stop(void);
	DVB_ERROR PlaybackPause(void);
	DVB_ERROR PlaybackFast(BOOL IP_I = TRUE); // TRUE: I- and P-frames; FALSE: I-frames only
	DVB_ERROR PlaybackSlow(BYTE Repeat = 2); // Repeat: how many times a picture is repeated

	// returns packet-length if record is running and a packet was copied to Buf
	// returns  0 if no packet present (none-blocking mode)
	// returns -1 if Buf is to small for packet
	// returns -2 if record is stopped
	int ReadPacket(BYTE* Buf, DWORD BufLen, BOOL bBlockingMode = TRUE);

	// returns PktLen if playback is running and the packet was written
	// returns  0 if the packet was not written (none-blocking mode)
	// returns -1 if the packet is to big for the hardware
	// returns -2 if playback is stopped
	int WritePacket(BYTE* Pkt, DWORD PktLen, BOOL bBlockingMode = TRUE);

	REC_PLAY_FORMAT GetRecPlayFormat(void) {return m_RecPlayFormat;};
	REC_PLAY_MODE GetRecPlayMode(void) {return m_RecPlayMode;};
	//WORD GetSampleFreqency(void) {return m_wSampleFreq;};

protected:
	REC_PLAY_FORMAT m_RecPlayFormat;
	REC_PLAY_MODE   m_RecPlayMode;
	//WORD m_wSampleFreq;

private:
	BYTE m_bFilterIDRead;
	BYTE m_bFilterIDWrite;

	BYTE m_data[MAX_BLOCK_SIZE];
	void* m_pAccu;
	CEvent m_ReadEvent;
	CEvent m_WriteEvent;

	BOOL SetupWriteEvent(BOOL bInstall);

	void TSCallback(int FilterType, BYTE FilterID, BYTE* pStart, DWORD Length, BYTE Flags);
	// Eintrittsfunktion der Daten
	static void ExtCallback(void* pObj, int FilterType, BYTE FilterID, BYTE* pStart, DWORD Length, BYTE Flags);
};

#endif // !defined(AFX_DVBRECPLAY_H__FB6408C5_FA92_487C_8B26_868E2ED012BE__INCLUDED_)
