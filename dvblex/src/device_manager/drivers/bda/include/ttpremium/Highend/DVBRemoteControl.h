//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     DVBRemoteControl.h
//
//  Project(s):   ttdvbacc
//
//  Author:       GRi
//
//  Purpose:      IR-Remote-Control class
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: DVBRemoteControl.h $
//   $Revision: 2 $  
//    $Modtime: 17.07.01 16:40 $
//
//   $Log: /DVB-PC/API/HighEnd/include/DVBRemoteControl.h $
//     >>   
//     >>   2     18.07.01 14:41 Guido
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   3     15.05.01 16:30 Guido
//
///////////////////////////////////////////////////////////////////////////

// DVBRemoteControl.h: Schnittstelle f�r die Klasse CDVBRemoteControl.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DVBREMOTECONTROL_H__8220248B_8786_49AF_9B4A_F3451276F6FB__INCLUDED_)
#define AFX_DVBREMOTECONTROL_H__8220248B_8786_49AF_9B4A_F3451276F6FB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DVBCommon.h"

// Macros for RC-5 Remote Controls
#define RC5_KEYCODE(a) (a & 0x003f)
#define RC5_DEVADDR(a) ((a >> 6) & 0x001f)
#define RC5_KEYPUSHED(a) ((a & 0x8000) ? TRUE : FALSE)
#define RC5_KEYRELEASED(a) (! RC5_KEYPUSHED(a))

// timeout in ms
#define STOP_THREAD_TIMEOUT 200

class AFX_EXT_CLASS CDVBRemoteControl
{
public:
	// supported Remote Controls
	typedef enum IR_TYPE
	{	IR_NONE,
		IR_RC5
	} IR_TYPE, *PIR_TYPE;
	
	// Key-Type for RC-5 Remote Controls	
	typedef enum _RC5_KEYCODE_TYPE
	{	RC5KEY_0         = 0x00,	RC5KEY_1         = 0x01,
		RC5KEY_2         = 0x02,	RC5KEY_3         = 0x03,
		RC5KEY_4         = 0x04,	RC5KEY_5         = 0x05,
		RC5KEY_6         = 0x06,	RC5KEY_7         = 0x07,
		RC5KEY_8         = 0x08,	RC5KEY_9         = 0x09,
		RC5KEY_TV        = 0x0F,	RC5KEY_RADIO     = 0x0C,
		RC5KEY_UP        = 0x20,	RC5KEY_LEFT      = 0x11,
		RC5KEY_RIGHT     = 0x10,	RC5KEY_DOWN      = 0x21,
		RC5KEY_FULLSCR   = 0x2E,	RC5KEY_MUTE      = 0x0D,
		RC5KEY_SOURCE    = 0x22,	RC5KEY_RESERVED  = 0x1E,
		RC5KEY_MINIMIZE  = 0x26
	} RC5_KEYCODE_TYPE, *PRC5_KEYCODE_TYPE;

	
	CDVBRemoteControl();
	virtual ~CDVBRemoteControl();

	DVB_ERROR SetRemoteControl(IR_TYPE type, WORD wRCDevAddr = (WORD)-1);
	IR_TYPE GetIR_Type(void) {return m_type;};

protected:
	// CDVBRemoteControl Overridable callback
	virtual void OnRemoteControlCode(BYTE* Buff, int len);

private:
	IR_TYPE m_type;
//	BOOL m_fIsOn;
	void* m_pAccu;
	CEvent m_Event;

	BYTE m_bFilterID;
	void TSCallback(int FilterType, BYTE FilterID, BYTE* pStart, DWORD Length, BYTE Flags);

	BOOL StartThread(int iPriority = THREAD_PRIORITY_NORMAL);
	BOOL StopThread(DWORD dwTimeout = STOP_THREAD_TIMEOUT); // in Millisekunden
	BOOL IsRunning();
	BOOL Run();

	CCriticalSection m_CriticalSection;
	volatile long m_lRunThread;
	CWinThread* m_pThread;

	static UINT WorkerThread(LPVOID pParam);
	// Eintrittsfunktion der Daten
	static void ExtCallback(void* pObj, int FilterType, BYTE FilterID, BYTE* pStart, DWORD Length, BYTE Flags);
};

#endif // !defined(AFX_DVBREMOTECONTROL_H__8220248B_8786_49AF_9B4A_F3451276F6FB__INCLUDED_)
