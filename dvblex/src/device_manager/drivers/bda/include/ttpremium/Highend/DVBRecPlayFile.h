//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     DVBRecPlayFile.h
//
//  Project(s):   ttdvbacc
//
//  Author:       GRi
//
//  Purpose:      File based Record/Playback class declaration
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: DVBRecPlayFile.h $
//   $Revision: 19 $  
//    $Modtime: 30.09.04 14:47 $
//
//   $Log: /DVB-PC/API/HighEnd/include/DVBRecPlayFile.h $
//     >>   
//     >>   19    30.09.04 16:53 J�rg
//     >>   
//     >>   18    30.09.04 12:15 Guido
//     >>   
//     >>   17    28.09.04 9:33 J�rg
//     >>   
//     >>   16    27.09.04 16:32 J�rg
//     >>   
//     >>   15    3.08.04 17:13 Sebastian
//     >>   
//     >>   14    18.10.02 15:31 Oleh
//     >>   
//     >>   13    22.05.02 16:59 Oleh
//     >>   
//     >>   12    22.05.02 16:48 Guido
//     >>   
//     >>   11    15.03.02 15:19 Oleh
//     >>   
//     >>   10    7.03.02 11:31 Oleh
//     >>   
//     >>   9     6.03.02 9:01 J�rg
//     >>   
//     >>   8     18.02.02 15:38 J�rg
//     >>   
//     >>   7     9.01.02 13:11 Oleh
//     >>   
//     >>   6     6.12.01 15:28 Oleh
//     >>   
//     >>   5     3.08.01 15:08 Patrick
//     >>   
//     >>   4     18.07.01 16:19 Guido
//     >>   
//     >>   3     18.07.01 14:41 Guido
//     >>   
//     >>   2     1.01.98 2:05 Guido
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   5     15.05.01 16:54 Guido
//     >>   
//     >>   4     15.05.01 16:30 Guido
//
///////////////////////////////////////////////////////////////////////////

// DVBRecPlayFile.h: Schnittstelle f�r die Klasse CDVBRecPlayFile.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DVBRECPLAYFILE_H__819D8DE0_0674_4F8D_9422_02A40C47D89D__INCLUDED_)
#define AFX_DVBRECPLAYFILE_H__819D8DE0_0674_4F8D_9422_02A40C47D89D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <DVBRecPlay.h>
#include <DVBAVCtrl.h>

#define	PLAYBACK	TRUE
#define	RECORD		FALSE
// timeout in ms
#define STOP_THREAD_TIMEOUT 200

#define RCVTRMBUFLEN	MAX_BLOCK_SIZE
#define SEEKBUFLEN		(2*MAX_BLOCK_SIZE)
//#define MAXFILESIZEKB	4200000
#define MAXFILESIZEKB	4190000

#define   V_A_PVAPTS_DELAY   33000    // gew�nschte Delay zwischen Video und Audio PTS
#define MAXPVAVIDBUFF		 200000

class AFX_EXT_CLASS CDVBRecPlayFile : protected CDVBRecPlay
{
public:
	typedef enum _REC_PLAY_FILE_FORMAT
	{	FAudioMp2,		// MPEG1/2-LayerII Audio-Frames (*.mp2)
		FVideoPPES,		// packetized Video-PES (*.psv)
		FAudioVideoPPES,// packetized Audio- and Video-PES (*.pva)
		FAudioVideoPS,	// Audio- and Video-Program-Stream (*.mpeg, only for Record!)
		Analog_720_576,	// encode MPEG-2 720x576 ( DVD)	 only for Record!
		Analog_640_480,	// encode MPEG-2 640x480 (SVCD)	 only for Record!
		Analog_352_288  // encode MPEG-1 352x288 ( VCD)	 only for Record!
	} REC_PLAY_FILE_FORMAT, *PREC_PLAY_FILE_FORMAT;

	typedef enum TYPE_MODUS
	{
		MODUS_PLAYBACK,
		MODUS_RECORD,
		MODUS_TIMESHIFT,
		MODUS_ANALOG

	}TYPE_MODUS, *PTYPE_MODUS;


	CDVBRecPlayFile();
	virtual ~CDVBRecPlayFile();

	BOOL  Open(const CString& FileName, REC_PLAY_FORMAT format, BOOL RecPlay); // old
	BOOL  Open(const CString& FileName, REC_PLAY_FILE_FORMAT eFileFormat, TYPE_MODUS eModus, int iFileSplitSizeKB /*-1 = No Split File*/); // new SH 08_2004
	//BOOL  Open(const CString& FileName, REC_PLAY_FILE_FORMAT format, BOOL RecPlay); // new
	//BOOL  Open(const CString& FileName, REC_PLAY_FILE_FORMAT format, BOOL RecPlay, DWORD SplitAfterKB); // newer
	//BOOL  Open(const CString& FileName, REC_PLAY_FILE_FORMAT format, BOOL RecPlay, DWORD SplitAfterKB, BOOL bRecRingBuff); // noch newer
	BOOL  Close(void);

	// allowed record/playback state transitions (functions return FALSE otherwise):
	// from Stop			to	Record and Playback
	// from Record			to	Stop
	// from Playback		to	Stop, PlaybackPause, PlaybackSlow, PlaybackFast, PlaybackPause
	// from PlaybackPause	to	Stop, Playback, PlaybackSlow, PlaybackFast
	// from PlaybackFast	to	Stop, PlaybackPause, PlaybackSlow, Playback
	// from PlaybackSlow	to	Stop, PlaybackPause, Playback, PlaybackFast

	BOOL  Playback(DWORD ms_offset = 0);
	BOOL  Record(void);
	BOOL  Stop(void);
	BOOL  PlaybackPause(void);
	BOOL  PlaybackFast(BOOL IP_I = TRUE); // TRUE: I- and P-frames; FALSE: I-frames only
	BOOL  PlaybackSlow(BYTE Repeat = 2); // Repeat: how many times a picture is repeated

	DWORD GetFileSize();

	ULONGLONG GetFilePosition();

	void  GetStatus(DWORD& TotalTime, DWORD& DataRate, DWORD& ElapsedTime, REC_PLAY_MODE& RecPlayMode, REC_PLAY_FORMAT& RecPlayFormat); // old
	void  GetStatus(DWORD& TotalTime, DWORD& DataRate, DWORD& ElapsedTime, REC_PLAY_MODE& RecPlayMode, REC_PLAY_FILE_FORMAT& RecPlayFileFormat); // new
	//Msg is sent every 500ms to hWnd if SendMsg set to TRUE
	void  SetStatusChangedMessage(HWND hWnd, UINT Msg, BOOL SendMsg);
	void PauseRecording(void);

private:

	HWND	m_hWnd;
	UINT	m_Msg;
	BOOL	m_boolSendMsg;
	DWORD	m_dwDataRate;
	DWORD	m_dwElapsedTime;
	DWORD	m_filelen;
	BOOL	m_bSplitFile;

	DWORD m_maxFileSizeKB;
//	CString m_strFileName;	now in DVBRecPlay class
	DWORD m_dwFileLen;
	DWORD m_dwTotalTime;
	DWORD m_dwStartOffset;
	CFile m_File;
	BOOL m_rp;
	WORD m_wSampleFreq;
	REC_PLAY_FORMAT m_format;
	REC_PLAY_FILE_FORMAT m_fileformat;

	BYTE m_RcvTrmBuf[RCVTRMBUFLEN];
	BYTE *m_pRcvTrmBuf;
	BYTE m_SeekBuf[SEEKBUFLEN];

	BYTE *  VidBuf;
	WORD  PackLen[MAXPVAVIDBUFF/400];		// Lange f�r gespeicherte Pakete
	DWORD PackPTS[MAXPVAVIDBUFF/400];		// PTS f�r gespeicherte Pakete
	DWORD Start,			// f�r FileWrite Paket aus (VidBuf+Start) lesen
		  End;				// neu Paket in (VidBuf+End) speichern
	WORD  FirstPack,		// das erste Paket f�r FileWrite
		  LastPack,			// LastPack-1 - das letzte gespeicherte Paket
		  CurrLast;			// das letzte gespeicherte Paket am Ende des Puffers
	bool  Jump;				// der PufferKopf ist hinter dem Schwanz
	DWORD LastA_PTS,        // der letzte bekommende in Puffer AudioPTS
		  LastV_PTS;	    // der letzte bekommende in Puffer VideoPTS


	BOOL  m_bRecRingBuff;	//Record mit RingBuffer	(TimeShift) 
	BOOL  m_bIsPause, m_bStartPauseA, m_bStartPauseV;
	BYTE  m_PVACountA, m_PVACountV;
	DWORD m_lPausePTS_A, m_lPausePTS_V, m_lNormA_PTS, m_lNormV_PTS;
	DWORD m_lLastA_PTS, m_lLastV_PTS;

	BOOL		 m_bIsRecordWDM;
	CString		 m_strFileName;
	CDVBAVControl::DEVICE_INPUT m_eInput;

	void SendStatusMessage();
	BOOL GetFileParams(CFile& file, REC_PLAY_FORMAT format, DWORD& FileLen, DWORD& TotalTime, WORD& SampleFreq);
	BOOL GetHeader(CFile& file, BYTE* buf, REC_PLAY_FORMAT format);
	BOOL GotoNextHeader(CFile& file, DWORD& fpos, REC_PLAY_FORMAT format);
	BOOL SeekNextHeader(CFile& file, DWORD& fpos, REC_PLAY_FORMAT format);
//	int SeekFileBlock(CFile& file, BYTE* buff, REC_PLAY_FORMAT format, DWORD& PlayTime, int ms_offs);
	int SeekFileBlock2(CFile& file, BYTE* buff, REC_PLAY_FORMAT format, REC_PLAY_MODE mode, DWORD& PlayTime, int ms_offs);
//	int ReadFileBlock(CFile& file, BYTE* buff, REC_PLAY_FORMAT format, DWORD& PlayTime);
	int ReadFileBlock2(CFile& file, BYTE* buff, REC_PLAY_FORMAT format, REC_PLAY_MODE mode, DWORD& PlayTime);
	int GetPVAPacket(CFile& file, BYTE* buff, REC_PLAY_FORMAT format, REC_PLAY_MODE mode, DWORD& PlayTime);
//	int GetMemBlock(BYTE* Buf, int BufLen);
//	int PutMemBlock(BYTE* Buf, int len);
//	HANDLE GetRecPlayEvent(void);

	DWORD LastAudioPTS(CFile &);
	DWORD GetPTStimeMS(BYTE *buff);

	CString GetNewFileName(int n);
	static const WORD bitrates1[16]; // MPEG1 kBit/s
	static const WORD bitrates2[16]; // MPEG2 kBit/s
	static const WORD samplerates1[4]; // MPEG1 samples/s
	static const WORD samplerates2[4]; // MPEG2 samples/s
	static const double ms_per_frame[4]; // Rahmendauer in ms

	//---------------------------------------------------------------------
	BOOL StartThread(int iPriority = THREAD_PRIORITY_NORMAL);
	BOOL StopThread(DWORD dwTimeout = STOP_THREAD_TIMEOUT); // in Millisekunden
	BOOL IsRunning();
	BOOL Run();

	CCriticalSection m_CriticalSection;
	volatile long m_lRunThread;
	CWinThread* m_pThread;

	static UINT WorkerThread(LPVOID pParam);

	//---------------------------------------------------------------------
	LONGLONG DecodePTS(BYTE *pPES);
	void GetRecordPTS(BYTE *pPES);
	void SetRecordPTS(BYTE *pPES, DWORD PTS);
	DWORD VideoBuff(BYTE * buf, DWORD len, BYTE * Buff);
	void ClearVidBuff();
};

#endif // !defined(AFX_DVBRECPLAYFILE_H__819D8DE0_0674_4F8D_9422_02A40C47D89D__INCLUDED_)
