//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     DVBAVCtrl.h
//
//  Project(s):   ttdvbacc
//
//  Author:       GRi
//
//  Purpose:      Audio-/Video-Control class 
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: DVBAVCtrl.h $
//   $Revision: 16 $  
//    $Modtime: 30.09.04 8:49 $
//
//   $Log: /DVB-PC/API/HighEnd/include/DVBAVCtrl.h $
//     >>   
//     >>   16    30.09.04 9:02 J�rg
//     >>   
//     >>   15    31.08.04 13:56 J�rg
//     >>   
//     >>   14    30.03.04 8:57 J�rg
//     >>   
//     >>   13    17.03.04 16:33 J�rg
//     >>   
//     >>   12    5.03.04 9:44 J�rg
//     >>   AC3(Octal)
//     >>   
//     >>   11    27.10.03 8:21 J�rg
//     >>   
//     >>   10    24.10.03 13:55 J�rg
//     >>   
//     >>   9     23.10.03 10:50 J�rg
//     >>   
//     >>   8     23.10.03 9:36 J�rg
//     >>   
//     >>   7     14.10.03 14:53 J�rg
//     >>   
//     >>   6     14.10.03 11:36 J�rg
//     >>   
//     >>   5     23.05.03 13:58 Guido
//     >>   
//     >>   4     19.03.03 16:40 Guido
//     >>   
//     >>   3     28.01.03 10:36 Guido
//     >>   
//     >>   2     18.07.01 14:41 Guido
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   12    15.05.01 16:29 Guido
//
///////////////////////////////////////////////////////////////////////////

// DVBAVCtrl.h: Schnittstelle f�r die Klasse CDVBAVControl.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DVBAVCTRL_H__69968730_F1E1_444F_8EE7_5D50B384315F__INCLUDED_)
#define AFX_DVBAVCTRL_H__69968730_F1E1_444F_8EE7_5D50B384315F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DVBCommon.h"


#define SB_OFF		0		//SlowBlank aus (TV-Mode)
#define SB_ON		1		//SlowBlank an  (AV-Mode)
#define SB_WIDE		0xFF	//SlowBlank 6V  (16/9-Mode) nicht realisiert
#define FB_OFF		0		//FastBlank aus (CVBS-Mode)
#define FB_ON		1		//FastBlank an  (RGB-Mode)
#define FB_LOOP		0xFF	//FastBlank der PC-Grafik durchschleifen
// switch defines
#define SWITCH_PC_TO_MAIN	TRUE	//PC-Grafik auf TV schalten
#define SWITCH_DVB_TO_MAIN	FALSE	//DVB-Video auf TV schalten

// defines f�r Hardware-Eigenschaften (GetCapabilities)
#define HAS_VOLUME	0x00000001
#define HAS_LOOP	0x00000010
#define HAS_MSP		0x00000100
#define HAS_ANALOG	0x00010000
#define HAS_SWITCH	0x00100000
#define HAS_HW_AC3	0x01000000


class CDVB7114;

class AFX_EXT_CLASS CDVBAVControl  
{
public:
	typedef enum _AB_CHANNELS
	{
		AUDIO_CHANNEL_AB,
		AUDIO_CHANNEL_A,
		AUDIO_CHANNEL_B
	} AB_CHANNELS, *PAB_CHANNELS;

	typedef enum _VIDEOOUTPUTMODE
	{
		NO_OUT		 =	0,		//disable analog Output
		CVBS_RGB_OUT =	1,
		CVBS_YC_OUT	 =	2,
		YC_OUT		 =	3
	} VIDEOOUTPUTMODE, *PVIDEOOUTPUTMODE;

	typedef enum _TYPE_MONITOR_AR
	{
		_MONITOR_4_3,
		_MONITOR_16_9
	} TYPE_MONITOR_AR, *PTYPE_MONITOR_AR;

	typedef enum _TYPE_STRETCH_MODE // bei 16:9 im 4:3 Bild
	{
		_STRETCH_NONE,		// keine Skalierung (=16:9)
		_STRETCH_PAN_SCAN,	// Pan Scan
		_STRETCH_LETTER_BOX, // Letter Box
	} TYPE_STRETCH_MODE, *PTYPE_STRETCH_MODE;

	typedef enum _VIDEOTESTMODE
	{
		TEST_BAR_OFF =	0,
		TEST_BAR_1	 =	1,
		TEST_BAR_2	 =	2
	} VIDEOTESTMODE, *PVIDEOTESTMODE;

	typedef enum _VIDEOSTANDARD
	{
		VS_PAL_MODE			= 0, // normal PAL 68
		VS_NTSC_MODE		= 1, // normal NTSC 68
		VS_AUTO_MODE		= 2, // ergibt VS_PAL_MODE oder VS_NTSC_MODE (nicht VS_PAL_M_MODE)
		VS_PAL_MODE_46		= 3, // selbsterkl�rend...
		VS_NTSC_MODE_46		= 4,
		VS_PAL_MODE_64		= 5,
		VS_NTSC_MODE_64		= 6,
		VS_PAL_MODE_68		= 7,
		VS_NTSC_MODE_68		= 8,
		VS_LAST_MODE		= 0xFF	// aktuelle Version wird nachgeladen, harter Reset des Videodecoders
	} VIDEOSTANDARD, *PVIDEOSTANDARD;

	typedef enum _MSPSound
	{
		MSP_DETECT_MONO = 0,
		MSP_DETECT_STEREO,
		MSP_DETECT_DUAL,
		MSP_MONO = 0,
		MSP_STEREO,
		MSP_LEFT,
		MSP_RIGHT
	} MSPSound;

	typedef enum _MSPSoundStd //_MSPStandard
	{
		MSP_SSTD_PAL_BG_GER,	//MSP_PAL_BG
		MSP_SSTD_PAL_UK,		//MSP_PAL_I
		MSP_SSTD_PAL_DK,		//MSP_PAL_DK
		MSP_SSTD_SECAM_FR,		//MSP_SECAM_L
		MSP_SSTD_PAL_MN,		//MSP_PAL_MN
		MSP_SSTD_PAL_BG_SCAND,	//MSP_PAL_BH
		MSP_SSTD_SECAM_USSR,
		MSP_SSTD_MTSC_US,
		MSP_SSTD_PAL_ASTRA_M,
		MSP_SSTD_PAL_ASTRA_S
	} MSPSoundStd; //MSPStandard


	typedef enum
	{
		INPUT_ANALOG_TUNER,		//old #define SWITCH_ANALOG_OUT		FALSE	//Analog-TV am Videoausgang bzw. auf TV (nur wenn 2. Switch best�ckt)
		INPUT_COMPOSITE_JP1,	//new JP1
		INPUT_COMPOSITE_EXT,	//new ext. Slot 
		INPUT_COMPOSITE_AUX,	//new ext. Slot 
		INPUT_XPORT_DVB,		//old #define SWITCH_DVB_OUT		TRUE	//DVB am Videoausgang bzw. auf TV
	} DEVICE_INPUT;

	struct _VIDEOSTATE;
	typedef _VIDEOSTATE VIDEOSTATE, *PVIDEOSTATE;

	CDVBAVControl();
	virtual ~CDVBAVControl();

	DVB_ERROR Init(BOOL bOctalVideoPort = FALSE);
	// Eigenschaften der Hardware abfragen
	DWORD GetCapabilities(void) {return m_Capabilities;};
	// alle notwendigen PIDs einstellen
	DVB_ERROR SetPIDs(WORD wAudPid, WORD wVidPid, WORD wPcrPid = 0);

	DVB_ERROR GetSTC(BYTE& bSTCHighestBit, DWORD& dwSTC);

	// -------------------Audio-Part---------------------------------
	
	// Aufruf immer m�glich
	DVB_ERROR SetAudioPID(WORD wAudPid);
	DVB_ERROR SetAudioMute(BOOL bMute);
	DVB_ERROR SetAudioChannel(AB_CHANNELS AudioChannel);
	AB_CHANNELS GetAudioChannel(void){return m_AudioChannel;};
	WORD GetAudioPID(void) {return m_wAudioPID;};
	DVB_ERROR GetAudioDecoderStatus(BYTE& bufstatus, DWORD& audiostatus); //bufstatus == Audio buffer fullness in percent
	
	// Aufruf nur m�glich wenn (GetCapabilities() & HAS_VOLUME) == TRUE
	DVB_ERROR SetVolume(BYTE nVolLeft, BYTE nVolRight); // 0..100
	
	// Aufruf nur m�glich wenn (GetCapabilities() & HAS_LOOP) == TRUE
	DVB_ERROR LoopCDAudio(BOOL bLoop);
	
	// Aufruf nur m�glich wenn (GetCapabilities() & HAS_MSP) == TRUE
	DVB_ERROR SetTrebleBass(int iTreble, int iBass); // -50..50
	DVB_ERROR SetAudioWide(BOOL bWide);
	DVB_ERROR ReadStereo(MSPSound& Sound);
	DVB_ERROR SetStereo(MSPSound Sound);
	DVB_ERROR SetStandard(MSPSoundStd SoundStd);
	
	DVB_ERROR SetAC3PID(WORD wAC3Pid);//J.R. with SetArmRegister


	// -------------------Video-Part---------------------------------

	// Aufruf immer m�glich
	DVB_ERROR SetVideoPID(WORD wVidPid);
	DVB_ERROR SetVideoOutputMode(VIDEOOUTPUTMODE mode);
	DVB_ERROR SetVideoTestMode(VIDEOTESTMODE mode);
	DVB_ERROR SetVideoStandard(VIDEOSTANDARD mode);
	DVB_ERROR SetAspectRatio(TYPE_MONITOR_AR mode);
	DVB_ERROR SetPrefStretchMode(TYPE_STRETCH_MODE mode);
	VIDEOSTANDARD GetVideoStandard(void) {return m_VideoStandard;};
	WORD GetVideoPID(void) {return m_wVideoPID;};
	WORD GetPCRPID(void) {return m_wPCRPID;};
	DVB_ERROR GetVideoDecoderStatus(VIDEOSTATE& videostatus);

	// Aufruf nur m�glich wenn (GetCapabilities() & HAS_ANALOG) == TRUE
	DVB_ERROR SetADSwitch(DEVICE_INPUT eInput); // Analog/Digital TV-Switch
	static BOOL		 GetAnalog(void);
	static void*	 m_this;
	static DVB_ERROR GetADSwitchExt(DEVICE_INPUT& rInput);
	static DVB_ERROR SetADSwitchExt(DEVICE_INPUT  eInput);

	//BOOL IsAnalogStatusLocked(void);


	// Aufruf nur m�glich wenn (GetCapabilities() & HAS_SWITCH) == TRUE
	DVB_ERROR SetSlowBlank(BYTE sb);
	DVB_ERROR SetFastBlank(BYTE fb);
	DVB_ERROR SetMainSwitch(BOOL ms);

	// Helligkeit, Kontrast, Farbs�ttigung (jeweils 0..255)
	DVB_ERROR SetPictureParams(BYTE nBrightness, BYTE nContrast, BYTE nSaturation);

private:
	typedef enum _MSPInput
	{
		MSP_IN_SCART1,
		MSP_IN_SCART2,
		MSP_IN_SCART3,
		MSP_IN_MONO,
		MSP_IN_FM,
		MSP_IN_STEREO
	} MSPInput;

	typedef enum _MSPOutput
	{
		MSP_OUT_SCART1,
		MSP_OUT_SCART2,
		MSP_OUT_SPEAKER
	} MSPOutput;
	
	typedef enum _DACTYPE
	{
		DAC_NONE,
		DAC_TI_AD80,
		DAC_MSP3400,
		DAC_CS4341
	} DACTYPE;
	
	DACTYPE		 m_DACType;
	DEVICE_INPUT m_eInput;
	
	DVB_ERROR InitTI_DAC(void);
	DVB_ERROR SetVolumeTI_DAC(BYTE nVolLeft, BYTE nVolRight);
	DVB_ERROR SetAudioMuteTI_DAC(BOOL bMute);
	DVB_ERROR LoopCDAudioTI_DAC(BOOL bLoop);
	DVB_ERROR SendTI_DAC(BYTE addr, BYTE data);

	DVB_ERROR InitCS4341();
	DVB_ERROR SetVolumeCS4341(BYTE nVolLeft, BYTE nVolRight);
	DVB_ERROR SetAudioMuteCS4341(BOOL bMute);
	int WriteCS4341(WORD Addr, WORD Len, int *pData);

	DVB_ERROR InitMSP3400();
	DVB_ERROR ResetMSP3400(BOOL bAssert);
	DVB_ERROR SetVolumeMSP3400(int iVolume, int iBalance);
	DVB_ERROR SetAudioMuteMSP3400(BOOL bMute);
	static DVB_ERROR WriteMSP3400(int part, BYTE* data);
	static DVB_ERROR ReadMSP3400(int part, BYTE* data);
	DVB_ERROR ReadMSP3400RomCode(DWORD& m_dwMSPRomCode);
	DVB_ERROR HasMSP34xx();
	static DVB_ERROR DetectAnalogAudioStandard(void);
	DVB_ERROR LoopSpdif(BOOL loop);
	DVB_ERROR GetDCOIncrement(DWORD frequ, DWORD& DCO_HI, DWORD& DCO_LO);
	DVB_ERROR GetDCOVal(MSPSoundStd TVStandard, BYTE* DCO_Coeff);
	DVB_ERROR SetChannel(MSPInput Input, MSPOutput Output);
	BYTE m_nVolume;
	WORD m_nScartSwitch;
	MSPSound m_Sound;
	BOOL m_bLoopSpdif;
	static CDVB7114 m_pDVB7114;

	DVB_ERROR SetHSync(WORD wTimming);

	BOOL m_bHasMSP3400;
	DWORD m_dwMSPRomCode;
	BOOL m_bHasCS4341;
	BYTE m_nLastVolLeft, m_nLastVolRight;
	WORD m_wAudioPID, m_wVideoPID, m_wPCRPID, m_wAC3PID;
	VIDEOSTANDARD m_VideoStandard;
	AB_CHANNELS m_AudioChannel;
	DWORD m_Capabilities;
	static BOOL m_bAnalog;
	BOOL m_bSNI;
	BOOL m_bOctalCable;
	BOOL m_bOctalSat;
	BOOL m_bPrem23;
};


struct CDVBAVControl::_VIDEOSTATE
{
	WORD	ProcessingState;		//0
	WORD	CommandID;
	WORD	dummy1;					//2
	WORD	dummy2;
	WORD	RateBuffFullness;		//4
	WORD	dummy3;
	WORD	BytesDecoded;			//6
	WORD	SkippedPictures;
	WORD	RepeatedPictures;		//8
	WORD	MostRecentPTS;
	WORD	LastPicture;			//10
	WORD	InitDone;
	WORD	FreezeIndex;			//12
	WORD	FindIndex;
	WORD	DistanceI;				//14
	WORD	ThresholdPTS;
	WORD	dummy4;					//16
	WORD	dummy5;
	WORD	DisablePTSFilt;			//18
	WORD	HSize;
	WORD	VSize;					//20
	WORD	AspectRatio;
	WORD	FrameRate;				//22
	WORD	BitRate;
	WORD	VBVBuffSize;			//24
	WORD	ConstParamFlag;
	WORD	IntraQ;					//26
	WORD	NonIntraQ;
	WORD	FrameInterval;			//28
	WORD	HeaderBackup;
	WORD	RedHeaderFlag;			//30
	WORD	SeqExtension;
	WORD	GOPTimeCode1;			//32
	WORD	GOPTimeCode2;
	WORD	ClosedGOP;				//34
	WORD	PICHeadTempRef;
	WORD	SegHeaderExt;			//36
	WORD	ColorDesc;
	WORD	ColorPrim;				//38
	WORD	TransferChar;
	WORD	MatrixCoeff;			//40
	WORD	DisplayHSize;
	WORD	DisplayVSize;			//42
	WORD	GOPHeader;
	WORD	TimeCodeW1;				//44
	WORD	TimeCodeW2;
};


#endif // !defined(AFX_DVBAVCTRL_H__69968730_F1E1_444F_8EE7_5D50B384315F__INCLUDED_)
