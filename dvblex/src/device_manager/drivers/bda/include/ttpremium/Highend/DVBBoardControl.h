//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     DVBBoardControl.h
//
//  Project(s):   ttdvbacc
//
//  Author:       GRi
//
//  Purpose:      Hardware-Control class
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: DVBBoardControl.h $
//   $Revision: 15 $  
//    $Modtime: 10.09.04 15:44 $
//
//   $Log: /DVB-PC/API/HighEnd/include/DVBBoardControl.h $
//     >>   
//     >>   15    10.09.04 16:19 Guido
//     >>   
//     >>   14    6.09.04 11:09 Guido
//     >>   
//     >>   13    1.06.04 18:38 Guido
//     >>   
//     >>   12    23.03.04 15:09 Sebastian
//     >>   
//     >>   11    1.07.03 10:43 Sebastian
//     >>   
//     >>   10    30.01.03 14:47 Sebastian
//     >>   
//     >>   9     20.09.02 9:12 Guido
//     >>   
//     >>   8     18.09.02 17:38 Sebastian
//     >>   
//     >>   7     18.09.02 12:02 Sebastian
//     >>   
//     >>   6     23.05.02 14:22 J�rg
//     >>   
//     >>   5     22.05.02 16:55 Guido
//     >>   
//     >>   4     30.04.02 10:44 Guido
//     >>   
//     >>   3     7.11.01 15:54 Guido
//     >>   
//     >>   2     18.07.01 14:41 Guido
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   8     15.05.01 16:39 Guido
//     >>   
//     >>   7     15.05.01 16:30 Guido
//
///////////////////////////////////////////////////////////////////////////

// DVBBoardControl.h: interface for the CDVBBoardControl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BOARDCONTROL_H__3191AE5F_2648_11D4_B88F_00105A22770E__INCLUDED_)
#define AFX_BOARDCONTROL_H__3191AE5F_2648_11D4_B88F_00105A22770E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DVBCommon.h"

#define DVB_PACKET_DIRECTED           0x0001
#define DVB_PACKET_MULTICAST          0x0002
#define DVB_PACKET_ALL_MULTICAST      0x0004
#define DVB_PACKET_BROADCAST          0x0008
#define DVB_PACKET_SOURCE_ROUTING     0x0010
#define DVB_PACKET_PROMISCUOUS        0x0020
#define DVB_PACKET_SMT                0x0040
#define DVB_PACKET_MAC_FRAME          0x8000
#define DVB_PACKET_FUNCTIONAL         0x4000
#define DVB_PACKET_ALL_FUNCTIONAL     0x2000
#define DVB_PACKET_GROUP              0x1000

#define MULTICASTLISTMAX 64
#define LENGTH_OF_ETH_ADDRESS 6

//Version Control
#define VER_VALID_DLL	0x1800
//#define VER_VALID_DRV	0x1706 // obsolete, use GetValidDrvVersion() instead!
#define VER_VALID_NET	0x1900


class AFX_EXT_CLASS CDVBBoardControl
{
public:
	typedef struct _BE_VERSION
	{	DWORD	Firmware;		//Firmware
		DWORD	RTSLib;			//Runtime Support Library
		DWORD	VideoDec;		//Video Microcode
		char	CompDate[16];	//Compilation Date
		char	CompTime[10];	//Compilation Time
	} BE_VERSION, *PBE_VERSION; //Backend Versionsstruct

	typedef struct _MULTICAST_LIST
	{
		UINT MulticastListMax;
		UINT MulticastListInUse;
		UINT PacketFilter; // Flags (DVB_PACKET_...)
		BYTE AddressList[MULTICASTLISTMAX][LENGTH_OF_ETH_ADDRESS];
	} MULTICAST_LIST, *PMULTICAST_LIST;
	
	CDVBBoardControl();
	virtual ~CDVBBoardControl();

	DVB_ERROR ResetInit(void);
	DVB_ERROR EnableDataDMA(BOOL bEnable);

	DVB_ERROR GetMulticastList(MULTICAST_LIST& MCList);
	DVB_ERROR GetMACAddress(DWORD& dwEEPromOID, DWORD& dwEEPromLow3Bytes);
	DVB_ERROR Get_IP_SM_BP(DWORD& dwIP, DWORD& dwSM, WORD& wBasePort);
	
	DVB_ERROR BootARM(CString* path);
	DVB_ERROR GetBEVersion(BE_VERSION& BEVersion);
	DVB_ERROR GetSubDeviceIDs(WORD& wVendorID, WORD& wDeviceID);
	DVB_ERROR GetVersions(DWORD& dwDllVer, DWORD& dwDrvVer, DWORD& dwNwDrvVer);
	DVB_ERROR Saa7146RegDump(DWORD* RegBuf = NULL); // 80 DWORDs
	void ResetAllSlaveFilters(void);

	DWORD GetValidDrvVersion(void);

// tempor�r exportiert
	typedef enum _VIDEO_CAP_MODE
	{	CAP_DVB_PAL = 0, CAP_DVB_NTSC = 1, CAP_ANALOG_PAL = 2, CAP_ANALOG_NTSC = 3
	} VIDEO_CAP_MODE;
	BOOL SetVideoCaptureInputParams(VIDEO_CAP_MODE mode = CAP_DVB_PAL);
	BOOL SetVideoCaptureInputParamsExt(VIDEO_CAP_MODE mode = CAP_DVB_PAL,
											  int nStartX = 64, int nWidth  = 768,
											  int nStartY = 24, int nHeight = 288);


	DVB_ERROR BootARMnew(CString *path); // for use with new Boot-File-Type only
private:
	BOOL TerminateBootloader();
	BOOL DownloadImage(CString FileName, DWORD DestAddr);
	BOOL LoadImageToDpram(CString FileName);
	void ResetBoard(BOOL reset);
	void WordSwap(WORD *pWord, int Length);
	void WriteDebiBlock(WORD *pSource, WORD Dest, int Length);

};

#endif // !defined(AFX_BOARDCONTROL_H__3191AE5F_2648_11D4_B88F_00105A22770E__INCLUDED_)
