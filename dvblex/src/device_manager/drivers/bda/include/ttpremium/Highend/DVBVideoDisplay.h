//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     DVBVideoDisplay.h
//
//  Project(s):   ttdvbacc
//
//  Author:       JRe, GRi
//
//  Purpose:      Video-Display class
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: DVBVideoDisplay.h $
//   $Revision: 18 $  
//    $Modtime: 1.02.05 16:58 $
//
//   $Log: /DVB-PC/API/HighEnd/include/DVBVideoDisplay.h $
//     >>   
//     >>   18    1.02.05 17:24 Sebastian
//     >>   
//     >>   17    1.02.05 15:44 Guido
//     >>   
//     >>   16    1.02.05 14:42 J�rg
//     >>   
//     >>   15    4.10.04 13:23 J�rg
//     >>   
//     >>   14    29.09.04 14:34 J�rg
//     >>   
//     >>   13    16.09.04 9:00 J�rg
//     >>   
//     >>   12    30.03.04 11:55 Sebastian
//     >>   
//     >>   11    23.10.03 10:50 J�rg
//     >>   
//     >>   10    22.10.03 11:53 J�rg
//     >>   
//     >>   9     14.10.03 16:38 J�rg
//     >>   
//     >>   8     1.10.03 16:25 Sebastian
//     >>   
//     >>   7     4.12.02 8:25 J�rg
//     >>   
//     >>   6     20.12.01 10:46 J�rg
//     >>   neuer Schl�ssel OddEven
//     >>   
//     >>   5     21.08.01 14:05 J�rg
//     >>   
//     >>   4     17.08.01 12:05 J�rg
//     >>   
//     >>   3     18.07.01 14:41 Guido
//     >>   
//     >>   2     7.06.01 17:08 J�rg
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   17    15.05.01 16:54 Guido
//     >>   
//     >>   16    15.05.01 16:30 Guido
//
///////////////////////////////////////////////////////////////////////////

// DVBVideoDisplay.h: Schnittstelle f�r die Klasse CDVBVideoDisplay.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VIDEODISPLAY_H__A9C0946E_60DB_4FEC_BAAF_ED521B6A838A__INCLUDED_)
#define AFX_VIDEODISPLAY_H__A9C0946E_60DB_4FEC_BAAF_ED521B6A838A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DVBCommon.h"

#define NORM_MAX	3

class AFX_EXT_CLASS CDVBVideoDisplay  
{
public:
	typedef enum DD_MODE  //nc WDM-KS
	{	MODE_AUTO,
		MODE_OVERLAY,
		MODE_SURFACE,
		MODE_INLAY	//only FullScreen 
	};

	typedef struct DD_VGA
	{	DWORD dwHeight;
		DWORD dwWidth;
		DWORD dwRGBBitCount;
		DWORD dwRefreshRate;
	} DD_VGA;

	typedef enum TV_NORM
	{	NORM_PAL,
		NORM_NTSC,
		NORM_ANALOG
	};

	typedef enum TV_RATIO
	{	RATIO_FREE,
		RATIO_4_3,
		RATIO_16_9
	};

	typedef enum TV_COLOR
	{	COLOR_YUV   = 0x000,
		COLOR_RGB15 = 0x213,	
		COLOR_RGB16 = 0x210,
		COLOR_RGB32 = 0x202, 
		COLOR_RGB24 = 0x201
	};

	typedef enum RENDER_MODE   //only WDM-KS
	{	RENDER_DEFAULT,
		RENDER_OVERLAY,
		RENDER_VMR7,
		RENDER_VMR9
	};

public:
	CDVBVideoDisplay(BOOL bExtraGraph = FALSE); // FALSE: don't build graph with MPEG-Encoder filters (standard)
	virtual ~CDVBVideoDisplay();

	int		  EnumAnalogCaptureDevices(LPSTR  szBuffer, int iBuffSize);
static void	  SetAnalogCaptureDevices (LPCSTR szAnalogCapture);
static LPCSTR GetAnalogCaptureDevices (void);


	HWND	Create (HWND hWndParent, int iX, int iY, int iWidth, int iHeight, 
				DWORD dwStyle=WS_POPUP, BOOL bFullScreen=FALSE, RENDER_MODE eRenderMode=RENDER_VMR7,
				DD_MODE eDDMode=MODE_AUTO, TV_NORM eTVNorm=NORM_PAL, BOOL bOddEven=FALSE);
		
	void	Close    (void);

	HWND	GetWnd   (void);
	HWND	GetParent(void);
		
	BOOL	SetTVNormValues (TV_NORM eTVNorm, WORD wOffsetX, WORD wBorderX, WORD wOffsetY, WORD wBorderY);
	BOOL	GetDDMode (DD_MODE& rDDMode);
	BOOL	GetVGA	  (DD_VGA&	rVGA);

	void	SetRatio		(TV_RATIO eRatio);
	BOOL	GetRatio       (TV_RATIO& rRatio);
	void	SetMove			(BOOL bMove);
	void	SetShowStatus	(BOOL bShow);
	void	SetBOB			(BOOL bBOB);
	void	SetMAX			(BOOL bMAX);

	void	SetCaption   (void);
	void	PurgeCaption (void);

	void	SetFriendWindow(HWND hWndFriend);
		
	DVB_ERROR SetPictureParams(BYTE  nBrightness, BYTE  nContrast, BYTE  nSaturation);
	DVB_ERROR GetPictureParams(BYTE& nBrightness, BYTE& nContrast, BYTE& nSaturation);

	void*	GetDirectDrawPointer (void);
	
	//SAA direct internal 
	BOOL	InitSAA(CDVBVideoDisplay::TV_COLOR eGrabMode, CDVBVideoDisplay::TV_NORM eTVNorm);
	DWORD	GetMemPhysAddress(PVOID LinAddr);
	BOOL	GetEventName(char* szEvent, int iSize);
	int     SetSAAValues (DWORD dwAddr, int iX, int iY, int iPitch);
	BOOL	ExitSAA();
	//SAA direct internal

static	void*	m_this;
static	BOOL	StartStopRecord (LPCSTR szRecordFileName);

private:
	void*	m_pDDvideo;
	void*	m_pWDMvideo;
	BOOL	m_bExtraGraph;
};


#endif // !defined(AFX_VIDEODISPLAY_H__A9C0946E_60DB_4FEC_BAAF_ED521B6A838A__INCLUDED_)
