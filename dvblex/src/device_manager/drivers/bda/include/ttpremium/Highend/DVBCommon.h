//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     DVBCommon.h
//
//  Project(s):   ttdvbacc
//
//  Author:       GRi
//
//  Purpose:      Driver-/Device-Control class
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: DVBCommon.h $
//   $Revision: 5 $  
//    $Modtime: 17.12.04 12:40 $
//
//   $Log: /DVB-PC/API/HighEnd/include/DVBCommon.h $
//     >>   
//     >>   5     17.12.04 13:50 J�rg
//     >>   
//     >>   4     13.06.03 12:13 Sebastian
//     >>   
//     >>   3     15.10.01 14:45 Guido
//     >>   
//     >>   2     18.07.01 14:41 Guido
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   4     15.05.01 16:39 Guido
//     >>   
//     >>   3     15.05.01 16:30 Guido
//
///////////////////////////////////////////////////////////////////////////

// DVBCommon.h: interface for the CDVBCommon class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMMON_H__0E929338_258F_11D4_B88E_00105A22770E__INCLUDED_)
#define AFX_COMMON_H__0E929338_258F_11D4_B88E_00105A22770E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


//--- Common return value type
typedef enum
{
	DVB_ERR_NONE,		// kein Fehler
	DVB_ERR_DRIVER,		// Treiberzugriffsfehler
	DVB_ERR_HARDWARE,	// Hardware-Fehler
	DVB_ERR_PARAMETER,	// ung�ltiger Parameter
	DVB_ERR_TIMEOUT,	// Timeout
	DVB_ERR_STATE,		// Zustandsfehler
	DVB_ERR_RESOURCES,	// Mangel an Ressourcen
	DVB_ERR_GENERAL		// allgemeiner Fehler
} DVB_ERROR;

class AFX_EXT_CLASS CDVBCommon
{
public:
    //--- Device-Driver open and close --------------------
	static WORD   GetNumberOfDevices(void);
	static BOOL   GetDeviceInfo(WORD wDevIdx, CString& Name, CString& MAC, CString& IP);

	static BOOL   OpenDevice(WORD wDevIdx, LPCTSTR AppName = NULL, BOOL NoNetwork = FALSE);
	static void   CloseDevice(void);
	static BOOL   IsOpen(void);
	static BOOL   IsDrvWDM(void);

	static WORD   GetActDeviceIdx(void);
	static HANDLE GetActDeviceHandle(void);

	// --- optional functions for handling several applications/devices
	static BOOL	   GetCurrentDevApp (WORD wDevIdx, LPCTSTR& AppPath, LPCTSTR& AppName);
	static BOOL	   CloseCurrentApp(WORD wDevIdx, DWORD dwTimeout = 5000); // 5 Seconds
	static LPCTSTR GetAppCloseMessage(void);
	static BOOL	   GetDeviceRegistryPath(BOOL bFromNet, CString& sRegPath);

protected:
	static HANDLE	m_hDevice;
	static WORD		m_wDevIdx;

private:
	static void CheckChangeMAC(WORD wDevIdx);
	static int GetWinSysVer(void);
	static BOOL OpenDriver(const char* csDriver);
	
};

#endif // !defined(AFX_COMMON_H__0E929338_258F_11D4_B88E_00105A22770E__INCLUDED_)
