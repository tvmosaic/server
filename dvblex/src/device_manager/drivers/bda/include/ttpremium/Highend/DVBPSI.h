//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     DVBPSI.h
//
//  Project(s):   ttdvbacc
//
//  Author:       GRi
//
//  Purpose:      Program-Service-Information class
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: DVBPSI.h $
//   $Revision: 2 $  
//    $Modtime: 17.07.01 16:40 $
//
//   $Log: /DVB-PC/API/HighEnd/include/DVBPSI.h $
//     >>   
//     >>   2     18.07.01 14:41 Guido
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   4     15.05.01 16:30 Guido
//
///////////////////////////////////////////////////////////////////////////

// DVBPsi.h: Schnittstelle f�r die Klasse CDVBPsi.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DVBPSI_H__6C354ADA_8932_47D0_AB93_2DD02431FEB5__INCLUDED_)
#define AFX_DVBPSI_H__6C354ADA_8932_47D0_AB93_2DD02431FEB5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DVBCommon.h"

// timeout in ms
#define STOP_THREAD_TIMEOUT 200

// class for DVB programm service information
// Do not use this class and the CDVBComnIF class at the same time!
class AFX_EXT_CLASS CDVBPsi  
{
public:
	typedef enum _typ_PSI_STATE
	{
		PSI_STATUS_STOP				= 0,
		PSI_STATUS_DECODING			= 1,
		PSI_STATUS_READY_COMPLETE	= 2,
		PSI_STATUS_READY_INCOMPLETE	= 3,
		PSI_STATUS_ERROR			= 4
	} typ_PSI_STATE;

	typedef struct _typ_IF_AudioEntry
	{
		WORD wAudioPID;
		char czLanguageCode[4]; // ISO 639
		WORD wStuffing;
	} typ_IF_AudioEntry, *typ_pIF_AudioEntry;
	
	typedef struct _typ_IF_ProgramTableEntry // struct for service information
	{
		char czServiceLabel[32];
		char czProviderLabel[32];
		WORD wProgramNumber;
		WORD wVideoPID;
		WORD wTeletextPID;
		WORD wAC3_PID;
		BYTE nServiceType;			// see document EN 300 468 service descriptor
		BYTE nServiceStatus;		// EIT_schedule_flag (1Bit) + EIT_present_following_flag (1Bit)
									// + Running status (3Bit) + free_CA_mode (1Bit)
		WORD wPCR_PID;
		WORD wAPIDTabSize;			// number of valid entries in the audio PID table
		WORD wStuffing;
		typ_IF_AudioEntry APIDTab[32]; // audio PID table
	} typ_IF_ProgramTableEntry, *typ_pIF_ProgramTableEntry;

	typedef struct _typ_IF_TS_Infos // service list struct
	{
		WORD wNoOfPrograms;		// number of services found in the actual transport stream
		WORD wOriginalNetworkID;
		WORD wNetworkID;
		WORD wTransportstreamID;
	} typ_IF_TS_Infos, *typ_pIF_TS_Infos;

	CDVBPsi();
	virtual ~CDVBPsi();

	// open class for use
	DVB_ERROR Open();
	DVB_ERROR Close();
	
	// starts a transponder/channel PSI scan; returns after scan, 
	// after timeout or immediately if bBlockingMode is set to FALSE
	typ_PSI_STATE Start(WORD PNR = 0, BOOL bBlockingMode = TRUE);
	// stops a running transponder/channel PSI scan
	DVB_ERROR Stop(BOOL bClear = FALSE);
	// returns status of scanning process (e.g. if bBlockingMode is set to FALSE)
	typ_PSI_STATE GetScanningState(void);

	// picks up the transponder info after a scan
	DVB_ERROR GetTsInfos(typ_IF_TS_Infos& TSInfos);
	// picks up the programme info after a scan
	DVB_ERROR GetPrgInfos(WORD wIndex, typ_IF_ProgramTableEntry& PTEntry);

	// removes control codes from the label strings or returns the short name;
	// returns the length of pOutString
	WORD FormatNameString(char* pInString, char* pOutString, BOOL bReturnShortName = FALSE);

private:
	typedef enum _typ_PSI_COMMAND
	{
		PSI_START_DECODING	= 0,
		PSI_STOP_DECODING	= 1,
		PSI_GET_STATUS		= 2,
		PSI_GET_TS_INFO		= 3,
		PSI_GET_PRG_INFO	= 4
	} typ_PSI_COMMAND;

	void ProcessSrvData(BYTE* Buff, int len); // handler for the incoming service data
	DVB_ERROR SendSdCommand(BYTE SubCommand, BYTE *Params, BYTE ParamLen,WORD &wRetVal);

	WORD	m_wNoOfServices;
	CEvent m_TSEvent, m_PTEvent;
	typ_IF_TS_Infos m_TSI;
	typ_IF_ProgramTableEntry m_PTE;

private:
	void* m_pAccu;
	CEvent m_Event;

	BYTE m_bFilterID;
	void TSCallback(int FilterType, BYTE FilterID, BYTE* pStart, DWORD Length, BYTE Flags);

	BOOL StartThread(int iPriority = THREAD_PRIORITY_NORMAL);
	BOOL StopThread(DWORD dwTimeout = STOP_THREAD_TIMEOUT); // in Millisekunden
	BOOL IsRunning();
	BOOL Run();

	CCriticalSection m_CriticalSection;
	volatile long m_lRunThread;
	CWinThread* m_pThread;

	static UINT WorkerThread(LPVOID pParam);
	// Eintrittsfunktion der Daten
	static void ExtCallback(void* pObj, int FilterType, BYTE FilterID, BYTE* pStart, DWORD Length, BYTE Flags);
};

#endif // !defined(AFX_DVBPSI_H__6C354ADA_8932_47D0_AB93_2DD02431FEB5__INCLUDED_)
