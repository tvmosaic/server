//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2003
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     USBRemoteControl.h
//
//  Project(s):   ttdvbacc
//
//  Author:       GRi
//
//  Purpose:      IR-Remote-Control class
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

// USBRemoteControl.h: Schnittstelle f�r die Klasse CUSBRemoteControl.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_USBRemoteControl_H__4AD9B3CF_44AB_4961_AB16_FB52B36ED825__INCLUDED_)
#define AFX_USBRemoteControl_H__4AD9B3CF_44AB_4961_AB16_FB52B36ED825__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DVBCommon.h"

// Macros for RC-5 Remote Controls 
// Valid for OnRemoteControlCode and OnRC5Code.
#define RC5_KEYCODE(a) (a & 0x003f)
#define RC5_DEVADDR(a) ((a >> 6) & 0x001f)
// Valid for OnRemoteControlCode only.
#define RC5_KEYPUSHED(a) ((a & 0x8000) ? TRUE : FALSE)
#define RC5_KEYRELEASED(a) (! RC5_KEYPUSHED(a))
// Valid for OnRC5Code only.
#define RC5_EXTKEYCODE(a)	((a & 0x003f) | (((a >> 6) & 0x0040) ^ 0x0040))
#define RC5_TOGGLEBIT(a)	((a >> 11) & 0x0001)

// timeout in ms
#define STOP_THREAD_TIMEOUT 200

class AFX_EXT_CLASS CUSBRemoteControl
{
public:
	// supported Remote Controls
	typedef enum IR_TYPE
	{	IR_NONE,
		IR_RC5
	} IR_TYPE, *PIR_TYPE;
	
	// Key-Type for RC-5 Remote Controls	
	typedef enum _RC5_KEYCODE_TYPE
	{	RC5KEY_0         = 0x00,	RC5KEY_1         = 0x01,
		RC5KEY_2         = 0x02,	RC5KEY_3         = 0x03,
		RC5KEY_4         = 0x04,	RC5KEY_5         = 0x05,
		RC5KEY_6         = 0x06,	RC5KEY_7         = 0x07,
		RC5KEY_8         = 0x08,	RC5KEY_9         = 0x09,
		RC5KEY_TV        = 0x0F,	RC5KEY_RADIO     = 0x0C,
		RC5KEY_UP        = 0x20,	RC5KEY_LEFT      = 0x11,
		RC5KEY_RIGHT     = 0x10,	RC5KEY_DOWN      = 0x21,
		RC5KEY_FULLSCR   = 0x2E,	RC5KEY_MUTE      = 0x0D,
		RC5KEY_SOURCE    = 0x22,	RC5KEY_RESERVED  = 0x1E,
		RC5KEY_MINIMIZE  = 0x26
	} RC5_KEYCODE_TYPE, *PRC5_KEYCODE_TYPE;

	
	CUSBRemoteControl();
	virtual ~CUSBRemoteControl();

	DVB_ERROR SetRemoteControl(IR_TYPE type, WORD wRCDevAddr = (WORD)-1);
	IR_TYPE GetIR_Type(void) {return m_type;};

protected:
	// CUSBRemoteControl Overridable callback
	virtual void OnRemoteControlCode(BYTE* Buff, int len);
	
	// unbearbeiteter RC5-Code
	virtual void OnRC5Code(WORD code);

private:
	IR_TYPE m_type;
	CEvent m_Event;

	//BOOL SetupIREvent(BOOL bInstall);
	BOOL StartThread(int iPriority = THREAD_PRIORITY_NORMAL);
	BOOL StopThread(DWORD dwTimeout = STOP_THREAD_TIMEOUT); // in Millisekunden
	BOOL IsRunning();
	BOOL Run();

	CCriticalSection m_CriticalSection;
	volatile long m_lRunThread;
	CWinThread* m_pThread;

	static UINT WorkerThread(LPVOID pParam);


	HANDLE m_hIRDev;
/*	typedef enum _USBIR_MODES
	{
		USBIR_MODE_NONE, USBIR_MODE_RAW, USBIR_MODE_DIV, USBIR_MODE_RC5
	} USBIR_MODES;
	
	BYTE	irGetNumDevices (void);
	HANDLE	irOpen	(int DevIdx, USBIR_MODES IRMode);
	void	irClose	(HANDLE hOpen);
	BOOL	SetupReadEvent(HANDLE hOpen, HANDLE hEvent, BOOL bInstall);
	BOOL	SetUsbIrMode(HANDLE hOpen, USBIR_MODES Mode);

	BOOL DeviceIoControlWait(
	  HANDLE hDevice,              // handle to device
	  DWORD dwIoControlCode,       // operation control code
	  LPVOID lpInBuffer = NULL,        // input data buffer
	  DWORD nInBufferSize = 0,         // size of input data buffer
	  LPVOID lpOutBuffer = NULL,       // output data buffer
	  DWORD nOutBufferSize = 0,        // size of output data buffer
	  LPDWORD lpBytesReturned = NULL,  // byte count
	  DWORD dwMilliseconds = 0     // time-out interval
	);

	BOOL ReadFileWait(
	  HANDLE hDevice,              // handle to device
	  LPVOID lpBuffer,             // data buffer
	  DWORD nNumberOfBytesToRead,  // number of bytes to read
	  LPDWORD lpNumberOfBytesRead = NULL,  // number of bytes read
	  DWORD dwMilliseconds = 0     // time-out interval
	);
*/
};

#endif // !defined(AFX_USBRemoteControl_H__4AD9B3CF_44AB_4961_AB16_FB52B36ED825__INCLUDED_)
