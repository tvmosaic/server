//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
//
//  (C) TechnoTrend AG 1999-2001
//
//  All rights are reserved. Reproduction in whole or in part is prohibited 
//  without the written consent of the copyright owner. TechnoTrend 
//  reserves the right to make changes without notice at any time.  
//
//
//  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  -  TechnoTrend  -  TT  
//
///////////////////////////////////////////////////////////////////////////
//
//  Filename:     DVBFrontend.h
//
//  Project(s):   ttdvbacc
//
//  Author:       GRi
//
//  Purpose:      Frontend-Control class
//   
//  Environment:  Win32
//
//  Dev. Tool(s): MSVC 6.0
//
//  Note(s):      
// 
///////////////////////////////////////////////////////////////////////////
// 
//  File History
//
//   $Workfile: DVBFrontend.h $
//   $Revision: 35 $  
//    $Modtime: 14.12.04 11:32 $
//
//   $Log: /DVB-PC/API/Common/include/DVBFrontend.h $
//     >>   
//     >>   35    23.12.04 16:46 Guido
//     >>   
//     >>   34    4.11.04 17:10 Guido
//     >>   
//     >>   33    26.05.04 9:48 Guido
//     >>   
//     >>   32    25.05.04 16:55 Guido
//     >>   
//     >>   31    7.04.04 12:59 Guido
//     >>   
//     >>   30    24.03.04 17:32 Guido
//     >>   
//     >>   29    14.10.03 14:53 J�rg
//     >>   
//     >>   28    14.10.03 12:25 J�rg
//     >>   
//     >>   27    2.04.03 15:34 Guido
//     >>   
//     >>   26    14.03.03 14:31 Guido
//     >>   
//     >>   25    10.03.03 11:51 Guido
//     >>   
//     >>   24    20.02.03 9:40 Guido
//     >>   
//     >>   23    13.02.03 11:27 Kfrank
//     >>   
//     >>   22    23.01.03 11:02 Guido
//     >>   
//     >>   21    23.01.03 10:10 Guido
//     >>   
//     >>   20    23.01.03 9:51 Kfrank
//     >>   
//     >>   19    18.10.02 14:32 Guido
//     >>   
//     >>   18    28.06.02 10:58 Guido
//     >>   
//     >>   17    25.06.02 20:17 Guido
//     >>   
//     >>   16    27.05.02 10:07 Guido
//     >>   
//     >>   15    25.04.02 14:33 Volker
//     >>   
//     >>   14    20.12.01 18:01 Guido
//     >>   
//     >>   13    27.11.01 17:18 Patrick
//     >>   
//     >>   12    27.11.01 15:11 Guido
//     >>   
//     >>   11    7.11.01 15:55 Guido
//     >>   
//     >>   10    6.11.01 18:54 Guido
//     >>   
//     >>   9     7.09.01 16:26 Guido
//     >>   
//     >>   8     14.08.01 13:01 Volker
//     >>   
//     >>   7     18.07.01 14:41 Guido
//     >>   
//     >>   6     1.01.98 2:05 Guido
//     >>   
//     >>   5     13.06.01 10:36 Guido
//     >>   
//     >>   4     12.06.01 18:22 Guido
//     >>   
//     >>   3     11.06.01 16:45 Guido
//     >>   
//     >>   2     8.06.01 18:08 Guido
//     >>   
//     >>   1     7.06.01 16:06 Guido
//     >>   
//     >>   1     5.06.01 16:01 Guido
//     >>   
//     >>   21    17.05.01 14:08 Guido
//     >>   
//     >>   20    15.05.01 16:30 Guido
//
///////////////////////////////////////////////////////////////////////////

// DVBFrontend.h: interface for the CDVBFrontend class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FRONTEND_H__0E929337_258F_11D4_B88E_00105A22770E__INCLUDED_)
#define AFX_FRONTEND_H__0E929337_258F_11D4_B88E_00105A22770E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "..\Highend\DVBCommon.h"

// defines f�r Hardware-Eigenschaften (GetCapabilities)
#define HAS_VR_AUTO		0x00000001 // automatic Viterbi-Rate Detection
#define HAS_SI_AUTO		0x00000010 // automatic Spectral-Inversion Detection
#define HAS_CH_SCAN		0x00000100 // Channel-Scan to next Channel
#define HAS_BW_AUTO		0x00001000 // automatic BandWidth Detection
#define HAS_ANT_PWR		0x00010000 // Active Antenna Power Supply

// timeout in ms
#define STOP_THREAD_TIMEOUT 200

//class __declspec(dllexport) CDVBFrontend
class AFX_EXT_CLASS CDVBFrontend
{
public:

	// Frontend type indication
	typedef enum _FRONTEND_TYPE {	
		FE_UNKNOWN,
		FE_SATELLITE,	// DVB-S
		FE_CABLE,		// DVB-C
		FE_TERRESTRIAL,	// DVB-T
	} FRONTEND_TYPE;

	typedef	struct _LOCKSTATE_DVB_S
	{
		unsigned	RX_Signal	:1;
		unsigned	C_Lock		:1;
		unsigned	V_Sync		:1;
		unsigned	F_Sync		:1;
		unsigned	V_BER		:2;
	} LOCKSTATE_DVB_S;

	typedef	struct _LOCKSTATE_DVB_C
	{
		unsigned	EQ_Algo		:1;
		unsigned	C_Lock		:1;
		unsigned	NODVB		:1;
		unsigned	F_Sync		:1;
		unsigned	BER			:2;
	} LOCKSTATE_DVB_C;

	typedef	struct _LOCKSTATE_DVB_T
	{
		unsigned    TPS_LOCK    :1;
		unsigned	BER			:2;
	} LOCKSTATE_DVB_T;

	typedef struct _LOCK_STATE 
	{	
		FRONTEND_TYPE	Mode;

		struct		// Logical status - common
		{
			unsigned	FELock	:1;
		};
		union		// Logical status - DVB mode specific
		{
			LOCKSTATE_DVB_S		L_sat;
			LOCKSTATE_DVB_C		L_cab;
			LOCKSTATE_DVB_T		L_ter;
			BYTE				bLogValue;	// Cast to Byte
		};
	} LOCK_STATE;

	typedef enum _BANDWITH_TYPE {
		BW_6MHz  = 0,
		BW_7MHz  = 1,
		BW_8MHz  = 2,
		BW_AUTO  = 3,
		BW_NONE  = 4
	} BANDWITH_TYPE;

	// LNB power indication (DVB-S)
	typedef enum _LNB_POWER_TYPE {
		VOLT00  =  0, POWER_OFF	=  0,	// zero volt (power off)
		VOLT13  = 13, POL_VERT	= 13,	// 13 volt (vertical polarisation)
		VOLT18  = 18, POL_HORZ  = 18,	// 18 volt (horizontal polarisation)
	} LNB_POWER_TYPE;

	// Viterbi rate indication (DVB-S)
	typedef enum _VITERBI_TYPE {
		VR_AUTO  =  0,				// DVB-S: automtic detection of viterbi rate
		VR_1_2	 =  1,				// DVB-S: viterbi rate: 1/2
		VR_2_3	 =  2,				// DVB-S: viterbi rate: 2/3
		VR_3_4	 =  3,				// DVB-S: viterbi rate: 3/4
		VR_4_5	 =  4,				// DVB-S: viterbi rate: 4/5
		VR_5_6	 =  5,				// DVB-S: viterbi rate: 5/6
		VR_6_7	 =  6,				// DVB-S: viterbi rate: 6/7
		VR_7_8	 =  7,				// DVB-S: viterbi rate: 7/8
		VR_8_9	 =  8,				// DVB-S: viterbi rate: 8/9
	} VITERBI_TYPE;
	
	// Modulation indication (DVB-C)
	typedef enum _QAM_TYPE {		
		QAM_16   =  0,				// DVB-C: Modulation:  16-QAM
		QAM_32   =  1,				// DVB-C: Modulation:  32-QAM
		QAM_64   =  2,				// DVB-C: Modulation:  64-QAM (default)
		QAM_128  =  3,				// DVB-C: Modulation: 128-QAM
		QAM_256  =  4,				// DVB-C: Modulation: 256-QAM
	} QAM_TYPE;

	// Spectral inversion indication (DVB-S, DVB-C, DVB-T)
	typedef enum _SPECINVERSION_TYPE {
		SI_AUTO  =  2,				// automatic detection of spectral inversion
		SI_ON    =  0,				// spectral inversion
		SI_OFF   =  1,				// no spectral inversion	
	} SPECINVERSION_TYPE;

	// Input signal properties (DVB-S, DVB-C, DVB-T)
	typedef struct _SIGNAL_TYPE {
		int		nAFC;				// Frequency derivation in kHz
		double	BER;				// Viterbi Bit Error Rate
		BYTE	AGC;				// 0..255 gives an idea of input signal level
									// 0: strong signal - 255: weak signal
		BYTE	SNR_SQE;			// 0..255 - Gives an idea of the C/N of the channel (DVB-S) or
									// the Mean Square Error of the demodulated output signal (DVB-C)
		WORD	SNRdB;				// C/N in 1/10 dB; e.g. 95 means 9.5dB (valid only for DVB-S), 
									// derived from SNR_SQE (for DVB-C SNRdB is always set to 0)
		WORD	SNR100;				// Signal quality/level indicator from 0 to 100%, derived from
									// SNR_SQE
	} SIGNAL_TYPE, *PSIGNAL_TYPE;

	// Channel configuration (DVB-S, DVB-C, DVB-T)
	typedef struct _CHANNEL_TYPE {
		union {
			struct {
				DWORD				dwFreq;			// frequency in kHz
				SPECINVERSION_TYPE	Inversion;		// Spectral inversion
				DWORD				dwSymbRate;		// Symbol rate in Symbols per second
				DWORD				dwLOF;			// LNB local oscillator frequency in kHz
				BOOL				bF22kHz;		// LNB 22kHz power modulation (High/Low-Band or A/B-Pos)
				LNB_POWER_TYPE		LNB_Power;		// LNB power level (Polarization)
				VITERBI_TYPE		Viterbi;		// Viterbi rate
			} dvb_s;

			struct {
				DWORD				dwFreq;			// frequency in kHz
				SPECINVERSION_TYPE	Inversion;		// Spectral inversion
				DWORD				dwSymbRate;		// Symbol rate in Symbols per second
				QAM_TYPE			Qam;			// Modulation
				BANDWITH_TYPE		BandWidth;		// BandWidth of the Channel
			} dvb_c;

			struct {
				DWORD				dwFreq;			// frequency in kHz
				SPECINVERSION_TYPE	Inversion;		// Spectral inversion
				BOOL				bScan;			// Komplete Rescan ??
				BANDWITH_TYPE		BandWidth;		// BandWidth of the Channel
			} dvb_t;
		};
	} CHANNEL_TYPE, *PCHANNEL_TYPE;

/*
	typedef enum _TONE_BURST_TYPE {
		TB_NONE	=  0,				// no Tone Burst
		TB_A	=  1,				// Tone Burst 'Satellite Position A'
		TB_B	=  2,				// Tone Burst 'Satellite Position B'
	} TONE_BURST_TYPE;

	typedef enum _DISEQC_TYPE {
		DISEQC_NON		=  0,				// no DiSEqC-Message
		DISEQC_RESET	=  1,				// Reset DiSEqC-Equipment Message
		DISEQC_RAW		=  2,				// 'raw' DiSEqC-Message (diseqc_raw)
		DISEQC_SWITCH	=  3,				// Command to a DiSEqC-Switch (diseqc_switch)
	} DISEQC_TYPE;

	typedef struct _LNB_TYPE {
		DWORD				dwLOF;			// LNB local oscillator frequency in kHz
		BOOL				bF22kHz;		// LNB 22kHz power modulation (High/Low-Band or A/B-Pos)
		LNB_POWER_TYPE		LNB_Power;		// LNB power level (Polarization)
		TONE_BURST_TYPE		ToneBurst;		// Tone Burst (None/Pos-A/Pos-B)
		DISEQC_TYPE			Diseqc;			// Type of Diseqc-Message following (raw or switch)
		BYTE nRepeats;						// repetitions of the DiSEqC-Message (0..n)
		union {
			struct {
				BYTE Data[6];				// up to 6 Bytes DiSEqC-Message
				BYTE nlen;					// number of Bytes used in Data (3-6)
			} diseqc_raw;

			struct {
				BOOL bBand;					// bBand== TRUE: High-Band, FALSE: Low-Band
				BOOL bPol;					// bPol == TRUE: Vertical,  FALSE: Horizontal
				BOOL bPos;					// bPos == TRUE: Pos-B, FALSE: Pos-A
				BOOL bOpt;					// bOpt == TRUE: Opt-B, FALSE: Opt-A
			} diseqc_switch;
		};
	} LNB_TYPE, *PLNB_TYPE;

	// Channel2 configuration (DVB-S, DVB-C, DVB-T)
	typedef struct _CHANNEL_TYPE2 {
		DWORD				dwFreq;			// frequency in kHz
		SPECINVERSION_TYPE	Inversion;		// Spectral inversion
		union {
			struct {
				DWORD				dwSymbRate;		// Symbol rate in Symbols per second
				VITERBI_TYPE		Viterbi;		// Viterbi rate
			} dvb_s;

			struct {
				DWORD				dwSymbRate;		// Symbol rate in Symbols per second
				QAM_TYPE			Qam;			// Modulation
			} dvb_c;

			struct {
				BOOL				bScan;			// Komplete Rescan ??
			} dvb_t;
		};
	} CHANNEL_TYPE2, *PCHANNEL_TYPE2;
*/
	
	CDVBFrontend();
	virtual ~CDVBFrontend();

	DVB_ERROR Init();
	FRONTEND_TYPE GetType(void);
	// Eigenschaften des FrontEnds abfragen
	DWORD GetCapabilities(void) {return m_Capabilities;};
	void GetFrequencyRange(DWORD& min, DWORD& max); // Frequency in kHz
	void GetBandwidthAndGap(DWORD& dwBW, DWORD& min, DWORD& max); // Frequency in kHz
	void GetSymbolRateRange(DWORD& min, DWORD& max); // Symbol rate in Symbols per second
	void GetBandwidthRange(BANDWITH_TYPE& min, BANDWITH_TYPE& max);
	
	DVB_ERROR SetChannel(CHANNEL_TYPE ch, BOOL bPowerOnly = FALSE);
	DVB_ERROR GetChannel(CHANNEL_TYPE& ch);
	DVB_ERROR GetState(BOOL& Locked, SIGNAL_TYPE* Signal = NULL, LOCK_STATE* pLS = NULL);

	// only for DVB-S (use GetType())
	DVB_ERROR SendDiSEqCMsg(BYTE* pData, BYTE bLen, BYTE bToneBurst);
	// bType -> 0: Off, 1: Simple DiSEqC, 2: DiSEqC 1.0
	// bPos/bOpt == TRUE: B-Pos., FALSE: A-Pos
	// bPol == TRUE: Vertical, FALSE: Horizontal
	// bBand == TRUE: High-Band, FALSE: Low-Band
	DVB_ERROR SendDiSEqCMsg(BYTE nType, BOOL bPos, BOOL bOpt, BOOL bPol, BOOL bBand, BYTE nRepeats);

	DVB_ERROR SetAntennaPower(BOOL enable); // DVB-T Active Antenna Power Supply On/Off
	DVB_ERROR GetAntennaPower(DWORD& state);// State of the Antenna Power Supply (0 Off; 1 Off; 2 short circuit; 3 unknown)
/*	
	DVB_ERROR SetChannel2(CHANNEL_TYPE2 ch);
	DVB_ERROR GetChannel2(CHANNEL_TYPE2& ch);
	DVB_ERROR SetLNB(LNB_TYPE lnb);
	DVB_ERROR GetLNB(LNB_TYPE& lnb);
*/
	// Search for Channel/Transponder (only if(GetCapabilities() & HAS_CH_SCAN) == TRUE)
	// starts a transponder/channel search; returns after search, 
	// after timeout or immediately if bBlockingMode is set to FALSE
	// return: 0 RUNNING; 1 FAILED; 2 COMPLETED
	DWORD SearchNextChannel(DWORD dwUpperFreq, BOOL bBlockingMode = TRUE); // call SetChannel with LowerFreq before
	// stops a running transponder/channel scan
	void StopSearch(void); // only useful if bBlockingMode was set to FALSE
	// returns status of scanning process (e.g. if bBlockingMode is set to FALSE)
	DWORD GetSearchState(void) {return m_ScanningState;}; // return: 0 RUNNING; 1 FAILED; 2 COMPLETED

	typedef struct _TYPE_TPS // DVB-T
	{	BOOL	InitBit;		WORD	SyncWord;
		BYTE	Length;			BYTE	FrameNr;
		BYTE	Constellation;	BYTE	Hierarchy;
		BYTE	CodeRateHP;		BYTE	CodeRateLP;
		BYTE	Guard;			BYTE	Mode;
		WORD	ReservedX;		WORD	BCH_Code;
		BOOL	TPS_Lock;		BOOL	TPS_Valid;
		BOOL	TimeSync;		BOOL	AFC_Lock;
		int     Freq_Offset;  		// IMPORTANT for Scan
	} TYPE_TPS;

	int GetTPS(TYPE_TPS *pTPS); // DVB-T; returns FrameNr,Constellation,Hierarchy,CodeRateHP,CodeRateLP,Guard,Mode members of TPS

private:

	void RetrieveTPS(LPCTSTR sRegKey,  TYPE_TPS & TPS);
	void StoreTPS(LPCTSTR sRegKey,  TYPE_TPS TPS);
	int i2cWriteRead(BYTE Slave, int* pWData, int WLength, int* pRData, int RLength);
	int i2cRead(BYTE Slave, int* pData, int Length);
	int i2cWrite(BYTE Slave, int* pData, int Length);
	int m_I2C_Retry;
	
	int WriteI2CReg(BYTE Slave, BYTE Addr, BYTE Data);
	int ReadI2CReg(BYTE Slave, BYTE Addr, BYTE& Data);
	BYTE WriteI2CArrayMC(BYTE SA, BYTE *InitTab, int Len);
	BYTE WriteI2CArray(BYTE SA, BYTE *InitTab, int Len);
	int WriteI2CMultiReg(BYTE Slave, BYTE Addr, BYTE Length, BYTE *pData);
	int WriteI2CByte(BYTE Slave, BYTE Data);
	int ReadI2CMultiReg(BYTE Slave, BYTE Addr, BYTE Length, BYTE *pData);
	int ReadI2CReg0297(BYTE Slave, BYTE Addr, BYTE& Data);
	int ReadI2CReg1010(BYTE Slave, BYTE Addr, BYTE &Data);
	int Write2VES18xx(BYTE Slave, BYTE Addr, BYTE Data1, BYTE Data2);
	int WriteBlockVES18xx(BYTE Slave, BYTE Addr, BYTE Len, BYTE* pData);
	int ReadVES18xx(BYTE Slave, BYTE Addr, BYTE& pData);
	int WriteVES18xx(BYTE Slave, BYTE Addr, BYTE Data);

	int Write87J205(WORD Addr, WORD Len, BYTE *pData);
	int Read87J205(WORD Addr, WORD Len, BYTE *pData);

	void ClrBit_1877_93(void);
	void ClrBit_1820(void);
	void ClrBit_8083(void);

	WORD InitFE(void);
	WORD InitIntern(void);

	DWORD SetFreq(DWORD Freq, DWORD LOF, DWORD SR, BANDWITH_TYPE BW, BOOL bScan = FALSE);
	DWORD SetSymRate(DWORD SymRate);
	//void SetHV22(BOOL Power, BOOL HV, BOOL F22);
	BOOL GetLockState(LOCK_STATE * pLS = NULL);
	BOOL IsAnalogLocked(void);
	void SetBER_Exp(DWORD Exp);
	int SetInversion(int Invers);
	int GetInversion(void);
	void SetViterbi(VITERBI_TYPE viterbi);
	void SetQAM(QAM_TYPE QAM_Mode);
	double GetBER(void);
	int GetAFC(void);
	BYTE GetAGC(void);
	WORD GetSNR_SQE(void);
	void TunerI2CEnable();
//	BYTE GetTunerState(void);
//	int GetSymRateOffset();
//	int GetCodeRate(BYTE &CodeRate);
	void SetSTV0299Search(bool search);
//	DWORD VerfifyCabTerFreq(DWORD dwFrequency);

	int StartDemod();
	int SetOFDMParams(TYPE_TPS *tps);
//	int GetTPS(TYPE_TPS *pTPS);
	BYTE GetPWM(void); // nur aus EEPROM lesen
	void SetPWM(BYTE pwm); // in 1820-Reg schreiben
	void SetFinePWM(WORD pwm);

	//FE_CHIP_TYPE m_FE_Chip;
	int m_FE_Chip;
	FRONTEND_TYPE m_FE_Type;
	DWORD m_BER_Exp;
	int m_inversion;
	int m_AFC_Accu;
//	BOOL m_f22;
//	BOOL m_LastHV;
	WORD m_Derotate;
	CHANNEL_TYPE m_ch;
	
//	CHANNEL_TYPE2 m_ch2;
//	LNB_TYPE m_lnb;

	DWORD m_Capabilities;
	DWORD m_ScanningState;
	BOOL m_japanese_lnb_support;
	//void SetJapaneseLnbSupport(bool jls);
	BOOL m_bAntennaPower;

	static double CDVBFrontend::Log2Const[];
	static double CDVBFrontend::dCR[];
	static double CDVBFrontend::dGuard[];
	static double CDVBFrontend::dMode[];
	
	BOOL m_bUse0299Api;
	BOOL m_bUseZApi;
	void* m_pZApiFE;
	BOOL m_bAnalog;
	BOOL m_lastAnalog;
	TYPE_TPS m_tps; //L64781
	void* m_pFE87J205;
	void* m_pFE0297;
	void* m_pFE0299;
	void* m_pFE_STB;
	void* m_pTDA10045;
	void* m_pTDA10046;
	void* m_pFEPN1010;
	void* m_pFE46ST;
	void* m_pTDA10086;
	
	USHORT m_boardRev;

	typedef struct OTI7K_Status
	{	double	BER1;
		double	BER2;
		DWORD	UncorCnt;
		bool	ViterbiLock;
	} OTI7K_Status;

	int   o7k_SetInversion(BOOL Invert);
	int   o7k_GetStatus(BYTE Mask, BOOL &Lock, OTI7K_Status *Status);
	void  o7k_ConnectTunerI2C(BOOL connect);
	DWORD o7k_SetTunerFreq(DWORD Freq, BOOL bALPS);
	void  o7k_InitOTI7K(BOOL bALPS);
	DWORD o7k_ChannelChange(DWORD Freq, BOOL bALPS);
	int   o7k_WriteOTI7K(BYTE Slave, BYTE Addr, BYTE DATA);
	void  o7k_EnableTunerI2C(BOOL enable);
	int   o7k_ReadOTI7K(BYTE Slave, BYTE Addr, BYTE &Data);
	BYTE  o7k_GetOTI7K_ID();
	int   o7k_GetTPS(TYPE_TPS *pTPS);
	//BOOL  m_o7k_ALPS;
	//int   m_o7k_LeakyBucket;
	int GetTPSMode(TYPE_TPS tps);
	DWORD VerfifyCabTerFreq(DWORD dwFreq);
	DWORD VerfifyCabTerFreq_US(DWORD dwFreq);
	
	DWORD SetFreq_64781(DWORD Freq, DWORD LOF, BOOL bScan);
	DWORD SetFreq_64781_neu(DWORD Freq, DWORD LOF, BOOL bScan);
	DWORD SetFreq_64781_6MHz(DWORD Freq, DWORD LOF, BOOL bScan);

	WORD GetBoardRevision(void);
	//int SetAnalogMode(BOOL bAnalog);

	BOOL StartThread(int iPriority = THREAD_PRIORITY_NORMAL);
	BOOL StopThread(DWORD dwTimeout = STOP_THREAD_TIMEOUT); // in Millisekunden
	BOOL IsRunning();
	BOOL Run();

	CCriticalSection m_CriticalSection;
	volatile long m_lRunThread;
	CWinThread* m_pThread;
	CEvent m_Event;

	static UINT WorkerThread(LPVOID pParam);

};

#endif // !defined(AFX_FRONTEND_H__0E929337_258F_11D4_B88E_00105A22770E__INCLUDED_)
