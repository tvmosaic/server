// DVBRecPlayPES.h: Schnittstelle f�r die Klasse CDVBRecPlayPES.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DVBREC2AVPES_H__502D29CB_7217_4038_B2B7_01D3E37B5019__INCLUDED_)
#define AFX_DVBREC2AVPES_H__502D29CB_7217_4038_B2B7_01D3E37B5019__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "..\HighEnd\DVBRecPlay.h"

// timeout in ms
#define STOP_THREAD_TIMEOUT 200
#define RCVTRMBUFLEN	MAX_BLOCK_SIZE

//#define DBG_PVA2FILE // f�r Debug-Ausgabe der PVA-Pakete in die Datei "C:\\pvastream.pva"

class AFX_EXT_CLASS CDVBRecPlayPES : protected CDVBRecPlay
{
public:
	CDVBRecPlayPES();
	virtual ~CDVBRecPlayPES();
// Record
	BOOL StartRecord(REC_PLAY_FORMAT format);
	BOOL StopRecord(void);
	void PauseRecording(void);

	DWORD GetByteCount(void) {return m_dwByteCount;};
// Playback
	BOOL StartPlayback(REC_PLAY_FORMAT format);
	BOOL StopPlayback(void);
	BOOL WritePES(BYTE* pBuffer, WORD PES_Length);

public:
	// CDVBRecPlayPES Overridable callbacks (Record)
	virtual void OnPVAPacketArrival(BYTE* Buff, int len);
	virtual void OnMP2AudioArrival(BYTE* Buff, int len);

	virtual void OnPESAudio(BYTE* Buff, int len);
	virtual void OnPESVideo(BYTE* Buff, int len);

	virtual void OnPESRecord(BYTE* Buff, int len);
	void InitClearBuffer();//J.R.
private:
	
	BYTE m_RcvTrmBuf[RCVTRMBUFLEN];
	DWORD m_dwByteCount;
	void* p_mpeg2pva;
	void* p_pva2mpg;

	//---------------------------------------------------------------------
	BOOL StartThread(int iPriority = THREAD_PRIORITY_NORMAL);
	BOOL StopThread(DWORD dwTimeout = STOP_THREAD_TIMEOUT); // in Millisekunden
	BOOL IsRunning();
	BOOL Run();

	CCriticalSection m_CriticalSection;
	volatile long m_lRunThread;
	CWinThread* m_pThread;

	static UINT WorkerThread(LPVOID pParam);

	//---------------------------------------------------------------------
#ifdef DBG_PVA2FILE
	CFile fx;
#endif
};

#endif // !defined(AFX_DVBREC2AVPES_H__502D29CB_7217_4038_B2B7_01D3E37B5019__INCLUDED_)
