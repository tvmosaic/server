/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "v4l_capmt.h"
#include <vector>
#include <string>
#include <string.h>
#include <stdlib.h>

struct TCA_PMT_CA_DESC
{
    unsigned char* cadesc;
    int length;
};

static void GetCADescriptorsFromPMT(void* pmtBuffer, int pmtLength, std::vector<TCA_PMT_CA_DESC>& ca_desc_list)
{
	ca_desc_list.clear();

	unsigned char* byte_buf = (unsigned char*)pmtBuffer;
	//Get offset to the PMT descriptors part
	int program_info_len = (((((unsigned short)byte_buf[10]) << 8) & 0xFF00) | byte_buf[11]) & 0x0FFF;
	int pmt_desc_offs = 0;
	byte_buf += 12;
	while (pmt_desc_offs < program_info_len)
	{
		unsigned char* cur_buf = byte_buf + pmt_desc_offs;
		int desc_len = cur_buf[1];
		if (cur_buf[0] == 0x09)
		{
			TCA_PMT_CA_DESC desc_info;
            desc_info.length = desc_len + 2;
            //be on the safe side
            if (desc_info.length < pmtLength)
            {
                desc_info.cadesc = (unsigned char*)malloc(desc_info.length);
                memcpy(desc_info.cadesc, cur_buf, desc_info.length);
			    ca_desc_list.push_back(desc_info);
            }
		}
		pmt_desc_offs += desc_len + 2;
	}
}

struct TCA_PMT_ES_DESC
{
    unsigned short pid;
    unsigned char type;
    std::vector<TCA_PMT_CA_DESC> ca_desc_list;
    std::vector<TCA_PMT_CA_DESC> other_desc_list;
};

static bool GetPMTStreamsInfo(unsigned char* pmt_section, int pmt_len, unsigned short& ServiceId, std::vector<TCA_PMT_ES_DESC>& stream_info)
{
	bool ret_val = true;
	stream_info.clear();

	unsigned char* byte_buf = (unsigned char*)pmt_section;
    //service ID == program number
	ServiceId = ((((unsigned short)byte_buf[3]) << 8) & 0xFF00) | byte_buf[4];
    //Get offset to the stream description part
	int program_info_len = (((((unsigned short)byte_buf[10]) << 8) & 0xFF00) | byte_buf[11]) & 0x0FFF;
	int streams_desc_offs = program_info_len + 12;
	//scan PMT for PIDs
	while (streams_desc_offs < pmt_len - 4)
	{
		unsigned char* cur_buf = byte_buf + streams_desc_offs;
		TCA_PMT_ES_DESC si;
		si.pid = (((((unsigned short)cur_buf[1]) << 8) & 0xFF00) | cur_buf[2]) & 0x1FFF;
		si.type = cur_buf[0];
		int stream_desc_len = (((((unsigned short)cur_buf[3]) << 8) & 0xFF00) | cur_buf[4]) & 0x0FFF;
        //collect ca descriptors
	    int pmt_ca_desc_offs = 0;
        unsigned char* ca_desc_buf_start = cur_buf + 5;
	    while (pmt_ca_desc_offs < stream_desc_len)
	    {
		    unsigned char* ca_desc_buf = ca_desc_buf_start + pmt_ca_desc_offs;
		    int ca_desc_len = ca_desc_buf[1];
            std::vector<TCA_PMT_CA_DESC>* list_ptr;
		    if (ca_desc_buf[0] == 0x09)
                list_ptr = &si.ca_desc_list;
            else
                list_ptr = &si.other_desc_list;

            TCA_PMT_CA_DESC desc_info;
            desc_info.length = ca_desc_len + 2;
            //be on the safe side
            if (desc_info.length < pmt_len)
            {
                desc_info.cadesc = (unsigned char*)malloc(desc_info.length);
                memcpy(desc_info.cadesc, ca_desc_buf, desc_info.length);
		        list_ptr->push_back(desc_info);
            }

            pmt_ca_desc_offs += ca_desc_len + 2;
	    }
        //save resulting info
		stream_info.push_back(si);
		//Advance offset pointer
		streams_desc_offs += stream_desc_len + 5;
	}

	return ret_val;
}

static bool IsAVStream(unsigned char type, std::vector<TCA_PMT_CA_DESC>& stream_desc_list)
{
    bool ret_val = false;
    switch (type)
    {
      case 0x1b://H.264
      case 0x10://MPEG4 ISO/IEC 14496-2
      case 0x1://MPEG-1 VIDEO ISO/IEC 11172 
      case 0x2://MPEG-2 VIDEO ITU-T Rec. H.262 | ISO/IEC 13818-2 Video or ISO/IEC 11172-2 constrained parameter video stream
      case 0x3://MPEG-1 AUDIO ISO/IEC 11172 
      case 0x4://MPEG-3 AUDIO ISO/IEC 13818-3 
      case 0x81://AC3 AUDIO
      case 0x0f://AAC AUDIO
      case 0x11://LATM AAC AUDIO
          ret_val = true;
        break;
      case 0x06:
          //this can be ac3 audio
          //go through descriptors to see if it is so
          for (unsigned int i=0; i<stream_desc_list.size(); i++)
          {
              if (stream_desc_list[i].length > 0 &&
                  stream_desc_list[i].cadesc[0] == 0x6a) //ac3 descriptor
              {
                  ret_val = true;
                  break;
              }
          }
          break;
      default:
          break;
    }
    return ret_val;
}

bool CreateCAPMTFromPMT(unsigned char* pmt, int pmt_len, unsigned long listmgt, unsigned long cmd,
		unsigned char* ca_pmt, int& ca_pmt_len)
{
    unsigned char version = 1; //hardcode it for now
    //get information about ES from pmt
    std::vector<TCA_PMT_ES_DESC> stream_info;
    unsigned short ServiceId;
    GetPMTStreamsInfo(pmt, pmt_len, ServiceId, stream_info);
    //get all ca descriptors
    std::vector<TCA_PMT_CA_DESC> ca_desc_list;
    GetCADescriptorsFromPMT(pmt, pmt_len, ca_desc_list);
    //Start forming the CA_PMT
    ca_pmt_len = 0;
    //list management
    ca_pmt[ca_pmt_len] = listmgt;
    ca_pmt_len += 1;
    //program number
	ca_pmt[ca_pmt_len] = (ServiceId >> 8) & 0xFF;
    ca_pmt[ca_pmt_len+1] = ServiceId & 0xFF;
    ca_pmt_len += 2;
	//Reserved, Version number, Current-Next indicator (always set to 1)
	ca_pmt[ca_pmt_len] = (unsigned char)(0xC0|(((version&0x1F)<<1) & 0xFE) | 0x01);
    ca_pmt_len += 1;
    //remember program info length offset and reserve place for it
    int prg_info_len_offset = ca_pmt_len;
    ca_pmt_len += 2;
    if (ca_desc_list.size() > 0)
    {
        //command
        ca_pmt[ca_pmt_len] = cmd; //Ok descrambling
        ca_pmt_len += 1;
        //CA descriptors
        for (unsigned int i=0; i<ca_desc_list.size(); i++)
        {
            memcpy(ca_pmt + ca_pmt_len, ca_desc_list[i].cadesc, ca_desc_list[i].length);
            ca_pmt_len += ca_desc_list[i].length;
        }
    }
    //update length field
    unsigned short l = ca_pmt_len - prg_info_len_offset - 2;
	ca_pmt[prg_info_len_offset] = (l >> 8) & 0x0F;
	ca_pmt[prg_info_len_offset+1] = (l & 0x00FF);
    //elementary streams
    for (unsigned int i=0; i<stream_info.size(); i++)
    {
        if (IsAVStream(stream_info[i].type, stream_info[i].other_desc_list) || stream_info[i].ca_desc_list.size() != 0)
        {
            //stream type
            ca_pmt[ca_pmt_len] = stream_info[i].type;
            ca_pmt_len += 1;
            //ES pid
            ca_pmt[ca_pmt_len] = (unsigned char)(0xE0 | ((stream_info[i].pid >> 8) & 0x1F));
	        ca_pmt[ca_pmt_len + 1] = stream_info[i].pid & 0xFF;
            ca_pmt_len += 2;
            //remember ES info length offset and reserve place for it
            int es_info_len_offset = ca_pmt_len;
            ca_pmt_len += 2;
            if (stream_info[i].ca_desc_list.size() > 0)
            {
                //command
                ca_pmt[ca_pmt_len] = cmd;
                ca_pmt_len += 1;
                //CA descriptors
                for (unsigned int es_ca_idx=0; es_ca_idx<stream_info[i].ca_desc_list.size(); es_ca_idx++)
                {
                    memcpy(ca_pmt + ca_pmt_len, stream_info[i].ca_desc_list[es_ca_idx].cadesc, stream_info[i].ca_desc_list[es_ca_idx].length);
                    ca_pmt_len += stream_info[i].ca_desc_list[es_ca_idx].length;
                }
            }
            //update es info length field
            unsigned short l = ca_pmt_len - es_info_len_offset - 2;
	        ca_pmt[es_info_len_offset] = ((l >> 8) & 0x0F);
	        ca_pmt[es_info_len_offset+1] = (l & 0x00FF);
        }
    }
    //free all allocated ca descriptors
    for (unsigned int i=0; i< ca_desc_list.size(); i++)
        free(ca_desc_list[i].cadesc);
    for (unsigned int i=0; i< stream_info.size(); i++)
    {
        for (unsigned int j=0; j< stream_info[i].ca_desc_list.size(); j++)
            free(stream_info[i].ca_desc_list[j].cadesc);
    }
    return true;
}
