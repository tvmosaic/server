/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <sstream>
#include <fcntl.h>
#include <stropts.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <frontend.h>
#include <dl_logger.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <boost/algorithm/string.hpp>

#include <drivers/v4l.h>
#include "v4ltypes_conv.h"
#include "v4l_stream_src.h"
#include "v4ltuners_ca.h"

#ifndef O_CLOEXEC
# define O_CLOEXEC 02000000
#endif

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;
#define INVALID_DEVICE_IDX 0xFFFF

bool get_frontend_abs_path(const char* frontend_path, std::string& frontend_abs_path)
{
	struct stat statbuf;
	//check if frontend file exists
	if (stat(frontend_path, &statbuf) < 0)
	{
		return false;
	}
	//device type - block vs. char
	std::string device_type;
	if (S_ISBLK(statbuf.st_mode))
		device_type = "block";
	else if (S_ISCHR(statbuf.st_mode))
		device_type = "char";
	else
		return false;

	char sys_path[1024];
	/* use /sys/dev/{block,char}/<maj>:<min> link */
	snprintf(sys_path, sizeof(sys_path), "/sys/dev/%s/%u:%u",
			device_type.c_str(), major(statbuf.st_rdev), minor(statbuf.st_rdev));

	//traverse symbolic link (if needed)
	char real_path[1024];
	int len = readlink(sys_path, real_path, sizeof(real_path));
	if (len <= 0 || len == (ssize_t)sizeof(real_path))
		return false;
	real_path[len] = '\0';

	//check if this is relative or absolute link
	if (real_path[0] != '/')
	{
		//count a number of times that real_path begins with ../
		char* prepend_ptr = real_path;
		int back_count = 0;
		const char* back_str = "../";
		while (strncmp(prepend_ptr, back_str, strlen(back_str)) == 0)
		{
			back_count += 1;
			prepend_ptr += strlen(back_str);
		}
		//the last entry in sys_path is the file itself - remove it
		back_count += 1;
		while (back_count > 0)
		{
			char* pos = strrchr(sys_path, '/');
			if (pos != NULL)
				pos[0] = '\0';
			back_count -= 1;
		}
		frontend_abs_path = sys_path;
		frontend_abs_path += '/';
		frontend_abs_path += prepend_ptr;
	} else
	{
		frontend_abs_path = real_path;
	}
	return true;
}

bool read_metafile_contens(const char* path, std::string& contents)
{
	bool ret_val = false;
	contents.clear();

	int fd = open(path, O_RDONLY|O_CLOEXEC);
	if (fd >= 0)
	{
		char value[1024];
		size_t size = read(fd, value, sizeof(value));
		close(fd);
		if (size >= 0 && size < sizeof(value))
		{
			value[size] = '\0';
			contents = value;
			boost::trim(contents);
			ret_val = true;
		}
	}
	return ret_val;
}

bool get_tuner_meta_properties(const char* frontend_path, std::string& tuner_name, std::string& tuner_path, std::string& vendor_id,
		std::string& product_id, std::string& serial, dvblink::DL_E_TUNER_CONNECTION_TYPE& connection_type)
{
	bool ret_val = false;
	tuner_path.clear();
	tuner_name.clear();
	connection_type = dvblink::TCT_UNKNOWN;
	serial.clear();

	std::string frontend_real_path;
	if (!(get_frontend_abs_path(frontend_path, frontend_real_path)))
		return false;

	//look for "device" link within frontend path
	frontend_real_path += "/device";

	char resolved_path[PATH_MAX];
	if (realpath(frontend_real_path.c_str(), resolved_path) != NULL)
	{
		tuner_path = resolved_path;
		//read the contents of "product" file (if it exists)
		std::string fpath = resolved_path;
		fpath += "/product";
		read_metafile_contens(fpath.c_str(), tuner_name);
		fpath = resolved_path;
		fpath += "/idProduct";
		if (!read_metafile_contens(fpath.c_str(), product_id))
		{
			//try "device" file instead - centos
			fpath = resolved_path;
			fpath += "/device";
			read_metafile_contens(fpath.c_str(), product_id);
		}
		fpath = resolved_path;
		fpath += "/idVendor";
		if (!read_metafile_contens(fpath.c_str(), vendor_id))
		{
			//try "vendor" file instead - centos
			fpath = resolved_path;
			fpath += "/vendor";
			read_metafile_contens(fpath.c_str(), vendor_id);
		}
		fpath = resolved_path;
		fpath += "/serial";
		read_metafile_contens(fpath.c_str(), serial);

		fpath = resolved_path;
		fpath += "/subsystem";
		char resolved_subsystem_path[PATH_MAX];
		if (realpath(fpath.c_str(), resolved_subsystem_path) != NULL)
		{
			if (boost::iends_with(resolved_subsystem_path, "usb"))
				connection_type = dvblink::TCT_USB;
			else if (boost::iends_with(resolved_subsystem_path, "pci") ||
					boost::iends_with(resolved_subsystem_path, "pci_express"))
				connection_type = dvblink::TCT_PCIE;
		}

		ret_val = true;
	}

	return ret_val;
}

static void post_process_list_for_dual_tuners(dvblink::PDevAPIDevListEx pDL)
{
	std::map<std::wstring, std::vector<int> > device_path_to_idx;
	for (int i=0; i<pDL->Count; i++)
	{
		std::wstring device_path = pDL->Devices[i].szDevicePathW;
		if (device_path_to_idx.find(device_path) == device_path_to_idx.end())
			device_path_to_idx[device_path] = std::vector<int>();
		bool bfound = false;
		for (size_t j=0; j<device_path_to_idx[device_path].size(); j++)
		{
			if (pDL->Devices[i].TunerIdx == device_path_to_idx[device_path].at(j))
			{
				bfound = true;
				break;
			}
		}
		if (!bfound)
			device_path_to_idx[device_path].push_back(pDL->Devices[i].TunerIdx);
	}
	//walk through the map and process all devices with two or more device indexes
	std::map<std::wstring, std::vector<int> >::iterator it = device_path_to_idx.begin();
	while (it != device_path_to_idx.end())
	{
		if (it->second.size() > 1)
		{
			//sort array on device indexes
			std::sort(it->second.begin(), it->second.end());
			//update device path for those device indexes
			for (size_t didx=0; didx < it->second.size(); didx++)
			{
				for (int i=0; i<pDL->Count; i++)
				{
					if (pDL->Devices[i].TunerIdx == it->second.at(didx))
					{
						std::wstringstream buf;
						buf.clear(); buf.str(L"");
						buf << pDL->Devices[i].szDevicePathW << L":" << didx;
						wcscpy(pDL->Devices[i].szDevicePathW, buf.str().c_str());
					}
				}
			}
		}
		++it;
	}
}

static unsigned long dvbv5_get_tuner_types(int frontend_fd)
{
	unsigned long ret_val = dvblink::TUNERTYPE_UNKNOWN;

	dtv_property p;
	p.cmd = DTV_ENUM_DELSYS;

	dtv_properties cmdseq;
	cmdseq.num = 1;
	cmdseq.props = &p;

    if ((ioctl(frontend_fd, FE_GET_PROPERTY, &cmdseq)) != -1)
    {
    	int num_tuner_types = p.u.buffer.len;
    	if (num_tuner_types > 0)
    	{
			for(;p.u.buffer.len > 0; p.u.buffer.len--)
			{
				fe_delivery_system_t delsys = (fe_delivery_system_t)p.u.buffer.data[p.u.buffer.len - 1];
				switch (delsys)
				{
					case SYS_DVBT:
					case SYS_DVBT2:
						ret_val |= dvblink::TUNERTYPE_DVBT;
						break;
					case SYS_DVBC_ANNEX_C:
					case SYS_DVBC_ANNEX_A:
					case SYS_DVBC_ANNEX_B:
						ret_val |= dvblink::TUNERTYPE_DVBC;
						break;
					case SYS_DVBS2:
					case SYS_DVBS:
						ret_val |= dvblink::TUNERTYPE_DVBS;
						break;
					case SYS_ATSC:
						ret_val |= dvblink::TUNERTYPE_ATSC;
						break;
					default:
						break;
				}
			}
    	}
    }
    adjust_qam_for_atsc(ret_val);
    return ret_val;
}

static int DeviceGetList_impl(dvblink::PDevAPIDevListEx pDL)
{
	pDL->Count = 0;

	unsigned int start_idx = 0;

    log_info(L"v4l::DeviceGetList_impl. getting v4l device list");

	for (unsigned int i=start_idx; i<DL_DEVAPI_MAX_DEVICES; i++)
	{
		for (unsigned int frontend_idx = 0; frontend_idx < 8; frontend_idx++) //up to 8 frontends
		{
			std::string frontend_path = get_v4l_node_path(i, frontend_idx, "frontend");
			std::wstring wfrontend_path = string_cast<EC_UTF8>(frontend_path);

			int frontend_fd = open(frontend_path.c_str(), O_RDONLY);
			if (frontend_fd < 0)
			{
				//this is not-existent device. Proceed to the next one
				int e = errno;
				if (e != ENOENT)
				    log_error(L"v4l::DeviceGetList_impl. open failed on frontend %1% (%2%)") % wfrontend_path % e;
				continue;
			}

		    log_info(L"v4l::DeviceGetList_impl. Successfully opened frontend %1%") % wfrontend_path;

			//get supported tuner types
			unsigned long supported_types = dvbv5_get_tuner_types(frontend_fd);

			//get frontend properties
			dvb_frontend_info fe_info;
			if (ioctl(frontend_fd, FE_GET_INFO, &fe_info) != -1)
			{
				if (supported_types != dvblink::TUNERTYPE_UNKNOWN)
					pDL->Devices[pDL->Count].TunerType = supported_types;
				else
					pDL->Devices[pDL->Count].TunerType = v4lTunerTypeTotvs(fe_info.type);

				//the highest byte is frontend index
				pDL->Devices[pDL->Count].TunerIdx = i;
				pDL->Devices[pDL->Count].FrontendIdx = frontend_idx;
				//get tuner name and path
				std::string tuner_path;
				std::string tuner_name;
				std::string vendor_id;
				std::string product_id;
				std::string serial;
				dvblink::DL_E_TUNER_CONNECTION_TYPE connection_type;
				if (get_tuner_meta_properties(frontend_path.c_str(), tuner_name, tuner_path,
						vendor_id, product_id, serial, connection_type))
				{
		            strcpy(pDL->Devices[pDL->Count].vid, vendor_id.c_str());
		            strcpy(pDL->Devices[pDL->Count].pid, product_id.c_str());
					
					std::wstring wtuner_path = string_cast<EC_UTF8>(tuner_path);
					wcscpy(pDL->Devices[pDL->Count].szDevicePathW, wtuner_path.c_str());
					
					//uuid is a combination of path and serial (vid/pid if serial is empty)
					std::string uuid = tuner_path;
					if (!serial.empty())
						uuid += ":" + serial;
					else
						uuid += ":" + vendor_id + ":" + product_id;
					strcpy(pDL->Devices[pDL->Count].uuid, uuid.c_str());

					if (tuner_name.size() > 0)
						strcpy(pDL->Devices[pDL->Count].szName, tuner_name.c_str());
					else
						strcpy(pDL->Devices[pDL->Count].szName, fe_info.name);

					if (connection_type != dvblink::TCT_UNKNOWN)
						pDL->Devices[pDL->Count].connection_type = connection_type;
				} else
				{
					//do it in the old way
					std::wstring wtuner_path = string_cast<EC_UTF8>(frontend_path);
					wcscpy(pDL->Devices[pDL->Count].szDevicePathW, wtuner_path.c_str());
					strcpy(pDL->Devices[pDL->Count].szName, fe_info.name);
					strcpy(pDL->Devices[pDL->Count].uuid, frontend_path.c_str());
				}
				pDL->Count += 1;
			} else
			{
				int e = errno;

			    log_error(L"v4l::DeviceGetList_impl. ioctl FE_GET_INFO failed on frontend %1% (%2%)") % wfrontend_path % e;
			}

			close(frontend_fd);
		}
	}

	post_process_list_for_dual_tuners(pDL);

	return dvblink::SUCCESS;
}

int v4l_tuner_factory_t::DeviceGetList(dvblink::PDevAPIDevListEx pDL)
{
	return DeviceGetList_impl(pDL);
}

tuner_t* v4l_tuner_factory_t::get_tuner(const std::string& device_path, int fdx)
{
	return new v4l_tuner_t(device_path, fdx);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

v4l_tuner_t::v4l_tuner_t(const std::string& device_path, int fdx) :
	tuner_t(device_path, fdx), g_frontend_fd(-1), g_streamSrc(NULL), g_devNum(INVALID_DEVICE_IDX), g_caHandler(NULL), tuner_type_(dvblink::TUNERTYPE_UNKNOWN)
{
	g_fe_info = new dvb_frontend_info();
}

v4l_tuner_t::~v4l_tuner_t()
{
	delete g_fe_info;
}


bool v4l_tuner_t::v4lStartStreaming()
{
	bool ret_val = false;
	if (g_streamSrc == NULL && g_devNum != INVALID_DEVICE_IDX)
	{
		g_streamSrc = new CV4lTunersStreamSrc();
		ret_val = g_streamSrc->Start(g_devNum, fdx_, &v4l_tuner_t::stream_callback, this);
		if (ret_val)
		{
			//add TDT pid and initialize TDT parser: required for CA operations
			g_streamSrc->AddPid(PID_TDT);
		}
	}
	return ret_val;
}

void v4l_tuner_t::v4lStopStreaming()
{
	if (g_streamSrc != NULL)
	{
		g_streamSrc->Stop();
		delete g_streamSrc;
		g_streamSrc = NULL;
	}
}

static int find_device_by_path(const std::wstring& wdevice_path)
{
    int ret_val = -1;

    if (!wdevice_path.empty())
    {
        log_info(L"find_device_by_path. searching for matching tuner with device path %1%") % wdevice_path;
		dvblink::TDevAPIDevListEx device_list;
		if (DeviceGetList_impl(&device_list) == dvblink::SUCCESS)
		{
			//walk through present devices to match the device path
			for (int i=0; i<device_list.Count; i++)
			{
				if (boost::iequals(device_list.Devices[i].szDevicePathW, wdevice_path))
				{
					//found
					ret_val = device_list.Devices[i].TunerIdx;
					log_info(L"find_device_by_path. Found device index %1% for device path %2%") % ret_val % wdevice_path;
					break;
				}
			}
		}
    }
    return ret_val;
}

int v4l_tuner_t::StartDevice(DL_E_TUNER_TYPES tuner_type)
{
	std::wstring wdevice_path = string_cast<EC_UTF8>(device_path_);
	
	int dwDeviceNo = find_device_by_path(wdevice_path);
	tuner_type_ = tuner_type;
	
	if (dwDeviceNo == -1)
	{
		log_error(L"StartDevice. Could not find the match for %1%") % wdevice_path;
		return dvblink::FAIL;
	}
	
	log_info(L"Starting tuner %d, frontend %d") % dwDeviceNo % fdx_;

	int ret_val = dvblink::FAIL;
	if (g_frontend_fd == -1)
	{
		std::string frontend_path = get_v4l_node_path(dwDeviceNo, fdx_, "frontend");

		g_frontend_fd = open(frontend_path.c_str(), O_RDWR | O_NONBLOCK);
		if (g_frontend_fd != -1)
		{
			if (ioctl(g_frontend_fd, FE_GET_INFO, g_fe_info) != -1)
			{
				log_info(L"tuner %d, frontend %d was opened successfully") % dwDeviceNo % fdx_;
				g_devNum = dwDeviceNo;

				//get product and vendor id
				std::string tuner_name;
				std::string tuner_path;
				std::string serial;
				dvblink::DL_E_TUNER_CONNECTION_TYPE connection_type;
				if (get_tuner_meta_properties(frontend_path.c_str(), tuner_name, tuner_path,
						vendor_id_, product_id_, serial, connection_type))
				{
					//log info about the detected tuner
					std::wstring wpid = string_cast<EC_UTF8>(product_id_);
					std::wstring wvid = string_cast<EC_UTF8>(vendor_id_);
					log_info(L"detected tuner ids: vendor (%1%), product (%2%)") % wvid % wpid;
				}

				//turn LNB power on for dvb-s
				if (g_fe_info->type == FE_QPSK)
				{
					LNBPower(dvblink::POW_13V);
				}

				//try to initialize CA (if present)
				g_caHandler = new CV4lTunerCAHandler();
				if (!g_caHandler->Init(g_devNum, fdx_))
				{
					delete g_caHandler;
					g_caHandler = NULL;
				}

				ret_val = dvblink::SUCCESS;
			} else
			{
				log_error(L"cannot query frontend information for tuner %d, frontend %d") % dwDeviceNo % fdx_;
				close(g_frontend_fd);
				g_frontend_fd = -1;
			}
		} else
		{
			log_error(L"cannot open frontend %d for tuner %d") % fdx_ % dwDeviceNo;
		}
	} else
	{
		log_error(L"frontend is still open. Close it first!");
	}

	return ret_val;
}

int v4l_tuner_t::StopDevice()
{
	log_info(L"Stop device request for tuner %d, frontend %d") % g_devNum % fdx_;
	//stop stream if it is running
	v4lStopStreaming();

	//turn LNB power off for dvb-s
	if (g_fe_info->type == FE_QPSK)
	{
		LNBPower(dvblink::POW_OFF);
	}
	
	//delete CA handler
	if (g_caHandler != NULL)
	{
		g_caHandler->Term();
		delete g_caHandler;
		g_caHandler = NULL;
	}

	if (g_frontend_fd != -1)
	{
		close(g_frontend_fd);
		g_frontend_fd = -1;
	}

	g_devNum = INVALID_DEVICE_IDX;

	return dvblink::SUCCESS;
}

int v4l_tuner_t::CISendPMT(unsigned char *buf, int len, int listmng)
{
	int ret_val = dvblink::FAIL;

	dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd = (dvblink::DL_E_SERVICE_DECRYPTION_CMD)listmng;
	
	if (g_caHandler != NULL)
	{
		if (g_caHandler->SendPMT(buf, len, cmd))
			ret_val = dvblink::SUCCESS;
	}
	return ret_val;
}

static inline void msleep(unsigned int msec)
{
	struct timespec req = { msec / 1000, 1000000 * (msec % 1000) };

	while (nanosleep(&req, &req))
		;
}

bool v4l_tuner_t::IsSignalLocked()
{
	bool ret_val = false;

	fe_status_t status;
	if (ioctl(g_frontend_fd, FE_READ_STATUS, &status) != -1)
	{
		ret_val = (status & FE_HAS_LOCK);
	}

	return ret_val;
}

bool v4l_tuner_t::get_dvbv5_strength(int& strength)
{
	bool ret_val = false;

	dtv_property p;
	p.cmd = DTV_STAT_SIGNAL_STRENGTH;

	dtv_properties props;
	props.num = 1;
	props.props = &p;
	//send clear command
	if ((ioctl(g_frontend_fd, FE_GET_PROPERTY, &props)) >= 0 && p.u.st.len > 0)
	{
		ret_val = true;
		log_ext_info(L"DTV_STAT_SIGNAL_STRENGTH: %1%, %2%, %3%, %4%") % p.u.st.len % p.u.st.stat[0].svalue % p.u.st.stat[0].uvalue % p.u.st.stat[0].scale;

		if (p.u.st.stat[0].scale == FE_SCALE_DECIBEL)
		{
			strength = 100 * pow(10, (p.u.st.stat[0].svalue)/100000.0);

			//stretch it betweem 15 (equals to zero) and 50 (equals to 100)
			strength -= 15;
			strength *= 3.5;
		} else
			if (p.u.st.stat[0].scale == FE_SCALE_RELATIVE)
				strength = (int)((p.u.st.stat[0].uvalue/65535.0)*100);
			else
				ret_val = false;

		if (ret_val)
		{
			if (strength < 0)
				strength = 0;

			if (strength > 100)
				strength = 100;
		}

	} else
	{
		log_ext_info(L"DTV_STAT_SIGNAL_STRENGTH has failed for tuner %d, frontend %d") % g_devNum % fdx_;
	}

	return ret_val;
}

bool v4l_tuner_t::get_dvbv5_quality(int& quality)
{
	bool ret_val = false;

	dtv_property p;
	p.cmd = DTV_STAT_CNR;

	dtv_properties props;
	props.num = 1;
	props.props = &p;
	//send clear command
	if ((ioctl(g_frontend_fd, FE_GET_PROPERTY, &props)) >= 0 && p.u.st.len > 0)
	{
		log_ext_info(L"DTV_STAT_CNR: %1%, %2%, %3%") % p.u.st.len % p.u.st.stat[0].svalue % p.u.st.stat[0].scale;
		ret_val = true;

		if (p.u.st.stat[0].scale == FE_SCALE_DECIBEL)
		{
			quality = 100 * pow(10, (p.u.st.stat[0].svalue)/100000.0);

			//stretch it betweem 130 (equals to zero) and 199 (equals to 100)
			quality -= 130;
		} else
			if (p.u.st.stat[0].scale == FE_SCALE_RELATIVE)
				quality = (int)((p.u.st.stat[0].uvalue/65535.0)*100);
			else
				ret_val = false;

		if (ret_val)
		{
			if (quality < 0)
				quality = 0;

			if (quality > 100)
				quality = 100;
		}

	} else
	{
		log_ext_info(L"DTV_STAT_CNR has failed for tuner %d, frontend %d") % g_devNum % fdx_;
	}

	return ret_val;
}

void v4l_tuner_t::GetSignalInfo(int& strength, int& quality)
{
	strength = 0;
	quality = 0;
	uint16_t i;

	if (!get_dvbv5_strength(strength))
	{
		if (ioctl(g_frontend_fd, FE_READ_SIGNAL_STRENGTH, &i) != -1)
		{
			log_ext_info(L"strength: %1%") % i;

			if (boost::iequals(vendor_id_, "2013") && boost::iequals(product_id_, "025f"))
			{
				//PCTV 292e - mask the lowest byte
				i = (i & 0x00FF);
				strength = (int)((i/255.0)*100);
			} else
			if ((boost::iequals(vendor_id_, "0b48") && (boost::iequals(product_id_, "300d") || boost::iequals(product_id_, "300a"))))
				strength = (int)((i/784.0)*100); //TT 3650 CT and S2
			else
				strength = (int)((i/65535.0)*100);

			if (strength > 100)
				strength = 100;
		} else
		{
			log_error(L"FE_READ_SIGNAL_STRENGTH failed for tuner %d, frontend %d") % g_devNum % fdx_;
		}
	}

	if (!get_dvbv5_quality(quality))
	{
		if (ioctl(g_frontend_fd, FE_READ_SNR, &i) != -1)
		{
			log_ext_info(L"quality: %1%") % i;
			if (boost::iequals(vendor_id_, "2013") || boost::iequals(vendor_id_, "2304") || boost::iequals(vendor_id_, "2040"))
			{
				if (boost::iequals(product_id_, "025f"))
				{
					//PCTV 292e - mask the highest byte
					i = ((i & 0xFF00) >> 8);
					quality = (int)((i/255.0)*100);
				} else
				if (boost::iequals(product_id_, "0258"))
				{
					//PCTV 461e - units unknown
					quality = (int)((i/255.0)*100);
				} else
				if (boost::iequals(product_id_, "0246"))
				{
					quality = (int)((i/8192.0)*100); //for PCTV 74e - kind of decibel, but linearly stretched
				} else
				{
					quality = pow(10, i/100.0); //for PCTV/Hauppauge devices quality is in db
				}
			} else
			{
				if (boost::iequals(vendor_id_, "734c"))
					quality = (int)((i/8192.0)*100); //for TBS - kind of decibel, but linearly stretched
				else
				if (boost::iequals(vendor_id_, "0b48") || boost::iequals(vendor_id_, "0572"))
					quality = i*10; //TT/DVBSky
				else
				if (boost::iequals(vendor_id_, "10b8") || boost::iequals(vendor_id_, "1fa0"))
					quality = (int)((i/255.0)*100); //Geniatech/Mygica isdb-t
				else
					quality = (int)((i/65535.0)*100);
			}
			if (quality > 100)
				quality = 100;
		} else
		{
			log_error(L"FE_READ_SNR failed for tuner %d, frontend %d") % g_devNum % fdx_;
		}
	}
}

bool v4l_tuner_t::SetupLNB(dvblink::PTransponderInfo Tp)
{
	fe_sec_tone_mode_t t;
	t = Tp->dwLnbKHz == dvblink::LNB_SELECTION_22 ? SEC_TONE_ON : SEC_TONE_OFF;
	if (ioctl(g_frontend_fd, FE_SET_TONE, t) == -1)
	{
		log_error(L"set tone has failed for tuner %d, frontend %d") % g_devNum % fdx_;
		return false;
	}
	msleep(50);

	fe_sec_voltage_t v;
	v = Tp->Pol == dvblink::POL_VERTICAL ? SEC_VOLTAGE_13 : SEC_VOLTAGE_18;
	if (ioctl(g_frontend_fd, FE_SET_VOLTAGE, v) == -1)
	{
		log_error(L"set voltage has failed for tuner %d, frontend %d") % g_devNum % fdx_;
		return false;
	}
	usleep(100 * 1000); // wait for at least 15ms (currently 100 ms because of broken drivers)

	return true;
}

int v4l_tuner_t::SetTuner(dvblink::PTransponderInfo Tp)
{
	int ret_val = dvblink::FAIL;

	log_info(L"Tuning request. %d, %d, %d, %d, %d, %d, %d") % Tp->dwFreq % Tp->dwLOF % Tp->dwSr % Tp->dwModulation % Tp->dwLnbKHz % Tp->dwFec % Tp->plp_id;

	//stop streaming
	v4lStopStreaming();

	//Reset CA handler
	if (g_caHandler != NULL)
		g_caHandler->Reset();

	if (g_frontend_fd != -1)
	{
		dtv_property p;
		p.cmd = DTV_CLEAR;
		dtv_properties props;
		props.num = 1;
		props.props = &p;
		//send clear command
		if ((ioctl(g_frontend_fd, FE_SET_PROPERTY, &props)) == -1)
		{
			log_error(L"FE_SET_PROPERTY DTV_CLEAR has failed for tuner %d, frontend %d") % g_devNum % fdx_;
			return dvblink::FAIL;
		}

		//for dvb-s - setup LNB parameters
		if (g_fe_info->type == FE_QPSK)
		{
			if (!SetupLNB(Tp))
				return dvblink::FAIL;
		}

		v4l_transponder_t fe_params;
		
        if (tvsTransponderInfoTov4l(tuner_type_, Tp, &fe_params))
		{
			struct dvb_frontend_event ev;

			const int tune_commands_num = 20;
			dtv_property tune_commands[tune_commands_num];
			int cmd_num = 0;
			tune_commands[cmd_num].cmd = DTV_DELIVERY_SYSTEM; tune_commands[cmd_num].u.data = fe_params.delivery_system; ++cmd_num;
			tune_commands[cmd_num].cmd = DTV_FREQUENCY; tune_commands[cmd_num].u.data = fe_params.frequency; ++cmd_num;
			tune_commands[cmd_num].cmd = DTV_INVERSION; tune_commands[cmd_num].u.data = fe_params.inversion; ++cmd_num;
			switch (tuner_type_)
			{
				case dvblink::TUNERTYPE_DVBS:
					tune_commands[cmd_num].cmd = DTV_SYMBOL_RATE; tune_commands[cmd_num].u.data = fe_params.symbol_rate; ++cmd_num;
					tune_commands[cmd_num].cmd = DTV_INNER_FEC; tune_commands[cmd_num].u.data = fe_params.fec; ++cmd_num;
					tune_commands[cmd_num].cmd = DTV_PILOT; tune_commands[cmd_num].u.data = PILOT_AUTO; ++cmd_num;
					tune_commands[cmd_num].cmd = DTV_MODULATION; tune_commands[cmd_num].u.data = fe_params.modulation; ++cmd_num;
					tune_commands[cmd_num].cmd = DTV_ROLLOFF; tune_commands[cmd_num].u.data = fe_params.rolloff; ++cmd_num;
					break;
				case dvblink::TUNERTYPE_DVBC:
					tune_commands[cmd_num].cmd = DTV_SYMBOL_RATE; tune_commands[cmd_num].u.data = fe_params.symbol_rate; ++cmd_num;
					tune_commands[cmd_num].cmd = DTV_MODULATION; tune_commands[cmd_num].u.data = fe_params.modulation; ++cmd_num;
					tune_commands[cmd_num].cmd = DTV_INNER_FEC; tune_commands[cmd_num].u.data = fe_params.fec; ++cmd_num;
					break;
				case dvblink::TUNERTYPE_DVBT:
					tune_commands[cmd_num].cmd = DTV_BANDWIDTH_HZ; tune_commands[cmd_num].u.data = fe_params.bandwidth; ++cmd_num;
					tune_commands[cmd_num].cmd = DTV_MODULATION; tune_commands[cmd_num].u.data = fe_params.modulation; ++cmd_num;
					if (fe_params.delivery_system == SYS_DVBT2)
					{
						tune_commands[cmd_num].cmd = DTV_ISDBS_TS_ID_LEGACY; tune_commands[cmd_num].u.data = fe_params.plp_id; ++cmd_num; //aka DTV_STREAM_ID in new frontend.h header
					}
					tune_commands[cmd_num].cmd = DTV_CODE_RATE_HP; tune_commands[cmd_num].u.data = FEC_AUTO; ++cmd_num;
					tune_commands[cmd_num].cmd = DTV_CODE_RATE_LP; tune_commands[cmd_num].u.data = FEC_AUTO; ++cmd_num;
					tune_commands[cmd_num].cmd = DTV_TRANSMISSION_MODE; tune_commands[cmd_num].u.data = TRANSMISSION_MODE_AUTO; ++cmd_num;
					tune_commands[cmd_num].cmd = DTV_GUARD_INTERVAL; tune_commands[cmd_num].u.data = GUARD_INTERVAL_AUTO; ++cmd_num;
					tune_commands[cmd_num].cmd = DTV_HIERARCHY; tune_commands[cmd_num].u.data = HIERARCHY_AUTO; ++cmd_num;
					break;
				case dvblink::TUNERTYPE_ATSC:
				case dvblink::TUNERTYPE_CLEARQAM:
					tune_commands[cmd_num].cmd = DTV_BANDWIDTH_HZ; tune_commands[cmd_num].u.data = fe_params.bandwidth; ++cmd_num;
					tune_commands[cmd_num].cmd = DTV_MODULATION; tune_commands[cmd_num].u.data = fe_params.modulation; ++cmd_num;
					break;
			}
			tune_commands[cmd_num].cmd = DTV_TUNE; ++cmd_num;

			props.num = cmd_num;
			props.props = tune_commands;

			if (ioctl(g_frontend_fd, FE_SET_PROPERTY, &props) != -1)
			{
				// wait for zero status indicating start of tunning
				do
				{
					ioctl(g_frontend_fd, FE_GET_EVENT, &ev);
                    usleep(10 * 1000);
				}
                while(ev.status != 0);

				//start streaming (even if no lock is detected - it might happen later)
				v4lStartStreaming();
	            ret_val = dvblink::SUCCESS;
			}
            else
			{
				log_error(L"Setting frontend parameters failed for tuner %d, frontend %d") % g_devNum % fdx_;
			}
		}
        else
		{
			log_error(L"cannot convert transponder info to frontend params for tuner %d, frontend %d") % g_devNum % fdx_;
		}
	}

	return ret_val;
}

void v4l_tuner_t::stream_callback(const unsigned char* buf, unsigned long len, void* user_param)
{
	v4l_tuner_t* parent = (v4l_tuner_t*)user_param;
	
	if (parent->g_caHandler != NULL)
		parent->g_caHandler->process_stream((unsigned char*)buf, len);

	if (parent->callback_ != NULL)
		parent->callback_((unsigned char*)buf, len, parent->callback_param_);
}

int v4l_tuner_t::AddFilter(int pid) // Requests a pid if hardware does HW filtering.
{
	//PID_TDT is always ignored as it is used for internal operations
	if (g_streamSrc != NULL && pid != PID_TDT)
	{
		g_streamSrc->AddPid(pid);
	}

	return dvblink::SUCCESS;
}

int v4l_tuner_t::DelFilter(int pid) // Deletes pid from HW filters
{
	//PID_TDT is always ignored as it is used for internal operations
	if (g_streamSrc != NULL && pid != PID_TDT)
	{
		g_streamSrc->DeletePid(pid);
	}

	return dvblink::SUCCESS;
}

int v4l_tuner_t::GetTunerState(dvblink::PSignalInfo TunerState, dvblink::PTransponderInfo Tp)
{
	TunerState->Locked = IsSignalLocked();
	int strength = 0;
	int quality = 0;
	if (TunerState->Locked)
		GetSignalInfo(strength, quality);

	TunerState->Level = strength;
	TunerState->Quality = quality;

	return dvblink::SUCCESS;
}

int v4l_tuner_t::LNBPower(dvblink::DL_E_LNB_POWER_TYPES lnb_power)
{
	fe_sec_voltage_t v = SEC_VOLTAGE_OFF;
	switch (lnb_power)
	{
		case dvblink::POW_OFF:
			v = SEC_VOLTAGE_OFF;
			break;
		case dvblink::POW_13V:
			v = SEC_VOLTAGE_13;
			break;
		case dvblink::POW_18V:
			v = SEC_VOLTAGE_18;
			break;
	}
	if (ioctl(g_frontend_fd, FE_SET_VOLTAGE, v) == -1)
	{
		log_error(L"set voltage has failed for tuner %d, frontend %d") % g_devNum % fdx_;
		return false;
	}
	msleep(15);
	return dvblink::SUCCESS;
}

int v4l_tuner_t::SendDiseqc(dvblink::PDiseqcCmd RawDiseqc, int ToneBurst)
{
	fe_sec_tone_mode_t t = SEC_TONE_OFF;
	if (ioctl(g_frontend_fd, FE_SET_TONE, t) == -1)
	{
		log_error(L"set tone has failed for tuner %d, frontend %d") % g_devNum % fdx_;
		return false;
	}
	usleep(50000);

	fe_sec_voltage_t volt = SEC_VOLTAGE_18;
	// configure LNB voltage
	if ( ioctl( g_frontend_fd, FE_SET_VOLTAGE, volt ) < 0 )
	{
		log_error(L"set voltage has failed for tuner %d") % g_devNum;
		return false;
	}

	usleep(100 * 1000); // wait for at least 15ms (currently 100 ms because of broken drivers)

	int ret_val = dvblink::FAIL;
	if (ToneBurst != dvblink::TONEBURST_NONE)
	{
		//send tone burst - simple diseqc switching
		if (ioctl(g_frontend_fd, FE_DISEQC_SEND_BURST, tvsToneBurstTov4l(ToneBurst)) != -1)
		{
			//wait a bit
			usleep(100 * 1000);
			ret_val = dvblink::SUCCESS;
		} else
		{
			log_error(L"send burst failed for tuner %d, frontend %d") % g_devNum % fdx_;
		}
	} else
	{
		if (RawDiseqc != NULL)
		{
		    std::wostringstream ostr;
			ostr << L"SendDiseqc()  ";

			for(int i = 0; i<RawDiseqc->iLength; i++)
				ostr << std::setw( 2 ) << std::setfill(L'0') << std::hex << std::uppercase << (int)RawDiseqc->Data[i];
			ostr << L" Len:" << RawDiseqc->iLength;
			log_info(L"%s") % ostr.str().c_str();

			dvb_diseqc_master_cmd cmd;
			for (int i=0; i<RawDiseqc->iLength; i++)
				cmd.msg[i] = RawDiseqc->Data[i];
			cmd.msg_len = RawDiseqc->iLength;
			//send diseqc command
			if (ioctl(g_frontend_fd, FE_DISEQC_SEND_MASTER_CMD, &cmd) != -1)
			{
				//wait a bit
				usleep(100 * 1000);
/*
				//repeat diseqc command for PCTV 461e tuner
				if (boost::iequals(vendor_id_, "2013") && boost::iequals(product_id_, "0258"))
				{
					if (ioctl(g_frontend_fd, FE_DISEQC_SEND_MASTER_CMD, &cmd) != -1)
					{
						//wait a bit
						usleep(100 * 1000);
					}
				}
*/
				ret_val = dvblink::SUCCESS;
			} else
			{
				log_error(L"send diseqc failed for tuner %d, frontend %d") % g_devNum % fdx_;
			}
		}
	}

	return ret_val;
}

// $Id: $
