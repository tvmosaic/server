/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <string.h>
#include <sstream>
#include <boost/filesystem.hpp>
#include "v4ltypes_conv.h"

unsigned long v4lTunerTypeTotvs(fe_type_t frontend_type)
{
	unsigned long ret_val = dvblink::TUNERTYPE_DVBT;
	switch (frontend_type)
	{
	case FE_QPSK:
		ret_val = dvblink::TUNERTYPE_DVBS;
		break;
	case FE_QAM:
		ret_val = dvblink::TUNERTYPE_DVBC;
		break;
	case FE_OFDM:
		ret_val = dvblink::TUNERTYPE_DVBT;
		break;
	case FE_ATSC:
		ret_val = dvblink::TUNERTYPE_ATSC;
		break;
	}
	return ret_val;
}

fe_sec_mini_cmd_t tvsToneBurstTov4l(unsigned long diseqc_mini)
{
	fe_sec_mini_cmd_t ret_val = SEC_MINI_A;
	switch (diseqc_mini)
	{
	case dvblink::TONEBURST_1:
		ret_val = SEC_MINI_A;
		break;
	case dvblink::TONEBURST_2:
		ret_val = SEC_MINI_B;
		break;
	}
	return ret_val;
}

fe_delivery_system_t tvsDeliverySystemTov4l(dvblink::DL_E_TUNER_TYPES tuner_type, dvblink::PTransponderInfo tr_info)
{
	fe_delivery_system_t ret_val = SYS_UNDEFINED;

	switch (tuner_type)
	{
	case dvblink::TUNERTYPE_DVBS:
		ret_val = tr_info->dwModulation == dvblink::MOD_DVBS_QPSK ? SYS_DVBS : SYS_DVBS2;
		break;
	case dvblink::TUNERTYPE_DVBC:
		ret_val = SYS_DVBC_ANNEX_AC;
		break;
	case dvblink::TUNERTYPE_DVBT:
		ret_val = tr_info->plp_id == 0 ? SYS_DVBT : SYS_DVBT2;
		break;
	case dvblink::TUNERTYPE_ATSC:
		ret_val = SYS_ATSC;
		break;
	case dvblink::TUNERTYPE_CLEARQAM:
		ret_val = SYS_DVBC_ANNEX_B;
		break;
	default:
		ret_val = SYS_UNDEFINED;
		break;
	}
	return ret_val;
}

fe_modulation_t tvsDVBSModulationTov4l(unsigned long modulation)
{
	fe_modulation_t ret_val = QPSK;

	switch (modulation)
	{
	case dvblink::MOD_DVBS_8PSK:
		ret_val = PSK_8;
		break;
	case dvblink::MOD_DVBS_16APSK:
		ret_val = APSK_16;
		break;
	case dvblink::MOD_DVBS_32APSK:
		ret_val = APSK_32;
		break;
	case dvblink::MOD_DVBS_NBC_QPSK:
		ret_val = QPSK;
		break;
	case dvblink::MOD_TURBO_8PSK:
	case dvblink::MOD_TURBO_QPSK:
	case dvblink::MOD_DVBS_QPSK:
	default:
		ret_val = QPSK;
		break;
	}

	return ret_val;
}

unsigned int tvsBandwidthTov4l(unsigned long bndw)
{
	unsigned int ret_val = 0;
	switch (bndw)
	{
	case 6:
		ret_val = 6000000;
		break;
	case 7:
		ret_val = 7000000;
		break;
	case 8:
		ret_val = 8000000;
		break;
	default:
		ret_val = 0;
		break;
	}
	return ret_val;
}

fe_modulation_t tvsQAMModulationTov4l(unsigned long modulation)
{
	fe_modulation_t ret_val = QAM_16;

	switch (modulation)
	{
	case dvblink::MOD_DVBC_QAM_16:
		ret_val = QAM_16;
		break;
	case dvblink::MOD_DVBC_QAM_32:
		ret_val = QAM_32;
		break;
	case dvblink::MOD_DVBC_QAM_64:
		ret_val = QAM_64;
		break;
	case dvblink::MOD_DVBC_QAM_128:
		ret_val = QAM_128;
		break;
	case dvblink::MOD_DVBC_QAM_256:
		ret_val = QAM_256;
		break;
	case dvblink::MOD_DVBC_QAM_4:
	default:
		ret_val = QAM_AUTO;
		break;
	}

	return ret_val;
}

fe_code_rate_t tvsFECTov4l(unsigned long fec)
{
	fe_code_rate_t ret_val = FEC_AUTO;

	switch (fec)
	{
	case dvblink::FEC_AUTO:
		ret_val = FEC_AUTO;
		break;
	case dvblink::FEC_1_2:
		ret_val = FEC_1_2;
		break;
	case dvblink::FEC_2_3:
		ret_val = FEC_2_3;
		break;
	case dvblink::FEC_3_4:
		ret_val = FEC_3_4;
		break;
	case dvblink::FEC_4_5:
		ret_val = FEC_4_5;
		break;
	case dvblink::FEC_5_6:
		ret_val = FEC_5_6;
		break;
	case dvblink::FEC_6_7:
		ret_val = FEC_6_7;
		break;
	case dvblink::FEC_7_8:
		ret_val = FEC_7_8;
		break;
	case dvblink::FEC_8_9:
		ret_val = FEC_8_9;
		break;
	case dvblink::FEC_9_10:
		ret_val = FEC_9_10;
		break;
	case dvblink::FEC_3_5:
		ret_val = FEC_3_5;
		break;
	default:
		ret_val = FEC_AUTO;
		break;
	}

	return ret_val;
}

unsigned long get_frequency_from_atsc_channel(int channel)
{
	switch (channel)
	{
		case 2 : return  57028615;
		case 3 : return  63028615;
		case 4 : return  69028615;
		case 5 : return  79028615;
		case 6 : return  85028615;
		case 7 : return 177028615;
		case 8 : return 183028615;
		case 9 : return 189028615;
		case 10 : return 195028615;
		case 11 : return 201028615;
		case 12 : return 207028615;
		case 13 : return 213028615;
		case 14 : return 473028615;
		case 15 : return 479028615;
		case 16 : return 485028615;
		case 17 : return 491028615;
		case 18 : return 497028615;
		case 19 : return 503028615;
		case 20 : return 509028615;
		case 21 : return 515028615;
		case 22 : return 521028615;
		case 23 : return 527028615;
		case 24 : return 533028615;
		case 25 : return 539028615;
		case 26 : return 545028615;
		case 27 : return 551028615;
		case 28 : return 557028615;
		case 29 : return 563028615;
		case 30 : return 569028615;
		case 31 : return 575028615;
		case 32 : return 581028615;
		case 33 : return 587028615;
		case 34 : return 593028615;
		case 35 : return 599028615;
		case 36 : return 605028615;
		case 37 : return 611028615;
		case 38 : return 617028615;
		case 39 : return 623028615;
		case 40 : return 629028615;
		case 41 : return 635028615;
		case 42 : return 641028615;
		case 43 : return 647028615;
		case 44 : return 653028615;
		case 45 : return 659028615;
		case 46 : return 665028615;
		case 47 : return 671028615;
		case 48 : return 677028615;
		case 49 : return 683028615;
		case 50 : return 689028615;
		case 51 : return 695028615;
		case 52 : return 701028615;
		case 53 : return 707028615;
		case 54 : return 713028615;
		case 55 : return 719028615;
		case 56 : return 725028615;
		case 57 : return 731028615;
		case 58 : return 737028615;
		case 59 : return 743028615;
		case 60 : return 749028615;
		case 61 : return 755028615;
		case 62 : return 761028615;
		case 63 : return 767028615;
		case 64 : return 773028615;
		case 65 : return 779028615;
		case 66 : return 785028615;
		case 67 : return 791028615;
		case 68 : return 797028615;
		case 69 : return 803028615;
	}
	return 0;
}

bool tvsTransponderInfoTov4l(dvblink::DL_E_TUNER_TYPES tuner_type, dvblink::PTransponderInfo tr_info, v4l_transponder_t* v4l_tr)
{
	bool ret_val = true;
	memset(v4l_tr, 0, sizeof(v4l_transponder_t));

	v4l_tr->inversion = INVERSION_AUTO;
	v4l_tr->delivery_system = tvsDeliverySystemTov4l(tuner_type, tr_info);
	switch (tuner_type)
	{
	case dvblink::TUNERTYPE_DVBS: 	//DVB-S/S2
		v4l_tr->frequency = tr_info->dwFreq - tr_info->dwLOF;
		v4l_tr->modulation = tvsDVBSModulationTov4l(tr_info->dwModulation);
		v4l_tr->fec = tvsFECTov4l(tr_info->dwFec);
		v4l_tr->symbol_rate = tr_info->dwSr;
		v4l_tr->rolloff = ROLLOFF_35;
		break;
	case dvblink::TUNERTYPE_DVBC:	//DVB-C
		v4l_tr->frequency = tr_info->dwFreq*1000;
		v4l_tr->fec = FEC_AUTO;
		v4l_tr->symbol_rate = tr_info->dwSr;
		v4l_tr->modulation = tvsQAMModulationTov4l(tr_info->dwLOF);
		break;
	case dvblink::TUNERTYPE_DVBT:	//DVB-T
		v4l_tr->frequency = tr_info->dwFreq*1000;
		v4l_tr->bandwidth = tvsBandwidthTov4l(tr_info->dwLOF);
		v4l_tr->modulation = QAM_AUTO;
		if (v4l_tr->delivery_system == SYS_DVBT2)
			v4l_tr->plp_id = tr_info->plp_id - 1;
		break;
	case dvblink::TUNERTYPE_CLEARQAM: //ClearQAM
		v4l_tr->frequency = tr_info->dwFreq*1000;
		v4l_tr->bandwidth = 6000000; //? needed?
		v4l_tr->modulation = tvsQAMModulationTov4l(tr_info->dwLOF);
		break;
	case dvblink::TUNERTYPE_ATSC: //ATSC tuner
		v4l_tr->frequency = get_frequency_from_atsc_channel(tr_info->dwFreq);
		v4l_tr->bandwidth = 6000000; //hardcoded by the standard
		v4l_tr->modulation = VSB_8;
		break;
	default:
		ret_val = false;
		break;
	}
	return ret_val;
}

std::string get_v4l_node_path(int device_idx, int fe_idx, const char* node)
{
    //try android path first: /dev/dvbX.frontendX
    std::stringstream buf;
    buf.clear(); buf.str("");
    buf << "/dev/dvb" << device_idx << "." << node << fe_idx;

    if (!boost::filesystem::exists(buf.str()))
    {
        //use standard v4l notation otherwise
        buf.clear(); buf.str("");
        buf << "/dev/dvb/adapter" << device_idx << "/" << node << fe_idx;
    }

    return buf.str();
}
