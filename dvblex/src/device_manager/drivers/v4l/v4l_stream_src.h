/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <boost/thread.hpp>
#include <dl_ts_aligner.h>
#include <drivers/deviceapi.h>

class CV4lTunersStreamSrc
{
public:
    typedef void (*stream_callback_f)(const unsigned char* buf, unsigned long len, void* user_param);

	CV4lTunersStreamSrc();
	~CV4lTunersStreamSrc();

	bool Start(unsigned long deviceIdx, unsigned long frontend_idx, stream_callback_f cb, void* user_param);
	void Stop();
	void AddPid(unsigned short pid);
	void DeletePid(unsigned short pid);

protected:
	boost::thread* m_streamThread;
	stream_callback_f m_callback;
	void* m_user_param;
	unsigned long m_idx;
	unsigned long m_frontend_idx;
	bool m_stopFlag;
	unsigned char m_packetBuffer[188];
	int m_fragmentLength;
	unsigned char* m_readBuf;
	int m_readBufSize;
	dvblink::engine::ts_packet_aligner packet_aligner_;
	boost::mutex pids_lock_;
	std::vector<unsigned short> pids_to_add_;
	std::vector<unsigned short> pids_to_delete_;

	void StreamThreadFunction();
	static void  __stdcall filter_ts_callback(const unsigned char* buf, unsigned long len, void* user_param);
};
