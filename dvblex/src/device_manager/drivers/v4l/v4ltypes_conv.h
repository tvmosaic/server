/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <frontend.h>
#include <drivers/deviceapi.h>

struct v4l_transponder_t
{
	fe_delivery_system_t delivery_system;
	unsigned int frequency;
	fe_modulation_t modulation;
	unsigned int symbol_rate;
	fe_code_rate_t fec;
	fe_spectral_inversion_t inversion;
	fe_rolloff_t rolloff;
	unsigned int bandwidth;
	int plp_id;
};

unsigned long v4lTunerTypeTotvs(fe_type_t frontend_type);
fe_delivery_system_t tvsDeliverySystemTov4l(dvblink::DL_E_TUNER_TYPES tuner_type, unsigned long modulation);
bool tvsTransponderInfoTov4l(dvblink::DL_E_TUNER_TYPES tuner_type, dvblink::PTransponderInfo tr_info, v4l_transponder_t* v4l_tr);
fe_sec_mini_cmd_t tvsToneBurstTov4l(unsigned long diseqc_mini);

std::string get_v4l_node_path(int device_idx, int fe_idx, const char* node);