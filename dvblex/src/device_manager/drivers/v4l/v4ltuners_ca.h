/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#include <boost/thread.hpp>
#include <vector>
#include <map>
#include <dl_ts_info.h>
#include <dl_command_queue.h>
#include <drivers/deviceapi.h>

//forward declarations
struct en50221_transport_layer;
struct en50221_session_layer;
struct en50221_stdcam;
struct en50221_app_pmt_reply;

typedef std::map<unsigned short, std::vector<unsigned char> > pmt_services_map_t;

class CV4lTunerCAHandler
{
protected:
	enum commands_e {EC_PROCESS_CA_REQUEST, CA_RESET};

    struct process_ca_request_data
    {
		unsigned char* pmt;
		int pmt_len;
		dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd;
		bool res;
    };
		
public:
	CV4lTunerCAHandler();
	~CV4lTunerCAHandler();

	bool Init(int deviceIdx, int frontend_idx);
	bool Term();
	bool SendPMT(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd);
	bool Reset();
	void process_stream(unsigned char* buf, int len);

protected:
	en50221_transport_layer* m_tl;
	en50221_session_layer* m_sl;
	en50221_stdcam* m_stdcam;
	int m_deviceIdx;
	int m_frontendIdx;
	bool m_StopFlag;
	boost::thread* m_thread;
	bool m_ciModulePresent;
	dvblink::engine::ts_section_payload_parser tdt_parser_;

	static int ca_info_callback(void *arg, unsigned char slot_id, unsigned short session_number, unsigned int ca_id_count,
			unsigned short *ca_ids);

	static int ai_callback(void *arg, unsigned char slot_id, unsigned short session_number,
				     unsigned char application_type, unsigned short application_manufacturer,
				     unsigned short manufacturer_code, unsigned char menu_string_length,
				     unsigned char *menu_string);

	static int ca_pmt_reply_callback(void *arg, unsigned char slot_id, unsigned short session_number,
			en50221_app_pmt_reply* reply, unsigned int reply_size);

	void CamThreadFunc();
	void send_pmt_to_cam(unsigned char* pmt, int pmt_len, int list_management, int cmd_id);
	void reset_cam();

	bool b_resend_pmts_;
    dvblink::engine::command_queue command_queue_;
	pmt_services_map_t current_services_;
	void process_ca_request(process_ca_request_data* request_params, bool& bfirst);
	void resend_all_pmts();
	void remove_all_pmts();
};
