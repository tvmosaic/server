/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "v4ltuners_ca.h"
#include <libdvben50221/en50221_stdcam.h>
#include <ca.h>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_ts.h>
#include "v4l_capmt.h"
#include "v4ltypes_conv.h"

using namespace dvblink::logging;
using namespace dvblink::engine;

CV4lTunerCAHandler::CV4lTunerCAHandler() :
	m_tl(NULL),
	m_sl(NULL),
	m_stdcam(NULL),
	m_thread(NULL)
{

}

CV4lTunerCAHandler::~CV4lTunerCAHandler()
{

}

struct en50221_stdcam_llci
{
	struct en50221_stdcam stdcam;
	int cafd;
	int slotnum;
	int state;
};

void CV4lTunerCAHandler::reset_cam()
{
	struct en50221_stdcam_llci *llci = (struct en50221_stdcam_llci *) m_stdcam;
	ioctl(llci->cafd, CA_RESET, (1 << llci->slotnum));
}

bool CV4lTunerCAHandler::Init(int deviceIdx, int frontend_idx)
{
	bool ret_val = false;

	
	std::string ca_path = get_v4l_node_path(deviceIdx, 0, "ca");
	
	//check if ca0 file node exists
	struct stat statbuf;
	if (stat(ca_path.c_str(), &statbuf) < 0)
	{
		log_info(L"No ca0 node is present for tuner %d") % deviceIdx;
		return false;
	}
	
	m_deviceIdx = deviceIdx;
	m_frontendIdx = frontend_idx;

	// create transport layer
	m_tl = en50221_tl_create(1, 16);
	if (m_tl != NULL)
	{
		// create session layer
		m_sl = en50221_sl_create(m_tl, 16);
		if (m_sl != NULL)
		{
			// create the stdcam instance
			m_stdcam = en50221_stdcam_create(m_deviceIdx, 0, m_tl, m_sl); //slot is always 0 - we assume that moost cards have only one CA slot
			if (m_stdcam != NULL)
			{
				// hook up the AI callbacks
				if (m_stdcam->ai_resource)
					en50221_app_ai_register_callback(m_stdcam->ai_resource, ai_callback, this);

				// hook up the CA callbacks
				if (m_stdcam->ca_resource)
				{
					en50221_app_ca_register_info_callback(m_stdcam->ca_resource, ca_info_callback, this);
					en50221_app_ca_register_pmt_reply_callback(m_stdcam->ca_resource, ca_pmt_reply_callback, this);
				}

				//init TDT payload parser
				tdt_parser_.Init(PID_TDT);
				// start the cam thread
				m_StopFlag = false;
				m_thread = new boost::thread(boost::bind(&CV4lTunerCAHandler::CamThreadFunc, this));

				//reset CAM
				reset_cam();

				ret_val = true;
			} else
			{
				log_warning(L"Failed to create stdcam. tuner %d, ca0") % m_deviceIdx;
				en50221_sl_destroy(m_sl);
				en50221_tl_destroy(m_tl);
			}

		} else
		{
			log_error(L"Failed to create session layer. tuner %d") % m_deviceIdx;
			en50221_tl_destroy(m_tl);
		}
	} else
	{
		log_error(L"Failed to create transport layer. tuner %d") % m_deviceIdx;
	}

	if (!ret_val)
	{
		m_tl = NULL;
		m_sl = NULL;
		m_stdcam = NULL;
	}

	return ret_val;
}

bool CV4lTunerCAHandler::Term()
{
	if (m_stdcam == NULL)
		return false;

	Reset();
		
	// shutdown the cam thread
	m_StopFlag = true;
	m_thread->join();
	delete m_thread;
	m_thread = NULL;

	// destroy the stdcam
	if (m_stdcam->destroy)
		m_stdcam->destroy(m_stdcam, 1);
	m_stdcam = NULL;

	// destroy session layer
	en50221_sl_destroy(m_sl);
	m_sl = NULL;

	// destroy transport layer
	en50221_tl_destroy(m_tl);
	m_tl = NULL;


	return true;
}

bool CV4lTunerCAHandler::Reset()
{
	if (m_stdcam == NULL)
		return false;

    command_queue_.ExecuteCommand(CA_RESET, NULL);
	return true;
}

void CV4lTunerCAHandler::process_stream(unsigned char* buf, int len)
{
	if (m_stdcam != NULL && m_stdcam->dvbtime != NULL)
	{
		int c = len / TS_PACKET_SIZE;
		for (int i=0; i<c; i++)
		{
			unsigned char* packet = buf + i*TS_PACKET_SIZE;
			if (ts_process_routines::GetPacketPID(packet) == PID_TDT)
			{
				ts_payload_parser::ts_section_list found_sections;
				if (tdt_parser_.AddPacket(packet, TS_PACKET_SIZE, found_sections))
				{
					for (unsigned int ii=0; ii<found_sections.size(); ii++)
					{
						time_t curtime;
						if (ts_process_routines::GetUTCTimeFromTDT(found_sections[ii].section, found_sections[ii].length, curtime))
						{
							m_stdcam->dvbtime(m_stdcam, curtime);
							log_ext_info(L"Sent current time (%1%) for tuner %2%") % curtime % m_deviceIdx;
						}
					}
					tdt_parser_.ResetFoundSections(found_sections);
				}
			}
		}
	}
}

bool CV4lTunerCAHandler::SendPMT(unsigned char *buf, int len, dvblink::DL_E_SERVICE_DECRYPTION_CMD cmd)
{
	if (m_stdcam == NULL)
		return false;

	process_ca_request_data params;
	params.pmt = buf;
	params.pmt_len = len;
	params.cmd = cmd;

    command_queue_.ExecuteCommand(EC_PROCESS_CA_REQUEST, &params);
	
	return params.res;
}

void CV4lTunerCAHandler::send_pmt_to_cam(unsigned char* pmt, int pmt_len, int list_management, int cmd_id)
{
	if (m_ciModulePresent)
	{
		int capmtbuf_size = 4096;
		unsigned char capmt_buf[4096];
		CreateCAPMTFromPMT(pmt, pmt_len, list_management, cmd_id, capmt_buf, capmtbuf_size);

		// set it
		if (en50221_app_ca_pmt(m_stdcam->ca_resource, m_stdcam->ca_session_number, capmt_buf, capmtbuf_size) == 0)
		{
			log_info(L"send_pmt_to_cam: success. tuner %1%; (%2%, %3%) ") % m_deviceIdx % list_management % cmd_id;
		} else
		{
			log_error(L"send_pmt_to_cam: Failed to send PMT. tuner %1%; (%2%, %3%)") % m_deviceIdx % list_management % cmd_id;
		}
	}
}

int CV4lTunerCAHandler::ca_info_callback(void *arg, unsigned char slot_id, unsigned short session_number, unsigned int ca_id_count,
		unsigned short *ca_ids)
{
	CV4lTunerCAHandler* parent = (CV4lTunerCAHandler*)arg;

	log_info(L"CAM supports the following ca system ids:");

	for(unsigned int i=0; i < ca_id_count; i++)
		log_info(L"0x%04x") % ca_ids[i];

	parent->m_ciModulePresent = true;
	//request resending the pmts
	parent->b_resend_pmts_ = true;

	return 0;
}

int CV4lTunerCAHandler::ai_callback(void *arg, unsigned char slot_id, unsigned short session_number,
			     unsigned char application_type, unsigned short application_manufacturer,
			     unsigned short manufacturer_code, unsigned char menu_string_length,
			     unsigned char *menu_string)
{
	std::wstring str;
	dvblink::engine::ConvertMultibyteToUC(dvblink::engine::EC_ISO_8859_1, (const char*)menu_string, menu_string_length, str);
	log_info(L"CAM Application type: %02x") % application_type;
	log_info(L"CAM Application manufacturer: %04x") % application_manufacturer;
	log_info(L"CAM Manufacturer code: %04x") % manufacturer_code;
	log_info(L"CAM Menu string: %.*s") % str.c_str();

	return 0;
}

int CV4lTunerCAHandler::ca_pmt_reply_callback(void *arg, unsigned char slot_id, unsigned short session_number,
		en50221_app_pmt_reply* reply, unsigned int reply_size)
{
	log_info(L"CAM PMT callback");
	log_info(L"Program number %d") % reply->program_number;

	switch(reply->CA_enable)
	{
	case CA_ENABLE_DESCRAMBLING_POSSIBLE:
		log_info(L"Descrambling possible");
		break;
	case CA_ENABLE_DESCRAMBLING_POSSIBLE_PURCHASE:
		log_info(L"Descrambling possible under conditions (purchase dialogue)");
		break;
	case CA_ENABLE_DESCRAMBLING_POSSIBLE_TECHNICAL:
		log_info(L"Descrambling possible under conditions (technical dialog)");
		break;
	case CA_ENABLE_DESCRAMBLING_NOT_POSSIBLE_NO_ENTITLEMENT:
		log_info(L"Descrambling not possible (no entitlement)");
		break;
	case CA_ENABLE_DESCRAMBLING_NOT_POSSIBLE_TECHNICAL:
		log_info(L"Descrambling not possible (for technical reasons)");
		break;
	default:
		log_info(L"unknown ca_enable");
	}

	return 0;
}

void CV4lTunerCAHandler::resend_all_pmts()
{
	bool bfirst = true;
	pmt_services_map_t::iterator it = current_services_.begin();
	while (it != current_services_.end())
	{
		int list_mgmt = bfirst ? V4L_CA_LIST_MANAGEMENT_ONLY : V4L_CA_LIST_MANAGEMENT_ADD;
		send_pmt_to_cam(it->second.data(), it->second.size(), list_mgmt, V4L_CA_PMT_CMD_ID_OK_DESCRAMBLING);
		
		bfirst = false;
		
		++it;
	}
}

void CV4lTunerCAHandler::remove_all_pmts()
{
	pmt_services_map_t::iterator it = current_services_.begin();
	while (it != current_services_.end())
	{
		send_pmt_to_cam(it->second.data(), it->second.size(), V4L_CA_LIST_MANAGEMENT_UPDATE, V4L_CA_PMT_CMD_ID_NOT_SELECTED);
		++it;
	}
	
	current_services_.clear();
}

void CV4lTunerCAHandler::process_ca_request(process_ca_request_data* request_params, bool& bfirst)
{
	//get service id from pmt
	unsigned short sid;
	ts_process_routines::GetPMTSectionServiceID(request_params->pmt, request_params->pmt_len, sid);
	switch (request_params->cmd)
	{
	case dvblink::SERVICE_DECRYPT_ADD:
		{
			log_info(L"CV4lTunerCAHandler::process_ca_request. Add program %1%") % sid;

			int list_mgmt = V4L_CA_LIST_MANAGEMENT_ADD;
			if (bfirst)
			{
				//first service after channel change request
				list_mgmt = V4L_CA_LIST_MANAGEMENT_ONLY;
				bfirst = false;
			}
		
			send_pmt_to_cam(request_params->pmt, request_params->pmt_len, list_mgmt, V4L_CA_PMT_CMD_ID_OK_DESCRAMBLING);
			//add this pmt to the list of current services
			std::vector<unsigned char> pmt_vec;
			pmt_vec.assign(request_params->pmt, request_params->pmt + request_params->pmt_len);
			current_services_[sid] = pmt_vec;
		}
		break;
	case dvblink::SERVICE_DECRYPT_UPDATE:
		{
			log_info(L"CV4lTunerCAHandler::process_ca_request. Update program %1%") % sid;

			send_pmt_to_cam(request_params->pmt, request_params->pmt_len, V4L_CA_LIST_MANAGEMENT_UPDATE, V4L_CA_PMT_CMD_ID_OK_DESCRAMBLING);
			//add this pmt to the list of current services
			std::vector<unsigned char> pmt_vec;
			pmt_vec.assign(request_params->pmt, request_params->pmt + request_params->pmt_len);
			current_services_[sid] = pmt_vec;
		}
		break;
	case dvblink::SERVICE_DECRYPT_REMOVE:
		log_info(L"CV4lTunerCAHandler::process_ca_request. Remove program %1%") % sid;
			
		send_pmt_to_cam(request_params->pmt, request_params->pmt_len, V4L_CA_LIST_MANAGEMENT_UPDATE, V4L_CA_PMT_CMD_ID_NOT_SELECTED);
		//remove this pmt from the currently selected services
		current_services_.erase(sid);
		break;
	default:
		break;
	}
}

void CV4lTunerCAHandler::CamThreadFunc()
{
	en50221_stdcam_status prev_status = EN50221_STDCAM_CAM_NONE;
	m_ciModulePresent = false;
	b_resend_pmts_ = false;
	bool bfirst = true;

	while(!m_StopFlag)
	{
		SDLCommandItem* item;
		while (command_queue_.PeekCommand(&item))
		{
			switch (item->id)
			{
			case EC_PROCESS_CA_REQUEST:
				{
					process_ca_request_data* request_params = (process_ca_request_data*)item->param;
					process_ca_request(request_params, bfirst);
					request_params->res = true;
				}
				break;
			case CA_RESET:
				remove_all_pmts();
				bfirst = true;
				break;
			default:
				break;
			}
			command_queue_.FinishCommand(&item);
		}

		en50221_stdcam_status status = m_stdcam->poll(m_stdcam);
		if (status != prev_status)
		{
			log_info(L"CAM status changed: %1%") % status;

			//if CAM becomes OK, resend PMT information (if any)
			if (status == EN50221_STDCAM_CAM_OK)
			{
//				log_info(L"requesting PMT re-send");
			}

			prev_status = status;
		}

		//reset CI presence flag if CAM is not in OK state
		if (status != EN50221_STDCAM_CAM_OK)
			m_ciModulePresent = false;

		if (b_resend_pmts_)
		{
			log_info(L"Resending PMTs. Tuner %1%") % m_deviceIdx;
			b_resend_pmts_ = false;
			resend_all_pmts();
		}

		usleep(100000);
	}
}
