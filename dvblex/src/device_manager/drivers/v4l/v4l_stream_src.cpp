/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <unistd.h>
#include <dmx.h>
#include <dl_logger.h>
#include <dl_ts.h>
#include <dl_ts_info.h>
#include "v4l_stream_src.h"
#include "v4ltypes_conv.h"

using namespace dvblink::logging;
using namespace dvblink::engine;

CV4lTunersStreamSrc::CV4lTunersStreamSrc()
	: m_streamThread(NULL),
	  m_callback(NULL),
	  packet_aligner_(&filter_ts_callback, this)
{
	m_readBufSize = 188*100;
	m_readBuf = new unsigned char[m_readBufSize];
}

CV4lTunersStreamSrc::~CV4lTunersStreamSrc()
{
	Stop();
	delete m_readBuf;
}

bool CV4lTunersStreamSrc::Start(unsigned long deviceIdx, unsigned long frontend_idx, stream_callback_f cb, void* user_param)
{
	if (cb == NULL)
		return false;

	//stop streaming if it is running already
	if (m_streamThread != NULL)
		Stop();

	m_fragmentLength = 0;
	m_idx = deviceIdx;
	m_callback = cb;
	m_user_param = user_param;
	m_frontend_idx = frontend_idx;

	m_stopFlag = false;
	m_streamThread = new boost::thread(boost::bind(&CV4lTunersStreamSrc::StreamThreadFunction, this));

    pthread_t thread_id = (pthread_t)m_streamThread->native_handle();
    int policy = SCHED_RR;
    sched_param param;
    param.sched_priority =  sched_get_priority_min(policy) + (sched_get_priority_max(policy) - sched_get_priority_min(policy))*3/4;
    int retcode;
    if ((retcode = pthread_setschedparam(thread_id, policy, &param)) != 0)
    {
        log_warning(L"CV4lTunersStreamSrc::Start. pthread_setschedparam failed %1%") % retcode;
    }

	return true;
}

void CV4lTunersStreamSrc::Stop()
{
	if (m_streamThread != NULL)
	{
		m_stopFlag = true;
		m_streamThread->join();
		delete m_streamThread;
		m_streamThread = NULL;
	}
	{
		boost::unique_lock<boost::mutex> lock(pids_lock_);
		pids_to_add_.clear();
	}
}

void CV4lTunersStreamSrc::AddPid(unsigned short pid)
{
	boost::unique_lock<boost::mutex> lock(pids_lock_);
	pids_to_add_.push_back(pid);
}

void CV4lTunersStreamSrc::DeletePid(unsigned short pid)
{
	boost::unique_lock<boost::mutex> lock(pids_lock_);
	pids_to_delete_.push_back(pid);
}

typedef std::map<unsigned short, int> demux_handles_map_t;

void CV4lTunersStreamSrc::StreamThreadFunction()
{
	log_info(L"Starting streaming thread for tuner %d, frontend %d") % m_idx % m_frontend_idx;

	int r;
	
	//set shared as default
	int dvr_idx = 0;
	int demux_idx = 0;
	struct stat statbuf;
	//check whether dvr and demux devices are shared or dedicated
	
		//dvr
	std::string dvr_path = get_v4l_node_path(m_idx, m_frontend_idx, "dvr");
		
	if (stat(dvr_path.c_str(), &statbuf) != -1)
		dvr_idx = m_frontend_idx;
	
		//demux
	std::string demux_path = get_v4l_node_path(m_idx, m_frontend_idx, "demux");
	if (stat(demux_path.c_str(), &statbuf) != -1)
		demux_idx = m_frontend_idx;

    dvr_path = get_v4l_node_path(m_idx, dvr_idx, "dvr");
    demux_path = get_v4l_node_path(m_idx, demux_idx, "demux");

	
	//vector of demuxes
	demux_handles_map_t demux_handles;

	//setup dvr
	int dvr = open(dvr_path.c_str(), O_RDONLY|O_NONBLOCK);
	if (dvr < 0)
	{
		log_error(L"open device %d, dvr%d failed") % m_idx % dvr_idx;
		return;
	}

	fd_set rfds;
	timeval tv;
	while (!m_stopFlag)
	{
		//check pids to add
		unsigned short pid = 0xFFFF;
		{
			boost::unique_lock<boost::mutex> lock(pids_lock_);
			if (pids_to_add_.size() > 0)
			{
				pid = pids_to_add_[0];
				pids_to_add_.erase(pids_to_add_.begin());
			}
		}
		
		//ignore NULL packets
		if (pid != 0xFFFF && pid != NULL_PACKET_PID)
		{
			log_info(L"tuner %d, demux %d. adding PID %d") % m_idx % demux_idx % pid;

			int dmx = open(demux_path.c_str(), O_RDWR);
			if (dmx !=-1)
			{
				demux_handles[pid] = dmx;

				dmx_pes_filter_params pesfilter;
				pesfilter.pid = pid;
				pesfilter.input = DMX_IN_FRONTEND;
				pesfilter.output = DMX_OUT_TS_TAP;
				pesfilter.pes_type = DMX_PES_OTHER;
				pesfilter.flags = DMX_IMMEDIATE_START;
				r = ioctl(dmx, DMX_SET_PES_FILTER, &pesfilter);
				if (r < 0)
				{
					log_warning(L"ioctl DMX_SET_PES_FILTER failed for tuner %d, demux %d") % m_idx % demux_idx;
				}
			} else
			{
				log_warning(L"open demux failed for tuner %d, demux %d") % m_idx % demux_idx;
			}
		}

		//check pids to delete
		pid = 0xFFFF;
		{
			boost::unique_lock<boost::mutex> lock(pids_lock_);
			if (pids_to_delete_.size() > 0)
			{
				pid = pids_to_delete_[0];
				pids_to_delete_.erase(pids_to_delete_.begin());
			}
		}

		if (pid != 0xFFFF)
		{
			log_info(L"tuner %d, demux %d. Deleting PID %d") % m_idx % demux_idx % pid;
			if (demux_handles.find(pid) != demux_handles.end())
			{
				close(demux_handles[pid]);
				demux_handles.erase(pid);
			}
		}

		FD_ZERO(&rfds);
		FD_SET(dvr, &rfds);
		tv.tv_sec = 0;
		tv.tv_usec = 10000;
		r = select(dvr+1, &rfds, NULL, NULL, &tv);
		if (r==-1)
		{
			log_error(L"select() failed for tuner %d, dvr %d") % m_idx % dvr_idx;
			continue; //?????????????
		}
		if (r==0) //no data (yet?)
			continue;
		if (FD_ISSET(dvr, &rfds))
		{
			// packets are 188 bytes long, so we're careful with alignment
			r = read(dvr, m_readBuf, m_readBufSize);
			if(r < 0)
			{
				int err = errno;
				if (err != EAGAIN)
				{
					log_error(L"can't read from dvr for tuner %d, dvr %d, err %d") % m_idx % dvr_idx % err;
					continue; //?????????????
				}
			}
			packet_aligner_.write_stream(m_readBuf, r);
		}
	}

	close(dvr);
	demux_handles_map_t::iterator it = demux_handles.begin();
	while (it != demux_handles.end())
	{
		close(it->second);
		++it;
	}

	log_info(L"Exiting streaming thread for tuner %d, frontend %d") % m_idx % m_frontend_idx;
}

void  __stdcall CV4lTunersStreamSrc::filter_ts_callback(const unsigned char* buf, unsigned long len, void* user_param)
{
	CV4lTunersStreamSrc* parent = (CV4lTunersStreamSrc*)user_param;
	
    unsigned char* start = (unsigned char*)buf;
    unsigned char* end = start + len;
    unsigned char* cur = start;

    size_t to_send = 0;
    
    while (cur < end)
    {
        if (ts_process_routines::GetPacketPID(cur) == NULL_PACKET_PID) //do not send NULL packets
        {
            // send ready data
            if (to_send)
            {
                parent->m_callback(start, to_send, parent->m_user_param);
                to_send = 0;
            }
            
            cur += TS_PACKET_SIZE;
            start = cur;
        }
        else
        {
            cur += TS_PACKET_SIZE;
            to_send += TS_PACKET_SIZE;
        }
    }

    if (to_send)
    {
        parent->m_callback(start, to_send, parent->m_user_param);
    }
}
