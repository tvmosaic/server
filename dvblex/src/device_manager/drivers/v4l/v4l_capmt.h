/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#pragma once

#define V4L_CA_LIST_MANAGEMENT_MORE     0x00
#define V4L_CA_LIST_MANAGEMENT_FIRST    0x01
#define V4L_CA_LIST_MANAGEMENT_LAST     0x02
#define V4L_CA_LIST_MANAGEMENT_ONLY     0x03
#define V4L_CA_LIST_MANAGEMENT_ADD      0x04
#define V4L_CA_LIST_MANAGEMENT_UPDATE   0x05

#define V4L_CA_PMT_CMD_ID_OK_DESCRAMBLING   0x01
#define V4L_CA_PMT_CMD_ID_OK_MMI            0x02
#define V4L_CA_PMT_CMD_ID_QUERY             0x03
#define V4L_CA_PMT_CMD_ID_NOT_SELECTED      0x04

bool CreateCAPMTFromPMT(unsigned char* pmt, int pmt_len, unsigned long listmgt, unsigned long cmd,
		unsigned char* ca_pmt, int& ca_pmt_len);
