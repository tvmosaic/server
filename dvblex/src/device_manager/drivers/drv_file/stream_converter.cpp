/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_logger.h>
#include <dl_ts.h>
#include <boost/thread.hpp>
#include "stream_converter.h"

using namespace dvblink::engine;
using namespace dvblink::logging;

namespace dvblink {

stream_converter::stream_converter(PSTREAMCALLBACK callback, void* param, volatile bool* exit_flag) :
    callback_(callback),
    param_(param),
    exit_flag_(exit_flag)
{
}

stream_converter::~stream_converter()
{
}

void stream_converter::init()
{
    pmt_parser_.Init();
    pcr0_ = -1;
}

void stream_converter::term()
{
}

static __int64 GetBufferMaxPCR(const unsigned char* buffer, int len, unsigned short pcr_pid)
{
    __int64 ret_val = -1;
   
    int c = len/188;
    for (int i=0; i<c; i++)
    {
        const unsigned char* curpack = buffer + 188*i;
        unsigned short pid = ts_process_routines::GetPacketPID(curpack);
        if (pid != pcr_pid)
            continue;
            
        boost::uint64_t pcr;
        if (ts_process_routines::GetPCRValue((void*)curpack, pcr))
            ret_val = pcr;
    }
    return ret_val;
}

void stream_converter::process_stream(const unsigned char* buf, size_t len)
{
    if (pmt_parser_.GetPmtInfo() == NULL)
    {
        pmt_parser_.ProcessStream((unsigned char*)buf, len);
        if (pmt_parser_.GetPmtInfo() != NULL)
        {
            //initialize stream variables
            size_t pmt_len = 0;
            const unsigned char* pmt_buf = pmt_parser_.GetPmtInfo()->GetPMTPointer(pmt_len);
            ts_process_routines::GetPMTSectionPCRPID((void*)pmt_buf, pmt_len, pcr_pid_);
        }
    }

    if (pmt_parser_.GetPmtInfo() != NULL)
    {
        int delta = 2000; //delta in milliseconds

        __int64 buffer_pcr = GetBufferMaxPCR(buf, len, pcr_pid_);

        if (pcr0_ == -1)
        {
            //there was no pcr assigned yet
            if (buffer_pcr != -1)
            {
                pcr0_ = buffer_pcr;
                t0_ = boost::posix_time::microsec_clock::local_time();
            }
            //send this buffer
            callback_(buf, len, param_);
        }
        else
        {
            if (buffer_pcr != -1)
            {
                while (!*exit_flag_)
                {
                    //get current time
                    boost::posix_time::ptime time_now(boost::posix_time::microsec_clock::local_time());
                    //check the elapsed time
                    boost::posix_time::time_duration td = time_now - t0_;
                    double stream_millisec = ((buffer_pcr - pcr0_)/27000000.0)*1000;
                    int delta1 = int(td.total_milliseconds() + delta);
                    int delta2 = int(stream_millisec);
                    if (delta2 < 0 || (abs(delta1 - delta2) > 10000))
                    {
                        //pcr wrap around?
                        pcr0_ = buffer_pcr;
                        t0_ = boost::posix_time::microsec_clock::local_time();
                        callback_(buf, len, param_);
                        break;
                    }
                    else
                    {
                        if (delta1 > delta2)
                        {
                            //send this buffer
                            callback_(buf, len, param_);
                            break;
                        }
                    }
		            boost::this_thread::sleep(boost::posix_time::milliseconds(10));
                }
            }
            else
            {
                //send this buffer now
                callback_(buf, len, param_);
            }
        }
    }
}

} //dvblink
