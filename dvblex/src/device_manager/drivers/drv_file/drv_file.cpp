/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
#include "stdafx.h"
#include <sstream>
#include <fstream>
#include <boost/filesystem.hpp>
#include <dl_logger.h>
#include <dl_strings.h>
#include <dl_ts.h>
#include <drivers/deviceapi.h>
#include <drivers/drv_file.h>
#include <dl_file_procedures.h>
#include "stream_converter.h"

using namespace dvblink;
using namespace dvblink::logging;
using namespace dvblink::engine;

static dvblink::filesystem_path_t get_stream_file(const std::string& device_path)
{
    dvblink::filesystem_path_t ret_val;

    dvblink::filesystem_path_t desc_file(string_cast<EC_UTF8>(device_path));
    desc_file /= L"drv_file.ini";

    if (boost::filesystem::exists(desc_file.to_boost_filesystem()))
    {
        std::ifstream infile(desc_file.to_string().c_str());
        std::string line;
        if (std::getline(infile, line))
        {
            ret_val = string_cast<EC_UTF8>(line);
            log_info(L"file_tuner::get_stream_file. Stream file: %1%") % ret_val.to_wstring();
        } else
        {
            log_ext_info(L"file_tuner::get_stream_file. File %1% is empty?") % desc_file.to_wstring();
        }
    } else
    {
        log_ext_info(L"file_tuner::get_stream_file. File %1% does not exist") % desc_file.to_wstring();
    }

    return ret_val;
}

//file_tuner_factory_t *******************
int file_tuner_factory_t::DeviceGetList(PDevAPIDevListEx pDL)
{
	pDL->Count = 0;

    dvblink::filesystem_path_t stream_file = get_stream_file(device_config_path_.to_string());
    if (!stream_file.empty() && boost::filesystem::exists(stream_file.to_boost_filesystem()))
    {
        pDL->Devices[pDL->Count].TunerIdx = pDL->Count;
        pDL->Devices[pDL->Count].FrontendIdx = 0;

        boost::filesystem::path fs = stream_file.to_boost_filesystem();

#ifdef WIN32
        std::string src_name = string_cast<EC_UTF8>(fs.filename().wstring());
#else
        std::string src_name = fs.filename().string();
#endif

        strcpy(pDL->Devices[pDL->Count].szName, src_name.c_str());
        wcscpy(pDL->Devices[pDL->Count].szDevicePathW, L"file_source");
        pDL->Devices[pDL->Count].TunerType = TUNERTYPE_DVBT | TUNERTYPE_DVBC | TUNERTYPE_DVBS;
        pDL->Count += 1;
    }

	return dvblink::SUCCESS;
}

tuner_t* file_tuner_factory_t::get_tuner(const std::string& device_path, int fdx)
{
    return new file_tuner_t(device_path, fdx, device_config_path_);
}

//file_tuner_t ************************
file_tuner_t::file_tuner_t(const std::string& device_path, int fdx, const dvblink::filesystem_path_t& dir) :
    tuner_t(device_path, fdx),
    streaming_thread_(NULL),
    exit_flag_(false),
    packet_aligner_(aligner_callback, this)
{
    stream_file_ = get_stream_file(dir.to_string());
    stream_converter_ = new stream_converter(&process, this, &exit_flag_);
}

file_tuner_t::~file_tuner_t()
{
    delete stream_converter_;
}

int file_tuner_t::StartDevice(DL_E_TUNER_TYPES tuner_type)
{	
	int res = dvblink::FAIL;

    if (boost::filesystem::exists(stream_file_.to_boost_filesystem()))
    {
        if (streaming_thread_ == NULL)
        {
            //Start streaming thread
            exit_flag_ = false;
            streaming_thread_ = new boost::thread(boost::bind(&file_tuner_t::streaming_thread, this));
        }
        res = dvblink::SUCCESS;
    }

	return res;
}

int file_tuner_t::StopDevice()
{
    if (streaming_thread_)
    {
        // Stop streaming thread if started
        exit_flag_ = true;
        streaming_thread_->join();
        delete streaming_thread_;
        streaming_thread_ = NULL;
    }

    return dvblink::SUCCESS;
}

int file_tuner_t::SetTuner(dvblink::PTransponderInfo Tp)
{	
	int res = dvblink::SUCCESS;

    return res;
}

int file_tuner_t::GetTunerState(dvblink::PSignalInfo TunerState, dvblink::PTransponderInfo Tp)
{
	int res = dvblink::FAIL;

    if (true)
	{
		TunerState->dwSize = sizeof(dvblink::TSignalInfo);
		TunerState->Level = 100;
		TunerState->Locked = 1;
		TunerState->Quality = 100;
		res = dvblink::SUCCESS;
	}

	return res;
}

void file_tuner_t::streaming_thread()
{
    while (!exit_flag_)
    {
        FILE* f = open_file();
        read_file(f);
    }
}

void file_tuner_t::read_file(FILE* f)
{
    if (f != NULL)
    {
        size_t buffer_size = 188*32;
        unsigned char* buffer = new unsigned char[buffer_size];

        stream_converter_->init();

        while (!exit_flag_)
        {
            size_t bytes_read = fread(buffer, 1, buffer_size, f);

            if (ferror(f) != 0)
            {
                //error. exit reading loop
                log_info(L"file_tuner_t::read_file. Error reading %1%. Exiting...") % stream_file_.to_wstring();
                break;
            }

            if (bytes_read > 0)
                packet_aligner_.write_stream(buffer, bytes_read);

            if (feof(f) != 0)
            {
                //end of file is reached. re-position to the beginning
                fseek(f, 0, SEEK_SET);
            }
        }

        stream_converter_->term();

        fclose(f);
        delete buffer;
    }
}

FILE* file_tuner_t::open_file()
{
    FILE* f = NULL;

    while (!exit_flag_)
    {
        f = filesystem::universal_open_file(stream_file_, "rb");

        if (f == NULL)
        {
            boost::this_thread::sleep(boost::posix_time::milliseconds(200));
        } else
        {
            break;
        }
    }
    return f;
}

void __stdcall file_tuner_t::aligner_callback(const unsigned char* buf, unsigned long len, void* user_param)
{
    file_tuner_t* parent = (file_tuner_t*)user_param;

    parent->stream_converter_->process_stream(buf, len);
}

void file_tuner_t::process(const unsigned char* buf, size_t len, void* user_param)
{
    file_tuner_t* parent = (file_tuner_t*)user_param;
    if (parent->callback_ != NULL)
        parent->callback_((unsigned char*)buf, len, parent->callback_param_);
}

