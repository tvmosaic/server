/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/asio.hpp>
#include <dl_logger.h>
#include <dl_ts.h>
#include "tc_data_provider.h"

using namespace dvblink::logging;
using namespace dvblink::engine;

namespace dvblink { namespace transcoder {

#ifdef BEFORE_PIPE_WRITE_FILE
#ifdef _WIN32
std::string data_provider_file_name = "c:/data_before_ffmpeg_pipe.ts";
#else
std::string data_provider_file_name = "~/data_before_ffmpeg_pipe.ts";
#endif
#endif

data_provider::pipe_wait_thread::pipe_wait_thread() :
    quit_(false)
#ifdef BEFORE_PIPE_WRITE_FILE
    ,m_pFile(NULL)
#endif
{
#ifdef BEFORE_PIPE_WRITE_FILE
    m_pFile = fopen(data_provider_file_name.c_str(), "w+b");
    if (m_pFile)
    {
        setbuf(m_pFile, m_buffer);
    }
#endif
}

data_provider::pipe_wait_thread::~pipe_wait_thread()
{
#ifdef BEFORE_PIPE_WRITE_FILE
    fclose(m_pFile);
#endif
}

#ifdef _WIN32
void data_provider::pipe_wait_thread::thread(const file_handle& pipe, ts_circle_buffer* queue)
{
    OVERLAPPED ov;
    memset(&ov, 0, sizeof(ov));
    
    ov.hEvent = CreateEvent(NULL, TRUE, TRUE, NULL);
    if (BOOL connected = ConnectNamedPipe(pipe.get(), &ov))
    {
        // overlapped ConnectNamedPipe should return FALSE
        return;
    }

    switch (GetLastError()) 
    { 
    case ERROR_IO_PENDING: 
        // overlapped connection in progress
        break; 

    case ERROR_PIPE_CONNECTED:
        // client is already connected, so signal an event
        SetEvent(ov.hEvent);
        break;

    default:
        // an error occurs
        log_error(L"data_provider - can't connect to named pipe");
        return;
    }

    if (WaitForSingleObject(ov.hEvent, 30000) != WAIT_OBJECT_0)
    {
        log_error(L"data_provider - pipe connection timed out");
        return;
    }
    else
    {
        ResetEvent(ov.hEvent);
    }

    BOOL res;
    ts_circle_buffer::node_t node = NULL;
    DWORD counter = 0, bytes_to_write = 0, bytes_written = 0;
    while (!quit_)
    {
        if (!counter)
        {
            node = queue->tear_node(50);
        }
        if (node != NULL)
        {
            bytes_to_write = node->size_to_read() - counter;
            bytes_written = 0;
            
            res = WriteFile(pipe.get(), node->data() + counter, bytes_to_write, &bytes_written, &ov);

        #ifdef BEFORE_PIPE_WRITE_FILE
            fwrite(node->data() + counter, bytes_written, 1, m_pFile);
        #endif

            if (res)
            {
                ResetEvent(ov.hEvent);
            }
            else
            {
                DWORD err = GetLastError();
                switch (err)
                {
                case ERROR_IO_PENDING:
                    if (WaitForSingleObject(ov.hEvent, 8000) == WAIT_OBJECT_0)
                    {
                        GetOverlappedResult(pipe.get(), &ov, &bytes_written, FALSE);
                    }
                    break;
                case ERROR_BROKEN_PIPE:
                default:
                    quit_ = true;
                    break;
                }

            }

            if (bytes_to_write == bytes_written)
            {
                queue->put_node(node);
                node = NULL;
                counter = 0;
            }
            else
            {
                counter += bytes_written;
            }
        }
    }

    if (node)
    {
        queue->put_node(node);
    }

    DisconnectNamedPipe(pipe.get());
    CloseHandle(ov.hEvent);
}
#else

#define WH_SEND_ERROR_CODE 1
#define WH_SEND_OK_CODE 2

void data_provider::pipe_wait_thread::write_handler(dvblink::event* wait_send, int* result, const boost::system::error_code& error, std::size_t bytes_transferred)
{
	//signal that data was written (or error happened)
	*result = WH_SEND_OK_CODE;
	if (error)
		*result = WH_SEND_ERROR_CODE;
	wait_send->signal();
}

void data_provider::pipe_wait_thread::thread(const file_handle& pipe, ts_circle_buffer* queue)
{
	boost::asio::io_service io_service;

	boost::asio::io_service::work* work = new boost::asio::io_service::work(io_service);

	boost::asio::posix::stream_descriptor* output = new boost::asio::posix::stream_descriptor(io_service, pipe.get());

	boost::thread_group worker_threads;
	worker_threads.create_thread(boost::bind(&boost::asio::io_service::run, &io_service) );

	dvblink::event wait_send;
	int wait_result;

    while (!quit_)
    {
    	ts_circle_buffer::node_t node = queue->tear_node(50);
		if (node != NULL)
		{
			wait_send.reset();
		
			async_write(*output, boost::asio::buffer(node->data(), node->size_to_read()),
					boost::bind(&data_provider::pipe_wait_thread::write_handler, this, &wait_send, &wait_result, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
			bool bsent = false;
			//wait until data is sent
			while (!quit_ && !bsent)
			{
				if (wait_send.wait(1000))
				{
					if (wait_result == WH_SEND_OK_CODE)
					{
						bsent = true;
					} else
					{
						log_error(L"data_provider::pipe_wait_thread::thread. Error sending data to transcoder. Exiting thread.");
						quit_ = true;
					}
				}
			}
			//cancel all async operations if quit was signaled
			if (!bsent)
				output->cancel();
			//return the node
			queue->put_node(node);
		}
    }

    delete output;
    delete work;

    worker_threads.join_all();

}
#endif

///////////////////////////////////////////////////////////////////////////////

data_provider::data_provider(const file_handle& pipe_handle) :
    pipe_handle_(pipe_handle),
    pipe_wait_thread_(NULL),
    data_process_thread_(),
	queue_(256, 32 * TS_PACKET_SIZE, L"data_provider")
{
    pipe_wait_thread_ = new boost::thread(boost::bind(
        &pipe_wait_thread::thread,
        &data_process_thread_, pipe_handle_, &queue_));
}

data_provider::~data_provider()
{
    data_process_thread_.quit();

#ifdef _WIN32
	//if (!pipe_wait_thread_->timed_join(boost::posix_time::seconds(2)))
	//{
	//	//kill thread
	//	pipe_wait_thread_->interrupt();
	//}
    pipe_wait_thread_->join();
#else
	pipe_wait_thread_->join();
#endif

    delete pipe_wait_thread_;
}

bool data_provider::process_data(const unsigned char* pData, unsigned int uLen, boost::int64_t /*PTS_27MHz*/)
{
	queue_.write_stream(pData, uLen);
    return true;
}

} //transcoder
} //dvblink
