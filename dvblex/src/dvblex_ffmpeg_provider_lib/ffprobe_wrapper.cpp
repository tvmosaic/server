/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/thread.hpp>
#include <dl_logger.h>
#include <dl_external_control.h>
#include <dl_pugixml_helper.h>
#include "tc_ffprobe_wrapper.h"

using namespace dvblink::logging;
using namespace dvblink::engine;
using namespace dvblex::settings;

namespace dvblink { namespace transcoder {

tc_ffprobe_wrapper::tc_ffprobe_wrapper(const dvblink::filesystem_path_t& ffprobe_file_path, const dvblink::filesystem_path_t& ffprobe_dir, 
                                       const dvblink::filesystem_path_t& output_temp_dir)
    : ffprobe_file_path_(ffprobe_file_path), ffprobe_dir_(ffprobe_dir), output_temp_dir_(output_temp_dir)
{
}

bool tc_ffprobe_wrapper::get_stream_descriptions(const dvblink::filesystem_path_t& stream_file, ffprobe_stream_desc_list_t& stream_list)
{
    boost::uuids::uuid new_guid = boost::uuids::random_generator()();
    dvblink::filesystem_path_t out_info_file = output_temp_dir_ / boost::lexical_cast<std::string>(new_guid);

	const char* quote_sym = "";
#ifdef _WIN32
	quote_sym = "\"";
#endif

	std::ostringstream ostr;

    std::vector<dvblink::launch_param_t> params;
    params.push_back("-show_streams");
    params.push_back("-print_format");
    params.push_back("xml");

	ostr.clear(); ostr.str("");
	ostr << quote_sym << stream_file.to_string() << quote_sym;
	params.push_back(ostr.str());

    params.push_back(">");

	ostr.clear(); ostr.str("");
	ostr << quote_sym << out_info_file.to_string() << quote_sym;
	params.push_back(ostr.str());

    boost::int64_t pid = external_control::start_process(ffprobe_file_path_, &ffprobe_dir_, params, external_control::NormalPriority);

    while (external_control::is_process_running(pid))
    {
        boost::this_thread::sleep(boost::posix_time::milliseconds(50));
    }

    //read resulting xml file
    pugi::xml_document doc;
    if (doc.load_file(out_info_file.to_string().c_str()).status == pugi::status_ok)
    {
        pugi::xml_node root_node = doc.first_child();
        if (root_node != NULL)
        {
            pugi::xml_node streams_node = root_node.child("streams");
            if (streams_node != NULL)
            {
                pugi::xml_node stream_node = streams_node.first_child();
                while (stream_node != NULL)
                {
                    ffprobe_stream_desc_t stream_desc;
                    std::string str;
                    if (pugixml_helpers::get_node_attribute(stream_node, "index", str))
                    {
                        string_conv::apply(str.c_str(), stream_desc.index, 0);

                        pugixml_helpers::get_node_attribute(stream_node, "codec_name", stream_desc.codec);
                        pugixml_helpers::get_node_attribute(stream_node, "codec_type", stream_desc.codec_type);

                        stream_list.push_back(stream_desc);
                    }

                    stream_node = stream_node.next_sibling();
                }
            }
        }
    }

    try {
        boost::filesystem::remove(out_info_file.to_boost_filesystem());
    } catch(...) {}

	ostr.clear(); ostr.str("");
    ostr << "tc_ffprobe_wrapper::get_stream_descriptions. Stream info: ";

    for (size_t i=0; i<stream_list.size(); i++)
    {
        ffprobe_stream_desc_t& desc = stream_list[i];
        if (i != 0)
            ostr << ", ";

        ostr << desc.index << "/" << (desc.codec.empty() ? "unknown" : desc.codec) << "/" << (desc.codec_type.empty() ? "unknown" : desc.codec_type);
    }

    log_info(string_cast<EC_UTF8>(ostr.str()).c_str());

    return stream_list.size() > 0;
}

} //transcoder
} //dvblink
