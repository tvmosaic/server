/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_messaging_core.h>


namespace dvblink { namespace messaging {

messaging_core::messaging_core() :
    shutdown_(false),
    uid_(engine::uuid::gen_uuid())
{

}

messaging_core::~messaging_core()
{
    shutdown();
}

void messaging_core::start()
{
}

void messaging_core::shutdown()
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);

    shutdown_ = true;
    while (!map_queue_.empty())
    {
        map_queue_.begin()->second->shutdown();
        map_queue_.erase(map_queue_.begin());
    }
}

i_result messaging_core::query_interface(const base_id_t& /*requestor_id*/, const i_guid& /*iid*/, i_base_object_t& /*obj*/)
{
    return i_not_implemented;
}

void messaging_core::register_queue(const i_client_message_queue_t& queue)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    if (!shutdown_)
    {
        std::pair<map_id_to_queue_t::iterator, bool> res = map_queue_.insert(map_id_to_queue_t::value_type(queue->get_uid(), queue));
        assert(res.second); // queue with such a id already exists in the map
    }
}

void messaging_core::unregister_queue(const message_queue_id_t& id)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_);
    iter_map_id_to_queue_t iter = map_queue_.find(id);
    if (iter != map_queue_.end())
    {
//        iter->second->shutdown();
        map_queue_.erase(iter);
    }
}

message_error messaging_core::post(const message_addressee_t& to,
    const message_sender_t& from, const char* message_type_name, const void* message_body, size_t message_body_size)
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);

    if (shutdown_)
        return queue_shutdown;

    message_error err = addressee_not_found;
    if (to == broadcast_addressee)
    {
        const_iter_map_id_to_queue_t iter = map_queue_.begin();
        while (iter != map_queue_.end())
        {
            iter->second->put_message(i_client_message_queue::post, message_id_t(0), from, message_type_name, message_body, message_body_size);
            ++iter;
        }
        err = success;
    }
    else
    {
        const_iter_map_id_to_queue_t iter = map_queue_.find(message_queue_id_t(to.get()));
        if (iter != map_queue_.end())
        {
            iter->second->put_message(i_client_message_queue::post, message_id_t(0), from, message_type_name, message_body, message_body_size);
            err = success;
        }
    }

    return err;
}

/*
    method is called by sender
*/
message_error messaging_core::send(const message_id_t id, const message_addressee_t& to,
    const message_sender_t& from, const char* message_type_name, const void* message_body, size_t message_body_size)
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);

    if (shutdown_)
        return queue_shutdown;

    const_iter_map_id_to_queue_t iter = map_queue_.find(message_queue_id_t(to.get()));
    if (iter != map_queue_.end())
    {
        iter->second->put_message(i_client_message_queue::send, id, from, message_type_name, message_body, message_body_size);
        return success;
    }

    return addressee_not_found;
}

void messaging_core::response(const message_id_t id, message_error error,
    const message_addressee_t& to, const message_sender_t& from, const void* message_body, size_t message_body_size)
{
    boost::shared_lock<boost::shared_mutex> lock(lock_);

    if (!shutdown_)
    {
        const_iter_map_id_to_queue_t iter = map_queue_.find(message_queue_id_t(to.get()));
        if (iter != map_queue_.end())
        {
            iter->second->put_response(id, error, from, message_body, message_body_size);
        }
    }
}

} //messaging
} //dvblink
