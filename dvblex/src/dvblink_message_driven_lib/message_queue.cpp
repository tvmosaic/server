/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_message_queue.h>

namespace dvblink { namespace messaging {

message_queue::message_queue() :
    shutdown_(false),
    queue_in_thread_(NULL),
    quit_(false),
    last_out_id_(0)
{
}

message_queue::message_queue(const message_queue_id_t& id) :
    id_(id),
    shutdown_(false),
    queue_in_thread_(NULL),
    quit_(false),
    last_out_id_(0)
{
}

message_queue::~message_queue()
{
    shutdown();
}

void message_queue::init(const i_server_messaging_core_t& msg_core)
{
    msg_core_ = msg_core;
    assert(!queue_in_thread_);

    if (!queue_in_thread_)
    {
        quit_ = false;
        queue_in_thread_ = new boost::thread(boost::bind(&message_queue::queue_proc, this));
    }
}

void message_queue::shutdown()
{
    if (!shutdown_)
    {
        shutdown_ = true;

        // stop message loop thread
        quit_ = true;
        
        if (queue_in_thread_)
        {
            queue_in_thread_->join();
            delete queue_in_thread_;
            queue_in_thread_ = NULL;
        }

        // release all wait events
        {
            boost::unique_lock<boost::shared_mutex> lock(lock_out_);
            iter_map_out_messages_t iter = queue_out_.begin(), end = queue_out_.end();
            while (iter != end)
            {
                iter->second->event_.signal();
                ++iter;
            }
        }
    }
}

/*
    method registers message type to deliver it to subscribers later
*/
void message_queue::register_message(const char* type_name,
    void* subscriber, client_invoke_function_t fn)
{
    if (!shutdown_)
        msg_container_.reg(type_name, subscriber, fn);
}

/*
    method unregisters message type
*/
void message_queue::unregister_message(const char* type_name)
{
    msg_container_.unreg(type_name);
}

/*
    method delivers message to the recipient
*/
void message_queue::deliver_message(const char* message_type_name,
    const message_id_t& id, const message_sender_t& sender, const std::string& message_body)
{
    message_container::item_message item;
    if (msg_container_.get(message_type_name, item))
    {
        // call message handler
        item.callback_(item.subscriber_, id, sender, message_body);
    }
    else
    {
        // handler is not registered
        msg_core_->response(id, handler_not_registered, sender.get(), message_sender_t(id_.get()), NULL, 0);
    }
}

/*
    message dispatcher calls this method to put message in the queue
*/
void message_queue::put_message(type message_type, const message_id_t& id,
    const message_sender_t& from, const char* message_type_name, const void* message_body, size_t message_body_size)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_in_);
    
    message_in_t msg = message_in_t(new message_in(id_.get(), from, message_type,
        message_type_name, reinterpret_cast<const boost::uint8_t*>(message_body), message_body_size, id));
    queue_in_.push(msg);

    new_msg_in_event_.signal();
}

void message_queue::put_response(const message_id_t& id, message_error error,
    const message_sender_t& /*from*/, const void* message_body, size_t message_body_size)
{
    boost::unique_lock<boost::shared_mutex> lock(lock_out_);

    const_iter_map_out_messages_t iter = queue_out_.find(id);
    if (iter != queue_out_.end())
    {
        iter->second->error_ = error;
        if (message_body && message_body_size)
        {
            iter->second->message_body_.assign(
                reinterpret_cast<const boost::uint8_t*>(message_body),
                reinterpret_cast<const boost::uint8_t*>(message_body) + message_body_size);
        }
        iter->second->event_.signal();
    }
}

void message_queue::queue_proc()
{
    while (!quit_)
    {
        if (!quit_ && new_msg_in_event_.wait(50))
        {
            lock_in_.lock();
            
            while (!queue_in_.empty() && !quit_)
            {
                const message_in_t msg = queue_in_.front();
                queue_in_.pop();

                lock_in_.unlock();

                try
                {
                    // process message
                    deliver_message(msg->message_type_name_.c_str(),
                        msg->id_, msg->from_, msg->message_body_);
                }
                catch (...)
                {
                }

                lock_in_.lock();
            }

            new_msg_in_event_.reset();
            lock_in_.unlock();
        }
    }
}

} //messaging
} //dvblink
