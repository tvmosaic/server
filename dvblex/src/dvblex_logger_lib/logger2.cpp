/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <stdexcept>
#include <dl_logger.h>

namespace dvblink { namespace logging {

logger* logger::instance()
{
    static logger logger_;
    return &logger_;
}

void logger::shutdown()
{
    logger_.reset();
}

void logger::set_interface(const i_logger_t& logger)
{
    if (logger_)
    {
        //throw std::runtime_error("logger::create_instance: logger has been already created!");
    }
    logger_ = logger;
}

void logger::log_message(e_log_level log_level, const wchar_t* log_str) const
{
    if (logger_)
    {
        logger_->log_message(log_level, log_str);
    }
}

} //logging
} //dvblink
