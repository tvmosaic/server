/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <time.h>
#include <assert.h>
#ifdef WIN32
#include <shlwapi.h>
#endif
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
#include <dl_uuid.h>
#include <dl_logger_file_writer.h>
#include <dl_strings.h>

#if defined(__ANDROID__)
#include <android/log.h>
#endif

#include <boost/date_time/local_time/local_time.hpp>

namespace dvblink { namespace logging {

logger_file_writer::logger_file_writer(const filesystem_path_t& file_name, e_log_level log_level, bool cmd_line_mode) :
    uid_(engine::uuid::gen_uuid()),
    file_name_(file_name),
    log_level_(log_level),
    quit_flag_(false),
    command_line_mode_(cmd_line_mode),
#ifdef WIN32
    log_file_(INVALID_HANDLE_VALUE)
#else
    log_file_(NULL)
#endif        
{
}

logger_file_writer::~logger_file_writer()
{
    stop();
}

void logger_file_writer::set_command_line_mode(bool cmd_line_mode)
{
    command_line_mode_ = cmd_line_mode;
}

bool logger_file_writer::truncate_log_file(boost::uint64_t max_size)
{
    bool res = false;

    if (boost::filesystem::exists(file_name_.get()))
    {
        bool in_res = false;
        std::string tempstr;
        boost::uint64_t size = boost::filesystem::file_size(file_name_.get());
        if (size > max_size)
        {
            tempstr = engine::string_cast<engine::EC_UTF8>(file_name_.get());
            std::ifstream in_log(tempstr.c_str());
            if (in_log.is_open())
            {
                std::string line;
                //while (std::getline(in_log, line))
                //{
                //    if (size - in_log.tellg() < max_size)
                //    {
                //        in_res = true;
                //        break;
                //    }
                //}

                // seek to the needed position
                in_log.seekg(static_cast<std::streamoff>(size - max_size));
                // get the current line because it can be cut
                std::getline(in_log, line);

                //if (in_res)
                {
                    std::wstringstream out_name;
                    out_name << file_name_.get() << L".temp";

                    try
                    {
                        boost::filesystem::remove(out_name.str());

                        tempstr = engine::string_cast<engine::EC_UTF8>(out_name.str());
                        std::ofstream out_log(tempstr.c_str(), std::ios::out | std::ios::trunc);
                        in_res = out_log.is_open();
                        if (in_res)
                        {
                            while (std::getline(in_log, line))
                            {
                                out_log << line << "\n";
                            }
                            out_log.close();
                        }

                        in_log.close();
                        if (in_res)
                        {
                            boost::filesystem::rename(out_name.str(), file_name_.get());
                            res = true;
                        }
                    }
                    catch (const boost::filesystem::filesystem_error& /*ex*/)
                    {
                        out_message(L"logger_file_writer::truncate_log_file: log file is locked", boost::posix_time::second_clock::local_time());
                        res = false;
                    }
                }
            }
        }
    }
    return res;
}

bool logger_file_writer::start()
{
    bool res = true;
    if (log_level_ > log_level_none)
    {
#if defined(WIN32)
        log_file_ = CreateFile(file_name_.c_str(), FILE_APPEND_DATA, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL/* | FILE_FLAG_NO_BUFFERING*/, NULL);
        if (log_file_ == INVALID_HANDLE_VALUE)
        {
            res = false;
        }
#else
        log_file_ = fopen(file_name_.to_string().c_str(), "a+");
        if (log_file_ == NULL)
        {
            res = false;
        }
#endif

        boost::posix_time::ptime time = boost::posix_time::second_clock::local_time();

        out_message(L"", time);
        out_message(L"==============================================================", time);
        out_message(L"                   Logging is started", time);
        out_message(L"==============================================================", time);
        if (!res)
        {
            out_message(L"Log file couldn't be created", time);
        }
    }
    return true;
}

void logger_file_writer::set_log_level(e_log_level log_level)
{
    boost::mutex::scoped_lock lock(lock_);
    log_level_ = log_level;
}

void logger_file_writer::stop()
{
#ifdef WIN32
    if (log_file_ != INVALID_HANDLE_VALUE)
    {
        CloseHandle(log_file_);
        log_file_ = INVALID_HANDLE_VALUE;
    }
#else
    if (log_file_)
    {
        fclose(log_file_);
        log_file_ = NULL;
    }
#endif
}

void logger_file_writer::out_message(const std::wstring& message, const boost::posix_time::ptime& time) const
{
    std::wstring cr;
#ifdef WIN32
    cr = L"\r"; //for windows based servers add \r to have a good formatted log file
#endif

    std::wstringstream mes;
    mes << boost::posix_time::to_simple_wstring(time) << L":   " << message << cr << std::endl;

    if (command_line_mode_)
    {
        std::wcout << mes.str() << std::flush;
    }

    std::string outStr = dvblink::engine::string_cast<dvblink::engine::EC_UTF8>(mes.str());

#if defined(__ANDROID__)
    __android_log_print(ANDROID_LOG_INFO, "TVMosaicJNI", "%s\n", outStr.c_str());
#endif

    if (log_file_)
    {        
#if defined(WIN32)
        unsigned long dwWritten;
        ::WriteFile(log_file_, outStr.c_str(), static_cast<unsigned long>(outStr.size()), &dwWritten, NULL);

        //output to debug log
        OutputDebugStringA(outStr.c_str());
#else
        fprintf(log_file_, "%s", outStr.c_str());
        fflush(log_file_);
#endif
    }
}

void logger_file_writer::log_message(dvblink::logging::e_log_level log_level, const wchar_t* message) const
{
    boost::mutex::scoped_lock lock(lock_);

    if (log_level_ > log_level_none)
    {
        if (log_level <= log_level_ || log_level == log_level_forced_info)
        {
            boost::posix_time::ptime timestamp = boost::posix_time::second_clock::local_time();
            out_message(message, timestamp);
        }
    }
}

} //logging
} //dvblink
