/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/regex.hpp>
#include <cassert>
#include <dl_event.h>
#include <dl_strings.h>
#include <dl_debug.h>
#include <dl_fifo_buffer.h>
#include <dl_network_helper.h>
#include "dl_http_comm.curl.h"
#include "http_request.h"
#include "http_utils.h"

using namespace dvblink::engine;
using namespace dvblink::streaming;

namespace dvblink {

http_comm_handler::http_comm_handler(const char* session_name,
    const char* address, const char* login,
    const char* pswd, unsigned short port, const char* data_dir,
    const char* ca_cert_name, const char* client_cert_name) :
address_(address), port_(port), auth_(http_auth_any), timeout_sec_(0)
{
    assert(!address_.empty());

    if (login && login[0])
    {
        login_ = login;

        if (pswd && pswd[0])
            password_ = pswd;
    }

    if (data_dir && data_dir[0])
        data_dir_ = data_dir;

	if (session_name && session_name[0])
        user_agent_ = session_name;

    if (ca_cert_name && ca_cert_name[0])
        ca_cert_name_ = ca_cert_name;

    if (client_cert_name && client_cert_name[0])
        client_cert_name_ = client_cert_name;

    //_TRACE("**** http_comm_handler()\n");
}

http_comm_handler::http_comm_handler(const char* session_name,
    const char* address, const char* login,
    const char* pswd, unsigned short port, const char* data_dir,
    const char* ca_cert_name, const char* client_cert,
    const char* client_priv_key) :
address_(address), port_(port), auth_(http_auth_any), timeout_sec_(0)
{
    assert(!address_.empty());

    if (login && login[0])
    {
        login_ = login;

        if (pswd && pswd[0])
            password_ = pswd;
    }

    if (data_dir && data_dir[0])
        data_dir_ = data_dir;

	if (session_name && session_name[0])
        user_agent_ = session_name;

    if (ca_cert_name && ca_cert_name[0])
        ca_cert_name_ = ca_cert_name;

    if (client_cert)
        client_cert_ = client_cert;

    if (client_priv_key)
        client_priv_key_ = client_priv_key;

    //_TRACE("**** http_comm_handler()\n");
}

http_comm_handler::~http_comm_handler()
{
    //_TRACE("**** ~http_comm_handler()\n");
}

bool http_comm_handler::Init()
{
    return true;
}

bool http_comm_handler::Term()
{
    return true;
}

http_comm_handler* http_comm_handler::Clone()
{
    http_comm_handler* h = new http_comm_handler(
        NULL, address_.c_str(), NULL, NULL, port_, NULL);

    h->login_ = login_;
    h->password_ = password_;
    h->data_dir_ = data_dir_;
    h->ca_cert_name_ = ca_cert_name_;
    h->client_cert_name_ = client_cert_name_;

    return h;
}

namespace {
    struct http_request_cb : public http_request::callbacks,
        private boost::noncopyable
    {
        http_request_cb(std::string& result) : result_(result)
        {
            result_.clear();
        }

        void on_data_received(const void* data, size_t size)
        {
            if (data && size)
            {
                result_.append((const char*)data, size);
            }
        }

        void on_header_received(const std::string& hdr)
        {
            if (!hdr.empty())
            {
                //is this a status line?
                //If yes, delete all collected headers - there will be a set of new ones (usually after redirect)
                std::string protocol;
                std::string version;
                unsigned int status_code;
                std::string text;
                if (parse_status_line(hdr, protocol, version, status_code, text))
                    headers_.clear();

                headers_.push_back(hdr);
            }
        }

        void on_request_completed(bool success)
        {
            if (!success)
            {
                result_.clear();
                headers_.clear();
            }
        }

        std::string& result_;
        http_comm_handler::http_headers_t headers_;
    };
}

static bool _make_url(const std::string& address,
    const std::string& suffix, std::string& url, bool secure,
    unsigned short port)
{
    url.clear();

    if (address.empty() || suffix.empty())
    {
        return false;
    }

    url = address;

    // add port
    if ((port != 0) && (port != 80))
    {
        if (*url.rbegin() == '/')
        {
            // remove last character
            url.resize(url.size() - 1);
        }

        url += ":";
        url += boost::lexical_cast<std::string>(port);
    }

    if ((*url.rbegin() != '/') &&
        (*suffix.begin() != '/'))
    {
        url += L'/';
    }

    url += suffix;

    if (url.empty())
    {
        return false;
    }

    if (url.find("://") == std::string::npos)
    {
        if (secure)
        {
            url.insert(0, "https://");
        }
        else
        {
            url.insert(0, "http://");
        }
    }

    return true;
}

static bool _set_parameters(http_request& req, const std::string& user_agent, 
    const std::string& login, const std::string& password,
    unsigned short port, http_auth auth,
    const std::string& data_dir,
    const std::string& ca_cert_name,
    const std::string& client_cert_name)
{
    bool ok = true;

	if (!user_agent.empty())
		req.set_user_agent(user_agent);
	
    if (!login.empty())
    {
        unsigned long curl_auth = CURLAUTH_ANY;

        switch (auth)
        {
        case http_auth_any:
            curl_auth = CURLAUTH_ANY;
            break;
        case http_auth_anysafe:
            curl_auth = CURLAUTH_ANYSAFE;
            break;
        case http_auth_basic:
            curl_auth = CURLAUTH_BASIC;
            break;
        case http_auth_digest:
            curl_auth = CURLAUTH_DIGEST;
            break;
        case http_auth_digest_ie:
            curl_auth = CURLAUTH_DIGEST_IE;
            break;
        case http_auth_gss_negotiate:
            curl_auth = CURLAUTH_DIGEST;
            break;
        case http_auth_ntlm:
            curl_auth = CURLAUTH_NTLM;
            break;
        case http_auth_ntlm_wb:
            curl_auth = CURLAUTH_NTLM_WB;
            break;
        default:
            break;
        }

        ok = req.set_credentials(login, password, curl_auth);

        if (!ok)
        {
            return false;
        }
    }

    if (port != 0)
    {
        ok = req.set_port(port);

        if (!ok)
        {
            return false;
        }
    }

    if (!data_dir.empty())
    {
        namespace fs = boost::filesystem;
        fs::path dir(data_dir);

        if (!fs::exists(dir) || !fs::is_directory(dir))
        {
            return false;
        }

        bool verify_peer = true;

        if (!client_cert_name.empty())
        {
            fs::path client_cert_path = dir / client_cert_name;

            if (fs::exists(client_cert_path) &&
                fs::is_regular_file(client_cert_path))
            {
                ok = req.set_client_cert_path(client_cert_path.string());

                if (!ok)
                {
                    return false;
                }

                verify_peer = false;
            }
        }

        fs::path ca_cert_path = ca_cert_name.empty() ?
            dir / "cacert.pem" :
        dir / ca_cert_name;

        if (fs::exists(ca_cert_path) &&
            fs::is_regular_file(ca_cert_path))
        {
            ok = req.set_ca_cert_path(ca_cert_path.string(), verify_peer);

            if (!ok)
            {
                return false;
            }
        }

        fs::path cookie_file = dir / "cookie.jar";

        ok = req.set_cookie_file(cookie_file.string());

        if (!ok)
        {
            return false;
        }
    }

    return ok;
}

static bool _set_parameters(http_request& req, const std::string& user_agent, 
    const std::string& login, const std::string& password,
    unsigned short port, http_auth auth,
    const std::string& data_dir,
    const std::string& ca_cert_name,
    const std::string& client_cert,
    const std::string& client_priv_key)
{
    bool ok = true;

	if (!user_agent.empty())
		req.set_user_agent(user_agent);
	
    if (!login.empty())
    {
        unsigned long curl_auth = CURLAUTH_ANY;

        switch (auth)
        {
        case http_auth_any:
            curl_auth = CURLAUTH_ANY;
            break;
        case http_auth_anysafe:
            curl_auth = CURLAUTH_ANYSAFE;
            break;
        case http_auth_basic:
            curl_auth = CURLAUTH_BASIC;
            break;
        case http_auth_digest:
            curl_auth = CURLAUTH_DIGEST;
            break;
        case http_auth_digest_ie:
            curl_auth = CURLAUTH_DIGEST_IE;
            break;
        case http_auth_gss_negotiate:
            curl_auth = CURLAUTH_DIGEST;
            break;
        case http_auth_ntlm:
            curl_auth = CURLAUTH_NTLM;
            break;
        case http_auth_ntlm_wb:
            curl_auth = CURLAUTH_NTLM_WB;
            break;
        default:
            break;
        }

        ok = req.set_credentials(login, password, curl_auth);

        if (!ok)
        {
            return false;
        }
    }

    if (port != 0)
    {
        ok = req.set_port(port);

        if (!ok)
        {
            return false;
        }
    }

    if (!data_dir.empty())
    {
        namespace fs = boost::filesystem;
        fs::path dir(data_dir);

        if (!fs::exists(dir) || !fs::is_directory(dir))
        {
            return false;
        }

        bool verify_peer = true;

        if (!client_cert.empty() && !client_priv_key.empty())
        {
            ok = req.set_client_cert(client_cert.c_str(),
                client_priv_key.c_str());

            if (!ok)
            {
                return false;
            }

            verify_peer = false;
        }

        fs::path ca_cert_path = ca_cert_name.empty() ?
            dir / "cacert.pem" :
        dir / ca_cert_name;

        if (fs::exists(ca_cert_path) &&
            fs::is_regular_file(ca_cert_path))
        {
            ok = req.set_ca_cert_path(ca_cert_path.string(), verify_peer);

            if (!ok)
            {
                return false;
            }
        }

        fs::path cookie_file = dir / "cookie.jar";

        ok = req.set_cookie_file(cookie_file.string());

        if (!ok)
        {
            return false;
        }
    }

    return ok;
}

static bool _set_custom_headers(http_request& req,
    const http_comm_handler::http_headers_t& headers)
{
    bool ok = req.reset_http_headers();
    const size_t size = headers.size();

    if (size > 0)
    {
        for (size_t j = 0; j < size; j++)
        {
            const std::string& h = headers[j];

            if (h.empty())
            {
                ok = false;
                break;
            }

            ok = req.add_http_header(h);

            if (!ok)
            {
                break;
            }
        }

        if (ok)
        {
            ok = req.set_http_headers();
        }

        if (!ok)
        {
            req.reset_http_headers();
        }
    }

    return ok;
}

std::string http_comm_handler::get_last_effective_url()
{
    std::string ret_val = last_effective_url_;

    //there is a bug in libcurl, returning sometimes concatenated urls (http://xxx/http:// etc.)
    std::string lc_url = boost::to_lower_copy(last_effective_url_);
    size_t http_pos = lc_url.rfind(network_helper::get_proto_string(DL_NET_PROTO_HTTP));
    if (http_pos == std::string::npos)
        http_pos = 0;

    size_t https_pos = lc_url.rfind(network_helper::get_proto_string(DL_NET_PROTO_HTTPS));
    if (https_pos == std::string::npos)
        https_pos = 0;

    size_t pos = http_pos;
    if (https_pos > pos)
        pos = https_pos;

    if (pos != 0)
        ret_val = last_effective_url_.substr(pos);

    return ret_val;
}

static bool is_gzip_compressed(const std::string& header)
{
    bool res = false;
    boost::smatch result;
    static boost::regex http_regexp(std::string("(?i)\\s*+content-encoding:\\s*+(gzip)"));
    if (boost::regex_search(header, result, http_regexp))
    {
        res = boost::iequals(result[1].str(), "gzip");
    }
    return res;
}

bool http_comm_handler::ExecuteGetWithResponseGZ(const char* url_suffix,
    bool bSecure, std::string& response, http_headers_t* headers,
    http_headers_t* out_headers)
{
    bool ret_val = false;

    http_comm_handler::http_headers_t input_headers;
    http_comm_handler::http_headers_t output_headers;

    http_comm_handler::http_headers_t* headers_in = headers == NULL ? &input_headers : headers;
    http_comm_handler::http_headers_t* headers_out = out_headers == NULL ? &output_headers : out_headers;

    headers_in->push_back("Accept-Encoding: gzip");

    void* handle = SendGetRequest(url_suffix, bSecure, headers_in);
    if (handle != NULL)
    {
        ret_val = true;
        bool b_headers_processed = false;

        boost::iostreams::filtering_ostream fos;
        std::stringstream uncompressed;

        const unsigned long allocated_buffer_size = 1024*32;
        unsigned char* buffer = (unsigned char*)malloc(allocated_buffer_size);
        unsigned long read_size = 0;
        unsigned long buf_size = allocated_buffer_size;
        bool exit = false;
        while (ReadRequestResponse(handle, buffer, buf_size, &exit))
        {
            if (!b_headers_processed)
            {
                ReadResponseHeaders(handle, headers_out);

                //check content encoding
                for (size_t hdr_idx = 0; hdr_idx<headers_out->size(); hdr_idx++)
                {
                    if (is_gzip_compressed(headers_out->at(hdr_idx)))
                    {
                        fos.push(boost::iostreams::gzip_decompressor());
                        break;
                    }
                }
                fos.push(uncompressed);

                b_headers_processed = true;
            }

            if (buf_size > 0)
            {
                try {
                    ret_val = fos.write((const char*)buffer, buf_size).good();
                } catch (...) 
                {
                    ret_val = false;
                }
            } else
            {
                break;
            }

            read_size += buf_size;

            //break on error during writing
            if (!ret_val)
                break;

            buf_size = allocated_buffer_size;
        }

        free(buffer);

        ret_val = ret_val & (read_size > 0);

        if (ret_val)
        {
            boost::iostreams::close(fos);
            response = uncompressed.str();
        }

        CloseRequest(handle);
    }

    return ret_val;
}

bool http_comm_handler::ExecuteGetWithResponse(const char* url_suffix,
    bool bSecure, std::string& response, http_headers_t* headers,
    http_headers_t* out_headers)
{
    if (!url_suffix)
    {
        return false;
    }

    bool ok = true;

    try
    {
        std::string url;
        ok = _make_url(address_, url_suffix, url, bSecure, port_);

        if (!ok)
        {
            return false;
        }

        http_request::callbacks_ptr cb_ptr(new http_request_cb(response));
        http_get get(url, cb_ptr);

        if (client_cert_name_.empty())
        {
            ok = _set_parameters(get, user_agent_, login_, password_, port_,
                auth_, data_dir_, ca_cert_name_,
                client_cert_, client_priv_key_);
        }
        else
        {
            ok = _set_parameters(get, user_agent_, login_, password_, port_,
                auth_, data_dir_, ca_cert_name_, client_cert_name_);
        }

        if (!ok)
        {
            return false;
        }

        if (headers && !headers->empty())
        {
            ok = _set_custom_headers(get, *headers);

            if (!ok)
            {
                return false;
            }
        }

        ok = get.execute(timeout_sec_, last_effective_url_);

        if (out_headers)
        {
            http_request_cb* ptr = static_cast<http_request_cb*>(cb_ptr.get());
            out_headers->swap(ptr->headers_);
        }
    }
    catch (...)
    {
        return false;
    }

    return ok;
}

bool http_comm_handler::ExecuteDeleteWithResponse(const char* url_suffix,
    bool bSecure, std::string& response, http_headers_t* headers,
    http_headers_t* out_headers)
{
    if (!url_suffix)
    {
        return false;
    }

    bool ok = true;

    try
    {
        std::string url;
        ok = _make_url(address_, url_suffix, url, bSecure, port_);

        if (!ok)
        {
            return false;
        }

        http_request::callbacks_ptr cb_ptr(new http_request_cb(response));
        http_delete del(url, cb_ptr);

        if (client_cert_name_.empty())
        {
            ok = _set_parameters(del, user_agent_, login_, password_, port_,
                auth_, data_dir_, ca_cert_name_,
                client_cert_, client_priv_key_);
        }
        else
        {
            ok = _set_parameters(del, user_agent_, login_, password_, port_,
                auth_, data_dir_, ca_cert_name_, client_cert_name_);
        }

        if (!ok)
        {
            return false;
        }

        if (headers && !headers->empty())
        {
            ok = _set_custom_headers(del, *headers);

            if (!ok)
            {
                return false;
            }
        }

        ok = del.execute(timeout_sec_, last_effective_url_);

        if (out_headers)
        {
            http_request_cb* ptr = static_cast<http_request_cb*>(cb_ptr.get());
            out_headers->swap(ptr->headers_);
        }
    }
    catch (...)
    {
        return false;
    }

    return ok;
}

bool http_comm_handler::ExecutePostWithResponse(const char* url_suffix,
    const char* post_data, bool bSecure, std::string& response,
    http_headers_t* headers, http_headers_t* out_headers)
{
    if (!post_data)
        return false;

    std::string data(post_data);
    return ExecutePostDataWithResponse(url_suffix, data.c_str(), data.size(), bSecure, response, headers, out_headers);
}

bool http_comm_handler::ExecutePostDataWithResponse(const char* url_suffix,
    const char* post_data, size_t post_data_size, bool bSecure, std::string& response, 
    http_headers_t* headers, http_headers_t* out_headers)
{
    if (!url_suffix)
    {
        return false;
    }

    bool ok = true;

    try
    {
        std::string url;
        ok = _make_url(address_, url_suffix, url, bSecure, port_);

        if (!ok)
        {
            return false;
        }

        http_request::callbacks_ptr cb_ptr(new http_request_cb(response));
        http_post post(url, cb_ptr, post_data, post_data_size);

        if (client_cert_name_.empty())
        {
            ok = _set_parameters(post, user_agent_, login_, password_, port_,
                auth_, data_dir_, ca_cert_name_,
                client_cert_, client_priv_key_);
        }
        else
        {
            ok = _set_parameters(post, user_agent_, login_, password_, port_,
                auth_, data_dir_, ca_cert_name_, client_cert_name_);
        }

        if (!ok)
        {
            return false;
        }

        if (headers && !headers->empty())
        {
            ok = _set_custom_headers(post, *headers);

            if (!ok)
            {
                return false;
            }
        }

        ok = post.execute(timeout_sec_, last_effective_url_);

        if (out_headers)
        {
            http_request_cb* ptr = static_cast<http_request_cb*>(cb_ptr.get());
            out_headers->swap(ptr->headers_);
        }
    }
    catch (...)
    {
        return false;
    }

    return ok;
}

bool http_comm_handler::ExecutePutWithResponse(const char* url_suffix,
    const char* put_data, size_t put_data_size, bool bSecure, std::string& response, 
    http_headers_t* headers, http_headers_t* out_headers)
{
    if (!url_suffix || !put_data)
    {
        return false;
    }

    bool ok = true;

    try
    {
        std::string url;
        ok = _make_url(address_, url_suffix, url, bSecure, port_);

        if (!ok)
        {
            return false;
        }

        http_request::callbacks_ptr cb_ptr(new http_request_cb(response));
        http_put put(url, cb_ptr, put_data, put_data_size);

        if (client_cert_name_.empty())
        {
            ok = _set_parameters(put, user_agent_, login_, password_, port_,
                auth_, data_dir_, ca_cert_name_,
                client_cert_, client_priv_key_);
        }
        else
        {
            ok = _set_parameters(put, user_agent_, login_, password_, port_,
                auth_, data_dir_, ca_cert_name_, client_cert_name_);
        }

        if (!ok)
        {
            return false;
        }

        if (headers && !headers->empty())
        {
            ok = _set_custom_headers(put, *headers);

            if (!ok)
            {
                return false;
            }
        }

        ok = put.execute(timeout_sec_, last_effective_url_);

        if (out_headers)
        {
            http_request_cb* ptr = static_cast<http_request_cb*>(cb_ptr.get());
            out_headers->swap(ptr->headers_);
        }
    }
    catch (...)
    {
        return false;
    }

    return ok;
}

namespace {
    struct request_handle
    {
        dvblink::engine::fifo_buffer buf_;
        http_comm_handler::http_headers_t headers_;
        std::string last_effective_url_;

        http_request_ptr req_ptr_;
        http_request::callbacks_ptr cb_ptr_;

        bool started_;
        bool finished_;
        bool success_;
        dvblink::event finish_event_;

        typedef boost::shared_ptr<boost::thread> thread_ptr;
        thread_ptr thread_ptr_;
        long req_timeout_sec_;

        request_handle() :
        started_(false), finished_(false), success_(false), req_timeout_sec_(0)
        {
        }

        ~request_handle()
        {
            stop();
            //req_ptr_ = http_request_ptr();
            //cb_ptr_ = http_request::callbacks_ptr();
        }

        bool start()
        {
            if (started_)
            {
                return false;
            }

            finish_event_.reset();
            finished_ = false;
            started_ = true;

            thread_ptr_ = thread_ptr(new boost::thread(boost::bind(
                &request_handle::thread_func, this)));

            return true;
        }

        bool stop()
        {
            assert(req_ptr_);

            if (!started_)
            {
                return false;
            }

            if (finished_ && thread_ptr_)
            {
                thread_ptr_->join();
                thread_ptr_.reset();
                return true;
            }

            if (started_ && !finished_)
            {
                req_ptr_->cancel();
                finish_event_.wait();

                if (finished_)
                {
                    thread_ptr_->join();
                    thread_ptr_ = thread_ptr();
                }
                else
                {
                    req_ptr_->close();
                    finish_event_.wait();

                    if (finished_)
                    {
                        thread_ptr_->join();
                        thread_ptr_ = thread_ptr();
                    }
                }
            }

            return finished_;
        }

    private:
        void thread_func()
        {
            assert(req_ptr_);

            success_ = req_ptr_->execute(req_timeout_sec_, last_effective_url_);

            finished_ = true;
            finish_event_.signal();
        }
    };

    struct request_handle_cb : public http_request::callbacks,
        private boost::noncopyable
    {
        request_handle_cb(request_handle* h) : handle_(h)
        {
            assert(handle_);
        }

        void on_data_received(const void* data, size_t size)
        {
            if (data && size)
            {
                handle_->buf_.write((const unsigned char*)data, size);
            }
        }

        void on_header_received(const std::string& hdr)
        {
            if (!hdr.empty())
            {
                //If yes, delete all collected headers - there will be a set of new ones (usually after redirect)
                std::string protocol;
                std::string version;
                unsigned int status_code;
                std::string text;
                if (parse_status_line(hdr, protocol, version, status_code, text))
                    handle_->headers_.clear();

                handle_->headers_.push_back(hdr);
            }
        }

        void on_request_completed(bool success)
        {
            if (!success)
            {
            }
        }

        request_handle* handle_;
    };
}

void* http_comm_handler::SendGetRequest(const char* url_suffix,
    bool bSecure, http_headers_t* headers)
{
    if (!url_suffix)
    {
        return NULL;
    }

    bool ok = true;
    request_handle* handle = NULL;

    try
    {
        std::string url;
        ok = _make_url(address_, url_suffix, url, bSecure, port_);

        if (!ok)
        {
            return NULL;
        }

        handle = new request_handle();

        handle->cb_ptr_ = http_request::callbacks_ptr(
            new request_handle_cb(handle));

        handle->req_ptr_ = http_request_ptr(
            new http_get(url, handle->cb_ptr_));

        if (client_cert_name_.empty())
        {
            ok = _set_parameters(*(handle->req_ptr_), user_agent_, 
                login_, password_, port_,
                auth_, data_dir_, ca_cert_name_,
                client_cert_, client_priv_key_);
        }
        else
        {
            ok = _set_parameters(*(handle->req_ptr_), user_agent_, 
                login_, password_, port_,
                auth_, data_dir_, ca_cert_name_,
                client_cert_name_);
        }

        if (ok && headers && !headers->empty())
        {
            ok = _set_custom_headers(*(handle->req_ptr_), *headers);
        }

        if (ok)
        {
            ok = handle->start();
        }
    }
    catch (...)
    {
        ok = false;
    }

    if (!ok && handle)
    {
        delete handle;
        handle = NULL;
    }

    return handle;    
}

void* http_comm_handler::SendPostRequest(const char* url_suffix,
    const char* post_data, bool bSecure, http_headers_t* headers)
{
    if (!url_suffix || !post_data)
    {
        return NULL;
    }

    std::string data(post_data);

    if (data.size() == 0)
    {
        return NULL;
    }

    bool ok = true;
    request_handle* handle = NULL;

    try
    {
        std::string url;
        ok = _make_url(address_, url_suffix, url, bSecure, port_);

        if (!ok)
        {
            return NULL;
        }

        handle = new request_handle();

        handle->cb_ptr_ = http_request::callbacks_ptr(
            new request_handle_cb(handle));

        handle->req_ptr_ = http_request_ptr(new http_post(url,
            handle->cb_ptr_, data.c_str(), data.size()));

        if (client_cert_name_.empty())
        {
            ok = _set_parameters(*(handle->req_ptr_), user_agent_, 
                login_, password_, port_,
                auth_, data_dir_, ca_cert_name_,
                client_cert_, client_priv_key_);
        }
        else
        {
            ok = _set_parameters(*(handle->req_ptr_), user_agent_, 
                login_, password_, port_,
                auth_, data_dir_, ca_cert_name_,
                client_cert_name_);
        }

        if (ok && headers && !headers->empty())
        {
            ok = _set_custom_headers(*(handle->req_ptr_), *headers);
        }

        if (ok)
        {
            ok = handle->start();
        }
    }
    catch (...)
    {
        ok = false;
    }

    if (!ok && handle)
    {
        delete handle;
        handle = NULL;
    }

    return handle;    
}

bool http_comm_handler::ReadRequestResponse(void* handle,
    unsigned char* buffer, unsigned long& buffer_size, bool* b_exit_flag)
{
    if (!handle || !buffer || !buffer_size)
    {
        return false;
    }

    request_handle* rqh = static_cast<request_handle*>(handle);

    if (!rqh->started_)
    {
        buffer_size = 0;
        return false;
    }

    for ( ; ; )
    {
        if (b_exit_flag != NULL && *b_exit_flag)
        {
            rqh->req_ptr_->close_socket();
        }

        size_t size = rqh->buf_.size();

        if (size > 0)
        {
            if (buffer_size < size)
            {
                size = buffer_size;
            }

            bool ok = rqh->buf_.read(buffer, size);

            if (ok)
            {
                buffer_size = size;
                return true;
            }
        }
        else // size == 0
        {
            rqh->buf_.wait_for_readable(boost::posix_time::milliseconds(100));

            if (rqh->finished_ && (rqh->buf_.size() == 0))
            {
                buffer_size = 0;
                return false;
            }
        }
    }

    // never reached
    // return false;
}

bool http_comm_handler::CancelRequest(void* handle)
{
    if (!handle)
    {
        return false;
    }

    request_handle* rqh = static_cast<request_handle*>(handle);
    bool ok = rqh->stop();

    return ok;
}

void http_comm_handler::CloseRequest(void* handle)
{
    request_handle* rqh = static_cast<request_handle*>(handle);
    delete rqh;
}

bool http_comm_handler::ReadResponseHeaders(void* req_handle,
    http_headers_t* headers)
{
    if (!req_handle || !headers)
    {
        return false;
    }

    request_handle* h = static_cast<request_handle*>(req_handle);
    *headers = h->headers_;

    return true;
}

} // namespace dvblink

// $Id: http_comm.curl.cpp 13094 2016-07-25 20:05:09Z pashab $
