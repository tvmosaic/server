/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#ifdef _WIN32
#include <ws2tcpip.h>
#else
#include <netdb.h>
#include <arpa/inet.h>
#endif

#include <cstdio>
#include <cstring>
#include <dl_sock_addr.h>

namespace dvblink {

sock_addr::sock_addr(dvblink::ip_version ipver)
{
    ::memset(&saddr_, 0, sizeof(saddr_));

    if (ipver == ip_v6)
    {
        saddr_.sa_family = AF_INET6;
    }
    else
    {
        sockaddr_in* paddr = (sockaddr_in*)&saddr_;
        paddr->sin_family = AF_INET;
        paddr->sin_addr.s_addr = INADDR_ANY;
        paddr->sin_port = 0;
    }
}

sock_addr::sock_addr(const struct sockaddr& sa)
{
    saddr_ = sa;
}

sock_addr::sock_addr(const sock_addr& addr)
{
    *this = addr;
}

sock_addr& sock_addr::operator =(const sock_addr& rhs)
{
    saddr_ = rhs.saddr_;
    return *this;
}

ip_version sock_addr::get_ip_version() const
{
    if (saddr_.sa_family == AF_INET6)
    {
        return ip_v6;
    }

    return ip_v4;
}

port_t sock_addr::get_port() const
{
    if (saddr_.sa_family == AF_INET6)
    {
        const sockaddr_in6* p = (const sockaddr_in6*)&saddr_;
        return ntohs(p->sin6_port);
    }

    const sockaddr_in* p = (const sockaddr_in*)&saddr_;
    return ntohs(p->sin_port);
}

void sock_addr::set_port(dvblink::port_t port)
{
    if (saddr_.sa_family == AF_INET6)
    {
        sockaddr_in6* p = (sockaddr_in6*)&saddr_;
        p->sin6_port = htons(port);
    }
    else
    {
        sockaddr_in* p = (sockaddr_in*)&saddr_;
        p->sin_port = htons(port);
    }
}

bool sock_addr::is_inaddr_any()
{
    sockaddr_in* paddr = (sockaddr_in*)&saddr_;

    return (paddr->sin_addr.s_addr == INADDR_ANY);
}

errcode_t sock_addr::set_address(const std::string& ipaddr)
{
    if (get_ip_version() == ip_v6)
    {
        return err_not_implemented;
    }

    sockaddr_in* paddr = (sockaddr_in*)&saddr_;

    if (ipaddr.empty())
    {
        paddr->sin_addr.s_addr = INADDR_ANY;
        return err_none;
    }

#ifdef __ANDROID__

    inet_aton(ipaddr.c_str(), &paddr->sin_addr);
    return err_none;

#else
    unsigned int u1 = 0, u2 = 0, u3 = 0, u4 = 0;

#ifdef _WIN32
    int n = sscanf_s(ipaddr.c_str(), "%u.%u.%u.%u", &u1, &u2, &u3, &u4);
#else
    int n = sscanf(ipaddr.c_str(), "%u.%u.%u.%u", &u1, &u2, &u3, &u4);
#endif

    if (n != 4)
    {
        // resolve host name
        struct hostent* he = ::gethostbyname(ipaddr.c_str());

        if (he)
        {
            ::memcpy(&(paddr->sin_addr), he->h_addr, he->h_length);            
        }
        else
        {
            return err_not_resolved;
        }
    }
    else
    {
        // dotted address
        if ((u1 > 255) || (u2 > 255) || (u3 > 255) || (u4 > 255))
        {
            return err_invalid_param;
        }

    #ifndef _TVM_BIG_ENDIAN
        paddr->sin_addr.s_addr = ((u1) | (u2 << 8) | (u3 << 16) | (u4 << 24));
    #else
        paddr->sin_addr.s_addr = ((u4) | (u3 << 8) | (u2 << 16) | (u1 << 24));
    #endif
    }

    return err_none;
#endif
}

errcode_t sock_addr::get_address(std::string& ipaddr) const
{
    ipaddr.clear();

    if (get_ip_version() == ip_v6)
    {
        return err_not_implemented;
    }

    const sockaddr_in* paddr = (const sockaddr_in*)&saddr_;

#ifdef __ANDROID__

    ipaddr = inet_ntoa(paddr->sin_addr);
    return err_none;

#else

    char buf[32] = { 0 };
    unsigned long addr = paddr->sin_addr.s_addr;

    #ifdef _WIN32
    #define SPRINTF sprintf_s
    #else
    #define SPRINTF snprintf
    #endif

#ifndef _TVM_BIG_ENDIAN
    SPRINTF(buf, sizeof(buf), "%u.%u.%u.%u",
        boost::uint8_t((addr      ) & 0xFF),
        boost::uint8_t((addr >>  8) & 0xFF),
        boost::uint8_t((addr >> 16) & 0xFF),
        boost::uint8_t((addr >> 24) & 0xFF));
#else
    SPRINTF(buf, sizeof(buf), "%u.%u.%u.%u",
        boost::uint8_t((addr >> 24) & 0xFF),
        boost::uint8_t((addr >> 16) & 0xFF),
        boost::uint8_t((addr >>  8) & 0xFF),
        boost::uint8_t((addr      ) & 0xFF));
#endif
    ipaddr = buf;

    return err_none;
#endif
}

bool sock_addr::is_multicast() const
{
    const sockaddr_in* paddr = (const sockaddr_in*)&saddr_;
    unsigned long addr = paddr->sin_addr.s_addr;
    
#ifndef _TVM_BIG_ENDIAN    
    return ((addr & 0xE0) == 0xE0);
#else
    return (((addr >> 24) & 0xE0) == 0xE0);
#endif
}

} // namespace dvblink

// $Id: sock_addr.cpp 7679 2013-01-14 13:22:00Z mike $
