/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include "dl_vista_firewall.h"


CDLVistaFirewall::CDLVistaFirewall() 
    : m_pNetFwPolicy(NULL)
{

}

CDLVistaFirewall::~CDLVistaFirewall()
{

}

FW_ERROR_CODE CDLVistaFirewall::Initialize()
{
	HRESULT hr = S_FALSE;

	FW_ERROR_CODE ret = FW_NOERROR;
	try
	{
		if (m_pNetFwPolicy)
		{
			throw FW_ERR_INITIALIZED;
		}

		hr = CoCreateInstance(__uuidof(NetFwPolicy2), NULL, CLSCTX_INPROC_SERVER, __uuidof(INetFwPolicy2), (void**)&m_pNetFwPolicy);
		if (FAILED(hr))
		{
			throw FW_ERR_CREATE_SETTING_MANAGER;  
		}
	}
	catch (FW_ERROR_CODE nError)
	{
		ret = nError;
	}

	return ret;
}

FW_ERROR_CODE CDLVistaFirewall::Uninitialize()
{
	if (m_pNetFwPolicy)
	{
		m_pNetFwPolicy->Release();
		m_pNetFwPolicy = NULL;
	}

	return FW_NOERROR;
}

FW_ERROR_CODE CDLVistaFirewall::AddRule(const wchar_t* lpszProcessImageFileName, 
										const wchar_t* lpszRuleName, 
										const wchar_t* lpszRuleGroupName)
{
	FW_ERROR_CODE ret = FW_NOERROR;
	HRESULT hr;
	INetFwRules* pFwRules = NULL;
    INetFwRule* pFwRule = NULL;
	BSTR bstrProcessImageFileName = NULL;
	BSTR bstrRuleName = NULL;
	BSTR bstrRuleGroupName = NULL;

	try
	{
		if (m_pNetFwPolicy == NULL)
		{
			throw FW_ERR_INITIALIZED;
		}

		if (lpszProcessImageFileName == NULL || lpszRuleName  == NULL || lpszRuleGroupName == NULL)
		{
			throw FW_ERR_INVALID_ARG;
		}

        bstrProcessImageFileName = SysAllocString(lpszProcessImageFileName);
		if (SysStringLen(bstrProcessImageFileName) == 0)
		{
			throw FW_ERR_SYS_ALLOC_STRING;
		}

		bstrRuleName = SysAllocString(lpszRuleName);
		if (SysStringLen(bstrRuleName) == 0)
		{
			throw FW_ERR_SYS_ALLOC_STRING;
		}

		bstrRuleGroupName = SysAllocString(lpszRuleGroupName);
		if (SysStringLen(bstrRuleGroupName) == 0)
		{
			throw FW_ERR_SYS_ALLOC_STRING;
		}

		// Retrieve INetFwRules
		hr = m_pNetFwPolicy->get_Rules(&pFwRules);
		if (FAILED(hr))
		{
			throw FW_UNKNOWN;
		}

        // Get rule, if exist return no error
        hr = pFwRules->Item(bstrRuleName, &pFwRule);
        if (SUCCEEDED(hr))
		{
            throw FW_NOERROR;
        }

        // Create a new Firewall Rule object
        hr = CoCreateInstance(__uuidof(NetFwRule), NULL, CLSCTX_INPROC_SERVER, __uuidof(INetFwRule), (void**)&pFwRule);
        if (FAILED(hr))
        {
            throw FW_UNKNOWN;
        }

		// Populate the Firewall Rule object
		pFwRule->put_Name(bstrRuleName);
		pFwRule->put_ApplicationName(bstrProcessImageFileName);
		pFwRule->put_Protocol(NET_FW_IP_PROTOCOL_ANY);
		pFwRule->put_Grouping(bstrRuleGroupName);
		pFwRule->put_Profiles(NET_FW_PROFILE2_ALL);
		pFwRule->put_Action(NET_FW_ACTION_ALLOW);
		pFwRule->put_Enabled(VARIANT_TRUE);
		
		// Add the Firewall Rule
		hr = pFwRules->Add(pFwRule);
		if (FAILED(hr))
		{
			throw FW_UNKNOWN;
		}
	}
	catch (FW_ERROR_CODE nError)
	{
		ret = nError;
	}

	SysFreeString(bstrProcessImageFileName);
	SysFreeString(bstrRuleName);
	SysFreeString(bstrRuleGroupName);

	if (pFwRule)
	{
		pFwRule->Release();
	}
	
	if (pFwRules)
	{
		pFwRules->Release();
	}

	return ret;
}

FW_ERROR_CODE CDLVistaFirewall::RemoveRule(const wchar_t* lpszRuleName)
{
	FW_ERROR_CODE ret = FW_NOERROR;
	HRESULT hr;
	INetFwRules* pFwRules = NULL;
	BSTR bstrRuleName = NULL;

	try
	{
		if (m_pNetFwPolicy == NULL)
		{
			throw FW_ERR_INITIALIZED;
		}

		if (lpszRuleName  == NULL)
		{
			throw FW_ERR_INVALID_ARG;
		}

		// Retrieve INetFwRules
		hr = m_pNetFwPolicy->get_Rules(&pFwRules);
		if (FAILED(hr))
		{
			throw FW_UNKNOWN;
		}

		bstrRuleName = SysAllocString(lpszRuleName);
		if (SysStringLen(bstrRuleName) == 0)
		{
			throw FW_ERR_SYS_ALLOC_STRING;
		}
	
		// Remove the Firewall Rule
		hr = pFwRules->Remove(bstrRuleName);
		if (FAILED(hr))
		{
			throw FW_UNKNOWN;
		}
	}
	catch (FW_ERROR_CODE nError)
	{
		ret = nError;
	}

	SysFreeString(bstrRuleName);

	if (pFwRules)
	{
		pFwRules->Release();
	}

	return ret;
}