/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/algorithm/string.hpp>
#include <string>
#include <stdexcept>
#include <dl_common.h>
#include <dl_debug.h>
#include <dl_strings.h>
#include <dl_logger.h>
#include <boost/filesystem.hpp>
#include <openssl/ssl.h>
#include "http_request.h"

namespace fs = boost::filesystem;

namespace dvblink {

using namespace engine;

//-----------------------------------------------------------------------------
// http_request
//-----------------------------------------------------------------------------

struct http_request::global_init
{
    global_init()
    {
        // this function sets up the program environment
        // that curl needs
        //
        CURLcode err = curl_global_init(CURL_GLOBAL_ALL);

        if (err != CURLE_OK)
        {
            throw std::runtime_error("curl_global_init() failed");
        }
    }

    ~global_init()
    {
        curl_global_cleanup();
    }
};

// static members
boost::mutex http_request::global_lock_;
boost::shared_ptr<http_request::global_init> http_request::global_init_ptr_;

int http_request::socket_cb(void *clientp, curl_socket_t curlfd, curlsocktype purpose)
{
    http_request* obj = static_cast<http_request*>(clientp);
    obj->socket_ = curlfd;
    return 0;
}

http_request::http_request(const std::string& url, callbacks_ptr cb_ptr) :
curl_handle_(NULL), http_headers_(NULL), cb_ptr_(cb_ptr),
    canceled_(false), running_(false)
{
    assert(!url.empty());
    assert(cb_ptr_);

    socket_ = CURL_SOCKET_BAD;

    if (!global_init_ptr_)
    {
        boost::mutex::scoped_lock lock(global_lock_);

        if (!global_init_ptr_)
        {
            global_init_ptr_ = boost::shared_ptr<global_init>(new global_init);
        }
    }

    // init the curl session
    curl_handle_ = curl_easy_init();

    if (!curl_handle_)
    {
        throw std::runtime_error("curl_easy_init() failed");
    }

    curl_easy_setopt(curl_handle_, CURLOPT_ERRORBUFFER, errbuf_);

    curl_easy_setopt(curl_handle_, CURLOPT_USERAGENT, PRODUCT_NAME_UTF8);

    //curl_easy_setopt(curl_handle_, CURLOPT_HEADER, 1);
    //curl_easy_setopt(curl_handle_, CURLOPT_VERBOSE, 1);
    curl_easy_setopt(curl_handle_, CURLOPT_SSL_VERIFYPEER, 0);
    curl_easy_setopt(curl_handle_, CURLOPT_FOLLOWLOCATION, 1);

    CURLcode err = curl_easy_setopt(curl_handle_, CURLOPT_URL, url.c_str());

    if (err != CURLE_OK)
    {
        throw std::runtime_error("invalid url");
    }

#ifdef __ANDROID__
    //android oreo has disabled access to the DNS servers information
    curl_easy_setopt(curl_handle_, CURLOPT_DNS_SERVERS, "8.8.8.8,208.67.222.222,8.8.4.4,208.67.220.220");
#endif

    curl_easy_setopt(curl_handle_, CURLOPT_WRITEDATA, (void*)this);
    curl_easy_setopt(curl_handle_, CURLOPT_WRITEHEADER, (void*)this);
    curl_easy_setopt(curl_handle_, CURLOPT_SOCKOPTDATA, (void*)this);
    curl_easy_setopt(curl_handle_, CURLOPT_PROGRESSDATA, (void*)this);

    curl_easy_setopt(curl_handle_, CURLOPT_NOPROGRESS, 0);
    curl_easy_setopt(curl_handle_, CURLOPT_PROGRESSFUNCTION,
        &http_request::on_progress);

    curl_easy_setopt(curl_handle_, CURLOPT_WRITEFUNCTION,
        &http_request::on_data_received);
    curl_easy_setopt(curl_handle_, CURLOPT_HEADERFUNCTION,
        &http_request::on_header_received);

    curl_easy_setopt(curl_handle_, CURLOPT_SOCKOPTFUNCTION,
        &http_request::socket_cb);
}

http_request::~http_request()
{
    close();
}

bool http_request::close()
{
    reset_http_headers();

    boost::mutex::scoped_lock lock(lock_);

    if (curl_handle_)
    {
        curl_easy_cleanup(curl_handle_);
        curl_handle_ = NULL;
        return true;
    }

    return false;
}

bool http_request::set_ca_cert_path(const std::string& cert_path,
    bool verify_peer)
{
    boost::mutex::scoped_lock lock(lock_);

    if (!curl_handle_)
    {
        return false;
    }

    if (cert_path.empty())
    {
        ca_cert_path_.clear();
        curl_easy_setopt(curl_handle_, CURLOPT_SSL_VERIFYPEER, 0);
        return true;
    }

    // check path
    fs::path fspath(cert_path);

    if (!fs::exists(fspath) || !fs::is_regular_file(fspath))
    {
        return false;
    }

    ca_cert_path_ = cert_path;

    if (verify_peer)
    {
        curl_easy_setopt(curl_handle_, CURLOPT_SSL_VERIFYPEER, 1);
    }

    CURLcode err = curl_easy_setopt(curl_handle_,
        CURLOPT_CAINFO, ca_cert_path_.c_str());

    return (err == CURLE_OK);
}

bool http_request::set_client_cert_path(const std::string& cert_path)
{
    boost::mutex::scoped_lock lock(lock_);

    if (!curl_handle_ || cert_path.empty())
    {
        return false;
    }

    curl_easy_setopt(curl_handle_, CURLOPT_SSLCERTTYPE, "PEM");

    CURLcode err = curl_easy_setopt(curl_handle_,
        CURLOPT_SSLCERT, cert_path.c_str());

    return (err == CURLE_OK);
}

// CURL callback function
//
static CURLcode sslctx_function(CURL *curl, void *sslctx, void *parm)
{
    BIO *bio = NULL;
    BIO *kbio = NULL;
    X509 *cert = NULL;
    RSA *rsa = NULL;

    do 
    {
        http_request::ssl_ctx_data* ctx_data =
            (http_request::ssl_ctx_data*)parm;

        if (!ctx_data)
        {
            break;
        }

        const char *pem = ctx_data->pem.c_str();
        const char *key = ctx_data->key.c_str();

        if (!pem || !key)
        {
            break;
        }

        // get a BIO
        bio = BIO_new_mem_buf((void*)pem, -1);

        if (!bio)
        {
            break;
        }

        // Read the PEM formatted certificate from memory into an X509
        // structure that SSL can use
        //
        cert = PEM_read_bio_X509(bio, NULL, 0, NULL);

        if (!cert)
        {
            break;
        }

        // Tell SSL to use the X509 certificate
        //
        int ret = SSL_CTX_use_certificate((SSL_CTX*)sslctx, cert);

        if (ret != 1)
        {
            break;
        }

        // Create a bio for the RSA key
        //
        kbio = BIO_new_mem_buf((void*)key, -1);

        if (!kbio)
        {
            break;
        }

        // Read the key bio into an RSA object
        //
        rsa = PEM_read_bio_RSAPrivateKey(kbio, NULL, 0, NULL);

        if (!rsa)
        {
            break;
        }

        // Tell SSL to use the RSA key from memory
        //
        ret = SSL_CTX_use_RSAPrivateKey((SSL_CTX*)sslctx, rsa);

        if (ret != 1)
        {
            break;
        }
    }
    while (0);

    // Free resources that have been allocated by OpenSSL functions
    //
    if (bio)
    {
        BIO_free(bio);
    }

    if (kbio)
    {
        BIO_free(kbio);
    }

    if (rsa)
    {
        RSA_free(rsa);
    }

    if (cert)
    {
        X509_free(cert);
    }

    return CURLE_OK ;
}

bool http_request::set_client_cert(const std::string& pem,
    const std::string& key)
{
    boost::mutex::scoped_lock lock(lock_);

    CURLcode err = CURLE_OK;

    do 
    {
        err = curl_easy_setopt(curl_handle_, CURLOPT_SSLCERTTYPE, "PEM");

        if (err != CURLE_OK)
        {
            break;
        }

        ssl_ctx_data_.pem = pem;
        ssl_ctx_data_.key = key;

        err = curl_easy_setopt(curl_handle_,
            CURLOPT_SSL_CTX_DATA, &ssl_ctx_data_);

        if (err != CURLE_OK)
        {
            break;
        }

        err = curl_easy_setopt(curl_handle_,
            CURLOPT_SSL_CTX_FUNCTION, sslctx_function);

        if (err != CURLE_OK)
        {
            break;
        }
    }
    while (0);

    return (err == CURLE_OK);
}

bool http_request::set_cookie_file(const std::string& file_path)
{
    boost::mutex::scoped_lock lock(lock_);

    if (!curl_handle_ || file_path.empty())
    {
        return false;
    }

    CURLcode err = curl_easy_setopt(curl_handle_,
        CURLOPT_COOKIEJAR, file_path.c_str());

    if (err == CURLE_OK)
    {
        curl_easy_setopt(curl_handle_,
            CURLOPT_COOKIEFILE, file_path.c_str());
    }

    return (err == CURLE_OK);
}

bool http_request::set_user_agent(const std::string& user_agent)
{
    boost::mutex::scoped_lock lock(lock_);

    if (!curl_handle_)
    {
        return false;
    }

    CURLcode err = curl_easy_setopt(curl_handle_, CURLOPT_USERAGENT, user_agent.c_str());

    return (err == CURLE_OK);
}

bool http_request::set_credentials(const std::string& username,
    const std::string& password, unsigned long curl_auth)
{
    if (!password.empty() && username.empty())
    {
        return false;
    }

    boost::mutex::scoped_lock lock(lock_);

    if (!curl_handle_)
    {
        return false;
    }

    curl_easy_setopt(curl_handle_, CURLOPT_HTTPAUTH, curl_auth);

    CURLcode err = curl_easy_setopt(curl_handle_,
        CURLOPT_USERNAME, username.c_str());

    if (err == CURLE_OK)
    {
        err = curl_easy_setopt(curl_handle_,
            CURLOPT_PASSWORD, password.c_str());
    }

    return (err == CURLE_OK);
}

bool http_request::set_port(boost::uint16_t port)
{
    if (port == 0)
    {
        return false;
    }

    boost::mutex::scoped_lock lock(lock_);

    if (!curl_handle_)
    {
        return false;
    }

    CURLcode err = curl_easy_setopt(curl_handle_, CURLOPT_PORT, port);
    return (err == CURLE_OK);
}

bool http_request::add_http_header(const std::string& http_header)
{
    if (http_header.empty())
    {
        return false;
    }

    boost::mutex::scoped_lock lock(lock_);

    if (!curl_handle_)
    {
        return false;
    }

    struct curl_slist* new_headers = curl_slist_append(http_headers_,
        http_header.c_str());

    if (!new_headers)
    {
        return false;
    }

    http_headers_ = new_headers;
    return true;
}

bool http_request::set_http_headers()
{
    boost::mutex::scoped_lock lock(lock_);

    if (!curl_handle_ || !http_headers_)
    {
        return false;
    }

    CURLcode err = curl_easy_setopt(curl_handle_,
        CURLOPT_HTTPHEADER, http_headers_);

    return (err == CURLE_OK);
}

bool http_request::reset_http_headers()
{
    boost::mutex::scoped_lock lock(lock_);

    if (http_headers_)
    {
        curl_slist_free_all(http_headers_);
        http_headers_ = NULL;
    }

    if (!curl_handle_)
    {
        return false;
    }

    CURLcode err = curl_easy_setopt(curl_handle_, CURLOPT_HTTPHEADER, NULL);
    return (err == CURLE_OK);
}

// This function gets called by curl as soon as there is data
// received that needs to be saved. The size of the data pointed to
// by `ptr` is `size` multiplied with `nmemb`.
//
size_t http_request::on_data_received(void* ptr, size_t size,
    size_t nmemb, void* user_data)
{
    size_t len = size * nmemb;
    http_request* obj = static_cast<http_request*>(user_data);

    if (!user_data || !ptr || (len == 0))
    {
        return 0;
    }
    else
    {
        boost::mutex::scoped_lock lock(obj->lock_);

        if (obj->canceled_)
        {
            return 0;
        }
    }

    //_TRACE("--- on_data_received(%u)\n", len);
    obj->cb_ptr_->on_data_received(ptr, len);
    //_TRACE("---\n");

    // return the number of bytes actually taken care of
    return len;
}

size_t http_request::on_header_received(void* ptr, size_t size,
    size_t nmemb, void* user_data)
{
    size_t len = size * nmemb;
    http_request* obj = static_cast<http_request*>(user_data);

    if (!user_data || !ptr || (len == 0))
    {
        return 0;
    }
    else
    {
        boost::mutex::scoped_lock lock(obj->lock_);

        if (obj->canceled_)
        {
            return 0;
        }
    }

    std::string str((char*)ptr, (char*)ptr + len);    

    // remove CR-LF
    boost::replace_all(str, "\r", "");
    boost::replace_all(str, "\n", "");

    if (!str.empty())
    {
        obj->cb_ptr_->on_header_received(str);
    }

    // return the number of bytes actually taken care of
    return len;
}

int http_request::on_progress(void* clientp, double dltotal, double dlnow, double ultotal, double ulnow)
{
    http_request* obj = static_cast<http_request*>(clientp);

	boost::mutex::scoped_lock lock(obj->lock_);

	if (obj->canceled_)
	{
		return 1;
	}

    return 0;
}

bool http_request::execute(long timeout_sec, std::string& effective_url)
{
    effective_url.clear();

    // check state
    {
        boost::mutex::scoped_lock lock(lock_);

        if (curl_handle_ == NULL)
        {
            return false;
        }

        if (running_ || canceled_)
        {
            return false; // in progress
        }

        running_ = true;
    }

    if (timeout_sec != 0)
        curl_easy_setopt(curl_handle_, CURLOPT_CONNECTTIMEOUT, timeout_sec);

    errbuf_[0] = 0;

    CURLcode err = curl_easy_perform(curl_handle_);

    if (err != CURLE_OK)
    {
        logging::log_error(L"http_request::execute error %1%") % err;
        logging::log_error(L"http_request::execute error description %1%") % string_cast<EC_UTF8>(std::string(errbuf_));
    }

    bool ok = (err == CURLE_OK);

    //get actual resource location (in case of redirect)
    char* _effective_url = NULL;
    curl_easy_getinfo(curl_handle_, CURLINFO_EFFECTIVE_URL, &_effective_url);
    if (_effective_url != NULL)
        effective_url = _effective_url;

    cb_ptr_->on_request_completed(ok);

    {
        boost::mutex::scoped_lock lock(lock_);
        running_ = false;
        canceled_ = false;
    }

    return ok;
}

void http_request::close_socket()
{
    if (curl_handle_ != NULL)
    {
#ifdef WIN32
        closesocket(socket_);
#else		
        ::close(socket_);
#endif		
    }
}

bool http_request::cancel()
{
    boost::mutex::scoped_lock lock(lock_);

    if (!running_ || canceled_ || (curl_handle_ == NULL))
    {
        return false;
    }

    canceled_ = true;
    return true;
}

//-----------------------------------------------------------------------------
// http_get
//-----------------------------------------------------------------------------

http_get::http_get(const std::string& url, http_get::callbacks_ptr cb_ptr) :
http_request(url, cb_ptr)
{
}

bool http_get::set_new_url(const std::string& url)
{
    if (url.empty() || running_)
    {
        return false;
    }

    boost::mutex::scoped_lock lock(lock_);

    if (!curl_handle_)
    {
        return false;
    }

    CURLcode err = curl_easy_setopt(curl_handle_, CURLOPT_URL, url.c_str());
    return (err == CURLE_OK);
}

//-----------------------------------------------------------------------------
// http_delete
//-----------------------------------------------------------------------------

http_delete::http_delete(const std::string& url,
    http_delete::callbacks_ptr cb_ptr) : http_request(url, cb_ptr)
{
    curl_easy_setopt(curl_handle_, CURLOPT_CUSTOMREQUEST, "DELETE");
}

//-----------------------------------------------------------------------------
// http_post
//-----------------------------------------------------------------------------

http_post::http_post(const std::string& url, callbacks_ptr cb_ptr,
    const void* data, size_t size) : http_request(url, cb_ptr)
{
    if (data != NULL && size > 0)
        data_.assign(static_cast<const char*>(data), size);

    curl_easy_setopt(curl_handle_, CURLOPT_POST, 1);

    CURLcode err = curl_easy_setopt(curl_handle_,
        CURLOPT_POSTFIELDS, &data_[0]);

    if (err == CURLE_OK)
    {
        err = curl_easy_setopt(curl_handle_,
            CURLOPT_POSTFIELDSIZE, data_.size());
    }

    if (err != CURLE_OK)
    {
        throw std::runtime_error("http_post::http_post() : invalid data");
    }
}

//-----------------------------------------------------------------------------
// http_put
//-----------------------------------------------------------------------------

http_put::http_put(const std::string& url, callbacks_ptr cb_ptr,
    const void* data, size_t size) : http_request(url, cb_ptr)
{
    assert(data);
    data_.assign(static_cast<const char*>(data), size);

    //curl_easy_setopt(curl_handle_, CURLOPT_PUT, 1);
    curl_easy_setopt(curl_handle_, CURLOPT_CUSTOMREQUEST, "PUT");

    CURLcode err = curl_easy_setopt(curl_handle_, CURLOPT_POSTFIELDS, &data_[0]);
    if (err == CURLE_OK)
    {
        err = curl_easy_setopt(curl_handle_, CURLOPT_POSTFIELDSIZE, data_.size());
    }

    if (err != CURLE_OK)
    {
        throw std::runtime_error("http_put::http_put() : invalid data");
    }
}

} // namespace dvblink

// $Id: http_request.cpp 13093 2016-07-25 16:20:51Z pashab $
