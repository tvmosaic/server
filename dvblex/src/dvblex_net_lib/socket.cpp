/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"

#ifndef _WIN32
#include <errno.h>
#include <fcntl.h>
#endif

#include <dl_socket.h>

#ifdef _MSC_VER
#pragma warning(disable : 4127)
#endif

namespace dvblink {

basic_socket::basic_socket(basic_socket::socket_type type, ip_version ipv) :
    sock_(bad_socket), type_(type), ipver_(ipv)
{
    basic_socket::open();
}

basic_socket::basic_socket(handle_t sock, socket_type type, ip_version ipv) :
    sock_(sock), type_(type), ipver_(ipv)
{
}

basic_socket::~basic_socket()
{
    basic_socket::close();
}

errcode_t basic_socket::open()
{
    if (sock_ != bad_socket)
    {
        return err_already_opened;
    }

    int type = SOCK_STREAM;

    if (type_ == sock_udp)
    {
        type = SOCK_DGRAM;
    }
    else if (type_ == sock_raw)
    {
        type = SOCK_RAW;
    }

    int ipver = (ipver_ == ip_v6) ? AF_INET6 : AF_INET;

    handle_t sock = ::socket(ipver, type, 0);

    if (sock == bad_socket)
    {
        return err_open_error;        
    }

    if (type_ == sock_raw)
    {
        // TODO: set IP_HDRINCL (IPV6_HDRINCL) socket option
    }

    sock_ = sock;

    return err_none;
}

errcode_t basic_socket::close()
{
    if (sock_ == bad_socket)
    {
        return err_closed;
    }

#ifdef _WIN32
    set_blocking_mode(true);
    int rc = ::closesocket(sock_);
#else
    int rc = ::close(sock_);
#endif

    if (rc == 0)
    {
        sock_ = bad_socket;
        return err_none;
    }    

    return err_error;
}

errcode_t basic_socket::get_recv_bufsize(int& size)
{
    return get_option(SO_RCVBUF, SOL_SOCKET, size);
}

errcode_t basic_socket::get_send_bufsize(int& size)
{
    return get_option(SO_SNDBUF, SOL_SOCKET, size);
}

errcode_t basic_socket::set_recv_bufsize(int size)
{
    return set_option(SO_RCVBUF, SOL_SOCKET, size);
}

errcode_t basic_socket::set_send_bufsize(int size)
{
    return set_option(SO_SNDBUF, SOL_SOCKET, size);
}

errcode_t basic_socket::set_reuse_addr(bool onoff)
{
    int opt = (onoff? 1 : 0);
    return set_option(SO_REUSEADDR, SOL_SOCKET, opt);
}

errcode_t basic_socket::get_sock_error(int& err)
{
    return get_option(SO_ERROR, SOL_SOCKET, err);
}

errcode_t basic_socket::bind(const sock_addr& sa)
{
    if (sock_ == bad_socket)
    {
        return err_closed;
    }

    int rc = ::bind(sock_, 
        (const struct sockaddr*)sa,
        sizeof(struct sockaddr));

#ifdef _WIN32
    if (rc == SOCKET_ERROR)
    {
        int err = ::WSAGetLastError();

        if (err == WSAEINVAL)
        {
            // the socket is already bound to an address
            return err_already_bound;
        }

        return err_error;
    }
#else
    if (rc == -1)
    {
        switch (errno)
        {
        case ENOENT:
            return err_invalid_handle;

        case EINVAL:
            return err_already_bound;

        case ELOOP:        
            return err_invalid_param;

        default:
            return err_error;
        }
    }
#endif

    return err_none;
}

errcode_t basic_socket::get_sock_addr(sock_addr& sa)
{
    if (sock_ == bad_socket)
    {
        return err_closed;
    }

    struct sockaddr s;
    socklen_t len = sizeof(struct sockaddr);

    int rc = ::getsockname(sock_, &s, &len);

    if (rc == 0)
    {
        sa = s;
        return err_none;
    }

    return err_error;
}

errcode_t basic_socket::wait_for_readable(dvblink::timeout_t to)
{
    if (sock_ == bad_socket)
    {
        return err_closed;
    }

#ifndef _WIN32
    if (sock_ >= FD_SETSIZE)
    {
        // too many sockets opened
        return err_out_of_range;
    }
#endif

    fd_set set;
    FD_ZERO(&set);
    FD_SET(sock_, &set);

    int rc = -1;

    if (to.is_special())    // infinite timeout
    {
        rc = ::select((int)sock_ + 1, &set, NULL, NULL, NULL);
    }
    else
    {
        // prepare timeout value
        struct timeval tv;
        tv.tv_sec = long(to.total_milliseconds() / MILLI);
        tv.tv_usec = (to.total_milliseconds() % MILLI) * MILLI;

        rc = ::select((int)sock_ + 1, &set, NULL, NULL, &tv);
    }

    if (rc > 0)
    {
        return err_none;
    }

    if (rc == 0)
    {
        return err_timeout;
    }

    return err_error;
}

errcode_t basic_socket::wait_for_writable(dvblink::timeout_t to)
{
    if (sock_ == bad_socket)
    {
        return err_closed;
    }

#ifndef _WIN32
    if (sock_ >= FD_SETSIZE)
    {
        // too many sockets opened
        return err_out_of_range;
    }
#endif

    fd_set set;
    FD_ZERO(&set);
    FD_SET(sock_, &set);

    int rc = -1;

    if (to.is_special())    // infinite timeout
    {
        rc = ::select((int)sock_ + 1, NULL, &set, NULL, NULL);
    }
    else
    {
        // prepare timeout value
        struct timeval tv;
        tv.tv_sec = long(to.total_milliseconds() / MILLI);
        tv.tv_usec = (to.total_milliseconds() % MILLI) * MILLI;

        rc = ::select((int)sock_ + 1, NULL, &set, NULL, &tv);
    }

    if (rc > 0)
    {
        return err_none;
    }

    if (rc == 0)
    {
        return err_timeout;
    }

    return err_error;
}

bool basic_socket::is_readable()
{
    if (sock_ == bad_socket)
    {
        return false;
    }

#ifndef _WIN32
    if (sock_ >= FD_SETSIZE)
    {
        // too many sockets opened
        return false;
    }
#endif

    fd_set set;
    FD_ZERO(&set);
    FD_SET(sock_, &set);
    struct timeval tv = { 0, 0 };

    int rc = ::select((int)sock_ + 1, &set, NULL, NULL, &tv);

    return (rc > 0) ? true : false;
}

bool basic_socket::is_writable()
{
    if (sock_ == bad_socket)
    {
        return false;
    }

#ifndef _WIN32
    if (sock_ >= FD_SETSIZE)
    {
        // too many sockets opened
        return false;
    }
#endif

    fd_set set;
    FD_ZERO(&set);
    FD_SET(sock_, &set);
    struct timeval tv = { 0, 0 };

    int rc = ::select((int)sock_ + 1, NULL, &set, NULL, &tv);

    return (rc > 0) ? true : false;
}

errcode_t basic_socket::set_blocking_mode(bool onoff)
{
    if (sock_ == bad_socket)
    {
        return err_closed;
    }

#ifdef _WIN32
    u_long arg = (onoff ? 0 : 1);
    int rc = ::ioctlsocket(sock_, FIONBIO, &arg);

    if (rc != 0)
    {
        switch (::WSAGetLastError())
        {
        case WSANOTINITIALISED:
            return err_not_initialized;

        case WSAENETDOWN:
            return err_net_down;

        case WSAEINPROGRESS:
            return err_in_progress;

        case WSAENOTSOCK:
            return err_invalid_handle;

        default:
            return err_error;
        }
    }
#else
    int flags = ::fcntl(sock_, F_GETFL);   // get flags

    if (flags == -1)
    {
        return err_error;   // fcntl() failed
    }

    if (onoff)
    {
        flags &= ~O_NONBLOCK;        
    }
    else
    {
        flags |= O_NONBLOCK;        
    }

    int rc = ::fcntl(sock_, F_SETFL, flags);   // set flags

    if (rc)
    {
        return err_error;
    }
#endif

    return err_none;
}

} // namespace dvblink

// $Id: socket.cpp 2853 2011-06-23 07:34:23Z mike $
