/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/regex.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include "http_utils.h"

#include <dl_strings.h>
#include <dl_common.h>
#include <dl_network_helper.h>
#include <dl_http_comm.curl.h>
#include <dl_file_procedures.h>

namespace dvblink {
namespace streaming {

using namespace engine;

// TODO:
// Lines are terminated by CRLF, but receivers should be prepared to also
// interpret CR and LF by themselves as line terminators.
//
bool parse_http_message(const std::string& msg,
    std::string& start_line, http_headers& headers,
    size_t& body_offs)
{
    start_line.clear();
    headers.clear();
    body_offs = 0;

    if (msg.empty())
    {
        return false;
    }

    size_t offs = msg.find("\r\n\r\n");

    if (offs == std::string::npos)
    {
        return false;
    }

    body_offs = offs + 4;

    // read start line
    //
    offs = msg.find("\r\n");

    if (offs == std::string::npos)
    {
        return false;
    }

    start_line.assign(msg, 0, offs);
    offs += 2;

    // read headers
    //
    bool success = true;

    while (offs < (body_offs - 2))
    {
        header_field field;
        size_t i = msg.find(":", offs);

        if (i == std::string::npos)
        {
            success = false;
            break;
        }

        field.name.assign(msg, offs, i - offs);
        offs = i + 1;

        while (msg[offs] == ' ')
        {
            offs++;
        }

        size_t j = msg.find("\r\n", offs);

        if (j == std::string::npos)
        {
            success = false;
            break;
        }

        field.value.assign(msg, offs, j - offs);
        offs = j + 2;

        headers.push_back(field);
    }

    return success;
}

bool parse_status_line(const std::string& status_line,
    std::string& protocol, std::string& version,
    unsigned int& status_code, std::string& text)
{
    protocol.clear();
    version.clear();
    text.clear();
    status_code = 0;

    std::string input = boost::trim_right_copy(status_line);

    if (input.empty())
    {
        return false;
    }

    try
    {
    	bool ok = false;
        int respCode = -1;
        int version_minor = 0;
        int version_major = 1;
        char respText[2048];
    	// RTSP/1.0 200 OK\r\n
    	if (sscanf(input.c_str(), "RTSP/%1d.%1d %3d %s", &version_major, &version_minor, &respCode, respText) == 4)
    	{
    		protocol = "RTSP";
    		ok = true;
    	}
    	// HTTP/1.0 200 OK\r\n
    	if (sscanf(input.c_str(), "HTTP/%1d.%1d %3d %s", &version_major, &version_minor, &respCode, respText) == 4)
    	{
    		protocol = "HTTP";
    		ok = true;
    	}

        if (!ok)
        {
            return false;   // not matched
        } else
        {
    		version = boost::lexical_cast<std::string>(version_major) + "." + boost::lexical_cast<std::string>(version_minor);
    		status_code = respCode;
    		text = respText;
            boost::replace_all(text, "\r", "");
            boost::replace_all(text, "\n", "");
        }
    }
    catch (...)
    {
        return false;
    }

    return true;
}

bool find_http_header(const http_headers& headers,
    const std::string& header_name, std::string& header_value)
{
    header_value.clear();

    if (headers.empty() || header_name.empty())
    {
        return false;
    }

    bool found  = false;

    BOOST_FOREACH(const header_field& x, headers)
    {
        if (boost::iequals(x.name, header_name))
        {
            header_value = x.value;
            found = true;
            break;
        }
    }

    return found;
}

errcode_t get_description_xml(const std::string& url, std::string& xml)
{
    using namespace dvblink::engine;

    xml.clear();
    errcode_t ret = err_unexpected;

    do 
    {
        if (url.empty())
        {
            ret = err_invalid_param;
            break;
        }

        std::string addr;
        std::string user;
        std::string pass;
        std::string suff;
        unsigned short port = 0;

        EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(
            url.c_str(), addr, user, pass, port, suff);

        if (addr.empty())
        {
            ret = err_invalid_address;
            break;
        }

        http_comm_handler http_reader(PRODUCT_NAME_UTF8, addr.c_str(), user.c_str(), pass.c_str(), port);

        bool ok = http_reader.Init();

        if (!ok)
        {
            ret = err_not_initialized;
            break;
        }

        std::string response;

        ok = http_reader.ExecuteGetWithResponse(suff.c_str(),
            proto == DL_NET_PROTO_HTTPS, response);

        http_reader.Term();

        if (!ok)
        {
            ret = err_error;
            break;
        }

        xml.swap(response);
        ret = err_none;
    }
    while (0);

    return ret;
}

bool download_http_image(const dvblink::url_address_t& url, const dvblink::filesystem_path_t& file_base_path, dvblink::filesystem_path_t& outfile_path)
{
    bool ret_val = false;
    if (!url.empty())
    {
        unsigned short port;
        std::string address, user, pswd, url_suffix;
        EDL_NET_PROTOCOLS proto = network_helper::parse_net_url(url.c_str(), address, user, pswd, port, url_suffix);
        if (DL_NET_PROTO_UNKNOWN != proto)
        {
            http_comm_handler http_comm(PRODUCT_NAME_UTF8, address.c_str(), user.c_str(), pswd.c_str(), port);
            if (http_comm.Init())
            {
                http_comm_handler::http_headers_t out_headers;
                std::string response;
                if (http_comm.ExecuteGetWithResponse(url_suffix.c_str(), proto == DL_NET_PROTO_HTTPS, response, NULL, &out_headers))
                {
                    //check image format: walk through http headers
                    std::string image_ext = ".jpg"; //jpg by default
                    for (size_t i = 0; i < out_headers.size(); i++)
                    {
                        if (boost::icontains(out_headers[i], "content-type"))
                        {
                            if (boost::icontains(out_headers[i], "jpeg"))
                            {
                                image_ext = ".jpg";
                            }
                            else
                            if (boost::icontains(out_headers[i], "gif"))
                            {
                                image_ext = ".gif";
                            }
                            else
                            if (boost::icontains(out_headers[i], "png"))
                            {
                                image_ext = ".png";
                            }
                            break;
                        }
                    }
                    outfile_path = file_base_path.to_string() + image_ext;
                    if (FILE* f = filesystem::universal_open_file(outfile_path, "wb"))
                    {
                        fwrite(&(response[0]), response.size(), 1, f);
                        fclose(f);
                        ret_val = true;
                    }
                }
                http_comm.Term();
            }
        }
    }
    return ret_val;
}

} // namespace streaming
} // namespace dvblink

// $Id: http_utils.cpp 11070 2015-02-03 17:44:49Z pashab $
