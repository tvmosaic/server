/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <string>
#include <vector>
#include <sstream>
#include <cassert>
#include <dl_strings.h>
#include <dl_discovery.h>
#include <dl_network_helper.h>
#include <boost/date_time.hpp>

#ifndef _WIN32
#include <unistd.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#endif

#ifdef __MACH__
#include <ifaddrs.h>
#endif

namespace pt = boost::posix_time;

namespace dvblink {

namespace {

#ifdef _WIN32
bool enum_network_inerfaces(std::vector<std::string>& ifs)
{
    using namespace dvblink::engine;

    ifs.clear();
    TNetworkAdaptersInfo adapters;
    network_helper::get_local_net_adapters(adapters);
    sock_addr sa;

    for (size_t j = 0; j < adapters.size(); j++)
    {
        try
        {
            SNetworkAdaptersInfo& t = adapters[j];
            std::string addr = string_cast<EC_UTF8>(t.m_strAddress);
            errcode_t err = sa.set_address(addr);

            if ((err == err_none) && (addr != "127.0.0.1"))
            {
                ifs.push_back(addr);
            }
        }
        catch (...)
        {
        }
    }

    return (ifs.size() > 0);
}
#elif defined(__MACH__) // Apple
bool find_compatible_interfaces(sock_addr addr, std::vector<sock_addr>& ifs)
{
    ifs.clear();    
    ifaddrs* ifap = 0;
    unsigned int in_addr = ((sockaddr_in*)((const sockaddr*)addr))->sin_addr.s_addr;

    if (getifaddrs(&ifap) == 0)
    {
        for(ifaddrs* p = ifap; p != 0; p = p->ifa_next)
        {
            if ((p->ifa_addr)->sa_family != AF_INET)
            {
                continue;
            }

            if (((p->ifa_flags & IFF_UP) == 0) ||
                ((p->ifa_flags & IFF_LOOPBACK) != 0))
            {
                continue;
            }

            sockaddr_in* sa = (sockaddr_in*)(p->ifa_addr);
            sockaddr_in* sn = (sockaddr_in*)(p->ifa_netmask);

            if (!sa || !sn)
            {
                continue;
            }

            unsigned int if_addr = sa->sin_addr.s_addr;
            unsigned int net_mask = sn->sin_addr.s_addr;

            if ((in_addr & net_mask) == (if_addr & net_mask))
            {
                sock_addr a(*(p->ifa_addr));
                ifs.push_back(a);
            }
        }

        freeifaddrs(ifap);
    }

    return !ifs.empty();
}

#else
bool find_compatible_interfaces(sock_addr addr, std::vector<sock_addr>& ifs)
{
    ifs.clear();
    int sd = socket(PF_INET, SOCK_DGRAM, 0);

    if (sd <= 0)
    {
        return false;
    }

    ifreq ifr[16];
    ifconf ifc;
    ifc.ifc_len = sizeof(ifr);
    ifc.ifc_ifcu.ifcu_buf = (caddr_t)ifr;

    if (ioctl(sd, SIOCGIFCONF, &ifc) == 0)
    {
        int ifc_num = ifc.ifc_len / sizeof(ifreq);

        for (int i = 0; i < ifc_num; i++)
        {
            const ifreq& r = ifr[i];

            if (r.ifr_addr.sa_family != AF_INET)
            {
                continue;
            }

            if (ioctl(sd, SIOCGIFFLAGS, &r)) 
            {
                continue;
            }

            if (((r.ifr_flags & IFF_UP) == 0) ||
                ((r.ifr_flags & IFF_LOOPBACK) != 0) ||
                ((r.ifr_flags & IFF_BROADCAST) == 0))
            {
                continue;
            }

            if (ioctl(sd, SIOCGIFADDR, &r))
            {
                continue;
            }

            unsigned int if_addr = ((struct sockaddr_in*)(&r.ifr_addr))->sin_addr.s_addr;
            sock_addr a(r.ifr_addr);

            if (ioctl(sd, SIOCGIFNETMASK, &r))
            {
                continue;
            }

            unsigned int in_addr = ((struct sockaddr_in*)((const sockaddr*)addr))->sin_addr.s_addr;
            unsigned int net_mask = ((struct sockaddr_in*)(&r.ifr_netmask))->sin_addr.s_addr;

            if ((in_addr & net_mask) == (if_addr & net_mask))
            {
                ifs.push_back(a);
            }
        }                      
    }

    close(sd);
    return (ifs.size() > 0);
}
#endif

} // unnamed namespace
    
//-----------------------------------------------------------------------------
// Server
//
discovery_server::discovery_server(const server_settings& settings) :
    settings_(settings), shutdown_(false)
{
    if (settings_.discovery_port == 0)
    {
        throw std::runtime_error("discovery_server - invalid parameter");
    }

    std::vector<std::string> addrs;

#if defined(_WIN32)
    bool ok = enum_network_inerfaces(addrs);

    if (!ok)
    {
        throw std::runtime_error("discovery_server - no network interfaces found");
    }
#elif defined(__MACH__)
    addrs.push_back("0.0.0.0");
#else
    addrs.push_back("255.255.255.255"); // broadcast address
#endif

    for (size_t j = 0; j < addrs.size(); j++)
    {
        sock_addr sa;
        sa.set_port(settings_.discovery_port);
        errcode_t err = sa.set_address(addrs[j]);

        if (err == err_none)
        {
            udp_socket::sock_ptr ptr = udp_socket::sock_ptr(new udp_socket());
            ptr->set_broadcast();

            err = ptr->bind(sa);

            if (err == err_none)
            {
                socket_info si;
                si.addr = addrs[j];
                si.sock_ptr = ptr;                
                sockets_.push_back(si);
            }
        }
    }

    if (sockets_.empty())
    {
        throw std::runtime_error("discovery_server - bind() failed");
    }

    thread_ = thread_ptr(new boost::thread(
        boost::bind(&discovery_server::thread_func, this)));
}

discovery_server::~discovery_server()
{
    shutdown_ = true;
    
    if (thread_)
    {
        thread_->join();
        thread_.reset();
    }
}

struct client_info
{
    sock_addr from;
    pt::ptime t;
};

void discovery_server::thread_func()
{
    const timeout_t to = pt::milliseconds(100);
    const pt::time_duration treq = pt::milliseconds(200);

    std::string buf(1500, '\0');    
    std::vector<client_info> cinfo;

    while (!shutdown_)
    {
        bool readable = wait_for_readable(to);

        if (readable && !shutdown_)
        {
            pt::ptime now = pt::microsec_clock::universal_time();
            std::vector<client_info> cinfo_tmp;

            for (size_t i = 0; i < cinfo.size(); i++)
            {
                const client_info& ci = cinfo[i];
                pt::time_duration td = (now - ci.t);

                if (td < treq)
                {
                    cinfo_tmp.push_back(ci);
                }
            }

            cinfo.swap(cinfo_tmp);

            for (size_t k = 0; k < sockets_.size(); k++)
            {
                udp_socket::sock_ptr ptr = sockets_[k].sock_ptr;

                if (ptr->is_readable())
                {
                    size_t size = 0;
                    sock_addr from;            

                    errcode_t err = ptr->receive_datagram(&buf[0],
                        buf.size(), size, from);

                    //std::wcout << L"datagram received: " << size << L"\n";

                    if ((err == err_none) && (size == 1) &&
                        ((unsigned char)buf[0] == discovery_request_code))
                    {
                        bool skip = false;

                        for (size_t j = 0; j < cinfo.size(); j++)
                        {
                            client_info& ci = cinfo[j];
                            unsigned int addr1 = ((sockaddr_in*)((const sockaddr*)ci.from))->sin_addr.s_addr;
                            unsigned int addr2 = ((sockaddr_in*)((const sockaddr*)from))->sin_addr.s_addr;

                            if (addr1 == addr2)
                            {
                                ci.t = now;
                                skip = true;
                                break;
                            }
                        }

                        if (skip)
                        {
                            continue;
                        }

                        std::string xml;
                        std::vector<std::string> server_ips;

                    #ifdef _WIN32
                        server_ips.push_back(sockets_[k].addr);
                    #else
                        std::vector<sock_addr> ifs;
                        find_compatible_interfaces(from, ifs);

                        for (size_t j = 0; j < ifs.size(); j++)
                        {
                            std::string a;
                            ifs[j].get_address(a);
                            server_ips.push_back(a);
                        }
                    #endif

                        if (server_ips.size() > 0)
                        {
                            client_info ci;
                            ci.from = from;
                            ci.t = now;
                            cinfo.push_back(ci);

                            create_xml(server_ips, xml);
                            ptr->send_datagram(&xml[0], xml.size(), from);
                        }
                    }
                }
            }
        }
    }
}

void discovery_server::create_xml(const std::vector<std::string>& server_ips,
    std::string& xml)
{
    xml.clear();
    std::stringstream ss;

    ss << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
    ss << "<server_info>\n";

    if (!settings_.server_name.empty())
    {
        ss << "  <server_name>" << settings_.server_name << "</server_name>\n";
    }

    if (!settings_.server_id.empty())
    {
        ss << "  <server_id>" << settings_.server_id << "</server_id>\n";
    }

    for (size_t j = 0; j < server_ips.size(); j++)
    {
        ss << "  <server_address>" << server_ips[j] << "</server_address>\n";
    }

    if (settings_.http_port != 0)
        ss << "  <base_streaming_port>" << settings_.http_port<< "</base_streaming_port>\n";

    if (settings_.https_port != 0)
        ss << "  <https_port>" << settings_.https_port << "</https_port>\n";

    ss << "</server_info>\n";

    xml = ss.str();
}

bool discovery_server::wait_for_readable(dvblink::timeout_t to)
{
    if (sockets_.empty())
    {
        return false;
    }

    fd_set set;
    FD_ZERO(&set);
    int fd_max = 0;

    for (size_t j = 0; j < sockets_.size(); j++)
    {
        udp_socket::sock_ptr ptr = sockets_[j].sock_ptr;

        if (ptr)
        {
        #ifndef _WIN32
            basic_socket::handle_t fd = ptr->get_handle();

            if (fd >= FD_SETSIZE)
            {
                continue;
            }

            if (fd > fd_max)
            {
                fd_max = fd;
            }

            FD_SET(fd, &set);
        #else
            if (j < FD_SETSIZE)
            {                
                FD_SET(ptr->get_handle(), &set);
            }
        #endif
        }
    }

    int rc = -1;

    if (to.is_special()) // infinite timeout
    {
        rc = ::select(fd_max + 1, &set, NULL, NULL, NULL);
    }
    else
    {
        // prepare timeout value
        struct timeval tv;
        tv.tv_sec = long(to.total_milliseconds() / MILLI);
        tv.tv_usec = (to.total_milliseconds() % MILLI) * MILLI;

        rc = ::select(fd_max + 1, &set, NULL, NULL, &tv);
    }

    return (rc > 0);
}

//-----------------------------------------------------------------------------
// Client
//
bool discover_dvblink_servers(std::vector<std::string>& server_info,
    timeout_t timeout, unsigned short discovery_port)
{
    assert(discovery_port > 0);
    assert(!timeout.is_special());

    server_info.clear();

    if ((discovery_port == 0) || timeout.is_special())
    {
        return false;
    }

    sock_addr local_addr;
    udp_socket sock;
    sock.set_broadcast();
    errcode_t err = sock.bind(local_addr);

    if (err != err_none)
    {
        return false;
    }

    sock_addr broadcast_addr;
    broadcast_addr.set_port(discovery_port);
    err = broadcast_addr.set_address("255.255.255.255");    

    if (err != err_none)
    {
        return false;
    }

    unsigned char req = discovery_request_code;
    err = sock.send_datagram(&req, 1, broadcast_addr);

    if (err != err_none)
    {
        return false;
    }

    pt::ptime t_start = pt::microsec_clock::universal_time();
    const timeout_t to = pt::milliseconds(100);
    std::string buf(1500, '\0');

    // waiting for response...
    //
    for ( ; ; )
    {
        pt::ptime t_now = pt::microsec_clock::universal_time();

        if ((t_now - t_start) >= timeout)
        {
            break; // timeout expired
        }

        err = sock.wait_for_readable(to);

        if (err != err_none)
        {
            continue;
        }

        // response received

        size_t size = 0;
        sock_addr from;
        err = sock.receive_datagram(&buf[0], buf.size(), size, from);

        if ((err == err_none) && (size > 0))
        {
            std::string xml(&buf[0], size);
            server_info.push_back(xml);
        }
    }

    return (server_info.size() > 0);
}

} // namespace dvblink
