/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_socket.h>

#ifdef _WIN32
#include <ws2tcpip.h>
#else
#include <netinet/in.h>
#endif

namespace dvblink {

udp_socket::udp_socket(ip_version ipv) :
    basic_socket(basic_socket::sock_udp, ipv)
{
}

udp_socket::udp_socket(handle_t sock, ip_version ipv) :
    basic_socket(sock, basic_socket::sock_udp, ipv)
{
}

errcode_t udp_socket::send_datagram(const void* dg, size_t dg_size,
    const sock_addr& dst_addr)
{
    if (sock_ == bad_socket)
    {
        return err_closed;
    }

    if (!dg || !dg_size)
    {
        // send a zero-length transport datagram
        int rc = ::sendto(sock_, "", 0, 0,
            dst_addr, sizeof(struct sockaddr));

        if (rc == 0)
        {
            return err_none;
        }

        return err_send_error;
    }

    int rc = ::sendto(sock_, (const char*)dg, int(dg_size), 0,
        dst_addr, sizeof(struct sockaddr));

    if ((rc >= 0) && (size_t(rc) == dg_size))
    {
        return err_none;
    }
    
    return err_send_error;
}

errcode_t udp_socket::receive_datagram(void* buf, size_t buf_size,
    size_t& bytes_received, sock_addr& from, bool peek)
{
    if (sock_ == bad_socket)
    {
        return err_closed;
    }

    bytes_received = 0;

    if (!buf || !buf_size)
    {
        return err_invalid_param;
    }

    struct sockaddr sa;
    socklen_t sa_len = sizeof(sa);
    int flags = (peek == true) ? MSG_PEEK : 0;

    int rc = ::recvfrom(sock_, (char*)buf, 
        int(buf_size), flags, &sa, &sa_len);

    if (rc >= 0)
    {
        bytes_received = rc;
        from = sock_addr(sa);
        return err_none;
    }

#ifdef _WIN32
    if (::WSAGetLastError() == WSAEMSGSIZE)
    {
        bytes_received = buf_size;
        from = sock_addr(sa);
        return err_no_space;    // message truncated
    }
#endif

    return err_receive_error;
}

errcode_t udp_socket::add_membership(const sock_addr& group_addr,
    const sock_addr& local_addr)
{
    struct ip_mreq opt;
    memset(&opt, 0, sizeof(opt));

    opt.imr_interface = ((const sockaddr_in*)
        ((const sockaddr*)local_addr))->sin_addr;
    opt.imr_multiaddr = ((const sockaddr_in*)
        ((const sockaddr*)group_addr))->sin_addr;

    return set_option(IP_ADD_MEMBERSHIP, IPPROTO_IP, opt);
}

errcode_t udp_socket::drop_membership(const sock_addr& group_addr,
    const sock_addr& local_addr)
{
    struct ip_mreq opt;
    memset(&opt, 0, sizeof(opt));

    opt.imr_interface = ((const sockaddr_in*)
        ((const sockaddr*)local_addr))->sin_addr;
    opt.imr_multiaddr = ((const sockaddr_in*)
        ((const sockaddr*)group_addr))->sin_addr;

    return set_option(IP_DROP_MEMBERSHIP, IPPROTO_IP, opt);
}

errcode_t udp_socket::add_ssm_membership(const sock_addr& group_addr, const sock_addr& source_addr,
    const sock_addr& local_addr)
{
    struct ip_mreq_source opt;
    memset(&opt, 0, sizeof(opt));

#ifndef __ANDROID__
    opt.imr_interface = ((const sockaddr_in*)
        ((const sockaddr*)local_addr))->sin_addr;
    opt.imr_sourceaddr = ((const sockaddr_in*)
        ((const sockaddr*)source_addr))->sin_addr;
    opt.imr_multiaddr = ((const sockaddr_in*)
        ((const sockaddr*)group_addr))->sin_addr;
#else
    opt.imr_interface = ((const sockaddr_in*)
        ((const sockaddr*)local_addr))->sin_addr.s_addr;
    opt.imr_sourceaddr = ((const sockaddr_in*)
        ((const sockaddr*)source_addr))->sin_addr.s_addr;
    opt.imr_multiaddr = ((const sockaddr_in*)
        ((const sockaddr*)group_addr))->sin_addr.s_addr;
#endif
    return set_option(IP_ADD_SOURCE_MEMBERSHIP, IPPROTO_IP, opt);
}

errcode_t udp_socket::drop_ssm_membership(const sock_addr& group_addr, const sock_addr& source_addr,
    const sock_addr& local_addr)
{
    struct ip_mreq_source opt;
    memset(&opt, 0, sizeof(opt));

#ifndef __ANDROID__
    opt.imr_interface = ((const sockaddr_in*)
        ((const sockaddr*)local_addr))->sin_addr;
    opt.imr_sourceaddr = ((const sockaddr_in*)
        ((const sockaddr*)source_addr))->sin_addr;
    opt.imr_multiaddr = ((const sockaddr_in*)
        ((const sockaddr*)group_addr))->sin_addr;
#else
    opt.imr_interface = ((const sockaddr_in*)
        ((const sockaddr*)local_addr))->sin_addr.s_addr;
    opt.imr_sourceaddr = ((const sockaddr_in*)
        ((const sockaddr*)source_addr))->sin_addr.s_addr;
    opt.imr_multiaddr = ((const sockaddr_in*)
        ((const sockaddr*)group_addr))->sin_addr.s_addr;
#endif    
    return set_option(IP_DROP_SOURCE_MEMBERSHIP, IPPROTO_IP, opt);
}

errcode_t udp_socket::set_ttl(int ttl)
{
    unsigned char opt = static_cast<unsigned char>(ttl);
    return set_option(IP_MULTICAST_TTL, IPPROTO_IP, opt);
}

errcode_t udp_socket::set_multicast_loopback(bool onoff)
{
    int opt = (onoff? 1 : 0);
    return set_option(IP_MULTICAST_LOOP, IPPROTO_IP, opt);
}

errcode_t udp_socket::set_multicast_if(const sock_addr& addr)
{
    return set_option(IP_MULTICAST_IF, IPPROTO_IP,
        ((const sockaddr_in*)((const sockaddr*)addr))->sin_addr);
}

errcode_t udp_socket::set_broadcast(bool onoff)
{
    int opt = (onoff? 1 : 0);
    return set_option(SO_BROADCAST, SOL_SOCKET, opt);
}

} // namespace dvblink

// $Id: udp_socket.cpp 11237 2015-03-25 09:55:28Z pashab $
