/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <dl_socket.h>
#include <dl_debug.h>

#ifdef _WIN32
#include <ws2ipdef.h>
#else
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#endif

using namespace dvblink;
namespace pt = boost::posix_time;

tcp_socket::tcp_socket(ip_version ipv) :
    basic_socket(basic_socket::sock_tcp, ipv)
{
}

tcp_socket::tcp_socket(handle_t sock, ip_version ipv) :
    basic_socket(sock, basic_socket::sock_tcp, ipv)
{
}

tcp_socket::~tcp_socket()
{
    tcp_socket::disconnect(pt::milliseconds(100));
}

dvblink::errcode_t tcp_socket::set_no_delay(bool onoff)
{
    int opt = (onoff? 1 : 0);
    return set_option(TCP_NODELAY, IPPROTO_TCP, opt);
}

dvblink::errcode_t tcp_socket::set_keep_alive(bool onoff)
{
    int opt = (onoff? 1 : 0);
    return set_option(SO_KEEPALIVE, IPPROTO_TCP, opt);
}

dvblink::errcode_t tcp_socket::connect(const sock_addr& sa, timeout_t to)
{
    if (sock_ == bad_socket)
    {
        return err_closed;
    }

    // switch socket to non-blocking mode
    dvblink::errcode_t err = set_blocking_mode(false);

    if (err != err_none)
    {
        return err;
    }

    int rc = ::connect(sock_, (const struct sockaddr*)sa,
        sizeof(struct sockaddr));

    if (rc == 0)
    {
        return err_none;    // connected
    }

#ifdef _WIN32
    int winerr = ::WSAGetLastError();

    switch (winerr)
    {
    case WSAEISCONN:
        // the socket is already connected
        return err_already_connected;

    case WSAEINTR:
        // the socket was closed
        return err_closed;

    case WSAECONNREFUSED:
        // the attempt to connect was forcefully rejected
        return err_conn_refused;

    case WSAENETUNREACH:
        // the network cannot be reached from this host at this time
        return err_net_unreached;

    case WSAEINPROGRESS:
    case WSAEWOULDBLOCK:
    case WSAEALREADY:
#else
    switch (errno)
    {
    case EAGAIN:
        // no more free local ports
        return err_no_resources;

    case EISCONN:
        // the socket is already connected
        return err_already_connected;

    case ECONNREFUSED:
        // no one listening on the remote address
        return err_not_listen;

    case EINPROGRESS:
    case EALREADY:
#endif
        // The socket is non-blocking and the connection cannot be completed
        // immediately. After select() indicates writability, use getsockopt()
        // to read the SO_ERROR to determine whether connect() completed
        // successfully (SO_ERROR is zero)
        {
            // wait for a connection to be established
            err = wait_for_writable(to);

            if (err != err_none)
            {
                return err; // error
            }

            // get error code
            int sockerr = 0;
            err = get_sock_error(sockerr);

            if (err != err_none)
            {
                return err;
            }

            if (sockerr == 0)
            {
                return err_none;    // connected
            }
        }

    default:
        break;
    }

    return err_not_connected;
}

dvblink::errcode_t tcp_socket::listen()
{
    if (sock_ == bad_socket)
    {
        return err_closed;
    }

    int rc = ::listen(sock_, SOMAXCONN);
    return ((rc == 0) ? err_none : err_error);
}

dvblink::errcode_t tcp_socket::accept(tcp_socket::sock_ptr& new_sock,
    sock_addr& peer_addr)
{
    if (sock_ == bad_socket)
    {
        return err_closed;
    }

    struct sockaddr sa;
    socklen_t sa_len = sizeof(sa);

    handle_t _sock = ::accept(sock_, &sa, &sa_len);

    if (_sock != bad_socket)
    {
        new_sock = sock_ptr(new tcp_socket(_sock, ipver_));
        peer_addr = sock_addr(sa);
        return err_none;
    }

    new_sock = sock_ptr();
    peer_addr = sock_addr(ipver_);

#ifdef _WIN32
    int winerr = ::WSAGetLastError();

    switch (winerr)
    {
    case WSAEINVAL:
        // the listen function was not invoked prior to accept
        return err_not_listen;

    case WSAEWOULDBLOCK:
        // the socket is marked as non-blocking and no connections
        // are present to be accepted
        return err_retry;
    }
#else
    switch (errno)
    {
    case EPERM:
        // firewall rules forbid connection
        return err_access_denied;

    case EAGAIN:
        // The socket is marked as nonblocking and no connections
        // are present to be accepted
        return err_retry;
    }
#endif

    return err_error;
}

dvblink::errcode_t tcp_socket::send(const void* data, size_t data_size,
    size_t& bytes_sent, timeout_t to)
{
    bytes_sent = 0;

    if (sock_ == bad_socket)
    {
        return err_closed;
    }

    if (data_size == 0)
    {
        return wait_for_writable(to);
    }

    if ((to == zero_timeout) || to.is_special())
    {
        if ((to == zero_timeout) && (is_writable() == false))
        {
            return err_timeout;
        }

        int rc = ::send(sock_, (const char*)data, int(data_size), 0);

        if (rc >= 0)
        {
            bytes_sent = rc;
            return err_none;
        }

    #ifdef _WIN32
        int winerr = ::WSAGetLastError();

        if (winerr == WSAEWOULDBLOCK)
        {
            return err_retry;   // try again later
        }

        if (winerr == WSAENOTCONN)
        {
            return err_not_connected;
        }
    #else
        if (errno == EAGAIN)
        {
            return err_retry;   // try again later
        }

        if (errno == ENOTCONN)
        {
            return err_not_connected;
        }
    #endif

        return err_send_error;
    }
    else
    {
        pt::ptime t_start = pt::microsec_clock::local_time();
        pt::ptime t_now = t_start;
        pt::time_duration t_left = to;

        while (bytes_sent < data_size)
        {
            dvblink::errcode_t err = wait_for_writable(t_left);

            if (err != err_none)
            {
                return err;
            }

            int rc = ::send(sock_,
                (const char*)data + bytes_sent,
                (int)(data_size - bytes_sent), 0);

        #ifdef _WIN32
            if (rc == SOCKET_ERROR)
            {
                switch (::WSAGetLastError())
                {
                case WSAEWOULDBLOCK:
                    return err_retry;
                case WSAEINTR:
                    return err_canceled;
                case WSAENOBUFS:
                    return err_no_space;
                case WSAENOTCONN:
                    return err_not_connected;
                case WSAENOTSOCK:
                    return err_invalid_handle;
                case WSAESHUTDOWN:
                    return err_shutdown;
                case WSAEHOSTUNREACH:
                    return err_host_unreached;
                case WSAECONNABORTED:
                    return err_aborted;
                default:
                    return err_send_error;
                }
            }
        #else
            if (rc == -1)
            {
                if (errno == EAGAIN)
                {
                    return err_retry;
                }
                else
                {
                    return err_send_error;
                }
            }
        #endif

            bytes_sent += rc;

            if (bytes_sent >= data_size)
            {
                return err_none;    // done
            }

            t_now = pt::microsec_clock::local_time();
            pt::time_duration t_spent = (t_now - t_start);

            if (t_spent >= to)
            {
                // timeout expired
                return err_timeout;
            }

            t_left = (to - t_spent);
        }
    }

    return err_none;
}

dvblink::errcode_t tcp_socket::receive(void* buf, size_t buf_size,
    size_t& bytes_received, timeout_t to, bool peek)
{
    bytes_received = 0;

    if (sock_ == bad_socket)
    {
        return err_closed;
    }

    if (!buf)
    {
        return err_invalid_pointer;
    }

    if (to == zero_timeout)
    {
        if (is_readable() == false)
        {
            return err_retry;   // try again later
        }
    }
    else if (!to.is_special())
    {
        // wait for incoming data on a socket
        dvblink::errcode_t err = wait_for_readable(to);

        if (err != err_none)
        {
            return err;
        }
    }

    if (buf_size == 0)
    {
        return err_none;
    }

    int flags = (peek == true) ? MSG_PEEK : 0;

    // The receive calls normally return any data available, up to
    // the requested amount, rather than waiting for receipt of the
    // full amount requested.
    //
    int rc = ::recv(sock_, (char*)buf, int(buf_size), flags);

    if (rc == 0)
    {
        // connection has been gracefully closed
        basic_socket::close();
        return err_closed;
    }
    else if (rc > 0)
    {
        bytes_received = rc;
    }
    else
    {
    #ifdef _WIN32
        int last_err = ::WSAGetLastError();

        if (last_err == WSAEWOULDBLOCK)
        {
            return err_retry;   // try again later
        }
        else if (last_err == WSAECONNABORTED)
        {
            // an established connection was aborted
            return err_aborted;
        }        
        else
        {
            _TRACE("tcp_socket::receive(): WSAGetLastError() returned %d\n",
                last_err);
        }
    #else
        if (errno == EAGAIN)
        {
            return err_retry;   // try again later
        }
    #endif        

        return err_receive_error;
    }

    return err_none;
}

dvblink::errcode_t tcp_socket::receive_max(void* buf,
    size_t buf_size, size_t& bytes_received, dvblink::timeout_t to)
{
    bytes_received = 0;

    if (sock_ == bad_socket)
    {
        return err_closed;
    }

    if (!buf || !buf_size)
    {
        return err_invalid_param;
    }

    if (to == zero_timeout)
    {
        return tcp_socket::receive(buf, buf_size,
            bytes_received, zero_timeout, false);
    }
    else if (to.is_special())   // infinite timeout
    {
        while (bytes_received < buf_size)
        {
            dvblink::errcode_t err = wait_for_readable(infinite_timeout);

            if (err != err_none)
            {
                return err;
            }

            int rc = ::recv(sock_,
                (char*)buf + bytes_received,
                (int)(buf_size - bytes_received), 0);

            if (rc == 0)
            {
                // connection has been gracefully closed
                basic_socket::close();
                return err_closed;
            }
            else if (rc > 0)
            {
                bytes_received += rc;

                if (bytes_received >= buf_size)
                {
                    return err_none;    // done
                }
            }
            else
            {
                return err_receive_error;
            }
        }
    }
    else // finite timeout
    {
        pt::ptime t_start = pt::microsec_clock::local_time();
        pt::ptime t_now = t_start;
        pt::time_duration t_left = to;

        while (bytes_received < buf_size)
        {
            dvblink::errcode_t err = wait_for_readable(t_left);

            if (err != err_none)
            {
                return err;
            }

            int rc = ::recv(sock_, (char*)buf + bytes_received,
                (int)(buf_size - bytes_received), 0);

            if (rc == 0)
            {
                // connection has been gracefully closed
                basic_socket::close();
                return err_closed;
            }
            else if (rc > 0)
            {
                bytes_received += rc;

                if (bytes_received >= buf_size)
                {
                    return err_none;    // done
                }

                t_now = pt::microsec_clock::local_time();
                pt::time_duration t_spent = (t_now - t_start);

                if (t_spent >= to)
                {
                    // timeout expired
                    return err_timeout;
                }

                t_left = (to - t_spent);
            }
            else
            {
                return err_receive_error;
            }
        }
    }

    return err_none;
}

dvblink::errcode_t tcp_socket::peek_max(void* buf,
    size_t buf_size, size_t& bytes_received, dvblink::timeout_t to)
{
    bytes_received = 0;

    if (sock_ == bad_socket)
    {
        return err_closed;
    }

    if (!buf || !buf_size)
    {
        return err_invalid_param;
    }

    // TODO: check timeout
    while (bytes_received < buf_size)
    {
        dvblink::errcode_t err = tcp_socket::receive(buf, buf_size,
            bytes_received, to, true);

        if (err != err_none)
        {
            return err;
        }
    }

    return err_none;
}

dvblink::errcode_t tcp_socket::get_peer_address(sock_addr& peer_addr)
{
    if (sock_ == bad_socket)
    {
        return err_closed;
    }

    struct sockaddr sa;
    socklen_t len = sizeof(sa);
    
    int rc = ::getpeername(sock_, &sa, &len);

    if (rc == 0)
    {
        peer_addr = sock_addr(sa);
        return err_none;
    }

#ifdef _WIN32
    if (::WSAGetLastError() == WSAENOTCONN)
    {
        return err_not_connected;
    }
#else
    if (errno == ENOTCONN)
    {
        return err_not_connected;
    }
#endif

    return err_error;
}

bool tcp_socket::is_connected()
{
    sock_addr peer_addr;
    dvblink::errcode_t err = get_peer_address(peer_addr);
    return (err == err_none);
}

dvblink::errcode_t tcp_socket::disconnect(dvblink::timeout_t to)
{
    if (sock_ == bad_socket)
    {
        return err_closed;
    }

    if (is_connected() == false)
    {
        return err_not_connected;
    }

    ::shutdown(sock_, 1);  // SD_SEND

    if (to != zero_timeout)
    {
        pt::ptime t_start = pt::microsec_clock::local_time();
        pt::time_duration t_spent = zero_timeout;
        char buf[512];

        while (t_spent <= to)
        {
            size_t bytes = 0;

            dvblink::errcode_t err =
                tcp_socket::receive(buf, sizeof(buf), bytes, zero_timeout);

            if ((err != err_none) && (err != err_retry))
            {
                break;
            }

            pt::ptime t_now = pt::microsec_clock::local_time();
            t_spent = (t_now - t_start);
            
            if (t_spent > to)
            {
                break;
            }

            if (err == err_retry)
            {
                pt::time_duration t_left = (to - t_spent);
                err = wait_for_readable(t_left);

                if (err != err_none)
                {
                    break;
                }
            }
        }
    }
        
    return basic_socket::close();
}

// $Id: tcp_socket.cpp 3698 2011-10-25 11:30:17Z mike $
