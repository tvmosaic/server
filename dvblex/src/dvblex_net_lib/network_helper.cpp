/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include <boost/tokenizer.hpp>
#include <dl_socket.h>
#include <sstream>
#include <iomanip>
#include <string.h>
#include <stdlib.h>

#ifdef _WIN32
#include <ws2tcpip.h>
#include <iphlpapi.h>

#define strncasecmp _strnicmp

#else

#ifdef __APPLE__
#include <net/if.h>
#include <ifaddrs.h>
#else
#include <linux/if.h>
#endif

#include <netdb.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#if !defined(__ANDROID__)
#define _wcsnicmp wcsncasecmp
#endif
#endif

#include <dl_utils.h>
#include <dl_sockets.h>
#include <dl_strings.h>
#include <dl_network_helper.h>

using namespace dvblink;
using namespace dvblink::engine;

//static const wchar_t* port_delimiter = L":";
static const wchar_t column_delimiter = L':';
static const wchar_t netparam_delimiter = L'@';

const int WAKEUP_PACKET_LENGTH = 102;
const int WAKEUP_PORT = 40000;
const int MAC_ADDRESS_LENGTH = 6;
const int MAC_ADDRESS_REPEAT_IN_PACKET = 15;

#if defined(__ANDROID__)
int _wcsnicmp(const wchar_t *s1, const wchar_t *s2, size_t n)
{
    if (!s1 && !s2)
    {
        return 0;
    }
    
    if (!s1)
    {
        return 1;        
    }
    
    if (!s2)
    {
        return -1;
    }
        
    size_t len1 = wcslen(s1);
    size_t len2 = wcslen(s2);
    
    if (len1 > n)
    {
        len1 = n;
    }

    if (len2 > n)
    {
        len2 = n;
    }
    
    if (len1 < len2)
    {
        return -1;
    }

    if (len1 > len2)
    {
        return 1;
    }
    
    for (int i = 0; i < len1; i++)
    {
        wint_t a = towlower(s1[i]);
        wint_t b = towlower(s2[i]);
        
        if (a < b)
        {
            return -1;
        }
        
        if (a > b)
        {
            return 1;
        }
    }
    
    return 0;
}    
#endif

namespace dvblink { 

static const char* proto_text[DL_NET_PROTO_UNKNOWN] = {"udp://", "rtp://", "http://", "rtsp://", "https://", "rtmp://"};
static unsigned short proto_default_ports[DL_NET_PROTO_UNKNOWN] = {0, 0, 80, 554, 443, 80};

network_helper::network_helper()
{

}

network_helper::~network_helper()
{

}

bool network_helper::get_local_net_adapters(TNetworkAdaptersInfo& adapters)
{
	bool bSuccess = false;
	adapters.clear();

#ifdef _WIN32
    WSADATA wsd;
    WSAStartup(MAKEWORD(2, 2), &wsd);

	PIP_ADAPTER_ADDRESSES pAddress = NULL;
	PIP_ADAPTER_ADDRESSES pAddresses = (PIP_ADAPTER_ADDRESSES)malloc(sizeof(IP_ADAPTER_ADDRESSES));
	ULONG ulOutBufLen = sizeof(IP_ADAPTER_ADDRESSES);

	if (ERROR_BUFFER_OVERFLOW == GetAdaptersAddresses(AF_INET, 0, NULL, pAddresses, &ulOutBufLen))
	{
		free(pAddresses);
		pAddresses = (PIP_ADAPTER_ADDRESSES)malloc(ulOutBufLen);
	}

	if (NO_ERROR == GetAdaptersAddresses(AF_INET, 0, NULL, pAddresses, &ulOutBufLen)) 
	{
		pAddress = pAddresses;
        while (pAddress) 
		{
			if (pAddress->OperStatus == IfOperStatusUp)
			{
				SNetworkAdaptersInfo adapter;
				ConvertMultibyteToUC(EC_UTF8, pAddress->AdapterName, adapter.m_strGUID);
				adapter.m_strDescription = pAddress->Description;
				adapter.m_strName = pAddress->FriendlyName;
				adapter.m_strAddress = L"0.0.0.0";

                std::string mac_str;
                for (size_t i = 0; i < pAddress->PhysicalAddressLength; i++)
                    mac_str += decode_char_to_hex(pAddress->PhysicalAddress[i]);
				ConvertMultibyteToUC(EC_UTF8, mac_str.c_str(), adapter.m_MAC);

				if (pAddress->FirstUnicastAddress != NULL)
				{
					wchar_t szAddress[NI_MAXHOST];
					if (GetNameInfoW(pAddress->FirstUnicastAddress->Address.lpSockaddr, 
						pAddress->FirstUnicastAddress->Address.iSockaddrLength,
						szAddress, sizeof(szAddress), NULL, 0, NI_NUMERICHOST) == 0) 
					{				
						adapter.m_strAddress = szAddress;
					}
					adapters.push_back(adapter);
				}
			}

			pAddress = pAddress->Next;
		}
		
		bSuccess = true;
	}

	free(pAddresses);

    WSACleanup();

#elif defined(__APPLE__)

    ifaddrs* ifap = 0;

    if (getifaddrs(&ifap) == 0)
    {
        for(ifaddrs* p = ifap; p != 0; p = p->ifa_next)
        {
            try
            {
                char* name = p->ifa_name;

                if (name)
                {
                    SNetworkAdaptersInfo a;
                    a.m_strName = string_cast<EC_UTF8>(name);

                    if ((p->ifa_addr)->sa_family == AF_LINK)
                    {
                        // The ifa_addr field references either the address of the
                        // interface or the link level address of the interface,
                        // if one exists, otherwise it is NULL.
                        //
                        unsigned char* mac = (unsigned char*)(p->ifa_addr);

                        if (mac)
                        {
                            char smac[16] = { 0 };
                            sprintf(smac, "%02X%02X%02X%02X%02X%02X",
                                mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);                            
                            a.m_strGUID = string_cast<EC_UTF8>(std::string(smac));
                            a.m_MAC = a.m_strGUID;
                        }
                    }
                    else if ((p->ifa_addr)->sa_family == AF_INET)
                    {
                        sockaddr_in* sa = (sockaddr_in*)(p->ifa_addr);

                        if (sa)
                        {
                            std::string addr = inet_ntoa(sa->sin_addr);                            
                            a.m_strAddress = string_cast<EC_UTF8>(addr);
                        }
                    }

                    bool found = false;

                    for (size_t j = 0; j < adapters.size(); j++)
                    {
                        SNetworkAdaptersInfo& b = adapters[j];
                        
                        if (b.m_strName == a.m_strName)
                        {
                            if (!a.m_strAddress.empty())
                            {
                                b.m_strAddress = a.m_strAddress;
                            }
                            else if (!a.m_strGUID.empty())
                            {
                                b.m_strGUID = a.m_strGUID;
                                b.m_MAC = a.m_MAC;
                            }

                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        adapters.push_back(a);
                    }
                }
            }
            catch (...)
            {
            }
        }

        freeifaddrs(ifap);
    }

    bSuccess = !adapters.empty();

#else
    int s = -1;
    ifreq* ifr = NULL;

    ifconf ifc;
    memset(&ifc, 0, sizeof(ifc));
    ifc.ifc_ifcu.ifcu_req = NULL;
    ifc.ifc_len = 0;

    do
    {
        s = socket(AF_INET, SOCK_DGRAM, 0);

        if (s < 0)
        {
            break;
        }

        if ((ioctl(s, SIOCGIFCONF, &ifc) < 0) || (ifc.ifc_len <= 0))
        {
            break;
        }

        ifr = (ifreq*)malloc(ifc.ifc_len);
        ifc.ifc_ifcu.ifcu_req = ifr;

        if (ioctl(s, SIOCGIFCONF, &ifc) < 0)
        {
            break;
        }

        int numif = ifc.ifc_len / sizeof(struct ifreq);

        for (int i = 0; i < numif; i++)
        {
            struct ifreq *r = &ifr[i];
            struct sockaddr_in *sin = (struct sockaddr_in *)&r->ifr_addr;

            SNetworkAdaptersInfo adapter;
            ConvertMultibyteToUC(EC_UTF8, r->ifr_name, adapter.m_strName);
            ConvertMultibyteToUC(EC_UTF8, inet_ntoa(sin->sin_addr),
                adapter.m_strAddress);
            std::string mac_str;
            
            for (int j = 0; j < 6; j++)
            {
                mac_str += decode_char_to_hex(r->ifr_hwaddr.sa_data[j]);
            }

            ConvertMultibyteToUC(EC_UTF8, mac_str.c_str(), adapter.m_strGUID);
            adapter.m_MAC = adapter.m_strGUID;
            //adapter.m_bDefault = false;

            adapters.push_back(adapter);
        }

        bSuccess = (adapters.empty() ? false : true);
    }
    while (0);

    if (s >= 0)
    {
        close(s);
    }

    if (ifr)
    {
        free(ifr);
    }
#endif

	return bSuccess;
}

bool network_helper::get_local_ip_address(const std::wstring& network_adapter, std::wstring& ip_address)
{
    bool bSuccess = false;
    ip_address = L"0.0.0.0";

    TNetworkAdaptersInfo adapters;
    if (get_local_net_adapters(adapters))
    {
        TNetworkAdaptersInfo::const_iterator it = adapters.begin();
        while (it != adapters.end())
        {
            if (it->m_strGUID == network_adapter)
            {
                ip_address = it->m_strAddress;
                bSuccess = true;
                break;
            }
            ++it;
        }
    }
    return bSuccess;
}

bool network_helper::get_remote_host_by_addr(const std::string& ip_address, SNetworkHostInfo& host_info)
{
    bool bSuccess = false;

#ifdef _WIN32
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif

    struct hostent* remote_host;
    struct in_addr addr = { 0 };
    addr.s_addr = inet_addr(ip_address.c_str());
    if (INADDR_NONE != addr.s_addr) 
    {
        remote_host = gethostbyaddr((char *)&addr, 4, AF_INET);
        if (NULL != remote_host)
        {
            host_info.name = remote_host->h_name;
            host_info.ip_addr = ip_address;
            bSuccess = get_remote_mac_addr(ip_address, host_info.mac_addr);
        }
    }

#ifdef _WIN32
    WSACleanup();
#endif

    return bSuccess;
}

bool network_helper::get_remote_host_by_name(const std::string& host_name, SNetworkHostInfo& host_info)
{
    bool success = false;

#ifdef _WIN32
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif

    LPHOSTENT lphost;
    lphost = gethostbyname(host_name.c_str());
    if (NULL != lphost) 
    { 
        host_info.name = host_name;
        host_info.ip_addr = inet_ntoa(*(LPIN_ADDR)lphost->h_addr);
        success = get_remote_mac_addr(host_info.ip_addr, host_info.mac_addr);
    }

#ifdef _WIN32
    WSACleanup();
#endif

    return success;
}

bool network_helper::get_remote_mac_addr(const std::string& ip_address, std::string& mac_addr)
{
    bool success = false;

#ifdef _WIN32
    PMIB_IPNETTABLE pIPNetTable = NULL;
    pIPNetTable = (MIB_IPNETTABLE*)malloc(sizeof(MIB_IPNETTABLE));
    ULONG ulOutBufLen = sizeof(MIB_IPNETTABLE);

    if (ERROR_INSUFFICIENT_BUFFER == GetIpNetTable(pIPNetTable, &ulOutBufLen, 0))
    {
        free(pIPNetTable);
        pIPNetTable = (MIB_IPNETTABLE*)malloc(ulOutBufLen);
    }

    if (NO_ERROR == GetIpNetTable(pIPNetTable, &ulOutBufLen, 0))
    {
        for (DWORD i = 0; i < pIPNetTable->dwNumEntries; i++)
        {
            if (pIPNetTable->table[i].dwAddr == inet_addr(ip_address.c_str()))
            {
				unsigned char* phys_addr = (unsigned char*)&pIPNetTable->table[i].bPhysAddr;
                for (int j = 0; j < MAC_ADDRESS_LENGTH; j++)
                {
                    mac_addr += decode_char_to_hex(phys_addr[j]);
				}
                success = true;
			}
		}
	}
#endif

    return success;
}

bool network_helper::wakeup_remote_host(const std::string& mac_addr)
{
    bool success = false;
    unsigned char magic_packet[WAKEUP_PACKET_LENGTH];

    if ((unsigned)MAC_ADDRESS_LENGTH == mac_addr.length() / 2)
    {
        // 6 bytes with 0xFF
        for (int i = 0; i < MAC_ADDRESS_LENGTH; i++)
        {
            magic_packet[i] = 0xFF;
        }

        // 6 bytes mac address
        const char* mac = mac_addr.c_str();
        for (int i = 0; i < MAC_ADDRESS_LENGTH; i++)
        {            
            magic_packet[i + MAC_ADDRESS_LENGTH] = decode_hex_to_char(&mac[i * 2]);
        }

        // 90 bytes with repeat mac
        for (int i = 0; i < MAC_ADDRESS_REPEAT_IN_PACKET; i++)
        {
            for (int j = 0; j < MAC_ADDRESS_LENGTH; j++)
            {
                magic_packet[(i + 2) * MAC_ADDRESS_LENGTH + j] = magic_packet[MAC_ADDRESS_LENGTH + j];
            }
        }

#ifdef _WIN32
        // Send packet
        WSADATA wsaData;
        WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif
        SOCKET udp_socket;
        if (INVALID_SOCKET != (udp_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)))
        {
            int optval = 1;
            if (SOCKET_ERROR != setsockopt(udp_socket, SOL_SOCKET, SO_BROADCAST, (char *)&optval, sizeof(int)))
            {
                sockaddr_in clientAddr;		
                clientAddr.sin_family = AF_INET;
                clientAddr.sin_port = htons(WAKEUP_PORT);
				clientAddr.sin_addr.s_addr = INADDR_BROADCAST;
                if (SOCKET_ERROR != sendto(udp_socket, (const char*)magic_packet, WAKEUP_PACKET_LENGTH, 0, 
                    (SOCKADDR *)&clientAddr, sizeof(clientAddr)))
                {
                    success = true;
                }       
            }
            closesocket(udp_socket);
        }
#ifdef _WIN32
        WSACleanup();
#endif
    }

    return success;
}

unsigned char network_helper::decode_hex_to_char(const std::string& str)
{
    if (str.length() > 1)
    {
        char hex_value[3];
        memset(hex_value, 0, 3);
        memcpy(hex_value, str.c_str(), 2);                
        return (const unsigned char)strtol(hex_value, NULL, 16);
    }
    return 0x00;
}

const std::string network_helper::decode_char_to_hex(unsigned char ch)
{
	int ich = ch;
    std::ostringstream ostr;
	ostr << std::setw( 2 ) << std::setfill('0') << std::hex << std::uppercase << ich;
	return ostr.str();
}

EDL_NET_PROTOCOLS network_helper::parse_net_url(const char* url, std::string& address, std::string& user, std::string& pswd, 
    unsigned short& port_num, std::string& url_suffix)
{
    EDL_NET_PROTOCOLS ret_val = DL_NET_PROTO_UNKNOWN;
    
    address.clear();
    user.clear();
    pswd.clear();
    url_suffix.clear();
    
	// Get protocol type
	size_t prefixLength;
	if (strncasecmp(url, proto_text[DL_NET_PROTO_UDP], strlen(proto_text[DL_NET_PROTO_UDP])) == 0)
	{
	    ret_val = DL_NET_PROTO_UDP;
		prefixLength = (int)strlen(proto_text[DL_NET_PROTO_UDP]);
		port_num = proto_default_ports[DL_NET_PROTO_UDP];
	}
	else if (strncasecmp(url, proto_text[DL_NET_PROTO_RTP], strlen(proto_text[DL_NET_PROTO_RTP])) == 0)
	{
	    ret_val = DL_NET_PROTO_RTP;
		prefixLength = (int)strlen(proto_text[DL_NET_PROTO_RTP]);
		port_num = proto_default_ports[DL_NET_PROTO_RTP];
	}
	else if (strncasecmp(url, proto_text[DL_NET_PROTO_HTTP], strlen(proto_text[DL_NET_PROTO_HTTP])) == 0)
	{
	    ret_val = DL_NET_PROTO_HTTP;
		prefixLength = (int)strlen(proto_text[DL_NET_PROTO_HTTP]);
		port_num = proto_default_ports[DL_NET_PROTO_HTTP];
	} 
	else if (strncasecmp(url, proto_text[DL_NET_PROTO_RTSP], strlen(proto_text[DL_NET_PROTO_RTSP])) == 0)
	{
	    ret_val = DL_NET_PROTO_RTSP;
		prefixLength = (int)strlen(proto_text[DL_NET_PROTO_RTSP]);
		port_num = proto_default_ports[DL_NET_PROTO_RTSP];
	}
	else if (strncasecmp(url, proto_text[DL_NET_PROTO_HTTPS], strlen(proto_text[DL_NET_PROTO_HTTPS])) == 0)
	{
	    ret_val = DL_NET_PROTO_HTTPS;
		prefixLength = (int)strlen(proto_text[DL_NET_PROTO_HTTPS]);
		port_num = proto_default_ports[DL_NET_PROTO_HTTPS];
	}
	else if (strncasecmp(url, proto_text[DL_NET_PROTO_RTMP], strlen(proto_text[DL_NET_PROTO_RTMP])) == 0)
	{
	    ret_val = DL_NET_PROTO_RTMP;
		prefixLength = (int)strlen(proto_text[DL_NET_PROTO_RTMP]);
		port_num = proto_default_ports[DL_NET_PROTO_RTMP];
	}
	else
	{
		return DL_NET_PROTO_UNKNOWN;
	}

    int parseBufferSize = strlen(url) + 1;
    std::string parseBufferStr;
    parseBufferStr.resize(parseBufferSize);
    char* parseBuffer = &parseBufferStr[0];
    char const* from = &url[prefixLength];

    //check for @ sign and get user name and password
    char* amp_ch = strchr((char*)from, netparam_delimiter);
    
    if (amp_ch != NULL)
    {
        // valid URL: http://nasa-f.akamaihd.net/public_h264_700@54826
        char* slash_ch = strchr((char*)from, '/');

        if ((slash_ch == NULL) || (slash_ch > amp_ch))
        {
            char* col_ch = strchr((char*)from, column_delimiter);

            if (col_ch != NULL && col_ch < amp_ch)
            {
                int len = col_ch - from;
                user.assign(from, from + len);

                len = amp_ch - col_ch - 1;
                pswd.assign(col_ch + 1, col_ch + 1 + len);
            }
            else if ((col_ch == NULL) || (col_ch > amp_ch))
            {
                // username without password
                //
                int len = amp_ch - from;
                user.assign(from, from + len);
            }            

            from = amp_ch + 1;
        }
    }

    char* to = &parseBuffer[0];
    int i;
    for (i = 0; i < parseBufferSize; ++i) 
	{
		if (*from == '\0' || *from == column_delimiter || *from == '/') 
		{
			// We've completed parsing the address
			*to = '\0';
			break;
		}
		*to++ = *from++;
	}
    if (i == parseBufferSize) 
	{
		return DL_NET_PROTO_UNKNOWN;
    }

    address = parseBuffer;
	
	// Get port
    char nextChar = *from;
    if (nextChar == column_delimiter) 
	{
		int portNumInt;
		if (sscanf(++from, "%d", &portNumInt) != 1) 
		{
			return DL_NET_PROTO_UNKNOWN;
		}
		if (portNumInt < 1 || portNumInt > 65535) 
		{
			return DL_NET_PROTO_UNKNOWN;
		}
		port_num = (unsigned short)portNumInt;
		while (*from >= '0' && *from <= '9')
		{
			++from; // skip over port number
		}
	}

    // The remainder of the URL is the suffix
	url_suffix = from;

	//if url_suffix is empty - it is '/'
	if (url_suffix.size() == 0)
		url_suffix = "/";
	
    return ret_val;
}

const char* network_helper::get_proto_string(EDL_NET_PROTOCOLS proto)
{
    return proto_text[proto];
}

EDL_NET_PROTOCOLS network_helper::get_proto(const char* url)
{
	std::string address;
	std::string user;
	std::string password;
	unsigned short portNum;
	std::string urlSuffix;
	return parse_net_url(url, address, user, password, portNum, urlSuffix);
}

unsigned long network_helper::get_ip_address(const std::wstring& host_name)
{
	unsigned long ip_address;
	
    std::string str_address;
    ConvertUCToMultibyte(EC_UTF8, host_name.c_str(), str_address);
    ip_address = inet_addr(str_address.c_str());
    if (INADDR_NONE == ip_address || INADDR_ANY == ip_address)
    {
        LPHOSTENT lphost;
        lphost = gethostbyname(str_address.c_str());
        if (NULL == lphost) 
        { 
            return INADDR_NONE;
        }
        ip_address = ((LPIN_ADDR)lphost->h_addr)->s_addr;
    }

	return ip_address;
}

bool network_helper::is_external_network_initialized()
{
#ifdef _WIN32
		bool retval = false;
	PIP_ADAPTER_INFO pAdapterInfo;
	PIP_ADAPTER_INFO pAdapter = NULL;
	unsigned long dwRetVal = 0;

	pAdapterInfo = (IP_ADAPTER_INFO*)malloc(sizeof(IP_ADAPTER_INFO));
	ULONG ulOutBufLen = sizeof(IP_ADAPTER_INFO);
	if (GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW)
	{
		free(pAdapterInfo);
		pAdapterInfo = (IP_ADAPTER_INFO*)malloc(ulOutBufLen);
	}

	if (GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == NO_ERROR) 
	{
		pAdapter = pAdapterInfo;
		while (pAdapter) 
		{
			if (strcmp("0.0.0.0", pAdapter->IpAddressList.IpAddress.String) != 0)
			{
				retval = true;
				break;
			}		
			pAdapter = pAdapter->Next;
		}
	}

	free(pAdapterInfo);	
	return retval;
#else
    TNetworkAdaptersInfo adapters;
    get_local_net_adapters(adapters);
    
    for (size_t j = 0; j < adapters.size(); j++)
    {
        if (adapters[j].m_strName != L"lo")
        {
            return true;
        }
    }

    return false;
#endif
}

bool network_helper::wait_for_network_initialization(time_t seconds)
{
    // wait until at least one network interface becomes active    
    const int delay = 200;
    int count = (seconds > 0) ? int((seconds * 1000) / delay) : 0;
    bool initialized = false;

    for (int j = 0; ; j++)
    {
        if ((count > 0) && (j > count))
        {
            break;
        }        

        if (network_helper::is_external_network_initialized())
        {
            initialized = true;
            break;
        }

        dvblink::engine::sleep(delay);
    }

    return initialized;
}

bool network_helper::is_tcp_port_available(unsigned short port)
{
    if (port == 0)
    {
        return false; // invalid param
    }

    tcp_socket sock;
    sock_addr addr;
    addr.set_port(port);
    return (sock.bind(addr) == err_none);
}

bool network_helper::is_udp_port_available(unsigned short port)
{
    if (port == 0)
    {
        return false; // invalid param
    }

    udp_socket sock;
    sock_addr addr;
    addr.set_port(port);
    return (sock.bind(addr) == err_none);
}

#define HOST_NAME_MAX_SIZE  256

bool network_helper::get_local_hostname(std::string& hostname)
{
    bool ret_val = false;

    char buf[HOST_NAME_MAX_SIZE] = { 0 };
	
    if (gethostname(buf, HOST_NAME_MAX_SIZE) == 0)
	{
		hostname = buf;
        ret_val = true;
	}

    return ret_val;
}

void network_helper::parse_http_suffix_params(const std::string& suffix, std::map<std::string, std::string>& http_params)
{
    //find question mark. Everything after it - params
    size_t qm_pos = suffix.find("?");
    if (qm_pos != std::string::npos)
    {
        std::string params = suffix.substr(qm_pos + 1);
        //split params string on x=y tokens
        typedef boost::tokenizer<boost::char_separator<char>, std::string::const_iterator, std::string> tokenizer_t;
        boost::char_separator<char> sep("&", 0, boost::keep_empty_tokens);
        tokenizer_t tokens(params, sep);

        for (tokenizer_t::iterator tok_iter = tokens.begin(); tok_iter != tokens.end(); ++tok_iter)
        {
            char key_buf[512];
            char param_buf[512];
            int n = sscanf((*tok_iter).c_str(), "%[^=]=%s", key_buf, param_buf);
            if (n == 2)
                http_params[key_buf] = param_buf;
        }
    }
}

}
