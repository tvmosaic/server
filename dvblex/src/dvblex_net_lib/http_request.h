/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#ifndef __DVBLINK_HTTP_REQUEST_H__
#define __DVBLINK_HTTP_REQUEST_H__

#include <string>
#include <curl/curl.h>
#include <boost/thread.hpp>
#include <boost/cstdint.hpp>
#include <boost/shared_ptr.hpp>

namespace dvblink {

class http_request
{
public:
    struct callbacks
    {
        virtual void on_data_received(const void* data, size_t size) = 0;
        virtual void on_header_received(const std::string& hdr) = 0;
        virtual void on_request_completed(bool success) = 0;
        virtual ~callbacks(){}
    };

    typedef boost::shared_ptr<callbacks> callbacks_ptr;

public:
    http_request(const std::string& url, callbacks_ptr cb_ptr);
    virtual ~http_request();

    bool set_credentials(const std::string& username,
        const std::string& password,
        unsigned long curl_auth = CURLAUTH_ANY);

    bool set_ca_cert_path(const std::string& cert_path,
        bool verify_peer = true);
    bool set_client_cert_path(const std::string& cert_path);
    bool set_client_cert(const std::string& pem, const std::string& key);

    bool set_user_agent(const std::string& user_agent);
	
    bool set_cookie_file(const std::string& file_path);
    bool set_port(boost::uint16_t port);    

    bool add_http_header(const std::string& http_header);
    bool set_http_headers();
    bool reset_http_headers();

    bool close();

    void close_socket();

    virtual bool execute(long timeout_sec, std::string& effective_url);
    virtual bool cancel();

private:
    static size_t on_data_received(void* ptr,
        size_t size, size_t nmemb, void* data);    
    static size_t on_header_received(void* ptr,
        size_t size, size_t nmemb, void* data);
    static int on_progress(void* clientp,
    		double dltotal, double dlnow, double ultotal, double ulnow);
    static int socket_cb(void *clientp, curl_socket_t curlfd, curlsocktype purpose);

public:
    // CURLOPT_SSL_CTX_DATA and CURLOPT_SSL_CTX_FUNCTION
    //
    struct ssl_ctx_data
    {
        std::string pem;
        std::string key;
    };

private:
    ssl_ctx_data ssl_ctx_data_;

protected:
    static boost::mutex global_lock_;    
    struct global_init;
    static boost::shared_ptr<global_init> global_init_ptr_;
    std::string ca_cert_path_;
    CURL* curl_handle_;
    struct curl_slist* http_headers_;
    curl_socket_t socket_;

    callbacks_ptr cb_ptr_;
    mutable boost::mutex lock_;
    bool canceled_;
    bool running_;
    char errbuf_[CURL_ERROR_SIZE];
};

typedef boost::shared_ptr<http_request> http_request_ptr;

class http_get : public http_request
{
public:
    http_get(const std::string& url, callbacks_ptr cb_ptr);

    // to reuse the same connection
    bool set_new_url(const std::string& url);
};

class http_post : public http_request
{
public:
    http_post(const std::string& url, callbacks_ptr cb_ptr,
        const void* data, size_t size);

private:
    std::string data_;
};

class http_delete : public http_request
{
public:
    http_delete(const std::string& url, callbacks_ptr cb_ptr);
};

class http_put : public http_request
{
public:
    http_put(const std::string& url, callbacks_ptr cb_ptr,
        const void* data, size_t size);

private:
    std::string data_;
};

} // namespace dvblink

#endif  // __DVBLINK_HTTP_REQUEST_H__

// $Id: http_request.h 12327 2016-01-08 14:32:51Z pashab $
