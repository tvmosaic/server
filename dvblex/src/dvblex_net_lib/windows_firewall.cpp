/*
Copyright © 2021 DVBLogic

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/

#include "stdafx.h"
#include "dl_windows_firewall.h"
#include "dl_win_xp_firewall.h"
#include "dl_vista_firewall.h"



CWinFirewall::CWinFirewall() :
    m_pXPFirewall(NULL),
    m_pVistaFirewall(NULL)
{
    OSVERSIONINFO ov;
    ov.dwOSVersionInfoSize = sizeof(ov);

    GetVersionEx(&ov);
    if (ov.dwMajorVersion == 5) // XP
    {
        m_pXPFirewall = new CDLWinXPFirewall();
    }
    else    // Vista and above
    {
        m_pVistaFirewall = new CDLVistaFirewall();
    }
}

CWinFirewall::~CWinFirewall()
{
    delete m_pXPFirewall;
    delete m_pVistaFirewall;
}

FW_ERROR_CODE CWinFirewall::Initialize()
{
    FW_ERROR_CODE err;
    if (m_pXPFirewall)
    {
        err = m_pXPFirewall->Initialize();
    }
    else
    {
        err = m_pVistaFirewall->Initialize();
    }
    return err;
}

FW_ERROR_CODE CWinFirewall::Uninitialize()
{
    FW_ERROR_CODE err;
    if (m_pXPFirewall)
    {
        err = m_pXPFirewall->Uninitialize();
    }
    else
    {
        err = m_pVistaFirewall->Uninitialize();
    }
    return err;
}

FW_ERROR_CODE CWinFirewall::AddRule(const wchar_t* lpszProcessImageFileName,
    const wchar_t* lpszRuleName, const wchar_t* lpszRuleGroupName)
{
    FW_ERROR_CODE err;
    if (m_pXPFirewall)
    {
        err = m_pXPFirewall->AddApplication(lpszProcessImageFileName, lpszRuleName);
    }
    else
    {
        err = m_pVistaFirewall->AddRule(lpszProcessImageFileName, lpszRuleName, lpszRuleGroupName);
    }
    return err;
}

FW_ERROR_CODE CWinFirewall::RemoveRule(const wchar_t* lpszRuleName)
{
    FW_ERROR_CODE err;
    if (m_pXPFirewall)
    {
        err = m_pXPFirewall->RemoveApplication(lpszRuleName);
    }
    else
    {
        err = m_pVistaFirewall->RemoveRule(lpszRuleName);
    }
    return err;
}




