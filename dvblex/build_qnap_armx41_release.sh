#!/bin/bash

mkdir -p /opt/cross-project
ln -sf /opt/sdk/tvmosaic/qnap/armx41 /opt/cross-project/arm

CROSS_COMPILER="/opt/sdk/tvmosaic/qnap/armx41/linaro/bin/arm-linux-gnueabihf-"

CONFIG_TYPE=qnap-armx41-release

mkdir ${CONFIG_TYPE}
cd ${CONFIG_TYPE}

cmake .. -DTVM_BUILD_TYPE="Release" -DTVM_CONFIG_TYPE="${CONFIG_TYPE}" \
 -DTVM_CFLAGS="-funroll-loops -fPIC -DNDEBUG -D__USE_LARGEFILE64 -D__USE_FILE_OFFSET64 -D_FILE_OFFSET_BITS=64 -D_QNAP_X41" \
 -DCMAKE_SYSTEM_NAME="Linux" -DCMAKE_C_COMPILER="${CROSS_COMPILER}gcc" -DCMAKE_CXX_COMPILER="${CROSS_COMPILER}g++" \
 -DTVM_CROSS="${CROSS_COMPILER}" -DCMAKE_EXE_LINKER_FLAGS="-lrt"

CORES=`getconf _NPROCESSORS_ONLN`

make -j $CORES

cd ..
