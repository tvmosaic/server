#!/bin/bash

#search_path="/Users/dev/work/svn5/dvblex/dvblex/../stage/darwin-x86_64-release/lib"
search_path=$1
replace_path=$search_path

chmod +rw ${search_path}/*.dylib

remove_install_path() {
  local inst_path=$1
  local dep=$2
  local depname=$(basename $dep)

  install_name_tool -id $depname $dep

  otool -L $dep | awk -v var1="$inst_path.*\.dylib" '$0 ~ var1 {print $1}' | while read lib; do
    local libname=$(basename $lib)
#    echo $lib
#    echo $libname
    install_name_tool -change $lib $libname $dep
  done
}

for i in $(find $search_path -maxdepth 1 -name '*.dylib'); do

remove_install_path $replace_path "$i";

done

#remove_install_path /Users/dev/work/svn5/dvblex/dvblex/../stage/darwin-x86_64-release/lib libcurl.4.dylib



