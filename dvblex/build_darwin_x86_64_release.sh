#!/bin/bash

if [[ $OSTYPE == 'darwin'* ]]; then

CONFIG_TYPE=darwin-x86_64-release

mkdir ${CONFIG_TYPE}
cd ${CONFIG_TYPE}

cmake .. -DTVM_BUILD_TYPE="Release" -DTVM_CONFIG_TYPE="${CONFIG_TYPE}" \
 -DTVM_CFLAGS="-mmmx -fPIC -DNDEBUG -D__USE_LARGEFILE64 -D__USE_FILE_OFFSET64 -D_FILE_OFFSET_BITS=64 -D_DARWIN_X86_64" \
 -DCMAKE_SYSTEM_NAME="Darwin" -DCMAKE_C_COMPILER="gcc" -DCMAKE_CXX_COMPILER="g++" -DTVM_BUILD_PYTHON="1" \
 -DCMAKE_EXE_LINKER_FLAGS="-framework CoreFoundation -framework IOKit -framework Carbon"

CORES=`getconf _NPROCESSORS_ONLN`

make -j $CORES

cd ..

else
	echo 'this build script shall only run on macos'
fi

