@@Echo off
:: variables
SET CMD_FILE_EXPORT="temp_cmd_export.txt"
SET CMD_FILE_IMPORT="temp_cmd_import.txt"
SET DUMP_FILE="dump_all.sql"
SET BACKUP_DIR="backup"

set TIMESTAMP=%TIME%
set /A TIMESTAMP=(1%TIMESTAMP:~0,2%-100)*360000 + (1%TIMESTAMP:~3,2%-100)*6000 + (1%TIMESTAMP:~6,2%-100)*100 + (1%TIMESTAMP:~9,2%-100)

CD %2
mkdir %BACKUP_DIR%

:delete old files (if any)
del /F /Q %DUMP_FILE%
del /F /Q %CMD_FILE_EXPORT%
del /F /Q %CMD_FILE_IMPORT%

::create command files
echo .mode insert >> %CMD_FILE_EXPORT%
echo .output dump_all.sql >>  %CMD_FILE_EXPORT%
echo .dump >>  %CMD_FILE_EXPORT%
echo .exit >>  %CMD_FILE_EXPORT%

echo .read dump_all.sql >>  %CMD_FILE_IMPORT%
echo .exit >>  %CMD_FILE_IMPORT%

:: export
%1\sqlite3.exe recorder_database.db < %CMD_FILE_EXPORT%

:: move recorder_database.db recorder_database_old.db
move recorder_database.db %BACKUP_DIR%\recorder_database_%TIMESTAMP%.db

:: import data
%1\sqlite3.exe recorder_database.db < %CMD_FILE_IMPORT%

del /F /Q %DUMP_FILE%
del /F /Q %CMD_FILE_EXPORT%
del /F /Q %CMD_FILE_IMPORT%
