#!/bin/bash

CROSS_COMPILER="/opt/sdk/tvmosaic/raspberry/gcc-linaro-4.9.4-2017.01-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-"

CONFIG_TYPE=raspberry-arm-release

mkdir ${CONFIG_TYPE}
cd ${CONFIG_TYPE}

cmake .. -DTVM_BUILD_TYPE="Release" -DTVM_CONFIG_TYPE="${CONFIG_TYPE}" \
 -DTVM_CFLAGS="-fPIC -DNDEBUG -D__USE_LARGEFILE64 -D__USE_FILE_OFFSET64 -D_FILE_OFFSET_BITS=64 -D_RASPBERRY_ARM -march=armv7-a -mfpu=neon -mfloat-abi=hard -funroll-loops" \
 -DCMAKE_SYSTEM_NAME="Linux" -DCMAKE_C_COMPILER="${CROSS_COMPILER}gcc" -DCMAKE_CXX_COMPILER="${CROSS_COMPILER}g++" \
 -DTVM_CROSS="${CROSS_COMPILER}"

CORES=`getconf _NPROCESSORS_ONLN`

make -j $CORES

cd ..
